

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.IO
Public Class CompDiv
    Inherits System.Web.UI.Page
    Protected WithEvents ovid249 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid248 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents btnaddtosite As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang2047 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2046 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2045 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2044 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2043 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2042 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2041 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2040 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2039 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2038 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2037 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2036 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2035 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2034 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2033 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2032 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2031 As System.Web.UI.WebControls.Label


    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    'Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim comp As New Utilities
    Dim dr As SqlDataReader
    Dim eq, fu, sql, dt, val, filt, cid, co, cod, coid, sid, did, clid, ro, appstr, typ, Login, usr, isecd, who, flg, chld, rtf As String
    Dim rpn, det As String
    Dim comi As New mmenu_utils_a
    Dim mu As New mmenu_utils_a
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchld As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpic As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtnewcomp As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtnewdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnAddFu As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtconame As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdesig As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbfailmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents txtnewfail As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnaddfail As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imgco As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents bte As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents bde As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btf As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents lblcur As System.Web.UI.WebControls.Label
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents lblcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnsave As System.Web.UI.WebControls.ImageButton
    Protected WithEvents bdf As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents MyFile As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents btnupload As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents po As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents pr As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ne As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btndel As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblcompchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblparcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btngtt As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btndelepic As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbltaskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbdev As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbopt As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbldel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbtdev As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbtopt As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents txtiorder As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents imgdel As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgsavdet As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblpcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrbimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbliorders As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovtimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrtimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcohold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtkey As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmfg As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddac As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cbatcl As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents tdcclass As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnaddasset As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btncget As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdsclass As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btncrem As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddoca As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblfailmode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbfailcomp As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblswitch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents aswitch As System.Web.UI.WebControls.Label
    Protected WithEvents imgchk As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblisecd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgetecd2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgetecd3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrecd2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewecd3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblecd1id As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblecd2txt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewecd1id As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdgtt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdgttrb As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdopts As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcomi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbrtf As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents txtrpn As System.Web.UI.WebControls.TextBox
    Protected WithEvents cbdet As System.Web.UI.HtmlControls.HtmlInputCheckBox

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        flg = "0"
        GetFSOVLIBS()



        GetFSLangs()
        Dim coi As String = comi.COMPI
        lblcomi.Value = coi
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
        End Try
        If Request.Form("lblcompchk") = "1" Then
            lblcompchk.Value = ""
            co = lblcoid.Value
            comp.Open()
            PopComp(co)
            comp.Dispose()
        ElseIf Request.Form("lblcompchk") = "delimg" Then
            lblcompchk.Value = ""
            comp.Open()
            DelImg()
            LoadPics()
            comp.Dispose()
            'lblchksav.Value = "yes"
        ElseIf Request.Form("lblcompchk") = "checkpic" Then
            lblcompchk.Value = ""
            comp.Open()
            LoadPics()
            comp.Dispose()
        ElseIf Request.Form("lblcompchk") = "savedets" Then
            lblcompchk.Value = ""
            comp.Open()
            SaveDets()
            LoadPics()
            comp.Dispose()
        ElseIf Request.Form("lblcompchk") = "crem" Then
            lblcompchk.Value = ""
            co = lblcoid.Value
            comp.Open()
            CREM(co)
            PopComp(co)
            comp.Dispose()
        ElseIf Request.Form("lblcompchk") = "movecomp" Then
            'lblcompchk.Value = ""
            comp.Open()
            SaveComp()
            If flg <> "1" Then
                movecomp()
            End If
            PopComp(coid)
            comp.Dispose()
        End If

        If Not IsPostBack Then
            'Try
            Try
                chld = Request.QueryString("chld").ToString
                lblchld.Value = chld
                who = Request.QueryString("who").ToString
                lblwho.Value = who
                If who = "tab" Or who = "noloc" Or who = "eqready" Or who = "locready" Then
                    tdgtt.Attributes.Add("class", "details")
                    tdgttrb.Attributes.Add("class", "details")
                    tdopts.Attributes.Add("width", "744")
                End If
            Catch ex As Exception

            End Try
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                btnAddFu.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                btnAddFu.Enabled = False
                btndel.ImageUrl = "../images/appbuttons/minibuttons/deldis.gif"
                btndel.Enabled = False
                btnupload.Disabled = True
                'cbtrans.Disabled = True
                btnsave.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                btnsave.Attributes.Add("onclick", "")
                'imgproc.Attributes.Add("src", "../images/appbuttons/minibuttons/uploaddis.gif")
                'imgproc.Attributes.Add("onclick", "")
                'btnaddtosite.Attributes.Add("class", "details")
                btntocomp.Visible = False
                btnfromcomp.Visible = False
                todis.Attributes.Add("class", "view")
                fromdis.Attributes.Add("class", "view")
            End If
            Try
                usr = HttpContext.Current.Session("username").ToString
                lbluser.Value = usr
            Catch ex As Exception

            End Try
            appstr = HttpContext.Current.Session("appstr").ToString() '"all" '
            CheckApp(appstr)
            Dim test As String = Request.QueryString("sid").ToString
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            did = Request.QueryString("did").ToString
            lbldid.Value = did
            clid = Request.QueryString("clid").ToString
            lblclid.Value = clid
            eq = Request.QueryString("eqid").ToString
            lbleqid.Value = eq
            cid = "0" 'Request.QueryString("cid").ToString
            lblcid.Value = cid
            eq = Request.QueryString("eqid").ToString
            lbleqid.Value = eq
            fu = Request.QueryString("fuid").ToString
            lblfuid.Value = fu
            coid = Request.QueryString("coid").ToString
            lblcoid.Value = coid
            lblpar.Value = coid

            isecd = mu.ECD
            lblisecd.Value = isecd
            If isecd = "1" Then
                lbfailcomp.SelectionMode = ListSelectionMode.Single
                lbfailmaster.SelectionMode = ListSelectionMode.Single
                'btnAddFu.Attributes.Add("onclick", "getecd1();")
                btnAddFu.OnClientClick = "Javascript:return getecd1();"
            End If

            comp.Open()
            CheckTaskCnt(coid)
            'PopAC()
            lblfailmode.Value = "norm"
            lblswitch.Value = "all"
            imgchk.Attributes.Add("class", "details")
            popoca()
            PopComp(coid)
            comp.Dispose()
            'Catch ex As Exception

            'End Try
        Else

        End If
        btnsave.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov265", "CompDiv.aspx.vb") & "', ABOVE)")
        btnsave.Attributes.Add("onmouseout", "return nd()")
        btnsave.Attributes.Add("onclick", "FreezeScreen('Your Data is Being Processed...');")
        btndel.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov266", "CompDiv.aspx.vb") & "', ABOVE)")
        btndel.Attributes.Add("onmouseout", "return nd()")
        'btnAddFu.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'btnAddFu.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
        'btntocomp.Attributes.Add("onclick", "DisableButton(this);")
        'btnfromcomp.Attributes.Add("onclick", "DisableButton(this);")

    End Sub
    Private Sub movecomp()
        Dim newfu As String = lblnewfuid.Value
        fu = lblfuid.Value
        co = lblcoid.Value
        Dim coname As String = txtconame.Text
        Dim newconame As String
        Dim cocnt, newcnt As Integer
        newcnt = 0
        sql = "select count(*) from components where compnum = '" & coname & "' and func_id = '" & newfu & "'"
        cocnt = comp.Scalar(sql)
        If cocnt > 0 Then
            While cocnt > 0
                newcnt += 1
                newconame = coname & "_" & newcnt
                sql = "select count(*) from components where compnum = '" & newconame & "' and func_id = '" & newfu & "'"
                cocnt = comp.Scalar(sql)
                If cocnt = 0 Then
                    sql = "update components set compnum = '" & newconame & "' where comid = '" & co & "' and func_id = '" & fu & "'"
                    comp.Update(sql)
                End If
            End While
        End If
        sql = "exec usp_movecomp '" & fu & "', '" & newfu & "', '" & co & "'"
        comp.Update(sql)
        lblnewfuid.Value = ""
        lblfuid.Value = newfu
    End Sub
    Private Sub CREM(ByVal coid As String)
        sql = "update components set assetclass = NULL, acid = NULL, " _
           + "scid = NULL, subclass = NULL where comid = '" & coid & "'"
        comp.Update(sql)
    End Sub
    Private Sub popoca()
        sql = "select * from ocareas"
        dr = comp.GetRdrData(sql)
        ddoca.DataSource = dr
        ddoca.DataTextField = "ocarea"
        ddoca.DataValueField = "oaid"
        ddoca.DataBind()
        dr.Close()
        ddoca.Items.Insert(0, "ALL")
    End Sub
    Private Sub SaveDets()
        Dim iord As String = txtiorder.Text
        Try
            Dim piord As Integer = System.Convert.ToInt32(iord)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr836", "CompDiv.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim oldiord As Integer = lblcurrp.Value + 1
        If oldiord <> iord Then
            Dim old As String = lbloldorder.Value
            Dim neword As String = oldiord 'txtiorder.Text
            coid = lblcoid.Value
            typ = "co"
            sql = "usp_reordereqimg '" & typ & "','" & coid & "','" & neword & "','" & old & "'"
            comp.Update(sql)
        End If
    End Sub
    Private Sub DelImg()
        coid = lblcoid.Value
        Dim pid As String = lblimgid.Value
        Dim old As String = lbloldorder.Value
        typ = "co"
        sql = "usp_deleqimg '" & typ & "','" & coid & "','" & pid & "','" & old & "'"
        comp.Update(sql)
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim pic, picn, picm As String
        pic = lblcurrbimg.Value
        picn = lblcurrtimg.Value
        picm = lblcurrimg.Value
        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        'Try
        'If File.Exists(fpic) Then
        'File.Delete1(fpic)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicm) Then
        'File.Delete1(fpicm)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicn) Then
        'File.Delete1(fpicn)
        'End If
        'Catch ex As Exception

        'End Try
        lblcurrp.Value = "0"
    End Sub
    Private Sub LoadPics()
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/tpmimages/"
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        'lblnsimage.Value = nsimage
        'lblstrfrom.Value = strfrom
        'imgco.Attributes.Add("src", "../images/appimages/compimg.gif")
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgco.Attributes.Add("src", "../images2/eng/compimg.gif")
        ElseIf lang = "fre" Then
            imgco.Attributes.Add("src", "../images2/fre/compimg.gif")
        ElseIf lang = "ger" Then
            imgco.Attributes.Add("src", "../images2/ger/compimg.gif")
        ElseIf lang = "ita" Then
            imgco.Attributes.Add("src", "../images2/ita/compimg.gif")
        ElseIf lang = "spa" Then
            imgco.Attributes.Add("src", "../images2/spa/compimg.gif")
        End If

        Dim img, imgs, picid As String

        coid = lblcoid.Value
        Dim pcnt As Integer
        sql = "select count(*) from pmpictures where comid is not null and comid = '" & coid & "'"
        pcnt = comp.Scalar(sql)
        lblpcnt.Value = pcnt

        Dim currp As String = lblcurrp.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpg.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lblpg.Text = "Image 1 of " & pcnt
                lblcurrp.Value = "0"
            End If

            sql = "select p.pic_id, p.picurl, p.picurltn, p.picurltm, p.picorder " _
            + "from pmpictures p where p.comid is not null and p.comid = '" & coid & "'" _
            + "order by p.picorder"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs, timg As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs, ovtimg, ovtimgs
            dr = comp.GetRdrData(sql)
            While dr.Read
                iflag += 1
                img = dr.Item("picurltm").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)
                bimg = dr.Item("picurl").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)

                timg = dr.Item("picurltn").ToString
                Dim timgarr() As String = timg.Split("/")
                ovtimg = timgarr(timgarr.Length - 1)

                picid = dr.Item("pic_id").ToString
                order = dr.Item("picorder").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If

                If ovtimgs = "" Then
                    ovtimgs = ovtimg
                Else
                    ovtimgs += "," & ovtimg
                End If

                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimg.Value = ovimg
                    lblcurrbimg.Value = ovbimg
                    lblcurrtimg.Value = ovtimg
                    'If File.Exists(img) Then
                    imgco.Attributes.Add("src", img)
                    'End If
                    'imgco.Attributes.Add("src", img)
                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgid.Value = picid

                    txtiorder.Text = order
                End If
                If imgs = "" Then
                    imgs = picid & ";" & img
                Else
                    imgs += "~" & picid & ";" & img
                End If
            End While
            dr.Close()
            lblimgs.Value = imgs
            lblbimgs.Value = bimgs
            lblovimgs.Value = ovimgs
            lblovbimgs.Value = ovbimgs
            lblovtimgs.Value = ovtimgs
            lbliorders.Value = iorders
        End If
    End Sub
    Private Sub CheckApp(ByVal appstr)
        Dim apparr() As String = appstr.Split(",")
        Dim app As Integer = 0
        'eq,dev,opt,inv
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                If apparr(i) = "dev" Then
                    rbdev.Disabled = False
                    app += 1
                ElseIf apparr(i) = "opt" Then
                    rbopt.Disabled = False
                    rbopt.Checked = True
                    app += 1
                ElseIf apparr(i) = "tpd" Then
                    rbtdev.Disabled = False
                    app += 1
                ElseIf apparr(i) = "tpo" Then
                    rbtopt.Disabled = False
                    app += 1
                End If
            Next
            If app = 0 Then
                lblappstr.Value = "no"
            Else
                lblappstr.Value = "yes"
                If rbopt.Disabled = True And rbdev.Disabled = True Then
                    If rbtopt.Disabled = True Then
                        rbtdev.Checked = True
                        lblapp.Value = "rbdev"
                    Else
                        rbtopt.Checked = True
                        lblapp.Value = "rbopt"
                    End If
                Else
                    If rbopt.Disabled = True Then
                        rbdev.Checked = True
                        lblapp.Value = "pmdev"
                    Else
                        rbopt.Checked = True
                        lblapp.Value = "pmopt"
                    End If
                End If
            End If

        Else
            lblappstr.Value = "yes"
            lblapp.Value = "pmopt"
            rbdev.Disabled = False
            rbopt.Disabled = False
            rbopt.Checked = True
            rbtdev.Disabled = False
            rbtopt.Disabled = False
        End If
    End Sub
    Private Sub CheckApp1(ByVal appstr)
        Dim apparr() As String = appstr.Split(",")
        Dim app As Integer = 0
        'eq,dev,opt,inv
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                If apparr(i) = "dev" Then
                    rbdev.Disabled = False
                    app += 1
                ElseIf apparr(i) = "opt" Then
                    rbopt.Disabled = False
                    rbopt.Checked = True
                    app += 1
                End If
            Next
            If app = 0 Then
                lblappstr.Value = "no"
                lblapp.Value = "none"
            Else
                lblappstr.Value = "yes"
                If rbopt.Disabled = True Then
                    rbdev.Checked = True
                    lblapp.Value = "pmdev"
                Else
                    rbopt.Checked = True
                    lblapp.Value = "pmopt"
                End If
            End If

        Else
            lblappstr.Value = "yes"
            rbdev.Disabled = False
            rbopt.Disabled = False
            rbopt.Checked = True
            lblapp.Value = "pmopt"
        End If
    End Sub
    Private Sub CheckTaskCnt(ByVal coid As String)
        sql = "select count(*) from pmtasks where comid = '" & coid & "'"
        Dim tcnt As Integer
        tcnt = comp.Scalar(sql)
        lbltaskcnt.Value = tcnt
    End Sub
    Private Sub ddoca_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddoca.SelectedIndexChanged
        Dim coid As String = lblcoid.Value
        Dim cid As String = "0"
        comp.Open()
        isecd = lblisecd.Value
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If isecd = "1" And oca <> "ALL" Then
            checkecd()
        End If
        PopFailList(cid, coid)
        PopFailComp(cid, coid)
        comp.Dispose()
    End Sub
    Private Sub checkecd()
        isecd = lblisecd.Value
        Dim ecd2, ecd2id, ecd1id As String
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        Dim coid As String = lblcoid.Value
        ecd1id = lblecd1id.Value
        If isecd = "1" Then
            sql = "select count(*) from ecd2 where ecd1id = '" & ecd1id & "' and ecddesc = '" & oca & "'"
        Else
            sql = "select count(*) from componentfailmodes where oaid = '" & oaid & "' and comid = '" & coid & "'"
        End If

        Dim oaidcnt As Integer = comp.Scalar(sql)
        If oaidcnt = 0 Then
            lblcurrecd2.Value = ""
            lblnewecd3.Value = ""
            lblgetecd2.Value = "1"
            lblecd2txt.Value = oca
            'lblecd1id.Value = ""
        Else
            If isecd = "1" Then
                sql = "select ecd1id, ecd2id from ecd2 where ecd1id = '" & ecd1id & "' and ecddesc = '" & oca & "'"
            Else
                sql = "select e.ecd2, e.ecd1id, c.ecd2id from componentfailmodes c left join ecd2 e on e.ecd2id = c.ecd2id where c.oaid = '" & oaid & "' and c.comid = '" & coid & "'"
            End If

            dr = comp.GetRdrData(sql)
            While dr.Read
                ecd2id = dr.Item("ecd2id").ToString
                ecd1id = dr.Item("ecd1id").ToString
            End While
            dr.Close()
            lblcurrecd2.Value = ecd2id & "-" & ecd1id
            lblnewecd3.Value = ""
            lblgetecd2.Value = "0"
            lblecd1id.Value = ecd1id
        End If
    End Sub
    Private Sub PopFailComp(ByVal cid As String, ByVal coid As String)
        Dim faillist As New Utilities
        Dim lang As String = lblfslang.Value
        Dim coi As String = lblcomi.Value
        faillist.Open()
        If coi <> "CAS" And coi <> "AGR" And coi <> "LAU" And coi <> "GLA" Then
            sql = "select compfailid, failuremode from ComponentFailModes where " _
          + "comid = '" & coid & "' and oaid is null order by failuremode"
        Else
            If lang = "fre" Then
                sql = "select compfailid, ffailuremode from ComponentFailModes where " _
          + "comid = '" & coid & "' and oaid is null order by failuremode"
            Else
                sql = "select compfailid, failuremode from ComponentFailModes where " _
              + "comid = '" & coid & "' and oaid is null order by failuremode"
            End If
        End If
        
        dr = faillist.GetRdrData(sql)
        lbfailcomp.DataSource = dr
        lbfailcomp.DataValueField = "compfailid"
        If coi <> "CAS" And coi <> "AGR" And coi <> "LAU" And coi <> "GLA" Then
            lbfailcomp.DataTextField = "failuremode"
        Else
            If lang = "fre" Then
                lbfailcomp.DataTextField = "ffailuremode"
            Else
                lbfailcomp.DataTextField = "failuremode"
            End If
        End If

        lbfailcomp.DataBind()
        dr.Close()
        Dim fcnt As Integer = 0
        Dim sw As String = lblswitch.Value
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If oca = "All" Or ddoca.SelectedIndex = 0 Or ddoca.SelectedIndex = -1 Then
            lblfailmode.Value = "norm"
            imgchk.Attributes.Add("class", "details")
            lbfailcomp.Attributes.Add("class", "details")
            lbfailmaster.Attributes.Add("class", "view")
            aswitch.Text = "Available Failure Modes"
        Else

            sql = "select count(*) from ComponentFailModes where " _
          + "comid = '" & coid & "' and oaid is null"
            fcnt = faillist.Scalar(sql)
            If fcnt > 0 Then
                lblfailmode.Value = "rev"
                imgchk.Attributes.Add("class", "view")
                If sw = "comp" Then
                    lbfailcomp.Attributes.Add("class", "view")
                    lbfailmaster.Attributes.Add("class", "details")
                    aswitch.Text = "Unassigned Failure Modes"
                Else
                    lbfailcomp.Attributes.Add("class", "details")
                    lbfailmaster.Attributes.Add("class", "view")
                    aswitch.Text = "Available Failure Modes"
                End If
            Else
                lblfailmode.Value = "norm"
                imgchk.Attributes.Add("class", "details")
                If sw = "comp" Then
                    lbfailcomp.Attributes.Add("class", "details")
                    lbfailmaster.Attributes.Add("class", "view")
                    aswitch.Text = "Available Failure Modes"
                    lblswitch.Value = "all"

                End If
            End If
        End If
        faillist.Dispose()
    End Sub
    Private Sub PopFailList(ByVal cid As String, ByVal coid As String)
        Dim faillist As New Utilities
        Dim lang As String = lblfslang.Value
        Dim coi As String = lblcomi.Value
        faillist.Open()
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        Dim ocas As String
        If oca = "ALL" Then
            If coi <> "CAS" And coi <> "AGR" And coi <> "LAU" And coi <> "GLA" Then
                sql = "select distinct compfailid, failuremode from ComponentFailModes where " _
                              + "comid = '" & coid & "'"
                sql = "usp_getcfall '" & coid & "','" & lang & "'"
            Else
                If lang = "fre" Then
                    sql = "select distinct compfailid, ffailuremode from ComponentFailModes where " _
                              + "comid = '" & coid & "'"
                    sql = "usp_getcfall '" & coid & "','" & lang & "'"
                Else
                    sql = "select distinct compfailid, failuremode from ComponentFailModes where " _
                              + "comid = '" & coid & "'"
                    sql = "usp_getcfall '" & coid & "','" & lang & "'"
                End If
            End If

        Else
            If coi <> "CAS" And coi <> "AGR" And coi <> "LAU" And coi <> "GLA" Then
                sql = "select distinct compfailid, failuremode from ComponentFailModes where " _
              + "comid = '" & coid & "' and oaid = '" & oaid & "' order by failuremode"
            Else
                If lang = "fre" Then
                    sql = "select distinct compfailid, ffailuremode from ComponentFailModes where " _
              + "comid = '" & coid & "' and oaid = '" & oaid & "' order by ffailuremode"
                Else
                    sql = "select distinct compfailid, failuremode from ComponentFailModes where " _
              + "comid = '" & coid & "' and oaid = '" & oaid & "' order by failuremode"
                End If
            End If

        End If

        dr = faillist.GetRdrData(sql)
        lbfailmodes.DataSource = dr
        If oca = "ALL" Then
            lbfailmodes.DataValueField = "compfailid"
            lbfailmodes.DataTextField = "failuremode"
        Else
            lbfailmodes.DataValueField = "compfailid"
            lbfailmodes.DataTextField = "failuremode"
        End If

        lbfailmodes.DataBind()
        dr.Close()

        faillist.Dispose()

    End Sub
    Private Sub PopFail(ByVal cid As String, ByVal comid As String)
        Dim fail As New Utilities
        Dim lang As String = lblfslang.Value
        fail.Open()
        Dim chk As String = lblfailchk.Value
        Dim scnt As Integer
        sid = lblsid.Value
        Dim coi As String = lblcomi.Value
        Dim dt, val, filt, dtx As String
        If coi <> "CAS" And coi <> "AGR" And coi <> "LAU" And coi <> "GLA" Then
            If chk = "" Then
                sql = "select count(*) from pmSiteFM where siteid = '" & sid & "'"
                scnt = fail.Scalar(sql)
                If scnt = 0 Then
                    lblfailchk.Value = "open"
                    chk = "open"
                Else
                    lblfailchk.Value = "site"
                    chk = "site"
                End If
            End If

            If chk = "open" Then
                dt = "FailureModes"
            ElseIf chk = "site" Then
                dt = "pmSiteFM"
            Else
                Exit Sub
            End If
            val = "failid, failuremode"
            dtx = "failuremode"
        Else
            chk = "open"
            dt = "FailureModes"
            If lang = "fre" Then
                val = "failid, fmeng"
                dtx = "fmeng"
            Else
                val = "failid, failuremode"
                dtx = "failuremode"
            End If
        End If

        Dim valq As String = val

        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If oca = "ALL" Then
            If chk = "site" Then
                filt = " where siteid = '" & sid & "' and " & dtx & " is not null and failid not in (" _
        + "select failid from componentfailmodes where comid = '" & comid & "' and oaid is null) " 'order by failuremode
            Else
                filt = " where " & dtx & " is not null and failid not in (" _
        + "select failid from componentfailmodes where comid = '" & comid & "' and oaid is null) " 'order by failuremode
            End If

        Else
            If chk = "site" Then
                filt = " where siteid = '" & sid & "' and " & dtx & " and failid not in (" _
           + "select failid from componentfailmodes where comid = '" & comid & "' and oaid = '" & oaid & "') " 'order by failuremode
            Else
                filt = " where " & dtx & " is not null and failid not in (" _
            + "select failid from componentfailmodes where comid = '" & comid & "' and oaid = '" & oaid & "') " 'order by failuremode
            End If

        End If

        dr = fail.GetList(dt, val, filt)
        lbfailmaster.DataSource = dr
        If lang = "fre" And (coi = "CAS" Or coi = "AGR" Or coi = "LAU" Or coi = "GLA") Then
            lbfailmaster.DataTextField = "fmeng"
        Else
            lbfailmaster.DataTextField = "failuremode"
        End If

        lbfailmaster.DataValueField = "failid"
        lbfailmaster.DataBind()
        dr.Close()
        fail.Dispose()
    End Sub

    Private Sub PopFailPrev(ByVal cid As String, ByVal coid As String)

        Dim fail As New Utilities
        fail.Open()
        Dim chk As String = lblfailchk.Value
        Dim scnt As Integer
        sid = lblsid.Value
        'If chk = "" Then
        'sql = "select count(*) from pmSiteFM where siteid = '" & sid & "'"
        ' scnt = fail.Scalar(sql)
        'If scnt = 0 Then
        lblfailchk.Value = "open"
        chk = "open"
        'Else
        'lblfailchk.Value = "site"
        'chk = "site"
        'End If
        'End If
        Dim dt, val, filt As String
        If chk = "open" Then
            dt = "FailureModes"
        ElseIf chk = "site" Then
            dt = "pmSiteFM"
        Else
            Exit Sub
        End If
        val = "failid, failuremode"
        filt = " where failid not in (" _
         + "select distinct failid from componentfailmodes where comid = '" & cid & "') order by failuremode asc"
        dr = fail.GetList(dt, val, filt)
        lbfailmaster.DataSource = dr
        lbfailmaster.DataTextField = "failuremode"
        lbfailmaster.DataValueField = "failid"
        lbfailmaster.DataBind()
        dr.Close()
        fail.Dispose()
    End Sub


    Private Sub PopComp(ByVal coid As String)
        cid = lblcid.Value
        lblcoid.Value = coid
        lblpar.Value = "comp"
        'comp.Open()
        GetCompRecord(coid)
        Dim ac, acid, ecd1id As String

        If dr.Read Then
            det = dr.Item("det").ToString
            rpn = dr.Item("rpn").ToString
            ecd1id = dr.Item("ecd1id").ToString
            lblecd1id.Value = ecd1id
            ac = dr.Item("assetclass").ToString
            acid = dr.Item("acid").ToString
            txtmfg.Text = dr.Item("mfg").ToString
            txtkey.Text = dr.Item("compkey").ToString
            txtconame.Text = dr.Item("compnum").ToString
            lblcohold.Value = dr.Item("compnum").ToString
            txtdesc.Text = dr.Item("compdesc").ToString
            txtdesig.Text = dr.Item("desig").ToString
            txtspl.Text = dr.Item("spl").ToString
            lblpic.Value = dr.Item("picurl").ToString
            'lblcnt.Text = dr.Item("piccnt").ToString
            lblparcoid.Value = dr.Item("origparent").ToString
            tdcclass.InnerHtml = dr.Item("assetclass").ToString
            tdsclass.InnerHtml = dr.Item("subclass").ToString
            rtf = dr.Item("rtf").ToString
        End If
        dr.Close()
        If rtf = "1" Then
            cbrtf.Checked = True
        Else
            cbrtf.Checked = False
        End If
        If det = "1" Then
            cbdet.Checked = True
        Else
            cbdet.Checked = False
        End If
        If rpn = "" Then
            rpn = "0"
        End If
        txtrpn.Text = rpn
        'Try
        'ddac.SelectedValue = acid
        'Catch ex As Exception

        'End Try
        If ac <> "" Then
            btncrem.Attributes.Add("class", "view")
        Else
            btncrem.Attributes.Add("class", "details")
        End If

        'Dim pic As String = lblpic.Value
        'sql = "select top 1 picurltm from pmPictures where comid = '" & coid & "'"
        'pic = comp.strScalar(sql)
        'lblpic.Value = pic
        'Dim piccnt As Integer
        'sql = "select count(*) from pmpictures where comid = '" & coid & "'"
        'piccnt = comp.Scalar(sql)
        'lblcnt.Value = piccnt

        'If Len(pic) <> 0 Then
        'imgco.Src = pic
        'End If
        fu = lblfuid.Value
        eq = lbleqid.Value
        GetCompEqParent(eq)
        If dr.Read Then
            bte.InnerHtml = "Equipment:    " & dr.Item("eqnum").ToString
            bde.InnerHtml = "Description: " & dr.Item("eqdesc").ToString
        End If
        dr.Close()
        GetCompFuParent(fu)
        If dr.Read Then
            btf.InnerHtml = "Function:     " & dr.Item("func").ToString
            bdf.InnerHtml = "SPL: " & dr.Item("spl").ToString
        End If
        dr.Close()

        Dim taskcnt As Integer
        sql = "select count(*) from pmtasks where comid = '" & coid & "'"
        taskcnt = comp.Scalar(sql)
        'comp.Dispose()
        btndel.Attributes.Add("onclick", "javascript:return " & _
        "confirm('Are you sure you want to delete Component " & _
        txtconame.Text & "\nReferenced in " & taskcnt & " Task(s)?')")
        'pic = lblpic.Value
        'If Len(pic) <> 0 Then
        'imgco.Src = pic
        'lblcur.Text = "1"
        'Else
        'lblcur.Text = "0"
        'lblcnt.Value = "0"
        'End If

        PopFailList(cid, coid)
        PopFail(cid, coid)
        PopFailComp(cid, coid)
        Dim lock, lockby As String
        Dim user As String = lbluser.Value
        lock = CheckLock(eq)
        If lock = "1" Then
            lockby = lbllockedby.Value
            If lockby = user Then
                lbllock.Value = "0"
            Else
                Dim strMessage As String = tmod.getmsg("cdstr837", "CompDiv.aspx.vb") & " " & lockby & "."
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                btnsave.Attributes.Add("class", "details")
                btnupload.Attributes.Add("class", "details")
                btndel.Attributes.Add("class", "details")
            End If
        ElseIf lock = "0" Then
            'LockRecord(user, eq)
        End If
        LoadPics()
    End Sub
    Private Function CheckLock(ByVal eqid As String) As String
        chld = lblchld.Value
        Dim lock As String
        If chld = "" Or chld = "0" Then
            sql = "select locked, lockedby from equipment where eqid = '" & eqid & "'"
            Try
                dr = comp.GetRdrData(sql)
                While dr.Read
                    lock = dr.Item("locked").ToString
                    lbllock.Value = dr.Item("locked").ToString
                    lbllockedby.Value = dr.Item("lockedby").ToString
                End While
                dr.Close()
            Catch ex As Exception

            End Try
        Else
            lock = "1"
            lbllock.Value = "1"
            lbllockedby.Value = "PMO Admin"
        End If

        Return lock
    End Function
    Private Sub btnAddFu_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddFu.Click
        lblgetecd2.Value = ""
        Dim ecd1id As String = lblnewecd1id.Value
        isecd = lblisecd.Value
        fu = lblfuid.Value
        cid = lblcid.Value
        sid = lblsid.Value
        co = txtnewcomp.Text
        co = comp.ModString1(co)
        'If isecd = "1" And Len(co) > 5 Then
        '  Dim strMessage As String = "New Component Name (Error Code 1) Limited to 5 Characters"
        '   Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        '   Exit Sub
        'Else
        If Len(co) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr663", "complibadd.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim cocnt As Integer
        'If isecd = "1" Then
        'sql = "select count(*) from ecd1 where ecd1 = '" & co & "'"
        'cocnt = comp.Scalar(sql)
        'If cocnt > 0 Then
        'Dim strMessage As String = "New Component Name (Error Code 1) Already Exists"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End If
        'End If
        Dim hold As String = lblcohold.Value
        If Len(co) > 0 Then
            Dim desc As String = txtnewdesc.Text
            desc = comp.ModString1(desc)
            Dim spl As String = ""
            Dim desig As String = ""
            'Dim cocnt As Integer
            comp.Open()
            'sql = "select count(*) from components where func_id = '" & fu & "' compnum = '" & co & "'"
            cocnt = 0 'comp.Scalar(sql)
            If cocnt = 0 Then
                sql = "sp_addComponent " & cid & ", " & fu & ", '" & co & "', '" & desc & "', '" & spl & "', '" & desig & "','" & ecd1id & "','" & sid & "'"
                coid = comp.Scalar(sql)
                lblcoid.Value = coid
                lblpar.Value = "comp"
                txtnewcomp.Text = ""
                txtnewdesc.Text = ""
                PopComp(coid)
                'GetCompRecord(coid)
                'If dr.Read Then
                'txtconame.Text = dr.Item("compnum").ToString
                'txtdesc.Text = dr.Item("compdesc").ToString
                'End If
                'dr.Close()
                'fu = lblfuid.Value
                'eq = lbleqid.Value

                'GetCompEqParent(eq)
                'If dr.Read Then
                'bte.InnerHtml = "Equipment:    " & dr.Item("eqnum").ToString
                'bde.InnerHtml = "Description: " & dr.Item("eqdesc").ToString
                'End If
                'dr.Close()
                'GetCompFuParent(fu)
                'If dr.Read Then
                'btf.InnerHtml = "Function:     " & dr.Item("func").ToString
                'bdf.InnerHtml = "Description: " & dr.Item("func_desc").ToString
                'End If
                'dr.Close()
                'PopComp(fu)
                comp.Dispose()
            Else
                Dim strMessage As String = tmod.getmsg("cdstr838", "CompDiv.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Else
            Dim strMessage As String = tmod.getmsg("cdstr839", "CompDiv.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If


    End Sub
    Private Sub SaveComp()
        Dim rtf As Integer
        If cbrtf.Checked = True Then
            rtf = 1
        Else
            rtf = 0
        End If
        If cbdet.Checked = True Then
            det = 1
        Else
            det = 0
        End If
        rpn = txtrpn.Text
        Dim rpni As Integer
        Try
            rpni = txtrpn.Text
        Catch ex As Exception
            rpni = 0
        End Try
        isecd = lblisecd.Value
        fu = lblfuid.Value
        coid = lblcoid.Value
        co = txtconame.Text
        co = comp.ModString1(co)

        Dim desc As String = txtdesc.Text
        desc = comp.ModString1(desc)
        Dim spl As String = txtspl.Text
        spl = comp.ModString1(spl)
        Dim desig As String = txtdesig.Text
        desig = comp.ModString1(desig)
        Dim mfg As String = txtmfg.Text
        mfg = comp.ModString1(mfg)
        If Len(mfg) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr840", "CompDiv.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            flg = "1"
            Exit Sub
        End If
        Dim ck As String = txtkey.Text
        ck = comp.ModString1(ck)
        If Len(ck) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr841", "CompDiv.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            flg = "1"
            Exit Sub
        End If
        Dim ac, acid As String
        'If ddac.SelectedIndex <> 0 Then
        'ac = ddac.SelectedItem.ToString
        'acid = ddac.SelectedValue.ToString
        ''End If
        Dim cqty As String = "1"
        Dim cocnt As Integer
        Dim hold As String = lblcohold.Value
        sql = "select count(*) from components where func_id = '" & fu & "' and compnum = '" & co & "'"
        cocnt = 0 'comp.Scalar(sql)
        If cocnt = 0 Or co = hold Then
            'sql = "usp_updateComponent '" & coid & "', '" & co & "', '" & cqty & "', '" & desc & "', '" & spl & "', '" & desig & "'"
            sql = "usp_updateComponent_new '" & coid & "', '" & ck & "', '" & co & "', '" & cqty & "', '" & desc & "', '" & spl & "', '" & desig & "', '" & ac & "','" & acid & "','" & mfg & "','" & rtf & "','" & rpn & "','" & det & "'"
            Try
                comp.Update(sql)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr842", "CompDiv.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                flg = "1"
            End Try
        Else
            Dim strMessage As String = tmod.getmsg("cdstr843", "CompDiv.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            flg = "1"
        End If


    End Sub
    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsave.Click
        comp.Open()
        SaveComp()
        coid = lblcoid.Value
        PopComp(coid)
        LoadPics()
        comp.Dispose()
    End Sub
    Public Function GetCompFuParent(ByVal fu As String) As SqlDataReader
        sql = "select func, spl from functions where func_id = '" & fu & "'"
        dr = comp.GetRdrData(sql)
        Return dr
    End Function
    Public Function GetCompEqParent(ByVal eq As String) As SqlDataReader
        sql = "select eqnum, eqdesc from equipment where eqid = '" & eq & "'"
        dr = comp.GetRdrData(sql)
        Return dr
    End Function

    Public Function GetCompRecord(ByVal co As String) As SqlDataReader
        sql = "select * from components where comid = '" & co & "'"
        dr = comp.GetRdrData(sql)
        Return dr
    End Function

    Private Sub GetItems(ByVal failid As String, ByVal failstr As String)
        isecd = lblisecd.Value
        cid = lblcid.Value
        coid = lblcoid.Value
        Dim sw As String = lblswitch.Value
        Dim fail As New Utilities
        fail.Open()
        Dim fcnt As Integer
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If oca = "ALL" Then
            sql = "select count(*) from ComponentFailModes where " _
        + "comid = '" & coid & "' and failid = '" & failid & "' and oaid is null"
        Else
            sql = "select count(*) from ComponentFailModes where " _
        + "comid = '" & coid & "' and failid = '" & failid & "' and oaid = '" & oaid & "'"
        End If
        fcnt = fail.Scalar(sql)
        Dim ecd As String = lblcurrecd2.Value
        'lblcurrecd2.Value = ""
        Dim ecd1, ecd2, ecd3 As String
        If ecd <> "" Then
            Dim ecdarr() As String = ecd.Split("-")
            ecd2 = ecdarr(0).ToString
            ecd1 = ecdarr(1).ToString
        End If
        ecd3 = lblnewecd3.Value
        lblnewecd3.Value = ""
        Dim ecd3id, ecd3cnt As Integer
        If isecd = "1" Then
            If ecd3 <> "" Then
                sql = "select count(*) from ecd3 where ecd3 = '" & ecd3 & "' and ecd1id = '" & ecd1 & "' and ecd2id = '" & ecd2 & "'"
                ecd3cnt = fail.Scalar(sql)
                If ecd3cnt = 0 Then
                    sql = "insert into ecd3 (ecd3, ecddesc, ecd1id, ecd2id) values ('" & ecd3 & "','" & oca & "','" & ecd1 & "','" & ecd2 & "') select @@identity"
                    ecd3id = fail.Scalar(sql)
                Else
                    sql = "select ecd3id from ecd3 where ecd3 = '" & ecd3 & "' and ecd1id = '" & ecd1 & "' and ecd2id = '" & ecd2 & "'"
                    ecd3id = fail.Scalar(sql)
                End If
            Else
                Dim strMessage As String = "No Error Code 3 Value Created"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        End If


        If fcnt = 0 Then
            If oca = "ALL" Then
                oaid = ""
            End If
            If sw = "comp" And oca <> "ALL" Then
                If ecd <> "" Then
                    sql = "update ComponentFailModes set oaid = '" & oaid & "', ecd1id = '" & ecd1 & "', ecd2id = '" & ecd2 & "', ecd3id = '" & ecd3id & "' where compfailid = '" & failid & "'"
                Else
                    sql = "update ComponentFailModes set oaid = '" & oaid & "' where compfailid = '" & failid & "'"
                End If

            Else
                sid = lblsid.Value
                sql = "sp_addFailureMode '" & cid & "', '" & coid & "', '" & failid & "', '" & failstr & "','" & oaid & "','" & ecd1 & "','" & ecd2 & "','" & ecd3id & "','" & sid & "'"
            End If

            fail.Update(sql)
        End If
        fail.Dispose()

    End Sub
    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        cid = lblcid.Value
        coid = lblcoid.Value
        Dim Item As ListItem
        Dim f, fi As String
        Dim fail As New Utilities
        Dim sw As String = lblswitch.Value
        fail.Open()
        If sw = "comp" Then
            For Each Item In lbfailcomp.Items
                If Item.Selected Then
                    f = Item.Text.ToString
                    fi = Item.Value.ToString
                    GetItems(fi, f)
                End If
            Next
        Else
            For Each Item In lbfailmaster.Items
                If Item.Selected Then
                    f = Item.Text.ToString
                    fi = Item.Value.ToString
                    GetItems(fi, f)
                End If
            Next
        End If

        PopFailList(cid, coid)
        PopFail(cid, coid)
        PopFailComp(cid, coid)
        fail.Dispose()
    End Sub
    Private Sub RemItems(ByVal failid As String, ByVal failstr As String)
        isecd = lblisecd.Value
        coid = lblcoid.Value
        Dim sw As String = lblswitch.Value
        Dim fail As New Utilities
        fail.Open()
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If oca = "ALL" Then
            oaid = ""
        End If
        Dim ecd3id, ecd3cnt As Integer
        If isecd = "1" Then
            sql = "delete from ecd3 where ecd3id = (select ecd3id from componentfailmodes where compfailid = '" & failid & "')"
            fail.Update(sql)
        End If
        If sw = "comp" Then
            sql = "update ComponentFailModes set oaid = NULL, ecd1id = NULL, ecd2id = NULL, ecd3id = NULL where compfailid = '" & failid & "';"
            sql += "usp_deltaskoaid '" & failid & "','" & oaid & "'"
        Else

            sql = "sp_delComponentFailMode '" & coid & "', '" & failid & "','" & oaid & "'"


        End If

        fail.Update(sql)
        fail.Dispose()
    End Sub
    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        cid = lblcid.Value
        coid = lblcoid.Value
        Dim Item As ListItem
        Dim f, fi As String
        Dim fail As New Utilities
        fail.Open()
        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                RemItems(fi, f)
            End If
        Next
        PopFailList(cid, coid)
        PopFail(cid, coid)
        PopFailComp(cid, coid)
        fail.Dispose()
    End Sub

    Private Sub btnaddfail_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnaddfail.Click
        Dim fail As New Utilities
        fail.Open()
        If txtnewfail.Text <> "" Then
            Dim failstr As String
            cid = lblcid.Value
            coid = lblcoid.Value
            sid = lblsid.Value
            Dim strchk As Integer
            sql = "select count(*) from FailureModes where failuremode = '" & failstr & "'"
            strchk = fail.Scalar(sql)
            If strchk = 0 Then
                Dim fid As Integer
                failstr = txtnewfail.Text
                sql = "insert into FailureModes " _
                + "(compid, failuremode, fmeng) values ('" & cid & "', '" & failstr & "','" & failstr & "')"
                fid = fail.Scalar(sql)
                sql = "usp_addSiteFM '" & cid & "', '" & sid & "', '" & fid & "', '" & failstr & "'"
                fail.Update(sql)
            Else
                Dim strMessage As String = tmod.getmsg("cdstr844", "CompDiv.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            PopFail(cid, coid)
        End If
        fail.Dispose()
    End Sub
    Private Sub btngotofunc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("FuncDiv.aspx", True)
    End Sub
    Private Sub btnreturntoeq_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("EQBot.aspx", True)
    End Sub

    Function ThumbnailCallback() As Boolean
        Return False
    End Function

    Private Sub btndel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btndel.Click
        comp.Open()
        Dim comid, fuid As String
        cid = lblcid.Value
        comid = lblcoid.Value
        fuid = lblfuid.Value
        Dim pic, picn, picm As String
        sql = "select picurl, picurltn, picurltm from pmpictures where comid = '" & comid & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            pic = dr.Item("picurl").ToString
            picn = dr.Item("picurltn").ToString
            picm = dr.Item("picurltm").ToString
            DelFuImg(pic, picn, picm)
        End While
        dr.Close()
        sql = "usp_delComp '" & comid & "'" ', '" & fuid & "'"
        comp.Update(sql)
        'sql = "update AdminTasks " _
        '           + "set com = (com - 1) where cid = '" & cid & "'"
        'comp.Update(sql)
        PopComp(fuid)
        comp.Dispose()
        txtconame.Text = ""
        txtdesc.Text = ""
        txtspl.Text = ""
        txtdesig.Text = ""
        bte.InnerHtml = ""
        bde.InnerHtml = ""
        btf.InnerHtml = ""
        bdf.InnerHtml = ""

        lbfailmodes.DataSource = Nothing
        lbfailmodes.DataTextField = ""
        lbfailmodes.DataValueField = ""
        lbfailmodes.DataBind()
        lbfailmodes.Items.Clear()

        lbfailmaster.DataSource = Nothing
        lbfailmaster.DataTextField = ""
        lbfailmaster.DataValueField = ""
        lbfailmaster.DataBind()
        lbfailmaster.Items.Clear()
        lbldel.Value = "1"
    End Sub
    Private Sub DelFuImg(ByVal pic As String, ByVal picn As String, ByVal picm As String)
        'fuid = lblfuid.Value

        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/eqimages/"

        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        'Try
        'If File.Exists(fpic) Then
        'File.Delete1(fpic)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicm) Then
        'File.Delete1(fpicm)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicn) Then
        'File.Delete1(fpicn)
        'End If
        'Catch ex As Exception

        'End Try
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2031.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2031")
        Catch ex As Exception
        End Try
        Try
            lang2032.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2032")
        Catch ex As Exception
        End Try
        Try
            lang2033.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2033")
        Catch ex As Exception
        End Try
        Try
            lang2034.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2034")
        Catch ex As Exception
        End Try
        Try
            lang2035.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2035")
        Catch ex As Exception
        End Try
        Try
            lang2036.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2036")
        Catch ex As Exception
        End Try
        Try
            lang2037.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2037")
        Catch ex As Exception
        End Try
        Try
            lang2038.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2038")
        Catch ex As Exception
        End Try
        Try
            lang2039.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2039")
        Catch ex As Exception
        End Try
        Try
            lang2040.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2040")
        Catch ex As Exception
        End Try
        Try
            lang2041.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2041")
        Catch ex As Exception
        End Try
        Try
            lang2042.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2042")
        Catch ex As Exception
        End Try
        Try
            lang2043.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2043")
        Catch ex As Exception
        End Try
        Try
            lang2044.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2044")
        Catch ex As Exception
        End Try
        Try
            lang2045.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2045")
        Catch ex As Exception
        End Try
        Try
            lang2046.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2046")
        Catch ex As Exception
        End Try
        Try
            lang2047.Text = axlabs.GetASPXPage("CompDiv.aspx", "lang2047")
        Catch ex As Exception
        End Try
        Try
            lblpg.Text = axlabs.GetASPXPage("CompDiv.aspx", "lblpg")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                btnAddFu.Attributes.Add("src", "../images2/eng/bgbuttons/badd.gif")
            ElseIf lang = "fre" Then
                btnAddFu.Attributes.Add("src", "../images2/fre/bgbuttons/badd.gif")
            ElseIf lang = "ger" Then
                btnAddFu.Attributes.Add("src", "../images2/ger/bgbuttons/badd.gif")
            ElseIf lang = "ita" Then
                btnAddFu.Attributes.Add("src", "../images2/ita/bgbuttons/badd.gif")
            ElseIf lang = "spa" Then
                btnAddFu.Attributes.Add("src", "../images2/spa/bgbuttons/badd.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btngtt.Attributes.Add("src", "../images2/eng/bgbuttons/jtotaskssm.gif")
            ElseIf lang = "fre" Then
                btngtt.Attributes.Add("src", "../images2/fre/bgbuttons/jtotaskssm.gif")
            ElseIf lang = "ger" Then
                btngtt.Attributes.Add("src", "../images2/ger/bgbuttons/jtotaskssm.gif")
            ElseIf lang = "ita" Then
                btngtt.Attributes.Add("src", "../images2/ita/bgbuttons/jtotaskssm.gif")
            ElseIf lang = "spa" Then
                btngtt.Attributes.Add("src", "../images2/spa/bgbuttons/jtotaskssm.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddasset.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("CompDiv.aspx", "btnaddasset") & "')")
            btnaddasset.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            btnaddtosite.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("CompDiv.aspx", "btnaddtosite") & "')")
            btnaddtosite.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            btncget.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("CompDiv.aspx", "btncget") & "')")
            btncget.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            btncrem.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("CompDiv.aspx", "btncrem") & "')")
            btncrem.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("CompDiv.aspx", "Img2") & "')")
            Img2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgdel.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("CompDiv.aspx", "imgdel") & "', ABOVE, LEFT)")
            imgdel.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsavdet.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("CompDiv.aspx", "imgsavdet") & "', ABOVE, LEFT)")
            imgsavdet.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid248.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("CompDiv.aspx", "ovid248") & "', ABOVE, LEFT)")
            ovid248.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid249.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("CompDiv.aspx", "ovid249") & "')")
            ovid249.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class

