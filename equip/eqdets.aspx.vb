

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class eqdets
    Inherits System.Web.UI.Page
	Protected WithEvents lang2280 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2279 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2278 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2277 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2276 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2275 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2274 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2273 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2272 As System.Web.UI.WebControls.Label


    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim sql As String
    Dim eqid, start As String
    Dim srch As New Utilities


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdcureq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tddesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdloc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdoem As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmod As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsn As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdac As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcmail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            If start = "yes" Then
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                srch.Open()
                GetDetails(eqid)
                srch.Dispose()
            End If
            

        End If
    End Sub
    Private Sub GetDetails(ByVal eqid As String)
        sql = "select * from equipment where eqid = '" & eqid & "'"
        dr = srch.GetRdrData(sql)
        While dr.Read
            tdcureq.InnerHtml = dr.Item("eqnum").ToString
            tdecr.InnerHtml = dr.Item("ecr").ToString
            tddesc.InnerHtml = dr.Item("eqdesc").ToString
            tdspl.InnerHtml = dr.Item("spl").ToString
            tdloc.InnerHtml = dr.Item("location").ToString
            tdoem.InnerHtml = dr.Item("oem").ToString
            tdmod.InnerHtml = dr.Item("model").ToString
            tdsn.InnerHtml = dr.Item("serial").ToString
            tdac.InnerHtml = dr.Item("assetclass").ToString
            tdcby.InnerHtml = dr.Item("createdby").ToString
            tdcdate.InnerHtml = dr.Item("createdate").ToString
        End While
        dr.Close()

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2272.Text = axlabs.GetASPXPage("eqdets.aspx", "lang2272")
        Catch ex As Exception
        End Try
        Try
            lang2273.Text = axlabs.GetASPXPage("eqdets.aspx", "lang2273")
        Catch ex As Exception
        End Try
        Try
            lang2274.Text = axlabs.GetASPXPage("eqdets.aspx", "lang2274")
        Catch ex As Exception
        End Try
        Try
            lang2275.Text = axlabs.GetASPXPage("eqdets.aspx", "lang2275")
        Catch ex As Exception
        End Try
        Try
            lang2276.Text = axlabs.GetASPXPage("eqdets.aspx", "lang2276")
        Catch ex As Exception
        End Try
        Try
            lang2277.Text = axlabs.GetASPXPage("eqdets.aspx", "lang2277")
        Catch ex As Exception
        End Try
        Try
            lang2278.Text = axlabs.GetASPXPage("eqdets.aspx", "lang2278")
        Catch ex As Exception
        End Try
        Try
            lang2279.Text = axlabs.GetASPXPage("eqdets.aspx", "lang2279")
        Catch ex As Exception
        End Try
        Try
            lang2280.Text = axlabs.GetASPXPage("eqdets.aspx", "lang2280")
        Catch ex As Exception
        End Try

    End Sub

End Class
