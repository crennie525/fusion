<%@ Page Language="vb" AutoEventWireup="false" Codebehind="funclist3.aspx.vb" Inherits="lucy_r12.funclist3" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>funclist3</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<script  type="text/javascript" src="../scripts/smartscroll.js"></script>
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts1/funclist3aspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script  type="text/javascript">
         function checkgo(who) {
             var ret;
             if (who == "go") {
                 var fuid = document.getElementById("lblfuid").value;
                 var coid = document.getElementById("lblcoid").value;

                 var func = document.getElementById("lblfunc").value;
                 var comp = document.getElementById("lblcomp").value;

                 ret = fuid + "," + coid + "," + func + "," + comp;
             }
             else {
                 ret = "no";
             }
             window.parent.handlereturn(ret);
         }
     </script>
	</HEAD>
	<body  onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table cellpadding="1" cellspacing="1" style="POSITION: absolute;  TOP: 4px;  LEFT: 4px" id="scrollmenu">
				<tr>
					<td>
						<table>
							<tr>
								<td class="bluelabel" height="36" style="width: 140px"><asp:Label id="lang2393" runat="server">Selected Equipment</asp:Label></td>
								<td class="plainlabel" id="tdeq" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="btmmenu plainlabel"><asp:Label id="lang2394" runat="server">Equipment Functions</asp:Label></td>
				</tr>
				<tr>
					<td><div id="divfu" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 320px; HEIGHT: 118px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
							runat="server"></div>
					</td>
				</tr>
				<tr>
					<td class="btmmenu plainlabel"><asp:Label id="lang2395" runat="server">Function Components</asp:Label></td>
				</tr>
				<tr>
					<td><div id="divco" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 320px; HEIGHT: 118px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
							runat="server"></div>
					</td>
				</tr>
				<tr id="trsave" class="details" runat="server">
					<td class="plainlabel" align="right"><A onclick="checkgo('go');" href="#"><asp:Label id="lang2396" runat="server">Jump To Selection</asp:Label></A>&nbsp;&nbsp;&nbsp;&nbsp;<A onclick="checkgo('can');" href="#"><asp:Label id="lang2397" runat="server">Cancel and Return</asp:Label></A></td>
				</tr>
				<tr>
					<td>
						<table>
							<tr>
								<td class="bluelabel" height="26" style="width: 110px"><asp:Label id="lang2398" runat="server">Selected Function</asp:Label></td>
								<td class="plainlabel" id="tdfunc"></td>
							</tr>
							<tr>
								<td class="bluelabel" height="26"><asp:Label id="lang2399" runat="server">Selected Component</asp:Label></td>
								<td class="plainlabel" id="tdcomp"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</TABLE> <input type="hidden" id="lbleqid" runat="server" NAME="lbleqid"> <input type="hidden" id="lbllog" runat="server" NAME="lbllog">
			<input type="hidden" id="lblsrchtyp" runat="server" NAME="lblsrchtyp"> <input type="hidden" id="lblfuid" runat="server" NAME="lblfuid">
			<input type="hidden" id="lblsubmit" runat="server" NAME="lblsubmit"> <input type="hidden" id="lblfunc" runat="server">
			<input type="hidden" id="lblcomp" runat="server"> <input type="hidden" id="lblcoid" runat="server">
			<input type="hidden" id="xcoord" runat="server" NAME="Hidden1"> <input type="hidden" id="ycoord" runat="server" NAME="Hidden2">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
