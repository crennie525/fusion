<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmlibeqdets.aspx.vb" Inherits="lucy_r12.pmlibeqdets" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>pmlibeqdets</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script  src="../scripts/imglibnav.js"></script>
		<script  src="../scripts1/pmlibeqdetsaspx.js"></script>
		<script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  class="tbg">
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 0px; LEFT: 0px" width="980">
				<TR>
					<td style="width: 120px"></td>
					<td width="210"></td>
					<td style="width: 120px"></td>
					<td width="300"></td>
					<td style="width: 150px"></td>
					<td style="width: 80px"></td>
				</TR>
				<tr height="20">
					<td class="label"><asp:Label id="lang2441" runat="server">Equipment#</asp:Label></td>
					<td class="plainlabel" id="tdeq" runat="server"></td>
					<td class="redlabel">ECR:</td>
					<td class="plainlabel" id="tdecr" runat="server"></td>
					<td align="center" colSpan="2" rowSpan="10"><A onclick="getbig();" href="#"><IMG id="imgeq" height="216" src="../images/appimages/eqimg1.gif" style="width: 216px" border="0"
								runat="server"></A></td>
				</tr>
				<tr height="20">
					<td class="label"><asp:Label id="lang2442" runat="server">Description</asp:Label></td>
					<td class="plainlabel" id="tddesc" colSpan="3" runat="server"></td>
				</tr>
				<tr height="20">
					<td class="label"><asp:Label id="lang2443" runat="server">Spl Identifier</asp:Label></td>
					<td class="plainlabel" id="tdspl" colSpan="3" runat="server"></td>
				</tr>
				<tr height="20">
					<td class="label"><asp:Label id="lang2444" runat="server">Location</asp:Label></td>
					<td class="plainlabel" id="tdloc" colSpan="3" runat="server"></td>
				</tr>
				<tr height="20">
					<td class="label">OEM</td>
					<td class="plainlabel" id="tdoem" runat="server"></td>
					<td class="label"><asp:Label id="lang2445" runat="server">Model#/Type</asp:Label></td>
					<td class="plainlabel" id="tdmod" runat="server"></td>
				</tr>
				<tr height="20">
					<td class="label"><asp:Label id="lang2446" runat="server">Serial#</asp:Label></td>
					<td class="plainlabel tbg" id="tdsn" runat="server"></td>
					<td class="label"></td>
					<td></td>
				</tr>
				<tr height="20">
					<td class="label"><asp:Label id="lang2447" runat="server">Asset Class</asp:Label></td>
					<td class="plainlabel" align="left" colSpan="3" id="tdac" runat="server"></td>
				</tr>
				<tr height="20">
					<td colSpan="4">&nbsp;</td>
				</tr>
				<tr height="20">
					<td class="label" id="Td8" runat="server"><asp:Label id="lang2448" runat="server">Created By</asp:Label></td>
					<td class="plainlabel" id="tdcby" runat="server"></td>
					<td class="label"><asp:Label id="lang2449" runat="server">Create Date</asp:Label></td>
					<td class="plainlabel" id="tdcdate" runat="server"></td>
				</tr>
				<tr height="20">
					<td class="label" id="Td5" runat="server"><asp:Label id="lang2450" runat="server">Phone</asp:Label></td>
					<td class="plainlabel" id="tdcphone" runat="server"></td>
					<td class="label"><asp:Label id="lang2451" runat="server">Email Address</asp:Label></td>
					<td class="plainlabel" id="tdcmail" runat="server"></td>
				</tr>
				<tr height="20">
					<td class="label" id="Td6" runat="server"><asp:Label id="lang2452" runat="server">Modified By</asp:Label></td>
					<td class="plainlabel" id="tdmby" runat="server"></td>
					<td class="label"><asp:Label id="lang2453" runat="server">Modified Date</asp:Label></td>
					<td class="plainlabel" id="tdmdate" runat="server"></td>
					<td align="center" colSpan="2"></td>
				</tr>
				<tr height="20">
					<td class="label" id="Td7" runat="server"><asp:Label id="lang2454" runat="server">Phone</asp:Label></td>
					<td class="plainlabel" id="tdmphone" runat="server">&nbsp;</td>
					<td class="label"><asp:Label id="lang2455" runat="server">Email Address</asp:Label></td>
					<td class="plainlabel" id="tdmail" runat="server">&nbsp;</td>
					<td align="center" colSpan="2">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 140px;" vAlign="middle" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabel">Image 0 of 0</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ilast" onclick="getplast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
								<td style="width: 20px"><IMG onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lbldb" runat="server"> <input type="hidden" id="lbleqid" runat="server">
			<input id="lblpcnte" type="hidden" runat="server" NAME="lblpcnt"> <input id="lblcurrp" type="hidden" runat="server" NAME="lblcurrp">
			<input id="lblimgs" type="hidden" name="lblimgs" runat="server"> <input id="lblimgid" type="hidden" name="lblimgid" runat="server">
			<input id="lblovimgs" type="hidden" name="lblovimgs" runat="server"> <input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server">
			<input id="lblcurrimg" type="hidden" name="lblcurrimg" runat="server"> <input id="lblcurrbimg" type="hidden" name="lblcurrbimg" runat="server">
			<input id="lblbimgs" type="hidden" name="lblbimgs" runat="server"><input id="lbliorders" type="hidden" name="lbliorders" runat="server">
			<input id="lbloldorder" type="hidden" runat="server" NAME="lbloldorder"><input id="lblovtimgs" type="hidden" name="lblovtimgs" runat="server">
			<input id="lblcurrtimg" type="hidden" name="lblcurrtimg" runat="server"> <input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
