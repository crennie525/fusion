

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmlibfudets
    Inherits System.Web.UI.Page
	Protected WithEvents lang2458 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2457 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2456 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim copy As New Utilities
    Dim eqid, fuid, db, start, Login As String
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rptrfuncrev As System.Web.UI.WebControls.Repeater
    Protected WithEvents lblpgf As System.Web.UI.WebControls.Label
    Protected WithEvents imgfu As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ifirstf As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprevf As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inextf As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilastf As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpcntf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrpf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgsf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgidf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovimgsf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovbimgsf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrimgf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrbimgf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimgsf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbliordersf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldorderf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovtimgsf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrtimgf As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            If start = "yes" Then
                eqid = Request.QueryString("eqid").ToString
                db = Request.QueryString("db").ToString
                lbleqid.Value = eqid
                lbldb.Value = db
                copy.Open()
                PopFuncRev(eqid)
                copy.Dispose()
            End If


        End If
    End Sub
    Private Sub PopFuncRev(ByVal eqid As String)
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        db = lbldb.Value
        Dim mdbarr() As String = db.Split(",")
        Dim tst As Integer = mdbarr.Length
        srvr = mdbarr(1).ToString
        mdb = mdbarr(2).ToString
        sql = "select * from  [" & srvr & "].[" & mdb & "].[dbo].[functions] where eqid = '" & eqid & "' order by routing"
        dr = copy.GetRdrData(sql)
        rptrfuncrev.DataSource = dr
        rptrfuncrev.DataBind()
        dr.Close()

    End Sub
    Private Sub LoadEqPics()
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/tpmimages/"
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        'lblnsimage.Value = nsimage
        'lblstrfrom.Value = strfrom
        'imgfu.Attributes.Add("src", "../images/appimages/funcimg.gif")
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgfu.Attributes.Add("src", "../images2/eng/funcimg.gif")
        ElseIf lang = "fre" Then
            imgfu.Attributes.Add("src", "../images2/fre/funcimg.gif")
        ElseIf lang = "ger" Then
            imgfu.Attributes.Add("src", "../images2/ger/funcimg.gif")
        ElseIf lang = "ita" Then
            imgfu.Attributes.Add("src", "../images2/ita/funcimg.gif")
        ElseIf lang = "spa" Then
            imgfu.Attributes.Add("src", "../images2/spa/funcimg.gif")
        End If

        Dim img, imgs, picid, eqid As String
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        db = lbldb.Value
        Dim mdbarr() As String = db.Split(",")
        Dim tst As Integer = mdbarr.Length
        srvr = mdbarr(1).ToString
        mdb = mdbarr(2).ToString
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        Dim pcnt As Integer
        sql = "select count(*) from  [" & srvr & "].[" & mdb & "].[dbo].[pmpictures] where comid is null and funcid = '" & fuid & "'"
        'sql = "select count(*) from pmpictures where funcid is null and comid is null and eqid = '" & eqid & "'"
        pcnt = copy.Scalar(sql)
        lblpcntf.Value = pcnt

        Dim currp As String = lblcurrpf.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpgf.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lblpgf.Text = "Image 1 of " & pcnt
                lblcurrpf.Value = "0"
            End If

            sql = "select p.pic_id, p.picurl, p.picurltn, p.picurltm, p.picorder " _
            + "from [" & srvr & "].[" & mdb & "].[dbo].[pmpictures] p where p.comid is null and p.funcid = '" & fuid & "'" _
            + "order by p.picorder"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs, timg As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs, ovtimg, ovtimgs
            dr = copy.GetRdrData(sql)
            While dr.Read
                iflag += 1
                img = dr.Item("picurltm").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)
                bimg = dr.Item("picurl").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)

                timg = dr.Item("picurltn").ToString
                Dim timgarr() As String = timg.Split("/")
                ovtimg = timgarr(timgarr.Length - 1)

                picid = dr.Item("pic_id").ToString
                order = dr.Item("picorder").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If

                If ovtimgs = "" Then
                    ovtimgs = ovtimg
                Else
                    ovtimgs += "," & ovtimg
                End If

                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimgf.Value = ovimg
                    lblcurrbimgf.Value = ovbimg
                    lblcurrtimgf.Value = ovtimg
                    imgfu.Attributes.Add("src", img)
                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgidf.Value = picid

                    'txtiorder.Text = order
                End If
                If imgs = "" Then
                    imgs = picid & ";" & img
                Else
                    imgs += "~" & picid & ";" & img
                End If
            End While
            dr.Close()
            lblimgsf.Value = imgs
            lblbimgsf.Value = bimgs
            lblovimgsf.Value = ovimgs
            lblovbimgsf.Value = ovbimgs
            lblovtimgsf.Value = ovtimgs
            lbliordersf.Value = iorders
        End If
    End Sub
    Public Function HighlightRowFR(ByVal rowid As Integer) As String
        Dim bgColor = "#FFFFFF"
        Try
            Dim fi As String = lblfuid.Value
            Dim id As Integer = fi
            If rowid = id Then
                bgColor = "#D6D4FB"
            End If
        Catch ex As Exception

        End Try
        Return bgColor
    End Function

    Private Sub rptrfuncrev_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrfuncrev.ItemCommand
        Dim fi, fp As String

        If e.CommandName = "Select" Then
            fi = CType(e.Item.FindControl("lblfuncrevid"), Label).Text
            lblfuid.Value = fi
            lblchk.value = "yes"
            copy.Open()
            eqid = lbleqid.Value
            PopFuncRev(eqid)
            LoadEqPics()
            copy.Dispose()
        End If
    End Sub

	



    Private Sub rptrfuncrev_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrfuncrev.ItemDataBound


        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang2456 As Label
                lang2456 = CType(e.Item.FindControl("lang2456"), Label)
                lang2456.Text = axlabs.GetASPXPage("pmlibfudets.aspx", "lang2456")
            Catch ex As Exception
            End Try
            Try
                Dim lang2457 As Label
                lang2457 = CType(e.Item.FindControl("lang2457"), Label)
                lang2457.Text = axlabs.GetASPXPage("pmlibfudets.aspx", "lang2457")
            Catch ex As Exception
            End Try
            Try
                Dim lang2458 As Label
                lang2458 = CType(e.Item.FindControl("lang2458"), Label)
                lang2458.Text = axlabs.GetASPXPage("pmlibfudets.aspx", "lang2458")
            Catch ex As Exception
            End Try
            Try
                Dim lblpgf As Label
                lblpgf = CType(e.Item.FindControl("lblpgf"), Label)
                lblpgf.Text = axlabs.GetASPXPage("pmlibfudets.aspx", "lblpgf")
            Catch ex As Exception
            End Try

        End If

    End Sub






    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2456.Text = axlabs.GetASPXPage("pmlibfudets.aspx", "lang2456")
        Catch ex As Exception
        End Try
        Try
            lang2457.Text = axlabs.GetASPXPage("pmlibfudets.aspx", "lang2457")
        Catch ex As Exception
        End Try
        Try
            lang2458.Text = axlabs.GetASPXPage("pmlibfudets.aspx", "lang2458")
        Catch ex As Exception
        End Try
        Try
            lblpgf.Text = axlabs.GetASPXPage("pmlibfudets.aspx", "lblpgf")
        Catch ex As Exception
        End Try

    End Sub

End Class
