<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmlibnewtaskgriddialog.aspx.vb" Inherits="lucy_r12.pmlibnewtaskgriddialog" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>pmlibnewtaskgriddialog</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<script  src="../scripts/sessrefdialog.js"></script>
		<script  src="../scripts1/pmlibnewtaskgriddialogaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script  type="text/javascript">
         function checklogin() {
             pageScroll();
             var log = document.getElementById("lbllog").value;
             if (log == "no") {
                 window.returnValue = "log";
                 window.close();
             }
             else if (log == "noeqid") {
                 window.returnValue = "no";
                 window.close();
             }
             else {
                 resetsess();
                 
             }
         }
     </script>
	</HEAD>
	<body  onload="checklogin();">
		<form id="form1" method="post" runat="server">
			<iframe id="ifeq" runat="server" width="100%" height="100%" frameborder="0"></iframe>
			<iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;"></iframe>
     <script type="text/javascript">
         document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script><input type="hidden" id="lblsessrefresh" runat="server" NAME="lblsessrefresh">
			<input type="hidden" id="lbllog" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
