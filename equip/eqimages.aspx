<%@ Page Language="vb" AutoEventWireup="false" Codebehind="eqimages.aspx.vb" Inherits="lucy_r12.eqimages" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>eqimages</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts/eqimgtab.js"></script>
		<script  src="../scripts1/eqimagesaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script  type="text/javascript">
         var flag = 0;
         function eqtab() {
             var eqid = document.getElementById("lbleqid").value;
             var fuid = document.getElementById("lblfuid").value;
             if (fuid == "") {
                 fuid = "0";
             }
             document.getElementById("ifimg").src = "eqimg.aspx?eqid=" + eqid + "&fuid=" + fuid;
             document.getElementById("eq").className = "thdrhov plainlabel";
             /*
             */
             if (flag == 0) {
                 flag = 1;
                 window.setTimeout("eqtab();", 50);
             }
         }
         function futab() {
             var eqid = document.getElementById("lbleqid").value;
             var fuid = document.getElementById("lblfuid").value;
             document.getElementById("ifimg").src = "fuimg.aspx?eqid=" + eqid + "&fuid=" + fuid;
             document.getElementById("fu").className = "thdrhov plainlabel";
         }
         function cotab() {
             var eqid = document.getElementById("lbleqid").value;
             var fuid = document.getElementById("lblfuid").value;
             var coid = document.getElementById("lblcoid").value;
             document.getElementById("ifimg").src = "coimg.aspx?eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid;
             document.getElementById("co").className = "thdrhov plainlabel";
         }
         function gettab(id) {
             document.getElementById("eq").className = "thdr plainlabel";
             document.getElementById("fu").className = "thdr plainlabel";
             document.getElementById("co").className = "thdr plainlabel";
             if (id == "eq") {
                 eqtab();
             }
             else if (id == "fu") {
                 futab();
             }
             else if (id == "co") {
                 cotab();
             }
         }
     </script>
	</HEAD>
	<body onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 0px; POSITION: absolute; TOP: 1px" cellSpacing="1" cellPadding="0">
				<TR height="14">
					<TD class="thdrhov plainlabel" id="eq" onclick="gettab('eq');" style="width: 140px"><asp:Label id="lang2281" runat="server">Equipment</asp:Label></TD>
					<TD class="thdr plainlabel" id="fu" onclick="gettab('fu');" width="70"><asp:Label id="lang2282" runat="server">Functions</asp:Label></TD>
					<TD class="thdr plainlabel" id="co" onclick="gettab('co');" width="70"><asp:Label id="lang2283" runat="server">Components</asp:Label></TD>
					<td width="0"></td>
				</TR>
				<tr>
					<td style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid"
						align="center" colSpan="4"><iframe id="ifimg" style="WIDTH: 250px; HEIGHT: 272px" src="eqimg.aspx?eqid=0&fuid=0" frameBorder="no"
							runat="server"></iframe>
					</td>
				</tr>
			</table>
			<input id="lbleqid" type="hidden" runat="server"> <input type="hidden" id="lblfuid" runat="server">
			<input type="hidden" id="lblcoid" runat="server"><input type="hidden" id="lbltab" runat="server">
			<input type="hidden" id="lblin" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
