

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text

Public Class DeptArch
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim PageNumber As Integer
    Dim PageSize As Integer = 50
    Dim intPgNav As Integer
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim main As New Utilities
    Dim chk, Login, typ, sid, clid, srch As String
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdarch As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            Exit Sub
        End Try
        If Not IsPostBack Then
            If Request.QueryString("start").ToString = "no" Then
                tdarch.InnerHtml = "<font class='plainlabelblue'>Waiting for Location...</font>"
            Else
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                lblloc.Value = Request.QueryString("lid").ToString
                lbldid.Value = Request.QueryString("did").ToString
                clid = Request.QueryString("clid").ToString
                If clid = "na" Then
                    clid = ""
                End If
                lblclid.Value = clid
                lblchk.Value = Request.QueryString("chk").ToString
                Try
                    lblsid.Value = Request.QueryString("sid").ToString
                Catch ex As Exception
                    Try
                        lblsid.Value = Request.QueryString("psite").ToString
                    Catch ex1 As Exception

                    End Try
                End Try
                txtpg.Value = "1"
                main.Open()
                GetCount()
                GetArch(typ)
                main.Dispose()
            End If
        Else
            
            If Request.Form("lblret") = "next" Then
                main.Open()
                GetNext()
                main.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                main.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetArch()
                main.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                main.Open()
                GetPrev()
                main.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                main.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetArch()
                main.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "srch" Then
                main.Open()
                PageNumber = 1
                txtpg.Value = PageNumber

                typ = "srch"
                GetArch(typ)
                main.Dispose()
                lblret.Value = ""
            End If

        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetArch()
        Catch ex As Exception
            main.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr328", "PMArchMan.aspx.vb")

            main.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetArch()
        Catch ex As Exception
            main.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr329", "PMArchMan.aspx.vb")

            main.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetCount()
        Dim srch As String = ""
        'srch = txtsrch.Text
        'srch = nmm.ModString2(srch)
        sid = lblsid.Value
        Dim comp As String = "0"
        If srch = "" Then
            srch = "%"
        Else
            srch = "%" & srch & "%"
        End If
        sql = "SELECT count(*) FROM equipment where siteid = '" & sid & "' and " _
        + "(eqnum like '" & srch & "' or eqdesc like '" & srch & "' or spl like '" & srch & "')"
        intPgNav = main.PageCount(sql, PageSize)
        txtpgcnt.Value = intPgNav
    End Sub
    Private Sub GetArch(Optional ByVal typ As String = "reg")
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", " ", , , vbTextCompare)
        srch = "%" & srch & "%"

        Dim sb As StringBuilder = New StringBuilder
        Dim dept As String = lbldid.Value '"24"
        Dim cell As String = lblclid.Value '"11"
        Dim lid As String = lblloc.Value
        Dim sid, did, clid As String
        sid = lblsid.Value
        Dim eqnum As String = ""
        Dim eqid As String = ""
        Dim eqdesc As String

        PageNumber = txtpg.Value
        PageSize = 50 'lblpagesize.Value

        'intPgNav = txtpgcnt.Value
        'lblpg.Text = "Page " & PageNumber & " of " & intPgNav

        sb.Append("<table cellspacing=""0"" border=""0""><tr>")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""215""></tr>" & vbCrLf)
        Dim loccnt As Integer = 0
        chk = lblchk.Value
        If typ = "loc" Then
            sql = "select location from pmlocations where locid = '" & lid & "'"
            Dim loc2 As String = main.strScalar(sql)
            sql = "select count(*) from pmlocations where locid in (select locid from pmlocations where location in (select location from pmlocheir where parent = '" & loc2 & "' and siteid = '" & sid & "')) " _
            + "and type = 'EQUIPMENT'"
            loccnt = main.Scalar(sql)
            If loccnt = 0 Then
                sql = "select top 1 isnull(p.parent,'0') from pmlocheir p left join pmlocations l on l.location = p.location where p.location = '" & loc2 & "' " _
                    + "and l.locid = '" & lid & "'"
                loc2 = main.strScalar(sql)
                If loc2 <> "0" Then
                    sql = "select count(*) from pmlocations where locid in (select locid from pmlocations where location in (select location from pmlocheir where parent = '" & loc2 & "' and siteid = '" & sid & "')) " _
                    + "and type = 'EQUIPMENT'"
                    loccnt = main.Scalar(sql)
                End If
            End If
            If loccnt > 0 Then

                sql = "select distinct e.siteid, e.locid, e.dept_id, e.cellid, e.eqid, e.eqnum, isnull(e.eqdesc, 'No Description') as eqdesc, isnull(f.func_id, 0) as func_id, " _
                + "e.locked, e.lockedby, " _
                + "f.func, isnull(c.comid, 0) as comid, c.compnum, cnt = (select count(c.compnum) " _
                + "from components c where c.func_id = f.func_id), " _
                + "epcnt = (select count(*) from pmpictures p where p.eqid = e.eqid and p.funcid is null and p.comid is null), " _
                + "fpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid is null), " _
                + "cpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid = c.comid), f.routing, c.crouting " _
                + "from equipment e left outer join functions f on f.eqid = e.eqid " _
                + "left outer join components c on c.func_id = f.func_id " _
                + "where e.locid in (select locid from pmlocations where location in (select location from pmlocheir where parent = '" & loc2 & "' and siteid = '" & sid & "')) and e.siteid = '" & sid & "' and e.dept_id is null order by e.eqnum, f.routing, c.crouting"

            Else
                sql = "select distinct e.siteid, e.locid, e.dept_id, e.cellid, e.eqid, e.eqnum, isnull(e.eqdesc, 'No Description') as eqdesc, isnull(f.func_id, 0) as func_id, " _
                + "e.locked, e.lockedby, " _
                + "f.func, isnull(c.comid, 0) as comid, c.compnum, cnt = (select count(c.compnum) " _
                + "from components c where c.func_id = f.func_id), " _
                + "epcnt = (select count(*) from pmpictures p where p.eqid = e.eqid and p.funcid is null and p.comid is null), " _
                + "fpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid is null), " _
                + "cpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid = c.comid), f.routing, c.crouting " _
                + "from equipment e left outer join functions f on f.eqid = e.eqid " _
                + "left outer join components c on c.func_id = f.func_id " _
                + "where e.locid = '" & lid & "' order by e.eqnum, f.routing, c.crouting"
            End If

            Dim dum As String = "ok"
        ElseIf typ = "reg" Then
            If chk = "yes" Then
                sql = "select distinct e.siteid, e.locid, e.dept_id, e.cellid, e.eqid, e.eqnum, isnull(e.eqdesc, 'No Description') as eqdesc, isnull(f.func_id, 0) as func_id, " _
                + "e.locked, e.lockedby, " _
                + "f.func, isnull(c.comid, 0) as comid, c.compnum, cnt = (select count(c.compnum) " _
                + "from components c where c.func_id = f.func_id), " _
                + "epcnt = (select count(*) from pmpictures p where p.eqid = e.eqid and p.funcid is null and p.comid is null), " _
                + "fpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid is null), " _
                + "cpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid = c.comid), f.routing, c.crouting " _
                + "from equipment e left outer join functions f on f.eqid = e.eqid " _
                + "left outer join components c on c.func_id = f.func_id " _
                + "where(e.dept_id = '" & dept & "' and e.cellid = '" & cell & "') order by e.eqnum, f.routing, c.crouting"
            Else
                sql = "select distinct e.siteid, e.locid, e.dept_id, e.cellid, e.eqid, e.eqnum, isnull(e.eqdesc, 'No Description') as eqdesc, isnull(f.func_id, 0) as func_id, " _
                + "e.locked, e.lockedby, " _
                + "f.func, isnull(c.comid, 0) as comid, c.compnum, cnt = (select count(c.compnum) " _
                + "from components c where c.func_id = f.func_id), " _
                + "epcnt = (select count(*) from pmpictures p where p.eqid = e.eqid and p.funcid is null and p.comid is null), " _
        + "fpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid is null), " _
        + "cpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid = c.comid), f.routing, c.crouting " _
                + "from equipment e left outer join functions f on f.eqid = e.eqid " _
                + "left outer join components c on c.func_id = f.func_id " _
                + "where(e.dept_id = '" & dept & "') order by e.eqnum, f.routing, c.crouting"
            End If
        ElseIf typ = "dloc" Then
            If chk = "yes" Then
                sql = "select distinct e.siteid, e.locid, e.dept_id, e.cellid, e.eqid, e.eqnum, isnull(e.eqdesc, 'No Description') as eqdesc, isnull(f.func_id, 0) as func_id, " _
                + "e.locked, e.lockedby, " _
                + "f.func, isnull(c.comid, 0) as comid, c.compnum, cnt = (select count(c.compnum) " _
                + "from components c where c.func_id = f.func_id), " _
                + "epcnt = (select count(*) from pmpictures p where p.eqid = e.eqid and p.funcid is null and p.comid is null), " _
        + "fpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid is null), " _
        + "cpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid = c.comid), f.routing, c.crouting " _
                + "from equipment e left outer join functions f on f.eqid = e.eqid " _
                + "left outer join components c on c.func_id = f.func_id " _
                + "where(e.dept_id = '" & dept & "' and e.cellid = '" & cell & "' and e.locid = '" & lid & "') order by e.eqnum, f.routing, c.crouting" ' and e.locid = '" & lid & "'
            Else
                sql = "select distinct e.siteid, e.locid, e.dept_id, e.cellid, e.eqid, e.eqnum, isnull(e.eqdesc, 'No Description') as eqdesc, isnull(f.func_id, 0) as func_id, " _
                + "e.locked, e.lockedby, " _
                + "f.func, isnull(c.comid, 0) as comid, c.compnum, cnt = (select count(c.compnum) " _
                + "from components c where c.func_id = f.func_id), " _
                + "epcnt = (select count(*) from pmpictures p where p.eqid = e.eqid and p.funcid is null and p.comid is null), " _
        + "fpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid is null), " _
        + "cpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid = c.comid), f.routing, c.crouting " _
                + "from equipment e left outer join functions f on f.eqid = e.eqid " _
                + "left outer join components c on c.func_id = f.func_id " _
                + "where(e.dept_id = '" & dept & "' and e.locid = '" & lid & "') order by e.eqnum, f.routing, c.crouting" ' and e.locid = '" & lid & "'
            End If
            Dim sqll As String
            sqll = "select distinct e.siteid, e.locid, e.dept_id, e.cellid, e.eqid, e.eqnum, isnull(e.eqdesc, 'No Description') as eqdesc, isnull(f.func_id, 0) as func_id, " _
          + "e.locked, e.lockedby, " _
          + "f.func, isnull(c.comid, 0) as comid, c.compnum, cnt = (select count(c.compnum) " _
          + "from components c where c.func_id = f.func_id), " _
          + "epcnt = (select count(*) from pmpictures p where p.eqid = e.eqid and p.funcid is null and p.comid is null), " _
        + "fpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid is null), " _
        + "cpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid = c.comid), f.routing, c.crouting " _
          + "from equipment e left outer join functions f on f.eqid = e.eqid " _
          + "left outer join components c on c.func_id = f.func_id " _
          + "where(e.locid = '" & lid & "') order by e.eqnum, f.routing, c.crouting"
            GetBoth(sql, sqll)
            Exit Sub
        Else
            typ = "reg"
            If chk = "yes" Then
                sql = "select distinct e.siteid, e.locid, e.dept_id, e.cellid, e.eqid, e.eqnum, isnull(e.eqdesc, 'No Description') as eqdesc, isnull(f.func_id, 0) as func_id, " _
                + "e.locked, e.lockedby, " _
                + "f.func, isnull(c.comid, 0) as comid, c.compnum, cnt = (select count(c.compnum) " _
                + "from components c where c.func_id = f.func_id), " _
                + "epcnt = (select count(*) from pmpictures p where p.eqid = e.eqid and p.funcid is null and p.comid is null), " _
        + "fpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid is null), " _
        + "cpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid = c.comid), f.routing, c.crouting " _
                + "from equipment e left outer join functions f on f.eqid = e.eqid " _
                + "left outer join components c on c.func_id = f.func_id " _
                + "where(e.dept_id = '" & dept & "' and e.cellid = '" & cell & "') order by e.eqnum, f.routing, c.crouting"
            Else
                sql = "select distinct e.siteid, e.locid, e.dept_id, e.cellid, e.eqid, e.eqnum, isnull(e.eqdesc, 'No Description') as eqdesc, isnull(f.func_id, 0) as func_id, " _
                + "e.locked, e.lockedby, " _
                + "f.func, isnull(c.comid, 0) as comid, c.compnum, cnt = (select count(c.compnum) " _
                + "from components c where c.func_id = f.func_id), " _
                + "epcnt = (select count(*) from pmpictures p where p.eqid = e.eqid and p.funcid is null and p.comid is null), " _
        + "fpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid is null), " _
        + "cpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid = c.comid), f.routing, c.crouting " _
                + "from equipment e left outer join functions f on f.eqid = e.eqid " _
                + "left outer join components c on c.func_id = f.func_id " _
                + "where(e.dept_id = '" & dept & "') order by e.eqnum, f.routing, c.crouting"
            End If
        End If


        Dim locby As String
        Dim lock As String = "0"
        Dim fid As String = "0"
        Dim cid As String = "0"
        Dim eid As String = "0"
        Dim loc As String = "0"
        Dim cidhold As Integer = 0

        Dim epcnt As Integer = 0
        Dim fpcnt As Integer = 0
        Dim cpcnt As Integer = 0

        Dim cnt As Integer = 0
        'h.Open()
        dr = main.GetRdrData(sql)
        Dim nchk As Integer = 0
        While dr.Read
            nchk = 1
            If dr.Item("eqid") <> eid Then
                If eid <> 0 Then
                    sb.Append("</table></td></tr>")
                End If
                eid = dr.Item("eqid").ToString
                sid = dr.Item("siteid").ToString
                loc = dr.Item("locid").ToString
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                epcnt = dr.Item("epcnt").ToString
                If clid <> "" Then
                    chk = "yes"
                Else
                    chk = "no"
                End If
                eqnum = dr.Item("eqnum").ToString
                eqdesc = dr.Item("eqdesc").ToString
                lock = dr.Item("locked").ToString
                locby = dr.Item("lockedby").ToString
                'sb.Append("<tr><td><img id='i" + eid + "' ")
                'sb.Append("onclick=""fclose('t" + eid + "', 'i" + eid + "');""")
                'sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>")

                sb.Append("<tr><td><img id='i" + eid + "' ")
                sb.Append("onclick=""getdets('" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & lid & "','" & eqnum & "');""")
                sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>")

                If epcnt <> 0 Then
                    sb.Append("<td><img src=""../images/appbuttons/minibuttons/gridpic.gif"" onclick=""geteqport('" + eid + "')""></td>" & vbCrLf)
                Else
                    sb.Append("<td><img src=""../images/appbuttons/minibuttons/gridpicdis.gif""></td>" & vbCrLf)
                End If
                sb.Append("<td colspan=""2"" class=""plainlabel""><a href=""#"" onclick=""gotoeq('" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & loc & "')"" class=""linklabel"">" & eqnum & "</a> - " & eqdesc)
                'sb.Append("</td></tr>" & vbCrLf)
                If lock = "0" OrElse Len(lock) = 0 Then
                    sb.Append("</td></tr>" & vbCrLf)
                Else
                    sb.Append("&nbsp;<img src='../images/appbuttons/minibuttons/lillock.gif' ")
                    sb.Append("onmouseover=""return overlib('" & tmod.getov("cov267", "DeptArch.aspx.vb") & ": " & locby & "')"" ")
                    sb.Append("onmouseout=""return nd()""></td></tr>" & vbCrLf)
                End If
                sb.Append("<tr><td></td><td colspan=""3""><table class=""details"" cellspacing=""0"" id='t" + eid + "' border=""0"">")
                sb.Append("<tr><td width=""15""></td><td width=""15""></td><td width=""170""></td></tr>")
            End If




        End While
        dr.Close()
        'h.Dispose()
        sb.Append("</td></tr></table></td></tr></table>")
        'Response.Write(sb.ToString)
        If nchk <> 0 Then
            tdarch.InnerHtml = sb.ToString
        Else
            tdarch.InnerHtml = "<font class='plainlabelblue'>Waiting for Location...</font>"
        End If

    End Sub
    Private Sub GetBoth(ByVal sql As String, ByVal sqll As String)
        Dim sb As StringBuilder = New StringBuilder
        Dim sid, did, clid As String
        Dim eqnum As String = ""
        Dim eqid As String = ""
        Dim eqdesc As String

        dr = main.GetRdrData(sql)
        Dim locby As String
        Dim lock As String = "0"
        Dim fid As String = "0"
        Dim cid As String = "0"
        Dim eid As String = "0"
        Dim loc As String = "0"
        Dim cidhold As Integer = 0
        Dim cnt As Integer = 0

        sb.Append("<table cellspacing=""0"" border=""0""><tr>")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""155""></tr>" & vbCrLf)
        sb.Append("<tr><td><img id=""t1"" ")
        sb.Append("onclick=""tclose('top1','t1');""")
        sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>")
        sb.Append("<td colspan=""3"" class=""bluelabel"">" & tmod.getlbl("cdlbl434" , "DeptArch.aspx.vb") & "</td></tr>")
        sb.Append("<tr id=""top1""><td></td><td colspan=""3""><table cellspacing=""0"" border=""0""><tr>")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""155""></tr>" & vbCrLf)
        While dr.Read
            If dr.Item("eqid") <> eid Then
                If eid <> 0 Then
                    sb.Append("</table></td></tr>")
                End If
                eid = dr.Item("eqid").ToString
                sid = dr.Item("siteid").ToString
                loc = dr.Item("locid").ToString
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                If clid <> "" Then
                    chk = "yes"
                Else
                    chk = "no"
                End If
                eqnum = dr.Item("eqnum").ToString
                eqdesc = dr.Item("eqdesc").ToString
                lock = dr.Item("locked").ToString
                locby = dr.Item("lockedby").ToString
                sb.Append("<tr><td><img id='i" + eid + "' ")
                sb.Append("onclick=""fclose('t" + eid + "', 'i" + eid + "');""")
                sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>")
                sb.Append("<td colspan=""2"" class=""plainlabel""><a href=""#"" onclick=""gotoeq('" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & loc & "')"" class=""linklabel"">" & eqnum & "</a> - " & eqdesc)
                'sb.Append("</td></tr>" & vbCrLf)
                If lock = "0" OrElse Len(lock) = 0 Then
                    sb.Append("</td></tr>" & vbCrLf)
                Else
                    sb.Append("&nbsp;<img src='../images/appbuttons/minibuttons/lillock.gif' ")
                    sb.Append("onmouseover=""return overlib('" & tmod.getov("cov268" , "DeptArch.aspx.vb") & ": " &  locby & "')"" ")
                    sb.Append("onmouseout=""return nd()""></td></tr>" & vbCrLf)
                End If
                sb.Append("<tr><td></td><td colspan=""2""><table class=""details"" cellspacing=""0"" id='t" + eid + "' border=""0"">")
                sb.Append("<tr><td width==""15""></td><td width==""155""></td></tr>")
            End If


            If dr.Item("func_id").ToString <> fid Then
                If dr.Item("func_id").ToString <> "0" Then
                    eid = dr.Item("eqid").ToString
                    fid = dr.Item("func_id").ToString
                    sid = dr.Item("siteid").ToString
                    loc = dr.Item("locid").ToString
                    did = dr.Item("dept_id").ToString
                    clid = dr.Item("cellid").ToString
                    cidhold = dr.Item("cnt").ToString
                    sb.Append("<tr>" & vbCrLf & "<td colspan=""2""><table cellspacing=""0"" border=""0"">" & vbCrLf)
                    sb.Append("<tr><td><img id='i" + fid + "' onclick=""fclose('t" + fid + "', 'i" + fid + "');"" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                    sb.Append("<td ><a href=""#"" onclick=""gotofu('" & eid & "', '" & fid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & loc & "')"" class=""linklabelblk"">" & dr.Item("func").ToString & "</a></td></tr></table></td></tr>" & vbCrLf)
                    If dr.Item("comid").ToString <> cid Then
                        If dr.Item("comid").ToString <> "0" Then
                            cid = dr.Item("comid").ToString
                            eid = dr.Item("eqid").ToString
                            fid = dr.Item("func_id").ToString
                            sid = dr.Item("siteid").ToString
                            loc = dr.Item("locid").ToString
                            did = dr.Item("dept_id").ToString
                            clid = dr.Item("cellid").ToString
                            If cnt = 0 Then
                                cnt = cnt + 1
                                sb.Append("<tr><td width=""15""></td>" & vbCrLf & "<td width=""155""><table border=""0"" class=""details"" cellspacing=""0"" id=""t" + fid + """>" & vbCrLf)
                                sb.Append("<tr><td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & eid & "', '" & fid & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & loc & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                                If cnt = cidhold Then
                                    cnt = 0
                                    sb.Append("</table></td></tr>")
                                End If
                            End If
                        Else
                            'cnt = 0
                            'sb.Append("</table></td></tr>")
                        End If

                    End If
                Else
                    fid = "0"
                End If

            ElseIf dr.Item("comid").ToString <> cid Then
                If fid <> "0" Then
                    cid = dr.Item("comid").ToString
                    If cnt = 0 Then
                        cnt = cnt + 1
                        sb.Append("<tr><td></td><td></td>" & vbCrLf & "<td><table cellspacing=""0"" id=""t" + fid + """>" & vbCrLf)
                        sb.Append("<tr><td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & eid & "', '" & fid & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & loc & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                    Else
                        cnt = cnt + 1
                        sb.Append("<tr><td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & eid & "', '" & fid & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & loc & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                    End If
                    If cnt = cidhold Then
                        cnt = 0
                        sb.Append("</table></td></tr>")
                    End If
                    'Else
                    'cid = "0"
                    'cnt = 0
                    'sb.Append("</table></td></tr>")
                End If

            End If

        End While
        dr.Close()
        'h.Dispose()
        sb.Append("</td></tr></table></td></tr></table></td></tr></table>")

        lock = "0"
        fid = "0"
        cid = "0"
        eid = "0"
        loc = "0"
        cidhold = 0
        cnt = 0

        dr = main.GetRdrData(sqll)

        sb.Append("<table cellspacing=""0"" border=""0""><tr>")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""155""></tr>" & vbCrLf)
        sb.Append("<tr><td><img id=""t2"" ")
        sb.Append("onclick=""tclose('top2','t2');""")
        sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>")
        sb.Append("<td colspan=""3"" class=""bluelabel"">" & tmod.getlbl("cdlbl435" , "DeptArch.aspx.vb") & "</td></tr>")
        sb.Append("<tr id=""top2""><td></td><td colspan=""3""><table cellspacing=""0"" border=""0""><tr>")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""155""></tr>" & vbCrLf)
        While dr.Read
            If dr.Item("eqid") <> eid Then
                If eid <> 0 Then
                    sb.Append("</table></td></tr>")
                End If
                eid = dr.Item("eqid").ToString
                sid = dr.Item("siteid").ToString
                loc = dr.Item("locid").ToString
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                If clid <> "" Then
                    chk = "yes"
                Else
                    chk = "no"
                End If
                eqnum = dr.Item("eqnum").ToString
                eqdesc = dr.Item("eqdesc").ToString
                lock = dr.Item("locked").ToString
                locby = dr.Item("lockedby").ToString
                sb.Append("<tr><td><img id='i" + eid + "' ")
                sb.Append("onclick=""fclose('t1" + eid + "', 'i" + eid + "');""")
                sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>")
                sb.Append("<td colspan=""2"" class=""plainlabel""><a href=""#"" onclick=""gotoeq('" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & loc & "')"" class=""linklabel"">" & eqnum & "</a> - " & eqdesc)
                'sb.Append("</td></tr>" & vbCrLf)
                If lock = "0" OrElse Len(lock) = 0 Then
                    sb.Append("</td></tr>" & vbCrLf)
                Else
                    sb.Append("&nbsp;<img src='../images/appbuttons/minibuttons/lillock.gif' ")
                    sb.Append("onmouseover=""return overlib('" & tmod.getov("cov269" , "DeptArch.aspx.vb") & ": " &  locby & "')"" ")
                    sb.Append("onmouseout=""return nd()""></td></tr>" & vbCrLf)
                End If
                sb.Append("<tr><td></td><td colspan=""2""><table class=""details"" cellspacing=""0"" id='t1" + eid + "' border=""0"">")
                sb.Append("<tr><td width==""15""></td><td width==""155""></td></tr>")
            End If


            If dr.Item("func_id").ToString <> fid Then
                If dr.Item("func_id").ToString <> "0" Then
                    eid = dr.Item("eqid").ToString
                    fid = dr.Item("func_id").ToString
                    sid = dr.Item("siteid").ToString
                    loc = dr.Item("locid").ToString
                    did = dr.Item("dept_id").ToString
                    clid = dr.Item("cellid").ToString
                    cidhold = dr.Item("cnt").ToString
                    sb.Append("<tr>" & vbCrLf & "<td colspan=""2""><table cellspacing=""0"" border=""0"">" & vbCrLf)
                    sb.Append("<tr><td><img id='i" + fid + "' onclick=""fclose('t1" + fid + "', 'i" + fid + "');"" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                    sb.Append("<td ><a href=""#"" onclick=""gotofu('" & eid & "', '" & fid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & loc & "')"" class=""linklabelblk"">" & dr.Item("func").ToString & "</a></td></tr></table></td></tr>" & vbCrLf)
                    If dr.Item("comid").ToString <> cid Then
                        If dr.Item("comid").ToString <> "0" Then
                            cid = dr.Item("comid").ToString
                            eid = dr.Item("eqid").ToString
                            fid = dr.Item("func_id").ToString
                            sid = dr.Item("siteid").ToString
                            did = dr.Item("dept_id").ToString
                            clid = dr.Item("cellid").ToString
                            If cnt = 0 Then
                                cnt = cnt + 1
                                sb.Append("<tr><td width=""15""></td>" & vbCrLf & "<td width=""155""><table border=""0"" class=""details"" cellspacing=""0"" id=""t" + fid + """>" & vbCrLf)
                                sb.Append("<tr><td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & eid & "', '" & fid & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & loc & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                                If cnt = cidhold Then
                                    cnt = 0
                                    sb.Append("</table></td></tr>")
                                End If
                            End If
                        Else
                            'cnt = 0
                            'sb.Append("</table></td></tr>")
                        End If

                    End If
                Else
                    fid = "0"
                End If

            ElseIf dr.Item("comid").ToString <> cid Then
                If fid <> "0" Then
                    cid = dr.Item("comid").ToString
                    If cnt = 0 Then
                        cnt = cnt + 1
                        sb.Append("<tr><td></td><td></td>" & vbCrLf & "<td><table cellspacing=""0"" id=""t2" + fid + """>" & vbCrLf)
                        sb.Append("<tr><td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & eid & "', '" & fid & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & loc & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                    Else
                        cnt = cnt + 1
                        sb.Append("<tr><td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & eid & "', '" & fid & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & loc & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                    End If
                    If cnt = cidhold Then
                        cnt = 0
                        sb.Append("</table></td></tr>")
                    End If
                    'Else
                    'cid = "0"
                    'cnt = 0
                    'sb.Append("</table></td></tr>")
                End If

            End If

        End While
        dr.Close()
        'h.Dispose()
        sb.Append("</td></tr></table></td></tr></table></td></tr></table>")
        'Response.Write(sb.ToString)
        tdarch.InnerHtml = sb.ToString

    End Sub
End Class
