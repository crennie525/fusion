<%@ Page Language="vb" AutoEventWireup="false" Codebehind="weights.aspx.vb" Inherits="lucy_r12.weights" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>weights</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts1/weightsaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  class="tbg" onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 0px; LEFT: 0px" width="400">
				<TBODY>
					<tr>
						<td colSpan="2"><asp:label id="lblcat" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Small"
								ForeColor="Blue"></asp:label></td>
					</tr>
					<tr>
						<td colspan="2">
							<hr style="BORDER-BOTTOM: #0000ff 2px solid; BORDER-LEFT: #0000ff 2px solid; BORDER-TOP: #0000ff 2px solid; BORDER-RIGHT: #0000ff 2px solid">
						</td>
					</tr>
					<tr>
						<td><asp:label id="Label3" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Weight</asp:label></td>
						<td class="label"><input id="rb1" onclick="document.getElementById('lblwt2').value='1';" type="radio" value="1st value"
								name="wt" runat="server">1 <input id="rb2" onclick="document.getElementById('lblwt2').value='2';" type="radio" value="2nd value"
								name="wt" runat="server">2 <input id="rb3" onclick="document.getElementById('lblwt2').value='3';" type="radio" value="3rd value"
								name="wt" runat="server">3 <input id="rb4" onclick="document.getElementById('lblwt2').value='4';" type="radio" value="4th value"
								name="wt" runat="server">4 <input id="rb5" onclick="document.getElementById('lblwt2').value='5';" type="radio" value="5th value"
								name="wt" runat="server">5
							<asp:label id="lblwt" runat="server" ForeColor="White"></asp:label><asp:label id="lblcid" runat="server" ForeColor="White"></asp:label><asp:label id="lblecrid" runat="server" ForeColor="White"></asp:label></td>
					</tr>
					<tr>
						<td><asp:label id="Label4" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Level 5</asp:label></td>
						<td><asp:textbox id="t5" runat="server" Font-Names="Arial" Font-Size="X-Small" TextMode="MultiLine"
								Width="300px" CssClass="plainlabel" MaxLength="250"></asp:textbox></td>
					</tr>
					<tr>
						<td><asp:label id="Label5" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Level 4</asp:label></td>
						<td><asp:textbox id="t4" runat="server" Font-Names="Arial" Font-Size="X-Small" TextMode="MultiLine"
								Width="300px" CssClass="plainlabel" MaxLength="250"></asp:textbox></td>
					</tr>
					<tr>
						<td><asp:label id="Label6" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Level 3</asp:label></td>
						<td><asp:textbox id="t3" runat="server" Font-Names="Arial" Font-Size="X-Small" TextMode="MultiLine"
								Width="300px" CssClass="plainlabel" MaxLength="250"></asp:textbox></td>
					</tr>
					<tr>
						<td><asp:label id="Label7" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Level 2</asp:label></td>
						<td><asp:textbox id="t2" runat="server" Font-Names="Arial" Font-Size="X-Small" TextMode="MultiLine"
								Width="300px" CssClass="plainlabel" MaxLength="250"></asp:textbox></td>
					</tr>
					<tr>
						<td><asp:label id="Label8" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Level 1</asp:label></td>
						<td><asp:textbox id="t1" runat="server" Font-Names="Arial" Font-Size="X-Small" TextMode="MultiLine"
								Width="300px" CssClass="plainlabel" MaxLength="250"></asp:textbox></td>
					</tr>
					<tr>
						<td class="plainlabelblue" colSpan="2">
							<P><asp:Label id="lang2553" runat="server">Choose a weight for this category to represent its importance to the organization. Define 5 levels of impact on the organization with 5 being the highest.</asp:Label></P>
						</td>
					</tr>
					<tr>
						<td align="right" colSpan="2"><asp:imagebutton id="btnsave" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif"></asp:imagebutton></td>
					</tr>
				</TBODY>
			</table>
			<input type="hidden" id="lblwt2" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
