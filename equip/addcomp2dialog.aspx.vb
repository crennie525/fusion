﻿Public Class addcomp2dialog
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim Login, ro, tpm, sid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try
        If Not IsPostBack Then
            Dim cid, eqid, fuid As String
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            Try
                tpm = Request.QueryString("tpm").ToString
                If tpm = "no" Then
                    tpm = "N"
                End If
            Catch ex As Exception
                tpm = "N"
            End Try
            Try
                sid = Request.QueryString("sid").ToString
                fuid = Request.QueryString("fuid").ToString
                If Len(fuid) <> 0 AndAlso fuid <> "" AndAlso fuid <> "0" Then
                    cid = Request.QueryString("cid").ToString
                    eqid = Request.QueryString("eqid").ToString
                Else
                    Dim strMessage As String = tmod.getmsg("cdstr834", "CompDialog.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
                iffmh.Attributes.Add("src", "../equip/addcomp3.aspx?cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&sid=" + sid + "&date=" + Now + "&ro=" + ro)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr835", "CompDialog.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try
        End If
    End Sub

End Class