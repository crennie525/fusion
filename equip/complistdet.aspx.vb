

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class complistdet
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, fuid, Login, srchtyp As String
    Dim dr As SqlDataReader
    Dim rep As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents divco As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsrchtyp As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            'lbllog.Value = "no"
            'Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                fuid = Request.QueryString("fuid").ToString
                lblfuid.Value = fuid
                rep.Open()
                GetComponents()
                rep.Dispose()
            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Sub GetComponents()

        fuid = lblfuid.Value
        Dim ci, co, cd, cs As String
        Dim sb As New StringBuilder
        srchtyp = lblsrchtyp.Value

        sql = "select comid, compnum, compdesc, spl from components where func_id = '" & fuid & "' order by crouting"
        dr = rep.GetRdrData(sql)
        sb.Append("<table>")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""160"">" & tmod.getlbl("cdlbl427" , "complistdet.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""160"">" & tmod.getlbl("cdlbl428" , "complistdet.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""160"">" & tmod.getlbl("cdlbl429" , "complistdet.aspx.vb") & "</td></tr>" & vbCrLf)
        While dr.Read
            ci = dr.Item("comid").ToString
            co = dr.Item("compnum").ToString
            cd = dr.Item("compdesc").ToString
            cs = dr.Item("spl").ToString
            sb.Append("<tr><td class=""plainlabel"">")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getdco('" & ci & "','" & co & "')"">")
            sb.Append(co & "</a>")
            sb.Append("</td>")

            sb.Append("<td class=""plainlabel"">" & cd & "</td>")
            sb.Append("<td class=""plainlabel"">" & cs & "</td>")
            sb.Append("</tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        divco.InnerHtml = sb.ToString

    End Sub

End Class
