<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CompGrid.aspx.vb" Inherits="lucy_r12.CompGrid" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>CompGrid</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts/sessrefdialog.js"></script>
		<script  type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script  src="../scripts1/CompGridaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script  type="text/javascript">
         function getecd1() {
             var compnum = document.getElementById("txtnewcomp").value;
             //if (compnum != "") {
             var eReturn = window.showModalDialog("compdiv1dialog.aspx?compnum=" + compnum + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
             if (eReturn) {
                 //alert(eReturn)
                 var ret = eReturn.split("~");
                 var chk = ret[0];
                 if (chk != "can") {
                     var chk2 = ret[1];
                     if (chk2 != compnum) {
                         document.getElementById("txtnewcomp").value = chk2;
                     }
                     document.getElementById("lblecd1id").value = chk;
                     document.getElementById("form1").submit();
                 }
                 else {
                     return false;
                 }

             }
             else {

                 return false;
             }
             //}
         }
            </script>
	</HEAD>
	<body onload="scrolltop();checkit();checkit2();" onunload="stoptimer();" >
		<form id="form1" method="post" runat="server">
			<table id="scrollmenu" style="Z-INDEX: 102; POSITION: absolute; TOP: 8px; LEFT: 4px" width="930"
				bgColor="#0066ff">
				<tr height="22">
					<td class="label"><asp:Label id="lang2054" runat="server">Equipment#</asp:Label></td>
					<td class="whitelabel" id="tdeq" runat="server"></td>
					<td class="label"><asp:Label id="lang2055" runat="server">Description</asp:Label></td>
					<td class="whitelabel" id="tdeqd" runat="server" colspan="2"></td>
				</tr>
				<tr height="22">
					<td class="label"><asp:Label id="lang2056" runat="server">Function#</asp:Label></td>
					<td class="whitelabel" id="tdfu" runat="server"></td>
					<td class="label"><asp:Label id="lang2057" runat="server">Description</asp:Label></td>
					<td class="whitelabel" id="tdfud" runat="server" colspan="2"></td>
				</tr>
				<tr>
					<td><asp:label id="Label9" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Current Sort</asp:label></td>
					<td><asp:label id="lblsort" runat="server" ForeColor="White" Font-Bold="True" Font-Names="Arial"
							Font-Size="X-Small">Current Sort</asp:label></td>
					<td class="label"><asp:Label id="lang2058" runat="server">New Component#</asp:Label></td>
					<td>
						<asp:TextBox id="txtnewcomp" runat="server" Width="158px" MaxLength="100"></asp:TextBox></td>
					<td align="right">
						<asp:imagebutton id="addfunc" runat="server" ImageUrl="../images/appbuttons/bgbuttons/badd.gif"></asp:imagebutton>
						<asp:ImageButton id="ibtnreturn" runat="server" ImageUrl="../images/appbuttons/bgbuttons/return.gif"></asp:ImageButton></td>
				</tr>
				<tr>
					<td colspan="5" align="center"><asp:label id="Label1" runat="server" ForeColor="White" Font-Bold="False" Font-Names="Arial"
							Font-Size="X-Small">Note - List Order# provided for convenience only and will not be reflected in CMMS Output</asp:label></td>
				</tr>
				<tr>
					<td colspan="5" align="center"><asp:label id="ErrorLabel" runat="server" ForeColor="White" Font-Bold="True" Font-Names="Arial"
							Font-Size="X-Small"></asp:label></td>
				</tr>
				<tr>
					<td style="width: 100px"></td>
					<td width="400"></td>
					<td style="width: 130px"></td>
					<td width="160"></td>
					<td width="200"></td>
				</tr>
			</table>
			<table style="Z-INDEX: 101; POSITION: absolute; TOP: 112px; LEFT: 4px" width="1130" id="Table1">
				<TBODY>
					<tr>
						<td><asp:datagrid id="dgcomp" runat="server" AllowSorting="True" GridLines="None" AutoGenerateColumns="False"
								CellSpacing="1">
								<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
								<ItemStyle CssClass="plainlabel"></ItemStyle>
								<HeaderStyle CssClass="btmmenu plainlabel" Height="26px"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Edit">
										<HeaderStyle Width="90px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:ImageButton id="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
												ToolTip="Edit Record" CommandName="Edit"></asp:ImageButton>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:imagebutton id="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
												ToolTip="Save Changes" CommandName="Update"></asp:imagebutton>
											<asp:imagebutton id="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
												ToolTip="Cancel Changes" CommandName="Cancel"></asp:imagebutton>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="crouting desc" HeaderText="Sequence#">
										<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=lblroute runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.crouting") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox id=txtrouting runat="server" style="width: 40px" Text='<%# DataBinder.Eval(Container, "DataItem.crouting") %>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="compnum desc" HeaderText="Component#">
										<HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:textbox id="txtcompnum" runat="server" Width="130px" MaxLength="100" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
											</asp:textbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="290px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.compdesc") %>' ID="Label3" >
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox width="280" runat="server" id="txtdesc" MaxLength="100" Text='<%# DataBinder.Eval(Container, "DataItem.compdesc") %>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<ItemTemplate>
											<asp:Label runat="server" id="lblcoid" Text='<%# DataBinder.Eval(Container, "DataItem.comid") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label runat="server" id="lblcoide" Text='<%# DataBinder.Eval(Container, "DataItem.comid") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Special Description">
										<HeaderStyle Width="290px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label runat="server" id="lblspl" Text='<%# DataBinder.Eval(Container, "DataItem.spl") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox runat="server" id="txtspl" width="280" MaxLength="200" Text='<%# DataBinder.Eval(Container, "DataItem.spl") %>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Designation">
										<HeaderStyle Width="290px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label6" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.desig") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox runat="server" width="280" id="txtdesig" MaxLength="200" Text='<%# DataBinder.Eval(Container, "DataItem.desig") %>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td><asp:textbox id="xcoord" runat="server" ForeColor="Black" BorderColor="White" BorderStyle="None"
								Width="32px"></asp:textbox><asp:textbox id="ycoord" runat="server" ForeColor="Black" BorderColor="White" BorderStyle="None"
								Width="32px"></asp:textbox><asp:textbox id="txtco" runat="server" ForeColor="White" BorderColor="White" BorderStyle="None"
								Width="24px"></asp:textbox><asp:textbox id="txtfi" runat="server" ForeColor="White" BorderColor="White" BorderStyle="None"
								Width="24px"></asp:textbox></td>
					</tr>
					<tr>
						<td></td>
					</tr>
				</TBODY>
			</table>
			<iframe id="ifsession" class="details" src="" frameBorder="no" runat="server" style="Z-INDEX: 0">
			</iframe><input type="hidden" id="lblsessrefresh" runat="server" NAME="lblsessrefresh">
			<input type="hidden" id="lblcid" runat="server" NAME="lblcid"><input type="hidden" id="lbleqid" runat="server" NAME="lbleqid">
			<input type="hidden" id="lblcurrsort" runat="server" NAME="lblcurrsort"> <input type="hidden" id="lblco" runat="server" name="lblco">
			<input type="hidden" id="lblfuid" runat="server" name="lblfuid"><input type="hidden" id="spy" runat="server">
			<input type="hidden" id="lblchk" runat="server"><input id="lblsid" type="hidden" runat="server" NAME="lblsid"><input id="lbldid" type="hidden" runat="server" NAME="lbldid">
			<input id="lblclid" type="hidden" runat="server" NAME="lblclid"> <input type="hidden" id="lbleqchk" runat="server">
			<input type="hidden" id="lbluser" runat="server"> <input type="hidden" id="lblrouting" runat="server">
			<input type="hidden" id="lbllid" runat="server">
		<input type="hidden" id="lblustr" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblnewecd1id" runat="server" />
<input type="hidden" id="lblwho" runat="server" />
<input type="hidden" id="lbldept" runat="server" />
<input type="hidden" id="lblcell" runat="server" />
<input type="hidden" id="lblloc" runat="server" />
<input type="hidden" id="lbleqidh" runat="server" />
<input type="hidden" id="lbleq" runat="server" />
<input type="hidden" id="lblcoid" runat="server" />
</form>
	</body>
</HTML>
