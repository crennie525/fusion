<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ctadmin.aspx.vb" Inherits="lucy_r12.ctadmin" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>ctadmin</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="label" height="20"><asp:Label id="lang2061" runat="server">Copy Started</asp:Label></td>
					<td class="plainlabel" id="tdss" runat="server"><asp:Label id="lang2062" runat="server">Not Provided</asp:Label></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang2063" runat="server">Server Page Accessed</asp:Label></td>
					<td class="plainlabel" id="tdspa" runat="server"><asp:Label id="lang2064" runat="server">Not Provided</asp:Label></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang2065" runat="server">Copy Completed at Server</asp:Label></td>
					<td class="plainlabel" id="tdscs" runat="server"><asp:Label id="lang2066" runat="server">Not Provided</asp:Label></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang2067" runat="server">Client Page Loaded</asp:Label></td>
					<td class="plainlabel" id="tdcpl" runat="server"><asp:Label id="lang2068" runat="server">Not Provided</asp:Label></td>
				</tr>
				<tr id="trmsg" runat="server" class="details">
					<td colspan="2" align="center" class="plainlabelblue" height="20"><asp:Label id="lang2069" runat="server">Increasing the Page Size will result in longer search times</asp:Label></td>
				</tr>
				<tr>
					<td colspan="2" align="center" class="details">
						<table>
							<tr>
								<td class="label"><asp:Label id="lang2070" runat="server">Page Size</asp:Label></td>
								<td><asp:TextBox id="txtps" runat="server" Width="50px"></asp:TextBox></td>
								<td><img src="../images/appbuttons/minibuttons/saveDisk1.gif"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
