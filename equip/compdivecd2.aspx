﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="compdivecd2.aspx.vb" Inherits="lucy_r12.compdivecd2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet"/>
    <script  type ="text/javascript">
        function saveecd2() {
            var who = document.getElementById("lblwho").value;
            var chk = document.getElementById("txtnewecd2").value;
            if (who != "") {
                if (who == "txt") {
                    if (chk != "") {
                        if (chk.length > 5) {
                            alert("New Error Code 2 Value Limited to 5 Characters")
                        }
                        else if (chk.length < 5) {
                            var change = confirm("Your New Error Code 2 Value is Less Than 5 Characters\nAre you sure you want to continue?");
                            if (chang == true) {
                                document.getElementById("lblsubmit").value = "go";
                                document.getElementById("form1").submit();
                            }
                            else {
                                alert("Action Cancelled")
                            }
                        }
                        else {
                            document.getElementById("lblsubmit").value = "go";
                            document.getElementById("form1").submit();
                        }

                    }
                }
                else if (who == "dd") {
                    var ec2 = document.getElementById("lblecd2id").value;
                    if (ec2 != "") {
                        document.getElementById("lblret").value = "go";
                        checkret();
                    }
            
                }
            }
        }
        function checkret() {
            var chk = document.getElementById("lblret").value;
            if (chk == "go") {
                var ec2 = document.getElementById("lblecd2id").value;
                var ec1 = document.getElementById("lblecd1id").value;
                var ret = ec2 + "-" + ec1;
                //alert(ret)
                window.parent.handlereturn(ret);
            }
        }
        function fillnew(val) {
            document.getElementById("lblwho").value = "txt";
            document.getElementById("ddecd2").value = "Select";
            document.getElementById("lblecd2id").value = "";
        }
        function filldown(val) {
            //alert(val)
            if (val != "Select" && val != "0") {
                document.getElementById("txtnewecd2").value = "";
                document.getElementById("lblwho").value = "dd";
                document.getElementById("lblecd2id").value = val;
            }
            else {
                document.getElementById("lblwho").value = "";
                document.getElementById("lblecd2id").value = "";
            }
        }
    </script>
</head>
<body onload="checkret();">
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td class="redlabel">Enter New Error Code 2 Value</td>
    </tr>
    <tr>
    <td><asp:TextBox ID="txtnewecd2" runat="server"></asp:TextBox></td>
    </tr>
    
    <tr id="tr1" runat="server">
    <td class="redlabel">or Select an Existing Error Code 2 Value from this Component</td>
    </tr>
    <tr id="tr2" runat="server">
    <td><asp:DropDownList ID="ddecd2" runat="server">
        </asp:DropDownList></td>
    </tr>
    <tr>
     <td align="right"><img src="../images/appbuttons/minibuttons/savedisk1.gif" alt="" onclick="saveecd2();" /></td>
    </tr>
    </table>
    </div>
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblecd1id" runat="server" />
    <input type="hidden" id="lblecd2id" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblnewecd" runat="server" />
    <input type="hidden" id="lblecd2txt" runat="server" />
    </form>
</body>
</html>
