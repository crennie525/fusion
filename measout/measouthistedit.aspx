<%@ Page Language="vb" AutoEventWireup="false" Codebehind="measouthistedit.aspx.vb" Inherits="lucy_r12.measouthistedit" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>measouthistedit</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script language="JavaScript" src="../scripts1/measouthisteditaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  class="tbg" onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table id="scrollmenu" width="364">
				<tr>
					<td vAlign="top" align="right"><IMG id="btnreturn" onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
							runat="server"></td>
				</tr>
				<tr height="22">
					<td class="thdrsing label"><asp:Label id="lang3231" runat="server">Measurement History</asp:Label></td>
				</tr>
				<tr>
					<td>
						<table>
							<tr>
								<td class="label"><asp:Label id="lang3232" runat="server">From Date</asp:Label></td>
								<td><asp:textbox id="txtfrom" runat="server" Font-Size="9pt" ReadOnly="True"></asp:textbox></td>
								<td><IMG onclick="getcal('txtfrom');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"></td>
							</tr>
							<tr>
								<td class="label" width="70"><asp:Label id="lang3233" runat="server">To Date</asp:Label></td>
								<td><asp:textbox id="txtto" runat="server" Font-Size="9pt" ReadOnly="True"></asp:textbox></td>
								<td><IMG onclick="getcal('txtto');" height="18" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
										width="18"></td>
								<td align="right" width="110"><IMG onclick="resetsrch();" alt="" src="../images/appbuttons/minibuttons/switch.gif">
									<asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:datagrid id="dgmeas" runat="server" ShowFooter="True" CellPadding="0" GridLines="None" AllowPaging="True"
							AllowCustomPaging="True" AutoGenerateColumns="False" CellSpacing="1" BackColor="transparent">
							<FooterStyle BackColor="transparent"></FooterStyle>
							<EditItemStyle Height="15px" BackColor="transparent"></EditItemStyle>
							<AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
							<ItemStyle CssClass="ptransrow"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel" Height="20px"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Measurement">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Width="90px" Text='<%# DataBinder.Eval(Container, "DataItem.measurement") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtmeas" runat="server" Width="90px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.measurement") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Measure Date">
									<HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label1" runat="server" Width="140px" Text='<%# DataBinder.Eval(Container, "DataItem.measdate") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Textbox1" runat="server" Width="140px" Text='<%# DataBinder.Eval(Container, "DataItem.measdate") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<ItemTemplate>
										<asp:Label id="lblmeasidi" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.mtid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblmeaside" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.mtid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Remove" ItemStyle-HorizontalAlign="Center">
									<HeaderStyle Width="64px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="imgdel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
								ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lblmeasid" runat="server"> <input type="hidden" id="txtpg" runat="server" NAME="Hidden1"><input type="hidden" id="txtpgcnt" runat="server" NAME="txtpgcnt">
			<input type="hidden" id="lblret" runat="server"> <input type="hidden" id="lblfilter" runat="server">
			<input type="hidden" id="lbllog" runat="server" NAME="lbllog">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
