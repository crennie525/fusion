

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class measoutadd
    Inherits System.Web.UI.Page
	Protected WithEvents lang3204 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3203 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3202 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3201 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3200 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3199 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3198 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3197 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3196 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3195 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3194 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3193 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3192 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim madd As New Utilities
    Protected WithEvents lblmeasid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdco As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdout As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtype As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmeas As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdchar As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ddhrspm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddminspm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddapspm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cbdate As System.Web.UI.WebControls.CheckBox
    Protected WithEvents CheckBox1 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cbtime As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdhi As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdspec As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtdate As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtnewmeas As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblhi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmalert As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim measid, Login As String
    Protected WithEvents ddsecspm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here

        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            measid = Request.QueryString("measid").ToString '"2" ' 
            lblmeasid.Value = measid
            madd.Open()
            GetDetails()
            madd.Dispose()
        Else
            If Request.Form("lblsubmit") = "go" Then
                lblsubmit.Value = ""
                madd.Open()
                SaveMeas()
                madd.Dispose()

            End If
        End If
    End Sub
    Private Sub GetDetails()
        measid = lblmeasid.Value
        sql = "select o.measid, o.meastitle, o.type, o.measure, o.hi, o.lo, o.spec, o.char, o.siteid, o.deptid, o.cellid, o.loc, o.eqid, o.funcid, " _
        + "o.comid, o.createdby, o.createdate, isnull(f.func, 'None Selected') as func, isnull(c.compnum, 'None Selected') as compnum, e.eqnum from outmeasure o " _
        + "left join functions f on f.func_id = o.funcid left join components c on c.comid = o.comid " _
        + "left join equipment e on e.eqid = o.eqid where o.measid = '" & measid & "'"
        dr = madd.GetRdrData(sql)
        While dr.Read
            tdeq.InnerHtml = dr.Item("eqnum").ToString
            tdfu.InnerHtml = dr.Item("func").ToString
            tdco.InnerHtml = dr.Item("compnum").ToString
            tdout.InnerHtml = dr.Item("meastitle").ToString
            tdtype.InnerHtml = dr.Item("type").ToString
            tdmeas.InnerHtml = dr.Item("measure").ToString
            tdchar.InnerHtml = dr.Item("char").ToString
            tdhi.InnerHtml = dr.Item("hi").ToString
            lblhi.Value = dr.Item("hi").ToString
            tdlo.InnerHtml = dr.Item("lo").ToString
            lbllo.Value = dr.Item("lo").ToString
            tdspec.InnerHtml = dr.Item("spec").ToString
        End While
        dr.Close()
    End Sub
    Private Sub SaveMeas()
        Dim currd As Date = Now.ToShortDateString
        Dim currt As Date = Now.ToLongTimeString
        If cbdate.Checked <> True And txtdate.Text <> "" Then
            currd = txtdate.Text
        End If
        Dim pstr, ph, pm, pa, ps As String
        If cbtime.Checked <> True Then
            ph = ddhrspm.SelectedValue
            pm = ddminspm.SelectedValue
            pa = ddapspm.SelectedValue
            ps = ddsecspm.SelectedValue
            currt = ph & ":" & pm & ":" & ps & " " & pa
        End If
        Dim currdt As Date = currd & " " & currt
        Dim m As String = txtnewmeas.text
        Dim mchk As Decimal
        Try
            mchk = System.Convert.ToDecimal(m)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1565" , "measoutadd.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        measid = lblmeasid.Value
        sql = "insert into outmeasuretrack (measid, measdate, measurement) values (" _
        + "'" & measid & "','" & currdt & "','" & m & "')"
        madd.Update(sql)

        txtnewmeas.Text = ""
        txtdate.Text = ""
        Try
            ddhrspm.SelectedIndex = 0
        Catch ex As Exception

        End Try

        Try
            ddminspm.SelectedIndex = 0
        Catch ex As Exception

        End Try

        Try
            ddapspm.SelectedIndex = 0
        Catch ex As Exception

        End Try

        Dim hi, lo As String
        hi = lblhi.Value
        lo = lbllo.Value

        Dim hchk As Decimal
        Try
            hchk = System.Convert.ToDecimal(hi)
            If mchk > hchk Then
                lblmalert.Value = "hi"
            End If
            Exit Sub
        Catch ex As Exception
           
        End Try
        Dim lchk As Decimal
        Try
            lchk = System.Convert.ToDecimal(lo)
            If lchk < mchk Then
                lblmalert.Value = "lo"
            End If
            Exit Sub
        Catch ex As Exception

        End Try


    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3192.Text = axlabs.GetASPXPage("measoutadd.aspx", "lang3192")
        Catch ex As Exception
        End Try
        Try
            lang3193.Text = axlabs.GetASPXPage("measoutadd.aspx", "lang3193")
        Catch ex As Exception
        End Try
        Try
            lang3194.Text = axlabs.GetASPXPage("measoutadd.aspx", "lang3194")
        Catch ex As Exception
        End Try
        Try
            lang3195.Text = axlabs.GetASPXPage("measoutadd.aspx", "lang3195")
        Catch ex As Exception
        End Try
        Try
            lang3196.Text = axlabs.GetASPXPage("measoutadd.aspx", "lang3196")
        Catch ex As Exception
        End Try
        Try
            lang3197.Text = axlabs.GetASPXPage("measoutadd.aspx", "lang3197")
        Catch ex As Exception
        End Try
        Try
            lang3198.Text = axlabs.GetASPXPage("measoutadd.aspx", "lang3198")
        Catch ex As Exception
        End Try
        Try
            lang3199.Text = axlabs.GetASPXPage("measoutadd.aspx", "lang3199")
        Catch ex As Exception
        End Try
        Try
            lang3200.Text = axlabs.GetASPXPage("measoutadd.aspx", "lang3200")
        Catch ex As Exception
        End Try
        Try
            lang3201.Text = axlabs.GetASPXPage("measoutadd.aspx", "lang3201")
        Catch ex As Exception
        End Try
        Try
            lang3202.Text = axlabs.GetASPXPage("measoutadd.aspx", "lang3202")
        Catch ex As Exception
        End Try
        Try
            lang3203.Text = axlabs.GetASPXPage("measoutadd.aspx", "lang3203")
        Catch ex As Exception
        End Try
        Try
            lang3204.Text = axlabs.GetASPXPage("measoutadd.aspx", "lang3204")
        Catch ex As Exception
        End Try

    End Sub

End Class
