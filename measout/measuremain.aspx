<%@ Page Language="vb" AutoEventWireup="false" Codebehind="measuremain.aspx.vb" Inherits="lucy_r12.measuremain" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>measuremain</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="javascript" src="../scripts/smartscroll4.js"></script>
		<script language="JavaScript" src="../scripts/reportnav.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/measuremainaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="GetsScroll();checkpage();">
		<form id="form1" method="post" runat="server">
			<div style="POSITION: absolute; TOP: 8px; LEFT: 4px" id="spdiv"></div>
			<table style="POSITION: absolute; TOP: 0px; LEFT: 4px" cellSpacing="0" cellPadding="0"
				width="1080" id="scrollmenu">
				<tr>
					<td width="26"></td>
					<td width="218"></td>
					<td width="270"></td>
					<td width="5"></td>
					<td width="26"></td>
					<td width="174"></td>
					<td width="5"></td>
					<td width="26"></td>
					<td width="275"></td>
				</tr>
				<tr>
					<td colSpan="9">
						<table width="1025" cellpadding="0" cellspacing="0">
							<tr>
								<td width="230"><IMG src="../menu/mimages/fusionsm.gif"></td>
								<td vAlign="top" width="420"><IMG src="../images/appheaders/meas.gif"></td>
								<td vAlign="top" align="right" width="375"><IMG id="btnreturn" onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="9">
						<table width="1025">
							<tr>
								<td class="plainlabelred" id="tdeq" colSpan="2" height="30" runat="server"><asp:Label id="lang3261" runat="server">No Equipment Record Selected</asp:Label></td>
								<td class="plainlabelred" id="tdfu" colSpan="2" height="30" runat="server"><asp:Label id="lang3262" runat="server">No Function Record Selected</asp:Label></td>
								<td class="plainlabelred" id="tdco" colSpan="2" height="30" runat="server"><asp:Label id="lang3263" runat="server">No Component Record Selected</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="trhead" runat="server">
					<td class="thdrsinglft"><IMG src="../images/appbuttons/minibuttons/meashdr.gif"></td>
					<td class="thdrsingrt label" align="left" colSpan="5"><asp:Label id="lang3264" runat="server">Measurement Location Details</asp:Label></td>
					<td>&nbsp;</td>
					<td class="thdrsinglft"><IMG src="../images/appbuttons/minibuttons/meashdr.gif"></td>
					<td class="thdrsingrt label" align="left"><asp:Label id="lang3265" runat="server">Add Measurements</asp:Label></td>
				</tr>
				<tr id="trmain" runat="server">
					<td vAlign="top" colSpan="2">
						<table width="244">
							<tr>
								<td class="btmmenu plainlabel" colSpan="2"><asp:Label id="lang3266" runat="server">Search Equipment</asp:Label></td>
							</tr>
							<tr>
								<td class="label" width="84"><asp:Label id="lang3267" runat="server">Equipment#</asp:Label></td>
								<td><asp:textbox id="txteqnum" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang3268" runat="server">Description</asp:Label></td>
								<td><asp:textbox id="txteqdesc" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label">SPL</td>
								<td><asp:textbox id="txtspl" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label">OEM</td>
								<td><asp:textbox id="txtoem" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang3269" runat="server">Model</asp:Label></td>
								<td><asp:textbox id="txtmodel" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang3270" runat="server">Serial#</asp:Label></td>
								<td><asp:textbox id="txtserial" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang3271" runat="server">Asset Class</asp:Label></td>
								<td><asp:dropdownlist id="ddac" runat="server" CssClass="plainlabel"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td align="right" colSpan="2"><IMG onclick="srcheq();" src="../images/appbuttons/minibuttons/srchsm.gif">
									<IMG onclick="goback();" src="../images/appbuttons/minibuttons/refreshit.gif">
								</td>
							</tr>
						</table>
					</td>
					<td vAlign="top">
						<table width="270">
							<tr>
								<td class="btmmenu plainlabel"><asp:Label id="lang3272" runat="server">Equipment Records</asp:Label></td>
							</tr>
							<tr>
								<td>
									<div id="diveq" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 270px; HEIGHT: 168px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
										runat="server"></div>
								</td>
							</tr>
							<tr>
								<td align="center">
									<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
										cellSpacing="0" cellPadding="0">
										<tr>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="190"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
													runat="server"></td>
											<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
													runat="server"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td></td>
					<td vAlign="top" colSpan="2">
						<table width="200">
							<tr>
								<td class="btmmenu plainlabel"><asp:Label id="lang3273" runat="server">Equipment Functions</asp:Label></td>
							</tr>
							<tr>
								<td>
									<div id="divfu" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 200px; HEIGHT: 88px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
										runat="server"></div>
								</td>
							</tr>
							<tr>
								<td class="btmmenu plainlabel"><asp:Label id="lang3274" runat="server">Function Components</asp:Label></td>
							</tr>
							<tr>
								<td><div id="divco" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 200px; HEIGHT: 88px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
										runat="server"></div>
								</td>
							</tr>
						</table>
					</td>
					<td></td>
					<td vAlign="top" colSpan="2"><iframe id="ifmeas" style="BACKGROUND-COLOR: transparent" src="measureout.aspx?curr=none"
							frameBorder="no" width="312" scrolling="no" height="230" allowTransparency runat="server"></iframe>
					</td>
				</tr>
				<tr>
					<td colspan="9">
						<table cellspacing="0">
							<tr>
								<td id="tdcm1" class="thdrsingorangelft hand" width="26" onclick="getcm();"><IMG id="imgcm" src="../images/appbuttons/minibuttons/meashdro.gif"></td>
								<td id="tdcm2" class="thdrsingorangert label hand" align="left" width="149" onclick="getcm();"><asp:Label id="lang3275" runat="server">Custom Measurements</asp:Label></td>
								<td width="5">&nbsp;</td>
								<td id="tdtm1" class="thdrsinglft hand" width="26" onclick="gettm();"><IMG id="imgtm" src="../images/appbuttons/minibuttons/meashdr.gif"></td>
								<td id="tdtm2" class="thdrsingrt label hand" align="left" width="149" onclick="gettm();"><asp:Label id="lang3276" runat="server">PM Task Measurements</asp:Label></td>
								<td width="5">&nbsp;</td>
								<td id="tdrt1" class="thdrsinglft hand" width="26" onclick="getrt();"><IMG id="imgrt" src="../images/appbuttons/minibuttons/meashdr.gif"></td>
								<td id="tdrt2" class="thdrsingrt label hand" align="left" width="149" onclick="gettr();"><asp:Label id="lang3277" runat="server">Measurement Routes</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="9"><iframe id="ifsel" style="BACKGROUND-COLOR: transparent" src="measoutedit.aspx" frameBorder="no"
							width="1080" height="380" allowTransparency runat="server"></iframe>
					</td>
				</tr>
			</table>
			<input id="lbleqid" type="hidden" name="lbleqid" runat="server"> <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input id="lblcid" type="hidden" name="lblcid" runat="server"><input id="lblpgemstxt" type="hidden" name="lblpgemstxt" runat="server">
			<input id="lbleqnum" type="hidden" name="lbleqnum" runat="server"><input id="lblpgemshref" type="hidden" name="Hidden1" runat="server">
			<input id="lbltasklev" type="hidden" name="lbltasklev" runat="server"> <input id="lblsid" type="hidden" name="lblsid" runat="server">
			<input id="lbldid" type="hidden" name="lbldid" runat="server"> <input id="lblclid" type="hidden" name="lblclid" runat="server">
			<input id="lblchk" type="hidden" name="lblchk" runat="server"> <input id="lblmod" type="hidden" name="lblmod" runat="server">
			<input id="lblredir" type="hidden" name="lblredir" runat="server"><input id="lblsubmit" type="hidden" name="lblsubmit" runat="server">
			<input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server"><input id="txtpg" type="hidden" name="txtpg" runat="server">
			<input id="lbleqdesc" type="hidden" name="lbleqdesc" runat="server"> <input id="lblspl" type="hidden" name="lblspl" runat="server">
			<input id="lblmodel" type="hidden" name="lblmodel" runat="server"> <input id="lbloem" type="hidden" name="lbloem" runat="server">
			<input id="lblserial" type="hidden" name="lblserial" runat="server"> <input id="lblac" type="hidden" name="lblac" runat="server">
			<input id="lbleqnumsrch" type="hidden" name="lbleqnumsrch" runat="server"> <input id="lblrepcnt" type="hidden" name="lblrepcnt" runat="server">
			<input id="lblmeascnt" type="hidden" name="lblmeascnt" runat="server"> <input id="lblfunctions" type="hidden" runat="server">
			<input id="lblcomponents" type="hidden" runat="server"> <input id="lblcoid" type="hidden" runat="server">
			<input id="lblcharturl" type="hidden" runat="server"> <input type="hidden" id="lblcurrtab" runat="server">
			<input id="xCoord" type="hidden" name="xCoord" runat="server"> <input id="yCoord" type="hidden" name="yCoord" runat="server">
			<input type="hidden" id="lbllog" runat="server" NAME="lbllog"><input id="spdivy" type="hidden" name="spdivy" runat="server">
			<input type="hidden" id="lblcursrch" runat="server"> <input type="hidden" id="lblfunc" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
