<%@ Page Language="vb" AutoEventWireup="false" Codebehind="measoutroutes.aspx.vb" Inherits="lucy_r12.measoutroutes" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>measoutroutes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts/reportnav.js"></script>
		<script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script language="JavaScript" src="../scripts1/measoutroutesaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="scrolltop();checkit();" >
		<form id="form1" method="post" runat="server">
			<table id="scrollmenu">
				<tr>
					<td>
						<table>
							<tr>
								<td class="bluelabel" width="80"><asp:Label id="lang3234" runat="server">New Route</asp:Label></td>
								<td width="120"><asp:textbox id="txtnewroute" runat="server"></asp:textbox></td>
								<td width="40"><IMG onclick="addroute();" src="../images/appbuttons/minibuttons/addmod.gif"></td>
								<td class="bluelabel" width="130"><asp:Label id="lang3235" runat="server">Search Routes (Name)</asp:Label></td>
								<td width="120"><asp:textbox id="txtsrch" runat="server"></asp:textbox></td>
								<td width="40"><IMG onclick="srchroute();" src="../images/appbuttons/minibuttons/srchsm.gif"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:datagrid id="dgmeas" runat="server" ShowFooter="True" CellPadding="0" GridLines="None" AllowPaging="True"
							AllowCustomPaging="True" AutoGenerateColumns="False" CellSpacing="1" BackColor="transparent">
							<FooterStyle BackColor="transparent"></FooterStyle>
							<EditItemStyle Height="15px" BackColor="transparent"></EditItemStyle>
							<AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
							<ItemStyle CssClass="ptransrow"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel" Height="20px"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Route Name">
									<HeaderStyle Width="220px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label1" runat="server" Width="220px" Text='<%# DataBinder.Eval(Container, "DataItem.route") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="txtroute" runat="server" Width="220px" Text='<%# DataBinder.Eval(Container, "DataItem.route") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Frequency">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Width="90px" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txtfreq" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Last Date">
									<HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label4" runat="server" Width="140px" Text='<%# DataBinder.Eval(Container, "DataItem.lastdate") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label5" runat="server" Width="140px" Text='<%# DataBinder.Eval(Container, "DataItem.lastdate") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Next Date">
									<HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label6" runat="server" Width="140px" Text='<%# DataBinder.Eval(Container, "DataItem.nextdate") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txtnext" runat="server" Width="140px" Text='<%# DataBinder.Eval(Container, "DataItem.nextdate") %>'>
										</asp:textbox><IMG id="imgnext" runat="server" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Work Order#" Visible="False">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label8" runat="server" Width="90px" Text='<%# DataBinder.Eval(Container, "DataItem.wonum") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblwonum" runat="server" Width="90px" Text='<%# DataBinder.Eval(Container, "DataItem.wonum") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<ItemTemplate>
										<asp:Label id="lblmeasidi" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.routeid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblmeaside" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.routeid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<ItemTemplate>
										<asp:Label id="Label3" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.rhid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblrhid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.rhid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Tasks">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<IMG id="img" runat="server" onmouseover="return overlib('Print Route Report', ABOVE, LEFT)"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/printex.gif">
										<IMG id="imgei" runat="server" onmouseover="return overlib('Enter Route Measurement Values', ABOVE, LEFT)"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/pencil.gif"> <IMG id="Img1" runat="server" onmouseover="return overlib('Add or Edit Measurements for this Route', ABOVE, LEFT)"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/addmod.gif">
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Remove" ItemStyle-HorizontalAlign="Center">
									<HeaderStyle Width="64px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="imgdel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
								ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td align="center">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lbleqid" type="hidden" runat="server"> <input id="lblfuid" type="hidden" runat="server">
			<input id="lblcoid" type="hidden" runat="server"> <input id="lblsubmit" type="hidden" runat="server">
			<input id="lblsid" type="hidden" runat="server"><input id="xCoord" type="hidden" name="xCoord" runat="server">
			<input id="yCoord" type="hidden" name="yCoord" runat="server"> <input type="hidden" id="txtpg" runat="server" NAME="Hidden1"><input type="hidden" id="txtpgcnt" runat="server" NAME="txtpgcnt">
			<input type="hidden" id="lblret" runat="server"> <input type="hidden" id="lbllog" runat="server" NAME="lbllog">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
