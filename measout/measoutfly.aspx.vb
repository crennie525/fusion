

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class measoutfly
    Inherits System.Web.UI.Page
	Protected WithEvents ovid284 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid283 As System.Web.UI.HtmlControls.HtmlImage

	

	

	Protected WithEvents imgei As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents imgee As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents imga As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents img As System.Web.UI.HtmlControls.HtmlImage

	

	

	Protected WithEvents lang3230 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3229 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3228 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3227 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3226 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3225 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3224 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3223 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3222 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3221 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim meas As New Utilities
    Dim pmtskid, pmid, cid, pmtid, ro, eqid, fuid, eqnum, coid, Login As String
    Protected WithEvents imgmag As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchngfunc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfunc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdfunc As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddfunc As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dgmeas As System.Web.UI.WebControls.DataGrid
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtsup As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlead As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddwt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtstart As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtprob As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcorr As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnwo As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Label24 As System.Web.UI.WebControls.Label
    Protected WithEvents txttsk As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdnofail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbsupe As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbleade As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldragid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrowid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcharturl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnum As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()



	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Dim charturl As String = System.Configuration.ConfigurationManager.AppSettings("chartURL")
            lblcharturl.Value = charturl
            cid = "0"

            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                dgmeas.Columns(0).Visible = False
                btnwo.ImageUrl = "../images/appbuttons/bgbuttons/submitdis.gif"
                btnwo.Enabled = False
            End If
            Try
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
            Catch ex As Exception

            End Try
            Try
                fuid = Request.QueryString("fuid").ToString
                If fuid = "" Then
                    fuid = "0"
                End If
                lblfuid.Value = fuid
                imgmag.Attributes.Add("class", "view")
                tdfunc.Attributes.Add("class", "details")
                ddfunc.Attributes.Add("class", "details")
            Catch ex As Exception
                fuid = "0"
                lblfuid.Value = fuid
            End Try
            Try
                coid = Request.QueryString("coid").ToString
                If coid = "" Then
                    coid = "0"
                End If
                lblcoid.Value = coid
                imgmag.Attributes.Add("class", "view")
                tdfunc.Attributes.Add("class", "details")
                ddfunc.Attributes.Add("class", "details")
            Catch ex As Exception
                coid = "0"
                lblcoid.Value = coid
            End Try


            meas.Open()
            GetLists(eqid)
            GetMeas(eqid, fuid, coid)
            meas.Dispose()

        End If
    End Sub
    Private Sub GetLists(ByVal eqid As String)
        cid = lblcid.Value
        sql = "select skillid, skill " _
        + "from pmSkills where compid = '" & cid & "'"
        dr = meas.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("Select"))
        ddskill.Items(0).Value = 0

        Dim mcnt As Integer
        sql = "select count(*) from pmTaskMeasDetMan m " _
        + "left join pm p on p.pmid = m.pmid where p.eqid = '" & eqid & "'"
        mcnt = meas.Scalar(sql)

        If mcnt > 0 Then
            sql = "select distinct f.func_id, f.func from pmTaskMeasDetMan m " _
            + "left join pm p on p.pmid = m.pmid " _
            + "left join pmtasks op on op.pmtskid = m.pmtskid  " _
            + "left join functions f on f.func_id = op.funcid  " _
            + "where p.eqid = '" & eqid & "' order by f.func_id, f.func"

            dr = meas.GetRdrData(sql)
            ddfunc.DataSource = dr
            ddfunc.DataTextField = "func"
            ddfunc.DataValueField = "func_id"
            ddfunc.DataBind()
            dr.Close()
            ddfunc.Items.Insert(0, New ListItem("Select"))
            ddfunc.Items(0).Value = 0

            'Dim fuid As String
            ' = "select top 1 f.func_id from pmTaskMeasDetMan m " _
            '+ "left join pm p on p.pmid = m.pmid " _
            '+ "left join pmtasks op on op.pmtskid = m.pmtskid  " _
            '+ "left join functions f on f.func_id = op.funcid  " _
            '+ "where p.eqid = '" & eqid & "' order by op.funcid"
            'fuid = meas.Scalar(sql)
            'ddfunc.SelectedValue = fuid
            'lblfuid.Value = fuid
            '(eqid, fuid)
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr1569" , "measoutfly.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
    End Sub

    Private Sub GetMeas(ByVal eqid As String, Optional ByVal fuid As String = "0", Optional ByVal coid As String = "0")
        Dim mcnt As Integer
        If fuid = "0" And coid = "0" Then
            sql = "select count(*) from pmTaskMeasDetMan m " _
                    + "left join pm p on p.pmid = m.pmid " _
                    + "left join pmtasks op on op.pmtskid = m.pmtskid  " _
                    + "left join functions f on f.func_id = op.funcid  " _
                    + "where p.eqid = '" & eqid & "'"
            mcnt = meas.Scalar(sql)
            lblmcnt.Value = mcnt

            sql = "select m.tmdid, m.tmdidpar, m.pmtskid, p.eqid, op.funcid, f.func, op.comid, op.compnum, " _
            + "m.pmid, m.type, m.hi, m.mover, m.lo, m.munder, m.spec, m.record, " _
            + "m.measurement2, m.malert, t.tasknum, t.taskdesc, " _
            + "cnt = (select count(*) from pmTaskMeasDetManHist m1 left join pm p on p.pmid = m1.pmid " _
            + "left join pmtasks op on op.pmtskid = m1.pmtskid " _
            + "left join functions f on f.func_id = op.funcid where p.eqid = '" & eqid & "' and op.pmtskid = m.pmtskid)  " _
            + "from pmTaskMeasDetMan m  " _
            + "left join pmtrack t on t.pmtskid = m.pmtskid  " _
            + "left join pm p on p.pmid = m.pmid  " _
            + "left join pmtasks op on op.pmtskid = m.pmtskid  " _
            + "left join functions f on f.func_id = op.funcid  " _
            + "where p.eqid = '" & eqid & "' " _
            + "order by op.funcid, t.tasknum  "
        ElseIf fuid <> "0" And coid = "0" Then
            sql = "select count(*) from pmTaskMeasDetMan m " _
                    + "left join pm p on p.pmid = m.pmid " _
                    + "left join pmtasks op on op.pmtskid = m.pmtskid  " _
                    + "left join functions f on f.func_id = op.funcid  " _
                    + "where p.eqid = '" & eqid & "' and op.funcid = '" & fuid & "'"
            mcnt = meas.Scalar(sql)
            lblmcnt.Value = mcnt

            sql = "select m.tmdid, m.tmdidpar, m.pmtskid, p.eqid, op.funcid, f.func, op.comid, op.compnum, " _
            + "m.pmid, m.type, m.hi, m.mover, m.lo, m.munder, m.spec, m.record, " _
            + "m.measurement2, m.malert, t.tasknum, t.taskdesc, " _
            + "cnt = (select count(*) from pmTaskMeasDetManHist m1 left join pm p on p.pmid = m1.pmid " _
            + "left join pmtasks op on op.pmtskid = m1.pmtskid " _
            + "left join functions f on f.func_id = op.funcid where p.eqid = '" & eqid & "' and op.funcid = '" & fuid & "' and op.pmtskid = m.pmtskid )  " _
            + "from pmTaskMeasDetMan m  " _
            + "left join pmtrack t on t.pmtskid = m.pmtskid  " _
            + "left join pm p on p.pmid = m.pmid  " _
            + "left join pmtasks op on op.pmtskid = m.pmtskid  " _
            + "left join functions f on f.func_id = op.funcid  " _
            + "where p.eqid = '" & eqid & "' and op.funcid = '" & fuid & "' " _
            + "order by op.funcid, t.tasknum  "
        Else
            sql = "select count(*) from pmTaskMeasDetMan m " _
                    + "left join pm p on p.pmid = m.pmid " _
                    + "left join pmtasks op on op.pmtskid = m.pmtskid  " _
                    + "left join functions f on f.func_id = op.funcid  " _
                    + "where p.eqid = '" & eqid & "' and op.funcid = '" & fuid & "' and op.comid = '" & coid & "'"
            mcnt = meas.Scalar(sql)
            lblmcnt.Value = mcnt

            sql = "select m.tmdid, m.tmdidpar, m.pmtskid, p.eqid, op.funcid, f.func, op.comid, op.compnum, " _
            + "m.pmid, m.type, m.hi, m.mover, m.lo, m.munder, m.spec, m.record, " _
            + "m.measurement2, m.malert, t.tasknum, t.taskdesc, " _
            + "cnt = (select count(*) from pmTaskMeasDetManHist m1 left join pm p on p.pmid = m1.pmid " _
            + "left join pmtasks op on op.pmtskid = m1.pmtskid " _
            + "left join functions f on f.func_id = op.funcid where p.eqid = '" & eqid & "' and op.funcid = '" & fuid & "' and op.comid = '" & coid & "' and op.pmtskid = m.pmtskid)  " _
            + "from pmTaskMeasDetMan m  " _
            + "left join pmtrack t on t.pmtskid = m.pmtskid  " _
            + "left join pm p on p.pmid = m.pmid  " _
            + "left join pmtasks op on op.pmtskid = m.pmtskid  " _
            + "left join functions f on f.func_id = op.funcid  " _
            + "where p.eqid = '" & eqid & "' and op.funcid = '" & fuid & "' and op.comid = '" & coid & "' " _
            + "order by op.funcid, t.tasknum  "
        End If


        dr = meas.GetRdrData(sql)
        dgmeas.DataSource = dr
        dgmeas.DataBind()
        dr.Close()

    End Sub

    Private Sub dgmeas_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgmeas.ItemDataBound
        Dim cnt, pmid As String ', neg
        Dim icnt, ineg As Integer
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            cnt = DataBinder.Eval(e.Item.DataItem, "cnt").ToString
            Try
                icnt = Convert.ToInt32(cnt)
            Catch ex As Exception
                icnt = 0
            End Try



            Dim img As HtmlImage = CType(e.Item.FindControl("ihg"), HtmlImage)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "pmtskid").ToString
            Dim img1 As HtmlImage = CType(e.Item.FindControl("img"), HtmlImage)
            Dim id1 As String = DataBinder.Eval(e.Item.DataItem, "tmdidpar").ToString
            Dim hii As String = DataBinder.Eval(e.Item.DataItem, "hi").ToString
            Dim speci As String = DataBinder.Eval(e.Item.DataItem, "spec").ToString
            Dim ide As String = DataBinder.Eval(e.Item.DataItem, "tmdid").ToString

            Dim imgedit As HtmlImage = CType(e.Item.FindControl("imgedit"), HtmlImage)
            imgedit.Attributes.Add("onclick", "getedit('" & ide & "','" & id & "')")

            Dim img4 As HtmlImage = CType(e.Item.FindControl("img1"), HtmlImage)
            img4.Attributes.Add("onclick", "getadd('" & ide & "','" & id & "');")

            If icnt > 2 And ineg > -1 Then
                img.Attributes("onclick") = "getchart('" & id & "','" & id1 & "','" & hii & "','" & speci & "');"
            Else
                img.Attributes("class") = "details"
            End If
            If icnt > 0 Then
                img1.Attributes("onclick") = "print('" & id & "','" & id1 & "');"
            Else
                img1.Attributes("class") = "details"
            End If
            Dim hi As String = DataBinder.Eval(e.Item.DataItem, "mover").ToString
            Dim lo As String = DataBinder.Eval(e.Item.DataItem, "munder").ToString
            Dim img2 As HtmlImage = CType(e.Item.FindControl("iwo"), HtmlImage)
            If (hi <> "0" And hi <> "" And hi <> "0.00") Or (lo <> "0" And lo <> "" And lo <> "0.00") Then
                img2.Attributes("onclick") = "getrt('" & id & "','" & id1 & "');"
            Else
                img2.Attributes("class") = "details"
            End If
            Dim img3 As HtmlImage = CType(e.Item.FindControl("imgmi"), HtmlImage)
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "taskdesc").ToString
            img3.Attributes.Add("onclick", "gettsk('" & tsk & "');")
            Dim imgei As HtmlImage = CType(e.Item.FindControl("imgei"), HtmlImage)
            Dim id2 As String = DataBinder.Eval(e.Item.DataItem, "tmdid").ToString
            imgei.Attributes.Add("onclick", "gettskedit('" & eqid & "', '" & id2 & "');")
        ElseIf e.Item.ItemType = ListItemType.EditItem Then
            cnt = DataBinder.Eval(e.Item.DataItem, "cnt").ToString
            Try
                icnt = Convert.ToInt32(cnt)
            Catch ex As Exception
                icnt = 0
            End Try

            Dim img As HtmlImage = CType(e.Item.FindControl("ihga"), HtmlImage)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "pmtskid").ToString
            Dim img1 As HtmlImage = CType(e.Item.FindControl("imga"), HtmlImage)
            Dim id1 As String = DataBinder.Eval(e.Item.DataItem, "tmdid").ToString
            Dim hii As String = DataBinder.Eval(e.Item.DataItem, "hi").ToString
            If icnt > 2 Then
                img.Attributes("onclick") = "getchart('" & id & "','" & id1 & "','" & hii & "');"
            Else
                img.Attributes("class") = "details"
            End If
            If icnt > 0 Then
                img1.Attributes("onclick") = "print('" & id & "','" & id1 & "');"
            Else
                img1.Attributes("class") = "details"
            End If
            Dim hi As String = DataBinder.Eval(e.Item.DataItem, "mover").ToString
            Dim lo As String = DataBinder.Eval(e.Item.DataItem, "munder").ToString
            Dim img2 As HtmlImage = CType(e.Item.FindControl("iwoa"), HtmlImage)
            If (hi <> "0" And hi <> "" And hi <> "0.00") Or (lo <> "0" And lo <> "" And lo <> "0.00") Then
                img2.Attributes("onclick") = "getrt('" & id & "','" & id1 & "');"
            Else
                img2.Attributes("class") = "details"
            End If
            Dim img3 As HtmlImage = CType(e.Item.FindControl("imgme"), HtmlImage)
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "taskdesc").ToString
            img3.Attributes.Add("onclick", "gettsk('" & tsk & "');")
            Dim imgee As HtmlImage = CType(e.Item.FindControl("imgee"), HtmlImage)
            Dim id2 As String = DataBinder.Eval(e.Item.DataItem, "tmdid").ToString
            imgee.Attributes.Add("onclick", "gettskedit('" & eqid & "', '" & id2 & "');")
        End If
    End Sub

    Private Sub ddfunc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddfunc.SelectedIndexChanged
        fuid = ddfunc.SelectedValue
        lblfuid.Value = fuid
        lblfunc.Value = ddfunc.SelectedItem.ToString
        eqid = lbleqid.Value
        meas.Open()
        GetMeas(eqid, fuid)
        meas.Dispose()
        lblchngfunc.Value = "yes"

    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgmeas.Columns(1).HeaderText = dlabs.GetDGPage("measoutfly.aspx", "dgmeas", "1")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(3).HeaderText = dlabs.GetDGPage("measoutfly.aspx", "dgmeas", "3")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(4).HeaderText = dlabs.GetDGPage("measoutfly.aspx", "dgmeas", "4")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(5).HeaderText = dlabs.GetDGPage("measoutfly.aspx", "dgmeas", "5")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(6).HeaderText = dlabs.GetDGPage("measoutfly.aspx", "dgmeas", "6")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(7).HeaderText = dlabs.GetDGPage("measoutfly.aspx", "dgmeas", "7")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(8).HeaderText = dlabs.GetDGPage("measoutfly.aspx", "dgmeas", "8")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(9).HeaderText = dlabs.GetDGPage("measoutfly.aspx", "dgmeas", "9")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label24.Text = axlabs.GetASPXPage("measoutfly.aspx", "Label24")
        Catch ex As Exception
        End Try
        Try
            lang3221.Text = axlabs.GetASPXPage("measoutfly.aspx", "lang3221")
        Catch ex As Exception
        End Try
        Try
            lang3222.Text = axlabs.GetASPXPage("measoutfly.aspx", "lang3222")
        Catch ex As Exception
        End Try
        Try
            lang3223.Text = axlabs.GetASPXPage("measoutfly.aspx", "lang3223")
        Catch ex As Exception
        End Try
        Try
            lang3224.Text = axlabs.GetASPXPage("measoutfly.aspx", "lang3224")
        Catch ex As Exception
        End Try
        Try
            lang3225.Text = axlabs.GetASPXPage("measoutfly.aspx", "lang3225")
        Catch ex As Exception
        End Try
        Try
            lang3226.Text = axlabs.GetASPXPage("measoutfly.aspx", "lang3226")
        Catch ex As Exception
        End Try
        Try
            lang3227.Text = axlabs.GetASPXPage("measoutfly.aspx", "lang3227")
        Catch ex As Exception
        End Try
        Try
            lang3228.Text = axlabs.GetASPXPage("measoutfly.aspx", "lang3228")
        Catch ex As Exception
        End Try
        Try
            lang3229.Text = axlabs.GetASPXPage("measoutfly.aspx", "lang3229")
        Catch ex As Exception
        End Try
        Try
            lang3230.Text = axlabs.GetASPXPage("measoutfly.aspx", "lang3230")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnwo.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                btnwo.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                btnwo.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                btnwo.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "spa" Then
                btnwo.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib

        Try
            img.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("measoutfly.aspx", "img") & "', ABOVE, LEFT)")
            img.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("measoutfly.aspx", "Img1") & "', ABOVE, LEFT)")
            Img1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imga.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("measoutfly.aspx", "imga") & "', ABOVE, LEFT)")
            imga.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgee.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("measoutfly.aspx", "imgee") & "', ABOVE, LEFT)")
            imgee.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgei.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("measoutfly.aspx", "imgei") & "', ABOVE, LEFT)")
            imgei.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgmag.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("measoutfly.aspx", "imgmag") & "', ABOVE, LEFT)")
            imgmag.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

        Try
            ovid283.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("measoutfly.aspx", "ovid283") & "')")
            ovid283.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid284.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("measoutfly.aspx", "ovid284") & "')")
            ovid284.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
