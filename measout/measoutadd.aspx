<%@ Page Language="vb" AutoEventWireup="false" Codebehind="measoutadd.aspx.vb" Inherits="lucy_r12.measoutadd" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>measoutadd</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/measoutaddaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checkmalert();">
		<form id="form1" method="post" runat="server">
			<table width="450">
				<tr>
					<td class="label" width="140" height="20"><asp:Label id="lang3192" runat="server">Current Equipment:</asp:Label></td>
					<td class="plainlabel" id="tdeq" width="310" runat="server"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3193" runat="server">Current Function:</asp:Label></td>
					<td class="plainlabel" id="tdfu" runat="server"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3194" runat="server">Current Component:</asp:Label></td>
					<td class="plainlabel" id="tdco" runat="server"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3195" runat="server">Current Measurement:</asp:Label></td>
					<td class="plainlabel" id="tdout" runat="server"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3196" runat="server">Measure Type:</asp:Label></td>
					<td class="plainlabel" id="tdtype" runat="server"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3197" runat="server">Measurement:</asp:Label></td>
					<td class="plainlabel" id="tdmeas" runat="server"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3198" runat="server">Characteristic:</asp:Label></td>
					<td class="plainlabel" id="tdchar" runat="server"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3199" runat="server">Hi Value:</asp:Label></td>
					<td class="plainlabel" id="tdhi" runat="server"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3200" runat="server">Lo Value:</asp:Label></td>
					<td class="plainlabel" id="tdlo" runat="server"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3201" runat="server">Specific Value:</asp:Label></td>
					<td class="plainlabel" id="tdspec" runat="server"></td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<table>
							<tr>
								<td class="bluelabel"><asp:Label id="lang3202" runat="server">Measure Date</asp:Label></td>
								<td>
									<asp:TextBox id="txtdate" runat="server"></asp:TextBox></td>
								<td><IMG onclick="getcal('txtdate');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
										width="19"></td>
								<td>
									<asp:CheckBox id="cbdate" runat="server" CssClass="plainlabel" Checked="True" Text="Use Current"></asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td class="bluelabel"><asp:Label id="lang3203" runat="server">Measure Time</asp:Label></td>
								<td>
									<table>
										<tr>
											<td><asp:dropdownlist id="ddhrspm" runat="server" CssClass="plainlabel">
													<asp:ListItem Value="NA">NA</asp:ListItem>
													<asp:ListItem Value="01">01</asp:ListItem>
													<asp:ListItem Value="02">02</asp:ListItem>
													<asp:ListItem Value="03">03</asp:ListItem>
													<asp:ListItem Value="04">04</asp:ListItem>
													<asp:ListItem Value="05">05</asp:ListItem>
													<asp:ListItem Value="06">06</asp:ListItem>
													<asp:ListItem Value="07">07</asp:ListItem>
													<asp:ListItem Value="08">08</asp:ListItem>
													<asp:ListItem Value="09">09</asp:ListItem>
													<asp:ListItem Value="10">10</asp:ListItem>
													<asp:ListItem Value="11">11</asp:ListItem>
													<asp:ListItem Value="12">12</asp:ListItem>
												</asp:dropdownlist></td>
											<td class="bluelabel">:
											</td>
											<td><asp:dropdownlist id="ddminspm" runat="server" CssClass="plainlabel">
													<asp:ListItem Value="00">00</asp:ListItem>
													<asp:ListItem Value="01">01</asp:ListItem>
													<asp:ListItem Value="02">02</asp:ListItem>
													<asp:ListItem Value="03">03</asp:ListItem>
													<asp:ListItem Value="04">04</asp:ListItem>
													<asp:ListItem Value="05">05</asp:ListItem>
													<asp:ListItem Value="06">06</asp:ListItem>
													<asp:ListItem Value="07">07</asp:ListItem>
													<asp:ListItem Value="08">08</asp:ListItem>
													<asp:ListItem Value="09">09</asp:ListItem>
													<asp:ListItem Value="10">10</asp:ListItem>
													<asp:ListItem Value="11">11</asp:ListItem>
													<asp:ListItem Value="12">12</asp:ListItem>
													<asp:ListItem Value="13">13</asp:ListItem>
													<asp:ListItem Value="14">14</asp:ListItem>
													<asp:ListItem Value="15">15</asp:ListItem>
													<asp:ListItem Value="16">16</asp:ListItem>
													<asp:ListItem Value="17">17</asp:ListItem>
													<asp:ListItem Value="18">18</asp:ListItem>
													<asp:ListItem Value="19">19</asp:ListItem>
													<asp:ListItem Value="20">20</asp:ListItem>
													<asp:ListItem Value="21">21</asp:ListItem>
													<asp:ListItem Value="22">22</asp:ListItem>
													<asp:ListItem Value="23">23</asp:ListItem>
													<asp:ListItem Value="24">24</asp:ListItem>
													<asp:ListItem Value="25">25</asp:ListItem>
													<asp:ListItem Value="26">26</asp:ListItem>
													<asp:ListItem Value="27">27</asp:ListItem>
													<asp:ListItem Value="28">28</asp:ListItem>
													<asp:ListItem Value="29">29</asp:ListItem>
													<asp:ListItem Value="30">30</asp:ListItem>
													<asp:ListItem Value="31">31</asp:ListItem>
													<asp:ListItem Value="32">32</asp:ListItem>
													<asp:ListItem Value="33">33</asp:ListItem>
													<asp:ListItem Value="34">34</asp:ListItem>
													<asp:ListItem Value="35">35</asp:ListItem>
													<asp:ListItem Value="36">36</asp:ListItem>
													<asp:ListItem Value="37">37</asp:ListItem>
													<asp:ListItem Value="38">38</asp:ListItem>
													<asp:ListItem Value="39">39</asp:ListItem>
													<asp:ListItem Value="40">40</asp:ListItem>
													<asp:ListItem Value="41">41</asp:ListItem>
													<asp:ListItem Value="42">42</asp:ListItem>
													<asp:ListItem Value="43">43</asp:ListItem>
													<asp:ListItem Value="44">44</asp:ListItem>
													<asp:ListItem Value="45">45</asp:ListItem>
													<asp:ListItem Value="46">46</asp:ListItem>
													<asp:ListItem Value="47">47</asp:ListItem>
													<asp:ListItem Value="48">48</asp:ListItem>
													<asp:ListItem Value="49">49</asp:ListItem>
													<asp:ListItem Value="50">50</asp:ListItem>
													<asp:ListItem Value="51">51</asp:ListItem>
													<asp:ListItem Value="52">52</asp:ListItem>
													<asp:ListItem Value="53">53</asp:ListItem>
													<asp:ListItem Value="54">54</asp:ListItem>
													<asp:ListItem Value="55">55</asp:ListItem>
													<asp:ListItem Value="56">56</asp:ListItem>
													<asp:ListItem Value="57">57</asp:ListItem>
													<asp:ListItem Value="58">58</asp:ListItem>
													<asp:ListItem Value="59">59</asp:ListItem>
												</asp:dropdownlist></td>
											<td class="bluelabel">:
											</td>
											<td><asp:dropdownlist id="ddsecspm" runat="server" CssClass="plainlabel">
													<asp:ListItem Value="00">00</asp:ListItem>
													<asp:ListItem Value="01">01</asp:ListItem>
													<asp:ListItem Value="02">02</asp:ListItem>
													<asp:ListItem Value="03">03</asp:ListItem>
													<asp:ListItem Value="04">04</asp:ListItem>
													<asp:ListItem Value="05">05</asp:ListItem>
													<asp:ListItem Value="06">06</asp:ListItem>
													<asp:ListItem Value="07">07</asp:ListItem>
													<asp:ListItem Value="08">08</asp:ListItem>
													<asp:ListItem Value="09">09</asp:ListItem>
													<asp:ListItem Value="10">10</asp:ListItem>
													<asp:ListItem Value="11">11</asp:ListItem>
													<asp:ListItem Value="12">12</asp:ListItem>
													<asp:ListItem Value="13">13</asp:ListItem>
													<asp:ListItem Value="14">14</asp:ListItem>
													<asp:ListItem Value="15">15</asp:ListItem>
													<asp:ListItem Value="16">16</asp:ListItem>
													<asp:ListItem Value="17">17</asp:ListItem>
													<asp:ListItem Value="18">18</asp:ListItem>
													<asp:ListItem Value="19">19</asp:ListItem>
													<asp:ListItem Value="20">20</asp:ListItem>
													<asp:ListItem Value="21">21</asp:ListItem>
													<asp:ListItem Value="22">22</asp:ListItem>
													<asp:ListItem Value="23">23</asp:ListItem>
													<asp:ListItem Value="24">24</asp:ListItem>
													<asp:ListItem Value="25">25</asp:ListItem>
													<asp:ListItem Value="26">26</asp:ListItem>
													<asp:ListItem Value="27">27</asp:ListItem>
													<asp:ListItem Value="28">28</asp:ListItem>
													<asp:ListItem Value="29">29</asp:ListItem>
													<asp:ListItem Value="30">30</asp:ListItem>
													<asp:ListItem Value="31">31</asp:ListItem>
													<asp:ListItem Value="32">32</asp:ListItem>
													<asp:ListItem Value="33">33</asp:ListItem>
													<asp:ListItem Value="34">34</asp:ListItem>
													<asp:ListItem Value="35">35</asp:ListItem>
													<asp:ListItem Value="36">36</asp:ListItem>
													<asp:ListItem Value="37">37</asp:ListItem>
													<asp:ListItem Value="38">38</asp:ListItem>
													<asp:ListItem Value="39">39</asp:ListItem>
													<asp:ListItem Value="40">40</asp:ListItem>
													<asp:ListItem Value="41">41</asp:ListItem>
													<asp:ListItem Value="42">42</asp:ListItem>
													<asp:ListItem Value="43">43</asp:ListItem>
													<asp:ListItem Value="44">44</asp:ListItem>
													<asp:ListItem Value="45">45</asp:ListItem>
													<asp:ListItem Value="46">46</asp:ListItem>
													<asp:ListItem Value="47">47</asp:ListItem>
													<asp:ListItem Value="48">48</asp:ListItem>
													<asp:ListItem Value="49">49</asp:ListItem>
													<asp:ListItem Value="50">50</asp:ListItem>
													<asp:ListItem Value="51">51</asp:ListItem>
													<asp:ListItem Value="52">52</asp:ListItem>
													<asp:ListItem Value="53">53</asp:ListItem>
													<asp:ListItem Value="54">54</asp:ListItem>
													<asp:ListItem Value="55">55</asp:ListItem>
													<asp:ListItem Value="56">56</asp:ListItem>
													<asp:ListItem Value="57">57</asp:ListItem>
													<asp:ListItem Value="58">58</asp:ListItem>
													<asp:ListItem Value="59">59</asp:ListItem>
												</asp:dropdownlist></td>
											<td><asp:dropdownlist id="ddapspm" runat="server" CssClass="plainlabel">
													<asp:ListItem Value="AM">AM</asp:ListItem>
													<asp:ListItem Value="PM">PM</asp:ListItem>
												</asp:dropdownlist></td>
										</tr>
									</table>
								</td>
								<td></td>
								<td>
									<asp:CheckBox id="cbtime" runat="server" CssClass="plainlabel" Checked="True" Text="Use Current"></asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td class="bluelabel"><asp:Label id="lang3204" runat="server">Measured Value</asp:Label></td>
								<td>
									<asp:TextBox id="txtnewmeas" runat="server"></asp:TextBox></td>
								<td></td>
								<td align="right"><IMG id="btnadd" onclick="addToTask();" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif"
										runat="server">&nbsp;<IMG id="btnreturn" onclick="handleexit();" alt="" src="../images/appbuttons/minibuttons/candisk1.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lblmeasid" runat="server"> <input type="hidden" id="lblsubmit" runat="server">
			<input type="hidden" id="lblhi" runat="server"> <input type="hidden" id="lbllo" runat="server">
			<input type="hidden" id="lblmalert" runat="server"> <input type="hidden" id="lbllog" runat="server" NAME="lbllog">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
