

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class measoutwo
    Inherits System.Web.UI.Page
	Protected WithEvents ovid286 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid285 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang3258 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3257 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3256 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3255 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3254 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3253 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3252 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim pmf As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Protected WithEvents lblmeasid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim cid, eqid, comid, fuid, eqnum, sid, deptid, cellid, locid, funcid, measid, pmtskid, tmdid, Login As String
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldeptid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcellid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltmdid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtsup As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlead As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddwt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtstart As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtprob As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcorr As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnwo As System.Web.UI.WebControls.ImageButton
    Protected WithEvents cbsupe As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbleade As System.Web.UI.HtmlControls.HtmlInputCheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()



	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                measid = Request.QueryString("measid").ToString
                lblmeasid.Value = measid
                pmf.Open()
                GetDetails(measid)
                pmf.Dispose()
            Catch ex As Exception
                pmtskid = Request.QueryString("pmtskid").ToString '"2" ' 
                lblpmtskid.Value = pmtskid
                tmdid = Request.QueryString("tmdid").ToString '"2" ' 
                lbltmdid.Value = tmdid
                pmf.Open()
                GetDetails2(pmtskid, tmdid)
                pmf.Dispose()
            End Try


        End If
    End Sub
    Private Sub GetDetails2(ByVal pmtskid As String, ByVal tmdid As String)
        sql = "select m.tmdid, m.tmdidpar, m.pmtskid, p.eqid, op.funcid, f.func, op.comid, op.compnum, " _
       + "m.pmid, m.type, m.hi, m.mover, m.lo, m.munder, m.spec, m.record, " _
       + "m.measurement2, m.malert, t.tasknum, t.taskdesc, " _
       + "e.eqid, e.eqnum, e.deptid, e.cellid, e.siteid " _
       + "from pmTaskMeasDetMan m  " _
       + "left join pmtrack t on t.pmtskid = m.pmtskid  " _
       + "left join pm p on p.pmid = m.pmid  " _
       + "left join pmtasks op on op.pmtskid = m.pmtskid  " _
       + "left join equipment e on e.eqid = op.eqid  " _
       + "left join functions f on f.func_id = op.funcid  " _
       + "where m.tmdid = '" & tmdid & "' and m.pmtskid = '" & pmtskid & "' "
        dr = pmf.GetRdrData(sql)
        While dr.Read
            lblcoid.Value = dr.Item("comid").ToString
            lblfuid.Value = dr.Item("funcid").ToString
            lbleqid.Value = dr.Item("eqid").ToString
            lbleqnum.Value = dr.Item("eqnum").ToString
            lblsid.Value = dr.Item("siteid").ToString
            lblcellid.Value = dr.Item("cellid").ToString
            lbldeptid.Value = dr.Item("deptid").ToString
        End While
        dr.Close()
    End Sub
    Private Sub GetDetails(ByVal measid As String)

        sql = "select o.*, e.eqnum from outmeasure o left join equipment e on e.eqid = o.eqid where o.measid = '" & measid & "'"
        dr = pmf.GetRdrData(sql)
        While dr.Read
            lblcoid.Value = dr.Item("comid").ToString
            lblfuid.Value = dr.Item("funcid").ToString
            lbleqid.Value = dr.Item("eqid").ToString
            lbleqnum.Value = dr.Item("eqnum").ToString
            lblsid.Value = dr.Item("siteid").ToString
            lblcellid.Value = dr.Item("cellid").ToString
            lbldeptid.Value = dr.Item("deptid").ToString
        End While
        dr.Close()

    End Sub
    Private Sub GetLists()
        cid = "0" 'lblcid.Value
        sql = "select skillid, skill " _
        + "from pmSkills where compid = '" & cid & "'"
        dr = pmf.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("Select"))
        ddskill.Items(0).Value = 0
    End Sub

    Private Sub btnwo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnwo.Click
        pmf.Open()
        GenCorr()
        pmf.Dispose()
    End Sub
    Private Sub GenCorr()
        Dim wonum As Integer
        Dim superid, super, leadid, lead, se, le, start, skillid, skill As String
        superid = lblsup.Value
        super = txtsup.Text
        leadid = lbllead.Value
        lead = txtlead.Text
        If cbsupe.Checked = True Then
            se = "1"
        Else
            se = "0"
        End If
        If cbleade.Checked = True Then
            le = "1"
        Else
            le = "0"
        End If
        start = txtstart.Text
        If start = "" Then
            start = "NULL"
        Else
            start = "'" & start & "'"
        End If
        skillid = ddskill.SelectedValue.ToString
        Try
            skill = ddskill.SelectedItem.ToString
        Catch ex As Exception

        End Try
        Dim usr As String
        Try
            usr = HttpContext.Current.Session("username").ToString
        Catch ex As Exception
            usr = "PM User"
        End Try

        comid = lblcoid.Value
        fuid = lblfuid.Value
        eqid = lbleqid.Value
        eqnum = lbleqnum.Value
        sid = lblsid.Value
        cellid = lblcellid.Value
        deptid = lbldeptid.Value

        Dim prob, corr As String
        prob = txtprob.Text
        corr = txtcorr.Text


        sql = "insert into workorder (status, statusdate, changeby, changedate, reportedby, reportdate, worktype, eqid, " _
        + "eqnum, siteid, deptid, cellid, funcid, comid, targstartdate, leadcraftid, leadcraft, leadealert, " _
        + "superid, supervisor, supealert, skillid, skill) " _
        + "values ('WAPPR', getDate(),'" & usr & "', getDate(),'" & usr & "', getDate(), 'CM','" & eqid & "', " _
        + "'" & eqnum & "','" & sid & "','" & deptid & "','" & cellid & "','" & fuid & "','" & comid & "', " _
        + "" & start & ",'" & leadid & "','" & lead & "','" & le & "','" & superid & "','" & super & "','" & se & "', " _
        + "'" & skillid & "','" & skill & "') " _
        + "select @@identity"
        wonum = pmf.Scalar(sql)
        lblwo.Value = wonum
        Dim desc As String
        sql = "insert into wocorr (wctype, wonum, problem, corraction) values ('fail','" & wonum & "','" & txtprob.Text & "','" & txtcorr.Text & "')"
        pmf.Update(sql)
        desc = "Corrective Action for Equipment# " & eqnum
        SaveDesc(desc)

        Dim mail As New pmmail
        If cbsupe.Checked = True Then
            'SendIt("sup", superid, wonum, desc, start)
            Dim mail1 As New pmmail
            mail1.CheckIt("sup", superid, wonum)
        End If
        If cbleade.Checked = True Then
            'SendIt("lead", leadid, wonum, desc, start)
            Dim mail2 As New pmmail
            mail2.CheckIt("lead", leadid, wonum)
        End If
    End Sub
    Private Sub SaveDesc(ByVal lg As String)
        Dim wonum As String = lblwo.Value
        Dim sh As String
        Dim lgcnt As Integer
        Dim test As String = lg
        If Len(lg) > 79 Then
            sh = Mid(lg, 1, 79)
            sql = "update workorder set description = '" & sh & "' where wonum = '" & wonum & "'"
            pmf.Update(sql)
            lg = Mid(lg, 80)
            sql = "select count(*) from wolongdesc where wonum = '" & wonum & "'"
            lgcnt = pmf.Scalar(sql)
            If lgcnt <> 0 Then
                sql = "update wolongdesc set longdesc = '" & lg & "' where wonum = '" & wonum & "'"
                pmf.Update(sql)
            Else
                sql = "insert into wolongdesc (wonum, longdesc) values ('" & wonum & "','" & lg & "')"
                pmf.Update(sql)
            End If
        Else
            sql = "update workorder set description = '" & lg & "' where wonum = '" & wonum & "'" _
            + "delete from wolongdesc where wonum = '" & wonum & "'"
            pmf.Update(sql)
        End If

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3252.Text = axlabs.GetASPXPage("measoutwo.aspx", "lang3252")
        Catch ex As Exception
        End Try
        Try
            lang3253.Text = axlabs.GetASPXPage("measoutwo.aspx", "lang3253")
        Catch ex As Exception
        End Try
        Try
            lang3254.Text = axlabs.GetASPXPage("measoutwo.aspx", "lang3254")
        Catch ex As Exception
        End Try
        Try
            lang3255.Text = axlabs.GetASPXPage("measoutwo.aspx", "lang3255")
        Catch ex As Exception
        End Try
        Try
            lang3256.Text = axlabs.GetASPXPage("measoutwo.aspx", "lang3256")
        Catch ex As Exception
        End Try
        Try
            lang3257.Text = axlabs.GetASPXPage("measoutwo.aspx", "lang3257")
        Catch ex As Exception
        End Try
        Try
            lang3258.Text = axlabs.GetASPXPage("measoutwo.aspx", "lang3258")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnwo.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                btnwo.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                btnwo.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                btnwo.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "spa" Then
                btnwo.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            ovid285.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("measoutwo.aspx", "ovid285") & "')")
            ovid285.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid286.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("measoutwo.aspx", "ovid286") & "')")
            ovid286.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
