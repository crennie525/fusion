

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class measoutroutesmini
    Inherits System.Web.UI.Page
	Protected WithEvents imgei As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents img As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang3251 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim dslev As DataSet
    Dim meas As New Utilities
    Dim gmeas As New Utilities
    Dim eqid, fuid, coid, sid, cid As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 5
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim Sort As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents dgmeas As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                sid = HttpContext.Current.Session("dfltps").ToString()
                lblsid.Value = sid
            Catch ex As Exception
                lblsid.Value = "20"
            End Try
            Try
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
            Catch ex As Exception

            End Try
            Try
                fuid = Request.QueryString("fuid").ToString
                lblfuid.Value = fuid
            Catch ex As Exception

            End Try
            Try
                coid = Request.QueryString("coid").ToString
                lblcoid.Value = coid
            Catch ex As Exception

            End Try
            meas.Open()
            GetRoutes(PageNumber)
            meas.Dispose()
        Else
            If Request.Form("lblsubmit") = "addrte" Then
                lblsubmit.Value = ""
                meas.Open()
                'AddRte()
                meas.Dispose()
            ElseIf Request.Form("lblsubmit") = "uprte" Then
                lblsubmit.Value = ""
                PageNumber = txtpg.Value
                GetRoutes(PageNumber)
                meas.Dispose()
            End If
            If Request.Form("lblsubmit") = "next" Then
                meas.Open()
                GetNext()
                meas.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "last" Then
                meas.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetRoutes(PageNumber)
                meas.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "prev" Then
                meas.Open()
                GetPrev()
                meas.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "first" Then
                meas.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetRoutes(PageNumber)
                meas.Dispose()
                lblsubmit.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetRoutes(PageNumber)
        Catch ex As Exception
            meas.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1577" , "measoutroutesmini.aspx.vb")
 
            meas.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetRoutes(PageNumber)
        Catch ex As Exception
            meas.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1578" , "measoutroutesmini.aspx.vb")
 
            meas.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetRoutes(ByVal PageNumber As Integer)
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        coid = lblcoid.Value

        sid = lblsid.Value
        Filter = "siteid = ''" & sid & "'' "
        FilterCnt = "siteid = '" & sid & "' "
        Dim srch As String = txtsrch.Text
        srch = meas.ModString2(srch)
        If Len(srch) > 0 Then
            Filter += "and route like ''%" & srch & "%'' "
            FilterCnt += "and route like '%" & srch & "%' "
        End If
        If eqid <> "" Then
            If fuid <> "" And coid <> "" Then
                Filter += "and routeid in (select routeid from outmeasureroutesmeas where measid in (select measid from outmeasure where eqid = ''" & eqid & "'' and funcid = ''" & fuid & "'' and comid = ''" & coid & "''))"
                FilterCnt += "and routeid in (select routeid from outmeasureroutesmeas where measid in (select measid from outmeasure where eqid = '" & eqid & "' and funcid = '" & fuid & "' and comid = '" & coid & "'))"
            ElseIf fuid <> "" And coid = "" Then
                Filter += "and routeid in (select routeid from outmeasureroutesmeas where measid in (select measid from outmeasure where eqid = ''" & eqid & "'' and funcid = ''" & fuid & "''))"
                FilterCnt += "and routeid in (select routeid from outmeasureroutesmeas where measid in (select measid from outmeasure where eqid = '" & eqid & "' and funcid = '" & fuid & "'))"
            Else
                Filter += "and routeid in (select routeid from outmeasureroutesmeas where measid in (select measid from outmeasure where eqid = ''" & eqid & "''))"
                FilterCnt += "and routeid in (select routeid from outmeasureroutesmeas where measid in (select measid from outmeasure where eqid = '" & eqid & "'))"
            End If

        End If
        sql = "select Count(*) from outmeasureroutes " _
           + "where " & FilterCnt
        Dim dc As Integer = meas.Scalar(sql)

        dgmeas.VirtualItemCount = dc

        Tables = "outmeasureroutes"
        PK = "routeid"
        PageSize = "10"
        Fields = "*"
        dr = meas.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        dgmeas.DataSource = dr
        Try
            dgmeas.DataBind()
        Catch ex As Exception
            dgmeas.CurrentPageIndex = 0
            dgmeas.DataBind()
        End Try
        dgmeas.Visible = True

        dr.Close()
        txtpg.Value = PageNumber
        txtpgcnt.Value = dgmeas.PageCount
        lblpg.Text = "Page " & PageNumber & " of " & dgmeas.PageCount
    End Sub
    
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer

        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgmeas.Columns(1).HeaderText = dlabs.GetDGPage("measoutroutesmini.aspx", "dgmeas", "1")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(2).HeaderText = dlabs.GetDGPage("measoutroutesmini.aspx", "dgmeas", "2")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3251.Text = axlabs.GetASPXPage("measoutroutesmini.aspx", "lang3251")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            img.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("measoutroutesmini.aspx", "img") & "', ABOVE, LEFT)")
            img.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("measoutroutesmini.aspx", "Img1") & "', ABOVE, LEFT)")
            Img1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgei.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("measoutroutesmini.aspx", "imgei") & "', ABOVE, LEFT)")
            imgei.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
