<%@ Page Language="vb" AutoEventWireup="false" Codebehind="measoutaddrt.aspx.vb" Inherits="lucy_r12.measoutaddrt" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>measoutaddrt</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/reportnav.js"></script>
		<script language="JavaScript" src="../scripts1/measoutaddrtaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checkmalert();">
		<form id="form1" method="post" runat="server">
			<table width="450">
				<tr>
					<td class="label" width="140" height="20"><asp:Label id="lang3205" runat="server">Current Route:</asp:Label></td>
					<td class="plainlabel" id="tdroute" width="310" runat="server"></td>
				</tr>
				<tr>
					<td class="label" width="140" height="20"><asp:Label id="lang3206" runat="server">Current Equipment:</asp:Label></td>
					<td class="plainlabel" id="tdeq" width="310" runat="server"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3207" runat="server">Current Function:</asp:Label></td>
					<td class="plainlabel" id="tdfu" runat="server"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3208" runat="server">Current Component:</asp:Label></td>
					<td class="plainlabel" id="tdco" runat="server"></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3209" runat="server">Current Measurement:</asp:Label></td>
					<td class="plainlabelred" id="tdout" runat="server"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3210" runat="server">Measure Type:</asp:Label></td>
					<td class="plainlabelred" id="tdtype" runat="server"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3211" runat="server">Measurement:</asp:Label></td>
					<td class="plainlabelred" id="tdmeas" runat="server"></td>
				</tr>
				<tr>
					<td class="label" height="21" style="HEIGHT: 21px"><asp:Label id="lang3212" runat="server">Characteristic:</asp:Label></td>
					<td class="plainlabelred" id="tdchar" runat="server" style="HEIGHT: 21px"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3213" runat="server">Hi Value:</asp:Label></td>
					<td class="plainlabelred" id="tdhi" runat="server"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3214" runat="server">Lo Value:</asp:Label></td>
					<td class="plainlabelred" id="tdlo" runat="server"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang3215" runat="server">Specific Value:</asp:Label></td>
					<td class="plainlabelred" id="tdspec" runat="server"></td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<table>
							<tr>
								<td class="bluelabel" height="24"><asp:Label id="lang3216" runat="server">Recorded Date:</asp:Label></td>
								<td colspan="3" class="plainlabel" id="tdrecdate" runat="server"></td>
							</tr>
							<tr>
								<td class="bluelabel"><asp:Label id="lang3217" runat="server">Measure Date</asp:Label></td>
								<td>
									<asp:TextBox id="txtdate" runat="server"></asp:TextBox></td>
								<td><IMG onclick="getcal('txtdate');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
										width="19"></td>
								<td>
									<asp:CheckBox id="cbdate" runat="server" CssClass="plainlabel" Checked="True" Text="Use Current"></asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td class="bluelabel"><asp:Label id="lang3218" runat="server">Measure Time</asp:Label></td>
								<td>
									<table>
										<tr>
											<td><asp:dropdownlist id="ddhrspm" runat="server" CssClass="plainlabel">
													<asp:ListItem Value="NA">NA</asp:ListItem>
													<asp:ListItem Value="01">01</asp:ListItem>
													<asp:ListItem Value="02">02</asp:ListItem>
													<asp:ListItem Value="03">03</asp:ListItem>
													<asp:ListItem Value="04">04</asp:ListItem>
													<asp:ListItem Value="05">05</asp:ListItem>
													<asp:ListItem Value="06">06</asp:ListItem>
													<asp:ListItem Value="07">07</asp:ListItem>
													<asp:ListItem Value="08">08</asp:ListItem>
													<asp:ListItem Value="09">09</asp:ListItem>
													<asp:ListItem Value="10">10</asp:ListItem>
													<asp:ListItem Value="11">11</asp:ListItem>
													<asp:ListItem Value="12">12</asp:ListItem>
												</asp:dropdownlist></td>
											<td class="bluelabel">:
											</td>
											<td><asp:dropdownlist id="ddminspm" runat="server" CssClass="plainlabel">
													<asp:ListItem Value="00">00</asp:ListItem>
													<asp:ListItem Value="01">01</asp:ListItem>
													<asp:ListItem Value="02">02</asp:ListItem>
													<asp:ListItem Value="03">03</asp:ListItem>
													<asp:ListItem Value="04">04</asp:ListItem>
													<asp:ListItem Value="05">05</asp:ListItem>
													<asp:ListItem Value="06">06</asp:ListItem>
													<asp:ListItem Value="07">07</asp:ListItem>
													<asp:ListItem Value="08">08</asp:ListItem>
													<asp:ListItem Value="09">09</asp:ListItem>
													<asp:ListItem Value="10">10</asp:ListItem>
													<asp:ListItem Value="11">11</asp:ListItem>
													<asp:ListItem Value="12">12</asp:ListItem>
													<asp:ListItem Value="13">13</asp:ListItem>
													<asp:ListItem Value="14">14</asp:ListItem>
													<asp:ListItem Value="15">15</asp:ListItem>
													<asp:ListItem Value="16">16</asp:ListItem>
													<asp:ListItem Value="17">17</asp:ListItem>
													<asp:ListItem Value="18">18</asp:ListItem>
													<asp:ListItem Value="19">19</asp:ListItem>
													<asp:ListItem Value="20">20</asp:ListItem>
													<asp:ListItem Value="21">21</asp:ListItem>
													<asp:ListItem Value="22">22</asp:ListItem>
													<asp:ListItem Value="23">23</asp:ListItem>
													<asp:ListItem Value="24">24</asp:ListItem>
													<asp:ListItem Value="25">25</asp:ListItem>
													<asp:ListItem Value="26">26</asp:ListItem>
													<asp:ListItem Value="27">27</asp:ListItem>
													<asp:ListItem Value="28">28</asp:ListItem>
													<asp:ListItem Value="29">29</asp:ListItem>
													<asp:ListItem Value="30">30</asp:ListItem>
													<asp:ListItem Value="31">31</asp:ListItem>
													<asp:ListItem Value="32">32</asp:ListItem>
													<asp:ListItem Value="33">33</asp:ListItem>
													<asp:ListItem Value="34">34</asp:ListItem>
													<asp:ListItem Value="35">35</asp:ListItem>
													<asp:ListItem Value="36">36</asp:ListItem>
													<asp:ListItem Value="37">37</asp:ListItem>
													<asp:ListItem Value="38">38</asp:ListItem>
													<asp:ListItem Value="39">39</asp:ListItem>
													<asp:ListItem Value="40">40</asp:ListItem>
													<asp:ListItem Value="41">41</asp:ListItem>
													<asp:ListItem Value="42">42</asp:ListItem>
													<asp:ListItem Value="43">43</asp:ListItem>
													<asp:ListItem Value="44">44</asp:ListItem>
													<asp:ListItem Value="45">45</asp:ListItem>
													<asp:ListItem Value="46">46</asp:ListItem>
													<asp:ListItem Value="47">47</asp:ListItem>
													<asp:ListItem Value="48">48</asp:ListItem>
													<asp:ListItem Value="49">49</asp:ListItem>
													<asp:ListItem Value="50">50</asp:ListItem>
													<asp:ListItem Value="51">51</asp:ListItem>
													<asp:ListItem Value="52">52</asp:ListItem>
													<asp:ListItem Value="53">53</asp:ListItem>
													<asp:ListItem Value="54">54</asp:ListItem>
													<asp:ListItem Value="55">55</asp:ListItem>
													<asp:ListItem Value="56">56</asp:ListItem>
													<asp:ListItem Value="57">57</asp:ListItem>
													<asp:ListItem Value="58">58</asp:ListItem>
													<asp:ListItem Value="59">59</asp:ListItem>
												</asp:dropdownlist></td>
											<td><asp:dropdownlist id="ddapspm" runat="server" CssClass="plainlabel">
													<asp:ListItem Value="AM">AM</asp:ListItem>
													<asp:ListItem Value="PM">PM</asp:ListItem>
												</asp:dropdownlist></td>
										</tr>
									</table>
								</td>
								<td></td>
								<td>
									<asp:CheckBox id="cbtime" runat="server" CssClass="plainlabel" Checked="True" Text="Use Current"></asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td class="bluelabel"><asp:Label id="lang3219" runat="server">Measured Value</asp:Label></td>
								<td>
									<asp:TextBox id="txtnewmeas" runat="server"></asp:TextBox></td>
								<td></td>
								<td align="right"><IMG id="btnadd" onclick="addToTask();" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif"
										runat="server">&nbsp;<IMG id="btnreturn" onclick="handleexit();" alt="" src="../images/appbuttons/minibuttons/candisk1.gif"
										runat="server"></td>
								<td>
									<img src="../images/appbuttons/minibuttons/comp.gif" id="imgcomp" runat="server" class="details">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lblmeasid" runat="server" NAME="lblmeasid"> <input type="hidden" id="lblsubmit" runat="server" NAME="lblsubmit">
			<input type="hidden" id="lblhi" runat="server" NAME="lblhi"> <input type="hidden" id="lbllo" runat="server" NAME="lbllo">
			<input type="hidden" id="lblmalert" runat="server" NAME="lblmalert"> <input type="hidden" id="lblrouteid" runat="server">
			<input type="hidden" id="txtpg" runat="server" NAME="Hidden1"><input type="hidden" id="txtpgcnt" runat="server" NAME="txtpgcnt">
			<input type="hidden" id="lblret" runat="server" NAME="lblret"> <input type="hidden" id="lblrtid" runat="server">
			<input type="hidden" id="lblcompcnt" runat="server"> <input type="hidden" id="lblwonum" runat="server">
			<input type="hidden" id="lblrhid" runat="server"> <input type="hidden" id="lblnextdate" runat="server">
			<input type="hidden" id="lblfreq" runat="server"> <input type="hidden" id="lblroute" runat="server">
			<input type="hidden" id="lbllog" runat="server" NAME="lbllog">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
