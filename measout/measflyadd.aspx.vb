

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class measflyadd
    Inherits System.Web.UI.Page
	Protected WithEvents lang3191 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3190 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3189 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3188 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3187 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3186 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3185 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3184 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3183 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3182 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim madd As New Utilities
    Dim pmtskid, tmdid, Login As String
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtdate As System.Web.UI.WebControls.TextBox
    Protected WithEvents cbdate As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddhrspm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddminspm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddapspm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cbtime As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtnewmeas As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdco As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdout As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtype As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmeas As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdchar As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdhi As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdspec As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblmeasid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmalert As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltmdid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtskid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            pmtskid = Request.QueryString("pmtskid").ToString '"2" ' 
            lblpmtskid.Value = pmtskid
            tmdid = Request.QueryString("tmdid").ToString '"2" ' 
            lbltmdid.Value = tmdid
            madd.Open()
            GetDetails()
            madd.Dispose()
        Else
            If Request.Form("lblsubmit") = "go" Then
                lblsubmit.Value = ""
                madd.Open()
                SaveMeas()
                madd.Dispose()

            End If
        End If
    End Sub
    Private Sub GetDetails()
        pmtskid = lblpmtskid.Value
        tmdid = lbltmdid.Value

        sql = "select m.tmdid, m.tmdidpar, m.pmtskid, p.eqid, op.funcid, f.func, op.comid, op.compnum, " _
        + "m.pmid, m.type, m.hi, m.mover, m.lo, m.munder, m.spec, m.record, " _
        + "m.measurement2, m.malert, t.tasknum, t.taskdesc, e.eqnum " _
        + "from pmTaskMeasDetMan m  " _
        + "left join pmtrack t on t.pmtskid = m.pmtskid  " _
        + "left join pm p on p.pmid = m.pmid  " _
        + "left join pmtasks op on op.pmtskid = m.pmtskid  " _
        + "left join equipment e on e.eqid = op.eqid  " _
        + "left join functions f on f.func_id = op.funcid  " _
        + "where m.tmdid = '" & tmdid & "' and m.pmtskid = '" & pmtskid & "' "

        dr = madd.GetRdrData(sql)
        While dr.Read
            tdeq.InnerHtml = dr.Item("eqnum").ToString
            tdfu.InnerHtml = dr.Item("func").ToString
            tdco.InnerHtml = dr.Item("compnum").ToString
            tdtype.InnerHtml = dr.Item("type").ToString
            tdhi.InnerHtml = dr.Item("hi").ToString
            lblhi.Value = dr.Item("hi").ToString
            tdlo.InnerHtml = dr.Item("lo").ToString
            lbllo.Value = dr.Item("lo").ToString
            tdspec.InnerHtml = dr.Item("spec").ToString
        End While
        dr.Close()
    End Sub
    Private Sub SaveMeas()
        Dim currd As Date = Now.ToShortDateString
        Dim currt As Date = Now.ToShortTimeString
        If cbdate.Checked <> True And txtdate.Text <> "" Then
            currd = txtdate.Text
        End If
        Dim pstr, ph, pm, pa As String
        If cbtime.Checked <> True Then
            ph = ddhrspm.SelectedValue
            pm = ddminspm.SelectedValue
            pa = ddapspm.SelectedValue
            currt = ph & ":" & pm & ":" & pa
        End If
        Dim currdt As Date = currd & " " & currt
        Dim m As String = txtnewmeas.Text
        Dim mchk As Decimal
        Try
            mchk = System.Convert.ToDecimal(m)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1564" , "measflyadd.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        pmtskid = lblpmtskid.Value
        tmdid = lbltmdid.Value

        sql = "usp_upmeasfly2 '" & tmdid & "', '" & pmtskid & "', '" & m & "', '" & currdt & "'"
        madd.Update(sql)

        txtnewmeas.Text = ""
        txtdate.Text = ""
        Try
            ddhrspm.SelectedIndex = 0
        Catch ex As Exception

        End Try

        Try
            ddminspm.SelectedIndex = 0
        Catch ex As Exception

        End Try

        Try
            ddapspm.SelectedIndex = 0
        Catch ex As Exception

        End Try

        Dim hi, lo As String
        hi = lblhi.Value
        lo = lbllo.Value

        Dim hchk As Decimal
        Try
            hchk = System.Convert.ToDecimal(hi)
            If mchk > hchk Then
                lblmalert.Value = "hi"
            End If
            Exit Sub
        Catch ex As Exception

        End Try
        Dim lchk As Decimal
        Try
            lchk = System.Convert.ToDecimal(lo)
            If lchk < mchk Then
                lblmalert.Value = "lo"
            End If
            Exit Sub
        Catch ex As Exception

        End Try


    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3182.Text = axlabs.GetASPXPage("measflyadd.aspx", "lang3182")
        Catch ex As Exception
        End Try
        Try
            lang3183.Text = axlabs.GetASPXPage("measflyadd.aspx", "lang3183")
        Catch ex As Exception
        End Try
        Try
            lang3184.Text = axlabs.GetASPXPage("measflyadd.aspx", "lang3184")
        Catch ex As Exception
        End Try
        Try
            lang3185.Text = axlabs.GetASPXPage("measflyadd.aspx", "lang3185")
        Catch ex As Exception
        End Try
        Try
            lang3186.Text = axlabs.GetASPXPage("measflyadd.aspx", "lang3186")
        Catch ex As Exception
        End Try
        Try
            lang3187.Text = axlabs.GetASPXPage("measflyadd.aspx", "lang3187")
        Catch ex As Exception
        End Try
        Try
            lang3188.Text = axlabs.GetASPXPage("measflyadd.aspx", "lang3188")
        Catch ex As Exception
        End Try
        Try
            lang3189.Text = axlabs.GetASPXPage("measflyadd.aspx", "lang3189")
        Catch ex As Exception
        End Try
        Try
            lang3190.Text = axlabs.GetASPXPage("measflyadd.aspx", "lang3190")
        Catch ex As Exception
        End Try
        Try
            lang3191.Text = axlabs.GetASPXPage("measflyadd.aspx", "lang3191")
        Catch ex As Exception
        End Try

    End Sub

End Class
