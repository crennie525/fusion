

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class measoutroutes
    Inherits System.Web.UI.Page
	Protected WithEvents imgei As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents img As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang3235 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3234 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim dslev As DataSet
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim meas As New Utilities
    Dim gmeas As New Utilities
    Dim eqid, fuid, coid, sid, cid, Login As String
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtnewroute As System.Web.UI.WebControls.TextBox
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Sort As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgmeas As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                sid = HttpContext.Current.Session("dfltps").ToString()
                lblsid.Value = sid
            Catch ex As Exception
                lblsid.Value = "20"
            End Try
            Try
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
            Catch ex As Exception

            End Try
            Try
                fuid = Request.QueryString("fuid").ToString
                lblfuid.Value = fuid
            Catch ex As Exception

            End Try
            Try
                coid = Request.QueryString("coid").ToString
                lblcoid.Value = coid
            Catch ex As Exception

            End Try
            meas.Open()
            GetRoutes(PageNumber)
            meas.Dispose()
        Else
            If Request.Form("lblsubmit") = "addrte" Then
                lblsubmit.Value = ""
                meas.Open()
                AddRte()
                meas.Dispose()
            ElseIf Request.Form("lblsubmit") = "uprte" Then
                lblsubmit.Value = ""
                PageNumber = txtpg.Value
                GetRoutes(PageNumber)
                meas.Dispose()
            End If
            If Request.Form("lblsubmit") = "next" Then
                meas.Open()
                GetNext()
                meas.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "last" Then
                meas.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetRoutes(PageNumber)
                meas.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "prev" Then
                meas.Open()
                GetPrev()
                meas.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "first" Then
                meas.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetRoutes(PageNumber)
                meas.Dispose()
                lblsubmit.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetRoutes(PageNumber)
        Catch ex As Exception
            meas.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1573" , "measoutroutes.aspx.vb")
 
            meas.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetRoutes(PageNumber)
        Catch ex As Exception
            meas.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1574" , "measoutroutes.aspx.vb")
 
            meas.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub AddRte()
        Dim newrte As String = txtnewroute.Text
        newrte = meas.ModString3(newrte)
        sid = lblsid.Value

        sql = "insert into outmeasureroutes (route, siteid, freqindex) values ('" & newrte & "','" & sid & "','0')"
        meas.Update(sql)
        PageNumber = txtpg.Value
        GetRoutes(PageNumber)
    End Sub
    Private Sub GetRoutes(ByVal PageNumber As Integer)
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        coid = lblcoid.Value

        sid = lblsid.Value
        Filter = "siteid = ''" & sid & "'' "
        FilterCnt = "siteid = '" & sid & "' "
        Dim srch As String = txtsrch.Text
        srch = meas.ModString2(srch)
        If Len(srch) > 0 Then
            Filter += "and route like ''%" & srch & "%'' "
            FilterCnt += "and route like '%" & srch & "%' "
        End If
        If eqid <> "" Then
            If fuid <> "" And coid <> "" Then
                Filter += "and routeid in (select routeid from outmeasureroutesmeas where measid in (select measid from outmeasure where eqid = ''" & eqid & "'' and funcid = ''" & fuid & "'' and comid = ''" & coid & "''))"
                FilterCnt += "and routeid in (select routeid from outmeasureroutesmeas where measid in (select measid from outmeasure where eqid = '" & eqid & "' and funcid = '" & fuid & "' and comid = '" & coid & "'))"
            ElseIf fuid <> "" And coid = "" Then
                Filter += "and routeid in (select routeid from outmeasureroutesmeas where measid in (select measid from outmeasure where eqid = ''" & eqid & "'' and funcid = ''" & fuid & "''))"
                FilterCnt += "and routeid in (select routeid from outmeasureroutesmeas where measid in (select measid from outmeasure where eqid = '" & eqid & "' and funcid = '" & fuid & "'))"
            Else
                Filter += "and routeid in (select routeid from outmeasureroutesmeas where measid in (select measid from outmeasure where eqid = ''" & eqid & "''))"
                FilterCnt += "and routeid in (select routeid from outmeasureroutesmeas where measid in (select measid from outmeasure where eqid = '" & eqid & "'))"
            End If
           
        End If
        sql = "select Count(*) from outmeasureroutes " _
           + "where " & FilterCnt
        Dim dc As Integer = meas.Scalar(sql)

        dgmeas.VirtualItemCount = dc

        Tables = "outmeasureroutes"
        PK = "routeid"
        PageSize = "10"
        Fields = "*"
        dr = meas.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        dgmeas.DataSource = dr
        Try
            dgmeas.DataBind()
        Catch ex As Exception
            dgmeas.CurrentPageIndex = 0
            dgmeas.DataBind()
        End Try
        dgmeas.Visible = True

        dr.Close()
        txtpg.Value = PageNumber
        txtpgcnt.Value = dgmeas.PageCount
        lblpg.Text = "Page " & PageNumber & " of " & dgmeas.PageCount
    End Sub
   
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer

        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function

    Private Sub dgmeas_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.EditCommand
        dgmeas.EditItemIndex = e.Item.ItemIndex
        meas.Open()
        PageNumber = txtpg.Value
        GetRoutes(PageNumber)
        meas.Dispose()
    End Sub

    Private Sub dgmeas_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.CancelCommand
        dgmeas.EditItemIndex = -1
        meas.Open()
        PageNumber = txtpg.Value
        GetRoutes(PageNumber)
        meas.Dispose()
    End Sub

    Private Sub dgmeas_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgmeas.ItemDataBound
        If e.Item.ItemType = ListItemType.EditItem Then
            Dim nd As String = CType(e.Item.FindControl("txtnext"), TextBox).ClientID
            Dim img As HtmlImage = CType(e.Item.FindControl("imgnext"), HtmlImage)
            img.Attributes.Add("onclick", "getcal('" & nd & "');")

            Dim img1 As HtmlImage = CType(e.Item.FindControl("Img1"), HtmlImage)
            Dim rteid As String = DataBinder.Eval(e.Item.DataItem, "routeid").ToString
            Dim rte As String = DataBinder.Eval(e.Item.DataItem, "route").ToString
            Dim freq As String = DataBinder.Eval(e.Item.DataItem, "freq").ToString
            img1.Attributes.Add("onclick", "getrouteadd('" & rteid & "','" & rte & "','" & freq & "');")
            Dim imgei As HtmlImage = CType(e.Item.FindControl("imgei"), HtmlImage)
            Dim wonum As String = DataBinder.Eval(e.Item.DataItem, "wonum").ToString
            If wonum <> "" Then
                imgei.Attributes.Add("onclick", "getuproute('" & rteid & "','" & rte & "','" & freq & "');")
            Else
                imgei.Attributes.Add("class", "details")
            End If


            Dim img2 As HtmlImage = CType(e.Item.FindControl("img"), HtmlImage)
            img2.Attributes.Add("onclick", "printmreport('" & rteid & "');")

        ElseIf e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim img1 As HtmlImage = CType(e.Item.FindControl("Img1"), HtmlImage)
            Dim rteid As String = DataBinder.Eval(e.Item.DataItem, "routeid").ToString
            Dim rte As String = DataBinder.Eval(e.Item.DataItem, "route").ToString
            Dim freq As String = DataBinder.Eval(e.Item.DataItem, "freq").ToString
            img1.Attributes.Add("onclick", "getrouteadd('" & rteid & "','" & rte & "','" & freq & "');")
            Dim imgei As HtmlImage = CType(e.Item.FindControl("imgei"), HtmlImage)
            Dim wonum As String = DataBinder.Eval(e.Item.DataItem, "wonum").ToString
            If wonum <> "" Then
                imgei.Attributes.Add("onclick", "getuproute('" & rteid & "','" & rte & "','" & freq & "');")
            Else
                imgei.Attributes.Add("class", "details")
            End If
            Dim img As HtmlImage = CType(e.Item.FindControl("img"), HtmlImage)
            img.Attributes.Add("onclick", "printmreport('" & rteid & "');")
        End If
    End Sub

    Private Sub dgmeas_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.UpdateCommand
        Dim nd As String = CType(e.Item.FindControl("txtnext"), TextBox).Text
        Dim fr As String = CType(e.Item.FindControl("txtfreq"), TextBox).Text
        Dim frchk As Long
        Try
            frchk = System.Convert.ToInt32(fr)
        Catch ex As Exception
            Dim strMessage As String = "Frequency Must be a Non Decimal Numeric Value"

            meas.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim fri As String = "0"
        Dim rteid As String = CType(e.Item.FindControl("lblmeaside"), Label).Text
        Dim rhid As String = CType(e.Item.FindControl("lblrhid"), Label).Text
        Dim wo As String = CType(e.Item.FindControl("lblwonum"), Label).Text
        Dim route As String = CType(e.Item.FindControl("txtroute"), Label).Text
        Dim ustr, usr As String
        Try
            usr = HttpContext.Current.Session("username").ToString()
            ustr = Replace(usr, "'", Chr(180), , , vbTextCompare)

        Catch ex As Exception
            ustr = "PM Admin"
        End Try
      
        sql = "update outmeasureroutes set freq = '" & fr & "', freqindex = '" & fri & "', nextdate = '" & nd & "' " _
        + " where routeid = '" & rteid & "'"
        'usp_updatemeasroute(@routeid int, @freq int, @freqindex int, @nextdate datetime, 
        '@wonum int, @rhid int, @uid varchar(50), @route varchar(50))
        Dim cmd As New SqlCommand("exec usp_updatemeasroute @routeid, @freq, @freqindex, @nextdate, " _
        + "@wonum, @rhid, @uid, @route")
        Dim param1 = New SqlParameter("@routeid", SqlDbType.VarChar)
        If Len(rteid) > 0 Then
            param1.Value = rteid
        Else
            param1.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param1)
        Dim param2 = New SqlParameter("@freq", SqlDbType.VarChar)
        If Len(fr) > 0 Or fr <> "Select" Then
            param2.Value = fr
        Else
            param2.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param2)
        Dim param3 = New SqlParameter("@freqindex", SqlDbType.VarChar)
        If Len(fr) > 0 Or fr <> "Select" Then
            param3.Value = fri
        Else
            param3.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param3)
        Dim param4 = New SqlParameter("@nextdate", SqlDbType.VarChar)
        If Len(nd) > 0 Then
            param4.Value = nd
        Else
            param4.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param4)
        Dim param5 = New SqlParameter("@wonum", SqlDbType.VarChar)
        If Len(wo) > 0 Then
            param5.Value = wo
        Else
            param5.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param5)
        Dim param6 = New SqlParameter("@rhid", SqlDbType.VarChar)
        If Len(rhid) > 0 Then
            param6.Value = rhid
        Else
            param6.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param6)
        Dim param7 = New SqlParameter("@uid", SqlDbType.VarChar)
        If Len(ustr) > 0 Then
            param7.Value = ustr
        Else
            param7.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param7)
        Dim param8 = New SqlParameter("@route", SqlDbType.VarChar)
        If Len(route) > 0 Then
            param8.Value = route
        Else
            param8.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param8)

        meas.Open()
        'meas.Update(sql)
        meas.UpdateHack(cmd)
        dgmeas.EditItemIndex = -1
        PageNumber = txtpg.Value
        GetRoutes(PageNumber)
        meas.Dispose()
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgmeas.Columns(0).HeaderText = dlabs.GetDGPage("measoutroutes.aspx", "dgmeas", "0")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(1).HeaderText = dlabs.GetDGPage("measoutroutes.aspx", "dgmeas", "1")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(2).HeaderText = dlabs.GetDGPage("measoutroutes.aspx", "dgmeas", "2")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(3).HeaderText = dlabs.GetDGPage("measoutroutes.aspx", "dgmeas", "3")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(4).HeaderText = dlabs.GetDGPage("measoutroutes.aspx", "dgmeas", "4")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(8).HeaderText = dlabs.GetDGPage("measoutroutes.aspx", "dgmeas", "8")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(9).HeaderText = dlabs.GetDGPage("measoutroutes.aspx", "dgmeas", "9")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3234.Text = axlabs.GetASPXPage("measoutroutes.aspx", "lang3234")
        Catch ex As Exception
        End Try
        Try
            lang3235.Text = axlabs.GetASPXPage("measoutroutes.aspx", "lang3235")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            img.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("measoutroutes.aspx", "img") & "', ABOVE, LEFT)")
            img.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("measoutroutes.aspx", "Img1") & "', ABOVE, LEFT)")
            Img1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgei.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("measoutroutes.aspx", "imgei") & "', ABOVE, LEFT)")
            imgei.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
