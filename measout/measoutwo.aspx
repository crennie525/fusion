<%@ Page Language="vb" AutoEventWireup="false" Codebehind="measoutwo.aspx.vb" Inherits="lucy_r12.measoutwo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>measoutwo</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/measoutwoaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="checkstuff();" >
		<form id="form1" method="post" runat="server">
			<table>
				<tr height="20">
					<td width="20"></td>
					<td width="20"></td>
					<td class="bluelabel" width="120"><asp:Label id="lang3252" runat="server">Skill Required</asp:Label></td>
					<td colSpan="2"><asp:dropdownlist id="ddskill" runat="server" cssclass="plainlabel" Width="160px"></asp:dropdownlist></td>
					<td width="50"></td>
				</tr>
				<tr height="20">
					<td><INPUT id="cbsupe" type="checkbox" name="cbsupe" runat="server"></td>
					<td><IMG onmouseover="return overlib('Check to send email alert to Supervisor')" onmouseout="return nd()"
							src="../images/appbuttons/minibuttons/emailcb.gif"></td>
					<td class="bluelabel"><asp:Label id="lang3253" runat="server">Supervisor</asp:Label></td>
					<td width="140"><asp:textbox id="txtsup" runat="server" Width="130px" ReadOnly="True" CssClass="plainlabel"></asp:textbox></td>
					<td width="20"><IMG onclick="getsuper('sup');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
							border="0"></td>
					<td></td>
				</tr>
				<tr height="20">
					<td><INPUT id="cbleade" type="checkbox" name="cbleade" runat="server"></td>
					<td><IMG onmouseover="return overlib('Check to send email alert to Lead Craft')" onmouseout="return nd()"
							src="../images/appbuttons/minibuttons/emailcb.gif"></td>
					<td class="bluelabel"><asp:Label id="lang3254" runat="server">Lead Craft</asp:Label></td>
					<td><asp:textbox id="txtlead" runat="server" Width="130px" ReadOnly="True" CssClass="plainlabel"></asp:textbox></td>
					<td><IMG onclick="getsuper('lead');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
							border="0"></td>
					<td></td>
				</tr>
				<tr height="20">
					<td></td>
					<td></td>
					<td class="bluelabel"><asp:Label id="lang3255" runat="server">Work Type</asp:Label></td>
					<td colSpan="2"><asp:dropdownlist id="ddwt" runat="server" CssClass="plainlabel">
							<asp:ListItem Value="CM" Selected="True">Corrective Maintenance</asp:ListItem>
							<asp:ListItem Value="EM">Emergency Maintenance</asp:ListItem>
						</asp:dropdownlist></td>
					<td></td>
				</tr>
				<tr height="20">
					<td></td>
					<td></td>
					<td class="bluelabel"><asp:Label id="lang3256" runat="server">Target Start</asp:Label></td>
					<td><asp:textbox id="txtstart" runat="server" Width="130px" ReadOnly="True" CssClass="plainlabel"></asp:textbox></td>
					<td><IMG onclick="getcal('tstart');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
							width="19"></td>
					<td></td>
				</tr>
				<tr height="20">
					<td class="bluelabel" colSpan="6"><asp:Label id="lang3257" runat="server">Problem Description</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="6"><asp:textbox id="txtprob" runat="server" Width="380px" CssClass="plainlabel" Height="50px" TextMode="MultiLine"></asp:textbox></td>
				</tr>
				<tr height="20">
					<td class="bluelabel" colSpan="6"><asp:Label id="lang3258" runat="server">Corrective Action</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="6"><asp:textbox id="txtcorr" runat="server" Width="380px" CssClass="plainlabel" Height="50px" TextMode="MultiLine"></asp:textbox></td>
				</tr>
				<tr height="30">
					<td align="right" colSpan="6"><asp:imagebutton id="btnwo" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td colSpan="6">&nbsp;</td>
				</tr>
			</table>
			<input id="lblmeasid" type="hidden" runat="server"><input id="lblsup" type="hidden" runat="server">
			<input id="lbllead" type="hidden" runat="server"> <input id="lblfuid" type="hidden" runat="server">
			<input id="lblcoid" type="hidden" runat="server"> <input id="lblwo" type="hidden" runat="server">
			<input id="lbleqid" type="hidden" runat="server"> <input id="lbleqnum" type="hidden" runat="server">
			<input id="lblsid" type="hidden" runat="server"> <input id="lbldeptid" type="hidden" runat="server">
			<input id="lblcellid" type="hidden" runat="server"> <input type="hidden" id="lblsubmit" runat="server">
			<input type="hidden" id="lbltmdid" runat="server"> <input type="hidden" id="lblpmtskid" runat="server">
			<input type="hidden" id="lbllog" runat="server" NAME="lbllog">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
