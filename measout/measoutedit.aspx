<%@ Page Language="vb" AutoEventWireup="false" Codebehind="measoutedit.aspx.vb" Inherits="lucy_r12.measoutedit" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>measoutedit</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script language="JavaScript" src="../scripts1/measouteditaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="checkstuff();scrolltop();" >
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 4px; POSITION: absolute; TOP: 0px" id="scrollmenu">
				<tr>
					<td>
						<table>
							<tr>
								<td class="label" id="tdfunc" width="80" runat="server"><asp:Label id="lang3220" runat="server">Function</asp:Label></td>
								<td width="220"><asp:dropdownlist id="ddfunc" runat="server" AutoPostBack="True"></asp:dropdownlist></td>
								<td align="right" width="750"><IMG id="imgmag" onmouseover="return overlib('View All Equipment Records', ABOVE, LEFT)"
										onclick="getall();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/magnifier.gif" runat="server">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:datagrid id="dgmeas" runat="server" Width="1050px" BackColor="transparent" CellSpacing="1"
							AutoGenerateColumns="False" AllowCustomPaging="True" AllowPaging="True" GridLines="None" CellPadding="0"
							ShowFooter="True">
							<FooterStyle BackColor="transparent"></FooterStyle>
							<EditItemStyle Height="15px" BackColor="transparent"></EditItemStyle>
							<AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
							<ItemStyle CssClass="ptransrow"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="30px" Height="18px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<img id="imgedit" runat="server" src="../images/appbuttons/minibuttons/lilpentrans.gif">
									</ItemTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Title">
									<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label17" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.meastitle") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label18" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.meastitle") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Function">
									<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label1" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label11" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Component">
									<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label12" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label13" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Measure Type">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label4 runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label3" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Measurement">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.measure") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label14" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.measure") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Characteristic">
									<HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label15" runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.char") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label16" runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.char") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Hi">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label5" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label6" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Lo">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label7" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label8" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Spec">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label9" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label10" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="History">
									<HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<img src="../images/appbuttons/minibuttons/lchart.gif" onmouseover="return overlib('View History Graph', ABOVE, LEFT)"
											onmouseout="return nd()" id="ihg" runat="server"> <IMG id="img" runat="server" onmouseover="return overlib('Print History', ABOVE, LEFT)"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/printex.gif">
										<IMG id="imgei" runat="server" onmouseover="return overlib('Edit History', ABOVE, LEFT)"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/pencil.gif"> <IMG id="Img1" runat="server" onmouseover="return overlib('Add History', ABOVE, LEFT)"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/addmod.gif">
									</ItemTemplate>
									<EditItemTemplate>
										<img src="../images/appbuttons/minibuttons/lchart.gif" onmouseover="return overlib('View History Graph', ABOVE, LEFT)"
											onmouseout="return nd()" id="ihga" runat="server"> <IMG id="imga" runat="server" onmouseover="return overlib('Print History', ABOVE, LEFT)"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/printex.gif">
										<IMG id="imgee" runat="server" onmouseover="return overlib('Edit History', ABOVE, LEFT)"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/pencil.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<ItemTemplate>
										<asp:Label id=lblpmtskid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.measid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblpmtskida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.measid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
								ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
						</asp:datagrid></td>
				</tr>
			</table>
			<input id="lblcharturl" type="hidden" name="lblcharturl" runat="server"> <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lblro" type="hidden" runat="server"> <input id="lblfuid" type="hidden" runat="server">
			<input id="lblcoid" type="hidden" runat="server"> <input id="lblfcnt" type="hidden" runat="server">
			<input id="lblchngfunc" type="hidden" runat="server"> <input id="lblchngcomp" type="hidden" runat="server">
			<input id="lblsubmit" type="hidden" runat="server"><input id="xCoord" type="hidden" name="xCoord" runat="server">
			<input id="yCoord" type="hidden" name="yCoord" runat="server"> <input type="hidden" id="lblfunc" runat="server">
			<input type="hidden" id="lblcomp" runat="server"> <input type="hidden" id="lbllog" runat="server" NAME="lbllog">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
