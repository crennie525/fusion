

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class measoutaddrt
    Inherits System.Web.UI.Page
	Protected WithEvents lang3219 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3218 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3217 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3216 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3215 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3214 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3213 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3212 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3211 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3210 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3209 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3208 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3207 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3206 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3205 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim madd As New Utilities
    Dim measid, routeid, rtid, route, Login As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 1
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim Sort As String
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdroute As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdrecdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgcomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnextdate As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblroute As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtdate As System.Web.UI.WebControls.TextBox
    Protected WithEvents cbdate As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddhrspm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddminspm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddapspm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cbtime As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtnewmeas As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdco As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdout As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtype As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmeas As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdchar As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdhi As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdspec As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblmeasid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmalert As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrouteid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            routeid = Request.QueryString("routeid").ToString '"2" ' 
            lblrouteid.Value = routeid
            route = Request.QueryString("rte").ToString '"2" ' 
            lblroute.Value = route
            tdroute.InnerHtml = route
            txtpg.Value = "1"
            madd.Open()
            GetDetails(PageNumber)
            madd.Dispose()
        Else
            If Request.Form("lblsubmit") = "go" Then
                lblsubmit.Value = ""
                madd.Open()
                SaveMeas()
                madd.Dispose()
            ElseIf Request.Form("lblsubmit") = "comp" Then
                lblsubmit.Value = ""
                madd.Open()
                CompMeas()
                madd.Dispose()
            End If
            If Request.Form("lblsubmit") = "next" Then
                madd.Open()
                GetNext()
                madd.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "last" Then
                madd.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetDetails(PageNumber)
                madd.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "prev" Then
                madd.Open()
                GetPrev()
                madd.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "first" Then
                madd.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetDetails(PageNumber)
                madd.Dispose()
                lblsubmit.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetDetails(PageNumber)
        Catch ex As Exception
            madd.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1566" , "measoutaddrt.aspx.vb")
 
            madd.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetDetails(PageNumber)
        Catch ex As Exception
            madd.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1567" , "measoutaddrt.aspx.vb")
 
            madd.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetDetails(ByVal PageNumber As Integer)
        routeid = lblrouteid.Value

        sql = "select count(*) from outmeasroutetrack where routeid = '" & routeid & "'"
        Dim intPgCnt As Integer = madd.Scalar(sql)

        sql = "usp_getallmeasroutepg '" & routeid & "','" & PageNumber & "','1'"
        dr = madd.GetRdrData(sql)
        While dr.Read
            lblnextdate.Value = dr.Item("nextdate").ToString
            lblfreq.Value = dr.Item("freq").ToString
            lblrhid.Value = dr.Item("rhid").ToString
            lblrtid.Value = dr.Item("rtid").ToString
            lblwonum.Value = dr.Item("wonum").ToString
            lblmeasid.Value = dr.Item("measid").ToString
            tdeq.InnerHtml = dr.Item("eqnum").ToString
            tdfu.InnerHtml = dr.Item("func").ToString
            tdco.InnerHtml = dr.Item("compnum").ToString
            tdout.InnerHtml = dr.Item("meastitle").ToString
            tdtype.InnerHtml = dr.Item("type").ToString
            tdmeas.InnerHtml = dr.Item("measure").ToString
            tdchar.InnerHtml = dr.Item("char").ToString
            tdhi.InnerHtml = dr.Item("hi").ToString
            lblhi.Value = dr.Item("hi").ToString
            tdlo.InnerHtml = dr.Item("lo").ToString
            lbllo.Value = dr.Item("lo").ToString
            tdspec.InnerHtml = dr.Item("spec").ToString
            txtnewmeas.Text = dr.Item("measurement").ToString
            lblcompcnt.Value = dr.Item("compcnt").ToString
            tdrecdate.InnerHtml = dr.Item("measdate").ToString
        End While
        dr.Close()
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgCnt
        lblpg.Text = "Page " & PageNumber & " of " & intPgCnt
        Dim compcnt As Integer
        Try
            compcnt = System.Convert.ToInt32(lblcompcnt.Value)
        Catch ex As Exception
            compcnt = 0
        End Try
        If intPgCnt = compcnt Then
            imgcomp.Attributes.Add("class", "view")
            imgcomp.Attributes.Add("onclick", "docomp();")
        Else
            imgcomp.Attributes.Add("class", "details")
            imgcomp.Attributes.Add("onclick", "")
        End If
    End Sub
    Private Sub CompMeas()
        Dim routeid, rhid, wonum, nextdate, freq, uid, route As String
        'usp_compmeasroute (@routeid int, @rhid int, @wonum int, @nextdate datetime, @freq int, 
        '@uid varchar(50), @route varchar(50)
        routeid = lblrouteid.Value
        rhid = lblrhid.Value
        wonum = lblwonum.Value
        nextdate = lblnextdate.Value
        freq = lblfreq.Value
        Dim ustr, usr As String
        Try
            usr = HttpContext.Current.Session("username").ToString()
            ustr = Replace(usr, "'", Chr(180), , , vbTextCompare)

        Catch ex As Exception
            ustr = "PM Admin"
        End Try
        route = lblroute.Value
        sql = "usp_compmeasroute '" & routeid & "','" & rhid & "','" & wonum & "','" & nextdate & "', " _
        + "'" & freq & "','" & ustr & "','" & route & "'"
        madd.Update(sql)
        lblret.Value = "return"
    End Sub
    Private Sub SaveMeas()
        Dim currd As Date = Now.ToShortDateString
        Dim currt As Date = Now.ToLongTimeString
        If cbdate.Checked <> True And txtdate.Text <> "" Then
            currd = txtdate.Text
        End If
        Dim pstr, ph, pm, pa As String
        If cbtime.Checked <> True Then
            ph = ddhrspm.SelectedValue
            pm = ddminspm.SelectedValue
            pa = ddapspm.SelectedValue
            currt = ph & ":" & pm & ":" & pa
        End If
        Dim currdt As Date = currd & " " & currt
        Dim m As String = txtnewmeas.Text
        Dim mchk As Decimal
        Try
            mchk = System.Convert.ToDecimal(m)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1568" , "measoutaddrt.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        measid = lblmeasid.Value
        routeid = lblrouteid.Value
        rtid = lblrtid.Value
        Dim ustr, usr As String
        Try
            usr = HttpContext.Current.Session("username").ToString()
            ustr = Replace(usr, "'", Chr(180), , , vbTextCompare)

        Catch ex As Exception
            ustr = "PM Admin"
        End Try
        sql = "update outmeasroutetrack set measurement = '" & m & "', measdate = '" & currd & "', measby = " _
        + "'" & ustr & "' where rtid = '" & rtid & "'"
        madd.Update(sql)

        txtnewmeas.Text = ""
        txtdate.Text = ""
        Try
            ddhrspm.SelectedIndex = 0
        Catch ex As Exception

        End Try

        Try
            ddminspm.SelectedIndex = 0
        Catch ex As Exception

        End Try

        Try
            ddapspm.SelectedIndex = 0
        Catch ex As Exception

        End Try

        Dim hi, lo As String
        hi = lblhi.Value
        lo = lbllo.Value

        Dim hchk As Decimal
        Try
            hchk = System.Convert.ToDecimal(hi)
            If mchk > hchk Then
                lblmalert.Value = "hi"
            End If
            Exit Sub
        Catch ex As Exception

        End Try
        Dim lchk As Decimal
        Try
            lchk = System.Convert.ToDecimal(lo)
            If lchk < mchk Then
                lblmalert.Value = "lo"
            End If
            Exit Sub
        Catch ex As Exception

        End Try
        PageNumber = txtpg.Value
        GetDetails(PageNumber)

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3205.Text = axlabs.GetASPXPage("measoutaddrt.aspx", "lang3205")
        Catch ex As Exception
        End Try
        Try
            lang3206.Text = axlabs.GetASPXPage("measoutaddrt.aspx", "lang3206")
        Catch ex As Exception
        End Try
        Try
            lang3207.Text = axlabs.GetASPXPage("measoutaddrt.aspx", "lang3207")
        Catch ex As Exception
        End Try
        Try
            lang3208.Text = axlabs.GetASPXPage("measoutaddrt.aspx", "lang3208")
        Catch ex As Exception
        End Try
        Try
            lang3209.Text = axlabs.GetASPXPage("measoutaddrt.aspx", "lang3209")
        Catch ex As Exception
        End Try
        Try
            lang3210.Text = axlabs.GetASPXPage("measoutaddrt.aspx", "lang3210")
        Catch ex As Exception
        End Try
        Try
            lang3211.Text = axlabs.GetASPXPage("measoutaddrt.aspx", "lang3211")
        Catch ex As Exception
        End Try
        Try
            lang3212.Text = axlabs.GetASPXPage("measoutaddrt.aspx", "lang3212")
        Catch ex As Exception
        End Try
        Try
            lang3213.Text = axlabs.GetASPXPage("measoutaddrt.aspx", "lang3213")
        Catch ex As Exception
        End Try
        Try
            lang3214.Text = axlabs.GetASPXPage("measoutaddrt.aspx", "lang3214")
        Catch ex As Exception
        End Try
        Try
            lang3215.Text = axlabs.GetASPXPage("measoutaddrt.aspx", "lang3215")
        Catch ex As Exception
        End Try
        Try
            lang3216.Text = axlabs.GetASPXPage("measoutaddrt.aspx", "lang3216")
        Catch ex As Exception
        End Try
        Try
            lang3217.Text = axlabs.GetASPXPage("measoutaddrt.aspx", "lang3217")
        Catch ex As Exception
        End Try
        Try
            lang3218.Text = axlabs.GetASPXPage("measoutaddrt.aspx", "lang3218")
        Catch ex As Exception
        End Try
        Try
            lang3219.Text = axlabs.GetASPXPage("measoutaddrt.aspx", "lang3219")
        Catch ex As Exception
        End Try

    End Sub

End Class
