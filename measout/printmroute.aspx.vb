

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class printmroute
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim routeid As String
    Dim rt As New Utilities
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Response.Clear()
            routeid = Request.QueryString("routeid").ToString
            rt.Open()
            GetRoute(routeid)
            rt.Dispose()
        End If
    End Sub
    Private Sub GetRoute(ByVal routeid As String)
        Dim sb As New System.Text.StringBuilder


        sb.Append("<html><head>")
        sb.Append("<LINK href=""../styles/pmcssa1.css"" type=""text/css"" rel=""stylesheet"">")
        sb.Append("</head><body>")
        Dim rd, freq As String
        sql = "select route, freq from outmeasureroutes where routeid = '" & routeid & "'"
        dr = rt.GetRdrData(sql)
        While dr.Read
            rd = dr.Item("route").ToString
            freq = dr.Item("freq").ToString
        End While
        dr.Close()
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""850"">")
        sb.Append("<tr><td class=""bigbold"" align=""center"">" & tmod.getlbl("cdlbl621" , "printmroute.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td class=""plainlabel"" align=""center""><b>Route:&nbsp;&nbsp;</b>" & rd & "</td></tr>")
        sb.Append("<tr><td class=""plainlabel"" align=""center""><b>Frequency:&nbsp;&nbsp;</b>" & freq & "</td></tr>")
        sb.Append("<tr><td>&nbsp;</td></tr>")
        sb.Append("</table>")


        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""850"">")
        sb.Append("<tr><td width=""50"" class=""label""><u>Seq#</u></td>")
        sb.Append("<td width=""120"" class=""label""><u>Title</u></td>")
        sb.Append("<td width=""120"" class=""label""><u>Equipment:</u></td>")
        sb.Append("<td width=""120"" class=""label""><u>Function:</u></td>")
        sb.Append("<td width=""120"" class=""label""><u>Component:</u></td>")
        sb.Append("<td width=""120"" class=""label""><u>Type:</u></td>")
        sb.Append("<td width=""50"" class=""label""><u>Hi</u></td>")
        sb.Append("<td width=""50"" class=""label""><u>Lo</u></td>")
        sb.Append("<td width=""50"" class=""label""><u>Spec</u></td></tr>")
      


        sql = "select  o.measid, o.meastitle, o.type, o.measure, o.hi, o.lo, o.spec, o.char, o.siteid, o.deptid, o.cellid, o.loc, o.eqid, o.funcid, " _
        + "o.comid, o.createdby, o.createdate, e.eqnum, f.func, c.compnum, r.routeorder, r.routeid " _
        + "from outmeasure o " _
        + "left join outmeasureroutesmeas r on r.measid = o.measid " _
        + "left join equipment e on e.eqid = o.eqid " _
        + "left join functions f on f.func_id = o.funcid " _
        + "left join components c on c.comid = o.comid " _
        + "where r.routeid = '" & routeid & "' order by r.routeorder"

        Dim seq, title, eq, fu, co, typ, hi, lo, spec As String
        dr = rt.GetRdrData(sql)
        While dr.Read
            seq = dr.Item("routeorder").ToString
            title = dr.Item("meastitle").ToString
            eq = dr.Item("eqnum").ToString
            fu = dr.Item("func").ToString
            co = dr.Item("compnum").ToString
            typ = dr.Item("type").ToString
            hi = dr.Item("hi").ToString
            lo = dr.Item("lo").ToString
            spec = dr.Item("spec").ToString

            sb.Append("<tr><td class=""plainlabel"">" & seq & "</td>")
            sb.Append("<td class=""plainlabel"">" & title & "</td>")
            sb.Append("<td class=""plainlabel"">" & eq & "</td>")
            sb.Append("<td class=""plainlabel"">" & fu & "</td>")
            sb.Append("<td class=""plainlabel"">" & co & "</td>")
            sb.Append("<td class=""plainlabel"">" & typ & "</td>")
            sb.Append("<td class=""plainlabel"">" & hi & "</td>")
            sb.Append("<td class=""plainlabel"">" & lo & "</td>")
            sb.Append("<td class=""plainlabel"">" & spec & "</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")


        Response.Write(sb.ToString)

    End Sub
End Class
