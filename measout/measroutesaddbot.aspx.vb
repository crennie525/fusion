

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class measroutesaddbot
    Inherits System.Web.UI.Page
	Protected WithEvents lang3260 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3259 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim rteid, eqid, fuid, coid As String
    Dim dr As SqlDataReader
    Dim sql As String
    Dim rte As New Utilities

    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dgmeas As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgmeasrt As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lbloldroute As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblrteid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                rteid = Request.QueryString("rteid").ToString
                lblrteid.Value = rteid
            Catch ex As Exception
                rteid = "0"
            End Try
            Try
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
            Catch ex As Exception
                eqid = "0"
            End Try
            Try
                fuid = Request.QueryString("fuid").ToString
                lblfuid.Value = fuid
            Catch ex As Exception
                fuid = "0"
                lblfuid.Value = "0"
            End Try
            Try
                coid = Request.QueryString("coid").ToString
                lblcoid.Value = coid
            Catch ex As Exception
                coid = "0"
                lblcoid.Value = "0"
            End Try
            rte.Open()
            If rteid <> "-1" Then
                GetRteMeas(rteid)
            End If
            If eqid <> "-1" Then
                GetAllMeas(rteid, eqid, fuid, coid)
            End If
            rte.Dispose()
        End If
    End Sub
    Private Sub GetRteMeas(ByVal rteid As String)
        sql = "select  o.measid, o.meastitle, o.type, o.measure, o.hi, o.lo, o.spec, o.char, o.siteid, o.deptid, o.cellid, o.loc, o.eqid, o.funcid, " _
            + "o.comid, o.createdby, o.createdate, e.eqnum, f.func, c.compnum, r.routeorder, r.routeid " _
        + "from outmeasure o " _
        + "left join outmeasureroutesmeas r on r.measid = o.measid " _
        + "left join equipment e on e.eqid = o.eqid " _
        + "left join functions f on f.func_id = o.funcid " _
        + "left join components c on c.comid = o.comid " _
        + "where r.routeid = '" & rteid & "' order by r.routeorder"
        dr = rte.GetRdrData(sql)
        dgmeasrt.DataSource = dr
        dgmeasrt.DataBind()
        dr.Close()
    End Sub
    Private Sub GetAllMeas(ByVal rteid As String, ByVal eqid As String, Optional ByVal fuid As String = "0", Optional ByVal coid As String = "0")
        If fuid = "0" And coid = "0" Then
            sql = "select o.measid, o.meastitle, o.type, o.measure, o.hi, o.lo, o.spec, o.char, o.siteid, o.deptid, o.cellid, o.loc, o.eqid, o.funcid, " _
            + "o.comid, o.createdby, o.createdate, f.func, c.compnum " _
             + "from outmeasure o " _
             + "left join functions f on f.func_id = o.funcid " _
             + "left join components c on c.comid = o.comid " _
             + "where o.eqid = '" & eqid & "' and o.measid not in ( " _
             + "select measid from outmeasureroutesmeas where routeid = '" & rteid & "')"
        ElseIf fuid <> "0" And coid = "0" Then
            sql = "select o.measid, o.meastitle, o.type, o.measure, o.hi, o.lo, o.spec, o.char, o.siteid, o.deptid, o.cellid, o.loc, o.eqid, o.funcid, " _
            + "o.comid, o.createdby, o.createdate, f.func, c.compnum " _
             + "from outmeasure o " _
             + "left join functions f on f.func_id = o.funcid " _
             + "left join components c on c.comid = o.comid " _
             + "where o.eqid = '" & eqid & "' and o.funcid = '" & fuid & "' and o.measid not in ( " _
             + "select measid from outmeasureroutesmeas where routeid = '" & rteid & "')"
        ElseIf fuid <> "0" And coid <> "0" Then
            sql = "select o.measid, o.meastitle, o.type, o.measure, o.hi, o.lo, o.spec, o.char, o.siteid, o.deptid, o.cellid, o.loc, o.eqid, o.funcid, " _
             + "o.comid, o.createdby, o.createdate, f.func, c.compnum " _
              + "from outmeasure o " _
             + "left join functions f on f.func_id = o.funcid " _
             + "left join components c on c.comid = o.comid " _
             + "where o.eqid = '" & eqid & "' and o.funcid = '" & fuid & "' and o.comid = '" & coid & "' and o.measid not in ( " _
             + "select measid from outmeasureroutesmeas where routeid = '" & rteid & "')"

        End If


        dr = rte.GetRdrData(sql)
        dgmeas.DataSource = dr
        dgmeas.DataBind()
        dr.Close()
    End Sub

    Private Sub dgmeas_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.ItemCommand
        If e.CommandName = "Select" Then
            Dim measid As String = CType(e.Item.FindControl("lblpmtskid"), Label).Text
            Dim rteid As String = lblrteid.Value
            sql = "usp_addroutemeas '" & measid & "','" & rteid & "'"
            rte.Open()
            rte.Update(sql)
            eqid = lbleqid.Value
            fuid = lblfuid.Value
            coid = lblcoid.Value
            dgmeasrt.SelectedIndex = -1
            GetRteMeas(rteid)
            GetAllMeas(rteid, eqid, fuid, coid)
            rte.Dispose()

        End If
    End Sub

    Private Sub dgmeasrt_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeasrt.EditCommand
        dgmeasrt.EditItemIndex = e.Item.ItemIndex
        rte.Open()
        lbloldroute.Value = CType(e.Item.FindControl("lblroute"), Label).Text
        rteid = lblrteid.Value
        GetRteMeas(rteid)
        rte.Dispose()
    End Sub

    Private Sub dgmeasrt_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeasrt.CancelCommand
        dgmeasrt.EditItemIndex = -1
        rte.Open()
        rteid = lblrteid.Value
        GetRteMeas(rteid)
        rte.Dispose()
    End Sub

    Private Sub dgmeasrt_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeasrt.UpdateCommand
        Dim nrte As String = CType(e.Item.FindControl("txtrouting"), TextBox).Text
        Dim orte As String = lbloldroute.Value
        Dim rteid As String = lblrteid.Value
        Dim rtechk As Long
        Try
            rtechk = System.Convert.ToInt32(nrte)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1579" , "measroutesaddbot.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        sql = "usp_uproutemeasorder '" & rteid & "','" & nrte & "','" & orte & "'"
        rte.Open()
        rte.Update(sql)
        dgmeasrt.EditItemIndex = -1
        GetRteMeas(rteid)
        rte.Dispose()
    End Sub

    Private Sub dgmeasrt_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeasrt.DeleteCommand
        Dim nrte As String
        Dim measid As String
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

        ElseIf e.Item.ItemType = ListItemType.EditItem Then

        End If
        Try
            measid = CType(e.Item.FindControl("lblmeasid"), Label).Text
        Catch ex As Exception
            measid = CType(e.Item.FindControl("lblmeaside"), Label).Text
        End Try
        Try
            nrte = CType(e.Item.FindControl("txtrouting"), TextBox).Text
        Catch ex As Exception
            nrte = CType(e.Item.FindControl("lblroute"), Label).Text
        End Try
        Dim rtechk As Long
        Try
            rtechk = System.Convert.ToInt32(nrte)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1580" , "measroutesaddbot.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim rteid As String = lblrteid.Value
        sql = "usp_delroutemeas '" & rteid & "','" & measid & "','" & nrte & "'"
        rte.Open()
        rte.Update(sql)
        dgmeasrt.EditItemIndex = -1
        GetRteMeas(rteid)
        rte.Dispose()
    End Sub
	

	

	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgmeas.Columns(0).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","0")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(1).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","1")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(2).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","2")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(3).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","3")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(4).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","4")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(5).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","5")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(6).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","6")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(7).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","7")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(8).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","8")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(10).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","10")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(12).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","12")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(13).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","13")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(14).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","14")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(15).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","15")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(16).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","16")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(17).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","17")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(18).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","18")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(19).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","19")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(20).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","20")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(21).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","21")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(24).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeas","24")
		Catch ex As Exception
		End Try
		Try
			dgmeasrt.Columns(0).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeasrt","0")
		Catch ex As Exception
		End Try
		Try
			dgmeasrt.Columns(2).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeasrt","2")
		Catch ex As Exception
		End Try
		Try
			dgmeasrt.Columns(3).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeasrt","3")
		Catch ex As Exception
		End Try
		Try
			dgmeasrt.Columns(4).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeasrt","4")
		Catch ex As Exception
		End Try
		Try
			dgmeasrt.Columns(5).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeasrt","5")
		Catch ex As Exception
		End Try
		Try
			dgmeasrt.Columns(6).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeasrt","6")
		Catch ex As Exception
		End Try
		Try
			dgmeasrt.Columns(7).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeasrt","7")
		Catch ex As Exception
		End Try
		Try
			dgmeasrt.Columns(8).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeasrt","8")
		Catch ex As Exception
		End Try
		Try
			dgmeasrt.Columns(9).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeasrt","9")
		Catch ex As Exception
		End Try
		Try
			dgmeasrt.Columns(10).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeasrt","10")
		Catch ex As Exception
		End Try
		Try
			dgmeasrt.Columns(11).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeasrt","11")
		Catch ex As Exception
		End Try
		Try
			dgmeasrt.Columns(14).HeaderText = dlabs.GetDGPage("measroutesaddbot.aspx","dgmeasrt","14")
		Catch ex As Exception
		End Try

	End Sub

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang3259.Text = axlabs.GetASPXPage("measroutesaddbot.aspx","lang3259")
		Catch ex As Exception
		End Try
		Try
			lang3260.Text = axlabs.GetASPXPage("measroutesaddbot.aspx","lang3260")
		Catch ex As Exception
		End Try

	End Sub

End Class
