<%@ Page Language="vb" AutoEventWireup="false" Codebehind="measroutesaddbot.aspx.vb" Inherits="lucy_r12.measroutesaddbot" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>measroutesaddbot</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/measroutesaddbotaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 0px" cellSpacing="0" width="1050">
				<tr>
					<td class="label" colSpan="3"><asp:Label id="lang3259" runat="server">Available Measurements</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="3">
						<div id="spdiv" style="OVERFLOW: auto; WIDTH: 1025px; HEIGHT: 100px" onscroll="SetsDivPosition();"><asp:datagrid id="dgmeas" runat="server" ShowFooter="True" CellPadding="0" GridLines="None" AllowPaging="True"
								AllowCustomPaging="True" AutoGenerateColumns="False" CellSpacing="1" BackColor="transparent">
								<FooterStyle BackColor="transparent"></FooterStyle>
								<EditItemStyle Height="15px" BackColor="transparent"></EditItemStyle>
								<AlternatingItemStyle CssClass="ptransrowblue" Height="20px"></AlternatingItemStyle>
								<ItemStyle CssClass="ptransrow" Height="20px"></ItemStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Title">
										<HeaderStyle Width="150px" CssClass="btmmenu plainlabel" Height="20px"></HeaderStyle>
										<ItemTemplate>
											<asp:linkbutton id="Label17" CommandName="Select" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.meastitle") %>'>
											</asp:linkbutton>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label18" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.meastitle") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Function">
										<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label1" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label11" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Component">
										<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label12" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label13" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Measure Type">
										<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=Label4 runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label3" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Measurement">
										<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label2" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.measure") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label14" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.measure") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Characteristic">
										<HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label15" runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.char") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label16" runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.char") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Hi">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label5" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label6" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Lo">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label7" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label8" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Spec">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label9" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label10" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<ItemTemplate>
											<asp:Label id=lblpmtskid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.measid") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="lblpmtskida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.measid") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
									ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
							</asp:datagrid></div>
					</td>
				</tr>
				<tr>
					<td class="label" colSpan="3"><asp:Label id="lang3260" runat="server">Selected Measurements</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="3">
						<div id="spdiv1" style="OVERFLOW: auto; WIDTH: 1025px; HEIGHT: 200px" onscroll="SetsDivPosition1();"><asp:datagrid id="dgmeasrt" runat="server" ShowFooter="True" CellPadding="0" GridLines="None"
								AllowPaging="True" AllowCustomPaging="True" AutoGenerateColumns="False" CellSpacing="1" BackColor="transparent" >
								<FooterStyle BackColor="transparent"></FooterStyle>
								<EditItemStyle Height="15px" BackColor="transparent"></EditItemStyle>
								<AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
								<ItemStyle CssClass="ptransrow"></ItemStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Edit">
										<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:ImageButton id="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
												ToolTip="Edit Record" CommandName="Edit"></asp:ImageButton>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:imagebutton id="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
												ToolTip="Save Changes" CommandName="Update"></asp:imagebutton>
											<asp:imagebutton id="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
												ToolTip="Cancel Changes" CommandName="Cancel"></asp:imagebutton>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="routing desc" HeaderText="Seq#">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=lblroute runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.routeorder") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox id=txtrouting runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.routeorder") %>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Title">
										<HeaderStyle Width="150px" CssClass="btmmenu plainlabel" Height="20px"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label19" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.meastitle") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label20" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.meastitle") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Equipment">
										<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label37" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label39" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Function">
										<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label21" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label22" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Component">
										<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label23" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label24" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Measure Type">
										<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label25" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label26" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Measurement">
										<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label27" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.measure") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label28" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.measure") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Characteristic">
										<HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label29" runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.char") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label30" runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.char") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Hi">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label31" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label32" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Lo">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label33" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label34" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Spec">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label35" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label36" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<ItemTemplate>
											<asp:Label id="lblmeasid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.measid") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="lblmeaside" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.measid") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<ItemTemplate>
											<asp:Label id="Label38" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.routeid") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="lblrteide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.routeid") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Remove" ItemStyle-HorizontalAlign="Center">
										<HeaderStyle Width="64px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:ImageButton id="Imagebutton16" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
												CommandName="Delete"></asp:ImageButton>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
												CommandName="Delete"></asp:ImageButton>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
									ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
							</asp:datagrid></div>
					</td>
				</tr>
			</table>
			<input id="lblrteid" type="hidden" runat="server"> <input id="lbleqid" type="hidden" runat="server">
			<input id="lblfuid" type="hidden" runat="server"> <input id="lblcoid" type="hidden" runat="server">
			<input id="lbloldroute" type="hidden" runat="server"> <input type="hidden" id="spdivy" runat="server">
			<input type="hidden" id="spdivy1" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
