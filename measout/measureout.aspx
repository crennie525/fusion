<%@ Page Language="vb" AutoEventWireup="false" Codebehind="measureout.aspx.vb" Inherits="lucy_r12.measureout" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>measureout</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/measureoutaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg"  onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 2px; POSITION: absolute; TOP: 2px" width="310" cellpadding="1" cellspacing="1">
				<TBODY>
					<tr>
						<td colspan="2">
							<table>
								<tr>
									<td class="label" width="110"><asp:Label id="lang3278" runat="server">Measurement Title</asp:Label></td>
									<td>
										<asp:TextBox id="txtmtitle" runat="server" Width="150px" CssClass="plainlabel"></asp:TextBox></td>
								</tr>
							</table>
						</td>
					<tr>
						<td vAlign="top">
							<table>
								<tr>
									<td class="label" colspan="2" height="20"><asp:Label id="lang3279" runat="server">Measurement Type</asp:Label></td>
								</tr>
								<tr>
									<td><asp:dropdownlist id="ddtypes" runat="server" AutoPostBack="True" Width="150px" CssClass="plainlabel"></asp:dropdownlist></td>
									<td><IMG onclick="getAddType();" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
											width="20" id="iaddtype" runat="server"></td>
								</tr>
								<tr>
									<td class="label" colspan="2" height="20"><asp:Label id="lang3280" runat="server">Measurement</asp:Label></td>
								</tr>
								<tr>
									<td><asp:dropdownlist id="ddmeas" runat="server" AutoPostBack="True" Width="150px"></asp:dropdownlist></td>
									<td><IMG onclick="getAddMeas();" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
											width="20" id="iaddmeas" runat="server"></td>
								</tr>
							</table>
						</td>
						<td vAlign="top">
							<table>
								<tr>
									<td vAlign="middle"><asp:radiobuttonlist id="rblchar" runat="server" AutoPostBack="True" CssClass="label" RepeatLayout="Flow">
											<asp:ListItem Value="spec">Specific</asp:ListItem>
											<asp:ListItem Value="range" Selected="True">Range</asp:ListItem>
											<asp:ListItem Value="max">Not to Exceed</asp:ListItem>
											<asp:ListItem Value="rec">Record Only</asp:ListItem>
										</asp:radiobuttonlist></td>
								</tr>
							</table>
						</td>
					<tr id="trspec" runat="server">
						<td align="center" colSpan="2">
							<table>
								<tr>
									<td class="disablelabel" id="tdspec" runat="server"><asp:Label id="lang3281" runat="server">Hi Limit</asp:Label></td>
									<td><asp:textbox id="t1" runat="server" MaxLength="50" CssClass="plainlabel"></asp:textbox></td>
									<td class="plainlabel" id="tdstype" runat="server"></td>
								</tr>
								<tr>
									<td class="disablelabel" id="tdspec2" runat="server"><asp:Label id="lang3282" runat="server">Lo Limit</asp:Label></td>
									<td><asp:textbox id="t2" runat="server" MaxLength="50" CssClass="plainlabel"></asp:textbox></td>
									<td class="plainlabel" id="tdstype2" runat="server"></td>
								</tr>
								<tr>
									<td class="disablelabel" id="tdspec3" runat="server"><asp:Label id="lang3283" runat="server">Specific</asp:Label></td>
									<td><asp:textbox id="t3" runat="server" MaxLength="50" CssClass="plainlabel"></asp:textbox></td>
									<td class="plainlabel" id="tdstype3" runat="server"></td>
									<td align="right"><IMG id="btnadd" onclick="addToTask();" alt="" src="../images/appbuttons/minibuttons/addmod.gif"
											runat="server">&nbsp;<IMG id="btnreturn" onclick="handleexit();" alt="" src="../images/appbuttons/minibuttons/refreshit.gif"
											runat="server">
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</TBODY>
			</table>
			<input id="lbltaskid" type="hidden" runat="server" NAME="lbltaskid"> <input id="lblcid" type="hidden" runat="server" NAME="lblcid">
			<input id="lblrettype" type="hidden" runat="server" NAME="lblrettype"><input id="lbltypeid" type="hidden" runat="server" NAME="lbltypeid">
			<input id="lblmeasid" type="hidden" runat="server" NAME="lblmeasid"><input id="lblchr" type="hidden" runat="server" NAME="lblchr">
			<input id="lblout" type="hidden" runat="server" NAME="lblout"><input id="lbllog" type="hidden" name="lbllog" runat="server">
			<input id="lbltyp" type="hidden" runat="server" NAME="lbltyp"> <input type="hidden" id="lblwonum" runat="server" NAME="lblwonum">
			<input type="hidden" id="lbljpid" runat="server" NAME="lbljpid"> <input type="hidden" id="lblro" runat="server" NAME="lblro">
			<input type="hidden" id="lblrev" runat="server" NAME="lblrev"> <input type="hidden" id="lbldid" runat="server">
			<input type="hidden" id="lblsid" runat="server"> <input type="hidden" id="lblclid" runat="server">
			<input type="hidden" id="lbllocid" runat="server"> <input type="hidden" id="lbleqid" runat="server">
			<input type="hidden" id="lblfuid" runat="server"> <input type="hidden" id="lblcoid" runat="server">
			<input type="hidden" id="lblcurrtab" runat="server">
			
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
