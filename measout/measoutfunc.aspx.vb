

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class measoutfunc
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, eqid, Login As String
    Dim dr As SqlDataReader
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim rep As New Utilities

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents divfu As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                rep.Open()
                GetFunctions()
                rep.Dispose()
            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Sub GetFunctions()
        eqid = lbleqid.Value
        Dim fi, fu As String
        Dim sb As New StringBuilder

        sql = "select func_id, func from functions where eqid = '" & eqid & "' order by routing"
        dr = rep.GetRdrData(sql)
        sb.Append("<table>")
        While dr.Read
            fi = dr.Item("func_id").ToString
            fu = dr.Item("func").ToString
            sb.Append("<tr><td>")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getdfu('" & fi & "','" & fu & "','down')"">")
            sb.Append(fu)
            sb.Append("</td></tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        divfu.InnerHtml = sb.ToString

    End Sub
End Class
