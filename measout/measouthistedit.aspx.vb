

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class measouthistedit
    Inherits System.Web.UI.Page
	Protected WithEvents lang3233 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3232 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3231 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim Sort As String
    Dim dr As SqlDataReader
    Dim meas As New Utilities
    Protected WithEvents lblmeasid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilter As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtfrom As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtto As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim measid, Login As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgmeas As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            measid = Request.QueryString("measid").ToString '"2" '
            lblmeasid.Value = measid
            meas.Open()
            txtpg.Value = "1"
            PageNumber = 1
            LoadHist(PageNumber)
            meas.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                meas.Open()
                GetNext()
                meas.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                meas.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                LoadHist(PageNumber)
                meas.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                meas.Open()
                GetPrev()
                meas.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                meas.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                LoadHist(PageNumber)
                meas.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub LoadHist(ByVal PageNumber As Integer)
        measid = lblmeasid.Value
        Filter = "measid = ''" & measid & "''"
        FilterCnt = "measid = '" & measid & "'"

        Dim dfrom, dto As String
        dfrom = txtfrom.Text
        dto = txtto.Text

        If dfrom <> "" And dto <> "" Then
            Filter += "and (measdate >= ''" & dfrom & "'' and measdate <= ''" & dto & "'') "
            FilterCnt += "and (measdate >= '" & dfrom & "' and measdate <= '" & dto & "') "
        ElseIf dfrom <> "" And dto = "" Then
            Filter += "and (measdate >= ''" & dfrom & "'') "
            FilterCnt += "and (measdate >= '" & dfrom & "') " 
        ElseIf dto <> "" And dfrom = "" Then
            Filter += "and (measdate <= ''" & dto & "'') "
            FilterCnt += "and (measdate <= '" & dto & "') "
        End If

        lblfilter.Value = FilterCnt

        sql = "select Count(*) from outmeasuretrack " _
        + "where " & FilterCnt
        Dim dc As Integer = meas.Scalar(sql)
        dgmeas.VirtualItemCount = dc

        Tables = "outmeasuretrack"
        PK = "mtid"
        PageSize = "10"
        Fields = "*"
        Sort = "measdate desc"

        dr = meas.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        dgmeas.DataSource = dr
        dgmeas.DataBind()
        txtpg.Value = PageNumber
        txtpgcnt.Value = dgmeas.PageCount
        lblpg.Text = "Page " & PageNumber & " of " & dgmeas.PageCount
        dr.Close()

    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            LoadHist(PageNumber)
        Catch ex As Exception
            meas.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1570" , "measouthistedit.aspx.vb")
 
            meas.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            LoadHist(PageNumber)
        Catch ex As Exception
            meas.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1571" , "measouthistedit.aspx.vb")
 
            meas.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub dgmeas_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.CancelCommand
        dgmeas.EditItemIndex = -1
        meas.Open()
        PageNumber = txtpg.Value
        LoadHist(PageNumber)
        meas.Dispose()
    End Sub

    Private Sub dgmeas_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.EditCommand
        PageNumber = txtpg.Value
        dgmeas.EditItemIndex = e.Item.ItemIndex
        meas.Open()
        LoadHist(PageNumber)
        meas.Dispose()
    End Sub

    Private Sub dgmeas_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.UpdateCommand
        Dim mstr As String = CType(e.Item.FindControl("txtmeas"), TextBox).Text
        Dim mtid As String = CType(e.Item.FindControl("lblmeaside"), Label).Text
        Dim txtchk As Long
        Try
            txtchk = System.Convert.ToDecimal(mstr)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1572" , "measouthistedit.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If mstr <> "" Then
            sql = "update outmeasuretrack set measurement = '" & mstr & "' where mtid = '" & mtid & "'"
            meas.Open()
            meas.Update(sql)
            dgmeas.EditItemIndex = -1
            PageNumber = txtpg.Value
            LoadHist(PageNumber)
            meas.Dispose()
        End If
    End Sub

    Private Sub dgmeas_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.DeleteCommand
        Dim mtid As String
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.EditItem Then
            mtid = CType(e.Item.FindControl("lblmeasidi"), Label).Text
        ElseIf e.Item.ItemType = ListItemType.EditItem Then
            mtid = CType(e.Item.FindControl("lblmeaside"), Label).Text
        End If
        sql = "delete from outmeasuretrack where mtid = '" & mtid & "'"
        meas.Open()
        meas.Update(sql)
        dgmeas.EditItemIndex = -1
        PageNumber = txtpg.Value
        LoadHist(PageNumber)
        meas.Dispose()

    End Sub

    Private Sub dgmeas_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgmeas.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                 e.Item.ItemType <> ListItemType.Footer Then
            Dim deleteButton As ImageButton = CType(e.Item.FindControl("imgdel"), ImageButton)
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete this Measurement Entry')"

        End If
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        PageNumber = 1

        meas.Open()
        LoadHist(PageNumber)
        meas.Dispose()
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgmeas.Columns(0).HeaderText = dlabs.GetDGPage("measouthistedit.aspx", "dgmeas", "0")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(1).HeaderText = dlabs.GetDGPage("measouthistedit.aspx", "dgmeas", "1")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(2).HeaderText = dlabs.GetDGPage("measouthistedit.aspx", "dgmeas", "2")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(4).HeaderText = dlabs.GetDGPage("measouthistedit.aspx", "dgmeas", "4")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3231.Text = axlabs.GetASPXPage("measouthistedit.aspx", "lang3231")
        Catch ex As Exception
        End Try
        Try
            lang3232.Text = axlabs.GetASPXPage("measouthistedit.aspx", "lang3232")
        Catch ex As Exception
        End Try
        Try
            lang3233.Text = axlabs.GetASPXPage("measouthistedit.aspx", "lang3233")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
