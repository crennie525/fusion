

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class measureoutedit
    Inherits System.Web.UI.Page
	Protected WithEvents lang3289 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3288 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3287 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3286 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3285 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3284 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tm As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim taskid, eqid, func, comp, cid, type, login, typ, wonum, jpid, ro, rev, measid As String
    Dim sid, did, clid, locid, fuid, comid, coid, uid As String
    Protected WithEvents lblstart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblparmeasid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtmtitle As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddtypes As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddmeas As System.Web.UI.WebControls.DropDownList
    Protected WithEvents rblchar As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents t1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents t2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents t3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents iaddtype As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iaddmeas As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents trspec As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdspec As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdstype As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdspec2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdstype2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdspec3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdstype3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrettype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltypeid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmeasid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblout As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldtype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldmeas As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldhi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldlo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldspec As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldchar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldtitle As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            If lbllog.Value <> "no" Then
                'Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                lblro.Value = ro
                If ro = "1" Then
                    iaddtype.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                    iaddmeas.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                    iaddtype.Attributes.Add("onclick", "")
                    iaddmeas.Attributes.Add("onclick", "")
                    btnadd.Attributes.Add("src", "../images/appbuttons/bgbuttons/badddis.gif")
                    btnadd.Attributes.Add("onclick", "")
                Else
                    'btnadd.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
                    'btnadd.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
                End If
                Try
                    measid = Request.QueryString("measid").ToString
                    lblparmeasid.Value = measid
                Catch ex As Exception

                End Try

                cid = "0" ' HttpContext.Current.Session("comp").ToString
                lblcid.Value = cid
                tm.Open()
                LoadTypes(cid)
                GetMeas(measid)
                tm.Dispose()
                'ClearInput()
                'rblchar.Enabled = False
                'ddmeas.Enabled = False
                'lbltypeid.Value = "0"
                'lblmeasid.Value = "0"
                'lblchr.Value = "range"

            End If
        Else
            If lbllog.Value <> "no" Then
                If Request.Form("lblrettype") = "type" Then
                    lblrettype.Value = ""
                    cid = lblcid.Value
                    tm.Open()
                    LoadTypes(cid)
                    tm.Dispose()
                ElseIf Request.Form("lblrettype") = "meas" Then
                    lblrettype.Value = ""
                    type = lbltypeid.Value
                    tm.Open()
                    LoadDDMeas(type)
                    tm.Dispose()
                ElseIf Request.Form("lblrettype") = "add" Then
                    lblrettype.Value = ""
                    tm.Open()
                    AddMeas()
                    tm.Dispose()
                End If

            End If
        End If

        'btnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'btnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Sub GetMeas(ByVal measid As String)
        sql = "select * from outmeasure where measid = '" & measid & "'"
        Dim mtype, meas, chari As String
        dr = tm.GetRdrData(sql)
        While dr.Read
            txtmtitle.Text = dr.Item("meastitle").ToString
            lbloldtitle.Value = dr.Item("meastitle").ToString

            t1.Text = dr.Item("hi").ToString
            lbloldhi.Value = dr.Item("hi").ToString

            t2.Text = dr.Item("lo").ToString
            lbloldlo.Value = dr.Item("lo").ToString

            t3.Text = dr.Item("spec").ToString
            lbloldspec.Value = dr.Item("spec").ToString

            mtype = dr.Item("typeid").ToString
            meas = dr.Item("parmeasid").ToString
            chari = dr.Item("char").ToString

        End While
        dr.Close()
        Dim strmeas As String
        Select Case chari
            Case "Range"
                strmeas = "Range"
                rblchar.SelectedValue = "range"
                lbloldchar.Value = "range"
            Case "Specific"
                strmeas = "Specific"
                rblchar.SelectedValue = "spec"
                lbloldchar.Value = "spec"
            Case "Not to Exceed"
                strmeas = "Not to Exceed"
                rblchar.SelectedValue = "max"
                lbloldchar.Value = "max"
            Case "Record Only"
                strmeas = "Record Only"
                rblchar.SelectedValue = "rec"
                lbloldchar.Value = "rec"
        End Select
        lbloldtype.Value = mtype
        lbltypeid.Value = mtype
        Try
            ddtypes.SelectedValue = mtype
        Catch ex As Exception

        End Try
        lbloldmeas.Value = meas
        lblmeasid.Value = meas
        Try
            lblstart.Value = "1"
            LoadDDMeas(mtype)
            lblstart.Value = "0"
            ddmeas.SelectedValue = meas
            StartInput(strmeas)
        Catch ex As Exception

        End Try




    End Sub
    Private Sub LoadTypes(ByVal cid As String)
        'ClearInput()
        rblchar.Enabled = False
        ddmeas.Enabled = False
        lbltypeid.Value = "0"
        lblmeasid.Value = "0"
        sql = "select * from pmMeasureTypes where compid = '" & cid & "'"
        dr = tm.GetRdrData(sql)
        ddtypes.DataSource = dr
        ddtypes.DataValueField = "typeid"
        ddtypes.DataTextField = "type"
        ddtypes.DataBind()
        dr.Close()
        ddtypes.Items.Insert(0, "Select Type")
    End Sub
    Private Sub LoadDDMeas(ByVal type As String)
        'ClearInput()
        rblchar.Enabled = False
        ddmeas.Enabled = True
        If lblstart.Value <> "1" Then
            lblmeasid.Value = "0"
        End If


        sql = "select * from pmMeasurements where typeid = '" & type & "'"
        dr = tm.GetRdrData(sql)
        ddmeas.DataSource = dr
        ddmeas.DataValueField = "measid"
        ddmeas.DataTextField = "measurement"
        ddmeas.DataBind()
        dr.Close()
        ddmeas.Items.Insert(0, "Select Measurement")
    End Sub
    Private Sub SetInput(ByVal meas As String)
        Dim chr As String = rblchar.SelectedValue.ToString
        Select Case chr
            Case "spec"
                lblchr.Value = "spec"
                t1.Text = ""
                t1.Enabled = False
                tdstype.InnerHtml = ""
                tdspec.Attributes.Add("class", "disablelabel")
                t2.Text = ""
                t2.Enabled = False
                tdstype2.InnerHtml = ""
                tdspec2.Attributes.Add("class", "disablelabel")
                t3.Text = ""
                t3.Enabled = True
                tdstype3.InnerHtml = meas
                tdspec3.Attributes.Add("class", "bluelabellt")
            Case "range"
                lblchr.Value = "range"
                t1.Text = ""
                t1.Enabled = True
                tdstype.InnerHtml = meas
                tdspec.Attributes.Add("class", "bluelabellt")
                t2.Text = ""
                t2.Enabled = True
                tdstype2.InnerHtml = meas
                tdspec2.Attributes.Add("class", "bluelabellt")
                t3.Text = ""
                t3.Enabled = True
                tdstype3.InnerHtml = ""
                tdspec3.Attributes.Add("class", "bluelabellt")
            Case "max"
                lblchr.Value = "max"
                t1.Text = ""
                t1.Enabled = True
                tdstype.InnerHtml = meas
                tdspec.Attributes.Add("class", "bluelabellt")
                t2.Text = ""
                t2.Enabled = False
                tdstype2.InnerHtml = ""
                tdspec2.Attributes.Add("class", "disablelabel")
                t3.Text = ""
                t3.Enabled = True
                tdstype3.InnerHtml = ""
                tdspec3.Attributes.Add("class", "bluelabellt")
            Case "rec"
                lblchr.Value = "rec"
                ClearInput()
                Dim strMessage As String =  tmod.getmsg("cdstr1585" , "measureoutedit.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Select

    End Sub
    Private Sub StartInput(ByVal meas As String)
        Dim chr As String = rblchar.SelectedValue.ToString
        Select Case chr
            Case "spec"
                lblchr.Value = "spec"
                t1.Text = ""
                t1.Enabled = False
                tdstype.InnerHtml = ""
                tdspec.Attributes.Add("class", "disablelabel")
                t2.Text = ""
                t2.Enabled = False
                tdstype2.InnerHtml = ""
                tdspec2.Attributes.Add("class", "disablelabel")
                't3.Text = ""
                t3.Enabled = True
                tdstype3.InnerHtml = meas
                tdspec3.Attributes.Add("class", "bluelabellt")
            Case "range"
                lblchr.Value = "range"
                't1.Text = ""
                t1.Enabled = True
                tdstype.InnerHtml = meas
                tdspec.Attributes.Add("class", "bluelabellt")
                't2.Text = ""
                t2.Enabled = True
                tdstype2.InnerHtml = meas
                tdspec2.Attributes.Add("class", "bluelabellt")
                't3.Text = ""
                t3.Enabled = True
                tdstype3.InnerHtml = ""
                tdspec3.Attributes.Add("class", "bluelabellt")
            Case "max"
                lblchr.Value = "max"
                't1.Text = ""
                t1.Enabled = True
                tdstype.InnerHtml = meas
                tdspec.Attributes.Add("class", "bluelabellt")
                t2.Text = ""
                t2.Enabled = False
                tdstype2.InnerHtml = ""
                tdspec2.Attributes.Add("class", "disablelabel")
                't3.Text = ""
                t3.Enabled = True
                tdstype3.InnerHtml = ""
                tdspec3.Attributes.Add("class", "bluelabellt")
            Case "rec"
                lblchr.Value = "rec"
                ClearInput()
                Dim strMessage As String =  tmod.getmsg("cdstr1586" , "measureoutedit.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Select

    End Sub
    Private Sub rblchar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblchar.SelectedIndexChanged
        Dim measi As String = ddmeas.SelectedValue.ToString
        lblmeasid.Value = measi
        Dim meas As String = ddmeas.SelectedItem.ToString
        SetInput(meas)
    End Sub
    Private Sub ClearInput()
        t1.Text = ""
        t1.Enabled = False
        tdstype.InnerHtml = ""
        tdspec.Attributes.Add("class", "disablelabel")
        t2.Text = ""
        t2.Enabled = False
        tdstype2.InnerHtml = ""
        tdspec2.Attributes.Add("class", "disablelabel")
        t3.Text = ""
        t3.Enabled = False
        tdstype3.InnerHtml = ""
        tdspec3.Attributes.Add("class", "disablelabel")
    End Sub

    Private Sub ddtypes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddtypes.SelectedIndexChanged
        If ddtypes.SelectedIndex <> 0 Then
            Dim type As String = ddtypes.SelectedValue.ToString
            lbltypeid.Value = type
            tm.Open()
            LoadDDMeas(type)
            tm.Dispose()
        End If
    End Sub

    Private Sub ddmeas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddmeas.SelectedIndexChanged
        If ddmeas.SelectedIndex <> 0 Then
            Dim measi As String = ddmeas.SelectedValue.ToString
            lblmeasid.Value = measi
            Dim meas As String = ddmeas.SelectedItem.ToString
            rblchar.Enabled = True
            SetInput(meas)
        End If
    End Sub
    Private Sub AddMeas()
        Dim chr As String = rblchar.SelectedValue.ToString
        Dim meas As String = ddmeas.SelectedItem.ToString
        Dim measi As String = ddmeas.SelectedValue
        Dim type As String = ddtypes.SelectedItem.ToString
        Dim typei As String = ddtypes.SelectedValue
        Dim hi, lo, spec, max, rec, out, charout, mtitle As String
        mtitle = txtmtitle.Text
        If Len(mtitle) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1587" , "measureoutedit.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        measid = lblparmeasid.Value
        Dim mcnt As Integer
        sql = "select count(*) from outmeasure where meastitle = '" & mtitle & "' and measid <> '" & measid & "'"
        mcnt = tm.Scalar(sql)
        If mcnt <> 0 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1588" , "measureoutedit.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If

        Select Case chr
            Case "range"
                hi = t1.Text
                lo = t2.Text
                spec = t3.Text '"NA"
                rec = "Yes"
                out = "(___)Hi:" & hi & meas & "-Lo:" & lo & meas & "-Spec:" & spec & meas & ";"
                charout = "Range"
            Case "spec"
                hi = "NA"
                lo = "NA"
                spec = t3.Text
                rec = "Yes"
                out = "(___)Spec:" & spec & meas & ";"
                charout = "Specific"
            Case "max"
                hi = t1.Text
                lo = "NA"
                spec = t3.Text '"NA"
                rec = "Yes"
                out = "(___)Max:" & hi & meas & ";"
                charout = "Not to Exceed"
            Case "rec"
                hi = "NA"
                lo = "NA"
                spec = "NA"
                rec = "Yes"
                out = "(___)" & meas & ";"
                charout = "Record Only"

        End Select
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        coid = lblcoid.Value


        'sql = "usp_addoutmeas '" & sid & "', '" & did & "','" & did & ",'" & clid & "','" & eqid & "', " _
        '+ "'" & fuid & "','" & coid & "','" & type & "', '" & meas & "', '" & hi & "', '" & lo & "', " _
        '+ "'" & spec & "', '" & out & "','" & uid & "','" & typei & "','" & measi & "'"

        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_editoutmeas @measid, " _
        + "@type, @meas, " _
        + "@hi, @lo, @spec, " _
        + "@user, @out, @char, @typei, @measi, @mtitle"

        Dim param = New SqlParameter("@measid", SqlDbType.Int)
        If measid = "" Then
            param.Value = System.DBNull.Value
        Else
            param.Value = measid
        End If
        cmd.Parameters.Add(param)


        Dim param06 = New SqlParameter("@type", SqlDbType.VarChar)
        If type = "" Then
            param06.Value = System.DBNull.Value
        Else
            param06.Value = type
        End If
        cmd.Parameters.Add(param06)
        Dim param07 = New SqlParameter("@meas", SqlDbType.VarChar)
        If meas = "" Then
            param07.Value = System.DBNull.Value
        Else
            param07.Value = meas
        End If
        cmd.Parameters.Add(param07)
        Dim param08 = New SqlParameter("@hi", SqlDbType.VarChar)
        If hi = "" Then
            param08.Value = System.DBNull.Value
        Else
            param08.Value = hi
        End If
        cmd.Parameters.Add(param08)
        Dim param09 = New SqlParameter("@lo", SqlDbType.VarChar)
        If lo = "" Then
            param09.Value = System.DBNull.Value
        Else
            param09.Value = lo
        End If
        cmd.Parameters.Add(param09)
        Dim param10 = New SqlParameter("@spec", SqlDbType.VarChar)
        If spec = "" Then
            param10.Value = System.DBNull.Value
        Else
            param10.Value = spec
        End If
        cmd.Parameters.Add(param10)
        Dim param11 = New SqlParameter("@user", SqlDbType.VarChar)
        If uid = "" Then
            param11.Value = System.DBNull.Value
        Else
            param11.Value = uid
        End If
        cmd.Parameters.Add(param11)
        Dim param12 = New SqlParameter("@out", SqlDbType.VarChar)
        If out = "" Then
            param12.Value = System.DBNull.Value
        Else
            param12.Value = out
        End If
        cmd.Parameters.Add(param12)
        Dim param13 = New SqlParameter("@char", SqlDbType.VarChar)
        If charout = "" Then
            param13.Value = System.DBNull.Value
        Else
            param13.Value = charout
        End If
        cmd.Parameters.Add(param13)
        Dim param14 = New SqlParameter("@typei", SqlDbType.VarChar)
        If typei = "" Then
            param14.Value = System.DBNull.Value
        Else
            param14.Value = typei
        End If
        cmd.Parameters.Add(param14)
        Dim param15 = New SqlParameter("@measi", SqlDbType.VarChar)
        If measi = "" Then
            param15.Value = System.DBNull.Value
        Else
            param15.Value = measi
        End If
        cmd.Parameters.Add(param15)
        Dim param16 = New SqlParameter("@mtitle", SqlDbType.VarChar)
        If mtitle = "" Then
            param16.Value = System.DBNull.Value
        Else
            param16.Value = mtitle
        End If
        cmd.Parameters.Add(param16)


        Try
            tm.UpdateHack(cmd)
        Catch ex As Exception
            Dim strMessage As String = ex.Message.ToString
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        lbltypeid.Value = "0"
        lblmeasid.Value = "0"
        Try
            ddtypes.SelectedIndex = 0
        Catch ex As Exception

        End Try

        ddmeas.Enabled = False
        rblchar.Enabled = False
        ClearInput()
        lblrettype.Value = "added"
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3284.Text = axlabs.GetASPXPage("measureoutedit.aspx", "lang3284")
        Catch ex As Exception
        End Try
        Try
            lang3285.Text = axlabs.GetASPXPage("measureoutedit.aspx", "lang3285")
        Catch ex As Exception
        End Try
        Try
            lang3286.Text = axlabs.GetASPXPage("measureoutedit.aspx", "lang3286")
        Catch ex As Exception
        End Try
        Try
            lang3287.Text = axlabs.GetASPXPage("measureoutedit.aspx", "lang3287")
        Catch ex As Exception
        End Try
        Try
            lang3288.Text = axlabs.GetASPXPage("measureoutedit.aspx", "lang3288")
        Catch ex As Exception
        End Try
        Try
            lang3289.Text = axlabs.GetASPXPage("measureoutedit.aspx", "lang3289")
        Catch ex As Exception
        End Try

    End Sub

End Class
