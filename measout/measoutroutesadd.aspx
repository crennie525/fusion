<%@ Page Language="vb" AutoEventWireup="false" Codebehind="measoutroutesadd.aspx.vb" Inherits="lucy_r12.measoutroutesadd" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>measoutroutesadd</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/reportnav.js"></script>
		<script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/measoutroutesaddaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="checkit();checkpage();">
		<form id="Form2" method="post" runat="server">
			<table style="LEFT: 4px; POSITION: absolute; TOP: 2px" cellSpacing="0" cellPadding="0"
				width="1050" id="scrollmenu">
				<tr>
					<td colSpan="6">
						<table cellSpacing="0" cellPadding="0" width="1025">
							<tr>
								<td width="230" rowSpan="2"><IMG src="../menu/mimages/fusionsm.gif"></td>
								<td vAlign="top" width="420"><IMG src="../images/appheaders/measrt.gif"></td>
								<td vAlign="top" align="right" width="375"><IMG id="btnreturn" onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
										runat="server"></td>
							</tr>
							<tr>
								<td vAlign="middle" align="center" colSpan="2">
									<table>
										<tr>
											<td class="label" width="172" height="24"><asp:Label id="lang3236" runat="server">Current Measurement Route:</asp:Label></td>
											<td class="labellt" id="tdcomp" colSpan="3" height="24" runat="server"></td>
											<td></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="6">
						<table width="1025">
							<tr>
								<td class="plainlabelred" id="tdeq" colSpan="2" height="30" runat="server"><asp:Label id="lang3237" runat="server">No Equipment Record Selected</asp:Label></td>
								<td class="plainlabelred" id="tdfu" colSpan="2" height="30" runat="server"><asp:Label id="lang3238" runat="server">No Function Record Selected</asp:Label></td>
								<td class="plainlabelred" id="tdco" colSpan="2" height="30" runat="server"><asp:Label id="lang3239" runat="server">No Component Record Selected</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="trhead" runat="server">
					<td colSpan="6">
						<table cellSpacing="0" cellPadding="0">
							<tr>
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/meashdr.gif"></td>
								<td class="thdrsingrt label" align="left" width="714"><asp:Label id="lang3240" runat="server">Measurement Location Details</asp:Label></td>
								<td>&nbsp;</td>
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/meashdr.gif"></td>
								<td class="thdrsingrt label" align="left" width="300"><asp:Label id="lang3241" runat="server">Measurement Routes (Reference)</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="trmain" runat="server">
					<td vAlign="top">
						<table width="244">
							<tr>
								<td class="btmmenu plainlabel" colSpan="2"><asp:Label id="lang3242" runat="server">Search Equipment</asp:Label></td>
							</tr>
							<tr>
								<td class="label" width="84"><asp:Label id="lang3243" runat="server">Equipment#</asp:Label></td>
								<td><asp:textbox id="txteqnum" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang3244" runat="server">Description</asp:Label></td>
								<td><asp:textbox id="txteqdesc" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label">SPL</td>
								<td><asp:textbox id="txtspl" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label">OEM</td>
								<td><asp:textbox id="txtoem" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang3245" runat="server">Model</asp:Label></td>
								<td><asp:textbox id="txtmodel" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang3246" runat="server">Serial#</asp:Label></td>
								<td><asp:textbox id="txtserial" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang3247" runat="server">Asset Class</asp:Label></td>
								<td><asp:dropdownlist id="ddac" runat="server" CssClass="plainlabel"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td align="right" colSpan="2"><IMG onclick="srcheq();" src="../images/appbuttons/minibuttons/srchsm.gif">
									<IMG onclick="goback();" src="../images/appbuttons/minibuttons/refreshit.gif">
								</td>
							</tr>
						</table>
					</td>
					<td vAlign="top">
						<table width="270">
							<tr>
								<td class="btmmenu plainlabel"><asp:Label id="lang3248" runat="server">Equipment Records</asp:Label></td>
							</tr>
							<tr>
								<td>
									<div id="diveq" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; OVERFLOW: auto; BORDER-LEFT: black 1px solid; WIDTH: 270px; BORDER-BOTTOM: black 1px solid; HEIGHT: 168px"
										runat="server"></div>
								</td>
							</tr>
							<tr>
								<td align="center">
									<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
										cellSpacing="0" cellPadding="0">
										<tr>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="190"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
													runat="server"></td>
											<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
													runat="server"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td width="5"></td>
					<td vAlign="top">
						<table width="200">
							<tr>
								<td class="btmmenu plainlabel"><asp:Label id="lang3249" runat="server">Equipment Functions</asp:Label></td>
							</tr>
							<tr>
								<td><iframe id="iffunc" style="BACKGROUND-COLOR: transparent" src="measoutfunc.aspx?eqid=0"
										frameBorder="no" width="200" scrolling="no" height="88" allowTransparency runat="server"></iframe>
								</td>
							</tr>
							<tr>
								<td class="btmmenu plainlabel"><asp:Label id="lang3250" runat="server">Function Components</asp:Label></td>
							</tr>
							<tr>
								<td><iframe id="ifcomp" style="BACKGROUND-COLOR: transparent" src="measoutcomp.aspx?eqid=0&amp;fuid=0"
										frameBorder="no" width="200" scrolling="no" height="88" allowTransparency runat="server"></iframe>
								</td>
							</tr>
						</table>
					</td>
					<td width="5">&nbsp;</td>
					<td vAlign="top">
						<table width="326">
							<tr>
								<td><iframe id="ifrte" style="BACKGROUND-COLOR: transparent" src="measoutroutesmini.aspx" frameBorder="no"
										width="326" scrolling="no" height="200" allowTransparency runat="server"></iframe>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" colSpan="6"><iframe id="ifbot" style="BACKGROUND-COLOR: transparent" src="measroutesaddbot.aspx" frameBorder="no"
							width="1050" scrolling="no" height="470" allowTransparency runat="server"></iframe>
					</td>
				</tr>
			</table>
			<input id="lblrteid" type="hidden" runat="server"> <input id="lblsid" type="hidden" runat="server">
			<input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server"><input id="txtpg" type="hidden" name="txtpg" runat="server">
			<input id="lbleqdesc" type="hidden" name="lbleqdesc" runat="server"> <input id="lblspl" type="hidden" name="lblspl" runat="server">
			<input id="lblmodel" type="hidden" name="lblmodel" runat="server"> <input id="lbloem" type="hidden" name="lbloem" runat="server">
			<input id="lblserial" type="hidden" name="lblserial" runat="server"> <input id="lblac" type="hidden" name="lblac" runat="server">
			<input id="lbleqnumsrch" type="hidden" name="lbleqnumsrch" runat="server"> <input id="lblrepcnt" type="hidden" name="lblrepcnt" runat="server">
			<input id="lblmeascnt" type="hidden" name="lblmeascnt" runat="server"> <input id="lblsubmit" type="hidden" runat="server">
			<input id="lbleqnum" type="hidden" runat="server"> <input id="lbldid" type="hidden" runat="server">
			<input id="lblclid" type="hidden" runat="server"> <input id="lbleqid" type="hidden" runat="server">
			<input id="lblfuid" type="hidden" runat="server"> <input id="lblcoid" type="hidden" runat="server">
			<input type="hidden" id="lbllog" runat="server" NAME="lbllog"> <input id="xCoord" type="hidden" name="xCoord" runat="server">
			<input id="yCoord" type="hidden" name="yCoord" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
