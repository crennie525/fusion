

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class printoutmeas
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim meas As New Utilities
    Dim measid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            measid = Request.QueryString("measid").ToString '"2" '
            meas.Open()
            PrintMeas(measid)
            meas.Dispose()

        End If
    End Sub
    Private Sub PrintMeas(ByVal measid As String)
        sql = "select o.meastitle, o.measure, o.[type], o.hi, o.lo, o.spec, o.[char], o.measout, " _
        + "o.createdby, o.createdate, o.modifiedby, o.modifieddate, t.measurement, t.measdate, t.measby, " _
        + "e.eqnum, isnull(f.func, 'Not Provided') as func, isnull(c.compnum, 'Not Provided') as compnum " _
        + "from outmeasuretrack t " _
        + "left join outmeasure o on o.measid = t.measid " _
        + "left join equipment e on e.eqid = o.eqid " _
        + "left join functions f on f.func_id = o.funcid " _
        + "left join components c on c.comid = o.comid " _
        + "where t.measid = '" & measid & "' " _
        + "order by measdate desc"
        Dim i As Integer
        Dim y As Integer = 0
        Dim cnt As Integer = 0
        Dim hi, lo, typ, spec, mea, pm, func, task, spstr, dt, ms, eq, comp, chari, measout As String
        Dim crby, crdate, moby, modate As String
        Dim sb As New StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
        sb.Append("<tr><td width=""10""></td><td width=""130""></td><td width=""510""></td></tr>")
        sb.Append("<tr height=""30"" align=""center""><td class=""bigbold1"" colspan=""3"">" & tmod.getlbl("cdlbl622" , "printoutmeas.aspx.vb") & "</td></tr>")
        sb.Append("<tr height=""30"" align=""center""><td class=""plainlabel"" colspan=""3"">" & Now & "</td></tr>")
        dr = meas.GetRdrData(sql)
        While dr.Read
            If cnt = 0 Then
                pm = dr.Item("meastitle")
                sb.Append("<tr height=""30""><td class=""label"" colspan=""2"">" & tmod.getlbl("cdlbl623" , "printoutmeas.aspx.vb") & "</td><td class=""label"" colspan=""1"">" & pm & "</td></tr>")
                crby = dr.Item("createdby").ToString
                crdate = dr.Item("createdate").ToString
                moby = dr.Item("modifiedby").ToString
                modate = dr.Item("modifieddate").ToString
                sb.Append("<tr height=""20""><td class=""label"" colspan=""2"">" & tmod.getlbl("cdlbl624" , "printoutmeas.aspx.vb") & "</td><td class=""plainlabel"" colspan=""1"">" & crby & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""2"">" & tmod.getlbl("cdlbl625" , "printoutmeas.aspx.vb") & "</td><td class=""plainlabel"" colspan=""1"">" & crdate & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""2"">" & tmod.getlbl("cdlbl626" , "printoutmeas.aspx.vb") & "</td><td class=""plainlabel"" colspan=""1"">" & moby & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""2"">" & tmod.getlbl("cdlbl627" , "printoutmeas.aspx.vb") & "</td><td class=""plainlabel"" colspan=""1"">" & modate & "</td></tr>")

                sb.Append("<tr height=""20""><td colspan=""3"">&nbsp;</td></tr>")

                eq = dr.Item("eqnum").ToString
                sb.Append("<tr height=""20""><td class=""label"" colspan=""2"">" & tmod.getlbl("cdlbl628" , "printoutmeas.aspx.vb") & "</td><td class=""plainlabel"" colspan=""1""> " & eq & "</td></tr>")
                func = dr.Item("func").ToString
                sb.Append("<tr height=""20""><td class=""label"" colspan=""2"">" & tmod.getlbl("cdlbl629" , "printoutmeas.aspx.vb") & "</td><td class=""plainlabel"" colspan=""1""> " & func & "</td></tr>")
                comp = dr.Item("compnum").ToString
                sb.Append("<tr height=""20""><td class=""label"" colspan=""2"">" & tmod.getlbl("cdlbl630" , "printoutmeas.aspx.vb") & "</td><td class=""plainlabel"" colspan=""1""> " & comp & "</td></tr>")

                sb.Append("<tr height=""20""><td colspan=""3"">&nbsp;</td></tr>")

                hi = dr.Item("hi").ToString
                lo = dr.Item("lo").ToString
                typ = dr.Item("type").ToString
                mea = dr.Item("measure").ToString
                chari = dr.Item("char").ToString
                spec = dr.Item("spec").ToString
                measout = dr.Item("measout").ToString

                sb.Append("<tr height=""20""><td class=""label"" colspan=""2"">" & tmod.getlbl("cdlbl631" , "printoutmeas.aspx.vb") & "</td><td class=""plainlabel"" colspan=""1"">" & hi & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""2"">" & tmod.getlbl("cdlbl632" , "printoutmeas.aspx.vb") & "</td><td class=""plainlabel"" colspan=""1"">" & lo & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""2"">" & tmod.getlbl("cdlbl633" , "printoutmeas.aspx.vb") & "</td><td class=""plainlabel"" colspan=""1"">" & spec & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""2"">" & tmod.getlbl("cdlbl634" , "printoutmeas.aspx.vb") & "</td><td class=""plainlabel"" colspan=""1"">" & chari & "</td></tr>")
                sb.Append("<tr><td class=""bigbold"" colspan=""3""><hr></td></tr>")
                sb.Append("<tr><td class=""plainlabel"">&nbsp;</td><td class=""label""><u>Measure Date</u></td><td class=""label""><u>Measurement</u></td></tr>")
                cnt = cnt + 1
            End If
            ms = dr.Item("measurement").ToString
            dt = dr.Item("measdate").ToString
            sb.Append("<tr><td></td><td class=""plainlabel"">" & dt & "</td><td class=""plainlabel"">" & ms & "</td></tr>")
        End While
        dr.Close()
        meas.Dispose()
        sb.Append("</table>")
        Response.Write(sb.ToString)
    End Sub
End Class
