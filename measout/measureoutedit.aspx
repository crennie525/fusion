<%@ Page Language="vb" AutoEventWireup="false" Codebehind="measureoutedit.aspx.vb" Inherits="lucy_r12.measureoutedit" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>measureoutedit</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/measureouteditaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 2px; LEFT: 2px" cellSpacing="1" cellPadding="1"
				width="310">
				<TBODY>
					<tr>
						<td colSpan="2">
							<table>
								<tr>
									<td class="label" width="110"><asp:Label id="lang3284" runat="server">Measurement Title</asp:Label></td>
									<td><asp:textbox id="txtmtitle" runat="server" CssClass="plainlabel" Width="150px"></asp:textbox></td>
								</tr>
							</table>
						</td>
					<tr>
						<td vAlign="top">
							<table>
								<tr>
									<td class="label" colSpan="2" height="20"><asp:Label id="lang3285" runat="server">Measurement Type</asp:Label></td>
								</tr>
								<tr>
									<td><asp:dropdownlist id="ddtypes" runat="server" CssClass="plainlabel" Width="150px" AutoPostBack="True"></asp:dropdownlist></td>
									<td><IMG id="iaddtype" onclick="getAddType();" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
											width="20" runat="server"></td>
								</tr>
								<tr>
									<td class="label" colSpan="2" height="20"><asp:Label id="lang3286" runat="server">Measurement</asp:Label></td>
								</tr>
								<tr>
									<td><asp:dropdownlist id="ddmeas" runat="server" Width="150px" AutoPostBack="True"></asp:dropdownlist></td>
									<td><IMG id="iaddmeas" onclick="getAddMeas();" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
											width="20" runat="server"></td>
								</tr>
							</table>
						</td>
						<td vAlign="top">
							<table>
								<tr>
									<td vAlign="middle"><asp:radiobuttonlist id="rblchar" runat="server" CssClass="label" AutoPostBack="True" RepeatLayout="Flow">
											<asp:ListItem Value="spec">Specific</asp:ListItem>
											<asp:ListItem Value="range" Selected="True">Range</asp:ListItem>
											<asp:ListItem Value="max">Not to Exceed</asp:ListItem>
											<asp:ListItem Value="rec">Record Only</asp:ListItem>
										</asp:radiobuttonlist></td>
								</tr>
							</table>
						</td>
					<tr id="trspec" runat="server">
						<td align="center" colSpan="2">
							<table>
								<tr>
									<td class="disablelabel" id="tdspec" runat="server"><asp:Label id="lang3287" runat="server">Hi Limit</asp:Label></td>
									<td><asp:textbox id="t1" runat="server" CssClass="plainlabel" MaxLength="50"></asp:textbox></td>
									<td class="plainlabel" id="tdstype" runat="server"></td>
								</tr>
								<tr>
									<td class="disablelabel" id="tdspec2" runat="server"><asp:Label id="lang3288" runat="server">Lo Limit</asp:Label></td>
									<td><asp:textbox id="t2" runat="server" CssClass="plainlabel" MaxLength="50"></asp:textbox></td>
									<td class="plainlabel" id="tdstype2" runat="server"></td>
								</tr>
								<tr>
									<td class="disablelabel" id="tdspec3" runat="server"><asp:Label id="lang3289" runat="server">Specific</asp:Label></td>
									<td><asp:textbox id="t3" runat="server" CssClass="plainlabel" MaxLength="50"></asp:textbox></td>
									<td class="plainlabel" id="tdstype3" runat="server"></td>
									<td align="right"><IMG id="btnadd" onclick="addToTask();" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif"
											runat="server">&nbsp;<IMG id="btnreturn" onclick="handleexit();" alt="" src="../images/appbuttons/minibuttons/candisk1.gif"
											runat="server">
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</TBODY>
			</table>
			<input id="lblcid" type="hidden" name="lblcid" runat="server"> <input id="lblrettype" type="hidden" name="lblrettype" runat="server"><input id="lbltypeid" type="hidden" name="lbltypeid" runat="server">
			<input id="lblmeasid" type="hidden" name="lblmeasid" runat="server"><input id="lblchr" type="hidden" name="lblchr" runat="server">
			<input id="lblout" type="hidden" name="lblout" runat="server"><input id="lbllog" type="hidden" name="lbllog" runat="server">
			<input id="lbltyp" type="hidden" name="lbltyp" runat="server"> <input id="lblro" type="hidden" name="lblro" runat="server">
			<input id="lblrev" type="hidden" name="lblrev" runat="server"> <input id="lbldid" type="hidden" name="lbldid" runat="server">
			<input id="lblsid" type="hidden" name="lblsid" runat="server"> <input id="lblclid" type="hidden" name="lblclid" runat="server">
			<input id="lbllocid" type="hidden" name="lbllocid" runat="server"> <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lblfuid" type="hidden" name="lblfuid" runat="server"> <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
			<input id="lbloldtype" type="hidden" name="lbloldtype" runat="server"> <input id="lbloldmeas" type="hidden" name="lbloldmeas" runat="server">
			<input id="lbloldhi" type="hidden" name="lbloldhi" runat="server"> <input id="lbloldlo" type="hidden" name="lbloldlo" runat="server">
			<input id="lbloldspec" type="hidden" name="lbloldspec" runat="server"> <input id="lbloldchar" type="hidden" name="lbloldchar" runat="server">
			<input id="lbloldtitle" type="hidden" name="lbloldtitle" runat="server"> <input id="lblparmeasid" type="hidden" runat="server">
			<input type="hidden" id="lblstart" runat="server"> <input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
