

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class measoutroutesadd
    Inherits System.Web.UI.Page
	Protected WithEvents lang3250 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3249 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3248 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3247 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3246 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3245 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3244 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3243 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3242 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3241 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3240 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3239 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3238 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3237 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3236 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim eqid As String
    Dim dr As SqlDataReader
    Dim sql As String
    Dim rep As New Utilities
    Dim fuid, cid, psite, filt, dt, val, eq, route, freq As String
    Dim Login As String
    Dim tl, sid, did, clid, chk As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim Sort As String
    Protected WithEvents lblrteid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim rteid As String
    Protected WithEvents txteqnum As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtoem As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmodel As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtserial As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddac As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents trmain As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents diveq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iffunc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifcomp As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqdesc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblspl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmodel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloem As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblserial As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblac As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnumsrch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrepcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmeascnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdco As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents trhead As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents ifrte As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifbot As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            sid = HttpContext.Current.Session("dfltps").ToString()
            lblsid.Value = sid
            rteid = Request.QueryString("rteid").ToString
            lblrteid.Value = rteid
            ifbot.Attributes.Add("src", "measroutesaddbot.aspx?rteid=" & rteid)
            route = Request.QueryString("rte").ToString
            freq = Request.QueryString("freq").ToString
            tdcomp.InnerHtml = route & " - " & freq & " Days"

            txtpg.Value = "1"
            rep.Open()
            GetEqList()
            PopAC()
            rep.Dispose()
        Else
            If Request.Form("lblsubmit") = "srcheq" Then
                lblsubmit.Value = ""
                txtpg.Value = "1"
                rep.Open()
                SrchEq()
                GetEqList()
                rep.Dispose()
            End If
            If Request.Form("lblsubmit") = "next" Then
                rep.Open()
                GetNext()
                rep.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "last" Then
                rep.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetEqList()
                rep.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "prev" Then
                rep.Open()
                GetPrev()
                rep.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "first" Then
                rep.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetEqList()
                rep.Dispose()
                lblsubmit.Value = ""
            End If
        End If
        'ibtnret.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ibtnret.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
        'btntocomp.Attributes.Add("onclick", "DisableButton(this);")
        'btnfromcomp.Attributes.Add("onclick", "DisableButton(this);")
    End Sub
    Private Sub SrchEq()
        Dim Filter, eq, ed, sp, oe, mo, se, ac As String
        eq = txteqnum.Text
        If Len(eq) <> 0 Then
            eq = ModString(eq)
            Filter = "eqnum like ''%" & eq & "%'' "
            FilterCnt = "eqnum like '%" & eq & "%' "
            lbleqnumsrch.Value = eq
        End If
        ed = txteqdesc.Text
        If Len(ed) <> 0 Then
            ed = ModString(ed)
            If Len(Filter) = 0 Then
                Filter = "eqdesc like ''%" & ed & "%'' "
                FilterCnt = "eqdesc like '%" & ed & "%' "
            Else
                Filter += "and eqdesc like ''%" & ed & "%'' "
                FilterCnt += "and eqdesc like '%" & ed & "%' "
            End If
            lbleqdesc.Value = ed
        End If
        sp = txtspl.Text
        If Len(sp) <> 0 Then
            sp = ModString(sp)
            If Len(Filter) = 0 Then
                Filter = "spl like ''%" & sp & "%'' "
                FilterCnt = "spl like '%" & sp & "%' "
            Else
                Filter += "and spl like ''%" & sp & "%'' "
                FilterCnt += "and spl like '%" & sp & "%' "
            End If
            lblspl.Value = sp
        End If
        oe = txtoem.Text
        If Len(oe) <> 0 Then
            oe = ModString(oe)
            If Len(Filter) = 0 Then
                Filter = "oem like ''%" & oe & "%'' "
                FilterCnt = "oem like '%" & oe & "%' "
            Else
                Filter += "and oem like ''%" & oe & "%'' "
                FilterCnt += "and oem like '%" & oe & "%' "
            End If
            lbloem.Value = oe
        End If
        mo = txtmodel.Text
        If Len(mo) <> 0 Then
            mo = ModString(mo)
            If Len(Filter) = 0 Then
                Filter = "model like ''%" & mo & "%'' "
                FilterCnt = "model like '%" & mo & "%' "
            Else
                Filter += "and model like ''%" & mo & "%'' "
                FilterCnt += "and model like '%" & mo & "%' "
            End If
            lblmodel.Value = mo
        End If
        se = txtserial.Text
        If Len(se) <> 0 Then
            se = ModString(se)
            If Len(Filter) = 0 Then
                Filter = "serial like ''%" & se & "%'' "
                FilterCnt = "serial like '%" & se & "%' "
            Else
                Filter += "and serial like ''%" & se & "%'' "
                FilterCnt += "and serial like '%" & se & "%' "
            End If
            lblserial.Value = se
        End If
        If ddac.SelectedIndex <> 0 Then
            ac = ddac.SelectedValue.ToString
            If Len(Filter) = 0 Then
                Filter = "acid like ''%" & ac & "%'' "
                FilterCnt = "acid like '%" & ac & "%' "
            Else
                Filter += "and acid like ''%" & ac & "%'' "
                FilterCnt += "and acid like '%" & ac & "%' "
            End If
            lblac.Value = ac
        End If

    End Sub
    Private Sub ClearFields()
        lbleqnumsrch.Value = ""
        txteqnum.Text = ""
        lbleqnum.Value = ""
        txteqnum.Text = ""
        lbleqdesc.Value = ""
        txteqdesc.Text = ""
        lblspl.Value = ""
        txtspl.Text = ""
        lbloem.Value = ""
        txtoem.Text = ""
        lblmodel.Value = ""
        txtmodel.Text = ""
        lblserial.Value = ""
        txtserial.Text = ""
        lblac.Value = ""
        Try
            ddac.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetEqList()
        Catch ex As Exception
            rep.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1575" , "measoutroutesadd.aspx.vb")
 
            rep.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetEqList()
        Catch ex As Exception
            rep.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1576" , "measoutroutesadd.aspx.vb")
 
            rep.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Function ModString(ByVal str As String)
        str = Replace(str, "'", Chr(180), , , vbTextCompare)
        str = Replace(str, "--", "-", , , vbTextCompare)
        str = Replace(str, ";", ":", , , vbTextCompare)
        Return str
    End Function
    Private Sub GetEqList()
        PageNumber = txtpg.Value
        Dim sb As New StringBuilder
        Dim eqnum, eqdesc As String
        Dim Filter, eq, ed, sp, oe, mo, se, ac As String
        eq = lbleqnumsrch.Value '
        If Len(eq) <> 0 Then
            eq = ModString(eq)
            Filter = "eqnum like ''%" & eq & "%'' "
            FilterCnt = "eqnum like '%" & eq & "%' "
            txteqnum.Text = eq
        End If
        ed = lbleqdesc.Value '
        If Len(ed) <> 0 Then
            ed = ModString(ed)
            If Len(Filter) = 0 Then
                Filter = "eqdesc like ''%" & ed & "%'' "
                FilterCnt = "eqdesc like '%" & ed & "%' "
            Else
                Filter += "and eqdesc like ''%" & ed & "%'' "
                FilterCnt += "and eqdesc like '%" & ed & "%' "
            End If
            txteqdesc.Text = ed
        End If
        sp = lblspl.Value '
        If Len(sp) <> 0 Then
            sp = ModString(sp)
            If Len(Filter) = 0 Then
                Filter = "spl like ''%" & sp & "%'' "
                FilterCnt = "spl like '%" & sp & "%' "
            Else
                Filter += "and spl like ''%" & sp & "%'' "
                FilterCnt += "and spl like '%" & sp & "%' "
            End If
            txtspl.Text = sp
        End If
        oe = lbloem.Value '
        If Len(oe) <> 0 Then
            oe = ModString(oe)
            If Len(Filter) = 0 Then
                Filter = "oem like ''%" & oe & "%'' "
                FilterCnt = "oem like '%" & oe & "%' "
            Else
                Filter += "and oem like ''%" & oe & "%'' "
                FilterCnt += "and oem like '%" & oe & "%' "
            End If
            txtoem.Text = oe
        End If
        mo = lblmodel.Value '
        If Len(mo) <> 0 Then
            mo = ModString(mo)
            If Len(Filter) = 0 Then
                Filter = "model like ''%" & mo & "%'' "
                FilterCnt = "model like '%" & mo & "%' "
            Else
                Filter += "and model like ''%" & mo & "%'' "
                FilterCnt += "and model like '%" & mo & "%' "
            End If
            txtmodel.Text = mo
        End If
        se = lblserial.Value '
        If Len(se) <> 0 Then
            se = ModString(se)
            If Len(Filter) = 0 Then
                Filter = "serial like ''%" & se & "%'' "
                FilterCnt = "serial like '%" & se & "%' "
            Else
                Filter += "and serial like ''%" & se & "%'' "
                FilterCnt += "and serial like '%" & se & "%' "
            End If
            txtserial.Text = se
        End If
        If ddac.SelectedIndex <> 0 And ddac.SelectedIndex <> -1 Then
            ac = lblac.Value '
            If Len(Filter) = 0 Then
                Filter = "acid like ''%" & ac & "%'' "
                FilterCnt = "acid like '%" & ac & "%' "
            Else
                Filter += "and acid like ''%" & ac & "%'' "
                FilterCnt += "and acid like '%" & ac & "%' "
            End If
            ddac.SelectedValue = ac
        End If
        If Len(Filter) = 0 Then
            psite = lblsid.Value
            FilterCnt = "siteid = '" & psite & "'"
            Filter = "siteid = ''" & psite & "''"
        End If
        sql = "select Count(*) from Equipment where " & FilterCnt
        Dim dc As Integer = rep.PageCount(sql, PageSize) 'rep.Scalar(sql)
        If dc = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & dc
        End If
        txtpgcnt.Value = dc
        ' sql = "select count(*) from pmTaskMeasDetMan m " _
        '+ "left join pm p on p.pmid = m.pmid where p.eqid = '" & eqid & "'"
        Tables = "equipment"
        PK = "eqid"
        PageSize = "200"
        Fields = "dept_id, cellid, eqid, eqnum, eqdesc, mcnt = ( " _
        + "select count(*) from pmTaskMeasDetMan m left join pm p on p.pmid = m.pmid where p.eqid = equipment.eqid)"
        dr = rep.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        'sql = "select dept_id, cellid, eqid, eqnum, eqdesc from equipment where siteid = '" & psite & "'"
        sb.Append("<table>")
        'dr = rep.GetRdrData(sql)
        'If dr.Read Then
        Dim mcnt As String
        While dr.Read
            did = dr.Item("dept_id").ToString
            mcnt = dr.Item("mcnt").ToString
            clid = dr.Item("cellid").ToString
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            eqnum = Replace(eqnum, "/", "//", , , vbTextCompare)
            eqnum = Replace(eqnum, """", Chr(180) & Chr(180), , , vbTextCompare)
            eqnum = ModString(eqnum)
            eqdesc = dr.Item("eqdesc").ToString
            eqdesc = Replace(eqdesc, "/", "//", , , vbTextCompare)
            eqdesc = Replace(eqdesc, """", Chr(180) & Chr(180), , , vbTextCompare)
            eqdesc = ModString(eqdesc)
            sb.Append("<tr><td>")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getdeq('" & did & "','" & clid & "','" & eqid & "','" & eqnum & "','" & eqdesc & "','" & mcnt & "')"">")
            sb.Append(eqnum & " - " & eqdesc)
            sb.Append("</td></tr>")
        End While
        'Else
        'sb.Append("<tr><td class=""plainlabelred"">" & tmod.getlbl("cdlbl619" , "measoutroutesadd.aspx.vb") & "</td></tr>")
        'End If
        dr.Close()
        sb.Append("</table>")
        diveq.InnerHtml = sb.ToString
    End Sub


    Private Sub PopAC()
        cid = "0" 'lblcid.Value
        sql = "select * from pmAssetClass where compid = '" & cid & "' order by assetclass"
        dr = rep.GetRdrData(sql)
        ddac.DataSource = dr
        ddac.DataTextField = "assetclass"
        ddac.DataValueField = "acid"
        ddac.DataBind()
        dr.Close()
        ddac.Items.Insert(0, "Select Asset Class")
    End Sub
    Private Sub PopAvail(ByVal rteid As String)

        Dim scnt As Integer
        sid = lblsid.Value



        val = "measid, meastitle"
        'filt = " where measid not in (" _
        ' + "select failid from componentfailmodes where comid = '" & comid & "') order by failuremode asc"
        'dr = fail.GetList(dt, val, filt) 'removed compid = '" & cid & "' and 
        'lbfailmaster.DataSource = dr
        'lbfailmaster.DataTextField = "failuremode"
        'lbfailmaster.DataValueField = "failid"
        'lbfailmaster.DataBind()
        'dr.Close()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3236.Text = axlabs.GetASPXPage("measoutroutesadd.aspx", "lang3236")
        Catch ex As Exception
        End Try
        Try
            lang3237.Text = axlabs.GetASPXPage("measoutroutesadd.aspx", "lang3237")
        Catch ex As Exception
        End Try
        Try
            lang3238.Text = axlabs.GetASPXPage("measoutroutesadd.aspx", "lang3238")
        Catch ex As Exception
        End Try
        Try
            lang3239.Text = axlabs.GetASPXPage("measoutroutesadd.aspx", "lang3239")
        Catch ex As Exception
        End Try
        Try
            lang3240.Text = axlabs.GetASPXPage("measoutroutesadd.aspx", "lang3240")
        Catch ex As Exception
        End Try
        Try
            lang3241.Text = axlabs.GetASPXPage("measoutroutesadd.aspx", "lang3241")
        Catch ex As Exception
        End Try
        Try
            lang3242.Text = axlabs.GetASPXPage("measoutroutesadd.aspx", "lang3242")
        Catch ex As Exception
        End Try
        Try
            lang3243.Text = axlabs.GetASPXPage("measoutroutesadd.aspx", "lang3243")
        Catch ex As Exception
        End Try
        Try
            lang3244.Text = axlabs.GetASPXPage("measoutroutesadd.aspx", "lang3244")
        Catch ex As Exception
        End Try
        Try
            lang3245.Text = axlabs.GetASPXPage("measoutroutesadd.aspx", "lang3245")
        Catch ex As Exception
        End Try
        Try
            lang3246.Text = axlabs.GetASPXPage("measoutroutesadd.aspx", "lang3246")
        Catch ex As Exception
        End Try
        Try
            lang3247.Text = axlabs.GetASPXPage("measoutroutesadd.aspx", "lang3247")
        Catch ex As Exception
        End Try
        Try
            lang3248.Text = axlabs.GetASPXPage("measoutroutesadd.aspx", "lang3248")
        Catch ex As Exception
        End Try
        Try
            lang3249.Text = axlabs.GetASPXPage("measoutroutesadd.aspx", "lang3249")
        Catch ex As Exception
        End Try
        Try
            lang3250.Text = axlabs.GetASPXPage("measoutroutesadd.aspx", "lang3250")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
