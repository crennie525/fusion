﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GRPLogin.aspx.vb" Inherits="lucy_r12.GRPLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GRP Login</title>
    <link rel="stylesheet" type="text/css" href="styles/pmcssa1.css" />
    <script type="text/javascript">
		<!--
        function lpload() {
            if (document.all) {
                showFocus(); checkqs();
            }
            else {
                showFocus(); checkqs();
            }
            GetClientsUTCOffset();
        }


        var chk;
        function showFocus() {

            var up = document.getElementById("lblupdate").value;
            if (up == "yes") {
                document.getElementById("tbllogin").className = "details";
                document.getElementById("tblupdate").className = "view";
            }
            else {
                document.getElementById("txtuid").focus();
            }
        }
        function gotosub() {
            document.getElementById("btnsubmit").focus();
        }
        function changeImg(id, img) {
            document.getElementById(id).src = img;
        }
        function ShowHide(id) {
            document.getElementById("r1").className = "details";
            document.getElementById("r2").className = "details";
            document.getElementById("r3").className = "details";
            document.FgetElementById("pmapp").value = id;
            document.getElementById("tbllogin").className = "view";
            document.getElementById("tbllogin").style.top = "250px";
            document.getElementById("tbllogin").style.left = "285px";
            if (id == "pmdev") {
                document.getElementById("lbllogtitle").innerHTML = "PM Developer Login";
            }
            else if (id == "pmman") {
                document.getElementById("lbllogtitle").innerHTML = "PM Manager Login";
            }
            else if (id == "pmopt") {
                document.getElementById("lbllogtitle").innerHTML = "PM Optimizer Login";
            }
            showFocus();
        }
        function showIntro(id) {

        }
        function hideIntro(id) {

        }
        function hideme() {
            document.getElementById("r1").className = "view";
            document.getElementById("r2").className = "view";
            document.getElementById("r3").className = "view";
            document.getElementById("tbllogin").className = "details";
        }
        function hideerror() {
            document.getElementById("tblloginerror").className = "details";
        }
        function bmsg(brow) {
            var msg = "PM Requires Microsoft Internet Explorer version 5.0 or higher";
            document.getElementById("wb").innerHTML = msg;
        }
        function checkqs() {
            var brow = navigator.appName;
            if (brow != "Microsoft Internet Explorer") {
                bmsg(brow);
            }
            var query = window.location.search.substring(1);
            var parms = query.split('&');
            for (var i = 0; i < parms.length; i++) {
                var pos = parms[i].indexOf('=');
                if (pos > 0) {
                    var key = parms[i].substring(0, pos);
                    var val = parms[i].substring(pos + 1);
                }
            }
            if (val == "pmopt") ShowHide('pmopt');
            else if (val == "pmdev") ShowHide('pmdev');
            else if (val == "pmman") ShowHide('pmman');
        }

        var myclose = false;

        function ConfirmClose() {
            if (event.clientY < 0) {
                //event.returnValue = 'Any message you want';

                setTimeout('myclose=false', 100);
                myclose = true;
            }
        }

        function HandleOnClose() {
            if (myclose == true) {
                //alert("Window is closed");
                document.getElementById("lblsubmit").value = "go"
                document.getElementById("form1").submit();
            }
        }

        function GetClientsUTCOffset() {
            var tzo = (new Date().getTimezoneOffset() / 60) * (-1);
            document.getElementById("lblutcoffset").value = tzo;
        }
		-->
    </script>
</head>
<body ms_positioning="GridLayout" onload="lpload();">
    <form id="Form1" method="post" runat="server">
    <table width="900">
        <tr>
            <td colspan="2" align="center">
                <asp:Image ID="Image2" runat="server" ImageUrl="images/appimages/grptop.gif"></asp:Image>
            </td>
        </tr>
        <tr>
            <td height="20" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td id="tduni" class="bigbold" height="20" colspan="2" align="center" runat="server">
                Pfizer Global Reliability Program (English Database)
            </td>
        </tr>
        <tr>
            <td align="center">
                <table width="810" background="images/appimages/globeback.gif" border="0">
                    <tr>
                        <td height="80" colspan="3" align="center" id="tdlang" runat="server">
                            <table class="view">
                            <tr>
                            <td class="label">Choose Language</td>
                            <td>
                                <asp:DropDownList ID="ddlang" runat="server" AutoPostBack="True">
                                <asp:ListItem Value="spa">Spanish</asp:ListItem>
                                <asp:ListItem Value="eng">English</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td class="bluelabel" colspan="2" align="center">
                            Please Log In...
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td colspan="2" align="center">
                            <table id="tbllogin" cellspacing="0" cellpadding="0" width="420">
                                <tr>
                                    <td width="70">
                                    </td>
                                    <td width="100">
                                    </td>
                                    <td width="120">
                                    </td>
                                    <td width="100">
                                    </td>
                                    <td width="30">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblname" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">User Name</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtuid" runat="server" Font-Names="Arial" Font-Size="X-Small" Width="120px"></asp:TextBox>
                                    </td>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblpass" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Password</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtpass" runat="server" Font-Names="Arial" Font-Size="X-Small" Width="120px"
                                            TextMode="Password"></asp:TextBox>
                                    </td>
                                    <td align="center">
                                        <asp:ImageButton ID="btnlogin" runat="server" ImageUrl="images/appbuttons/bgbuttons/blogin.gif">
                                        </asp:ImageButton>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr class="details" height="30">
                                    <td colspan="5" align="center">
                                        <a class="A1" onclick="addfav();" href="#">Add The PM Login Page To My Favorites</a>
                                    </td>
                                </tr>
                            </table>
                            <table id="tblupdate" class="details" cellspacing="0" cellpadding="0" width="420">
                                <tr>
                                    <td class="redlabel" align="center">
                                        The GRP Web Site is being updated and will be back on-line shortly.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="redlabel" align="center">
                                        We apologize for the inconvenience.
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td height="210" colspan="3">
                            <table>
                                <tr>
                                    <td height="40">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="images/appimages/laibot.gif">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="pmapp" type="hidden" runat="server" name="pmapp"><input id="logbad" type="hidden"
        runat="server" name="logbad">
    <input type="hidden" id="lblupdate" runat="server" name="lblupdate">
    <input type="hidden" id="lblbrowser" runat="server" name="lblbrowser">
    <input type="hidden" id="lblversion" runat="server" name="lblversion">
    <input type="hidden" id="lblos" runat="server" name="lblos">
    <input type="hidden" id="lblutcoffset" runat="server" name="lblutcoffset">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
