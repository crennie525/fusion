﻿Imports System.Data.SqlClient
Public Class pmo123e2rdets
    Inherits System.Web.UI.Page
    Dim tasks As New Utilities
    Dim tasksadd As New Utilities
    Dim tmod As New transmod
    Dim comi As New mmenu_utils_a
    Public dr As SqlDataReader
    Dim ds As DataSet
    Dim sql As String
    Dim sid, ttid, eqid, who, coid, ustr, usetot, ghostoff, xstatus As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim coi As String = comi.COMPI

        If coi = "GSK" Then
            lblghostoff.Value = "yes"
        End If
        lblcid.Value = "0"
        lblro.Value = "0"
        lblenable.Value = "0"
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            ttid = Request.QueryString("ttid").ToString
            eqid = Request.QueryString("eqid").ToString
            coid = Request.QueryString("coid").ToString
            who = Request.QueryString("who").ToString
            ustr = Request.QueryString("ustr").ToString
            Try
                usetot = Request.QueryString("usetot").ToString
            Catch ex As Exception
                usetot = "0"
            End Try

            lblsid.Value = sid
            lblpmtskid.Value = ttid
            lbleqid.Value = eqid
            lblcoid.Value = coid
            lblwho.Value = who
            lblustr.Value = ustr
            lblusetot.Value = usetot
            If who = "rev" Or who = "revdnd" Then
                tdwho.InnerHtml = "Current Task Details - Revised"
                asw.InnerHtml = "Switch to Original"
            Else
                tdwho.InnerHtml = "Current Task Details - Original"
                asw.InnerHtml = "Switch to Revised"
            End If
            If usetot = "1" Then
                If who = "orig" Or who = "origdnd" Then

                End If
            End If
            tasks.Open()
            GetLists()
            If coid <> "" And coid <> "0" Then
                If who = "rev" Or who = "revdnd" Then
                    PopCompFailList(coid)
                    trfailrev.Attributes.Add("class", "view")
                    trfailrev2.Attributes.Add("class", "view")
                    trfailorig.Attributes.Add("class", "details")
                    trfailorig2.Attributes.Add("class", "details")
                Else
                    PopCompFailList2(coid)
                    trfailrev.Attributes.Add("class", "details")
                    trfailrev2.Attributes.Add("class", "details")
                    trfailorig.Attributes.Add("class", "view")
                    trfailorig2.Attributes.Add("class", "view")
                End If
            Else
                If who = "rev" Or who = "revdnd" Then
                    'PopCompFailList(coid)
                    trfailrev.Attributes.Add("class", "view")
                    trfailrev2.Attributes.Add("class", "view")
                    trfailorig.Attributes.Add("class", "details")
                    trfailorig2.Attributes.Add("class", "details")
                Else
                    'PopCompFailList2(coid)
                    trfailrev.Attributes.Add("class", "details")
                    trfailrev2.Attributes.Add("class", "details")
                    trfailorig.Attributes.Add("class", "view")
                    trfailorig2.Attributes.Add("class", "view")
                End If
            End If
            If who = "rev" Or who = "revdnd" Then
                If coid <> "" And coid <> "0" Then
                    PopTaskFailModes(coid)
                    PopFailList(coid)
                End If
            Else
                If coid <> "" And coid <> "0" Then
                    PopoTaskFailModes(coid)
                End If
            End If
            getrev(ttid, who)
            tasks.Dispose()
        Else
            If Request.Form("lblsubmit") = "sav" Then
                lblsubmit.Value = ""
                ttid = lblpmtskid.Value
                who = lblwho.Value
                tasks.Open()
                saverev(ttid, who)
                tasks.Dispose()
            ElseIf Request.Form("lblsubmit") = "upcompfail" Then
                lblsubmit.Value = ""
                coid = lblcoid.Value
                tasks.Open()
                PopCompFailList(coid)
                tasks.Dispose()
            ElseIf Request.Form("lblsubmit") = "upmeter" Then
                lblsubmit.Value = ""
                ttid = lblpmtskid.Value
                who = lblwho.Value
                tasks.Open()
                getrev(ttid, who)
                tasks.Dispose()
            End If
        End If
    End Sub
    Private Sub PopCompFailList(ByVal comp As String)
        ttid = lblpmtskid.Value
        sql = "select compfailid, failuremode " _
         + "from componentfailmodes where comid = '" & comp & "'"
        sql = "usp_getcfall_co '" & comp & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        lbfailmaster.DataSource = dr
        lbfailmaster.DataTextField = "failuremode"
        lbfailmaster.DataValueField = "compfailid"
        lbfailmaster.DataBind()
        dr.Close()
    End Sub
    Private Sub PopCompFailList2(ByVal comp As String)
        ttid = lblpmtskid.Value
        sql = "select compfailid, failuremode " _
         + "from componentfailmodes where comid = '" & comp & "'"
        sql = "usp_getcfall_co '" & comp & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        lbCompFM.DataSource = dr
        lbCompFM.DataTextField = "failuremode"
        lbCompFM.DataValueField = "compfailid"
        lbCompFM.DataBind()
        dr.Close()
    End Sub
    Private Sub PopFailList(ByVal comp As String)
        ttid = lblpmtskid.Value

        sql = "select compfailid, failuremode " _
        + "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
        + "select failid from pmtaskfailmodes where comid = '" & comp & "')" ' and taskid = '" & ttid & "')"

        sql = "usp_getcfall_tskna '" & comp & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try
        lbfaillist.DataSource = dr
        lbfaillist.DataTextField = "failuremode"
        lbfaillist.DataValueField = "compfailid"
        lbfaillist.DataBind()
        dr.Close()
    End Sub

    Private Sub PopTaskFailModes(ByVal comp As String)
        ttid = lblpmtskid.Value
        'sql = "select * from pmtaskfailmodes where taskid = '" & ttid & "'"
        sql = "select * from pmtaskfailmodes where comid = '" & comp & "' and taskid = '" & ttid & "'"
        sql = "usp_getcfall_tsk '" & comp & "','" & ttid & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        lbfailmodes.DataSource = dr
        lbfailmodes.DataTextField = "failuremode"
        lbfailmodes.DataValueField = "failid"
        lbfailmodes.DataBind()
        dr.Close()
    End Sub
    Private Sub PopoTaskFailModes(ByVal comp As String)
        ttid = lblpmtskid.Value
        sql = "select * from pmotaskfailmodes where taskid = '" & ttid & "'"
        sql = "usp_getcfall_tsko '" & ttid & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try


        lbofailmodes.DataSource = dr
        lbofailmodes.DataTextField = "failuremode"
        lbofailmodes.DataValueField = "failid"
        lbofailmodes.DataBind()
        dr.Close()
    End Sub
    Private Sub saverev(ByVal ttid As String, ByVal who As String)
        Dim freq, rdid, qty, skillid, ismeter, meterid, fixed, mfid, tid, ptid, lotoid, conid, ttime, rdt As String
        Dim origfreq, origrdid, origqty, origskillid, meterido, fixedo, mfido, origtid, origptid, origttime, origrdt As String
        Dim fuid, taskstatusid, taskstat, rd, func, skill, eqnum, rdo, skillo As String
        Dim typ, typstr, frestr, tr As String
        Dim usemeter As String = lblismeter.Value
        Dim usemetero As String = lblismetero.Value
        If ddtaskstat.SelectedIndex <> 0 Then
            taskstatusid = ddtaskstat.SelectedValue
            taskstat = ddtaskstat.SelectedItem.ToString
            taskstat = Replace(taskstat, "'", Chr(180), , , vbTextCompare)
        Else
            taskstatusid = "0"
        End If
        If ddtype.SelectedIndex <> 0 Then
            typ = ddtype.SelectedValue
            typstr = ddtype.SelectedItem.ToString
            typstr = Replace(typstr, "'", Chr(180), , , vbTextCompare)
        Else
            typ = "0"
        End If
        If who = "revdnd" Or who = "origdnd" Then
            frestr = lblfreq.Value
        Else
            frestr = txtfreq.Text
        End If

        qty = txtqty.Text
        If Len(qty) = 0 Then
            qty = "1"
        End If
        Dim usetot As String = lblusetot.Value
        If who = "origdnd" Then
            If usetot = "1" Then
                tr = lbltr.Value
            Else
                tr = txttr.Text
            End If
        Else
            tr = txttr.Text
        End If
        If Len(tr) = 0 Then
            tr = "0"
        End If
        Dim eqs, eqsstr As String
        If ddeqstat.SelectedIndex <> 0 Then
            eqs = ddeqstat.SelectedValue
            eqsstr = ddeqstat.SelectedItem.ToString
            eqsstr = Replace(eqsstr, "'", Chr(180), , , vbTextCompare)
        Else
            eqs = "0"
        End If
        If eqs = "0" Then
            If usemeter = "1" Then
                Dim strMessage1 As String = "Running or Down Status Required When Using Meter Frequency"
                Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                Exit Sub
            End If
        End If
        rdt = txtrdt.Text
        If rdt = "" Then
            rdt = "0"
        End If
        Dim lot, cs As String
        If cbloto.Checked = True Then
            lot = "1"
        Else
            lot = "0"
        End If
        If cbcs.Checked = True Then
            cs = "1"
        Else
            cs = "0"
        End If
        Dim fm As String
        Dim Item As ListItem
        Dim f, fi As String
        Dim ipar As Integer = 0
        For Each Item In lbfailmodes.Items
            f = Item.Text.ToString
            ipar = f.LastIndexOf("(")
            If ipar <> -1 Then
                f = Mid(f, 1, ipar)
            End If
            If Len(fm) = 0 Then
                fm = f & "(___)"
            Else
                fm += " " & f & "(___)"
            End If
        Next
        fm = tasks.ModString2(fm)
        Dim pt, ptstr As String
        If ddpt.SelectedIndex <> 0 Then
            pt = ddpt.SelectedValue
            ptstr = ddpt.SelectedItem.ToString
            ptstr = Replace(ptstr, "'", Chr(180), , , vbTextCompare)
        Else
            pt = "0"
        End If
        Dim ski, skistr As String
        If ddskill.SelectedIndex <> 0 Then
            ski = ddskill.SelectedValue
            skistr = ddskill.SelectedItem.ToString
            skistr = Replace(skistr, "'", Chr(180), , , vbTextCompare)
        Else
            ski = "0"
        End If
        If ski = "0" Then
            If usemeter = "1" Then
                Dim strMessage1 As String = "Skill Required When Using Meter Frequency"
                Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                Exit Sub
            End If
        End If
        fixed = lblfixed.Value
        If cbfixed.Checked = True Then
            fixed = "1"
        End If
       
        Dim cmd As New SqlCommand
        If who = "rev" Or who = "revdnd" Then
            cmd.CommandText = "update pmtasks set taskstatus = @taskstat, ttid = @typ, tasktype = @typstr, freq = @frestr, " _
            + "qty = @qty, ttime = @tr, rdid = @eqs, rd = @eqsstr, rdt = @rdt, lotoid = @lot, conid = @cs, fm1 = @fm, ptid = @pt, " _
            + "pretech = @ptstr, skillid = @ski, skill = @skistr where pmtskid = @ttid"
        Else
            If who = "origdnd" Then
                cmd.CommandText = "update pmtasks set taskstatus = @taskstat, origttid = @typ, origtasktype = @typstr, " _
                + "origttime = @tr, origrdt = @rdt, lotoid = @lot, conid = @cs, ofm1 = @fm, origptid = @pt, " _
                + "origpretech = @ptstr where pmtskid = @ttid"
            Else
                ghostoff = lblghostoff.Value
                xstatus = lblxstatus.Value
                If ghostoff = "yes" And xstatus = "yes" Then
                    cmd.CommandText = "update pmtasks set taskstatus = @taskstat, origttid = @typ, origtasktype = @typstr, " _
                + "origttime = @tr, origrdt = @rdt, lotoid = @lot, conid = @cs, ofm1 = @fm, origptid = @pt, " _
                + "origpretech = @ptstr where pmtskid = @ttid"
                Else
                    cmd.CommandText = "update pmtasks set taskstatus = @taskstat, origttid = @typ, origtasktype = @typstr, origfreq = @frestr, " _
               + "origqty = @qty, origttime = @tr, origrdid = @eqs, origrd = @eqsstr, origrdt = @rdt, lotoid = @lot, conid = @cs, ofm1 = @fm, origptid = @pt, " _
               + "origpretech = @ptstr, origskillid = @ski, origskill = @skistr where pmtskid = @ttid"
                End If
               
            End If
            
        End If
        Dim param = New SqlParameter("@ttid", SqlDbType.Int)
        param.Value = ttid
        cmd.Parameters.Add(param)
        Dim param02 = New SqlParameter("@qty", SqlDbType.Int)
        If qty = "" Then
            param02.Value = "1"
        Else
            param02.Value = qty
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@tr", SqlDbType.Decimal)
        If tr = "" Then
            param03.Value = "0"
        Else
            param03.Value = tr
        End If
        cmd.Parameters.Add(param03)
        Dim param04 = New SqlParameter("@eqs", SqlDbType.Int)
        If eqs = "" Then
            param04.Value = "0"
        Else
            param04.Value = eqs
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@eqsstr", SqlDbType.VarChar)
        If eqsstr = "" Or eqsstr = "Select" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = eqsstr
        End If
        cmd.Parameters.Add(param05)
        Dim param06 = New SqlParameter("@frestr", SqlDbType.Int)
        If frestr = "" Then
            param06.Value = "0"
        Else
            param06.Value = frestr
        End If
        cmd.Parameters.Add(param06)
        Dim param08 = New SqlParameter("@lot", SqlDbType.Int)
        If lot = "" Then
            param08.Value = "0"
        Else
            param08.Value = lot
        End If
        cmd.Parameters.Add(param08)
        Dim param09 = New SqlParameter("@cs", SqlDbType.Int)
        If cs = "" Then
            param09.Value = "0"
        Else
            param09.Value = cs
        End If
        cmd.Parameters.Add(param09)
        Dim param10 = New SqlParameter("@typ", SqlDbType.Int)
        If typ = "" Then
            param10.Value = "0"
        Else
            param10.Value = typ
        End If
        cmd.Parameters.Add(param10)
        Dim param11 = New SqlParameter("@typstr", SqlDbType.VarChar)
        If typstr = "" Or typstr = "Select" Then
            param11.Value = System.DBNull.Value
        Else
            param11.Value = typstr
        End If
        cmd.Parameters.Add(param11)
        Dim param14 = New SqlParameter("@fm", SqlDbType.VarChar)
        If fm = "" Then
            param14.Value = System.DBNull.Value
        Else
            param14.Value = fm
        End If
        cmd.Parameters.Add(param14)
        Dim param17 = New SqlParameter("@rdt", SqlDbType.Decimal)
        If rdt = "" Then
            param17.Value = "0"
        Else
            param17.Value = rdt
        End If
        cmd.Parameters.Add(param17)
        Dim param48 = New SqlParameter("@ustr", SqlDbType.VarChar)
        If ustr = "" Then
            param48.Value = System.DBNull.Value
        Else
            param48.Value = ustr
        End If
        cmd.Parameters.Add(param48)

       

        Dim param86 = New SqlParameter("@taskstat", SqlDbType.VarChar)
        If taskstat = "" Or taskstat = "Select" Then
            param86.Value = System.DBNull.Value
        Else
            param86.Value = taskstat
        End If
        cmd.Parameters.Add(param86)
        Dim param21 = New SqlParameter("@pt", SqlDbType.Int)
        If pt = "" Then
            param21.Value = "0"
        Else
            param21.Value = pt
        End If
        cmd.Parameters.Add(param21)
        Dim param22 = New SqlParameter("@ptstr", SqlDbType.VarChar)
        If ptstr = "" Or ptstr = "Select" Then
            param22.Value = System.DBNull.Value
        Else
            param22.Value = ptstr
        End If
        cmd.Parameters.Add(param22)
        Dim param23 = New SqlParameter("@ski", SqlDbType.Int)
        If ski = "" Then
            param23.Value = "0"
        Else
            param23.Value = ski
        End If
        cmd.Parameters.Add(param23)
        Dim param24 = New SqlParameter("@skistr", SqlDbType.VarChar)
        If skistr = "" Or ptstr = "Select" Then
            param24.Value = System.DBNull.Value
        Else
            param24.Value = skistr
        End If
        cmd.Parameters.Add(param24)
        Dim param30 = New SqlParameter("@fixed", SqlDbType.VarChar)
        If fixed = "" Then
            param30.Value = System.DBNull.Value
        Else
            param30.Value = fixed
        End If
        cmd.Parameters.Add(param30)

        Dim tpm As Integer
        'Try
        tpm = tasks.ScalarHack(cmd)
        'Catch ex As Exception
        'tpm = tasksadd.ScalarHack(cmd)
        'End Try

        'Dim ismeter As String = lblmeterid.Value
        'If tpm = 1 And ismeter <> "1" Then
        'lbltpmalert.Value = "yes"
        'Else
        'lbltpmalert.Value = "no"
        'End If
    End Sub
    Private Function CheckLock(ByVal eqid As String) As String
        Dim lock As String
        sql = "select locked, lockedby from equipment where eqid = '" & eqid & "'"
        Try
            dr = tasks.GetRdrData(sql)
            While dr.Read
                lock = dr.Item("locked").ToString
                lbllock.Value = dr.Item("locked").ToString
                lbllockedby.Value = dr.Item("lockedby").ToString
            End While
            dr.Close()
        Catch ex As Exception

        End Try
        Return lock
    End Function
    Private Sub getrev(ByVal ttid As String, ByVal who As String)
        Dim freq, rdid, qty, skillid, ismeter, meterid, fixed, mfid, tid, ptid, lotoid, conid, ttime, rdt As String
        Dim origfreq, origrdid, origqty, origskillid, ismetero, meterido, fixedo, mfido, origtid, origptid, origttime, origrdt As String
        Dim fuid, taskstatus, rd, func, skill, eqnum, rdo, skillo, compnum, tasknum, eqid, tpmhold, did, clid, coid As String
        sql = "select t.ttid, t.ptid, t.skillid, t.skill, t.qty, t.rdid, t.freq, t.rd, t.origrd, t.rdt, t.lotoid, t.conid, t.ttime, " _
            + "t.origttid, t.origptid, t.origskillid, t.origskill, t.origqty, t.origrdid, t.origfreq, t.origrdt, t.lotoid, t.conid, t.origttime, " _
            + "t.fixed, t.usemeter, t.meterid, t.mfid, t.fixedo, t.usemetero, t.meterido, t.mfido, c.compnum, t.funcid, t.taskstatus, e.xstatus, " _
            + "t.tasknum, t.eqid, t.tpmhold, t.deptid, t.cellid, t.comid " _
            + "from pmtasks t " _
            + "left join components c on c.comid = t.comid " _
             + "left join equipment e on e.eqid = t.eqid " _
            + "where t.pmtskid = '" & ttid & "'"
        dr = tasks.GetRdrData(sql)
        While dr.Read
            tasknum = dr.Item("tasknum").ToString
            eqid = dr.Item("eqid").ToString
            tpmhold = dr.Item("tpmhold").ToString
            did = dr.Item("deptid").ToString
            clid = dr.Item("cellid").ToString
            coid = dr.Item("comid").ToString
            'gen
            fuid = dr.Item("funcid").ToString
            taskstatus = dr.Item("taskstatus").ToString
            'rev
            tid = dr.Item("ttid").ToString
            ptid = dr.Item("ptid").ToString
            skillid = dr.Item("skillid").ToString
            skill = dr.Item("skill").ToString
            qty = dr.Item("qty").ToString
            rdid = dr.Item("rdid").ToString
            rd = dr.Item("rd").ToString
            freq = dr.Item("freq").ToString
            rdt = dr.Item("rdt").ToString
            lotoid = dr.Item("lotoid").ToString
            conid = dr.Item("conid").ToString
            ttime = dr.Item("ttime").ToString
            fixed = dr.Item("fixed").ToString
            ismeter = dr.Item("usemeter").ToString
            meterid = dr.Item("meterid").ToString
            mfid = dr.Item("mfid").ToString
            'orig
            origtid = dr.Item("origttid").ToString
            origptid = dr.Item("origptid").ToString
            origskillid = dr.Item("origskillid").ToString
            skillo = dr.Item("origskill").ToString
            origqty = dr.Item("origqty").ToString
            origrdid = dr.Item("origrdid").ToString
            rdo = dr.Item("origrd").ToString
            origfreq = dr.Item("origfreq").ToString
            origrdt = dr.Item("origrdt").ToString
            origttime = dr.Item("origttime").ToString
            fixedo = dr.Item("fixedo").ToString
            ismetero = dr.Item("usemetero").ToString
            meterido = dr.Item("meterido").ToString
            mfido = dr.Item("mfido").ToString
            xstatus = dr.Item("xstatus").ToString
            lblxstatus.Value = xstatus
            lblcomp.Value = dr.Item("compnum").ToString
        End While
        dr.Close()
        lblpg.Value = tasknum
        lbltpmhold.Value = tpmhold
        lbldid.Value = did
        lbltaskid.Value = ttid
        lblclid.Value = clid
        lblco.Value = coid
        CheckLock(eqid)
        lbllock.Value = "0"
        'gen
        lblismeter.Value = ismeter
        lblismetero.Value = ismetero
        lblfuid.Value = fuid
        If lotoid = "1" Then
            cbloto.Checked = True
        Else
            cbloto.Checked = False
        End If
        If conid = "1" Then
            cbcs.Checked = True
        Else
            cbcs.Checked = False
        End If
        Try
            ddtaskstat.SelectedValue = taskstatus
        Catch ex As Exception

        End Try
        'rev
        lblfixed.Value = fixed
        lblmeterid.Value = meterid
        lblmfid.Value = mfid
        lblskillid.Value = skillid
        'orig
        lblfixedo.Value = fixedo
        lblmeterido.Value = meterido
        lblmfido.Value = mfido
        lblskillido.Value = origskillid
        If ismeter = "1" Or ismetero = "1" Then
            txtfreq.Enabled = False

        End If
        'who
        If who = "revdnd" Then
            trsknorm.Attributes.Add("class", "details")
            trskdnd.Attributes.Add("class", "view")
            trqtynorm.Attributes.Add("class", "details")
            trqtydnd.Attributes.Add("class", "view")
            treqsnorm.Attributes.Add("class", "details")
            treqsdnd.Attributes.Add("class", "view")
            trfrnorm.Attributes.Add("class", "details")
            trfrdnd.Attributes.Add("class", "view")

            Try
                ddtype.SelectedValue = tid
            Catch ex As Exception

            End Try
            Try
                ddpt.SelectedValue = ptid
            Catch ex As Exception

            End Try

            Try
                tdskill.InnerHtml = skill
            Catch ex As Exception

            End Try
            tdqty.InnerHtml = qty
            txttr.Text = ttime
            lbltr.Value = ttime
            Try
                tdeqs.InnerHtml = rd
            Catch ex As Exception

            End Try
            txtrdt.Text = rdt
            tdfreq.InnerHtml = freq
            lblfreq.Value = freq
            Try
                ddtaskstat.SelectedValue = tid
            Catch ex As Exception

            End Try
        ElseIf who = "orig" Or who = "origdnd" Then
            Dim usetot As String = lblusetot.Value
            If usetot = "1" Then
                trdtnorm.Attributes.Add("class", "details")
                trdtdnd.Attributes.Add("class", "view")
                trtrnorm.Attributes.Add("class", "details")
                trtrdnd.Attributes.Add("class", "view")
            Else
                trdtnorm.Attributes.Add("class", "view")
                trdtdnd.Attributes.Add("class", "details")
                trtrnorm.Attributes.Add("class", "view")
                trtrdnd.Attributes.Add("class", "details")
            End If
            trsknorm.Attributes.Add("class", "details")
            trskdnd.Attributes.Add("class", "view")
            trqtynorm.Attributes.Add("class", "details")
            trqtydnd.Attributes.Add("class", "view")
            treqsnorm.Attributes.Add("class", "details")
            treqsdnd.Attributes.Add("class", "view")
            trfrnorm.Attributes.Add("class", "details")
            trfrdnd.Attributes.Add("class", "view")


            Try
                ddtype.SelectedValue = origtid
            Catch ex As Exception

            End Try
            Try
                ddpt.SelectedValue = origptid
            Catch ex As Exception

            End Try

            Try
                tdskill.InnerHtml = skillo
            Catch ex As Exception

            End Try
            tdqty.InnerHtml = origqty
            tdtr.InnerHtml = origttime
            txttr.Text = origttime
            lbltr.Value = origttime
            Try
                tdeqs.InnerHtml = rdo
            Catch ex As Exception

            End Try
            txtrdt.Text = origrdt
            tddt.InnerHtml = origrdt
            tdfreq.InnerHtml = origfreq
            lblfreq.Value = origfreq
            Try
                ddtaskstat.SelectedValue = tid
            Catch ex As Exception

            End Try
        ElseIf who = "rev" Then
            trsknorm.Attributes.Add("class", "view")
            trskdnd.Attributes.Add("class", "details")
            trqtynorm.Attributes.Add("class", "view")
            trqtydnd.Attributes.Add("class", "details")
            treqsnorm.Attributes.Add("class", "view")
            treqsdnd.Attributes.Add("class", "details")
            trfrnorm.Attributes.Add("class", "view")
            trfrdnd.Attributes.Add("class", "details")
            trdtnorm.Attributes.Add("class", "view")
            trdtdnd.Attributes.Add("class", "details")
            trtrnorm.Attributes.Add("class", "view")
            trtrdnd.Attributes.Add("class", "details")

            Dim usetot As String = lblusetot.Value
            If usetot = "1" Then
                trdtnorm.Attributes.Add("class", "details")
                trdtdnd.Attributes.Add("class", "view")
                trtrnorm.Attributes.Add("class", "details")
                trtrdnd.Attributes.Add("class", "view")
            Else
                trdtnorm.Attributes.Add("class", "view")
                trdtdnd.Attributes.Add("class", "details")
                trtrnorm.Attributes.Add("class", "view")
                trtrdnd.Attributes.Add("class", "details")
            End If

            Try
                ddtype.SelectedValue = tid
            Catch ex As Exception

            End Try
            Try
                ddpt.SelectedValue = ptid
            Catch ex As Exception

            End Try

            Try
                ddskill.SelectedValue = skillid
            Catch ex As Exception

            End Try
            txtqty.Text = qty
            txttr.Text = ttime
            Try
                ddeqstat.SelectedValue = rdid
            Catch ex As Exception

            End Try
            txtrdt.Text = rdt
            txtfreq.Text = freq
            Try
                ddtaskstat.SelectedValue = tid
            Catch ex As Exception

            End Try
        Else
            trsknorm.Attributes.Add("class", "view")
            trskdnd.Attributes.Add("class", "details")
            trqtynorm.Attributes.Add("class", "view")
            trqtydnd.Attributes.Add("class", "details")
            treqsnorm.Attributes.Add("class", "view")
            treqsdnd.Attributes.Add("class", "details")
            trfrnorm.Attributes.Add("class", "view")
            trfrdnd.Attributes.Add("class", "details")
            trdtnorm.Attributes.Add("class", "view")
            trdtdnd.Attributes.Add("class", "details")
            trtrnorm.Attributes.Add("class", "view")
            trtrdnd.Attributes.Add("class", "details")

            Dim usetot As String = lblusetot.Value
            If usetot = "1" Then
                trdtnorm.Attributes.Add("class", "details")
                trdtdnd.Attributes.Add("class", "view")
                trtrnorm.Attributes.Add("class", "details")
                trtrdnd.Attributes.Add("class", "view")
            Else
                trdtnorm.Attributes.Add("class", "view")
                trdtdnd.Attributes.Add("class", "details")
                trtrnorm.Attributes.Add("class", "view")
                trtrdnd.Attributes.Add("class", "details")
            End If

            Try
                ddtype.SelectedValue = origtid
            Catch ex As Exception

            End Try
            Try
                ddpt.SelectedValue = origptid
            Catch ex As Exception

            End Try

            Try
                ddskill.SelectedValue = origskillid
            Catch ex As Exception

            End Try
            txtqty.Text = origqty
            txttr.Text = origttime
            Try
                ddeqstat.SelectedValue = origrdid
            Catch ex As Exception

            End Try
            txtrdt.Text = origrdt
            txtfreq.Text = origfreq
            Try
                ddtaskstat.SelectedValue = tid
            Catch ex As Exception

            End Try
        End If

            GetMeterDetails(skillid, rdid, fuid, eqid, origrdid, origskillid)
    End Sub
    
    Private Sub GetMeterDetails(ByVal skillid As String, ByVal rdid As String, ByVal fuid As String, ByVal eqid As String, _
                                ByVal rdido As String, ByVal skillido As String)
        Dim rd, func, skill, eqnum, rdo, skillo, mfid, mfido As String
        sql = "declare @rd varchar(50), @func varchar(50), @skill varchar(50), @eqnum varchar(50) "
        sql += "declare @rdo varchar(50), @skillo varchar(50), @mfid int, @mfido int "
        sql += "set @rd = (select status from pmstatus where statid = '" & rdid & "') "
        sql += "set @rdo = (select status from pmstatus where statid = '" & rdido & "') "
        sql += "set @func = (select func from functions where func_id = '" & fuid & "') "
        sql += "set @skill = (select skill from pmskills where skillid = '" & skillid & "') "
        sql += "set @skillo = (select skill from pmskills where skillid = '" & skillido & "') "
        sql += "set @eqnum = (select eqnum from equipment where eqid = '" & eqid & "') "
        sql += "select @rd as rd, @func as func, @skill as skill, @eqnum as eqnum, @rdo as rdo, @skillo as skillo"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try
        While dr.Read
            rd = dr.Item("rd").ToString
            rdo = dr.Item("rdo").ToString
            func = dr.Item("func").ToString
            skill = dr.Item("skill").ToString
            skillo = dr.Item("skillo").ToString
            eqnum = dr.Item("eqnum").ToString
        End While
        dr.Close()
        lblrd.Value = rd
        lblfunc.Value = func
        lblskill.Value = skill
        lbleqnum.Value = eqnum
        lblskillo.Value = skillo
        lblrdo.Value = rdo
    End Sub
    Private Sub GetLists()
        
        sql = "select ttid, tasktype " _
        + "from pmTaskTypes where tasktype <> 'Select' order by compid"
        dr = tasks.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataBind()
        dr.Close()
        ddtype.Items.Insert(0, New ListItem("Select"))
        ddtype.Items(0).Value = 0
        
        sid = lblsid.Value
        Dim scnt As Integer
        sql = "select count(*) from pmSiteSkills where siteid = '" & sid & "'"
        scnt = tasks.Scalar(sql)
        If scnt <> 0 Then
            sql = "select skillid, skill " _
            + "from pmSiteSkills where siteid = '" & sid & "'"
        Else
            sql = "select skillid, skill " _
            + "from pmSkills"
        End If
        dr = tasks.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("Select"))
        ddskill.Items(0).Value = 0


        sql = "select ptid, pretech " _
        + "from pmPreTech"
        dr = tasks.GetRdrData(sql)
        ddpt.DataSource = dr
        ddpt.DataTextField = "pretech"
        ddpt.DataValueField = "ptid"
        ddpt.DataBind()
        dr.Close()
        ddpt.Items.Insert(0, New ListItem("None"))
        ddpt.Items(0).Value = 0


        sql = "select statid, status " _
        + "from pmStatus"
        dr = tasks.GetRdrData(sql)
        ddeqstat.DataSource = dr
        ddeqstat.DataTextField = "status"
        ddeqstat.DataValueField = "statid"
        ddeqstat.DataBind()
        dr.Close()
        ddeqstat.Items.Insert(0, New ListItem("Select"))
        ddeqstat.Items(0).Value = 0
        
    End Sub
    

    
    Private Sub UpdateFailStats(ByVal comp As String)
        Dim fmcnt, fmcnta As Integer
        sql = "select count(*) from ComponentFailModes where comid = '" & comp & "'"
        Try
            fmcnt = tasks.Scalar(sql)
        Catch ex As Exception
            fmcnt = tasksadd.Scalar(sql)
        End Try

        sql = "select count(distinct failid) from pmTaskFailModes where comid = '" & comp & "'"
        Try
            fmcnta = tasks.Scalar(sql)
        Catch ex As Exception
            fmcnta = tasksadd.Scalar(sql)
        End Try

        fmcnta = fmcnt - fmcnta
    End Sub
    Private Sub UpdateFM()
        Dim ttid As String = lblpmtskid.Value
        sql = "usp_UpdateFM1 '" & ttid & "'"
        tasks.Update(sql)
    End Sub
    

    
    Private Sub GetoItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal oaid As String)
        ttid = lblpmtskid.Value
        Dim fcnt, fcnt2 As Integer
        If oaid <> "0" Then
            sql = "select count(*) from pmoTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and oaid = '" & oaid & "'"
        Else
            sql = "select count(*) from pmoTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and oaid is null"
        End If
        'sql = "select count(*) from pmoTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "'"
        fcnt = tasks.Scalar(sql)
        sql = "select count(*) from pmoTaskFailModes where taskid = '" & ttid & "'"
        fcnt2 = tasks.Scalar(sql)
        If fcnt2 >= 5 Then
            Dim strMessage As String = tmod.getmsg("cdstr410", "PMOptTasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf fcnt > 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr411", "PMOptTasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Try
                sql = "sp_addTaskFailureMode " & ttid & ", " & fi & ", '" & f & "', '" & comp & "', 'opt', '" & oaid & "'"
                tasks.Update(sql)
                Dim Item As ListItem
                Dim ofm, ofs, ofi As String
                Dim ipar As Integer = 0
                For Each Item In lbofailmodes.Items
                    ofs = Item.ToString
                    ipar = ofs.LastIndexOf("(")
                    If ipar <> -1 Then
                        ofs = Mid(ofs, 1, ipar)
                    End If

                    If Len(ofm) = 0 Then
                        ofm = ofs & "(___)"
                    Else
                        ofm += " " & ofs & "(___)"
                    End If
                Next
                ofm = tasks.ModString2(ofm)
                sql = "update pmtasks set ofm1 = '" & ofm & "' where pmtskid = '" & ttid & "'"
                tasks.Update(sql)
            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Sub RemoItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal oaid As String)
        ttid = lblpmtskid.Value
        Try
            sql = "sp_deloTaskFailureMode " & ttid & ", " & fi & ", '" & f & "','" & oaid & "'"
            tasks.Update(sql)
            Dim Item As ListItem
            Dim ofm, ofs, ofi As String
            Dim ipar As Integer = 0
            For Each Item In lbofailmodes.Items
                ofs = Item.ToString
                ipar = ofs.LastIndexOf("(")
                If ipar <> -1 Then
                    ofs = Mid(ofs, 1, ipar)
                End If

                If Len(ofm) = 0 Then
                    ofm = ofs & "(___)"
                Else
                    ofm += " " & ofs & "(___)"
                End If
            Next
            ofm = tasks.ModString2(ofm)
            sql = "update pmtasks set ofm1 = '" & ofm & "' where pmtskid = '" & ttid & "'"
            tasks.Update(sql)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ibReuse_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibReuse.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0


        Dim comp As String = lblcoid.Value
        tasks.Open()

        For Each Item In lbfailmaster.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            
        Catch ex As Exception
        End Try

        tasks.Dispose()
    End Sub
    Private Sub GetItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal oaid As String)
        ttid = lblpmtskid.Value
        Dim fcnt, fcnt2 As Integer
        If oaid <> "0" Then
            sql = "select count(*) from pmTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and oaid = '" & oaid & "'"
        Else
            sql = "select count(*) from pmTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and (oaid is null or oaid = '0')"
        End If

        fcnt = tasks.Scalar(sql)
        sql = "select count(*) from pmTaskFailModes where taskid = '" & ttid & "'"
        fcnt2 = tasks.Scalar(sql)

        If fcnt2 >= 5 Then
            Dim strMessage As String = tmod.getmsg("cdstr408", "PMOptTasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf fcnt > 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr409", "PMOptTasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Try
                sql = "sp_addTaskFailureMode " & ttid & ", " & fi & ", '" & f & "', '" & comp & "', 'dev','" & oaid & "'"
                tasks.Update(sql)
                Dim Item As ListItem
                Dim fm As String
                Dim ipar As Integer = 0
                For Each Item In lbfailmodes.Items
                    f = Item.Text.ToString
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                    If Len(fm) = 0 Then
                        fm = f & "(___)"
                    Else
                        fm += " " & f & "(___)"
                    End If
                Next
                fm = tasks.ModString2(fm)
                sql = "update pmtasks set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
                tasks.Update(sql)

            Catch ex As Exception

            End Try
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
        End If
    End Sub

    Protected Sub ibToTask_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibToTask.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0

        Dim comp As String = lblcoid.Value
        tasks.Open()
        Dim tst As String
        For Each Item In lbfaillist.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetItems(f, fi, comp, oa)
            End If
        Next

        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            UpdateFM()
            
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub

    Protected Sub ibFromTask_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibFromTask.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0

        Dim comp As String = lblcoid.Value
        tasks.Open()

        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                RemItems(f, fi, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            UpdateFM()
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub
    Private Sub RemItems(ByVal f As String, ByVal fi As String, ByVal oaid As String)
        ttid = lblpmtskid.Value
        Try
            sql = "sp_delTaskFailureMode '" & ttid & "', '" & fi & "','" & oaid & "'"
            tasks.Update(sql)
            Dim fm As String
            Dim Item As ListItem
            Dim ipar As Integer = 0
            For Each Item In lbfailmodes.Items
                f = Item.Text.ToString
                ipar = f.LastIndexOf("(")
                If ipar <> -1 Then
                    f = Mid(f, 1, ipar)
                End If
                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
            Next
            fm = tasks.ModString2(fm)
            sql = "update pmtasks set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
            tasks.Update(sql)
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ibfromo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibfromo.Click


        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0

        Dim comp As String = lblcoid.Value
        tasks.Open()

        For Each Item In lbCompFM.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If

                End If
                GetoItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            PopoTaskFailModes(comp)
            UpdateFailStats(comp)
            
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub

    Protected Sub ibtoo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtoo.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0

        Dim comp As String = lblcoid.Value
        tasks.Open()

        For Each Item In lbofailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If

                End If
                RemoItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopoTaskFailModes(comp)
            UpdateFailStats(comp)

        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub
End Class