﻿Imports System.Text
Imports System.Data.SqlClient
Public Class pmo123e2dnd
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim rp As New Utilities
    Dim comi As New mmenu_utils_a
    Dim eqid, eqnum, cid, fuid, start, skill, freq, stat, sknum, frnum, stnum, sid, proc, skillqty, oskillqty As String
    Dim meterid, dayfreq, meterunit, maxdays, meterfreq, meter, ustr, ghostoff, xstatus As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim coi As String = comi.COMPI

        If coi = "GSK" Then
            lblghostoff.Value = "yes"
        End If
        If Not IsPostBack Then

            lbltyp.Value = "orig"
            start = "yes" 'Request.QueryString("start").ToString '"yes" '
            lblstart.Value = start
            eqid = Request.QueryString("eqid").ToString '"164" '
            lbleqid.Value = eqid
            eqnum = Request.QueryString("eqnum").ToString
            lbleqnum.Value = eqnum
            'tdeq.InnerHtml = eqnum
            proc = Request.QueryString("proc").ToString '"164" '
            lblproc.Value = proc
            'tdpm.InnerHtml = "No PM Selected"
            txtqtyo.Text = "1"
            lblskillqty.Value = "1"
            Try
                ustr = Request.QueryString("ustr").ToString
                lbluser.Value = ustr
            Catch ex As Exception

            End Try
            downlabel.Attributes.Add("class", "graylabel")
            txtdowntime.Enabled = False

            totlabel.Attributes.Add("class", "graylabel")
            txttottime.Enabled = False

            rp.Open()
            GetStuff()
            GetLists()
            'PopProcedures(eqid)
            Try
                'ddproc.SelectedValue = proc
            Catch ex As Exception

            End Try
            'LoadTaskFields()
            rp.Dispose()

            'ddskillo.Attributes.Add("onchange", "disbelow();")
            'txtqtyo.Attributes.Add("onchange", "disbelow();")

            'ddeqstato.Attributes.Add("onchange", "disbelow();")
        Else
            If Request.Form("lblsubmit") = "savetasks" Then
                lblsubmit.Value = ""
                rp.Open()
                Dim delid, dodel As String
                delid = lbldeltaskid.Value
                dodel = lbldodel.Value
                If dodel = "yes" Then
                    lbldodel.Value = ""
                    deltask(delid)
                End If

                Dim copyid, docopy As String
                copyid = lblcopytaskid.Value
                docopy = lbldocopy.Value
                If docopy = "yes" Then
                    lbldocopy.Value = ""
                    copytask(copyid)
                End If

                SaveTasks()

                LoadCompFields()
                rp.Dispose()
            ElseIf Request.Form("lblsubmit") = "savettasks" Then
                lblsubmit.Value = ""
                rp.Open()
                'SaveTasks()
                Try
                    'SaveTTasks()
                Catch ex As Exception

                End Try
                'LoadTaskFields()
                LoadCompFields()
                rp.Dispose()
            ElseIf Request.Form("lblsubmit") = "orig" Or Request.Form("lblsubmit") = "rev" Then
                lblsubmit.Value = ""
                rp.Open()
                Try
                    'SaveTTasks()
                Catch ex As Exception

                End Try
                SaveTasks()
                'LoadTaskFields()
                LoadCompFields()
                rp.Dispose()
            ElseIf Request.Form("lblsubmit") = "changepm" Then
                lblsubmit.Value = ""
                rp.Open()
                'try to save prev tasks
                Try
                    SaveTasks()
                Catch ex As Exception

                End Try
                Try
                    LoadPM()
                Catch ex As Exception

                End Try
                rp.Dispose()
            ElseIf Request.Form("lblsubmit") = "reload" Then
                lblsubmit.Value = ""
                rp.Open()
                Try
                    LoadPM()
                Catch ex As Exception

                End Try
                rp.Dispose()
            ElseIf Request.Form("lblsubmit") = "pr" Then
                lblsubmit.Value = ""
                rp.Open()
                Try
                    SaveTasks()
                Catch ex As Exception

                End Try
                Try
                    'SaveTTasks()
                Catch ex As Exception

                End Try
                'PopProcedures(eqid)
                'LoadTaskFields()
                LoadCompFields()
                rp.Dispose()
            ElseIf Request.Form("lblsubmit") = "goback" Then
                lblsubmit.Value = ""
                rp.Open()
                Try
                    'SaveTasks()
                Catch ex As Exception

                End Try
                Try
                    'SaveTTasks()
                Catch ex As Exception

                End Try
                rp.Dispose()
                ReturnOpt()
            ElseIf Request.Form("lblsubmit") = "maketot" Then
                lblsubmit.Value = ""
                rp.Open()
                MakeTot()
                rp.Dispose()
            ElseIf Request.Form("lblsubmit") = "untot" Then
                lblsubmit.Value = ""
                rp.Open()
                UnTot()
                rp.Dispose()
            ElseIf Request.Form("lblsubmit") = "savetot" Then
                'not used?
                lblsubmit.Value = ""
                rp.Open()
                SaveTot()
                rp.Dispose()
            End If
            eqnum = lbleqnum.Value
            'tdeq.InnerHtml = eqnum
        End If
        'ddfu.Attributes.Add("onchange", "checksav();")
        'txtpaste.Attributes.Add("onKeyPress", "return paste(this,event)")

        txtfreqo.Attributes.Add("onkeyup", "filldownfreq(this.value);")

        ddskillo.Attributes.Add("onchange", "changepm('skill');")
        ddeqstato.Attributes.Add("onchange", "changepm('stat');")

        cbtot.Attributes.Add("onclick", "checktot();")
        txtqtyo.Attributes.Add("onkeyup", "filldown(this.value);")
    End Sub
    Private Sub copytask(ByVal copyid)
        Dim fuid As String = lblcopyfuid.Value
        Dim tasknum As String = lblcopytasknum.Value
        Dim ustr As String = lbluser.Value
        Dim stid As String = "0"
        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_copytask @pmtskid, @tasknum, @fuid, @ustr, @subtask"
        Dim param01 = New SqlParameter("@pmtskid", SqlDbType.VarChar)
        If copyid = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = copyid
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@tasknum", SqlDbType.VarChar)
        If tasknum = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = tasknum
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@fuid", SqlDbType.VarChar)
        If fuid = "" Then
            param03.Value = System.DBNull.Value
        Else
            param03.Value = fuid
        End If
        cmd.Parameters.Add(param03)
        Dim param04 = New SqlParameter("@ustr", SqlDbType.VarChar)
        If ustr = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = ustr
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@subtask", SqlDbType.VarChar)
        If stid = "" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = stid
        End If
        cmd.Parameters.Add(param05)

        rp.UpdateHack(cmd)
        lblcopytaskid.Value = ""
        lblcopyfuid.Value = ""
        lblcopytasknum.Value = ""
    End Sub
    Private Sub deltask(ByVal delid)
        Dim fuid As String = lbldelfuid.Value
        Dim tasknum As String = lbldeltasknum.Value
        sql = "usp_delpmtask '" & fuid & "', '" & delid & "', '" & tasknum & "'"
        rp.Update(sql)
        lbldeltaskid.Value = ""
        lbldelfuid.Value = ""
        lbldeltasknum.Value = ""
    End Sub
    Private Sub SaveTot()
        eqid = lbleqid.Value
        skill = lblskillid.Value
        skillqty = lblskillqty.Value
        freq = txtfreqo.Text 'lblfreq.Value
        stat = lblstatusid.Value
        sknum = lblskill.Value
        frnum = txtfreqo.Text 'lblfreq.Value
        stat = lblstatusid.Value
        stnum = lblstatus.Value

        Dim ndown As String = txtdowntime.Text

        Dim nchk As Long
        Try
            nchk = System.Convert.ToInt64(ndown)
        Catch ex As Exception
            Dim strMessage As String = "Total Down Time (mins) Must Be a Numeric Value!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        Dim ntot As String = txttottime.Text

        Dim ntchk As Long
        Try
            ntchk = System.Convert.ToInt64(ntot)
        Catch ex As Exception
            Dim strMessage As String = "Total Time (mins) Must Be a Numeric Value!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        sql = "update pmtottime set tottime = '" & ntot & "', downtime = '" & ndown & "' where eqid = '" & eqid & "' and skillid = '" & skill & "' " _
        + "and freq = '" & freq & "' and rdid = '" & stat & "' and skillqty = '" & skillqty & "'"
        rp.Update(sql)
        txttottime.Text = ntot
        lbloldtot.Value = ntot
        Try
            ddskillo.SelectedValue = skill
        Catch ex As Exception

        End Try
        Try

            txtfreqo.Text = freq
        Catch ex As Exception

        End Try
        Try
            ddeqstato.SelectedValue = stat
        Catch ex As Exception

        End Try
        txtqtyo.Text = skillqty
    End Sub
    Private Sub UnTot()
        eqid = lbleqid.Value
        skill = lblskillid.Value
        freq = lblfreqid.Value
        stat = lblstatusid.Value
        sknum = lblskill.Value
        frnum = txtfreqo.Text 'lblfreq.Value
        stat = lblstatusid.Value
        stnum = lblstatus.Value
        skillqty = txtqtyo.Text
        sql = "update equipment set usetot = '0' where eqid = '" & eqid & "'"
        rp.Update(sql)
        totlabel.Attributes.Add("class", "graylabel")
        txttottime.Enabled = False
        downlabel.Attributes.Add("class", "graylabel")
        txtdowntime.Enabled = False
        cbtot.Checked = False
        lblusetot.Value = "0"
        tdtotpms.Attributes.Add("class", "view")
        Try
            ddskillo.SelectedValue = skill
        Catch ex As Exception

        End Try
        Try

            txtfreqo.Text = freq
        Catch ex As Exception

        End Try
        Try
            ddeqstato.SelectedValue = stat
        Catch ex As Exception

        End Try
        txtqtyo.Text = skillqty
    End Sub
    Private Sub MakeTot()
        eqid = lbleqid.Value
        skill = lblskillid.Value
        freq = lblfreqid.Value
        stat = lblstatusid.Value
        sknum = lblskill.Value
        frnum = txtfreqo.Text 'lblfreq.Value
        stat = lblstatusid.Value
        stnum = lblstatus.Value
        skillqty = txtqtyo.Text
        sql = "update equipment set usetot = '1' where eqid = '" & eqid & "'"
        rp.Update(sql)

        totlabel.Attributes.Add("class", "label")
        txttottime.Enabled = True

        downlabel.Attributes.Add("class", "label")
        txtdowntime.Enabled = True

        'downlabel.Attributes.Add("class", "graylabel")
        'txtdowntime.Enabled = False


        cbtot.Checked = True


        lblusetot.Value = "1"

        'SaveTasks() '????
        'SaveTasks()
        'LoadTaskFields()
        'LoadCompFields()

        Try
            ddskillo.SelectedValue = skill
        Catch ex As Exception

        End Try
        Try

            txtfreqo.Text = freq
        Catch ex As Exception

        End Try
        Try
            ddeqstato.SelectedValue = stat
        Catch ex As Exception

        End Try
        txtqtyo.Text = skillqty
    End Sub
    Private Sub GetStuff()
        cbtot.Checked = False
        eqid = lbleqid.Value
        sql = "select * from equipment where eqid = '" & eqid & "'"
        dr = rp.GetRdrData(sql)
        Dim did, clid, chk, tot, loc As String
        While dr.Read
            cid = dr.Item("compid").ToString
            lblcid.Value = cid
            sid = dr.Item("siteid").ToString
            lblsid.Value = sid
            did = dr.Item("dept_id").ToString
            lbldid.Value = did
            clid = dr.Item("cellid").ToString
            lblclid.Value = clid
            eqid = dr.Item("eqid").ToString
            lbleqid.Value = eqid
            eqnum = dr.Item("eqnum").ToString
            lbleqnum.Value = eqnum
            'tdeq.InnerHtml = eqnum
            tot = dr.Item("usetot").ToString
            lblusetot.Value = tot
            loc = dr.Item("locid").ToString
            lbllid.Value = loc
            xstatus = dr.Item("xstatus").ToString
            lblxstatus.Value = xstatus
        End While
        dr.Close()
        If tot <> "1" Then
            'totlabel.Attributes.Add("class", "graylabel")
            'txttottime.Enabled = False
            lblusetot.Value = "0"
            cbtot.Checked = False
            lblusetot.Value = "0"
            tdtotpms.Attributes.Add("class", "view")
        Else
            totlabel.Attributes.Add("class", "label")
            downlabel.Attributes.Add("class", "label")
            cbtot.Checked = True
            lblusetot.Value = "1"
            tdtotpms.Attributes.Add("class", "view")
            txttottime.Enabled = True
            txtdowntime.Enabled = True
        End If
        If clid <> "0" And clid <> "" Then
            sql = "select cell_name from cells where cellid = '" & clid & "'"
            Dim cellname As String = rp.strScalar(sql)
            If cellname <> "No Cells" Then
                chk = "no"
            Else
                chk = "yes"
            End If
        Else
            chk = "no"
        End If
        lblchk.Value = chk
    End Sub
    Private Sub ReturnOpt()
        Dim tli, did, funid, clid, comid, cell, lid, typ As String
        tli = "3"
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        eqid = lbleqid.Value
        funid = lblfuid.Value
        clid = lblclid.Value
        Dim pmid, pmstr As String
        pmid = lbldocpmid.Value
        pmstr = lbldocpmstr.Value
        'If lblcoid.Value = "no" Then
        comid = "0"
        'Else
        'comid = lblcoid.Value
        'End If
        cell = lblchk.Value
        lid = lbllid.Value
        If did <> "" And lid <> "" Then
            typ = "dloc"
        ElseIf did <> "" And lid = "" Then
            typ = "reg"
        ElseIf did = "" And lid <> "" Then
            typ = "loc"
        End If
        Dim ustr As String = lbluser.Value
        'Response.Redirect("../appsopt/PM3OptMain.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & funid & "&comid=" & comid & "&chk=" & cell & "&pmid=" & pmid & "&pmstr=" & pmstr & "&app=opt&typ=reg")
        lblret.Value = "../appsopt/PM3OptMain.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & funid & "&comid=" & comid & "&chk=" & cell & "&pmid=" & pmid & "&pmstr=" & pmstr & "&app=opt&typ=" & typ & "&lid=" & lid & "&ustr=" & ustr

    End Sub
    Private Sub CheckTot()
        Dim tot As String = lblusetot.Value

    End Sub
    Private Sub LoadPM()
        lblstart.Value = "yes"
        eqid = lbleqid.Value
        skill = lblskillid.Value
        skillqty = lblskillqty.Value
        freq = txtfreqo.Text 'lblfreq.Value
        stat = lblstatusid.Value
        sknum = lblskill.Value
        frnum = txtfreqo.Text 'lblfreq.Value
        stat = lblstatusid.Value
        stnum = lblstatus.Value

        meterid = lblmeterid.Value
        meterunit = lblmeterunit.Value
        meterfreq = lblmeterfreq.Value
        maxdays = lblmaxdays.Value
        dayfreq = lbldayfreq.Value
        meter = lblmeter.Value

        If meterid = "" Then
            'tdpm.InnerHtml = sknum & "(" & skillqty & ") / " & frnum & " days / " & stnum
        Else
            'tdpm.InnerHtml = sknum & "(" & skillqty & ") / " & frnum & " days / " & stnum & " - " & meter & " / " & meterunit & " / " & meterfreq
        End If


        Dim tot As String = lblusetot.Value
        Dim ntot, nqty As String

        Dim frechk As Long
        Try
            frechk = System.Convert.ToInt64(frnum)
        Catch ex As Exception
            Dim strMessage As String = "Frequency Must Be a Numeric Value!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        Dim sqchk As Long
        Try
            sqchk = System.Convert.ToInt64(skillqty)
        Catch ex As Exception
            Dim strMessage As String = "Skill Qty Must Be a Numeric Value!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        'If meterid = "" Then
        'sql = "usp_checktottime '" & eqid & "','" & skill & "','" & sknum & "','" & freq & "', " _
        '+ "'" & frnum & "','" & stat & "','" & stnum & "','0','0','" & skillqty & "','0','0'"
        'rp.Update(sql)
        'End If

        If tot = "1" Then
            If meterid = "" Then
                sql = "select tottime, downtime from pmtottime where eqid = '" & eqid & "' and skillid = '" & skill & "' " _
            + "and freq = '" & freq & "' and rdid = '" & stat & "' and skillqty = '" & skillqty & "'"
            Else
                sql = "select tottime, downtime from pmtottime where eqid = '" & eqid & "' and skillid = '" & skill & "' " _
                + "and freq = '" & freq & "' and rdid = '" & stat & "' and skillqty = '" & skillqty & "' " _
                + "and meterid = '" & meterid & "' and meterfreq = '" & meterfreq & "' and maxdays = '" & maxdays & "'"
            End If

            dr = rp.GetRdrData(sql)
            While dr.Read
                ntot = dr.Item("tottime").ToString
                nqty = dr.Item("downtime").ToString
            End While
            dr.Close()
            txttottime.Text = ntot
            lbloldtot.Value = ntot
            txtdowntime.Text = nqty
            lblodown.Value = nqty

            totlabel.Attributes.Add("class", "label")
            txttottime.Enabled = True
            tdtotpms.Attributes.Add("class", "view")

            If stnum.ToLower = "down" Then
                downlabel.Attributes.Add("class", "label")
                txtdowntime.Enabled = True
            Else
                downlabel.Attributes.Add("class", "graylabel")
                txtdowntime.Enabled = False
            End If

        Else
            txttottime.Text = "0"
            lbloldtot.Value = "0"
            txtdowntime.Text = "0"
            lblodown.Value = "0"

            '***** disabling these to see if tottime can be added later

            'totlabel.Attributes.Add("class", "graylabel")
            'txttottime.Enabled = False

            'txttottime.Enabled = True

            'downlabel.Attributes.Add("class", "graylabel")
            'txtdowntime.Enabled = False

            'txtdowntime.Enabled = True

            tdtotpms.Attributes.Add("class", "view")

        End If
        GetLists()
        'PopProcedures(eqid)
        'LoadTaskFields()
        LoadCompFields()
        'Reload Top
        Try
            ddskillo.SelectedValue = skill
        Catch ex As Exception

        End Try
        Try

            txtfreqo.Text = freq
        Catch ex As Exception

        End Try
        Try
            ddeqstato.SelectedValue = stat
        Catch ex As Exception

        End Try
        txtqtyo.Text = skillqty

        lbloskillqty.Value = skillqty

        lbloskillid.Value = skill
        lbloskill.Value = sknum

        lblofreqid.Value = freq
        lblofreq.Value = frnum

        lblostatusid.Value = stat
        lblostatus.Value = stnum
    End Sub

    Private Sub SaveTTasks()
        eqid = lbleqid.Value
        Dim task As String
        Dim ttasks As String = lblcreatetasks.Value
        Dim ttasksarr() As String = ttasks.Split("~")
        sql = "delete from draghold where eqid = '" & eqid & "'"
        rp.Update(sql)
        If ttasksarr.Length > 1 Then
            Dim j As Integer
            For j = 0 To ttasksarr.Length - 1
                task = ttasksarr(j)
                Dim tcomarr() As String = task.Split(";")
                'com = tcomarr(0)
                task = tcomarr(1)
                task = rp.ModString2(task)
                sql = "insert into draghold (eqid, task) values ('" & eqid & "','" & task & "')"
                rp.Update(sql)
            Next
        End If

    End Sub
    Private Sub SaveTasks()

        eqid = lbleqid.Value
        fuid = lblfuid.Value
        'use prev values
        skill = lblskillid.Value
        skillqty = txtqtyo.Text
        sknum = lblskill.Value
        freq = lblfreqid.Value
        frnum = txtfreqo.Text 'lblfreq.Value
        stat = lblstatusid.Value
        stnum = lblstatus.Value

        meterid = lblmeterid.Value
        meterunit = lblmeterunit.Value
        meterfreq = lblmeterfreq.Value
        maxdays = lblmaxdays.Value
        dayfreq = lbldayfreq.Value
        meter = lblmeter.Value

        'use prev skill value to determine if anything was there that might need to be saved
        If skill <> "" Then
            Dim tot As String = lblusetot.Value
            Dim otot As String = lbloldtot.Value
            Dim ntot As String = txttottime.Text
            If ntot = "" Then
                ntot = "0"
            End If

            Dim ndown, odown As String
            odown = lblodown.Value
            ndown = txtdowntime.Text
            If ndown = "" Then
                ndown = "0"
            End If
            If tot = "1" Then
                If stnum.ToLower <> "down" Then
                    ndown = "0"
                End If
                Dim nchk As Long
                Try
                    nchk = System.Convert.ToInt64(ndown)
                Catch ex As Exception
                    Dim strMessage As String = "Total Down Time (mins) Must Be a Numeric Value!"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try

                Dim sqchk As Long
                Try
                    sqchk = System.Convert.ToInt64(skillqty)
                Catch ex As Exception
                    Dim strMessage As String = "Skill Qty Must Be a Numeric Value!"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
                Dim ntchk As Long
                Try
                    ntchk = System.Convert.ToInt64(ntot)
                Catch ex As Exception
                    Dim strMessage As String = "Total Time (mins) Must Be a Numeric Value!"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
                If meterid = "" Then
                    sql = "usp_checktottime '" & eqid & "','" & skill & "','" & sknum & "','" & freq & "', " _
                + "'" & frnum & "','" & stat & "','" & stnum & "','" & ntot & "','" & otot & "','" & skillqty & "','" & ndown & "','" & odown & "'"
                    rp.Update(sql)
                Else
                    sql = "usp_checktottime_mtr '" & eqid & "','" & skill & "','" & sknum & "','" & freq & "', " _
                + "'" & frnum & "','" & stat & "','" & stnum & "','" & ntot & "','" & otot & "','" & skillqty & "','" & ndown & "','" & odown & "', " _
                    + "'" & meterid & "','" & meterfreq & "','" & maxdays & "','" & meter & "','" & meterunit & "'"
                    rp.Update(sql)
                End If

            Else
                If meterid = "" Then
                    sql = "usp_checktottime '" & eqid & "','" & skill & "','" & sknum & "','" & freq & "', " _
                + "'" & frnum & "','" & stat & "','" & stnum & "','0','0','" & skillqty & "','" & ndown & "','" & odown & "'"
                    rp.Update(sql)
                Else
                    sql = "usp_checktottime_mtr '" & eqid & "','" & skill & "','" & sknum & "','" & freq & "', " _
                + "'" & frnum & "','" & stat & "','" & stnum & "','0','0','" & skillqty & "','" & ndown & "','" & odown & "', " _
                    + "'" & meterid & "','" & meterfreq & "','" & maxdays & "','" & meter & "','" & meterunit & "'"
                    rp.Update(sql)
                End If

            End If

            Dim com, task As String
            Dim tasks As String = lbl1.Value
            Dim tasksarr() As String = tasks.Split("~")
            Dim usr As String
            Try
                usr = HttpContext.Current.Session("username").ToString()
            Catch ex As Exception
                usr = lbluser.Value
            End Try

            Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
            Dim tst As Integer = tasksarr.Length
            Dim cidstr As String
            If tasksarr.Length >= 1 Then
                Dim i As Integer
                For i = 0 To tasksarr.Length - 1
                    Try
                        task = tasksarr(i)
                        Dim comarr() As String = task.Split(";")
                        com = comarr(0)
                        task = comarr(2)
                        task = rp.ModString2(task)
                        fuid = comarr(1)
                        'usp_AddDragTask] (
                        '@eqid int, @fuid int, @comid int, @uid varchar(50), @skillid int, @skill varchar(50),
                        '@statid int, @stat varchar(50), @freqid int, @freq varchar(50), @task varchar(500)) 
                        ghostoff = lblghostoff.Value
                        xstatus = lblxstatus.Value
                        If ghostoff = "yes" And xstatus = "yes" Then
                            If meterid = "" Then
                                sql = "usp_AddDragTask2x '" & eqid & "','" & fuid & "','" & com & "','" & ustr & "', " _
                           + "'" & skill & "','" & sknum & "','" & stat & "','" & stnum & "','" & freq & "', " _
                           + "'" & frnum & "','" & task & "','" & skillqty & "'"
                                rp.Update(sql)
                            Else
                                sql = "usp_AddDragTask_mtrx '" & eqid & "','" & fuid & "','" & com & "','" & ustr & "', " _
                                + "'" & skill & "','" & sknum & "','" & stat & "','" & stnum & "','" & freq & "', " _
                                + "'" & frnum & "','" & task & "','" & skillqty & "','" & meterid & "','" & meterfreq & "','" & maxdays & "'"
                                rp.Update(sql)
                            End If
                        Else
                            If meterid = "" Then
                                sql = "usp_AddDragTask2 '" & eqid & "','" & fuid & "','" & com & "','" & ustr & "', " _
                           + "'" & skill & "','" & sknum & "','" & stat & "','" & stnum & "','" & freq & "', " _
                           + "'" & frnum & "','" & task & "','" & skillqty & "'"
                                rp.Update(sql)
                            Else
                                sql = "usp_AddDragTask_mtr '" & eqid & "','" & fuid & "','" & com & "','" & ustr & "', " _
                                + "'" & skill & "','" & sknum & "','" & stat & "','" & stnum & "','" & freq & "', " _
                                + "'" & frnum & "','" & task & "','" & skillqty & "','" & meterid & "','" & meterfreq & "','" & maxdays & "'"
                                rp.Update(sql)
                            End If
                        End If


                    Catch ex As Exception

                    End Try

                Next
                lbl1.Value = ""
            End If

            Try
                ddskillo.SelectedValue = skill
            Catch ex As Exception

            End Try
            Try

                txtfreqo.Text = frnum
            Catch ex As Exception

            End Try
            Try
                ddeqstato.SelectedValue = stat
            Catch ex As Exception

            End Try
            txtqtyo.Text = skillqty
        End If



    End Sub
    Private Sub LoadCompFields()
        eqid = lbleqid.Value
        eqnum = lbleqnum.Value
        'fuid = ddfu.SelectedValue
        skill = lblskillid.Value
        sknum = lblskill.Value
        freq = lblfreqid.Value
        frnum = txtfreqo.Text 'lblfreq.Value
        stat = lblstatusid.Value
        stnum = lblstatus.Value
        skillqty = lblskillqty.Value

        meterid = lblmeterid.Value
        meterunit = lblmeterunit.Value
        meterfreq = lblmeterfreq.Value
        maxdays = lblmaxdays.Value
        dayfreq = lbldayfreq.Value
        meter = lblmeter.Value

        Dim sb As StringBuilder = New StringBuilder
        Dim jb As StringBuilder = New StringBuilder
        If skill <> "" Then
            'jb.Append("CreateDragContainer(")
            jb.Append("function CreateDragContainerOpen() { CreateDragContainer(")
            'sql = "select comid, compnum from components where func_id = '" & fuid & "'"
            'and c.comid is not null and c.comid <> 0
            If meterid = "" Then
                sql = "select distinct f.func_id, f.func, c.comid, c.compnum, c.compdesc, " _
            + "tcnt = (select count(*) from pmtasks t where t.comid = c.comid and t.origskillid = '" & skill & "' and " _
            + "t.origfreq = '" & frnum & "' and t.origrdid = '" & stat & "' and t.origqty = '" & skillqty & "'), " _
            + "ccnt = (select count(*) from components c1 where c1.func_id = f.func_id) " _
            + "from functions f " _
            + "left join components c on c.func_id = f.func_id " _
            + "where f.eqid = '" & eqid & "'  " _
            + "order by f.func_id, c.comid "
            Else
                sql = "select distinct f.func_id, f.func, c.comid, c.compnum, c.compdesc, " _
                + "tcnt = (select count(*) from pmtasks t where t.comid = c.comid and t.origskillid = '" & skill & "' and " _
                + "t.origfreq = '" & frnum & "' and t.origrdid = '" & stat & "' and t.origqty = '" & skillqty & "' " _
                + "and t.usemetero = '1' and t.meterido = '" & meterid & "' and t.meterfreqo = '" & meterfreq & "' " _
                + "and t.maxdayso = '" & maxdays & "'), " _
                + "ccnt = (select count(*) from components c1 where c1.func_id = f.func_id) " _
                + "from functions f " _
                + "left join components c on c.func_id = f.func_id " _
                + "where f.eqid = '" & eqid & "' " _
                + "order by f.func_id, c.comid "
            End If


            Dim comp, comid, funcid, funchold, func, task, comhold, pmtskid, compd As String
            Dim tcnt As Integer
            Dim ccnt As Integer
            Dim i As Integer = 1
            funchold = ""
            Dim ds As New DataSet
            Dim dt As New DataTable
            ds = rp.GetDSData(sql)
            dt = New DataTable
            dt = ds.Tables(0)
            Dim row As DataRow
            sb.Append("<img src=""../images/appbuttons/minibuttons/3300.gif"">")
            For Each row In dt.Rows
                comp = row("compnum").ToString
                compd = row("compdesc").ToString
                comid = row("comid").ToString
                funcid = row("func_id").ToString
                func = row("func").ToString
                tcnt = row("tcnt").ToString
                ccnt = row("ccnt").ToString
                'dr = rp.GetRdrData(sql)
                'While dr.Read
                'comp = dr.Item("compnum").ToString
                'comid = dr.Item("comid").ToString
                'funcid = dr.Item("func_id").ToString
                'func = dr.Item("func").ToString
                'task = dr.Item("otaskdesc").ToString
                i += 1
                If funcid <> funchold Then
                    If funchold <> "" Then

                        sb.Append("</div>")

                    End If
                    funchold = funcid
                    sb.Append("<div class=""DragContainer3""  id=""div,dfunc" & i & ",img" & i & """ >")
                    If ccnt > 0 Then
                        sb.Append("<img id='imgf" & i & "' ")
                        sb.Append("onclick=""fclose('dfunc" & i & "','imgf" & i & "');""")
                        sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif"">&nbsp;")
                    Else
                        sb.Append("<img id='imgf" & i & "' ")
                        sb.Append(" src=""../images/appbuttons/minibuttons/minusgray.gif"" ")
                        sb.Append(" onmouseover=""return overlib('No Components for this Function', ABOVE, LEFT)"" onmouseout='return nd()'>")
                    End If

                    sb.Append("<font class=""label"">" & func & "&nbsp;&nbsp;</font>")

                    sb.Append("<img src=""../images/appbuttons/minibuttons/minimag.gif"" onclick=""getfunctasks('" & funcid & "','" & func & "')""")
                    sb.Append("onmouseover=""return overlib('View Current Tasks for this Function', ABOVE, LEFT)"" onmouseout='return nd()'>")

                    sb.Append("&nbsp<img src=""../images/appbuttons/minibuttons/addlil.gif"" onclick=""getcompadd('" & tcnt & "','" & eqid & "','" & funcid & "')""")
                    sb.Append("onmouseover=""return overlib('Add a New Component for this Function', ABOVE, LEFT)"" onmouseout='return nd()'>")
                    'sb.Append("<img src=""../images/appbuttons/minibuttons/copylil.gif"" onclick=""getcompcopy('" & tcnt & "','" & eqid & "','" & funcid & "')""")
                    'sb.Append("onmouseover=""return overlib('Copy a Component for this Function', ABOVE, LEFT)"" onmouseout='return nd()'>")
                    sb.Append("</div>")
                    sb.Append("<img src=""../images/appbuttons/minibuttons/3300.gif"">")
                    sb.Append("<div class=""details"" id=""dfunc" & i & """>")
                    If i = 2 Then
                        lblcreatefobjs.Value += "dfunc" & i & ";imgf" & i
                    Else
                        lblcreatefobjs.Value += ","
                        lblcreatefobjs.Value += "dfunc" & i & ";imgf" & i
                    End If
                End If
                If ccnt > 0 Then
                    sb.Append("<div class=""DragContainer3 bluelabel10"" id=""div,DragContainer" & i & "-" & comid & "-" & funcid & ",img" & i & """>")
                    sb.Append("<img id='img" & i & "' ")
                    If tcnt > 0 Then
                        sb.Append("onclick=""fclose('DragContainer" & i & "-" & comid & "-" & funcid & "','img" & i & "','YES');""")
                        sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif"" onmouseover=""return overlib('Expand this Component', ABOVE, RIGHT)"" onmouseout=""return nd()"">&nbsp;<img id='img2" & i & "' ")
                    Else
                        sb.Append("onclick=""fclose('DragContainer" & i & "-" & comid & "-" & funcid & "','img" & i & "','NO');""")
                        sb.Append(" src=""../images/appbuttons/minibuttons/minusgray.gif"" onmouseover=""return overlib('Expand this Component', ABOVE, RIGHT)"" onmouseout=""return nd()"">&nbsp;<img id='img2" & i & "' ")
                    End If

                    sb.Append("onclick=""addnode('DragContainer" & i & "-" & comid & "-" & funcid & "','img" & i & "');""") '
                    If compd = "" Then
                        sb.Append(" src=""../images/appbuttons/minibuttons/plusgrn.gif"" onmouseover=""return overlib('Add a Task to this Component', ABOVE, RIGHT)"" onmouseout=""return nd()"">&nbsp;" & comp & "&nbsp;&nbsp;")
                    Else
                        sb.Append(" src=""../images/appbuttons/minibuttons/plusgrn.gif"" onmouseover=""return overlib('Add a Task to this Component', ABOVE, RIGHT)"" onmouseout=""return nd()"">&nbsp;" & comp & " - " & compd & "&nbsp;&nbsp;")
                    End If

                    sb.Append("<img src=""../images/appbuttons/minibuttons/minimag.gif"" onclick=""getcomptasks('" & comid & "','" & comp & "')""")
                    sb.Append("onmouseover=""return overlib('View Current Tasks for this Component', ABOVE, LEFT)"" onmouseout='return nd()'></font></div>")
                    sb.Append("<div class=""details"" id=""DragContainer" & i & "-" & comid & "-" & funcid & """>")
                    'Else
                    'sb.Append("<div class=""DragContainer3 bluelabel10"" id=""div,DragContainer" & i & "-" & comid & "-" & funcid & ",img" & i & """>")
                    'sb.Append("<img id='img" & i & "' ")
                    ''sb.Append("onclick=""fclose('DragContainer" & i & "-" & comid & "-" & funcid & "','img" & i & "');""")
                    'sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif"" onmouseover=""return overlib('No Tasks Available for this Component in this PM', ABOVE, RIGHT)"" onmouseout=""return nd()"">&nbsp;<img id='img2" & i & "' ")
                    'sb.Append("onclick=""addnode('DragContainer" & i & "-" & comid & "-" & funcid & "','img" & i & "');""") '
                    'sb.Append(" src=""../images/appbuttons/minibuttons/plusgrn.gif"" onmouseover=""return overlib('Add a Task to this Component', ABOVE, RIGHT)"" onmouseout=""return nd()"">&nbsp;" & comp & "&nbsp;&nbsp;")
                    'sb.Append("<img src=""../images/appbuttons/minibuttons/minimag.gif"" onclick=""getcomptasks('" & comid & "','" & comp & "')""")
                    'sb.Append("onmouseover=""return overlib('View Current Tasks for this Component', ABOVE, LEFT)"" onmouseout='return nd()'></font></div>")
                    'sb.Append("<div class=""details"" id=""DragContainer" & i & "-" & comid & "-" & funcid & """>")
                    'End If

                    If tcnt > 0 Then
                        Dim typ As String = lbltyp.Value
                        If typ = "orig" Then
                            If meterid = "" Then
                                sql = "select distinct pmtskid, tasknum, otaskdesc from pmtasks where comid = '" & comid & "' and origskillid = '" & skill & "' and " _
                                + "origfreq = '" & frnum & "' and origrdid = '" & stat & "' and origqty = '" & skillqty & "' "
                            Else
                                sql = "select distinct t.pmtskid, t.tasknum, t.otaskdesc from pmtasks t where t.comid = '" & comid & "' and " _
                                    + "t.origskillid = '" & skill & "' and " _
                                    + "t.origfreq = '" & frnum & "' and t.origrdid = '" & stat & "' and t.origqty = '" & skillqty & "' " _
                                    + "and t.usemetero = '1' and t.meterido = '" & meterid & "' and t.meterfreqo = '" & meterfreq & "' " _
                                    + "and t.maxdayso = '" & maxdays & "' "
                            End If

                        Else
                            sql = "select distinct tasknum, taskdesc from pmtasks where comid = '" & comid & "' and skillid = '" & skill & "' and " _
                                              + "freq = '" & frnum & "' and rdid = '" & stat & "' "
                        End If
                        Dim taskid As String
                        dr = rp.GetRdrData(sql)
                        While dr.Read
                            If typ = "orig" Then
                                task = dr.Item("otaskdesc").ToString
                            Else
                                task = dr.Item("taskdesc").ToString
                            End If
                            pmtskid = dr.Item("tasknum").ToString
                            taskid = dr.Item("pmtskid").ToString
                            sid = lblsid.Value
                            If task <> "" Then
                                sb.Append("<div id=""nodrag"" class=""NoDragBox""><table width=""560"" cellpadding=""2"" cellspacing=""0""><tr><td width=""500"" class=""plainlabel tdborder1"" valign=""top"">" & task & "</td>")
                                sb.Append("<td><img src=""../images/appbuttons/minibuttons/pencil12.gif"" onclick=""gettaskedit('" & pmtskid & "','" & funcid & "')""")
                                sb.Append("onmouseover=""return overlib('Edit This Task', ABOVE, LEFT)"" onmouseout='return nd()'>")
                                'eqid, fuid, coid, sid, pmtskid, eqnum, func, comp, task
                                'addsplb.gif
                                sb.Append("<br><img src=""../images/appbuttons/minibuttons/addmod.gif"" onclick=""getfail('" & eqid & "','" & funcid & "','" & comid & "','" & sid & "','" & taskid & "','" & eqnum & "','" & func & "','" & comp & "','" & pmtskid & "');""")
                                sb.Append("onmouseover=""return overlib('Generate Work Order for This Task', ABOVE, LEFT)"" onmouseout='return nd()'></td>")

                                sb.Append("<td><img src=""../images/appbuttons/minibuttons/magnifier.gif"" onclick=""getdt('" & taskid & "','" & eqid & "','" & comid & "')""")
                                sb.Append("onmouseover=""return overlib('View or Edit Task Details', ABOVE, LEFT)"" onmouseout='return nd()'>")
                                sb.Append("<br><img src=""../images/appbuttons/minibuttons/del.gif"" onclick=""deltask('" & taskid & "','" & funcid & "','" & pmtskid & "');""")
                                sb.Append("onmouseover=""return overlib('Delete This Task', ABOVE, LEFT)"" onmouseout='return nd()'></td>")

                                sb.Append("<td><img src=""../images/appbuttons/minibuttons/sgrid2.gif"" onclick=""getsgrid('" & pmtskid & "','" & funcid & "')""")
                                sb.Append("onmouseover=""return overlib('Add Sub Tasks to this Task', ABOVE, LEFT)"" onmouseout='return nd()'>")
                                sb.Append("<br><img src=""../images/appbuttons/minibuttons/copybg2.gif"" onclick=""copytask('" & taskid & "','" & funcid & "','" & pmtskid & "');""")
                                sb.Append("onmouseover=""return overlib('Copy This Task', ABOVE, LEFT)"" onmouseout='return nd()'></td>")
                                sb.Append("</tr></table></div>")
                            End If

                        End While
                        dr.Close()
                    End If
                    'sb.Append("<div class=""DragBox tdborder"" id=""dummy""  dragClass=""DragDragBox"" overClass=""OverDragBox""></div>")
                    sb.Append("</div>")
                    If i = 2 Then
                        jb.Append("document.getElementById('DragContainer1')")
                        jb.Append(",")
                        jb.Append("document.getElementById('DragContainer2-" & comid & "-" & funcid & "')")
                        lblcreateobjs.Value += "DragContainer2-" & comid & "-" & funcid & ";img" & i

                    End If
                    If (i > 1) Then
                        jb.Append(",")
                        jb.Append("document.getElementById('DragContainer" & i & "-" & comid & "-" & funcid & "')")
                        lblcreateobjs.Value += ","
                        lblcreateobjs.Value += "DragContainer" & i & "-" & comid & "-" & funcid & ";img" & i

                    End If
                End If
                sb.Append("<img src=""../images/appbuttons/minibuttons/3300.gif"">")
                sb.Append("<img src=""../images/appbuttons/minibuttons/3300.gif"">")


            Next
            'End While
            jb.Append("); }")

            ''sb.Append("<img id='img2' ")
            'sb.Append("onclick=""fclose('DragContainer3','img2');""")
            'sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif"">&nbsp;")
            'sb.Append("Comp2")
            'sb.Append("<div class=""details"" id=""DragContainer3"">")
            'sb.Append("<div class=""DragBox tdborder"" id=""dummy3""  dragClass=""DragDragBox"" overClass=""OverDragBox""></div>")
            'sb.Append("</div>")

            'tdcomps.InnerHtml = sb.ToString
            divcomp.InnerHtml = sb.ToString
            Dim strMessage As String = jb.ToString
            Utilities.CreateJS(Me, strMessage, "strKey1")
        End If

    End Sub
    Private Sub LoadTaskFields()
        eqid = lbleqid.Value
        Dim sb As StringBuilder = New StringBuilder
        Dim i As Integer
        Dim j As Integer = 0
        Dim task As String
        sql = "select task from draghold where eqid = '" & eqid & "'"
        dr = rp.GetRdrData(sql)
        While dr.Read
            j += 1
            task = dr.Item("task").ToString
            sb.Append("<div class=""DragBox tdborder"" id=""" & j & """  dragClass=""DragDragBox"" overClass=""OverDragBox"">" & task & "</div>")
            lblmove.Value += j & ";"
        End While
        dr.Close()
        For i = j To 6
            sb.Append("<div class=""DragBox tdborder"" id=""" & i & """  dragClass=""DragDragBox"" overClass=""OverDragBox""></div>")
            lblmove.Value += i & ";"
        Next
        'DragContainer1.InnerHtml = sb.ToString
    End Sub
    Private Sub GetLists()

        cid = lblcid.Value '"0" 


        sid = lblsid.Value '"12" 'HttpContext.Current.Session("dfltps").ToString() 'lblsid.Value
        Dim scnt As Integer
        sql = "select count(*) from pmsiteSkills where siteid = '" & sid & "'"
        scnt = rp.Scalar(sql)
        If scnt <> 0 Then
            sql = "select skillid, skill " _
            + "from pmsiteSkills where compid = '" & cid & "' and siteid = '" & sid & "'"
        Else
            sql = "select skillid, skill " _
            + "from pmskills where compid = '" & cid & "' order by compid"
        End If
        'orig
        dr = rp.GetRdrData(sql)
        ddskillo.DataSource = dr
        ddskillo.DataTextField = "skill"
        ddskillo.DataValueField = "skillid"
        Try
            ddskillo.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddskillo.Items.Insert(0, New ListItem("Select"))
        ddskillo.Items(0).Value = 0

        sql = "select statid, status " _
        + "from pmstatus where compid = '" & cid & "' or compid = '0' order by compid"
        'orig
        dr = rp.GetRdrData(sql)
        ddeqstato.DataSource = dr
        ddeqstato.DataTextField = "status"
        ddeqstato.DataValueField = "statid"
        Try
            ddeqstato.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddeqstato.Items.Insert(0, New ListItem("Select"))
        ddeqstato.Items(0).Value = 0
    End Sub
    

End Class