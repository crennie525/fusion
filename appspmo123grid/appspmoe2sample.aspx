﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="appspmoe2sample.aspx.vb"
    Inherits="lucy_r12.appspmoe2sample" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function pageLoad() {
            //$get('Panel1').style.visibility = 'hidden';
            //$find('dpe').add_move(onMove);
            //alert()
        } 
        

    </script>
</head>
<body>
    <form id="form1" runat="server">
   <asp:ScriptManager ID="ScriptManager1" runat="server"/>
   <ajaxToolkit:DragPanelExtender 
        ID="DragPanelExtender1" runat="server" 
        TargetControlID="Panel2"
        DragHandleID="lsthandle">
    </ajaxToolkit:DragPanelExtender>
    
<ajaxToolkit:RoundedCornersExtender                 
ID="RoundedCornersExtender1"                
runat="server"                
TargetControlID="Panel2"                
Corners="All"                
Color="blue"                
Radius="5">            
</ajaxToolkit:RoundedCornersExtender>
    <div style="width: 435px; height: 306px;" class="details">
     <br />
     <asp:Panel ID="Panel1" runat="server" 
      Height="500px" Width="50px">
       <table cellspacing="0" cellpadding="3" width="500"  border="1">
            <tr >
                <td class="labelwht" bgcolor="blue" height="20" id="lsthandle0" runat="server" valign="top">
                    &nbsp;<asp:Label ID="lang268" runat="server">Task Options</asp:Label>
                </td>
                <td align="right" bgcolor="blue" >
                    <img onclick="closelst();" height="18" alt="" src="../images/close.gif" width="18" />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="plainlabel" height="20">
                    sample text here
                </td>
            </tr>
            <tr>
                <td colspan="2" class="plainlabel">
                    sample text here
                </td>
            </tr>
            <tr>
                <td colspan="2" class="plainlabel">
                    sample text here
                </td>
            </tr>
        </table>
     </asp:Panel>
    </div>
    <div style="width: 435px; height: 306px;">
    <asp:Panel ID="Panel2" runat="server" 
      Height="300px" Width="300px">
      <table cellspacing="0" cellpadding="3" width="300" bgcolor="white">
      <tr bgcolor="blue" >
                <td class="labelwht" height="20" id="lsthandle" runat="server">
                    <asp:Label ID="Label1" runat="server">Task Options</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closelst();" height="18" alt="" src="../images/close.gif" width="18">
                </td>
            </tr>
            <tr>
                <td colspan="2" class="plainlabel" height="20">
                    sample text here
                </td>
            </tr>
            <tr>
                <td colspan="2" class="plainlabel">
                    sample text here
                </td>
            </tr>
            <tr>
                <td colspan="2" class="plainlabel">
                    sample text here
                </td>
            </tr>
      </table>
</asp:Panel>
    </div>
    

    </form>
</body>
</html>
