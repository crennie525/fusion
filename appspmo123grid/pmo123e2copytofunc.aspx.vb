﻿Imports System.Data.SqlClient
Imports System.Text

Public Class pmo123e2copytofunc
    Inherits System.Web.UI.Page
    Dim pmo As New Utilities
    Dim tmod As New transmod
    Dim sql As String
    Dim dr As SqlDataReader
    Dim eqid, fuid, coid, who, pmtskid, newfuid, newcoid, sid, ustr As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            eqid = Request.QueryString("eqid").ToString
            fuid = Request.QueryString("fuid").ToString
            coid = Request.QueryString("coid").ToString
            who = Request.QueryString("who").ToString
            pmtskid = Request.QueryString("pmtskid").ToString
            Try
                ustr = Request.QueryString("usrname").ToString
            Catch ex As Exception
                ustr = Request.QueryString("ustr").ToString
            End Try

            lblustr.Value = ustr
            lblsid.Value = sid
            lbleqid.Value = eqid
            lblfuid.Value = fuid
            lblcoid.Value = coid
            lblwho.Value = who
            lblpmtskid.Value = pmtskid
            pmo.Open()
            getdets(fuid, coid)
            If who = "func" Or who = "funcdnd" Then
                getfuncs(eqid)
                trfunc.Attributes.Add("class", "view")
                trfuncdiv.Attributes.Add("class", "view")
            ElseIf who = "comp" Or who = "compdnd" Then
                getcomps(fuid, "orig")
                trcomp.Attributes.Add("class", "view")
                trcompdiv.Attributes.Add("class", "view")
            End If
            If who = "funcdnd" Then
                lblcheckdnd.Value = "yes"
            End If
            pmo.Dispose()
        Else
            If Request.Form("lblsubmit") = "checkcnt" Then
                lblsubmit.Value = ""
                newfuid = lblnewfuid.Value
                pmo.Open()
                checkcompcnt(newfuid)
                pmo.Dispose()
            ElseIf Request.Form("lblsubmit") = "getnewcomp" Then
                lblsubmit.Value = ""
                newfuid = lblnewfuid.Value
                pmo.Open()
                getcomps(newfuid, "new")
                pmo.Dispose()
                trfunc.Attributes.Add("class", "details")
                trfuncdiv.Attributes.Add("class", "details")
                trcomp.Attributes.Add("class", "details")
                trfuncsel.Attributes.Add("class", "view")
                tdfuncsel.InnerHtml = lblnewfunc.Value
                trcomp.Attributes.Add("class", "view")
                trcompdiv.Attributes.Add("class", "view")
            ElseIf Request.Form("lblsubmit") = "checkcopy" Then
                lblsubmit.Value = ""
                'in comp mode copies task to new component *required to be selected
                'in func mode copies task to new component if selected - just task to new function if not
                pmo.Open()
                copytasktocomp()
                pmo.Dispose()
                lblret.Value = "ok"
            ElseIf Request.Form("lblsubmit") = "justcopy" Then
                lblsubmit.Value = ""
                'this just copies task alone to new function
                pmo.Open()
                copytasktocomp()
                pmo.Dispose()
                lblret.Value = "ok"
            End If

            tdcurrcomp.InnerHtml = lblcurrcomp.Value
            tdcurrfunc.InnerHtml = lblcurrfunc.Value
        End If
    End Sub
    Private Sub copytasktocomp()
        '[dbo].[usp_copytasktonew](@pmtskid int, @fuid int, @coid int, @comp varchar(400), @ustr varchar(50), @origcoid int)
        Dim ci As String = lblnewcoid.Value
        Dim co As String = lblnewcomp.Value
        Dim cd As String = lblnewcompdesc.Value
        Dim comp As String = co & " - " & cd
        fuid = lblnewfuid.Value
        If fuid = "" Then
            fuid = lblfuid.Value
        End If

        coid = lblcoid.Value
        ustr = lblustr.Value
        pmtskid = lblpmtskid.Value
        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_copytasktonew @pmtskid, @fuid, @coid, @comp, @ustr, @origcoid"
        Dim param01 = New SqlParameter("@pmtskid", SqlDbType.VarChar)
        If pmtskid = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = pmtskid
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@fuid", SqlDbType.VarChar)
        If fuid = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = fuid
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@coid", SqlDbType.VarChar)
        If ci = "" Then
            param03.Value = System.DBNull.Value
        Else
            param03.Value = ci
        End If
        cmd.Parameters.Add(param03)
        Dim param04 = New SqlParameter("@comp", SqlDbType.VarChar)
        If comp = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = comp
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@ustr", SqlDbType.VarChar)
        If ustr = "" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = ustr
        End If
        cmd.Parameters.Add(param05)
        Dim param06 = New SqlParameter("@origcoid", SqlDbType.VarChar)
        If coid = "" Then
            param06.Value = System.DBNull.Value
        Else
            param06.Value = coid
        End If
        cmd.Parameters.Add(param06)

        pmo.UpdateHack(cmd)
    End Sub
    Private Sub copytasktofunc()

    End Sub
    Private Sub checkcompcnt(ByVal fuid As String)
        Dim compcnt As Integer = 0
        Dim dndchk As String = lblcheckdnd.Value
        sql = "select count(*) from components where func_id = '" & fuid & "'"
        compcnt = pmo.Scalar(sql)
        lblcompcnt.Value = compcnt
        If compcnt > 0 Then
            trfunc.Attributes.Add("class", "details")
            trfuncdiv.Attributes.Add("class", "details")
            trcompq.Attributes.Add("class", "view")
            If dndchk = "yes" Then
                trcompdnd.Attributes.Add("class", "view")
            Else
                trcompdnd.Attributes.Add("class", "details")
            End If
            trfuncsel.Attributes.Add("class", "view")
            tdfuncsel.InnerHtml = lblnewfunc.Value
        Else
            trfunc.Attributes.Add("class", "details")
            trfuncdiv.Attributes.Add("class", "details")
            trcompq.Attributes.Add("class", "details")

            trcompdnd.Attributes.Add("class", "details")

            trfuncsel.Attributes.Add("class", "view")
            tdfuncsel.InnerHtml = lblnewfunc.Value
            trsubmit.Attributes.Add("class", "view")
        End If
    End Sub
    Private Sub getcomps(ByVal fuid As String, ByVal typ As String)
        'fuid = lblfuid.Value
        Dim ci, co, cd As String
        Dim sb As New StringBuilder
        sql = "select comid, compnum, compdesc from components where func_id = '" & fuid & "' order by crouting"
        dr = pmo.GetRdrData(sql)
        sb.Append("<table>")
        While dr.Read
            ci = dr.Item("comid").ToString
            co = dr.Item("compnum").ToString
            cd = dr.Item("compdesc").ToString
            sb.Append("<tr><td class=""plainlabel"">")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getdco('" & ci & "','" & co & "','" & cd & "')"">")
            sb.Append(co & " - " & cd & "</a>")
            sb.Append("</td></tr>")
            If typ = "orig" Then

            End If
        End While
        dr.Close()
        sb.Append("</table>")
        divco.InnerHtml = sb.ToString
    End Sub
    Private Sub getfuncs(ByVal eqid As String)
        sql = "select func_id, func from functions where eqid = '" & eqid & "' order by routing, func"
        dr = pmo.GetRdrData(sql)
        Dim sb As New StringBuilder
        sb.Append("<table>")
        Dim fi, fu As String
        While dr.Read
            fi = dr.Item("func_id").ToString
            fu = dr.Item("func").ToString
            sb.Append("<tr><td>")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getdfu('" & fi & "','" & fu & "')"">")
            sb.Append(fu)
            sb.Append("</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        divfu.InnerHtml = sb.ToString
    End Sub
    Private Sub getdets(ByVal fuid As String, ByVal coid As String)
        Dim func As String = ""
        Dim comp As String = ""
        sql = "select f.func, c.compnum from functions f left join components c on c.func_id = f.func_id " _
            + "where f.func_id = '" & fuid & "' and c.comid = '" & coid & "'"
        dr = pmo.GetRdrData(sql)
        While dr.Read
            func = dr.Item("func").ToString
            comp = dr.Item("compnum").ToString
        End While
        dr.Close()
        lblcurrcomp.Value = comp
        lblcurrfunc.Value = func
        tdcurrcomp.InnerHtml = comp
        tdcurrfunc.InnerHtml = func
    End Sub
End Class