﻿Imports System.Data.SqlClient
Imports System.Text
Public Class pmo123e2profile
    Inherits System.Web.UI.Page
    Dim pmo As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim eqid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            pmo.Open()
            checkprofiles(eqid)
            getprofiles(eqid)
            pmo.Dispose()
        Else
            If Request.Form("lblsubmit") = "ret" Then
                eqid = lbleqid.Value
                pmo.Open()
                checkprofiles(eqid)
                getprofiles(eqid)
                pmo.Dispose()
                lblsubmit.Value = ""
            End If
        End If
    End Sub
    Private Sub checkprofiles(ByVal eqid As String)
        Dim pcnt As Integer = 0
        sql = "select count(*) from gridprofiles where eqid = '" & eqid & "'"
        pcnt = pmo.Scalar(sql)
        If pcnt = 0 Then
            sql = "insert into gridprofiles " _
                + "(eqid, ttid, tasktype, ptid, pretech, skillid, skill, skillqty, freqid, freq, rdid, rd, " _
                + "tottime, downtime, ismeter, meterid, meter, meterunit, meterfreq, maxdays,  fixed) " _
                + "select distinct '" & eqid & "', p.ttid, p.tasktype, p.ptid, p.pretech, p.skillid, p.skill, p.qty, 0, p.freq, p.rdid, p.rd,  " _
                + "0, p.rdt, p.usemeter, p.meterid, m.meter, m.unit, p.meterfreq, p.maxdays, p.fixed " _
                + "from pmtasks p left join meters m on m.meterid = p.meterid where p.eqid = '" & eqid & "'"
            pmo.Update(sql)
        End If
    End Sub
    Private Sub getprofiles(ByVal eqid As String)
        'eqid, ttid, tasktype, ptid, pretech, skillid, skill, skillqty, freqid, freq, rdid, rd, tottime, downtime, ismeter, meterid, meter, meterunit, meterfreq, maxdays,  fixed
        Dim pcnt As Integer = 0
        sql = "select count(*) from gridprofiles where eqid = '" & eqid & "'"
        pcnt = pmo.Scalar(sql)
        Dim rowflag As Integer = 0
        Dim bg As String = ""
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        sb.Append("<tr>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""50"">Select</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""110"">Task Type</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""140"">PreTech</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""140"">Skill</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""25"">Qty</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""60"">Frequency</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""40"">Fixed</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""140"">Meter</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""60"">Frequency</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""50"">Max Days</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""70"">R/D</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""60"">Down Time</td>")
        sb.Append("</tr>")
        If pcnt = 0 Then
            sb.Append("<tr><td class=""plainlabelred"" align=""center"" colspan=""12"">No Records Found</td></tr>")
            sb.Append("</table>")
            eqdiv.InnerHtml = sb.ToString
        Else
            Dim ttid, tasktype, ptid, pretech, skillid, skill, qty, rdid, rd, rdt, freq, meter, meterid, meterfreq, maxdays, fixed, unit As String
            Dim profile As String = ""
            sql = "select distinct ttid, tasktype, ptid, pretech, skillid, skill, skillqty, rdid, rd, freq, meter, meterid, " _
                + "meterfreq, maxdays, fixed, downtime, meterunit from gridprofiles where eqid = '" & eqid & "'"


            dr = pmo.GetRdrData(sql)
            While dr.Read
                If rowflag = 0 Then
                    bg = "transrow"
                    rowflag = 1
                Else
                    bg = "transrowblue"
                    rowflag = "0"
                End If
                ttid = dr.Item("ttid").ToString
                tasktype = dr.Item("tasktype").ToString
                ptid = dr.Item("ptid").ToString
                pretech = dr.Item("pretech").ToString
                skillid = dr.Item("skillid").ToString
                skill = dr.Item("skill").ToString
                qty = dr.Item("skillqty").ToString
                rdid = dr.Item("rdid").ToString
                rd = dr.Item("rd").ToString
                rdt = dr.Item("downtime").ToString
                freq = dr.Item("freq").ToString
                meter = dr.Item("meter").ToString
                meterid = dr.Item("meterid").ToString
                meterfreq = dr.Item("meterfreq").ToString
                maxdays = dr.Item("maxdays").ToString
                fixed = dr.Item("fixed").ToString
                unit = dr.Item("meterunit").ToString
                If rdt = "" Then
                    rdt = "0"
                End If

                profile = ttid & "~" & tasktype & "~"
                profile += ptid & "~" & pretech & "~"
                profile += skillid & "~" & skill & "~"
                profile += qty & "~"
                profile += rdid & "~" & rd & "~"
                profile += freq & "~"
                profile += meterid & "~" & meterfreq & "~"
                profile += maxdays & "~" & fixed & "~"
                profile += rdt & "~" & unit

                sb.Append("<tr>")
                sb.Append("<td class=""" & bg & " plainlabel""><a href=""#"" onclick=""getprofile('" & profile & "');"">Select</a></td>")
                sb.Append("<td class=""" & bg & " plainlabel"">" & tasktype & "</td>")
                sb.Append("<td class=""" & bg & " plainlabel"">" & pretech & "</td>")
                sb.Append("<td class=""" & bg & " plainlabel"">" & skill & "</td>")
                sb.Append("<td class=""" & bg & " plainlabel"" align=""center"">" & qty & "</td>")
                sb.Append("<td class=""" & bg & " plainlabel"">" & freq & "</td>")
                sb.Append("<td class=""" & bg & " plainlabel"" align=""center"">" & fixed & "</td>")
                sb.Append("<td class=""" & bg & " plainlabel"">" & meter & "</td>")
                sb.Append("<td class=""" & bg & " plainlabel"" align=""center"">" & meterfreq & "</td>")
                sb.Append("<td class=""" & bg & " plainlabel"" align=""center"">" & maxdays & "</td>")
                sb.Append("<td class=""" & bg & " plainlabel"">" & rd & "</td>")
                sb.Append("<td class=""" & bg & " plainlabel"" align=""center"">" & rdt & "</td>")
                sb.Append("</tr>")
            End While
            dr.Close()
            sb.Append("</table>")
            eqdiv.InnerHtml = sb.ToString
        End If
        

    End Sub
End Class