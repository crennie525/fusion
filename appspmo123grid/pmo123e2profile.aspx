﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123e2profile.aspx.vb" Inherits="lucy_r12.pmo123e2profile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Task Profiles</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function addprofile() {
            var eqid = document.getElementById("lbleqid").value;
            var eReturn = window.showModalDialog("pmo123e2profileadddialog.aspx?eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
            if (eReturn) {
                document.getElementById("lblsubmit").value = "ret";
                document.getElementById("form1").submit();
            }
        }
        function getprofile(profile) {
            window.parent.handlereturn(profile);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td>
    <table>
    <tr>
    <td class="plainlabelblue">Add a Task Profile for use with this Equipment Record</td>
    <td>&nbsp;<img alt="" src="../images/appbuttons/minibuttons/addmod.gif" onclick="addprofile();" /></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td>
    <div id="eqdiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; OVERFLOW: auto; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid; HEIGHT: 300px; WIDTH: 960px;"
							runat="server"></div>
    </td>
    </tr>
    </table>
    </div>
    <input type="hidden" id="lblfuid" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />

    </form>
</body>
</html>
