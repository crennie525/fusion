﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123e2h1.aspx.vb" Inherits="lucy_r12.pmo123e2h1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Drag and Drop Help</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <style type="text/css">
        .inslabel
        {
            font-family: arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
        }
        .inslabelblu
        {
            font-family: arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: blue;
        }
        .inslabelb
        {
            font-family: arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="860">
    <tr>
					<td class="inslabelblu" width="860">
						Did You Know?<br /><br />
					</td>
				</tr>
				<tr>
					<td class="inslabel">
						You can Drag and Drop any selected text from an open external document into an active text box in Fusion.<br /><br />
                        Simply highlight the desired text within your external document with your mouse, click on the highlighted area, 
                        move your mouse to the desired textbox, and release the mouse button.<br /><br /> 
					</td>
				</tr>
                <tr>
					<td class="inslabelblu">
						Why Does My Microsoft Office Document Open in an External Window?<br /><br />
					</td>
				</tr>
                <tr>
					<td class="inslabel">
						In most cases this is because your document was created or saved in a newer version or Microsoft Office.<br /><br />
                        One alernative is to save your Microsoft Office Document as a Microsoft Office 97-2003 Document.<br />
                        Another alternative, if it is available to you, is to save your Microsoft Office Document as a PDF Document.<br /><br />
                        
					</td>
				</tr>
                 <tr>
					<td class="inslabelblu">
						When I select a Document a yellow bar appears at the top of my screen. When I right-click and choose to Download the Document
                        I am forced to re-select my Document from the drop down list.<br /><br />
					</td>
				</tr>
                <tr>
					<td class="inslabel">
						This is a feature in newer versions of Internet Explorer.<br />
                        To Enable Automatic Prompting;<br /><br />
                        In Internet Exploer go to your Tools Menu and select Internet Options.<br />
                        Select the Security Tab.<br />
                        Click the Custom Level Button.<br />
                        Scroll down to the Downloads section.<br />
                        Under Automatic prompting for file downlaods click the Enable Radio Button.<br />
                        Click the OK Button at the bottom of this Tab.<br />
                        When you are prompted with a pop-up screen reading "Are you sure you want to change the settings for this zone?" Click Yes.<br />
                        Click the OK Button and exit Internet Options.<br /><br />
					</td>
				</tr>
                <tr>
					<td class="inslabelblu">
						How Can I Use Drag and Drop in the All In One Grid View?<br /><br />
					</td>
				</tr>
                <tr>
					<td class="inslabel">
						To activate Standard Drag and Drop Mode check the box next to the Drag and Drop icon.<br /><br />
                        To return to the All In One Grid View from Standard Drag and Drop Mode un-check the box next to the Drag and Drop icon.<br /><br /> 
					</td>
				</tr>
                <tr>
					<td class="inslabelblu">
						How Do I return to Grid View from Standard Drag and Drop Mode?<br /><br />
					</td>
				</tr>
                <tr>
					<td class="inslabel">
                        To return to the All In One Grid View from Standard Drag and Drop Mode un-check the box next to the Drag and Drop icon.<br /><br /> 
					</td>
				</tr>
                <tr>
					<td class="inslabelblu">
						Why Doesn't My Document Mode Radio Button Selection Change to Side-By-Side when I Switch to Standard Drag and Drop Mode?<br /><br />
					</td>
				</tr>
                <tr>
					<td class="inslabel">
						The Side by Side and Top\Bottom Screen Selections are the Default for Grid View Only<br /><br />
                        If you selected Standard Drag and Drop Mode with the Top\Bottom Screen Selection Check Box Checked 
                        you will be returned to the Top\Bottom Screen mode. The same applies if Side by Side mode 
                        is selected before entering Standard Drag and Drop Mode.<br /><br /> 
					</td>
				</tr>
                 
			</table>
    </div>
    </form>
</body>
</html>
