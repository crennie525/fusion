﻿Imports System.Data.SqlClient
Public Class appspmo123e2excelout
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim sqa As New Utilities
    Dim sql, table As String
    Dim sessid, fuid, coid, who As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
        If Not IsPostBack Then
            sessid = Request.QueryString("sessid").ToString
            fuid = Request.QueryString("fuid").ToString
            coid = Request.QueryString("coid").ToString
            who = Request.QueryString("who").ToString
            sqa.Open()
            ExportDG(sessid, fuid, coid, who)
            sqa.Dispose()
        End If
    End Sub
    Private Sub ExportDG(ByVal sessid As String, ByVal fuid As String, ByVal coid As String, ByVal who As String)
        BindExport(sessid, fuid, coid, who)
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
    Private Sub BindExport(ByVal sessid As String, ByVal fuid As String, ByVal coid As String, ByVal who As String)
        'Dim osql As String
        sql = "usp_exceloutgrid '" & sessid & "','" & fuid & "','" & coid & "','" & who & "'"
        'osql = sqa.strScalar(sql)
        Dim ds As New DataSet
        ds = sqa.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
    End Sub

End Class