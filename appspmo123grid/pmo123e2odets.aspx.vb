﻿Imports System.Data.SqlClient
Public Class pmo123e2odets
    Inherits System.Web.UI.Page
    Dim tasks As New Utilities
    Dim tasksadd As New Utilities
    Dim tmod As New transmod
    Public dr As SqlDataReader
    Dim ds As DataSet
    Dim sql As String
    Dim sid, ttid, eqid, who, coid, ustr, sqty As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblro.Value = "0"
            lblcid.Value = "0"
            sid = Request.QueryString("sid").ToString
            ttid = Request.QueryString("ttid").ToString
            eqid = Request.QueryString("eqid").ToString
            coid = Request.QueryString("coid").ToString
            who = Request.QueryString("who").ToString
            ustr = Request.QueryString("ustr").ToString
            sqty = Request.QueryString("sqty").ToString
            lblsid.Value = sid
            lblpmtskid.Value = ttid
            lbleqid.Value = eqid
            lblcoid.Value = coid
            lblco.Value = coid
            lblwho.Value = who
            lblustr.Value = ustr
            lblskillqty.Value = sqty
            tasks.Open()
            GetLists()
            getrev(ttid, who)
            tasks.Dispose()
        Else
            If Request.Form("lblsubmit") = "sav" Then
                lblsubmit.Value = ""
                ttid = lblpmtskid.Value
                who = lblwho.Value
                tasks.Open()
                saverev(ttid, who)
                tasks.Dispose()
            End If
        End If
    End Sub
    Private Sub saverev(ByVal ttid As String, ByVal who As String)
        Dim freq, rdid, qty, skillid, ismeter, meterid, fixed, mfid, tid, ptid, lotoid, conid, ttime, rdt As String
        Dim origfreq, origrdid, origqty, origskillid, meterido, fixedo, mfido, origtid, origptid, origttime, origrdt As String
        Dim fuid, taskstatusid, taskstat, rd, func, skill, eqnum, rdo, skillo As String
        Dim typ, typstr, frestr, tr As String
       

        qty = txtqty.Text
        If Len(qty) = 0 Then
            qty = "1"
        End If


        tr = txttr.Text
        If Len(tr) = 0 Then
            tr = "0"
        End If
        
       
        rdt = txtrdt.Text
        If rdt = "" Then
            rdt = "0"
        End If
        
        Dim cmd As New SqlCommand
        cmd.CommandText = "update pmtasks set qty = @qty, ttime = @tr, rdt = @rdt where pmtskid = @ttid"

        Dim param = New SqlParameter("@ttid", SqlDbType.Int)
        param.Value = ttid
        cmd.Parameters.Add(param)
        Dim param02 = New SqlParameter("@qty", SqlDbType.Int)
        If qty = "" Then
            param02.Value = "1"
        Else
            param02.Value = qty
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@tr", SqlDbType.Decimal)
        If tr = "" Then
            param03.Value = "0"
        Else
            param03.Value = tr
        End If
        cmd.Parameters.Add(param03)
        Dim param17 = New SqlParameter("@rdt", SqlDbType.Decimal)
        If rdt = "" Then
            param17.Value = "0"
        Else
            param17.Value = rdt
        End If
        cmd.Parameters.Add(param17)

        Dim tpm As Integer
        Try
            tpm = tasks.ScalarHack(cmd)
        Catch ex As Exception
            tpm = tasksadd.ScalarHack(cmd)
        End Try

        'Dim ismeter As String = lblmeterid.Value
        'If tpm = 1 And ismeter <> "1" Then
        'lbltpmalert.Value = "yes"
        'Else
        'lbltpmalert.Value = "no"
        'End If
    End Sub
    Private Function CheckLock(ByVal eqid As String) As String
        Dim lock As String
        sql = "select locked, lockedby from equipment where eqid = '" & eqid & "'"
        Try
            dr = tasks.GetRdrData(sql)
            While dr.Read
                lock = dr.Item("locked").ToString
                lbllock.Value = dr.Item("locked").ToString
                lbllockedby.Value = dr.Item("lockedby").ToString
            End While
            dr.Close()
        Catch ex As Exception

        End Try
        Return lock
    End Function
    Private Sub getrev(ByVal ttid As String, ByVal who As String)
        Dim freq, rdid, qty, skillid, ismeter, meterid, fixed, mfid, tid, ptid, lotoid, conid, ttime, rdt As String
        Dim origfreq, origrdid, origqty, origskillid, ismetero, meterido, fixedo, mfido, origtid, origptid, origttime, origrdt As String
        Dim fuid, taskstatusid, rd, func, skill, eqnum, rdo, skillo, tasknum, eqid, tpmhold, did, clid, coid As String
        sql = "select t.ttid, t.ptid, t.skillid, t.qty, t.rdid, t.freq, t.rdt, t.lotoid, t.conid, t.ttime, " _
            + "t.origttid, t.origptid, t.origskillid, t.origqty, t.origrdid, t.origfreq, t.origrdt, t.lotoid, t.conid, t.origttime, " _
            + "t.fixed, t.usemeter, t.meterid, t.mfid, t.fixedo, t.usemetero, t.meterido, t.mfido, c.compnum, t.funcid, t.taskstatusid, " _
            + "t.tasknum, t.eqid, t.tpmhold, t.deptid, t.cellid, t.comid " _
            + "from pmtasks t " _
            + "left join components c on c.comid = t.comid " _
            + "where t.pmtskid = '" & ttid & "'"
        dr = tasks.GetRdrData(sql)
        While dr.Read
            tasknum = dr.Item("tasknum").ToString
            eqid = dr.Item("eqid").ToString
            tpmhold = dr.Item("tpmhold").ToString
            did = dr.Item("deptid").ToString
            clid = dr.Item("cellid").ToString
            coid = dr.Item("comid").ToString
            'gen
            fuid = dr.Item("funcid").ToString
            taskstatusid = dr.Item("taskstatusid").ToString
            'rev
            tid = dr.Item("ttid").ToString
            ptid = dr.Item("ptid").ToString
            skillid = dr.Item("skillid").ToString
            qty = dr.Item("qty").ToString
            rdid = dr.Item("rdid").ToString
            freq = dr.Item("freq").ToString
            rdt = dr.Item("rdt").ToString
            lotoid = dr.Item("lotoid").ToString
            conid = dr.Item("conid").ToString
            ttime = dr.Item("ttime").ToString
            fixed = dr.Item("fixed").ToString
            ismeter = dr.Item("usemeter").ToString
            meterid = dr.Item("meterid").ToString
            mfid = dr.Item("mfid").ToString
            'orig
            origtid = dr.Item("origttid").ToString
            origptid = dr.Item("origptid").ToString
            origskillid = dr.Item("origskillid").ToString
            origqty = dr.Item("origqty").ToString
            origrdid = dr.Item("origrdid").ToString
            origfreq = dr.Item("origfreq").ToString
            origrdt = dr.Item("origrdt").ToString
            origttime = dr.Item("origttime").ToString
            fixedo = dr.Item("fixedo").ToString
            ismetero = dr.Item("usemetero").ToString
            meterido = dr.Item("meterido").ToString
            mfido = dr.Item("mfido").ToString
        End While
        dr.Close()
        lblpg.Value = tasknum
        lbltpmhold.Value = tpmhold
        lbldid.Value = did
        lbltaskid.Value = ttid
        lblclid.Value = clid
        lblco.Value = coid
        CheckLock(eqid)
        lbllock.Value = "0"
        'gen
        lblismeter.Value = ismeter
        lblismetero.Value = ismetero
        lblfuid.Value = fuid
        
        
        'rev
        lblfixed.Value = fixed
        lblmeterid.Value = meterid
        lblmfid.Value = mfid
        lblskillid.Value = skillid
        'orig
        lblfixedo.Value = fixedo
        lblmeterido.Value = meterido
        lblmfido.Value = mfido
        lblskillido.Value = origskillid

        lblrdid.Value = rdid
        If rdid <> "2" Then
            txtrdt.Enabled = False
        End If
        lblskillqty.Value = qty
        If qty = "1" Then
            txtqty.Enabled = False
        End If

            Try
                ddtype.SelectedValue = tid
            Catch ex As Exception

            End Try
            Try
                ddpt.SelectedValue = ptid
            Catch ex As Exception

            End Try

            Try
                ddskill.SelectedValue = skillid
            Catch ex As Exception

            End Try
            txtqty.Text = qty
            txttr.Text = ttime
            Try
                ddeqstat.SelectedValue = rdid
            Catch ex As Exception

            End Try
            txtrdt.Text = rdt
            
       


    End Sub
   
    Private Sub GetLists()

        sql = "select ttid, tasktype " _
        + "from pmTaskTypes where tasktype <> 'Select' order by compid"
        dr = tasks.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataBind()
        dr.Close()
        ddtype.Items.Insert(0, New ListItem("Select"))
        ddtype.Items(0).Value = 0

        sid = lblsid.Value
        Dim scnt As Integer
        sql = "select count(*) from pmSiteSkills where siteid = '" & sid & "'"
        scnt = tasks.Scalar(sql)
        If scnt <> 0 Then
            sql = "select skillid, skill " _
            + "from pmSiteSkills where siteid = '" & sid & "'"
        Else
            sql = "select skillid, skill " _
            + "from pmSkills"
        End If
        dr = tasks.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("Select"))
        ddskill.Items(0).Value = 0


        sql = "select ptid, pretech " _
        + "from pmPreTech"
        dr = tasks.GetRdrData(sql)
        ddpt.DataSource = dr
        ddpt.DataTextField = "pretech"
        ddpt.DataValueField = "ptid"
        ddpt.DataBind()
        dr.Close()
        ddpt.Items.Insert(0, New ListItem("None"))
        ddpt.Items(0).Value = 0


        sql = "select statid, status " _
        + "from pmStatus"
        dr = tasks.GetRdrData(sql)
        ddeqstat.DataSource = dr
        ddeqstat.DataTextField = "status"
        ddeqstat.DataValueField = "statid"
        ddeqstat.DataBind()
        dr.Close()
        ddeqstat.Items.Insert(0, New ListItem("Select"))
        ddeqstat.Items(0).Value = 0

    End Sub
End Class