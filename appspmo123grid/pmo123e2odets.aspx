﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123e2odets.aspx.vb" Inherits="lucy_r12.pmo123e2odets" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
	
	<script language="javascript" type="text/javascript" src="../scripts/pmtaskdivfuncopt_1016_1.js"></script>
	<script language="JavaScript" type="text/javascript" src="../scripts1/PMOptTasksaspx_1016_2.js"></script>
    <script language="javascript" type="text/javascript">
    <!--
        function GetType(app, sval) {
            //alert(app + ", " + sval)
            var typlst
            var sstr
            var ssti
            if (app == "d") {
                typlst = document.getElementById("ddtype");
                ptlst = document.getElementById("ddpt");
                sstr = typlst.options[typlst.selectedIndex].text;
                ssti = document.getElementById("ddtype").value;
                if (ssti == "7") {
                    document.getElementById("ddpt").disabled = false;
                }
                else {
                    ptlst.value = "0";
                    document.getElementById("ddpt").disabled = true;
                }
            }
            else if (app == "o") {
                document.getElementById("ddtype").value = sval;
                typlst = document.getElementById("ddtypeo");
                ptlst = document.getElementById("ddpt");
                ptolst = document.getElementById("ddpto");
                sstr = typlst.options[typlst.selectedIndex].text;
                ssti = document.getElementById("ddtypeo").value;
                if (ssti == "7") {
                    document.getElementById("ddpt").disabled = false;
                    document.getElementById("ddpto").disabled = false;
                }
                else {
                    ptlst.value = "0";
                    ptolst.value = "0";
                    document.getElementById("ddpt").disabled = true;
                    document.getElementById("ddpto").disabled = true;
                }
            }
        }
        function valpgnums() {
            var tpmhold = document.getElementById("lbltpmhold").value;
            if (tpmhold != "1") {
                if (isNaN(document.getElementById("txtqty").value)) {
                    alert("Skill Qty is Not a Number")
                }
                else if (isNaN(document.getElementById("txttr").value)) {
                    alert("Skill Time is Not a Number")
                }
                else if (isNaN(document.getElementById("txtrdt").value)) {
                    alert("Running/Down Time is Not a Number")
                }
                else {
                    document.getElementById("lblsubmit").value = "sav";
                    // FreezeScreen('Your Data Is Being Processed...');
                    document.getElementById("form1").submit();
                }
            }
        }
//-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="422">
    
    
                <tr>
                <td class="label" width="100"><asp:Label id="lang255" runat="server">Task Type</asp:Label></td>
						<td colSpan="3"><asp:DropDownList id="ddtype" runat="server" CssClass="plainlabel" Width="160px" Rows="1" DataTextField="tasktype"
								DataValueField="ttid" Enabled="false"></asp:DropDownList></td>
                </tr>
				<tr>
                <td class="label"><asp:Label id="lang256" runat="server">PdM Tech</asp:Label></td>
						<td colSpan="3"><asp:dropdownlist id="ddpt" runat="server" CssClass="plainlabel" Width="160px" Enabled="false"></asp:dropdownlist></td>
                </tr>
                <tr>
                <td class="label"><asp:Label id="lang259" runat="server">Skill Required</asp:Label></td>
									<td colSpan="3"><asp:dropdownlist id="ddskill" runat="server" Width="160px" cssclass="plainlabel" Enabled="false"></asp:dropdownlist>
											</td>
                </tr>
                <tr>
                <td class="label" >Qty</td>
									<td colSpan="3"><asp:textbox id="txtqty" runat="server" CssClass="plainlabel" Width="30px"></asp:textbox></td>
                </tr>
                <tr>
                <td class="label"><asp:Label id="lang260" runat="server">Min Ea</asp:Label></td>
									<td colSpan="3"><asp:textbox id="txttr" runat="server" CssClass="plainlabel" Width="35px"></asp:textbox></td>
                </tr>
                <tr>
                <td class="label"><asp:Label id="lang263" runat="server">Equipment Status</asp:Label></td>
									<td colSpan="3"><asp:dropdownlist id="ddeqstat" runat="server" CssClass="plainlabel" Width="90px" Enabled="false"></asp:dropdownlist></td>
                </tr>
                <tr>
                <td class="label"><asp:Label id="lang264" runat="server">Down Time</asp:Label></td>
									<td colSpan="3"><asp:textbox id="txtrdt" runat="server" CssClass="plainlabel" Width="35px"></asp:textbox></td>
                </tr>
                
                
                 
                <tr>
                <td align="right" colSpan="3"><IMG onmouseover="return overlib('Choose Equipment Spare Parts for this Task')" onclick="GetSpareDiv();"
								onmouseout="return nd()" src="../images/appbuttons/minibuttons/spareparts.gif"><IMG id="img1" onmouseover="return overlib('Add/Edit Parts for this Task')" onclick="GetPartDiv();"
								onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/parttrans.gif" width="23" runat="server">
							<IMG onmouseover="return overlib('Add/Edit Tools for this Task')" onclick="GetToolDiv();"
								onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/tooltrans.gif"
								width="23"> <IMG onmouseover="return overlib('Add/Edit Lubricants for this Task')" onclick="GetLubeDiv();"
								onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/lubetrans.gif"
								width="23">
							</td>
                </tr>
                <tr>
                <td align="right" colspan="3">
                
                <IMG id="btnsav" onclick="valpgnums();" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif"
								width="20" height="20" runat="server"></td>
                </tr>
			</table>
    </div>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblpmtskid" runat="server" />
    <input type="hidden" id="lblcoid" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblmfid" runat="server" />
    <input type="hidden" id="lblmeterid" runat="server" />
    <input type="hidden" id="lblismeter" runat="server" />
    <input type="hidden" id="lblismetero" runat="server" />
    <input type="hidden" id="lblfixed" runat="server" />
    <input type="hidden" id="lblmfido" runat="server" />
    <input type="hidden" id="lblmeterido" runat="server" />
    <input type="hidden" id="lblfixedo" runat="server" />
    <input type="hidden" id="lblrd" runat="server" />
    <input type="hidden" id="lblrdo" runat="server" />
    <input type="hidden" id="lblfunc" runat="server" />
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblskill" runat="server" />
    <input type="hidden" id="lblskillo" runat="server" />
    <input type="hidden" id="lblfuid" runat="server" />
    <input type="hidden" id="lblskillid" runat="server" />
    <input type="hidden" id="lblskillido" runat="server" />
    <input type="hidden" id="lblrdid" runat="server" />
    <input type="hidden" id="lblrdido" runat="server" />
    <input type="hidden" id="lblcomp" runat="server" />
    <input type="hidden" id="lbltpmhold" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblustr" runat="server" />
    <input type="hidden" id="lblskillqty" runat="server" />
   <input id="lblro" type="hidden" runat="server" />
    <input id="lbllock" type="hidden" runat="server" />
    <input id="lblcid" type="hidden" runat="server" />
    <input id="lbllockedby" type="hidden" runat="server" />
    <input id="lbltaskid" type="hidden" runat="server" />
    <input id="lblpg" type="hidden" runat="server" />
    <input id="lblenable" type="hidden" runat="server" />
    <input id="lbldid" type="hidden" runat="server" />
    <input id="lblclid" type="hidden" runat="server" />
    <input id="lblco" type="hidden" runat="server" />
    <input id="lblsave" type="hidden" runat="server" />
    </form>
</body>
</html>
