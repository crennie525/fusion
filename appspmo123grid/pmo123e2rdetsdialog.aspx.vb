﻿Public Class pmo123e2rdetsdialog
    Inherits System.Web.UI.Page
    Dim sid, ttid, eqid, who, coid, ustr, sqty, usetot As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            ttid = Request.QueryString("ttid").ToString
            eqid = Request.QueryString("eqid").ToString
            coid = Request.QueryString("coid").ToString
            who = Request.QueryString("who").ToString
            ustr = Request.QueryString("ustr").ToString
            Try
                usetot = Request.QueryString("usetot").ToString
            Catch ex As Exception
                usetot = "0"
            End Try
            If who = "sub" Then
                sqty = Request.QueryString("sqty").ToString
                ifmeter.Attributes.Add("src", "pmo123e2rdets.aspx?eqid=" + eqid + "&ttid=" + ttid + "&coid=" + coid + "&who=" + who + "&sid=" + sid + "&ustr=" + ustr + "&sqty=" + sqty + "&usetot=" + usetot + "&date=" + Now)

            Else
                ifmeter.Attributes.Add("src", "pmo123e2rdets.aspx?eqid=" + eqid + "&ttid=" + ttid + "&coid=" + coid + "&who=" + who + "&sid=" + sid + "&ustr=" + ustr + "&usetot=" + usetot + "&date=" + Now)
            End If

        End If
    End Sub

End Class