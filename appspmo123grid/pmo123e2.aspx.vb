﻿Imports System.Data.SqlClient
Public Class pmo123e2
    Inherits System.Web.UI.Page
    Dim pmo As New Utilities
    Dim tmod As New transmod
    Dim sql As String
    Dim dr As SqlDataReader
    Dim eqid, sid, ustr As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 100
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = "0"
            lbleqid.Value = eqid
            sid = Request.QueryString("sid").ToString '"12" '
            lblsid.Value = sid
            ustr = Request.QueryString("usrname").ToString
            lblustr.Value = ustr
            txtpg.Value = 1
            pmo.Open()
            GetRev(eqid)
            geteq(eqid, 1)
            pmo.Dispose()
            ddproc.Attributes.Add("onchange", "checkdoc();")
            resize1.MinimumHeight = 2800
            resize1.MinimumWidth = 1270
            resize1.MaximumHeight = 2800
            resize1.MaximumWidth = 2800
            'use 0 at end to disable resizing
            resize1.HandleCssClass = "handleText0"
            resize1.ResizableCssClass = "resizingText0"
            p1.CssClass = "frameText0"
            p2.Attributes.Add("class", "details")
            p3.Attributes.Add("class", "details")
            '0 is edit
            '1 is func and comp
            '2 is func
            '3 is comp
            '4 is comp lookup
            GridView1.Columns(1).Visible = False
            GridView1.Columns(7).Visible = False
            tbldata.Attributes.Add("width", "1270")
            GridView1.Width = 1250
            lbldocmode.Value = "none"
            lblstart.Value = "no"
            txtpgcnt.Value = "0"
            plst.Style.Add("top", "100px")
            plst.Style.Add("left", "100px")
        Else
            Dim p1x, p1y As String
            p1x = lblp1xpos.Value
            p1y = lblp1ypos.Value

            If Request.Form("lblsubmit") = "getdept" Then
                lblsubmit.Value = ""
                eqid = lbleqid.Value
                pmo.Open()
                GetRev(eqid)
                geteq(eqid, 1)
                PopProcedures(eqid)
                pmo.Dispose()
            ElseIf Request.Form("lblsubmit") = "getproc" Then
                lblsubmit.Value = ""
                eqid = lbleqid.Value
                pmo.Open()
                'geteq(eqid, 1)
                PopProcedures(eqid)
                pmo.Dispose()
            ElseIf Request.Form("lblsubmit") = "checkdoc" Then
                lblsubmit.Value = ""
                pmo.Open()
                Dim tcnt As String = txtpgcnt.Value
                'need check for drag here
                If tcnt <> "0" Then
                    PageNumber = 1
                    txtpg.Value = PageNumber
                    eqid = lbleqid.Value
                    GetRev(eqid)
                    geteq(eqid, PageNumber)
                    lbltstat.Value = ""
                Else
                    divstart.Attributes.Add("class", "view")
                    divdata.Attributes.Add("class", "view")
                    trgrid.Attributes.Add("class", "details")
                End If
                'need siren check
                handledocmode()
                pmo.Dispose()
                End If
                eqid = lbleqid.Value
                If Request.Form("lblret") = "next" Then
                    pmo.Open()
                    GetNext()
                    pmo.Dispose()
                    lblret.Value = ""
                ElseIf Request.Form("lblret") = "last" Then
                    pmo.Open()
                    PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                eqid = lbleqid.Value
                    geteq(eqid, PageNumber)
                    pmo.Dispose()
                    lblret.Value = ""
                ElseIf Request.Form("lblret") = "prev" Then
                    pmo.Open()
                    GetPrev()
                    pmo.Dispose()
                    lblret.Value = ""
                ElseIf Request.Form("lblret") = "first" Then
                    pmo.Open()
                    PageNumber = 1
                txtpg.Value = PageNumber
                eqid = lbleqid.Value
                    geteq(eqid, PageNumber)
                    pmo.Dispose()
                    lblret.Value = ""
                ElseIf Request.Form("lblret") = "addptask" Then
                    pmo.Open()
                PageNumber = txtpg.Value
                If txtpg.Value = "" Or PageNumber = 0 Then
                    txtpg.Value = "1"
                    PageNumber = 1
                End If
                    'addtask here
                addptask()
                eqid = lbleqid.Value
                GetRev(eqid)
                    geteq(eqid, PageNumber)
                    pmo.Dispose()
                lblret.Value = ""
                lbltstat.Value = ""
                ElseIf Request.Form("lblret") = "rettask" Then
                    pmo.Open()
                PageNumber = txtpg.Value
                If txtpg.Value = "" Or PageNumber = 0 Then
                    txtpg.Value = "1"
                    PageNumber = 1
                End If
                eqid = lbleqid.Value
                    geteq(eqid, PageNumber)
                    pmo.Dispose()
                lblret.Value = ""
                lbltstat.Value = ""
                ElseIf Request.Form("lblret") = "addfunctask" Then
                    pmo.Open()
                PageNumber = txtpg.Value
                If txtpg.Value = "" Or PageNumber = 0 Then
                    txtpg.Value = "1"
                    PageNumber = 1
                End If
                    'addtask here
                addfunctask()
                eqid = lbleqid.Value
                GetRev(eqid)
                    geteq(eqid, PageNumber)
                    pmo.Dispose()
                lblret.Value = ""
                lbltstat.Value = ""
                ElseIf Request.Form("lblret") = "addcomptask" Then
                    pmo.Open()
                PageNumber = txtpg.Value
                If txtpg.Value = "" Or PageNumber = 0 Then
                    txtpg.Value = "1"
                    PageNumber = 1
                End If
                    'addtask here
                addcomptask()
                eqid = lbleqid.Value
                GetRev(eqid)
                    geteq(eqid, PageNumber)
                    pmo.Dispose()
                lblret.Value = ""
                lbltstat.Value = ""
            End If

            Dim eq, fu, co, fcnt As String
            fu = lblfu.Value
            co = lblcomp.Value
            eq = lbleq.Value
            fcnt = lblfcnt.Value
            tdsequip.InnerHtml = eq
            If fu <> "" Then
                tdsfunc.InnerHtml = fu
            Else
                tdsfunc.InnerHtml = "None"
            End If
            If co <> "" Then
                tdscomp.InnerHtml = co
            Else
                tdscomp.InnerHtml = "None"
            End If
            If eq <> "" And fcnt = "0" Then
                tdsmsg.InnerHtml = "Your Selected Equipment Record currently has no Functions.<br>"
                tdsmsg.InnerHtml += "Your Options are limited to using Equipment Tabs.<br>"
                tdsmsg.InnerHtml += "Please Select an Option Below"
                trdnd.Attributes.Add("class", "details")
                trselfunc.Attributes.Add("class", "details")
                trselcomp.Attributes.Add("class", "details")
                traddftask.Attributes.Add("class", "details")
                traddctask.Attributes.Add("class", "details")
            ElseIf eq <> "" And fcnt <> "0" And fu = "" Then
                tdsmsg.InnerHtml = "You currently have no Function Record Selected.<br>"
                tdsmsg.InnerHtml += "Your Options are limited to using Equipment Tabs, Selecting a Function, or Going Directly to Drag and Drop.<br>"
                tdsmsg.InnerHtml += "Please Select an Option Below"
                'trdnd.Attributes.Add("class", "details")
                trselcomp.Attributes.Add("class", "details")
                traddftask.Attributes.Add("class", "details")
                traddctask.Attributes.Add("class", "details")
            ElseIf eq <> "" And fcnt <> "0" And fu <> "" Then
                tdsmsg.InnerHtml = "You currently have both an Equipment and Function Record Selected.<br>"
                tdsmsg.InnerHtml += "Please Select an Option Below"
                trselcomp.Attributes.Add("class", "details")
            ElseIf eq <> "" And fcnt <> "0" And co <> "" Then
                tdsmsg.InnerHtml = "You currently have both an Equipment and Function Record Selected.<br>"
                tdsmsg.InnerHtml += "Please Select an Option Below"

            End If
        End If
    End Sub
    Private Sub handledocmode()
        Dim docmode As String = lbldocmode.Value
        Dim docname, docid As String
        docname = ddproc.SelectedItem.ToString
        docid = ddproc.SelectedValue.ToString
        If docmode = "sbs" Then
            'side settings
            resize1.MinimumHeight = 100
            resize1.MinimumWidth = 100
            resize1.MaximumHeight = 700
            resize1.MaximumWidth = 1010
            'use 0 at end to disable resizing
            resize1.HandleCssClass = "handleText"
            resize1.ResizableCssClass = "resizingText"
            p1.CssClass = "frameText"
            p3.CssClass = "frameText"
            trdocbot.Attributes.Add("class", "details")
            tddocside.Attributes.Add("class", "view")
            p2.Attributes.Add("class", "details")
            'p3.Attributes.Add("class", "view")
            '0 is edit
            '1 is func and comp
            '2 is func
            '3 is comp
            '4 is comp lookup
            GridView1.Columns(1).Visible = True
            GridView1.Columns(2).Visible = False
            GridView1.Columns(3).Visible = False

            GridView1.Columns(7).Visible = True
            GridView1.Columns(8).Visible = False
            GridView1.Columns(9).Visible = False

            'tbldata.Attributes.Add("width", "1010")
            tbldata.Attributes.Add("width", "748")
            GridView1.Width = 748 ' 1250
            ifpmdocs.Attributes.Add("src", "pmo123e2docout.aspx?docid=" + docid)
            lblstart.Value = "yes"
        ElseIf docmode = "ttb" Then
            'top to bottom settings
            resize1.MinimumHeight = 300
            resize1.MinimumWidth = 1270
            resize1.MaximumHeight = 2800
            resize1.MaximumWidth = 2800
            resize1.HandleCssClass = "handleText"
            resize1.ResizableCssClass = "resizingText"
            'p2.Attributes.Add("class", "view")
            p3.Attributes.Add("class", "details")
            p1.CssClass = "frameText"
            p2.CssClass = "frameText"
            trdocbot.Attributes.Add("class", "view")
            tddocside.Attributes.Add("class", "details")
            
            GridView1.Columns(1).Visible = False
            GridView1.Columns(2).Visible = True
            GridView1.Columns(3).Visible = True

            GridView1.Columns(7).Visible = False
            GridView1.Columns(8).Visible = True
            GridView1.Columns(9).Visible = True

            tbldata.Attributes.Add("width", "1250")
            GridView1.Width = 1250
            ifpmdoc.Attributes.Add("src", "pmo123e2docout.aspx?docid=" + docid)
            lblstart.Value = "yes"
        End If
        

    End Sub
    Private Sub PopProcedures(ByVal eqid As String)
        Dim prcnt As Integer
        sql = "select count(*) from PMOptOldProcedures where eqid = '" & eqid & "'"
        prcnt = pmo.Scalar(sql)
        Dim dt, val, filt As String
        If prcnt = 0 Then
            ddproc.Items.Insert(0, "Select Procedure")
            ddproc.Enabled = False
        Else
            dt = "PMOptOldProcedures"
            val = "eqppmid, filename"
            filt = " where eqid = '" & eqid & "'"
            dr = pmo.GetList(dt, val, filt)
            ddproc.DataSource = dr
            ddproc.DataTextField = "filename"
            ddproc.DataValueField = "eqppmid"
            ddproc.DataBind()
            dr.Close()
            ddproc.Items.Insert(0, "Select Procedure")
            ddproc.Enabled = True
        End If
    End Sub
    Private Sub addptask()
        Dim who As String = lblwho.Value
        Dim rid As String = lblretid.Value
        Dim rid2 As String = lblretid2.Value
        Dim ttid As String = lblttid.Value
        Dim tasktype As String = lbltasktype.Value
        Dim ptid As String = lblptid.Value
        Dim pretech As String = lblpretech.Value
        Dim skillid As String = lblskillid.Value
        Dim skill As String = lblskill.Value
        Dim qty As String = lblqty.Value
        Dim rdid As String = lblrdid.Value
        Dim rd As String = lblrd.Value
        Dim freq As String = lblfreq.Value
        Dim meterid As String = lblmeterid.Value
        Dim meterfreq As String = lblmeterfreq.Value
        Dim maxdays As String = lblmaxdays.Value
        Dim fixed As String = lblfixed.Value
        Dim rdt As String = lblrdt.Value

        Dim unit As String = lblmeterunit.Value

        Dim usemeter As String = "0"
        If meterid <> "" Then
            usemeter = "1"
        End If

        eqid = lbleqid.Value

        Dim cn, cd As String
        cn = ""
        cd = ""

        Dim fuid As String = lblretid.Value
        Dim coid As String = lblretid2.Value
        '@coid needs to be null if func

        If who = "func" Then
            coid = ""
        End If

        Dim sid As String = lblsid.Value
        Dim did As String = lbldid.Value
        Dim clid As String = lblclid.Value
        Dim uid As String = lblustr.Value

        Dim orig As String = "0"
        'alter procedure [dbo].[usp_addtaskwprofile] (@sid int, @did int, @clid int, @eqid int, @fuid int, @uid varchar(50),
        '@ttid int, @tasktype varchar(50), @ptid int, @pretech varchar(50), @skillid int, @skill varchar(50), @qty int,
        '@freq int, @rdid int, @rd varchar(50), @rdt int, @usemeter int, @meterid int, @unit int, @meterfreq varchar(50),
        '@maxdays int, @fixed int, @coid int, @orig int)
        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_addtaskwprofile @sid, @did, @clid, @eqid, @fuid, @uid, @ttid, @tasktype, @ptid, @pretech, " _
                + "@skillid, @skill, @qty, @freq, @rdid, @rd, @rdt, @usemeter, @meterid, @unit, @meterfreq, @maxdays, @fixed, " _
                + "@coid, @orig"
        Dim param01 = New SqlParameter("@sid", SqlDbType.VarChar)
        If sid = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = sid
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@did", SqlDbType.VarChar)
        If did = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = did
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@clid", SqlDbType.VarChar)
        If clid = "" Then
            param03.Value = System.DBNull.Value
        Else
            param03.Value = clid
        End If
        cmd.Parameters.Add(param03)
        Dim param04 = New SqlParameter("@eqid", SqlDbType.VarChar)
        If eqid = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = eqid
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@fuid", SqlDbType.VarChar)
        If fuid = "" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = fuid
        End If
        cmd.Parameters.Add(param05)
        Dim param06 = New SqlParameter("@uid", SqlDbType.VarChar)
        If uid = "" Then
            param06.Value = System.DBNull.Value
        Else
            param06.Value = uid
        End If
        cmd.Parameters.Add(param06)
        Dim param07 = New SqlParameter("@ttid", SqlDbType.VarChar)
        If ttid = "" Then
            param07.Value = System.DBNull.Value
        Else
            param07.Value = ttid
        End If
        cmd.Parameters.Add(param07)
        Dim param08 = New SqlParameter("@tasktype", SqlDbType.VarChar)
        If tasktype = "" Then
            param08.Value = System.DBNull.Value
        Else
            param08.Value = tasktype
        End If
        cmd.Parameters.Add(param08)
        Dim param09 = New SqlParameter("@ptid", SqlDbType.VarChar)
        If ptid = "" Then
            param09.Value = System.DBNull.Value
        Else
            param09.Value = ptid
        End If
        cmd.Parameters.Add(param09)
        Dim param010 = New SqlParameter("@pretech", SqlDbType.VarChar)
        If pretech = "" Then
            param010.Value = System.DBNull.Value
        Else
            param010.Value = pretech
        End If
        cmd.Parameters.Add(param010)
        Dim param011 = New SqlParameter("@skillid", SqlDbType.VarChar)
        If skillid = "" Then
            param011.Value = System.DBNull.Value
        Else
            param011.Value = skillid
        End If
        cmd.Parameters.Add(param011)
        Dim param012 = New SqlParameter("@skill", SqlDbType.VarChar)
        If skill = "" Then
            param012.Value = System.DBNull.Value
        Else
            param012.Value = skill
        End If
        cmd.Parameters.Add(param012)
        Dim param013 = New SqlParameter("@qty", SqlDbType.VarChar)
        If qty = "" Then
            param013.Value = System.DBNull.Value
        Else
            param013.Value = qty
        End If
        cmd.Parameters.Add(param013)
        Dim param014 = New SqlParameter("@freq", SqlDbType.VarChar)
        If freq = "" Then
            param014.Value = System.DBNull.Value
        Else
            param014.Value = freq
        End If
        cmd.Parameters.Add(param014)
        Dim param015 = New SqlParameter("@rdid", SqlDbType.VarChar)
        If rdid = "" Then
            param015.Value = System.DBNull.Value
        Else
            param015.Value = rdid
        End If
        cmd.Parameters.Add(param015)
        Dim param016 = New SqlParameter("@rd", SqlDbType.VarChar)
        If rd = "" Then
            param016.Value = System.DBNull.Value
        Else
            param016.Value = rd
        End If
        cmd.Parameters.Add(param016)
        Dim param017 = New SqlParameter("@rdt", SqlDbType.VarChar)
        If rdt = "" Then
            param017.Value = System.DBNull.Value
        Else
            param017.Value = rdt
        End If
        cmd.Parameters.Add(param017)
        Dim param018 = New SqlParameter("@usemeter", SqlDbType.VarChar)
        If usemeter = "" Then
            param018.Value = System.DBNull.Value
        Else
            param018.Value = usemeter
        End If
        cmd.Parameters.Add(param018)
        Dim param019 = New SqlParameter("@meterid", SqlDbType.VarChar)
        If meterid = "" Then
            param019.Value = System.DBNull.Value
        Else
            param019.Value = meterid
        End If
        cmd.Parameters.Add(param019)
        Dim param020 = New SqlParameter("@unit", SqlDbType.VarChar)
        If unit = "" Then
            param020.Value = System.DBNull.Value
        Else
            param020.Value = unit
        End If
        cmd.Parameters.Add(param020)
        Dim param021 = New SqlParameter("@meterfreq", SqlDbType.VarChar)
        If meterfreq = "" Then
            param021.Value = System.DBNull.Value
        Else
            param021.Value = meterfreq
        End If
        cmd.Parameters.Add(param021)
        Dim param022 = New SqlParameter("@maxdays", SqlDbType.VarChar)
        If maxdays = "" Then
            param022.Value = System.DBNull.Value
        Else
            param022.Value = maxdays
        End If
        cmd.Parameters.Add(param022)
        Dim param023 = New SqlParameter("@fixed", SqlDbType.VarChar)
        If fixed = "" Then
            param023.Value = System.DBNull.Value
        Else
            param023.Value = fixed
        End If
        cmd.Parameters.Add(param023)
        Dim param024 = New SqlParameter("@coid", SqlDbType.VarChar)
        If coid = "" Then
            param024.Value = System.DBNull.Value
        Else
            param024.Value = coid
        End If
        cmd.Parameters.Add(param024)
        Dim param025 = New SqlParameter("@orig", SqlDbType.VarChar)
        If orig = "" Then
            param025.Value = System.DBNull.Value
        Else
            param025.Value = orig
        End If
        cmd.Parameters.Add(param025)

        GridView1.EditIndex = -1
        'pmo.Open()
        PageNumber = txtpg.Value
        pmo.UpdateHack(cmd)
        geteq(eqid, PageNumber)
        'pmo.Dispose()


        lblwho.Value = ""

        lblttid.Value = ""
        lbltasktype.Value = ""
        lblptid.Value = ""
        lblpretech.Value = ""
        lblskillid.Value = ""
        lblskill.Value = ""
        lblqty.Value = ""
        lblrdid.Value = ""
        lblrd.Value = ""
        lblfreq.Value = ""
        lblmeterid.Value = ""
        lblmeterfreq.Value = ""
        lblmaxdays.Value = ""
        lblfixed.Value = ""
        lblrdt.Value = ""

        lblretid.Value = ""
        lblretid2.Value = ""
                
    End Sub

    Private Sub GetNext()
        eqid = lbleqid.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            geteq(eqid, PageNumber)
        Catch ex As Exception
            pmo.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        eqid = lbleqid.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            geteq(eqid, PageNumber)
        Catch ex As Exception
            pmo.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetRev(ByVal eqid As String)
        sql = "select rev, usetot, fcnt = (select count(*) from functions where eqid = '" & eqid & "') from equipment where eqid = '" & eqid & "'"
        Dim rev, tot, usetot, fcnt As String
        'rev = tasks.strScalar(sql)
        dr = pmo.GetRdrData(sql)
        While dr.Read
            rev = dr.Item("rev").ToString
            usetot = dr.Item("usetot").ToString
            fcnt = dr.Item("fcnt").ToString
        End While
        dr.Close()
        lblfcnt.Value = fcnt
        'tdrev.InnerHtml = rev
        'trrev.Attributes.Add("class", "view")
        If usetot = "1" Then
            lblusetot.Value = "1"
            'trtots.Attributes.Add("class", "view")
        End If

    End Sub
    Private Sub geteq(ByVal eqid As String, ByVal PageNumber As Integer)
        Dim tstat As String = lbltstat.Value
        sql = "select t.*, f.func from pmtasks t " _
            + "left join functions f on f.func_id = t.funcid " _
            + "left join components c on c.func_id = f.func_id " _
            + "where t.eqid = '" & eqid & "' and subtask = 0 " _
            + "order by f.func, c.compnum, t.tasknum, t.subtask"
        'and subtask = 0
        Dim filt As String = lblfilt.Value
        Dim fuid As String = lblfuid.Value
        Dim coid As String = lblcoid.Value
        If filt = "func" And fuid <> "no" Then
            sql = "select count(*) from pmtasks where eqid = '" & eqid & "' and funcid = '" & fuid & "'"
        ElseIf filt = "comp" And coid <> "no" Then
            sql = "select count(*) from pmtasks where eqid = '" & eqid & "' and comid = '" & coid & "'"
        Else
            sql = "select count(*) from pmtasks where eqid = '" & eqid & "'"
        End If
        If filt = "" Then
            filt = "reg"
        End If
        Dim intPgNav, intPgCnt As Integer
        intPgCnt = pmo.Scalar(sql)
        If intPgCnt = 0 Then
            lblpg.Text = "Page 0 of 0"
            If eqid <> "0" Then
                'need msg and siren here - no popup
                trwait.Attributes.Add("class", "details")
                trnotasks.Attributes.Add("class", "view")
                imgsiren1.Attributes.Add("class", "view")
                imgsiren2.Attributes.Add("class", "view")
                'Dim strMessage As String = "No Records Found"
                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                divstart.Attributes.Add("class", "view")
                divdata.Attributes.Add("class", "details")

            Else
                divstart.Attributes.Add("class", "view")
                divdata.Attributes.Add("class", "details")
                trwait.Attributes.Add("class", "view")
                trnotasks.Attributes.Add("class", "details")
               
            End If
            Exit Sub
        Else
            divstart.Attributes.Add("class", "details")
            divdata.Attributes.Add("class", "view")
            trgrid.Attributes.Add("class", "view")
            trwait.Attributes.Add("class", "view")
            trnotasks.Attributes.Add("class", "details")
            imgsiren1.Attributes.Add("class", "details")
            imgsiren2.Attributes.Add("class", "details")
        End If
        'this dependent on page mode
        Dim p1ht As Integer = (intPgCnt * 68) + 26
        Dim docmode As String = lbldocmode.Value
        If docmode = "none" Then
            resize1.MinimumHeight = p1ht
            resize1.MaximumHeight = p1ht
        End If

        PageSize = "100"
        intPgNav = pmo.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav

        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If
        'dr = pmo.GetRdrData(sql)
        'dr = eca.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        sql = "usp_getalltaskspg '" & eqid & "','" & PageNumber & "','" & PageSize & "','" & filt & "','" & fuid & "','" & coid & "'"
        dr = pmo.GetRdrData(sql)
        GridView1.DataSource = dr
        GridView1.DataBind()
        dr.Close()

    End Sub

    Private Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit
        GridView1.EditIndex = -1
        pmo.Open()
        PageNumber = txtpg.Value
        eqid = lbleqid.Value
        geteq(eqid, PageNumber)
        pmo.Dispose()
    End Sub
    Private Sub addfunctask()
        Dim did As String = lbldid.Value
        Dim clid As String = lblclid.Value
        sid = lblsid.Value
        eqid = lbleqid.Value
        Dim fuid As String = lblfuid.Value
        ustr = lblustr.Value
        Dim tli As String = "5"
        Dim cid As String = "0"
        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_AddTask @tli, @cid, @sid, @did, @clid, @eqid, @fuid, @ustr"
        Dim param01 = New SqlParameter("@tli", SqlDbType.VarChar)
        If tli = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = tli
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@cid", SqlDbType.VarChar)
        If cid = "0" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = cid
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@sid", SqlDbType.VarChar)
        If sid = "" Then
            param03.Value = System.DBNull.Value
        Else
            param03.Value = sid
        End If
        cmd.Parameters.Add(param03)
        Dim param04 = New SqlParameter("@did", SqlDbType.VarChar)
        If did = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = did
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@clid", SqlDbType.VarChar)
        If clid = "" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = clid
        End If
        cmd.Parameters.Add(param05)
        Dim param06 = New SqlParameter("@eqid", SqlDbType.VarChar)
        If eqid = "" Then
            param06.Value = System.DBNull.Value
        Else
            param06.Value = eqid
        End If
        cmd.Parameters.Add(param06)
        Dim param07 = New SqlParameter("@fuid", SqlDbType.VarChar)
        If fuid = "" Then
            param07.Value = System.DBNull.Value
        Else
            param07.Value = fuid
        End If
        cmd.Parameters.Add(param07)
        Dim param08 = New SqlParameter("@ustr", SqlDbType.VarChar)
        If ustr = "" Then
            param08.Value = System.DBNull.Value
        Else
            param08.Value = ustr
        End If
        cmd.Parameters.Add(param08)

        pmo.UpdateHack(cmd)
    End Sub
    Private Sub addcomptask()
        Dim did As String = lbldid.Value
        Dim clid As String = lblclid.Value
        sid = lblsid.Value
        eqid = lbleqid.Value
        Dim fuid As String = lblfuid.Value
        Dim coid As String = lblcoid.Value
        ustr = lblustr.Value
        Dim tli As String = "5"
        Dim cid As String = "0"
        Dim co, cd, comp As String
        sql = "select compnum, compdesc from components where comid = '" & coid & "'"
        dr = pmo.GetRdrData(sql)
        While dr.Read
            co = dr.Item("compnum").ToString
            cd = dr.Item("compdesc").ToString

        End While
        dr.Close()
        If cd <> "" Then
            comp = co & " - " & cd
        Else
            comp = co
        End If
        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_AddTask2 @tli, @cid, @sid, @did, @clid, @eqid, @fuid, @ustr, @comid, @compnum"
        Dim param01 = New SqlParameter("@tli", SqlDbType.VarChar)
        If tli = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = tli
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@cid", SqlDbType.VarChar)
        If cid = "0" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = cid
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@sid", SqlDbType.VarChar)
        If sid = "" Then
            param03.Value = System.DBNull.Value
        Else
            param03.Value = sid
        End If
        cmd.Parameters.Add(param03)
        Dim param04 = New SqlParameter("@did", SqlDbType.VarChar)
        If did = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = did
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@clid", SqlDbType.VarChar)
        If clid = "" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = clid
        End If
        cmd.Parameters.Add(param05)
        Dim param06 = New SqlParameter("@eqid", SqlDbType.VarChar)
        If eqid = "" Then
            param06.Value = System.DBNull.Value
        Else
            param06.Value = eqid
        End If
        cmd.Parameters.Add(param06)
        Dim param07 = New SqlParameter("@fuid", SqlDbType.VarChar)
        If fuid = "" Then
            param07.Value = System.DBNull.Value
        Else
            param07.Value = fuid
        End If
        cmd.Parameters.Add(param07)
        Dim param08 = New SqlParameter("@ustr", SqlDbType.VarChar)
        If ustr = "" Then
            param08.Value = System.DBNull.Value
        Else
            param08.Value = ustr
        End If
        cmd.Parameters.Add(param08)
        Dim param09 = New SqlParameter("@comid", SqlDbType.VarChar)
        If coid = "" Then
            param09.Value = System.DBNull.Value
        Else
            param09.Value = coid
        End If
        cmd.Parameters.Add(param09)
        Dim param10 = New SqlParameter("@compnum", SqlDbType.VarChar)
        If comp = "" Then
            param10.Value = System.DBNull.Value
        Else
            param10.Value = comp
        End If
        cmd.Parameters.Add(param10)

        pmo.UpdateHack(cmd)
    End Sub
    Private Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "Edit" Or e.CommandName = "Delete" Or e.CommandName = "Cancel" Or e.CommandName = "Update" Then
            Exit Sub
        End If
        Dim did As String = lbldid.Value
        Dim clid As String = lblclid.Value
        sid = lblsid.Value
        eqid = lbleqid.Value
        ustr = lblustr.Value
        Dim tli As String = "5"
        Dim cid As String = "0"
        Dim cmd As New SqlCommand
        pmo.Open()
        If e.CommandName = "addftask" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim row As GridViewRow = GridView1.Rows(index)

            Dim pmtskid As String = CType(row.FindControl("lblpmtskidi"), Label).Text
            Dim fuid As String = CType(row.FindControl("lblfuidi"), Label).Text
            Dim coid As String = CType(row.FindControl("lblcoidi"), Label).Text
            Dim comp As String = CType(row.FindControl("lblcompi"), Label).Text
            Dim tid As String = CType(row.FindControl("lbltnumi"), Label).Text

            cmd.CommandText = "exec usp_AddTask @tli, @cid, @sid, @did, @clid, @eqid, @fuid, @ustr"
            Dim param01 = New SqlParameter("@tli", SqlDbType.VarChar)
            If tli = "" Then
                param01.Value = System.DBNull.Value
            Else
                param01.Value = tli
            End If
            cmd.Parameters.Add(param01)
            Dim param02 = New SqlParameter("@cid", SqlDbType.VarChar)
            If cid = "0" Then
                param02.Value = System.DBNull.Value
            Else
                param02.Value = cid
            End If
            cmd.Parameters.Add(param02)
            Dim param03 = New SqlParameter("@sid", SqlDbType.VarChar)
            If sid = "" Then
                param03.Value = System.DBNull.Value
            Else
                param03.Value = sid
            End If
            cmd.Parameters.Add(param03)
            Dim param04 = New SqlParameter("@did", SqlDbType.VarChar)
            If did = "" Then
                param04.Value = System.DBNull.Value
            Else
                param04.Value = did
            End If
            cmd.Parameters.Add(param04)
            Dim param05 = New SqlParameter("@clid", SqlDbType.VarChar)
            If clid = "" Then
                param05.Value = System.DBNull.Value
            Else
                param05.Value = clid
            End If
            cmd.Parameters.Add(param05)
            Dim param06 = New SqlParameter("@eqid", SqlDbType.VarChar)
            If eqid = "" Then
                param06.Value = System.DBNull.Value
            Else
                param06.Value = eqid
            End If
            cmd.Parameters.Add(param06)
            Dim param07 = New SqlParameter("@fuid", SqlDbType.VarChar)
            If fuid = "" Then
                param07.Value = System.DBNull.Value
            Else
                param07.Value = fuid
            End If
            cmd.Parameters.Add(param07)
            Dim param08 = New SqlParameter("@ustr", SqlDbType.VarChar)
            If ustr = "" Then
                param08.Value = System.DBNull.Value
            Else
                param08.Value = ustr
            End If
            cmd.Parameters.Add(param08)

            pmo.UpdateHack(cmd)

        ElseIf e.CommandName = "addctask" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim row As GridViewRow = GridView1.Rows(index)

            Dim pmtskid As String = CType(row.FindControl("lblpmtskidi"), Label).Text
            Dim fuid As String = CType(row.FindControl("lblfuidi"), Label).Text
            Dim coid As String = CType(row.FindControl("lblcoidi"), Label).Text
            Dim comp As String = CType(row.FindControl("lblcompi"), Label).Text
            Dim tid As String = CType(row.FindControl("lbltnumi"), Label).Text

            cmd.CommandText = "exec usp_AddTask2 @tli, @cid, @sid, @did, @clid, @eqid, @fuid, @ustr, @comid, @compnum"
            Dim param01 = New SqlParameter("@tli", SqlDbType.VarChar)
            If tli = "" Then
                param01.Value = System.DBNull.Value
            Else
                param01.Value = tli
            End If
            cmd.Parameters.Add(param01)
            Dim param02 = New SqlParameter("@cid", SqlDbType.VarChar)
            If cid = "0" Then
                param02.Value = System.DBNull.Value
            Else
                param02.Value = cid
            End If
            cmd.Parameters.Add(param02)
            Dim param03 = New SqlParameter("@sid", SqlDbType.VarChar)
            If sid = "" Then
                param03.Value = System.DBNull.Value
            Else
                param03.Value = sid
            End If
            cmd.Parameters.Add(param03)
            Dim param04 = New SqlParameter("@did", SqlDbType.VarChar)
            If did = "" Then
                param04.Value = System.DBNull.Value
            Else
                param04.Value = did
            End If
            cmd.Parameters.Add(param04)
            Dim param05 = New SqlParameter("@clid", SqlDbType.VarChar)
            If clid = "" Then
                param05.Value = System.DBNull.Value
            Else
                param05.Value = clid
            End If
            cmd.Parameters.Add(param05)
            Dim param06 = New SqlParameter("@eqid", SqlDbType.VarChar)
            If eqid = "" Then
                param06.Value = System.DBNull.Value
            Else
                param06.Value = eqid
            End If
            cmd.Parameters.Add(param06)
            Dim param07 = New SqlParameter("@fuid", SqlDbType.VarChar)
            If fuid = "" Then
                param07.Value = System.DBNull.Value
            Else
                param07.Value = fuid
            End If
            cmd.Parameters.Add(param07)
            Dim param08 = New SqlParameter("@ustr", SqlDbType.VarChar)
            If ustr = "" Then
                param08.Value = System.DBNull.Value
            Else
                param08.Value = ustr
            End If
            cmd.Parameters.Add(param08)
            Dim param09 = New SqlParameter("@comid", SqlDbType.VarChar)
            If coid = "" Then
                param09.Value = System.DBNull.Value
            Else
                param09.Value = coid
            End If
            cmd.Parameters.Add(param09)
            Dim param10 = New SqlParameter("@compnum", SqlDbType.VarChar)
            If comp = "" Then
                param10.Value = System.DBNull.Value
            Else
                param10.Value = comp
            End If
            cmd.Parameters.Add(param10)

            pmo.UpdateHack(cmd)

        ElseIf e.CommandName = "addsub" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim row As GridViewRow = GridView1.Rows(index)

            Dim pmtskid As String = CType(row.FindControl("lblpmtskidi"), Label).Text
            Dim fuid As String = CType(row.FindControl("lblfuidi"), Label).Text
            Dim coid As String = CType(row.FindControl("lblcoidi"), Label).Text
            Dim comp As String = CType(row.FindControl("lblcompi"), Label).Text
            Dim tid As String = CType(row.FindControl("lbltnumi"), Label).Text

            Dim stcnt As Integer
            sql = "Select count(*) from pmTasks " _
            + "where funcid = '" & fuid & "' and tasknum = '" & pmtskid & "' and subtask <> 0"
            stcnt = pmo.Scalar(sql)
            Dim newtst As String = stcnt + 1
            sql = "insert into pmtasks (compid, siteid, deptid, cellid, eqid, funcid, tasknum, subtask, comid, compnum, " _
            + "skillid, skillindex, skill, freqid, freqindex, freq, rdid, rd, rdindex, taskstatus, ptid, ptindex, pretech, tasktype) " _
            + "select distinct " _
            + "compid, siteid, deptid, cellid, eqid, funcid, '" & tid & "', '" & newtst & "', comid, compnum, " _
            + "skillid, skillindex, skill, freqid, freqindex, freq, rdid, rd, rdindex, taskstatus, " _
            + "ptid, ptindex, pretech, tasktype from pmtasks " _
            + "where funcid = '" & fuid & "' and tasknum = '" & tid & "' and subtask = 0"

            pmo.Update(sql)

        ElseIf e.CommandName = "copytask" Then
            
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim row As GridViewRow = GridView1.Rows(index)

            Dim pmtskid As String = CType(row.FindControl("lblpmtskidi"), Label).Text
            Dim fuid As String = CType(row.FindControl("lblfuidi"), Label).Text
            Dim coid As String = CType(row.FindControl("lblcoidi"), Label).Text
            Dim comp As String = CType(row.FindControl("lblcompi"), Label).Text
            Dim tid As String = CType(row.FindControl("lbltnumi"), Label).Text
            Dim stid As String = CType(row.FindControl("lblstnumi"), Label).Text

            cmd.CommandText = "exec usp_copytask @pmtskid, @tasknum, @fuid, @ustr, @subtask"
            Dim param01 = New SqlParameter("@pmtskid", SqlDbType.VarChar)
            If pmtskid = "" Then
                param01.Value = System.DBNull.Value
            Else
                param01.Value = pmtskid
            End If
            cmd.Parameters.Add(param01)
            Dim param02 = New SqlParameter("@tasknum", SqlDbType.VarChar)
            If tid = "" Then
                param02.Value = System.DBNull.Value
            Else
                param02.Value = tid
            End If
            cmd.Parameters.Add(param02)
            Dim param03 = New SqlParameter("@fuid", SqlDbType.VarChar)
            If fuid = "" Then
                param03.Value = System.DBNull.Value
            Else
                param03.Value = fuid
            End If
            cmd.Parameters.Add(param03)
            Dim param04 = New SqlParameter("@ustr", SqlDbType.VarChar)
            If ustr = "" Then
                param04.Value = System.DBNull.Value
            Else
                param04.Value = ustr
            End If
            cmd.Parameters.Add(param04)
            Dim param05 = New SqlParameter("@subtask", SqlDbType.VarChar)
            If stid = "" Then
                param05.Value = System.DBNull.Value
            Else
                param05.Value = stid
            End If
            cmd.Parameters.Add(param05)

            pmo.UpdateHack(cmd)
            'or e.CommandName = hd or e.CommandName = sd
            'need show hide here
        ElseIf e.CommandName = "hd" Then
            GridView1.Columns(8).Visible = False
            GridView1.Columns(9).Visible = False
            'tbldata.Attributes.Add("width", "1010")
            tbldata.Attributes.Add("width", "748")
            GridView1.Width = 748 ' 1250
        ElseIf e.CommandName = "sd" Then
            GridView1.Columns(8).Visible = True
            GridView1.Columns(9).Visible = True
            tbldata.Attributes.Add("width", "1010")
            GridView1.Width = 1010
        End If

        If e.CommandName <> "hd" Or e.CommandName <> "sd" Then
            GridView1.EditIndex = -1
            PageNumber = txtpg.Value
            eqid = lbleqid.Value
            geteq(eqid, PageNumber)
        End If
        
        pmo.Dispose()

    End Sub

    Private Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated

        If e.Row.RowType = DataControlRowType.DataRow Then
            Try
                Dim ibcopy As ImageButton = CType(e.Row.FindControl("ibcopytask"), ImageButton)
                ibcopy.CommandArgument = e.Row.RowIndex.ToString()
                Dim ibadds As ImageButton = CType(e.Row.FindControl("ibaddsub"), ImageButton)
                ibadds.CommandArgument = e.Row.RowIndex.ToString()
                Dim ibaddc As ImageButton = CType(e.Row.FindControl("ibaddctask"), ImageButton)
                ibaddc.CommandArgument = e.Row.RowIndex.ToString()
                Dim ibaddf As ImageButton = CType(e.Row.FindControl("ibaddftask"), ImageButton)
                ibaddf.CommandArgument = e.Row.RowIndex.ToString()
            Catch ex As Exception

            End Try


        End If

    End Sub

    Private Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Try
                Dim ibas As ImageButton = CType(e.Row.FindControl("ibaddsub"), ImageButton)
                Dim ibaf As ImageButton = CType(e.Row.FindControl("ibaddftask"), ImageButton)
                Dim ibac As ImageButton = CType(e.Row.FindControl("ibaddctask"), ImageButton)
                Dim iotdi As HtmlImage = CType(e.Row.FindControl("imgotdi"), HtmlImage)
                Dim itdi As HtmlImage = CType(e.Row.FindControl("imgtdi"), HtmlImage)
                Dim imgftask As HtmlImage = CType(e.Row.FindControl("imgaddftask"), HtmlImage)
                Dim imgctask As HtmlImage = CType(e.Row.FindControl("imgaddctask"), HtmlImage)
                Dim imgcopyf As HtmlImage = CType(e.Row.FindControl("ibcopyf"), HtmlImage)
                Dim imgcopyc As HtmlImage = CType(e.Row.FindControl("ibcopyc"), HtmlImage)

                Dim fuid As String = DataBinder.Eval(e.Row.DataItem, "funcid", "")
                Dim coid As String = DataBinder.Eval(e.Row.DataItem, "comid", "")
                Dim eqid As String = DataBinder.Eval(e.Row.DataItem, "eqid", "")
                Dim eqnum As String = DataBinder.Eval(e.Row.DataItem, "eqnum", "")
                Dim ttid As String = DataBinder.Eval(e.Row.DataItem, "pmtskid", "")
                Dim tasknum As String = DataBinder.Eval(e.Row.DataItem, "tasknum", "")
                Dim subtask As String = DataBinder.Eval(e.Row.DataItem, "subtask", "")
                Dim sqty As String = DataBinder.Eval(e.Row.DataItem, "qty", "")

                If subtask <> "0" Then
                    ibas.Visible = False
                    ibaf.Visible = False
                    ibac.Visible = False
                    iotdi.Attributes("class") = "details"
                    'itdi.Attributes("class") = "details"
                    itdi.Attributes("onclick") = "getdts('" & ttid & "','" & eqid & "','" & coid & "','" & sqty & "','sub');"
                    imgcopyf.Attributes("class") = "details"
                    imgcopyc.Attributes("class") = "details"
                    imgftask.Attributes("class") = "details"
                    imgctask.Attributes("class") = "details"
                Else
                    iotdi.Attributes("onclick") = "getdt('" & ttid & "','" & eqid & "','" & coid & "','orig');"
                    itdi.Attributes("onclick") = "getdt('" & ttid & "','" & eqid & "','" & coid & "','rev');"
                    imgftask.Attributes("onclick") = "addfunctaskprof('func','" & fuid & "');"
                    imgctask.Attributes("onclick") = "addfunctaskprof('comp','" & fuid & "','" & coid & "');"

                    imgcopyf.Attributes("onclick") = "copyfunctask('func','" & ttid & "','" & fuid & "','" & coid & "');"
                    imgcopyc.Attributes("onclick") = "copyfunctask('comp','" & ttid & "','" & fuid & "','" & coid & "');"
                End If

            Catch ex As Exception
                Dim otaskid = CType(e.Row.FindControl("txtotaske"), TextBox).ClientID
                Dim taskid = CType(e.Row.FindControl("txttaske"), TextBox).ClientID
                Dim task As TextBox = CType(e.Row.FindControl("txttaske"), TextBox)
                Dim otask As TextBox = CType(e.Row.FindControl("txtotaske"), TextBox)
                otask.Attributes("onkeyup") = "document.getElementById('" & taskid & "').value=this.value;"

                Dim tnum As TextBox = CType(e.Row.FindControl("txttnume"), TextBox)
                Dim snum As TextBox = CType(e.Row.FindControl("txtstnume"), TextBox)

                Dim tasknum As String = DataBinder.Eval(e.Row.DataItem, "tasknum", "")
                Dim subtask As String = DataBinder.Eval(e.Row.DataItem, "subtask", "")
                Dim sqty As String = DataBinder.Eval(e.Row.DataItem, "qty", "")

                If subtask = "0" Then
                    snum.Enabled = False
                Else
                    tnum.Enabled = False
                End If

                Dim fuid As String = DataBinder.Eval(e.Row.DataItem, "funcid", "")
                Dim coid As String = DataBinder.Eval(e.Row.DataItem, "comid", "")
                Dim eqid As String = DataBinder.Eval(e.Row.DataItem, "eqid", "")
                Dim eqnum As String = DataBinder.Eval(e.Row.DataItem, "eqnum", "")
                Dim ttid As String = DataBinder.Eval(e.Row.DataItem, "pmtskid", "")

                Dim iotde As HtmlImage = CType(e.Row.FindControl("imgotde"), HtmlImage)
                Dim itde As HtmlImage = CType(e.Row.FindControl("imgtde"), HtmlImage)
                If subtask = "0" Then
                    iotde.Attributes("onclick") = "getdt('" & ttid & "','" & eqid & "','" & coid & "','orig');"
                    itde.Attributes("onclick") = "getdt('" & ttid & "','" & eqid & "','" & coid & "','rev');"
                Else
                    'need task details for subtask - will disable for now
                    'iotde.Attributes("src") = "../images/appbuttons/minibuttons/magnifierdis.gif"
                    iotde.Attributes("class") = "details"
                    'itde.Attributes("src") = "../images/appbuttons/minibuttons/magnifierdis.gif"
                    itde.Attributes("onclick") = "getdts('" & ttid & "','" & eqid & "','" & coid & "','" & sqty & "','sub');"
                End If

                'to return new comp selection
                Dim cbox = CType(e.Row.FindControl("txtcompe"), Label).ClientID
                Dim ico As HtmlImage = CType(e.Row.FindControl("imgcoe"), HtmlImage)
                If subtask = "0" Then
                    ico.Attributes("onclick") = "getco('" & fuid & "','" & cbox & "');"
                Else
                    ico.Attributes("src") = "../images/appbuttons/minibuttons/magnifierdis.gif"
                End If


            End Try
        End If
    End Sub

    Private Sub GridView1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridView1.RowDeleting
        Dim index As Integer = Convert.ToInt32(e.RowIndex)

        Dim row As GridViewRow = GridView1.Rows(index)
        Dim tasknum, pmtskid, fuid As String
        Try
            pmtskid = CType(row.FindControl("lblpmtskide"), Label).Text
            fuid = CType(row.FindControl("lblfuide"), Label).Text
            tasknum = CType(row.FindControl("txttnume"), TextBox).Text
        Catch ex As Exception
            pmtskid = CType(row.FindControl("lblpmtskidi"), Label).Text
            fuid = CType(row.FindControl("lblfuidi"), Label).Text
            tasknum = CType(row.FindControl("lbltnumi"), Label).Text
        End Try
        sql = "usp_delpmtask '" & fuid & "', '" & pmtskid & "', '" & tasknum & "'"
        pmo.Open()
        pmo.Update(sql)
        GridView1.EditIndex = -1
        PageNumber = txtpg.Value
        eqid = lbleqid.Value
        geteq(eqid, PageNumber)
        pmo.Dispose()
    End Sub

    Private Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridView1.RowEditing
        PageNumber = txtpg.Value
        eqid = lbleqid.Value
        GridView1.EditIndex = e.NewEditIndex
        pmo.Open()
        geteq(eqid, PageNumber)
        pmo.Dispose()
    End Sub

    Private Sub GridView1_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridView1.RowUpdating
        Dim index As Integer = Convert.ToInt32(e.RowIndex)
        'index += 1
        Dim row As GridViewRow = GridView1.Rows(index)
        Dim tasknum, otasknum, subnum, osubnum As String
        Dim taskdesc, otaskdesc, pmtskid, fuid As String
        Try
            pmtskid = CType(row.FindControl("lblpmtskide"), Label).Text
        Catch ex As Exception
            pmtskid = CType(row.FindControl("lblpmtskidi"), Label).Text
        End Try
        Try
            fuid = CType(row.FindControl("lblfuide"), Label).Text
        Catch ex As Exception
            fuid = CType(row.FindControl("lblfuidi"), Label).Text
        End Try


        tasknum = CType(row.FindControl("txttnume"), TextBox).Text
        Dim tnchk As Long
        Try
            tnchk = System.Convert.ToInt64(tasknum)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr216", "GTasksFunc2.aspx.vb")
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        otasknum = CType(row.FindControl("txtotnume"), Label).Text
        subnum = CType(row.FindControl("txtstnume"), TextBox).Text
        Dim stnchk As Long
        Try
            stnchk = System.Convert.ToInt64(subnum)
        Catch ex As Exception
            Dim strMessage As String = "Sub Task Number Must be a Non Decimal Numeric Value"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        osubnum = CType(row.FindControl("txtostnume"), Label).Text

        taskdesc = CType(row.FindControl("txttaske"), TextBox).Text
        otaskdesc = CType(row.FindControl("txtotaske"), TextBox).Text

        taskdesc = pmo.ModString2(taskdesc)
        otaskdesc = pmo.ModString2(otaskdesc)

        If Len(taskdesc) > 500 Then
            Dim strMessage As String = "Revised Task Description Limited to 500 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If

        If Len(otaskdesc) > 500 Then
            Dim strMessage As String = "Original Task Description Limited to 500 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If

        pmo.Open()
        sql = "update pmtasks set taskdesc = '" & taskdesc & "', otaskdesc = '" & otaskdesc & "' where pmtskid = '" & pmtskid & "'"
        pmo.Update(sql)
        If otasknum <> tasknum Then
            sql = "usp_reorderPMTasks '" & pmtskid & "', '" & fuid & "', '" & tasknum & "', '" & otasknum & "'"
            pmo.Update(sql)
        End If
        If osubnum <> subnum Then
            sql = "usp_reorderPMSubTasks2 '" & pmtskid & "', '" & fuid & "', '" & tasknum & "', '" & subnum & "', '" & osubnum & "'"
            pmo.Update(sql)
        End If
        GridView1.EditIndex = -1
        PageNumber = txtpg.Value
        eqid = lbleqid.Value
        geteq(eqid, PageNumber)
        pmo.Dispose()
    End Sub

    Private Sub ddproc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddproc.SelectedIndexChanged

    End Sub
End Class