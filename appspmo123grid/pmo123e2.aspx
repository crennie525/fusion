﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123e2.aspx.vb" Inherits="lucy_r12.pmo123e2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="javascript" type="text/javascript">
        function getValueAnalysis() {
            closelst();
            tl = "5"
            cid = "0"
            sid = document.getElementById("lblsid").value;
            did = document.getElementById("lbldid").value;
            clid = document.getElementById("lblclid").value;
            eqid = document.getElementById("lbleqid").value;
            //alert(eqid)
            chk = ""; // document.getElementById("lblchk").value;
            fuid = document.getElementById("lblfuid").value;
            if (eqid != "") {
                //window.parent.setref();
                var ht = "2000"; //screen.Height - 20;
                var wd = "1000"; //screen.Width - 20;

                var eReturn = window.showModalDialog("../reports/reportdialog.aspx?who=pmog&tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:1000px; dialogWidth:2000px;resizable=yes");
                //var eReturn = window.showModalDialog("../reports/reportdialog.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:610px; dialogWidth:610px");
                if (eReturn) {
                    if (eReturn != "ret") {
                        document.getElementById("lblsvchk").value = "7";
                        document.getElementById("pgflag").value = eReturn;
                        //alert(eReturn)
                        document.getElementById("form1").submit();
                    }
                }
            }
            else {
                alert("No Task Records Selected Yet!")
            }
        }
        function exporttoexcel(who) {
            closelst();

            var sessid = document.getElementById("lbleqid").value;
            var fuid = document.getElementById("lblfuid").value;
            var coid = document.getElementById("lblcoid").value;
            if (fuid == "") {
                fuid = "0"
            }
            if (coid == "") {
                coid = "0"
            }
            if (sessid != "") {
                window.open("appspmo123e2excelout.aspx?sessid=" + sessid + "&fuid=" + fuid + "&coid=" + coid + "&who=" + who)
            }
        }
        function getdyk() {
            closelst();
            //did you know drag and drop message
            var eReturn = window.showModalDialog("pmo123e2h1.aspx", "", "dialogHeight:450px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
            }
        }
        function chgdiv() {

        }
        function gotoothera() {
            var cdnd = document.getElementById("cbdnd")
            cdnd.checked = true
            document.getElementById("imgsiren1").className = "details";
            document.getElementById("imgsiren2").className = "details";
            gotoother()
        }
        function gotoother() {
            closelst();
            var cdnd = document.getElementById("cbdnd")
            var eqid = document.getElementById("lbleqid").value;
            var docid = document.getElementById("lbldocid").value;
            var docmode = document.getElementById("lbldocmode").value;
            if (eqid != "") {
                document.getElementById("lblpagemode").value = "drag";
                if (cdnd.checked == true) {
                    document.getElementById("divstart").className = "details";
                    document.getElementById("divdata").className = "details";
                    document.getElementById("divdrag").className = "container";

                    document.getElementById("imgsiren1").className = "details";
                    document.getElementById("imgsiren2").className = "details";

                    var eqnum = document.getElementById("lbleq").value;
                    var cap = "pmo123e2dnd.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&proc=" + docid;
                    document.getElementById("ifdrag").src = cap;
                    document.getElementById("lblstart").value = "yes";
                    ResizeDelay();
                    //need to check for doc
                    //alert(docid)
                    if (docid != "") {
                        //"pmo123e2docout.aspx?docid=" + docid
                        document.getElementById("ifpmdocsd").src = "pmo123e2docout.aspx?docid=" + docid;
                    }
                }
                else {
                    document.getElementById("lblpagemode").value = "";
                    document.getElementById("lblsubmit").value = "checkdoc";
                    document.getElementById("form1").submit();
                    /*
                    document.getElementById("divdata").className = "container";
                    document.getElementById("divdrag").className = "details";
                    if (docid != "Select Procedure" && docid != "") {
                        if (docmode == "sbs") {
                            document.getElementById("ifpmdocs").src = "pmo123e2docout.aspx?docid=" + docid;
                        }
                        else {
                            document.getElementById("ifpmdoc").src = "pmo123e2docout.aspx?docid=" + docid;
                        }
                    }
                    */
                }
            }
        }
        function handleweb(wfile) {
            closelst();
            var docmode = document.getElementById("lbldocmode").value;
            var pagemode = document.getElementById("lblpagemode").value;
            if (docmode == "sbs") {
                document.getElementById("ifpmdocs").src = "../eqimages/" + wfile;
            }
            else if (pagemode == "drag") {
                document.getElementById("ifpmdocsd").src = "../eqimages/" + wfile;
            }
            else {
                document.getElementById("ifpmdoc").src = "../eqimages/" + wfile;
            }
        }
        var popwin = "directories=0,height=500,width=800,location=0,menubar=0,resizable=0,status=0,toolbar=0,scrollbars=1";
        var docflag = 0;

        function checkrb(who) {
            var dndchk = document.getElementById("lblpagemode").value;
            if (dndchk != "drag") {
                closelst();
                var tstat = document.getElementById("lbltstat").value;
                var tcnt = document.getElementById("txtpgcnt").value;
                var eqid = document.getElementById("lbleqid").value;
                var docmode = document.getElementById("lbldocmode").value;
                if (tstat != " notasks" && tcnt != "0" && eqid != "") {
                    if (docmode != who) {
                        docflag = 1;
                    }
                    if (who == "ttb") {
                        docmode = "ttb"
                        document.getElementById("lbldocmode").value = "ttb";
                    }
                    else {
                        docmode = "sbs"
                        document.getElementById("lbldocmode").value = "sbs";
                    }
                    checkdoc();
                }
                else {
                    document.getElementById("rbsbs").checked = false;
                    document.getElementById("rbttb").checked = true;
                    alert("Cannot Change Document Mode with No Task Found")
                }
            }
            else {
                alert("Cannot Change Page Orientation in Drag and Drop Mode")
            }
        }

        function checkdoc() {
            //we only want to submit if doc mode is changing and page mode is not drag
            //and txtpgcnt <> 0
            //alert(document.getElementById("txtpgcnt").value)

            var tcnt = document.getElementById("txtpgcnt").value;
            if (tcnt == "0") {
                document.getElementById("lbltstat").value = "notasks";
            }
            var docid = document.getElementById("ddproc").value;
            if (docid != "Select Procedure" && docid != "") {
                var docmode = document.getElementById("lbldocmode").value;
                var pagemode = document.getElementById("lblpagemode").value;
                document.getElementById("lbldocid").value = docid;
                //alert(document.getElementById("lbldocmode").value)

                var rsbs = document.getElementById("rbsbs");
                var rttb = document.getElementById("rbttb");
                if (docmode == "none") {
                    docflag = 1;
                    if (rsbs.checked == true) {
                        docmode = "sbs"
                    }
                    else {
                        docmode = "ttb"
                    }
                    if (docmode == "ttb") {
                        docmode = "ttb"
                        document.getElementById("lbldocmode").value = "ttb";
                    }
                    else if (docmode == "sbs" || pagemode == "drag") {
                        docmode = "sbs"
                        document.getElementById("lbldocmode").value = "sbs";
                    }
                    else {
                        docmode = "ttb"
                        document.getElementById("lbldocmode").value = "ttb";
                    }
                }
                else {
                    if (docmode == "ttb") {
                        docmode = "ttb"
                        document.getElementById("lbldocmode").value = "ttb";
                    }
                    else if (docmode == "sbs" || pagemode == "drag") {
                        docmode = "sbs"
                        document.getElementById("lbldocmode").value = "sbs";
                    }
                    else {
                        docmode = "ttb"
                        document.getElementById("lbldocmode").value = "ttb";
                    }
                }
                //alert(docmode)
                if (pagemode != "drag") {
                    if (docflag == 1) {
                        docflag = 0;
                        //if(tcnt != "0") {
                        document.getElementById("lblsubmit").value = "checkdoc";
                        document.getElementById("form1").submit();
                        // }
                        /*
                        else {
                        if (docmode == "sbs") {
                        //alert(cant switch page mode with 0 tasks
                        //document.getElementById("ifpmdocs").src = "pmo123e2docout.aspx?docid=" + docid;
                        }
                        else {
                        document.getElementById("p1").className = "frametext";
                        document.getElementById("p2").className = "frametext";
                        document.getElementById("trdocbot").className = "view";
                        document.getElementById("tddocside").className = "details";
                        document.getElementById("p3").className = "details";
                        document.getElementById("ifpmdoc").src = "pmo123e2docout.aspx?docid=" + docid;
                        }
                        }
                        */
                    }
                    else {
                        if (docmode == "sbs") {
                            document.getElementById("ifpmdocs").src = "pmo123e2docout.aspx?docid=" + docid;
                        }
                        else {
                            document.getElementById("ifpmdoc").src = "pmo123e2docout.aspx?docid=" + docid;
                        }
                    }

                }
                else {
                    document.getElementById("ifpmdocsd").src = "pmo123e2docout.aspx?docid=" + docid;
                }
            }
        }
        function filtlook1() {
            //document.getElementById("plst").style.visible = true;
        /*
            document.getElementById("lstdiv").style.position = "absolute";
            document.getElementById("lstdiv").style.top = "100px";
            document.getElementById("lstdiv").style.left = "100px";
            document.getElementById("lstdiv").className = "view";
            */
            //document.getElementById("lblreturn").value = ret;
           // document.getElementById("iflst").src = "lillook.aspx?list=" + list + "&col=" + col;
        }
        function closelst1() {
            document.getElementById("lstdiv").className = "details";
        }
        function checkpage() {
            $get('plst').style.visibility = 'hidden'
            var tstat = document.getElementById("lbltstat").value;
            if (tstat == "notasks") {
                document.getElementById("tbldata").style.top = "306px";
            }
            else {
                document.getElementById("tbldata").style.top = "106px";
            }
            //alert(document.getElementById("txtpgcnt").value)
            checkit();
            retpos();
            var rettyp = document.getElementById("lblrettyp").value;
            if (rettyp == "depts") {
                document.getElementById("trdepts").className = "view";
                document.getElementById("treqdets").className = "view";
                document.getElementById("tddept").innerHTML = document.getElementById("lbldept").value;
                document.getElementById("tdcell").innerHTML = document.getElementById("lblcell").value;
                document.getElementById("tdeq").innerHTML = document.getElementById("lbleq").value;
                document.getElementById("tdfu").innerHTML = document.getElementById("lblfu").value;
                document.getElementById("tdco").innerHTML = document.getElementById("lblcomp").value;
            }
            else if (rettyp == "locs") {
                document.getElementById("trlocs").className = "view";
                document.getElementById("treqdets").className = "view";
                document.getElementById("tdloc3").innerHTML = document.getElementById("lblloc").value;
                document.getElementById("tdeq").innerHTML = document.getElementById("lbleq").value;
                document.getElementById("tdfu").innerHTML = document.getElementById("lblfu").value;
                document.getElementById("tdco").innerHTML = document.getElementById("lblcomp").value;
            }
        }
        //use next 2 for div pos - per div
        function checkpos() {
            var p1 = document.getElementById("p1");
            var p1x, p1y;
            p1x = p1.scrollTop;
            p1y = p1.scrollLeft;
            document.getElementById("lblp1xpos").value = p1x;
            document.getElementById("lblp1ypos").value = p1y;
        }
        function retpos() {
            var p1 = document.getElementById("p1");
            var p1x, p1y;
            p1x = document.getElementById("lblp1xpos").value;
            p1y = document.getElementById("lblp1ypos").value;
            p1.scrollTop = p1x;
            p1.scrollLeft = p1y;

            var p2 = document.getElementById("p2");
            var p2x, p2y;
            p2x = document.getElementById("lblp2xpos").value;
            p2y = document.getElementById("lblp2ypos").value;
            p2.scrollTop = p2x;
            p2.scrollLeft = p2y;

            var p3 = document.getElementById("p3");
            var p3x, p3y;
            p3x = document.getElementById("lblp3xpos").value;
            p3y = document.getElementById("lblp3ypos").value;
            p3.scrollTop = p3x;
            p3.scrollLeft = p3y;

            var p4 = document.getElementById("p4");
            var p4x, p4y;
            p4x = document.getElementById("lblp4xpos").value;
            p4y = document.getElementById("lblp4ypos").value;
            p4.scrollTop = p4x;
            p4.scrollLeft = p4y;
        }

        function checkposp2() {
            var p2 = document.getElementById("p2");
            var p2x, p2y;
            p2x = p2.scrollTop;
            p2y = p2.scrollLeft;
            document.getElementById("lblp2xpos").value = p2x;
            document.getElementById("lblp2ypos").value = p2y;
        }
        function checkposp3() {
            var p3 = document.getElementById("p3");
            var p3x, p3y;
            p3x = p3.scrollTop;
            p3y = p3.scrollLeft;
            document.getElementById("lblp3xpos").value = p3x;
            document.getElementById("lblp3ypos").value = p3y;
        }
        function checkposp4() {
            var p4 = document.getElementById("p4");
            var p4x, p4y;
            p4x = p4.scrollTop;
            p4y = p4.scrollLeft;
            document.getElementById("lblp4xpos").value = p4x;
            document.getElementById("lblp4ypos").value = p4y;
        }

        function GetProcDiv() {
            closelst();
            var eqid = document.getElementById("lbleqid").value;
            var eqnum = document.getElementById("lbleq").value;
            if (eqid != "") {
                var eReturn = window.showModalDialog("../appsopt/OptDialog.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:580px; resizable=yes");
            }
            if (eReturn) {
                document.getElementById("lblsubmit").value = "getproc";
                document.getElementById("form1").submit();
            }
        }
        function checkit() {
            sstchur_SmartScroller_Scroll();
        }

        function resetfilt() {
            document.getElementById("lblfilt").value = "";
            document.getElementById("lblret").value = "first";
            document.getElementById("form1").submit();
        }
        function filtit(who) {
            if (who == "fu") {
                var fuid = document.getElementById("lblfuid").value
                if (fuid != "") {
                    document.getElementById("lblfilt").value = "func"
                    document.getElementById("lblret").value = "first"
                    document.getElementById("form1").submit();
                }
            }
            if (who == "co") {
                var coid = document.getElementById("lblcoid").value
                if (coid != "") {
                    document.getElementById("lblfilt").value = "comp"
                    document.getElementById("lblret").value = "first"
                    document.getElementById("form1").submit();
                }
            }
        }
        function geteqtabs(who) {
            closelst();
            //alert(who)
            var sid = document.getElementById("lblsid").value;
            var did = document.getElementById("lbldid").value;
            var dept = document.getElementById("lbldept").value;
            var clid = document.getElementById("lblclid").value;
            var cell = document.getElementById("lblcell").value;
            var eqid = document.getElementById("lbleqid").value;
            var fuid = document.getElementById("lblfuid").value;
            var coid = document.getElementById("lblcoid").value;
            var eq = document.getElementById("lbleq").value;
            var lid = document.getElementById("lbllid").value;
            var loc = document.getElementById("lblloc").value;
            var typ = document.getElementById("lblrettyp").value;
            var gototasks = "1";
            var ustr = document.getElementById("lblustr").value;
            if (who == "eqready" && (eqid == "" || eqid == "0")) {
                alert("No Equipment Record Selected")
            }
            else {
                var eReturn = window.showModalDialog("../appspmo123tab/pmo123tabeqmaindialog.aspx?who=" + who + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&clid=" + clid +
            "&cell=" + cell + "&eqid=" + eqid + "&eq=" + eq + "&lid=" + lid + "&loc=" + loc + "&rettyp=" + typ + "&gototasks=" + gototasks +
            "&fuid=" + fuid + "&coid=" + coid + "&ncid=&ustr=" + ustr + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:1200px; resizable=yes");
                if (eReturn) {
                    //document.getElementById("lbleqid").value = eReturn;
                    //var eqid = eReturn;
                    //var ret = did + "~" + dept + "~" + clid + "~" + cell + "~" + eqid + "~" + eq + "~" + lid + "~" + loc + "~" + typ;
                    var ret = eReturn.split("~");
                    //alert(eReturn)
                    document.getElementById("lbldid").value = ret[0];
                    document.getElementById("lbldept").value = ret[1];
                    document.getElementById("lblclid").value = ret[2];
                    document.getElementById("lblcell").value = ret[3];
                    document.getElementById("lbleqid").value = ret[4];
                    var eqid = ret[4];

                    document.getElementById("lbleq").value = ret[5];
                    document.getElementById("lbllid").value = ret[6];
                    document.getElementById("lblloc").value = ret[7];
                    document.getElementById("lblrettyp").value = ret[8];
                    var rettyp = ret[8];
                    if (rettyp == "dept") {
                        document.getElementById("tddept").innerHTML = ret[1];
                        document.getElementById("tdcell").innerHTML = ret[3];
                        //document.getElementById("tdeq").innerHTML = ret[5];
                        document.getElementById("trdepts").className = "view";
                        document.getElementById("treqdets").className = "view";
                    }
                    else if (rettyp == "loc") {
                        document.getElementById("tdloc3").innerHTML = ret[7];
                        document.getElementById("trlocs").className = "view";
                        document.getElementById("treqdets").className = "view";
                    }

                    if (eqid != "") {
                        document.getElementById("lblsubmit").value = "getdept";
                        document.getElementById("form1").submit();
                    }
                }
            }
        }

        function copyfunctask(who, pmtskid, fuid, coid) {
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                var eqid = document.getElementById("lbleqid").value;
                var sid = document.getElementById("lblsid").value;
                var ustr = document.getElementById("lblustr").value;
                //"pmo123e2copytofunc.aspx?sid=" + sid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&who=" + who + "&pmtskid=" + pmtskid + "&ustr=" + ustr
                var eReturn = window.showModalDialog("pmo123e2copytofunc.aspx?sid=" + sid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&who=" + who + "&pmtskid=" + pmtskid + "&ustr=" + ustr + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                if (eReturn) {
                    document.getElementById("lblret").value = "rettask"
                    document.getElementById("form1").submit();
                }
            }
        }
        function addfunctask() {
            closelst();
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                var fuid = document.getElementById("lblfuid").value
                if (fuid != "") {
                    document.getElementById("lblret").value = "addfunctask"
                    document.getElementById("form1").submit();
                }
            }
        }
        function addcomptask() {
            closelst();
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                var coid = document.getElementById("lblcoid").value
                var fuid = document.getElementById("lblfuid").value
                if (fuid != "") {
                    document.getElementById("lblret").value = "addcomptask"
                    document.getElementById("form1").submit();
                }
            }
        }

        function addfunctaskprof(who, id, id2) {
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                document.getElementById("lblwho").value = who;
                if (id == "0") {
                    if (who == "func") {
                        id = document.getElementById("lblfuid").value;
                    }
                    else if (who == "comp") {
                        id = document.getElementById("lblfuid").value;
                        id2 = document.getElementById("lblcoid").value;
                    }
                }
                document.getElementById("lblretid").value = id;
                document.getElementById("lblretid2").value = id2;
                if (id != "") {
                    var eqid = document.getElementById("lbleqid").value;
                    var eReturn = window.showModalDialog("pmo123e2profiledialog.aspx?eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:1100px; resizable=yes");
                    if (eReturn) {
                        /*
                        profile = ttid & "~" & tasktype & "~"
                        profile += ptid & "~" & pretech & "~"
                        profile += skillid & "~" & skill & "~"
                        profile += qty & "~"
                        profile += rdid & "~" & rd & "~"
                        profile += freq & "~"
                        profile += meterid & "~" & meterfreq & "~"
                        profile += maxdays & "~" & fixed & "~"
                        profile += rdt
                        */
                        var retarr = eReturn.split("~");
                        document.getElementById("lblttid").value = retarr[0];
                        document.getElementById("lbltasktype").value = retarr[1];
                        document.getElementById("lblptid").value = retarr[2];
                        document.getElementById("lblpretech").value = retarr[3];
                        document.getElementById("lblskillid").value = retarr[4];
                        document.getElementById("lblskill").value = retarr[5];
                        document.getElementById("lblqty").value = retarr[6];
                        document.getElementById("lblrdid").value = retarr[7];
                        document.getElementById("lblrd").value = retarr[8];
                        document.getElementById("lblfreq").value = retarr[9];
                        document.getElementById("lblmeterid").value = retarr[10];
                        document.getElementById("lblmeterfreq").value = retarr[11];
                        document.getElementById("lblmaxdays").value = retarr[12];
                        document.getElementById("lblfixed").value = retarr[13];
                        document.getElementById("lblrdt").value = retarr[14];
                        document.getElementById("lblmeterunit").value = retarr[15];

                        document.getElementById("lblret").value = "addptask"
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        function getdt(ttid, eqid, coid, who) {
            var usetot = document.getElementById("lblusetot").value;
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                var sid = document.getElementById("lblsid").value;
                var ustr = document.getElementById("lblustr").value;
                var eReturn = window.showModalDialog("pmo123e2rdetsdialog.aspx?eqid=" + eqid + "&ttid=" + ttid + "&coid=" + coid + "&who=" + who + "&sid=" + sid + "&ustr=" + ustr + "&usetot=" + usetot + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:600px; resizable=yes");
                if (eReturn) {

                }
            }
        }


        function getdts(ttid, eqid, coid, sqty, who) {
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                var sid = document.getElementById("lblsid").value;
                var ustr = document.getElementById("lblustr").value;
                var eReturn = window.showModalDialog("pmo123e2rdetsdialog.aspx?eqid=" + eqid + "&ttid=" + ttid + "&coid=" + coid + "&who=" + who + "&sid=" + sid + "&ustr=" + ustr + "&sqty=" + sqty + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:600px; resizable=yes");
                if (eReturn) {

                }
            }
        }


        function getco(fuid, cbox) {
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                var retflg = 0;
                if (fuid == "0") {
                    fuid = document.getElementById("lblfuid").value;
                    var rettyp = document.getElementById("lblrettyp").value;
                    //if (rettyp == "depts") {
                    cbox = document.getElementById("tdco");
                    //}
                    //else {
                    //    cbox = document.getElementById("tdco3");
                    //}
                    retflg = 1;
                }
                if (fuid != "") {
                //alert(fuid)
                    var eReturn = window.showModalDialog("../equip/complistdialog.aspx?fuid=" + fuid + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                    if (eReturn) {
                        //returns coid and comp
                        var ret = eReturn;
                        var retarr = ret.split(",");
                        var coid = retarr[0];
                        var comp = retarr[1];
                        document.getElementById(cbox).innerHTML = comp;
                        if (retflg == 0) {
                            document.getElementById(lblretcompid).value = coid;
                        }
                        else {
                            document.getElementById(lblcoid).value = coid;
                            document.getElementById("lblcomp").value = comp;
                            //need postback

                        }

                    }
                }
            }
        }
        function getco2() {
            var pagemode = document.getElementById("lblpagemode").value;
            var fuid = document.getElementById("lblfuid").value;
            if (pagemode != "drag") {
                var retflg = 0;
                if (fuid == "0") {
                    fuid = document.getElementById("lblfuid").value;
                    var rettyp = document.getElementById("lblrettyp").value;
                    //if (rettyp == "depts") {
                    cbox = document.getElementById("tdco");
                    //}
                    //else {
                    //    cbox = document.getElementById("tdco3");
                    //}
                    retflg = 1;
                }
                if (fuid != "") {
                    //alert(fuid)
                    var eReturn = window.showModalDialog("../equip/complistdialog.aspx?fuid=" + fuid + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                    if (eReturn) {
                        //returns coid and comp
                        var ret = eReturn;
                        if (ret != "no") {
                            var retarr = ret.split(",");
                            var coid = retarr[0];
                            var comp = retarr[1];
                            cbox = document.getElementById("tdco");
                            document.getElementById("tdco").innerHTML = comp;
                            if (retflg == 0) {
                                document.getElementById("lblcoid").value = coid;
                                document.getElementById("lblcomp").value = comp;
                            }
                            else {
                                document.getElementById("lblcoid").value = coid;
                                document.getElementById("lblcomp").value = comp;
                                //need postback

                            }
                        }
                        

                    }
                }
            }
        }
        function getfu2() {
            closelst();
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                var eqnum = document.getElementById("lbleq").value;
                var eqid = document.getElementById("lbleqid").value;
                if (eqid != "") {
                    var eReturn = window.showModalDialog("../equip/fulist2dialog.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                    if (eReturn) {
                        var ret = eReturn;
                        if (ret != "no") {
                            var retarr = ret.split(",");
                            var fid = retarr[0];
                            var cid = retarr[1];
                            var func = retarr[2];
                            var comp = retarr[3];
                            document.getElementById("lblcoid").value = cid;
                            document.getElementById("lblcomp").value = comp;
                            document.getElementById("lblfuid").value = fid;
                            document.getElementById("lblfu").value = func;

                            var rettyp = document.getElementById("lblrettyp").value;

                            //if (rettyp == "depts") {
                            document.getElementById("tdfu").innerHTML = func;
                            document.getElementById("tdco").innerHTML = comp;
                            //}
                            //else {
                            //    document.getElementById("tdfu").innerHTML = func;
                            //    document.getElementById("tdco3").innerHTML = comp;
                            //}

                            //need postback
                            //alert()
                            //document.getElementById("lblfilt").value = "func";
                            //alert(document.getElementById("lblfilt").value)
                            //document.getElementById("lblret").value = "first";
                            //document.getElementById("form1").submit();
                        }
                        
                        

                    }
                }
            }
        }
        function getfu(eqid, eqnum, lblfid, lblcid, txtf, txtc) {
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                eqnum = eqnum.replace(/#/, "%23");
                var eReturn = window.showModalDialog("../equip/fulist2dialog.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                if (eReturn) {
                    var ret = eReturn;
                    if (ret != "no") {
                        var retarr = ret.split(",");
                        var fid = retarr[0];
                        var cid = retarr[1];
                        var func = retarr[2];
                        var comp = retarr[3];
                        if (fid == "log") {
                            //window.parent.setref();
                        }
                        else {
                            if (fid != "no") {
                                if (cid != "") {
                                    //document.getElementById(lblfid).value = fid;
                                    document.getElementById(txtf).innerHTML = func;
                                    //document.getElementById(lblcid).value = cid;
                                    document.getElementById(txtc).innerHTML = comp;
                                }
                                else {
                                    document.getElementById(lblfid).innerHTML = fid;
                                    document.getElementById(txtf).value = func;
                                }
                            }
                        }
                    }
                    
                }
            }
        }

        function getminsrch() {
            closelst();
            var did = document.getElementById("lbldid").value;
                //alert(did)
            if (did != "") {
                retminsrch();
            }
            else {
                var sid = document.getElementById("lblsid").value;
                var wo = "";
                var typ;
                if (wo == "") {
                    typ = "lu";
                }
                else {
                    typ = "wo";
                }
                var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    clearall();
                    var ret = eReturn.split("~");
                    document.getElementById("lbldid").value = ret[0];
                    document.getElementById("lbldept").value = ret[1];
                    document.getElementById("tddept").innerHTML = ret[1];
                    document.getElementById("lblclid").value = ret[2];
                    document.getElementById("lblcell").value = ret[3];
                    document.getElementById("tdcell").innerHTML = ret[3];
                    document.getElementById("lbleqid").value = ret[4];
                    document.getElementById("lbleq").value = ret[5];
                    document.getElementById("tdeq").innerHTML = ret[5];
                    document.getElementById("lblfuid").value = ret[6];
                    document.getElementById("lblfu").value = ret[7];
                    document.getElementById("tdfu").innerHTML = ret[7];
                    document.getElementById("lblcoid").value = ret[8];
                    document.getElementById("lblcomp").value = ret[9];
                    document.getElementById("tdco").innerHTML = ret[9];
                    document.getElementById("lbllid").value = ret[12];
                    document.getElementById("lblloc").value = ret[13];
                    document.getElementById("trdepts").className = "view";
                    document.getElementById("treqdets").className = "view";
                    document.getElementById("lblrettyp").value = "depts";
                    var did = ret[0];
                    var eqid = ret[4];
                    var clid = ret[2];
                    var fuid = ret[6];
                    var coid = ret[8];
                    var lid = ret[12];
                    var typ;
                    if (lid == "") {
                        typ = "reg";
                    }
                    else {
                        typ = "dloc";
                    }
                    var task = "";
                    if (eqid != "") {
                        document.getElementById("lblsubmit").value = "getdept";
                        document.getElementById("form1").submit();
                    }

                    //window.location = "pmget.aspx?jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;
                }
            }
        }
        function retminsrch(who) {
            closelst();
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "ret";
            var did = document.getElementById("lbldid").value;
            var dept = document.getElementById("lbldept").value;
            var clid = document.getElementById("lblclid").value;
            var cell = document.getElementById("lblcell").value;
            var eqid = document.getElementById("lbleqid").value;
            var eq = document.getElementById("lbleq").value;
            var fuid = document.getElementById("lblfuid").value;
            var fu = document.getElementById("lblfu").value;
            var coid = document.getElementById("lblcoid").value;
            var comp = document.getElementById("lblcomp").value;
            var lid = document.getElementById("lbllid").value;
            var loc = document.getElementById("lblloc").value;
            //if (cell == "" && who != "checkfu" && who != "checkeq") {
                who = "deptret";
            //}
            //alert("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who)
            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("lbldept").value = ret[1];
                document.getElementById("tddept").innerHTML = ret[1];
                document.getElementById("lblclid").value = ret[2];
                document.getElementById("lblcell").value = ret[3];
                document.getElementById("tdcell").innerHTML = ret[3];
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("lbleq").value = ret[5];
                document.getElementById("tdeq").innerHTML = ret[5];
                document.getElementById("lblfuid").value = ret[6];
                document.getElementById("lblfu").value = ret[7];
                document.getElementById("tdfu").innerHTML = ret[7];
                document.getElementById("lblcoid").value = ret[8];
                document.getElementById("lblcomp").value = ret[9];
                document.getElementById("lbllid").value = ret[12];
                document.getElementById("lblloc").value = ret[13];
                document.getElementById("trdepts").className = "view";
                document.getElementById("lblrettyp").value = "depts";
                var did = ret[0];
                var eqid = ret[4];
                var clid = ret[2];
                var fuid = ret[6];
                var coid = ret[8];
                var lid = ret[12];
                var typ;
                //if(lid=="") {
                //typ = "reg";
                //}
                //else {
                //typ = "dloc";
                //}
                typ = "reg"
                var task = "";
                if (eqid != "") {
                    document.getElementById("lblsubmit").value = "getdept";
                    document.getElementById("form1").submit();
                }

                //window.location = "pmget.aspx?jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;
            }
        }
        function getlocs1() {
            closelst();
            var sid = document.getElementById("lblsid").value;
            var lid = document.getElementById("lbllid").value;
            var typ = "lu"
            //alert(lid)
            if (lid != "") {
                typ = "retloc"
            }
            var eqid = document.getElementById("lbleqid").value;
            var fuid = document.getElementById("lblfuid").value;
            var coid = document.getElementById("lblcoid").value;
            //alert(fuid)
            var wo = "";
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                //ret = lidi + "~" + lid + "~" + loc + "~" + eq + "~" + eqnum + "~" + fu + "~" + func + "~" + co + "~" + comp + "~" + lev;
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("lblloc").value = ret[1];
                document.getElementById("tdloc3").innerHTML = ret[2];

                document.getElementById("lbleq").value = ret[4];
                document.getElementById("tdeq").innerHTML = ret[4];
                document.getElementById("lbleqid").value = ret[3];

                document.getElementById("lblfuid").value = ret[5];
                document.getElementById("lblfu").value = ret[6];
                document.getElementById("tdfu").innerHTML = ret[6];

                document.getElementById("lblcoid").value = ret[7];
                document.getElementById("lblcomp").value = ret[8];

                document.getElementById("lbllevel").value = ret[9];

                document.getElementById("trlocs").className = "view";
                document.getElementById("treqdets").className = "view";
                document.getElementById("lblrettyp").value = "locs";
                var did = "";
                var eqid = ret[3];
                var clid = "";
                var fuid = ret[5];
                var coid = ret[7];
                var lid = ret[0];
                var typ;
                typ = "loc";
                var task = "";
                if (eqid != "") {
                    document.getElementById("lblsubmit").value = "getdept";
                    document.getElementById("form1").submit();
                }
            }
        }
        function clearall() {
            document.getElementById("lbldid").value = "";
            document.getElementById("lbldept").value = "";
            document.getElementById("tddept").innerHTML = "";
            document.getElementById("lblclid").value = "";
            document.getElementById("lblcell").value = "";
            document.getElementById("tdcell").innerHTML = "";
            document.getElementById("lbleqid").value = "";
            document.getElementById("lbleq").value = "";
            document.getElementById("tdeq").innerHTML = "";
            document.getElementById("lblfuid").value = "";
            document.getElementById("lblfu").value = "";
            document.getElementById("tdfu").innerHTML = "";
            document.getElementById("lblcoid").value = "";
            document.getElementById("lbllid").value = "";
            document.getElementById("lblloc").value = "";
            //document.getElementById("tdloc3").innerHTML = "";
            //document.getElementById("tdeq3").innerHTML = "";
            //document.getElementById("tdfu3").innerHTML = "";
            document.getElementById("trdepts").className = "details";
            document.getElementById("trlocs").className = "details";
            document.getElementById("treqdets").className = "details";
            document.getElementById("lblrettyp").value = "";

            document.getElementById("lbllevel").value = "";
        }

        function GetCompDiv() {
            closelst();
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                //window.parent.setref();
                cid = "0"; //document.getElementById("lblcid").value;
                eqid = document.getElementById("lbleqid").value;
                fuid = document.getElementById("lblfuid").value;
                ro = "0"; //document.getElementById("lblro").value;
                did = document.getElementById("lbldid").value;
                clid = document.getElementById("lblclid").value;
                var eReturn = window.showModalDialog("../equip/CompDialog.aspx?usr=&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&did=" + did + "&clid=" + clid + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:700px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    document.getElementById("lblret").value = "rettask"
                    document.getElementById("form1").submit();
                }
            }
        }

        function GetFuncDiv2() {
            closelst();
            //window.parent.setref();
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                cid = "0";
                eqid = document.getElementById("lbleqid").value;
                ro = "0";
                var eReturn = window.showModalDialog("../equip/AddEditFuncDialog.aspx?cid=" + cid + "&eqid=" + eqid + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:600px; dialogWidth:750px; resizable=yes");
                if (eReturn) {
                    //alert(eReturn)
                    var fuid = document.getElementById("lblfuid").value = eReturn;
                    document.getElementById("lblfilt").value = "func"
                    document.getElementById("lblret").value = "first"
                    document.getElementById("form1").submit();

                }
            }
        }

        function GetCompCopy() {
            closelst();
            //window.parent.setref();
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                cid = "0"; //document.getElementById("lblcid").value;
                eqid = document.getElementById("lbleqid").value;
                fuid = document.getElementById("lblfuid").value;
                sid = document.getElementById("lblsid").value;
                ro = "0"; //document.getElementById("lblro").value;
                db = document.getElementById("lbldb").value;
                oloc = document.getElementById("lbloloc").value;
                did = document.getElementById("lbldid").value;
                clid = document.getElementById("lblclid").value;
                var eReturn = window.showModalDialog("../equip/CompCopyMiniDialog.aspx?usr=&cid=" + cid + "&sid=" + sid + "&eqid=" + eqid + "&fuid=" + fuid + "&ro=" + ro + "&did=" + did + "&clid=" + clid + "&date=" + Date() + "&db=" + db + "&oloc=" + oloc, "", "dialogHeight:700px; dialogWidth:880px; resizable=yes");
                if (eReturn) {
                    document.getElementById("lblcoid").value = eReturn;
                    document.getElementById("lblfilt").value = "func"
                    document.getElementById("lblret").value = "first"
                    document.getElementById("form1").submit();
                }
            }
        }

        function GetFuncCopy2() {
            closelst();
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                //handleapp();
                cid = "0";
                sid = document.getElementById("lblsid").value;
                did = document.getElementById("lbldid").value;
                clid = document.getElementById("lblclid").value;
                eqid = document.getElementById("lbleqid").value;
                ro = "0";

                if (eqid.length != 0 || eqid != "" || eqid != "0") {
                    //(eReturn) {
                    if (eReturn == "log") {
                        window.parent.handlelogout();
                    }
                    else if (eReturn != "no") {
                        document.getElementById("lblfuid").value = eReturn;
                        document.getElementById("lblfilt").value = "func"
                        document.getElementById("lblret").value = "first"
                        document.getElementById("form1").submit();

                    }

                }
                else {
                    alert("No Equipment Record Selected")
                }
            }
        }

        function GetEqDiv2() {
            closelst();
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                //handleapp();
                sid = document.getElementById("lblsid").value;
                dept = document.getElementById("lbldept").value;
                cell = document.getElementById("lblclid").value;
                eqid = document.getElementById("lbleqid").value;
                lid = document.getElementById("lbllid").value;
                ro = "0";
                var chk = document.getElementById("lblrettyp").value;
                if (chk != "") {
                    //window.parent.setref();
                    var eReturn = window.showModalDialog("../equip/EqPopDialog.aspx?sid=" + sid + "&dept=" + dept + "&cell=" + cell + "&eqid=" + eqid + "&lid=" + lid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:500px; resizable=yes");
                    if (eReturn) {
                        if (eReturn == "log") {
                            window.parent.handlelogout();
                        }
                        else if (eReturn != "no") {
                            document.getElementById("lbleqid").value = eReturn;
                            var eqid = eReturn;
                            if (eqid != "") {
                                document.getElementById("lblsubmit").value = "getdept";
                                document.getElementById("form1").submit();
                            }

                        }
                    }
                }
                else {
                    alert("Error Retrieving Location Data")
                }
            }
        }
        function GetEqCopy2() {
            closelst();
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                //handleapp();
                sid = document.getElementById("lblsid").value;
                dept = document.getElementById("lbldept").value;
                cell = document.getElementById("lblclid").value;
                eqid = document.getElementById("lbleqid").value;
                lid = document.getElementById("lbllid").value;
                ro = "0";
                var chk = document.getElementById("lblrettyp").value;
                if (chk != "") {
                    //window.parent.setref();
                    var eReturn = window.showModalDialog("../equip/EQCopyMiniDialog.aspx?sid=" + sid + "&dept=" + dept + "&cell=" + cell + "&eqid=" + eqid + "&lid=" + lid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:660px; dialogWidth:660px; resizable=yes");
                    if (eReturn) {
                        if (eReturn == "log") {
                            window.parent.handlelogout();
                        }
                        else if (eReturn != "no") {
                            document.getElementById("lbleqid").value = eReturn;
                            var eqid = eReturn;
                            if (eqid != "") {
                                document.getElementById("lblsubmit").value = "getdept";
                                document.getElementById("form1").submit();
                            }
                        }

                    }
                }
                else {
                    alert("Error Retrieving Location Data")
                }
            }
        }
        function getappr() {
            closelst();
            //window.parent.setref();
            var eqid = document.getElementById("lbleqid").value;
            var eReturn = window.showModalDialog("../utils/PMApprovalDialog.aspx?eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:650px; dialogWidth:860px; resizable=yes");
            if (eReturn) {

            }
        }
        function getnext() {
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                var cnt = document.getElementById("txtpgcnt").value;
                var pg = document.getElementById("txtpg").value;
                pg = parseInt(pg);
                cnt = parseInt(cnt)
                if (pg < cnt) {
                    document.getElementById("lblret").value = "next"
                    document.getElementById("form1").submit();
                }
            }
        }
        function getlast() {
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                var cnt = document.getElementById("txtpgcnt").value;
                var pg = document.getElementById("txtpg").value;
                pg = parseInt(pg);
                cnt = parseInt(cnt)
                if (pg < cnt) {
                    document.getElementById("lblret").value = "last"
                    document.getElementById("form1").submit();
                }
            }
        }
        function getprev() {
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                var cnt = document.getElementById("txtpgcnt").value;
                var pg = document.getElementById("txtpg").value;
                pg = parseInt(pg);
                cnt = parseInt(cnt)
                if (pg > 1) {
                    document.getElementById("lblret").value = "prev"
                    document.getElementById("form1").submit();
                }
            }
        }
        function getfirst() {
            var pagemode = document.getElementById("lblpagemode").value;
            if (pagemode != "drag") {
                var cnt = document.getElementById("txtpgcnt").value;
                var pg = document.getElementById("txtpg").value;
                pg = parseInt(pg);
                cnt = parseInt(cnt)
                if (pg > 1) {
                    document.getElementById("lblret").value = "first"
                    document.getElementById("form1").submit();
                }
            }
        }

        function sstchur_SmartScroller_GetCoords() {

            var scrollX, scrollY;

            if (document.all) {
                if (!document.documentElement.scrollLeft)
                    scrollX = document.body.scrollLeft;
                else
                    scrollX = document.documentElement.scrollLeft;

                if (!document.documentElement.scrollTop)
                    scrollY = document.body.scrollTop;
                else
                    scrollY = document.documentElement.scrollTop;
            }
            else {
                scrollX = window.pageXOffset;
                scrollY = window.pageYOffset;
            }
            //document.getElementById("tdcoords").innerHTML = scrollX + ", " + scrollY;
            document.getElementById("xCoord").value = scrollX;
            document.getElementById("yCoord").value = scrollY;

        }
        function sstchur_SmartScroller_Scroll() {
            var x = document.getElementById("xCoord").value;
            var y = document.getElementById("yCoord").value;
            if (x == "") {
                x = 0;
            }
            if (y == "") {
                y = 0;
            }
            //document.getElementById("tdcoords").innerHTML = x + ", " + y;
            //alert("sstchur_SmartScroller_Scroll2(" + x + "," + y + ");")
            window.setTimeout("sstchur_SmartScroller_Scroll2(" + x + "," + y + ");", 100);
            //window.scrollTo(x, y);


        }
        function sstchur_SmartScroller_Scroll2(x, y) {
            window.scrollTo(x, y);
        }

        window.onscroll = sstchur_SmartScroller_GetCoords;
        window.onkeypress = sstchur_SmartScroller_GetCoords;
        window.onclick = sstchur_SmartScroller_GetCoords;
    </script>
    <style type="text/css">
        body
        {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
            padding: 5em 0 0 0;
        }
        .sc
        {
            position: fixed;
            _position: absolute;
            top: 0;
            _top: expression(eval(document.body.scrollTop));
            left: 4;
            margin: 0;
            padding: 0;
            background: white;
            z-index: 200;
            height: 64;
        }
        .gc
        {
            position: fixed;
            _position: absolute;
            top: 64;
            _top: expression(eval(document.body.scrollTop)); /*
  left:0;
  margin:0;
  padding:0;
  background:lime;
  */
            z-index: 200;
        }
        /* if you want to emulate bottom:0;, use this:
 h1{
  position:fixed;
  _position:absolute;
  bottom:0;
  _top:expression(document.body.scrollTop+document.body.clientHeight-this.clientHeight);
  background:red;
 */
        .frameText
        {
            width: 1010px;
            height: 700px;
            overflow: auto;
            float: left;
            background-color: #ffffff;
            border-style: solid;
            border-width: 1px;
            border-color: Gray;
            font-family: Helvetica;
            line-height: normal;
        }
        .frameText0
        {
            /*width:100px;
	height:100px;*/
            overflow: auto;
            float: left;
            background-color: #ffffff;
            line-height: normal;
        }
        .handleText
        {
            width: 16px;
            height: 16px;
            background-image: url(../images/HandleGrip.png);
            overflow: hidden;
            cursor: se-resize;
        }
        .handleText0
        {
            overflow: hidden;
        }
        .resizingText
        {
            padding: 0px;
            border-style: solid;
            border-width: 1px;
            border-color: #7391BA;
        }
        .resizingText0
        {
            padding: 0px;
        }
        
        
        .gridViewHeader
        {
            background-image: url(../images/appbuttons/minibuttons/gradient2.gif);
            background-repeat: repeat-x;
            background-position: top left;
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black; /* _top: expression(eval(document.body.scrollTop));
    background-color:Navy;
    color:White;
    font-size:12px;
    font-weight:bold;

     position:fixed;
    _position: absolute;
            top: 8;
    z-index:10;
    _top:expression(this.offsetParent.scrollTop);

    position:relative;
    */
        }
        .center
        {
            margin-left: auto;
            margin-right: auto;
        }
        .dragdiv {Z-INDEX: 999; BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 500px; HEIGHT: 500px; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid; background-color: White}
    </style>
</head>
<body onload="checkpage();ResizeDelay();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server"/>
    
    
    
 <ajaxToolkit:DragPanelExtender ID="DPE1" runat="server"
 BehaviorID="DPEB1"
    TargetControlID="plst"
    DragHandleID="lsthandle" />

    <ajaxToolkit:ResizableControlExtender ID="resize1" runat="server" BehaviorID="ResizableControlBehavior1"
        TargetControlID="p1" ResizableCssClass="resizingText0" HandleCssClass="handleText"
        MinimumWidth="500" MinimumHeight="200" MaximumWidth="1010" MaximumHeight="700" />
    <ajaxToolkit:ResizableControlExtender ID="resize2" runat="server" BehaviorID="ResizableControlBehavior2"
        TargetControlID="p2" ResizableCssClass="resizingText" HandleCssClass="handleText"
        MinimumWidth="1270" MinimumHeight="200" MaximumWidth="1270" MaximumHeight="900" />
    <ajaxToolkit:ResizableControlExtender ID="resize3" runat="server" BehaviorID="ResizableControlBehavior3"
        TargetControlID="p3" ResizableCssClass="resizingText" HandleCssClass="handleText"
        MinimumWidth="700" MinimumHeight="500" MaximumWidth="1260" MaximumHeight="700" />
    <ajaxToolkit:ResizableControlExtender ID="resize4" runat="server" BehaviorID="ResizableControlBehavior4"
        TargetControlID="p4" ResizableCssClass="resizingText" HandleCssClass="handleText"
        MinimumWidth="700" MinimumHeight="500" MaximumWidth="1260" MaximumHeight="700"
        OnClientResize="checkframe" />
    <ajaxToolkit:ResizableControlExtender ID="resize5" runat="server" BehaviorID="ResizableControlBehavior5"
        TargetControlID="p5" ResizableCssClass="resizingText" HandleCssClass="handleText"
        MinimumWidth="400" MinimumHeight="500" MaximumWidth="1260" MaximumHeight="700" />
        
    <div class="sc">
        <table width="1270" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td>
                    <table cellpadding="1" cellspacing="1">
                        <tr>
                            <td colspan="11">
                                <table width="720" cellpadding="1" cellspacing="1">
                                    <tr>
                                        <td id="tddepts" class="bluelabel" runat="server" width="120" valign="middle">
                                            Use Departments
                                        </td>
                                        <td width="30" valign="middle">
                                            <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                                        </td>
                                        <td id="tdlocs1" class="bluelabel" runat="server" width="100" valign="middle">
                                            Use Locations
                                        </td>
                                        <td width="30" valign="middle">
                                            <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                                        </td>
                                        <td class="bluelabel" width="200" valign="middle">
                                            Start with Equipment Tabs
                                        </td>
                                        <td width="30" valign="middle">
                                            <img onclick="geteqtabs('noloc');" src="../images/appbuttons/minibuttons/magnifier.gif"
                                                alt="" />
                                        </td>
                                        <td width="380" id="tdcoords" valign="middle">
                                            <img alt="" src="../images/appbuttons/minibuttons/siron30.gif" height="22" width="22"
                                                id="imgsiren2" runat="server" class="details" onmouseover="filtlook();" />
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td rowspan="1" valign="top" align="right" width="540">
                                <img alt="" src="../images2/eng/allinone.gif" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="1">
                        <tr id="trdepts" class="details" runat="server">
                            <td class="label" width="80">
                                Department
                            </td>
                            <td id="tddept" class="plainlabel" width="220" runat="server">
                            </td>
                            <td>
                                <img onclick="retminsrch('deptret');" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td class="label" width="80">
                                Station\Cell
                            </td>
                            <td id="tdcell" class="plainlabel" runat="server" width="220">
                            </td>
                            <td>
                                <img onclick="retminsrch('checkcell');" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                        </tr>
                        <tr id="trlocs" class="details" runat="server">
                            <td class="label" width="80">
                                Location
                            </td>
                            <td id="tdloc3" class="plainlabel" width="220" runat="server">
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr id="treqdets" class="view" runat="server">
                            <td class="label" width="80">
                                Equipment
                            </td>
                            <td id="tdeq" class="plainlabel" runat="server" width="220">
                            </td>
                            <td width="20">
                                <img onclick="retminsrch('checkeq');" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    onmouseover="return overlib('Select an Equipment Record (Location Required)', ABOVE, LEFT)"
                                    onmouseout="return nd()" alt="" />
                            </td>
                            <td class="label" width="20">
                                <img id="imgaddeq" class="imgbutton" onmouseover="return overlib('Add a New Equipment Record')"
                                    onmouseout="return nd()" onclick="GetEqDiv2();" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
                                    width="20" height="20" runat="server">
                            </td>
                            <td width="20">
                                <img alt="" src="../images/appbuttons/minibuttons/pencilnobg.gif" onmouseover="return overlib('Edit in Equipment Tabs', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="geteqtabs('eqready');" />
                            </td>
                            <td class="label" width="20">
                                <img id="imgcopyeq" onmouseover="return overlib('Copy an Equipment Record')" onmouseout="return nd()"
                                    onclick="GetEqCopy2();" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                                    width="20" height="20" runat="server">
                            </td>
                            <td class="label" width="20">
                                &nbsp;<img onmouseover="return overlib('Review\Edit PM Approval Process for this Asset', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="getappr();" alt="" src="../images/appbuttons/minibuttons/gauge.gif">
                            </td>
                            <td width="20">
                                <img alt="" src="../images/appbuttons/minibuttons/switch.gif" onclick="resetfilt();"
                                    onmouseover="return overlib('Reset Function and Component Filters', ABOVE, LEFT)"
                                    onmouseout="return nd()" />
                            </td>
                            <td width="80" class="label">
                                Procedures
                            </td>
                            <td width="220">
                                <asp:DropDownList ID="ddproc" runat="server" CssClass="plainlabel" Width="210px"
                                    AutoPostBack="False">
                                </asp:DropDownList>
                            </td>
                            <td width="20">
                                <img class="imgbutton" id="imgproc" onmouseover="return overlib('Add Procedures to Optimize', ABOVE, LEFT)"
                                    onclick="GetProcDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/upload.gif"
                                    width="20" runat="server" />
                            </td>
                            <td colspan="2" width="52">
                                <input type="radio" id="rbsbs" name="rb" onclick="checkrb('sbs');" runat="server" />
                                <img src="../images/appbuttons/minibuttons/sbs.gif" alt="" onmouseover="return overlib('Document View is Left - Right', ABOVE, LEFT)"
                                    onmouseout="return nd()" />
                            </td>
                            <td colspan="2" width="52">
                                <input type="radio" checked id="rbttb" name="rb" onclick="checkrb('ttb');" runat="server" />
                                <img src="../images/appbuttons/minibuttons/sbstb2.gif" alt="" onmouseover="return overlib('Document View is Top - Bottom', ABOVE, LEFT)"
                                    onmouseout="return nd()" />
                            </td>
                            <td colspan="2" width="60">
                                <input type="checkbox" onclick="gotoother();" id="cbdnd" runat="server" />
                                <img src="../images/appbuttons/minibuttons/dnd.gif" alt="" onmouseover="return overlib('Document View is Left - Right, Page Mode is Standard Drag and Drop', ABOVE, LEFT)"
                                    onmouseout="return nd()" />
                            </td>
                            <td width="20">
                                <img alt="" onclick="getdyk();" src="../images/appbuttons/minibuttons/Q.gif" height="18"
                                    width="18" />
                            </td>
                            <td width="30">
                                <img alt="" src="../images/appbuttons/minibuttons/siron30.gif" height="22" width="22"
                                    id="imgsiren1" runat="server" class="details" onmouseover="filtlook();" />
                            </td>
                            <td align="right" width="270">
                                <img onmouseover="return overlib('Export Results to Excel - All Records', ABOVE, LEFT)"
                                    onclick="exporttoexcel('0');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/excelexp.gif"
                                    alt="" />
                                <img onmouseover="return overlib('Export Results to Excel - Just Revised Records', ABOVE, LEFT)"
                                    onclick="exporttoexcel('1');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/excelexp.gif"
                                    alt="" />
                                <img onmouseover="return overlib('Export Results to Excel - Just Original Records', ABOVE, LEFT)"
                                    onclick="exporttoexcel('2');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/excelexp.gif"
                                    alt="" />
                                <img onmouseover="return overlib('View Reports')" onclick="getValueAnalysis();" onmouseout="return nd()"
                                    alt="" src="../images/appbuttons/minibuttons/report.gif">&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr id="treqdets2" class="view" runat="server">
                            <td width="80" class="label">
                                Function
                            </td>
                            <td id="tdfu" runat="server" class="plainlabel" width="220">
                            </td>
                            <td>
                                <img onclick="getfu2();" src="../images/appbuttons/minibuttons/magnifier.gif" onmouseover="return overlib('Select a Function and Component (optional) from this Equipment Record', ABOVE, LEFT)"
                                    onmouseout="return nd()" alt="" />
                            </td>
                            <td>
                                <img onclick="filtit('fu');" src="../images/appbuttons/minibuttons/filter.gif" onmouseover="return overlib('Filter Records by Selected Function', ABOVE, LEFT)"
                                    onmouseout="return nd()" alt="" />
                            </td>
                            <td>
                                <img id="imgaddfu" class="imgbutton" onmouseover="return overlib('Add a New Function Record')"
                                    onmouseout="return nd()" onclick="GetFuncDiv2();" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
                                    width="20" height="20" runat="server">
                            </td>
                            <td>
                                <img id="imgcopyfu" onmouseover="return overlib('Copy a Function Record')" onmouseout="return nd()"
                                    onclick="GetFuncCopy2();" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                                    width="20" height="20" runat="server">
                            </td>
                            <td>
                                <img id="img1" class="imgbutton" onmouseover="return overlib('Add a Task to this Function')"
                                    onmouseout="return nd()" onclick="addfunctask();" alt="" src="../images/appbuttons/minibuttons/addsplb.gif"
                                    runat="server">
                            </td>
                            <td>
                                <img src="../images/appbuttons/minibuttons/magnifier_plusy.gif" onmouseover="return overlib('Add a Task to this Function Using an Existing Profile')"
                                    onmouseout="return nd()" onclick="addfunctaskprof('func','0');" alt="" />
                            </td>
                            <td width="80" class="label">
                                Component
                            </td>
                            <td id="tdco" runat="server" class="plainlabel" width="220">
                            </td>
                            <td>
                                <img onclick="getco2();" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    onmouseover="return overlib('Select a Component from the Selected Function', ABOVE, LEFT)"
                                    onmouseout="return nd()" alt="" />
                            </td>
                            <td>
                                <img onclick="filtit('co');" src="../images/appbuttons/minibuttons/filter.gif" onmouseover="return overlib('Filter Records by Selected Component', ABOVE, LEFT)"
                                    onmouseout="return nd()">
                            </td>
                            <td>
                                <img id="btnaddcomp" onmouseover="return overlib('Add/Edit Components for Selected Function', LEFT)"
                                    onclick="GetCompDiv();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/addmod.gif"
                                    runat="server">
                            </td>
                            <td>
                                <img id="imgcopycomp" onmouseover="return overlib('Copy Components for Selected Function', LEFT)"
                                    onclick="GetCompCopy();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                                    runat="server">
                            </td>
                            <td>
                                <img id="img2" class="imgbutton" onmouseover="return overlib('Add a Task to this Component')"
                                    onmouseout="return nd()" onclick="addcomptask();" alt="" src="../images/appbuttons/minibuttons/addspla.gif"
                                    runat="server">
                            </td>
                            <td>
                                <img src="../images/appbuttons/minibuttons/magnifier_plusg.gif" onmouseover="return overlib('Add a Task to this Component Using an Existing Profile')"
                                    onmouseout="return nd()" onclick="addfunctaskprof('comp','0');" alt="" />
                            </td>
                            <td>
                            </td>
                            <td align="center" colspan="3">
                                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                                    border-right: blue 1px solid" cellspacing="0" cellpadding="0" width="300">
                                    <tr>
                                        <td style="border-right: blue 1px solid" width="20">
                                            <img id="Img3" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                                runat="server">
                                        </td>
                                        <td style="border-right: blue 1px solid" width="20">
                                            <img id="Img4" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                                runat="server">
                                        </td>
                                        <td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
                                            <asp:Label ID="lblpgtop" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                                        </td>
                                        <td style="border-right: blue 1px solid" width="20">
                                            <img id="Img5" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                                runat="server">
                                        </td>
                                        <td width="20">
                                            <img id="Img6" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                                runat="server">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <iframe id="ifmeter" runat="server" width="0" height="0" frameborder="0"></iframe>
                </td>
            </tr>
        </table>
    </div>
    <div id="divstart" runat="server">
        <table style="z-index: 102; position: absolute; top: 120px; left: 4px" width="1260; z-index: 100;">
            <tr>
                <td>
                    <img alt="" src="../images/appbuttons/minibuttons/aio2.gif" />
                </td>
            </tr>
            <tr id="trwait" runat="server">
                <td align="center" class="plainlabelblue" height="22">
                    Waiting for User Data...
                </td>
            </tr>
            <tr id="trnotasks" runat="server" class="details">
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <img alt="" src="../images/appbuttons/minibuttons/siron30.gif" height="30" width="30" onmouseover="filtlook();" />
                            </td>
                            <td class="plainlabelred">
                                No Task Records Found
                            </td>
                            <td>
                                <img alt="" src="../images/appbuttons/minibuttons/siron30.gif" height="30" width="30" onmouseover="filtlook();" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
         <asp:Panel runat="server" ID="plst" Height="500" Width="500" CssClass="details">
        <table cellspacing="0" cellpadding="3" width="500" bgcolor="white">
            <tr bgcolor="blue" >
                <td class="labelwht" height="20" id="lsthandle" runat="server">
                    <asp:Label ID="lang268" runat="server">Task Options</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closelst();" height="18" alt="" src="../images/close.gif" width="18">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                    <tr>
                    <td class="label" height="22">Selected Equipment</td>
                    <td class="plainlabel" id="tdsequip" runat="server"></td>
                    </tr>
                    <tr>
                    <td class="label" height="22">Selected Function</td>
                    <td class="plainlabel" id="tdsfunc" runat="server"></td>
                    </tr>
                    <tr>
                    <td class="label" height="22">Selected Component</td>
                    <td class="plainlabel" id="tdscomp" runat="server"></td>
                    </tr>
                    <tr>
                    <td><img alt="" src="../images/appbuttons/minibuttons/4PX.gif" /></td>
                    </tr>
                    <tr>
                    <td colspan="2" class="plainlabelred" align="center" id="tdsmsg" runat="server"></td>
                    </tr>
                    <tr>
                    <td><img alt="" src="../images/appbuttons/minibuttons/4PX.gif" /></td>
                    </tr>
                    <tr id="trdnd" runat="server">
                    <td colspan="2" class="plainlabel"><a href="#" onclick="gotoothera();">Switch to Drag and Drop</a></td>
                    </tr>
                    <tr id="treqtabs" runat="server">
                    <td colspan="2" class="plainlabel"><a href="#" onclick="geteqtabs('eqready');">Work in Equipment Tabs</a></td>
                    </tr>
                     <tr id="trselfunc" runat="server">
                    <td colspan="2" class="plainlabel"><a href="#" onclick="getfu2();">Select a Function</a></td>
                    </tr>
                    <tr id="trselcomp" runat="server">
                    <td colspan="2" class="plainlabel"><a href="#" onclick="getco('co');">Select a Component</a></td>
                    </tr>
                    <tr id="traddftask" runat="server">
                    <td colspan="2" class="plainlabel"><a href="#" onclick="addfunctask();">Add a Task to the Selected Function</a></td>
                    </tr>
                    <tr id="traddctask" runat="server">
                    <td colspan="2" class="plainlabel"><a href="#" onclick="addcomptask();">Add a Task to the Selected Component</a></td>
                    </tr>
                    <tr id="tr1" runat="server">
                    <td colspan="2" class="plainlabel"><a href="#" onclick="">Close this Window</a></td>
                    </tr>
                    </table>
                </td>
            </tr>
        </table>
        
   </asp:Panel>
    </div>
    <div id="divdata" runat="server" class="container">
        <table style="z-index: 102; position: absolute; top: 120px; left: 4px" width="1250"
            id="tbldata" runat="server" border="0">
            <tr id="trgrid" runat="server">
                <td valign="top">
                    <asp:Panel runat="server" ID="p1" class="frameText" Height="600" Width="1010" onscroll="checkpos();">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" EnableModelValidation="True"
                            Width="1010">
                            <AlternatingRowStyle CssClass="ptransrowblue"></AlternatingRowStyle>
                            <RowStyle CssClass="ptransrow"></RowStyle>
                            <HeaderStyle CssClass="gridViewHeader" />
                            <Columns>
                                <asp:TemplateField HeaderText="Edit">
                                    <HeaderStyle Height="20px" Width="104px"></HeaderStyle>
                                    <ItemStyle Width="104px" Height="40px" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibedit" runat="server" ImageUrl="../images/appbuttons/minibuttons/pencilnobg.gif"
                                            CommandName="Edit" ToolTip="Edit"></asp:ImageButton>
                                        <asp:ImageButton ID="ibaddftask" runat="server" ImageUrl="../images/appbuttons/minibuttons/addsplb.gif"
                                            CommandName="addftask" ToolTip="Add a Task to this Function"></asp:ImageButton>
                                        <img src="../images/appbuttons/minibuttons/magnifier_plusy.gif" onmouseover="return overlib('Add a Task to this Function Using an Existing Profile')"
                                            onmouseout="return nd()" alt="" id="imgaddftask" runat="server" />
                                        <asp:ImageButton ID="ibaddctask" runat="server" ImageUrl="../images/appbuttons/minibuttons/addspla.gif"
                                            CommandName="addctask" ToolTip="Add a Task to This Component"></asp:ImageButton>
                                        <img src="../images/appbuttons/minibuttons/magnifier_plusg.gif" onmouseover="return overlib('Add a Task to this Component Using an Existing Profile')"
                                            onmouseout="return nd()" alt="" id="imgaddctask" runat="server" />
                                        <img src="../images/appbuttons/minibuttons/16PX.gif" alt="" />
                                        <asp:ImageButton ID="ibaddsub" runat="server" ImageUrl="../images/appbuttons/minibuttons/sgrid2.gif"
                                            CommandName="addsub" ToolTip="Add a Sub Task"></asp:ImageButton>
                                        <asp:ImageButton ID="ibcopytask" runat="server" ImageUrl="../images/appbuttons/minibuttons/copybg2.gif"
                                            CommandName="copytask" ToolTip="Copy This Task"></asp:ImageButton>
                                        <img id="ibcopyf" runat="server" src="../images/appbuttons/minibuttons/copybgfunc.gif"
                                            alt="" onmouseover="return overlib('Copy This Task to another Function')" onmouseout="return nd()" />
                                        <img id="ibcopyc" runat="server" src="../images/appbuttons/minibuttons/copybgcomp.gif"
                                            alt="" onmouseover="return overlib('Copy This Task to another Component')" onmouseout="return nd()" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:ImageButton ID="ibsave" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                            CommandName="Update"></asp:ImageButton>
                                        <asp:ImageButton ID="Imagebutton9" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                            CommandName="Cancel"></asp:ImageButton>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Function \ Component">
                                    <HeaderStyle Width="220px"></HeaderStyle>
                                    <ItemStyle Width="220px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblfunci1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>

                                        </asp:Label><br />
                                        <asp:Label ID="lblcompi1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.compnumd") %>'>

                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="txtfunce1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>

                                        </asp:Label><br />
                                        <asp:Label ID="txtcompe1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.compnumd") %>'>

                                        </asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Function">
                                    <HeaderStyle Width="220px"></HeaderStyle>
                                    <ItemStyle Width="220px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblfunci" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>

                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="txtfunce" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>

                                        </asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Component">
                                    <HeaderStyle Width="240px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblcompi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.compnumd") %>'>

                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="txtcompe" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.compnumd") %>'>

                                        </asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif">
                                    <HeaderStyle Width="22px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <img src="../images/appbuttons/minibuttons/magnifierdis.gif" id="imgcoi" runat="server" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <img src="../images/appbuttons/minibuttons/magnifier.gif" id="imgcoe" runat="server" />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Task#">
                                    <HeaderStyle Width="40px"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lbltnumi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>

                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txttnume" runat="server" Width="36px" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>

                                        </asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sub#">
                                    <HeaderStyle Width="38px"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblstnumi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>'>

                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtstnume" runat="server" Width="34px" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>'>

                                        </asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <HeaderTemplate>
                                        <asp:ImageButton ID="ImageButton1" runat="server" CommandName="hd" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                            ToolTip="Hide Original Task Description"></asp:ImageButton><br />
                                        <asp:ImageButton ID="ImageButton2" runat="server" CommandName="sd" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                            ToolTip="Show Original Task Description"></asp:ImageButton>
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Original Task Description">
                                    <HeaderStyle Width="240px"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblotaski" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.otaskdesc") %>'>

                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtotaske" CssClass="plainlabel" Rows="7" runat="server" Width="230px"
                                            TextMode="MultiLine" Text='<%# DataBinder.Eval(Container, "DataItem.otaskdesc") %>'>

                                        </asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif">
                                    <HeaderStyle Width="22px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <img id="imgotdi" runat="server" alt="" src="../images/appbuttons/minibuttons/magnifier.gif" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <img id="imgotde" runat="server" alt="" src="../images/appbuttons/minibuttons/magnifier.gif" />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Revised Task Description">
                                    <HeaderStyle Width="240px"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lbltaski" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>'>

                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txttaske" CssClass="plainlabel" Rows="7" runat="server" Width="230px"
                                            TextMode="MultiLine" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>'>

                                        </asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif">
                                    <HeaderStyle Width="22px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <img id="imgtdi" runat="server" alt="" src="../images/appbuttons/minibuttons/magnifier.gif" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <img id="imgtde" runat="server" alt="" src="../images/appbuttons/minibuttons/magnifier.gif" />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="false">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblpmtskidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>

                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblpmtskide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>

                                        </asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="false">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblfuidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.funcid") %>'>

                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblfuide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.funcid") %>'>

                                        </asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="false">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblcoidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.comid") %>'>

                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblcoide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.comid") %>'>

                                        </asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Task#" Visible="false">
                                    <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lbltnumif" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>

                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="txtotnume" runat="server" Width="50px" MaxLength="100" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>

                                        </asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sub#" Visible="false">
                                    <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblstnumif" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>'>

                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="txtostnume" runat="server" Width="50px" MaxLength="100" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>'>

                                        </asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderImageUrl="../images/appbuttons/minibuttons/del.gif" ItemStyle-HorizontalAlign="Center">
                                    <HeaderStyle Height="20px" Width="22px"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                            CommandName="Delete"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <table class="center">
                            <tr id="tdnavbot" runat="server">
                                <td align="center">
                                    <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                                        border-right: blue 1px solid" cellspacing="0" cellpadding="0" width="300">
                                        <tr>
                                            <td style="border-right: blue 1px solid" width="20">
                                                <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                                    runat="server">
                                            </td>
                                            <td style="border-right: blue 1px solid" width="20">
                                                <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                                    runat="server">
                                            </td>
                                            <td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
                                                <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                                            </td>
                                            <td style="border-right: blue 1px solid" width="20">
                                                <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                                    runat="server">
                                            </td>
                                            <td width="20">
                                                <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                                    runat="server">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td id="tddocside" runat="server" valign="top" align="left">
                    <asp:Panel runat="server" ID="p3" class="frameText" Height="500" onscroll="checkposp3();">
                        <iframe id="ifpmdocs" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                            margin: 0px; padding-top: 0px; background-color: transparent" src="../appsopt/OptHolder.aspx"
                            frameborder="0" width="700" scrolling="no" height="1200" runat="server"></iframe>
                    </asp:Panel>
                </td>
            </tr>
            <tr id="trdocbot" runat="server" valign="top">
                <td>
                    <asp:Panel runat="server" ID="p2" class="frameText" Height="200" onscroll="checkposp2();">
                        <iframe id="ifpmdoc" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                            margin: 0px; padding-top: 0px; background-color: transparent" src="../appsopt/OptHolder.aspx"
                            frameborder="0" width="1244" scrolling="no" height="1200" runat="server"></iframe>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        
    </div>
    <div id="divdrag" runat="server" class="details">
        <table style="z-index: 102; position: absolute; top: 120px; left: 4px" width="1250"
            id="Table1" runat="server" border="0">
            <tr>
                <td valign="top">
                    <asp:Panel runat="server" ID="p4" class="frameText" Height="200" onscroll="checkposp4();">
                        <iframe id="ifdrag" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                            margin: 0px; padding-top: 0px; background-color: transparent" src="../appsopt/OptHolder.aspx"
                            frameborder="0" width="684" scrolling="no" height="480" runat="server"></iframe>
                    </asp:Panel>
                </td>
                <td id="tddocsidedrag" runat="server" valign="top" align="left">
                    <asp:Panel runat="server" ID="p5" class="frameText" Height="500">
                        <iframe id="ifpmdocsd" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                            margin: 0px; padding-top: 0px; background-color: transparent" src="../appsopt/OptHolder.aspx"
                            frameborder="0" width="700" scrolling="no" height="1200" runat="server"></iframe>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
   <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;"></iframe>
     
     <script type="text/javascript">
         document.getElementById("ifsession").src = "../session.aspx?who=mm";
         function ResizeDelay() {
             var start = document.getElementById("lblstart").value;
             var docmode = document.getElementById("lbldocmode").value;
             var pagemode = document.getElementById("lblpagemode").value;
             if (start == "yes") {
                 if (pagemode == "drag") {
                     //alert()
                 }
                 document.getElementById("lblstart").value = "no";
                 window.setTimeout("Resize('" + docmode + "','" + pagemode + "');", 100);
             }

         }
         function filtlook() {
             $get('plst').style.visibility = 'visible'
             $get('plst').className = 'dragdiv'
             //var DragPanel = $find('DPEB1')
             //DragPanel.style.visibility = true
         }
         function closelst() {
             $get('plst').style.visibility = 'hidden'
             $get('plst').className = 'details'
         }
         function Resize(docmode, pagemode) {
             var ResizePanel = $find('ResizableControlBehavior1')
             var ResizePanel2 = $find('ResizableControlBehavior2')
             var ResizePanel3 = $find('ResizableControlBehavior3')
             var ResizePanel4 = $find('ResizableControlBehavior4')
             var ResizePanel5 = $find('ResizableControlBehavior5')
             var docHeight;
             var docWidth;
             if (docmode == "sbs" && pagemode != "drag") {
                 docHeight = 500;
                 docWidth = 768;
             }
             else if (docmode == "ttb" && pagemode != "drag") {
                 docHeight = 300;
                 docWidth = 1270;
             }
             try {
                 ResizePanel.set_Size({ width: docWidth, height: docHeight });
             }
             catch (err) {

             }
             if (pagemode == "drag") {
                 docHeight = 500;
                 docWidth = 550;
                 try {
                     ResizePanel5.set_Size({ width: docWidth, height: docHeight });
                 }
                 catch (err) {

                 }
                 docWidth = 700;
                 try {
                     ResizePanel4.set_Size({ width: docWidth, height: docHeight });
                 }
                 catch (err) {

                 }
             }
         }
         function checkframe() {
             var p4h = document.getElementById("p4").style.height;
             document.getElementById("ifdrag").style.height = parseInt(p4h) - 10;
         }
         function setCookie(cookie_name, value, expiredays) {
             var exdate = new Date();
             exdate.setDate(exdate.getDate() + expiredays);
             document.cookie = cookie_name + "=" + escape(value) + ";expires=" + exdate.toGMTString();
         }
    </script>
      
    <input type="hidden" id="lbleqid" runat="server" />
    <input id="txtpg" type="hidden" runat="server" />
    <input id="txtpgcnt" type="hidden" runat="server" />
    <input id="spdivy" type="hidden" runat="server" />
    <input id="xCoord" type="hidden" runat="server" /><input id="yCoord" type="hidden"
        runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblrettyp" runat="server" />
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lbldept" type="hidden" name="lbldid" runat="server">
    <input id="lblclid" type="hidden" name="lblclid" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lbllid" type="hidden" name="lbllid" runat="server">
    <input id="lblcell" type="hidden" runat="server">
    <input id="lbldid" type="hidden" runat="server">
    <input id="lbleq" type="hidden" runat="server">
    <input id="lblfu" type="hidden" runat="server">
    <input type="hidden" id="lblloc" runat="server" /><input type="hidden" id="lblcomp"
        runat="server" />
    <input type="hidden" id="lbllevel" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblretcompid" runat="server" />
    <input type="hidden" id="lblustr" runat="server" />
    <input type="hidden" id="lblttid" runat="server" />
    <input type="hidden" id="lbltasktype" runat="server" />
    <input type="hidden" id="lblptid" runat="server" />
    <input type="hidden" id="lblpretech" runat="server" />
    <input type="hidden" id="lblskillid" runat="server" />
    <input type="hidden" id="lblskill" runat="server" />
    <input type="hidden" id="lblqty" runat="server" />
    <input type="hidden" id="lblrdid" runat="server" />
    <input type="hidden" id="lblrd" runat="server" />
    <input type="hidden" id="lblfreq" runat="server" />
    <input type="hidden" id="lblmeterid" runat="server" />
    <input type="hidden" id="lblmeterfreq" runat="server" />
    <input type="hidden" id="lblmaxdays" runat="server" />
    <input type="hidden" id="lblfixed" runat="server" />
    <input type="hidden" id="lblrdt" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblretid" runat="server" />
    <input type="hidden" id="lblretid2" runat="server" />
    <input type="hidden" id="lblmeterunit" runat="server" />
    <input type="hidden" id="lblfilt" runat="server" />
    <input type="hidden" id="lblp1xpos" runat="server" />
    <input type="hidden" id="lblp1ypos" runat="server" />
    <input type="hidden" id="lblp2xpos" runat="server" />
    <input type="hidden" id="lblp2ypos" runat="server" />
    <input type="hidden" id="lblp3xpos" runat="server" />
    <input type="hidden" id="lblp3ypos" runat="server" />
    <input type="hidden" id="lblp4xpos" runat="server" />
    <input type="hidden" id="lblp4ypos" runat="server" />
    <input type="hidden" id="lbldocmode" runat="server" />
    <input type="hidden" id="lblstart" runat="server" />
    <input type="hidden" id="lblpagemode" runat="server" />
    <input type="hidden" id="lbldocid" runat="server" />
    <input type="hidden" id="lblproc" runat="server" />
    <input type="hidden" id="lbltstat" runat="server" />
    <input type="hidden" id="lblusetot" runat="server" />
    <input type="hidden" id="lblfcnt" runat="server" />
    </form>
</body>
</html>
