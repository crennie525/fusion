﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123e2profileadd.aspx.vb"
    Inherits="lucy_r12.pmo123e2profileadd" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="javascript" type="text/javascript" src="../scripts/pmtaskdivfuncopt_1016_1.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMOptTasksaspx_1016_2.js"></script>
    <script language="javascript" type="text/javascript">
    <!--
        function GetType(app, sval) {
            //alert(app + ", " + sval)
            var typlst
            var sstr
            var ssti
            if (app == "d") {
                typlst = document.getElementById("ddtype");
                ptlst = document.getElementById("ddpt");
                sstr = typlst.options[typlst.selectedIndex].text;
                ssti = document.getElementById("ddtype").value;
                if (ssti == "7") {
                    document.getElementById("ddpt").disabled = false;
                }
                else {
                    ptlst.value = "0";
                    document.getElementById("ddpt").disabled = true;
                }
            }
            else if (app == "o") {
                document.getElementById("ddtype").value = sval;
                typlst = document.getElementById("ddtypeo");
                ptlst = document.getElementById("ddpt");
                ptolst = document.getElementById("ddpto");
                sstr = typlst.options[typlst.selectedIndex].text;
                ssti = document.getElementById("ddtypeo").value;
                if (ssti == "7") {
                    document.getElementById("ddpt").disabled = false;
                    document.getElementById("ddpto").disabled = false;
                }
                else {
                    ptlst.value = "0";
                    ptolst.value = "0";
                    document.getElementById("ddpt").disabled = true;
                    document.getElementById("ddpto").disabled = true;
                }
            }
        }
        function getmeter() {
            var enable = "0"; // document.getElementById("lblenable").value;
            var typ = "dad"; // document.getElementById("lblwho").value;
            if (typ == "rev") {
                typ = "reg";
            }
            else {
                typ = "orig";
            }
            var eqid = document.getElementById("lbleqid").value;
            var eqnum = document.getElementById("lbleqnum").value;
            var pmtskid = document.getElementById("lbltaskid").value;
            var fuid = document.getElementById("lblfuid").value;
            var skillid = document.getElementById("lblskillid").value;
            var skillqty = document.getElementById("lblskillqty").value;
            var rdid = document.getElementById("lblrdid").value;
            var func = document.getElementById("lblfunc").value;
            var skill = document.getElementById("lblskill").value;
            var rd = document.getElementById("lblrd").value;
            var meterid = document.getElementById("lblmeterid").value;
            var varflg = 0;
            if (enable == "0" && pmtskid != "") {
                if (skillid == "" || skillid == "0") {
                    skillid = document.getElementById("ddskill").value;
                    if (skillid == "" || skillid == "0") {
                        alert("Skill Required")
                        varflg = 1;
                    }
                    else {
                        var sklist = document.getElementById("ddskill");
                        var skstr = sklist.options[sklist.selectedIndex].text;
                        skill = skstr;
                    }
                }
                else if (skillqty == "") {
                    skillqty = document.getElementById("txtqty").value;
                    if (skillqty == "") {
                        alert("Skill Qty Required")
                        varflg = 1;
                    }
                }
                else if (rdid == "" || rdid == "0") {
                    rdid = document.getElementById("ddeqstat").value;
                    if (rdid == "" || rdid == "0") {
                        alert("Running or Down Value Required")
                        varflg = 1;
                    }
                    else {
                        var rdlist = document.getElementById("ddeqstat");
                        var rdstr = sklist.options[rdlist.selectedIndex].text;
                        rd = rdstr;
                    }
                }
                if (varflg == 0) {
                    var eReturn = window.showModalDialog("../equip/usemeterdialog.aspx?typ=" + typ + "&eqid=" + eqid + "&pmtskid=" + pmtskid + "&fuid=" + fuid + "&skillid=" + skillid
                                   + "&skillqty=" + skillqty + "&rdid=" + rdid + "&func=" + func + "&skill=" + skill + "&rd=" + rd
                                   + "&meterid=" + meterid + "&rdt=" + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:900px;resizable=yes");
                    if (eReturn) {
                    /*
                        document.getElementById("lblcompchk").value = "2";
                        document.getElementById("form1").submit();
                        */
                        var ret = eReturn.split("~");
                        //alert(ret)
                        document.getElementById("lblmeterid").value = ret[0];
                        document.getElementById("lblmeterfreq").value = ret[1];
                        document.getElementById("lblfreq").value = ret[2];
                        document.getElementById("lbldayfreq").value = ret[2];
                        document.getElementById("txtfreqo").value = ret[2];
                        document.getElementById("lblmeterunit").value = ret[3];
                        document.getElementById("lblmaxdays").value = ret[4];
                        document.getElementById("lblmeter").value = ret[5];
                    }
                }
            }
        }
        function valpgnums() {
            var tpmhold = document.getElementById("lbltpmhold").value;
            if (tpmhold != "1") {
                if (isNaN(document.getElementById("txtqty").value)) {
                    alert("Skill Qty is Not a Number")
                }
                else if (isNaN(document.getElementById("txtfreq").value)) {
                    alert("Frequency is Not a Number")
                }
                else if (isNaN(document.getElementById("txttr").value)) {
                    alert("Skill Time is Not a Number")
                }
                else if (isNaN(document.getElementById("txtrdt").value)) {
                    alert("Running/Down Time is Not a Number")
                }
                else if (document.getElementById("ddtaskstat").value == 'Delete') {
                    //CheckChanges();
                }
                else {
                    document.getElementById("lblsubmit").value = "addprofile";
                    // FreezeScreen('Your Data Is Being Processed...');
                    document.getElementById("form1").submit();
                }
            }
        }
        function doswitch() {
            var who = document.getElementById("lblwho").value;
            var ttid = document.getElementById("lblpmtskid").value;
            var eqid = document.getElementById("lbleqid").value;
            var sid = document.getElementById("lblsid").value;
            var coid = document.getElementById("lblcoid").value;
            var ustr = document.getElementById("lblustr").value;
            if (who == "rev") {
                who = "orig";
            }
            else {
                who = "rev";
            }
            window.location = "pmo123e2rdets?who=" + who + "&ttid=" + ttid + "&eqid=" + eqid + "&sid=" + sid + "&coid=" + coid + "&ustr=" + ustr;
        }
        function goback() {
            window.parent.handlereturn("ok");
        }
        //-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="300">
            <tr>
                <td colspan="3" class="bluelabel" id="tdwho" runat="server" align="center" height="22">
                </td>
            </tr>
            <tr>
                <td colspan="3" class="plainlabel" align="center" height="22">
                    <a href="#" id="asw" runat="server" onclick="doswitch();"></a>
                </td>
            </tr>
            <tr>
                <td class="label" width="100">
                    <asp:Label ID="lang255" runat="server">Task Type</asp:Label>
                </td>
                <td colspan="3">
                    <asp:DropDownList ID="ddtype" runat="server" CssClass="plainlabel" Width="160px"
                        Rows="1" DataTextField="tasktype" DataValueField="ttid">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang256" runat="server">PdM Tech</asp:Label>
                </td>
                <td colspan="3">
                    <asp:DropDownList ID="ddpt" runat="server" CssClass="plainlabel" Width="160px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang259" runat="server">Skill Required</asp:Label>
                </td>
                <td colspan="3">
                    <asp:DropDownList ID="ddskill" runat="server" Width="160px" CssClass="plainlabel">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Qty
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtqty" runat="server" CssClass="plainlabel" Width="30px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang260" runat="server">Min Ea</asp:Label>
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txttr" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang263" runat="server">Equipment Status</asp:Label>
                </td>
                <td colspan="3">
                    <asp:DropDownList ID="ddeqstat" runat="server" CssClass="plainlabel" Width="90px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang264" runat="server">Down Time</asp:Label>
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtrdt" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang265" runat="server">Frequency</asp:Label>
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtfreq" runat="server" CssClass="plainlabel" Width="60px"></asp:TextBox>
                    <img alt="" onclick="getmeter();" onmouseover="return overlib('View, Add, or Edit Meter Frequency for this Task', ABOVE, LEFT)"
                        onmouseout="return nd()" src="../images/appbuttons/minibuttons/meter1.gif" /><input
                            id="cbfixed" runat="server" type="checkbox" onmouseover="return overlib('Check if Frequency is Fixed (Calendar Frequency Only)')"
                            onmouseout="return nd()" />
                </td>
            </tr>
            <tr class="details">
                <td class="bluelabel">
                    <asp:Label ID="lang1033" runat="server">Task Status</asp:Label>
                </td>
                <td colspan="3">
                    <asp:DropDownList ID="ddtaskstat" runat="server" CssClass="plainlabel" AutoPostBack="False">
                        <asp:ListItem Value="Select">Select</asp:ListItem>
                        <asp:ListItem Value="UnChanged">UnChanged</asp:ListItem>
                        <asp:ListItem Value="Add">Add</asp:ListItem>
                        <asp:ListItem Value="Revised">Revised</asp:ListItem>
                        <asp:ListItem Value="Delete">Delete</asp:ListItem>
                        <asp:ListItem Value="Detailed Steps">Detailed Steps</asp:ListItem>
                    </asp:DropDownList>
                </td>
                
            </tr>
            <tr>
            <td class="plainlabelred" colspan="4" align="center" id="tdmsg" runat="server"></td>
            </tr>
            <tr>
                <td align="right" colspan="4">
                    <img id="btnsav" onclick="valpgnums();" alt="" src="../images/appbuttons/minibuttons/addmod.gif"
                        width="20" height="20" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="right" colspan="4" class="plainlabel">
                   <a href="#" onclick="goback();">Return</a>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblpmtskid" runat="server" />
    <input type="hidden" id="lblcoid" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblmfid" runat="server" />
    <input type="hidden" id="lblmeterid" runat="server" />
    <input type="hidden" id="lblismeter" runat="server" />
    <input type="hidden" id="lblismetero" runat="server" />
    <input type="hidden" id="lblfixed" runat="server" />
    <input type="hidden" id="lblmfido" runat="server" />
    <input type="hidden" id="lblmeterido" runat="server" />
    <input type="hidden" id="lblfixedo" runat="server" />
    <input type="hidden" id="lblrd" runat="server" />
    <input type="hidden" id="lblrdo" runat="server" />
    <input type="hidden" id="lblfunc" runat="server" />
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblskill" runat="server" />
    <input type="hidden" id="lblskillo" runat="server" />
    <input type="hidden" id="lblfuid" runat="server" />
    <input type="hidden" id="lblskillid" runat="server" />
    <input type="hidden" id="lblskillido" runat="server" />
    <input type="hidden" id="lblrdid" runat="server" />
    <input type="hidden" id="lblrdido" runat="server" />
    <input type="hidden" id="lblcomp" runat="server" />
    <input type="hidden" id="lbltpmhold" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblustr" runat="server" />
    <input type="hidden" id="lbldayfreq" runat="server" />
<input type="hidden" id="lblmeterunit" runat="server" />
<input type="hidden" id="lblmaxdays" runat="server" />
<input type="hidden" id="lblmeterfreq" runat="server" />
<input type="hidden" id="lblmeter" runat="server" />
<input id="lblro" type="hidden" runat="server" />
<input id="lbllock" type="hidden" name="lbllock" runat="server">
    </form>
</body>
</html>
