﻿Imports System.Data.SqlClient
Imports Microsoft.Office.Interop.Word
Public Class pmo123e2docout
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim od As New officedown
    Dim doc As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim docid, opt As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'type & "," & file & "," & xfile & "," & web & "," & eqid
        Dim type, file, xfile, web, eqid, ret, cv As String
        docid = Request.QueryString("docid").ToString '"655" '
        opt = "web" 'Request.QueryString("opt").ToString
        doc.Open()
        ret = getfiledets(docid)
        Dim retar() As String = ret.Split(",")
        type = retar(0)
        file = retar(1)
        xfile = retar(2)
        web = retar(3)
        eqid = retar(4)
        'assuming that app will force attachment if something goes wrong and we have an x file
        lblfile.Value = file
        lblret.Value = "go"

        doc.Dispose()

    End Sub
   
    Private Sub getattach(ByVal type As String, ByVal file As String)
        If type = "1" Or type = "2" Then
            Response.Clear()
            Dim cp As New cltproc
            Dim longstring As String
            longstring = cp.PopOLD(file, type)
            Response.Write(longstring)
        Else

            If type = "pdf" Then
                Response.ContentType = "Application/pdf"
                Response.AddHeader("Content-Disposition", "attachment;filename=ClientCopy.pdf")
                Try
                    Dim FilePath As String = MapPath("..\eqimages\" & file)
                    Response.WriteFile(FilePath)
                    Response.End()
                Catch ex As Exception
                    Try
                        Dim FilePath As String = MapPath("../eqimages/" & file)
                        Response.WriteFile(FilePath)
                        Response.End()
                    Catch ex1 As Exception
                        Response.WriteFile(Server.MapPath("\") & "/eqimages/" & file)
                        Response.End()
                    End Try
                End Try
            ElseIf type = "msword" Or type = "ms-word" Then
                Response.ContentType = "Application/msword"
                Response.AddHeader("Content-Disposition", "attachment;filename=ClientCopy.doc")
                Try
                    Dim FilePath As String = MapPath("..\eqimages\" & file)
                    Response.WriteFile(FilePath)
                    Response.End()
                Catch ex As Exception
                    Try
                        Dim FilePath As String = MapPath("../eqimages/" & file)
                        Response.WriteFile(FilePath)
                        Response.End()
                    Catch ex1 As Exception
                        Response.WriteFile(Server.MapPath("\") & "/eqimages/" & file)
                        Response.End()
                    End Try
                End Try

            ElseIf type = "mswordx" Or type = "ms-wordx" Then
                Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                Response.AddHeader("Content-Disposition", "attachment;filename=ClientCopy.docx")
                Try
                    Dim FilePath As String = MapPath("..\eqimages\" & file)
                    Response.WriteFile(FilePath)
                    Response.End()
                Catch ex As Exception
                    Try
                        Dim FilePath As String = MapPath("../eqimages/" & file)
                        Response.WriteFile(FilePath)
                        Response.End()
                    Catch ex1 As Exception
                        Response.WriteFile(Server.MapPath("\") & "/eqimages/" & file)
                        Response.End()
                    End Try
                End Try
            ElseIf type = "msexcel" Or type = "ms-excel" Then
                Response.ContentType = "Application/x-msexcel"
                Response.AddHeader("Content-Disposition", "attachment;filename=ClientCopy.xls")
                Try
                    Dim FilePath As String = MapPath("..\eqimages\" & file)
                    Response.WriteFile(FilePath)
                    Response.End()
                Catch ex As Exception
                    Try
                        Dim FilePath As String = MapPath("../eqimages/" & file)
                        Response.WriteFile(FilePath)
                        Response.End()
                    Catch ex1 As Exception
                        Response.WriteFile(Server.MapPath("\") & "/eqimages/" & file)
                        Response.End()
                    End Try
                End Try
            ElseIf type = "msexcelx" Or type = "ms-excelx" Then
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                Response.AddHeader("Content-Disposition", "attachment;filename=ClientCopy.xlsx")
                Try
                    Dim FilePath As String = MapPath("..\eqimages\" & file)
                    Response.WriteFile(FilePath)
                    Response.End()
                Catch ex As Exception
                    Try
                        Dim FilePath As String = MapPath("../eqimages/" & file)
                        Response.WriteFile(FilePath)
                        Response.End()
                    Catch ex1 As Exception
                        Response.WriteFile(Server.MapPath("\") & "/eqimages/" & file)
                        Response.End()
                    End Try
                End Try
            Else
                Dim strMessage As String = tmod.getmsg("cdstr389", "doc.aspx.vb") & " " & type & ", " & file
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If
    End Sub
    Private Sub getapp(ByVal type As String, ByVal file As String)
        If type = "1" Or type = "2" Then
            Response.Clear()
            Dim cp As New cltproc
            Dim longstring As String
            longstring = cp.PopOLD(file, type)
            Response.Write(longstring)
        Else

            If type = "pdf" Then
                Response.ContentType = "Application/pdf"
                Response.AddHeader("Content-Disposition", "inline;filename=ClientCopy.pdf")
                Try
                    Dim FilePath As String = MapPath("..\eqimages\" & file)
                    Response.WriteFile(FilePath)
                    Response.End()
                Catch ex As Exception
                    Try
                        Dim FilePath As String = MapPath("../eqimages/" & file)
                        Response.WriteFile(FilePath)
                        Response.End()
                    Catch ex1 As Exception
                        Response.WriteFile(Server.MapPath("\") & "/eqimages/" & file)
                        Response.End()
                    End Try
                End Try
            ElseIf type = "msword" Or type = "ms-word" Then
                Response.ContentType = "Application/msword"
                Response.AddHeader("Content-Disposition", "inline;filename=ClientCopy.doc")
                Try
                    Dim FilePath As String = MapPath("..\eqimages\" & file)
                    Response.WriteFile(FilePath)
                    Response.End()
                Catch ex As Exception
                    Try
                        Dim FilePath As String = MapPath("../eqimages/" & file)
                        Response.WriteFile(FilePath)
                        Response.End()
                    Catch ex1 As Exception
                        Response.WriteFile(Server.MapPath("\") & "/eqimages/" & file)
                        Response.End()
                    End Try
                End Try

            ElseIf type = "mswordx" Or type = "ms-wordx" Then
                Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                Response.AddHeader("Content-Disposition", "inline;filename=ClientCopy.docx")
                Try
                    Dim FilePath As String = MapPath("..\eqimages\" & file)
                    Response.WriteFile(FilePath)
                    Response.End()
                Catch ex As Exception
                    Try
                        Dim FilePath As String = MapPath("../eqimages/" & file)
                        Response.WriteFile(FilePath)
                        Response.End()
                    Catch ex1 As Exception
                        Response.WriteFile(Server.MapPath("\") & "/eqimages/" & file)
                        Response.End()
                    End Try
                End Try
            ElseIf type = "msexcel" Or type = "ms-excel" Then
                Response.ContentType = "Application/x-msexcel"
                Response.AddHeader("Content-Disposition", "inline;filename=ClientCopy.xls")
                Try
                    Dim FilePath As String = MapPath("..\eqimages\" & file)
                    Response.WriteFile(FilePath)
                    Response.End()
                Catch ex As Exception
                    Try
                        Dim FilePath As String = MapPath("../eqimages/" & file)
                        Response.WriteFile(FilePath)
                        Response.End()
                    Catch ex1 As Exception
                        Response.WriteFile(Server.MapPath("\") & "/eqimages/" & file)
                        Response.End()
                    End Try
                End Try
            ElseIf type = "msexcelx" Or type = "ms-excelx" Then
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                Response.AddHeader("Content-Disposition", "inline;filename=ClientCopy.xlsx")
                Try
                    Dim FilePath As String = MapPath("..\eqimages\" & file)
                    Response.WriteFile(FilePath)
                    Response.End()
                Catch ex As Exception
                    Try
                        Dim FilePath As String = MapPath("../eqimages/" & file)
                        Response.WriteFile(FilePath)
                        Response.End()
                    Catch ex1 As Exception
                        Response.WriteFile(Server.MapPath("\") & "/eqimages/" & file)
                        Response.End()
                    End Try
                End Try
            Else
                Dim strMessage As String = tmod.getmsg("cdstr389", "doc.aspx.vb") & " " & type & ", " & file
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If
    End Sub
    Private Function getfiledets(ByVal eqppmid As String) As String
        Dim ret As String = ""
        Dim type As String = ""
        Dim file As String = ""
        Dim web As String = ""
        Dim xfile As String = ""
        Dim eqid As String = ""

        sql = "select * from pmoptoldprocedures where eqppmid = '" & eqppmid & "'"
        dr = doc.GetRdrData(sql)
        While dr.Read
            type = dr.Item("doctype").ToString
            file = dr.Item("filename").ToString

            eqid = dr.Item("eqid").ToString
            xfile = dr.Item("xname").ToString
            web = dr.Item("webname").ToString

        End While
        dr.Close()
        ret = type & "," & file & "," & xfile & "," & web & "," & eqid
        Return ret
    End Function

    Private Sub errcatch()
        Dim sb As New System.Text.StringBuilder
        sb.Append("<html><head>")
        sb.Append("<LINK href=""../styles/pmcssa1.css"" type=""text/css"" rel=""stylesheet"">")
        sb.Append("</head><body>")
        sb.Append("<table width=""100%"" height=""100%"" border=""0""><tr><td width=""100%"" height=""100%"" class=""bluelabel"" align=""center"" valign=""center"">" & tmod.getlbl("cdlbl114", "doc.aspx.vb") & "<br><br>Please Verify That File Was Uploaded At This Location</td>")
        sb.Append("</tr></table>")
        sb.Append("</body></html>")
        Response.Clear()
        Response.ContentType = "text/html"
        Response.AddHeader("Contenet-Disposition", "inline;filename=ClientCopy.html")
        Response.Write(sb.ToString)
        'End Try
    End Sub

End Class