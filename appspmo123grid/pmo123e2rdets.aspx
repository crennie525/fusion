﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123e2rdets.aspx.vb" Inherits="lucy_r12.pmo123e2rdets" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
	
	<script language="javascript" type="text/javascript" src="../scripts/pmtaskdivfuncopt_1016_1.js"></script>
	<script language="JavaScript" type="text/javascript" src="../scripts1/PMOptTasksaspx_1016_2.js"></script>
    <script language="javascript" type="text/javascript">
    <!--
        function GetType(app, sval) {
            //alert(app + ", " + sval)
            var typlst
            var sstr
            var ssti
            if (app == "d") {
                typlst = document.getElementById("ddtype");
                ptlst = document.getElementById("ddpt");
                sstr = typlst.options[typlst.selectedIndex].text;
                ssti = document.getElementById("ddtype").value;
                if (ssti == "7") {
                    document.getElementById("ddpt").disabled = false;
                }
                else {
                    ptlst.value = "0";
                    document.getElementById("ddpt").disabled = true;
                }
            }
            else if (app == "o") {
                document.getElementById("ddtype").value = sval;
                typlst = document.getElementById("ddtypeo");
                ptlst = document.getElementById("ddpt");
                ptolst = document.getElementById("ddpto");
                sstr = typlst.options[typlst.selectedIndex].text;
                ssti = document.getElementById("ddtypeo").value;
                if (ssti == "7") {
                    document.getElementById("ddpt").disabled = false;
                    document.getElementById("ddpto").disabled = false;
                }
                else {
                    ptlst.value = "0";
                    ptolst.value = "0";
                    document.getElementById("ddpt").disabled = true;
                    document.getElementById("ddpto").disabled = true;
                }
            }
        }
        function GetFailDiv() {
            var coid = document.getElementById("lblcoid").value;
            if (coid != "" && coid != "0") {
                var cid = "0";
                var eqid = document.getElementById("lbleqid").value;
                var fuid = document.getElementById("lblfuid").value;
                var ptid = document.getElementById("lblpmtskid").value;
                var ro = "0";
                cvalu = document.getElementById("lblcomp").value; 
                var eReturn = window.showModalDialog("../equip/CompFailDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&cvalu=" + cvalu + "&ro=" + ro, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                if (eReturn) {
                    document.getElementById("lblsubmit").value = "upcompfail";
                    document.getElementById("form1").submit()
                }
             }
             else {
                 alert("No Component Selected")
             }
        }
        function getmeter() {
            var enable = "0"; // document.getElementById("lblenable").value;
            var typ = document.getElementById("lblwho").value;
            if (typ == "rev") {
                typ = "reg";
            }
            else {
                typ = "orig";
            }
            var eqid = document.getElementById("lbleqid").value;
            var eqnum = document.getElementById("lbleqnum").value;
            var pmtskid = document.getElementById("lbltaskid").value;
            var fuid = document.getElementById("lblfuid").value;
            var skillid = document.getElementById("lblskillid").value;
            var skillqty = document.getElementById("lblskillqty").value;
            var rdid = document.getElementById("lblrdid").value;
            var func = document.getElementById("lblfunc").value;
            var skill = document.getElementById("lblskill").value;
            var rd = document.getElementById("lblrd").value;
            var meterid = document.getElementById("lblmeterid").value;
            var varflg = 0;
            if (enable == "0" && pmtskid != "") {
                if (skillid == "" || skillid == "0") {
                    skillid = document.getElementById("ddskill").value;
                    if (skillid == "" || skillid == "0") {
                        alert("Skill Required")
                        varflg = 1;
                    }
                    else {
                        var sklist = document.getElementById("ddskill");
                        var skstr = sklist.options[sklist.selectedIndex].text;
                        skill = skstr;
                    }
                }
                else if (skillqty == "") {
                    skillqty = document.getElementById("txtqty").value;
                    if (skillqty == "") {
                        alert("Skill Qty Required")
                        varflg = 1;
                    }
                }
                else if (rdid == "" || rdid == "0") {
                    rdid = document.getElementById("ddeqstat").value;
                    if (rdid == "" || rdid == "0") {
                        alert("Running or Down Value Required")
                        varflg = 1;
                    }
                    else {
                        var rdlist = document.getElementById("ddeqstat");
                        var rdstr = sklist.options[rdlist.selectedIndex].text;
                        rd = rdstr;
                    }
                }
                if (varflg == 0) {
                    var eReturn = window.showModalDialog("../equip/usemeterdialog.aspx?typ=" + typ + "&eqid=" + eqid + "&pmtskid=" + pmtskid + "&fuid=" + fuid + "&skillid=" + skillid
                                   + "&skillqty=" + skillqty + "&rdid=" + rdid + "&func=" + func + "&skill=" + skill + "&rd=" + rd
                                   + "&meterid=" + meterid + "&rdt=" + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:900px;resizable=yes");
                    if (eReturn) {
                        document.getElementById("lblsubmit").value = "upmeter";
                        document.getElementById("form1").submit()
                    }
                }
            }
        }
        function valpgnums() {
        var tpmhold = document.getElementById("lbltpmhold").value;
        if (tpmhold != "1") {
            if (isNaN(document.getElementById("txtqty").value)) {
                alert("Skill Qty is Not a Number")
            }
            else if (isNaN(document.getElementById("txtfreq").value)) {
                alert("Frequency is Not a Number")
            }
            else if (isNaN(document.getElementById("txttr").value)) {
                alert("Skill Time is Not a Number")
            }
            else if (isNaN(document.getElementById("txtrdt").value)) {
                alert("Running/Down Time is Not a Number")
            }
            else if (isNaN(document.getElementById("txttr").value)) {
                alert("Minutes Each is Not a Number")
            }
            else if (document.getElementById("ddtaskstat").value == 'Delete') {
                //CheckChanges();
            }
            else {
                document.getElementById("lblsubmit").value = "sav";
               // FreezeScreen('Your Data Is Being Processed...');
                document.getElementById("form1").submit();
            }
        }
    }
    function doswitch() {
        var who = document.getElementById("lblwho").value;
        var ttid = document.getElementById("lblpmtskid").value;
        var eqid = document.getElementById("lbleqid").value;
        var sid = document.getElementById("lblsid").value;
        var coid = document.getElementById("lblcoid").value;
        var ustr = document.getElementById("lblustr").value;
        var usetot = document.getElementById("lblusetot").value;
        if (who == "rev") {
            who = "orig";
        }
        else if (who == "revdnd") {
            who = "origdnd";
        }
        else if (who == "origdnd") {
            who = "revdnd";
        }
        else {
            who = "rev";
        }
        window.location = "pmo123e2rdets.aspx?who=" + who + "&ttid=" + ttid + "&eqid=" + eqid + "&sid=" + sid + "&coid=" + coid + "&ustr=" + ustr + "&usetot=" + usetot;
    }
    function GetRationale() {

        handleapp();
        tnum = document.getElementById("lblpg").innerHTML;
        tid = document.getElementById("lbltaskid").value;
        did = document.getElementById("lbldid").value;
        clid = document.getElementById("lblclid").value;
        eqid = document.getElementById("lbleqid").value;
        fuid = document.getElementById("lblfuid").value;
        coid = document.getElementById("lblco").value;
        sav = document.getElementById("lblsave").value;
        ro = document.getElementById("lblro").value;
        var tpmhold = document.getElementById("lbltpmhold").value;
        if (tid == "") {
            alert("No Task Records Seleted!")
        }
        else if (sav == "no") {
            alert("Task Record Is Not Saved!")
        }
        else {
            if (tpmhold != "1") {
                window.parent.setref();
                var eReturn = window.showModalDialog("../appsopt/PMRationaleDialog.aspx?tnum=" + tnum + "&tid=" + tid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:650px; dialogWidth:860px; resizable=yes");
                if (eReturn) {
                    document.getElementById("form1").submit();
                }
            }
        }
    }
    //-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="422">
    <tr>
    <td colspan="3" class="bluelabel" id="tdwho" runat="server" align="center" height="22"></td>
    </tr>
    <tr>
    <td colspan="3" class="plainlabel" align="center" height="22"><a href="#" id="asw" runat="server" onclick="doswitch();"></a></td>
    </tr>
    <tr id="trfailorig" runat="server" class="view">
    <td class="label" align="center"><asp:Label id="Label5" runat="server">Component Failure Modes</asp:Label></td>
					<td></td>
                    <td class="label" align="center"><asp:label id="Label6" runat="server">Task Failure Modes</asp:label></td>
    </tr>
    <tr id="trfailorig2" runat="server" class="view">
    <td align="center" width="200"><asp:listbox id="lbCompFM" runat="server" Height="60px" SelectionMode="Multiple" Width="170px"></asp:listbox></td>
    
    <td><IMG id="todis1" class="details" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
											width="20" height="20" runat="server"> <IMG id="fromdis1" class="details" src="../images/appbuttons/minibuttons/backgraybg.gif"
											width="20" height="20" runat="server">
										<asp:imagebutton id="ibfromo" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardbg.gif"></asp:imagebutton><br>
										<asp:imagebutton id="ibtoo" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbgdrk.gif"></asp:imagebutton></td>
    <td align="center" width="200"><asp:listbox id="lbofailmodes" runat="server" Height="60px" SelectionMode="Multiple" Width="170px"></asp:listbox></td>
    </tr>
				<tr id="trfailrev" runat="server">
					<td class="label" align="center"><asp:Label id="lang1159" runat="server">Component Failure Modes</asp:Label></td>
					<td></td>
                    <td class="redlabel" align="center"><asp:label id="lang1021" runat="server">Not Addressed</asp:label></td>
									
                                    <td></td>
					<td class="label" align="center"><asp:Label id="lang1160" runat="server">Task Failure Modes</asp:Label></td>
				</tr>
				<tr id="trfailrev2" runat="server">
					<td align="center" width="200"><asp:listbox id="lbfailmaster" runat="server" Height="60px" SelectionMode="Multiple" Width="170px"></asp:listbox></td>
					<td><IMG id="btnaddnewfail" onmouseover="return overlib('Add a New Failure Mode to the Component Selected Above')"
											onclick="GetFailDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
											width="20" runat="server"><br>
										<asp:imagebutton id="ibReuse" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><IMG class="details" id="fromreusedis" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
											width="20" runat="server"></td>
									<td align="center"><asp:listbox id="lbfaillist" runat="server" CssClass="plainlabel" Width="150px" Height="60px"
											SelectionMode="Multiple" ForeColor="Red"></asp:listbox></td>
									<td><IMG class="details" id="todis" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
											width="20" runat="server"> <IMG class="details" id="fromdis" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
											width="20" runat="server">
										<asp:imagebutton id="ibToTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><br>
										<asp:imagebutton id="ibFromTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton></td>
					<td align="center" width="200"><asp:listbox id="lbfailmodes" runat="server" Height="60px" SelectionMode="Multiple" Width="170px"></asp:listbox></td>
				</tr>
                <tr>
                <td class="label" width="100"><asp:Label id="lang255" runat="server">Task Type</asp:Label></td>
						<td colSpan="3"><asp:DropDownList id="ddtype" runat="server" CssClass="plainlabel" Width="160px" Rows="1" DataTextField="tasktype"
								DataValueField="ttid"></asp:DropDownList></td>
                </tr>
				<tr>
                <td class="label"><asp:Label id="lang256" runat="server">PdM Tech</asp:Label></td>
						<td colSpan="3"><asp:dropdownlist id="ddpt" runat="server" CssClass="plainlabel" Width="160px"></asp:dropdownlist></td>
                </tr>
                <tr id="trsknorm" runat="server">
                <td class="label"><asp:Label id="lang259" runat="server">Skill Required</asp:Label></td>
									<td colSpan="3"><asp:dropdownlist id="ddskill" runat="server" Width="160px" cssclass="plainlabel"></asp:dropdownlist>
											</td>
                </tr>
                <tr id="trskdnd" runat="server" class="details">
                <td class="label"><asp:Label id="Label1" runat="server">Skill Required</asp:Label></td>
                <td colspan="3" class="plainlabel" id="tdskill" runat="server"></td>
                </tr>
                <tr id="trqtynorm" runat="server">
                <td class="label" >Qty</td>
									<td colSpan="3"><asp:textbox id="txtqty" runat="server" CssClass="plainlabel" Width="30px"></asp:textbox></td>
                </tr>
                <tr id="trqtydnd" runat="server" class="details">
                <td class="label" >Qty</td>
                <td colspan="3" class="plainlabel" id="tdqty" runat="server"></td>
                </tr>
                <tr id="trtrnorm" runat="server">
                <td class="label"><asp:Label id="lang260" runat="server">Min Ea</asp:Label></td>
									<td colSpan="3"><asp:textbox id="txttr" runat="server" CssClass="plainlabel" Width="35px"></asp:textbox></td>
                </tr>
                <tr id="trtrdnd" runat="server" class="details">
                <td class="label" >Min Ea</td>
                <td colspan="3" class="plainlabel" id="tdtr" runat="server"></td>
                </tr>
                <tr id="treqsnorm" runat="server">
                <td class="label"><asp:Label id="lang263" runat="server">Equipment Status</asp:Label></td>
									<td colSpan="3"><asp:dropdownlist id="ddeqstat" runat="server" CssClass="plainlabel" Width="90px"></asp:dropdownlist></td>
                </tr>
                <tr id="treqsdnd" runat="server" class="details">
                <td class="label"><asp:Label id="Label2" runat="server">Equipment Status</asp:Label></td>
									<td colspan="3" class="plainlabel" id="tdeqs" runat="server"></td>
                </tr>
                <tr id="trdtnorm" runat="server">
                <td class="label"><asp:Label id="lang264" runat="server">Down Time</asp:Label></td>
									<td colSpan="3"><asp:textbox id="txtrdt" runat="server" CssClass="plainlabel" Width="35px"></asp:textbox></td>
                </tr>
                <tr id="trdtdnd" runat="server" class="details">
                <td class="label"><asp:Label id="Label3" runat="server">Down Time</asp:Label></td>
									<td colspan="3" class="plainlabel" id="tddt" runat="server"></td>
                </tr>
                <tr id="trfrnorm" runat="server">
                <td class="label"><asp:Label id="lang265" runat="server">Frequency</asp:Label></td>
									<td colSpan="3"><asp:textbox id="txtfreq" runat="server" CssClass="plainlabel" Width="60px"></asp:textbox>
                                    <img alt="" onclick="getmeter();" onmouseover="return overlib('View, Add, or Edit Meter Frequency for this Task', ABOVE, LEFT)"
                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/meter1.gif" /><input id="cbfixed" runat="server" type="checkbox" onmouseover="return overlib('Check if Frequency is Fixed (Calendar Frequency Only)')"
											onmouseout="return nd()" />
                                    </td>
                </tr>
                <tr id="trfrdnd" runat="server" class="details">
                <td class="label"><asp:Label id="Label4" runat="server">Frequency</asp:Label></td>
									<td colspan="3" class="plainlabel" id="tdfreq" runat="server"></td>
                </tr>
                 <tr>
									<td class="bluelabel"><asp:label id="lang1033" runat="server">Task Status</asp:label></td>
									<td colSpan="3"><asp:dropdownlist id="ddtaskstat" runat="server" CssClass="plainlabel" AutoPostBack="False">
											<asp:ListItem Value="Select">Select</asp:ListItem>
											<asp:ListItem Value="UnChanged">UnChanged</asp:ListItem>
											<asp:ListItem Value="Add">Add</asp:ListItem>
											<asp:ListItem Value="Revised">Revised</asp:ListItem>
											<asp:ListItem Value="Delete">Delete</asp:ListItem>
											<asp:ListItem Value="Detailed Steps">Detailed Steps</asp:ListItem>
										</asp:dropdownlist></td>
									<td id="tdtpmhold" class="plainlabelred" colSpan="6" runat="server"></td>
								</tr>
                
                <tr>
                <td></td>
                <td class="label" colspan="3"><asp:checkbox id="cbloto" runat="server"></asp:checkbox>LOTO&nbsp;&nbsp;<asp:checkbox id="cbcs" runat="server"></asp:checkbox>CS
									</td>
                </tr>
                <tr>
                <td align="right" colSpan="5"><IMG onmouseover="return overlib('Choose Equipment Spare Parts for this Task')" onclick="GetSpareDiv();"
								onmouseout="return nd()" src="../images/appbuttons/minibuttons/spareparts.gif"><IMG id="img1" onmouseover="return overlib('Add/Edit Parts for this Task')" onclick="GetPartDiv();"
								onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/parttrans.gif" width="23" runat="server">
							<IMG onmouseover="return overlib('Add/Edit Tools for this Task')" onclick="GetToolDiv();"
								onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/tooltrans.gif"
								width="23"> <IMG onmouseover="return overlib('Add/Edit Lubricants for this Task')" onclick="GetLubeDiv();"
								onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/lubetrans.gif"
								width="23">&nbsp;<IMG onmouseover="return overlib('Add/Edit Notes for this Task')" onclick="GetNoteDiv();"
								onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/notessm.gif" width="25"><IMG id="Img5" onmouseover="return overlib('Edit Pass/Fail Adjustment Levels - This Task', ABOVE, LEFT)"
								onclick="editadj('0');" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/screwd.gif" border="0" runat="server">
							</td>
                </tr>
                <tr>
                <td align="right" colspan="5">
                <IMG id="imgrat" onmouseover="return overlib('Add/Edit Rationale for Task Changes')"
								onmouseout="return nd()" onclick="GetRationale();" alt="" src="../images/appbuttons/minibuttons/rationale.gif" width="20" height="20"
								runat="server">
                <IMG id="btnsav" onclick="valpgnums();" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif"
								width="20" height="20" runat="server"></td>
                </tr>
			</table>
    </div>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblpmtskid" runat="server" />
    <input type="hidden" id="lblcoid" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblmfid" runat="server" />
    <input type="hidden" id="lblmeterid" runat="server" />
    <input type="hidden" id="lblismeter" runat="server" />
    <input type="hidden" id="lblismetero" runat="server" />
    <input type="hidden" id="lblfixed" runat="server" />
    <input type="hidden" id="lblmfido" runat="server" />
    <input type="hidden" id="lblmeterido" runat="server" />
    <input type="hidden" id="lblfixedo" runat="server" />
    <input type="hidden" id="lblrd" runat="server" />
    <input type="hidden" id="lblrdo" runat="server" />
    <input type="hidden" id="lblfunc" runat="server" />
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblskill" runat="server" />
    <input type="hidden" id="lblskillo" runat="server" />
    <input type="hidden" id="lblfuid" runat="server" />
    <input type="hidden" id="lblskillid" runat="server" />
    <input type="hidden" id="lblskillido" runat="server" />
    <input type="hidden" id="lblrdid" runat="server" />
    <input type="hidden" id="lblrdido" runat="server" />
    <input type="hidden" id="lblcomp" runat="server" />
    <input type="hidden" id="lbltpmhold" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblustr" runat="server" />
    <input type="hidden" id="lblusetot" runat="server" />
    <input type="hidden" id="lblfreq" runat="server" />
    <input type="hidden" id="lbltr" runat="server" />
        <input type="hidden" id="lblghostoff" runat="server" />
    <input type="hidden" id="lblxstatus" runat="server" />
    <input id="lblro" type="hidden" runat="server" />
    <input id="lbllock" type="hidden" runat="server" />
    <input id="lblcid" type="hidden" runat="server" />
    <input id="lbllockedby" type="hidden" runat="server" />
    <input id="lbltaskid" type="hidden" runat="server" />
    <input id="lblpg" type="hidden" runat="server" />
    <input id="lblenable" type="hidden" runat="server" />
    <input id="lbldid" type="hidden" runat="server" />
    <input id="lblclid" type="hidden" runat="server" />
    <input id="lblco" type="hidden" runat="server" />
    <input id="lblsave" type="hidden" runat="server" />
    </form>
</body>
</html>
