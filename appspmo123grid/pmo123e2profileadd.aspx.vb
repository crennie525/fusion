﻿Imports System.Data.SqlClient
Public Class pmo123e2profileadd
    Inherits System.Web.UI.Page
    Dim tasks As New Utilities
    Dim tasksadd As New Utilities
    Dim tmod As New transmod
    Public dr As SqlDataReader
    Dim ds As DataSet
    Dim sql As String
    Dim sid, ttid, eqid, who, coid, ustr As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
        Else
            If Request.Form("lblsubmit") = "addprofile" Then
                lblsubmit.Value = ""
                eqid = lbleqid.Value
                tasks.Open()
                addprofile(eqid)
                tasks.Dispose()
            End If
        End If
    End Sub
    Private Sub cleanall()

    End Sub
    Private Sub addprofile(ByVal eqid As String)
        Dim typ, typstr, frestr, qty, rdt, tr, fixed As String
        Dim usemeter As String = lblismeter.Value
        Dim usemetero As String = lblismetero.Value

        If ddtype.SelectedIndex <> 0 Then
            typ = ddtype.SelectedValue
            typstr = ddtype.SelectedItem.ToString
            typstr = Replace(typstr, "'", Chr(180), , , vbTextCompare)
        Else
            typ = "0"
        End If
        frestr = txtfreq.Text
        qty = txtqty.Text
        If Len(qty) = 0 Then
            qty = "1"
        End If
        tr = txttr.Text
        If Len(tr) = 0 Then
            tr = "0"
        End If
        Dim eqs, eqsstr As String
        If ddeqstat.SelectedIndex <> 0 Then
            eqs = ddeqstat.SelectedValue
            eqsstr = ddeqstat.SelectedItem.ToString
            eqsstr = Replace(eqsstr, "'", Chr(180), , , vbTextCompare)
        Else
            eqs = "0"
        End If
        If eqs = "0" Then
            If usemeter = "1" Then
                Dim strMessage1 As String = "Running or Down Status Required When Using Meter Frequency"
                Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                Exit Sub
            End If
        End If
        rdt = txtrdt.Text
        If rdt = "" Then
            rdt = "0"
        End If
        Dim pt, ptstr As String
        If ddpt.SelectedIndex <> 0 Then
            pt = ddpt.SelectedValue
            ptstr = ddpt.SelectedItem.ToString
            ptstr = Replace(ptstr, "'", Chr(180), , , vbTextCompare)
        Else
            pt = "0"
        End If
        Dim ski, skistr As String
        If ddskill.SelectedIndex <> 0 Then
            ski = ddskill.SelectedValue
            skistr = ddskill.SelectedItem.ToString
            skistr = Replace(skistr, "'", Chr(180), , , vbTextCompare)
        Else
            ski = "0"
        End If
        If ski = "0" Then
            If usemeter = "1" Then
                Dim strMessage1 As String = "Skill Required When Using Meter Frequency"
                Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                Exit Sub
            End If
        End If
        fixed = lblfixed.Value
        If cbfixed.Checked = True Then
            fixed = "1"
        End If
        Dim meterid As String = lblmeterid.Value
        Dim meter As String = lblmeter.Value
        Dim meterunit As String = lblmeterunit.Value
        Dim meterfreq As String = lblmeterfreq.Value
        Dim maxdays As String = lblmaxdays.Value

        Dim pcnt As Integer = 0
        sql = "select count(*) from gridprofiles where eqid = '" & eqid & "' and ttid = '" & typ & "' and ptid = '" & pt & "' " _
            + "and skillid = '" & ski & "' and qty = '" & tr & "' and freq = '" & frestr & "' and rdid = '" & eqs & "' and rdt = '" & rdt & "' " _
            + "and ismeter = '" & usemeter & "' and meterid = '" & meterid & "' and meter = '" & meter & "' and meterunit = '" & meterunit & "' " _
            + "and meterfreq = '" & meterfreq & "' and maxdays = '" & maxdays & "' and fixed = '" & fixed & "'"
        pcnt = tasks.Scalar(sql)
        If pcnt = 0 Then
            sql = "insert into gridprofiles " _
                + "(eqid, ttid, tasktype, ptid, pretech, skillid, skill, skillqty, freqid, freq, rdid, rd, " _
                + "tottime, downtime, ismeter, meterid, meter, meterunit, meterfreq, maxdays,  fixed) " _
                + "values ('" & eqid & "','" & typ & "','" & typstr & "','" & pt & "','" & ptstr & "','" & ski & "', " _
                + "'" & skistr & "','" & tr & "', 0,'" & frestr & "','" & eqs & "','" & eqsstr & "',  " _
                + "0,'" & rdt & "','" & usemeter & "','" & meterid & "','" & meter & "','" & meterunit & "','" & meterfreq & "', " _
                + "'" & maxdays & "','" & fixed & "') "
            tasks.Update(sql)
            tdmsg.InnerHtml = "Task Profile Added<br><br>Modify Selections Above to Add Another or Click Return to Exit"
        Else
            Dim strMessage1 As String = "This Task Profile Already Exists for this Equipment Record"
            Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
            tdmsg.InnerHtml = ""
        End If
    End Sub
End Class