﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class pmo123e2odets

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''lang255 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang255 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddtype control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddtype As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lang256 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang256 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddpt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddpt As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lang259 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang259 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddskill control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddskill As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtqty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtqty As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lang260 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang260 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txttr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txttr As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lang263 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang263 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddeqstat control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddeqstat As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lang264 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang264 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtrdt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtrdt As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''img1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents img1 As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''btnsav control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnsav As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''lblsid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblpmtskid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblpmtskid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblcoid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblcoid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbleqid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbleqid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblwho control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblwho As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblmfid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblmfid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblmeterid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblmeterid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblismeter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblismeter As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblismetero control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblismetero As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblfixed control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblfixed As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblmfido control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblmfido As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblmeterido control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblmeterido As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblfixedo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblfixedo As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblrd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblrd As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblrdo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblrdo As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblfunc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblfunc As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbleqnum control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbleqnum As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblskill control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblskill As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblskillo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblskillo As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblfuid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblfuid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblskillid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblskillid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblskillido control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblskillido As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblrdid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblrdid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblrdido control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblrdido As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblcomp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblcomp As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbltpmhold control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbltpmhold As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsubmit As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblustr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblustr As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblskillqty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblskillqty As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblro As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbllock control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbllock As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblcid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblcid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbllockedby control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbllockedby As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbltaskid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbltaskid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblpg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblpg As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblenable control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblenable As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbldid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbldid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblclid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblclid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblco control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblco As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsave As Global.System.Web.UI.HtmlControls.HtmlInputHidden
End Class
