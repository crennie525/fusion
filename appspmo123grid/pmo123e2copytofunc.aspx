﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123e2copytofunc.aspx.vb" Inherits="lucy_r12.pmo123e2copytofunc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function getdfu(fi, fu) {
            document.getElementById("lblnewfuid").value = fi;
            document.getElementById("lblnewfunc").value = fu;
            document.getElementById("trcomp").className = "view";
            document.getElementById("trfuncsel").className = "view";
            document.getElementById("tdfuncsel").innerHTML = fu;
            document.getElementById("lblsubmit").value = "checkcnt";
            document.getElementById("form1").submit();
        }
        function getdco(ci, co, cd) {
            document.getElementById("lblnewcoid").value = ci;
            document.getElementById("lblnewcomp").value = co;
            document.getElementById("lblnewcompdesc").value = cd;
            document.getElementById("trcompsel").className = "view";
            document.getElementById("tdcompsel").innerHTML = co + " - " + cd;
            document.getElementById("trsubmit").className = "view";
        }
        function checkrb(who) {
            document.getElementById("lblcopymode").value = who;
            document.getElementById("lblsubmit").value = who;
            document.getElementById("form1").submit();
        }
        function docopy() {
            document.getElementById("lblsubmit").value = "checkcopy";
            document.getElementById("form1").submit();
        }
        function checkret() {
            ret = document.getElementById("lblret").value;
            if (ret == "ok") {
                var fuid = document.getElementById("lblnewfuid").value;
                var coid = document.getElementById("lblnewcoid").value;
                if (fuid == "") {
                    fuid = document.getElementById("lblfuid").value;
                }
                var newret = fuid + "~" + coid;
                window.parent.handlereturn(newret);
            }

        }
    </script>
</head>
<body onload="checkret();">
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td>
    <table>
    <tr>
    <td class="label" height="22">Current Function</td>
    <td class="plainlabel" id="tdcurrfunc" runat="server"></td>
    </tr>
    <tr>
    <td class="label" height="22">Current Component</td>
    <td class="plainlabel" id="tdcurrcomp" runat="server"></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr id="trfunc" runat="server" class="details">
    <td class="bluelabel" height="22">Select Function</td>
    </tr>
    <tr id="trfuncdiv" runat="server" class="details">
    <td><div id="divfu" style="border: 1px solid black; WIDTH: 300px; HEIGHT: 118px; OVERFLOW: auto; bottom: 407px;"
				runat="server"></div></td>
    </tr>
    <tr id="trcompq" runat="server" class="details">
    <td class="plainlabel">
    <br /><input type="radio" id="rbgetnewcomp" name="rbcc" runat="server" onclick="checkrb('getnewcomp');" />Select a Component to Copy Task To?
    <br /><input type="radio" id="rbcopycompno" name="rbcc" runat="server" onclick="checkrb('justcopy');" />Just Copy Task (Failure Modes will be Dropped)</td>
    </tr>
    <tr id="trcompdnd" runat="server" class="details">
    <td class="plainlabelred">
    Note: If you choose not to select a Component your task will not appear in the Drag and Drop screen.
    </td>
    </tr>
    <tr id="trcomp" runat="server" class="details">
    <td class="bluelabel" height="22">Select Component</td>
    </tr>
    <tr id="trcompdiv" runat="server" class="details">
    <td><div id="divco" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 300px; HEIGHT: 118px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid;"
				runat="server"></div></td>
    </tr>
    <tr>
    <td>
    <table>
    <tr id="trfuncsel" runat="server" class="details">
    <td class="redlabel" height="22">Selected Function</td>
    <td class="plainlabel" id="tdfuncsel" runat="server"></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td>
    <table>
    <tr id="trcompsel" runat="server" class="details">
    <td class="redlabel" height="22">Selected Component</td>
    <td class="plainlabel" id="tdcompsel" runat="server"></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr id="trsubmit" runat="server" align="right" class="details">
    <td class="plainlabel"><a href="#" onclick="docopy();" id="asubmit" runat="server">Submit</a></td>
    </tr>
    </table>
    </div>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lblfuid" runat="server" />
    <input type="hidden" id="lblcoid" runat="server" />
    <input type="hidden" id="lblcurrcomp" runat="server" />
    <input type="hidden" id="lblcurrfunc" runat="server" />
    <input type="hidden" id="lblnewcomp" runat="server" />
    <input type="hidden" id="lblnewcompdesc" runat="server" />
    <input type="hidden" id="lblnewfunc" runat="server" />
    <input type="hidden" id="lblnewcoid" runat="server" />
    <input type="hidden" id="lblnewfuid" runat="server" />
    <input type="hidden" id="lblpmtskid" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblcopymode" runat="server" />
    <input type="hidden" id="lblcompcnt" runat="server" />
    <input type="hidden" id="lblustr" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblcheckdnd" runat="server" />
    </form>
</body>
</html>
