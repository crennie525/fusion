<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmmetpm.aspx.vb" Inherits="lucy_r12.pmmetpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>pmme</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmmetpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
     <!--
        function getchart(id, id1, hi, spec) {
            //alert(id + ", " + id1)
            var popwin = "directories=0,height=600,width=950,location=0,menubar=0,resizable=0,status=0,toolbar=0,scrollbars=1";
            window.open("ChartSampleTPM.aspx?pmtskid=" + id + "&mid=" + id1, "repWin", popwin);
            /*
            var curl = document.getElementById("lblcharturl").value;
            if (hi = "") {
            hi = 6;
            }
            if (spec == "") {
            spec = 6;
            }
            if (hi > 5 && spec > 5) {
            window.open(curl + "PMMeasViewer0.aspx?pmtskid=" + id + "&tmdidpar=" + id1);
            }
            else {
            window.open(curl + "PMMeasViewer.aspx?pmtskid=" + id + "&tmdidpar=" + id1);
            }
            */
        }
        function getsuper(typ) {
            var skill //= document.getElementById("lblskillid").value;
            var ro = document.getElementById("lblro").value;
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    var retarr = eReturn.split(",")
                    if (typ == "sup") {
                        document.getElementById("lblsup").value = retarr[0];
                        document.getElementById("txtsup").value = retarr[1];
                    }
                    else {
                        document.getElementById("lbllead").value = retarr[0];
                        document.getElementById("txtlead").value = retarr[1];
                        document.getElementById("lblsup").value = retarr[2];
                        document.getElementById("txtsup").value = retarr[3];
                    }
                }
            }
        }
        function gettsk1(pmid, task) {
            var eReturn = window.showModalDialog("../appsmantpm/fmtasktpmdialog.aspx?pmid=" + pmid + "&task=" + task, "", "dialogHeight:300px; dialogWidth:500px; resizable=yes");
            /*
            var ycoord = document.getElementById("yCoord").value;
            if (ycoord != "") {
            var tp = parseInt(ycoord) + 10;
            tp = tp + "px"
            document.getElementById("tskdiv").style.top = tp;
            }
            else {
            document.getElementById("tskdiv").style.top = "10px";
            }
            document.getElementById("tskdiv").style.left = "10px";
            document.getElementById("tskdiv").className = "view";
            document.getElementById("txttsk").value = tsk;
            */
        }
        function closetsk1() {
            document.getElementById("tskdiv").className = "details";
        }
         //-->
    </script>
</head>
<body onload="scrolltop();checkit();" class="tbg">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" width="700">
        <tr>
            <td>
                <table width="770">
                    <tr height="22">
                        <td class="label" width="80">
                            <asp:Label ID="lang890" runat="server">Work Order#</asp:Label>
                        </td>
                        <td class="plainlabel" id="tdwo" width="140" runat="server">
                        </td>
                        <td class="plainlabel" id="tdwod" width="450" runat="server">
                        </td>
                        <td align="right" width="50">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="22">
            <td class="thdrsing label">
                <asp:Label ID="lang891" runat="server">TPM Task Measurements</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DataGrid ID="dgmeas" runat="server" Width="688px" ShowFooter="True" CellPadding="0"
                    GridLines="None" AllowPaging="True" AllowCustomPaging="True" AutoGenerateColumns="False"
                    CellSpacing="1" BackColor="transparent" PageSize="100">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <EditItemStyle Height="15px"></EditItemStyle>
                    <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Task#">
                            <HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label28" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label29" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif">
                            <HeaderStyle Width="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <img src="../images/appbuttons/minibuttons/magnifier.gif" id="imgmi" runat="server">
                            </ItemTemplate>
                            <EditItemTemplate>
                                <img src="../images/appbuttons/minibuttons/magnifier.gif" id="imgme" runat="server">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Type">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label3" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Hi">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label6" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Lo">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label8" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Spec">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label10" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Measurement">
                            <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Width="90px" Text='<%# DataBinder.Eval(Container, "DataItem.measurement2") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtmeas" runat="server" Width="90px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.measurement2") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="History">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <img src="../images/appbuttons/minibuttons/lchart.gif" onmouseover="return overlib('View History Graph', ABOVE, LEFT)"
                                    onmouseout="return nd()" id="ihg" runat="server">
                                <img id="img" runat="server" onmouseover="return overlib('Print History', ABOVE, LEFT)"
                                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/printex.gif">
                            </ItemTemplate>
                            <EditItemTemplate>
                                <img src="../images/appbuttons/minibuttons/lchart.gif" onmouseover="return overlib('View History Graph', ABOVE, LEFT)"
                                    onmouseout="return nd()" id="ihga" runat="server">
                                <img id="imga" runat="server" onmouseover="return overlib('Print History', ABOVE, LEFT)"
                                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/printex.gif">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="WO">
                            <HeaderStyle Width="30px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <img src="../images/appbuttons/minibuttons/wo.gif" onmouseover="return overlib('Generate Corrective Action Work Order', ABOVE, LEFT)"
                                    onmouseout="return nd()" id="iwo" runat="server">
                            </ItemTemplate>
                            <EditItemTemplate>
                                <img src="../images/appbuttons/minibuttons/wo.gif" onmouseover="return overlib('Generate Corrective Action Work Order', ABOVE, LEFT)"
                                    onmouseout="return nd()" id="iwoa" runat="server">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblpmtskid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblpmtskida" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbltmdid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.tmdid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lbltmdida" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.tmdid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
                        ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <div class="details" id="rtdiv" style="z-index: 999; border-bottom: black 1px solid;
        border-left: black 1px solid; width: 370px; height: 180px; border-top: black 1px solid;
        border-right: black 1px solid">
        <table cellspacing="0" cellpadding="0" width="370" bgcolor="white">
            <tr bgcolor="blue" height="20">
                <td class="whitelabel">
                    <asp:Label ID="lang892" runat="server">Work Order Details</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closert();" height="18" alt="" src="../images/close.gif" width="18"><br>
                </td>
            </tr>
            <tr class="details" id="trrt" height="180">
                <td class="bluelabelb" valign="middle" align="center" colspan="2">
                    <asp:Label ID="lang893" runat="server">Moving Window...</asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr height="20">
                            <td width="20">
                            </td>
                            <td width="20">
                            </td>
                            <td width="120" class="bluelabel">
                                <asp:Label ID="lang894" runat="server">Skill Required</asp:Label>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="ddskill" runat="server" Width="160px" CssClass="plainlabel">
                                </asp:DropDownList>
                            </td>
                            <td width="50">
                            </td>
                        </tr>
                        <tr height="20">
                            <td>
                                <input id="cbsupe" type="checkbox" runat="server" name="cbsupe">
                            </td>
                            <td>
                                <img onmouseover="return overlib('Check to send email alert to Supervisor')" onmouseout="return nd()"
                                    src="../images/appbuttons/minibuttons/emailcb.gif">
                            </td>
                            <td class="bluelabel">
                                <asp:Label ID="lang895" runat="server">Supervisor</asp:Label>
                            </td>
                            <td width="140">
                                <asp:TextBox ID="txtsup" runat="server" Width="130px" CssClass="plainlabel" ReadOnly="True"></asp:TextBox>
                            </td>
                            <td width="20">
                                <img onclick="getsuper('sup');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    border="0">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr height="20">
                            <td>
                                <input id="cbleade" type="checkbox" runat="server" name="cbleade">
                            </td>
                            <td>
                                <img onmouseover="return overlib('Check to send email alert to Lead Craft')" onmouseout="return nd()"
                                    src="../images/appbuttons/minibuttons/emailcb.gif">
                            </td>
                            <td class="bluelabel">
                                <asp:Label ID="lang896" runat="server">Lead Craft</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtlead" runat="server" Width="130px" CssClass="plainlabel" ReadOnly="True"></asp:TextBox>
                            </td>
                            <td>
                                <img onclick="getsuper('lead');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    border="0">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr height="20">
                            <td>
                            </td>
                            <td>
                            </td>
                            <td class="bluelabel">
                                <asp:Label ID="lang897" runat="server">Work Type</asp:Label>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="ddwt" runat="server" CssClass="plainlabel">
                                    <asp:ListItem Value="CM" Selected="True">Corrective Maintenance</asp:ListItem>
                                    <asp:ListItem Value="EM">Emergency Maintenance</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr height="20">
                            <td>
                            </td>
                            <td>
                            </td>
                            <td class="bluelabel">
                                <asp:Label ID="lang898" runat="server">Target Start</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtstart" runat="server" Width="130px" CssClass="plainlabel" ReadOnly="True"></asp:TextBox>
                            </td>
                            <td>
                                <img onclick="getcal('tstart');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                    width="19">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr height="20">
                            <td colspan="6" class="bluelabel">
                                <asp:Label ID="lang899" runat="server">Problem Description</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <asp:TextBox ID="txtprob" runat="server" Width="380px" TextMode="MultiLine" Height="50px"
                                    CssClass="plainlabel"></asp:TextBox>
                            </td>
                        </tr>
                        <tr height="20">
                            <td colspan="6" class="bluelabel">
                                <asp:Label ID="lang900" runat="server">Corrective Action</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <asp:TextBox ID="txtcorr" runat="server" Width="380px" TextMode="MultiLine" Height="50px"
                                    CssClass="plainlabel"></asp:TextBox>
                            </td>
                        </tr>
                        <tr height="30">
                            <td align="right" colspan="6">
                                <asp:ImageButton ID="btnwo" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif">
                                </asp:ImageButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div class="details" id="tskdiv" style="z-index: 999; border-bottom: black 1px solid;
        position: absolute; border-left: black 1px solid; width: 450px; height: 180px;
        border-top: black 1px solid; border-right: black 1px solid">
        <table cellspacing="0" cellpadding="0" width="450" bgcolor="white">
            <tr bgcolor="blue">
                <td>
                    &nbsp;
                    <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
                        ForeColor="White">Task Description</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closetsk();" alt="" src="../images/appbuttons/minibuttons/close.gif"><br>
                </td>
            </tr>
            <tr class="tbg" height="30">
                <td style="padding-bottom: 3px; padding-left: 3px; padding-right: 3px; padding-top: 3px"
                    colspan="2">
                    <asp:TextBox ID="txttsk" runat="server" TextMode="MultiLine" ReadOnly="True" Width="440px"
                        CssClass="plainlabel" Height="170px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <input id="xCoord" type="hidden" name="xCoord" runat="server">
    <input id="yCoord" type="hidden" name="yCoord" runat="server">
    <input id="lbltid" type="hidden" runat="server" name="lbltid">
    <input type="hidden" id="lblmcnt" runat="server" name="lblmcnt">
    <input type="hidden" id="lblmup" runat="server" name="lblmup">
    <input type="hidden" id="lblpmid" runat="server" name="lblpmid">
    <input type="hidden" id="lblpmtid" runat="server" name="lblpmtid">
    <input type="hidden" id="lbltmid" runat="server" name="lbltmid"><input type="hidden"
        id="lblcid" runat="server" name="lblcid">
    <input type="hidden" id="lblsup" runat="server" name="lblsup">
    <input type="hidden" id="lbllead" runat="server" name="lbllead">
    <input type="hidden" id="lblwo" runat="server" name="lblwo"><input type="hidden"
        id="lblcomid" runat="server" name="lblcomid">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblshift" runat="server">
    <input id="lbldragid" type="hidden" name="lbldragid" runat="server"><input id="lblrowid"
        type="hidden" name="lblrowid" runat="server">
    <input type="hidden" id="lbllog" runat="server" name="lbllog">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    </form>
</body>
</html>
