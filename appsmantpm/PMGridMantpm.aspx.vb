

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMGridMantpm
    Inherits System.Web.UI.Page
    Protected WithEvents iwi As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents iwa As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang858 As System.Web.UI.WebControls.Label

    Protected WithEvents lang857 As System.Web.UI.WebControls.Label

    Protected WithEvents lang856 As System.Web.UI.WebControls.Label

    Protected WithEvents lang855 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim ds As DataSet
    Dim tasksg As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim start, tl, cid, sid, did, clid, eqid, chk, fuid, Val, field, name, Filter, coid, pmid, jump, typ, fcust As String
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluserid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblislabor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisplanner As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcadm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim co_only, Login, userid, islabor, isplanner, cadm As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rptrtasks As System.Web.UI.WebControls.Repeater

    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents taskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        'Dim app As New AppUtils
        'Dim url As String = app.Switch
        'If url <> "ok" Then
        'appchk.Value = "switch"
        'End If

        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try

        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            Try
                userid = HttpContext.Current.Session("userid").ToString()
                islabor = HttpContext.Current.Session("islabor").ToString()
                isplanner = HttpContext.Current.Session("isplanner").ToString()
                cadm = HttpContext.Current.Session("cadm").ToString()
                lbluserid.Value = userid
                lblislabor.Value = islabor
                lblisplanner.Value = isplanner
                lblcadm.Value = cadm
            Catch ex As Exception

            End Try
            start = Request.QueryString("start").ToString
            jump = Request.QueryString("jump").ToString
            sid = lblsid.Value
            taskcnt.Value = "0"
            If start = "yes" Then
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                If jump = "yes" Then
                    fcust = Request.QueryString("fcust").ToString
                    pmid = Request.QueryString("pmid").ToString
                    lblpmid.Value = pmid
                    typ = Request.QueryString("typ").ToString
                    lbltyp.Value = typ
                    If typ = "no" Then
                        Response.Redirect("PMDivMantpm.aspx?pmid=" & pmid & "&eqid=" & eqid & "&fcust=" & fcust & "&sid=" & sid)
                    Else
                        fuid = Request.QueryString("fu").ToString
                        lblfuid.Value = fuid
                        coid = Request.QueryString("co").ToString
                        lblcoid.Value = coid
                        tasksg.Open()
                        LoadPage(eqid, typ)
                        tasksg.Dispose()
                    End If

                Else
                    tasksg.Open()
                    LoadPage(eqid)
                    tasksg.Dispose()
                End If
            Else
                eqid = "na"
                tasksg.Open()
                LoadPage(eqid)
                tasksg.Dispose()
            End If
        End If

    End Sub
    Private Sub LoadPage(ByVal eqid As String, Optional ByVal typ As String = "no")
        Dim cnt As Integer
        Dim sqlcnt As String
        isplanner = lblisplanner.Value
        islabor = lblislabor.Value
        cadm = lblcadm.Value
        userid = lbluserid.Value
        If eqid = "na" Then
            cnt = 0
            sql = "select distinct pm.pmid, pm.fcust, " _
            + "pmd = (" _
            + "case t.ptid " _
            + "when 0 then 'None' " _
            + "else t.pretech " _
            + "End " _
            + "), " _
            + "pm = ('Operator/' + t.freq + ' days/' + t.rd), " _
            + "pm.lastdate, pm.nextdate, '0' as 'flag' " _
            + "from tpm pm join pmtaskstpm t on t.pmid = pm.pmid " _
            + "where pm.eqid = 0 order by nextdate"
        Else
            cnt = 1
            If typ = "no" Then
                'sql = "select distinct pm.pmid, " _
                '+ "pmd = (" _
                '+ "case pm.ptid " _
                '+ "when 0 then 'None' " _
                '+ "else pm.pretech " _
                '+ "End " _
                '+ "), " _
                '+ "pm = ('Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd " _
                '+ "+ case len(pm.days) when  0 then '' else ' - ' + pm.days end), " _
                '+ "Convert(char(10), pm.lastdate,101) as 'lastdate', Convert(char(10), pm.nextdate,101) as 'nextdate', '0' as 'flag' " _
                '+ "from tpm pm " _
                '+ "where pm.eqid = '" & eqid & "' order by nextdate"

                sql = "select distinct pm.pmid, pm.fcust, pmd = (case pm.ptid when 0 then 'None' else pm.pretech End ), " _
               + "pm = ( " _
                + "case isnull(len(pm.shift),0) " _
                + "when 0 then 'Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd  " _
                + "else " _
                + "case pm.freq " _
                + "when 0 then 'Operator/STATIC/' + pm.rd + ' - ' + isnull(pm.shift,'open') + " _
                + "case len(pm.days) " _
                + "when 0 then '' " _
                + "else " _
                + "case pm.freq " _
                + "when 1 then '' " _
                + "else ' - ' " _
                + "+ pm.days end end  " _
                + "else 'Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd + ' - ' + isnull(pm.shift,'open') +  " _
                + "case len(pm.days) " _
                + "when 0 then '' " _
                + "else " _
                + "case pm.freq " _
                + "when 1 then '' " _
                + "else ' - ' " _
                + "+ pm.days  " _
                + "end end end end), " _
               + "Convert(char(10), pm.lastdate,101) as 'lastdate', Convert(char(10), pm.nextdate,101) as 'nextdate', '0' as 'flag' " _
               + "from tpm pm where pm.eqid = '" & eqid & "' and pm.rd is not null"
                If cadm <> "1" Then
                    If islabor = "1" And isplanner <> "1" Then
                        sql += " and (pm.eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "') or " _
                        + "pm.eqid in (select eqid from tpm where leadid = '" & userid & "'))"
                    End If
                End If

                sql += " order by nextdate"
            ElseIf typ = "fu" Then
                fuid = lblfuid.Value
                'sql = "select distinct pm.pmid, " _
                '+ "pmd = (case pm.ptid when 0 then 'None' else pm.pretech End ), " _
                '+ "pm = ('Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd), " _
                '+ "lastdate = Convert(char(10), pm.lastdate,101), " _
                '+ "nextdate = Convert(char(10), pm.nextdate,101), " _
                '+ "flag = (select count(*) from pmtracktpm t where t.pmid = pm.pmid and t.funcid = '" & fuid & "') " _
                '+ "from tpm pm where pm.eqid = '" & eqid & "' " _
                '+ "order by flag desc, nextdate asc"

                sql = "select distinct pm.pmid, pm.fcust, pmd = (case pm.ptid when 0 then 'None' else pm.pretech End ), " _
               + "pm = ( " _
                + "case isnull(len(pm.shift),0) " _
                + "when 0 then 'Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd  " _
                + "else " _
                + "case pm.freq " _
                + "when 0 then 'Operator/STATIC/' + pm.rd + ' - ' + isnull(pm.shift,'open') + " _
                + "case len(pm.days) " _
                + "when 0 then '' " _
                + "else " _
                + "case pm.freq " _
                + "when 1 then '' " _
                + "else ' - ' " _
                + "+ pm.days end end  " _
                + "else 'Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd + ' - ' + isnull(pm.shift,'open') +  " _
                + "case len(pm.days) " _
                + "when 0 then '' " _
                + "else " _
                + "case pm.freq " _
                + "when 1 then '' " _
                + "else ' - ' " _
                + "+ pm.days  " _
                + "end end end end), " _
               + "Convert(char(10), pm.lastdate,101) as 'lastdate', Convert(char(10), pm.nextdate,101) as 'nextdate', '0', " _
                + "flag = (select count(*) from pmtracktpm t where t.pmid = pm.pmid and t.funcid = '" & fuid & "') " _
               + "from tpm pm where pm.eqid = '" & eqid & "' and pm.rd is not null "
                If cadm <> "1" Then
                    If islabor = "1" And isplanner <> "1" Then
                        sql += " and (pm.eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "') or " _
                        + "pm.eqid in (select eqid from tpm where leadid = '" & userid & "'))"
                    End If
                End If

                sql += " order by flag desc, nextdate"
            ElseIf typ = "co" Then
                coid = lblcoid.Value
                'sql = "select distinct pm.pmid, " _
                '+ "pmd = (case pm.ptid when 0 then 'None' else pm.pretech End ), " _
                '+ "pm = ('Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd), " _
                '+ "lastdate = Convert(char(10), pm.lastdate,101), " _
                '+ "nextdate = Convert(char(10), pm.nextdate,101), " _
                '+ "flag = (select count(*) from pmtracktpm t where t.pmid = pm.pmid and t.comid = '" & coid & "') " _
                '+ "from tpm pm where pm.eqid = '" & eqid & "' " _
                '+ "order by flag desc, nextdate asc"
                sql = "select distinct pm.pmid, pm.fcust, pmd = (case pm.ptid when 0 then 'None' else pm.pretech End ), " _
                + "pm = ( " _
                + "case isnull(len(pm.shift),0) " _
                + "when 0 then 'Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd  " _
                + "else " _
                + "case pm.freq " _
                + "when 0 then 'Operator/STATIC/' + pm.rd + ' - ' + isnull(pm.shift,'open') + " _
                + "case len(pm.days) " _
                + "when 0 then '' " _
                + "else " _
                + "case pm.freq " _
                + "when 1 then '' " _
                + "else ' - ' " _
                + "+ pm.days end end  " _
                + "else 'Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd + ' - ' + isnull(pm.shift,'open') +  " _
                + "case len(pm.days) " _
                + "when 0 then '' " _
                + "else " _
                + "case pm.freq " _
                + "when 1 then '' " _
                + "else ' - ' " _
                + "+ pm.days  " _
                + "end end end end), " _
                + "Convert(char(10), pm.lastdate,101) as 'lastdate', Convert(char(10), pm.nextdate,101) as 'nextdate', " _
                + "flag = (select count(*) from pmtracktpm t where t.pmid = pm.pmid and t.comid = '" & coid & "') " _
                + "from tpm pm where pm.eqid = '" & eqid & "' and pm.rd is not null "

                If cadm <> "1" Then
                    If islabor = "1" And isplanner <> "1" Then
                        sql += " and (pm.eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "') or " _
                        + "pm.eqid in (select eqid from tpm where leadid = '" & userid & "'))"
                    End If
                End If

                sql += " order by flag desc, nextdate"
            Else
                sql = "select distinct pm.pmid, pm.fcust, pmd = (case pm.ptid when 0 then 'None' else pm.pretech End ), " _
              + "pm = ( " _
               + "case isnull(len(pm.shift),0) " _
               + "when 0 then 'Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd  " _
               + "else " _
               + "case pm.freq " _
               + "when 0 then 'Operator/STATIC/' + pm.rd + ' - ' + isnull(pm.shift,'open') + " _
               + "case len(pm.days) " _
               + "when 0 then '' " _
               + "else " _
               + "case pm.freq " _
               + "when 1 then '' " _
               + "else ' - ' " _
               + "+ pm.days end end  " _
               + "else 'Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd + ' - ' + isnull(pm.shift,'open') +  " _
               + "case len(pm.days) " _
               + "when 0 then '' " _
               + "else " _
               + "case pm.freq " _
               + "when 1 then '' " _
               + "else ' - ' " _
               + "+ pm.days  " _
               + "end end end end), " _
              + "Convert(char(10), pm.lastdate,101) as 'lastdate', Convert(char(10), pm.nextdate,101) as 'nextdate', '0' as 'flag' " _
              + "from tpm pm where pm.eqid = '" & eqid & "' and pm.rd is not null"
                If cadm <> "1" Then
                    If islabor = "1" And isplanner <> "1" Then
                        sql += " and (pm.eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "') or " _
                        + "pm.eqid in (select eqid from tpm where leadid = '" & userid & "'))"
                    End If
                End If

                sql += " order by nextdate"
            End If


        End If
        If cadm <> "1" Then
            If islabor = "1" And isplanner <> "1" Then
                sqlcnt = "select count(*) from tpm where eqid = '" & eqid & "' " _
               + "and (eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "') or " _
               + "eqid in (select eqid from tpm where leadid = '" & userid & "'))"
            Else
                sqlcnt = "select count(*) from tpm where eqid = '" & eqid & "'"
            End If
        Else
            sqlcnt = "select count(*) from tpm where eqid = '" & eqid & "'"
        End If

        If cnt <> 0 Then
            cnt = tasksg.Scalar(sqlcnt)
            taskcnt.Value = cnt

        Else
            If eqid <> "na" And islabor = "1" Then
                Dim strMessage As String = "No Records Found for Current User"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            taskcnt.Value = "0"
        End If
        ds = tasksg.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        rptrtasks.DataSource = dt
        Dim i, eint As Integer
        eint = 15
        For i = cnt To eint
            dt.Rows.InsertAt(dt.NewRow(), i)
        Next
        rptrtasks.DataBind()
    End Sub


    Private Sub rptrtasks_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrtasks.ItemCommand
        If e.CommandName = "Select" Then
            Dim pmid, pmstr, pdm, dlast, dnext, fcust As String
            Try
                pmid = CType(e.Item.FindControl("lblpmiditem"), Label).Text
                pmstr = CType(e.Item.FindControl("lbltn"), LinkButton).Text
                pdm = CType(e.Item.FindControl("lblpdm"), Label).Text
                dlast = CType(e.Item.FindControl("lbllast"), Label).Text
                dnext = CType(e.Item.FindControl("lblnext"), Label).Text
                fcust = CType(e.Item.FindControl("lblcusti"), Label).Text
            Catch ex As Exception
                pmid = CType(e.Item.FindControl("lblpmidalt"), Label).Text
                pmstr = CType(e.Item.FindControl("lbltnalt"), LinkButton).Text
                pdm = CType(e.Item.FindControl("lblpdmalt"), Label).Text
                dlast = CType(e.Item.FindControl("lbllastalt"), Label).Text
                dnext = CType(e.Item.FindControl("lblnextalt"), Label).Text
                fcust = CType(e.Item.FindControl("lblcusta"), Label).Text
            End Try
            tl = lbltasklev.Value
            'cid = lblcid.Value
            sid = lblsid.Value
            'did = lbldid.Value
            'clid = lblclid.Value
            eqid = lbleqid.Value
            'chk = lblchk.Value
            'fuid = lblfuid.Value
            'coid = lblcoid.Value
            Response.Redirect("PMDivMantpm.aspx?pmid=" & pmid & "&eqid=" & eqid & "&pmstr=" & pmstr & "&pdm=" & pdm & "&last=" & dlast & "&next=" & dnext & "&fcust=" & fcust & "&sid=" & sid)

        End If
    End Sub

    Private Sub rptrtasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtasks.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Then
            Dim link As LinkButton = CType(e.Item.FindControl("lbltn"), LinkButton)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "flag").ToString
            If id <> "0" Then
                link.Attributes.Add("class", "redlink")
                'link.Attributes("CssClass") = "redlink"
                'link.Attributes("ForeColor") = "Red"
            End If
            Dim simg As HtmlImage = CType(e.Item.FindControl("iwi"), HtmlImage)
            Dim newdate As String = DataBinder.Eval(e.Item.DataItem, "nextdate").ToString
            If newdate <> "" Then
                newdate = CType(newdate, DateTime)
                If newdate < Now Then
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/rwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov83", "PMGridMantpm.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                Else
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/gwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov84", "PMGridMantpm.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                End If
            Else
                simg.Attributes.Add("src", "../images/appbuttons/minibuttons/2PX.gif")
            End If

        ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim link As LinkButton = CType(e.Item.FindControl("lbltnalt"), LinkButton)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "flag").ToString
            If id <> "0" Then
                link.Attributes.Add("class", "redlink")
                'link.Attributes("CssClass") = "linklabelred"
            End If
            Dim simg As HtmlImage = CType(e.Item.FindControl("iwa"), HtmlImage)
            Dim newdate As String = DataBinder.Eval(e.Item.DataItem, "nextdate").ToString
            If newdate <> "" Then
                newdate = CType(newdate, DateTime)
                If newdate < Now Then
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/rwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov85", "PMGridMantpm.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                Else
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/gwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov86", "PMGridMantpm.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                End If
            Else
                simg.Attributes.Add("src", "../images/appbuttons/minibuttons/2PX.gif")
            End If
        End If

        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang855 As Label
                lang855 = CType(e.Item.FindControl("lang855"), Label)
                lang855.Text = axlabs.GetASPXPage("PMGridMantpm.aspx", "lang855")
            Catch ex As Exception
            End Try
            Try
                Dim lang856 As Label
                lang856 = CType(e.Item.FindControl("lang856"), Label)
                lang856.Text = axlabs.GetASPXPage("PMGridMantpm.aspx", "lang856")
            Catch ex As Exception
            End Try
            Try
                Dim lang857 As Label
                lang857 = CType(e.Item.FindControl("lang857"), Label)
                lang857.Text = axlabs.GetASPXPage("PMGridMantpm.aspx", "lang857")
            Catch ex As Exception
            End Try
            Try
                Dim lang858 As Label
                lang858 = CType(e.Item.FindControl("lang858"), Label)
                lang858.Text = axlabs.GetASPXPage("PMGridMantpm.aspx", "lang858")
            Catch ex As Exception
            End Try

        End If

    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang855.Text = axlabs.GetASPXPage("PMGridMantpm.aspx", "lang855")
        Catch ex As Exception
        End Try
        Try
            lang856.Text = axlabs.GetASPXPage("PMGridMantpm.aspx", "lang856")
        Catch ex As Exception
        End Try
        Try
            lang857.Text = axlabs.GetASPXPage("PMGridMantpm.aspx", "lang857")
        Catch ex As Exception
        End Try
        Try
            lang858.Text = axlabs.GetASPXPage("PMGridMantpm.aspx", "lang858")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            iwa.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMGridMantpm.aspx", "iwa") & "', ABOVE, LEFT)")
            iwa.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            iwi.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMGridMantpm.aspx", "iwi") & "', ABOVE, LEFT)")
            iwi.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
