<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMSetAdjManPREtpm.aspx.vb"
    Inherits="lucy_r12.PMSetAdjManPREtpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMSetAdjManPRE</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMSetAdjManPREtpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td>
                    <asp:DataList ID="dgadj" runat="server">
                        <HeaderTemplate>
                            <table width="880">
                                <tr>
                                    <td width="60">
                                    </td>
                                    <td width="60">
                                    </td>
                                    <td width="510">
                                    </td>
                                    <td width="100">
                                    </td>
                                    <td width="100">
                                    </td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr id="trfunc" runat="server">
                                <td class="label">
                                    <asp:Label ID="lang901" runat="server">Function:</asp:Label>
                                </td>
                                <td class="plainlabel" colspan="5">
                                    <%# DataBinder.Eval(Container, "DataItem.func") %>
                                </td>
                            </tr>
                            <tr id="trdiv" runat="server">
                                <td colspan="5">
                                    <hr>
                                </td>
                            </tr>
                            <tr id="trhead" runat="server">
                                <td colspan="4" class="plainlabel">
                                    <b>
                                        <asp:Label ID="lang902" runat="server">TPM Designation:</asp:Label></b>&nbsp;&nbsp;<%# DataBinder.Eval(Container, "DataItem.pm") %>
                                </td>
                                <td>
                                    <asp:Label ID="A1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'
                                        CssClass="details">
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr id="trhdr" runat="server">
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang903" runat="server">Edit</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang904" runat="server">Task#</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang905" runat="server">Task Description</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang906" runat="server">Current Pass Adj</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang907" runat="server">Use Pass Adj?</asp:Label>
                                </td>
                            </tr>
                            <tr id="trtask" runat="server">
                                <td rowspan="3">
                                    <asp:ImageButton ID="Imagebutton1" runat="server" ToolTip="Edit Record" CommandName="Edit"
                                        ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"></asp:ImageButton>
                                </td>
                                <td class="plainlabel">
                                    <asp:Label ID="lbltasknume" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'
                                        CssClass="plainlabel">
                                    </asp:Label>
                                </td>
                                <td class="plainlabel">
                                    <%# DataBinder.Eval(Container, "DataItem.task") %>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:Label ID="lblokadjcurr" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.okadj") %>'
                                        CssClass="plainlabel">
                                    </asp:Label>
                                </td>
                                <td class="plainlabel" align="center">
                                    <%# DataBinder.Eval(Container, "DataItem.useadj") %>
                                </td>
                            </tr>
                            <tr>
                                <td class="btmmenu plainlabel" colspan="2">
                                    <asp:Label ID="lang908" runat="server">Failure Modes</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang909" runat="server">Current Fail Adj</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang910" runat="server">Use Fail Adj?</asp:Label>
                                </td>
                            </tr>
                            <tr id="trfm" runat="server">
                                <td class="plainlabel" colspan="2">
                                    <%# DataBinder.Eval(Container, "DataItem.fm1") %>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:Label ID="lblfadjcurr" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fadj") %>'
                                        CssClass="plainlabel">
                                    </asp:Label>
                                </td>
                                <td class="plainlabel" align="center">
                                    <%# DataBinder.Eval(Container, "DataItem.usefadj") %>
                                </td>
                            </tr>
                            <tr id="Tr4" runat="server">
                                <td colspan="5">
                                    <hr>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <tr id="trfunce" runat="server">
                                <td class="label">
                                    <asp:Label ID="lang911" runat="server">Function:</asp:Label>
                                </td>
                                <td class="plainlabel" colspan="5">
                                    <%# DataBinder.Eval(Container, "DataItem.func") %>
                                </td>
                            </tr>
                            <tr id="trdive" runat="server">
                                <td colspan="5">
                                    <hr>
                                </td>
                            </tr>
                            <tr runat="server">
                                <td colspan="5" class="plainlabel">
                                    <b>
                                        <asp:Label ID="lang912" runat="server">TPM Designation:</asp:Label></b>&nbsp;&nbsp;<%# DataBinder.Eval(Container, "DataItem.pm") %>
                                </td>
                            </tr>
                            <tr id="Tr2" runat="server">
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang913" runat="server">Edit</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang914" runat="server">Task#</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang915" runat="server">Task Description</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang916" runat="server">Current Pass Adj</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang917" runat="server">Use Pass Adj?</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="3">
                                    <asp:ImageButton ID="Imagebutton2" runat="server" ToolTip="Save Changes" CommandName="Update"
                                        ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton3" runat="server" ToolTip="Cancel Changes" CommandName="Cancel"
                                        ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"></asp:ImageButton>
                                </td>
                                <td class="plainlabel">
                                    <%# DataBinder.Eval(Container, "DataItem.tasknum") %>
                                </td>
                                <td class="plainlabel">
                                    <%# DataBinder.Eval(Container, "DataItem.task") %>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:TextBox ID="lblokadj" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.okadj") %>'
                                        Width="40px">
                                    </asp:TextBox>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:DropDownList ID="ddokuse" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("oi")) %>'>
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="btmmenu plainlabel" colspan="2">
                                    <asp:Label ID="lang918" runat="server">Failure Modes</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang919" runat="server">Current Fail Adj</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang920" runat="server">Use Fail Adj?</asp:Label>
                                </td>
                            </tr>
                            <tr id="Tr1" runat="server">
                                <td class="plainlabel" colspan="2">
                                    <%# DataBinder.Eval(Container, "DataItem.fm1") %>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:TextBox ID="lblfadj" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fadj") %>'
                                        Width="40px">
                                    </asp:TextBox>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:DropDownList ID="ddfuse" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("fi")) %>'>
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="Tr5" runat="server">
                                <td colspan="5">
                                    <hr>
                                </td>
                            </tr>
                        </EditItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:DataList>
                </td>
            </tr>
        </tbody>
    </table>
    <input id="lblpmid" type="hidden" runat="server" name="lblpmid">
    <input id="lbltaskcnt" type="hidden" runat="server" name="lbltaskcnt"><input id="lbltaskcurrcnt"
        type="hidden" runat="server" name="lbltaskcurrcnt">
    <input type="hidden" id="lblrow" runat="server" name="lblrow"><input type="hidden"
        id="lbltid" runat="server">
    <input type="hidden" id="lbloadjc" runat="server">
    <input type="hidden" id="lblfadjc" runat="server">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
