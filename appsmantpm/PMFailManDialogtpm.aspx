<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMFailManDialogtpm.aspx.vb"
    Inherits="lucy_r12.PMFailManDialogtpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Task Failure Mode History</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" type="text/javascript" src="../scripts/sessrefdialog.js"></script>
    <script language="javascript" type="text/javascript">
        <!--
        function handlereturn() {
            window.close();
        }
        function pageScroll() {
            window.scrollTo(0, top);
            //scrolldelay = setTimeout('pageScroll()', 200); 
        } 
            //-->	
    </script>
</head>
<body onload="resetsess();">
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td>
                <iframe id="iffmh" runat="server" src="" width="770" height="300" frameborder="no">
                </iframe>
            </td>
        </tr>
    </table>
    <iframe id="ifsession" class="details" src="" frameborder="no" runat="server" style="z-index: 0">
    </iframe>
    <input type="hidden" id="lblsessrefresh" runat="server" name="lblsessrefresh">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
