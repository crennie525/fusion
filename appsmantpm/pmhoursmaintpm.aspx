<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmhoursmaintpm.aspx.vb"
    Inherits="lucy_r12.pmhoursmaintpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>pmhoursmain</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmhoursmaintpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table cellpadding="0" cellspacing="0">
        <tr id="trgraph" runat="server">
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <iframe id="ifg" runat="server" src="" width="360" height="230" frameborder="no">
                            </iframe>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <input class="plainlabel lilmenu" id="Button1" onmouseover="this.className='plainlabel lilmenuhov'"
                                style="width: 100px; height: 24px" onclick="getprint('wk');" onmouseout="this.className='plainlabel lilmenu'"
                                type="button" value="Print Week List" name="btnsub" runat="server">
                            <input class="plainlabel lilmenu" id="Button2" onmouseover="this.className='plainlabel lilmenuhov'"
                                style="width: 100px; height: 24px" onclick="getprint('qtr');" onmouseout="this.className='plainlabel lilmenu'"
                                type="button" value="Print Qtr List" name="btnsub" runat="server">
                            <input class="plainlabel lilmenu" id="btnsw" onmouseover="this.className='plainlabel lilmenuhov'"
                                style="width: 100px; height: 24px" onclick="switchmh();" onmouseout="this.className='plainlabel lilmenu'"
                                type="button" value="Switch To Minutes" name="btnsub" runat="server">&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lbltyp" runat="server">
    <input type="hidden" id="lblnam" runat="server">
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lbldat" runat="server">
    <input type="hidden" id="lblwk" runat="server">
    <input type="hidden" id="lblpdm" runat="server">
    <input type="hidden" id="lblqtr" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
