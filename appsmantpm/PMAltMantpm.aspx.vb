

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMAltMantpm
    Inherits System.Web.UI.Page
	Protected WithEvents ovid95 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid94 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang795 As System.Web.UI.WebControls.Label

	Protected WithEvents lang794 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskip As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim orig, pmid, rev, use, last, freq, ro, who, wonum As String
    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents lblpost As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllast As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgsav As System.Web.UI.HtmlControls.HtmlImage
    Dim alt As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtnewdate As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblorig As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdtop As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            Try
                who = Request.QueryString("who").ToString
            Catch ex As Exception
                who = ""
            End Try
            lblwho.Value = who
            If ro = "1" Then
                imgsav.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                imgsav.Attributes.Add("onclick", "")
            End If
            If who = "" Then
                pmid = Request.QueryString("pmid").ToString
                orig = Request.QueryString("orig").ToString
                last = Request.QueryString("last").ToString
                freq = Request.QueryString("freq").ToString
                lblorig.Value = orig
                lblpmid.Value = pmid
                lbllast.Value = last
                lblfreq.Value = freq
                tdtop.InnerHtml = "Original Date:  " & orig
                rbval.Value = "0"
            Else
                pmid = Request.QueryString("pmid").ToString
                lblpmid.Value = pmid
                alt.Open()
                GetPMData(pmid)
                alt.Dispose()
            End If
        ElseIf Request.Form("lblpost") = "go" Then
            alt.Open()
            ChangeDate()
            alt.Dispose()
        End If

    End Sub
    Private Sub GetPMData(ByVal pmid As String)
        sql = "select isnull(cast(lastdate as varchar(50)), '0') as lastdate, nextdate, wonum, freq from tpm where pmid = '" & pmid & "'"
        dr = alt.GetRdrData(sql)
        Dim origs, lasts, freqs As String
        While dr.Read
            origs = dr.Item("nextdate").ToString
            lasts = dr.Item("lastdate").ToString
            freqs = dr.Item("freq").ToString
            wonum = dr.Item("wonum").ToString
        End While
        dr.Close()
        lblorig.Value = origs
        lblwonum.Value = wonum
        lbllast.Value = lasts
        lblfreq.Value = freqs
        tdtop.InnerHtml = "Original Date:  " & origs
        rbval.Value = "0"
    End Sub
    Private Sub ChangeDate()
        who = lblwho.Value
        wonum = lblwonum.Value
        pmid = lblpmid.Value
        orig = lblorig.Value
        rev = txtnewdate.Text
        use = rbval.Value
        last = lbllast.Value
        freq = lblfreq.Value
        Dim skip As String = lblskip.Value '"yes"
        Dim fdate As Date
        If last <> "0" Then
            If Convert.ToDateTime(rev) <= Convert.ToDateTime(last) And skip <> "yes" Then
                Dim strMessage As String = tmod.getmsg("cdstr359", "PMAltMantpm.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lblpost.Value = "no"
                Exit Sub
            ElseIf orig = "" Then
                use = "0"
                sql = "usp_upalttpm '" & pmid & "', '" & orig & "', '" & rev & "', '" & use & "'"
            Else
                fdate = Convert.ToDateTime(orig)
                fdate = fdate.AddDays(freq)
                If Convert.ToDateTime(rev) >= fdate And skip <> "yes" Then
                    Dim strMessage As String = tmod.getmsg("cdstr360", "PMAltMantpm.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lblpost.Value = "no"
                    Exit Sub
                Else
                    sql = "usp_upalttpm '" & pmid & "', '" & orig & "', '" & rev & "', '" & use & "'"
                End If
            End If
        Else
            sql = "usp_upalttpm '" & pmid & "', '" & orig & "', '" & rev & "', '" & use & "'"
            'something here to address previous changes?
        End If

        Try
            alt.Update(sql)
            If who = "wopm" Then
                sql = "update workorder set schedstart = '" & rev & "' where wonum = '" & wonum & "'"
                alt.Update(sql)
            End If

        Catch ex As Exception
            'stored procedure catches error and takes the appropriate measures
            'this catch needs to remain with no action
        End Try
        If who = "wopm" Then
            lblpost.Value = "wo"
        Else
            lblpost.Value = "go"
        End If
    End Sub


	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang794.Text = axlabs.GetASPXPage("PMAltMantpm.aspx", "lang794")
        Catch ex As Exception
        End Try
        Try
            lang795.Text = axlabs.GetASPXPage("PMAltMantpm.aspx", "lang795")
        Catch ex As Exception
        End Try

    End Sub



    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgsav.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMAltMantpm.aspx", "imgsav") & "', ABOVE, LEFT)")
            imgsav.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid94.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMAltMantpm.aspx", "ovid94") & "', ABOVE, LEFT)")
            ovid94.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid95.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMAltMantpm.aspx", "ovid95") & "', ABOVE, LEFT)")
            ovid95.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
