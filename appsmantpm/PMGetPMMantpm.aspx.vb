

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMGetPMMantpm
    Inherits System.Web.UI.Page
    Protected WithEvents ovid104 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid103 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid102 As System.Web.UI.HtmlControls.HtmlImage


    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim tasks As New Utilities
    Dim sql, cid, sid, eq, eqid, fu, fuid, did, clid, jump, co, coid, typ, fcust, appstr As String
    Dim Filter, filt, dt, df, tl, val, tli, ro, userid, islabor, isplanner, cadm As String
    Dim pmid, type, login As String
    Protected WithEvents tddesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblerr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmod As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljump As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdw As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents iw As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcurr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifrev As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents iadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents jts As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpiccnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tddepts As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlocs1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lang854 As System.Web.UI.WebControls.Label
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfcust As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluserid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblislabor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisplanner As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcadm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbleqdesc As System.Web.UI.WebControls.Label
    Protected WithEvents eqdiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcleantasks As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgototasks As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgetarch As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        'If lbllog.Value <> "no" Then

        If Not IsPostBack Then
            Try
                userid = HttpContext.Current.Session("userid").ToString()
                islabor = HttpContext.Current.Session("islabor").ToString()
                appstr = HttpContext.Current.Session("appstr").ToString()
                isplanner = HttpContext.Current.Session("isplanner").ToString()
                cadm = HttpContext.Current.Session("cadm").ToString()
                lbluserid.Value = userid
                lblislabor.Value = islabor
                lblisplanner.Value = isplanner
                lblcadm.Value = cadm
                lblappstr.Value = appstr
                CheckApps(appstr)
            Catch ex As Exception

            End Try
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                iadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                iadd.Attributes.Add("onclick", "")
            End If
            lblgototasks.Value = "2"
            lblcleantasks.Value = "2"
            lbltaskcnt.Value = "2"
            cid = "0" 'HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            tasks.Open()
            'PopEq(sid)
            jump = Request.QueryString("jump").ToString
            lbljump.Value = jump
            If jump = "yes" Then
                Try
                    fcust = Request.QueryString("fcust").ToString
                    lblfcust.Value = fcust
                Catch ex As Exception

                End Try
                eq = Request.QueryString("eqid").ToString
                lbleqid.Value = eq
                If eq <> "" Then
                    lbltaskcnt.Value = "2"
                    Try
                        'catch is temp
                        typ = Request.QueryString("typ").ToString
                        lbltyp.Value = typ
                        If typ <> "no" Then
                            fuid = Request.QueryString("fu").ToString
                            lblfuid.Value = fuid
                            coid = Request.QueryString("co").ToString
                            lblcoid.Value = coid
                        End If
                    Catch ex As Exception

                    End Try
                    pmid = Request.QueryString("pmid").ToString
                    lblpmid.Value = pmid
                    Try
                        'ddeq.SelectedValue = eq
                        PopEqDesc(eq)
                        CheckTasks(eq)
                    Catch ex As Exception
                        lbltaskcnt.Value = "0"
                    End Try
                    lblcleantasks.Value = "0"
                    CleanTasks()
                End If
            Else
                lbltaskcnt.Value = "0"
            End If
            tasks.Dispose()
        Else
            If Request.Form("lblsubmit") = "new" Then
                tasks.Open()
                eqid = lbleqid.Value
                If eqid <> "" Then
                    CheckEq(eqid)
                End If
                tasks.Dispose()
            ElseIf Request.Form("lblsubmit") = "checkeq" Then
                tasks.Open()
                eqid = lbleqid.Value
                If eqid <> "" Then
                    CheckEq2()
                End If
                tasks.Dispose()
            End If
            tdeq.InnerHtml = lbleq.Value
            appstr = lblappstr.Value
            CheckApps(appstr)
        End If
        'End If
    End Sub
    Private Sub CheckApps(ByVal appstr As String)
        Dim apparr() As String = appstr.Split(",")
        Dim o As Integer = 1
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                Select Case apparr(i)
                    Case "sch"
                        o = 0
                End Select
            Next
        Else
            o = 0
        End If
        If o <> 0 Then
            jts.Attributes.Add("class", "details")
        End If
    End Sub
    Private Sub CheckEq2()
        eq = lbleqid.Value
        'tasks.Open()
        PopEqDesc(eq)
        CheckTasks(eq)
        lblcleantasks.Value = "0"
        CleanTasks()
        'tasks.Dispose()
        lbltaskcnt.Value = "0"
    End Sub
    Private Sub PopEqDesc(ByVal eqid As String)
        'ddeq.SelectedValue = eqid
        Dim eqnum As String
        Dim lmod As Integer, dmod As Integer
        dt = "equipment"
        df = "eqdesc"
        filt = " where eqid = '" & eqid & "'"
        sql = "select * from equipment where eqid = '" & eqid & "'"
        dr = tasks.GetRdrData(sql)
        If dr.Read Then
            tddesc.InnerHtml = dr.Item("eqdesc").ToString
            eqnum = dr.Item("eqnum").ToString
            tdeq.InnerHtml = eqnum
            lbleq.Value = eqnum
            'tdloc.InnerHtml = dr.Item("location").ToString
            'tdspl.InnerHtml = dr.Item("spl").ToString
            lblmod.Value = dr.Item("hastpmmod").ToString
        End If
        dr.Close()

        Dim pcnt As Integer, tcnt As Integer
        Dim pccnt As Integer, tccnt As Integer, ccomid As Integer, allcnt As Integer
        Dim tfcnt As Integer, pfcnt As Integer
        allcnt = 0
        Try
            sql = "usp_checkpmupdatetpm '" & eqid & "'"
            allcnt = tasks.Scalar(sql)

        Catch ex As Exception
            allcnt = 1
        End Try

        'If lblmod.Value = "0" Then
        If allcnt = 0 And lmod = 0 Then
            iw.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov81", "PMGetPMMantpm.aspx.vb") & "', ABOVE, LEFT)")
            iw.Attributes.Add("onmouseout", "return nd()")
            iw.Attributes.Add("onclick", "checkup();") 'Released Temp
            iw.Attributes.Add("src", "../images/appbuttons/minibuttons/gwarning.gif")
            'ElseIf lblmod.Value = "1" Then
        ElseIf allcnt > 0 Or lmod = 1 Then
            'iw.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov82" , "PMGetPMMantpm.aspx.vb") & "', ABOVE, LEFT)")
            'iw.Attributes.Add("onmouseout", "return nd()")
            iw.Attributes.Add("onclick", "checkup();")
            iw.Attributes.Add("src", "../images/appbuttons/minibuttons/rwarning.gif")
        End If
    End Sub
    Private Sub CleanTasks()
        Dim tskchk As String = lblcleantasks.Value
        If tskchk = "0" Then
            lblcleantasks.Value = "1"
            lblgototasks.Value = "1"
        End If
    End Sub
    Private Sub GetEqDesc(ByVal eq As String)
        dt = "equipment"
        df = "eqdesc, pmtyp"
        filt = " where eqid = '" & eq & "'"
        dr = tasks.GetDesc(dt, df, filt)
        If dr.Read Then
            lbleqdesc.Text = dr.Item("eqdesc").ToString
        End If
        dr.Close()
    End Sub

    Private Sub CheckTasks(ByVal eq As String)
        sql = "select count(*) from tpm where eqid = '" & eq & "'"
        Dim cnt As Integer
        cnt = tasks.Scalar(sql)
        isplanner = lblisplanner.Value
        islabor = lblislabor.Value
        cadm = lblcadm.Value
        If cnt = 0 Then
            lbltaskcnt.Value = "0"
            lblgototasks.Value = "0"
            'iadd.Attributes.Add("class", "view")
            'iw.Attributes.Add("class", "details")
            'Dim strMessage As String = tmod.getmsg("cdstr336", "PMGetPMMan.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'CheckEq(eq)

            If islabor = "1" Then
                If cadm = "0" And isplanner = "0" Then
                    Dim strMessage As String = "No Records Found for Current User"

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    iadd.Attributes.Add("class", "view")
                    iw.Attributes.Add("class", "details")
                    Dim strMessage As String = tmod.getmsg("cdstr374", "PMGetPMMan.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If

            Else

            End If
        Else
            If lbljump.Value = "yes" Then
                lbltaskcnt.Value = "1"
                If lblpmid.Value = "0" Then
                    lbltaskcnt.Value = "0"
                End If
                If lbltyp.Value <> "no" Then
                    lbltaskcnt.Value = "2"
                End If
            Else
                lbltaskcnt.Value = "0"
            End If
            lblgototasks.Value = "1"
            iadd.Attributes.Add("class", "details")
            iw.Attributes.Add("class", "view")
        End If
    End Sub
    Private Sub CheckEq(ByVal eq As String)
        sql = "select count(*) from pmtaskstpm where ((freq <> 0 or fcust = 1) and rdid <> 0 and ttid <> 0 and ttid is not null) and eqid = '" & eq & "' and subtask = 0"
        Dim errcnt As Integer
        Dim pmcnt As Integer
        errcnt = tasks.Scalar(sql)
        lblerr.Value = errcnt
        If errcnt <> 0 Then
            sql = "select count(*) from pmtaskstpm where eqid = '" & eq & "'"
            errcnt = tasks.Scalar(sql)
            lblerr.Value = errcnt
            sql = "select count(*) from tpm where eqid = '" & eq & "'"
            pmcnt = tasks.Scalar(sql)
            If pmcnt = 0 Then
                If errcnt <> 0 Then
                    sql = "usp_makenewtpm '" & eq & "'"
                    errcnt = tasks.Scalar(sql)
                    If errcnt <> 0 Then
                        sql = "usp_pmrevtpm '" & eq & "'"
                        tasks.Update(sql)
                        lblgototasks.Value = "1"
                        iadd.Attributes.Add("class", "details")
                        iw.Attributes.Add("class", "view")
                    Else
                        lblgototasks.Value = "0"
                        iadd.Attributes.Add("class", "view")
                        iw.Attributes.Add("class", "details")
                    End If
                Else
                    lblgototasks.Value = "0"
                    iadd.Attributes.Add("class", "view")
                    iw.Attributes.Add("class", "details")
                End If
            Else
                'Dim strMessage As String = tmod.getmsg("cdstr375", "PMGetPMMantpm.aspx.vb")

                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                CheckEq2()
            End If
        Else
            lblgototasks.Value = "0"
            iadd.Attributes.Add("class", "view")
            iw.Attributes.Add("class", "details")
        End If
    End Sub











    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang854.Text = axlabs.GetASPXPage("PMGetPMMantpm.aspx", "lang854")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            iadd.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMGetPMMantpm.aspx", "iadd") & "', ABOVE, RIGHT)")
            iadd.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            iw.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMGetPMMantpm.aspx", "iw") & "', ABOVE, LEFT)")
            iw.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid102.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMGetPMMantpm.aspx", "ovid102") & "', ABOVE, LEFT)")
            ovid102.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid103.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMGetPMMantpm.aspx", "ovid103") & "', ABOVE, LEFT)")
            ovid103.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid104.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMGetPMMantpm.aspx", "ovid104") & "', ABOVE, LEFT)")
            ovid104.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
