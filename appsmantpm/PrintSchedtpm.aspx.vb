

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PrintSchedtpm
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim meas As New Utilities
    Dim typ, nam, dat, wk, qtr, sid, pdm As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                typ = Request.QueryString("typ").ToString
            Catch ex As Exception
                'use for testing
                typ = "wk" 'Request.QueryString("typ").ToString
            End Try
            nam = Request.QueryString("nam").ToString '"2" '
            dat = Request.QueryString("dat").ToString '"39545" '
            wk = Request.QueryString("wk").ToString '"15" '
            qtr = Request.QueryString("qtr").ToString '"2" '
            sid = Request.QueryString("sid").ToString '"12" '
            pdm = Request.QueryString("pdm").ToString '"0" '
        End If

        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
        sb.Append("<tr><td width=""10""></td><td width=""90""></td><td width=""100""></td><td width=""150""></td><td width=""50""><td width=""50""></td></tr>")
        sb.Append("<tr height=""30"" align=""center""><td class=""bigbold"" colspan=""6"">" & tmod.getlbl("cdlbl113" , "PrintSchedtpm.aspx.vb") & "</td></tr>")
        sb.Append("<tr height=""30"" align=""center""><td class=""plainlabel"" colspan=""6"">Effective: " & Now & "</td></tr>")

        'sb.Append("<tr><td class=""label""><u>Start Date</u></td>")
        'sb.Append("<td class=""label""><u>Task</u></td>")
        'sb.Append("</tr>")

        Dim i As Integer
        Dim y As Integer = 0
        Dim cnt As Integer = 0
        Dim sdate, sskill As String
        Dim dhold As String = ""
        Dim shold As String = ""
        sql = "usp_gethoursp4tpm '0', '" & qtr & "', '" & sid & "', '" & nam & "', '%', '0', '0', '0', '0', '" & wk & "', '" & dat & "','" & typ & "'"
        meas.Open()
        dr = meas.GetRdrData(sql)
        While dr.Read
            sdate = dr.Item("StartDate").ToString
            If dhold <> sdate Then
                dhold = sdate
                sb.Append("<tr><td class=""plainlabel graybot"" colspan=""6""><b>Start Date:</b>  " & sdate & "</td>")
                sb.Append("</tr>")
                sskill = dr.Item("skill").ToString
                'If shold <> sskill Then
                shold = sskill
                sb.Append("<tr><td></td><td class=""label""><u>Skill</u></td>")
                sb.Append("<td class=""label""><u>Task</u></td>")
                sb.Append("<td class=""label""><u>Minutes Required</u></td>")
                sb.Append("<td class=""label""><u>Hours Required</u></td>")
                sb.Append("</tr>")
                'End If
            End If
            sb.Append("<tr><td>&nbsp;</td><td class=""plainlabel"">" & sskill & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("name").ToString & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("ttime").ToString & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("ttimeh").ToString & "</td>")
            sb.Append("</tr>")

        End While
        dr.Close()
        meas.Dispose()
        sb.Append("</table>")
        Response.Write(sb.ToString)

    End Sub

End Class
