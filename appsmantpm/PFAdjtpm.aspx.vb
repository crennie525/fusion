

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class PFAdjtpm
    Inherits System.Web.UI.Page
	Protected WithEvents lang767 As System.Web.UI.WebControls.Label

	Protected WithEvents lang766 As System.Web.UI.WebControls.Label

	Protected WithEvents lang765 As System.Web.UI.WebControls.Label

	Protected WithEvents lang764 As System.Web.UI.WebControls.Label

	Protected WithEvents lang763 As System.Web.UI.WebControls.Label

	Protected WithEvents lang762 As System.Web.UI.WebControls.Label

	Protected WithEvents lang761 As System.Web.UI.WebControls.Label

	Protected WithEvents lang760 As System.Web.UI.WebControls.Label

	Protected WithEvents lang759 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim thres As New Utilities
    Dim sql, sid, ro As String
    Protected WithEvents tdpass As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ImageButton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents rbp As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents rbf As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents rbpmp As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents rbpmf As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents lblpass As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfail As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
    Dim pa As Integer = 0
    Protected WithEvents txtdays As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim fa As Integer = 0
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtpass As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtfail As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdf As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                ImageButton1.ImageUrl = "../images/appbuttons/bgbuttons/savedis.gif"
                ImageButton1.Enabled = False
            End If
            sid = HttpContext.Current.Session("dfltps").ToString()
            lblsid.Value = sid
            thres.Open()
            GetVals()
            thres.Dispose()
        End If
        'ImageButton1.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
        'ImageButton1.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")

    End Sub
    Private Sub GetVals()
        Dim cid As String = "0" 
        sid = lblsid.Value
        sql = "select passthrestpm, failthrestpm from pmsysvars where compid = '" & cid & "'"
        dr = thres.GetRdrData(sql)
        While dr.Read
            tdpass.InnerHtml = dr.Item("passthrestpm").ToString
            lblpass.Value = (dr.Item("passthrestpm").ToString)
            tdfail.InnerHtml = dr.Item("failthrestpm").ToString
            lblfail.Value = dr.Item("failthrestpm").ToString
        End While
        dr.Close()
        sql = "select evalue from evars where etype = 'THRESHTPM'" ' and siteid = '" & sid & "'"
        dr = thres.GetRdrData(sql)
        While dr.Read
            txtdays.Text = dr.Item("evalue").ToString
        End While
        dr.Close()

    End Sub

    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim cid As String = "0" 
        Dim p, f, po, fo, pmp, pmf As String
        p = txtpass.Text
        If p = "" Then
            p = lblpass.Value
            pa = 1
        End If
        f = txtfail.Text
        If f = "" Then
            f = lblfail.Value
            fa = 1
        End If
        po = rbp.SelectedValue
        fo = rbf.SelectedValue
        pmp = rbpmp.SelectedValue
        pmf = rbpmf.SelectedValue
        Try
            p = Convert.ToInt32(p)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr351" , "PFAdjtpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Try
            f = Convert.ToInt32(f)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr352" , "PFAdjtpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        thres.Open()
        sql = "usp_thresadmintpm '" & cid & "','" & p & "','" & f & "','" & po & "','" & fo & "','" & pmp & "','" & pmf & "', '" & pa & "','" & fa & "'"
        thres.Update(sql)
        txtpass.Text = ""
        txtfail.Text = ""
        sid = lblsid.Value
        Dim edays As String = txtdays.Text
        sql = "update evars set evalue = '" & edays & "' where etype = 'THRESHTPM'" ' and siteid = '" & sid & "'"
        thres.Update(sql)
        GetVals()
        thres.Dispose()

    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang759.Text = axlabs.GetASPXPage("PFAdjtpm.aspx","lang759")
		Catch ex As Exception
		End Try
		Try
			lang760.Text = axlabs.GetASPXPage("PFAdjtpm.aspx","lang760")
		Catch ex As Exception
		End Try
		Try
			lang761.Text = axlabs.GetASPXPage("PFAdjtpm.aspx","lang761")
		Catch ex As Exception
		End Try
		Try
			lang762.Text = axlabs.GetASPXPage("PFAdjtpm.aspx","lang762")
		Catch ex As Exception
		End Try
		Try
			lang763.Text = axlabs.GetASPXPage("PFAdjtpm.aspx","lang763")
		Catch ex As Exception
		End Try
		Try
			lang764.Text = axlabs.GetASPXPage("PFAdjtpm.aspx","lang764")
		Catch ex As Exception
		End Try
		Try
			lang765.Text = axlabs.GetASPXPage("PFAdjtpm.aspx","lang765")
		Catch ex As Exception
		End Try
		Try
			lang766.Text = axlabs.GetASPXPage("PFAdjtpm.aspx","lang766")
		Catch ex As Exception
		End Try
		Try
			lang767.Text = axlabs.GetASPXPage("PFAdjtpm.aspx","lang767")
		Catch ex As Exception
		End Try

	End Sub

	

	

	Private Sub GetBGBLangs()
		Dim lang as String = lblfslang.value
		Try
			If lang = "eng" Then
			ImageButton1.Attributes.Add("src" , "../images2/eng/bgbuttons/save.gif")
			ElseIf lang = "fre" Then
			ImageButton1.Attributes.Add("src" , "../images2/fre/bgbuttons/save.gif")
			ElseIf lang = "ger" Then
			ImageButton1.Attributes.Add("src" , "../images2/ger/bgbuttons/save.gif")
			ElseIf lang = "ita" Then
			ImageButton1.Attributes.Add("src" , "../images2/ita/bgbuttons/save.gif")
			ElseIf lang = "spa" Then
			ImageButton1.Attributes.Add("src" , "../images2/spa/bgbuttons/save.gif")
			End If
		Catch ex As Exception
		End Try

	End Sub

End Class
