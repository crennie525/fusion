

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class TPMManager
    Inherits System.Web.UI.Page
    Protected WithEvents lang953 As System.Web.UI.WebControls.Label

    Protected WithEvents lang952 As System.Web.UI.WebControls.Label

    Protected WithEvents lang951 As System.Web.UI.WebControls.Label

    Protected WithEvents lang950 As System.Web.UI.WebControls.Label

    Protected WithEvents lang949 As System.Web.UI.WebControls.Label

    Protected WithEvents lang948 As System.Web.UI.WebControls.Label

    Protected WithEvents lang947 As System.Web.UI.WebControls.Label

    Protected WithEvents lang946 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Login, cid, jump, ret As String
    Dim tasklev, sid, did, clid, eqid, fuid, coid, chk, tli, pmid, typ As String
    Dim wo, uid, username, islabor, isplanner, issuper, Logged_In, ro, ms, appstr As String
    Dim main As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljump As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifmain As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifrev As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdact As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcost As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents eq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents pm As System.Web.UI.HtmlControls.HtmlTableCell
    Dim sql As String
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblislabor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblissuper As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisplanner As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblLogged_In As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblms As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents geteq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents iftaskdet As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifarch As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifimg As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents taskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgetarch As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)

        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            Response.Redirect(url)
        End If
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("TPMManager.aspx")
        Catch ex As Exception

        End Try

        'sid = Request.Form("lblps")
        If Not IsPostBack Then
            Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                If ro = "1" Then
                    'pgtitle.InnerHtml = "TPM Manager (Read Only)"
                Else
                    'pgtitle.InnerHtml = "TPM Manager"
                End If
            Catch ex As Exception

            End Try

            lblro.Value = ro
            'tdarch.InnerHtml = "Waiting for location..."
            Dim cid As String = "0" 'HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            taskcnt.Value = "0"
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            ifarch.Attributes.Add("src", "PMArchMantpm.aspx?start=yes" + "&sid=" + sid)
            Try
                jump = Request.QueryString("jump").ToString
            Catch ex As Exception
                jump = "no"
            End Try

            Logged_In = Request.QueryString("Logged_In").ToString
            lblLogged_In.Value = Logged_In
            ro = Request.QueryString("ro").ToString
            lblro.Value = ro
            ms = Request.QueryString("ms").ToString
            lblms.Value = ms
            appstr = Request.QueryString("appstr").ToString
            lblappstr.Value = appstr
            Try
                uid = Request.QueryString("uid").ToString
            Catch ex As Exception
                uid = Request.QueryString("userid").ToString
            End Try
            lbluid.Value = uid
            Try
                username = Request.QueryString("usrname").ToString
            Catch ex As Exception
                username = Request.QueryString("username").ToString
            End Try
            lblusername.Value = username
            islabor = Request.QueryString("islabor").ToString
            lblislabor.Value = islabor
            issuper = Request.QueryString("issuper").ToString
            lblissuper.Value = issuper
            isplanner = Request.QueryString("isplanner").ToString
            lblisplanner.Value = isplanner

            If jump = "yes" Then
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                'typ = Request.QueryString("typ").ToString
                pmid = Request.QueryString("pmid").ToString
                lblpmid.Value = pmid
                tdpm.Attributes.Add("class", "thdr label")
                tdeq.Attributes.Add("class", "thdrhov label")
                pm.Attributes.Add("class", "details")
                eq.Attributes.Add("class", "tdborder view")
                geteq.Attributes.Add("src", "PMGetPMMantpm.aspx?jump=yes&typ=no&eqid=" & eqid & "&pmid=" & pmid)
                lbljump.Value = ""
                sid = lblsid.Value
            Else
                ifmain.Attributes.Add("src", "PMMainMantpm.aspx?sid=" + sid + "&date=" + Now)
                iftaskdet.Attributes.Add("src", "PMGridMantpm.aspx?start=no&jump=no&sid=" + sid)
            End If
        Else
            If Page.Request("lblgetarch") = "eq" Then
                lblgetarch.Value = "yes"
                cid = lblcid.Value
                tli = "4"
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = lblcoid.Value
                chk = lblchk.Value
                geteq.Attributes.Add("src", "PMGetTasksFunc.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid)
            ElseIf Page.Request("lblgetarch") = "fu" Then
                lblgetarch.Value = "yes"
                cid = lblcid.Value
                tli = "5"
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = "" 'lblcoid.Value
                chk = lblchk.Value
                geteq.Attributes.Add("src", "PMGetTasksFunc.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid)
            ElseIf Page.Request("lblgetarch") = "co" Then
                lblgetarch.Value = "yes"
                cid = lblcid.Value
                tli = "5"
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = lblcoid.Value
                chk = lblchk.Value
                geteq.Attributes.Add("src", "PMGetTasksFunc.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid)
            End If
        End If

    End Sub











    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang946.Text = axlabs.GetASPXPage("TPMManager.aspx", "lang946")
        Catch ex As Exception
        End Try
        Try
            lang947.Text = axlabs.GetASPXPage("TPMManager.aspx", "lang947")
        Catch ex As Exception
        End Try
        Try
            lang948.Text = axlabs.GetASPXPage("TPMManager.aspx", "lang948")
        Catch ex As Exception
        End Try
        Try
            lang949.Text = axlabs.GetASPXPage("TPMManager.aspx", "lang949")
        Catch ex As Exception
        End Try
        Try
            lang950.Text = axlabs.GetASPXPage("TPMManager.aspx", "lang950")
        Catch ex As Exception
        End Try
        Try
            lang951.Text = axlabs.GetASPXPage("TPMManager.aspx", "lang951")
        Catch ex As Exception
        End Try
        Try
            lang952.Text = axlabs.GetASPXPage("TPMManager.aspx", "lang952")
        Catch ex As Exception
        End Try
        Try
            lang953.Text = axlabs.GetASPXPage("TPMManager.aspx", "lang953")
        Catch ex As Exception
        End Try

    End Sub

End Class
