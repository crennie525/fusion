<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMFailMantpm.aspx.vb"
    Inherits="lucy_r12.PMFailMantpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMFailMan</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts/dragitem.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMFailMantpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
     <!--
        function getsuper(typ) {
            var skill //= document.getElementById("lblskillid").value;
            var ro = document.getElementById("lblro").value;
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    var retarr = eReturn.split(",")
                    if (typ == "sup") {
                        document.getElementById("lblsup").value = retarr[0];
                        document.getElementById("txtsup").value = retarr[1];
                    }
                    else {
                        document.getElementById("lbllead").value = retarr[0];
                        document.getElementById("txtlead").value = retarr[1];
                        if (retarr[2] != "" && retarr[2] != "0") {
                            document.getElementById("lblsup").value = retarr[2];
                            document.getElementById("txtsup").value = retarr[3];
                        }
                    }
                }
            }
        }
        function checkit() {
            var chk = document.getElementById("lblsave").value;
            if (chk == "yes") {
                window.parent.handlereturn();
            }
        }
         //-->
    </script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td width="100">
            </td>
            <td width="20">
            </td>
            <td width="80">
            </td>
            <td width="20">
            </td>
            <td width="130">
            </td>
            <td width="5">
            </td>
            <td width="50">
            </td>
            <td width="50">
            </td>
            <td width="250">
            </td>
        </tr>
        <tr>
            <td class="plainlabelred" id="tdmsg" align="center" colspan="9" runat="server">
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="X-Small" Font-Names="Arial">Task#</asp:Label>
            </td>
            <td>
            </td>
            <td>
                <asp:Label ID="lbltsk" runat="server" Font-Bold="True" Font-Size="X-Small" Font-Names="Arial"></asp:Label>
            </td>
            <td>
            </td>
            <td align="right" colspan="5">
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif">
                </asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="X-Small" Font-Names="Arial">Failure Mode:</asp:Label>
            </td>
            <td>
                <img id="Img2" onmouseover="return overlib('View Failure Mode History', ABOVE, LEFT)"
                    onclick="getfail();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/fmhist.gif"
                    border="0" runat="server">
            </td>
            <td colspan="6">
                <asp:Label ID="lblfail" runat="server" Font-Bold="True" Font-Size="X-Small" Font-Names="Arial"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="X-Small" Font-Names="Arial">Work Order:</asp:Label>
            </td>
            <td>
                <img id="iwoadd" onmouseover="return overlib('Create a Corrective or Emergency Maintenance Work Order')"
                    onclick="getrt();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/addwhite.gif"
                    runat="server">
            </td>
            <td>
                <asp:Label ID="lblwonum" runat="server" Font-Bold="True" Font-Size="X-Small" Font-Names="Arial"></asp:Label>
            </td>
            <td>
                <img id="imgi2" onmouseover="return overlib('Print This Work Order', ABOVE, LEFT)"
                    onclick="printwo();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/woprint.gif"
                    runat="server">
            </td>
        </tr>
        <tr>
            <td class="btmmenu plainlabel" colspan="5">
                <asp:Label ID="lang821" runat="server">Problem Encountered</asp:Label>
            </td>
            <td>
                &nbsp;
            </td>
            <td class="btmmenu plainlabel" colspan="3">
                <asp:Label ID="lang822" runat="server">Corrective Action</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <asp:TextBox ID="txtprob" runat="server" Width="360px" Height="120px" TextMode="MultiLine"
                    CssClass="plainlabel"></asp:TextBox>
            </td>
            <td>
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtcorr" runat="server" Width="352px" Height="120px" TextMode="MultiLine"
                    CssClass="plainlabel"></asp:TextBox>
            </td>
        </tr>
        <tr class="details">
            <td colspan="7">
                <table>
                    <tr>
                        <td class="bluelabel" colspan="2">
                            <asp:Label ID="lang823" runat="server">Generate Corrective Action Work Order?</asp:Label>
                        </td>
                        <td>
                            <input id="cbcwo" onclick="getrt();" type="checkbox" runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="9" align="right">
                <table>
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif">
                            </asp:ImageButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div class="details" id="rtdiv" style="border-right: black 1px solid; border-top: black 1px solid;
        z-index: 999; border-left: black 1px solid; width: 370px; border-bottom: black 1px solid;
        height: 180px">
        <table cellspacing="0" cellpadding="0" width="370" bgcolor="white">
            <tr bgcolor="blue" height="20">
                <td class="whitelabel">
                    <asp:Label ID="lang824" runat="server">Work Order Details</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closert();" height="18" alt="" src="../images/close.gif" width="18"><br>
                </td>
            </tr>
            <tr class="details" id="trrt" height="180">
                <td class="bluelabelb" valign="middle" align="center" colspan="2">
                    <asp:Label ID="lang825" runat="server">Moving Window...</asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table bgcolor="white">
                        <tr height="20">
                            <td>
                            </td>
                            <td>
                            </td>
                            <td class="bluelabel">
                                <asp:Label ID="lang826" runat="server">Skill Required</asp:Label>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="ddskill" runat="server" Width="160px" CssClass="plainlabel">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr height="20">
                            <td>
                                <input id="cbsupe" type="checkbox" name="cbsupe" runat="server">
                            </td>
                            <td>
                                <img onmouseover="return overlib('Check to send email alert to Supervisor')" onmouseout="return nd()"
                                    src="../images/appbuttons/minibuttons/emailcb.gif">
                            </td>
                            <td class="bluelabel">
                                <asp:Label ID="lang827" runat="server">Supervisor</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtsup" runat="server" Width="130px" CssClass="plainlabel" ReadOnly="True"></asp:TextBox>
                            </td>
                            <td>
                                <img onclick="getsuper('sup');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    border="0">
                            </td>
                        </tr>
                        <tr height="20">
                            <td>
                                <input id="cbleade" type="checkbox" name="cbleade" runat="server">
                            </td>
                            <td>
                                <img onmouseover="return overlib('Check to send email alert to Lead Craft')" onmouseout="return nd()"
                                    src="../images/appbuttons/minibuttons/emailcb.gif">
                            </td>
                            <td class="bluelabel">
                                <asp:Label ID="lang828" runat="server">Lead Craft</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtlead" runat="server" Width="130px" CssClass="plainlabel" ReadOnly="True"></asp:TextBox>
                            </td>
                            <td>
                                <img onclick="getsuper('lead');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    border="0">
                            </td>
                        </tr>
                        <tr height="20">
                            <td>
                            </td>
                            <td>
                            </td>
                            <td class="bluelabel">
                                <asp:Label ID="lang829" runat="server">Work Type</asp:Label>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="ddwt" runat="server" CssClass="plainlabel">
                                    <asp:ListItem Value="CM" Selected="True">Corrective Maintenance</asp:ListItem>
                                    <asp:ListItem Value="EM">Emergency Maintenance</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr height="20">
                            <td>
                            </td>
                            <td>
                            </td>
                            <td class="bluelabel">
                                <asp:Label ID="lang830" runat="server">Target Start</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtstart" runat="server" Width="130px" CssClass="plainlabel" ReadOnly="True"></asp:TextBox>
                            </td>
                            <td>
                                <img onclick="getcal('txtstart');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                    width="19">
                            </td>
                        </tr>
                        <tr height="30">
                            <td align="right" colspan="5">
                                <asp:ImageButton ID="btnwo" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif">
                                </asp:ImageButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblpmid" type="hidden" runat="server">
    <input id="lblpmhid" type="hidden" runat="server">
    <input id="lblpmtid" type="hidden" runat="server">
    <input id="lblpmfid" type="hidden" runat="server">
    <input id="lblfailid" type="hidden" runat="server">
    <input id="lbltask" type="hidden" runat="server">
    <input id="lblfm" type="hidden" runat="server">
    <input id="lblfmid" type="hidden" runat="server">
    <input id="lblwo" type="hidden" runat="server"><input id="lblsubmit" type="hidden"
        runat="server">
    <input id="lblcomid" type="hidden" runat="server"><input id="lblcid" type="hidden"
        runat="server">
    <input id="leade" type="hidden" runat="server">
    <input id="supe" type="hidden" runat="server">
    <input id="lbllead" type="hidden" runat="server">
    <input id="lblsup" type="hidden" runat="server">
    <input id="lblro" type="hidden" runat="server">
    <input id="lbldir" type="hidden" runat="server">
    <input id="lbldragid" type="hidden" name="lbldragid" runat="server"><input id="lblrowid"
        type="hidden" name="lblrowid" runat="server">
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblsave" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblcoi" runat="server" />
    </form>
</body>
</html>
