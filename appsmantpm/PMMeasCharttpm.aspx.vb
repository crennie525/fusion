

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class PMMeasCharttpm
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, pmtskid, tmdid, shift As String
    Dim dr As SqlDataReader
    Dim ch As New Utilities

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        pmtskid = Request.QueryString("tid").ToString '"24" '
        tmdid = Request.QueryString("mid").ToString '"4" '
        shift = Request.QueryString("shift").ToString '"No Shift" '
        Dim c As Line = New Line(680, 380, Page)

        sql = "select top 10 m.hi, m.lo, m.measurement, Convert(char(10), m.pmdate, 101) as schedstart, " _
        + "a.tasknum, a.func, a.compnum, a.cqty, m.type, m.measure, m.spec, " _
        + "maxy = (select max(measurement) from pmTaskMeasDetManHISTtpm where pmtskid = '" & pmtskid & "' and tmdidh in " _
        + "(select top 10 h.tmdidh from pmTaskMeasDetManHISTtpm h where h.pmtskid = '" & pmtskid & "' order by m.pmdate desc)),  " _
        + "maxl = (select min(measurement) from pmTaskMeasDetManHISTtpm where pmtskid = '" & pmtskid & "' and tmdidh in " _
        + "(select top 10 h.tmdidh from pmTaskMeasDetManHISTtpm h where h.pmtskid = '" & pmtskid & "' order by m.pmdate desc)),  " _
        + "pm = (e.eqnum + ' - ' + pm.skill + '/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd) " _
        + "from pmTaskMeasDetManHISTtpm m left join pmhisttpm h on m.pmhid = h.pmhid " _
        + "left join tpm pm on pm.pmid = m.pmid left join equipment e on e.eqid = pm.eqid " _
        + "left join pmtracktpm a on a.pmtskid = m.pmtskid where m.pmtskid = '" & pmtskid & "' " _
        + "and m.tmdidpar = '" & tmdid & "' and m.shift = '" & shift & "' order by m.pmdate desc"

        Dim i As Integer
        Dim y As Integer = 0
        Dim cnt As Integer = 0
        Dim hi, lo, meas As String
        Dim top, bottom As Integer
        Dim maxy, maxd, maxlo, maxlod As Integer
        Dim typ, mea, h, l, s As String
        Dim marr As ArrayList = New ArrayList
        ch.Open()
        dr = ch.GetRdrData(sql)
        While dr.Read
            If cnt = 0 Then
                h = dr.Item("hi").ToString
                hi = dr.Item("maxy").ToString
                l = dr.Item("lo").ToString
                lo = dr.Item("maxl").ToString
                c.Title = dr.Item("pm").ToString
                c.SubTitle = dr.Item("func").ToString
                c.TaskTitle = "Task# " & dr.Item("tasknum").ToString & " - " & dr.Item("compnum").ToString & "(" & dr.Item("cqty").ToString & ")"
                typ = dr.Item("type").ToString
                mea = dr.Item("measure").ToString
                s = dr.Item("spec").ToString
            End If
            cnt = cnt + 1
            Try
                meas = dr.Item("measurement")
                marr.Add(dr.Item("measurement"))
            Catch ex As Exception
                meas = 0
                marr.Add(0)
            End Try


            c.AddxLabel(dr.Item("schedstart").ToString)
        End While
        dr.Close()
        ch.Dispose()
        Dim ht, lt As Integer
        Try
            If Not Double.IsNaN(h) Then
                c.HiLimit = Math.Round(CType(h, Double))
            End If
        Catch ex As Exception

        End Try
        Try
            If Not Double.IsNaN(l) Then
                c.LoLimit = Math.Round(CType(l, Double))
            End If
        Catch ex As Exception

        End Try
        Try
            If Not Double.IsNaN(s) Then
                c.Spec = Math.Round(CType(s, Double))
            End If
        Catch ex As Exception

        End Try

        marr.Reverse()
        For i = 0 To marr.Count - 1
            c.AddValue(y, marr(i))
            If y = 0 Then
                y = 100
            Else
                y = y + 100
            End If
        Next

        c.SpecTitle = typ & " - " & mea & " - HI(" & h & ")-LO(" & l & ")-SPEC(" & s & ")"


        c.Xorigin = 0
        c.ScaleX = (cnt - 1) * 100 '900
        c.Xdivs = cnt - 1 '9

        Try
            If Not Double.IsNaN(hi) Then
                top = Math.Round(CType(hi, Double)) / 10
            End If

        Catch ex As Exception
            top = Math.Round(CType(meas, Double))
            If top = 0 Then
                hi = top + 1
            ElseIf Len(System.Convert.ToString(top)) = 1 Then
                hi = top + 10
            ElseIf Len(System.Convert.ToString(top)) = 2 Then
                hi = top + 100
            ElseIf Len(System.Convert.ToString(top)) = 3 Then
                hi = top + 1000
            ElseIf Len(System.Convert.ToString(top)) = 4 Then
                hi = top + 10000
            ElseIf Len(System.Convert.ToString(top)) = 5 Then
                hi = top + 100000
            End If
        End Try

        If top = 0 Then
            maxy = hi + 1
            maxd = maxy
        ElseIf Len(System.Convert.ToString(top)) = 1 Then
            maxy = (top * 10) + 50 'hi + 10
            maxd = maxy / 10
        ElseIf Len(System.Convert.ToString(top)) = 2 Then
            maxy = (top * 10) + 100 'hi + 100
            maxd = maxy / 10
        ElseIf Len(System.Convert.ToString(top)) = 3 Then
            maxy = (top * 100) + 1000 'hi + 1000
            maxd = maxy / 1000
        ElseIf Len(System.Convert.ToString(top)) = 4 Then
            maxy = (top * 1000) + 10000 'hi + 10000
            maxd = maxy / 10000
        ElseIf Len(System.Convert.ToString(top)) = 5 Then
            maxy = (top * 10000) + 100000 'hi + 100000
            maxd = maxy / 100000
        End If

        Try
            If Not Double.IsNaN(lo) Then
                bottom = (Math.Round(CType(lo, Double)) * -1) / 10
            End If
        Catch ex As Exception
            bottom = Math.Round(CType(meas, Double))
            If bottom = 0 Then
                lo = bottom - 1
            ElseIf Len(System.Convert.ToString(bottom)) = 2 Then
                lo = bottom + 10
            ElseIf Len(System.Convert.ToString(bottom)) = 3 Then
                lo = bottom + 100
            ElseIf Len(System.Convert.ToString(bottom)) = 4 Then
                lo = bottom + 1000
            ElseIf Len(System.Convert.ToString(bottom)) = 5 Then
                lo = bottom + 10000
            ElseIf Len(System.Convert.ToString(bottom)) = 6 Then
                lo = bottom + 100000
            End If
        End Try


        If bottom = 0 Then
            maxlo = lo - 1
            maxlod = maxlo
        ElseIf Len(System.Convert.ToString(bottom)) = 2 Then
            maxlo = (bottom * 10) + 50
            maxlod = maxlo / 10
        ElseIf Len(System.Convert.ToString(bottom)) = 3 Then
            maxlo = (bottom * 10) + 100
            maxlod = maxlo / 10
        ElseIf Len(System.Convert.ToString(bottom)) = 4 Then
            maxlo = (bottom * 100) + 1000
            maxlod = maxlo / 1000
        ElseIf Len(System.Convert.ToString(bottom)) = 5 Then
            maxlo = (bottom * 1000) + 10000
            maxlod = maxlo / 10000
        ElseIf Len(System.Convert.ToString(bottom)) = 6 Then
            maxlo = (bottom * 10000) + 10000
            maxlod = maxlo / 100000
        End If

        If maxlo < 0 Then
            maxlo = maxlo * -1
        End If
        c.Yorigin = 0 - maxlo
        c.ScaleY = maxy + maxlo
        If maxlod < 0 Then
            maxlod = maxlod * -1
        End If
        c.Ydivs = maxd + maxlod

        'orig no neg ref
        'c.Yorigin = 0
        'c.ScaleY = maxy
        'c.Ydivs = maxd

        c.Draw()
    End Sub

End Class
