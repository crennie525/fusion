

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMFailMantpm
    Inherits System.Web.UI.Page
	Protected WithEvents ovid101 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid100 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang830 As System.Web.UI.WebControls.Label

	Protected WithEvents lang829 As System.Web.UI.WebControls.Label

	Protected WithEvents lang828 As System.Web.UI.WebControls.Label

	Protected WithEvents lang827 As System.Web.UI.WebControls.Label

	Protected WithEvents lang826 As System.Web.UI.WebControls.Label

	Protected WithEvents lang825 As System.Web.UI.WebControls.Label

	Protected WithEvents lang824 As System.Web.UI.WebControls.Label

	Protected WithEvents lang823 As System.Web.UI.WebControls.Label

	Protected WithEvents lang822 As System.Web.UI.WebControls.Label

	Protected WithEvents lang821 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim mu As New mmenu_utils_a
    Dim coi As String
    Dim pmid, pmtid, pmhid, pmfid, fm, fmid, task, comid, sid As String
    Dim pmf As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents lbltask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents lblwonum As System.Web.UI.WebControls.Label
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbcwo As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtsup As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlead As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddwt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtstart As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnwo As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sql, cid, ro, dir As String
    Protected WithEvents leade As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents supe As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbsupe As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbleade As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents iwoadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgi2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Textbox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldir As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdmsg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbldragid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrowid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsave As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoi As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents lbltsk As System.Web.UI.WebControls.Label
    Protected WithEvents ImageButton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ImageButton2 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtprob As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcorr As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents lblfail As System.Web.UI.WebControls.Label
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmfid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfailid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        coi = mu.COMPI
        lblcoi.Value = coi
	GetFSOVLIBS()



	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            cid = "0"
            lblcid.Value = cid
            'ro = Request.QueryString("ro").ToString
            Try
                dir = HttpContext.Current.Session("dir").ToString
                lbldir.Value = dir
            Catch ex As Exception
                dir = "0"
                lbldir.Value = "0"
            End Try
            If dir <> "0" Then
                iwoadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                iwoadd.Attributes.Add("onclick", "")
                ImageButton1.Attributes.Add("src", "../images/appbuttons/bgbuttons/savedis.gif")
                ImageButton1.Enabled = False
                tdmsg.InnerHtml = "Cannot Save Record or Generate Work Order in TPM Performer Demo Mode"
            Else
                iwoadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addwhite.gif")
                iwoadd.Attributes.Add("onclick", "getrt();")
                ImageButton1.Attributes.Add("src", "../images/appbuttons/bgbuttons/save.gif")
                ImageButton1.Enabled = True
            End If
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            pmid = Request.QueryString("pmid").ToString
            pmhid = Request.QueryString("pmhid").ToString
            pmtid = Request.QueryString("pmtid").ToString
            fm = Request.QueryString("fm").ToString
            fmid = Request.QueryString("fmid").ToString
            task = Request.QueryString("task").ToString
            pmfid = Request.QueryString("pmfid").ToString
            comid = Request.QueryString("comid").ToString
            sid = Request.QueryString("sid").ToString
            lblpmid.Value = pmid
            lblpmhid.Value = pmhid
            lblsid.Value = sid
            lblpmfid.Value = pmfid
            lblfm.Value = fm
            lblpmtid.Value = pmtid
            lblfmid.Value = fmid
            lblfail.Text = fm
            lbltask.Value = task
            lbltsk.Text = task
            lblcomid.Value = comid
            pmf.Open()
            GetLists()
            'If pmfid <> "" Then
            GetFail(pmtid, fmid)
            'End If
            pmf.Dispose()
            ImageButton1.Attributes.Add("class", "details")
            ImageButton2.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov64", "PMFailMan.aspx.vb") & "', ABOVE, LEFT)")
            ImageButton2.Attributes.Add("onmouseout", "return nd()")
        Else
            If Request.Form("lblsubmit") = "gencorr" Then
                lblsubmit.Value = ""
                pmf.Open()
                GenCorr()
                pmf.Dispose()
            ElseIf Request.Form("lblsubmit") = "can" Then
                lblsubmit.Value = ""
                pmf.Open()
                CanCorr()
                pmf.Dispose()
            End If
        End If
    End Sub
    Private Sub GetLists()
        cid = lblcid.Value
        sql = "select skillid, skill " _
        + "from pmSkills where compid = '" & cid & "'"
        dr = pmf.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("Select"))
        ddskill.Items(0).Value = 0
    End Sub
    Private Sub CanCorr()
        Dim wonum As String = lblwo.Value
        Dim stat As String
        sql = "select status from workorder where wonum = '" & wonum & "'"
        stat = pmf.Scalar(sql)
        If stat = "WAPPR" Then
            sql = "update workorder set status = 'CAN' where wonum = '" & wonum & "'"
            pmf.Update(sql)
            Dim strMessage As String = tmod.getmsg("cdstr372", "PMFailMantpm.aspx.vb") & " " & wonum & " is Cancelled"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Dim strMessage As String = tmod.getmsg("cdstr373", "PMFailMantpm.aspx.vb") & " " & wonum & " was not Cancelled\As it has already been Approved\or has been changed to some other Status"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Private Sub btnwo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnwo.Click
        pmf.Open()
        GenCorr()
        pmf.Dispose()
    End Sub
    Private Sub GenCorr()
        SaveChanges()
        pmfid = lblpmfid.Value
        pmid = lblpmid.Value
        Dim eqid, eqnum, sid, deptid, cellid, locid, location, desc, funcid, comid, chrg As String
        sql = "select p.eqid, e.* from tpm p left join equipment e on e.eqid = p.eqid where pmid = '" & pmid & "'"
        dr = pmf.GetRdrData(sql)
        While dr.Read
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            sid = dr.Item("siteid").ToString
            deptid = dr.Item("dept_id").ToString
            cellid = dr.Item("cellid").ToString
            locid = dr.Item("locid").ToString
            chrg = dr.Item("chargenum").ToString
        End While
        dr.Close()
        Dim wonum As Integer
        Dim superid, super, leadid, lead, se, le, start, skillid, skill As String
        superid = lblsup.Value
        super = txtsup.Text
        leadid = lbllead.Value
        lead = txtlead.Text
        If cbsupe.Checked = True Then
            se = "1"
        Else
            se = "0"
        End If
        If cbleade.Checked = True Then
            le = "1"
        Else
            le = "0"
        End If
        start = txtstart.Text
        If start = "" Then
            start = "NULL"
        Else
            start = "'" & start & "'"
        End If
        skillid = ddskill.SelectedValue.ToString
        skill = ddskill.SelectedItem.ToString
        Dim wt As String
        Dim wtc As Integer = 0
        coi = lblcoi.Value
        If coi = "NISS" Or coi = "NISS_SYM" Then
            sql = "select count(*) from wotype where wotype = 'PM4'"
            Try
                wtc = pmf.Scalar(sql)
            Catch ex As Exception
                wtc = 0
            End Try
            If wtc > 0 Then
                wt = "PM4"
            Else
                wt = "CM"
            End If
        Else
            wt = "CM"
        End If
        Dim usr As String = HttpContext.Current.Session("username").ToString
        comid = lblcomid.Value
        sql = "select func_id from components where comid = '" & comid & "'"
        funcid = pmf.Scalar(sql)
        sql = "insert into workorder (status, statusdate, changeby, changedate, reportedby, reportdate, worktype, pmfid, eqid, " _
        + "eqnum, siteid, deptid, cellid, locid, funcid, comid, chargenum, targstartdate, leadcraftid, leadcraft, leadealert, " _
        + "superid, supervisor, supealert, skillid, skill) " _
      + "values ('WAPPR', getDate(),'" & usr & "', getDate(),'" & usr & "', getDate(), '" & wt & "' ,'" & pmfid & "','" & eqid & "', " _
      + "'" & eqnum & "','" & sid & "','" & deptid & "','" & cellid & "','" & locid & "','" & funcid & "','" & comid & "','" & chrg & "', " _
      + "" & start & ",'" & leadid & "','" & lead & "','" & le & "','" & superid & "','" & super & "','" & se & "', " _
      + "'" & skillid & "','" & skill & "') " _
      + "select @@identity"
        wonum = pmf.Scalar(sql)
        lblwonum.Text = wonum
        lblwo.Value = wonum
        sql = "update pmfailtpm set wonum = '" & wonum & "' where pmfid = '" & pmfid & "'"
        pmf.Update(sql)
        'Insert Failure Mode to wofail - use sp
        fmid = lblfmid.Value
        fm = lblfm.Value
        sql = "usp_addWoFailureMode " & wonum & ", " & fmid & ", '" & fm & "', '" & comid & "','hascomp'"
        pmf.Update(sql)
        sql = "insert into wocorr (wctype, wonum, problem, corraction) values ('fail','" & wonum & "','" & txtprob.Text & "','" & txtcorr.Text & "')"
        pmf.Update(sql)
        desc = "Corrective Action for Equipment# " & eqnum & " - " & txtcorr.Text
        SaveDesc(desc)
        Dim mail As New pmmail
        If cbsupe.Checked = True Then
            'SendIt("sup", superid, wonum, desc, start)
            Dim mail1 As New pmmail
            mail1.CheckIt("sup", superid, wonum)
        End If
        If cbleade.Checked = True Then
            'SendIt("lead", leadid, wonum, desc, start)
            Dim mail2 As New pmmail
            mail2.CheckIt("lead", leadid, wonum)
        End If
    End Sub
    Private Sub SendIt(ByVal typ As String, ByVal uid As String, ByVal wonum As String, ByVal desc As String, ByVal start As String)
        Dim email As New System.Web.Mail.MailMessage

        Dim lid, pas, ema As String
        lid = "test"
        pas = "test"

        email.To = "chuck.rennie@adelphia.net"
        email.From = "system_admin@laisoftware.com"
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 1
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "mail.laisoftware.com"
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = "system_admin"
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = "sysadm1"
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
        email.Fields.Item("http://schemas.Microsoft.com/cdo/configuration/smtpserverport") = 25

        Dim intralog As New mmenu_utils_a
        Dim isintra As Integer = intralog.INTRA
        If isintra <> 1 Then
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\Inetpub\mailroot\pickup"
        End If

        email.Body = AlertBody(wonum, desc, start)
        email.Subject = "New Work Order#  " + wonum
        email.BodyFormat = Web.Mail.MailFormat.Html
        System.Web.Mail.SmtpMail.SmtpServer.Insert(0, "mail.laisoftware.com")
        System.Web.Mail.SmtpMail.Send(email)
    End Sub
    Private Function AlertBody(ByVal wo As String, ByVal desc As String, ByVal start As String) As String
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrle")
        Dim appname As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim body As String
        body = "<table width='800px' style='font-size:8pt; font-family:Verdana;'>"

        body &= "<tr><td>" & tmod.getlbl("cdlbl107" , "PMFailMantpm.aspx.vb") & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Work Order #  " & wo & "<br><br></td></tr>" & vbCrLf
        body &= "<tr><td>Description:  " & desc & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Target Start: " & start & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>" & tmod.getlbl("cdlbl108" , "PMFailMantpm.aspx.vb") & "<a href='" + urlname + appname + "/appswo/woprint.aspx?typ=wo&wo=" + wo + "'>Work Order# " + wo + "</td></tr>" & vbCrLf & vbCrLf
        body &= "</table>"

        Return body
    End Function
    Private Sub SaveDesc(ByVal lg As String)
        Dim wonum As String = lblwo.Value
        Dim sh As String
        Dim lgcnt As Integer
        Dim test As String = lg
        If Len(lg) > 79 Then
            sh = Mid(lg, 1, 79)
            sql = "update workorder set description = '" & sh & "' where wonum = '" & wonum & "'"
            pmf.Update(sql)
            lg = Mid(lg, 80)
            sql = "select count(*) from wolongdesc where wonum = '" & wonum & "'"
            lgcnt = pmf.Scalar(sql)
            If lgcnt <> 0 Then
                sql = "update wolongdesc set longdesc = '" & lg & "' where wonum = '" & wonum & "'"
                pmf.Update(sql)
            Else
                sql = "insert into wolongdesc (wonum, longdesc) values ('" & wonum & "','" & lg & "')"
                pmf.Update(sql)
            End If
        Else
            sql = "update workorder set description = '" & lg & "' where wonum = '" & wonum & "'" _
            + "delete from wolongdesc where wonum = '" & wonum & "'"
            pmf.Update(sql)
        End If

    End Sub
    Private Sub GetFail(ByVal pmtid As String, ByVal fmid As String)
        Dim wochk, lchk As String
        sql = "select * from pmfailtpm where pmtid = '" & pmtid & "' and failid = '" & fmid & "'"
        dr = pmf.GetRdrData(sql)
        While dr.Read
            txtprob.Text = dr.Item("problem").ToString
            txtcorr.Text = dr.Item("corraction").ToString
            lblwo.Value = dr.Item("wonum").ToString
            lblwonum.Text = dr.Item("wonum").ToString
            'lblcomid.Value = dr.Item("comid").ToString
            'lchk = dr.Item("saveforgen").ToString
        End While
        dr.Close()
        If lblwo.Value <> "" Then
            cbcwo.Checked = True
            iwoadd.Attributes.Add("class", "details")
            imgi2.Attributes.Add("class", "view")
            GetWo(lblwo.Value)
        Else
            iwoadd.Attributes.Add("class", "view")
            imgi2.Attributes.Add("class", "details")
        End If
        'If lchk <> "0" Then
        'cbeqr.Checked = True
        'End If

    End Sub
    Private Sub GetWo(ByVal wo As String)
        sql = "select skillid, targstartdate, superid, supervisor, supealert, leadcraftid, leadcraft, leadealert from workorder where wonum = '" & wo & "'"
        Dim skillid, se, le As String
        dr = pmf.GetRdrData(sql)
        While dr.Read
            skillid = dr.Item("skillid").ToString
            If skillid <> "" Then
                Try
                    ddskill.SelectedValue = skillid
                Catch ex As Exception

                End Try
                txtstart.Text = dr.Item("targstartdate").ToString
                txtsup.Text = dr.Item("supervisor").ToString
                txtlead.Text = dr.Item("leadcraft").ToString
                lblsup.Value = dr.Item("superid").ToString
                lbllead.Value = dr.Item("leadcraftid").ToString
                le = dr.Item("leadealert").ToString
                se = dr.Item("supealert").ToString
                If le = "1" Then
                    cbleade.Checked = True
                End If
                If se = "1" Then
                    cbsupe.Checked = True
                End If
            End If
        End While
        dr.Close()
    End Sub
    Private Sub SaveChanges()
        pmfid = lblpmfid.Value
        Dim prob, corr, cmid, cmfid, compnum As String
        prob = txtprob.Text
        prob = pmf.ModString2(prob)
        corr = txtcorr.Text
        corr = pmf.ModString2(corr)

        If pmfid <> "" Then
            sql = "update pmfailtpm set problem = '" & prob & "', corraction = '" & corr & "', faildate = getDate() where pmfid= '" & pmfid & "'"
            pmf.Update(sql)
        Else
            Dim ret As Integer
            pmid = lblpmid.Value
            pmhid = lblpmhid.Value
            pmtid = lblpmtid.Value
            Dim fm, fmid As String
            fm = lblfm.Value
            fmid = lblfmid.Value
            cmid = lblcomid.Value
            sql = "select compfailid from componentfailmodes where comid = '" & comid & "' and failid = '" & fmid & "'"
            dr = pmf.GetRdrData(sql)
            While dr.Read
                cmfid = dr.Item("compfailid").ToString
            End While
            dr.Close()
            sql = "select compnum from components where comid = '" & comid & "'"
            dr = pmf.GetRdrData(sql)
            While dr.Read
                compnum = dr.Item("compnum").ToString
            End While
            dr.Close()
            sql = "insert into pmfailtpm (pmid, pmtid, pmhid, problem, corraction, faildate, failid, failuremode, comid, compfailid, compnum) values " _
            + "('" & pmid & "', '" & pmtid & "', '" & pmhid & "', '" & prob & "', '" & corr & "', getDate(), " _
            + "'" & fmid & "','" & fm & "','" & cmid & "','" & cmfid & "','" & compnum & "') select @@identity as 'identity'"
            ret = pmf.Scalar(sql)
            lblpmfid.Value = ret

        End If


    End Sub
    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        pmf.Open()
        SaveChanges()
        pmtid = lblpmtid.Value
        fmid = lblfmid.Value
        GetFail(pmtid, fmid)
        pmf.Dispose()
    End Sub


	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label1.Text = axlabs.GetASPXPage("PMFailMantpm.aspx", "Label1")
        Catch ex As Exception
        End Try
        Try
            Label2.Text = axlabs.GetASPXPage("PMFailMantpm.aspx", "Label2")
        Catch ex As Exception
        End Try
        Try
            Label3.Text = axlabs.GetASPXPage("PMFailMantpm.aspx", "Label3")
        Catch ex As Exception
        End Try
        Try
            lang821.Text = axlabs.GetASPXPage("PMFailMantpm.aspx", "lang821")
        Catch ex As Exception
        End Try
        Try
            lang822.Text = axlabs.GetASPXPage("PMFailMantpm.aspx", "lang822")
        Catch ex As Exception
        End Try
        Try
            lang823.Text = axlabs.GetASPXPage("PMFailMantpm.aspx", "lang823")
        Catch ex As Exception
        End Try
        Try
            lang824.Text = axlabs.GetASPXPage("PMFailMantpm.aspx", "lang824")
        Catch ex As Exception
        End Try
        Try
            lang825.Text = axlabs.GetASPXPage("PMFailMantpm.aspx", "lang825")
        Catch ex As Exception
        End Try
        Try
            lang826.Text = axlabs.GetASPXPage("PMFailMantpm.aspx", "lang826")
        Catch ex As Exception
        End Try
        Try
            lang827.Text = axlabs.GetASPXPage("PMFailMantpm.aspx", "lang827")
        Catch ex As Exception
        End Try
        Try
            lang828.Text = axlabs.GetASPXPage("PMFailMantpm.aspx", "lang828")
        Catch ex As Exception
        End Try
        Try
            lang829.Text = axlabs.GetASPXPage("PMFailMantpm.aspx", "lang829")
        Catch ex As Exception
        End Try
        Try
            lang830.Text = axlabs.GetASPXPage("PMFailMantpm.aspx", "lang830")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnwo.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                btnwo.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                btnwo.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                btnwo.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "spa" Then
                btnwo.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                ImageButton1.Attributes.Add("src", "../images2/eng/bgbuttons/save.gif")
            ElseIf lang = "fre" Then
                ImageButton1.Attributes.Add("src", "../images2/fre/bgbuttons/save.gif")
            ElseIf lang = "ger" Then
                ImageButton1.Attributes.Add("src", "../images2/ger/bgbuttons/save.gif")
            ElseIf lang = "ita" Then
                ImageButton1.Attributes.Add("src", "../images2/ita/bgbuttons/save.gif")
            ElseIf lang = "spa" Then
                ImageButton1.Attributes.Add("src", "../images2/spa/bgbuttons/save.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            Img2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMFailMantpm.aspx", "Img2") & "', ABOVE, LEFT)")
            Img2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgi2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMFailMantpm.aspx", "imgi2") & "', ABOVE, LEFT)")
            imgi2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            iwoadd.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMFailMantpm.aspx", "iwoadd") & "')")
            iwoadd.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid100.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMFailMantpm.aspx", "ovid100") & "')")
            ovid100.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid101.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMFailMantpm.aspx", "ovid101") & "')")
            ovid101.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        pmf.Open()
        SaveChanges()
        pmtid = lblpmtid.Value
        fmid = lblfmid.Value
        GetFail(pmtid, fmid)
        pmf.Dispose()
        lblsave.Value = "yes"
    End Sub
End Class
