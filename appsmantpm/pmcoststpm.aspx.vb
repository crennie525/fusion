

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class pmcoststpm
    Inherits System.Web.UI.Page
	Protected WithEvents lang801 As System.Web.UI.WebControls.Label

	Protected WithEvents lang800 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim comp As New Utilities
    Dim ap As New AppUtils
    Dim sql As String
    Dim dr As SqlDataReader
    Dim wonum, jpid, stat, icost, pmid, Login As String
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwod As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Tr1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdcosts As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnotes As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            wonum = Request.QueryString("wo").ToString '"1216" '
            lblwo.Value = wonum
            If wonum <> "" Then
                comp.Open()
                GetWoHead(wonum)
                GetCosts()
                comp.Dispose()
            End If

        End If
    End Sub

    Private Sub GetWoHead(ByVal wonum As String)
        'wonum = lblwo.Value
        sql = "select w.wonum, w.description, w.jpid, w.tpmid, w.status, j.jpnum, j.description as jdesc " _
        + "from workorder w left join wojobplans j on j.jpid = w.jpid  where w.wonum = '" & wonum & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdwo.InnerHtml = dr.Item("wonum").ToString
            tdwod.InnerHtml = dr.Item("description").ToString
            lbljpid.Value = dr.Item("jpid").ToString
            jpid = dr.Item("jpid").ToString
            stat = dr.Item("status").ToString
            pmid = dr.Item("tpmid").ToString
            'tdjpd.InnerHtml = dr.Item("jdesc").ToString
        End While
        dr.Close()
        lbljpid.Value = jpid
        lblstat.Value = stat
        lblpmid.Value = pmid
    End Sub
    Private Sub GetCosts()
        wonum = lblwo.Value
        jpid = lbljpid.Value
        icost = ap.InvEntry()
        Dim sb As New StringBuilder
        sb.Append("<table cellspacing=""0"" width=""750"" border=""0"">")
        sb.Append("<tr height=""20""><td colspan=""11"" class=""bluelabel"" style=""border-bottom: black 1px solid;"">" & tmod.getlbl("cdlbl90" , "pmcoststpm.aspx.vb") & "</td>")
        sb.Append("<tr height=""20"">")
        sb.Append("<td width=""200px""></td>" & vbCrLf)
        sb.Append("<td width=""25px"">&nbsp;</td>")
        sb.Append("<td class=""label"" width=""80px"" align=""center""><u>Estimated</u></td>" & vbCrLf)
        sb.Append("<td width=""25px"">&nbsp;</td>")
        sb.Append("<td class=""label"" width=""80px"" align=""center""><u>Actual (Reported)</u></td>" & vbCrLf)
        sb.Append("<td width=""25px"">&nbsp;</td>")
        sb.Append("<td class=""label"" width=""80px"" align=""center""><u>Actual (Optimize)</u></td>" & vbCrLf)
        sb.Append("<td width=""25px"">&nbsp;</td>")
        sb.Append("<td class=""label"" width=""80px"" align=""center""><u>Savings (Reported)</u></td>" & vbCrLf)
        sb.Append("<td width=""25px"">&nbsp;</td>")
        sb.Append("<td class=""label"" width=""80px"" align=""center""><u>Savings (Optimize)</u></td></tr>" & vbCrLf)
        'sb.Append("<td class=""label"" width=""120px"" align=""center""><u>Total</u></td></tr>" & vbCrLf)

        'sql = "select estlabhrs, estlabcost, estmatcost, esttoolcost, estlubecost, " _
        '+ "actlabhrs, actlabcost, actmatcost, acttoolcost, actlubecost, " _
        '+ "totlabhrs, totlabcost, totmatcost, tottoolcost, totlubecost " _
        '+ "from workorder where wonum = '" & wonum & "'"

        sql = "select distinct w.estlabhrs, w.estlabcost, w.estmatcost, w.esttoolcost, w.estlubecost, isnull(w.actlabhrs,0) as actlabhrs, " _
        + "isnull(w.actlabcost,0) as actlabcost, " _
        + "isnull(w.actmatcost,0) as actmatcost, isnull(w.acttoolcost,0) as acttoolcost, isnull(w.actlubecost,0) as actlubecost, " _
        + "isnull(sum(p.actlabcost),0) as pact, isnull(sum(p.acttime / 60),0) as patime " _
        + "from workorder w " _
        + "left join pmtracktpm p on p.pmid = w.pmid " _
        + "where w.wonum = '" & wonum & "' " _
        + "group by w.estlabhrs, w.estlabcost, w.estmatcost, w.esttoolcost, w.estlubecost, " _
        + "w.actlabhrs, w.actlabcost, w.actmatcost, w.acttoolcost, w.actlubecost"

        dr = comp.GetRdrData(sql)
        Dim sqty As Integer
        Dim dhr, sve, sva, dte, dta, svt, svte, svta, svle, svla, svoe, svoa, dhr1, dte1, dta1, sva1 As Decimal
        dte = 0
        dta = 0
        dte1 = 0
        svt = 0
        Dim esthr, estjphr, acthr, actjphr As Decimal
        Dim estcost, estjpcost, actcost, actjpcost As Decimal
        Dim estmatcost, estjpmatcost, actmatcost, actjpmatcost As Decimal
        Dim esttoolcost, estjptoolcost, acttoolcost, actjptoolcost As Decimal
        Dim estlubecost, estjplubecost, actlubecost, actjplubecost As Decimal
        Dim pact, patime As Decimal
        Dim sclass, aster As String
        If icost = "inv" Or icost = "ext" Then
            aster = "*"
        Else
            aster = ""
        End If
        While dr.Read
            Try
                sqty = dr.Item("qty").ToString
            Catch ex As Exception
                sqty = 1
            End Try
            'estjphr = dr.Item("estjplabhrs").ToString
            esthr = dr.Item("estlabhrs").ToString
            acthr = dr.Item("actlabhrs").ToString
            pact = dr.Item("pact").ToString
            patime = dr.Item("patime").ToString
            'actjphr = dr.Item("actjplabhrs").ToString
            sb.Append("<tr height=""20"" class=""transrowlime"">")

            sb.Append("<tr height=""20"" class=""transrowlime"">")
            sb.Append("<td class=""greenlabel transrowlime"">" & tmod.getlbl("cdlbl91", "pmcoststpm.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowlime""></td>")
            dhr = dr.Item("estlabhrs").ToString
            dhr = sqty * dhr
            dte += dhr
            sb.Append("<td class=""greenlabel transrowlime"" align=""center"">" & Format(dhr, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowlime""></td>")
            dhr = dr.Item("actlabhrs").ToString
            dta += dhr
            sb.Append("<td class=""greenlabel transrowlime"" align=""center"">" & Format(dhr, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowlime""></td>")
            dhr = dr.Item("patime").ToString
            dta1 += dhr
            sb.Append("<td class=""greenlabel transrowlime"" align=""center"">" & Format(dhr, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowlime""></td>")
            'If dta <> 0 Then
            dhr = dte - dta
            dhr1 = dte - dta1
            'Else
            'dhr = 0
            'End If
            svt = 0
            dte = 0
            dta = 0
            dte1 = 0
            dta1 = 0
            'svt += dhr
            If dhr < 0 Then
                sclass = "redlabel"
            Else
                sclass = "greenlabel"
            End If
            sb.Append("<td class=""" & sclass & " transrowlime"" align=""center"">" & Format(dhr, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""" & sclass & " transrowlime""></td>")
            sb.Append("<td class=""" & sclass & " transrowlime"" align=""center"">" & Format(dhr1, "##############0.00") & "</td></tr>" & vbCrLf)

            sb.Append("<tr><td colspan=""5""><img src=""../images/appbuttons/minibuttons/6PX.gif""></td></tr>" & vbCrLf)
            estcost = dr.Item("estlabcost").ToString
            actcost = dr.Item("actlabcost").ToString
            'estjpcost = dr.Item("estjplabcost").ToString
            'actjpcost = dr.Item("actjplabcost").ToString


            sb.Append("<tr height=""20"" class=""transrowblue"">")
            sb.Append("<td class=""bluelabel transrowblue"">" & tmod.getlbl("cdlbl92", "pmcoststpm.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowblue""></td>")
            dhr = dr.Item("estlabcost").ToString
            dhr = sqty * dhr
            dte += dhr
            sb.Append("<td class=""bluelabel transrowblue"" align=""center"">$" & Format(dhr, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowblue""></td>")
            dhr = dr.Item("actlabcost").ToString
            dta += dhr
            sb.Append("<td class=""bluelabel transrowblue"" align=""center"">$" & Format(dhr, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowblue""></td>")
            dhr1 = dr.Item("pact").ToString
            dta1 += dhr1
            sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr1, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowblue""></td>")
            dhr = sve + sva
            svt += dhr
            'If dta <> 0 Then
            dhr = dte - dta
            dhr1 = dte - dta1
            'Else
            'dhr = 0
            'End If
            If dhr < 0 Then
                sclass = "redlabel"
            Else
                sclass = "bluelabel"
            End If
            sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""" & sclass & " transrowblue""></td>")
            sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"">" & Format(dhr1, "$##############0.00") & "</td></tr>" & vbCrLf)


            '*******************Materials********************************

            sb.Append("<tr><td colspan=""5""><img src=""../images/appbuttons/minibuttons/6PX.gif""></td></tr>" & vbCrLf)
            estmatcost = dr.Item("estmatcost").ToString
            actmatcost = dr.Item("actmatcost").ToString
            'estjpmatcost = dr.Item("estjpmatcost").ToString
            'actjpmatcost = dr.Item("actjpmatcost").ToString



            sb.Append("<tr height=""20""  class=""bluelabel transrowblue"">")
            sb.Append("<td class=""bluelabel transrowblue"">" & tmod.getlbl("cdlbl93", "pmcoststpm.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowblue""></td>")
            dhr = dr.Item("estmatcost").ToString
            dte += dhr
            sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowblue""></td>")
            If icost = "use" Then
                dhr = dr.Item("actmatcost").ToString
            Else
                dhr = actmatcost
            End If
            sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowblue""></td>")
            If icost = "use" Then
                dhr = dr.Item("actmatcost").ToString
            Else
                dhr = actmatcost
            End If
            sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowblue""></td>")
            dta += dhr
            dta1 += dhr
            dhr = sve + sva
            svt += dhr
            'If dta <> 0 Then
            If icost <> "use" Then
                dhr = (estmatcost) - (actmatcost) 'dte + dta
            Else
                dhr = (estmatcost) - (actmatcost)
            End If
            'Else
            'dhr = 0
            'End If
            If dhr < 0 Then
                sclass = "redlabel"
            Else
                sclass = "bluelabel"
            End If
            sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""" & sclass & " transrowblue""></td>")
            sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td></tr>" & vbCrLf)

            sb.Append("<tr><td colspan=""5""><img src=""../images/appbuttons/minibuttons/6PX.gif""></td></tr>" & vbCrLf)
            esttoolcost = dr.Item("esttoolcost").ToString
            acttoolcost = dr.Item("acttoolcost").ToString
            'estjptoolcost = dr.Item("estjptoolcost").ToString
            'actjptoolcost = dr.Item("actjptoolcost").ToString

            sb.Append("<tr height=""20""  class=""transrowblue"">")
            sb.Append("<td class=""bluelabel transrowblue"">" & tmod.getlbl("cdlbl94", "pmcoststpm.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowblue""></td>")
            dhr = dr.Item("esttoolcost").ToString
            dte += dhr
            sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowblue""></td>")
            dhr = dr.Item("acttoolcost").ToString
            dta += dhr
            dta1 += dhr
            sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowblue""></td>")
            dhr = dr.Item("acttoolcost").ToString
            'dta += dhr
            sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowblue""></td>")

            dhr = svte + svta
            svt += dhr
            'If dta <> 0 Then
            If icost = "use" Then
                dhr = (esttoolcost) - (acttoolcost) 'dte + dta
            Else
                dhr = (esttoolcost) - (acttoolcost)
            End If

            'Else
            'dhr = 0
            'End If
            If dhr < 0 Then
                sclass = "redlabel"
            Else
                sclass = "bluelabel"
            End If
            sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""" & sclass & " transrowblue""></td>")
            sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td></tr>" & vbCrLf)

            sb.Append("<tr><td colspan=""5""><img src=""../images/appbuttons/minibuttons/6PX.gif""></td></tr>" & vbCrLf)
            estlubecost = dr.Item("estlubecost").ToString
            actlubecost = dr.Item("actlubecost").ToString
            'estjplubecost = dr.Item("estjplubecost").ToString
            'actjplubecost = dr.Item("actjplubecost").ToString


            sb.Append("<tr height=""20""  class=""transrowblue"">")
            sb.Append("<td class=""bluelabel transrowblue"">" & tmod.getlbl("cdlbl95", "pmcoststpm.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowblue""></td>")
            dhr = dr.Item("estlubecost").ToString
            dte += dhr
            sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowblue""></td>")
            dhr = dr.Item("actlubecost").ToString
            dta += dhr
            dta1 += dhr
            sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowblue""></td>")
            sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowblue""></td>")
            dhr = sve + sva
            svt += dhr
            'If dta <> 0 Then
            If icost = "use" Then
                dhr = (estlubecost) - (actjplubecost) 'dte + dta
            Else
                dhr = (estlubecost) - (actlubecost)
            End If

            'Else
            'dhr = 0
            'End If
            If dhr < 0 Then
                sclass = "redlabel"
            Else
                sclass = "bluelabel"
            End If
            sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""" & sclass & " transrowblue""></td>")
            sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td></tr>" & vbCrLf)

            sb.Append("<tr><td colspan=""5""><img src=""../images/appbuttons/minibuttons/6PX.gif""></td></tr>" & vbCrLf)

            '***************************************totals********************************************

            sb.Append("<tr height=""20""  >")
            sb.Append("<td class=""label transrowblue"">" & tmod.getlbl("cdlbl96", "pmcoststpm.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""label transrowblue""></td>")
            sb.Append("<td align=""center"" class=""label transrowblue"">" & Format(dte, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""label transrowblue""></td>")
            sb.Append("<td align=""center"" class=""label transrowblue"">" & Format(dta1, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""label transrowblue""></td>")
            sb.Append("<td align=""center"" class=""label transrowblue"">" & Format(dta, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""label transrowblue""></td>")
            'If dta = 0 Then
            'svt = 0
            'Else
            svt = dte - dta
            sva1 = dte - dta1
            'End If
            If svt < 0 Then
                sclass = "redlabel"
            Else
                sclass = "label"
            End If
            sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"" bgcolor=""#BFDDFB"">" & Format(svt, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""" & sclass & " transrowblue""></td>")
            sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"" bgcolor=""#BFDDFB"">" & Format(sva1, "$##############0.00") & "</td></tr>" & vbCrLf)

        End While
        dr.Close()
        sb.Append("<td>&nbsp;</td>")
        sb.Append("</table>")

        sb.Append("<table cellspacing=""0"" width=""750"" border=""0"">")
        sb.Append("<tr height=""20""><td colspan=""11"" class=""bluelabel"" style=""border-bottom: black 1px solid;"">" & tmod.getlbl("cdlbl97" , "pmcoststpm.aspx.vb") & "</td>")
        sb.Append("<tr height=""20"">")
        sb.Append("<td width=""200px""></td>" & vbCrLf)
        sb.Append("<td width=""25px"">&nbsp;</td>")
        sb.Append("<td class=""label"" width=""80px"" align=""center""><u>Estimated</u></td>" & vbCrLf)
        sb.Append("<td width=""25px"">&nbsp;</td>")
        sb.Append("<td class=""label"" width=""80px"" align=""center""><u>Actual (Reported)</u></td>" & vbCrLf)
        sb.Append("<td width=""25px"">&nbsp;</td>")
        sb.Append("<td class=""label"" width=""80px"" align=""center""><u>Actual (Optimize)</u></td>" & vbCrLf)
        sb.Append("<td width=""25px"">&nbsp;</td>")
        sb.Append("<td class=""label"" width=""80px"" align=""center""><u>Savings (Reported)</u></td>" & vbCrLf)
        sb.Append("<td width=""25px"">&nbsp;</td>")
        sb.Append("<td class=""label"" width=""80px"" align=""center""><u>Savings (Optimize)</u></td></tr>" & vbCrLf)
        'sb.Append("</table>")

        Dim oldwo As String
        pmid = lblpmid.Value
        sql = "select max(wonum) from workorder where tpmid = '" & pmid & "' and status = 'COMP'"
        Try
            oldwo = comp.strScalar(sql)
        Catch ex As Exception
            oldwo = ""
        End Try

        If oldwo <> "" Then
            sql = "select distinct w.estlabhrs, w.estlabcost, w.estmatcost, w.esttoolcost, w.estlubecost, isnull(w.actlabhrs,0) as actlabhrs, " _
                   + "isnull(w.actlabcost,0) as actlabcost, " _
                   + "isnull(w.actmatcost,0) as actmatcost, isnull(w.acttoolcost,0) as acttoolcost, isnull(w.actlubecost,0) as actlubecost, " _
                   + "isnull(sum(p.actlabcost),0) as pact, isnull(sum(p.acttime / 60),0) as patime " _
                   + "from workorder w " _
                   + "left join pmtracktpm p on p.pmid = w.pmid " _
                   + "where w.wonum = '" & oldwo & "' " _
                   + "group by w.estlabhrs, w.estlabcost, w.estmatcost, w.esttoolcost, w.estlubecost, " _
                   + "w.actlabhrs, w.actlabcost, w.actmatcost, w.acttoolcost, w.actlubecost"

            dr = comp.GetRdrData(sql)
            dte = 0
            dta = 0
            dte = 0
            dta = 0
            dta1 = 0
            While dr.Read
                'estjphr = dr.Item("estjplabhrs").ToString
                esthr = dr.Item("estlabhrs").ToString
                acthr = dr.Item("actlabhrs").ToString
                pact = dr.Item("pact").ToString
                patime = dr.Item("patime").ToString
                'actjphr = dr.Item("actjplabhrs").ToString
                sb.Append("<tr height=""20"" class=""transrowlime"">")

                sb.Append("<tr height=""20"" class=""transrowlime"">")
                sb.Append("<td class=""greenlabel transrowlime"">" & tmod.getlbl("cdlbl98" , "pmcoststpm.aspx.vb") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowlime""></td>")
                dhr = dr.Item("estlabhrs").ToString
                dte += dhr
                sb.Append("<td class=""greenlabel transrowlime"" align=""center"">" & Format(dhr, "##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowlime""></td>")
                dhr = dr.Item("actlabhrs").ToString
                dta += dhr
                sb.Append("<td class=""greenlabel transrowlime"" align=""center"">" & Format(dhr, "##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowlime""></td>")
                dhr = dr.Item("patime").ToString
                dta1 += dhr
                sb.Append("<td class=""greenlabel transrowlime"" align=""center"">" & Format(dhr, "##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowlime""></td>")
                'If dta <> 0 Then
                dhr = dte - dta
                dhr1 = dte - dta1
                'Else
                'dhr = 0
                'End If
                svt = 0
                dte = 0
                dta = 0
                dte1 = 0
                dta1 = 0
                'svt += dhr
                If dhr < 0 Then
                    sclass = "redlabel"
                Else
                    sclass = "greenlabel"
                End If
                sb.Append("<td class=""" & sclass & " transrowlime"" align=""center"">" & Format(dhr, "##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""" & sclass & " transrowlime""></td>")
                sb.Append("<td class=""" & sclass & " transrowlime"" align=""center"">" & Format(dhr1, "##############0.00") & "</td></tr>" & vbCrLf)

                sb.Append("<tr><td colspan=""5""><img src=""../images/appbuttons/minibuttons/6PX.gif""></td></tr>" & vbCrLf)
                estcost = dr.Item("estlabcost").ToString
                actcost = dr.Item("actlabcost").ToString
                'estjpcost = dr.Item("estjplabcost").ToString
                'actjpcost = dr.Item("actjplabcost").ToString


                sb.Append("<tr height=""20"" class=""transrowblue"">")
                sb.Append("<td class=""bluelabel transrowblue"">" & tmod.getlbl("cdlbl99" , "pmcoststpm.aspx.vb") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowblue""></td>")
                dhr = dr.Item("estlabcost").ToString
                dte += dhr
                sb.Append("<td class=""bluelabel transrowblue"" align=""center"">$" & Format(dhr, "##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowblue""></td>")
                dhr = dr.Item("actlabcost").ToString
                dta += dhr
                sb.Append("<td class=""bluelabel transrowblue"" align=""center"">$" & Format(dhr, "##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowblue""></td>")
                dhr1 = dr.Item("pact").ToString
                dta1 += dhr1
                sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr1, "##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowblue""></td>")
                dhr = sve + sva
                svt += dhr
                'If dta <> 0 Then
                dhr = dte - dta
                dhr1 = dte - dta1
                'Else
                'dhr = 0
                'End If
                If dhr < 0 Then
                    sclass = "redlabel"
                Else
                    sclass = "bluelabel"
                End If
                sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""" & sclass & " transrowblue""></td>")
                sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"">" & Format(dhr1, "$##############0.00") & "</td></tr>" & vbCrLf)


                '*******************Materials********************************

                sb.Append("<tr><td colspan=""5""><img src=""../images/appbuttons/minibuttons/6PX.gif""></td></tr>" & vbCrLf)
                estmatcost = dr.Item("estmatcost").ToString
                actmatcost = dr.Item("actmatcost").ToString
                'estjpmatcost = dr.Item("estjpmatcost").ToString
                'actjpmatcost = dr.Item("actjpmatcost").ToString



                sb.Append("<tr height=""20""  class=""bluelabel transrowblue"">")
                sb.Append("<td class=""bluelabel transrowblue"">" & tmod.getlbl("cdlbl100" , "pmcoststpm.aspx.vb") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowblue""></td>")
                dhr = dr.Item("estmatcost").ToString
                dte += dhr
                sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowblue""></td>")
                If icost = "use" Then
                    dhr = dr.Item("actmatcost").ToString
                Else
                    dhr = actmatcost
                End If
                sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowblue""></td>")
                If icost = "use" Then
                    dhr = dr.Item("actmatcost").ToString
                Else
                    dhr = actmatcost
                End If
                sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowblue""></td>")
                dta += dhr
                dta1 += dhr
                dhr = sve + sva
                svt += dhr
                'If dta <> 0 Then
                If icost <> "use" Then
                    dhr = (estmatcost) - (actmatcost) 'dte + dta
                Else
                    dhr = (estmatcost) - (actmatcost)
                End If
                'Else
                'dhr = 0
                'End If
                If dhr < 0 Then
                    sclass = "redlabel"
                Else
                    sclass = "bluelabel"
                End If
                sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""" & sclass & " transrowblue""></td>")
                sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td></tr>" & vbCrLf)

                sb.Append("<tr><td colspan=""5""><img src=""../images/appbuttons/minibuttons/6PX.gif""></td></tr>" & vbCrLf)
                esttoolcost = dr.Item("esttoolcost").ToString
                acttoolcost = dr.Item("acttoolcost").ToString
                'estjptoolcost = dr.Item("estjptoolcost").ToString
                'actjptoolcost = dr.Item("actjptoolcost").ToString

                sb.Append("<tr height=""20""  class=""transrowblue"">")
                sb.Append("<td class=""bluelabel transrowblue"">" & tmod.getlbl("cdlbl101" , "pmcoststpm.aspx.vb") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowblue""></td>")
                dhr = dr.Item("esttoolcost").ToString
                dte += dhr
                sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowblue""></td>")
                dhr = dr.Item("acttoolcost").ToString
                dta += dhr
                dta1 += dhr
                sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowblue""></td>")
                dhr = dr.Item("acttoolcost").ToString
                'dta += dhr
                sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowblue""></td>")

                dhr = svte + svta
                svt += dhr
                'If dta <> 0 Then
                If icost = "use" Then
                    dhr = (esttoolcost) - (acttoolcost) 'dte + dta
                Else
                    dhr = (esttoolcost) - (acttoolcost)
                End If

                'Else
                'dhr = 0
                'End If
                If dhr < 0 Then
                    sclass = "redlabel"
                Else
                    sclass = "bluelabel"
                End If
                sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""" & sclass & " transrowblue""></td>")
                sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td></tr>" & vbCrLf)

                sb.Append("<tr><td colspan=""5""><img src=""../images/appbuttons/minibuttons/6PX.gif""></td></tr>" & vbCrLf)
                estlubecost = dr.Item("estlubecost").ToString
                actlubecost = dr.Item("actlubecost").ToString
                'estjplubecost = dr.Item("estjplubecost").ToString
                'actjplubecost = dr.Item("actjplubecost").ToString


                sb.Append("<tr height=""20""  class=""transrowblue"">")
                sb.Append("<td class=""bluelabel transrowblue"">" & tmod.getlbl("cdlbl102" , "pmcoststpm.aspx.vb") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowblue""></td>")
                dhr = dr.Item("estlubecost").ToString
                dte += dhr
                sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowblue""></td>")
                dhr = dr.Item("actlubecost").ToString
                dta += dhr
                dta1 += dhr
                sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowblue""></td>")
                sb.Append("<td class=""bluelabel transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""transrowblue""></td>")
                dhr = sve + sva
                svt += dhr
                'If dta <> 0 Then
                If icost = "use" Then
                    dhr = (estlubecost) - (actjplubecost) 'dte + dta
                Else
                    dhr = (estlubecost) - (actlubecost)
                End If

                'Else
                'dhr = 0
                'End If
                If dhr < 0 Then
                    sclass = "redlabel"
                Else
                    sclass = "bluelabel"
                End If
                sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""" & sclass & " transrowblue""></td>")
                sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"">" & Format(dhr, "$##############0.00") & "</td></tr>" & vbCrLf)

                sb.Append("<tr><td colspan=""5""><img src=""../images/appbuttons/minibuttons/6PX.gif""></td></tr>" & vbCrLf)

                '***************************************totals********************************************

                sb.Append("<tr height=""20""  >")
                sb.Append("<td class=""label transrowblue"">" & tmod.getlbl("cdlbl103" , "pmcoststpm.aspx.vb") & "</td>" & vbCrLf)
                sb.Append("<td class=""label transrowblue""></td>")
                sb.Append("<td align=""center"" class=""label transrowblue"">" & Format(dte, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""label transrowblue""></td>")
                sb.Append("<td align=""center"" class=""label transrowblue"">" & Format(dta1, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""label transrowblue""></td>")
                sb.Append("<td align=""center"" class=""label transrowblue"">" & Format(dta, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""label transrowblue""></td>")
                'If dta = 0 Then
                'svt = 0
                'Else
                svt = dte - dta
                sva1 = dte - dta1
                'End If
                If svt < 0 Then
                    sclass = "redlabel"
                Else
                    sclass = "label"
                End If
                sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"" bgcolor=""#BFDDFB"">" & Format(svt, "$##############0.00") & "</td>" & vbCrLf)
                sb.Append("<td class=""" & sclass & " transrowblue""></td>")
                sb.Append("<td class=""" & sclass & " transrowblue"" align=""center"" bgcolor=""#BFDDFB"">" & Format(sva1, "$##############0.00") & "</td></tr>" & vbCrLf)

            End While
            dr.Close()
            sb.Append("<td>&nbsp;</td>")
            sb.Append("</table>")
        End If
        tdcosts.InnerHtml = sb.ToString
        'tdnotes.InnerHtml = "Job Plan Actual Labor Hours are Presented for Reference Purposes Only."
        'If icost <> "use" Then
        'tdnotes.InnerHtml += "Material Usage and Costs are Entered Externally - Material Costs are Presented for Reference Purposes Only"
        'End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang800.Text = axlabs.GetASPXPage("pmcoststpm.aspx", "lang800")
        Catch ex As Exception
        End Try
        Try
            lang801.Text = axlabs.GetASPXPage("pmcoststpm.aspx", "lang801")
        Catch ex As Exception
        End Try

    End Sub

End Class
