<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMMainMantpm.aspx.vb"
    Inherits="lucy_r12.PMMainMantpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMMainMan</title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts/dragitem.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMMainMantpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
      <!--
        function jump(id, eid, fcust) {
            //alert(id + "," + eid + "," + fcust)
            window.parent.handlejumptpm(id, eid, fcust);
            //window.location="TPMMainMantpm.aspx?jump=yes&eqid=" + eid + "&pmid=" + id;
        }
        function getdocs() {
            var cbs = document.getElementById("lbldocs").value;
            var cbsarr = cbs.split(";")
            for (var i = 0; i < cbsarr.length; i++) {
                //alert(i)
                var cbstr = cbsarr[i];
                //alert(cbstr)
                if (cbstr != "") {
                    var cbstrarr = cbstr.split("~")
                    var fnstr = cbstrarr[1]
                    var docstr = cbstrarr[0]
                    OpenFile(fnstr, docstr);
                }
            }
        }
        function resetsrch() {

            var sid = document.getElementById("lblsid").value;
            window.location = "PMMainMantpm.aspx?jump=no&sid=" + sid;
        }
        function handlert(id, rt) {
            document.getElementById("lblrtid").value = id;
            document.getElementById("tdrte").innerHTML = rt;
            document.getElementById("lblrt").value = rt;
            closert()
        }
        function getcal(fld) {
            var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
            if (eReturn) {
                var fldret = "td" + fld;
                document.getElementById(fldret).innerHTML = eReturn;
                //document.getElementById("lblschk").value = "1";
                if (fld == "n") {
                    document.getElementById("lblnext").value = eReturn;
                }
                if (fld == "s") {
                    document.getElementById("lbls").value = eReturn;
                }
                else if (fld == "e") {
                    document.getElementById("lble").value = eReturn;
                }
            }
        }

        function getsuper(typ) {
            var skill = document.getElementById("lblskillid").value;
            var ro = document.getElementById("lblro").value;
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    var retarr = eReturn.split(",")
                    if (typ == "sup") {
                        document.getElementById("lblsup").value = retarr[0];
                        document.getElementById("tdsup").innerHTML = retarr[1];
                        document.getElementById("lblsupstr").value = retarr[1];
                    }
                    else {
                        document.getElementById("lbllead").value = retarr[0];
                        document.getElementById("tdlead").innerHTML = retarr[1];
                        document.getElementById("lblleadstr").value = retarr[1];
                    }
                }
            }
        }
        function checkother() {
            document.getElementById("tdrte").innerHTML = document.getElementById("lblrt").value;
            document.getElementById("tds").innerHTML = document.getElementById("lbls").value;
            document.getElementById("tde").innerHTML = document.getElementById("lble").value;
            document.getElementById("tdsup").innerHTML = document.getElementById("lblsupstr").value;
            document.getElementById("tdlead").innerHTML = document.getElementById("lblleadstr").value;
        }
        function checkprint() {
            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                window.parent.doref();
            }
            else {
                window.parent.setref();
            }
            var chk = document.getElementById("lblprint").value;
            if (chk == "yes") {
                document.getElementById("lblprint").value = "no";
                var filt = document.getElementById("lblfilt").value;
                document.getElementById("lblfilt").value = "";
                var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
                var typ = document.getElementById("lblprintwolist").value;
                document.getElementById("lblprintwolist").value = "";
                if (typ == "pm") {
                    window.open("PMPrintAlltpm.aspx?filt=" + filt, "repWin", popwin);
                }
                else {
                    window.open("PMWOPrintAlltpm.aspx?filt=" + filt, "repWin", popwin);
                }
            }
            var who = document.getElementById("lblwho").value;
            if (who != "") {
                checkwho(who);
            }
            checkother();
        }
        function checkwho(who) {
            if (who == "depts") {
                document.getElementById("tddept").innerHTML = document.getElementById("lbldept").value;
                document.getElementById("tdcell").innerHTML = document.getElementById("lblcell").value;
                document.getElementById("tdeq").innerHTML = document.getElementById("lbleq").value;
                document.getElementById("tdfu").innerHTML = document.getElementById("lblfu").value;
                document.getElementById("tdco").innerHTML = document.getElementById("lblcomp").value;
            }
            else if (who == "locs") {
                document.getElementById("tdloc3").innerHTML = document.getElementById("lblloc").value;
                document.getElementById("tdeq3").innerHTML = document.getElementById("lbleq").value;
                document.getElementById("tdfu3").innerHTML = document.getElementById("lblfu").value;
                document.getElementById("tdco3").innerHTML = document.getElementById("lblcomp").value;
            }
        }
        function getminsrch() {
            var did = document.getElementById("lbldid").value;
            //alert(did)
            if (did != "") {
                retminsrch();
            }
            else {
                var sid = document.getElementById("lblsid").value;
                //alert(sid)
                var typ = "lul";
                var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    document.getElementById("lbllid").value = "";
                    document.getElementById("lblloc").value = "";
                    document.getElementById("tdloc3").innerHTML = "";
                    document.getElementById("lbllocstr").value = "";

                    document.getElementById("lbleq").value = "";
                    document.getElementById("tdeq3").innerHTML = "";
                    document.getElementById("lbleqid").value = "";

                    document.getElementById("lblfuid").value = "";
                    document.getElementById("lblfu").value = "";
                    document.getElementById("tdfu3").innerHTML = "";

                    document.getElementById("lblcomid").value = "";
                    document.getElementById("lblcomp").value = "";
                    document.getElementById("tdco3").innerHTML = "";

                    document.getElementById("trlocs3").className = "details";

                    var ret = eReturn.split("~");
                    document.getElementById("lbldid").value = ret[0];
                    document.getElementById("lbldept").value = ret[1];
                    document.getElementById("tddept").innerHTML = ret[1];
                    document.getElementById("lblclid").value = ret[2];
                    document.getElementById("lblcell").value = ret[3];
                    document.getElementById("tdcell").innerHTML = ret[3];
                    document.getElementById("lbleqid").value = ret[4];
                    document.getElementById("lbleq").value = ret[5];
                    document.getElementById("tdeq").innerHTML = ret[5];
                    document.getElementById("lblfuid").value = ret[6];
                    document.getElementById("lblfu").value = ret[7];
                    document.getElementById("tdfu").innerHTML = ret[7];
                    document.getElementById("lblcomid").value = ret[8];
                    document.getElementById("lblcomp").value = ret[9];
                    document.getElementById("tdco").innerHTML = ret[9];
                    document.getElementById("lbllid").value = ret[12];
                    document.getElementById("lblloc").value = ret[13];
                    document.getElementById("trdepts").className = "view";
                    document.getElementById("lblwho").value = "depts";
                }
            }


        }
        function retminsrch() {
            //alert()
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "ret";
            var did = document.getElementById("lbldid").value;
            var dept = document.getElementById("lbldept").value;
            var clid = document.getElementById("lblclid").value;
            var cell = document.getElementById("lblcell").value;
            var eqid = document.getElementById("lbleqid").value;
            var eq = document.getElementById("lbleq").value;
            var fuid = document.getElementById("lblfuid").value;
            var fu = document.getElementById("lblfu").value;
            var coid = document.getElementById("lblcoid").value;
            var comp = document.getElementById("lblcomp").value;
            var lid = document.getElementById("lbllid").value;
            var loc = document.getElementById("lblloc").value;
            //if (cell == "" && who != "checkfu" && who != "checkeq") {
            who = "deptret";
            //}

            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                document.getElementById("lbllid").value = "";
                document.getElementById("lblloc").value = "";
                document.getElementById("tdloc3").innerHTML = "";
                document.getElementById("lbllocstr").value = "";

                document.getElementById("lbleq").value = "";
                document.getElementById("tdeq3").innerHTML = "";
                document.getElementById("lbleqid").value = "";

                document.getElementById("lblfuid").value = "";
                document.getElementById("lblfu").value = "";
                document.getElementById("tdfu3").innerHTML = "";

                document.getElementById("lblcomid").value = "";
                document.getElementById("lblcomp").value = "";
                document.getElementById("tdco3").innerHTML = "";

                document.getElementById("trlocs3").className = "details";

                var ret = eReturn.split("~");
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("lbldept").value = ret[1];
                document.getElementById("tddept").innerHTML = ret[1];
                document.getElementById("lblclid").value = ret[2];
                document.getElementById("lblcell").value = ret[3];
                document.getElementById("tdcell").innerHTML = ret[3];
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("lbleq").value = ret[5];
                document.getElementById("tdeq").innerHTML = ret[5];
                document.getElementById("lblfuid").value = ret[6];
                document.getElementById("lblfu").value = ret[7];
                document.getElementById("tdfu").innerHTML = ret[7];
                document.getElementById("lblcomid").value = ret[8];
                document.getElementById("lblcomp").value = ret[9];
                document.getElementById("tdco").innerHTML = ret[9];
                document.getElementById("lbllid").value = ret[12];
                document.getElementById("lblloc").value = ret[13];
                document.getElementById("trdepts").className = "view";
                document.getElementById("lblwho").value = "depts";
            }
        }
        function getlocs1() {
            var sid = document.getElementById("lblsid").value;
            var lid = document.getElementById("lbllid").value;
            var typ = "lul"
            //alert(lid)
            if (lid != "") {
                typ = "retloc"
            }
            var eqid = document.getElementById("lbleqid").value;
            var fuid = document.getElementById("lblfuid").value;
            var coid = document.getElementById("lblcomid").value;
            //alert(fuid)
            var wo = "";
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                document.getElementById("lbldid").value = "";
                document.getElementById("lbldept").value = "";
                document.getElementById("tddept").innerHTML = "";
                document.getElementById("lblclid").value = "";
                document.getElementById("lblcell").value = "";
                document.getElementById("tdcell").innerHTML = "";
                document.getElementById("lbleqid").value = "";
                document.getElementById("lbleq").value = "";
                document.getElementById("tdeq").innerHTML = "";
                document.getElementById("lblfuid").value = "";
                document.getElementById("lblfu").value = "";
                document.getElementById("tdfu").innerHTML = "";
                document.getElementById("lblcomid").value = "";
                document.getElementById("lblcomp").value = "";
                document.getElementById("tdco").innerHTML = "";
                document.getElementById("lbllid").value = "";
                document.getElementById("lblloc").value = "";
                document.getElementById("trdepts").className = "details";

                var ret = eReturn.split("~");
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("lblloc").value = ret[1];
                document.getElementById("tdloc3").innerHTML = ret[2];
                document.getElementById("lbllocstr").value = ret[1];

                document.getElementById("lbleq").value = ret[4];
                document.getElementById("tdeq3").innerHTML = ret[4];
                document.getElementById("lbleqid").value = ret[3];

                document.getElementById("lblfuid").value = ret[5];
                document.getElementById("lblfu").value = ret[6];
                document.getElementById("tdfu3").innerHTML = ret[6];

                document.getElementById("lblcomid").value = ret[7];
                document.getElementById("lblcomp").value = ret[8];
                document.getElementById("tdco3").innerHTML = ret[8];

                document.getElementById("trlocs3").className = "view";
                document.getElementById("lblwho").value = "locs";



            }

        }
        function srchloc1() {
            //alert("Temporarily Disabled")
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "lu"
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=lul&sid=" + sid + "&wo=" + wo, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                var ret = eReturn.split("~");
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("lblloc").innerHTML = ret[1];
                document.getElementById("lbllocstr").value = ret[1];
                //document.getElementById("lblsubmit").value = "srch";
                //document.getElementById("form1").submit();
            }
        }
         //-->
    </script>
</head>
<body onload="checkprint();" class="tbg">
    <form id="form1" method="post" runat="server">
    <table style="left: 0px; position: absolute; top: 0px" width="750">
        <tbody>
            <tr>
                <td colspan="3">
                    <asp:Repeater ID="rptrtasks" runat="server">
                        <HeaderTemplate>
                            <table cellspacing="2" cellpadding="0" width="750">
                                <tr class="tbg" height="26">
                                    <td class="thdrsingg plainlabel" width="150">
                                        <asp:Label ID="lang859" runat="server">Equipment#</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel" width="90">
                                        <asp:Label ID="lang860" runat="server">Work Order#</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel" width="340">
                                        <asp:Label ID="lang861" runat="server">TPM</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel" width="90">
                                        <asp:Label ID="lang862" runat="server">Next Date</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel" width="20">
                                        <img src="../images/appbuttons/minibuttons/gwarningnbg.gif">
                                    </td>
                                    <td class="thdrsingg plainlabel" width="40">
                                        <asp:Label ID="lang863" runat="server">Print</asp:Label>
                                    </td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="tbg" height="20">
                                <td class="plainlabel">
                                    &nbsp;
                                    <asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>' runat="server">
                                    </asp:Label>
                                </td>
                                <td class="plainlabel">
                                    &nbsp;
                                    <asp:Label ID="Label3" Text='<%# DataBinder.Eval(Container.DataItem,"wonum")%>' runat="server">
                                    </asp:Label>
                                </td>
                                <td class="details">
                                    &nbsp;
                                    <asp:Label ID="lblpdm" Text='<%# DataBinder.Eval(Container.DataItem,"pmd")%>' runat="server">
                                    </asp:Label>
                                </td>
                                <td class="plainlabel">
                                    <asp:LinkButton ID="lbltn" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"pm")%>'
                                        runat="server">
                                    </asp:LinkButton>
                                </td>
                                <td class="plainlabel">
                                    <asp:Label ID="lbllast" Text='<%# DataBinder.Eval(Container.DataItem,"nextdates")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td align="center">
                                    <img id="iwi" onmouseover="return overlib('Status Check Icon', ABOVE, LEFT)" onmouseout="return nd()"
                                        src="../images/appbuttons/minibuttons/gwarning.gif" runat="server">
                                </td>
                                <td class="plainlabel" align="center">
                                    <img id="imgi" runat="server" onmouseover="return overlib('Print This TPM', ABOVE, LEFT)"
                                        onclick="printpm();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/printx.gif">
                                    <img id="imgi2" runat="server" onmouseover="return overlib('Print This TPM Work Order', ABOVE, LEFT)"
                                        onclick="printwo();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/woprint.gif">
                                </td>
                                <td class="details">
                                    <asp:Label ID="lblpmiditem" Text='<%# DataBinder.Eval(Container.DataItem,"pmid")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td class="details">
                                    <asp:Label ID="lblcusti" Text='<%# DataBinder.Eval(Container.DataItem,"fcust")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr height="20">
                                <td class="plainlabel transrowblue">
                                    &nbsp;
                                    <asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>' runat="server">
                                    </asp:Label>
                                </td>
                                <td class="plainlabel transrowblue">
                                    &nbsp;
                                    <asp:Label ID="Label4" Text='<%# DataBinder.Eval(Container.DataItem,"wonum")%>' runat="server">
                                    </asp:Label>
                                </td>
                                <td class="details">
                                    &nbsp;
                                    <asp:Label ID="lblpdmalt" Text='<%# DataBinder.Eval(Container.DataItem,"pmd")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td class="plainlabel transrowblue">
                                    <asp:LinkButton ID="lbltnalt" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"pm")%>'
                                        runat="server">
                                    </asp:LinkButton>
                                </td>
                                <td class="plainlabel transrowblue">
                                    <asp:Label ID="lbllastalt" Text='<%# DataBinder.Eval(Container.DataItem,"nextdates")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td align="center" class="plainlabel transrowblue">
                                    <img id="iwa" onmouseover="return overlib('Status Check Icon', ABOVE, LEFT)" onmouseout="return nd()"
                                        src="../images/appbuttons/minibuttons/gwarning.gif" runat="server">
                                </td>
                                <td class="plainlabel transrowblue" align="center">
                                    <img id="imga" runat="server" onmouseover="return overlib('Print This TPM', ABOVE, LEFT)"
                                        onclick="printpm();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/printx.gif">
                                    <img id="imga2" runat="server" onmouseover="return overlib('Print This TPM Work Order', ABOVE, LEFT)"
                                        onclick="printwo();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/woprint.gif">
                                </td>
                                <td class="details">
                                    <asp:Label ID="lblpmidalt" Text='<%# DataBinder.Eval(Container.DataItem,"pmid")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td class="details">
                                    <asp:Label ID="lblcusta" Text='<%# DataBinder.Eval(Container.DataItem,"fcust")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="3">
                    <table style="border-right: blue 1px solid; border-top: blue 1px solid; border-left: blue 1px solid;
                        border-bottom: blue 1px solid" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                                <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                    runat="server">
                            </td>
                            <td width="20">
                                <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                    runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="2" width="750">
                        <tr>
                            <td width="26">
                            </td>
                            <td width="54">
                            </td>
                            <td width="170">
                            </td>
                            <td width="80">
                            </td>
                            <td width="170">
                            </td>
                            <td width="200">
                            </td>
                        </tr>
                        <tr height="22">
                            <td class="thdrsinglft">
                                <img src="../images/appbuttons/minibuttons/pmgridhdr.gif" border="0">
                            </td>
                            <td class="thdrsingrt label" align="left" colspan="5">
                                <asp:Label ID="lang864" runat="server">TPM Record Search</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <table cellspacing="0" cellpadding="2" width="750">
                                    <tr>
                                        <td width="110">
                                        </td>
                                        <td width="135">
                                        </td>
                                        <td width="25">
                                        </td>
                                        <td width="120">
                                        </td>
                                        <td width="135">
                                        </td>
                                        <td width="25">
                                        </td>
                                        <td width="70">
                                        </td>
                                        <td width="160">
                                        </td>
                                        <td width="20">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="9">
                                            <table cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td id="tddepts" class="bluelabel" runat="server">
                                                        Use Departments
                                                    </td>
                                                    <td>
                                                        <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif" />
                                                    </td>
                                                    <td id="tdlocs1" class="bluelabel" runat="server">
                                                        Use Locations
                                                    </td>
                                                    <td>
                                                        <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="trdepts" class="details" runat="server">
                                        <td colspan="9">
                                            <table cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td class="label" width="100">
                                                        Department
                                                    </td>
                                                    <td id="tddept" class="plainlabel" width="170" runat="server">
                                                    </td>
                                                    <td width="30">
                                                    </td>
                                                    <td class="label" width="100">
                                                        Equipment
                                                    </td>
                                                    <td id="tdeq" class="plainlabel" width="170" runat="server">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="label">
                                                        Station\Cell
                                                    </td>
                                                    <td id="tdcell" class="plainlabel" runat="server">
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td class="label">
                                                        Function
                                                    </td>
                                                    <td id="tdfu" class="plainlabel" runat="server">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="label">
                                                        <asp:Label ID="Label3" runat="server"></asp:Label>
                                                    </td>
                                                    <td id="tdcharge1" class="plainlabel" runat="server">
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td class="label">
                                                        Component
                                                    </td>
                                                    <td id="tdco" class="plainlabel" runat="server">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="trlocs3" class="details" runat="server">
                                        <td colspan="9">
                                            <table cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td class="label" width="100">
                                                        Location
                                                    </td>
                                                    <td id="tdloc3" class="plainlabel" runat="server" width="170">
                                                    </td>
                                                    <td width="30">
                                                    </td>
                                                    <td class="label" width="100">
                                                        Function
                                                    </td>
                                                    <td id="tdfu3" class="plainlabel" runat="server" width="170">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="label">
                                                        Equipment
                                                    </td>
                                                    <td id="tdeq3" class="plainlabel" runat="server">
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td class="label">
                                                        Component
                                                    </td>
                                                    <td id="tdco3" class="plainlabel" runat="server">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">
                                            <asp:Label ID="lang871" runat="server">Work Order#</asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtsrchwo" runat="server" CssClass="plainlabel" Font-Size="9pt"></asp:TextBox>
                                        </td>
                                        <td class="label">
                                            <asp:Label ID="lang872" runat="server">Work Status</asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:DropDownList ID="ddstatus" runat="server" CssClass="plainlabel" Width="150px">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="label">
                                            <asp:Label ID="lang873" runat="server">Skill</asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:DropDownList ID="ddskill" runat="server" CssClass="plainlabel" Width="150px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">
                                            <asp:Label ID="lang874" runat="server">Supervisor</asp:Label>
                                        </td>
                                        <td class="plainlabel" id="tdsup">
                                        </td>
                                        <td>
                                            <img onmouseover="return overlib('Select Supervisor', LEFT)" onclick="getsuper('sup');"
                                                onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                                border="0">
                                        </td>
                                        <td class="label">
                                            <asp:Label ID="lang875" runat="server">Lead Craft</asp:Label>
                                        </td>
                                        <td class="plainlabel" id="tdlead">
                                        </td>
                                        <td>
                                            <img onmouseover="return overlib('Select Lead Craft', LEFT)" onclick="getsuper('lead');"
                                                onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                                border="0">
                                        </td>
                                        <td class="label">
                                            PdM
                                        </td>
                                        <td colspan="2">
                                            <asp:DropDownList ID="ddpdmlist" runat="server" CssClass="plainlabel" Width="150px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">
                                            <asp:Label ID="lang876" runat="server">From Date</asp:Label>
                                        </td>
                                        <td class="plainlabel" id="tds">
                                        </td>
                                        <td>
                                            <img onmouseover="return overlib('Get From Date', LEFT)" onclick="getcal('s');" onmouseout="return nd()"
                                                height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg" width="19">
                                        </td>
                                        <td class="label">
                                            <asp:Label ID="lang877" runat="server">To Date</asp:Label>
                                        </td>
                                        <td class="plainlabel" id="tde">
                                        </td>
                                        <td>
                                            <img onmouseover="return overlib('Get To Date', LEFT)" onclick="getcal('e');" onmouseout="return nd()"
                                                height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg" width="19">
                                        </td>
                                        <td class="label">
                                            <asp:Label ID="lang878" runat="server">Route</asp:Label>
                                        </td>
                                        <td class="plainlabel" id="tdrte">
                                        </td>
                                        <td>
                                            <img onmouseover="return overlib('Get Route', LEFT)" onclick="getrt('pmrt');" onmouseout="return nd()"
                                                alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                                        </td>
                                    </tr>
                                    <tr>
                                    <td class="label" colspan="2">
                                    <input type="checkbox" id="cbbylab" runat="server" />Bypass Labor Restriction
                                    </td>
                                        <td align="right" colspan="7">
                                            <img class="details" id="imgregpm" onmouseover="return overlib('Print This TPM Collection', ABOVE, LEFT)"
                                                onclick="printpm('pm');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/printx.gif"
                                                runat="server">&nbsp;
                                            <img class="details" id="imgwopm" onmouseover="return overlib('Print This TPM Work Order Collection', ABOVE, LEFT)"
                                                onclick="printpm('wo');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/woprint.gif"
                                                runat="server">&nbsp;
                                            <img class="details" id="imgrteprint" onmouseover="return overlib('Print This TPM Route Collection', ABOVE, LEFT)"
                                                onclick="printpm('pm');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/routeall.gif"
                                                runat="server">&nbsp;
                                            <img class="details" id="imgrtewprint" onmouseover="return overlib('Print This TPM Route Work Order Collection', ABOVE, LEFT)"
                                                onclick="printpm('wo');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/routeallwo.gif"
                                                runat="server">&nbsp;
                                            <img onclick="resetsrch();" alt="" src="../images/appbuttons/minibuttons/switch.gif">
                                            <asp:ImageButton ID="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                                            </asp:ImageButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <div onmouseup="mouseUp();restore('ifrt', 'trrt');" class="details" onmousedown="makeDraggable(this, 'ifrt', 'trrt');"
        id="rtdiv" style="border-right: black 1px solid; border-top: black 1px solid;
        z-index: 999; border-left: black 1px solid; width: 570px; border-bottom: black 1px solid;
        height: 450px">
        <table cellspacing="0" cellpadding="0" width="570" class="tbg">
            <tr bgcolor="blue" height="20">
                <td class="whitelabel">
                    <asp:Label ID="lang879" runat="server">Record Lookup</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closert();" height="18" alt="" src="../images/close.gif" width="18"><br>
                </td>
            </tr>
            <tr class="details" id="trrt" height="450">
                <td class="bluelabelb" valign="middle" align="center" colspan="2">
                    <asp:Label ID="lang880" runat="server">Moving Window...</asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <iframe id="ifrt" style="width: 570px; height: 450px" src="" frameborder="no" runat="server">
                    </iframe>
                </td>
            </tr>
        </table>
    </div>
    <input id="txtpg" type="hidden" runat="server">
    <input id="txtpgcnt" type="hidden" runat="server"><input id="lblfilt" type="hidden"
        runat="server">
    <input id="lblprint" type="hidden" runat="server"><input id="lblpmid" type="hidden"
        runat="server">
    <input id="lblret" type="hidden" runat="server">
    <input id="lblsrch" type="hidden" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lbltyp" type="hidden" name="lbltyp" runat="server">
    <input id="lblncid" type="hidden" name="lblncid" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lbldept" type="hidden" name="lbldept" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server">
    <input id="lbllid" type="hidden" name="lbllid" runat="server"><input id="lblclid"
        type="hidden" name="lblclid" runat="server">
    <input id="lblskillid" type="hidden" name="lblskillid" runat="server">
    <input id="lblsup" type="hidden" name="Hidden1" runat="server">
    <input id="lbllead" type="hidden" name="Hidden1" runat="server">
    <input id="lblpchk" type="hidden" name="lblpchk" runat="server"><input id="lbldragid"
        type="hidden"><input id="lblrowid" type="hidden">
    <input id="lblrtid" type="hidden" name="lblrtid" runat="server">
    <input id="lblprintwolist" type="hidden" runat="server">
    <input id="lblfiltwo" type="hidden" runat="server">
    <input type="hidden" id="lbldocs" runat="server">
    <input type="hidden" id="lblro" runat="server"><input type="hidden" id="lbllocret"
        runat="server" />
    <input type="hidden" id="lbllocstr" runat="server" />
    <input type="hidden" id="lblcomid" runat="server" />
    <input type="hidden" id="lblcomp" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lblcell" runat="server" />
    <input type="hidden" id="lblfu" runat="server" />
    <input type="hidden" id="lblloc" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblsupstr" runat="server" />
    <input type="hidden" id="lblleadstr" runat="server" />
    <input type="hidden" id="lbls" runat="server" />
    <input type="hidden" id="lble" runat="server" />
    <input type="hidden" id="lblrt" runat="server" />
    <input type="hidden" id="lbllog" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lbluserid" runat="server" />
    <input type="hidden" id="lblislabor" runat="server" />
    <input type="hidden" id="lblissched" runat="server" /><input type="hidden" id="lblisplanner"
        runat="server" />
    <input type="hidden" id="lblcadm" runat="server" />
    </form>
</body>
</html>
