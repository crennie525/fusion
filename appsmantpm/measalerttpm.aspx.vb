

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class measalerttpm
    Inherits System.Web.UI.Page
	Protected WithEvents lang758 As System.Web.UI.WebControls.Label

	Protected WithEvents lang757 As System.Web.UI.WebControls.Label

	Protected WithEvents lang756 As System.Web.UI.WebControls.Label

	Protected WithEvents lang755 As System.Web.UI.WebControls.Label

	Protected WithEvents lang754 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim mu As New mmenu_utils_a
    Dim coi As String
    Dim meas As New Utilities
    Dim sql, typ, mid, atyp, sid, ro As String
    Protected WithEvents lblmeas As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rblead As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbsup As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rblog As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblatyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntolist As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromlist As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbselect As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbllc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblforum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladmin As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                btntolist.Visible = False
                btnfromlist.Visible = False
                todis.Attributes.Add("class", "view")
                fromdis.Attributes.Add("class", "view")
            End If
            sid = HttpContext.Current.Session("dfltps").ToString()
            lblsid.Value = sid
            atyp = Request.QueryString("typ").ToString
            lblatyp.Value = atyp
            If atyp = "meas" Then
                mid = Request.QueryString("tmdid").ToString
                lblmeas.Value = mid
                atyp = "tpmmeas"
            ElseIf atyp = "thresh" Then
                lblmeas.Value = "0"
                atyp = "tpmthresh"
            End If
            lbltyp.Value = "log"
            meas.Open()
            GetAll()
            GetSelected()
            meas.Dispose()
        Else
            If Request.Form("lblsubmit") = "switch" Then
                lblsubmit.Value = ""
                meas.Open()
                GetAll()
                GetSelected()
                meas.Dispose()
            End If
        End If
    End Sub
    Private Sub GetAll()
        typ = lbltyp.Value
        atyp = lblatyp.Value
        mid = lblmeas.Value
        sid = lblsid.Value
        Select Case typ
            Case "log"
                sql = "select username, userid from pmsysusers where userid not in ( " _
                + "select userid from ealert where alertid = '" & mid & "' and alerttype = '" & atyp & "' and userid is not null and siteid = '" & sid & "')"
                dr = meas.GetRdrData(sql)
                lbmaster.DataSource = dr
                lbmaster.DataValueField = "userid"
                lbmaster.DataTextField = "username"
                lbmaster.DataBind()
                dr.Close()
            Case "lead"
                sql = "select username, userid from pmsysusers where userid not in ( " _
                + "select laborid from ealert where alertid = '" & mid & "' and alerttype = '" & atyp & "' and userid is not null and siteid = '" & sid & "') " _
                + "and islabor = 1"
                dr = meas.GetRdrData(sql)
                lbmaster.DataSource = dr
                lbmaster.DataValueField = "userid"
                lbmaster.DataTextField = "username"
                lbmaster.DataBind()
                dr.Close()
            Case "sup"
                sql = "select name, username, userid from pmsysusers where userid not in ( " _
               + "select superid from ealert where alertid = '" & mid & "' and alerttype = '" & atyp & "' and userid is not null and siteid = '" & sid & "') " _
                + "and islabor = 1"
                dr = meas.GetRdrData(sql)
                lbmaster.DataSource = dr
                lbmaster.DataValueField = "userid"
                lbmaster.DataTextField = "username"
                lbmaster.DataBind()
                dr.Close()
        End Select
    End Sub
    Private Sub GetSelected()
        typ = lbltyp.Value
        mid = lblmeas.Value
        sid = lblsid.Value
        Select Case typ
            Case "log"
                sql = "select username, userid from ealert where alertid = '" & mid & "' and alerttype = '" & atyp & "' and userid is not null and siteid = '" & sid & "'"
                dr = meas.GetRdrData(sql)
                lbselect.DataSource = dr
                lbselect.DataValueField = "userid"
                lbselect.DataTextField = "username"
                lbselect.DataBind()
                dr.Close()
            Case "lead"
                sql = "select username, userid from ealert where alertid = '" & mid & "' and alerttype = '" & atyp & "' and laborid is not null and siteid = '" & sid & "'"
                dr = meas.GetRdrData(sql)
                lbselect.DataSource = dr
                lbselect.DataValueField = "userid"
                lbselect.DataTextField = "username"
                lbselect.DataBind()
                dr.Close()
            Case "sup"
                sql = "select username, userid from ealert where alertid = '" & mid & "' and alerttype = '" & atyp & "' and superid is not null and siteid = '" & sid & "'"
                dr = meas.GetRdrData(sql)
                lbselect.DataSource = dr
                lbselect.DataValueField = "userid"
                lbselect.DataTextField = "username"
                lbselect.DataBind()
                dr.Close()
        End Select

    End Sub

    Private Sub btntolist_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntolist.Click
        Dim Item As ListItem
        Dim f, fi As String
        meas.Open()
        For Each Item In lbmaster.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                GetItems(fi, f)
            End If
        Next
        GetAll()
        GetSelected()
        meas.Dispose()
    End Sub
    Private Sub GetItems(ByVal id As String, ByVal str As String)
        typ = lbltyp.Value
        atyp = lblatyp.Value
        mid = lblmeas.Value
        sid = lblsid.Value
        Select Case typ
            Case "log"
                sql = "insert into ealert (alerttype, alertid, userid, username, siteid) values ('" & atyp & "','" & mid & "','" & id & "','" & str & "', '" & sid & "')"
                meas.Update(sql)
            Case "lead"
                sql = "insert into ealert (alerttype, alertid, userid, username, siteid) values ('" & atyp & "','" & mid & "','" & id & "','" & str & "', '" & sid & "')"
                meas.Update(sql)
            Case "sup"
                sql = "insert into ealert (alerttype, alertid, userid, username, siteid) values ('" & atyp & "','" & mid & "','" & id & "','" & str & "', '" & sid & "')"
                meas.Update(sql)
        End Select

    End Sub

    Private Sub btnfromlist_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromlist.Click
        Dim Item As ListItem
        Dim f, fi As String
        meas.Open()
        For Each Item In lbselect.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                RemItems(fi, f)
            End If
        Next
        GetAll()
        GetSelected()
        meas.Dispose()
    End Sub
    Private Sub RemItems(ByVal id As String, ByVal str As String)
        typ = lbltyp.Value
        atyp = lblatyp.Value
        mid = lblmeas.Value
        sid = lblsid.Value
        Select Case typ
            Case "log"
                sql = "delete from ealert where alertid = '" & mid & "' and userid = '" & id & "' and alerttype = '" & atyp & "' and siteid = '" & sid & "'"
                meas.Update(sql)
            Case "lead"
                sql = "delete from ealert where alertid = '" & mid & "' and userid = '" & id & "' and alerttype = '" & atyp & "' and siteid = '" & sid & "'"
                meas.Update(sql)
            Case "sup"
                sql = "delete from ealert where alertid = '" & mid & "' and userid = '" & id & "' and alerttype = '" & atyp & "' and siteid = '" & sid & "'"
                meas.Update(sql)
        End Select

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang754.Text = axlabs.GetASPXPage("measalerttpm.aspx", "lang754")
        Catch ex As Exception
        End Try
        Try
            lang755.Text = axlabs.GetASPXPage("measalerttpm.aspx", "lang755")
        Catch ex As Exception
        End Try
        Try
            lang756.Text = axlabs.GetASPXPage("measalerttpm.aspx", "lang756")
        Catch ex As Exception
        End Try
        Try
            lang757.Text = axlabs.GetASPXPage("measalerttpm.aspx", "lang757")
        Catch ex As Exception
        End Try
        Try
            lang758.Text = axlabs.GetASPXPage("measalerttpm.aspx", "lang758")
        Catch ex As Exception
        End Try

    End Sub

End Class
