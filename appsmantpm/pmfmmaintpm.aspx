<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmfmmaintpm.aspx.vb" Inherits="lucy_r12.pmfmmaintpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>pmfmmain</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmfmmaintpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table style="left: 2px; position: absolute; top: 2px" cellspacing="1" cellpadding="2"
        width="730">
        <tr>
            <td>
                <table width="730">
                    <tr height="22">
                        <td class="label" width="80">
                            <asp:Label ID="lang831" runat="server">Current PM</asp:Label>
                        </td>
                        <td class="plainlabel" id="tdwo" width="140" runat="server">
                        </td>
                        <td class="plainlabel" id="tdwod" width="450" runat="server">
                        </td>
                        <td align="right" width="30">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="2" width="730">
                    <tr height="22">
                        <td class="thdrhov label" id="tdj" onclick="gettab('jp');" width="120" runat="server">
                            <asp:Label ID="lang832" runat="server">Failure Modes</asp:Label>
                        </td>
                        <td class="thdr label" id="tdjpm" onclick="gettab('jpm');" width="120" runat="server">
                            <asp:Label ID="lang833" runat="server">Measurements</asp:Label>
                        </td>
                        <td width="490">
                        </td>
                    </tr>
                    <tr>
                        <td id="jp" valign="top" colspan="4" runat="server" class="tdborder view">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <iframe id="ifjp" src="pmholdtpm.htm" frameborder="no" width="760" scrolling="auto"
                                            height="420" runat="server" style="background-color: transparent" allowtransparency>
                                        </iframe>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblwo" type="hidden" runat="server" name="lblwo">
    <input type="hidden" id="lbljpid" runat="server" name="lbljpid">
    <input type="hidden" id="lblupsav" runat="server" name="lblupsav">
    <input type="hidden" id="lblstat" runat="server" name="lblstat">
    <input type="hidden" id="lblpmid" runat="server">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
