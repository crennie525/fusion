<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMMeastpm.aspx.vb" Inherits="lucy_r12.PMMeastpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMMeas</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMMeastpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="domup();">
    <form id="form1" method="post" runat="server">
    <table width="560">
        <tr>
            <td>
                <asp:DataGrid ID="dgmeas" runat="server" Width="540px" ShowFooter="True" CellPadding="0"
                    GridLines="None" AllowPaging="True" AllowCustomPaging="True" AutoGenerateColumns="False"
                    CellSpacing="1">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <EditItemStyle Height="15px"></EditItemStyle>
                    <AlternatingItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="#E7F1FD">
                    </AlternatingItemStyle>
                    <ItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="ControlLightLight">
                    </ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Type">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label3" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Hi">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label6" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Lo">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label8" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Spec">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label10" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Measurement">
                            <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Width="90px" Text='<%# DataBinder.Eval(Container, "DataItem.measurement2") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtmeas" runat="server" Width="90px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.measurement2") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="History">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <img src="../images/appbuttons/minibuttons/lchart.gif" onmouseover="return overlib('View History Graph', ABOVE, LEFT)"
                                    onmouseout="return nd()" id="ihg" runat="server">
                                <img id="img" runat="server" onmouseover="return overlib('Print History', ABOVE, LEFT)"
                                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/printex.gif">
                            </ItemTemplate>
                            <EditItemTemplate>
                                <img src="../images/appbuttons/minibuttons/lchart.gif" onmouseover="return overlib('View History Graph', ABOVE, LEFT)"
                                    onmouseout="return nd()" id="ihga" runat="server">
                                <img id="imga" runat="server" onmouseover="return overlib('Print History', ABOVE, LEFT)"
                                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/printex.gif">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="WO">
                            <HeaderStyle Width="30px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <img src="../images/appbuttons/minibuttons/wo.gif" onmouseover="return overlib('View History Graph', ABOVE, LEFT)"
                                    onmouseout="return nd()" id="iwo" runat="server">
                            </ItemTemplate>
                            <EditItemTemplate>
                                <img src="../images/appbuttons/minibuttons/wo.gif" onmouseover="return overlib('View History Graph', ABOVE, LEFT)"
                                    onmouseout="return nd()" id="iwoa" runat="server">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <HeaderStyle></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblpmtskid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblpmtskida" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <HeaderStyle></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lbltmdid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.tmdid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lbltmdida" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.tmdid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
                        ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <div class="details" id="rtdiv" style="border-right: black 1px solid; border-top: black 1px solid;
        z-index: 999; border-left: black 1px solid; width: 370px; border-bottom: black 1px solid;
        height: 180px">
        <table cellspacing="0" cellpadding="0" width="370" bgcolor="white">
            <tr bgcolor="blue" height="20">
                <td class="whitelabel">
                    <asp:Label ID="lang881" runat="server">Work Order Details</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closert();" height="18" alt="" src="../images/close.gif" width="18"><br>
                </td>
            </tr>
            <tr class="details" id="trrt" height="180">
                <td class="bluelabelb" valign="middle" align="center" colspan="2">
                    <asp:Label ID="lang882" runat="server">Moving Window...</asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr height="20">
                            <td width="20">
                            </td>
                            <td width="20">
                            </td>
                            <td width="120" class="bluelabel">
                                <asp:Label ID="lang883" runat="server">Skill Required</asp:Label>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="ddskill" runat="server" Width="160px" CssClass="plainlabel">
                                </asp:DropDownList>
                            </td>
                            <td width="50">
                            </td>
                        </tr>
                        <tr height="20">
                            <td>
                                <input id="cbsupe" type="checkbox" runat="server" name="cbsupe">
                            </td>
                            <td>
                                <img onmouseover="return overlib('Check to send email alert to Supervisor')" onmouseout="return nd()"
                                    src="../images/appbuttons/minibuttons/emailcb.gif">
                            </td>
                            <td class="bluelabel">
                                <asp:Label ID="lang884" runat="server">Supervisor</asp:Label>
                            </td>
                            <td width="140">
                                <asp:TextBox ID="txtsup" runat="server" Width="130px" CssClass="plainlabel" ReadOnly="True"></asp:TextBox>
                            </td>
                            <td width="20">
                                <img onclick="getsuper('sup');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    border="0">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr height="20">
                            <td>
                                <input id="cbleade" type="checkbox" runat="server" name="cbleade">
                            </td>
                            <td>
                                <img onmouseover="return overlib('Check to send email alert to Lead Craft')" onmouseout="return nd()"
                                    src="../images/appbuttons/minibuttons/emailcb.gif">
                            </td>
                            <td class="bluelabel">
                                <asp:Label ID="lang885" runat="server">Lead Craft</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtlead" runat="server" Width="130px" CssClass="plainlabel" ReadOnly="True"></asp:TextBox>
                            </td>
                            <td>
                                <img onclick="getsuper('lead');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    border="0">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr height="20">
                            <td>
                            </td>
                            <td>
                            </td>
                            <td class="bluelabel">
                                <asp:Label ID="lang886" runat="server">Work Type</asp:Label>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="ddwt" runat="server" CssClass="plainlabel">
                                    <asp:ListItem Value="CM" Selected="True">Corrective Maintenance</asp:ListItem>
                                    <asp:ListItem Value="EM">Emergency Maintenance</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr height="20">
                            <td>
                            </td>
                            <td>
                            </td>
                            <td class="bluelabel">
                                <asp:Label ID="lang887" runat="server">Target Start</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtstart" runat="server" Width="130px" CssClass="plainlabel" ReadOnly="True"></asp:TextBox>
                            </td>
                            <td>
                                <img onclick="getcal('tstart');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                    width="19">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr height="20">
                            <td colspan="6" class="bluelabel">
                                <asp:Label ID="lang888" runat="server">Problem Description</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <asp:TextBox ID="txtprob" runat="server" Width="380px" TextMode="MultiLine" Height="50px"
                                    CssClass="plainlabel"></asp:TextBox>
                            </td>
                        </tr>
                        <tr height="20">
                            <td colspan="6" class="bluelabel">
                                <asp:Label ID="lang889" runat="server">Corrective Action</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <asp:TextBox ID="txtcorr" runat="server" Width="380px" TextMode="MultiLine" Height="50px"
                                    CssClass="plainlabel"></asp:TextBox>
                            </td>
                        </tr>
                        <tr height="30">
                            <td align="right" colspan="6">
                                <asp:ImageButton ID="btnwo" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif">
                                </asp:ImageButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <input id="lbltid" type="hidden" runat="server">
    <input type="hidden" id="lblmcnt" runat="server">
    <input type="hidden" id="lblmup" runat="server">
    <input type="hidden" id="lblpmid" runat="server">
    <input type="hidden" id="lblpmtid" runat="server">
    <input type="hidden" id="lbltmid" runat="server"><input type="hidden" id="lblcid"
        runat="server">
    <input type="hidden" id="lblsup" runat="server">
    <input type="hidden" id="lbllead" runat="server">
    <input type="hidden" id="lblwo" runat="server"><input type="hidden" id="lblcomid"
        runat="server">
    <input type="hidden" id="lblro" runat="server">
    <input id="lbldragid" type="hidden" name="lbldragid" runat="server"><input id="lblrowid"
        type="hidden" name="lblrowid" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
