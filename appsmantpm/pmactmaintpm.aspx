<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmactmaintpm.aspx.vb"
    Inherits="lucy_r12.pmactmaintpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>pmactmain</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmactmaintpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 2px; left: 2px" cellspacing="1" cellpadding="2"
        width="750">
        <tr>
            <td>
                <table width="750">
                    <tr height="22">
                        <td class="label" width="80">
                            <asp:Label ID="lang768" runat="server">Work Order#</asp:Label>
                        </td>
                        <td class="plainlabel" id="tdwo" width="140" runat="server">
                        </td>
                        <td class="plainlabel" id="tdwod" width="450" runat="server">
                        </td>
                        <td align="right" width="50">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="2" width="750">
                    <tr height="22">
                        <td class="thdrhov label" id="tdj" onclick="gettab('jp');" width="200" runat="server">
                            <asp:Label ID="lang769" runat="server">TPM Task Details (Labor)</asp:Label>
                        </td>
                        <td class="thdr label" id="tdm" onclick="gettab('jpm');" width="200" runat="server">
                            <asp:Label ID="lang770" runat="server">TPM Task Details (Materials)</asp:Label>
                        </td>
                        <td class="thdr label" id="tdl" onclick="gettab('lab');" width="200" runat="server">
                            <asp:Label ID="lang771" runat="server">TPM Labor Reporting</asp:Label>
                        </td>
                        <td width="150">
                        </td>
                    </tr>
                    <tr>
                        <td class="details" id="wo" colspan="5" runat="server">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <iframe id="ifwo" src="pmholdtpm.htm" frameborder="no" width="760" height="440" runat="server"
                                            style="background-color: transparent" allowtransparency></iframe>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td id="jp" class="tdborder" valign="top" colspan="5" runat="server">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <iframe id="ifjp" src="pmholdtpm.htm" frameborder="no" width="750" height="440" runat="server"
                                            style="background-color: transparent" allowtransparency></iframe>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblwo" type="hidden" name="lblwo" runat="server">
    <input id="lblpmid" type="hidden" name="lblpmid" runat="server">
    <input id="lblupsav" type="hidden" name="lblupsav" runat="server">
    <input id="lblstat" type="hidden" name="lblstat" runat="server">
    <input type="hidden" id="lblpmhid" runat="server"><input type="hidden" id="lblro"
        runat="server">
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
