

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmactmmaintpm
    Inherits System.Web.UI.Page
	Protected WithEvents lang774 As System.Web.UI.WebControls.Label

	Protected WithEvents lang773 As System.Web.UI.WebControls.Label

	Protected WithEvents lang772 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim comp As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim wonum, pmid, pmhid, ro As String
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmhid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdpar As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtoo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlub As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents wo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifmain As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblupsav As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            wonum = Request.QueryString("wo").ToString '"1352" '
            lblwo.Value = wonum
            pmid = Request.QueryString("pmid").ToString '"139" '
            pmhid = Request.QueryString("pmhid").ToString '"501" '
            lblpmid.Value = pmid
            lblpmhid.Value = pmhid
            ifmain.Attributes.Add("src", "pmactmtpm.aspx?typ=p&wo=" & wonum & "&pmid=" & pmid & "&pmhid=" & pmhid & "&ro=" & ro)
        End If
    End Sub

	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang772.Text = axlabs.GetASPXPage("pmactmmaintpm.aspx","lang772")
		Catch ex As Exception
		End Try
		Try
			lang773.Text = axlabs.GetASPXPage("pmactmmaintpm.aspx","lang773")
		Catch ex As Exception
		End Try
		Try
			lang774.Text = axlabs.GetASPXPage("pmactmmaintpm.aspx","lang774")
		Catch ex As Exception
		End Try

	End Sub

End Class
