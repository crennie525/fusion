<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmcoststpm.aspx.vb" Inherits="lucy_r12.pmcoststpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>pmcosts</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmcoststpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="scrolltop();checkit();" class="tbg">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" cellspacing="0" cellpadding="2" width="750" style="position: absolute;
        top: 4px; left: 4px">
        <tbody>
            <tr>
                <td>
                    <table width="750">
                        <tr height="22">
                            <td class="label" width="80">
                                <asp:Label ID="lang800" runat="server">Work Order#</asp:Label>
                            </td>
                            <td class="plainlabel" id="tdwo" width="140" runat="server">
                            </td>
                            <td class="plainlabel" id="tdwod" width="430" runat="server">
                            </td>
                            <td align="right" width="100">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="24" runat="server" id="Tr1">
                <td class="thdrsing label">
                    <asp:Label ID="lang801" runat="server">TPM Costs Review</asp:Label>
                </td>
            </tr>
            <tr>
                <td id="tdcosts" runat="server" align="center">
                </td>
            </tr>
            <tr>
                <td id="tdnotes" runat="server" class="plainlabelblue" align="center">
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" id="lblwo" runat="server" name="lblwo">
    <input type="hidden" id="lbljpid" runat="server" name="lbljpid">
    <input type="hidden" id="lblstat" runat="server" name="lblstat">
    <input type="hidden" id="lblpmid" runat="server">
    <input id="xCoord" type="hidden" runat="server" name="xCoord"><input id="yCoord"
        type="hidden" runat="server" name="yCoord">
    <input type="hidden" id="lbllog" runat="server" name="lbllog">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
