<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMMeasDialogtpm.aspx.vb"
    Inherits="lucy_r12.PMMeasDialogtpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMMeasDialog</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/sessrefdialog.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMMeasDialogtpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onunload="handleret();" onload="resetsess();">
    <form id="form1" method="post" runat="server">
    <iframe id="ifmeas" runat="server" src="" width="560" height="430" frameborder="no">
    </iframe>
    <iframe id="ifsession" class="details" src="" frameborder="no" runat="server" style="z-index: 0">
    </iframe>
    <input type="hidden" id="lblsessrefresh" runat="server" name="lblsessrefresh">
    <input type="hidden" id="lblmcnt" runat="server" name="lblmcnt">
    <input type="hidden" id="lblmup" runat="server" name="lblmup">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
