<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmactmmaintpm.aspx.vb"
    Inherits="lucy_r12.pmactmmaintpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>pmactmmain</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmactmmaintpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table style="left: 2px; position: absolute; top: 2px" cellspacing="1" cellpadding="2"
        width="730">
        <tr>
            <td>
                <table cellspacing="0" cellpadding="2" width="720">
                    <tr height="22">
                        <td class="thdrhov label" id="tdpar" onclick="gettab('par');" width="150" runat="server">
                            <asp:Label ID="lang772" runat="server">TPM Task Parts</asp:Label>
                        </td>
                        <td class="thdr label" id="tdtoo" onclick="gettab('too');" width="150" runat="server">
                            <asp:Label ID="lang773" runat="server">TPM Task Tools</asp:Label>
                        </td>
                        <td class="thdr label" id="tdlub" onclick="gettab('lub');" width="150" runat="server">
                            <asp:Label ID="lang774" runat="server">TPM Task Lubes</asp:Label>
                        </td>
                        <td width="270">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdborder" id="wo" colspan="4" runat="server">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <iframe id="ifmain" src="pmholdtpm.htm" frameborder="no" width="730" scrolling="auto"
                                            height="350" runat="server" style="background-color: transparent" allowtransparency>
                                        </iframe>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblwo" type="hidden" name="lblwo" runat="server">
    <input id="lblpmid" type="hidden" name="lblpmid" runat="server">
    <input id="lblupsav" type="hidden" name="lblupsav" runat="server">
    <input id="lblstat" type="hidden" name="lblstat" runat="server">
    <input type="hidden" id="lblpmhid" runat="server"><input type="hidden" id="lblro"
        runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
