﻿Imports System.Data.SqlClient
Imports System.Web.UI.DataVisualization.Charting
Public Class ChartSampleTPM
    Inherits System.Web.UI.Page
    Dim ch As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim pmtskid, edate As String
    Dim hii, loi, speci As Decimal
    Dim hii_cnt, loi_cnt, speci_cnt, meas_cnt As Integer
    Dim hii_cnt_dist, loi_cnt_dist, speci_cnt_dist As Integer
    Dim pmid As String
    Dim freq As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
        Try
            ch.Open()
            pmtskid = Request.QueryString("pmtskid").ToString
            pmid = Request.QueryString("mid").ToString
            sql = "select freq from pm where pmid = '" & pmid & "'"
            freq = ch.Scalar(sql)
            'check spec counts
            sql = "select count(*) from pmTaskMeasDetManHISTtpm where pmtskid = '" & pmtskid & "' and measurement is not null"
            meas_cnt = ch.Scalar(sql)
            sql = "select count(*) from pmTaskMeasDetManHISTtpm where pmtskid = '" & pmtskid & "' and hi is not null"
            hii_cnt = ch.Scalar(sql)
            sql = "select count(*) from pmTaskMeasDetManHISTtpm where pmtskid = '" & pmtskid & "' and lo is not null"
            loi_cnt = ch.Scalar(sql)
            sql = "select count(*) from pmTaskMeasDetManHISTtpm where pmtskid = '" & pmtskid & "' and spec is not null"
            speci_cnt = ch.Scalar(sql)
            If hii_cnt < meas_cnt Then
                sql = "select isnull(hii, 0) as 'hii' from pmTaskMeasDetManHISTtpm where pmtskid = '" & pmtskid & "' and hii is not null group by hii having count(*) > 1"
                dr = ch.GetRdrData(sql)
                While dr.Read
                    hii = dr.Item("hii").ToString
                    If hii <> 0 Then
                        hii_cnt_dist += 1
                    End If
                End While
                dr.Close()
                If hii_cnt_dist > 1 Then
                    hii = 0
                End If
            Else
                sql = "select distinct hi from pmTaskMeasDetManHISTtpm where pmtskid = '" & pmtskid & "' and hi is not null"
                hii = ch.Scalar(sql)
            End If
            If loi_cnt < meas_cnt Then
                sql = "select isnull(lo, 0) as 'lo' from pmTaskMeasDetManHISTtpm where pmtskid = '" & pmtskid & "' and lo is not null group by lo having count(*) > 1"
                dr = ch.GetRdrData(sql)
                While dr.Read
                    loi = dr.Item("loi").ToString
                    If loi <> 0 Then
                        loi_cnt_dist += 1
                    End If
                End While
                dr.Close()
                If loi_cnt_dist > 1 Then
                    loi = 0
                End If
            Else
                sql = "select distinct lo from pmTaskMeasDetManHISTtpm where pmtskid = '" & pmtskid & "' and lo is not null"
                loi = ch.Scalar(sql)
            End If
            If speci_cnt < meas_cnt Then
                sql = "select isnull(spec, 0) as 'spec' from pmTaskMeasDetManHISTtpm where pmtskid = '" & pmtskid & "' and spec is not null group by speci having count(*) > 1"
                dr = ch.GetRdrData(sql)
                While dr.Read
                    speci = dr.Item("spec").ToString
                    If speci <> 0 Then
                        speci_cnt_dist += 1
                    End If
                End While
                dr.Close()
                If speci_cnt_dist > 1 Then
                    sql = "select distinct spec from pmTaskMeasDetManHISTtpm where pmtskid = '" & pmtskid & "' and spec is not null"
                    speci = ch.Scalar(sql)
                End If
            Else
                speci = 1
            End If

            sql = "declare @tbl table (hii decimal(10, 6), loi decimal(10,6), speci decimal(10, 6), measurement2 decimal(12,2), pmdate datetime) " _
                + "insert into @tbl " _
                + "select isnull(hi, '" & hii & "') as 'hii', isnull(lo, '" & loi & "') as 'lo', isnull(spec, '" & speci & "') as 'spec', measurement, pmdate from pmTaskMeasDetManHISTtpm where pmtskid = '" & pmtskid & "' order by pmdate desc " _
                + "select * from @tbl order by pmdate"

            dr = ch.GetRdrData(sql)
            Chart1.Series("Measurements").ChartType = SeriesChartType.Line
            Chart1.ChartAreas("MainChartArea").AxisX.IsMarginVisible = True
            Chart1.Series("Measurements")("ShowMarkerLines") = "True"
            Chart1.Series("Measurements").MarkerStyle = MarkerStyle.Square
            Chart1.ChartAreas(0).AxisX.Interval = 1


            If freq = 1 Then
                Chart1.ChartAreas(0).AxisX.IntervalType = DateTimeIntervalType.Days
            End If
            If freq > 1 And freq < 8 Then
                Chart1.ChartAreas(0).AxisX.IntervalType = DateTimeIntervalType.Weeks
            End If
            If freq > 27 And freq < 89 Then
                Chart1.ChartAreas(0).AxisX.IntervalType = DateTimeIntervalType.Months
            End If
            If freq >= 90 Then
                Chart1.ChartAreas(0).AxisX.IntervalType = DateTimeIntervalType.Years
            End If
            '
            Chart1.ChartAreas("MainChartArea").AxisX.TextOrientation = TextOrientation.Rotated270
            Chart1.Series("Measurements").SmartLabelStyle.Enabled = False
            Chart1.ChartAreas("MainChartArea").AxisX.LabelStyle.Angle = -90

            'Chart1.ChartAreas(0).AxisX.Minimum = 0
            ' Set Border Skin
            Chart1.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            ' Set Border Color
            Chart1.BorderSkin.BorderColor = Drawing.Color.Maroon
            ' Set Border Style
            Chart1.BorderSkin.BorderDashStyle = ChartDashStyle.Solid
            ' Set Border Width
            Chart1.BorderSkin.BorderWidth = 1
            Chart1.Legends("Default").BorderColor = Drawing.Color.Black
            Chart1.Legends("Default").BorderWidth = 1
            Chart1.Legends("Default").BorderDashStyle = ChartDashStyle.Solid
            Chart1.Legends("Default").ShadowOffset = 2

            Chart1.Series("Measurements").XValueMember = "pmdate"
            Chart1.Series("Measurements").YValueMembers = "measurement2"

            If hii <> 0 Then
                Chart1.Series("High").XValueMember = "pmdate"
                Chart1.Series("High").YValueMembers = "hii"
            End If

            If loi <> 0 Then
                Chart1.Series("Low").XValueMember = "pmdate"
                Chart1.Series("Low").YValueMembers = "loi"
            End If

            If speci <> 0 Then
                Chart1.Series("Spec").XValueMember = "pmdate"
                Chart1.Series("Spec").YValueMembers = "speci"
            End If


            Chart1.DataSource = dr
            Chart1.DataBind()
            dr.Close()
            ch.Dispose()
        Catch ex As Exception
            Dim strMessage As String = "Problem Creating Chart"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        

    End Sub

End Class