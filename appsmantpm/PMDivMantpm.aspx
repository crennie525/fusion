<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMDivMantpm.aspx.vb" Inherits="lucy_r12.PMDivMantpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMDivMan</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .FreezePaneOn6
        {
            position: absolute;
            top: 52px;
            left: 0px;
            visibility: visible;
            display: block;
            width: 760: 480px;
            background-color: #eeeded;
            z-index: 999;
            filter: alpha(opacity=35);
            -moz-opacity: 0.35;
            padding-top: 3%;
        }
        .FreezePaneOn7
        {
            position: absolute;
            top: 0px;
            left: 0px;
            visibility: visible;
            display: block;
            width: 760px;
            height: 480px;
            background-color: #eeeded;
            z-index: 999;
            filter: alpha(opacity=35);
            -moz-opacity: 0.35;
            padding-top: 3%;
        }
        .view2
        {
            visibility:visible;
        }
        .viewcell 
{ 
DISPLAY: table-cell;
VISIBILITY: visible; 
}
}
        .detailscell 
{ 
DISPLAY: table-cell;
VISIBILITY: hidden;
}
    </style>
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMDivMantpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
     <!--
        function checkit() {
            var nodown;
            var pmid = document.getElementById("lblpmid").value;
            var pmhid = document.getElementById("lblpmhid").value;
            var eqid = document.getElementById("lbleqid").value;
            var wo = document.getElementById("txtwonum").value;
            if (pmid != "") {
                window.parent.updatepmid(pmid, pmhid, eqid, wo);
            }
            var malert = document.getElementById("lblmalert").value;
            if (malert != "") {
                compalert(malert);
            }
            var alertstr = document.getElementById("lblalert").value;
            document.getElementById("lblalert").value = "";
            if (alertstr == "1" || alertstr == "2") {
                adjalert(alert);
            }
            else if (alertstr == "3") {
                document.getElementById("lblsubmit").value = "comp";
                FreezeScreenComp('Your Data Is Being Processed...');
                document.getElementById("form1").submit();
            }
            else if (alertstr == "4") {
                alert("Your New TPM Next Date is in the past\n Please Adjust if you believe this is incorrect")
                document.getElementById("lblsubmit").value = "comp";
                document.getElementById("form1").submit();
            }
            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                window.parent.doref();
            }
            else {
                window.parent.setref();
            }
        }
        function compalert(malert) {
            //alert(malert)
            var marr = malert.split(",");
            var mmsg;
            var hasflg = 0;
            mmsg = "This PM has ";
            for (i = 0; i < marr.length; i++) {
                if (marr[i] == "m") {
                    mmsg += "Measurements";
                }
                else if (marr[i] == "h") {
                    mmsg += "Task Times";
                }
                else if (marr[i] == "d") {
                    mmsg += "Down Times";
                    nodown = "yes"
                }
                //if(i!=marr.length) {
                //mmsg += ", ";
                //}
                //if(marr.length==1&&marr[i]!="m") {
                //hasflg = 1;
                //}
            }
            mmsg += " that have not been recorded\nDo you wish to Continue?";
            var conf = confirm(mmsg)
            if (conf == true) {
                window.parent.setref();
                //if (nodown == "yes") {
                //    document.getElementById("lblnodown").value = "yes";
                //}
                document.getElementById("lblmalert").value = "ok";
                document.getElementById("lblsubmit").value = "checkcomp"
                FreezeScreenComp('Your Data Is Being Processed...');
                document.getElementById("form1").submit();
            }
            else {
                document.getElementById("lblmalert").value = "";
                alert("Action Cancelled")
            }
        }
        function printwo() {
            var pm = document.getElementById("lblpmid").value;
            var wo = document.getElementById("lblwo").value;
            if (wo == "") {
                alert("No Work Order Assigned for this TPM")
            }
            else {
                window.parent.setref();
                getdocs();
                window.open("../appswo/woprint.aspx?typ=TPM&pm=" + pm + "&wo=" + wo, "repWin", popwin);
            }
        }
        function printpm() {
            window.parent.setref();
            getdocs();
            var pmid = document.getElementById("lblpmid").value;
            var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
            window.open("PrintPMtpm.aspx?pmid=" + pmid, "repWin", popwin);
        }

        function getdocs() {
            var cbs = document.getElementById("lbldocs").value;
            var cbsarr = cbs.split(";")
            for (var i = 0; i < cbsarr.length; i++) {
                //alert(i)
                var cbstr = cbsarr[i];
                //alert(cbstr)
                if (cbstr != "") {
                    var cbstrarr = cbstr.split("~")
                    var fnstr = cbstrarr[1]
                    var docstr = cbstrarr[0]
                    OpenFile(fnstr, docstr);
                }
            }
        }
        function getsuper(typ) {
            window.parent.setref();
            var skill = document.getElementById("lblskillid").value;
            var ro = document.getElementById("lblro").value;
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
            if (eReturn) {
                if (eReturn) {
                    if (eReturn != "log") {
                        if (eReturn != "") {
                            var retarr = eReturn.split(",")
                            if (typ == "sup") {
                                document.getElementById("lblsupid").value = retarr[0];
                                document.getElementById("lblsup").value = retarr[1];
                                document.getElementById("txtsup").value = retarr[1];
                                //document.getElementById("lblsubmit").value = "save";
                                //document.getElementById("form1").submit();
                            }
                            else {
                                document.getElementById("lblleadid").value = retarr[0];
                                document.getElementById("lbllead").value = retarr[1];
                                document.getElementById("txtlead").value = retarr[1];
                                //document.getElementById("lblsubmit").value = "save";
                                //document.getElementById("form1").submit();
                            }
                        }
                    }
                    else {
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        function getcal(fld) {
            var chk = document.getElementById("lblacnt").value;
            var calday = document.getElementById("lblcalday").value;
            var days = document.getElementById("lbldays").value;
            //alert(days)
            if (chk != "0") {
                var conf = confirm("Dates for this TPM have been Adjusted in the PM Scheduler\nAre you sure you want to continue?")
                if (conf == true) {
                    window.parent.setref();
                    var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld + "&calday=" + calday, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
                    if (eReturn) {
                        var retarr = eReturn.split(",")
                        var fldret = "txt" + fld;
                        if (days == "no") {
                            document.getElementById(fldret).value = retarr[0];
                        }
                        else {

                            document.getElementById(fldret).value = retarr[0];
                            document.getElementById("lblnext").value = retarr[0];
                            document.getElementById("lblretday").value = retarr[1];
                        }
                        document.getElementById("lblschk").value = "1";
                        //if(fld=="n") {
                        //document.getElementById("lblnext").value = eReturn;
                        if (fld == "s") {
                            document.getElementById("lblstart").value = retarr[0];
                            checknext();
                        }
                    }
                }
                else {
                    alert("Action Cancelled")
                }
            }
            else {
                window.parent.setref();
                var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld + "&days=" + days, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
                if (eReturn) {
                    var retarr = eReturn.split(",")
                    var fldret = "txt" + fld;
                    if (days == "no") {
                        document.getElementById(fldret).value = retarr[0];
                    }
                    else {

                        document.getElementById(fldret).value = retarr[0];
                        document.getElementById("lblretday").value = retarr[1];
                    }
                    //document.getElementById(fldret).value = eReturn;
                    document.getElementById("lblschk").value = "1";
                    if (fld == "n") {
                        if (days == "no") {
                            document.getElementById(fldret).value = retarr[0];
                        }
                        else {
                            var retarr = eReturn.split(",")
                            document.getElementById("lblnext").value = retarr[0];
                            document.getElementById("lblretday").value = retarr[1];
                        }
                        //document.getElementById("lblnext").value = eReturn;
                    }
                    if (fld == "s") {
                        document.getElementById("lblstart").value = retarr[0];
                        checknext();
                    }
                }
            }
        }

        function savetask() {
            document.getElementById("lblsubmit").value = "save";
            document.getElementById("form1").submit();
        }
        function checknext() {
            var cbs = document.getElementById("cbs");
            if (cbs.checked == true) {
                var nxt = document.getElementById("lblnext").value;
                if (nxt == "") {
                    var nnxt = document.getElementById("lblstart").value;
                    document.getElementById("txtn").value = nnxt;
                    document.getElementById("lblnext").value = nnxt;
                }
            }
        }
        function checkdel(who) {
            var conf
            if (who == "lead") {
                conf = confirm("Are you sure you want to Delete this Lead Craft?")
            }
            else if (who == "sup") {
                conf = confirm("Are you sure you want to Delete this Supervisor?")
            }
            if (conf == true) {
                if (who == "lead") {
                    document.getElementById("lblleadid").value = ""
                    document.getElementById("lbllead").value = ""
                    document.getElementById("txtlead").value = ""
                    document.getElementById("lblsubmit").value = "save"
                    document.getElementById("form1").submit();
                }
                else if (who == "sup") {
                    document.getElementById("lblsupid").value = ""
                    document.getElementById("lblsup").value = ""
                    document.getElementById("txtsup").value = ""
                    document.getElementById("lblsubmit").value = "save"
                    document.getElementById("form1").submit();
                }
            }
            else {
                alert("Action Cancelled")
            }
        }
        function checkoff() {
            var cbs = document.getElementById("cboff");
            var nxt = document.getElementById("lblnext").value;
            if (cbs.checked == true) {
                document.getElementById("txtn").value = "";
                document.getElementById("lblnext").value = "";
                document.getElementById("lblsubmit").value = "save";
                document.getElementById("form1").submit();
            }
        }
        function checkcomp() {
            var wo = document.getElementById("lblwo").value;
            var next = document.getElementById("lblnext").value;
            var cbs = document.getElementById("cboff");
            if (next != "") {
                if (wo != "") {
                    document.getElementById("lblsubmit").value = "checkcomp";
                    FreezeScreenComp('Your Data Is Being Processed...');
                    document.getElementById("form1").submit();
                }
                else {
                    alert("No PM Work Order Created")
                }
            }
            else {
                if (cbs.checked == true) {
                    if (wo != "") {
                        document.getElementById("lblsubmit").value = "checkcomp";
                        FreezeScreenComp('Your Data Is Being Processed...');
                        document.getElementById("form1").submit();
                    }
                    else {
                        alert("No PM Work Order Created")
                    }
                }
                else {
                    alert("No Next Date Selected for this PM")
                }

            }
        }
        function FreezeScreenComp(msg) {
            scroll(0, 0);
            var outerPane = document.getElementById('FreezePane');
            var innerPane = document.getElementById('InnerFreezePane');
            if (outerPane) outerPane.className = 'FreezePaneOn7';
            if (innerPane) innerPane.innerHTML = msg;

            var outerPane1 = document.getElementById('FreezePane1');
            var innerPane1 = document.getElementById('InnerFreezePane1');
            //if (outerPane1) outerPane1.className = 'FreezePaneOn5';
            //if (innerPane1) innerPane1.innerHTML = msg;
        }
         //-->
    </script>
</head>
<body onload="checkit();" class="tbg">
    <form id="form1" method="post" runat="server">
    <div id="FreezePane" class="FreezePaneOff" align="center">
        <div id="InnerFreezePane" class="InnerFreezePane">
        </div>
    </div>
    <div id="FreezePane1" class="FreezePaneOff" align="center">
        <div id="InnerFreezePane1" class="InnerFreezePane">
        </div>
    </div>
    <table style="position: absolute; top: 0px; left: 0px" cellspacing="0" width="760">
        <tr height="24">
            <td class="thdrsinglft" width="26">
                <img src="../images/appbuttons/minibuttons/pmgridhdr.gif" border="0">
            </td>
            <td class="thdrsingrt label" align="left" width="734">
                <asp:Label ID="lang802" runat="server">TPM Details</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table cellspacing="2" width="760">
                    <tr class="tbg" height="20">
                        <td class="btmmenulft plainlabel" width="400">
                            <asp:Label ID="lang803" runat="server">TPM</asp:Label>
                        </td>
                        <td class="btmmenulft plainlabel" width="120">
                            <asp:Label ID="lang804" runat="server">Last Date</asp:Label>
                        </td>
                        <td class="btmmenulft plainlabel" width="120">
                            <asp:Label ID="lang805" runat="server">Next Date</asp:Label>
                        </td>
                        <td class="btmmenulft plainlabel" width="50">
                            <asp:Label ID="lang806" runat="server">Status</asp:Label>
                        </td>
                        <td class="btmmenulft plainlabel" width="70">
                            <asp:Label ID="lang807" runat="server">TPM Count</asp:Label>
                        </td>
                    </tr>
                    <tr height="20">
                        <td class="label" id="pmtitle" runat="server">
                        </td>
                        <td class="label" id="pmlast" runat="server">
                        </td>
                        <td class="label" id="pmnext" runat="server">
                        </td>
                        <td class="plainlabelblue" id="tdstatus" runat="server" align="center">
                        </td>
                        <td class="plainlabelblue" id="tdcnt" runat="server" align="center">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table cellspacing="1" cellpadding="1" width="760">
                    <tr>
                        <td width="78">
                        </td>
                        <td width="102">
                        </td>
                        <td width="22">
                        </td>
                        <td width="98">
                        </td>
                        <td width="90">
                        </td>
                        <td width="102">
                        </td>
                        <td width="34">
                        </td>
                        <td width="72">
                        </td>
                        <td width="24">
                        </td>
                        <td width="86">
                        </td>
                        <td width="50">
                        </td>
                        
                    </tr>
                    <tr height="20">
                        <td class="thdrsingg plainlabel" colspan="4">
                            <asp:Label ID="lang808" runat="server">Scheduling Information</asp:Label>
                        </td>
                        <td class="thdrsingg plainlabel" colspan="3">
                            <asp:Label ID="lang809" runat="server">Charge Information</asp:Label>
                        </td>
                        <td class="thdrsingg plainlabel" colspan="5">
                            <asp:Label ID="lang810" runat="server">Responsibility</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel">
                            <asp:Label ID="lang811" runat="server">Start Date</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txts" runat="server" Width="100px" ReadOnly="True" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcal('s');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg">
                        </td>
                        <td class="plainlabelblue">
                            <asp:Label ID="lang812" runat="server">Use Start?</asp:Label><input id="cbs" onclick="checknext();"
                                type="checkbox" runat="server" name="cbs">
                        </td>
                        <td class="bluelabel">
                            <asp:Label ID="lang813" runat="server">Work Order#</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtwonum" runat="server" Width="100px" ReadOnly="True" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td>
                           
                                <input id="cbsupe" type="checkbox" name="cbsupe" runat="server" onmouseover="return overlib('Check to send email alert to Supervisor')"
                                onmouseout="return nd()">
                        </td>
                        <td class="bluelabel">
                             <asp:Label ID="lang814" runat="server">Supervisor</asp:Label>
                        </td>
                       
                        <td colspan="2">
                            <asp:TextBox ID="txtsup" runat="server" Width="130px" ReadOnly="True" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getsuper('sup');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                border="0">
                                <img onclick="checkdel('sup');" alt="" src="../images/appbuttons/minibuttons/del.gif"
                                height="12px" width="12px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel">
                            <asp:Label ID="lang815" runat="server">Next Date</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtn" runat="server" Width="100px" ReadOnly="True" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcal('n');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg">
                        </td>
                        <td class="plainlabelblue">
                            <input type="checkbox" id="cboff" runat="server" onclick="checkoff();" />Off
                        </td>
                        <td class="bluelabel" colspan="2">
                            <asp:Label ID="lang816" runat="server">Charge#</asp:Label>
                        </td>
                        <td>
                            <input id="cbleade" type="checkbox" name="cbleade" runat="server" onmouseover="return overlib('Check to send email alert to Lead Craft')"
                                onmouseout="return nd()">
                        </td>
                        <td class="bluelabel">
                            <asp:Label ID="lang817" runat="server">Lead Craft</asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtlead" runat="server" Width="130px" ReadOnly="True" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getsuper('lead');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                border="0">
                                <img onclick="checkdel('lead');" alt="" src="../images/appbuttons/minibuttons/del.gif"
                                height="12px" width="12px" />
                        </td>
                    </tr>
                    <tr height="20">
                        <td colspan="4" rowspan="2" align="center" valign="middle">
                            <table cellspacing="1" cellpadding="0">
                                <tr id="trshiftdays" runat="server">
                                    <td class="label" onmouseover="return overlib('Used to indicate Shift designation')"
                                        onmouseout="return nd()" align="center">
                                        <asp:Label ID="lang818" runat="server">Shift</asp:Label>
                                    </td>
                                    <td class="label">
                                    </td>
                                    <td class="label" align="center" width="23">
                                        M
                                    </td>
                                    <td class="label" align="center" width="23">
                                        Tu
                                    </td>
                                    <td class="label" align="center" width="23">
                                        W
                                    </td>
                                    <td class="label" align="center" width="23">
                                        Th
                                    </td>
                                    <td class="label" align="center" width="23">
                                        F
                                    </td>
                                    <td class="label" align="center" width="23">
                                        Sa
                                    </td>
                                    <td class="label" align="center" width="23">
                                        Su
                                    </td>
                                </tr>
                                <tr id="trshift1" runat="server">
                                    <td>
                                        <input id="cb1o" type="checkbox" name="cb1o" runat="server">
                                    </td>
                                    <td class="label">
                                        1st
                                    </td>
                                    <td align="center">
                                        <input id="cb1mon" type="checkbox" name="Checkbox1" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb1tue" type="checkbox" name="Checkbox2" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb1wed" type="checkbox" name="Checkbox3" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb1thu" type="checkbox" name="Checkbox4" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb1fri" type="checkbox" name="Checkbox5" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb1sat" type="checkbox" name="Checkbox6" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb1sun" type="checkbox" name="Checkbox7" runat="server">
                                    </td>
                                </tr>
                                <tr id="trshift2" runat="server">
                                    <td>
                                        <input id="cb2o" type="checkbox" name="cb1o" runat="server">
                                    </td>
                                    <td class="label">
                                        2nd
                                    </td>
                                    <td align="center">
                                        <input id="cb2mon" type="checkbox" name="Checkbox1" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb2tue" type="checkbox" name="Checkbox2" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb2wed" type="checkbox" name="Checkbox3" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb2thu" type="checkbox" name="Checkbox4" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb2fri" type="checkbox" name="Checkbox5" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb2sat" type="checkbox" name="Checkbox6" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb2sun" type="checkbox" name="Checkbox7" runat="server">
                                    </td>
                                </tr>
                                <tr id="trshift3" runat="server">
                                    <td>
                                        <input id="cb3o" type="checkbox" name="cb1o" runat="server">
                                    </td>
                                    <td class="label">
                                        3rd
                                    </td>
                                    <td align="center">
                                        <input id="cb3mon" type="checkbox" name="Checkbox1" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb3tue" type="checkbox" name="Checkbox2" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb3wed" type="checkbox" name="Checkbox3" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb3thu" type="checkbox" name="Checkbox4" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb3fri" type="checkbox" name="Checkbox5" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb3sat" type="checkbox" name="Checkbox6" runat="server">
                                    </td>
                                    <td align="center">
                                        <input id="cb3sun" type="checkbox" name="Checkbox7" runat="server">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtcharge" runat="server" Width="190px" ReadOnly="True" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcharge('wo');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                border="0">
                        </td>
                        <td colspan="3" class="bluelabel">
                            <asp:Label ID="lang819" runat="server">Email Lead Time (Days)</asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtleadtime" runat="server" Width="45px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                    </tr>
                    <tr height="24">
                        <td colspan="8" align="right" valign="bottom">
                            <img id="imgsav" onmouseover="return overlib('Save Changes Above', ABOVE, LEFT)"
                                onclick="savetask();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                width="20" runat="server">
                            <img onmouseover="return overlib('Return to Navigation Grid', ABOVE, LEFT)" onclick="gridret();"
                                onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/bgbuttons/navgrid.gif"
                                width="20">
                            <img id="Img1" onmouseover="return overlib('View TPM History', ABOVE, LEFT)" onclick="gethist();"
                                onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/medclock.gif"
                                border="0" runat="server">
                            <img id="Img2" onmouseover="return overlib('View Failure Mode History', ABOVE, LEFT)"
                                onclick="getfmhist();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/fmhist.gif"
                                border="0" runat="server"><img id="imgmeas" onmouseover="return overlib('View Measurements for this TPM')"
                                    onclick="getMeasDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/measure.gif"
                                    width="27" runat="server">
                            <img id="btnedittask" onmouseover="return overlib('View or Edit Pass/Fail Adjustment Levels - View Current Pass/Fail Counts', ABOVE, LEFT)"
                                onclick="editadj();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/screwdriver.gif"
                                border="0" runat="server">
                            <img class="details" id="Img3" onmouseover="return overlib('Adjust Email Lead Time', ABOVE, LEFT)"
                                onclick="editlead();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/emailadmin.gif"
                                border="0" runat="server"><img class="details" onmouseover="return overlib('Add Attachments', ABOVE, LEFT)"
                                    onclick="getattach();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/attach.gif">
                            <img onmouseover="return overlib('Print Current TPM', ABOVE, LEFT)" onclick="printpm();"
                                onmouseout="return nd()" src="../images/appbuttons/minibuttons/printex.gif">&nbsp;<img
                                    id="imgi2" onmouseover="return overlib('Print This TPM Work Order', ABOVE, LEFT)"
                                    onclick="printwo();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/woprint.gif"
                                    runat="server">
                            <img id="imgcomp" onmouseover="return overlib('Complete this TPM', ABOVE, LEFT)"
                                onclick="checkcomp();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/comp.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="20">
            <td class="thdrsinglft" style="height: 24px" width="26">
                <img src="../images/appbuttons/minibuttons/pmgridhdr.gif" border="0">
            </td>
            <td class="thdrsingrt label" style="height: 24px" align="left" width="724">
                <asp:Label ID="lang820" runat="server">TPM Tasks</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="tdtasks" style="width: 740px; height: 220px; overflow: auto" runat="server">
                </div>
            </td>
        </tr>
    </table>
    <input id="lblpmid" type="hidden" runat="server">
    <input id="lblsubmit" type="hidden" runat="server">
    <input id="lblpmhid" type="hidden" runat="server"><input id="lbleqid" type="hidden"
        runat="server">
    <input id="lblnext" type="hidden" runat="server"><input id="lblschk" type="hidden"
        runat="server">
    <input id="lblsup" type="hidden" runat="server">
    <input id="lbllead" type="hidden" runat="server">
    <input id="lblskillid" type="hidden" runat="server">
    <input id="lblacnt" type="hidden" runat="server">
    <input id="txtsearch" type="hidden" runat="server">
    <input id="lblwo" type="hidden" runat="server">
    <input id="lbllog" type="hidden" runat="server">
    <input id="lblmalert" type="hidden" runat="server">
    <input id="lblalert" type="hidden" runat="server">
    <input id="lbldocs" type="hidden" runat="server">
    <input id="lblondate" type="hidden" runat="server"><input id="lblro" type="hidden"
        runat="server">
    <input id="lblfreq" type="hidden" runat="server"><input type="hidden" id="lblcalday"
        runat="server">
    <input type="hidden" id="lblcallimit" runat="server">
    <input type="hidden" id="lblfcust" runat="server">
    <input type="hidden" id="lbldays" runat="server"><input type="hidden" id="lblretday"
        runat="server">
    <input id="lblusetdt" type="hidden" runat="server" name="lblusetdt">
    <input id="lblusetotal" type="hidden" runat="server" name="lblusetotal">
    <input id="lblttime" type="hidden" runat="server" name="lblttime">
    <input id="lbldtime" type="hidden" runat="server" name="lbldtime">
    <input id="lblacttime" type="hidden" runat="server" name="lblacttime">
    <input id="lblactdtime" type="hidden" runat="server" name="lblactdtime"><input type="hidden"
        id="lblhalert" runat="server" name="lblhalert">
    <input type="hidden" id="lbldalert" runat="server" name="lbldalert">
    <input type="hidden" id="lblstart" runat="server" />
    <input type="hidden" id="lblleadid" runat="server" />
    <input type="hidden" id="lblsupid" runat="server" />
    <input type="hidden" id="lblissched" runat="server" />
    <input id="lblusesched" type="hidden" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblusername" runat="server" />
    <input type="hidden" id="lblisactive" runat="server" />
    <input type="hidden" id="lblcoi" runat="server" />
    <input type="hidden" id="lblnodown" runat="server" />
     <input type="hidden" id="lblerrcnt" runat="server" />
    <input type="hidden" id="lbllcnt" runat="server" />
    <input type="hidden" id="lblMWNO" runat="server" />
    </form>
</body>
</html>
