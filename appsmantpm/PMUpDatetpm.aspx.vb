

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMUpDatetpm
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim up As New Utilities
    Dim sql, eqid As String
    Protected WithEvents btnyes As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdmsg As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            up.Open()
            CheckCurr(eqid)
            up.Dispose()
        End If
    End Sub
    Private Sub CheckCurr(ByVal eqid As String)
        sql = "select count(*) from TPM where eqid = '" & eqid & "' and nextdate is not null"
        Dim ecnt As Integer
        ecnt = up.Scalar(sql)
        If ecnt <> 0 Then
            tdmsg.InnerHtml = "You have TPM Records with pending due dates.<br>" _
            + "Performing this update could change task requirements.<br>" _
            + "Do you wish to continue?"
        Else
            tdmsg.InnerHtml = "Performing this update could change task requirements.<br>" _
            + "Do you wish to continue?"
        End If
    End Sub

    Private Sub btnyes_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnyes.ServerClick
        eqid = lbleqid.Value

        up.Open()
        sql = "usp_pmrevtpm '" & eqid & "'"
        up.Update(sql)
        up.Dispose()
        lblret.Value = "yes"
    End Sub
End Class
