<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmfmtpm.aspx.vb" Inherits="lucy_r12.pmfmtpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>pmfm</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmfmtpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        <!--
        function gettsk(pmid, task) {
            var eReturn = window.showModalDialog("../appsmantpm/fmtasktpmdialog.aspx?pmid=" + pmid + "&task=" + task, "", "dialogHeight:300px; dialogWidth:500px; resizable=yes");
            /*
            var ycoord = document.getElementById("yCoord").value;
            if (ycoord != "") {
            var tp = parseInt(ycoord) + 10;
            tp = tp + "px"
            document.getElementById("tskdiv").style.top = tp;
            }
            else {
            document.getElementById("tskdiv").style.top = "10px";
            }
            document.getElementById("tskdiv").style.left = "10px";
            document.getElementById("tskdiv").className = "view";
            document.getElementById("txttsk").value = tsk;
            */
        }
        function closetsk() {
            document.getElementById("tskdiv").className = "details";
        }
        function checkfail(val, pmid, pmhid, pmtid, pmfid, fm, fmid, task, comid) {
            var rb = document.getElementById(val).value;
            var ro = document.getElementById("lblro").value;
            var sid = document.getElementById("lblsid").value;
            if (rb == "2") {
                var eReturn = window.showModalDialog("PMFailManDialogtpm.aspx?pmid=" + pmid + "&pmhid=" + pmhid + "&pmtid=" + pmtid + "&pmfid=" + pmfid + "&fm=" + fm + "&fmid=" + fmid + "&task=" + task + "&comid=" + comid + "&date=" + Date() + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:320px; dialogWidth:790px; resizable=yes");
                if (eReturn) {

                }
            }

        }
            //-->
    </script>
</head>
<body onload="scrolltop();checkit();" class="tbg">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" cellspacing="0" cellpadding="2" width="700">
        <tbody>
            <tr>
                <td>
                    <table width="770">
                        <tr height="22">
                            <td class="label" width="80">
                                <asp:Label ID="lang834" runat="server">Work Order#</asp:Label>
                            </td>
                            <td class="plainlabel" id="tdwo" width="140" runat="server">
                            </td>
                            <td class="plainlabel" id="tdwod" width="450" runat="server">
                            </td>
                            <td align="right" width="50">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="22">
                <td class="thdrsing label">
                    <asp:Label ID="lang835" runat="server">PM Task Failure Modes</asp:Label>
                </td>
            </tr>
            <tbody>
                <tr>
                    <td align="center">
                        <asp:DataList ID="dltasks" runat="server">
                            <HeaderTemplate>
                                <table width="880">
                                    <tr>
                                        <td width="60">
                                        </td>
                                        <td width="40">
                                        </td>
                                        <td width="20">
                                        </td>
                                        <td width="100">
                                        </td>
                                        <td width="50">
                                        </td>
                                        <td width="100">
                                        </td>
                                        <td width="50">
                                        </td>
                                        <td width="100">
                                        </td>
                                        <td width="50">
                                        </td>
                                        <td width="100">
                                        </td>
                                        <td width="50">
                                        </td>
                                        <td width="100">
                                        </td>
                                        <td width="50">
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr id="trsep" runat="server">
                                    <td colspan="13">
                                        <hr>
                                    </td>
                                </tr>
                                <tr id="trfunc" runat="server">
                                    <td class="label">
                                        <asp:Label ID="lang836" runat="server">Function:</asp:Label>
                                    </td>
                                    <td class="plainlabel" colspan="8">
                                        <%# DataBinder.Eval(Container, "DataItem.func") %>
                                    </td>
                                    <td>
                                        <asp:Label ID="A1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtid") %>'
                                            CssClass="details">
                                        </asp:Label>
                                        <asp:Label ID="A2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fmcnt") %>'
                                            CssClass="details">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr id="trco" runat="server">
                                    <td class="label">
                                        <asp:Label ID="lang837" runat="server">Component:</asp:Label>
                                    </td>
                                    <td class="plainlabel" colspan="10">
                                        <%# DataBinder.Eval(Container, "DataItem.compnum") %>
                                    </td>
                                </tr>
                                <tr id="trhdr" runat="server">
                                    <td class="thdrsing plainlabel">
                                        <asp:Label ID="lang838" runat="server">Edit</asp:Label>
                                    </td>
                                    <td class="thdrsing plainlabel">
                                        <asp:Label ID="lang839" runat="server">Task#</asp:Label>
                                    </td>
                                    <td class="thdrsing plainlabel">
                                        <img src="../images/appbuttons/minibuttons/magnifier.gif">
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM1
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang840" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM2
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang841" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM3
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang842" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM4
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang843" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM5
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang844" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                </tr>
                                <tr id="trtask" runat="server">
                                    <td>
                                        <asp:ImageButton ID="Imagebutton1" runat="server" ToolTip="Edit Record" CommandName="Edit"
                                            ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"></asp:ImageButton>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label ID="lbltasknume" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'
                                            CssClass="plainlabel">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <img src="../images/appbuttons/minibuttons/magnifier.gif" id="imgti" runat="server">
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm1s") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm1dd") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm2s") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm2dd") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm3s") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm3dd") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm4s") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm4dd") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm5s") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm5dd") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <tr id="trsepe" runat="server">
                                    <td colspan="13">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr id="trfunce" runat="server">
                                    <td class="label">
                                        <asp:Label ID="lang845" runat="server">Function:</asp:Label>
                                    </td>
                                    <td class="plainlabel" colspan="10">
                                        <%# DataBinder.Eval(Container, "DataItem.func") %>
                                    </td>
                                </tr>
                                <tr id="trcoe" runat="server">
                                    <td class="label">
                                        <asp:Label ID="lang846" runat="server">Component:</asp:Label>
                                    </td>
                                    <td class="plainlabel" colspan="10">
                                        <%# DataBinder.Eval(Container, "DataItem.compnum") %>
                                    </td>
                                </tr>
                                <tr id="Tr2" runat="server">
                                    <td class="thdrsing plainlabel">
                                        <asp:Label ID="lang847" runat="server">Edit</asp:Label>
                                    </td>
                                    <td class="thdrsing plainlabel">
                                        <asp:Label ID="lang848" runat="server">Task#</asp:Label>
                                    </td>
                                    <td class="thdrsing plainlabel">
                                        <img src="../images/appbuttons/minibuttons/magnifier.gif">
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM1
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang849" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM2
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang850" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM3
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang851" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM4
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang852" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM5
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang853" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="Imagebutton2" runat="server" ToolTip="Save Changes" CommandName="Update"
                                            ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:ImageButton>
                                        <asp:ImageButton ID="Imagebutton3" runat="server" ToolTip="Cancel Changes" CommandName="Cancel"
                                            ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"></asp:ImageButton>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.tasknum") %>
                                    </td>
                                    <td>
                                        <img src="../images/appbuttons/minibuttons/magnifier.gif" id="imgte" runat="server">
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm1s") %>
                                    </td>
                                    <td class="plainlabel" bgcolor="#E7F1FD">
                                        <asp:DropDownList ID="ddfm1" CssClass="plainlabel" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm1dd")) %>'>
                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="plainlabel" bgcolor="#E7F1FD">
                                        <%# DataBinder.Eval(Container, "DataItem.fm2s") %>
                                    </td>
                                    <td class="plainlabel" bgcolor="#E7F1FD">
                                        <asp:DropDownList ID="ddfm2" CssClass="plainlabel" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm2dd")) %>'>
                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm3s") %>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:DropDownList ID="ddfm3" CssClass="plainlabel" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm3dd")) %>'>
                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="plainlabel" bgcolor="#E7F1FD">
                                        <%# DataBinder.Eval(Container, "DataItem.fm4s") %>
                                    </td>
                                    <td class="plainlabel" bgcolor="#E7F1FD">
                                        <asp:DropDownList ID="ddfm4" CssClass="plainlabel" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm4dd")) %>'>
                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm5s") %>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:DropDownList ID="ddfm5" CssClass="plainlabel" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm5dd")) %>'>
                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </EditItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:DataList>
                    </td>
                </tr>
            </tbody>
    </table>
    <div class="details" id="tskdiv" style="z-index: 999; border-bottom: black 1px solid;
        position: absolute; border-left: black 1px solid; width: 450px; height: 180px;
        border-top: black 1px solid; border-right: black 1px solid">
        <table cellspacing="0" cellpadding="0" width="450" bgcolor="white">
            <tr bgcolor="blue">
                <td>
                    &nbsp;
                    <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
                        ForeColor="White">Task Description</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closetsk();" alt="" src="../images/appbuttons/minibuttons/close.gif"><br>
                </td>
            </tr>
            <tr class="tbg" height="30">
                <td style="padding-bottom: 3px; padding-left: 3px; padding-right: 3px; padding-top: 3px"
                    colspan="2">
                    <asp:TextBox ID="txttsk" runat="server" TextMode="MultiLine" ReadOnly="True" Width="440px"
                        CssClass="plainlabel" Height="170px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblpmid" type="hidden" runat="server" name="lblpmid">
    <input id="lblpmhid" type="hidden" runat="server" name="lblpmhid">
    <input id="lblrow" type="hidden" runat="server" name="lblrow">
    <input id="lblfmcnt" type="hidden" runat="server" name="lblfmcnt"><input id="lblcurrcnt"
        type="hidden" runat="server" name="lblcurrcnt">
    <input id="lbltaskcnt" type="hidden" runat="server" name="lbltaskcnt"><input id="lbltaskcurrcnt"
        type="hidden" runat="server" name="lbltaskcurrcnt">
    <input id="lblokcnt" type="hidden" runat="server" name="lblokcnt">
    <input id="lblokadj" type="hidden" runat="server" name="lblokadj">
    <input id="lbluseadj" type="hidden" runat="server" name="lbluseadj">
    <input id="lblfcnt" type="hidden" runat="server" name="lblfcnt">
    <input id="lblfadj" type="hidden" runat="server" name="lblfadj">
    <input id="lblusefadj" type="hidden" runat="server" name="lblusefadj">
    <input id="lblsubmit" type="hidden" runat="server" name="lblsubmit"><input id="lbltasknum"
        type="hidden" runat="server" name="lbltasknum">
    <input id="lblalert" type="hidden" runat="server" name="lblalert">
    <input id="xCoord" type="hidden" runat="server" name="xCoord"><input id="yCoord"
        type="hidden" runat="server" name="yCoord">
    <input type="hidden" id="lblmflg" runat="server" name="lblmflg"><input type="hidden"
        id="lblmcomp" runat="server" name="lblmcomp">
    <input type="hidden" id="ealert" runat="server" name="ealert">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lbllog" runat="server" name="lbllog">
    <input type="hidden" id="lblwo" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    </form>
</body>
</html>
