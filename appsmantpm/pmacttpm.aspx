<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmacttpm.aspx.vb" Inherits="lucy_r12.pmacttpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>pmact</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmacttpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
     <!--
        function getcal(fld) {
            var ro = document.getElementById("lblro").value;
            if (ro != "1") {
                var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
                if (eReturn) {
                    var newfld = "txt" + fld;
                    var lblfld = "lbl" + fld;
                    document.getElementById("lblupsav").value = "yes"
                    document.getElementById(newfld).value = eReturn;
                    document.getElementById(lblfld).value = eReturn;
                    measalert(fld);
                }
            }
        }
        function savedtime() {
            var exd = document.getElementById("lblexd").value;
            if (exd != "Y") {
                var wo = document.getElementById("lblwo").value;
                if (wo != "") {
                    var chk = document.getElementById("txttdt").value;
                    if (isNaN(chk)) {
                        alert("Total Down Time is Not a Number")
                    }
                    else {
                        document.getElementById("lblsubmit").value = "savedtime";
                        document.getElementById("form1").submit();
                    }
                }
                else {
                    alert("No PM Work Order Created")
                }
            }
        }
        function savedtimep() {
            var exd = document.getElementById("lblexdd").value;
            if (exd != "Y") {
                var wo = document.getElementById("lblwo").value;
                if (wo != "") {
                    var chk = document.getElementById("txttdt").value;
                    if (isNaN(chk)) {
                        alert("Total Down Time is Not a Number")
                    }
                    else {
                        document.getElementById("lblsubmit").value = "savedtime";
                        document.getElementById("form1").submit();
                    }
                }
                else {
                    alert("No PM Work Order Created")
                }
            }
        }
         //-->
    </script>
</head>
<body class="tbg" onload="scrolltop();checkit();">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" cellspacing="0" cellpadding="2" width="1200">
        <tr height="24">
            <td style="height: 26px" class="thdrsing label" colspan="7">
                123<asp:Label ID="lang778" runat="server">TPM Work Order Details</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <table>
                    <tr height="24">
                        <td class="label" width="120">
                            <asp:Label ID="lang779" runat="server">Target Start:</asp:Label>
                        </td>
                        <td id="tdstart" class="plainlabel" width="120" runat="server">
                        </td>
                        <td width="30">
                        </td>
                        <td class="label" width="140">
                            <asp:Label ID="lang780" runat="server">Target Complete:</asp:Label>
                        </td>
                        <td id="Td2" class="plainlabel" width="120" runat="server">
                        </td>
                        <td width="30">
                        </td>
                        <td id="tdcomp" align="right" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="lang781" runat="server">Actual Start:</asp:Label>
                        </td>
                        <td id="tdactstart" class="plainlabel" runat="server">
                            <asp:TextBox ID="txts" runat="server" CssClass="plainlabel" Width="100px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcal('s');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg">
                        </td>
                        <td class="label">
                            <asp:Label ID="lang782" runat="server">Actual Complete:</asp:Label>
                        </td>
                        <td id="tdactfinish" class="plainlabel" runat="server">
                            <asp:TextBox ID="txtf" runat="server" CssClass="plainlabel" Width="100px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcal('f');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg">
                        </td>
                    </tr>
                    <tr height="24">
                        <td class="label">
                            <asp:Label ID="lang783" runat="server">Est Total Task Time:</asp:Label>
                        </td>
                        <td id="tdest" class="plainlabel" colspan="2" runat="server">
                        </td>
                        <td class="label">
                            <asp:Label ID="lang784" runat="server">Actual Total Task Time:</asp:Label>
                        </td>
                        <td id="tdact" class="plainlabel" runat="server">
                        </td>
                        <td id="tdttt" runat="server" class="details">
                            <asp:TextBox Style="z-index: 0" ID="txtttt" runat="server" Width="100px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td id="tdsavettt" runat="server">
                            <img src="../images/appbuttons/minibuttons/savedisk1.gif" onclick="savettime();">
                        </td>
                    </tr>
                    <tr height="24" id="trtdt" runat="server">
                        <td class="label">
                            <asp:Label ID="lang785" runat="server">Est Total Down Time:</asp:Label>
                        </td>
                        <td id="tdestdt" class="plainlabel" colspan="2" runat="server">
                        </td>
                        <td class="label">
                            <asp:Label ID="lang786" runat="server">Actual Total Down Time</asp:Label>
                        </td>
                        <td id="tddtt" runat="server" class="plainlabel">
                        </td>
                        <td id="tdtdt" runat="server" class="details">
                            <asp:TextBox Style="z-index: 0" ID="txttdt" runat="server" Width="100px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td id="tdsavedt" runat="server">
                            <img src="../images/appbuttons/minibuttons/savedisk1.gif" onclick="savedtime();">
                        </td>
                        <td class="label" id="lblactdt" runat="server">Actual Total Production Down Time</td>
                        <td id="tddttp" runat="server" class="plainlabel">
                        </td>
                        <td id="tdtdtp" runat="server" class="details">
                            <asp:TextBox Style="z-index: 0" ID="txttdtp" runat="server" Width="100px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td id="tdsavedtp" runat="server">
                            <img src="../images/appbuttons/minibuttons/savedisk1.gif" onclick="savedtimep();">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="thdrsing label" colspan="7"  height="24">
                <asp:Label ID="lang787" runat="server">TPM Task Labor Usage</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <asp:DataGrid ID="dghours" runat="server" Width="1240px" AutoGenerateColumns="False"
                    BackColor="Transparent">
                    <FooterStyle Width="1240px" BackColor="Transparent"></FooterStyle>
                    <EditItemStyle Height="15px"></EditItemStyle>
                    <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgeditfm" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Function">
                            <HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label13" runat="server" Width="140px" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label14" runat="server" Width="140px" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Component">
                            <HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label15" runat="server" Width="140px" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label16" runat="server" Width="140px" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Task#">
                            <HeaderStyle Width="30px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label3" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Complete">
                            <HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblcompl" runat="server" Width="30px">
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <input type="checkbox" id="cbcompl" runat="server" />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Task Description">
                            <HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label4a" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.task") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label3a" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.task") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Skill Required">
                            <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label2" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Qty">
                            <HeaderStyle Width="30px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label6" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Skill Min Ea">
                            <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="txtqty" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Actual Time (Min)">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.acttime") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtatime" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.acttime") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="R\D">
                            <HeaderStyle Width="30px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.rd") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label10" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.rd") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Actual R\D (Min)">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label11" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.actrd") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtactrd" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.actrd") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="Actual R\D (Min)">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label12" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblpmtskid" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <div style="z-index: 999; border-bottom: black 1px solid; position: absolute; border-left: black 1px solid;
        width: 450px; height: 180px; border-top: black 1px solid; border-right: black 1px solid"
        id="tskdiv" class="details">
        <table cellspacing="0" cellpadding="0" width="450" bgcolor="white">
            <tr bgcolor="blue">
                <td>
                    &nbsp;
                    <asp:Label ID="Label24" runat="server" ForeColor="White" Font-Size="10pt" Font-Names="Arial"
                        Font-Bold="True">Task Description</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closetsk();" alt="" src="../images/appbuttons/minibuttons/close.gif"><br>
                </td>
            </tr>
            <tr class="tbg" height="30">
                <td style="padding-bottom: 3px; padding-left: 3px; padding-right: 3px; padding-top: 3px"
                    colspan="2">
                    <asp:TextBox ID="txttsk" runat="server" CssClass="plainlabel" Width="440px" Height="170px"
                        TextMode="MultiLine" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblwo" type="hidden" name="lblwo" runat="server">
    <input id="lblsubmit" type="hidden" name="lblsubmit" runat="server">
    <input id="lblstat" type="hidden" name="lblstat" runat="server">
    <input id="lblpstr" type="hidden" name="lblpstr" runat="server">
    <input id="lbltstr" type="hidden" name="lbltstr" runat="server">
    <input id="lbllstr" type="hidden" name="lbllstr" runat="server">
    <input id="lblupsav" type="hidden" name="lblupsav" runat="server">
    <input id="xCoord" type="hidden" name="xCoord" runat="server">
    <input id="yCoord" type="hidden" name="yCoord" runat="server">
    <input id="lblpmid" type="hidden" runat="server" name="lblpmid">
    <input id="lblpmhid" type="hidden" runat="server" name="lblpmhid"><input id="lblro"
        type="hidden" runat="server" name="lblro"><input id="lbllog" type="hidden" name="lbllog"
            runat="server">
    <input type="hidden" id="lbleqid" runat="server" name="lbleqid"><input type="hidden"
        id="lblisdown" runat="server" name="lblisdown">
    <input type="hidden" id="lblusetdt" runat="server" name="lblusetdt">
    <input type="hidden" id="lblrd" runat="server" name="lblrd">
    <input type="hidden" id="lblactrd" runat="server" name="lblactrd"><input type="hidden"
        id="lblusetotal" runat="server" name="lblusetotal">
    <input type="hidden" id="lblest" runat="server" name="lblest">
    <input type="hidden" id="lbls" runat="server" />
    <input type="hidden" id="lblf" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
        <input type="hidden" id="lblexd" runat="server" />
    <input type="hidden" id="lblexdp" runat="server" />
    </form>
</body>
</html>
