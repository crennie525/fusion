

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Math
Public Class PMDivMantpm
    Inherits System.Web.UI.Page
    '***nissan only***
    Dim sc As New Utilities
    Dim sConfigPosition, sEquipment, sStructureType, sService, sResponsible, sDescription, sPlanningGroup, sPriority As String
    Dim sApprovedBy, sReasonCode, sStartDate, sFinishDate, sTxt1, sTxt2, sErrorCode1, sErrorCode2, sErrorCode3, sComplaintType As String
    Dim iRetroFlag, sItemNumber, sLotNumber, iPlannedQuantity, sFacility, sRequestedStartDte, sRequestedFinishDte, iSetupTime, iRuntime As String
    Dim iPlannedNumWkrs, sTextBlock, iOperation, sTransactionID, pmid, tpmid, sup, superid, wa, waid, lc, lcid, wnums, woecd, s1flag, s2flag, s3flag, s4flag As String
    Dim MWNO, OPNO, RPRE, UMAT, PCTP, REND As String
    Dim RPDT As String
    Dim UMAS, DOWT, DLY1, DLY2 As String
    Dim EMNO, FCLA, FCL2, FCL3 As String
    Dim retval As String
    Dim CONO As String = "1"
    Dim errcnt As Integer = 0
    Dim labflag As Integer = 0
    '***
    Protected WithEvents ovid99 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid98 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid97 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid96 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents lblnodown As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lblerrcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblMWNO As System.Web.UI.HtmlControls.HtmlInputHidden


    Protected WithEvents lang820 As System.Web.UI.WebControls.Label

    Protected WithEvents lang819 As System.Web.UI.WebControls.Label

    Protected WithEvents lang818 As System.Web.UI.WebControls.Label

    Protected WithEvents lang817 As System.Web.UI.WebControls.Label

    Protected WithEvents lang816 As System.Web.UI.WebControls.Label

    Protected WithEvents lang815 As System.Web.UI.WebControls.Label

    Protected WithEvents lang814 As System.Web.UI.WebControls.Label

    Protected WithEvents lang813 As System.Web.UI.WebControls.Label

    Protected WithEvents lang812 As System.Web.UI.WebControls.Label

    Protected WithEvents lang811 As System.Web.UI.WebControls.Label

    Protected WithEvents lang810 As System.Web.UI.WebControls.Label

    Protected WithEvents lang809 As System.Web.UI.WebControls.Label

    Protected WithEvents lang808 As System.Web.UI.WebControls.Label

    Protected WithEvents lang807 As System.Web.UI.WebControls.Label

    Protected WithEvents lang806 As System.Web.UI.WebControls.Label

    Protected WithEvents lang805 As System.Web.UI.WebControls.Label

    Protected WithEvents lang804 As System.Web.UI.WebControls.Label

    Protected WithEvents lang803 As System.Web.UI.WebControls.Label

    Protected WithEvents lang802 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisactive As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim mu As New mmenu_utils_a
    Dim sql As String
    Dim ds As DataSet
    Dim pmhid, pmstr, pdm, dlast, dnext, dstart, lead, eqid, usestart, ro, fcust, days, pdt, ttt, sid, sched, issched, isact, coi As String
    Dim pmi As New Utilities
    Dim ap As New AppUtils
    'Dim nsws As New nisscan_te
    Protected WithEvents pmtitle As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents pmlast As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents pmnext As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txts As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtn As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnedittask As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsup As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlead As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpmhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnext As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblschk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbs As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents tdcnt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdtasks As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblacnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsearch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtcharge As System.Web.UI.WebControls.TextBox
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgi2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgmeas As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents cbsupe As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbleade As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtleadtime As System.Web.UI.WebControls.TextBox
    Dim dr As SqlDataReader
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmalert As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblalert As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldocs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblondate As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgsav As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgcomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cb1mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents txtwonum As System.Web.UI.WebControls.TextBox
    Protected WithEvents imgwoshed As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdstatus As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trshift1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trshift2 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trshift3 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trshiftdays As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblcalday As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcallimit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfcust As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldays As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretday As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusetdt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusetotal As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblttime As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldtime As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblacttime As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblactdtime As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhalert As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldalert As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblleadid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsupid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusesched As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblissched As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoi As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Login As String

    Protected WithEvents cboff As System.Web.UI.HtmlControls.HtmlInputCheckBox
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
            Dim usr As String = HttpContext.Current.Session("username").ToString()
            lblusername.Value = usr
        Catch ex As Exception
            lbllog.Value = "no"
        End Try

        If Not IsPostBack Then
            sched = mu.Sched
            lblusesched.Value = sched
            issched = mu.Is_Sched
            lblissched.Value = issched
            If issched = 1 Then
                'Label2.Visible = True
            Else
                'Label2.Visible = False
                'divlg.Attributes.Add("class", "details")
            End If
            coi = mu.COMPI
            lblcoi.Value = coi
            If coi = "NISS" Then
                Try
                    isact = System.Configuration.ConfigurationManager.AppSettings("servicemode")
                    If isact = "active" Then
                        lblisactive.Value = "yes"
                    End If
                Catch ex As Exception

                End Try
                
            End If
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                imgsav.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                imgsav.Attributes.Add("onclick", "")
                imgcomp.Attributes.Add("onclick", "")
            End If
            pmid = Request.QueryString("pmid").ToString
            'pmstr = Request.QueryString("pmstr").ToString
            eqid = Request.QueryString("eqid").ToString
            fcust = Request.QueryString("fcust").ToString
            'pdm = Request.QueryString("pdm").ToString
            'dlast = Request.QueryString("last").ToString
            'dnext = Request.QueryString("next").ToString
            lblpmid.Value = pmid
            lblfcust.Value = fcust
            '
            'pmlast.InnerHtml = dlast
            'lblnext.Value = dnext
            'pmnext.InnerHtml = dnext
            lbleqid.Value = eqid
            pmi.Open()
            pdt = ap.PDTTEntry
            If pdt = "lvdttt" Then
                lblusetdt.Value = "no"

            Else
                lblusetdt.Value = "yes"

            End If
            ttt = ap.TTTTEntry
            If ttt = "lvtptt" Then
                lblusetotal.Value = "no"

            Else
                lblusetotal.Value = "yes"

            End If
            GetDocs()
            GetDates(pmid)
            GetTasks(pmid)
            CheckMeas(pmid)
            pmi.Dispose()
        Else
            If Request.Form("lblsubmit") = "save" Then
                lblsubmit.Value = ""
                pmi.Open()
                pmid = lblpmid.Value
                SavePM()
                pmid = lblpmid.Value
                GetDates(pmid)
                pmid = lblpmid.Value
                GetTasks(pmid)
                pmi.Dispose()
            ElseIf Request.Form("lblsubmit") = "comp" Then
                lblsubmit.Value = ""
                pmid = lblpmid.Value
                pmi.Open()
                pmid = lblpmid.Value
                GetDates(pmid)
                pmid = lblpmid.Value
                GetTasks(pmid)
                pmi.Dispose()
            ElseIf Request.Form("lblsubmit") = "s" Then

            ElseIf Request.Form("lblsubmit") = "n" Then

            ElseIf Request.Form("lblsubmit") = "checkcomp" Then
                lblsubmit.Value = ""
                pmi.Open()
                pmid = lblpmid.Value
                CheckPMComp()
                pmid = lblpmid.Value
                GetDates(pmid)
                pmid = lblpmid.Value
                GetTasks(pmid)
                pmi.Dispose()
            ElseIf Request.Form("lblsubmit") = "updocs" Then
                lblsubmit.Value = ""
                pmi.Open()
                GetDocs()
                pmi.Dispose()
            ElseIf Request.Form("lblsubmit") = "compwo" Then
                lblsubmit.Value = ""
                pmi.Open()
                pmid = lblpmid.Value
                Dim won As String = txtwonum.Text
                pmid = lblpmid.Value
                CheckActuals(won)
                CompWO()
                pmid = lblpmid.Value
                GetDates(pmid)
                pmid = lblpmid.Value
                GetTasks(pmid)
                pmi.Dispose()
            End If
        End If
    End Sub
    Private Sub GetDocs()
        pmid = lblpmid.Value
        Dim sbc As Integer
        Dim doc, docs, fn, ty As String
        sql = "select count(*) from pmManAttachtpm where pmid = '" & pmid & "'"
        sbc = pmi.Scalar(sql)
        If sbc > 0 Then ' should be using has rows instead of count
            sql = "select * from pmManAttachtpm where pmid = '" & pmid & "'"
            dr = pmi.GetRdrData(sql)
            While dr.Read
                fn = dr.Item("filename").ToString
                ty = dr.Item("doctype").ToString
                doc = ty & "~" & fn
                If docs = "" Then
                    docs = doc
                Else
                    docs += ";" & doc
                End If
            End While
            dr.Close()
            lbldocs.Value = docs
        End If


    End Sub
    Private Sub CheckPMComp()
        pmid = lblpmid.Value
        Dim won As String = txtwonum.Text
        Dim wom, womc, wojpm, wojpmc As Integer
        Dim chkalert As Integer = 0
        Dim mchk As String = lblmalert.Value
        lblmalert.Value = ""

        'Check Measurements
        If mchk <> "ok" Then
            sql = "select womc = (select count(*) from pmTaskMeasDetMantpm where pmid = '" & pmid & "' and measurement2 is null), " _
                    + "wom = (select count(*) from pmTaskMeasDetMantpm where pmid = '" & pmid & "')"
            dr = pmi.GetRdrData(sql)
            While dr.Read
                wom = dr.Item("wom").ToString
                womc = dr.Item("womc").ToString
            End While
            dr.Close()

            If (womc > 0 And wom <> 0) Then
                chkalert += 1
                lblmalert.Value = "m"
            End If
        End If

        'Check Total Time
        Dim esthrsstr, acthrsstr
        esthrsstr = lblttime.Value
        acthrsstr = lblacttime.Value
        If esthrsstr <> "0" Then
            If acthrsstr = "0" Then
                If mchk <> "ok" Then
                    chkalert += 1
                    If lblmalert.Value = "" Then
                        lblmalert.Value = "h"
                    Else
                        lblmalert.Value += ",h"
                    End If

                Else
                    SaveTTime(esthrsstr, esthrsstr)
                End If

            End If
        End If

        'Check Total Down Time
        Dim tdstart, tdstop, tdown As String
        Dim tdstartp, tdstopp, tdownp As String
        Dim exd, exdp As String
        sql = "select startdown, stopdown, totaldown, startdownp, stopdownp, totaldownp from eqhist where wonum = '" & won & "'"
        dr = pmi.GetRdrData(sql)
        While dr.Read
            tdstart = dr.Item("startdown").ToString
            tdstop = dr.Item("stopdown").ToString
            tdown = dr.Item("totaldown").ToString

            tdstartp = dr.Item("startdownp").ToString
            tdstopp = dr.Item("stopdownp").ToString
            tdownp = dr.Item("totaldownp").ToString
        End While
        dr.Close()
        If tdstart <> "" Then
            exd = "yes"
        End If
        If tdstartp <> "" Then
            exdp = "yes"
        End If
        'save for forced DT entry
        'If exdp = "yes" And tdownp = "" Then
        'If exd = "yes" And tdown = "" Then
        'Dim strMessage As String = "Your Production and Equipment Down Time Entries need to be Completed"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'Else
        'Dim strMessage As String = "Your Production Down Time Entry needs to be Completed"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End If
        'ElseIf exd = "yes" And tdown = "" Then
        'Dim strMessage As String = "Your Equipment Down Time Entry needs to be Completed"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'Else
        Dim esthrsstrd, acthrsstrd
        esthrsstrd = lbldtime.Value
        acthrsstrd = lblactdtime.Value
        If esthrsstrd <> "0" And esthrsstrd <> "0.00" And esthrsstrd <> "" Then
            If acthrsstrd = "0" Or acthrsstrd = "0.00" Or acthrsstrd = "" Then
                If mchk <> "ok" Then
                    chkalert += 1
                    If lblmalert.Value = "" Then
                        lblmalert.Value = "d"
                    Else
                        lblmalert.Value += ",d"
                    End If

                Else
                    SaveDTime(esthrsstrd, exd, exdp)
                End If

            End If
        End If
        'End If

        If chkalert = 0 Then
            CheckActuals(won)
            CompWO()
        Else
            Exit Sub
        End If


    End Sub
    Private Sub SaveDTime(ByVal ard As String, ByVal exd As String, ByVal expd As String)
        Dim currdt As Date = pmi.CNOW
        Dim usetdt As String = lblusetdt.Value
        Dim nodown As String = lblnodown.Value
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        Dim eqid As String = lbleqid.Value
        Dim wonum As String = lblwo.Value
        If usetdt = "no" Then
            Dim ardchk As Long
            Try
                ardchk = System.Convert.ToDecimal(ard)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr371", "PMDivMantpm.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        End If
        If exd = "yes" And expd = "yes" Then
            sql = "update eqhist set stopdown = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update workorder set isdown = '0' where wonum = '" & wonum & "'; " _
        + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
        + "select @stopdate = stopdown, @downdate = startdown from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "set @dmins = datediff(mi, @downdate, @stopdate); " _
        + "set @dhrs = @dmins / 60; " _
        + "update tpm set actdtime = @dmins where pmid = '" & pmid & "'; " _
        + "update pmhisttpm set actdtime = @dmins where pmhid = '" & pmhid & "'; " _
        + "update eqhist set totaldown = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
        + "where eqid = '" & eqid & "'"
            pmi.Update(sql)
            sql = "update eqhist set stopdownp = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update workorder set isdownp = '0' where wonum = '" & wonum & "'; " _
        + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
        + "select @stopdate = stopdownp, @downdate = startdownp from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "set @dmins = datediff(mi, @downdate, @stopdate); " _
        + "set @dhrs = @dmins / 60; " _
        + "update tpm set actdptime = @dmins where pmid = '" & pmid & "'; " _
        + "update pmhisttpm set actdptime = @dmins where pmhid = '" & pmhid & "'; " _
        + "update eqhist set totaldownp = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update equipment set totaldownp = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
        + "where eqid = '" & eqid & "'"
            pmi.Update(sql)
        ElseIf exd = "yes" Then
            sql = "update eqhist set stopdown = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update workorder set isdown = '0' where wonum = '" & wonum & "'; " _
        + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
        + "select @stopdate = stopdown, @downdate = startdown from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "set @dmins = datediff(mi, @downdate, @stopdate); " _
        + "set @dhrs = @dmins / 60; " _
        + "update tpm set actdtime = @dmins where pmid = '" & pmid & "'; " _
        + "update pmhisttpm set actdtime = @dmins where pmhid = '" & pmhid & "'; " _
        + "update eqhist set totaldown = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
        + "where eqid = '" & eqid & "'"
            pmi.Update(sql)
        ElseIf expd = "yes" Then
            sql = "update eqhist set stopdownp = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
       + "update workorder set isdownp = '0' where wonum = '" & wonum & "'; " _
       + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
       + "select @stopdate = stopdownp, @downdate = startdownp from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
       + "set @dmins = datediff(mi, @downdate, @stopdate); " _
       + "set @dhrs = @dmins / 60; " _
       + "update tpm set actdptime = @dmins where pmid = '" & pmid & "'; " _
       + "update pmhisttpm set actdptime = @dmins where pmhid = '" & pmhid & "'; " _
       + "update eqhist set totaldownp = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
       + "update equipment set totaldownp = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
       + "where eqid = '" & eqid & "'"
            pmi.Update(sql)
        Else
            If usetdt = "yes" And nodown <> "yes" Then
                sql = "update tpm set actdtime = '" & ard & "' where pmid = '" & pmid & "'; " _
        + "declare @dmins decimal(10,2), @dhrs decimal(10,2), @eqhid int; " _
        + "set @dmins = '" & ard & "'; " _
        + "set @dhrs = @dmins / 60; " _
        + "select @eqhid = eqhid from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "if @eqhid is null " _
        + "begin " _
        + "insert into eqhist (eqid, wonum, ispm, pmid, totaldown)" _
        + "values ('" & eqid & "', '" & wonum & "', 1, '" & pmid & "', @dhrs)" _
        + "end " _
        + "else " _
        + "begin " _
        + "update eqhist set totaldown = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "' " _
        + "end; " _
        + "update pmhisttpm set actdtime = @dmins where pmhid = '" & pmhid & "'; " _
        + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
        + "where eqid = '" & eqid & "'"
                pmi.Update(sql)
            End If
            
        End If

    End Sub
    Private Sub CheckActuals(ByVal wonum As String)
        Dim icost As String = ap.InvEntry
        If icost <> "ext" And icost <> "inv" Then
            sql = "update workorder set actmatcost = (select isnull(sum(total),0) from wotpmparts where wonum = '" & wonum & "' and used = 'Y') " _
            + "where wonum = '" & wonum & "'; update workorder set acttoolcost = (select isnull(sum(total),0) from wotpmtools where wonum = '" & wonum & "' and used = 'Y') " _
            + "where wonum = '" & wonum & "'; update workorder set actlubecost = (select isnull(sum(cost),0) from wotpmlubes where wonum = '" & wonum & "' and used = 'Y') " _
            + "where wonum = '" & wonum & "'"
            pmi.Update(sql)
        End If
    End Sub
    Private Sub CompWO()
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        fcust = lblfcust.Value
        Dim wonum As String = lblwo.Value
        coi = lblcoi.Value
        Dim usr As String = lblusername.Value 'HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        Dim cndate As Date = pmi.CNOW
        Dim nd As String = lblretday.Value
        Dim cd As Integer
        Dim nnd As String
        days = lbldays.Value
        Dim cndt As String = txtn.Text
        Dim cndy As String
        Dim i, n, nn, nnn, tn As Integer
        If days <> "no" Then
            If nd = "" And cndt <> "" Then
                Try
                    Dim tndi As Integer = System.Convert.ToDateTime(cndt).DayOfWeek
                    Select Case tndi
                        Case "1"
                            nd = "M"
                        Case "2"
                            nd = "Tu"
                        Case "3"
                            nd = "W"
                        Case "4"
                            nd = "Th"
                        Case "5"
                            nd = "F"
                        Case "6"
                            nd = "Sa"
                        Case "0"
                            nd = "Su"
                        Case Else
                            Dim strMessage As String = tmod.getmsg("cdstr365", "PMDivMantpm.aspx.vb")

                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            Exit Sub
                    End Select
                Catch ex As Exception
                    Dim strMessage As String = tmod.getmsg("cdstr366", "PMDivMantpm.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try

            End If
            Select Case nd
                Case "M"
                    cd = 1
                Case "Tu"
                    cd = 2
                Case "T"
                    cd = 2
                Case "W"
                    cd = 3
                Case "Th"
                    cd = 4
                Case "F"
                    cd = 5
                Case "Sa"
                    cd = 6
                Case "Su"
                    cd = 7
                Case Else
                    cd = -1
            End Select

            If cd <> -1 And cndt <> "" Then
                Dim daysarr() As String = days.Split(",")
                For i = 0 To daysarr.Length - 1
                    If daysarr(i) = nd Then
                        If i < daysarr.Length - 1 Then
                            n = i + 1
                        Else
                            n = 0
                        End If
                        cndy = daysarr(n)
                        Select Case cndy
                            Case "M"
                                nn = 1
                            Case "Tu"
                                nn = 2
                            Case "T"
                                nn = 2
                            Case "W"
                                nn = 3
                            Case "Th"
                                nn = 4
                            Case "F"
                                nn = 5
                            Case "Sa"
                                nn = 6
                            Case "Su"
                                nn = 7
                            Case Else
                                nn = -1
                        End Select
                        If nn <> -1 Then
                            If nn > cd Then
                                nnn = nn - cd 'nnn = days to add
                            ElseIf nn < cd Then
                                tn = 7 - nn
                                nnn = cd + nn
                                nnn = (7 - cd) + nn
                            ElseIf nn = cd Then
                                nnn = 7
                            End If
                            Try
                                nnd = DateAdd(DateInterval.Day, nnn, System.Convert.ToDateTime(cndt))
                            Catch ex As Exception
                                Dim strMessage As String = tmod.getmsg("cdstr367", "PMDivMantpm.aspx.vb")

                                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                                Exit Sub
                            End Try

                        Else
                            'error msg
                            Dim strMessage As String = tmod.getmsg("cdstr368", "PMDivMantpm.aspx.vb")

                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            Exit Sub
                        End If
                        Exit For
                    End If
                Next
            Else
                If cndt <> "" Then
                    Dim strMessage As String = tmod.getmsg("cdstr369", "PMDivMantpm.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
                'error msg
               
            End If

        End If
        pdt = lblusetdt.Value
        ttt = lblusetotal.Value
        'sql = "usp_comppm1tpm1 '" & pmid & "', '" & pmhid & "', '" & fcust & "', '" & nd & "', '" & cndy & "', '" & nnd & "', '" & ustr & "'"
        sql = "usp_comppm1tpm3 '" & pmid & "', '" & pmhid & "', '" & fcust & "', '" & nd & "', '" & cndy & "', '" & nnd & "', '" & wonum & "','" & cndate & "','" & ttt & "', " _
        + "'" & pdt & "','" & ustr & "','" & coi & "'"
        'pmi.Update(sql)

        Dim newdate As String
        'newdate = Now
        Try
            newdate = pmi.strScalar(sql)
            newdate = CType(newdate, DateTime)
        Catch ex As Exception

        End Try
        
        Dim chkdat As String = newdate
        If chkdat <> "" Then
            If newdate < Now Then
                lblalert.Value = "4"
            End If
        Else
            If chkdat <> "" Then
                lblalert.Value = "3"
            End If
        End If
        'If newdate < Now Then
        'lblalert.Value = "4"
        'Else
        'lblalert.Value = "3"
        'End If
        Dim won As String = txtwonum.Text
        sql = "usp_upreserved '" & won & "'"
        pmi.Update(sql)
        isact = lblisactive.Value

        If isact = "yes" Then
            'Try
            'Dim ps As String = nsws.SendCant(won)
            'If ps = "1" Then
            'Dim strMessage As String = "Problem with OpenWorkOrder or TimeEntry Transfer - Please Contact Your System Administrator"
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'End If
            'Catch ex As Exception

            'End Try
            Dim retwonum As String
            Try
                sql = "select retwonum from workorder where wonum = '" & won & "'"
                dr = pmi.GetRdrData(sql)
                While dr.Read
                    retwonum = dr.Item("retwonum").ToString
                End While
                dr.Close()
                sc.Open()
                If retwonum = "" Then
                    SendCant(won)
                End If
                sc.Dispose()
            Catch ex As Exception

            End Try

        End If

        Dim superid As String = lblsupid.Value
        If superid <> "" Then
            Try
                Dim mail1 As New pmmail
                mail1.CheckIt("comp", superid, wonum)
            Catch ex As Exception

            End Try
        End If

        'added for nissan
        Dim actfin, actsta, actdiff As String
        sql = "select isnull(Convert(char(10),actfinish,101), Convert(char(10),'01/01/1900',101)) as actfin, " _
            + "isnull(Convert(char(10),actstart,101), Convert(char(10),'01/01/1900',101)) as actsta " _
            + "from pmhisttpm where wonum = '" & wonum & "' and pmhid = '" & pmhid & "' and pmid = '" & pmid & "'"
        dr = pmi.GetRdrData(sql)
        While dr.Read
            actfin = dr.Item("actfin").ToString
            actsta = dr.Item("actsta").ToString
        End While
        dr.Close()
        'If actfin = "01/01/1900" Then
        'sql = "select isnull(Convert(char(10),actfinish,101), Convert(char(10),'01/01/1900',101)) from workorder where wonum = '" & wonum & "'"
        'Try
        'actfin = pmi.strScalar(sql)
        'Catch ex As Exception
        'actfin = "01/01/1900"
        'End Try
        'End If
        If actfin = "01/01/1900" Then
            sql = "select max(isnull(Convert(char(10),transdate,101),'')) from wolabtrans where wonum = '" & wonum & "'"
            Try
                actfin = pmi.strScalar(sql)
            Catch ex As Exception
                actfin = ""
            End Try
            If actfin <> "" Then
                sql = "update pmhisttpm set actfinish = '" & actfin & "' where wonum = '" & wonum & "' and pmhid = '" & pmhid & "' and pmid = '" & pmid & "'"
                pmi.Update(sql)
            End If
        Else
            sql = "update pmhisttpm set actfinish = '" & actfin & "' where wonum = '" & wonum & "' and pmhid = '" & pmhid & "' and pmid = '" & pmid & "'"
            pmi.Update(sql)
        End If
        If actsta = "01/01/1900" Then
            sql = "select min(isnull(Convert(char(10),transdate,101),'')) from wolabtrans where wonum = '" & wonum & "'"
            Try
                actsta = pmi.strScalar(sql)
            Catch ex As Exception
                actsta = ""
            End Try
            If actfin <> "" Then
                sql = "update pmhisttpm set actstart = '" & actsta & "' where wonum = '" & wonum & "' and pmhid = '" & pmhid & "' and pmid = '" & pmid & "'"
                pmi.Update(sql)
            End If
        Else
            sql = "update pmhisttpm set actstart = '" & actsta & "' where wonum = '" & wonum & "' and pmhid = '" & pmhid & "' and pmid = '" & pmid & "'"
            pmi.Update(sql)
        End If
        'end added

        If coi = "NISS" Then
            Dim nwo As String
            sql = "select max(isnull(wonum, '0')) from workorder where tpmid = '" & pmid & "' and status not in ('COMP','CLOSE','CAN','CANCEL')"
            Try
                nwo = pmi.strScalar(sql)
            Catch ex As Exception
                nwo = "0"
            End Try
            If nwo <> "0" Then
                sql = "update workorder set wopriority = 9 where wonum = '" & nwo & "'"
                pmi.Update(sql)
            End If
        End If

    End Sub
    Private Sub SaveTTime(ByVal atime As String, ByVal etime As String)
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        Dim wonum As String = lblwo.Value
        Dim usetotal As String = lblusetotal.Value
        If usetotal = "yes" Then
            Dim qtychk As Long
            Try
                qtychk = System.Convert.ToDecimal(atime)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr370", "PMDivMantpm.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            sql = "update tpm set acttime = '" & atime & "' where pmid = '" & pmid & "'; " _
            + "update pmhisttpm set acttime = '" & atime & "' where pmhid = '" & pmhid & "'; " _
            + "declare @dmins decimal(10,2), @dhrs decimal(10,2), @emins decimal(10,2), @ehrs decimal(10,2); " _
            + "set @dmins = '" & atime & "'; " _
            + "set @dhrs = @dmins / 60; " _
            + "set @emins = '" & etime & "'; " _
            + "set @ehrs = @emins / 60; " _
            + "update workorder set actlabhrs = @dhrs, estlabhrs = @ehrs where wonum = '" & wonum & "'; "
            pmi.Update(sql)
        Else
            Exit Sub
        End If
    End Sub
    
    Private Sub CheckMeas(ByVal pmid As String)
        Dim cnt As Integer
        sql = "select count(*) from pmtaskmeasdetmantpm where pmid = '" & pmid & "'"
        cnt = pmi.Scalar(sql)
        If cnt = 0 Then
            imgmeas.Attributes.Add("class", "details")
        End If
    End Sub
    Private Sub SavePM()
        Dim supi, leadi, ch, se, le, wo, nd, nnd As String
        Dim usr As String = lblusername.Value 'HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        dstart = lblstart.Value 'txts.Text
        dnext = lblnext.Value 'txtn.Text
        sup = lblsup.Value 'txtsup.Text
        lead = lbllead.Value 'txtlead.Text
        supi = lblsupid.Value
        wo = txtwonum.Text
        'If supi = "" Then supi = "0"
        leadi = lblleadid.Value
        'If leadi = "" Then leadi = "0"
        If cbs.Checked = True Then
            usestart = "1"
        Else
            usestart = "0"
        End If
        If cbsupe.Checked = True Then
            se = "1"
        End If
        If cbleade.Checked = True Then
            le = "1"
        End If
        ch = txtcharge.Text
        ch = pmi.ModString2(ch)
        Dim onext As String = lblondate.Value
        nd = lblretday.Value
        Select Case nd
            Case "1"
                nnd = "M"
            Case "2"
                nnd = "Tu"
            Case "3"
                nnd = "W"
            Case "4"
                nnd = "Th"
            Case "5"
                nnd = "F"
            Case "6"
                nnd = "Sa"
            Case "0"
                nnd = "Su"
            Case Else
                nnd = ""
        End Select
        sql = "usp_uppm2tpm '" & pmid & ", '" & dstart & "', '" & dnext & "', '" & sup & "', '" & lead & "', '" & usestart & "', '" & supi & "', '" & leadi & "', '" & pmhid & "', '" & ch & "','" & se & "','" & le & "','" & wo & "','" & ustr & "','" & onext & "'"
        Dim cmd As New SqlCommand("exec usp_uppm2tpm @pmid, @start, @next, @sup, @lead, @use, @supi, @leadi, @pmhid, @ch, @supa, @leada, @elead, @wo, @uid, @onext, @nd")
        Dim param0 = New SqlParameter("@pmid", SqlDbType.Int)
        param0.Value = pmid
        cmd.Parameters.Add(param0)
        Dim param02 = New SqlParameter("@pmhid", SqlDbType.Int)
        param02.Value = pmhid
        cmd.Parameters.Add(param02)
        Dim param01 = New SqlParameter("@use", SqlDbType.Int)
        param01.Value = usestart
        cmd.Parameters.Add(param01)
        Dim param1 = New SqlParameter("@start", SqlDbType.DateTime)
        If Len(dstart) > 0 Then
            param1.Value = dstart
        Else
            param1.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param1)
        Dim param2 = New SqlParameter("@next", SqlDbType.DateTime)
        If Len(dnext) > 0 Then
            param2.Value = dnext
        Else
            param2.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param2)
        Dim param3 = New SqlParameter("@sup", SqlDbType.VarChar)
        If Len(sup) > 0 Then
            param3.Value = sup
        Else
            param3.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param3)
        Dim param4 = New SqlParameter("@lead", SqlDbType.VarChar)
        If Len(lead) > 0 Then
            param4.Value = lead
        Else
            param4.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param4)
        Dim param5 = New SqlParameter("@supi", SqlDbType.Int)
        If Len(supi) > 0 Then
            param5.Value = supi
        Else
            param5.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param5)
        Dim param6 = New SqlParameter("@leadi", SqlDbType.Int)
        If Len(leadi) > 0 Then
            param6.Value = leadi
        Else
            param6.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param6)
        Dim param7 = New SqlParameter("@ch", SqlDbType.VarChar)
        If Len(ch) > 0 Then
            param7.Value = ch
        Else
            param7.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param7)

        Dim param8 = New SqlParameter("@supa", SqlDbType.Int)
        If cbsupe.Checked = True Then
            param8.Value = 1
        Else
            param8.Value = 0
        End If
        cmd.Parameters.Add(param8)
        Dim param9 = New SqlParameter("@leada", SqlDbType.Int)
        If cbleade.Checked = True Then
            param9.Value = 1
        Else
            param9.Value = 0
        End If
        cmd.Parameters.Add(param9)
        Dim param10 = New SqlParameter("@elead", SqlDbType.Int)
        If txtleadtime.Text <> "" Then
            param10.Value = txtleadtime.Text
        Else
            param10.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param10)

        Dim param11 = New SqlParameter("@wo", SqlDbType.Int)
        If wo <> "" Then
            param11.Value = wo
        Else
            param11.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param11)
        Dim param12 = New SqlParameter("@uid", SqlDbType.VarChar)
        If ustr <> "" Then
            param12.Value = ustr
        Else
            param12.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param12)
        Dim param13 = New SqlParameter("@onext", SqlDbType.VarChar)
        If onext <> "" Then
            param13.Value = onext
        Else
            param13.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param13)
        Dim param14 = New SqlParameter("@nd", SqlDbType.VarChar)
        If nnd <> "" Then
            param14.Value = nnd
        Else
            param14.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param14)
        pmi.UpdateHack(cmd)
        'dr = pmi.GetRdrDataHack(cmd)
        'While dr.Read
        'lblpmid.Value = dr.Item("pmid").ToString
        'lblpmid.Value = dr.Item("pmhid").ToString
        'End While
        'dr.Close()
    End Sub
    Private Sub GetDates(ByVal pmid As String)
        Dim usestart, pmcnt, se, le, shift, days, nextday As String
        Dim calday As Integer = -1
        Dim wonum As String = ""
        sql = "select Convert(char(10),p.startdate,101) as 'startdate', " _
         + "Convert(char(10),p.nextdate,101) as 'nextdate', Convert(char(10),p.lastdate,101) as 'lastdate', p.skillid, p.superid, p.super, p.leadid, p.lead, " _
         + "pmhid = (select max(p1.pmhid) from pmhisttpm p1 where p1.pmid = p.pmid), p.usestart, p.pmcnt, pm = (p.skill + '/' + cast(p.freq as varchar(10)) + ' days/' + p.rd), " _
         + "pmd = (case p.ptid when 0 then 'None' else p.pretech End), p.wonum, p.chargenum, p.supalert, p.leadalert, p.elead, " _
         + "isnull(p.ttime,0) as 'ttime', isnull(p.dtime,0) as 'dtime', isnull(p.acttime,0) as 'acttime', isnull(p.actdtime,0) as 'actdtime' " _
         + "from pm p left join pmhisttpm h on h.pmid = p.pmid where p.pmid = '" & pmid & "'"

        sql = "select Convert(char(10),p.startdate,101) as 'startdate', p.days, p.nextday, " _
        + "Convert(char(10),p.nextdate,101) as 'nextdate', Convert(char(10),p.lastdate,101) as 'lastdate', p.skillid, p.superid, p.super, p.leadid, p.lead, " _
        + "pmhid = (select max(p1.pmhid) from pmhisttpm p1 where p1.pmid = p.pmid), p.usestart, p.pmcnt, p.shift, p.days, " _
       + "pm = ( " _
                + "case isnull(len(p.shift),0) " _
                + "when 0 then 'Operator/' + cast(p.freq as varchar(10)) + ' days/' + p.rd  " _
                + "else " _
                + "case p.freq " _
                + "when 0 then 'Operator/STATIC/' + p.rd + ' - ' + isnull(p.shift,'open') + " _
                + "case len(p.days) " _
                + "when 0 then '' " _
                + "else " _
                + "case p.freq " _
                + "when 1 then '' " _
                + "else ' - ' " _
                + "+ p.days end end  " _
                + "else 'Operator/' + cast(p.freq as varchar(10)) + ' days/' + p.rd + ' - ' + isnull(p.shift,'open') +  " _
                + "case len(p.days) " _
                + "when 0 then '' " _
                + "else " _
                + "case p.freq " _
                + "when 1 then '' " _
                + "else ' - ' " _
                + "+ p.days  " _
                + "end end end end), " _
        + "pmd = (case p.ptid when 0 then 'None' else p.pretech End), p.wonum, p.chargenum, p.supalert, p.leadalert, p.elead, p.freq, " _
        + "isnull(p.ttime,0) as 'ttime', isnull(p.dtime,0) as 'dtime', isnull(p.acttime,0) as 'acttime', isnull(p.actdtime,0) as 'actdtime' " _
        + "from tpm p left join pmhisttpm h on h.pmid = p.pmid where p.pmid = '" & pmid & "'"
        dr = pmi.GetRdrData(sql)
        While dr.Read
            txts.Text = dr.Item("startdate").ToString
            txtn.Text = dr.Item("nextdate").ToString
            lblnext.Value = dr.Item("nextdate").ToString
            lblondate.Value = dr.Item("nextdate").ToString
            txtsup.Text = dr.Item("super").ToString
            txtlead.Text = dr.Item("lead").ToString
            lblsup.Value = dr.Item("super").ToString
            lbllead.Value = dr.Item("lead").ToString

            lblsupid.Value = dr.Item("superid").ToString
            lblleadid.Value = dr.Item("leadid").ToString

            pmhid = dr.Item("pmhid").ToString
            'lblpmid.Value = dr.Item("pmid").ToString
            lblpmhid.Value = dr.Item("pmhid").ToString
            usestart = dr.Item("usestart").ToString
            pmcnt = dr.Item("pmcnt").ToString
            pmlast.InnerHtml = dr.Item("lastdate").ToString
            pmtitle.InnerHtml = dr.Item("pm").ToString
            'pmpdm.InnerHtml = dr.Item("pmd").ToString
            lblskillid.Value = dr.Item("skillid").ToString
            wonum = dr.Item("wonum").ToString
            'txtwonum.Text = dr.Item("wonum").ToString
            txtcharge.Text = dr.Item("chargenum").ToString
            se = dr.Item("supalert").ToString
            le = dr.Item("leadalert").ToString
            txtleadtime.Text = dr.Item("elead").ToString
            lblfreq.Value = dr.Item("freq").ToString
            shift = dr.Item("shift").ToString
            days = dr.Item("days").ToString
            If days = "" Then
                days = "no"
            Else
                lbldays.Value = days
            End If
            lblretday.Value = dr.Item("nextday").ToString

            lblttime.Value = dr.Item("ttime").ToString
            lbldtime.Value = dr.Item("dtime").ToString

            lblacttime.Value = dr.Item("acttime").ToString
            lblactdtime.Value = dr.Item("actdtime").ToString
        End While
        dr.Close()
        txtwonum.Text = wonum
        If pmhid = "" And pmid <> "" Then
            Try
                sql = "select max(pmhid) from pmhisttpm where pmid = '" & pmid & "'"
                pmhid = pmi.strScalar(sql)
                lblpmhid.Value = pmhid
            Catch ex As Exception
                sql = "insert into pmhisttpm (pmid) values ('" & pmid & "')"
                pmi.Update(sql)
                Try
                    sql = "select max(pmhid) from pmhisttpm where pmid = '" & pmid & "'"
                    pmhid = pmi.strScalar(sql)
                    lblpmhid.Value = pmhid
                Catch ex1 As Exception

                End Try
            End Try
           
        End If
        Try
            Dim freq As Integer = lblfreq.Value
            If freq = 0 Or freq = 1 Or freq = 7 Then
                'activate days
                GetDays(pmid)
                If shift = "1st shift" Then
                    trshift2.Attributes.Add("class", "details")
                    trshift3.Attributes.Add("class", "details")
                ElseIf shift = "2nd shift" Then
                    trshift1.Attributes.Add("class", "details")
                    trshift3.Attributes.Add("class", "details")
                ElseIf shift = "3rd shift" Then
                    trshift1.Attributes.Add("class", "details")
                    trshift2.Attributes.Add("class", "details")
                Else
                    trshiftdays.Attributes.Add("class", "details")
                    trshift1.Attributes.Add("class", "details")
                    trshift2.Attributes.Add("class", "details")
                    trshift3.Attributes.Add("class", "details")
                End If
                Select Case days
                    Case "M"
                        calday = 1
                    Case "Tu"
                        calday = 2
                    Case "T"
                        calday = 2
                    Case "W"
                        calday = 3
                    Case "Th"
                        calday = 4
                    Case "F"
                        calday = 5
                    Case "Sa"
                        calday = 6
                    Case "Su"
                        calday = 0
                    Case Else
                        calday = -1
                End Select
                lblcalday.Value = calday
            Else
                'disable days
                DisableDays()
            End If
        Catch ex As Exception

        End Try


        If se = "1" Then
            cbsupe.Checked = True
        End If
        If le = "1" Then
            cbleade.Checked = True
        End If
        pmnext.InnerHtml = lblnext.Value
        If usestart = "1" Then
            cbs.Checked = True
        Else
            cbs.Checked = False
        End If
        Dim newdate As String = lblnext.Value
        tdcnt.InnerHtml = ""
        If newdate <> "" Then
            newdate = CType(newdate, DateTime)
            If newdate < Now Then
                tdstatus.InnerHtml = "<img src=""../images/appbuttons/minibuttons/rwarning.gif"" onmouseover=""return overlib('" & tmod.getov("cov80", "PMDivMantpm.aspx.vb") & "', ABOVE, LEFT)"" onmouseout=""return nd()"">"
            End If
        End If
        tdcnt.InnerHtml = pmcnt
    End Sub
    Private Sub EnableDays()
        If cb1o.Checked = True Or cb2o.Checked = True Or cb3o.Checked = True Then
            cb1o.Disabled = False
            'cb1.Disabled = True
            cb1mon.Disabled = True
            cb1tue.Disabled = True
            cb1wed.Disabled = True
            cb1thu.Disabled = True
            cb1fri.Disabled = True
            cb1sat.Disabled = True
            cb1sun.Disabled = True

            cb2o.Disabled = False
            'cb2.Disabled = True
            cb2mon.Disabled = True
            cb2tue.Disabled = True
            cb2wed.Disabled = True
            cb2thu.Disabled = True
            cb2fri.Disabled = True
            cb2sat.Disabled = True
            cb2sun.Disabled = True

            cb3o.Disabled = False
            'cb3.Disabled = True
            cb3mon.Disabled = True
            cb3tue.Disabled = True
            cb3wed.Disabled = True
            cb3thu.Disabled = True
            cb3fri.Disabled = True
            cb3sat.Disabled = True
            cb3sun.Disabled = True
        Else
            cb1o.Disabled = True
            'cb1.Disabled = True
            cb1mon.Disabled = False
            cb1tue.Disabled = False
            cb1wed.Disabled = False
            cb1thu.Disabled = False
            cb1fri.Disabled = False
            cb1sat.Disabled = False
            cb1sun.Disabled = False

            cb2o.Disabled = True
            'cb2.Disabled = True
            cb2mon.Disabled = False
            cb2tue.Disabled = False
            cb2wed.Disabled = False
            cb2thu.Disabled = False
            cb2fri.Disabled = False
            cb2sat.Disabled = False
            cb2sun.Disabled = False

            cb3o.Disabled = True
            'cb3.Disabled = True
            cb3mon.Disabled = False
            cb3tue.Disabled = False
            cb3wed.Disabled = False
            cb3thu.Disabled = False
            cb3fri.Disabled = False
            cb3sat.Disabled = False
            cb3sun.Disabled = False
        End If

    End Sub
    Private Sub DisableDays()
        cb1o.Disabled = True
        'cb1.Disabled = True
        cb1mon.Disabled = True
        cb1tue.Disabled = True
        cb1wed.Disabled = True
        cb1thu.Disabled = True
        cb1fri.Disabled = True
        cb1sat.Disabled = True
        cb1sun.Disabled = True

        cb2o.Disabled = True
        'cb2.Disabled = True
        cb2mon.Disabled = True
        cb2tue.Disabled = True
        cb2wed.Disabled = True
        cb2thu.Disabled = True
        cb2fri.Disabled = True
        cb2sat.Disabled = True
        cb2sun.Disabled = True

        cb3o.Disabled = True
        'cb3.Disabled = True
        cb3mon.Disabled = True
        cb3tue.Disabled = True
        cb3wed.Disabled = True
        cb3thu.Disabled = True
        cb3fri.Disabled = True
        cb3sat.Disabled = True
        cb3sun.Disabled = True
    End Sub
    Private Sub GetDays(ByVal pmid As String)
        sql = "select top 1 * from pmtrackdaystpm where pmid = '" & pmid & "'"
        dr = pmi.GetRdrData(sql)
        While dr.Read
            If dr.Item("cb1o").ToString = "1" Then
                cb1o.Checked = True
            End If
            If dr.Item("cb1mon").ToString = "1" Then
                cb1mon.Checked = True
            Else
                cb1mon.Checked = False
            End If
            If dr.Item("cb1tue").ToString = "1" Then
                cb1tue.Checked = True
            Else
                cb1tue.Checked = False
            End If
            If dr.Item("cb1wed").ToString = "1" Then
                cb1wed.Checked = True
            Else
                cb1wed.Checked = False
            End If
            If dr.Item("cb1thu").ToString = "1" Then
                cb1thu.Checked = True
            Else
                cb1thu.Checked = False
            End If
            If dr.Item("cb1fri").ToString = "1" Then
                cb1fri.Checked = True
            Else
                cb1fri.Checked = False
            End If
            If dr.Item("cb1sat").ToString = "1" Then
                cb1sat.Checked = True
            Else
                cb1sat.Checked = False
            End If
            If dr.Item("cb1sun").ToString = "1" Then
                cb1sun.Checked = True
            Else
                cb1sun.Checked = False
            End If

            If dr.Item("cb2o").ToString = "1" Then
                cb2o.Checked = True
            End If
            If dr.Item("cb2mon").ToString = "1" Then
                cb2mon.Checked = True
            Else
                cb2mon.Checked = False
            End If
            If dr.Item("cb2tue").ToString = "1" Then
                cb2tue.Checked = True
            Else
                cb2tue.Checked = False
            End If
            If dr.Item("cb2wed").ToString = "1" Then
                cb2wed.Checked = True
            Else
                cb2wed.Checked = False
            End If
            If dr.Item("cb2thu").ToString = "1" Then
                cb2thu.Checked = True
            Else
                cb2thu.Checked = False
            End If
            If dr.Item("cb2fri").ToString = "1" Then
                cb2fri.Checked = True
            Else
                cb2fri.Checked = False
            End If
            If dr.Item("cb2sat").ToString = "1" Then
                cb2sat.Checked = True
            Else
                cb2sat.Checked = False
            End If
            If dr.Item("cb2sun").ToString = "1" Then
                cb2sun.Checked = True
            Else
                cb2sun.Checked = False
            End If

            If dr.Item("cb3o").ToString = "1" Then
                cb3o.Checked = True
            End If
            If dr.Item("cb3mon").ToString = "1" Then
                cb3mon.Checked = True
            Else
                cb3mon.Checked = False
            End If
            If dr.Item("cb3tue").ToString = "1" Then
                cb3tue.Checked = True
            Else
                cb3tue.Checked = False
            End If
            If dr.Item("cb3wed").ToString = "1" Then
                cb3wed.Checked = True
            Else
                cb3wed.Checked = False
            End If
            If dr.Item("cb3thu").ToString = "1" Then
                cb3thu.Checked = True
            Else
                cb3thu.Checked = False
            End If
            If dr.Item("cb3fri").ToString = "1" Then
                cb3fri.Checked = True
            Else
                cb3fri.Checked = False
            End If
            If dr.Item("cb3sat").ToString = "1" Then
                cb3sat.Checked = True
            Else
                cb3sat.Checked = False
            End If
            If dr.Item("cb3sun").ToString = "1" Then
                cb3sun.Checked = True
            Else
                cb3sun.Checked = False
            End If
        End While
        dr.Close()
    End Sub
    Private Sub GetTasks(ByVal pmid As String)
        Dim flag As Integer = 0
        Dim subcnt As Integer = 0
        Dim subcnth As Integer = 0
        Dim func, funcchk, task, tasknum, subtask, pm As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
        sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""610""></td></tr>")
        sql = "usp_getpmmantpm '" & pmid & "'"
        dr = pmi.GetRdrData(sql)
        Dim headhold As String = "0"
        While dr.Read
            lblacnt.Value = dr.Item("acnt").ToString
            If headhold = "0" Then
                lblwo.Value = dr.Item("wonum").ToString
                headhold = "1"
                pm = dr.Item("pm").ToString
                sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""3"">" & pm & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">Total Estimated Time: " & dr.Item("ttime") & " Minutes</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">Total Down Time: " & dr.Item("dtime") & " Minutes</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">Scheduled Start Date: " & dr.Item("nextdate") & "</td></tr>")
                sb.Append("<tr><td class=""bigbold"" colspan=""3""><hr></td></tr>")
            End If

            func = dr.Item("func").ToString
            If funcchk <> func Then 'flag = 0 Then
                If flag = 0 Then
                    flag = 1
                Else
                    'sb.Append("<tr><td>&nbsp;</td></tr>")
                End If

                funcchk = func
                sb.Append("<tr height=""20""><td class=""bluelabel"" colspan=""3""><u>Function: " & func & "</u></td>")
                'sb.Append("<td class=""plainlabel"" colspan=""2"">" & func & "</td></tr>")
                'sb.Append("<tr><td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl104" , "PMDivMantpm.aspx.vb") & "</td>")
                'sb.Append("<td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl105" , "PMDivMantpm.aspx.vb") & "</td>")
                'sb.Append("<td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl106" , "PMDivMantpm.aspx.vb") & "</td></tr>")
            End If
            task = dr.Item("task").ToString
            tasknum = dr.Item("tasknum").ToString
            subtask = dr.Item("subtask").ToString
            subcnt = dr.Item("subcnt").ToString


            If subtask = "0" Then
                'sb.Append("<tr height=""20""><td></td><td class=""label"">" & subcnt & "</td></tr>")
                sb.Append("<tr><td class=""plainlabel"">[" & tasknum & "]</td>")
                If Len(dr.Item("task").ToString) > 0 Then
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Task</b> :" & task & "</td></tr>")
                Else
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Task:</b> No Task Description Provided</td></tr>")
                End If
                'sb.Append("<tr><td>&nbsp;</td></tr>")
                sb.Append("<tr height=""20""><td></td>")
                sb.Append("<td colspan=""2"" class=""plainlabel"">Check: OK(___) " & dr.Item("fm1").ToString & "</td></tr>")
                If dr.Item("lube") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Lubes: " & dr.Item("lube").ToString & "</td></tr>")
                End If
                If dr.Item("tool") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Tools: " & dr.Item("tool").ToString & "</td></tr>")
                End If
                If dr.Item("part") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Parts: " & dr.Item("part").ToString & "</td></tr>")
                End If
                If dr.Item("meas") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Measurements: " & dr.Item("meas").ToString & "</td></tr>")
                End If
                If subcnt = 0 Then
                    sb.Append("<tr><td>&nbsp;</td></tr>")
                Else
                    If subcnt <> 0 Then

                        sb.Append("<tr height=""20""><td></td><td class=""label"" colspan=""2""><u>Sub Tasks</u></td></tr>")
                    End If

                End If

            Else
                'sb.Append("<tr><td>" & subcnt & "</td></tr>")
                sb.Append("<tr height=""20""><td></td><td class=""plainlabel"">[" & subtask & "]</td>")
                If Len(dr.Item("subt").ToString) > 0 Then
                    sb.Append("<td class=""plainlabel""><b>Task</b>: " & dr.Item("subt").ToString & "</td></tr>")
                Else
                    sb.Append("<td class=""plainlabel""><b>Task</b>: No Sub Task Description Provided</td></tr>")
                End If
                sb.Append("<tr><td></td><td></td>")
                sb.Append("<td class=""plainlabel"">Check: OK(___)</td></tr>")
                sb.Append("<tr><td>&nbsp;</td></tr>")
            End If


        End While
        dr.Close()
        sb.Append("</table>")
        tdtasks.InnerHtml = sb.ToString

    End Sub











    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang802.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang802")
        Catch ex As Exception
        End Try
        Try
            lang803.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang803")
        Catch ex As Exception
        End Try
        Try
            lang804.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang804")
        Catch ex As Exception
        End Try
        Try
            lang805.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang805")
        Catch ex As Exception
        End Try
        Try
            lang806.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang806")
        Catch ex As Exception
        End Try
        Try
            lang807.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang807")
        Catch ex As Exception
        End Try
        Try
            lang808.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang808")
        Catch ex As Exception
        End Try
        Try
            lang809.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang809")
        Catch ex As Exception
        End Try
        Try
            lang810.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang810")
        Catch ex As Exception
        End Try
        Try
            lang811.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang811")
        Catch ex As Exception
        End Try
        Try
            lang812.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang812")
        Catch ex As Exception
        End Try
        Try
            lang813.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang813")
        Catch ex As Exception
        End Try
        Try
            lang814.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang814")
        Catch ex As Exception
        End Try
        Try
            lang815.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang815")
        Catch ex As Exception
        End Try
        Try
            lang816.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang816")
        Catch ex As Exception
        End Try
        Try
            lang817.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang817")
        Catch ex As Exception
        End Try
        Try
            lang818.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang818")
        Catch ex As Exception
        End Try
        Try
            lang819.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang819")
        Catch ex As Exception
        End Try
        Try
            lang820.Text = axlabs.GetASPXPage("PMDivMantpm.aspx", "lang820")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnedittask.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMantpm.aspx", "btnedittask") & "', ABOVE, LEFT)")
            btnedittask.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            cbleade.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMantpm.aspx", "cbleade") & "')")
            cbleade.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            cbsupe.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMantpm.aspx", "cbsupe") & "')")
            cbsupe.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMantpm.aspx", "Img1") & "', ABOVE, LEFT)")
            Img1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMantpm.aspx", "Img2") & "', ABOVE, LEFT)")
            Img2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img3.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMantpm.aspx", "Img3") & "', ABOVE, LEFT)")
            Img3.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgcomp.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMantpm.aspx", "imgcomp") & "', ABOVE, LEFT)")
            imgcomp.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgi2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMantpm.aspx", "imgi2") & "', ABOVE, LEFT)")
            imgi2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgmeas.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMantpm.aspx", "imgmeas") & "')")
            imgmeas.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsav.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMantpm.aspx", "imgsav") & "', ABOVE, LEFT)")
            imgsav.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgwoshed.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMantpm.aspx", "imgwoshed") & "', ABOVE, LEFT)")
            imgwoshed.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid96.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMantpm.aspx", "ovid96") & "')")
            ovid96.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid97.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMantpm.aspx", "ovid97") & "', ABOVE, LEFT)")
            ovid97.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid98.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMantpm.aspx", "ovid98") & "', ABOVE, LEFT)")
            ovid98.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid99.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMantpm.aspx", "ovid99") & "', ABOVE, LEFT)")
            ovid99.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub
    Private Sub SendCant(ByVal wonum As String)
        Dim ldkey, ldesc, reportedby, reportedbyid As String

        Dim rwo As String = lblMWNO.Value
        Dim nodate As Date = sc.CNOW

        Dim lcnt As String
        sql = "select distinct count(*) from wolabtrans where wonum = '" & wonum & "' and retwonum is null"
        lcnt = sc.Scalar(sql)
        lbllcnt.Value = lcnt


        '***
        s1flag = "0"
        s2flag = "0"
        s3flag = "0"
        s4flag = "0"
        woecd = "0" 'lblwoecd.Value
        wnums = "1" 'lblwnums.Value

        '***

        If rwo = "" Then
            sql = "select seed from pmseed where pmtable = 'service'; update pmseed set seed = seed + 1 where pmtable = 'service'"
            sTransactionID = sc.strScalar(sql)
            'SendCant_test(wonum)
            'sTransactionID = ""
            sConfigPosition = ""
            sEquipment = ""
            sStructureType = ""
            sService = ""
            sResponsible = ""
            iOperation = ""
            sDescription = ""
            sPlanningGroup = ""
            sPriority = ""
            sApprovedBy = ""
            sReasonCode = ""
            sStartDate = ""
            sFinishDate = ""
            sTxt1 = ""
            sTxt2 = ""
            sErrorCode1 = ""
            sErrorCode2 = ""
            sErrorCode3 = ""
            sComplaintType = ""
            iRetroFlag = ""
            sItemNumber = ""
            sLotNumber = ""
            iPlannedQuantity = ""
            sFacility = ""
            sRequestedStartDte = ""
            sRequestedFinishDte = ""
            iSetupTime = ""
            iRuntime = ""
            iPlannedNumWkrs = ""
            sTextBlock = ""
            'need xtra col in wotype for Operation value
            sql = "select e.eqnum, w.targstartdate, w.targcompdate, cast(isnull(w.actlabhrs, 0) as decimal(10,2)) as actlabhrs, pnw = (select count(*) from woassign where wonum = '" & wonum & "'), " _
            + "x.POSEQIP, x.RESPONSIBLE, isnull(w.actstart, w.schedstart) as actstart, w.actfinish, w.description, w.worktype, w.wopriority, w.reportedby, wa.workarea, w.wopriority, w.superid, w.leadcraftid, " _
            + "w.targstartdate, w.targcompdate, cast(isnull(w.estlabhrs, 0) as decimal(10,2)) as estlabhrs, cast(isnull(w.estjplabhrs, 0) as decimal(10,2)) as estjplabhrs, w.qty, t.xcol, t.xcol2, w.pmid, w.tpmid, l.longdesc " _
            + "from workorder w " _
            + "left join equipment e on e.eqid = w.eqid " _
            + "left join equipment_xtra x on x.eqid = e.eqid " _
            + "left join workareas wa on wa.waid = w.waid " _
            + "left join wotype t on t.wotype = w.worktype " _
            + "left join wolongdesc l on l.wonum = w.wonum " _
            + "where w.wonum = '" & wonum & "'"
            dr = sc.GetRdrData(sql)
            While dr.Read
                superid = dr.Item("superid").ToString
                lcid = dr.Item("leadcraftid").ToString
                sConfigPosition = dr.Item("POSEQIP").ToString
                sEquipment = dr.Item("eqnum").ToString
                '****
                'If sEquipment = "" Then
                'sEquipment = "0"
                'End If
                '****
                'sService = dr.Item("worktype").ToString
                sService = dr.Item("xcol2").ToString
                'sService = "1"
                reportedby = dr.Item("reportedby").ToString
                'sResponsible = "1"
                sDescription = dr.Item("description").ToString
                '***
                'If sDescription = "" Then
                'sDescription = "NONE"
                'End If
                '***
                sDescription = wonum & " - " & sDescription
                sDescription = Mid(sDescription, 1, 39)
                sPlanningGroup = dr.Item("workarea").ToString
                '***
                'If sPlanningGroup = "" Then
                'sPlanningGroup = "0"
                'End If
                '***
                sPriority = dr.Item("wopriority").ToString
                '***
                'If sPriority = "" Then
                'sPriority = "0"
                'End If
                '***
                sStartDate = dr.Item("actstart").ToString
                If sStartDate <> "" Then
                    Try
                        Dim sdate As Date = sStartDate
                        sStartDate = sdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                    Catch ex As Exception

                    End Try
                End If

                sFinishDate = dr.Item("actfinish").ToString
                RPDT = dr.Item("actfinish").ToString
                If sFinishDate <> "" Then
                    Try
                        Dim fdate As Date = sFinishDate
                        Dim rdate As Date = sFinishDate
                        sFinishDate = fdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                        RPDT = rdate.ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                    Catch ex As Exception

                    End Try
                End If

                '****ADDED FOR TEST****

                'If sStartDate = "" Then
                'If sFinishDate <> "" Then
                'sStartDate = sFinishDate
                's3flag = "1"
                'Else
                'sStartDate = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                'sFinishDate = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                's3flag = "1"
                's4flag = "1"
                'End If
                'End If
                'If sFinishDate = "" Then
                'sFinishDate = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                's4flag = "1"
                'End If
                '****END ADDED FOR TEST****

                'errorCode1 = dr.Item("worktype").ToString
                'errorCode2 = dr.Item("worktype").ToString
                'errorCode3 = dr.Item("worktype").ToString
                iRetroFlag = "1"
                sItemNumber = dr.Item("eqnum").ToString
                '***
                'If sItemNumber = "" Then
                'sItemNumber = "0"
                'End If
                '***
                sLotNumber = "1"
                sFacility = "CAN"
                sRequestedStartDte = dr.Item("targstartdate").ToString
                If sRequestedStartDte <> "" Then
                    Try
                        Dim rsfdate As Date = sRequestedStartDte
                        sRequestedStartDte = rsfdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                    Catch ex As Exception

                    End Try
                End If
                sRequestedFinishDte = dr.Item("targcompdate").ToString
                If sRequestedFinishDte <> "" Then
                    Try
                        Dim rfdate As Date = sRequestedFinishDte
                        sRequestedFinishDte = rfdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                    Catch ex As Exception

                    End Try
                End If
                '****ADDED FOR TEST****

                'If sRequestedStartDte = "" Then
                'If sRequestedFinishDte <> "" Then
                'sRequestedStartDte = sRequestedFinishDte
                's1flag = "1"
                'Else
                'sRequestedStartDte = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                'sRequestedFinishDte = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                's1flag = "1"
                's2flag = "1"
                'End If
                'End If
                'If sRequestedFinishDte = "" Then
                'sRequestedFinishDte = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                's2flag = "1"
                'End If
                '****END ADDED FOR TEST****
                iSetupTime = dr.Item("estjplabhrs").ToString
                UMAS = dr.Item("estjplabhrs").ToString
                '***
                If wnums = "1" Then
                    If UMAS = "" Then
                        UMAS = "0"
                    End If
                End If

                '***
                iRuntime = dr.Item("estlabhrs").ToString
                UMAT = dr.Item("estlabhrs").ToString
                '***
                If wnums = "1" Then
                    If UMAT = "" Then
                        UMAT = "0"
                    End If
                End If

                '***
                iPlannedNumWkrs = dr.Item("qty").ToString
                sTextBlock = dr.Item("description").ToString
                ldesc = dr.Item("longdesc").ToString
                If ldesc <> "" Then
                    sTextBlock = wonum & " - " & sTextBlock & ldesc
                Else
                    sTextBlock = wonum & " - " & sTextBlock
                End If
                '***
                'If sTextBlock = "" Then
                'sTextBlock = "NONE"
                'End If
                '***
                iOperation = dr.Item("xcol").ToString
                OPNO = dr.Item("xcol").ToString
                pmid = dr.Item("pmid").ToString
                tpmid = dr.Item("tpmid").ToString
            End While
            dr.Close()
            sql = "select userid from pmsysusers where username = '" & reportedby & "'"
            Try
                reportedbyid = sc.strScalar(sql)
            Catch ex As Exception
                reportedbyid = ""
            End Try

            If wnums = "1" Then
                iPlannedQuantity = "1"
                If iSetupTime = "" Then
                    iSetupTime = "0"
                End If
                If iRuntime = "" Then
                    iRuntime = "0"
                End If
                If iPlannedNumWkrs = "" Then
                    iPlannedNumWkrs = "0"
                End If
            End If

            Dim ecd1id, ecd2id, ecd3id As String
            If pmid <> "" Then
                sql = "select top 1 ecd1id, ecd2id, ecd3id from pmtaskfailmodesman1 where ecd1id in (select ecd1id from ecd1) " _
                    + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = pmtaskfailmodesman1.ecd1id) " _
                    + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = pmtaskfailmodesman1.ecd2id) " _
                    + "and pmid = '" & pmid & "'"
                'dr = sc.GetRdrData(sql)
                'While dr.Read
                'ecd1id = dr.Item("ecd1id").ToString
                'ecd2id = dr.Item("ecd2id").ToString
                'ecd3id = dr.Item("ecd3id").ToString

                'End While
                'dr.Close()

            ElseIf tpmid <> "" Then
                'sql = "select top 1 ecd1id, ecd2id, ecd3id from pmtaskfailmodestpmman where ecd1id in (select ecd1id from ecd1) " _
                '+ "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = pmtaskfailmodestpmman.ecd1id) " _
                '+ "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = pmtaskfailmodestpmman.ecd2id) " _
                '+ "and pmid = '" & pmid & "'"
                'dr = sc.GetRdrData(sql)
                'While dr.Read
                'ecd1id = dr.Item("ecd1id").ToString
                'ecd2id = dr.Item("ecd2id").ToString
                'ecd3id = dr.Item("ecd3id").ToString

                'End While
                'dr.Close()

            Else
                sql = "select top 1 ecd1id, ecd2id, ecd3id from wofailmodes1 where ecd1id in (select ecd1id from ecd1) " _
                    + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = wofailmodes1.ecd1id) " _
                    + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = wofailmodes1.ecd2id) " _
                    + "and wonum = '" & wonum & "'"
                dr = sc.GetRdrData(sql)
                While dr.Read
                    ecd1id = dr.Item("ecd1id").ToString
                    ecd2id = dr.Item("ecd2id").ToString
                    ecd3id = dr.Item("ecd3id").ToString

                End While
                dr.Close()

            End If

            If ecd1id <> "" Then
                sql = "select top 1 e1.ecd1, e2.ecd2, e3.ecd3 " _
                    + "from ecd1 e1 " _
                    + "left join ecd2 e2 on e2.ecd1id = e1.ecd1id " _
                    + "left join ecd3 e3 on e3.ecd2id = e2.ecd2id " _
                    + "where e1.ecd1id = '" & ecd1id & "' and e2.ecd2id = '" & ecd2id & "' and e3.ecd3id = '" & ecd3id & "'"
                dr = sc.GetRdrData(sql)
                While dr.Read
                    sErrorCode1 = dr.Item("ecd1").ToString
                    sErrorCode2 = dr.Item("ecd2").ToString
                    sErrorCode3 = dr.Item("ecd3").ToString
                    FCLA = dr.Item("ecd1").ToString
                    FCL2 = dr.Item("ecd2").ToString
                    FCL3 = dr.Item("ecd3").ToString
                End While
                dr.Close()
            End If


            '***ADDED FOR TEST***
            'If sErrorCode1 = "" Then
            'sErrorCode1 = "0"
            'End If
            'If sErrorCode2 = "" Then
            'sErrorCode2 = "0"
            'End If
            'If sErrorCode3 = "" Then
            'sErrorCode3 = "0"
            'End If

            'If sComplaintType = "" Then
            'sComplaintType = "0"
            'End If
            '***END***

            If woecd = "1" Then
                If sErrorCode1 <> "" Then
                    sErrorCode1 = ""
                End If
                If sErrorCode2 <> "" Then
                    sErrorCode2 = ""
                End If
                If sErrorCode3 <> "" Then
                    sErrorCode3 = ""
                End If
            End If

            If sResponsible = "" Then
                If pmid = "" And tpmid = "" Then
                    If reportedbyid <> "" Then
                        sql = "select top 1 empno, waid, workarea from pmsysusers where userid = '" & reportedbyid & "'"
                        dr = sc.GetRdrData(sql)
                        While dr.Read
                            sup = dr.Item("empno").ToString
                            waid = dr.Item("waid").ToString
                            wa = dr.Item("workarea").ToString
                        End While
                        dr.Close()
                        If sup <> "" Then
                            sResponsible = sup
                            If wa <> "" Then
                                If sPlanningGroup = "" Then
                                    sPlanningGroup = wa
                                End If
                            Else
                                If waid <> "" And sPlanningGroup = "" Then
                                    sql = "select workarea from workareas where waid = '" & waid & "'"
                                    Try
                                        wa = sc.strScalar(sql)
                                    Catch ex As Exception
                                        wa = ""
                                    End Try
                                End If
                                If wa <> "" And sPlanningGroup = "" Then
                                    sPlanningGroup = wa
                                End If
                            End If
                        End If
                    End If
                Else
                    If superid <> "" Then
                        sql = "select top 1 empno, waid, workarea from pmsysusers where userid = '" & superid & "'"
                        dr = sc.GetRdrData(sql)
                        While dr.Read
                            sup = dr.Item("empno").ToString
                            waid = dr.Item("waid").ToString
                            wa = dr.Item("workarea").ToString
                        End While
                        dr.Close()
                        If sup <> "" Then
                            sResponsible = sup
                            If wa <> "" Then
                                If sPlanningGroup = "" Then
                                    sPlanningGroup = wa
                                End If
                            Else
                                If waid <> "" And sPlanningGroup = "" Then
                                    sql = "select workarea from workareas where waid = '" & waid & "'"
                                    Try
                                        wa = sc.strScalar(sql)
                                    Catch ex As Exception
                                        wa = ""
                                    End Try
                                End If
                                If wa <> "" And sPlanningGroup = "" Then
                                    sPlanningGroup = wa
                                End If
                            End If
                        End If
                    End If
                End If
            End If


            If sResponsible = "" Then
                'Dim strMessage As String = "No Supervisor or Labor Entry Found to Use for sResponsible"
                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If

            sql = "insert into trans_temp (sTransactionID, sConfigPosition, sEquipment, sStructureType, sService, sResponsible, iOperation, sDescription, " _
                                        + "sPlanningGroup, sPriority, sApprovedBy, sReasonCode, sStartDate, sFinishDate, " _
                                        + "sTxt1, sTxt2, sErrorCode1, sErrorCode2, sErrorCode3, sComplaintType, iRetroFlag, " _
                                        + "sItemNumber, sLotNumber, iPlannedQuantity, sFacility, sRequestedStartDte, sRequestedFinishDte, " _
                                        + "iSetupTime, iRuntime, iPlannedNumWkrs, sTextBlock) " _
                    + "values('" & sTransactionID & "','" & sConfigPosition & "','" & sEquipment & "','" & sStructureType & "','" & sService & "','" & sResponsible & "','" _
                    + iOperation & "','" & sDescription & "','" _
                    + sPlanningGroup & "','" & sPriority & "','" & sApprovedBy & "','" & sReasonCode & "','" & sStartDate & "','" & sFinishDate & "','" _
                    + sTxt1 & "','" & sTxt2 & "','" & sErrorCode1 & "','" & sErrorCode2 & "','" & sErrorCode3 & "','" & sComplaintType & "','" & iRetroFlag & "','" _
                    + sItemNumber & "','" & sLotNumber & "','" & iPlannedQuantity & "','" & sFacility & "','" & sRequestedStartDte & "','" & sRequestedFinishDte & "','" _
                    + iSetupTime & "','" & iRuntime & "','" & iPlannedNumWkrs & "','" & sTextBlock & "')"
            sc.Update(sql)



            Dim cansend As New Service1
            Try
                cansend.Url = System.Configuration.ConfigurationManager.AppSettings("service1")

            Catch ex As Exception
                'Dim strMessage As String = "URL Not Found - Check Web Config File"
                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            Dim retstring As String

            Dim retwonum, retval, errmsg As String
            Dim retwonum_chk As String
            Try
                sql = "select retwonum from workorder where wonum = '" & wonum & "'"
                dr = sc.GetRdrData(sql)
                While dr.Read
                    retwonum_chk = dr.Item("retwonum").ToString
                End While
                dr.Close()
            Catch ex As Exception
                retwonum_chk = ""
            End Try

            If retwonum_chk = "" Then
                retstring = cansend.OpenWorkOrder(sTransactionID, sConfigPosition, sEquipment, sStructureType, sService, sResponsible, iOperation, sDescription, _
                                              sPlanningGroup, sPriority, sApprovedBy, sReasonCode, sStartDate, sFinishDate, _
                                              sTxt1, sTxt2, sErrorCode1, sErrorCode2, sErrorCode3, sComplaintType, iRetroFlag, _
                                              sItemNumber, sLotNumber, iPlannedQuantity, sFacility, sRequestedStartDte, sRequestedFinishDte, _
                                              iSetupTime, iRuntime, iPlannedNumWkrs, sTextBlock)

                

                'tdgo.InnerHtml = retstring
                Dim retarr() As String = retstring.Split(",")

                isact = "active" 'lblisact.Value
                'TransactionID : 001  , Work Order Number : 2766814  , ReturnValue : 0  , Error Message 
                Try
                    Dim woarr() As String = retarr(1).Split(":")
                    retwonum = woarr(1)
                    Dim valarr() As String = retarr(2).Split(":")
                    retval = valarr(1)
                    Dim msgarr() As String = retarr(3).Split(":")
                    errmsg = msgarr(1)
                    MWNO = RTrim(retwonum)
                    MWNO = LTrim(MWNO)
                    retwonum = MWNO
                    If retval = 1 Then
                        sql = "insert into woout (fuswonum, sdate, retwonum, retval, errmsg) " _
                        + "values ('" & wonum & "',getdate(),'" & retwonum & "','" & retval & "','" & errmsg & "')"
                        sc.Update(sql)
                        sendtime(wonum, MWNO, OPNO, RPDT, UMAS, FCLA, FCL2, FCL3)
                        lcnt -= 1
                    Else
                        If isact = "run" Or isact = "active" Then
                            sql = "update workorder set retwonum = '" & retwonum & "' where wonum = '" & wonum & "'"
                            sc.Update(sql)

                            sql = "insert into lawson_track (sTransactionID, sConfigPosition, sEquipment, sStructureType, sService, sResponsible, iOperation, sDescription, " _
                                            + "sPlanningGroup, sPriority, sApprovedBy, sReasonCode, sStartDate, sFinishDate, " _
                                            + "sTxt1, sTxt2, sErrorCode1, sErrorCode2, sErrorCode3, sComplaintType, iRetroFlag, " _
                                            + "sItemNumber, sLotNumber, iPlannedQuantity, sFacility, sRequestedStartDte, sRequestedFinishDte, " _
                                            + "iSetupTime, iRuntime, iPlannedNumWkrs, sTextBlock, retwonum, fuswonum) " _
                        + "values('" & sTransactionID & "','" & sConfigPosition & "','" & sEquipment & "','" & sStructureType & "','" & sService & "','" & sResponsible & "','" _
                        + iOperation & "','" & sDescription & "','" _
                        + sPlanningGroup & "','" & sPriority & "','" & sApprovedBy & "','" & sReasonCode & "','" & sStartDate & "','" & sFinishDate & "','" _
                        + sTxt1 & "','" & sTxt2 & "','" & sErrorCode1 & "','" & sErrorCode2 & "','" & sErrorCode3 & "','" & sComplaintType & "','" & iRetroFlag & "','" _
                        + sItemNumber & "','" & sLotNumber & "','" & iPlannedQuantity & "','" & sFacility & "','" & sRequestedStartDte & "','" & sRequestedFinishDte & "','" _
                        + iSetupTime & "','" & iRuntime & "','" & iPlannedNumWkrs & "','" & sTextBlock & "','" & retwonum & "','" & wonum & "')"
                            sc.Update(sql)
                        End If
                        sendtime(wonum, MWNO, OPNO, RPDT, UMAS, FCLA, FCL2, FCL3)
                        lcnt -= 1

                    End If

                    lblMWNO.Value = retwonum
                Catch ex As Exception
                    'Try
                    'sql = "update workorder set retwonum = '" & retwonum & "' where wonum = '" & wonum & "'"
                    'sc.Update(sql)
                    'Catch ex0 As Exception

                    'End Try
                    Try
                        sql = "insert into woout (fuswonum, sdate, errovr) " _
                        + "values ('" & wonum & "',getdate(),'" & retstring & "')"
                        sc.Update(sql)
                    Catch ex1 As Exception

                    End Try
                    'Dim strMessage As String = "Problem with TimeEntry Service - Please Stop and Contact LAI"
                    'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End Try
            End If
            

        Else
        If lcnt <> "0" Then
            labflag = 1
            MWNO = lblMWNO.Value
            sql = "select e.eqnum, w.targstartdate, w.targcompdate, w.actlabhrs, pnw = (select count(*) from woassign where wonum = '" & wonum & "'), " _
           + "x.POSEQIP, x.RESPONSIBLE, w.actstart, w.actfinish, w.description, w.worktype, w.wopriority, w.reportedby, wa.workarea, w.wopriority, " _
           + "w.targstartdate, w.targcompdate, w.estlabhrs, w.estjplabhrs, w.qty, t.xcol, w.pmid, w.tpmid " _
           + "from workorder w " _
           + "left join equipment e on e.eqid = w.eqid " _
           + "left join equipment_xtra x on x.eqid = e.eqid " _
           + "left join workareas wa on wa.waid = w.waid " _
           + "left join wotype t on t.wotype = w.worktype " _
           + "where w.wonum = '" & wonum & "'"
            dr = sc.GetRdrData(sql)
            While dr.Read
                RPDT = dr.Item("actfinish").ToString
                If sFinishDate <> "" Then
                    Try
                        Dim fdate As Date = sFinishDate
                        Dim rdate As Date = sFinishDate
                        sFinishDate = fdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                        RPDT = rdate.ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                    Catch ex As Exception

                    End Try
                Else
                    Try
                        Dim rdate As Date = RPDT
                        RPDT = rdate.ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                    Catch ex As Exception

                    End Try
                End If
                'If RPDT = "" Then
                'RPDT = sFinishDate
                'End If
                iSetupTime = dr.Item("estjplabhrs").ToString
                UMAS = dr.Item("estjplabhrs").ToString
                UMAT = dr.Item("estlabhrs").ToString
                OPNO = dr.Item("xcol").ToString
                tpmid = dr.Item("tpmid").ToString
                pmid = dr.Item("pmid").ToString
            End While
            dr.Close()
            '***
            If wnums = "1" Then
                If UMAS = "" Then
                    UMAS = "0"
                End If
                If UMAT = "" Then
                    UMAT = "0"
                End If
            End If
            'If iSetupTime = "" Then
            'iSetupTime = "0"
            'End If

            '***
            Dim ecd1id, ecd2id, ecd3id As String
            If pmid <> "" Then
                    sql = "select top 1 ecd1id, ecd2id, ecd3id from pmtaskfailmodesman1 where ecd1id in (select ecd1id from ecd1) " _
                    + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = pmtaskfailmodesman1.ecd1id) " _
                    + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = pmtaskfailmodesman1.ecd2id) " _
                    + "and pmid = '" & pmid & "'"
                'dr = sc.GetRdrData(sql)
                'While dr.Read
                'ecd1id = dr.Item("ecd1id").ToString
                'ecd2id = dr.Item("ecd2id").ToString
                'ecd3id = dr.Item("ecd3id").ToString

                'End While
                'dr.Close()

            ElseIf tpmid <> "" Then
                sql = "select top 1 ecd1id, ecd2id, ecd3id from pmtaskfailmodestpmman where ecd1id in (select ecd1id from ecd1) " _
                    + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = pmtaskfailmodestpmman.ecd1id) " _
                    + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = pmtaskfailmodestpmman.ecd2id) " _
                    + "and pmid = '" & pmid & "'"
                'dr = sc.GetRdrData(sql)
                'While dr.Read
                'ecd1id = dr.Item("ecd1id").ToString
                'ecd2id = dr.Item("ecd2id").ToString
                'ecd3id = dr.Item("ecd3id").ToString

                'End While
                'dr.Close()

            Else
                    'sql = "select top 1 ecd1id, ecd2id, ecd3id from wofailmodes1 where ecd1id in (select ecd1id from ecd1) " _
                    '+ "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = wofailmodes1.ecd1id) " _
                    '+ "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = wofailmodes1.ecd2id) " _
                '+ "and pmid = '" & pmid & "'"
                    sql = "select top 1 ecd1id, ecd2id, ecd3id from wofailmodes1 where ecd1id in (select ecd1id from ecd1) " _
                + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = wofailmodes1.ecd1id) " _
                + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = wofailmodes1.ecd2id) " _
                + "and wonum = '" & wonum & "'"
                dr = sc.GetRdrData(sql)
                While dr.Read
                    ecd1id = dr.Item("ecd1id").ToString
                    ecd2id = dr.Item("ecd2id").ToString
                    ecd3id = dr.Item("ecd3id").ToString

                End While
                dr.Close()

            End If
            If ecd1id <> "" Then
                sql = "select top 1 e1.ecd1, e2.ecd2, e3.ecd3 " _
                + "from ecd1 e1 " _
                + "left join ecd2 e2 on e2.ecd1id = e1.ecd1id " _
                + "left join ecd3 e3 on e3.ecd2id = e2.ecd2id " _
                + "where e1.ecd1id = '" & ecd1id & "' and e2.ecd2id = '" & ecd2id & "' and e3.ecd3id = '" & ecd3id & "'"
                dr = sc.GetRdrData(sql)
                While dr.Read
                    sErrorCode1 = dr.Item("ecd1").ToString
                    sErrorCode2 = dr.Item("ecd2").ToString
                    sErrorCode3 = dr.Item("ecd3").ToString
                    FCLA = dr.Item("ecd1").ToString
                    FCL2 = dr.Item("ecd2").ToString
                    FCL3 = dr.Item("ecd3").ToString
                End While
                dr.Close()
            End If



            If woecd = "1" Then
                If FCLA <> "" Then
                    FCLA = ""
                End If
                If FCL2 <> "" Then
                    FCL2 = ""
                End If
                If FCL3 <> "" Then
                    FCL3 = ""
                End If
            End If
            sendtime(wonum, MWNO, OPNO, RPDT, UMAS, FCLA, FCL2, FCL3)
        End If
        End If


        'PageNumber = txtpg.Value
        'getwr(PageNumber)
        '[POSEQIP],[ITEMGRP],[STATUS], [SUBPROCESS], [RESPONSIBLE], [FIXEDASSET], [PRIORITY], [CRITICALITYCLS],[PLANNINGPOSITION], [EQID], [structureType]
        'configPosition [POSEQIP]
        'Equipment [eqnum] 
        'structureType ??? ***added to equipment_xtra  [structureType]
        'Service ***is this work type??? [worktype]
        'Responsible [RESPONSIBLE]
        'Description ***first 40 characters of work description??? [description] 
        'planningGroup ***[PLANNINGPOSITION] or planning area??? 
        'Priority [wopriority]
        'approvedBy ***not in Fusion - can we use a default??? 
        'reasonCode ??? 
        'startDate [actstart] 
        'finishDate [actfinish] 
        'txt1 ??? 
        'txt2 ??? 
        '***Nissan has many PM records with multiple error code values
        '***Will use top 1 for testing
        'errorCode1 [ecd1] 
        'errorCode2 [ecd2] 
        'errorCode3 [ecd3] 
        'complaintType ???
        'retroFlag ***always 1??? 
        'itemNumber ??? 
        'lotNumber ??? 
        'plannedQuantity ??? 
        'Facility ***always CAN 
        'requestedStartDte [targstartdate] 
        'requestedFinishDte [targcompdate]
        'setupTime ??? 
        'Runtime ***total time??? [actlabhrs]
        'plannedNumWkrs [pnw] ***calculated field 
        'textBlock ??? 
        'Operation ???
    End Sub
    Private Sub sendtime(ByVal wonum As String, ByVal MWNO As String, ByVal OPNO As String, ByVal RPDT As String, ByVal UMAS As String, _
                         ByVal FCLA As String, ByVal FCL2 As String, ByVal FCL3 As String)
        'need RPRE, EMNO, DOWT (responsible for reporting, emp no, down time 
        '? PCTP (reg or over), REND (manual completion flag)
        '? DLY1 = 0.00, DLY2 = 0.00 (should be numeric, no more info)
        'default PCTP as 3 for now
        'CONO set at top
        wnums = "1" 'lblwnums.Value
        If labflag = 1 Then
            'tdgo.InnerHtml = ""
        End If
        Dim DOWTP As String
        sql = "select cast(isnull((totaldown * 60), '0.00') as decimal(10,2)) as 'dowt', cast(isnull((totaldownp * 60), '0.00') as decimal(10,2)) as 'dowtp' from eqhist where wonum = '" & wonum & "'"
        'DOWT = sc.strScalar(sql)
        dr = sc.GetRdrData(sql)
        While dr.Read
            DOWT = dr.Item("dowt").ToString
            DOWTP = dr.Item("dowtp").ToString
        End While
        dr.Close()
        DLY1 = DOWTP

        If DOWT = "" Then
            DOWT = "0"
        End If
        If DOWTP = "" Then
            DOWTP = "0"
            DLY1 = "0"
        End If

        sql = "select cast(sum(isnull((w.minutes_man / 60),0)) as decimal(10,2)) as minutes_man, w.laborid, u.username, u.empno, u.uid, u.islabor from wolabtrans w " _
            + "left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "' and w.retwonum is null group by w.laborid, u.username, u.empno, u.uid, u.islabor "
        Dim empcnt As Integer = 0
        errcnt = 0
        lblerrcnt.Value = errcnt
        Dim laborid, username, popstring, uid, islabor As String
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds = sc.GetDSData(sql)
        dt = New DataTable
        dt = ds.Tables(0)
        Dim row As DataRow
        For Each row In dt.Rows
            empcnt += 1
            uid = row("uid").ToString
            islabor = row("islabor").ToString
            username = row("username").ToString
            EMNO = row("empno").ToString
            If islabor = "0" Then
                EMNO = uid
            End If
            'EMNO = "1"
            laborid = row("laborid").ToString
            UMAT = row("minutes_man").ToString
            If UMAT = "" Then
                UMAT = "0"
            End If
            'Dim UMAT_n As Long
            'Try

            'UMAT_n = System.Convert.ToDecimal(UMAT)
            'UMAT_n = Math.Round(UMAT_n, 2)
            'UMAT = UMAT_n

            'Catch ex As Exception
            '***set UMAT to 0 for test
            ' If wnums = "1" Then
            'UMAT = "0"
            'End If

            'End Try
            If EMNO = "" Then
                errcnt += 1
                lblerrcnt.Value = errcnt
                'need to collect data for pop-up here
                If popstring = "" Then
                    popstring = empcnt + "~" + username + "~" + laborid + "~" + wonum + "~" + DOWT + "~" + MWNO + "~" + OPNO + "~" + RPDT + "~" + UMAT + "~" + UMAS + "~" + FCLA + "~" + FCL2 + "~" + FCL3 + "~" _
                        + EMNO
                Else
                    popstring += "," + empcnt + "~" + username + "~" + laborid + "~" + wonum + "~" + DOWT + "~" + "~" + MWNO + "~" + OPNO + "~" + RPDT + "~" + UMAT + "~" + UMAS + "~" + FCLA + "~" + FCL2 + "~" + FCL3 + "~" _
                        + EMNO
                End If
            Else
                PCTP = "3"
                If wnums = "1" Then
                    '*** set REND to 0 for test
                    REND = "0"
                    'DLY1 = "0.00"
                    DLY2 = "0.00"
                    '***set RPRE to 0 for test
                    'RPRE = "0"
                    sql = "select top 1 u.empno from wolabtrans w " _
            + "left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "' and retwonum is null"
                    Try
                        RPRE = "" 'sc.strScalar(sql)
                    Catch ex As Exception
                        RPRE = ""
                        'Dim strMessage As String = "No Labor Entry Found to Use for sResponsible"
                        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        'Exit Sub
                    End Try
                    If RPRE = "" Then
                        'Dim strMessage As String = "No Labor Entry Found to Use for sResponsible"
                        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        'Exit Sub
                    End If
                End If


                'sWorkOrdNum,sOpNum,sWorkDate,sUserId,sEmpNumbr,sLabrHrs,sCostTyp,sCloseOperat,sLabrSetupTim,
                'sFailurCls, sErrCde2, sErrCde3, sDownTim, sDelyTim1, sDelyTim2
                timeentry(wonum, laborid, MWNO, OPNO, RPDT, RPRE, EMNO, UMAT, PCTP, REND, UMAS, FCLA, FCL2, FCL3, DOWT, DLY1, DLY2)

            End If
            'check data and send - use else in above or separate proc?
            'error checking on return?

        Next
        'errcnt = lblerrcnt.Value
        If errcnt <> 0 Then
            'tdgo.InnerHtml += "<br />" & errcnt & " Time Entry Transfers Failed"

        End If
    End Sub
    Private Sub timeentry(ByVal wonum As String, ByVal laborid As String, _
       ByVal MWNO As String, _
       ByVal OPNO As String, _
       ByVal RPDT As String, _
       ByVal RPRE As String, _
       ByVal EMNO As String, _
       ByVal UMAT As String, _
       ByVal PCTP As String, _
       ByVal REND As String, _
       ByVal UMAS As String, _
       ByVal FCLA As String, _
       ByVal FCL2 As String, _
       ByVal FCL3 As String, _
       ByVal DOWT As String, _
       ByVal DLY1 As String, _
       ByVal DLY2 As String)
        '***TEST***
        'rem 04/30/2012 per Nissan
        FCLA = ""
        FCL2 = ""
        FCL3 = ""
        '*********
        wnums = "1" 'lblwnums.Value
        If wnums = "1" Then
            REND = "1"
            'DLY1 = "0.00"
            DLY2 = "0.00"
        End If
        PCTP = "3"

        Dim errcnt As Integer
        Try
            errcnt = lblerrcnt.Value
        Catch ex As Exception
            errcnt = 0
            lblerrcnt.Value = "0"
        End Try


        Dim cansend As New Service2
        Try
            cansend.Url = System.Configuration.ConfigurationManager.AppSettings("service2")

        Catch ex As Exception
            'Dim strMessage As String = "Time Entry URL Not Found - Check Web Config File"
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub

        End Try
        Dim retstring As String
        Dim ds As New DataSet

        Dim te(0) As TimeEntryInputVar
        te(0).sOpNum = OPNO
        te(0).sWorkDate = RPDT
        te(0).sUserId = RPRE
        te(0).sEmpNumbr = EMNO
        te(0).sLabrHrs = UMAT
        te(0).sCostTyp = PCTP
        te(0).sCloseOperat = REND
        te(0).sLabrSetupTim = UMAS
        te(0).sFailurCls = FCLA
        te(0).sErrCde2 = FCL2
        te(0).sErrCde3 = FCL3
        te(0).sDownTim = DOWT
        te(0).sDelyTim1 = DLY1
        te(0).sDelyTim2 = DLY2

        If sTransactionID = "" Then
            sql = "select seed from pmseed where pmtable = 'service'; update pmseed set seed = seed + 1 where pmtable = 'service'"
            sTransactionID = sc.strScalar(sql)
        End If

        Dim sWorkOrdNum As String = MWNO
        sWorkOrdNum = RTrim(sWorkOrdNum)
        sWorkOrdNum = LTrim(sWorkOrdNum)
        'redundant check !!! won't work in test
        'Dim rwochk As String
        'sql = "select retwonum from workorder where wonum = '" & wonum & "'"
        'dr = sc.GetRdrData(sql)
        'While dr.Read
        'rwochk = dr.Item("retwonum").ToString
        'End While
        'dr.Close()
        'If sWorkOrdNum <> rwochk Then
        'sWorkOrdNum = rwochk
        'End If
        '***
        sql = "insert into trans_temp_time (sTransactionID, sWorkOrdNum,sOpNum,sWorkDate,sUserId,sEmpNumbr,sLabrHrs,sCostTyp,sCloseOperat,sLabrSetupTim,sFailurCls,sErrCde2,sErrCde3,sDownTim,sDelyTim1,sDelyTim2) " _
            + "values ('" & sTransactionID & "','" & sWorkOrdNum & "','" & OPNO & "','" & RPDT & "','" & RPRE & "','" & EMNO & "','" & UMAT & "','" & PCTP & "','" & REND & "','" & UMAS & "','" & FCLA & "','" & FCL2 & "','" & FCL3 & "','" & DOWT & "','" & DLY1 & "','" & DLY2 & "')"
        sc.Update(sql)

        ds = cansend.TimeEntry(sWorkOrdNum, te)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        Dim drM As DataRow
        Dim errval, errval1, errval2 As String
        For Each drM In dt.Select()
            errval = drM(0).ToString
            errval1 = drM(1).ToString
            Try
                errval2 = drM(2).ToString
            Catch ex As Exception
                errval2 = ""
            End Try

            If retstring = "" Then
                If errval2 <> "" Then
                    retstring = errval + " - " + errval1 + " - " + errval2
                Else
                    retstring = errval + " - " + errval1
                End If

            Else
                If errval2 <> "" Then
                    retstring += ", " + errval + " - " + errval1 + " - " + errval2
                Else
                    retstring += ", " + errval + " - " + errval1
                End If

            End If
        Next
        Dim rtst As String = "<br /><br />Time Entry Return - Lawson Work Order Number as sent: " & sWorkOrdNum & " - Time Entry Return Message: " & retstring
        'tdgo.InnerHtml += "<br /><br />Time Entry Return - Lawson Work Order Number as sent: " & sWorkOrdNum & " - Time Entry Return Message: " & retstring & "<br /><br /><br /><br />"
        Dim retval, errmsg As String
        isact = "active" 'lblisact.Value
        Try
            'Dim retarr() As String = retstring.Split(",")
            'Dim valarr() As String = retarr(0).Split(":")
            'retval = valarr(1)
            'Dim msgarr() As String = retarr(1).Split(":")
            'errmsg = msgarr(1)
            If errval <> "0" Then
                sql = "insert into woout_te (fuswonum, sdate, retwonum, errmsg) " _
                + "values ('" & wonum & "',getdate(),'" & sWorkOrdNum & "','" & retstring & "')"
                sc.Update(sql)
            Else
                If isact = "run" Or isact = "active" Then
                    'sql = "select sum(isnull(w.minutes_man,0)) as minutes_man, w.laborid, u.username, u.empno from wolabtrans w left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "'"
                    sql = "update wolabtrans set retwonum = '" & sWorkOrdNum & "' where wonum = '" & wonum & "' and laborid = '" & laborid & "'"
                    sc.Update(sql)
                End If

            End If
        Catch ex As Exception
            'Try
            'sql = "update wolabtrans set retwonum = '" & MWNO & "' where wonum = '" & wonum & "' and laborid = '" & laborid & "'"
            'sc.Update(sql)
            'Catch ex0 As Exception

            'End Try
            Try
                sql = "insert into woout_te (fuswonum, sdate, errovr) " _
                    + "values ('" & wonum & "',getdate(),'" & retstring & "')"
                sc.Update(sql)
            Catch ex1 As Exception

            End Try
            errcnt += 1
            'lblerrcnt.Value = errcnt
            'Dim strMessage As String = "Problem with TimeEntry Return Value - Please Stop and Contact LAI"
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
End Class

