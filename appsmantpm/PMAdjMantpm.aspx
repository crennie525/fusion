<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMAdjMantpm.aspx.vb" Inherits="lucy_r12.PMAdjMantpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMAdjMan</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMAdjMantpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table>
        <tr height="26">
            <td class="label">
                <asp:Label ID="lang788" runat="server">Current Frequency</asp:Label>
            </td>
            <td class="plainlabel" id="tdcurr" runat="server">
            </td>
            <td class="label">
                <asp:Label ID="lang789" runat="server">Days</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang790" runat="server">P-F Interval</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtpfint" runat="server" CssClass="plainlabel" Width="40px"></asp:TextBox>
            </td>
            <td class="label">
                <asp:Label ID="lang791" runat="server">Days</asp:Label>
            </td>
            <td>
                <img id="btnpfint" onmouseover="return overlib('Estimate Frequency using the PF Interval')"
                    onclick="getPFDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/lilcalc.gif"
                    width="20">
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang792" runat="server">Choose Frequency</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtfreq" runat="server"></asp:TextBox>
            </td>
            <td class="label">
                <asp:Label ID="lang793" runat="server">Days</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="right">
                <img onmouseover="return overlib('Save Changes Above', ABOVE, LEFT)" onclick="savetask();"
                    onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif"
                    width="20">
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblpmtid" runat="server">
    <input type="hidden" id="lbleqid" runat="server">
    <input type="hidden" id="lblpmtskid" runat="server">
    <input type="hidden" id="lblpfreq" runat="server">
    <input type="hidden" id="lbltfreq" runat="server">
    <input type="hidden" id="lblpfint" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
