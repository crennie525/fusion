

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMMainMantpm
    Inherits System.Web.UI.Page
    Protected WithEvents ovid110 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid109 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid108 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid107 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid106 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid105 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents iwi As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents iwa As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents imgi2 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents imgi As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents imga2 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents imga As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang880 As System.Web.UI.WebControls.Label

    Protected WithEvents lang879 As System.Web.UI.WebControls.Label

    Protected WithEvents lang878 As System.Web.UI.WebControls.Label

    Protected WithEvents lang877 As System.Web.UI.WebControls.Label

    Protected WithEvents lang876 As System.Web.UI.WebControls.Label

    Protected WithEvents lang875 As System.Web.UI.WebControls.Label

    Protected WithEvents lang874 As System.Web.UI.WebControls.Label

    Protected WithEvents lang873 As System.Web.UI.WebControls.Label

    Protected WithEvents lang872 As System.Web.UI.WebControls.Label

    Protected WithEvents lang871 As System.Web.UI.WebControls.Label

    Protected WithEvents lang870 As System.Web.UI.WebControls.Label

    Protected WithEvents lang869 As System.Web.UI.WebControls.Label

    Protected WithEvents lang868 As System.Web.UI.WebControls.Label

    Protected WithEvents lang867 As System.Web.UI.WebControls.Label

    Protected WithEvents lang866 As System.Web.UI.WebControls.Label

    Protected WithEvents lang865 As System.Web.UI.WebControls.Label

    Protected WithEvents lang864 As System.Web.UI.WebControls.Label

    Protected WithEvents lang863 As System.Web.UI.WebControls.Label

    Protected WithEvents lang862 As System.Web.UI.WebControls.Label

    Protected WithEvents lang861 As System.Web.UI.WebControls.Label

    Protected WithEvents lang860 As System.Web.UI.WebControls.Label

    Protected WithEvents lang859 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim man As New Utilities
    Dim mu As New mmenu_utils_a
    Dim sid, cid, srch, jump, eqid, fuid, coid, typ, ro, islabor, issched, userid, isplanner, cadm As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = "10"
    Protected WithEvents cbbylab As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents TextBox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txts As System.Web.UI.WebControls.TextBox
    Protected WithEvents txte As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddpdmlist As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblprint As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dddepts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcells As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddeq As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddfunc As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcomp As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtsrchwo As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddstatus As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtsup As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlead As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblsrch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifrt As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents txtrte As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblrtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgrteprint As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgrtewprint As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblprintwolist As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgwopm As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblfiltwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldocs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgregpm As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcell As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trdepts As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trlocs3 As System.Web.UI.HtmlControls.HtmlTableRow

    Protected WithEvents tddept As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcell As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdco As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdloc3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdco3 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents lblsupstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblleadstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbls As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lble As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrt As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lbluserid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblislabor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblissched As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisplanner As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcadm As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cnt As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rptrtasks As System.Web.UI.WebControls.Repeater

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            issched = mu.Is_Sched
            lblissched.Value = issched
            Try
                userid = HttpContext.Current.Session("userid").ToString()
                islabor = HttpContext.Current.Session("islabor").ToString()
                isplanner = HttpContext.Current.Session("isplanner").ToString()
                cadm = HttpContext.Current.Session("cadm").ToString()
                lbluserid.Value = userid
                lblislabor.Value = islabor
                lblisplanner.Value = isplanner
                lblcadm.Value = cadm
            Catch ex As Exception

            End Try
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            sid = Request.QueryString("sid").ToString 'HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            man.Open()
            txtpg.Value = PageNumber
            'LoadSuper()
            LoadSkill()
            GetPdM()
            'GetDept()
            GetLists()
            'PopEq()
            'ddsuper.SelectedIndex = 0
            Try
                jump = Request.QueryString("jump").ToString
                If jump = "yes" Then
                    typ = Request.QueryString("typ").ToString
                    lbltyp.Value = typ
                    If typ = "eq" Then
                        eqid = Request.QueryString("eqid").ToString
                        lbleqid.Value = eqid
                    ElseIf typ = "fu" Then
                        fuid = Request.QueryString("fuid").ToString
                        lblfuid.Value = fuid
                    ElseIf typ = "co" Then
                        coid = Request.QueryString("coid").ToString
                        lblcoid.Value = coid
                    Else
                        lbltyp.Value = "no"
                    End If
                Else
                    lbltyp.Value = "no"
                End If
            Catch ex As Exception
                lbltyp.Value = "no"
            End Try
            GetPg(PageNumber)
            man.Dispose()
            'ddcells.Enabled = False
            lblprint.Value = "no"
        Else
            If Request.Form("lblprint") = "go" Then
                man.Open()
                GetPg(PageNumber, "yes")
                man.Dispose()
            End If
            If Request.Form("lblret") = "next" Then
                man.Open()
                GetNext()
                man.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                man.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetPg(PageNumber)
                man.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                man.Open()
                GetPrev()
                man.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                man.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetPg(PageNumber)
                man.Dispose()
                lblret.Value = ""
            End If
        End If
        'ibfilt.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov87" , "PMMainMantpm.aspx.vb") & "', BELOW, LEFT)")
        'ibfilt.Attributes.Add("onmouseout", "return nd()")

    End Sub
    Private Sub GetLists()
        'get default
        Dim def As String
        sql = "select wostatus from wostatus where compid = '" & cid & "' and isdefault = 1"
        dr = man.GetRdrData(sql)
        While dr.Read
            def = dr.Item("wostatus").ToString
        End While
        dr.Close()
        sql = "select wostatus from wostatus where compid = '" & cid & "' and active = 1"
        dr = man.GetRdrData(sql)
        ddstatus.DataSource = dr
        ddstatus.DataTextField = "wostatus"
        ddstatus.DataValueField = "wostatus"
        ddstatus.DataBind()
        dr.Close()
        ddstatus.Items.Insert(0, New ListItem("Select"))
        ddstatus.Items(0).Value = 0
        If def <> "" Then
            'ddstatus.SelectedValue = def
        Else
            'ddstatus.Items.Insert(0, New ListItem("Select"))
            'ddstatus.Items(0).Value = 0
        End If

    End Sub
    Private Sub GetCell()
        sid = HttpContext.Current.Session("dfltps").ToString
        Dim dept As String = dddepts.SelectedValue.ToString
        sql = "select c.cellid, c.cell_name from cells c where c.dept_id = '" & dept & "'"
        dr = man.GetRdrData(sql)
        ddcells.DataSource = dr
        ddcells.DataValueField = "cellid"
        ddcells.DataTextField = "cell_name"
        ddcells.DataBind()
        dr.Close()
        'ddskill.Items.Insert(0, "Select Skill")
        ddcells.Items.Insert(0, "All Cells")
        Try
            ddcells.SelectedIndex = 0
        Catch ex As Exception

        End Try

        ddcells.Enabled = True
    End Sub
    Private Sub GetDept()
        sid = HttpContext.Current.Session("dfltps").ToString
        sql = "select d.dept_id, d.dept_line from dept d where d.siteid = '" & sid & "'"
        dr = man.GetRdrData(sql)
        dddepts.DataSource = dr
        dddepts.DataValueField = "dept_id"
        dddepts.DataTextField = "dept_line"
        dddepts.DataBind()
        dr.Close()
        dddepts.Items.Insert(0, "All Departments")
        Try
            dddepts.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub PopEq()
        sid = HttpContext.Current.Session("dfltps").ToString
        Dim dt, val As String
        Dim eqcnt As Integer
        Dim filt As String
        sql = "select count(*) from equipment where siteid = '" & sid & "'"
        filt = " where siteid = '" & sid & "'"
        eqcnt = man.Scalar(sql)
        If eqcnt > 0 Then
            dt = "equipment"
            val = "eqid, eqnum "
            dr = man.GetList(dt, val, filt)
            ddeq.DataSource = dr
            ddeq.DataTextField = "eqnum"
            ddeq.DataValueField = "eqid"
            ddeq.DataBind()
            dr.Close()
            ddeq.Items.Insert(0, "Select Equipment")
            'eqdiv.Style.Add("display", "block")
            'eqdiv.Style.Add("visibility", "visible")
            ddeq.Enabled = True
        Else
            'Dim strMessage As String =  tmod.getmsg("cdstr376" , "PMMainMantpm.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ddeq.Items.Insert(0, "Select Equipment")
            'eqdiv.Style.Add("display", "block")
            'eqdiv.Style.Add("visibility", "visible")
            ddeq.Enabled = False
        End If

    End Sub
    Private Sub GetPdM()
        sid = HttpContext.Current.Session("dfltps").ToString
        sql = "select distinct pm.ptid, pm.pretech from equipment e " _
        + "right join pm pm on pm.eqid = e.eqid " _
        + "where e.siteid = '" + sid + "' and ptid <> 0"
        dr = man.GetRdrData(sql)
        ddpdmlist.DataSource = dr
        ddpdmlist.DataValueField = "ptid"
        ddpdmlist.DataTextField = "pretech"
        ddpdmlist.DataBind()
        dr.Close()
        'ddskill.Items.Insert(0, "Select Skill")
        ddpdmlist.Items.Insert(0, "All Skills")
        Try
            ddpdmlist.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub

    Private Sub LoadSkill()
        sql = "select distinct p.skillid, p.skill from pm p left join equipment e on e.eqid = p.eqid"
        dr = man.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataValueField = "skillid"
        ddskill.DataTextField = "skill"
        ddskill.DataBind()
        dr.Close()
        'ddskill.Items.Insert(0, "Select Skill")
        ddskill.Items.Insert(0, "All Skills")
        Try
            ddskill.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Function fixsrch(ByVal srch As String)
        srch = man.ModString2(srch)
        Return srch
    End Function
    Private Sub GetPg(ByVal PageNumber As Integer, Optional ByVal pall As String = "no")
        Dim filt, s, e, Filter, FilterCNT, srch As String
        sid = HttpContext.Current.Session("dfltps").ToString
        cid = "0"
        issched = lblissched.Value
        userid = lbluserid.Value
        isplanner = lblisplanner.Value
        islabor = lblislabor.Value
        cadm = lblcadm.Value

        Dim sup, nex, las As String
        Dim sflag As Integer = 0
        typ = lbltyp.Value
        'If typ = "no" Then
        If lblsrch.Value = "yes" Then
            Filter = "e.siteid = " & sid & " and pm.nextdate is not null "
            FilterCNT = "e.siteid = '" & sid & "' and pm.nextdate is not null "
        Else
            Filter = "e.siteid = " & sid & " and pm.nextdate is not null "
            FilterCNT = "e.siteid = '" & sid & "' and pm.nextdate is not null "
        End If

        If txtsrchwo.Text <> "" Then
            srch = txtsrchwo.Text
            srch = fixsrch(srch)
            Dim woi As Long
            Try
                woi = System.Convert.ToInt32(srch)
            Catch ex As Exception
                Dim strMessage As String = "Work Order Number Must Be A Numeric Value"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            Filter += "and pm.wonum = ''" & srch & "'' "
            FilterCNT += "and pm.wonum = '" & srch & "' "
        End If

        If ddstatus.SelectedIndex <> 0 And ddstatus.SelectedIndex <> -1 Then
            Filter += "and w.status = ''" & ddstatus.SelectedValue.ToString & "'' "
            FilterCNT += "and w.status = '" & ddstatus.SelectedValue.ToString & "' "
        End If

        If lblsup.Value <> "" Then
            srch = lblsup.Value 'txtsup.Text
            srch = lblsup.Value 'fixsrch(srch)
            Filter += "and pm.superid = ''" & srch & "'' "
            FilterCNT += "and pm.superid = '" & srch & "' "
        End If
        If lbllead.Value <> "" Then
            srch = lbllead.Value 'txtlead.Text
            srch = fixsrch(srch)
            Filter += "and pm.leadid = ''" & srch & "'' "
            FilterCNT += "and pm.leadid = '" & srch & "' "
        End If

        If lblrtid.Value <> "" Then
            srch = lblrtid.Value
            srch = fixsrch(srch)
            Filter += "and pm.rid = ''" & lblrtid.Value & "'' "
            FilterCNT += "and pm.rid = '" & lblrtid.Value & "' "
        End If

       
        'And dddepts.SelectedIndex <> 0
        If lbldept.Value <> "" Then
            Filter += "and e.dept_id = ''" & lbldid.Value & "'' "
            FilterCNT += "and e.dept_id = '" & lbldid.Value & "' "
        End If
        If lblclid.Value <> "" Then
            Filter += "and e.cellid = ''" & lblclid.Value & "'' "
            FilterCNT += "and e.cellid = '" & lblclid.Value & "' "
        End If
        Dim par As String = lbllocstr.Value
        If lbllid.Value <> "" And par <> "" Then
            'e.eqnum in (select h.location from pmlocheir h where h.parent = 'CRTST')
            'Filter += "and e.locid = ''" & lbllid.Value & "'' "
            'FilterCNT += "and e.locid = '" & lbllid.Value & "' "
            Filter += "and e.eqnum in (select h.location from pmlocheir h where h.parent = ''" & par & "'')"
            FilterCNT += "and e.eqnum in (select h.location from pmlocheir h where h.parent = '" & par & "')"
            lblloc.Value = lbllocstr.Value
        End If

        If lbleqid.Value <> "" Then
            Filter += "and pm.eqid = ''" & lbleqid.Value & "'' "
            FilterCNT += "and pm.eqid = '" & lbleqid.Value & "' "
        End If
        If lblfuid.Value <> "" Then
            Filter += "and pt.funcid = ''" & lblfuid.Value & "'' "
            FilterCNT += "and pt.funcid = '" & lblfuid.Value & "' "
        End If
        If lblcomid.Value <> "" Then
            Filter += "and pt.comid = ''" & lblcomid.Value & "'' "
            FilterCNT += "and pt.comid = '" & lblcomid.Value & "' "
        End If
        If ddskill.SelectedIndex <> 0 And ddskill.SelectedIndex <> -1 Then
            Filter += "and pm.skillid = ''" & ddskill.SelectedValue.ToString & "'' "
            FilterCNT += "and pm.skillid = '" & ddskill.SelectedValue.ToString & "' "
        End If

        If ddpdmlist.SelectedIndex <> 0 And ddpdmlist.SelectedIndex <> -1 Then
            FilterCNT += "and pm.ptid = '" & ddpdmlist.SelectedValue.ToString & "'"
            Filter += "and pm.ptid = ''" & ddpdmlist.SelectedValue.ToString & "''"
        End If

        Dim rtid As String
        If lblrt.Value <> "" Then
            rtid = lblrtid.Value
            FilterCNT += "and pm.rid = '" & rtid & "'"
            Filter += "and pm.rid = ''" & rtid & "''"
            imgrteprint.Attributes.Add("class", "")
            imgrtewprint.Attributes.Add("class", "")

            imgregpm.Attributes.Add("class", "details")
            imgwopm.Attributes.Add("class", "details")
        Else
            imgrteprint.Attributes.Add("class", "details")
            imgrtewprint.Attributes.Add("class", "details")
            If lblsrch.Value = "yes" Then
                imgregpm.Attributes.Add("class", "")
                imgwopm.Attributes.Add("class", "")
            Else
                imgregpm.Attributes.Add("class", "details")
                imgwopm.Attributes.Add("class", "details")
            End If
        End If
        'Else
        If typ = "eq" Then
            eqid = lbleqid.Value
            FilterCNT += "and pm.eqid = '" & eqid & "'"
            Filter += "and pm.eqid = ''" & eqid & "''"
        ElseIf typ = "fu" Then
            fuid = lblfuid.Value
            FilterCNT += "and pt.funcid = '" & fuid & "'"
            Filter += "and pt.funcid = ''" & fuid & "''"
        ElseIf typ = "co" Then
            coid = lblcoid.Value
            FilterCNT += "and pt.comid = '" & coid & "'"
            Filter += "and pt.comid = ''" & coid & "''"
        End If
        'End If
        'And issched = "0"
        If cadm <> "1" And cbbylab.Checked = False Then
            If islabor = "1" And isplanner <> "1" Then
                If Filter <> "" Then
                    Filter += " and (w.eqid in (select eqid from pmlaborlocs where laborid = ''" & userid & "'') or " _
                        + "w.eqid in (select eqid from workorder where leadcraftid = ''" & userid & "''))"
                    FilterCNT += " and (w.eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "') or " _
                        + "w.eqid in (select eqid from workorder where leadcraftid = '" & userid & "'))"
                Else
                    Filter += " (w.eqid in (select eqid from pmlaborlocs where laborid = ''" & userid & "'') or " _
                       + "w.eqid in (select eqid from workorder where leadcraftid = ''" & userid & "''))"
                    FilterCNT += " (w.eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "') or " _
                        + "w.eqid in (select eqid from workorder where leadcraftid = '" & userid & "'))"
                End If

            End If
        End If

        s = lbls.Value 'fixsrch(txts.Text)
        e = lble.Value 'fixsrch(txte.Text)
        If Len(s) <> 0 Then
            If Len(e) <> 0 Then
                FilterCNT += " and pm.nextdate between '" & s & "' and '" & e & "'"
                Filter += " and pm.nextdate between ''" & s & "'' and ''" & e & "''"
            Else
                FilterCNT += " and pm.nextdate >= '" & s & "'"
                Filter += " and pm.nextdate >= ''" & s & "''"
            End If
        End If

        sql = "select count(distinct pm.pmid) from tpm pm " _
        + "left join equipment e on e.eqid = pm.eqid " _
        + "left join workorder w on w.wonum = pm.wonum " _
        + "left join pmroutes pr on pr.rid = pm.rid " _
        + "left join pmtracktpm pt on pt.pmid = pm.pmid " _
               + " where " + FilterCNT

        Dim intPgCnt, intPgNav As Integer
        intPgCnt = man.Scalar(sql)


        If intPgCnt > 0 Then
            txtpg.Value = PageNumber
            intPgNav = man.PageCountRev(intPgCnt, PageSize)
            sql = "usp_getAllPMPg1tpm '" & sid & "', '" & PageNumber & "', '" & PageSize & "', '" & Filter & "'"
            Dim ds As New DataSet
            ds = man.GetDSData(sql)
            Dim dt As New DataTable
            dt = ds.Tables(0)
            rptrtasks.DataSource = dt
            rptrtasks.DataBind()
            Dim i, eint As Integer
            eint = 15
            lblfiltwo.Value = " where " & FilterCNT
            lblfilt.Value = " where " & FilterCNT
            filt = " where " & FilterCNT
            'GetDocs(filt)
            Dim row As DataRow
            Dim pmidstr, pmid As String
            For Each row In dt.Rows
                pmid = row("pmid").ToString
                If pmidstr = "" Then
                    pmidstr = pmid
                Else
                    pmidstr += "," & pmid
                End If
            Next

            Dim pmidarr() As String = pmidstr.Split(",")
            Dim sbc As Integer
            Dim doc, docs, fn, ty As String
            For i = 0 To pmidarr.Length - 1
                pmid = pmidarr(i).ToString
                'sql = "select count(*) from pmManAttach where pmid = '" & pmid & "'"
                'sbc = man.Scalar(sql)
                'If sbc > 0 Then ' should be using has rows instead of count
                sql = "select * from pmManAttach where pmid = '" & pmid & "'"
                ds = man.GetDSData(sql)
                dt = ds.Tables(0)
                'dr = man.GetRdrData(sql)
                'While dr.Read
                For Each row In dt.Rows
                    fn = row("filename").ToString 'dr.Item("filename").ToString
                    ty = row("doctype").ToString 'dr.Item("doctype").ToString
                    doc = ty & "~" & fn
                    If docs = "" Then
                        docs = doc
                    Else
                        docs += ";" & doc
                    End If
                Next

                'End While
                'dr.Close()
                lbldocs.Value = docs
                'End If
            Next
            If pall = "yes" Then
                lblprint.Value = "yes"
            End If
        Else
            lblfiltwo.Value = ""
            lblfilt.Value = ""
            lblprint.Value = "no"
            imgregpm.Attributes.Add("class", "details")
            imgwopm.Attributes.Add("class", "details")
            imgrteprint.Attributes.Add("class", "details")
            imgrtewprint.Attributes.Add("class", "details")
            Dim strMessage As String = tmod.getmsg("cdstr377", "PMMainMantpm.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        Dim who As String = lblwho.Value
        If who = "depts" Then
            trdepts.Attributes.Add("class", "view")
            'tddept.InnerHtml = lbldept.Value
            'tdcell.InnerHtml = lblcell.Value
            'tdeq.InnerHtml = lbleq.Value
            'tdfu.InnerHtml = lblfu.Value
            'tdco.InnerHtml = lblcomp.Value
        Else
            trdepts.Attributes.Add("class", "details")
        End If
        If who = "locs" Then
            trlocs3.Attributes.Add("class", "view")
            'tdloc3.InnerHtml = lblloc.Value
            'tdeq3.InnerHtml = lbleq.Value
            'tdfu3.InnerHtml = lblfu.Value
            'tdco3.InnerHtml = lblcomp.Value
        Else
            trlocs3.Attributes.Add("class", "details")
        End If
    End Sub
    Private Sub GetDocs(ByVal filt As String)
        sql = "select distinct pm.pmid from tpm pm left join equipment e on e.eqid = pm.eqid " _
        + "left join workorder w on w.pmid = pm.pmid " _
        + "left join pmroutes pr on pr.rid = pm.rid " _
        + "left join pmtracktpm pt on pt.pmid = pm.pmid " & filt
        Dim pmidstr, pmid As String
        Dim sbc As Integer
        Dim doc, docs, fn, ty As String
        dr = man.GetRdrData(sql)
        While dr.Read
            If pmidstr = "" Then
                pmidstr = dr.Item("pmid").ToString
            Else
                pmidstr += "," & dr.Item("pmid").ToString
            End If
        End While
        dr.Close()
        Dim pmidarr() As String = pmidstr.Split(",")
        Dim i As Integer
        For i = 0 To pmidarr.Length - 1
            pmid = pmidarr(i).ToString
            sql = "select count(*) from pmManAttachtpm where pmid = '" & pmid & "'"
            sbc = man.Scalar(sql)
            If sbc > 0 Then ' should be using has rows instead of count
                sql = "select * from pmManAttachtpm where pmid = '" & pmid & "'"
                dr = man.GetRdrData(sql)
                While dr.Read
                    fn = dr.Item("filename").ToString
                    ty = dr.Item("doctype").ToString
                    doc = ty & "~" & fn
                    If docs = "" Then
                        docs = doc
                    Else
                        docs += ";" & doc
                    End If
                End While
                dr.Close()
                lbldocs.Value = docs
            End If
        Next
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetPg(PageNumber)
        Catch ex As Exception
            man.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr378", "PMMainMantpm.aspx.vb")

            man.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetPg(PageNumber)
        Catch ex As Exception
            man.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr379", "PMMainMantpm.aspx.vb")

            man.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub


    Private Sub btnnext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim pg As Integer = txtpg.Value
        PageNumber = pg + 1
        txtpg.Value = PageNumber
        man.Open()
        GetPg(PageNumber)
        man.Dispose()
    End Sub

    Private Sub btnprev_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim pg As Integer = txtpg.Value
        PageNumber = pg - 1
        txtpg.Value = PageNumber
        man.Open()
        GetPg(PageNumber)
        man.Dispose()
    End Sub

    Private Sub rptrtasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtasks.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Then
            Dim img As HtmlImage = CType(e.Item.FindControl("imgi"), HtmlImage)
            Dim imgi As HtmlImage = CType(e.Item.FindControl("imgi2"), HtmlImage)
            Dim link As LinkButton = CType(e.Item.FindControl("lbltn"), LinkButton)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "pmid").ToString
            Dim eid As String = DataBinder.Eval(e.Item.DataItem, "eqid").ToString
            Dim wo As String = DataBinder.Eval(e.Item.DataItem, "wonum").ToString
            Dim fcust As String = DataBinder.Eval(e.Item.DataItem, "fcust").ToString
            img.Attributes("onclick") = "print('" & id & "');"
            imgi.Attributes("onclick") = "printwo('" & id & "','" & wo & "');"
            link.Attributes("onclick") = "jump('" & id & "', '" & eid & "','" & fcust & "');"
            Dim simg As HtmlImage = CType(e.Item.FindControl("iwi"), HtmlImage)
            Dim newdate As String = DataBinder.Eval(e.Item.DataItem, "nextdate").ToString
            Try
                newdate = CType(newdate, DateTime)
                If newdate < Now Then
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/rwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov88", "PMMainMantpm.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                Else
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/gwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov89", "PMMainMantpm.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                End If
            Catch ex As Exception

            End Try

        ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim img As HtmlImage = CType(e.Item.FindControl("imga"), HtmlImage)
            Dim imga As HtmlImage = CType(e.Item.FindControl("imga2"), HtmlImage)
            Dim link As LinkButton = CType(e.Item.FindControl("lbltnalt"), LinkButton)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "pmid").ToString
            Dim eid As String = DataBinder.Eval(e.Item.DataItem, "eqid").ToString
            Dim wo As String = DataBinder.Eval(e.Item.DataItem, "wonum").ToString
            Dim fcust As String = DataBinder.Eval(e.Item.DataItem, "fcust").ToString
            img.Attributes("onclick") = "print('" & id & "');"
            imga.Attributes("onclick") = "printwo('" & id & "','" & wo & "');"
            link.Attributes("onclick") = "jump('" & id & "', '" & eid & "','" & fcust & "');"
            Dim simg As HtmlImage = CType(e.Item.FindControl("iwa"), HtmlImage)
            Dim newdate As String = DataBinder.Eval(e.Item.DataItem, "nextdate").ToString
            Try
                newdate = CType(newdate, DateTime)
                If newdate < Now Then
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/rwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov90", "PMMainMantpm.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                Else
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/gwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov91", "PMMainMantpm.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                End If
            Catch ex As Exception

            End Try

        End If

        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang859 As Label
                lang859 = CType(e.Item.FindControl("lang859"), Label)
                lang859.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang859")
            Catch ex As Exception
            End Try
            Try
                Dim lang860 As Label
                lang860 = CType(e.Item.FindControl("lang860"), Label)
                lang860.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang860")
            Catch ex As Exception
            End Try
            Try
                Dim lang861 As Label
                lang861 = CType(e.Item.FindControl("lang861"), Label)
                lang861.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang861")
            Catch ex As Exception
            End Try
            Try
                Dim lang862 As Label
                lang862 = CType(e.Item.FindControl("lang862"), Label)
                lang862.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang862")
            Catch ex As Exception
            End Try
            Try
                Dim lang863 As Label
                lang863 = CType(e.Item.FindControl("lang863"), Label)
                lang863.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang863")
            Catch ex As Exception
            End Try
            Try
                Dim lang864 As Label
                lang864 = CType(e.Item.FindControl("lang864"), Label)
                lang864.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang864")
            Catch ex As Exception
            End Try
            Try
                Dim lang865 As Label
                lang865 = CType(e.Item.FindControl("lang865"), Label)
                lang865.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang865")
            Catch ex As Exception
            End Try
            Try
                Dim lang866 As Label
                lang866 = CType(e.Item.FindControl("lang866"), Label)
                lang866.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang866")
            Catch ex As Exception
            End Try
            Try
                Dim lang867 As Label
                lang867 = CType(e.Item.FindControl("lang867"), Label)
                lang867.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang867")
            Catch ex As Exception
            End Try
            Try
                Dim lang868 As Label
                lang868 = CType(e.Item.FindControl("lang868"), Label)
                lang868.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang868")
            Catch ex As Exception
            End Try
            Try
                Dim lang869 As Label
                lang869 = CType(e.Item.FindControl("lang869"), Label)
                lang869.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang869")
            Catch ex As Exception
            End Try
            Try
                Dim lang870 As Label
                lang870 = CType(e.Item.FindControl("lang870"), Label)
                lang870.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang870")
            Catch ex As Exception
            End Try
            Try
                Dim lang871 As Label
                lang871 = CType(e.Item.FindControl("lang871"), Label)
                lang871.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang871")
            Catch ex As Exception
            End Try
            Try
                Dim lang872 As Label
                lang872 = CType(e.Item.FindControl("lang872"), Label)
                lang872.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang872")
            Catch ex As Exception
            End Try
            Try
                Dim lang873 As Label
                lang873 = CType(e.Item.FindControl("lang873"), Label)
                lang873.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang873")
            Catch ex As Exception
            End Try
            Try
                Dim lang874 As Label
                lang874 = CType(e.Item.FindControl("lang874"), Label)
                lang874.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang874")
            Catch ex As Exception
            End Try
            Try
                Dim lang875 As Label
                lang875 = CType(e.Item.FindControl("lang875"), Label)
                lang875.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang875")
            Catch ex As Exception
            End Try
            Try
                Dim lang876 As Label
                lang876 = CType(e.Item.FindControl("lang876"), Label)
                lang876.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang876")
            Catch ex As Exception
            End Try
            Try
                Dim lang877 As Label
                lang877 = CType(e.Item.FindControl("lang877"), Label)
                lang877.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang877")
            Catch ex As Exception
            End Try
            Try
                Dim lang878 As Label
                lang878 = CType(e.Item.FindControl("lang878"), Label)
                lang878.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang878")
            Catch ex As Exception
            End Try
            Try
                Dim lang879 As Label
                lang879 = CType(e.Item.FindControl("lang879"), Label)
                lang879.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang879")
            Catch ex As Exception
            End Try
            Try
                Dim lang880 As Label
                lang880 = CType(e.Item.FindControl("lang880"), Label)
                lang880.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang880")
            Catch ex As Exception
            End Try

        End If

    End Sub

    Private Sub ibfilt_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        man.Open()
        GetPg(PageNumber)
        man.Dispose()
    End Sub
    Public Function CellCheck(ByVal filt As String) As String
        Dim cells As String
        Dim cellcnt As Integer
        sql = "select count(*) from Cells where Dept_ID = '" & filt & "'"
        cellcnt = man.Scalar(sql)
        Dim nocellcnt As Integer
        If cellcnt = 0 Then
            cells = "no"
        ElseIf cellcnt = 1 Then
            sql = "select count(*) from Cells where Dept_ID = '" & filt & "' and cell_name = 'No Cells'"
            nocellcnt = man.Scalar(sql)
            If nocellcnt = 1 Then
                cells = "no"
            Else
                cells = "yes"
            End If
        Else
            cells = "yes"
        End If
        Return cells
    End Function
    Private Sub dddepts_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub PopEq(ByVal var As String, ByVal typ As String)
        Dim filt, dt, val, did, lid, clid As String
        lid = lbllid.Value
        did = lbldept.Value
        clid = lblclid.Value
        Dim eqcnt As Integer
        If typ = "d" Then
            If lid = "" Then
                sql = "select count(*) from equipment where dept_id = '" & did & "'"
                filt = " where dept_id = '" & did & "'"
            Else
                sql = "select count(*) from equipment where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            End If

        ElseIf typ = "c" Then
            If lid = "" Then
                sql = "select count(*) from equipment where cellid = '" & clid & "'"
                filt = " where cellid = '" & clid & "'"
            Else
                sql = "select count(*) from equipment where cellid = '" & clid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & clid & "' and locid = '" & clid & "'"
            End If

        ElseIf typ = "l" Then
            If clid <> "" Then
                sql = "select count(*) from equipment where cellid = '" & clid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & clid & "' and locid = '" & lid & "'"
            ElseIf did <> "" Then
                sql = "select count(*) from equipment where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            Else
                sql = "select count(*) from equipment where locid = '" & lid & "'"
                filt = " where locid = '" & lid & "'"
            End If

        End If
        eqcnt = man.Scalar(sql)
        If eqcnt > 0 Then
            dt = "equipment"
            val = "eqid, eqnum "

            dr = man.GetList(dt, val, filt)
            ddeq.DataSource = dr
            ddeq.DataTextField = "eqnum"
            ddeq.DataValueField = "eqid"
            ddeq.DataBind()
            dr.Close()
            ddeq.Items.Insert(0, "Select Equipment")
            ddeq.Enabled = True
        Else
            'Dim strMessage As String =  tmod.getmsg("cdstr380" , "PMMainMantpm.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ddeq.Items.Insert(0, "Select Equipment")
            ddeq.Enabled = False
        End If
        GetLoc()
    End Sub
    Private Sub GetLoc(Optional ByVal lid As String = "0")
        If lid <> 0 Then
            lid = lbllid.Value
        End If

        If lid <> "" And lid <> "0" Then
            sql = "select location, description from pmlocations where locid = '" & lid & "'"
            dr = man.GetRdrData(sql)
            While dr.Read
                lblloc.Value = dr.Item("location").ToString
            End While
            dr.Close()
        End If

    End Sub
    Private Sub PopCells(ByVal dept As String)
        Dim filt, dt, val As String
        filt = " where dept_id = " & dept
        dt = "Cells"
        val = "cellid , cell_name "
        dr = man.GetList(dt, val, filt)
        ddcells.DataSource = dr
        ddcells.DataTextField = "cell_name"
        ddcells.DataValueField = "cellid"
        ddcells.DataBind()
        dr.Close()
        ddcells.Items.Insert(0, "Select Station/Cell")
        ddcells.Enabled = True
    End Sub

    Private Sub ddcells_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcells.SelectedIndexChanged
        Dim cell As String = ddcells.SelectedValue.ToString
        If ddcells.SelectedIndex <> 0 Then
            Try
                man.Open()
                lblclid.Value = cell
                PopEq(cell, "c")
                'PopNC(cell, "c")
                ddeq.Enabled = True
                man.Dispose()
            Catch ex As Exception
                man.Dispose()
                'Dim strMessage As String =  tmod.getmsg("cdstr381" , "PMMainMantpm.aspx.vb")

                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try
            lblclid.Value = ""
        End If
    End Sub

    Private Sub ddeq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddeq.SelectedIndexChanged
        Dim eq As String = ddeq.SelectedValue.ToString
        Dim did, clid, lid, Filter As String
        lbleqid.Value = eq
        Dim loctype As String
        If ddeq.SelectedIndex <> 0 Then
            man.Open()
            sql = "select * from equipment where eqid = '" & eq & "'"
            dr = man.GetRdrData(sql)
            While dr.Read
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                lid = dr.Item("locid").ToString
                loctype = dr.Item("loctype").ToString
            End While
            dr.Close()
            lbldept.Value = did
            lblclid.Value = clid
            lbllid.Value = lid
            If loctype = "reg" Or loctype = "dloc" Then
                If dddepts.SelectedIndex = 0 Then
                    dddepts.SelectedValue = did
                    Dim chk As String = CellCheck(Filter)
                    If chk = "no" Then
                        lblchk.Value = "no"
                    Else
                        lblchk.Value = "yes"
                        PopCells(did)
                        If clid <> "" Then
                            ddcells.SelectedValue = clid
                        End If
                    End If
                End If
            End If
            If loctype = "loc" Or loctype = "dloc" Then
                GetLoc(lid)
            End If
            PopFunc(eq)
            man.Dispose()
        Else
            lbleqid.Value = ""
        End If
    End Sub
    Private Sub PopComp()
        Dim fuid As String = lblfuid.Value
        sql = "select comid, compnum + ' - ' + isnull(compdesc, '') as compnum " _
          + "from components where func_id = '" & fuid & "' order by compnum desc"
        dr = man.GetRdrData(sql)
        ddcomp.DataSource = dr
        ddcomp.DataTextField = "compnum"
        ddcomp.DataValueField = "comid"
        ddcomp.DataBind()
        dr.Close()
        ddcomp.Items.Insert(0, New ListItem("Select"))
        ddcomp.Items(0).Value = 0
    End Sub
    Private Sub PopFunc(ByVal eq As String)
        Dim dt, val, filt As String
        Dim fucnt As Integer
        sql = "select count(*) from Functions where eqid = '" & eq & "'"
        fucnt = man.Scalar(sql)
        If fucnt = 0 Then
            ddfunc.Items.Insert(0, "Select Function")
            ddfunc.Enabled = "False"
            Dim strMessage As String = tmod.getmsg("cdstr382", "PMMainMantpm.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            dt = "Functions"
            val = "func_id, func"
            filt = " where eqid = '" & eq & "'"
            dr = man.GetList(dt, val, filt)
            ddfunc.DataSource = dr
            ddfunc.DataTextField = "func"
            ddfunc.DataValueField = "func_id"
            ddfunc.DataBind()
            dr.Close()
            ddfunc.Items.Insert(0, "Select Function")
            ddfunc.Enabled = "True"
        End If
    End Sub

    Private Sub ddfunc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddfunc.SelectedIndexChanged
        Dim fuid As String = ddfunc.SelectedValue.ToString
        lblfuid.Value = fuid
        If ddfunc.SelectedIndex <> 0 Then
            man.Open()
            PopComp()
            man.Dispose()
        Else
            lblfuid.Value = ""
        End If
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        lblsrch.Value = "yes"
        PageNumber = 1
        man.Open()
        GetPg(PageNumber)
        man.Dispose()
    End Sub

    Private Sub dddepts_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles dddepts.SelectedIndexChanged
        Dim dept As String = dddepts.SelectedValue.ToString
        lbldept.Value = dept
        Dim Filter, lid As String
        man.Open()
        If dddepts.SelectedIndex <> 0 Then

            Dim deptname As String = dddepts.SelectedItem.ToString
            Filter = dept
            Dim chk As String = CellCheck(Filter)
            If chk = "no" Then
                lblchk.Value = "no"
                lid = lbllid.Value
                If lid <> "" And lid <> "0" Then
                    PopEq(lid, "l")
                    'PopNC(lid, "l")
                Else
                    PopEq(dept, "d")
                    'PopNC(dept, "d")
                End If

            Else
                lblchk.Value = "yes"
                ddeq.Enabled = False
                PopCells(dept)

            End If
        Else
            lbldept.Value = ""
        End If
        man.Dispose()
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang859.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang859")
        Catch ex As Exception
        End Try
        Try
            lang860.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang860")
        Catch ex As Exception
        End Try
        Try
            lang861.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang861")
        Catch ex As Exception
        End Try
        Try
            lang862.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang862")
        Catch ex As Exception
        End Try
        Try
            lang863.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang863")
        Catch ex As Exception
        End Try
        Try
            lang864.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang864")
        Catch ex As Exception
        End Try
        Try
            lang865.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang865")
        Catch ex As Exception
        End Try
        Try
            lang866.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang866")
        Catch ex As Exception
        End Try
        Try
            lang867.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang867")
        Catch ex As Exception
        End Try
        Try
            lang868.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang868")
        Catch ex As Exception
        End Try
        Try
            lang869.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang869")
        Catch ex As Exception
        End Try
        Try
            lang870.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang870")
        Catch ex As Exception
        End Try
        Try
            lang871.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang871")
        Catch ex As Exception
        End Try
        Try
            lang872.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang872")
        Catch ex As Exception
        End Try
        Try
            lang873.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang873")
        Catch ex As Exception
        End Try
        Try
            lang874.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang874")
        Catch ex As Exception
        End Try
        Try
            lang875.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang875")
        Catch ex As Exception
        End Try
        Try
            lang876.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang876")
        Catch ex As Exception
        End Try
        Try
            lang877.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang877")
        Catch ex As Exception
        End Try
        Try
            lang878.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang878")
        Catch ex As Exception
        End Try
        Try
            lang879.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang879")
        Catch ex As Exception
        End Try
        Try
            lang880.Text = axlabs.GetASPXPage("PMMainMantpm.aspx", "lang880")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imga.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMantpm.aspx", "imga") & "', ABOVE, LEFT)")
            imga.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imga2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMantpm.aspx", "imga2") & "', ABOVE, LEFT)")
            imga2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgi.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMantpm.aspx", "imgi") & "', ABOVE, LEFT)")
            imgi.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgi2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMantpm.aspx", "imgi2") & "', ABOVE, LEFT)")
            imgi2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgregpm.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMantpm.aspx", "imgregpm") & "', ABOVE, LEFT)")
            imgregpm.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgrteprint.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMantpm.aspx", "imgrteprint") & "', ABOVE, LEFT)")
            imgrteprint.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgrtewprint.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMantpm.aspx", "imgrtewprint") & "', ABOVE, LEFT)")
            imgrtewprint.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgwopm.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMantpm.aspx", "imgwopm") & "', ABOVE, LEFT)")
            imgwopm.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            iwa.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMantpm.aspx", "iwa") & "', ABOVE, LEFT)")
            iwa.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            iwi.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMantpm.aspx", "iwi") & "', ABOVE, LEFT)")
            iwi.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid105.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMantpm.aspx", "ovid105") & "')")
            ovid105.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid106.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMantpm.aspx", "ovid106") & "')")
            ovid106.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid107.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMantpm.aspx", "ovid107") & "')")
            ovid107.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid108.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMantpm.aspx", "ovid108") & "')")
            ovid108.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid109.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMantpm.aspx", "ovid109") & "')")
            ovid109.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid110.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMantpm.aspx", "ovid110") & "')")
            ovid110.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
