<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PFAdjtpm.aspx.vb" Inherits="lucy_r12.PFAdjtpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PFAdj</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/PFAdjtpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table cellspacing="0" width="630" border="0">
        <tr>
            <td width="170">
            </td>
            <td width="70">
            </td>
            <td width="70">
            </td>
            <td width="160">
            </td>
            <td width="160">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td class="bluelabel" align="center">
                <asp:Label ID="lang759" runat="server">Current</asp:Label>
            </td>
            <td class="bluelabel" align="center">
                <asp:Label ID="lang760" runat="server">New</asp:Label>
            </td>
            <td class="bluelabel" align="center" colspan="2">
                <asp:Label ID="lang761" runat="server">Options</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label blackleft blacktop">
                <asp:Label ID="lang762" runat="server">Task Pass Threshold:</asp:Label>
            </td>
            <td class="plainlabel blackleft blacktop" id="tdpass" align="center" runat="server">
            </td>
            <td class="plainlabel blackleft blacktop" id="tdt" align="center" runat="server">
                <asp:TextBox ID="txtpass" runat="server" Width="60"></asp:TextBox>
            </td>
            <td class="blackleft blacktop">
                <asp:RadioButtonList ID="rbp" runat="server" CssClass="plainlabel" Height="16px"
                    RepeatDirection="Horizontal" Width="159px">
                    <asp:ListItem Value="1" Selected="True">Default Only</asp:ListItem>
                    <asp:ListItem Value="2">Update All</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td class="blackright blacktop">
                <asp:RadioButtonList ID="rbpmp" runat="server" CssClass="redlabellt" Width="170px">
                    <asp:ListItem Value="1">Apply to Active TPM Records</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="label blackleft">
                <asp:Label ID="lang763" runat="server">Task Fail Threshold:</asp:Label>
            </td>
            <td class="plainlabel blackleft" id="tdfail" align="center" runat="server">
            </td>
            <td class="plainlabel blackleft" id="tdf" align="center" runat="server">
                <asp:TextBox ID="txtfail" runat="server" Width="60"></asp:TextBox>
            </td>
            <td class="blackleft">
                <asp:RadioButtonList ID="rbf" runat="server" CssClass="plainlabel" Height="16px"
                    RepeatDirection="Horizontal" Width="160px">
                    <asp:ListItem Value="1" Selected="True">Default Only</asp:ListItem>
                    <asp:ListItem Value="2">Update All</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td class="blackright">
                <asp:RadioButtonList ID="rbpmf" runat="server" CssClass="redlabellt" Width="170px">
                    <asp:ListItem Value="1">Apply to Active TPM Records</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr height="26">
            <td class="bluelabellt" colspan="5">
                <asp:Label ID="lang764" runat="server">* The Default Option applies the New value to new records only</asp:Label>
            </td>
        </tr>
        <tr height="26">
            <td class="bluelabellt" colspan="5">
                <asp:Label ID="lang765" runat="server">* The Update All Option applies the New value to new records and to all Task Records where these values have not been modified</asp:Label>
            </td>
        </tr>
        <tr height="26">
            <td class="bluelabellt" colspan="5">
                <asp:Label ID="lang766" runat="server">* The Apply to Active TPM Records Option applies the New value to all Task Records in all Active TPM Records where these values have not been modified</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                &nbsp;
            </td>
        </tr>
        <tr class="details">
            <td colspan="5">
                <table>
                    <tr>
                        <td class="bluelabel">
                            <asp:Label ID="lang767" runat="server">Threshold Report Frequency (Days)</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtdays" runat="server" Width="60"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="geteadmin();" src="../images/appbuttons/minibuttons/emailcb.gif">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="26">
            <td valign="bottom" align="right" colspan="5">
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif">
                </asp:ImageButton>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblpass" runat="server">
    <input type="hidden" id="lblfail" runat="server">
    <input type="hidden" id="lblsid" runat="server"><input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
