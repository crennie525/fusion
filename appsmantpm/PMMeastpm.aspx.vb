

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class PMMeastpm
    Inherits System.Web.UI.Page
	Protected WithEvents ovid112 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid111 As System.Web.UI.HtmlControls.HtmlImage

	

	

	Protected WithEvents imga As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents img As System.Web.UI.HtmlControls.HtmlImage

	

	

	Protected WithEvents lang889 As System.Web.UI.WebControls.Label

	Protected WithEvents lang888 As System.Web.UI.WebControls.Label

	Protected WithEvents lang887 As System.Web.UI.WebControls.Label

	Protected WithEvents lang886 As System.Web.UI.WebControls.Label

	Protected WithEvents lang885 As System.Web.UI.WebControls.Label

	Protected WithEvents lang884 As System.Web.UI.WebControls.Label

	Protected WithEvents lang883 As System.Web.UI.WebControls.Label

	Protected WithEvents lang882 As System.Web.UI.WebControls.Label

	Protected WithEvents lang881 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim meas As New Utilities
    Protected WithEvents dgmeas As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lbltid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtsup As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlead As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddwt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtstart As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnwo As System.Web.UI.WebControls.ImageButton
    Protected WithEvents cbsupe As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbleade As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim pmtskid, pmid, cid, pmtid, ro As String
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtcorr As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtprob As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldragid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrowid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltmid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()



	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            cid = "0"
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            pmtskid = Request.QueryString("tid").ToString
            pmid = Request.QueryString("pmid").ToString
            pmtid = Request.QueryString("pmtid").ToString
            lbltid.Value = pmtskid
            lblpmid.Value = pmid
            lblpmtid.Value = pmtid
            meas.Open()
            GetLists()
            GetMeas(pmtskid)
            meas.Dispose()

        End If
    End Sub
    Private Sub GetLists()
        cid = lblcid.Value
        sql = "select skillid, skill " _
        + "from pmSkills where compid = '" & cid & "'"
        dr = meas.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("Select"))
        ddskill.Items(0).Value = 0
    End Sub
    Private Sub GetMeas(ByVal pmtskid As String)
        Dim mcnt As Integer
        sql = "select count(*) from pmTaskMeasDetMantpm where pmtskid = '" & pmtskid & "'"
        mcnt = meas.Scalar(sql)
        lblmcnt.Value = mcnt
        sql = "select *, cnt = (select count(*) from pmTaskMeasDetManHisttpm where pmtskid = '" & pmtskid & "') " _
        + "from pmTaskMeasDetMantpm where pmtskid = '" & pmtskid & "'"
        dr = meas.GetRdrData(sql)
        dgmeas.DataSource = dr
        dgmeas.DataBind()


    End Sub

    Private Sub dgmeas_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.EditCommand
        dgmeas.EditItemIndex = e.Item.ItemIndex
        pmtskid = lbltid.Value
        meas.Open()
        GetMeas(pmtskid)
        meas.Dispose()

    End Sub

    Private Sub dgmeas_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.UpdateCommand
        Dim mup As Integer
        Dim hi, lo As String
        Dim mstr As String = CType(e.Item.FindControl("txtmeas"), TextBox).Text
        Dim tmdid As String = CType(e.Item.FindControl("lbltmdida"), Label).Text
        Dim txtchk As Long
        Try
            txtchk = System.Convert.ToDecimal(mstr)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr383" , "PMMeastpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If mstr <> "" Then
            pmtskid = lbltid.Value
            'sql = "update pmTaskMeasDetMan set measurement = '" & mstr & "' where pmtskid = '" & pmtskid & "'"
            sql = "usp_upmeastpm '" & tmdid & "', '" & pmtskid & "', '" & mstr & "'"
            meas.Open()
            'meas.Update(sql)
            'sql = "select count(*) from pmTaskMeasDetManHist where pmtskid = '" & pmtskid & "' and measurement is not null"
            'mup = meas.Scalar(sql)
            dr = meas.GetRdrData(sql)
            While dr.Read
                mup = dr.Item("mup").ToString
                hi = dr.Item("hi").ToString
                mup = dr.Item("lo").ToString
            End While
            dr.Close()
            lblmup.Value = mup
            dgmeas.EditItemIndex = -1
            GetMeas(pmtskid)
            meas.Dispose()
            'typ, pmid, pmtskid, pmtid, tmdid
            Dim mail As New pmmail
            If hi = "1" Or lo = "1" Then
                'typ, pmid, pmtskid, pmtid, tmdid
                pmid = lblpmid.Value
                pmtid = lblpmtid.Value
                mail.CheckIt("meas", pmid, pmtskid, pmtid, tmdid)
            End If
        End If
    End Sub

    Private Sub dgmeas_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.CancelCommand
        dgmeas.EditItemIndex = -1
        pmtskid = lbltid.Value
        meas.Open()
        GetMeas(pmtskid)
        meas.Dispose()
    End Sub

    Private Sub dgmeas_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgmeas.ItemDataBound
        Dim cnt As String
        Dim icnt As Integer
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            cnt = DataBinder.Eval(e.Item.DataItem, "cnt").ToString
            icnt = Convert.ToInt32(cnt)
            Dim img As HtmlImage = CType(e.Item.FindControl("ihg"), HtmlImage)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "pmtskid").ToString
            Dim img1 As HtmlImage = CType(e.Item.FindControl("img"), HtmlImage)
            Dim id1 As String = DataBinder.Eval(e.Item.DataItem, "tmdidpar").ToString
            If icnt > 2 Then
                img.Attributes("onclick") = "getchart('" & id & "','" & id1 & "');"
            Else
                img.Attributes("class") = "details"
            End If
            If icnt > 0 Then
                img1.Attributes("onclick") = "print('" & id & "','" & id1 & "');"
            Else
                img1.Attributes("class") = "details"
            End If
            Dim hi As String = DataBinder.Eval(e.Item.DataItem, "mover").ToString
            Dim lo As String = DataBinder.Eval(e.Item.DataItem, "munder").ToString
            Dim img2 As HtmlImage = CType(e.Item.FindControl("iwo"), HtmlImage)
            If (hi <> "0" And hi <> "" And hi <> "0.00") Or (lo <> "0" And lo <> "" And lo <> "0.00") Then
                img2.Attributes("onclick") = "getrt('" & id & "','" & id1 & "');"
            Else
                img2.Attributes("class") = "details"
            End If
        ElseIf e.Item.ItemType = ListItemType.EditItem Then
            cnt = DataBinder.Eval(e.Item.DataItem, "cnt").ToString
            icnt = Convert.ToInt32(cnt)
            Dim img As HtmlImage = CType(e.Item.FindControl("ihga"), HtmlImage)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "pmtskid").ToString
            Dim img1 As HtmlImage = CType(e.Item.FindControl("imga"), HtmlImage)
            Dim id1 As String = DataBinder.Eval(e.Item.DataItem, "tmdid").ToString
            If icnt > 2 Then
                img.Attributes("onclick") = "getchart('" & id & "','" & id1 & "');"
            Else
                img.Attributes("class") = "details"
            End If
            If icnt > 0 Then
                img1.Attributes("onclick") = "print('" & id & "','" & id1 & "');"
            Else
                img1.Attributes("class") = "details"
            End If
            Dim hi As String = DataBinder.Eval(e.Item.DataItem, "mover").ToString
            Dim lo As String = DataBinder.Eval(e.Item.DataItem, "munder").ToString
            Dim img2 As HtmlImage = CType(e.Item.FindControl("iwoa"), HtmlImage)
            If (hi <> "0" And hi <> "" And hi <> "0.00") Or (lo <> "0" And lo <> "" And lo <> "0.00") Then
                img2.Attributes("onclick") = "getrt('" & id & "','" & id1 & "');"
            Else
                img2.Attributes("class") = "details"
            End If
        End If
    End Sub

    Private Sub btnwo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnwo.Click
        meas.Open()
        GenCorr()
        meas.Dispose()
    End Sub
    Private Sub GenCorr()
        'SaveChanges()
        Dim tmdid = lbltmid.Value
        pmid = lblpmid.Value
        Dim eqid, eqnum, sid, deptid, cellid, locid, location, desc, funcid, comid, chrg As String
        sql = "select p.eqid, e.* from tpm p left join equipment e on e.eqid = p.eqid where pmid = '" & pmid & "'"
        dr = meas.GetRdrData(sql)
        While dr.Read
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            sid = dr.Item("siteid").ToString
            deptid = dr.Item("dept_id").ToString
            cellid = dr.Item("cellid").ToString
            locid = dr.Item("locid").ToString
            chrg = dr.Item("chargenum").ToString
        End While
        dr.Close()
        Dim wonum As Integer
        Dim superid, super, leadid, lead, se, le, start, skillid, skill As String
        superid = lblsup.Value
        super = txtsup.Text
        leadid = lbllead.Value
        lead = txtlead.Text
        If cbsupe.Checked = True Then
            se = "1"
        Else
            se = "0"
        End If
        If cbleade.Checked = True Then
            le = "1"
        Else
            le = "0"
        End If
        start = txtstart.Text
        If start = "" Then
            start = "NULL"
        Else
            start = "'" & start & "'"
        End If
        skillid = ddskill.SelectedValue.ToString
        skill = ddskill.SelectedItem.ToString
        Dim usr As String = HttpContext.Current.Session("username").ToString
        pmtid = lblpmtid.Value
        sql = "select funcid, comid from pmtracktpm where pmtid = '" & pmtid & "'"
        dr = meas.GetRdrData(sql)
        While dr.Read
            funcid = dr.Item("funcid").ToString
            comid = dr.Item("comid").ToString
        End While
        dr.Close()

        sql = "insert into workorder (status, statusdate, changeby, changedate, reportedby, reportdate, worktype, tmdid, eqid, " _
        + "eqnum, siteid, deptid, cellid, locid, funcid, comid, chargenum, targstartdate, leadcraftid, leadcraft, leadealert, " _
        + "superid, supervisor, supealert, skillid, skill) " _
      + "values ('WAPPR', getDate(),'" & usr & "', getDate(),'" & usr & "', getDate(), 'CM' ,'" & tmdid & "','" & eqid & "', " _
      + "'" & eqnum & "','" & sid & "','" & deptid & "','" & cellid & "','" & locid & "','" & funcid & "','" & comid & "','" & chrg & "', " _
      + "" & start & ",'" & leadid & "','" & lead & "','" & le & "','" & superid & "','" & super & "','" & se & "', " _
      + "'" & skillid & "','" & skill & "') " _
      + "select @@identity"
        wonum = meas.Scalar(sql)
        'lblwonum.Text = wonum
        lblwo.Value = wonum
        Dim corr As String = txtcorr.Text
        Dim prob As String = txtprob.Text
        sql = "update pmtaskmeasdetmantpm set wonum = '" & wonum & "', corraction = '" & corr & "', problem = '" & prob & "' where tmdid = '" & tmdid & "'"
        meas.Update(sql)
        sql = "insert into wocorr (wctype, wonum, problem, corraction) values ('meas','" & wonum & "','" & prob & "','" & corr & "')"
        meas.Update(sql)
        'Insert Failure Mode to wofail - use sp
        'fmid = lblfmid.Value
        'fm = lblfm.Value
        'sql = "usp_addWoFailureMode " & wonum & ", " & fmid & ", '" & fm & "', '" & comid & "','hascomp'"
        'pmf.Update(sql)
        desc = "Corrective Action for Equipment# " & eqnum
        SaveDesc(desc)
        Dim mail As New pmmail
        If cbsupe.Checked = True Then
            'SendIt("sup", superid, wonum, desc, start)
            Dim mail1 As New pmmail
            mail1.CheckIt("sup", superid, wonum)
        End If
        If cbleade.Checked = True Then
            'SendIt("lead", leadid, wonum, desc, start)
            Dim mail2 As New pmmail
            mail2.CheckIt("lead", leadid, wonum)
        End If
    End Sub
    Private Sub SaveDesc(ByVal lg As String)
        Dim wonum As String = lblwo.Value
        Dim sh As String
        Dim lgcnt As Integer
        Dim test As String = lg
        If Len(lg) > 79 Then
            sh = Mid(lg, 1, 79)
            sql = "update workorder set description = '" & sh & "' where wonum = '" & wonum & "'"
            meas.Update(sql)
            lg = Mid(lg, 80)
            sql = "select count(*) from wolongdesc where wonum = '" & wonum & "'"
            lgcnt = meas.Scalar(sql)
            If lgcnt <> 0 Then
                sql = "update wolongdesc set longdesc = '" & lg & "' where wonum = '" & wonum & "'"
                meas.Update(sql)
            Else
                sql = "insert into wolongdesc (wonum, longdesc) values ('" & wonum & "','" & lg & "')"
                meas.Update(sql)
            End If
        Else
            sql = "update workorder set description = '" & lg & "' where wonum = '" & wonum & "'" _
            + "delete from wolongdesc where wonum = '" & wonum & "'"
            meas.Update(sql)
        End If

    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgmeas.Columns(0).HeaderText = dlabs.GetDGPage("PMMeastpm.aspx", "dgmeas", "0")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(1).HeaderText = dlabs.GetDGPage("PMMeastpm.aspx", "dgmeas", "1")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(2).HeaderText = dlabs.GetDGPage("PMMeastpm.aspx", "dgmeas", "2")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(3).HeaderText = dlabs.GetDGPage("PMMeastpm.aspx", "dgmeas", "3")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(4).HeaderText = dlabs.GetDGPage("PMMeastpm.aspx", "dgmeas", "4")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(5).HeaderText = dlabs.GetDGPage("PMMeastpm.aspx", "dgmeas", "5")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(6).HeaderText = dlabs.GetDGPage("PMMeastpm.aspx", "dgmeas", "6")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang881.Text = axlabs.GetASPXPage("PMMeastpm.aspx", "lang881")
        Catch ex As Exception
        End Try
        Try
            lang882.Text = axlabs.GetASPXPage("PMMeastpm.aspx", "lang882")
        Catch ex As Exception
        End Try
        Try
            lang883.Text = axlabs.GetASPXPage("PMMeastpm.aspx", "lang883")
        Catch ex As Exception
        End Try
        Try
            lang884.Text = axlabs.GetASPXPage("PMMeastpm.aspx", "lang884")
        Catch ex As Exception
        End Try
        Try
            lang885.Text = axlabs.GetASPXPage("PMMeastpm.aspx", "lang885")
        Catch ex As Exception
        End Try
        Try
            lang886.Text = axlabs.GetASPXPage("PMMeastpm.aspx", "lang886")
        Catch ex As Exception
        End Try
        Try
            lang887.Text = axlabs.GetASPXPage("PMMeastpm.aspx", "lang887")
        Catch ex As Exception
        End Try
        Try
            lang888.Text = axlabs.GetASPXPage("PMMeastpm.aspx", "lang888")
        Catch ex As Exception
        End Try
        Try
            lang889.Text = axlabs.GetASPXPage("PMMeastpm.aspx", "lang889")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnwo.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                btnwo.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                btnwo.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                btnwo.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "spa" Then
                btnwo.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib

        Try
            img.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMeastpm.aspx", "img") & "', ABOVE, LEFT)")
            img.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imga.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMeastpm.aspx", "imga") & "', ABOVE, LEFT)")
            imga.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

        Try
            ovid111.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMeastpm.aspx", "ovid111") & "')")
            ovid111.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid112.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMeastpm.aspx", "ovid112") & "')")
            ovid112.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
