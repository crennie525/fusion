<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMUpDatetpm.aspx.vb" Inherits="lucy_r12.PMUpDatetpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMUpDate</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMUpDatetpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        <!--
        function exitwo(id) {
            window.parent.handleexit(id);
        }
        function checkret() {
            var ret = document.getElementById("lblret").value;
            if (ret == "yes") {
                exitwo("yes");
            }
        }
        function checkup() {
            document.getElementById("tdmsg").innerHTML = "Performing Update... "
            document.getElementById("btnyes").disabled = true;
            document.getElementById("btnexit").disabled = true
            document.getElementById("tdbuttons").style.visibility = 'hidden';


        }
        -->
        </script>
</head>
<body onload="checkret();">
    <form id="form1" method="post" runat="server">
    <table style="left: 10px; position: absolute; top: 10px">
        <tr>
            <td class="bluelabel" id="tdmsg" align="center" runat="server">
            </td>
        </tr>
        <tr>
            <td align="center">
                <input id="btnyes" runat="server" class="button" type="button" value="Yes"><input
                    onclick="exitwo('no');" class="button" type="button" value="No">
            </td>
        </tr>
    </table>
    <input type="hidden" id="lbleqid" runat="server">
    <input type="hidden" id="lblret" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
