<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMArchMantpm.aspx.vb"
    Inherits="lucy_r12.PMArchMantpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMArchMan</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMArchMantpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
     <!--
        function gotoeq(eid) {
            window.parent.geteq(eid);
        }
        function gotofu(fid, eid) {
            window.parent.getfu(eid, fid);
        }
        function gotoco(eid, fid, cid) {
            window.parent.getco(eid, fid, cid);
        }
        function chksrch() {
            document.getElementById("lblret").value = "srch";
            document.getElementById("form1").submit();
        }
         //-->
    </script>
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 0px; background-color: transparent;" cellspacing="0"
        cellpadding="0" width="230">
        <tr>
            <td align="center">
                <table cellpadding="1" cellspacing="0">
                    <tr>
                        <td class="label">
                            <asp:Label ID="lang796" runat="server">Search</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtsrch" runat="server" CssClass="plainlabel" Width="150px"></asp:TextBox>
                        </td>
                        <td>
                            <img src="../images/appbuttons/minibuttons/srchsm1.gif" onclick="chksrch();">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="border-right: blue 1px solid; border-top: blue 1px solid; border-left: blue 1px solid;
                    border-bottom: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst2.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev2.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="140">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext2.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast2.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td id="tdarch" runat="server">
            </td>
        </tr>
    </table>
    <input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt"
        type="hidden" name="txtpgcnt" runat="server">
    <input type="hidden" id="lblret" runat="server" name="lblret">
    <input type="hidden" id="lblsiteid" runat="server" name="lblsiteid">
    <input type="hidden" id="lblcomp" runat="server" name="lblcomp">
    <input type="hidden" id="lbluserid" runat="server" />
    <input type="hidden" id="lblislabor" runat="server" />
    <input type="hidden" id="lblisplanner" runat="server" />
    <input type="hidden" id="lblcadm" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
