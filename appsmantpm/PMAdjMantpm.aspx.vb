

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMAdjMantpm
    Inherits System.Web.UI.Page
	Protected WithEvents ovid93 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents btnpfint As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang793 As System.Web.UI.WebControls.Label

	Protected WithEvents lang792 As System.Web.UI.WebControls.Label

	Protected WithEvents lang791 As System.Web.UI.WebControls.Label

	Protected WithEvents lang790 As System.Web.UI.WebControls.Label

	Protected WithEvents lang789 As System.Web.UI.WebControls.Label

	Protected WithEvents lang788 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, pmtid As String
    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents txtfreq As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpfint As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdcurr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpfint As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim freq As New Utilities

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            pmtid = Request.QueryString("pmtid").ToString
            lblpmtid.Value = pmtid
            freq.Open()
            GetData(pmtid)
            GetFreq()
            freq.Dispose()

        End If
    End Sub
    Private Sub GetData(ByVal pmtid As String)
        sql = "usp_pmadjdatatpm '" & pmtid & "'"
        Dim eqid, pmtskid, pfreq, tfreq, pfint
        dr = freq.GetRdrData(sql)
        While dr.Read
            eqid = dr.Item("eqid").ToString
            pmtskid = dr.Item("pmtskid").ToString
            pfreq = dr.Item("pfreq").ToString
            tfreq = dr.Item("tfreq").ToString
            pfint = dr.Item("pfint").ToString
        End While
        dr.Close()
        lbleqid.Value = eqid
        lblpmtskid.Value = pmtskid
        lblpfreq.Value = pfreq
        lbltfreq.Value = tfreq
        lblpfint.Value = pfint
        txtpfint.Text = pfint
        tdcurr.InnerHtml = pfreq
    End Sub
    Private Sub GetFreq()
       

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang788.Text = axlabs.GetASPXPage("PMAdjMantpm.aspx", "lang788")
        Catch ex As Exception
        End Try
        Try
            lang789.Text = axlabs.GetASPXPage("PMAdjMantpm.aspx", "lang789")
        Catch ex As Exception
        End Try
        Try
            lang790.Text = axlabs.GetASPXPage("PMAdjMantpm.aspx", "lang790")
        Catch ex As Exception
        End Try
        Try
            lang791.Text = axlabs.GetASPXPage("PMAdjMantpm.aspx", "lang791")
        Catch ex As Exception
        End Try
        Try
            lang792.Text = axlabs.GetASPXPage("PMAdjMantpm.aspx", "lang792")
        Catch ex As Exception
        End Try
        Try
            lang793.Text = axlabs.GetASPXPage("PMAdjMantpm.aspx", "lang793")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnpfint.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMAdjMantpm.aspx", "btnpfint") & "')")
            btnpfint.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid93.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMAdjMantpm.aspx", "ovid93") & "', ABOVE, LEFT)")
            ovid93.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
