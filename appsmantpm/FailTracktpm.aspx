<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FailTracktpm.aspx.vb"
    Inherits="lucy_r12.FailTracktpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>FailTrack</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/FailTracktpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
    <!--
        function getchart(tid, tid1) {
            //, hi, spec
            var popwin = "directories=0,height=600,width=950,location=0,menubar=0,resizable=0,status=0,toolbar=0,scrollbars=1";
            window.open("ChartSampleTPM.aspx?pmtskid=" + tid + "&mid=" + tid1, "repWin", popwin);
            /*
            var curl = document.getElementById("lblcharturl").value;
            if (hi == "") {
            hi = 6;
            }
            if (spec == "") {
            spec = 6;
            }

            if (hi > 5 && spec > 5) {
            window.open(curl + "PMMeasViewer0.aspx?pmtskid=" + tid + "&tmdidpar=" + tid1);
            }
            else {
            window.open(curl + "PMMeasViewer.aspx?pmtskid=" + tid + "&tmdidpar=" + tid1);
            }
            */
        }
    //-->
    </script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table width="816">
        <tr class="details">
            <td width="93">
                &nbsp;
            </td>
            <td class="details">
            </td>
            <td width="93">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdwi" runat="server" align="center">
            </td>
            <td>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
