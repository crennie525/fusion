<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TPMManager.aspx.vb" Inherits="lucy_r12.TPMManager" %>

<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title runat="server" id="pgtitle">TPM Manager</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/taskgridtpm.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/TPMManageraspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
     <!--
        function handleproj(val, pmid, typ) {
            setref();
            var ro = document.getElementById("lblro").value;
            var sid = document.getElementById("lblsid").value;
            var ro = "0"; //document.getElementById("lblro").value;
            //var eqid = "0";
            var won = document.getElementById("lblwo").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            window.location = "../appsman/PMScheduling2.aspx?jump=yes&typ=tpm&eqid=" + val + "&pmid=" + pmid + "&ro=" + ro + "&sid=" + sid + "&wo=" + won + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        function checkarch() {

            var jump = document.getElementById("lbljump").value;
            if (jump == "eq") {
                gettab(jump);
            }
            if (document.getElementById("lblgetarch").value == "yes") {
                document.getElementById("lblgetarch").value = "no"
                chk = document.getElementById("lblchk").value;
                did = document.getElementById("lbldid").value;
                clid = document.getElementById("lblclid").value;
                getarch(chk, did, clid)
            }
        }

        function gettab(name) {
            //alert(name)
            closeall(name);
            var piccnt = "";
            var sid = document.getElementById("lblsid").value;
            if (name == "pm") {
                document.getElementById("tdpm").className = "thdrhov label";
                document.getElementById("pm").className = 'tdborder view';
                document.getElementById("ifmain").src = "PMMainMantpm.aspx?sid=" + sid + "&date=" + Date();
            }
            else if (name == "eq") {
                document.getElementById("tdeq").className = "thdrhov label";
                document.getElementById("eq").className = 'tdborder view';
                //alert(name + "," + tflag)
                document.getElementById("lbljump").value = "eq";
                var id = document.getElementById("lblpmid").value;
                var eid = document.getElementById("lbleqid").value;
                var piccnt = "";
                //checkarch();
                document.getElementById("geteq").src = "PMGetPMMantpm.aspx?jump=yes&sid=" + sid + "&typ=no&eqid=" + eid + "&pmid=" + id + "&piccnt=" + piccnt;

            }
            else if (name == "fm") {
                var wo = document.getElementById("lblwo").value;
                document.getElementById("tdfm").className = "thdrhov label";
                document.getElementById("pm").className = 'tdborder view';
                var pmid = document.getElementById("lblpmid").value;
                if (pmid == "") pmid = "0";
                document.getElementById("ifmain").src = "pmfmmaintpm.aspx?pmid=" + pmid + "&date=" + Date() + "&wo=" + wo;

            }
            else if (name == "act") {

                //document.getElementById("tdact").className = "thdrhov label";
                document.getElementById("pm").className = 'tdborder view';
                var pmid = document.getElementById("lblpmid").value;
                var pmhid = document.getElementById("lblpmhid").value;
                var wo = document.getElementById("lblwo").value;
                var sid = document.getElementById("lblsid").value;
                if (document.getElementById("eq").className == 'tdborder view') {
                    document.getElementById("pm").className = 'tdborder details';
                }
                if (pmid == "") pmid = "0";
                //alert("pmactmaintpm.aspx?pmid=" + pmid + "&pmhid=" + pmhid + "&wo=" + wo + "&date=" + Date())
                //document.getElementById("ifmain").src = "pmactmaintpm.aspx?sid=" + sid + "&pmid=" + pmid + "&pmhid=" + pmhid + "&wo=" + wo + "&date=" + Date();
                var eReturn = window.showModalDialog("pmactdialogtpm.aspx?sid=" + sid + "&pmid=" + pmid + "&pmhid=" + pmhid + "&wo=" + wo + "&date=" + Date(), "", "dialogHeight: 700px; dialogWidth:1040px;resizable=yes");
                if (document.getElementById("eq").className == 'tdborder view') {
                    document.getElementById("tdact").className = "thdr label";
                    document.getElementById("pm").className = 'tdborder details';
                }
                if (eReturn) {
                    document.getElementById("tdact").className = "thdr label";
                    if (document.getElementById("eq").className == 'tdborder view') {
                        document.getElementById("pm").className = 'tdborder details';
                    }
                }
            }
            else if (name == "cost") {
                document.getElementById("tdcost").className = "thdrhov label";
                document.getElementById("pm").className = 'tdborder view';
                var pmid = document.getElementById("lblpmid").value;
                var pmhid = document.getElementById("lblpmhid").value;
                var wo = document.getElementById("lblwo").value;
                if (pmid == "") pmid = "0";
                document.getElementById("ifmain").src = "pmcoststpm.aspx?pmid=" + pmid + "&pmhid=" + pmhid + "&wo=" + wo + "&date=" + Date();

            }
        }
        function closeall(name) {

            if (name != "act") {

                document.getElementById("tdeq").className = "thdr label";
                document.getElementById("tdpm").className = "thdr label";
                document.getElementById("tdfm").className = "thdr label";
                document.getElementById("tdact").className = "thdr label";
                document.getElementById("tdcost").className = "thdr label";

                document.getElementById("pm").className = 'details';
                document.getElementById("eq").className = 'details';

            }

        }
         //-->
    </script>
</head>
<body class="tbg" onload="checkarch();">
    <form id="form1" method="post" runat="server">
    <table style="left: 7px; position: absolute; top: 83px; z-index: 1;" cellspacing="0"
        cellpadding="2" width="1020">
        <tr>
            <td width="790">
                <table cellspacing="1" cellpadding="2">
                    <tr>
                        <td class="thdrhov label" id="tdpm" onclick="gettab('pm');" width="160" runat="server"
                            height="20px">
                            <asp:Label ID="lang946" runat="server">Current TPM Records</asp:Label>
                        </td>
                        <td class="thdr label" id="tdeq" onclick="gettab('eq');" width="140" runat="server">
                            <asp:Label ID="lang947" runat="server">TPM Manager</asp:Label>
                        </td>
                        <td class="thdr label" id="tdfm" onclick="gettab('fm');" width="140" runat="server">
                            <asp:Label ID="lang948" runat="server">Failure Reporting</asp:Label>
                        </td>
                        <td class="thdr label" id="tdact" onclick="gettab('act');" width="140" runat="server">
                            <asp:Label ID="lang949" runat="server">TPM Actuals</asp:Label>
                        </td>
                        <td class="thdr label" id="tdcost" onclick="gettab('cost');" width="140" runat="server">
                            <asp:Label ID="lang950" runat="server">TPM Costs</asp:Label>
                        </td>
                        <td width="70">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td width="3">
                &nbsp;
            </td>
            <td class="thdrsinglft" id="e1" width="26">
                <img src="../images/appbuttons/minibuttons/eqarch.gif" border="0">
            </td>
            <td class="thdrsingrt label" id="e2" align="left" width="204">
                <asp:Label ID="lang951" runat="server">Asset Hierarchy</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="details" id="eq" runat="server">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <iframe id="geteq" style="border-top-style: none; border-right-style: none; border-left-style: none;
                                border-bottom-style: none; background-color: transparent" src="pmholdtpm.htm"
                                frameborder="no" width="780" scrolling="no" height="52" runat="server" allowtransparency>
                            </iframe>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <iframe id="iftaskdet" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                                margin: 0px; border-top-style: none; padding-top: 0px; border-right-style: none;
                                border-left-style: none; border-bottom-style: none; background-color: transparent"
                                src="pmholdtpm.htm" frameborder="no" width="780" scrolling="yes" height="450"
                                runat="server" allowtransparency></iframe>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="tdborder" id="pm" valign="top" runat="server">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <iframe id="ifmain" src="pmholdtpm.htm" frameborder="no" width="780" scrolling="no"
                                height="504" runat="server" style="background-color: transparent" allowtransparency>
                            </iframe>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
            <td valign="top" colspan="2" rowspan="2">
                <table cellspacing="0">
                    <tr>
                        <td colspan="2">
                            <iframe id="ifarch" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                                margin: 0px; border-top-style: none; padding-top: 0px; border-right-style: none;
                                border-left-style: none; border-bottom-style: none; background-color: transparent"
                                src="PMArchMantpm.aspx?start=no" frameborder="no" width="250" scrolling="yes"
                                height="200" runat="server" allowtransparency></iframe>
                        </td>
                    </tr>
                    <tr height="22">
                        <td class="thdrsinglft" width="26">
                            <img src="../images/appbuttons/minibuttons/eqarch.gif" border="0">
                        </td>
                        <td class="thdrsingrt label" width="204">
                            <asp:Label ID="lang952" runat="server">Asset Images</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <iframe id="ifimg" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                                margin: 0px; border-top-style: none; padding-top: 0px; border-right-style: none;
                                border-left-style: none; border-bottom-style: none; background-color: transparent"
                                src="../equip/eqimg.aspx?eqid=0&amp;fuid=0" frameborder="no" width="250" scrolling="no"
                                height="272" runat="server" allowtransparency></iframe>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div class="details" id="subdiv" style="border-right: black 1px solid; border-top: black 1px solid;
        z-index: 999; border-left: black 1px solid; width: 360px; border-bottom: black 1px solid;
        height: 100px">
        <table cellspacing="0" cellpadding="0" width="360" bgcolor="white">
            <tr bgcolor="blue" height="20">
                <td class="labelwht">
                    <asp:Label ID="lang953" runat="server">Revision Alert</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closerev();" height="18" alt="" src="../images/close.gif" width="18"><br>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <iframe id="ifrev" style="width: 360px; height: 100px" src="" frameborder="no" runat="server">
                    </iframe>
                </td>
            </tr>
        </table>
    </div>
    <input id="lbltab" type="hidden" name="lbltab" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lbldid" type="hidden" name="lbldid" runat="server"><input id="lblclid"
        type="hidden" name="lblclid" runat="server">
    <input id="lblret" type="hidden" name="lblret" runat="server"><input id="lblchk"
        type="hidden" name="lblchk" runat="server">
    <input id="lbldchk" type="hidden" name="lbldchk" runat="server"><input id="lblfuid"
        type="hidden" name="lblfuid" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
    <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="tasknum" type="hidden" name="tasknum" runat="server"><input id="taskcnt"
        type="hidden" name="taskcnt" runat="server">
    <input id="lblgetarch" type="hidden" name="lblgetarch" runat="server">
    <input id="lblpmid" type="hidden" runat="server">
    <input type="hidden" id="lbljump" runat="server">
    <input type="hidden" id="lblwo" runat="server">
    <input type="hidden" id="lblpmhid" runat="server"><input type="hidden" id="lblro"
        runat="server">
    <input type="hidden" id="lblusername" runat="server">
    <input type="hidden" id="lbluid" runat="server">
    <input type="hidden" id="lblislabor" runat="server">
    <input type="hidden" id="lblissuper" runat="server">
    <input type="hidden" id="lblisplanner" runat="server">
    <input type="hidden" id="lblLogged_In" runat="server" name="lblLogged_In">
    <input type="hidden" id="lblms" runat="server" name="lblms">
    <input type="hidden" id="lblappstr" runat="server" name="lblappstr">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
    <uc1:mmenu1 ID="Mmenu11" runat="server"></uc1:mmenu1>
</body>
</html>
