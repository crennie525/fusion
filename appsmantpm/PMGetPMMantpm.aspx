<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMGetPMMantpm.aspx.vb"
    Inherits="lucy_r12.PMGetPMMantpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMGetPMMan</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMGetPMMantpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        <!--
        function checkup() {
            var islabor = document.getElementById("lblislabor").value;
            if (islabor != "1") {
                var eqid = document.getElementById("lbleqid").value;
                window.parent.checkup(eqid);
            }
        }
        function checkpg() {
            document.getElementById("tdeq").innerHTML = document.getElementById("lbleq").value
            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                window.parent.doref();
            }
            else {
                window.parent.setref();
            }

            var gototasks = document.getElementById("lblgototasks").value;
            //alert(gototasks)
            if (log == "no") window.parent.setref();
            var app = document.getElementById("appchk").value;

            if (app == "switch") window.parent.handleapp(app);

            var chk = document.getElementById("lblpar").value;

            if (chk == "eq") {
                valu = document.getElementById("lbleqid").value;
                window.parent.handleeq(chk, valu);
            }

            var clean = document.getElementById("lblcleantasks").value;

            if (clean == "1") {
                document.getElementById("lblcleantasks").value = "2";
                var sid = document.getElementById("lblsid").value;
                window.parent.handletasks("PMGridMantpm.aspx?start=no&jump=no&sid=" + sid)
            }


            if (gototasks == "1") {

                document.getElementById("lblgototasks").value = "0";
                document.getElementById("lblcleantasks").value = "0";
                eqid = document.getElementById("lbleqid").value;
                pmid = document.getElementById("lblpmid").value;
                tcnt = document.getElementById("lbltaskcnt").value;
                fcust = document.getElementById("lblfcust").value;
                var mod = document.getElementById("lblgototasks").value;
                var fu = document.getElementById("lblfuid").value;
                var co = document.getElementById("lblcoid").value;
                var typ = document.getElementById("lbltyp").value;
                var sid = document.getElementById("lblsid").value;
                //alert(tcnt)
                if (tcnt == "1") {
                    window.parent.handletasks("PMGridMantpm.aspx?start=yes&jump=yes&typ=no&eqid=" + eqid + "&pmid=" + pmid + "&fcust=" + fcust + "&sid=" + sid, "ok")
                }
                else if (tcnt == "0") {
                    window.parent.handletasks("PMGridMantpm.aspx?start=yes&jump=no&typ=no&eqid=" + eqid + "&pmid=" + pmid + "&sid=" + sid, "ok")
                }
                else if (tcnt == "2") {
                    window.parent.handletasks("PMGridMantpm.aspx?start=yes&jump=yes&typ=" + typ + "&eqid=" + eqid + "&pmid=" + pmid + "&fu=" + fu + "&co=" + co + "&fcust=" + fcust + "&sid=" + sid, "ok")
                }
                else if (tcnt == "3") {
                    window.parent.handletasks("PMTaskDivFuncGrid.aspx?start=no", "cell")
                }
                else if (tcnt == "4") {
                    window.parent.handletasks("PMTaskDivFuncGrid.aspx?start=no", "eq")
                }
                else if (tcnt == "5") {
                    window.parent.handletasks("PMTaskDivFuncGrid.aspx?start=no", "fu")
                }

                window.parent.getimage(eqid);

            }

            else if (gototasks == "0") {
                //alert(document.getElementById("lbleqid").value)
                alert("No TPM Record Set Has Been Created For this Equipment Record\n\nPlease Review this Record in the TPM Optimizer or TPM Developer\nto ensure that All Skills, Frequencies, and Running/Down Times\nHave Been Established")
                //document.getElementById("ddeq").options.selectedIndex = 0;
                //document.getElementById("tdspl").innerHTML = "";
                //document.getElementById("tdloc").innerHTML = "";
                document.getElementById("lbleqid").value = "0";
                document.getElementById("tdeq").innerHTML = "";
                document.getElementById("lbleq").value = "";
                var sid = document.getElementById("lblsid").value;
                window.parent.handletasks("PMGridMantpm.aspx?start=no&jump=no&sid=" + sid)

            }
        }
        function getminsrch() {
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "lup";
            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                clearall();
                //alert(eReturn)
                var ret = eReturn.split("~");
                var eqid = ret[4];
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("tdeq").innerHTML = ret[5];
                document.getElementById("lbleq").value = ret[5];
                document.getElementById("lblsubmit").value = "new";
                document.getElementById("form1").submit();
            }
        }
        function getlocs1() {
            var sid = document.getElementById("lblsid").value;
            var lid = document.getElementById("lbllid").value;
            var typ = "lup"
            //alert(lid)
            if (lid != "") {
                typ = "retloc"
            }
            var eqid = document.getElementById("lbleqid").value;
            var fuid = document.getElementById("lblfuid").value;
            var coid = document.getElementById("lblcoid").value;
            //alert(fuid)
            var wo = "";
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                clearall();
                //alert(eReturn)
                var ret = eReturn.split("~");
                var eqid = ret[3];
                document.getElementById("lbleqid").value = ret[3];
                document.getElementById("tdeq").innerHTML = ret[2];
                document.getElementById("lbleq").value = ret[2];
                document.getElementById("lblsubmit").value = "new";
                document.getElementById("form1").submit();
            }
        }
        function clearall() {
            document.getElementById("lbleqid").value = "";
            document.getElementById("tdeq").innerHTML = "";
            document.getElementById("lbleq").value = "";
        }
//-->
    </script>
</head>
<body class="tbg" onload="checkpg();">
    <form id="form1" method="post" runat="server">
    <div style="position: absolute; top: 0px; left: 4px">
        <table cellspacing="0" cellpadding="0" width="740">
            <tr>
                <td width="92">
                </td>
                <td width="180">
                </td>
                <td width="20">
                </td>
                <td width="20">
                </td>
                <td width="20">
                </td>
                <td width="20">
                </td>
                <td width="20">
                </td>
                <td width="378">
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    <table>
                        <tr>
                            <td id="tddepts" class="bluelabel" runat="server">
                                Use Departments
                            </td>
                            <td>
                                <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                            <td id="tdlocs1" class="bluelabel" runat="server">
                                Use Locations
                            </td>
                            <td>
                                <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="eqdiv" height="20" runat="server">
                <td class="label">
                    <asp:Label ID="lang854" runat="server">Equipment#</asp:Label>
                </td>
                <td class="plainlabel" id="tdeq" runat="server">
                </td>
                <td>
                </td>
                <td>
                    <img id="iw" onmouseover="return overlib('Revision Check Icon', ABOVE, LEFT)" onmouseout="return nd()"
                        src="../images/appbuttons/minibuttons/gwarningnbg.gif" runat="server">
                </td>
                <td>
                    <img id="iadd" onmouseover="return overlib('Create PM Records For This Equipment Record', ABOVE, RIGHT, WIDTH, 350)"
                        onmouseout="return nd()" src="../images/appbuttons/minibuttons/addmod.gif" runat="server"
                        class="details" onclick="createpm();">
                </td>
                <td>
                    <img class="details" onmouseover="return overlib('Jump to PM Scheduling', ABOVE, LEFT)"
                        onclick="proj();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/sched.gif"
                        id="jts" runat="server">
                </td>
                <td>
                    <img onmouseover="return overlib('Help', ABOVE, LEFT)" onclick="gethelp();" onmouseout="return nd()"
                        src="../images/appbuttons/minibuttons/q.gif" height="19" width="19">
                </td>
                <td class="label" id="tddesc" runat="server">
                </td>
            </tr>
        </table>
    </div>
    <input id="lblcid" type="hidden" runat="server" name="lblcid">
    <input id="lblsid" type="hidden" runat="server" name="lblsid">
    <input id="lblchk" type="hidden" runat="server" name="lblchk"><input id="lbleqid"
        type="hidden" runat="server" name="lbleqid">
    <input id="lblpmid" type="hidden" runat="server" name="lblpmid"><input id="lblcleantasks"
        type="hidden" name="lblcleantasks" runat="server">
    <input id="lblgototasks" type="hidden" runat="server" name="lblgototasks"><input
        id="lbltaskcnt" type="hidden" name="lbltaskcnt" runat="server">
    <input id="appchk" type="hidden" runat="server" name="appchk"><input id="lbllog"
        type="hidden" name="lbllog" runat="server">
    <input id="lblgetarch" type="hidden" runat="server" name="lblgetarch">
    <input id="lblpar" type="hidden" runat="server" name="lblpar">
    <input id="lblerr" type="hidden" runat="server" name="lblerr">
    <input id="lblmod" type="hidden" runat="server" name="lblmod">
    <input id="lbljump" type="hidden" runat="server" name="lbljump"><input type="hidden"
        id="lblcurr" runat="server" name="lblcurr">
    <input type="hidden" id="lblfuid" runat="server" name="lblfuid">
    <input type="hidden" id="lblcoid" runat="server" name="lblcoid">
    <input type="hidden" id="lbltyp" runat="server" name="lbltyp"><input type="hidden"
        id="lblsubmit" runat="server" name="lblsubmit">
    <input type="hidden" id="lblro" runat="server" name="lblro"><input type="hidden"
        id="lblpiccnt" runat="server" name="lblpiccnt">
    <input type="hidden" id="lblfslang" runat="server" name="lblfslang">
    <input type="hidden" id="lbleq" runat="server" name="lbleq">
    <input type="hidden" id="lbllid" runat="server" name="lbllid">
    <input type="hidden" id="lblfcust" runat="server" />
    <input type="hidden" id="lbluserid" runat="server" />
    <input type="hidden" id="lblislabor" runat="server" />
    <input type="hidden" id="lblisplanner" runat="server" />
    <input type="hidden" id="lblcadm" runat="server" />
    <input type="hidden" id="lblappstr" runat="server" name="lblappstr">
    </form>
</body>
</html>
