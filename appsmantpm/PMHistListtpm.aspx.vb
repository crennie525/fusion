

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMHistListtpm
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim hist As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim pmid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            pmid = Request.QueryString("pmid").ToString
            hist.Open()
            GetHist(pmid)
            hist.Dispose()
        End If
    End Sub
    Private Sub GetHist(ByVal pmid As String)
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table width=""620"" border=""0"">")
        Dim pdm, pm, title, nextd As String
        sql = "select distinct pm.pmid, " _
              + "pdm = (case pm.ptid when 0 then 'None' else pm.pretech End ), " _
              + "pm = (pm.skill + '/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd), " _
              + "nextdate = Convert(char(10), pm.nextdate,101), pm.ttime, pm.dtime " _
              + "from tpm pm where pm.pmid = '" & pmid & "'"
        dr = hist.GetRdrData(sql)
        While dr.Read
            pdm = dr.Item("pdm")
            pm = dr.Item("pm")
            nextd = dr.Item("nextdate")
            If pdm <> "None" Then
                title = pdm & " - " & pm
            Else
                title = pm
            End If
            sb.Append("<tr><td colspan=""5"" class=""bigbold"">" & title & "</td></tr>")
            sb.Append("<tr><td colspan=""5"">&nbsp;</td></tr>")
            If nextd = "" Then nextd = "Not Scheduled"
            sb.Append("<tr><td colspan=""5"" class=""label"">Total Estimated Time: " & dr.Item("ttime") & " Minutes</td></tr>")
            sb.Append("<tr><td colspan=""5"" class=""label"">Total Down Time: " & dr.Item("dtime") & " Minutes</td></tr>")
            sb.Append("<tr><td colspan=""5"" class=""label"">Next Date: " & nextd & "</td></tr>")
        End While
        dr.Close()
        sb.Append("<tr><td colspan=""5"">&nbsp;</td></tr>")
        sb.Append("<tr height=""20""><td class=""label"" width=""120""><u>Complete Date</td>")
        sb.Append("<td class=""label"" width=""140""><u>Completed By</td>")
        sb.Append("<td class=""label"" width=""120""><u>Scheduled Start</td>")
        sb.Append("<td class=""label"" width=""120""><u>Actual Start</td>")
        sb.Append("<td class=""label"" width=""120""><u>Actual Finish</u></td></tr>")
        'sb.Append("<tr><td colspan=""5"">&nbsp;</td></tr>")



        sql = "select pmhid, compdate = isnull(Convert(char(10), compdate,101), 'Pending'), " _
        + "compby = isnull(compby, 'Pending'), schedstart = isnull(Convert(char(15), schedstart,101), 'Not Scheduled'), " _
        + "actstart = isnull(Convert(char(15), actstart,101), 'Not Provided'), " _
        + "actfinish = isnull(Convert(char(15), actfinish,101), 'Not Provided') from pmhisttpm " _
        + "where pmid = '" & pmid & "' order by pmhid"

        dr = hist.GetRdrData(sql)
        While dr.Read
            sb.Append("<tr height=""20""><td class=""plainlabel"">" & dr.Item("compdate") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("compby") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("schedstart") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("actstart") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("actfinish") & "</td></tr>")
        End While
        dr.Close()

        sb.Append("</table>")

        Response.Write(sb.ToString)



    End Sub
    Private Function GetHead(ByVal pmid As String) As String

    End Function
End Class
