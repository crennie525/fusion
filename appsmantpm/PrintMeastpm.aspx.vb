

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PrintMeastpm
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim meas As New Utilities
    Dim pmtskid, tmdid, shift As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        pmtskid = Request.QueryString("tid").ToString
        tmdid = Request.QueryString("mid").ToString
        shift = Request.QueryString("shift").ToString
        sql = "select m.hi, m.lo, m.measurement, Convert(char(10), m.pmdate, 101) as schedstart, " _
      + "a.tasknum, a.func, a.compnum, a.cqty, m.type, m.measure, m.spec, " _
      + "maxy = (select max(measurement) from pmTaskMeasDetManHISTtpm where pmtskid = '" & pmtskid & "'), " _
      + "maxl = (select min(measurement) from pmTaskMeasDetManHISTtpm where pmtskid = '" & pmtskid & "'), " _
      + "pm = (e.eqnum + ' - ' + pm.skill + '/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd) " _
      + "from pmTaskMeasDetManHISTtpm m left join pmhisttpm h on m.pmhid = h.pmhid " _
      + "left join tpm pm on pm.pmid = m.pmid left join equipment e on e.eqid = pm.eqid " _
       + "left join pmtracktpm a on a.pmtskid = m.pmtskid where m.pmtskid = '" & pmtskid & "' " _
        + "and m.tmdidpar = '" & tmdid & "' and m.shift = '" & shift & "' order by m.pmdate desc"

        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
        sb.Append("<tr><td width=""10""></td><td width=""130""></td><td width=""510""></td></tr>")
        sb.Append("<tr height=""30"" align=""center""><td class=""bigbold"" colspan=""3"">" & tmod.getlbl("cdlbl109" , "PrintMeastpm.aspx.vb") & "</td></tr>")

        Dim i As Integer
        Dim y As Integer = 0
        Dim cnt As Integer = 0
        Dim hi, lo, typ, spec, mea, pm, func, task, spstr, dt, ms As String
        meas.Open()
        dr = meas.GetRdrData(sql)
        While dr.Read
            If cnt = 0 Then
                pm = dr.Item("pm")
                sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""3"">" & pm & "</td></tr>")
                func = dr.Item("func").ToString
                sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">Function: " & func & "</td></tr>")
                task = "Task# " & dr.Item("tasknum") & " - " & dr.Item("compnum") & "(" & dr.Item("cqty") & ")"
                sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">" & task & "</td></tr>")
                hi = dr.Item("hi").ToString
                lo = dr.Item("lo").ToString
                typ = dr.Item("type").ToString
                mea = dr.Item("measure").ToString
                spec = dr.Item("spec").ToString
                spstr = typ & " - " & mea & " - HI(" & hi & ")-LO(" & lo & ")-SPEC(" & spec & ")"
                sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">" & spstr & "</td></tr>")
                sb.Append("<tr><td class=""bigbold"" colspan=""3""><hr></td></tr>")
                sb.Append("<tr><td class=""plainlabel"">&nbsp;</td><td class=""label""><u>TPM Date</u></td><td class=""label""><u>Measurement</u></td></tr>")
                cnt = cnt + 1
            End If
            ms = dr.Item("measurement").ToString
            dt = dr.Item("schedstart").ToString
            sb.Append("<tr><td></td><td class=""plainlabel"">" & dt & "</td><td class=""plainlabel"">" & ms & "</td></tr>")
        End While
        dr.Close()
        meas.Dispose()
        sb.Append("</table>")
        Response.Write(sb.ToString)

    End Sub

End Class
