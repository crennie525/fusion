

'********************************************************
'*
'********************************************************



Public Class pmfmmaintpm
    Inherits System.Web.UI.Page
	Protected WithEvents lang833 As System.Web.UI.WebControls.Label

	Protected WithEvents lang832 As System.Web.UI.WebControls.Label

	Protected WithEvents lang831 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim pmid, ro, wonum As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwod As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdj As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdjpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents wo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifwo As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents jp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifjp As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblupsav As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            pmid = Request.QueryString("pmid").ToString
            lblpmid.Value = pmid
            wonum = Request.QueryString("wo").ToString
            lblwo.Value = wonum
            ifjp.Attributes.Add("src", "pmfmtpm.aspx?pmid=" & pmid & "&wo=" & wonum)
        End If
    End Sub

	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang831.Text = axlabs.GetASPXPage("pmfmmaintpm.aspx","lang831")
		Catch ex As Exception
		End Try
		Try
			lang832.Text = axlabs.GetASPXPage("pmfmmaintpm.aspx","lang832")
		Catch ex As Exception
		End Try
		Try
			lang833.Text = axlabs.GetASPXPage("pmfmmaintpm.aspx","lang833")
		Catch ex As Exception
		End Try

	End Sub

End Class
