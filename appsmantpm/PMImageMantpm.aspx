<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMImageMantpm.aspx.vb"
    Inherits="lucy_r12.PMImageMantpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMImageMan</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMImageMantpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table style="left: 5px; position: absolute; top: 0px" cellpadding="0">
        <tr>
            <td colspan="3" align="center">
                <a onclick="getbig();" href="#">
                    <img id="imgeq" height="216" src="../images/appimages/eqimg1.gif" width="216" border="0"
                        runat="server"></a>
            </td>
        </tr>
        <tr>
            <td class="bluelabel" id="tdcnt" align="center" runat="server" width="150">
            </td>
            <td align="center">
                <a onclick="getprev();" href="#">
                    <img id="pr" alt="" src="../images/appbuttons/minibuttons/prevarrowbg.gif" border="0"
                        runat="server" width="20" height="20"></a> <a onclick="getnext();" href="#">
                            <img id="ne" alt="" src="../images/appbuttons/minibuttons/nextarrowbg.gif" border="0"
                                runat="server" width="20" height="20"></a>
            </td>
            <td align="center">
                <a onclick="getport();" href="#">
                    <img id="po" alt="" src="../images/appbuttons/bgbuttons/picgrid.gif" border="0" runat="server"
                        width="20" height="20"></a>
            </td>
        </tr>
    </table>
    <input id="lblpic" type="hidden" runat="server" name="lblpic"><input type="hidden"
        id="lbleqid" runat="server" name="lbleqid">
    <input type="hidden" id="lblcur" runat="server" name="lblcur"><input type="hidden"
        id="lblcnt" runat="server" name="lblcnt">
    <input type="hidden" id="lblpareqid" runat="server" name="lblpareqid"><input type="hidden"
        id="lblurl" runat="server" name="lblurl">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
