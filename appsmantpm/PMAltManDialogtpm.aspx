<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMAltManDialogtpm.aspx.vb"
    Inherits="lucy_r12.PMAltManDialogtpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMAltManDialog</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" type="text/javascript" src="../scripts/sessrefdialog.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMAltManDialogtpmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="resetsess();">
    <form id="form1" method="post" runat="server">
    <iframe id="ifcal" width="100%" height="100%" style="position: absolute; top: 0px;
        left: 0px" name="ifcal" src="" frameborder="no" runat="server"></iframe>
    <iframe id="ifsession" class="details" src="" frameborder="no" runat="server" style="z-index: 0">
    </iframe>
    <input type="hidden" id="lblsessrefresh" runat="server" name="lblsessrefresh">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
