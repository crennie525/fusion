

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wojpactm
    Inherits System.Web.UI.Page
	Protected WithEvents lang1425 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1424 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1423 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim jpid, wonum, typ, stat, icost, ro, rostr, Login As String
    Dim jpm As New Utilities
    Dim ap As New AppUtils
    Dim dr As SqlDataReader
    Protected WithEvents tdjpn As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents dgparts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblview As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trp As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trt As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trl As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Label24 As System.Web.UI.WebControls.Label
    Protected WithEvents txttsk As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdnotes As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdjpd As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                dgparts.Columns(0).Visible = False
            Else
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = jpm.CheckROS(rostr, "wo")
                    lblro.Value = ro
                    If ro = "1" Then
                        dgparts.Columns(0).Visible = False
                    End If
                End If
            End If
            wonum = Request.QueryString("wo").ToString
            jpid = Request.QueryString("jpid").ToString
            typ = Request.QueryString("typ").ToString
            stat = Request.QueryString("stat").ToString
            lblwo.Value = wonum
            lbljpid.Value = jpid
            lblview.Value = typ
            lblstat.Value = stat
            Dim icost As String = ap.InvEntry
            If icost = "ext" Then
                tdnotes.InnerHtml = "Material Costs\Usage are Entered Externally"
                dgparts.Columns(0).Visible = False
            ElseIf icost = "inv" Then
                tdnotes.InnerHtml = "Material Costs\Usage are Entered via Inventory"
                dgparts.Columns(0).Visible = False
            Else
                tdnotes.InnerHtml = "Material Costs\Usage are Entered Manually"
            End If
            If (stat <> "COMP" And stat <> "CAN" And stat <> "WAPPR") And (icost <> "ext" And icost <> "inv") Then
                dgparts.Columns(0).Visible = True
            Else
                dgparts.Columns(0).Visible = False
            End If
            If typ = "p" Then
                trp.Attributes.Add("class", "view")
            ElseIf typ = "t" Then
                trt.Attributes.Add("class", "view")
            ElseIf typ = "l" Then
                trl.Attributes.Add("class", "view")
            End If
            jpm.Open()
            GetParts()
            jpm.Dispose()
        End If
    End Sub
    Private Sub GetParts()
        wonum = lblwo.Value
        jpid = lbljpid.Value
        typ = lblview.Value
        If typ = "p" Then
            sql = "select m.*, j.tasknum, j.taskdesc from wojpparts m left join wojobtasks j on j.pmtskid = m.pmtskid " _
            + " where m.jpid = '" & jpid & "' and m.wonum = '" & wonum & "'"
        ElseIf typ = "t" Then
            sql = "select m.*, m.toolnum as itemnum, m.wotsktoolid as wotskpartid, m.rate as cost, j.tasknum, j.taskdesc from wojptools m " _
            + "left join wojobtasks j on j.pmtskid = m.pmtskid " _
            + " where m.jpid = '" & jpid & "' And m.wonum = '" & wonum & "'"
        ElseIf typ = "l" Then
            sql = "select m.*, m.lubenum as itemnum, m.wotsklubeid as wotskpartid, j.tasknum, j.taskdesc from wojplubes m left join wojobtasks j on j.pmtskid = m.pmtskid " _
            + " where m.jpid = '" & jpid & "' And m.wonum = '" & wonum & "'"
        End If

        dr = jpm.GetRdrData(sql)
        dgparts.DataSource = dr
        dgparts.DataBind()
        ro = lblro.Value
        If ro = "1" Then
            dgparts.Columns(0).Visible = False
        End If

    End Sub

    Private Sub dgparts_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.EditCommand
        stat = lblstat.Value
        If stat <> "COMP" And stat <> "CAN" Then
            dgparts.EditItemIndex = e.Item.ItemIndex
            jpm.Open()
            GetParts()
            jpm.Dispose()
        End If
    End Sub

    Private Sub dgparts_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.CancelCommand
        dgparts.EditItemIndex = -1
        jpm.Open()
        GetParts()
        jpm.Dispose()
    End Sub

    Private Sub dgparts_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.UpdateCommand
        Dim qtystr As String = CType(e.Item.FindControl("txtqty"), TextBox).Text
        Dim coststr As String = CType(e.Item.FindControl("txtcost"), TextBox).Text
        Dim pid As String = CType(e.Item.FindControl("lblpid"), Label).Text
        Dim ddu As DropDownList = CType(e.Item.FindControl("ddus"), DropDownList)
        Dim us As String = ddu.SelectedValue
        Dim txtchk As Long
        typ = lblview.Value
        If typ <> "l" Then
            Try
                txtchk = System.Convert.ToDecimal(qtystr)
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr571" , "wojpactm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        End If
        Try
            txtchk = System.Convert.ToDecimal(coststr)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr572" , "wojpactm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        jpm.Open()
        wonum = lblwo.Value
        If typ = "p" Then
            sql = "update wojpparts set qty = '" & qtystr & "', cost = '" & coststr & "' " _
            + "used = '" & us & "' where wotskpartid = '" & pid & "'; update wojpparts set usedtotal = (qtyused * cost) " _
            + "where wotskpartid = '" & pid & "'"
            If us = "Y" Then
                sql += "update workorder set actjpmatcost = (select sum(usedtotal) from wojpparts where wonum = '" & wonum & "' and " _
                + "jpid = '" & jpid & "' and used = 'Y') " _
                + "where wonum = '" & wonum & "'"
            End If
           
        ElseIf typ = "t" Then
            sql = "update wojptools set qty = '" & qtystr & "', rate = '" & coststr & "' " _
            + "used = '" & us & "' where wotsktoolid = '" & pid & "'; update wojptools set usedtotal = (qtyused * rate) " _
            + "where wotsktoolid = '" & pid & "'"
            If us = "Y" Then
                sql += "update workorder set actjptoolcost = (select sum(usedtotal) from wojptools where wonum = '" & wonum & "' and " _
                + "jpid = '" & jpid & "' and used = 'Y') " _
                + "where wonum = '" & wonum & "'"
            End If

        ElseIf typ = "l" Then
            sql = "update wojplubes set cost = '" & coststr & "', " _
            + "used = '" & us & "' where wotsklubeid = '" & pid & "'; "
            If us = "Y" Then
                sql += "update workorder set actjplubecost = (select sum(cost) from wojplubes where wonum = '" & wonum & "' and " _
                + "jpid = '" & jpid & "' and used = 'Y') where wonum = '" & wonum & "'"
            End If
           
        End If

        jpm.Update(sql)
        dgparts.EditItemIndex = -1
        GetParts()
        jpm.Dispose()
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If CatID <> "Select" And CatID <> "N/A" Then 'Not IsDBNull(CatID) OrElse  
            If CatID = "PASS" Then
                iL = 0
            ElseIf CatID = "FAIL" Then
                iL = 1
            ElseIf CatID = "Y" Then
                iL = 0
            ElseIf CatID = "N" Then
                iL = 1
            End If
        Else
            iL = -1
        End If
        Return iL
    End Function
    Private Sub dgparts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgparts.ItemDataBound
        typ = lblview.Value
        Dim ibfm As ImageButton
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            stat = lblstat.Value
            If stat = "COMP" Or stat = "CAN" Or stat = "WAPPR" Then
                ibfm = CType(e.Item.FindControl("imgeditfm"), ImageButton)
                ibfm.Attributes.Add("class", "details")

            End If
            Dim img As HtmlImage = CType(e.Item.FindControl("imgti"), HtmlImage)
            'Dim pmtskid As String = DataBinder.Eval(e.Item.DataItem, "pmtskid").ToString
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "taskdesc").ToString
            'tdtsk.InnerHtml = tsk
            img.Attributes.Add("onclick", "gettsk('" & tsk & "');")

        End If
        If e.Item.ItemType = ListItemType.EditItem Then
            If typ = "l" Then
                Dim qbox As TextBox = CType(e.Item.FindControl("txtqty"), TextBox)
                qbox.Enabled = False
            End If
            Dim img As HtmlImage = CType(e.Item.FindControl("imgte"), HtmlImage)
            'Dim pmtskid As String = DataBinder.Eval(e.Item.DataItem, "pmtskid").ToString
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "taskdesc").ToString
            'tdtsk.InnerHtml = tsk
            img.Attributes.Add("onclick", "gettsk('" & tsk & "');")
        End If

    End Sub
	

	

	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgparts.Columns(0).HeaderText = dlabs.GetDGPage("wojpactm.aspx","dgparts","0")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(1).HeaderText = dlabs.GetDGPage("wojpactm.aspx","dgparts","1")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(2).HeaderText = dlabs.GetDGPage("wojpactm.aspx","dgparts","2")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(3).HeaderText = dlabs.GetDGPage("wojpactm.aspx","dgparts","3")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(4).HeaderText = dlabs.GetDGPage("wojpactm.aspx","dgparts","4")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(6).HeaderText = dlabs.GetDGPage("wojpactm.aspx","dgparts","6")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(7).HeaderText = dlabs.GetDGPage("wojpactm.aspx","dgparts","7")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(8).HeaderText = dlabs.GetDGPage("wojpactm.aspx","dgparts","8")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(9).HeaderText = dlabs.GetDGPage("wojpactm.aspx","dgparts","9")
		Catch ex As Exception
		End Try

	End Sub

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			Label24.Text = axlabs.GetASPXPage("wojpactm.aspx","Label24")
		Catch ex As Exception
		End Try
		Try
			lang1423.Text = axlabs.GetASPXPage("wojpactm.aspx","lang1423")
		Catch ex As Exception
		End Try
		Try
			lang1424.Text = axlabs.GetASPXPage("wojpactm.aspx","lang1424")
		Catch ex As Exception
		End Try
		Try
			lang1425.Text = axlabs.GetASPXPage("wojpactm.aspx","lang1425")
		Catch ex As Exception
		End Try

	End Sub

End Class
