﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="womanfsns.aspx.vb" Inherits="lucy_r12.womanfsns" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <style type="text/css">
        .FreezePaneOn6
        {
            position: absolute;
            top: 52px;
            left: 0px;
            visibility: visible;
            display: block;
            width: 1230px;
            height: 480px;
            background-color: #eeeded;
            z-index: 999;
            filter: alpha(opacity=35);
            -moz-opacity: 0.35;
            padding-top: 3%;
        }
        .view2
        {
            visibility:visible;
        }
        .viewcell 
{ 
DISPLAY: table-cell;
VISIBILITY: visible; 
}
}
        .detailscell 
{ 
DISPLAY: table-cell;
VISIBILITY: hidden;
}
    </style>
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/woplansaspx.js"></script>
    <script language="javascript" type="text/javascript">
    <!--
        function uppri() {
            var chk = document.getElementById("lblusepri").value;
            if (chk == "yes") {
                var priid = document.getElementById("ddpri").value;
                document.getElementById("lblwoprid").value = priid;
            }
        }
        function checkpri() {
            var chk = document.getElementById("lblprimsg").value;
            if (chk == "usecust") {
                var conf = confirm("Your System is set for Custom Work Order Priorities\nWould you like to reset your current Work Order Priority\nselection and reload this Work Order?")
                if (conf == true) {
                    document.getElementById("lblsubmit").value = "updatepri";
                    document.getElementById("form1").submit();
                }
                else {
                    alert("Action Cancelled")
                }
            }
            else {
                var conf = confirm("Your System is set for Standard Work Order Priorities\nWould you like to reset your current Work Order Priority\nselection and reload this Work Order?")
                if (conf == true) {
                    document.getElementById("lblsubmit").value = "undopri";
                    document.getElementById("form1").submit();
                }
                else {
                    alert("Action Cancelled")
                }
            }
        }
        function checkent() {
            if (window.event) { e = window.event; }
            if (e.keyCode == 13) {
                var eq = document.getElementById("txteqauto").value;
                //alert("checkent")
                if (eq != "") {
                    get_eqauto();
                }
            }
        }
        function checkents() {
            if (window.event) { e = window.event; }
            if (e.keyCode == 66 || e.keyCode == 13) {
                var eq = document.getElementById("txtsuperauto").value;
                //alert("checkent")
                if (eq != "") {
                    get_superauto();
                }
            }
        }
        function checkentl() {
            //alert("checkent")
            if (window.event) { e = window.event; }
            //alert(e.keyCode)
            if (e.keyCode == 66 || e.keyCode == 13) {
                var eq = document.getElementById("txtleadauto").value;
                
                if (eq != "") {
                    get_leadauto();
                }
            }
        }
        function get_eqauto() {
            //alert("get_eqauto")
            var wo = document.getElementById("lblwonum").value;
            var sid = document.getElementById("lblsid").value;
            var eq = document.getElementById("txteqauto").value;
            var usr = document.getElementById("lbluser").value;
            //alert(eq + "," + wo + "," + sid)
            if (eq != "" && wo != "" && sid != "") {
                //alert("get_eqauto")
                document.getElementById("ifeqauto").src = "eqverif.aspx?wpage=wo&eqnum=" + eq + "&sid=" + sid + "&wonum=" + wo + "&usr=" + usr;
            }
        }
        function get_superauto() {
            //alert("get_eqauto")
            var wo = document.getElementById("lblwonum").value;
            var sid = document.getElementById("lblsid").value;
            var eq = document.getElementById("txtsuperauto").value;
            var usr = document.getElementById("lbluser").value;
            //alert(eq + "," + wo + "," + sid)
            if (eq != "" && wo != "" && sid != "") {
                //alert("get_eqauto")
                document.getElementById("ifsuperauto").src = "superverif.aspx?wpage=wo&eqnum=" + eq + "&sid=" + sid + "&wonum=" + wo + "&usr=" + usr;
            }
        }
        function get_leadauto() {
            //alert("get_eqauto")
            var wo = document.getElementById("lblwonum").value;
            var sid = document.getElementById("lblsid").value;
            var eq = document.getElementById("txtleadauto").value;
            var usr = document.getElementById("lbluser").value;
            //alert(eq + "," + wo + "," + sid)
            if (eq != "" && wo != "" && sid != "") {
                //alert("get_eqauto")
                document.getElementById("ifleadauto").src = "leadverif.aspx?wpage=wo&eqnum=" + eq + "&sid=" + sid + "&wonum=" + wo + "&usr=" + usr;
            }
        }
        function handle_superauto(ret) {
            var retarr = ret.split("~");
            var who = retarr[0];
            if (who == "1") {
                //not sure if values need to be recorded
                document.getElementById("tdsave").innerHTML = "";
                document.getElementById("lblchecksave").value = "";
            }
            else {
                checksave();
            }
        }
        function handle_leadauto(ret) {
            var retarr = ret.split("~");
            var who = retarr[0];
            if (who == "1") {
                //not sure if values need to be recorded
                document.getElementById("tdsave").innerHTML = "";
                document.getElementById("lblchecksave").value = "";
            }
            else {
                checksave();
            }
        }
        function handle_eqauto(ret) {
            //alert()
            var coi = document.getElementById("lbliscol").value;
            //ret = who + "~" + eqnum + "~" + eqid + "~" + eqdesc + "~" + fpc + "~" + loc + "~" + parid + "~" + dept + "~" + did + "~" + cell + "~" + clid;

            var retarr = ret.split("~");
            var who = retarr[0];

            if (who == "D") {
                clearall();
                document.getElementById("tdfail").innerHTML = "";
                document.getElementById("lbldid").value = retarr[8];
                document.getElementById("lbldept").value = retarr[7];
                document.getElementById("tddept").innerHTML = retarr[7];
                document.getElementById("lblclid").value = retarr[10];
                document.getElementById("lblcell").value = retarr[9];
                document.getElementById("tdcell").innerHTML = retarr[9];
                document.getElementById("lbleqid").value = retarr[2];
                document.getElementById("lbleq").value = retarr[1];
                document.getElementById("tdeq").innerHTML = retarr[1];
                document.getElementById("tdeqdesc").innerHTML = retarr[3];
                document.getElementById("trdepts").className = "view";
                document.getElementById("lbltyp").value = "depts";
                if (coi == "yes") {
                    document.getElementById("imgchd").className = "details";
                    document.getElementById("Label3").innerHTML = "Column";
                    document.getElementById("tdcharge1").innerHTML = retarr[4];
                }
            }
            else if (who == "L") {
                clearall();
                //alert(ret[6] + ", " + ret[5] + ", " + ret[2] + ", " + ret[3])
                document.getElementById("tdfail3").innerHTML = "";
                document.getElementById("lbllid").value = retarr[6];
                document.getElementById("lblloc").value = retarr[5];
                document.getElementById("tdloc3").innerHTML = retarr[5];

                document.getElementById("lbleq").value = retarr[1];
                document.getElementById("tdeq3").innerHTML = retarr[1];
                document.getElementById("lbleqid").value = retarr[2];

                document.getElementById("trmisc3").className = "details";

                document.getElementById("trlocs3").className = "view";
                document.getElementById("lbltyp").value = "locs";
                if (coi == "yes") {
                    document.getElementById("imgch").className = "details";
                    document.getElementById("Label5").innerHTML = "Column";
                    document.getElementById("tdcharge3").innerHTML = retarr[4];
                }
                document.getElementById("tdeqdesc3").innerHTML = retarr[3];
            }
            else if (who == "N") {
                alert("No Record Found")
            }
            else if (who == "M") {
                alert("Search Returned Multiple Records")
            }

        }
        function getnotes() {
            var rootid = document.getElementById("lblrootid").value;
            var noteid = document.getElementById("lblnoteid").value;
            var user = document.getElementById("lbluser").value;
            var userid = "";
            var wonum = document.getElementById("lblwonum").value;
            //alert(rootid + "," + noteid)
            var eReturn = window.showModalDialog("wonotesdialog.aspx?wonum=" + wonum + "&rootid=" + rootid + "&noteid=" + noteid + "&user=" + user + "&userid=" + userid, "", "dialogHeight:600px; dialogWidth:750px; resizable=yes");
            if (eReturn) {
                var retarr = eReturn.split(",");
                //alert(eReturn)
                document.getElementById("lblrootid").value = retarr[0];
                document.getElementById("lblnoteid").value = retarr[1];
            }
        }
        function getnotesfs() {
            var rootid = document.getElementById("lblrootid").value;
            var noteid = document.getElementById("lblnoteid").value;
            var user = document.getElementById("lbluser").value;
            var userid = "";
            var sid = document.getElementById("lblsid").value;
            var wonum = document.getElementById("lblwonum").value;
            var wo = document.getElementById("lblwonum").value;
            var jp = document.getElementById("lbljpid").value;
            var stat = document.getElementById("lblstat").value;
            var ro = document.getElementById("lblro").value;
            document.getElementById("ifnotes").src = "wonotesdialog.aspx?who=fs&wonum=" + wonum + "&rootid=" + rootid + "&noteid=" + noteid + "&user=" + user + "&userid=" + userid
            document.getElementById("ifwo").src = "wolabtransfs.aspx?sid=" + sid + "&wo=" + wo + "&jpid=" + jp + "&stat=" + stat + "&date=" + Date() + "&ro=" + ro;
            //alert(rootid + "," + noteid)
            /*
            var eReturn = window.showModalDialog("wonotesdialog.aspx?wonum=" + wonum + "&rootid=" + rootid + "&noteid=" + noteid + "&user=" + user + "&userid=" + userid, "", "dialogHeight:600px; dialogWidth:750px; resizable=yes");
            if (eReturn) {
            var retarr = eReturn.split(",");
            //alert(eReturn)
            document.getElementById("lblrootid").value = retarr[0];
            document.getElementById("lblnoteid").value = retarr[1];
            }
            */
        }
        function getplanner(typ) {
            //var atyp = "plan";
            //alert(typ)
            var stat = document.getElementById("lblstat").value;
            if (stat != "COMP" && stat != "CAN") {
                window.parent.setref();
                var skill = "";
                var ro = document.getElementById("lblro").value;
                var wo = document.getElementById("lblwonum").value;
                var sid = document.getElementById("lblsid").value;
                var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&wo=" + wo + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
                if (eReturn) {
                    if (eReturn != "") {
                        var retarr = eReturn.split(",")
                        if (typ == "plan") {
                            document.getElementById("lblplnrid").value = retarr[0];
                            document.getElementById("lblplanner").value = retarr[1];
                            document.getElementById("lblsubmit").value = "aplan";
                            document.getElementById("form1").submit();
                        }
                        else {
                            document.getElementById("lblwplannerid").value = retarr[0];
                            document.getElementById("lblwplanner").value = retarr[1];
                            document.getElementById("tdwoplanner").innerHTML = retarr[1];
                            document.getElementById("lblsubmit").value = "go";
                            document.getElementById("form1").submit();
                        }
                    }
                }
            }
        }
        function checkreq() {
            var dis = document.getElementById("lbldis").value;
            //alert(dis)
            //if (dis == "0" || dis == "") {
            var stat = document.getElementById("lblstat").value;
            if (stat != "COMP" && stat != "CAN" && stat != "INPRG") {
                var chk = document.getElementById("cbreq");

                if (chk.checked == true) {
                    window.parent.setref();
                    var jpref = "";
                    var wo = document.getElementById("lblwonum").value;
                    var eReturn = window.showModalDialog("../appsrt/jobrefscopedialog.aspx?jpref=" + jpref + "&wo=" + wo, "", "dialogHeight:260px; dialogWidth:440px; resizable=yes");
                    if (eReturn) {
                        //alert(eReturn)
                        if (eReturn != "none") {
                            if (eReturn != "rem") {
                                document.getElementById("lbljpref").value = eReturn;
                                document.getElementById("lblsubmit").value = "nplan";
                                document.getElementById("form1").submit();
                            }
                            else {
                                document.getElementById("cbreq").checked = false;
                                document.getElementById("lblplanner2").value = "";
                                document.getElementById("tdplanner2").innerHTML = "";
                                document.getElementById("tdref").innerHTML = "";
                                document.getElementById("tdplanner").className = "graylabel";
                                document.getElementById("imgplanner").src = "../images/appbuttons/minibuttons/magnifierdis.gif";
                                document.getElementById("imgplanner").onclick = "";
                                document.getElementById("tdjp").className = "label";
                                document.getElementById("imgplans").src = "../images/appbuttons/minibuttons/magnifier.gif";
                                document.getElementById("imgaddplan").src = "../images/appbuttons/minibuttons/addmod.gif";
                                document.getElementById("imgeditplan").src = "../images/appbuttons/minibuttons/pencil.gif";
                                document.getElementById("lbldis").value = "0";
                            }
                        }
                    }
                }
                else {
                    var conf = confirm("This will Cancel your Job Planning Request.\nAre you sure you want to Continue?")
                    if (conf == true) {
                        var jpref = "";
                        var wo = document.getElementById("lblwonum").value;
                        var eReturn = window.showModalDialog("../appsrt/jobrefscopedialog.aspx?jpref=" + jpref + "&wo=" + wo + "&typ=rem", "", "dialogHeight:260px; dialogWidth:440px; resizable=yes");
                        //document.getElementById("lblsubmit").value="rplan";
                        //document.getElementById("form1").submit();
                        if (eReturn) {
                            document.getElementById("cbreq").checked = false;
                            document.getElementById("lblplanner2").value = "";
                            document.getElementById("tdplanner2").innerHTML = "";
                            document.getElementById("tdref").innerHTML = "";
                            document.getElementById("tdplanner").className = "graylabel";
                            document.getElementById("imgplanner").src = "../images/appbuttons/minibuttons/magnifierdis.gif";
                            document.getElementById("imgplanner").onclick = "";
                            document.getElementById("tdjp").className = "label";
                            document.getElementById("imgplans").src = "../images/appbuttons/minibuttons/magnifier.gif";
                            document.getElementById("imgaddplan").src = "../images/appbuttons/minibuttons/addmod.gif";
                            document.getElementById("imgeditplan").src = "../images/appbuttons/minibuttons/pencil.gif";
                            document.getElementById("lbldis").value = "0";
                        }
                    }
                    else {
                        alert("Action Cancelled")
                    }

                }
            }
            if (stat == "INPRG") {
                alert("Request for Planning should be made before a Work Order is put In Progress")
            }
            //}
        }
        function geterr() {
            var stat = document.getElementById("lblstat").value;
            var sid = document.getElementById("lblsid").value;
            var wo = document.getElementById("lblwonum").value;
            var fail = "";
            var wt = document.getElementById("lblwt").value;
            if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
                var eReturn = window.showModalDialog("errorcodesdialog.aspx?typ=wo&sid=" + sid + "&wo=" + wo, "", "dialogHeight:400px; dialogWidth:500px; resizable=yes");
                if (eReturn) {
                    //alert(eReturn)
                    //alert(fail)
                    var ret = eReturn.split("~");
                    fail = "Error Code 1: " + ret[0] + "<br>";
                    fail += "Error Code 2: " + ret[1] + "<br>";
                    fail += "Error Code 3: " + ret[2];
                    document.getElementById("tderr").innerHTML = fail;
                }
            }
        }
        function getfc(who) {
            var stat = document.getElementById("lblstat").value;
            var sid = document.getElementById("lblsid").value;
            var wo = document.getElementById("lblwonum").value;
            var coid = document.getElementById("lblcomid").value;
            var fail = "";
            var wt = document.getElementById("lblwt").value;
            if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM" && coid != "") {
                var eReturn = window.showModalDialog("../apps/appgetfcdialog.aspx?sid=" + sid + "&wo=" + wo + "&coid=" + coid, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    //alert(eReturn)
                    //alert(fail)

                    var ret = eReturn.split(",");
                    for (i = 0; i < ret.length; i++) {
                        //alert(ret[i])
                        if (fail == "") {
                            fail = ret[i]
                        }
                        else {
                            fail += "<br>" + ret[i]
                        }
                        //alert(fail)
                    }
                    if (who == "1") {
                        document.getElementById("tdfail").innerHTML = "";
                        document.getElementById("tdfail").innerHTML = fail;
                    }
                    else {
                        document.getElementById("tdfail3").innerHTML = "";
                        document.getElementById("tdfail3").innerHTML = fail;
                    }
                }
            }
        }
        function getminsrch() {
            var did = document.getElementById("lbldid").value;
            //alert(did)
            if (did != "") {
                retminsrch();
            }
            else {
                var stat = document.getElementById("lblstat").value;
                var sid = document.getElementById("lblsid").value;
                var wo = document.getElementById("lblwonum").value;
                var typ;
                if (wo == "") {
                    typ = "lu";
                }
                else {
                    typ = "wo";
                }
                var wt = document.getElementById("lblwt").value;
                if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM" && wt != "MAXPM") {
                    var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
                    if (eReturn) {
                        clearall();
                        var ret = eReturn.split("~");
                        document.getElementById("tdfail").innerHTML = "";
                        document.getElementById("lbldid").value = ret[0];
                        document.getElementById("lbldept").value = ret[1];
                        document.getElementById("tddept").innerHTML = ret[1];
                        document.getElementById("lblclid").value = ret[2];
                        document.getElementById("lblcell").value = ret[3];
                        document.getElementById("tdcell").innerHTML = ret[3];
                        document.getElementById("lbleqid").value = ret[4];
                        document.getElementById("lbleq").value = ret[5];
                        document.getElementById("tdeq").innerHTML = ret[5];
                        document.getElementById("lblfuid").value = ret[6];
                        document.getElementById("lblfu").value = ret[7];
                        document.getElementById("tdfu").innerHTML = ret[7];
                        document.getElementById("lblcomid").value = ret[8];
                        document.getElementById("lblcomp").value = ret[9];
                        document.getElementById("tdco").innerHTML = ret[9];
                        var hasmisc = ret[10];
                        if (hasmisc != "") {
                            document.getElementById("lblncid").value = ret[10];
                            document.getElementById("lblnc").value = ret[11];
                            document.getElementById("tdmisc").innerHTML = ret[11];
                        }
                        else {
                            document.getElementById("trmisc").className = "details";
                        }

                        document.getElementById("lbllid").value = ret[12];
                        document.getElementById("lblloc").value = ret[13];
                        document.getElementById("tdloc").innerHTML = ret[13];
                        document.getElementById("trdepts").className = "view";
                        document.getElementById("lbltyp").value = "depts";

                        var coi = document.getElementById("lbliscol").value;
                        if (coi == "yes") {
                            document.getElementById("imgchd").className = "details";
                            document.getElementById("Label3").innerHTML = "Column";
                            document.getElementById("tdcharge1").innerHTML = ret[14];
                        }
                        document.getElementById("tdeqdesc").innerHTML = ret[15];
                    }
                }
            }
        }
        function retminsrch() {
            //alert()
            var sid = document.getElementById("lblsid").value;
            var wo = document.getElementById("lblwonum").value;
            var typ = "woret";
            var did = document.getElementById("lbldid").value;
            var dept = document.getElementById("lbldept").value;
            var clid = document.getElementById("lblclid").value;
            var cell = document.getElementById("lblcell").value;
            var eqid = document.getElementById("lbleqid").value;
            var eq = document.getElementById("lbleq").value;
            var fuid = document.getElementById("lblfuid").value;
            var fu = document.getElementById("lblfu").value;
            var coid = document.getElementById("lblcomid").value;
            var comp = document.getElementById("lblcomp").value;
            var lid = document.getElementById("lbllid").value;
            var loc = document.getElementById("lblloc").value;
            //if (cell == "" && who != "checkfu" && who != "checkeq") {
            who = "deptret";
            //}

            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("lbldept").value = ret[1];
                document.getElementById("tddept").innerHTML = ret[1];
                document.getElementById("lblclid").value = ret[2];
                document.getElementById("lblcell").value = ret[3];
                document.getElementById("tdcell").innerHTML = ret[3];
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("lbleq").value = ret[5];
                document.getElementById("tdeq").innerHTML = ret[5];
                document.getElementById("lblfuid").value = ret[6];
                document.getElementById("lblfu").value = ret[7];
                document.getElementById("tdfu").innerHTML = ret[7];
                document.getElementById("lblcomid").value = ret[8];
                document.getElementById("lblcomp").value = ret[9];
                document.getElementById("tdco").innerHTML = ret[9];
                var hasmisc = ret[10];
                if (hasmisc != "") {
                    document.getElementById("lblncid").value = ret[10];
                    document.getElementById("lblnc").value = ret[11];
                    document.getElementById("tdmisc").innerHTML = ret[11];
                }
                else {
                    document.getElementById("trmisc").className = "details";
                }
                document.getElementById("lbllid").value = ret[12];
                document.getElementById("lblloc").value = ret[13];
                document.getElementById("trdepts").className = "view";
                document.getElementById("lblrettyp").value = "depts";

                var coi = document.getElementById("lbliscol").value;
                if (coi == "yes") {
                    document.getElementById("imgchd").className = "details";
                    document.getElementById("Label3").innerHTML = "Column";
                    document.getElementById("tdcharge1").innerHTML = ret[14];
                }
                document.getElementById("tdeqdesc").innerHTML = ret[15];
                var did = ret[0];
                var eqid = ret[4];
                var clid = ret[2];
                var fuid = ret[6];
                var coid = ret[8];
                var lid = ret[12];
                var typ;
                //if(lid=="") {
                //typ = "reg";
                //}
                //else {
                //typ = "dloc";
                //}
                typ = "reg"
                var task = "";
                //window.location = "pmget.aspx?jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;
            }
        }
        function clearall() {
            document.getElementById("tdfail").innerHTML = "";
            document.getElementById("lbldid").value = "";
            document.getElementById("lbldept").value = "";
            document.getElementById("tddept").innerHTML = "";
            document.getElementById("lblclid").value = "";
            document.getElementById("lblcell").value = "";
            document.getElementById("tdcell").innerHTML = "";
            document.getElementById("lbleqid").value = "";
            document.getElementById("lbleq").value = "";
            document.getElementById("tdeq").innerHTML = "";
            document.getElementById("lblfuid").value = "";
            document.getElementById("lblfu").value = "";
            document.getElementById("tdfu").innerHTML = "";
            document.getElementById("lblcomid").value = "";
            document.getElementById("lblcomp").value = "";
            document.getElementById("tdco").innerHTML = "";
            document.getElementById("lblncid").value = "";
            document.getElementById("lblnc").value = "";
            document.getElementById("tdmisc").innerHTML = "";
            document.getElementById("lbllid").value = "";
            document.getElementById("lblloc").value = "";
            document.getElementById("tdloc").innerHTML = "";
            document.getElementById("trdepts").className = "details";
            document.getElementById("lbltyp").value = "";

            document.getElementById("tdfail3").innerHTML = "";
            document.getElementById("lbllid").value = "";
            document.getElementById("lblloc").value = "";
            document.getElementById("tdloc2").innerHTML = "";

            document.getElementById("lbleq").value = "";
            document.getElementById("tdeq2").innerHTML = "";
            document.getElementById("lbleqid").value = "";

            document.getElementById("trlocs").className = "details";

            document.getElementById("tdfail3").innerHTML = "";
            document.getElementById("lbllid").value = "";
            document.getElementById("lblloc").value = "";
            document.getElementById("tdloc3").innerHTML = "";

            document.getElementById("lbleq").value = "";
            document.getElementById("tdeq3").innerHTML = "";
            document.getElementById("lbleqid").value = "";

            document.getElementById("lblfuid").value = "";
            document.getElementById("lblfu").value = "";
            document.getElementById("tdfu3").innerHTML = "";

            document.getElementById("lblcomid").value = "";
            document.getElementById("lblcomp").value = "";
            document.getElementById("tdco3").innerHTML = "";

            document.getElementById("lblncid").value = "";
            document.getElementById("lblnc").value = "";
            document.getElementById("tdmisc3").innerHTML = "";

            document.getElementById("trlocs3").className = "details";

            document.getElementById("lblisdown").value = "";
            document.getElementById("lblisdownp").value = "";

        }
        function getlocs() {
            var wt = document.getElementById("lblwt").value;
            if (wt != "PM" && wt != "TPM" && wt != "MAXPM") {
                var stat = document.getElementById("lblstat").value;
                var sid = document.getElementById("lblsid").value;
                var wo = document.getElementById("lblwonum").value;
                if (stat != "COMP" && stat != "CAN") {
                    var eReturn = window.showModalDialog("../locs/locgetdialog.aspx?typ=wo&sid=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
                    if (eReturn) {
                        clearall();
                        //alert(eReturn)
                        document.getElementById("tdfail3").innerHTML = "";
                        var ret = eReturn.split("~");
                        document.getElementById("lbllid").value = ret[0];
                        document.getElementById("lblloc").value = ret[1];
                        document.getElementById("tdloc2").innerHTML = ret[1];

                        document.getElementById("lbleq").value = ret[2];
                        document.getElementById("tdeq2").innerHTML = ret[2];
                        document.getElementById("lbleqid").value = ret[3];

                        document.getElementById("trlocs").className = "view";
                        document.getElementById("lbltyp").value = "mod";
                        //var lid = ret[5];
                        //document.getElementById("lblpchk").value = "loc"
                        //document.getElementById("form1").submit();

                    }
                }
            }
        }
        function getlocs1() {
            var wt = document.getElementById("lblwt").value;
            if (wt != "PM" && wt != "TPM" && wt != "MAXPM") {
                var stat = document.getElementById("lblstat").value;
                var sid = document.getElementById("lblsid").value;
                var wo = document.getElementById("lblwonum").value;
                var lid = document.getElementById("lbllid").value;
                var typ = "wo"
                //alert(lid)
                if (lid != "") {
                    typ = "woret"
                }
                var eqid = document.getElementById("lbleqid").value;
                var fuid = document.getElementById("lblfuid").value;
                var coid = document.getElementById("lblcomid").value;
                if (stat != "COMP" && stat != "CAN") {
                    //alert("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid)
                    var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
                    if (eReturn) {
                        clearall();
                        //alert(eReturn)
                        //ret = lidi + "~" + lid + "~" + loc + "~" + eq + "~" eqnum + "~" + fu + "~" + func + "~" + co + "~" + comp;
                        var ret = eReturn.split("~");
                        document.getElementById("tdfail3").innerHTML = "";
                        document.getElementById("lbllid").value = ret[0];
                        document.getElementById("lblloc").value = ret[1];
                        document.getElementById("tdloc3").innerHTML = ret[2];

                        document.getElementById("lbleq").value = ret[4];
                        document.getElementById("tdeq3").innerHTML = ret[4];
                        document.getElementById("lbleqid").value = ret[3];

                        document.getElementById("lblfuid").value = ret[5];
                        document.getElementById("lblfu").value = ret[6];
                        document.getElementById("tdfu3").innerHTML = ret[6];

                        document.getElementById("lblcomid").value = ret[7];
                        document.getElementById("lblcomp").value = ret[8];
                        document.getElementById("tdco3").innerHTML = ret[8];
                        var hasmisc = ret[10];
                        if (hasmisc != "") {
                            document.getElementById("lblncid").value = ret[10];
                            document.getElementById("lblnc").value = ret[11];
                            document.getElementById("tdmisc3").innerHTML = ret[11];
                        }
                        else {
                            document.getElementById("trmisc3").className = "details";
                        }


                        document.getElementById("trlocs3").className = "view";
                        document.getElementById("lbltyp").value = "locs";
                        var coi = document.getElementById("lbliscol").value;
                        if (coi == "yes") {
                            document.getElementById("imgch").className = "details";
                            document.getElementById("Label5").innerHTML = "Column";
                            document.getElementById("tdcharge3").innerHTML = ret[12];
                        }
                        document.getElementById("tdeqdesc3").innerHTML = ret[13];
                        //var lid = ret[5];
                        //document.getElementById("lblpchk").value = "loc"
                        //document.getElementById("form1").submit();

                    }
                }
            }
        }
        function getwa(who) {
            var wo = document.getElementById("lblwonum").value;
            var sid = document.getElementById("lblsid").value;
            var stat = document.getElementById("lblstat").value;
            var wt = document.getElementById("lblwt").value;
            if ((stat != "COMP" && stat != "CAN")) {
                //&& wt != "PM" && wt != "TPM"
                var eReturn = window.showModalDialog("../labor/workareaselectdialog.aspx?typ=wo&sid=" + sid + "&wo=" + wo, "", "dialogHeight:500px; dialogWidth:820px; resizable=yes");
                if (eReturn) {
                    if (eReturn != "") {
                        var ret = eReturn.split("~~")
                        document.getElementById("lblwaid").value = ret[0];
                        document.getElementById("lblworkarea").value = ret[1];
                        if (who == "d") {
                            document.getElementById("tdwa").innerHTML = ret[1];
                        }
                        else {
                            document.getElementById("tdwa1").innerHTML = ret[1];
                        }
                        //document.getElementById("lblsubmit").value = "getweeks";
                        //document.getElementById("form1").submit();	
                    }
                }
            }
        }
        function checkdown(who) {
            var wt = document.getElementById("lblwt").value;
            // && wt != "PM" && wt != "TPM"
            if ((stat != "COMP" && stat != "CAN")) {
                if (who == "d") {
                    var cbd = document.getElementById("cbisdown");
                }
                else {
                    var cbd = document.getElementById("cbisdown3");
                }
                var wo = document.getElementById("lblwonum").value;
                var eqid = document.getElementById("lbleqid").value;
                var usr = document.getElementById("lbluser").value;
                var isdown = document.getElementById("lblisdown").value;
                var isdownp = document.getElementById("lblisdownp").value;
                var stat = document.getElementById("lblstat").value;
                var pmid = document.getElementById("lblpmid").value;
                var pmhid = document.getElementById("lblpmhid").value;
                //alert(isdown + "," + isdownp) 
                var eReturn = window.showModalDialog("woexpddialog.aspx?wo=" + wo + "&eqid=" + eqid + "&usr=" + usr + "&isdown=" + isdown + "&stat=" + stat + "&wt=" + wt + "&isdownp=" + isdownp + "&pmid=" + pmid + "&pmhid=" + pmhid, "", "dialogHeight:320px; dialogWidth:640px; resizable=yes");
                if (eReturn) {
                    var ret = eReturn;
                    var reta = ret.split(",");
                    var d = reta[0];
                    var dp = reta[1];
                    document.getElementById("lblisdown").value = reta[0];
                    document.getElementById("lblisdownp").value = reta[1];

                    if (d == "1" || dp == "1") {
                        cbd.checked = true;
                        if (who == "d") {
                            document.getElementById("tddown").className = "redlabel";
                            document.getElementById("tdeq").className = "plainlabelred";
                            document.getElementById("tdeqlbl").className = "redlabel";
                            document.getElementById("tdeqdesc").className = "plainlabelred";
                        }
                        else {
                            document.getElementById("tdisdown3").className = "redlabel";
                            document.getElementById("tdeq3").className = "plainlabelred";
                            document.getElementById("tdeq3lbl").className = "redlabel";
                            document.getElementById("tdeqdesc3").className = "plainlabelred";
                        }
                    }
                    else {
                        cbd.checked = false;
                        if (who == "d") {
                            document.getElementById("tddown").className = "label";
                            document.getElementById("tdeq").className = "plainlabel";
                            document.getElementById("tdeqlbl").className = "label";
                            document.getElementById("tdeqdesc").className = "plainlabel";
                        }
                        else {
                            document.getElementById("tdisdown3").className = "label";
                            document.getElementById("tdeq3").className = "plainlabel";
                            document.getElementById("tdeq3lbl").className = "label";
                            document.getElementById("tdeqdesc3").className = "plainlabel";
                        }
                    }
                }
                else {

                    wo = document.getElementById("lblwonum").value;
                    sid = document.getElementById("lblsid").value;

                    window.location = "womanfsns.aspx?jump=yes&wo=" + wo + "&sid=" + sid + "&usrname=" + usr;
                }

            }
        }
        function refreshit() {
            //"woman3.aspx?typ=" & typ & "&sid=" & sid & "&wo=" & wo
            sid = document.getElementById("lblsid").value;
            var usr = document.getElementById("lbluser").value;
            window.location = "woman3.aspx?typ=&wo=&sid=" + sid + "&usrname=" + usr;
        }
        function getsk() {
            wo = document.getElementById("lblwonum").value;
            sid = document.getElementById("lblsid").value;
            var stat = document.getElementById("lblstat").value;
            var wt = document.getElementById("lblwt").value;
            if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
                var eReturn = window.showModalDialog("../labor/labskillsdialog.aspx?who=wo&sid=" + sid + "&wo=" + wo, "", "dialogHeight:500px; dialogWidth:360px; resizable=yes");
                if (eReturn) {
                    if (eReturn != "") {
                        var ret = eReturn.split("~")

                        //document.getElementById(who).value = "yes";
                        document.getElementById("tdskill").innerHTML = ret[1];
                        document.getElementById("lblskill").value = ret[1];
                        document.getElementById("lblskillid").value = ret[0];

                    }
                }
            }
        }
        function getstat() {
            var stat = document.getElementById("lblstat").value;
            var ro = document.getElementById("lblro").value;
            var wt = document.getElementById("lblwt").value;
            var wo = document.getElementById("lblwonum").value;
            if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
                window.parent.setref();
                var eReturn = window.showModalDialog("wostatusdialog.aspx?typ=stat&wo=" + wo, "", "dialogHeight:370px; dialogWidth:370px; resizable=yes");
                if (eReturn) {
                    if (eReturn != "") {
                        document.getElementById("tdstat").innerHTML = eReturn;
                        document.getElementById("lblstat").value = eReturn;
                        if (ro != "1") {
                            if (eReturn == "COMP" || eReturn == "CAN") { //||eReturn=="CLOSE"
                                document.getElementById("lblsubmit").value = "checkstat";
                                document.getElementById("form1").submit();
                            }
                        }
                    }
                }
            }
        }
        function getwt() {
            var stat = document.getElementById("lblstat").value;
            var wt = document.getElementById("lblwt").value;
            var wo = document.getElementById("lblwonum").value;
            if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM" && wt != "MAXPM") {
                window.parent.setref();
                var eReturn = window.showModalDialog("wostatusdialog.aspx?typ=wt&wo=" + wo, "", "dialogHeight:370px; dialogWidth:370px; resizable=yes");
                if (eReturn) {
                    if (eReturn != "") {
                        document.getElementById("tdwt").innerHTML = eReturn;
                        document.getElementById("lblwt").value = eReturn;
                        document.getElementById("lblsubmit").value = "go3";
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        function getcomp() {
            var stat = document.getElementById("lblstat").value;
            var wt = document.getElementById("lblwt").value;
            var wo = document.getElementById("lblwonum").value;
            if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
                window.parent.setref();
                var eReturn = window.showModalDialog("../admin/admindialog.aspx?list=cont&typ=wo&wo=" + wo, "", "dialogHeight:500px; dialogWidth:640px; resizable=yes");
                if (eReturn) {
                    if (eReturn != "") {
                        document.getElementById("tdcontract").innerHTML = eReturn;
                    }
                }
            }
        }
        function getleadsched() {
            var stat = document.getElementById("lblstat").value;
            var wo = document.getElementById("lblwonum").value;
            var sid = document.getElementById("lblsid").value;
            var wt = document.getElementById("lblwt").value;
            var issched = document.getElementById("lblissched").value;
            var typ;
            if (wt == "PM") {
                typ = "pm";
            }
            else {
                typ = "wo";
            }
            //typ = wo;
            //&& wt != "PM"
            //alert(wo + "," + issched)
            //&& issched != "1"
            if (wo != "") {
                if ((stat != "COMP" && stat != "CAN") && wt != "TPM") {
                    //alert("womlabordialog.aspx?typ=wo&sid=" + sid + "&wo=" + wo + "&issched=" + issched)
                    var eReturn = window.showModalDialog("womlabordialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&issched=" + issched, "", "dialogHeight:520px; dialogWidth:820px; resizable=yes");
                    if (eReturn) {
                        document.getElementById("lblsubmit").value = "refresh";
                        document.getElementById("form1").submit();
                    }
                }
            }
            else {
                alert("A Work Order Number is Required to Add Labor in Scheduling Mode")
            }
        }
        function getsuper(typ) {
            var sched = document.getElementById("lblusesched").value;
            var issched = document.getElementById("lblissched").value;
            var stat = document.getElementById("lblstat").value;
            var wt = document.getElementById("lblwt").value;
            var wo = document.getElementById("lblwonum").value;
            //&& wt != "PM"
            if ((stat != "COMP" && stat != "CAN") && wt != "TPM") {
                //&&sched=="yes"&&issched=="1"
                if (typ == "lead") {
                    getleadsched();
                }
                else {
                    var skill = document.getElementById("lblskillid").value;
                    var sid = document.getElementById("lblsid").value;
                    var ro = document.getElementById("lblro").value;
                    var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&wo=" + wo + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
                    if (eReturn) {
                        if (eReturn != "") {
                            var retarr = eReturn.split(",")
                            if (typ == "sup") {
                                document.getElementById("lblsup").value = retarr[0];
                                document.getElementById("tdsup").innerHTML = retarr[1];
                                //alert(document.getElementById("lblsup").value)
                            }
                            else {
                                //alert(retarr[0])
                                document.getElementById("lbllead").value = retarr[0];
                                document.getElementById("tdlead").innerHTML = retarr[1];
                            }
                        }
                    }
                }
            }
        }
        function GetToolDiv() {
            var wonum = document.getElementById("lblwonum").value;
            if (wonum != "") {
                var stat = document.getElementById("lblstat").value;
                var wt = document.getElementById("lblwt").value;
                var ro = document.getElementById("lblro").value;
                if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
                    cid = document.getElementById("lblcid").value;
                    ptid = "wo"
                    wo = document.getElementById("lblwonum").value;
                    if (ptid != "") {
                        window.parent.setref();
                        window.open("../inv/devTaskToolList.aspx?typ=wo&ptid=" + ptid + "&cid=" + cid + "&wo=" + wo + "&ro=" + ro, "repWin", popwin);
                    }
                }
                else {
                    if ((stat != "COMP" || stat != "CAN") && wt == "PM") {
                        alert("Can't add Tools to a PM Work Order in the Work Manager")
                    }
                }
            }
        }
        function GetPartDiv() {
            var wonum = document.getElementById("lblwonum").value;
            if (wonum != "") {
                var stat = document.getElementById("lblstat").value;
                var wt = document.getElementById("lblwt").value;
                var ro = document.getElementById("lblro").value;
                if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
                    cid = document.getElementById("lblcid").value;
                    ptid = "wo"
                    wo = document.getElementById("lblwonum").value;
                    if (ptid != "") {
                        window.parent.setref();
                        window.open("../inv/devTaskPartList.aspx?typ=wo&ptid=" + ptid + "&cid=" + cid + "&wo=" + wo + "&ro=" + ro, "repWin", popwin);
                    }
                }
                else {
                    if ((stat != "COMP" || stat != "CAN") && wt == "PM") {
                        alert("Can't add Parts to a PM Work Order in the Work Manager")
                    }
                }
            }
        }
        function GetSpareDiv() {
            var wonum = document.getElementById("lblwonum").value;
            if (wonum != "") {
                var stat = document.getElementById("lblstat").value;
                var wt = document.getElementById("lblwt").value;
                if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
                    eqid = document.getElementById("lbleqid").value;
                    cid = document.getElementById("lblcid").value;
                    var ro = document.getElementById("lblro").value;
                    ptid = "wo"
                    wo = document.getElementById("lblwonum").value;
                    if (eqid != "") {
                        window.parent.setref();
                        window.open("../inv/sparepartpick.aspx?typ=wo&eqid=" + eqid + "&oflag=r&ptid=" + ptid + "&cid=" + cid + "&wo=" + wo + "&ro=" + ro, "repWin", popwin);
                    }
                    else {
                        alert("No Equipment Record Selected For This Work Order")
                    }
                }
                else {
                    if ((stat != "COMP" || stat != "CAN") && wt == "PM") {
                        alert("Can't add Spare Parts to a PM Work Order in the Work Manager")
                    }
                }
            }
        }
        function GetLubeDiv() {
            var wonum = document.getElementById("lblwonum").value;
            if (wonum != "") {
                var stat = document.getElementById("lblstat").value;
                var wt = document.getElementById("lblwt").value;
                var ro = document.getElementById("lblro").value;
                if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
                    cid = document.getElementById("lblcid").value;
                    ptid = "wo"
                    wo = document.getElementById("lblwonum").value;
                    if (ptid != "") {
                        window.parent.setref();
                        window.open("../inv/devTaskLubeList.aspx?typ=wo&ptid=" + ptid + "&cid=" + cid + "&wo=" + wo + "&ro=" + ro, "repWin", popwin);
                    }
                }
                else {
                    if ((stat != "COMP" || stat != "CAN") && wt == "PM") {
                        alert("Can't add Lubes to a PM Work Order in the Work Manager")
                    }
                }
            }
        }
        function getcal(fld) {
            var stat = document.getElementById("lblstat").value;
            var wt = document.getElementById("lblwt").value;
            var wo = document.getElementById("lblwonum").value;
            var fldret = "txt" + fld;
            var chk;
            var cdt = "";
            var sdt = "";
            var dtyp = "0";
            if (fld == "tstart") {
                chk = document.getElementById("tdtcomp").innerHTML;
                cdt = chk;
                sdt = document.getElementById("tdtstart").innerHTML;
                dtyp = "t";
            }
            if (fld == "sstart") {
                chk = document.getElementById("tdscomp").innerHTML;
                cdt = chk;
                sdt = document.getElementById("tdsstart").innerHTML;
                dtyp = "s";
            }
            if (fld == "astart") {
                chk = document.getElementById("tdacomp").innerHTML;
                cdt = chk;
                sdt = document.getElementById("tdastart").innerHTML;
                dtyp = "a";
            }
            if (fld == "tcomp") {
                chk = document.getElementById("tdtstart").innerHTML;
                cdt = chk;
                sdt = document.getElementById("tdtcomp").innerHTML;
                dtyp = "t";
            }
            if (fld == "scomp") {
                chk = document.getElementById("tdsstart").innerHTML;
                cdt = chk;
                sdt = document.getElementById("tdscomp").innerHTML;
                dtyp = "s";
            }
            if (fld == "acomp") {
                chk = document.getElementById("tdastart").innerHTML;
                cdt = chk;
                sdt = document.getElementById("tdacomp").innerHTML;
                dtyp = "a";
            }
            if (chk == "") {
                chk = "yes";
            }
            else {
                chk = "no";
            }
            if (stat != "COMP" && stat != "CAN" && wt != "PM" && wt != "TPM") {
                window.parent.setref();
                var eReturn = window.showModalDialog("../controls/caldialog2.aspx?typ=wo&who=" + fld + "&chk=" + chk + "&wo=" + wo + "&cdt=" + cdt + "&sdt=" + sdt + "&dtyp=" + dtyp, "", "dialogHeight:425px; dialogWidth:425px; resizable=yes");
                if (eReturn) {
                    var fldret = "td" + fld;
                    var retarr = eReturn.split(",");

                    document.getElementById(fldret).innerHTML = retarr[0];
                    var retchk = retarr[1];
                    if (fld == "tstart" && retchk == "yes") {
                        document.getElementById("tdtcomp").innerHTML = retarr[0];
                    }
                    if (fld == "sstart" && retchk == "yes") {
                        document.getElementById("tdscomp").innerHTML = retarr[0];
                    }
                    if (fld == "astart" && retchk == "yes") {
                        document.getElementById("tdacomp").innerHTML = retarr[0];
                        document.getElementById("lblacomp").value = retarr[0];
                    }

                    if (fld == "tcomp" && retchk == "yes") {
                        document.getElementById("tdtstart").innerHTML = retarr[0];
                    }
                    if (fld == "scomp" && retchk == "yes") {
                        document.getElementById("tdsstart").innerHTML = retarr[0];
                    }
                    if (fld == "acomp" && retchk == "yes") {
                        document.getElementById("tdastart").innerHTML = retarr[0];
                    }

                }
            }
        }
        function getwosched(typ) {
            var stat = document.getElementById("lblstat").value;
            var jpid = document.getElementById("lbljpid").value;
            var wonum = document.getElementById("lblwonum").value;
            var skill = document.getElementById("lblskill").value;
            var skillid = document.getElementById("lblskillid").value;
            var sdays = document.getElementById("lblsdays").value;
            var pmid = document.getElementById("lblpmid").value;
            var eqid = document.getElementById("lbleqid").value;
            var ro = document.getElementById("lblro").value;
            var eReturn
            var wt = document.getElementById("lblwt").value;
            if (wt != "PM" && wt != "TPM") {
                if (typ == "jp") {
                    window.parent.setref();
                    eReturn = window.showModalDialog("woscheddialog.aspx?typ=" + typ + "&stat=" + stat + "&jpid=" + jpid + "&wo=" + wonum + "&ro=" + ro, "", "dialogHeight:500px; dialogWidth:660px; resizable=yes");
                }
                else {
                    if (pmid == "") {
                        eReturn = window.showModalDialog("woscheddialog2.aspx?typ=" + typ + "&wo=" + wonum + "&jpid=" + jpid + "&stat=" + stat + "&skill=" + skill + "&skillid=" + skillid + "&sdays=" + sdays + "&ro=" + ro, "", "dialogHeight:370px; dialogWidth:620px; resizable=yes");
                    }
                    else {
                        eReturn = window.showModalDialog("../appsman/pmtaskscheddialog.aspx?pmid=" + pmid + "&eqid=" + eqid + "&ro=" + ro, "", "dialogHeight:550px; dialogWidth:750px; resizable=yes");
                    }
                }
                if (eReturn) {
                    document.getElementById("lblsubmit").value = "get";
                    document.getElementById("form1").submit();
                }
            }
        }
        function proj() {
            var stat = document.getElementById("lblstat").value;
            if (stat != "COMP" && stat != "CAN") {
                window.parent.handlesched();
            }
        }
        function getMeasDiv() {
            var wonum = document.getElementById("lblwonum").value;
            if (wonum != "") {
                var stat = document.getElementById("lblstat").value;
                var wt = document.getElementById("lblwt").value;
                var ro = document.getElementById("lblro").value;
                if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
                    window.parent.setref();
                    var valu = document.getElementById("lblwonum").value;
                    var eReturn = window.showModalDialog("../utils/tmdialog.aspx?typ=wo&wo=" + valu + "&ro=" + ro, "", "dialogHeight:650px; dialogWidth:550px; resizable=yes");
                    if (eReturn) {
                        //document.getElementById("form1").submit()
                    }
                }
                else {
                    if ((stat != "COMP" || stat != "CAN") && wt == "PM") {
                        alert("Can't add Measurements to a PM Work Order in the Work Manager")
                    }
                }
            }
        }
        function getwo() {

            var ro = document.getElementById("lblro").value;
            if (ro != "1") {
                document.getElementById("lblsubmit").value = "new"
                document.getElementById("form1").submit();
            }
        }
        function dupwo() {
            var wonum = document.getElementById("lblwonum").value;
            if (wonum != "") {
                var ro = document.getElementById("lblro").value;
                if (ro != "1") {
                    var chk = document.getElementById("lblwonum").value;
                    var wt = document.getElementById("lblwt").value;
                    if (chk != "" && wt != "PM" && wt != "TPM" && wt != "MAXPM") {
                        document.getElementById("lblsubmit").value = "dupwo";
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        function checkvals() {
            var wonum = document.getElementById("lblwonum").value;

            //alert(wonum)
            if (wonum != "") {
                var stat = document.getElementById("lblstat").value;
                var wt = document.getElementById("lblwt").value;
                if ((stat != "COMP" && stat != "CANCEL")) {

                    var ldt = document.getElementById("txtleadtime").value;
                    var est = document.getElementById("txtestlh").value;
                    var iscol = document.getElementById("lbliscol").value;
                    var gof = 0;
                    //alert(ldt + "," + est + "," + iscol)
                    if (ldt != "" && ldt != "0") {
                        if (isNaN(ldt)) {
                            alert("Email Lead Time Must be a Numeric Value")
                            gof = 1;
                        }
                    }
                    else if (est != "" && est != "0") {
                        if (isNaN(est)) {
                            alert("Estimated Labor Hours Must be a Numeric Value")
                            gof = 1;
                        }
                    }
                    else if (iscol == "yes") {
                        //alert(wt)
                        var sup = document.getElementById("lblsup").value;
                        var pri = document.getElementById("ddpri").value;
                        //alert(pri)
                        if (sup == "") {
                            gof = 1;
                            alert("Supervisor Required")
                        }
                        else if (wt == "") {
                            gof = 1;
                            alert("Work Type Required")
                        }
                        else if (pri == "" || pri == "0") {
                            gof = 1;
                            alert("Priority Required")
                        }

                    }
                    if (gof == 0) {
                        document.getElementById("lblsubmit").value = "go";
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        function keychk(f, e) {

            var keycode;
            if (window.event) keycode = window.event.keyCode;
            else if (e) keycode = e.which;
            else return true;
            if (keycode == 8) {

                return false;
                alert();
            }
        }

        function checksave() {
            document.getElementById("lblchecksave").value = "yes";
            //Please Save Changes Before Exiting
            document.getElementById("tdsave").innerHTML = "Save Changes";
        }
        function dosave() {
            var chk = document.getElementById("lblchecksave").value;
            if (chk == "yes") {
                var timer = window.setTimeout("impsave();", 3000);
                document.getElementById("lblchecksave").value = "";
            }
        }

        function impsave() {
            var cbs = document.getElementById("cbsupe");
            var cbl = document.getElementById("cbleade");
            var cbsv;
            var cblv;
            if (cbs.checked == true) {
                cbsv = "1";
            }
            else {
                cbsv = "0";
            }
            if (cbl.checked == true) {
                cblv = "1";
            }
            else {
                cblv = "0";
            }
            var elead = document.getElementById("txtleadtime").value;
            var olead = document.getElementById("lbloldelead").value;

            var wt = document.getElementById("lblwt").value;

            var wonum = document.getElementById("lblwonum").value;
            var wodesc = document.getElementById("txtwodesc").value;
            wodesc = wodesc.replace("#", "%23")
            var estw = document.getElementById("txtestlh").value;
            var estj = document.getElementById("txtestjp").value;
            var estmp = document.getElementById("txtestmp").value;
            var pri = document.getElementById("ddpri").value;
            /*
            var eReturn = window.showModalDialog("wosave.aspx?wo=" + wonum + "&wodesc=" + wodesc + "&estw=" + estw + "&estj=" + estj + "&pri=" + pri + "&estmp=" + estmp + "&cbsv=" + cbsv + "&cblv=" + cblv + "&elead=" + elead + "&olead=" + olead + "&wt=" + wt, "", "dialogHeight:30px; dialogWidth:120px; resizable=no");
            if (eReturn) {
            var err = eReturn;
            if (err == "ok") {
            document.getElementById("tdsave").innerHTML = "";
            document.getElementById("lblchecksave").value = "";
            //document.getElementById("tdsave").innerHTML="Please Save Changes Before Exiting";
            }
            else {
            document.getElementById("lblchecksave").value = "";
            }
            }
            */
            document.getElementById("ifsave").src = "wosave.aspx?wo=" + wonum + "&wodesc=" + wodesc + "&estw=" + estw + "&estj=" + estj + "&pri=" + pri + "&estmp=" + estmp + "&cbsv=" + cbsv + "&cblv=" + cblv + "&elead=" + elead + "&olead=" + olead + "&wt=" + wt;
            //document.getElementById("lblchecksave").value = "";
        }
        function handleifsave(err) {
            if (err == "ok") {
                document.getElementById("tdsave").innerHTML = "";
                document.getElementById("lblchecksave").value = "";
                //document.getElementById("tdsave").innerHTML="Please Save Changes Before Exiting";
            }
            else {
                document.getElementById("lblchecksave").value = "";
            }
        }
        function checkcomp() {
            var ro = document.getElementById("lblro").value;
            var wt = document.getElementById("lblwt").value;
            //if(wt!="PM"&&wt!="TPM") {
            //alert(wt)
            var wonum = document.getElementById("lblwonum").value;
            if (wonum != "") {
                if (ro != "1") {
                    var cchk = document.getElementById("lblchecksave").value;
                    if (cchk == "yes") {
                        var conf = confirm("You have made changes to Work Order Details without Saving Changes.\nAre you sure you want to Continue?")
                        if (conf == true) {
                            var stat = document.getElementById("lblstat").value
                            if (stat != "WAPPR" && stat != "COMP" && stat != "WAPPR") {
                                //child here
                                if (wt == "PM") {
                                    document.getElementById("lblsubmit").value = "checkpmcomp";
                                    document.getElementById("form1").submit();
                                }
                                else if (wt == "TPM") {
                                    document.getElementById("lblsubmit").value = "checktpmcomp";
                                    document.getElementById("form1").submit();
                                }
                                else if (wt == "MAXPM") {
                                    document.getElementById("lblsubmit").value = "checkpmjpcomp";
                                    document.getElementById("form1").submit();
                                }
                                else {
                                    document.getElementById("lblsubmit").value = "checkcomp";
                                    document.getElementById("form1").submit();
                                }
                            }
                            else {
                                if (stat == "WAPPR") {
                                    alert("This Work Order is Waiting for Approval")
                                }
                                else if (stat == "COMP") {
                                    alert("This Work Order has Already been Completed")
                                }
                                else if (stat == "CAN") {
                                    alert("This Work Order has been Cancelled")
                                }
                            }
                        }
                        else {
                            alert("Action Cancelled")
                        }
                    }
                    else {
                        var stat = document.getElementById("lblstat").value
                        if (stat != "WAPPR" && stat != "COMP" && stat != "WAPPR") {
                            //child here
                            if (wt == "PM") {
                                document.getElementById("lblsubmit").value = "checkpmcomp";
                                document.getElementById("form1").submit();
                            }
                            else if (wt == "TPM") {
                                document.getElementById("lblsubmit").value = "checktpmcomp";
                                document.getElementById("form1").submit();
                            }
                            else if (wt == "MAXPM") {
                                document.getElementById("lblsubmit").value = "checkpmjpcomp";
                                document.getElementById("form1").submit();
                            }
                            else {
                                document.getElementById("lblsubmit").value = "checkcomp";
                                document.getElementById("form1").submit();
                            }
                        }
                        else {
                            if (stat == "WAPPR") {
                                alert("This Work Order is Waiting for Approval")
                            }
                            else if (stat == "COMP") {
                                alert("This Work Order has Already been Completed")
                            }
                            else if (stat == "CAN") {
                                alert("This Work Order has been Cancelled")
                            }
                        }
                    }


                }
                //}
                //else {

                //}
            }
        }
        var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
        function printwo() {
            var stat = document.getElementById("lblstat").value;
            var wo = document.getElementById("lblwonum").value;
            if (wo != "") {
                var pm = document.getElementById("lblpmid").value;
                var jp = document.getElementById("lbljpid").value;
                var typ = document.getElementById("lblwt").value;
                //alert(typ)
                window.parent.setref();
                if (typ == "PM") {
                    getdocs();
                    getroute();
                }
                if (jp != "" && typ != "MAXPM") {
                    typ = "JP"
                }
                if (stat == "COMP" || stat == "CLOSE") {
                    window.open("awoprint.aspx?&wonum=" + wo, "repWin", popwin);
                }
                else {
                    window.open("woprint.aspx?typ=" + typ + "&pm=" + pm + "&wo=" + wo + "&jp=" + jp, "repWin", popwin);
                }
            }
        }
        function getroute() {
            var popwin = "directories=0,height=520,width=820,location=1,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
            var rtid = document.getElementById("lblrtid").value;
            //alert(rtid)
            var typ = "RBASM3";
            var pm = document.getElementById("lblpmid").value;
            if (rtid != "") {
                window.open("../reports/pmrhtml7_new.aspx?rid=" + rtid + "&typ=" + typ + "&pmid=" + pm, "repWin2", popwin);
            }
        }
        function getdocs() {
            var cbs = document.getElementById("lbldocs").value;
            var cbsarr = cbs.split(";")
            for (var i = 0; i < cbsarr.length; i++) {
                var cbstr = cbsarr[i];
                //alert(cbstr)
                if (cbstr != "") {
                    var cbstrarr = cbstr.split("~")
                    var fnstr = cbstrarr[1]
                    var docstr = cbstrarr[0]
                    OpenFile(fnstr, docstr);
                }
            }
        }
        function resetlead() {
            var wt = document.getElementById("lblwt").value;
            if (wt != "PM" && wt != "TPM") {
                //document.getElementById("lbllead").value="";
                //document.getElementById("txtlead").value="";
                document.getElementById("lblsubmit").value = "go1";
                document.getElementById("form1").submit();
            }
        }
        function resetsuper() {
            var wt = document.getElementById("lblwt").value;
            if (wt != "PM" && wt != "TPM") {
                //document.getElementById("lblsup").value="";
                //document.getElementById("txtsup").value="";
                document.getElementById("lblsubmit").value = "go2";
                document.getElementById("form1").submit();
            }
        }
        function checkstart() {
            var who = document.getElementById("lblwho").value;
            if (who != "") {
                window.parent.pageScroll();
            }
            var chk = document.getElementById("lblwonum").value;
            if (chk == "") {
                FreezeScreen('No Work Order Selected');
            }
            var wt = document.getElementById("lblwt").value;
            var malert = document.getElementById("lblmalert").value;
            if (malert != "") {
                compalert(malert);
            }
            var newwo = document.getElementById("lblsubmit").value;
            if (newwo == "upwo") {
                document.getElementById("lblsubmit").value = "";
                var who = document.getElementById("lblwho").value;
                //if (who == "") {
                window.parent.handlewo(chk);
                //}
            }
        }

        function compalert(malert) {
            var coi = document.getElementById("lblcoi").value;
            var wt = document.getElementById("lblwt").value;
            var marr = malert.split(",");
            var mmsg;
            var hasflg = 0;
            mmsg = "This Work Order has ";
            for (i = 0; i < marr.length; i++) {
                if (marr[i] == "m") {
                    if (mmsg == "This Work Order has ") {
                        mmsg = mmsg + "Measurements";
                    }
                    else {
                        mmsg = mmsg + ", Measurements";
                    }
                }
                else if (marr[i] == "h") {
                    if (mmsg == "This Work Order has ") {
                        mmsg = mmsg + "Task Times";
                    }
                    else {
                        mmsg = mmsg + ", Task Times";
                    }
                }
                else if (marr[i] == "l") {
                    if (mmsg == "This Work Order has ") {
                        mmsg = mmsg + "Labor Times";
                    }
                    else {
                        mmsg = mmsg + ", Labor Times";
                    }
                }
                else if (marr[i] == "d") {
                    if (mmsg == "This Work Order has ") {
                        mmsg = mmsg + "Down Times";
                    }
                    else {
                        mmsg = mmsg + ", Down Times";
                    }
                }
            }
            mmsg += " that have not been recorded\nDo you wish to Continue?";
            var conf = confirm(mmsg)
            if (conf == true) {
                //window.parent.setref();
                document.getElementById("lblmalert").value = "ok";
                if (wt != "PM" && wt != "TPM" && wt != "MAXPM") {
                    document.getElementById("lblsubmit").value = "checkcomp"
                }
                else if (wt == "PM") {
                    document.getElementById("lblsubmit").value = "checkpmcomp"
                }
                else if (wt == "TPM") {
                    document.getElementById("lblsubmit").value = "checktpmcomp"
                }
                else if (wt == "MAXPM") {
                    document.getElementById("lblsubmit").value = "checkpmjpcomp";
                }
                document.getElementById("form1").submit();
            }
            else {
                document.getElementById("lblmalert").value = "";
                alert("Action Cancelled")
            }
        }


        function FreezeScreen(msg) {
            scroll(0, 0);
            var outerPane = document.getElementById('FreezePane');
            var innerPane = document.getElementById('InnerFreezePane');
            if (outerPane) outerPane.className = 'FreezePaneOn6';
            if (innerPane) innerPane.innerHTML = msg;

            var outerPane1 = document.getElementById('FreezePane1');
            var innerPane1 = document.getElementById('InnerFreezePane1');
            //if (outerPane1) outerPane1.className = 'FreezePaneOn5';
            //if (innerPane1) innerPane1.innerHTML = msg;
        }
        function getwot() {
            var stat = document.getElementById("lblstat").value;
            var wo = document.getElementById("lblwonum").value;
            var eqid = document.getElementById("lbleqid").value;
            var ro = document.getElementById("lblro").value;
            var wt = document.getElementById("lblwt").value;
            var pmid = document.getElementById("lblpmid").value;
            var tpmid = document.getElementById("lbltpmid").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluser").value;
            var wonum = document.getElementById("lblwonum").value;
            if (wonum != "") {
                if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
                    //window.parent.setref();
                    var eReturn = window.showModalDialog("woreportingdialog.aspx?sid=" + sid + "&stat=" + stat + "&wo=" + wo + "&ro=" + ro + "&eqid=" + eqid + "&uid=" + uid, "", "dialogHeight:650px; dialogWidth:850px; resizable=yes");
                    if (eReturn) {
                        //document.getElementById("form1").submit()
                    }
                }
                else if ((stat != "COMP" && stat != "CAN") && wt == "PM") {
                    var eReturn = window.showModalDialog("pmreportingdialog.aspx?sid=" + sid + "&pmid=" + pmid + "&stat=" + stat + "&wo=" + wo + "&ro=" + ro + "&eqid=" + eqid + "&uid=" + uid, "", "dialogHeight:650px; dialogWidth:1040px; resizable=yes");
                    if (eReturn) {
                        //document.getElementById("form1").submit()
                    }
                }
                else if ((stat != "COMP" && stat != "CAN") && wt == "TPM") {
                    var eReturn = window.showModalDialog("tpmreportingdialog.aspx?sid=" + sid + "&pmid=" + tpmid + "&stat=" + stat + "&wo=" + wo + "&ro=" + ro + "&eqid=" + eqid + "&uid=" + uid, "", "dialogHeight:650px; dialogWidth:1040px; resizable=yes");
                    if (eReturn) {
                        //document.getElementById("form1").submit()
                    }
                }
            }
        }
        function getjot() {
            var stat = document.getElementById("lblstat").value;
            var wo = document.getElementById("lblwonum").value;
            var ro = document.getElementById("lblro").value;
            var wt = document.getElementById("lblwt").value;
            var jpid = document.getElementById("lbljpid").value;
            var sid = document.getElementById("lblsid").value;
            var wonum = document.getElementById("lblwonum").value;
            if (wonum != "") {
                if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
                    //window.parent.setref();
                    var eReturn = window.showModalDialog("wojpreportingdialog.aspx?stat=" + stat + "&jpid=" + jpid + "&wo=" + wo + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:650px; dialogWidth:850px; resizable=yes");
                    if (eReturn) {
                        //document.getElementById("form1").submit()
                    }
                }
            }
        }
        function getcosts() {
            var wo = document.getElementById("lblwonum").value;
            var wt = document.getElementById("lblwt").value;
            var wonum = document.getElementById("lblwonum").value;
            if (wonum != "") {
                if (wt != "PM" && wt != "TPM") {
                    var eReturn = window.showModalDialog("wocostsdialog.aspx?who=wo&wo=" + wo, "", "dialogHeight:650px; dialogWidth:850px; resizable=yes");
                    if (eReturn) {
                        //document.getElementById("form1").submit()
                    }
                }
                else if (wt == "PM") {
                    var eReturn = window.showModalDialog("wocostsdialog.aspx?who=pm&wo=" + wo, "", "dialogHeight:650px; dialogWidth:850px; resizable=yes");
                    if (eReturn) {
                        //document.getElementById("form1").submit()
                    }
                }
                else if (wt == "TPM") {
                    var eReturn = window.showModalDialog("wocostsdialog.aspx?who=tpm&wo=" + wo, "", "dialogHeight:650px; dialogWidth:850px; resizable=yes");
                    if (eReturn) {
                        //document.getElementById("form1").submit()
                    }
                }
            }
        }
        function getcharge(typ, who) {
            var wo = document.getElementById("lblwonum").value;
            var eReturn = window.showModalDialog("../admin/ChargeDialog.aspx?who=wo&wo=" + wo, "", "dialogHeight:350px; dialogWidth:350px; resizable=yes");
            if (eReturn) {
                if (who == "1") {
                    document.getElementById("tdcharge1").innerHTML = eReturn;
                }
                else if (who == "2") {
                    document.getElementById("tdcharge2").innerHTML = eReturn;
                }
            }
        }
        function getjplans() {
            var stat = document.getElementById("lblstat").value;
            var wo = document.getElementById("lblwonum").value;
            if (stat != "COMP" && stat != "CAN") {
                var dis = document.getElementById("lbldis").value;
                //alert(dis)
                if (dis == "0" || dis == "") {
                    window.parent.setref();
                    var eReturn = window.showModalDialog("../appsrt/JobPlanDialog.aspx?jump=yes&typ=wo&wo=" + wo, "", "dialogHeight:510px; dialogWidth:750px; resizable=yes");
                    if (eReturn) {

                        var retarr = eReturn.split(",")
                        document.getElementById("lbljpid").value = retarr[0];
                        document.getElementById("tdplan").innerHTML = retarr[1];
                        if (retarr[0] != "") {
                            //document.getElementById("lblsubmit").value="addplan";
                            //document.getElementById("form1").submit();
                        }
                    }
                }
            }
        }
        function gotojp() {
            var stat = document.getElementById("lblstat").value;
            if (stat != "COMP" && stat != "CAN") {
                var dis = document.getElementById("lbldis").value;
                if (dis == "0" || dis == "") {
                    var jp = document.getElementById("lbljpid").value;
                    if (jp == "") {
                        jp = "0"
                    }
                    window.parent.parent.handlejp(jp);
                }
            }
        }
        function getwjplan() {
            var jpid = document.getElementById("lbljpid").value;
            if (jpid != "") {
                window.parent.handleswjplan(jpid);
            }
        }
        //getnotesfs();
        //-->
    </script>
    <style type="text/css">
        .thdrsinglft
        {
            /*cursor:hand;*/
            padding: 1px 1px 1px 1px;
            margin: 1em;
            text-indent: 3px;
            text-align: left;
            vertical-align: middle;
            border-top: solid #6b6c6c 1px;
            border-left: solid #6b6c6c 2px;
            border-bottom: solid #6b6c6c 1px;
            background-image: url(../images/appbuttons/minibuttons/gradient1.gif);
            background-repeat: repeat-x;
            background-repeat: repeat;
            background-position: top left;
        }
        .thdrsingrt
        {
            padding: 1px 1px 1px 1px;
            margin: 1em;
            text-indent: 3px;
            text-align: left;
            vertical-align: middle;
            border-top: solid #6b6c6c 1px;
            border-right: solid #6b6c6c 1px;
            border-bottom: solid #6b6c6c 1px;
            background-image: url(../images/appbuttons/minibuttons/gradient1.gif);
            background-repeat: repeat-x;
            background-repeat: repeat;
            background-position: top left;
        }
        .listbox
        {
            border-bottom: 1px solid black;
            border-top: 1px solid black;
            border-left: 1px solid black;
            border-right: 1px solid black;
            background-color: White;
            overflow: auto;
            width: 150px;
            height: 300px;
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
            list-style-type: none;
            padding: 3px;
            margin: 0px;
        }
        .listboxshort
        {
            border-bottom: 1px solid black;
            border-top: 1px solid black;
            border-left: 1px solid black;
            border-right: 1px solid black;
            background-color: White;
            overflow: auto;
            width: 150px;
            height: 80px;
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
            list-style-type: none;
            padding: 3px;
            margin: 0px;
        }
        .listitem
        {
            list-style-type: none;
            padding: 0px;
            margin: 0px;
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
        }
        .listhigh
        {
            cursor: pointer;
            background-color: #67BAF9;
            list-style-type: none;
            padding: 0px;
            margin: 0px;
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
        }
    </style>
</head>
<body onload="checkstart();">
    <form id="form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="eqauto.asmx" />
            <asp:ServiceReference Path="superauto.asmx" />
            <asp:ServiceReference Path="leadauto.asmx" />
        </Services>
    </asp:ScriptManager>
    <div id="FreezePane" class="FreezePaneOff" align="center">
        <div id="InnerFreezePane" class="InnerFreezePane">
        </div>
    </div>
    <div id="FreezePane1" class="FreezePaneOff" align="center">
        <div id="InnerFreezePane1" class="InnerFreezePane">
        </div>
    </div>
    <table style="position: absolute; top: 0px; left: 2px" cellspacing="0" cellpadding="0"
        width="1230" border="0">
        <tr>
            <td width="630" valign="top">
                <table cellspacing="1" cellpadding="0" width="630" border="0">
                    <tr>
                        <td colspan="3">
                            <table cellspacing="0" cellpadding="0" width="630" border="0">
                                <tr>
                                    <td class="blue label" width="100">
                                        <asp:Label ID="lang1346" runat="server">Work Order#</asp:Label>
                                    </td>
                                    <td width="80">
                                        <asp:TextBox ID="txtwo" runat="server" Width="80px" CssClass="plainlabel" ReadOnly="True"></asp:TextBox>
                                    </td>
                                    <td width="18">
                                        <img onmouseover="return overlib('Create New Work Order With Auto Number', BELOW, RIGHT)"
                                            onmouseout="return nd()" onclick="getwo();" src="../images/appbuttons/minibuttons/addnew.gif">
                                    </td>
                                    <td width="18">
                                        <img onmouseover="return overlib('Duplicate This Work Order', BELOW, RIGHT)" onmouseout="return nd()"
                                            onclick="dupwo();" src="../images/appbuttons/minibuttons/copybg.gif">
                                    </td>
                                    <td width="18">
                                        <img onmouseover="return overlib('Refresh Screen to Create New Work Order', BELOW, RIGHT)"
                                            onmouseout="return nd()" onclick="refreshit();" src="../images/appbuttons/minibuttons/switch2.gif">
                                    </td>
                                    <td width="2">
                                        <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" width="3px" />
                                    </td>
                                    <td id="tdsave" class="redlabel" width="110">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" width="3px" />
                                    </td>
                                    <td width="120">
                                        <img onmouseover="return overlib('Choose Equipment Spare Parts for this Task', BELOW, RIGHT)"
                                            onmouseout="return nd()" onclick="GetSpareDiv();" src="../images/appbuttons/minibuttons/spareparts.gif"><img
                                                id="img1" onmouseover="return overlib('Add/Edit Parts for this Task', BELOW, RIGHT)"
                                                onmouseout="return nd()" onclick="GetPartDiv();" alt="" src="../images/appbuttons/minibuttons/parttrans.gif"
                                                runat="server"><img onmouseover="return overlib('Add/Edit Tools for this WO', BELOW, RIGHT)"
                                                    onmouseout="return nd()" onclick="GetToolDiv();" alt="" src="../images/appbuttons/minibuttons/tooltrans.gif"><img
                                                        onmouseover="return overlib('Add/Edit Lubricants for this WO', BELOW, RIGHT)"
                                                        onmouseout="return nd()" onclick="GetLubeDiv();" alt="" src="../images/appbuttons/minibuttons/lubetrans.gif"><img
                                                            onmouseover="return overlib('Add/Edit Measurements for this WO', BELOW, RIGHT)"
                                                            onmouseout="return nd()" onclick="getMeasDiv();" alt="" src="../images/appbuttons/minibuttons/measure.gif">
                                    </td>
                                    <td>
                                        <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" width="3px" />
                                    </td>
                                    <td width="18">
                                        <img id="imgsav" onclick="checkvals();" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                            runat="server">
                                    </td>
                                    <td width="18">
                                        <img onmouseover="return overlib('Print Current Work Order', BELOW, RIGHT)" onmouseout="return nd()"
                                            onclick="printwo();" src="../images/appbuttons/minibuttons/printex.gif">
                                    </td>
                                    <td>
                                        <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" width="3px" />
                                    </td>
                                    <td width="80" align="right">
                                        <img id="Img2" onmouseover="return overlib('Work Order Time and Materials Reporting', BELOW, RIGHT)"
                                            onmouseout="return nd()" onclick="getwot();" src="../images/appbuttons/minibuttons/clockgrn.gif"
                                            runat="server" width="19"><img id="Img4" onmouseover="return overlib('Job Plan Time and Materials Reporting', BELOW, RIGHT)"
                                                onmouseout="return nd()" onclick="getjot();" src="../images/appbuttons/minibuttons/clockyel.gif"
                                                runat="server" width="19"><img id="Img5" onmouseover="return overlib('Work Order Costs', BELOW, RIGHT)"
                                                    onmouseout="return nd()" onclick="getcosts();" src="../images/appbuttons/minibuttons/dollar.gif"
                                                    runat="server" width="19"><img id="imgcomp" onmouseover="return overlib('Complete this Work Order', BELOW, RIGHT)"
                                                        onmouseout="return nd()" onclick="checkcomp();" src="../images/appbuttons/minibuttons/comp.gif"
                                                        runat="server" width="19">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table cellspacing="0" cellpadding="0" width="630">
                                <tr>
                                    <td class="thdrsinglft" width="22">
                                        <img border="0" src="../images/appbuttons/minibuttons/wohdr.gif" width="22" height="20">
                                    </td>
                                    <td class="thdrsingrt label" width="608" align="left" height="20">
                                        <asp:Label ID="lang1350" runat="server">Work Order Details</asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table cellspacing="0" cellpadding="0" width="330">
                                <tr>
                                    <td colspan="2">
                                        <table cellpadding="1" cellspacing="1">
                                            <tr>
                                                <td id="tddepts" class="bluelabel" runat="server">
                                                    Department
                                                </td>
                                                <td>
                                                    <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                                                </td>
                                                <td id="tdlocs1" class="bluelabel" runat="server">
                                                    Location
                                                </td>
                                                <td>
                                                    <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txteqauto" runat="server" CssClass="plainlabel" Width="120"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <img onclick="get_eqauto();" src="../images/appbuttons/minibuttons/magnifier.gif"
                                                        id="imgeqauto" runat="server">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trdepts" class="details" runat="server">
                                    <td colspan="2">
                                        <table width="330" cellpadding="1" cellspacing="1">
                                            <tr>
                                                <td class="label" width="110">
                                                    Department
                                                </td>
                                                <td id="tddept" class="plainlabel" width="170" runat="server">
                                                </td>
                                                <td width="50">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label">
                                                    Station\Cell
                                                </td>
                                                <td id="tdcell" class="plainlabel" runat="server">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label" id="tdeqlbl" runat="server">
                                                    Equipment
                                                </td>
                                                <td id="tdeq" class="plainlabel" runat="server">
                                                </td>
                                                <td id="tddown" class="label" runat="server">
                                                    <input id="cbisdown" onclick="checkdown('d');" type="checkbox" name="cbisdown" runat="server" /><asp:Label
                                                        ID="lang1557" runat="server">Down?</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td id="tdeqdesc" class="plainlabel" runat="server" colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label">
                                                    <asp:Label ID="Label3" runat="server">Charge#</asp:Label>
                                                </td>
                                                <td id="tdcharge1" class="plainlabel" runat="server">
                                                </td>
                                                <td>
                                                    <img onclick="getcharge('wo','1');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                                        id="imgchd" runat="server">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label">
                                                    Function
                                                </td>
                                                <td id="tdfu" class="plainlabel" runat="server">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label">
                                                    Component
                                                </td>
                                                <td id="tdco" class="plainlabel" runat="server">
                                                </td>
                                            </tr>
                                            <tr id="trmisc" runat="server">
                                                <td class="label">
                                                    Misc Asset
                                                </td>
                                                <td id="tdmisc" class="plainlabel" runat="server">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label">
                                                    Location
                                                </td>
                                                <td id="tdloc" class="plainlabel" runat="server">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label" valign="top">
                                                    Failure Mode(s)
                                                </td>
                                                <td id="tdfail" class="plainlabelred" runat="server">
                                                </td>
                                                <td valign="top">
                                                    <img onclick="getfc('1');" src="../images/appbuttons/minibuttons/magnifier.gif">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trlocs" class="details" runat="server">
                                    <td colspan="2">
                                        <table cellpadding="1" cellspacing="1" width="330">
                                            <tr>
                                                <td class="label">
                                                    Location
                                                </td>
                                                <td id="tdloc2" class="plainlabel" runat="server" colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label">
                                                    Equipment
                                                </td>
                                                <td id="tdeq2" class="plainlabel" runat="server">
                                                </td>
                                                <td id="tdisdown1" class="label" runat="server">
                                                    <input id="cbisdown1" onclick="checkdown('l');" type="checkbox" name="cbisdown" runat="server"><asp:Label
                                                        ID="Label1" runat="server">Down?</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label">
                                                    <asp:Label ID="lang2152" runat="server">Charge#</asp:Label>
                                                </td>
                                                <td id="tdcharge2" class="plainlabel" runat="server">
                                                </td>
                                                <td>
                                                    <img onclick="getcharge('wo','2');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label" valign="top">
                                                    Error Codes
                                                </td>
                                                <td id="tderr" class="plainlabelred" runat="server">
                                                </td>
                                                <td valign="top">
                                                    <img onclick="geterr();" src="../images/appbuttons/minibuttons/magnifier.gif">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trlocs3" class="details" runat="server">
                                    <td colspan="2">
                                        <table cellpadding="1" cellspacing="1" width="330">
                                            <tr>
                                                <td class="label">
                                                    Location
                                                </td>
                                                <td id="tdloc3" class="plainlabel" runat="server" colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label" id="tdeq3lbl" runat="server">
                                                    Equipment
                                                </td>
                                                <td id="tdeq3" class="plainlabel" runat="server">
                                                </td>
                                                <td id="tdisdown3" class="label" runat="server">
                                                    <input id="cbisdown3" onclick="checkdown('l');" type="checkbox" name="cbisdown" runat="server"><asp:Label
                                                        ID="Label4" runat="server">Down?</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td id="tdeqdesc3" class="plainlabel" runat="server" colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label">
                                                    <asp:Label ID="Label5" runat="server">Charge#</asp:Label>
                                                </td>
                                                <td id="tdcharge3" class="plainlabel" runat="server">
                                                </td>
                                                <td>
                                                    <img onclick="getcharge('wo','3');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                                        id="imgch" runat="server">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label">
                                                    Function
                                                </td>
                                                <td id="tdfu3" class="plainlabel" runat="server">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label">
                                                    Component
                                                </td>
                                                <td id="tdco3" class="plainlabel" runat="server">
                                                </td>
                                            </tr>
                                            <tr id="trmisc3" runat="server">
                                                <td class="label">
                                                    Misc Asset
                                                </td>
                                                <td id="tdmisc3" class="plainlabel" runat="server">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label" valign="top">
                                                    Failure Mode(s)
                                                </td>
                                                <td id="tdfail3" class="plainlabelred" runat="server">
                                                </td>
                                                <td valign="top">
                                                    <img onclick="getfc('3');" src="../images/appbuttons/minibuttons/magnifier.gif">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="5">
                            &nbsp;
                        </td>
                        <td valign="top">
                            <table cellspacing="1" cellpadding="1" width="295" border="0">
                                <tr>
                                    <td class="label" colspan="3">
                                        Work Order Description
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:TextBox ID="txtwodesc" runat="server" Width="270" CssClass="plainlabel" TextMode="MultiLine"
                                            Height="60px"></asp:TextBox>
                                    </td>
                                   
                                </tr>
                                <tr>
                                    <td class="label" width="80">
                                        Requestor
                                    </td>
                                    <td id="tdreq" class="plainlabel" runat="server" width="190">
                                        <asp:TextBox ID="txtreq" runat="server" Width="160" CssClass="details"></asp:TextBox>
                                    </td>
                                    <td width="20">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        Request Date
                                    </td>
                                    <td id="tdreqdate" class="plainlabel" runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        Phone
                                    </td>
                                    <td id="tdphone" class="plainlabel" runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        Work Status
                                    </td>
                                    <td id="tdstat" class="plainlabel" runat="server">
                                    </td>
                                    <td>
                                        <img onclick="getstat();" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        Status Date
                                    </td>
                                    <td id="tdstatdate" class="plainlabel" runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        Work Type
                                    </td>
                                    <td id="tdwt" class="plainlabel" runat="server">
                                    </td>
                                    <td>
                                        <img id="imgwt" onclick="getwt();" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                            runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        Work Area
                                    </td>
                                    <td id="tdwa" class="plainlabel" runat="server">
                                    </td>
                                    <td>
                                        <img onclick="getwa('d');" src="../images/appbuttons/minibuttons/magnifier.gif">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        Priority
                                    </td>
                                    <td id="Td1" class="plainlabel" runat="server">
                                        <asp:DropDownList ID="ddpri" runat="server" CssClass="plainlabel">
                                            <asp:ListItem Value="0">0</asp:ListItem>
                                            <asp:ListItem Value="1">1</asp:ListItem>
                                            <asp:ListItem Value="2">2</asp:ListItem>
                                            <asp:ListItem Value="3">3</asp:ListItem>
                                            <asp:ListItem Value="4">4</asp:ListItem>
                                            <asp:ListItem Value="5">5</asp:ListItem>
                                            <asp:ListItem Value="6">6</asp:ListItem>
                                            <asp:ListItem Value="7">7</asp:ListItem>
                                            <asp:ListItem Value="8">8</asp:ListItem>
                                            <asp:ListItem Value="9">9</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <img onclick="checkpri();" src="../images/appbuttons/minibuttons/gwarning.gif" alt=""
                                            id="imgpri" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="thdrsingg plainlabel" width="630" colspan="3">
                            Job Planning Requirements
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table cellspacing="0" cellpadding="0" width="330">
                                <tr>
                                    <td id="tdjp" class="label" runat="server" width="100">
                                        Job Plan
                                    </td>
                                    <td id="tdplan" runat="server" class="plainlabel" width="150">
                                    </td>
                                    <td width="20">
                                        <img id="imgplans" onmouseover="return overlib('View Existing Job Plans', ABOVE, LEFT)"
                                            onmouseout="return nd()" onclick="getjplans();" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                            runat="server" />
                                    </td>
                                    <td width="20">
                                        <img id="imgaddplan" onmouseover="return overlib('Create a New Job Plan', ABOVE, LEFT)"
                                            onmouseout="return nd()" onclick="gotojp();" alt="" src="../images/appbuttons/minibuttons/addmod.gif"
                                            runat="server" />
                                    </td>
                                    <td width="20">
                                        <img id="imgeditplan" onmouseover="return overlib('Edit Selected Job Plan (Applies to Work Order Only)', ABOVE, LEFT)"
                                            onmouseout="return nd()" onclick="getwjplan();" alt="" src="../images/appbuttons/minibuttons/pencil.gif"
                                            runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <input id="cbreq" onclick="checkreq();" type="checkbox" name="cbreq" runat="server"><font
                                            class="label"><asp:Label ID="lang1565" runat="server">Planning Required?</asp:Label></font><font
                                                class="plainlabel"><asp:Label ID="lang1566" runat="server">(Overrides Job Plan Selection)</asp:Label></font>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                        <td valign="top">
                            <table cellspacing="0" cellpadding="0" width="295">
                                <tr>
                                    <td id="tdplanner" class="graylabel" runat="server" width="100">
                                        <asp:Label ID="lang1567" runat="server">Select Planner</asp:Label>
                                    </td>
                                    <td id="tdplanner2" runat="server" class="plainlabel" width="160">
                                    </td>
                                    <td width="20">
                                        <img id="imgplanner" border="0" alt="" src="../images/appbuttons/minibuttons/magnifierdis.gif"
                                            runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" height="18">
                                        <asp:Label ID="lang1568" runat="server">Reference#</asp:Label>
                                    </td>
                                    <td id="tdref" class="plainlabel" colspan="2" runat="server">
                                        <asp:Label ID="lang1569" runat="server">None</asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="thdrsingg plainlabel">
                            <asp:Label ID="lang1366" runat="server">Scheduling Details</asp:Label>
                        </td>
                        <td>
                        </td>
                        <td class="thdrsingg plainlabel">
                            <asp:Label ID="lang1367" runat="server">Responsibility</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table border="0" cellspacing="0" cellpadding="0" width="330">
                                <tr>
                                    <td width="70">
                                    </td>
                                    <td width="88">
                                    </td>
                                    <td width="20">
                                    </td>
                                    <td width="40">
                                    </td>
                                    <td width="50">
                                    </td>
                                    <td width="20">
                                    </td>
                                    <td width="20">
                                    </td>
                                </tr>
                                <tr height="18">
                                    <td>
                                    </td>
                                    <td class="label" align="center">
                                        <asp:Label ID="lang1368" runat="server">Start</asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="label" colspan="2" align="center">
                                        <asp:Label ID="lang1369" runat="server">Complete</asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <asp:Label ID="lang1370" runat="server">Target</asp:Label>
                                    </td>
                                    <td id="tdtstart" runat="server" class="plainlabel" align="center">
                                    </td>
                                    <td>
                                        <img onclick="getcal('tstart');" alt="" src="../images/appbuttons/minibuttons/btncal2.gif"
                                            width="19" height="19">
                                    </td>
                                    <td colspan="2" id="tdtcomp" runat="server" class="plainlabel" align="center">
                                    </td>
                                    <td>
                                        <img onclick="getcal('tcomp');" alt="" src="../images/appbuttons/minibuttons/btncal2.gif"
                                            width="19" height="19">
                                    </td>
                                </tr>
                                <tr id="trsched" runat="server">
                                    <td id="tdschlab" class="label" runat="server">
                                        <asp:Label ID="lang1371" runat="server">Scheduled</asp:Label>
                                    </td>
                                    <td id="tdsstart" runat="server" class="plainlabel" align="center">
                                    </td>
                                    <td>
                                        <img onclick="getcal('sstart');" alt="" src="../images/appbuttons/minibuttons/btncal2.gif"
                                            width="19" height="19">
                                    </td>
                                    <td colspan="2" id="tdscomp" runat="server" class="plainlabel" align="center">
                                    </td>
                                    <td>
                                        <img onclick="getcal('scomp');" alt="" src="../images/appbuttons/minibuttons/btncal2.gif"
                                            width="19" height="19">
                                    </td>
                                    <td>
                                        <img onmouseover="return overlib('Jump to Scheduling (Scheduled Start, Scheduled Complete, and top level Work Order Skill Required to view this record in Scheduler)', ABOVE, LEFT)"
                                            onmouseout="return nd()" onclick="proj();" src="../images/appbuttons/minibuttons/sched.gif">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <asp:Label ID="lang1372" runat="server">Actual</asp:Label>
                                    </td>
                                    <td id="tdastart" runat="server" class="plainlabel" align="center">
                                    </td>
                                    <td>
                                        <img onclick="getcal('astart');" alt="" src="../images/appbuttons/minibuttons/btncal2.gif"
                                            width="19" height="19">
                                    </td>
                                    <td colspan="2" id="tdacomp" runat="server" class="plainlabel" align="center">
                                    </td>
                                    <td>
                                        <img onclick="getcal('acomp');" alt="" src="../images/appbuttons/minibuttons/btncal2.gif"
                                            width="19" height="19">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" colspan="4">
                                        <asp:Label ID="lang1373a" runat="server">Estimated Labor Hours/Manpower</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtestlh" runat="server" Width="40px" CssClass="plainlabel"></asp:TextBox>
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="txtestmp" runat="server" Width="38px" CssClass="plainlabel"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" colspan="5">
                                        <asp:Label ID="lang1374" runat="server">Estimated Labor Hours (Job Plan)</asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="txtestjp" runat="server" Width="38px" CssClass="plainlabel" ReadOnly="True"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td valign="top">
                            <table border="0" cellspacing="0" cellpadding="0" width="295">
                                <tr>
                                    <td width="75">
                                    </td>
                                    <td width="180">
                                    </td>
                                    <td width="20">
                                    </td>
                                    <td width="20">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <asp:Label ID="Label6" runat="server">Planner</asp:Label>
                                    </td>
                                    <td id="tdwoplanner" class="plainlabel" colspan="2" runat="server">
                                    </td>
                                    <td>
                                        <img id="Img7" onclick="getplanner('wplan');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                            runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <asp:Label ID="lang1375" runat="server">Skill</asp:Label>
                                    </td>
                                    <td id="tdskill" class="plainlabel" colspan="2" runat="server">
                                    </td>
                                    <td>
                                        <img id="Img3" onclick="getsk();" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                            runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <asp:Label ID="lang1376" runat="server">Contract</asp:Label>
                                    </td>
                                    <td id="tdcontract" runat="server" class="plainlabel" colspan="2">
                                    </td>
                                    <td>
                                        <img id="imgcontract" onclick="getcomp();" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                            runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <asp:Label ID="lang1378" runat="server">Supervisor</asp:Label>
                                    </td>
                                    <td id="tdsup" runat="server" class="plainlabel" colspan="2">
                                        <asp:TextBox ID="txtsuperauto" runat="server" CssClass="plainlabel" Width="180"></asp:TextBox>
                                    </td>
                                    <td id="tdsupe" runat="server">
                                        <input id="cbsupe" class="view plainlabel" onmouseover="return overlib('Check to send email alert to Supervisor')"
                                            onmouseout="return nd()" type="checkbox" name="cbsupe" runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" height="18">
                                        <asp:Label ID="lang1377" runat="server">Lead Craft</asp:Label>
                                    </td>
                                    <td id="tdlead" runat="server" class="plainlabel" colspan="2">
                                    <asp:TextBox ID="txtleadauto" runat="server" CssClass="plainlabel" Width="180"></asp:TextBox>
                                    </td>
                                   
                                    <td id="tdleade" runat="server">
                                        <input id="cbleade" class="view plainlabel" onmouseover="return overlib('Check to send email alert to Lead Craft')"
                                            onmouseout="return nd()" type="checkbox" name="cbleade" runat="server">
                                    </td>
                                </tr>
                                <tr id="trgrp" runat="server">
                                    <td class="label" valign="top">
                                        <asp:Label ID="Label2" runat="server">Labor Group</asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <div style="border-bottom: 2px groove; border-left: 2px groove; width: 190px; height: 45px;
                                            overflow: auto; border-top: 2px groove; border-right: 2px groove" id="divlg"
                                            runat="server">
                                        </div>
                                    </td>
                                    <td>
                            <img id="imgleadcraft" onclick="getsuper('lead');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                runat="server">
                        </td>
                                </tr>
                                <tr>
                                    <td class="label" colspan="2">
                                        Email Lead Time
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtleadtime" runat="server" Width="45px" CssClass="plainlabel"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="600" valign="top">
                <iframe id="ifnotes" runat="server" src="" frameborder="0" style="z-index: 0;" width="600"
                    height="280"></iframe>
                <iframe id="ifwo" runat="server" src="" frameborder="0" style="z-index: 0;" width="600"
                    height="300"></iframe>
            </td>
        </tr>
    </table>
    <iframe id="ifeqauto" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
    <iframe id="ifsuperauto" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
    <iframe id="ifleadauto" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
    <iframe id="ifsave" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
    <input id="lblwt" type="hidden" name="lblwt" runat="server" />
    <input id="lblstat" type="hidden" name="lblstat" runat="server" />
    <input id="lbltyp" type="hidden" name="lbltyp" runat="server" />
    <input id="lbllid" type="hidden" name="lbllid" runat="server" />
    <input id="lblloc" type="hidden" name="lblloc" runat="server" />
    <input id="lblpchk" type="hidden" name="lblpchk" runat="server" />
    <input id="lblsid" type="hidden" name="lblsid" runat="server" />
    <input id="lblwonum" type="hidden" name="lblwonum" runat="server" />
    <input id="lbldid" type="hidden" name="lbldid" runat="server" />
    <input id="lbldept" type="hidden" name="lbldept" runat="server" />
    <input id="lblcid" type="hidden" name="lblcid" runat="server" />
    <input id="lblcell" type="hidden" name="lblcell" runat="server" />
    <input id="lblchk" type="hidden" name="lblchk" runat="server" />
    <input id="lblpar2" type="hidden" name="lblpar2" runat="server" />
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server" />
    <input id="lbleq" type="hidden" name="lbleq" runat="server" />
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server" />
    <input id="lblfu" type="hidden" name="lblfu" runat="server" /><input id="lblcomp"
        type="hidden" name="lblcomp" runat="server" />
    <input id="lblcomid" type="hidden" name="lblcomid" runat="server" /><input id="lbltabs"
        type="hidden" name="lbltabs" runat="server" />
    <input id="lblwaid" type="hidden" name="Hidden1" runat="server" />
    <input id="lblworkarea" type="hidden" name="Hidden2" runat="server" />
    <input id="lblncid" type="hidden" name="lblncid" runat="server" />
    <input id="lblnc" type="hidden" name="lblnc" runat="server" />
    <input id="lblld" type="hidden" name="lblld" runat="server" />
    <input id="lblisdown" type="hidden" name="lblisdown" runat="server" />
    <input id="lbluser" type="hidden" name="lbluser" runat="server" />
    <input id="lblskillid" type="hidden" name="lblskillid" runat="server" />
    <input id="lblskill" type="hidden" name="lblskill" runat="server" />
    <input id="lblro" type="hidden" name="lblro" runat="server" />
    <input id="lbldis" type="hidden" name="lbldis" runat="server" />
    <input id="lbljpref" type="hidden" name="lbljpref" runat="server" />
    <input id="lbljpid" type="hidden" name="lbljpid" runat="server" /><input id="lblsubmit"
        type="hidden" name="lblsubmit" runat="server" />
    <input id="lblplnrid" type="hidden" name="lblplnrid" runat="server" />
    <input id="lblplanner" type="hidden" name="lblplanner" runat="server" />
    <input id="lbljump" type="hidden" name="lbljump" runat="server" /><input id="lblsav"
        type="hidden" name="lblsav" runat="server" />
    <input id="lblcnt" type="hidden" name="lblcnt" runat="server" /><input id="lblclid"
        type="hidden" name="lblclid" runat="server" />
    <input id="lblsdays" type="hidden" name="lblsdays" runat="server" />
    <input id="lblse" type="hidden" name="lblse" runat="server" />
    <input id="lblle" type="hidden" name="lblle" runat="server" />
    <input id="lblpmid" type="hidden" name="lblpmid" runat="server" />
    <input id="lbllead" type="hidden" name="lbllead" runat="server" />
    <input id="lblsup" type="hidden" name="lblsup" runat="server" />
    <input id="lblchecksave" type="hidden" name="lblchecksave" runat="server" />
    <input id="lbllog" type="hidden" name="lbllog" runat="server" />
    <input id="lblpmhid" type="hidden" name="lblpmhid" runat="server" />
    <input id="lblmalert" type="hidden" name="lblmalert" runat="server" />
    <input id="lblusesched" type="hidden" name="lblusesched" runat="server" />
    <input id="lbltpmid" type="hidden" runat="server" />
    <input id="lblttime" type="hidden" name="lblttime" runat="server" />
    <input id="lblacttime" type="hidden" name="lblacttime" runat="server" />
    <input id="lbldtime" type="hidden" name="Hidden1" runat="server" />
    <input id="lblactdtime" type="hidden" name="lblactdtime" runat="server" />
    <input id="lblusetdt" type="hidden" name="lblusetdt" runat="server" />
    <input id="lblusetotal" type="hidden" name="lblusetotal" runat="server" />
    <input id="lblfcust" type="hidden" name="lblfcust" runat="server" />
    <input id="lblretday" type="hidden" name="lblretday" runat="server" />
    <input id="lbldays" type="hidden" name="lbldays" runat="server" />
    <input id="txtn" type="hidden" name="txtn" runat="server" />
    <input id="lblworuns" type="hidden" runat="server" />
    <input id="lblruns" type="hidden" runat="server" />
    <input type="hidden" id="lblplan" runat="server" />
    <input type="hidden" id="lblplanner2" runat="server" />
    <input type="hidden" id="lbltstart" runat="server" />
    <input type="hidden" id="lbltcomp" runat="server" />
    <input type="hidden" id="lblissched" runat="server" />
    <input type="hidden" id="lblappstr" runat="server" name="lblappstr"><input type="hidden"
        id="lbloldelead" runat="server" />
    <input type="hidden" id="lblscomp" runat="server" />
    <input type="hidden" id="lbllcomp" runat="server" />
    <input type="hidden" id="lblrettyp" runat="server" />
    <input type="hidden" id="lbliscol" runat="server" />
    <input type="hidden" id="lblisactive" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblreq" runat="server" />
    <input type="hidden" id="lblacomp" runat="server" />
    <input type="hidden" id="lblisdownp" runat="server" />
    <input type="hidden" id="lblcoi" runat="server" />
    <input type="hidden" id="lblrootid" runat="server" />
    <input type="hidden" id="lblnoteid" runat="server" />
    <input type="hidden" id="lblwtstr" runat="server" />
    <input type="hidden" id="lblpmnum" runat="server" />
    <input type="hidden" id="lblwoprid" runat="server" />
    <input type="hidden" id="lblprimsg" runat="server" />
    <input type="hidden" id="lblwplannerid" runat="server" />
    <input type="hidden" id="lblwplanner" runat="server" />
    <input type="hidden" id="lblusepri" runat="server" />
    <input type="hidden" id="lblrtid" runat="server" />
    <input type="hidden" id="lbldocs" runat="server" />
    <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" TargetControlID="txteqauto"
        runat="server" ServicePath="eqauto.asmx" ServiceMethod="geteq" MinimumPrefixLength="1"
        CompletionInterval="1000" EnableCaching="true" CompletionSetCount="12" CompletionListCssClass="listbox"
        CompletionListItemCssClass="listitem" CompletionListHighlightedItemCssClass="listhigh">
    </cc1:AutoCompleteExtender>
    <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" TargetControlID="txtsuperauto"
        runat="server" ServicePath="superauto.asmx" ServiceMethod="geteq" MinimumPrefixLength="1"
        CompletionInterval="1000" EnableCaching="true" CompletionSetCount="12" CompletionListCssClass="listboxshort"
        CompletionListItemCssClass="listitem" CompletionListHighlightedItemCssClass="listhigh">
    </cc1:AutoCompleteExtender>
    <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" TargetControlID="txtleadauto"
        runat="server" ServicePath="leadauto.asmx" ServiceMethod="geteq" MinimumPrefixLength="1"
        CompletionInterval="1000" EnableCaching="true" CompletionSetCount="12" CompletionListCssClass="listboxshort"
        CompletionListItemCssClass="listitem" CompletionListHighlightedItemCssClass="listhigh">
    </cc1:AutoCompleteExtender>
    </form>
</body>
</html>
