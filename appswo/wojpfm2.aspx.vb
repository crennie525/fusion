

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wojpfm2
    Inherits System.Web.UI.Page
	Protected WithEvents lang1513 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1512 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim comp As New Utilities
    Dim pmid, ro, rostr As String
    Dim ds As DataSet
    Dim wonum, jpid, stat, Login As String
    Protected WithEvents Label24 As System.Web.UI.WebControls.Label
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txttsk As System.Web.UI.WebControls.TextBox
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dltasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgmeas As System.Web.UI.WebControls.DataGrid
    Protected WithEvents tdjpn As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdjpd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnofail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcurrcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrow As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfmcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmstrvals As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmflg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dltasks.Columns(0).Visible = False
            Else
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = comp.CheckROS(rostr, "wo")
                    'lblro.Value = ro
                    If ro = "1" Then
                        dltasks.Columns(0).Visible = False
                    End If
                End If
            End If
            wonum = Request.QueryString("wo").ToString '"1342" '
            jpid = Request.QueryString("jpid").ToString '"3" '
            lblwo.Value = wonum
            lbljpid.Value = jpid
            stat = Request.QueryString("stat").ToString
            lblstat.Value = stat
            If (stat <> "COMP" And stat <> "CAN" And stat <> "WAPPR") Then
                dltasks.Columns(0).Visible = True
            Else
                dltasks.Columns(0).Visible = False
            End If
            comp.Open()
            GetJPHead(jpid, wonum)
            PopDL(jpid, wonum)
            comp.Dispose()
        End If
    End Sub

    Private Sub GetJPHead(ByVal jpid As String, ByVal wonum As String)
        sql = "select jpnum, description from wojobplans where wonum = '" & wonum & "' and jpid = '" & jpid & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdjpn.InnerHtml = dr.Item("jpnum").ToString
            tdjpd.InnerHtml = dr.Item("description").ToString
        End While
        dr.Close()
    End Sub
    Private Sub PopDL(ByVal jpid As String, ByVal wonum As String)
        sql = "usp_getwojpfail '" & jpid & "','" & wonum & "'"
        ds = comp.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        dltasks.DataSource = dv
        dltasks.DataBind()

        If dv.Count = 0 Then
            tdnofail.InnerHtml = "No Job Plan Level Failure Modes Found"
        End If
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If CatID <> "Select" And CatID <> "N/A" Then 'Not IsDBNull(CatID) OrElse  
            If CatID = "PASS" Then
                iL = 0
            ElseIf CatID = "FAIL" Then
                iL = 1
            ElseIf CatID = "YES" Then
                iL = 0
            ElseIf CatID = "NO" Then
                iL = 1
            End If
        Else
            iL = -1
        End If
        Return iL
    End Function
    Private Sub GetMeas(ByVal jpid As String, ByVal wonum As String)
        sql = "select m.*, j.tasknum, j.taskdesc from wojptaskmeasdetails m " _
        + "left join wojobtasks j on j.jpid = m.jpid and j.pmtskid = m.pmtskid " _
        + "where m.jpid = '" & jpid & "' and j.wonum = '" & wonum & "'"
        dr = comp.GetRdrData(sql)
        dgmeas.DataSource = dr
        dgmeas.DataBind()


    End Sub

    Private Sub dltasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dltasks.EditCommand
        comp.Open()
        jpid = lbljpid.Value
        wonum = lblwo.Value
        dltasks.EditItemIndex = e.Item.ItemIndex

        PopDL(jpid, wonum)
        comp.Dispose()
    End Sub

    Private Sub dltasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dltasks.UpdateCommand
        Dim ret, typ, fail, pass, task, atime As String
        Dim tsk, cnt, adj As String
        wonum = lblwo.Value
        jpid = lbljpid.Value
        Dim pmtskid As String = lblrow.Value
        Dim rb1, rb2, rb3, rb4, rb5, rbc As DropDownList
        Dim sel1, sel2, sel3, sel4, sel5, selc As String
        rb1 = CType(e.Item.FindControl("ddfm1"), DropDownList)
        sel1 = rb1.SelectedValue.ToString
        If sel1 = "" Then sel1 = "0"
        rb2 = CType(e.Item.FindControl("ddfm2"), DropDownList)
        sel2 = rb2.SelectedValue.ToString
        If sel2 = "" Then sel2 = "0"
        rb3 = CType(e.Item.FindControl("ddfm3"), DropDownList)
        sel3 = rb3.SelectedValue.ToString
        If sel3 = "" Then sel3 = "0"
        rb4 = CType(e.Item.FindControl("ddfm4"), DropDownList)
        sel4 = rb4.SelectedValue.ToString
        If sel4 = "" Then sel4 = "0"
        rb5 = CType(e.Item.FindControl("ddfm5"), DropDownList)
        sel5 = rb5.SelectedValue.ToString
        If sel5 = "" Then sel5 = "0"

        sql = "usp_upjptskfm '" & sel1 & "', '" & sel2 & "', '" & sel3 & "', '" & sel4 & "', '" & sel5 & "', '" & pmtskid & "', '" & jpid & "', '" & wonum & "'"

        comp.Open()
        comp.Update(sql)
        wonum = lblwo.Value
        jpid = lbljpid.Value
        dltasks.EditItemIndex = -1
        PopDL(jpid, wonum)
        comp.Dispose()
    End Sub

    Private Sub dltasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dltasks.ItemDataBound
        Dim trs, trf, trt, trfm, trhd As HtmlTableRow
        Dim pcnt, tcnt, lcnt, task As String
        Dim mea As String
        Dim fcnt As Integer = 0
        'Dim pcnt As Integer

        Dim fm1, fm2, fm3, fm4, fm5
        Dim rb1, rb2, rb3, rb4, rb5 As DropDownList
        Dim ibfm As ImageButton
        Dim rb1tc As RadioButtonList
        Dim rb1tci As String
        Dim rb1i, rb2i, rb3i, rb4i, rb5i As String
        Dim pmtid, pmfid, fm, fmid, comid As String

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            stat = lblstat.Value
            If stat = "COMP" Or stat = "CAN" Or stat = "WAPPR" Then
                ibfm = CType(e.Item.FindControl("imgeditfm"), ImageButton)
                ibfm.Attributes.Add("class", "details")

            End If

            Dim img As HtmlImage = CType(e.Item.FindControl("imgti"), HtmlImage)
            'Dim pmtskid As String = DataBinder.Eval(e.Item.DataItem, "pmtskid").ToString
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "task").ToString
            'tdtsk.InnerHtml = tsk
            img.Attributes.Add("onclick", "gettsk('" & tsk & "');")

        End If

        If e.Item.ItemType = ListItemType.EditItem Then


            Dim img As HtmlImage = CType(e.Item.FindControl("imgte"), HtmlImage)
            'Dim pmtskid As String = DataBinder.Eval(e.Item.DataItem, "pmtskid").ToString
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "task").ToString
            'tdtsk.InnerHtml = tsk
            img.Attributes.Add("onclick", "gettsk('" & tsk & "');")

            fcnt = 0
            fm1 = DataBinder.Eval(e.Item.DataItem, "fm1id").ToString 'e.Item.DataItem("fm1id").ToString
            If fm1 = 0 Then
                rb1 = CType(e.Item.FindControl("ddfm1"), DropDownList)
                rb1.Enabled = False
            Else
                If DataBinder.Eval(e.Item.DataItem, "fm1dd").ToString <> "Select" Then
                    fcnt = fcnt + 1
                End If

            End If
            fm2 = DataBinder.Eval(e.Item.DataItem, "fm2id").ToString 'e.Item.DataItem("fm2id").ToString
            If fm2 = 0 Then
                rb2 = CType(e.Item.FindControl("ddfm2"), DropDownList)
                rb2.Enabled = False
            Else
                If DataBinder.Eval(e.Item.DataItem, "fm2dd").ToString <> "Select" Then
                    fcnt = fcnt + 1
                End If

            End If
            fm3 = DataBinder.Eval(e.Item.DataItem, "fm3id").ToString 'e.Item.DataItem("fm3id").ToString
            If fm3 = 0 Then
                rb3 = CType(e.Item.FindControl("ddfm3"), DropDownList)
                rb3.Enabled = False
            Else
                If DataBinder.Eval(e.Item.DataItem, "fm3dd").ToString <> "Select" Then
                    fcnt = fcnt + 1
                End If

            End If
            fm4 = DataBinder.Eval(e.Item.DataItem, "fm4id").ToString 'e.Item.DataItem("fm4id").ToString
            If fm4 = 0 Then
                rb4 = CType(e.Item.FindControl("ddfm4"), DropDownList)
                rb4.Enabled = False
            Else
                If DataBinder.Eval(e.Item.DataItem, "fm4dd").ToString <> "Select" Then
                    fcnt = fcnt + 1
                End If

            End If
            fm5 = DataBinder.Eval(e.Item.DataItem, "fm5id").ToString 'e.Item.DataItem("fm5id").ToString
            If fm5 = 0 Then
                rb5 = CType(e.Item.FindControl("ddfm5"), DropDownList)
                rb5.Enabled = False
            Else
                If DataBinder.Eval(e.Item.DataItem, "fm5dd").ToString <> "Select" Then
                    fcnt = fcnt + 1
                End If

            End If

            ' rb1tc = CType(e.Item.FindControl("rbtc"), RadioButtonList)
            'rb1tci = rb1tc.ClientID.ToString
            'rb1tc.Attributes.Add("onclick", "checkcnt('" & rb1tci & "', '" & fcnt & "');")
            lblcurrcnt.Value = fcnt
        End If
    End Sub

    Private Sub dltasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dltasks.CancelCommand
        dltasks.EditItemIndex = -1
        comp.Open()
        wonum = lblwo.Value
        jpid = lbljpid.Value
        PopDL(jpid, wonum)
        comp.Dispose()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label24.Text = axlabs.GetASPXPage("wojpfm2.aspx", "Label24")
        Catch ex As Exception
        End Try
        Try
            lang1512.Text = axlabs.GetASPXPage("wojpfm2.aspx", "lang1512")
        Catch ex As Exception
        End Try
        Try
            lang1513.Text = axlabs.GetASPXPage("wojpfm2.aspx", "lang1513")
        Catch ex As Exception
        End Try

    End Sub

End Class
