<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wrdet.aspx.vb" Inherits="lucy_r12.wrdet" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wrdet</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/wrdetaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="checkstat();">
    <form id="form1" method="post" runat="server">
    <div id="FreezePane" class="FreezePaneOff" align="center">
        <div id="InnerFreezePane" class="InnerFreezePane">
        </div>
    </div>
    <table style="position: absolute; top: 0px; left: 0px" cellspacing="1" cellpadding="1"
        width="700">
        <tr>
            <td width="90">
            </td>
            <td width="160">
            </td>
            <td width="70">
            </td>
            <td width="20">
            </td>
            <td width="60">
            </td>
            <td width="50">
            </td>
            <td width="30">
            </td>
            <td width="70">
            </td>
            <td width="170">
            </td>
        </tr>
        <tr height="22">
            <td class="label" colspan="6">
                <asp:Label ID="lang1594" runat="server">Work Description</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="9" style="height: 53px">
                <asp:TextBox ID="txtwodesc" runat="server" Width="688px" TextMode="MultiLine" Height="50px"
                    CssClass="plainlabel"></asp:TextBox>
            </td>
        </tr>
        <tr height="22">
            <td class="label">
                <asp:Label ID="lang1595" runat="server">Requestor</asp:Label>
            </td>
            <td id="tdreq" class="plainlabel" runat="server">
            </td>
            <td class="label" colspan="2">
                <asp:Label ID="lang1596" runat="server">Request Date</asp:Label>
            </td>
            <td id="tdreqdate" class="plainlabel" colspan="3" runat="server">
            </td>
            <td class="label">
                <asp:Label ID="lang1597" runat="server">Phone</asp:Label>
            </td>
            <td class="plainlabel">
            </td>
        </tr>
        <tr height="22">
            <td class="label">
                <asp:Label ID="lang1598" runat="server">Status</asp:Label>
            </td>
            <td id="tdstatus" class="plainlabel" runat="server">
                <asp:Label ID="lang1599" runat="server">WAPPR</asp:Label>
            </td>
            <td class="label" colspan="2">
                <asp:Label ID="lang1600" runat="server">Status Date</asp:Label>
            </td>
            <td id="tdstatdate" class="plainlabel" colspan="3" runat="server">
            </td>
            <td class="label">
                <asp:Label ID="lang1601" runat="server">Work Type</asp:Label>
            </td>
            <td id="tdwt" class="plainlabel" runat="server">
                TBD
            </td>
        </tr>
        <tr height="22">
            <td class="label">
                <asp:Label ID="lang1602" runat="server">Charge#</asp:Label>
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtcharge" runat="server" Width="220px" CssClass="plainlabel"></asp:TextBox>
            </td>
            <td>
                <img onclick="getcharge('wo');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
            </td>
        </tr>
        <tr>
            <td class="thdrsingg plainlabel" colspan="4">
                <asp:Label ID="lang1603" runat="server">Scheduling Details</asp:Label>
            </td>
            <td class="thdrsingg plainlabel" colspan="5">
                <asp:Label ID="lang1604" runat="server">Responsibility</asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="4">
                <table cellspacing="1" cellpadding="1">
                    <tr>
                        <td class="label" colspan="3">
                            <asp:Label ID="lang1605" runat="server">Requested Start Date</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txttstart" runat="server" Width="100px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcal('tstart');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                width="19" height="19">
                        </td>
                    </tr>
                    <tr>
                        <td class="label" colspan="3">
                            <asp:Label ID="lang1606" runat="server">Requested Completion Date</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txttcomp" runat="server" Width="100px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcal('tcomp');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                width="19" height="19">
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td width="60">
                        </td>
                        <td width="60">
                        </td>
                        <td width="20">
                        </td>
                        <td width="60">
                        </td>
                        <td width="20">
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top" colspan="5">
                <table cellspacing="1" cellpadding="1" width="360">
                    <tr height="22">
                        <td class="label">
                            <asp:Label ID="lang1607" runat="server">Skill Required</asp:Label>
                        </td>
                        <td colspan="6">
                            <asp:DropDownList ID="ddskill" runat="server" Width="180px" CssClass="plainlabel">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr height="22">
                        <td class="label">
                            <asp:Label ID="lang1608" runat="server">Supervisor</asp:Label>
                        </td>
                        <td id="tdsuper" class="plainlabel" colspan="6" runat="server">
                        </td>
                    </tr>
                    <tr height="22">
                        <td class="label">
                            <asp:Label ID="lang1609" runat="server">Phone</asp:Label>
                        </td>
                        <td id="tdphone" class="plainlabel" colspan="6" runat="server">
                        </td>
                    </tr>
                    <tr height="22">
                        <td class="label">
                            <asp:Label ID="lang1610" runat="server">Email</asp:Label>
                        </td>
                        <td id="tdemail" class="plainlabel" colspan="6" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td width="100">
                        </td>
                        <td width="60">
                        </td>
                        <td width="50">
                        </td>
                        <td width="60">
                        </td>
                        <td width="20">
                        </td>
                        <td width="20">
                        </td>
                        <td width="50">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="border-top: #7ba4e0 thin solid" colspan="10" align="right">
                <input onmouseover="return overlib('Submit This Work Request and Exit')" onmouseout="return nd()"
                    onclick="handlesav('save');" value="Submit" type="button">&nbsp;<input onmouseover="return overlib('Save Changes to This Work Request Without Submitting and Exit')"
                        onmouseout="return nd()" onclick="handlesav('draft');" value="Save Draft" type="button">
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lblwo" type="hidden" name="lblwo" runat="server">
    <input id="lblsup" type="hidden" name="lblsup" runat="server">
    <input id="lbllead" type="hidden" name="lbllead" runat="server">
    <input id="lblskillid" type="hidden" name="lblskillid" runat="server"><input id="lbljpid"
        type="hidden" name="lbljpid" runat="server">
    <input id="lblpmid" type="hidden" name="lblpmid" runat="server"><input id="lblro"
        type="hidden" runat="server">
    <input id="lbllog" type="hidden" name="lbllog" runat="server"><input id="lblsubmit"
        type="hidden" runat="server">
    <input id="lblreqd" type="hidden" runat="server"><input type="hidden" id="lblstatus"
        runat="server">
    <input type="hidden" id="lbldid" runat="server">
    <input type="hidden" id="lbllid" runat="server">
    <input type="hidden" id="lblexit" runat="server"><input type="hidden" id="lblsid"
        runat="server">
    <input type="hidden" id="lbluser" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
