﻿Imports System.Data.SqlClient
Public Class cansendeout
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim sc As New Utilities
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            If Request.Form("lblret") = "excel" Then
                lblret.Value = ""
                sc.Open()
                BindExport()
                sc.Dispose()

            ElseIf Request.Form("lblret") = "excela" Then
                lblret.Value = ""
                sc.Open()
                BindExporta()
                sc.Dispose()
            End If

        End If
    End Sub
    Private Sub BindExport()
        sql = "select top 50 * from trans_temp order by sTransactionID desc"
        Dim ds As New DataSet
        ds = sc.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
        lblret.Value = ""
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
    Private Sub BindExporta()
        sql = "select top 50 * from trans_temp_time order by sTransactionID desc"
        Dim ds As New DataSet
        ds = sc.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgouta.DataSource = dv
        dgouta.DataBind()
        lblret.Value = ""
        cmpDataGridToExcel.DataGridToExcelNHD(dgouta, Response)
    End Sub
End Class