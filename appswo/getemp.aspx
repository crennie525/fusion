<%@ Page Language="vb" AutoEventWireup="false" Codebehind="getemp.aspx.vb" Inherits="lucy_r12.getemp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>getemp</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="javascript" type="text/javascript">
        <!--
		function saveemp() {
		document.getElementById("lblsubmit").value="savewo";
		document.getElementById("form1").submit();
		}
		function checkit() {
		var chk = document.getElementById("lblsubmit").value;
		if(chk=="return") {
		window.parent.handlereturn("ok");
		}
}
        //-->
		</script>
	</HEAD>
	<body onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="bluelabel" height="22">Employee</td>
					<td id="tdemp" class="plainlabel" runat="server"></td>
				</tr>
				<tr>
					<td class="bluelabel" height="22">Work Order</td>
					<td id="tdwo" class="plainlabel" runat="server"></td>
				</tr>
				<tr>
					<td class="bluelabel">Is Lead?</td>
					<td><input id="cblead" type="checkbox" runat="server"></td>
				</tr>
				<tr>
					<td class="bluelabel" height="22">Assigned Date</td>
					<td id="tddate" class="plainlabel" runat="server"></td>
				</tr>
				<tr>
					<td class="bluelabel">Assigned Hours</td>
					<td><asp:textbox id="txthrs" runat="server" Width="48px"></asp:textbox></td>
				</tr>
				<tr>
					<td class="plainlabelred" colSpan="2"><input id="cbusemin" type="checkbox" name="cbusemin" runat="server">Use 
						Minutes</td>
				</tr>
				<tr>
					<td colSpan="2" align="right"><IMG onclick="saveemp();" src="../images/appbuttons/minibuttons/savedisk1.gif"></td>
				</tr>
			</table>
			<input id="lbluid" type="hidden" runat="server"> <input id="lblwonum" type="hidden" runat="server">
			<input id="lblsubmit" type="hidden" runat="server"><input id="lblhrs" type="hidden" runat="server">
			<input id="lblislead" type="hidden" runat="server"> <input id="lblemp" type="hidden" runat="server">
			<input id="lbladate" type="hidden" runat="server"> <input id="lblwid" type="hidden" runat="server">
			<input id="lblshift" type="hidden" runat="server"> <input type="hidden" id="lblworktype" runat="server">
			<input type="hidden" id="lblpmid" runat="server">
            <input type="hidden" id="lblissched" runat="server" />
		</form>
	</body>
</HTML>
