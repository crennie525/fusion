

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wradd
    Inherits System.Web.UI.Page
	Protected WithEvents ovid206 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang1593 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1592 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1591 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1590 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1589 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim wo As New Utilities
    Dim cid, desc, def, usr, jump, won, sid, eq, eqid, fu, fuid, coid, did, clid, typ, lid, chk, ncid, ro, rostr, reqd, stat As String
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblreqd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstatus As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstart As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim wonum As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtwo As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtwodesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlongdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsavetask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents geteq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifdet As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljump As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsav As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhref As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgetarch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblncid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try

            lblro.Value = ro

            If ro <> "1" Then
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = wo.CheckROS(rostr, "wr")
                    lblro.Value = ro
                End If
            End If
            cid = "0"
            lblcid.Value = cid
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            usr = HttpContext.Current.Session("username").ToString
            lbluser.Value = usr
            'Try
            jump = Request.QueryString("jump").ToString
            If jump = "yes" Then
                won = Request.QueryString("wo").ToString
                txtwo.Text = won
                lblwo.Value = won
                lbljump.Value = "yes"
                wo.Open()
                'GetDesc()
                stat = CheckStatus()
                wo.Dispose()
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                did = Request.QueryString("did").ToString
                lbldid.Value = did
                clid = Request.QueryString("clid").ToString
                lblclid.Value = clid
                chk = Request.QueryString("chk").ToString
                lblchk.Value = chk
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                ncid = Request.QueryString("nid").ToString
                lblncid.Value = ncid
                fuid = Request.QueryString("fuid").ToString
                lblfuid.Value = fuid
                coid = Request.QueryString("coid").ToString
                lblcoid.Value = coid
                lid = Request.QueryString("lid").ToString
                lbllid.Value = lid
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                lblgetarch.Value = "yes"

                '"jump=yes&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eid + "&fuid=" + fid + "&coid=" + cid + "&lid=" + lid + "&typ=" + typ
                geteq.Attributes.Add("src", "wrman.aspx?jump=yes&wo=" + won + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&lid=" + lid + "&typ=" + typ + "&nid=" + ncid + "&stat=" + stat + "&usr=" + usr)
                If did <> "" Or lid <> "" Then
                    reqd = "ok"
                Else
                    reqd = "no"
                End If
                ifdet.Attributes.Add("src", "wrdet.aspx?jump=no&wo=" + won + "&reqd=" + reqd + "&stat=" + stat)
            End If
            'Catch ex As Exception

            'End Try
        Else
            If Request.Form("lblsubmit") = "new" Then
                wo.Open()
                GetWo()
                lbljump.Value = "no"
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "yes" Then
                lblsubmit.Value = ""
                CleanFields()
                wo.Open()
                SaveDesc()
                wo.Dispose()
            End If

        End If
        txtwodesc.Attributes.Add("onkeypress", "getcnt();")
        'txtlongdesc.Attributes.Add("onfocus", "SetEnd(this);")
    End Sub
    Private Sub CleanFields()
        lblsid.Value = ""
        lbldid.Value = ""
        lblclid.Value = ""
        lblchk.Value = ""
        lbleqid.Value = ""
        lblfuid.Value = ""
        lblcoid.Value = ""
        lbllid.Value = ""
        lbltyp.Value = ""
        'lblgetarch.Value = "yes"
    End Sub
    Private Function CheckStatus() As String
        wonum = lblwo.Value
        sql = "select status from workorder where wonum = '" & wonum & "'"
        stat = wo.strScalar(sql)
        lblstatus.Value = stat
        Return stat
    End Function
    Private Sub GetDesc()
        Try
            wonum = lblwo.Value
            sql = "select description from workorder where wonum = '" & wonum & "'"
            Dim desc, ldesc As String
            desc = wo.strScalar(sql)
            txtwodesc.Text = desc
            sql = "select longdesc from wolongdesc where wonum = '" & wonum & "'"
            ldesc = wo.strScalar(sql)
            If ldesc <> "" Then
                txtlongdesc.Text = desc + ldesc
                txtwodesc.Enabled = False
            End If
        Catch ex As Exception

        End Try

    End Sub
    Private Sub GetWo()
        cid = lblcid.Value
        sid = lblsid.Value
        usr = lbluser.Value
        'sql = "select wostatus from wostatus where compid = '" & cid & "' and isdefault = 1"
        'dr = wo.GetRdrData(sql)
        'While dr.Read
        'def = dr.Item("wostatus").ToString
        'End While
        'dr.Close()
        def = "HOLD"
        Dim statdate As DateTime = wo.CNOW

        sql = "insert into workorder (worktype, siteid, status, statusdate, changeby, changedate, reportedby, reportdate) " _
        + "values ('WR','" & sid & "','" & def & "', getDate(),'" & usr & "', '" & statdate & "','" & usr & "', getDate()) " _
        + "select @@identity"
        Try
            'wo.Update(sql)
            wonum = wo.Scalar(sql)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr615" , "wradd.aspx.vb")
 
            wo.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Try
            sql = "insert into wohist (wonum, wostatus, statusdate, changeby) values ('" & wonum & "','HOLD', '" & statdate & "', '" & usr & "')"
            wo.Update(sql)
        Catch ex As Exception

        End Try

        txtwo.Text = wonum
        txtwodesc.Text = ""
        lblwo.Value = wonum
        lbljump.Value = "no"
        lblstatus.Value = "HOLD"
        lblreqd.Value = "no"
    End Sub
    Private Sub SaveDesc()
        wonum = lblwo.Value
        Dim sh, lg As String
        Dim lgcnt As Integer
        sh = txtwodesc.Text
        'lg = txtlongdesc.Text
        sh = Replace(sh, "'", Chr(180), , , vbTextCompare)
        sh = Replace(sh, "--", "-", , , vbTextCompare)
        sh = Replace(sh, ";", ":", , , vbTextCompare)
        sh = Replace(sh, "/", " ", , , vbTextCompare)
        If Len(sh) > 79 Then
            sh = Mid(sh, 1, 79)
            lg = Mid(sh, 80)
        End If

        Dim cmd As New SqlCommand("exec usp_savewodesc @wonum, @sh, @lg")
        Dim param0 = New SqlParameter("@wonum", SqlDbType.Int)
        param0.Value = wonum
        cmd.Parameters.Add(param0)
        Dim param01 = New SqlParameter("@sh", SqlDbType.VarChar)
        If sh = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = sh
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@lg", SqlDbType.Text)
        If lg = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = lg
        End If
        cmd.Parameters.Add(param02)
        Try
            wo.UpdateHack(cmd)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr616" , "wradd.aspx.vb")
 
            wo.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        'sql = "update workorder set description = '" & sh & "' where wonum = '" & wonum & "'"
        'wo.Update(sql)
        'lg = Mid(lg, 80)
        'sql = "select count(*) from wolongdesc where wonum = '" & wonum & "'"
        'lgcnt = wo.Scalar(sql)
        'If lgcnt <> 0 Then
        'sql = "update wolongdesc set longdesc = '" & lg & "' where wonum = '" & wonum & "'"
        'wo.Update(sql)
        'Else
        'sql = "insert into wolongdesc (wonum, longdesc) values ('" & wonum & "','" & lg & "')"
        'wo.Update(sql)
        'End If
        'Else
        'sql = "update workorder set description = '" & sh & "' where wonum = '" & wonum & "'" _
        '+ "delete from wolongdesc where wonum = '" & wonum & "'"
        'wo.Update(sql)

        lblsav.Value = "0"
    End Sub
    Private Sub SaveDesc1()
        wonum = lblwo.Value
        Dim sh, lg As String
        Dim lgcnt As Integer
        sh = txtwodesc.Text
        lg = txtlongdesc.Text
        If Len(lg) > 79 Then
            sh = Mid(lg, 1, 79)
            sql = "update workorder set description = '" & sh & "' where wonum = '" & wonum & "'"
            wo.Update(sql)
            lg = Mid(lg, 80)
            sql = "select count(*) from wolongdesc where wonum = '" & wonum & "'"
            lgcnt = wo.Scalar(sql)
            If lgcnt <> 0 Then
                sql = "update wolongdesc set longdesc = '" & lg & "' where wonum = '" & wonum & "'"
                wo.Update(sql)
            Else
                sql = "insert into wolongdesc (wonum, longdesc) values ('" & wonum & "','" & lg & "')"
                wo.Update(sql)
            End If
        Else
            sql = "update workorder set description = '" & sh & "' where wonum = '" & wonum & "'" _
            + "delete from wolongdesc where wonum = '" & wonum & "'"
            wo.Update(sql)
        End If
        lblsav.Value = "0"
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1589.Text = axlabs.GetASPXPage("wradd.aspx", "lang1589")
        Catch ex As Exception
        End Try
        Try
            lang1590.Text = axlabs.GetASPXPage("wradd.aspx", "lang1590")
        Catch ex As Exception
        End Try
        Try
            lang1591.Text = axlabs.GetASPXPage("wradd.aspx", "lang1591")
        Catch ex As Exception
        End Try
        Try
            lang1592.Text = axlabs.GetASPXPage("wradd.aspx", "lang1592")
        Catch ex As Exception
        End Try
        Try
            lang1593.Text = axlabs.GetASPXPage("wradd.aspx", "lang1593")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            ovid206.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("wradd.aspx", "ovid206") & "', ABOVE, LEFT)")
            ovid206.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
