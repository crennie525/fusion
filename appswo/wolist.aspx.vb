

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class wolist
    Inherits System.Web.UI.Page
    Protected WithEvents ovid205 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang2968 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2967 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2966 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1546 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1545 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1544 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1543 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1542 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1541 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1540 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1539 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1538 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1537 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1536 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1535 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1534 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1533 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1532 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1531 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1530 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1529 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1528 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1527 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1526 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1525 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1524 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1523 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim mm As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 12
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim rowcnt As Integer
    Dim usid, nme, typ, eqid, fuid, coid, sid, cid, lid, did, clid, ro, rostr, Login, tab, issuper As String
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrchwo As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtsrchdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddstatus As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dddepts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcells As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddeq As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddfunc As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcomp As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtsup As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlead As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddnc As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblsrch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbllead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtfrom As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtto As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divywr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhide As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdwrs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tbwrs As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents trhide1 As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents trhide2 As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents ddref As System.Web.UI.WebControls.DropDownList
    Protected WithEvents trsa As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdsa As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbsa As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents tdr1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdr2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdr4 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnext As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbladm1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdrf As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbrf As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblrf As System.Web.UI.WebControls.Label
    Protected WithEvents trsa1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbsa1 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents tdssa As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbssa As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblissuper As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsearch As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcell As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trdepts As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trlocs3 As System.Web.UI.HtmlControls.HtmlTableRow

    Protected WithEvents tddept As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcell As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdco As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdloc3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdco3 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents lblsupstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblleadstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbls As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lble As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrt As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lblstart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnext As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblref As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents txtstart As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcomp As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdywr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                tab = Request.QueryString("tab").ToString
                lbltab.Value = tab
                tdwrs.Width = 674
                tbwrs.Width = 800
                divywr.Attributes.Add("class", "wolistsmall5")
            Catch ex As Exception
                divywr.Attributes.Add("class", "wolistsmall5")
            End Try
            lblhide.Value = "yes"
            'document.getElementById("lblhide").value="yes";
            'document.getElementById("trhide1").className="details";
            'document.getElementById("trhide2").className="details";
            'document.getElementById("divywr").className="wolistbig";
            trhide1.Attributes.Add("class", "details")
            trhide2.Attributes.Add("class", "details")

            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro <> "1" Then
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = mm.CheckROS(rostr, "wo")
                    lblro.Value = ro
                End If
            End If
            mm.Open()
            If Request.QueryString("jump").ToString = "yes" Then
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                If typ = "eq" Then
                    lbleqid.Value = Request.QueryString("eqid").ToString
                ElseIf typ = "fu" Then
                    lblfuid.Value = Request.QueryString("fu").ToString
                ElseIf typ = "fu" Then
                    lblcoid.Value = Request.QueryString("co").ToString
                End If
            End If
            Try
                sid = HttpContext.Current.Session("dfltps").ToString
                lblsid.Value = sid
                cid = HttpContext.Current.Session("comp").ToString
                lblcid.Value = cid
            Catch ex As Exception
                'use until session variables are eliminated
            End Try
            issuper = HttpContext.Current.Session("issuper").ToString()
            If issuper = "1" Then
                tdssa.Attributes.Add("class", "graylabel")

            Else
                tdssa.Attributes.Add("class", "bluelabel")

            End If
            lblissuper.Value = issuper
            If typ <> "wr" And tab <> "yes" Then
                trsa.Attributes.Add("class", "visible")
                trsa1.Attributes.Add("class", "details")
                lblrefresh.Value = "no"
                ddref.Enabled = False
                tdnext.InnerHtml = "N\A"
                ddref.Attributes.Add("onchange", "refref();")
                cbrf.Checked = False
                'rb1.Checked = True
                'rb2.Checked = False
                lbladm1.Value = "yes"
            Else
                trsa.Attributes.Add("class", "details")
                trsa1.Attributes.Add("class", "visible")
                'rb2.Checked = True
                'rb1.Checked = False
                lbladm1.Value = "no"
            End If
            PopSkills()
            'PopDepts(sid)
            'PopEq(sid)
            PopNC(sid)
            GetLists()
            BuildMyWR(PageNumber)
            mm.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                mm.Open()
                GetNext()
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                mm.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                BuildMyWR(PageNumber)
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                mm.Open()
                GetPrev()
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                mm.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                BuildMyWR(PageNumber)
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "sa" Then
                mm.Open()
                Try
                    PageNumber = txtpg.Value
                Catch ex As Exception
                    PageNumber = 1
                    txtpg.Value = PageNumber
                End Try
                BuildMyWR(PageNumber)
                mm.Dispose()
                lblret.Value = ""
            End If
            If Request.Form("lblpchk") = "loc" Then
                lblpchk.Value = ""
                mm.Open()
                lid = lbllid.Value
                PopEq(lid, "l")
                mm.Dispose()
            End If

        End If
    End Sub
    Private Sub CheckRF()
        typ = lbltyp.Value
        tab = lbltab.Value

        If typ <> "wr" And tab <> "yes" Then
            If cbrf.Checked = True Then
                lblrefresh.Value = "yes"
                tdr1.Attributes.Add("class", "bluelabel")
                tdr4.Attributes.Add("class", "bluelabel")
                ddref.Enabled = True
                lblrf.Text = "Stop Refresh"
            Else
                lblrefresh.Value = "no"
                tdr1.Attributes.Add("class", "graylabel")
                tdr4.Attributes.Add("class", "graylabel")
                ddref.Enabled = False
                lblrf.Text = "Start Refresh"
            End If
        End If
    End Sub
    Private Sub BuildMyWR(ByVal PageNumber As Integer)
        Dim sb As New StringBuilder
        Dim field, srch As String
        Dim srchi As Integer
        usid = HttpContext.Current.Session("uid").ToString()
        Dim uid1 As String = usid
        uid1 = Replace(uid1, "'", "''")
        nme = HttpContext.Current.Session("username").ToString()
        Dim nme1 As String = nme
        nme1 = Replace(nme, "'", "''")
        Dim sid1 As String = lblsid.Value
        typ = lbltyp.Value
        tab = lbltab.Value
        If lblsrch.Value = "yes" Then
            If typ = "wr" Then
                Filter = "reportedby like ''%'' and siteid = ''" & sid1 & "'' and worktype not in (''PM'') "
                FilterCNT = "reportedby like '%' and siteid = '" & sid1 & "' and worktype not in ('PM') "
            Else
                Filter = "reportedby like ''%'' and siteid = ''" & sid1 & "'' "
                FilterCNT = "reportedby like '%' and siteid = '" & sid1 & "' "
            End If

        Else
            If typ = "wr" Then
                Filter = "reportedby = ''" & nme1 & "'' and siteid = ''" & sid1 & "'' and worktype not in (''PM'') "
                FilterCNT = "reportedby = '" & nme1 & "' and siteid = '" & sid1 & "' and worktype not in ('PM') " ''CM',
            Else
                Filter = "reportedby = ''" & nme1 & "'' and siteid = ''" & sid1 & "'' "
                FilterCNT = "reportedby = '" & nme1 & "' and siteid = '" & sid1 & "' "
            End If

        End If
        Dim wonum As String = txtsrchwo.Text
        wonum = Replace(wonum, "'", "", , , vbTextCompare)
        wonum = Replace(wonum, "--", "", , , vbTextCompare)
        wonum = Replace(wonum, "-", "", , , vbTextCompare)
        wonum = Replace(wonum, ";", "", , , vbTextCompare)
        wonum = Replace(wonum, ":", "", , , vbTextCompare)
        wonum = Replace(wonum, "/", "", , , vbTextCompare)
        wonum = Replace(wonum, "%", "", , , vbTextCompare)
        wonum = Replace(wonum, "$", "", , , vbTextCompare)
        wonum = Replace(wonum, "#", "", , , vbTextCompare)
        wonum = Replace(wonum, "*", "", , , vbTextCompare)
        wonum = Replace(wonum, "&", "", , , vbTextCompare)
        wonum = Replace(wonum, "@", "", , , vbTextCompare)
        Dim woi As Integer
        Try
            woi = System.Convert.ToInt32(wonum)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr587", "wolist.aspx.vb")

            mm.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If wonum <> "" Then
            Filter += "and wonum = ''" & woi & "'' "
            FilterCNT += "and wonum = '" & woi & "' "
        End If
        Dim wodesc As String = txtsrchdesc.Text
        wodesc = Replace(wodesc, "'", Chr(180), , , vbTextCompare)
        wodesc = Replace(wodesc, "--", "-", , , vbTextCompare)
        wodesc = Replace(wodesc, ";", ":", , , vbTextCompare)
        wodesc = Replace(wodesc, "/", "", , , vbTextCompare)
        wodesc = Replace(wodesc, "%", "", , , vbTextCompare)
        wodesc = Replace(wodesc, "$", "", , , vbTextCompare)
        wodesc = Replace(wodesc, "#", "", , , vbTextCompare)
        wodesc = Replace(wodesc, "*", "", , , vbTextCompare)
        wodesc = Replace(wodesc, "&", "", , , vbTextCompare)
        wodesc = Replace(wodesc, "@", "", , , vbTextCompare)
        If wodesc <> "" Then
            Filter += "and description like ''%" & wodesc & "%'' "
            FilterCNT += "and description like '%" & wodesc & "%' "
        End If
        If ddstatus.SelectedIndex <> 0 And ddstatus.SelectedIndex <> -1 Then
            Filter += "and status = ''" & ddstatus.SelectedValue.ToString & "'' "
            FilterCNT += "and status = '" & ddstatus.SelectedValue.ToString & "' "
        Else
            If tab = "yes" Or typ = "wr" Then
                If cbsa1.Checked <> True Then
                    Filter += "and status not in (''COMP'',''CAN'',''CLOSE'') "
                    FilterCNT += "and status not in ('COMP','CAN','CLOSE') "
                End If
            Else
                If cbsa.Checked <> True Then
                    Filter += "and status not in (''COMP'',''CAN'',''CLOSE'') "
                    FilterCNT += "and status not in ('COMP','CAN','CLOSE') "
                End If
            End If

        End If
        If ddtype.SelectedIndex <> 0 And ddtype.SelectedIndex <> -1 Then
            Filter += "and worktype = ''" & ddtype.SelectedValue.ToString & "'' "
            FilterCNT += "and worktype = '" & ddtype.SelectedValue.ToString & "' "
        End If
        Dim wt As String = ddtype.SelectedValue.ToString
        If wonum = "" And wt <> "PM" Then
            Filter += "and worktype <> ''PM'' "
            FilterCNT += "and worktype <> 'PM' "
        End If
        If txtsup.Text <> "" Then
            Filter += "and supervisor = ''" & txtsup.Text & "'' "
            FilterCNT += "and supervisor = '" & txtsup.Text & "' "
        End If
        If txtlead.Text <> "" Then
            Filter += "and leadcraft = ''" & txtlead.Text & "'' "
            FilterCNT += "and leadcraft = '" & txtlead.Text & "' "
        End If

        If lblstart.Value <> "" Then
            Filter += "and targstartdate = ''" & lblstart.Value & "'' "
            FilterCNT += "and targstartdate = '" & lblstart.Value & "' "
        End If
        If lblnext.Value <> "" Then
            Filter += "and targcompdate = ''" & lblnext.Value & "'' "
            FilterCNT += "and targcompdate = '" & lblnext.Value & "' "
        End If

        Dim dfrom, dto As String
        dfrom = lbls.Value 'txtfrom.Text
        dto = lble.Value 'txtto.Text

        If dfrom <> "" And dto <> "" Then
            Filter += "and ((targstartdate >= ''" & dfrom & "'' and targstartdate <= ''" & dto & "'') " _
            + "or (targcompdate >= ''" & dfrom & "'' and targcompdate <= ''" & dto & "''))"
            FilterCNT += "and ((targstartdate >= '" & dfrom & "' and targstartdate <= '" & dto & "') " _
            + "or (targcompdate >= '" & dfrom & "' and targcompdate <= '" & dto & "'))"
        ElseIf dfrom <> "" And dto = "" Then
            Filter += "and ((targstartdate >= ''" & dfrom & "'') " _
            + "or (targcompdate >= ''" & dfrom & "''))"
            FilterCNT += "and ((targstartdate >= '" & dfrom & "') " _
            + "or (targcompdate >= '" & dfrom & "'))"
        ElseIf dto <> "" And dfrom = "" Then
            Filter += "and ((targstartdate <= ''" & dto & "'') " _
                       + "or (targcompdate <= ''" & dto & "''))"
            FilterCNT += "and ((targstartdate <= '" & dto & "') " _
            + "or (targcompdate <= '" & dto & "'))"
        End If
        ' And dddepts.SelectedIndex <> 0
        If lbldept.Value <> "" Then
            Filter += "and deptid = ''" & lbldid.Value & "'' "
            FilterCNT += "and deptid = '" & lbldid.Value & "' "
        End If
        If lblclid.Value <> "" Then
            Filter += "and cellid = ''" & lblclid.Value & "'' "
            FilterCNT += "and cellid = '" & lblclid.Value & "' "
        End If
        Dim par As String = lbllocstr.Value
        If lbllid.Value <> "" Then
            'e.eqnum in (select h.location from pmlocheir h where h.parent = 'CRTST')
            'Filter += "and e.locid = ''" & lbllid.Value & "'' "
            'FilterCNT += "and e.locid = '" & lbllid.Value & "' "
            Filter += "and eqnum in (select h.location from pmlocheir h where h.parent = ''" & par & "'')"
            FilterCNT += "and eqnum in (select h.location from pmlocheir h where h.parent = '" & par & "')"
            lblloc.Value = lbllocstr.Value
        End If
        If lblncid.Value <> "" Then
            Filter += "and ncid = ''" & lblncid.Value & "'' "
            FilterCNT += "and ncid = '" & lblncid.Value & "' "
        End If
        If lbleqid.Value <> "" Then
            Filter += "and eqid = ''" & lbleqid.Value & "'' "
            FilterCNT += "and eqid = '" & lbleqid.Value & "' "
        End If
        If lblfuid.Value <> "" Then
            Filter += "and funcid = ''" & lblfuid.Value & "'' "
            FilterCNT += "and funcid = '" & lblfuid.Value & "' "
        End If
        If lblcomid.Value <> "" Then
            Filter += "and comid = ''" & lblcomid.Value & "'' "
            FilterCNT += "and comid = '" & lblcomid.Value & "' "
        End If
        If ddskill.SelectedIndex <> 0 And ddskill.SelectedIndex <> -1 Then
            Filter += "and skillid = ''" & ddskill.SelectedValue.ToString & "'' "
            FilterCNT += "and skillid = '" & ddskill.SelectedValue.ToString & "' "
        End If
        issuper = lblissuper.Value
        If issuper = "1" Then
            If cbssa.Checked = True Then
                Filter += "and superid = ''" & usid & "'' "
                FilterCNT += "and superid = '" & usid & "' "
            End If
        End If

        sb.Append("<table cellspacing=""2""><tr height=""20""><td width=""20px"">&nbsp;" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""100px"">" & tmod.getlbl("cdlbl209", "wolist.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""70px"">" & tmod.getlbl("cdlbl210", "wolist.aspx.vb") & "</td>" & vbCrLf)
        If tab <> "yes" Then
            sb.Append("<td class=""thdrsingg plainlabel"" width=""200px"">" & tmod.getlbl("cdlbl211", "wolist.aspx.vb") & "</td>" & vbCrLf)
        End If



        sb.Append("<td class=""thdrsingg plainlabel"" width=""500px"">" & tmod.getlbl("cdlbl212", "wolist.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""70px"">" & tmod.getlbl("cdlbl213", "wolist.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""40px"">" & tmod.getlbl("cdlbl214", "wolist.aspx.vb") & "</td></tr>" & vbCrLf)

        Dim intPgNav, intPgCnt As Integer
        sql = "SELECT Count(*) FROM workorder where " & FilterCNT
        '@srch, @usid, @nme, @srchstr, @cnt, @start, @comp

        'Dim cmd As New SqlCommand("exec usp_searchwo @srch, @usid, @nme, @srchstr, @start, @comp")
        'Dim param = New SqlParameter("@srch", SqlDbType.Int)
        'param.Value = srchi
        'cmd.Parameters.Add(param)
        'Dim param1 = New SqlParameter("@usid", SqlDbType.VarChar)
        'param1.Value = usid
        'cmd.Parameters.Add(param1)
        'Dim param2 = New SqlParameter("@nme", SqlDbType.VarChar)
        'param2.Value = nme
        'cmd.Parameters.Add(param2)
        'Dim param3 = New SqlParameter("@srchstr", SqlDbType.VarChar)
        'param3.Value = txtsearch.Value
        'cmd.Parameters.Add(param3)
        'Dim param4 = New SqlParameter("@start", SqlDbType.DateTime)
        'If Len(start) = 0 Then
        'start = "1/1/1900"
        'End If
        'param4.Value = start
        'cmd.Parameters.Add(param4)
        'Dim param5 = New SqlParameter("@comp", SqlDbType.DateTime)
        'If Len(comp) = 0 Then
        'comp = "1/1/1900"
        'End If
        'param5.Value = comp
        'cmd.Parameters.Add(param5)
        'intPgCnt = mm.ScalarHack(cmd)
        intPgCnt = mm.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr588", "wolist.aspx.vb")

            mm.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        PageSize = "50"
        intPgNav = mm.PageCountRev(intPgCnt, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If

        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        Fields = "*, Convert(char(10),targstartdate, 101) as targstart, Convert(char(10),targcompdate, 101) as targcomp, " _
            + "phonenum = (select u.phonenum from pmsysusers u where u.username = workorder.reportedby), " _
            + "uemail = (select u.email from pmsysusers u where u.username = workorder.reportedby) "
        Tables = "workorder"
        PK = "wonum"
        Sort = "reportdate desc"
        dr = mm.GetPageHack(Tables, PK, Sort, PageNumber, PageSize, Fields, FilterCNT, Group)

        Dim wo, eid, sid, loc, did, clid, chk, fid, cid, nid, stat, isdown, ph, em As String
        Dim rowflag As Integer = 0
        Dim bg As String
        'Dim stat As String

        While dr.Read
            If rowflag = 0 Then
                bg = "ptransrow"
                rowflag = 1
            Else
                bg = "ptransrowblue"
                rowflag = "0"
            End If

            eid = dr.Item("eqid").ToString
            nid = dr.Item("ncid").ToString
            sid = dr.Item("siteid").ToString
            loc = dr.Item("locid").ToString
            did = dr.Item("deptid").ToString
            clid = dr.Item("cellid").ToString
            cid = dr.Item("comid").ToString
            fid = dr.Item("funcid").ToString
            wt = dr.Item("worktype").ToString

            ph = dr.Item("phonenum").ToString
            em = dr.Item("uemail").ToString

            isdown = dr.Item("isdown").ToString
            Dim cr, crl As String
            If isdown = "1" Then
                cr = "plainlabelred"
                crl = "A1R"
            Else
                cr = "plainlabel"
                crl = "A1U"
            End If

            stat = dr.Item("status").ToString

            Dim typ, jp, pm As String
            typ = dr.Item("worktype").ToString
            jp = dr.Item("jpid").ToString
            pm = dr.Item("pmid").ToString

            If clid <> "" Then
                chk = "yes"
            Else
                chk = "no"
            End If

            wo = dr.Item("wonum").ToString
            stat = dr.Item("status").ToString
            sb.Append("<tr ><td class=""" & bg & """><img id='i" + wo + "' src=""../images/appbuttons/bgbuttons/plus.gif"" " & vbCrLf)
            sb.Append("onclick=""fclose('t" + wo + "', 'i" + wo + "');""></td>" & vbCrLf)
            'If stat = "HOLD" OrElse stat = "WAPPR" Then
            sb.Append("<td class=""" & bg & """><a class=""" & crl & """ href=""#"" onclick=""gotoreq('" & wo & "', '" & eid & "', '" & fid & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & loc & "', '" & nid & "','" & stat & "','" & wt & "')"">" & wo & "</a></td>" & vbCrLf)
            'ElseIf stat = "CAN" Then
            'sb.Append("<td class=""plainlabel"">" & wo & "</td>" & vbCrLf)
            'Else
            'sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""gotoreq('../woreq/woreview2.aspx?wo=" + wo + "')"">" & wo & "</a></td>" & vbCrLf)
            'End If
            sb.Append("<td class=""" & bg & " " & cr & """>" & typ & "</td>" & vbCrLf)
            If tab <> "yes" Then
                sb.Append("<td class=""" & bg & " " & cr & """>" & dr.Item("chargenum").ToString & "</td>" & vbCrLf)

            End If


            sb.Append("<td class=""" & bg & " " & cr & """>" & dr.Item("description").ToString & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """>" & dr.Item("status").ToString & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ align=""center""><a href=""#"" onclick=""printwo('" & wo & "','" & typ & "','" & jp & "','" & pm & "')"" onmouseover=""return overlib('" & tmod.getov("cov174", "wolist.aspx.vb") & "', ABOVE, LEFT)"" onmouseout=""return nd()""><img src=""../images/appbuttons/minibuttons/printx.gif"" border=""0""></a></td></tr>" & vbCrLf)
            sb.Append("<tr class=""details"" id='t" + wo + "'><td>&nbsp;</td><td colspan=""5"">" & vbCrLf)

            sb.Append("<table border=""0"">")
            sb.Append("<tr><td class=""label " & bg & """ width=""100"">" & tmod.getlbl("cdlbl215", "wolist.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""plainlabel " & bg & """ width=""120"">" & dr.Item("targstart").ToString & "</td>")
            sb.Append("<td width=""130"" class=""label " & bg & """>" & tmod.getlbl("cdlbl216", "wolist.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel " & bg & """ width=""130"">" & dr.Item("targcomp").ToString & "</td>")
            sb.Append("<td width=""40"" class=""" & bg & """></td><td width=""180"" class=""" & bg & """></td></tr>")

            sb.Append("<tr><td class=""label " & bg & """ width=""100"">" & tmod.getlbl("cdlbl217", "wolist.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""plainlabel " & bg & """ width=""130"">" & dr.Item("schedstart").ToString & "</td>")
            sb.Append("<td width=""120"" class=""label " & bg & """>" & tmod.getlbl("cdlbl218", "wolist.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel " & bg & """ width=""130"">" & dr.Item("schedfinish").ToString & "</td>")
            sb.Append("<td width=""40"" class=""" & bg & """></td><td width=""180"" class=""" & bg & """></td></tr>")

            sb.Append("<tr><td class=""label " & bg & """ width=""100"">" & tmod.getlbl("cdlbl219", "wolist.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""plainlabel " & bg & """ width=""130"">" & dr.Item("actstart").ToString & "</td>")
            sb.Append("<td width=""120"" class=""label " & bg & """>" & tmod.getlbl("cdlbl220", "wolist.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel " & bg & """ width=""130"">" & dr.Item("actfinish").ToString & "</td>")
            sb.Append("<td width=""40"" class=""" & bg & """></td><td width=""180"" class=""" & bg & """></td></tr>")

            sb.Append("<tr><td class=""label " & bg & """ width=""100"">" & tmod.getlbl("cdlbl221", "wolist.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""plainlabel " & bg & """ width=""130"">" & dr.Item("supervisor").ToString & "</td>")
            sb.Append("<td width=""120"" class=""label " & bg & """>" & tmod.getlbl("cdlbl222", "wolist.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel " & bg & """ width=""130"">" & ph & "</td>")
            sb.Append("<td class=""label " & bg & """ width=""40"">" & tmod.getlbl("cdlbl223", "wolist.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel " & bg & """ width=""180""><a href=""mailto:""" & em & """>" & em & "</a></td></tr></table></td></tr>")
        End While
        dr.Close()
        sb.Append("</tr></table>123")
        divywr.InnerHtml = sb.ToString
        'lblpg.Text = "Page " & dgwork.CurrentPageIndex + 1 & " of " & dgwork.PageCount
        'Catch ex As Exception

        'End Try
        CheckRF()
        Dim who As String = lblwho.Value
        If who = "depts" Then
            trdepts.Attributes.Add("class", "view")
            'tddept.InnerHtml = lbldept.Value
            'tdcell.InnerHtml = lblcell.Value
            'tdeq.InnerHtml = lbleq.Value
            'tdfu.InnerHtml = lblfu.Value
            'tdco.InnerHtml = lblcomp.Value
        Else
            trdepts.Attributes.Add("class", "details")
        End If
        If who = "locs" Then
            trlocs3.Attributes.Add("class", "view")
            'tdloc3.InnerHtml = lblloc.Value
            'tdeq3.InnerHtml = lbleq.Value
            'tdfu3.InnerHtml = lblfu.Value
            'tdco3.InnerHtml = lblcomp.Value
        Else
            trlocs3.Attributes.Add("class", "details")
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            BuildMyWR(PageNumber)
        Catch ex As Exception
            mm.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr589", "wolist.aspx.vb")

            mm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            BuildMyWR(PageNumber)
        Catch ex As Exception
            mm.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr590", "wolist.aspx.vb")

            mm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        lblsrch.Value = "yes"
        PageNumber = 1
        mm.Open()
        BuildMyWR(PageNumber)
        mm.Dispose()
        Dim hide As String = lblhide.Value
        If hide = "yes" Then
            lblhide.Value = "no"
        Else
            lblhide.Value = "yes"
        End If
        txtcomp.Text = lblnext.Value
        txtstart.Text = lblstart.Value
        txtfrom.Text = lbls.Value
        txtto.Text = lble.Value
    End Sub
    Private Sub ibtnsrch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If Len(txtsearch.Value) <> 0 Then
                If Len(txtsearch.Value) > 50 Then
                    Dim strMessage As String = tmod.getmsg("cdstr591", "wolist.aspx.vb")

                    mm.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    PageNumber = 1
                    mm.Open()
                    BuildMyWR(PageNumber)
                    mm.Dispose()
                End If
            End If
        Catch ex As Exception
            mm.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr592", "wolist.aspx.vb")

            mm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try

    End Sub
    Private Sub GetLists()
        'get default
        Dim def As String = ""
        'sql = "select wostatus from wostatus where compid = '" & cid & "' and isdefault = 1"
        'dr = mm.GetRdrData(sql)
        'While dr.Read
        'def = dr.Item("wostatus").ToString
        'End While
        'dr.Close()
        sql = "select wostatus from wostatus where compid = '" & cid & "' and active = 1"
        dr = mm.GetRdrData(sql)
        ddstatus.DataSource = dr
        ddstatus.DataTextField = "wostatus"
        ddstatus.DataValueField = "wostatus"
        ddstatus.DataBind()
        dr.Close()
        If def <> "" Then
            ddstatus.SelectedValue = def
        Else
            ddstatus.Items.Insert(0, New ListItem("Select"))
            ddstatus.Items(0).Value = 0
        End If

        sql = "select wotype from wotype where compid = '" & cid & "' and active = 1"
        dr = mm.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataTextField = "wotype"
        ddtype.DataValueField = "wotype"
        ddtype.DataBind()
        dr.Close()
        ddtype.Items.Insert(0, New ListItem("Select"))
        ddtype.Items(0).Value = 0

    End Sub
    Private Sub PopSkills()
        Dim sid As String = lblsid.Value
        Dim cid As String = lblcid.Value
        Dim scnt As Integer
        sql = "select count(*) from pmSiteSkills where siteid = '" & sid & "'"
        scnt = mm.Scalar(sql)
        If scnt <> 0 Then
            sql = "select skillid, skill " _
            + "from pmSiteSkills where compid = '" & cid & "' and siteid = '" & sid & "'"
        Else
            sql = "select skillid, skill " _
            + "from pmSkills where compid = '" & cid & "' order by compid"
        End If
        dr = mm.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("Select"))
        ddskill.Items(0).Value = 0

    End Sub
    Private Sub PopDepts(ByVal site As String)
        Dim dt, val As String
        Dim filt As String = " where siteid = '" & site & "'"
        sql = "select count(*) from Dept where siteid = '" & site & "'"
        Dim dcnt As Integer = mm.Scalar(sql)
        If dcnt > 0 Then
            dt = "Dept"
            val = "dept_id , dept_line "
            dr = mm.GetList(dt, val, filt)
            dddepts.DataSource = dr
            dddepts.DataTextField = "dept_line"
            dddepts.DataValueField = "dept_id"
            dddepts.DataBind()
            dr.Close()
            dddepts.Enabled = True
        Else
            'Dim strMessage As String =  tmod.getmsg("cdstr593" , "wolist.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            dddepts.Enabled = False
        End If
        dddepts.Items.Insert(0, "Select Department")
    End Sub
    Private Sub PopEq(ByVal sid As String)
        Dim dt, val As String
        Dim eqcnt As Integer
        Dim filt As String
        sql = "select count(*) from equipment where siteid = '" & sid & "'"
        filt = " where siteid = '" & sid & "'"
        eqcnt = mm.Scalar(sql)
        If eqcnt > 0 Then
            dt = "equipment"
            val = "eqid, eqnum "
            dr = mm.GetList(dt, val, filt)
            ddeq.DataSource = dr
            ddeq.DataTextField = "eqnum"
            ddeq.DataValueField = "eqid"
            ddeq.DataBind()
            dr.Close()
            ddeq.Items.Insert(0, "Select Equipment")
            'eqdiv.Style.Add("display", "block")
            'eqdiv.Style.Add("visibility", "visible")
            ddeq.Enabled = True
        Else
            'Dim strMessage As String =  tmod.getmsg("cdstr594" , "wolist.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ddeq.Items.Insert(0, "Select Equipment")
            'eqdiv.Style.Add("display", "block")
            'eqdiv.Style.Add("visibility", "visible")
            ddeq.Enabled = False
        End If

    End Sub
    Private Sub PopNC(ByVal sid As String)
        Dim dt, val As String
        Dim eqcnt As Integer
        Dim filt As String
        sql = "select count(*) from noncritical where siteid = '" & sid & "'"
        filt = " where siteid = '" & sid & "'"
        eqcnt = mm.Scalar(sql)
        If eqcnt > 0 Then
            dt = "noncritical"
            val = "ncid, ncnum "
            dr = mm.GetList(dt, val, filt)
            ddnc.DataSource = dr
            ddnc.DataTextField = "ncnum"
            ddnc.DataValueField = "ncid"
            ddnc.DataBind()
            dr.Close()
            ddnc.Items.Insert(0, "Select Asset")
            'eqdiv.Style.Add("display", "block")
            'eqdiv.Style.Add("visibility", "visible")
            ddnc.Enabled = True
        Else
            'Dim strMessage As String =  tmod.getmsg("cdstr595" , "wolist.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ddnc.Items.Insert(0, "Select Asset")
            'eqdiv.Style.Add("display", "block")
            'eqdiv.Style.Add("visibility", "visible")
            ddnc.Enabled = False
        End If

    End Sub
    Public Function CellCheck(ByVal filt As String) As String
        Dim cells As String
        Dim cellcnt As Integer
        sql = "select count(*) from Cells where Dept_ID = '" & filt & "'"
        cellcnt = mm.Scalar(sql)
        Dim nocellcnt As Integer
        If cellcnt = 0 Then
            cells = "no"
        ElseIf cellcnt = 1 Then
            sql = "select count(*) from Cells where Dept_ID = '" & filt & "' and cell_name = 'No Cells'"
            nocellcnt = mm.Scalar(sql)
            If nocellcnt = 1 Then
                cells = "no"
            Else
                cells = "yes"
            End If
        Else
            cells = "yes"
        End If
        Return cells
    End Function
    Private Sub dddepts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dddepts.SelectedIndexChanged
        Dim dept As String = dddepts.SelectedValue.ToString
        lbldept.Value = dept

        mm.Open()
        If dddepts.SelectedIndex <> 0 Then

            Dim deptname As String = dddepts.SelectedItem.ToString
            Filter = dept
            Dim chk As String = CellCheck(Filter)
            If chk = "no" Then
                lblchk.Value = "no"
                lid = lbllid.Value
                If lid <> "" And lid <> "0" Then
                    PopEq(lid, "l")
                    PopNC(lid, "l")
                Else
                    PopEq(dept, "d")
                    PopNC(dept, "d")
                End If

            Else
                lblchk.Value = "yes"
                PopCells(dept)
                ddeq.Enabled = False
            End If
        Else
            lbldept.Value = ""
        End If
        mm.Dispose()
    End Sub
    Private Sub PopEq(ByVal var As String, ByVal typ As String)
        Dim filt, dt, val As String
        lid = lbllid.Value
        did = lbldept.Value
        clid = lblclid.Value
        Dim eqcnt As Integer
        If typ = "d" Then
            If lid = "" Then
                sql = "select count(*) from equipment where dept_id = '" & did & "'"
                filt = " where dept_id = '" & did & "'"
            Else
                sql = "select count(*) from equipment where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            End If

        ElseIf typ = "c" Then
            If lid = "" Then
                sql = "select count(*) from equipment where cellid = '" & clid & "'"
                filt = " where cellid = '" & clid & "'"
            Else
                sql = "select count(*) from equipment where cellid = '" & clid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & clid & "' and locid = '" & clid & "'"
            End If

        ElseIf typ = "l" Then
            If clid <> "" Then
                sql = "select count(*) from equipment where cellid = '" & clid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & clid & "' and locid = '" & lid & "'"
            ElseIf did <> "" Then
                sql = "select count(*) from equipment where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            Else
                sql = "select count(*) from equipment where locid = '" & lid & "'"
                filt = " where locid = '" & lid & "'"
            End If

        End If
        eqcnt = mm.Scalar(sql)
        If eqcnt > 0 Then
            dt = "equipment"
            val = "eqid, eqnum "

            dr = mm.GetList(dt, val, filt)
            ddeq.DataSource = dr
            ddeq.DataTextField = "eqnum"
            ddeq.DataValueField = "eqid"
            ddeq.DataBind()
            dr.Close()
            ddeq.Items.Insert(0, "Select Equipment")
            ddeq.Enabled = True
        Else
            'Dim strMessage As String =  tmod.getmsg("cdstr596" , "wolist.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ddeq.Items.Insert(0, "Select Equipment")
            ddeq.Enabled = False
        End If
        GetLoc()
    End Sub
    Private Sub PopNC(ByVal var As String, ByVal typ As String)
        Dim filt, dt, val As String
        lid = lbllid.Value
        did = lbldept.Value
        clid = lblclid.Value
        Dim eqcnt As Integer
        If typ = "d" Then
            If lid = "" Then
                sql = "select count(*) from noncritical where dept_id = '" & did & "'"
                filt = " where dept_id = '" & did & "'"
            Else
                sql = "select count(*) from noncritical where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            End If

        ElseIf typ = "c" Then
            If lid = "" Then
                sql = "select count(*) from noncritical where cellid = '" & clid & "'"
                filt = " where cellid = '" & clid & "'"
            Else
                sql = "select count(*) from noncritical where cellid = '" & clid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & clid & "' and locid = '" & clid & "'"
            End If

        ElseIf typ = "l" Then
            If clid <> "" Then
                sql = "select count(*) from noncritical where cellid = '" & clid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & clid & "' and locid = '" & lid & "'"
            ElseIf did <> "" Then
                sql = "select count(*) from noncritical where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            Else
                sql = "select count(*) from noncritical where locid = '" & lid & "'"
                filt = " where locid = '" & lid & "'"
            End If

        End If
        eqcnt = mm.Scalar(sql)
        If eqcnt > 0 Then
            dt = "noncritical"
            val = "ncid, ncnum "

            dr = mm.GetList(dt, val, filt)
            ddnc.DataSource = dr
            ddnc.DataTextField = "ncnum"
            ddnc.DataValueField = "ncid"
            ddnc.DataBind()
            dr.Close()
            ddnc.Items.Insert(0, "Select Asset")
            ddnc.Enabled = True
        Else
            'Dim strMessage As String =  tmod.getmsg("cdstr597" , "wolist.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ddnc.Items.Insert(0, "Select Asset")
            ddnc.Enabled = False
        End If
        GetLoc()
    End Sub
    Private Sub GetLoc(Optional ByVal lid As String = "0")
        If lid <> 0 Then
            lid = lbllid.Value
        End If

        If lid <> "" And lid <> "0" Then
            sql = "select location, description from pmlocations where locid = '" & lid & "'"
            dr = mm.GetRdrData(sql)
            While dr.Read
                lblloc.Value = dr.Item("location").ToString
            End While
            dr.Close()
        End If

    End Sub
    Private Sub PopCells(ByVal dept As String)
        Dim filt, dt, val As String
        filt = " where dept_id = " & dept
        dt = "Cells"
        val = "cellid , cell_name "
        dr = mm.GetList(dt, val, filt)
        ddcells.DataSource = dr
        ddcells.DataTextField = "cell_name"
        ddcells.DataValueField = "cellid"
        ddcells.DataBind()
        dr.Close()
        ddcells.Items.Insert(0, "Select Station/Cell")
    End Sub

    Private Sub ddcells_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcells.SelectedIndexChanged
        Dim cell As String = ddcells.SelectedValue.ToString
        If ddcells.SelectedIndex <> 0 Then
            Try
                mm.Open()
                lblclid.Value = cell
                PopEq(cell, "c")
                PopNC(cell, "c")
                ddeq.Enabled = True
                mm.Dispose()
            Catch ex As Exception
                mm.Dispose()
                'Dim strMessage As String =  tmod.getmsg("cdstr598" , "wolist.aspx.vb")

                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try
        Else
            lblclid.Value = ""
        End If
    End Sub

    Private Sub ddeq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddeq.SelectedIndexChanged
        Dim eq As String = ddeq.SelectedValue.ToString
        lbleqid.Value = eq
        Dim loctype As String
        If ddeq.SelectedIndex <> 0 Then
            mm.Open()
            sql = "select * from equipment where eqid = '" & eq & "'"
            dr = mm.GetRdrData(sql)
            While dr.Read
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                lid = dr.Item("locid").ToString
                loctype = dr.Item("loctype").ToString
            End While
            dr.Close()
            lbldept.Value = did
            lblclid.Value = clid
            lbllid.Value = lid
            If loctype = "reg" Or loctype = "dloc" Then
                If dddepts.SelectedIndex = 0 Then
                    dddepts.SelectedValue = did
                    Dim chk As String = CellCheck(Filter)
                    If chk = "no" Then
                        lblchk.Value = "no"
                    Else
                        lblchk.Value = "yes"
                        PopCells(did)
                        If clid <> "" Then
                            ddcells.SelectedValue = clid
                        End If
                    End If
                End If
            End If
            If loctype = "loc" Or loctype = "dloc" Then
                GetLoc(lid)
            End If
            PopFunc(eq)
            mm.Dispose()
        End If
    End Sub

    Private Sub ddnc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddnc.SelectedIndexChanged
        Dim eq As String = ddnc.SelectedValue.ToString
        lblncid.Value = eq
        Dim loctype As String
        If ddeq.SelectedIndex <> 0 Then
            mm.Open()
            sql = "select * from noncritical where ncid = '" & eq & "'"
            dr = mm.GetRdrData(sql)
            While dr.Read
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                lid = dr.Item("locid").ToString
                loctype = dr.Item("loctype").ToString
            End While
            dr.Close()
            lbldept.Value = did
            lblclid.Value = clid
            lbllid.Value = lid
            If loctype = "reg" Or loctype = "dloc" Then
                If dddepts.SelectedIndex = 0 Then
                    dddepts.SelectedValue = did
                    Dim chk As String = CellCheck(Filter)
                    If chk = "no" Then
                        lblchk.Value = "no"
                    Else
                        lblchk.Value = "yes"
                        PopCells(did)
                        If clid <> "" Then
                            ddcells.SelectedValue = clid
                        End If
                    End If
                End If
            End If
            If loctype = "loc" Or loctype = "dloc" Then
                GetLoc(lid)
            End If
            mm.Dispose()
        Else
            lblncid.Value = ""
        End If
    End Sub
    Private Sub PopComp()
        fuid = lblfuid.Value
        sql = "select comid, compnum + ' - ' + isnull(compdesc, '') as compnum " _
          + "from components where func_id = '" & fuid & "' order by compnum desc"
        dr = mm.GetRdrData(sql)
        ddcomp.DataSource = dr
        ddcomp.DataTextField = "compnum"
        ddcomp.DataValueField = "comid"
        ddcomp.DataBind()
        dr.Close()
        ddcomp.Items.Insert(0, New ListItem("Select"))
        ddcomp.Items(0).Value = 0
    End Sub
    Private Sub ddfunc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddfunc.SelectedIndexChanged
        fuid = ddfunc.SelectedValue.ToString
        lblfuid.Value = fuid
        If ddfunc.SelectedIndex <> 0 Then
            mm.Open()
            PopComp()
            mm.Dispose()
        Else
            lblfuid.Value = ""
        End If
    End Sub
    Private Sub PopFunc(ByVal eq As String)
        Dim dt, val, filt As String
        Dim fucnt As Integer
        sql = "select count(*) from Functions where eqid = '" & eq & "'"
        fucnt = mm.Scalar(sql)
        If fucnt = 0 Then
            ddfunc.Items.Insert(0, "Select Function")
            ddfunc.Enabled = "False"
            Dim strMessage As String = tmod.getmsg("cdstr599", "wolist.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            dt = "Functions"
            val = "func_id, func"
            filt = " where eqid = '" & eq & "'"
            dr = mm.GetList(dt, val, filt)
            ddfunc.DataSource = dr
            ddfunc.DataTextField = "func"
            ddfunc.DataValueField = "func_id"
            ddfunc.DataBind()
            dr.Close()
            ddfunc.Items.Insert(0, "Select Function")
            ddfunc.Enabled = "True"
        End If
    End Sub











    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1523.Text = axlabs.GetASPXPage("wolist.aspx", "lang1523")
        Catch ex As Exception
        End Try
        Try
            lang1524.Text = axlabs.GetASPXPage("wolist.aspx", "lang1524")
        Catch ex As Exception
        End Try
        Try
            lang1525.Text = axlabs.GetASPXPage("wolist.aspx", "lang1525")
        Catch ex As Exception
        End Try
        Try
            lang1526.Text = axlabs.GetASPXPage("wolist.aspx", "lang1526")
        Catch ex As Exception
        End Try
        Try
            lang1527.Text = axlabs.GetASPXPage("wolist.aspx", "lang1527")
        Catch ex As Exception
        End Try
        Try
            lang1528.Text = axlabs.GetASPXPage("wolist.aspx", "lang1528")
        Catch ex As Exception
        End Try
        Try
            lang1529.Text = axlabs.GetASPXPage("wolist.aspx", "lang1529")
        Catch ex As Exception
        End Try
        Try
            lang1530.Text = axlabs.GetASPXPage("wolist.aspx", "lang1530")
        Catch ex As Exception
        End Try
        Try
            lang1531.Text = axlabs.GetASPXPage("wolist.aspx", "lang1531")
        Catch ex As Exception
        End Try
        Try
            lang1532.Text = axlabs.GetASPXPage("wolist.aspx", "lang1532")
        Catch ex As Exception
        End Try
        Try
            lang1533.Text = axlabs.GetASPXPage("wolist.aspx", "lang1533")
        Catch ex As Exception
        End Try
        Try
            lang1534.Text = axlabs.GetASPXPage("wolist.aspx", "lang1534")
        Catch ex As Exception
        End Try
        Try
            lang1535.Text = axlabs.GetASPXPage("wolist.aspx", "lang1535")
        Catch ex As Exception
        End Try
        Try
            lang1536.Text = axlabs.GetASPXPage("wolist.aspx", "lang1536")
        Catch ex As Exception
        End Try
        Try
            lang1537.Text = axlabs.GetASPXPage("wolist.aspx", "lang1537")
        Catch ex As Exception
        End Try
        Try
            lang1538.Text = axlabs.GetASPXPage("wolist.aspx", "lang1538")
        Catch ex As Exception
        End Try
        Try
            lang1539.Text = axlabs.GetASPXPage("wolist.aspx", "lang1539")
        Catch ex As Exception
        End Try
        Try
            lang1540.Text = axlabs.GetASPXPage("wolist.aspx", "lang1540")
        Catch ex As Exception
        End Try
        Try
            lang1541.Text = axlabs.GetASPXPage("wolist.aspx", "lang1541")
        Catch ex As Exception
        End Try
        Try
            lang1542.Text = axlabs.GetASPXPage("wolist.aspx", "lang1542")
        Catch ex As Exception
        End Try
        Try
            lang1543.Text = axlabs.GetASPXPage("wolist.aspx", "lang1543")
        Catch ex As Exception
        End Try
        Try
            lang1544.Text = axlabs.GetASPXPage("wolist.aspx", "lang1544")
        Catch ex As Exception
        End Try
        Try
            lang1545.Text = axlabs.GetASPXPage("wolist.aspx", "lang1545")
        Catch ex As Exception
        End Try
        Try
            lang1546.Text = axlabs.GetASPXPage("wolist.aspx", "lang1546")
        Catch ex As Exception
        End Try
        Try
            lang2966.Text = axlabs.GetASPXPage("wolist.aspx", "lang2966")
        Catch ex As Exception
        End Try
        Try
            lang2967.Text = axlabs.GetASPXPage("wolist.aspx", "lang2967")
        Catch ex As Exception
        End Try
        Try
            lang2968.Text = axlabs.GetASPXPage("wolist.aspx", "lang2968")
        Catch ex As Exception
        End Try
        Try
            lblrf.Text = axlabs.GetASPXPage("wolist.aspx", "lblrf")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            ovid205.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("wolist.aspx", "ovid205") & "')")
            ovid205.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            tdwrs.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("wolist.aspx", "tdwrs") & "', ABOVE, LEFT)")
            tdwrs.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
