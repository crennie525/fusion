<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wofm.aspx.vb" Inherits="lucy_r12.wofm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wofm</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/wofmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="scrolltop();">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" width="700" style="left: 0px; position: absolute; top: 2px"
        cellspacing="0" cellpadding="2">
        <tr>
            <td>
                <img src="../images/appbuttons/minibuttons/6PX.gif">
            </td>
        </tr>
        <tr height="22">
            <td class="thdrhov label" id="tdf" onclick="gettab('f');" width="100" runat="server">
                <asp:Label ID="lang1393" runat="server">Failure Modes</asp:Label>
            </td>
            <td class="thdr label" id="tdm" onclick="gettab('m');" width="100" runat="server">
                <asp:Label ID="lang1394" runat="server">Measurements</asp:Label>
            </td>
            <td width="500">
            </td>
        </tr>
        <tr>
            <td class="tdborder" id="wo" colspan="4" runat="server">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <iframe id="ifwo" src="wohold.htm" frameborder="no" width="750" scrolling="auto"
                                height="380" runat="server"></iframe>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblwo" runat="server">
    <input type="hidden" id="lblfmstr" runat="server">
    <input type="hidden" id="lblmstr" runat="server">
    <input type="hidden" id="lblsubmit" runat="server">
    <input type="hidden" id="lblupsav" runat="server">
    <input type="hidden" id="lblstat" runat="server">
    <input id="xCoord" type="hidden" name="xCoord" runat="server">
    <input id="yCoord" type="hidden" name="yCoord" runat="server">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
