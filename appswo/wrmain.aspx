<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wrmain.aspx.vb" Inherits="lucy_r12.wrmain" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wrmain</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/wrmainaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkhref();" class="tbg">
    <form id="form1" method="post" runat="server">
    <table class="details" style="z-index: 1; left: 7px; position: absolute; top: 78px"
        cellspacing="0" cellpadding="2" width="1040">
        <tr>
            <td class="bigbluebold" valign="middle" align="center" width="100%" height="100%">
                <asp:Label ID="lang1612" runat="server">Sorry, This Module Not Available In Demo Mode</asp:Label>
            </td>
        </tr>
    </table>
    <table style="z-index: 1; left: 7px; position: absolute; top: 78px" cellspacing="0"
        cellpadding="2" width="1040">
        <tr>
            <td class="thdrhov label" id="tdpm" onclick="gettab('pm');" width="180">
                <asp:Label ID="lang1613" runat="server">My Work Requests</asp:Label>
            </td>
            <td class="thdr label" id="tdeq" onclick="gettab('eq');" width="180">
                <asp:Label ID="lang1614" runat="server">Add\Edit Work Requests</asp:Label>
            </td>
            <td width="480">
                &nbsp;
            </td>
            <td width="3">
                &nbsp;
            </td>
            <td class="details" id="e2" align="left" width="204" colspan="2">
                <asp:Label ID="lang1615" runat="server">Plant Site News</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tdborder" id="pm" colspan="3">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <iframe id="ifnew" src="wolist.aspx?jump=yes&amp;typ=wr&reqd=no" frameborder="no"
                                width="1080" scrolling="yes" height="532" runat="server" style="background-color: transparent"
                                allowtransparency></iframe>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="details" id="eq" valign="top" colspan="3">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <iframe id="ifmain" src="wrman2.aspx?sid=&wo=" frameborder="no" width="780" scrolling="no"
                                height="532" runat="server" style="background-color: transparent" allowtransparency>
                            </iframe>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
            <td valign="top" colspan="2" rowspan="2" class="details">
                <table cellspacing="0">
                    <tr>
                        <td colspan="2">
                            <iframe id="ifarch" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                                margin: 0px; border-top-style: none; padding-top: 0px; border-right-style: none;
                                border-left-style: none; background-color: transparent; border-bottom-style: none"
                                src="newssamp.aspx" frameborder="no" width="220" scrolling="no" height="476"
                                runat="server" allowtransparency></iframe>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblhref" type="hidden" name="lblhref" runat="server">
    <input id="lblchng" type="hidden" name="lblchng" runat="server">
    <input id="lbldesc" type="hidden" name="lbldesc" runat="server"><input id="lblwo"
        type="hidden" name="lblwo" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lbldid" type="hidden" name="lbldid" runat="server">
    <input id="lblclid" type="hidden" name="lblclid" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lblgetarch" type="hidden" name="lblgetarch" runat="server">
    <input id="lbltyp" type="hidden" name="lbltyp" runat="server">
    <input id="lbllid" type="hidden" name="lbllid" runat="server">
    <input id="lbljump" type="hidden" name="lbljump" runat="server">
    <input id="lblncid" type="hidden" name="lblncid" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblissuper" runat="server" name="lblissuper">
    <input type="hidden" id="Hidden1" runat="server" name="lblfslang">
    <input type="hidden" id="lblislabor" runat="server">
    <input type="hidden" id="lblfiltret" runat="server">
    <input type="hidden" id="lblusid" runat="server">
    <input type="hidden" id="lblnme" runat="server">
    <input type="hidden" id="lblisplanner" runat="server">
    <input type="hidden" id="Hidden2" runat="server">
    <input type="hidden" id="lblwho" runat="server"><input type="hidden" id="lblLogged_In"
        runat="server" name="lblLogged_In">
    <input type="hidden" id="lblms" runat="server" name="lblms"><input type="hidden"
        id="lbluid" runat="server">
    <input type="hidden" id="lblappstr" runat="server" name="lblappstr">
    </form>
    <uc1:mmenu1 ID="Mmenu11" runat="server"></uc1:mmenu1>
</body>
</html>
