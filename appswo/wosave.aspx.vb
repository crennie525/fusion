Imports System.Data.SqlClient
Public Class wosave
    Inherits System.Web.UI.Page
    Dim wo, wodesc, estw, estj, pri, estmp, cbsv, cblv, elead, olead, wt As String
    Dim sql As String
    Dim swo As New Utilities
    Protected WithEvents lblerr As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim tmod As New transmod

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            wo = Request.QueryString("wo").ToString
            estw = Request.QueryString("estw").ToString
            estj = Request.QueryString("estj").ToString
            estmp = Request.QueryString("estmp").ToString
            wodesc = Request.QueryString("wodesc").ToString
            pri = Request.QueryString("pri").ToString

            cbsv = Request.QueryString("cbsv").ToString
            cblv = Request.QueryString("cblv").ToString
            elead = Request.QueryString("elead").ToString
            olead = Request.QueryString("olead").ToString

            wt = Request.QueryString("wt").ToString

            swo.Open()
            Dim tnchk As Long
            Try
                tnchk = System.Convert.ToInt64(elead)
            Catch ex As Exception
                elead = "0"
            End Try

            'If wt <> "PM" And wt <> "TPM" Then
            If elead = "0" Then
                sql = "update workorder set leadealert = '" & cblv & "', supealert = '" & cbsv & "' where wonum = '" & wo & "'"
            Else
                If olead = elead Then
                    sql = "update workorder set leadealert = '" & cblv & "', supealert = '" & cbsv & "', elead = '" & elead & "' where wonum = '" & wo & "'"
                Else
                    sql = "update workorder set leadealert = '" & cblv & "', supealert = '" & cbsv & "', elead = '" & elead & "', scomp = 0, lcomp = 0 where wonum = '" & wo & "'"
                End If

            End If
            'End If


            If wodesc <> "" Then
                Try
                    SaveDesc(wo, wodesc)
                Catch ex As Exception
                    lblerr.Value = "yes"
                End Try

            End If
            If estw <> "" Then
                Try
                    SaveEst(wo, estw, estmp)
                Catch ex As Exception
                    lblerr.Value = "yes"
                End Try
            End If
            If pri <> "" Then
                Try
                    SavePri(wo, pri)
                Catch ex As Exception
                    lblerr.Value = "yes"
                End Try
            End If
            swo.Dispose()
        End If
    End Sub
    Private Sub SavePri(ByVal wonum As String, ByVal estw As String)
        sql = "update workorder set wopriority = '" & pri & "' where wonum = '" & wonum & "'"
        swo.Update(sql)
        lblerr.Value = "ok"
    End Sub
    Private Sub SaveEst(ByVal wonum As String, ByVal estw As String, ByVal estm As String)
        Dim tnchk As Long
        Try
            tnchk = System.Convert.ToInt64(estw)
        Catch ex As Exception
            Try
                tnchk = System.Convert.ToDecimal(estw)
            Catch ex1 As Exception
                Dim strMessage As String = "Estimated Hours Must Be A Numeric Value"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lblerr.Value = "yes"
                Exit Sub
            End Try

        End Try
        Try
            tnchk = System.Convert.ToInt64(estm)
        Catch ex As Exception
            estm = "0"
        End Try
        sql = "update workorder set estlabhrs = '" & estw & "', qty = '" & estm & "' where wonum = '" & wonum & "'"
        swo.Update(sql)
        lblerr.Value = "ok"

    End Sub
    Private Sub SaveDesc(ByVal wonum As String, ByVal wodesc As String)
        Dim sh, lg As String
        Dim lgcnt As Integer
        sh = wodesc
        sh = Replace(sh, "'", Chr(180), , , vbTextCompare)
        sh = Replace(sh, "--", "-", , , vbTextCompare)
        sh = Replace(sh, ";", ":", , , vbTextCompare)
        sh = Replace(sh, "/", " ", , , vbTextCompare)
        If Len(sh) > 79 Then
            lg = Mid(sh, 80)
            sh = Mid(sh, 1, 79)
        End If
        Dim cmd As New SqlCommand("exec usp_savewodesc @wonum, @sh, @lg")
        Dim param0 = New SqlParameter("@wonum", SqlDbType.Int)
        param0.Value = wonum
        cmd.Parameters.Add(param0)
        Dim param01 = New SqlParameter("@sh", SqlDbType.VarChar)
        If sh = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = sh
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@lg", SqlDbType.Text)
        If lg = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = lg
        End If
        cmd.Parameters.Add(param02)
        Try
            swo.UpdateHack(cmd)
        Catch ex As Exception
            Exit Sub
        End Try
        lblerr.Value = "ok"
    End Sub
End Class
