<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wrexpd.aspx.vb" Inherits="lucy_r12.wrexpd" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wrexpd</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/wrexpdaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td class="bluelabel">
                <asp:Label ID="lang1611" runat="server">Please Enter a Description</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtwodesc" runat="server" TextMode="MultiLine" Height="80px" Width="350px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                <img onclick="checkdesc();" src="../images/appbuttons/minibuttons/savedisk1.gif">
            </td>
        </tr>
    </table>
    <input id="lblwo" type="hidden" runat="server">
    <input id="lblsubmit" type="hidden" runat="server">
    <input id="lblret" type="hidden" runat="server"><input id="lblstat" type="hidden"
        runat="server">
    <input type="hidden" id="lbluser" runat="server"><input type="hidden" id="lbleqid"
        runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
