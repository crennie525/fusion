<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="woprint.aspx.vb" Inherits="lucy_r12.woprint" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>woprint</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/woprintaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <style type="text/css">
        .plainlabel
        {
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 14px;
            text-decoration: none;
            color: black;
        }
        .label
        {
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 14px;
            text-decoration: none;
            color: black;
            font-weight: bold;
        }
        .bigbold
        {
            font-family: Arial;
            font-size: 14px;
            color: Black;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table width="816">
        <tr class="details">
            <td width="93">
                &nbsp;
            </td>
            <td class="details">
            </td>
            <td width="93">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdwi" runat="server" align="center">
            </td>
            <td>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblwotype" runat="server">
    <input type="hidden" id="lblpsite" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblcomi" runat="server" />
    </form>
</body>
</html>
