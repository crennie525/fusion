Public Class getempdialog
    Inherits System.Web.UI.Page
    Dim uid, emp, islead, hrs, wonum, ohrs, adate, wid, shift, pmid, worktype, issched As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents iftd As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            wid = Request.QueryString("wid").ToString
            uid = Request.QueryString("uid").ToString
            wonum = Request.QueryString("wo").ToString
            emp = Request.QueryString("emp").ToString
            islead = Request.QueryString("islead").ToString
            hrs = Request.QueryString("hrs").ToString
            adate = Request.QueryString("adate").ToString
            shift = Request.QueryString("shift").ToString
            worktype = Request.QueryString("worktype").ToString
            pmid = Request.QueryString("pmid").ToString
            issched = Request.QueryString("issched").ToString
            iftd.Attributes.Add("src", "getemp.aspx?uid=" & uid & "&wo=" & wonum & "&emp=" & emp & "&islead=" & islead & "&hrs=" & hrs & "&adate=" & adate & "&wid=" & wid & "&worktype=" & worktype & "&shift=" & shift & "&pmid=" & pmid & "&issched=" & issched & "&date=" & Now)

        End If
    End Sub

End Class
