<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wolist.aspx.vb" Inherits="lucy_r12.wolist" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wolist</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/wolistaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <style type="text/css">
        wolistsmall5
        {
            width: 1020px;
            height: 430px;
            overflow: visible;
        }
        wolistbig5
        {
            width: 1020px;
            height: 360px;
            overflow: auto;
        }
    </style>
    <script language="javascript" type="text/javascript">
    <!--
        function resetsrch() {

            var url = window.location.href;
            var nohttp = url.split('//')[1];
            var hostPort = nohttp.split('/')[1]
            //alert (hostPort)
            //"/" + hostPort + "/
            window.location = "wolist.aspx?jump=no";


        }
        function handlert(id, rt) {
            document.getElementById("lblrtid").value = id;
            document.getElementById("txtrte").value = rt;
            document.getElementById("lblrt").value = rt;
            closert()
        }
        function getcal(fld) {
            var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
            if (eReturn) {
                //var fldret = "txt" + fld;
                document.getElementById(fld).value = eReturn;
                //document.getElementById("lblschk").value = "1";
                if (fld == "txtcomp") {
                    document.getElementById("lblnext").value = eReturn;
                }
                if (fld == "txtstart") {
                    document.getElementById("lblstart").value = eReturn;
                }
                if (fld == "txtfrom") {
                    document.getElementById("lbls").value = eReturn;
                }
                else if (fld == "txtto") {
                    document.getElementById("lble").value = eReturn;
                }
            }
        }

        function getsuper(typ) {
            var skill = document.getElementById("lblskillid").value;
            var ro = document.getElementById("lblro").value;
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    var retarr = eReturn.split(",")
                    if (typ == "sup") {
                        document.getElementById("lblsup").value = retarr[0];
                        document.getElementById("txtsup").value = retarr[1];
                        document.getElementById("lblsupstr").value = retarr[1];
                    }
                    else {
                        document.getElementById("lbllead").value = retarr[0];
                        document.getElementById("txtlead").value = retarr[1];
                        document.getElementById("lblleadstr").value = retarr[1];
                    }
                }
            }
        }
        function checkother() {
            //document.getElementById("txtrte").value = document.getElementById("lblrt").value;
            //document.getElementById("txtstart").value = document.getElementById("lbls").value;
            //document.getElementById("txtcomp").value = document.getElementById("lble").value;
            document.getElementById("txtsup").value = document.getElementById("lblsupstr").value;
            document.getElementById("txtlead").value = document.getElementById("lblleadstr").value;
        }
        function getsrch() {
            var chk = document.getElementById("lblhide").value;
            var tab = document.getElementById("lbltab").value;
            //alert(chk)
            //alert(tab)
            if (chk == "no") {
                document.getElementById("lblhide").value = "yes";
                document.getElementById("trhide1").className = "view";
                document.getElementById("trhide2").className = "view";
                var tab = document.getElementById("lbltab").value;
                if (tab == "yes") {
                    //document.getElementById("divywr").className = "wolistbig5";
                }
                else {
                    //document.getElementById("divywr").className = "wolistbig5";
                    document.getElementById("divywr").style.height = "180px";
                }
            }
            else {

                document.getElementById("lblhide").value = "no";
                document.getElementById("trhide1").className = "details";
                document.getElementById("trhide2").className = "details";
                if (tab == "yes") {
                    //document.getElementById("divywr").className = "wolistsmall5";

                }
                else {
                    //document.getElementById("divywr").className = "wolistsmall5";
                    document.getElementById("divywr").style.height = "420px";

                }
            }
            //alert(document.getElementById("divywr").style.height)
            var who = document.getElementById("lblwho").value;
            if (who != "") {
                checkwho(who);
            }
            checkother();
        }
        function checkprint() {
            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                window.parent.doref();
            }
            else {
                window.parent.setref();
            }
            var chk = document.getElementById("lblprint").value;
            if (chk == "yes") {
                document.getElementById("lblprint").value = "no";
                var filt = document.getElementById("lblfilt").value;
                document.getElementById("lblfilt").value = "";
                var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
                var typ = document.getElementById("lblprintwolist").value;
                document.getElementById("lblprintwolist").value = "";
                if (typ == "pm") {
                    window.open("PMPrintAll.aspx?filt=" + filt, "repWin", popwin);
                }
                else {
                    window.open("PMWOPrintAll.aspx?filt=" + filt, "repWin", popwin);
                }
            }
            var who = document.getElementById("lblwho").value;
            if (who != "") {
                checkwho(who);
            }
            checkother();
        }
        function checkwho(who) {
            if (who == "depts") {
                document.getElementById("tddept").innerHTML = document.getElementById("lbldept").value;
                document.getElementById("tdcell").innerHTML = document.getElementById("lblcell").value;
                document.getElementById("tdeq").innerHTML = document.getElementById("lbleq").value;
                document.getElementById("tdfu").innerHTML = document.getElementById("lblfu").value;
                document.getElementById("tdco").innerHTML = document.getElementById("lblcomp").value;
            }
            else if (who == "locs") {
                document.getElementById("tdloc3").innerHTML = document.getElementById("lbllocstr").value;
                document.getElementById("tdeq3").innerHTML = document.getElementById("lbleq").value;
                document.getElementById("tdfu3").innerHTML = document.getElementById("lblfu").value;
                document.getElementById("tdco3").innerHTML = document.getElementById("lblcomp").value;
            }
        }
        function getminsrch() {
            var sid = document.getElementById("lblsid").value;
            var typ = "lul";
            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                document.getElementById("lbllid").value = "";
                document.getElementById("lblloc").value = "";
                document.getElementById("tdloc3").innerHTML = "";
                document.getElementById("lbllocstr").value = "";

                document.getElementById("lbleq").value = "";
                document.getElementById("tdeq3").innerHTML = "";
                document.getElementById("lbleqid").value = "";

                document.getElementById("lblfuid").value = "";
                document.getElementById("lblfu").value = "";
                document.getElementById("tdfu3").innerHTML = "";

                document.getElementById("lblcomid").value = "";
                document.getElementById("lblcomp").value = "";
                document.getElementById("tdco3").innerHTML = "";

                document.getElementById("trlocs3").className = "details";

                var ret = eReturn.split("~");
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("lbldept").value = ret[1];
                document.getElementById("tddept").innerHTML = ret[1];
                document.getElementById("lblclid").value = ret[2];
                document.getElementById("lblcell").value = ret[3];
                document.getElementById("tdcell").innerHTML = ret[3];
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("lbleq").value = ret[5];
                document.getElementById("tdeq").innerHTML = ret[5];
                document.getElementById("lblfuid").value = ret[6];
                document.getElementById("lblfu").value = ret[7];
                document.getElementById("tdfu").innerHTML = ret[7];
                document.getElementById("lblcomid").value = ret[8];
                document.getElementById("lblcomp").value = ret[9];
                document.getElementById("tdco").innerHTML = ret[9];
                document.getElementById("lbllid").value = ret[12];
                //document.getElementById("lblloc").value = ret[13];
                document.getElementById("trdepts").className = "view";
                document.getElementById("lblwho").value = "depts";
            }



        }
        function getlocs1() {
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=lul&sid=" + sid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                document.getElementById("lbldid").value = "";
                document.getElementById("lbldept").value = "";
                document.getElementById("tddept").innerHTML = "";
                document.getElementById("lblclid").value = "";
                document.getElementById("lblcell").value = "";
                document.getElementById("tdcell").innerHTML = "";
                document.getElementById("lbleqid").value = "";
                document.getElementById("lbleq").value = "";
                document.getElementById("tdeq").innerHTML = "";
                document.getElementById("lblfuid").value = "";
                document.getElementById("lblfu").value = "";
                document.getElementById("tdfu").innerHTML = "";
                document.getElementById("lblcomid").value = "";
                document.getElementById("lblcomp").value = "";
                document.getElementById("tdco").innerHTML = "";
                document.getElementById("lbllid").value = "";
                document.getElementById("lblloc").value = "";
                document.getElementById("trdepts").className = "details";

                var ret = eReturn.split("~");
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("lblloc").value = ret[1];
                document.getElementById("tdloc3").innerHTML = ret[2];
                document.getElementById("lbllocstr").value = ret[1];

                document.getElementById("lbleq").value = ret[4];
                document.getElementById("tdeq3").innerHTML = ret[4];
                document.getElementById("lbleqid").value = ret[3];

                document.getElementById("lblfuid").value = ret[5];
                document.getElementById("lblfu").value = ret[6];
                document.getElementById("tdfu3").innerHTML = ret[6];

                document.getElementById("lblcomid").value = ret[7];
                document.getElementById("lblcomp").value = ret[8];
                document.getElementById("tdco3").innerHTML = ret[8];

                document.getElementById("trlocs3").className = "view";
                document.getElementById("lblwho").value = "locs";



            }

        }
        function srchloc1() {
            //alert("Temporarily Disabled")
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "lu"
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=lul&sid=" + sid + "&wo=" + wo, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                var ret = eReturn.split("~");
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("lblloc").innerHTML = ret[1];
                document.getElementById("lbllocstr").value = ret[1];
                //document.getElementById("lblsubmit").value = "srch";
                //document.getElementById("form1").submit();
            }
        }
        //-->
    </script>
</head>
<body class="tbg" onload="getsrch();">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 0px; left: 0px" cellpadding="0" cellspacing="0">
        <tr id="trsa" runat="server">
            <td align="center">
                <table>
                    <tr>
                        <td id="tdssa" class="bluelabel" width="120" align="center" runat="server">
                            <input id="cbssa" onclick="showsuper();" type="checkbox" name="cbssa" runat="server"><asp:Label
                                ID="lang1523" runat="server">Supervisor Only</asp:Label>
                        </td>
                        <td id="tdsa" class="bluelabel" width="80" align="center" runat="server">
                            <input id="cbsa" onclick="showall();" type="checkbox" name="cbsa" runat="server"><asp:Label
                                ID="lang1524" runat="server">Show All</asp:Label>
                        </td>
                        <td id="tdrf" class="bluelabel" width="120" align="center" runat="server">
                            <input id="cbrf" onclick="startrf();" type="checkbox" name="cbrf" runat="server">
                            <asp:Label ID="lblrf" runat="server">Start Refresh</asp:Label>
                        </td>
                        <td id="tdr1" class="graylabel" width="80" runat="server">
                            <asp:Label ID="lang1525" runat="server">Refresh Rate</asp:Label>
                        </td>
                        <td id="tdr2" width="90" runat="server">
                            <asp:DropDownList ID="ddref" runat="server">
                                <asp:ListItem Value="5" Selected="True">5 minutes</asp:ListItem>
                                <asp:ListItem Value="4">4 minutes</asp:ListItem>
                                <asp:ListItem Value="3">3 minutes</asp:ListItem>
                                <asp:ListItem Value="2">2 minutes</asp:ListItem>
                                <asp:ListItem Value="1">1 minute</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td id="tdr4" class="graylabel" width="80" runat="server">
                            <asp:Label ID="lang1526" runat="server">Next Refresh</asp:Label>
                        </td>
                        <td id="tdnext" class="plainlabel" width="170" runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trsa1" runat="server">
            <td align="center">
                <table>
                    <tr>
                        <td id="Td1" class="bluelabel" width="120" align="center" runat="server">
                            <input id="cbsa1" onclick="showall();" type="checkbox" name="cbsa1" runat="server"><asp:Label
                                ID="lang1527" runat="server">Show All</asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td id="tdywr" runat="server">
                <div id="divywr" style="height: 250px; width: 1040px; overflow:auto" runat="server">
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                    border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table id="tbwrs" cellspacing="0" width="1020" runat="server">
                    <tr>
                        <td class="thdrsinglft" width="26">
                            <img border="0" src="../images/appbuttons/minibuttons/wohdr.gif">
                        </td>
                        <td id="tdwrs" class="thdrsingrt label hand" onmouseover="return overlib('Click to Show or Hide Search Options', ABOVE, LEFT)"
                            onmouseout="return nd()" onclick="getsrch();" width="734" colspan="4" align="left"
                            runat="server">
                            <asp:Label ID="lang1528" runat="server">Work Order Record Search</asp:Label>
                        </td>
                    </tr>
                </table>
                <table id="trhide1" border="0" cellspacing="0" cellpadding="2" width="760" runat="server">
                    <tr>
                        <td width="110">
                        </td>
                        <td width="135">
                        </td>
                        <td width="25">
                        </td>
                        <td width="90">
                        </td>
                        <td width="135">
                        </td>
                        <td width="25">
                        </td>
                        <td width="70">
                        </td>
                        <td width="160">
                        </td>
                        <td width="20">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="9">
                            <table cellpadding="1" cellspacing="1">
                                <tr>
                                    <td id="tddepts" class="bluelabel" runat="server">
                                        Use Departments
                                    </td>
                                    <td>
                                        <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif" />
                                    </td>
                                    <td id="tdlocs1" class="bluelabel" runat="server">
                                        Use Locations
                                    </td>
                                    <td>
                                        <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trdepts" class="details" runat="server">
                        <td colspan="9">
                            <table cellpadding="1" cellspacing="1">
                                <tr>
                                    <td class="label" width="100">
                                        Department
                                    </td>
                                    <td id="tddept" class="plainlabel" width="170" runat="server">
                                    </td>
                                    <td width="30">
                                    </td>
                                    <td class="label" width="100">
                                        Equipment
                                    </td>
                                    <td id="tdeq" class="plainlabel" width="170" runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        Station\Cell
                                    </td>
                                    <td id="tdcell" class="plainlabel" runat="server">
                                    </td>
                                    <td>
                                    </td>
                                    <td class="label">
                                        Function
                                    </td>
                                    <td id="tdfu" class="plainlabel" runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <asp:Label ID="Label3" runat="server"></asp:Label>
                                    </td>
                                    <td id="tdcharge1" class="plainlabel" runat="server">
                                    </td>
                                    <td>
                                    </td>
                                    <td class="label">
                                        Component
                                    </td>
                                    <td id="tdco" class="plainlabel" runat="server">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trlocs3" class="details" runat="server">
                        <td colspan="9">
                            <table cellpadding="1" cellspacing="1">
                                <tr>
                                    <td class="label" width="100">
                                        Location
                                    </td>
                                    <td id="tdloc3" class="plainlabel" runat="server" width="170">
                                    </td>
                                    <td width="30">
                                    </td>
                                    <td class="label" width="100">
                                        Function
                                    </td>
                                    <td id="tdfu3" class="plainlabel" runat="server" width="170">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        Equipment
                                    </td>
                                    <td id="tdeq3" class="plainlabel" runat="server">
                                    </td>
                                    <td>
                                    </td>
                                    <td class="label">
                                        Component
                                    </td>
                                    <td id="tdco3" class="plainlabel" runat="server">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="lang1535" runat="server">Work Order#</asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtsrchwo" runat="server" CssClass="plainlabel" Font-Size="9pt"></asp:TextBox>
                        </td>
                        <td class="label">
                            <asp:Label ID="lang1536" runat="server">Description</asp:Label>
                        </td>
                        <td colspan="5">
                            <asp:TextBox ID="txtsrchdesc" runat="server" CssClass="plainlabel" Width="380px"
                                Font-Size="9pt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="lang1537" runat="server">Work Type</asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:DropDownList ID="ddtype" runat="server" CssClass="plainlabel" Width="150px">
                            </asp:DropDownList>
                        </td>
                        <td class="label">
                            <asp:Label ID="lang1538" runat="server">Work Status</asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:DropDownList ID="ddstatus" runat="server" CssClass="plainlabel" Width="150px">
                            </asp:DropDownList>
                        </td>
                        <td class="label">
                            <asp:Label ID="lang1539" runat="server">Skill</asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddskill" runat="server" CssClass="plainlabel" Width="150px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="lang1540" runat="server">Supervisor</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtsup" runat="server" CssClass="plainlabel" Font-Size="9pt" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getsuper('sup');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                        <td class="label">
                            <asp:Label ID="lang1541" runat="server">Lead Craft</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtlead" runat="server" CssClass="plainlabel" Font-Size="9pt" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getsuper('lead');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                        <td class="label">
                            <asp:Label ID="lang1542" runat="server">Misc Asset</asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:DropDownList ID="ddnc" runat="server" CssClass="plainlabel" Width="150px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <table id="trhide2" border="0" cellspacing="0" cellpadding="2" width="720" runat="server">
                    <tr>
                        <td class="label" colspan="2">
                            <asp:Label ID="lang1543" runat="server">From Date</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtfrom" runat="server" Font-Size="9pt" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcal('txtfrom');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg">
                        </td>
                        <td class="label">
                            <asp:Label ID="lang1544" runat="server">To Date</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtto" runat="server" Font-Size="9pt" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcal('txtto');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                width="18" height="18">
                        </td>
                    </tr>
                    <tr>
                        <td class="label" colspan="2">
                            <asp:Label ID="lang1545" runat="server" CssClass="details">Requested Start Date</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtstart" runat="server" Font-Size="9pt" ReadOnly="True" CssClass="details"></asp:TextBox>
                        </td>
                        <td>
                            <img class="details" onclick="getcal('txtstart');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg">
                        </td>
                        <td class="label">
                            <asp:Label ID="lang1546" runat="server" CssClass="details">Requested Completion Date</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtcomp" runat="server" Font-Size="9pt" ReadOnly="True" CssClass="details"></asp:TextBox>
                        </td>
                        <td>
                            <img class="details" onclick="getcal('txtcomp');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                width="18" height="18">
                        </td>
                        <td width="100" align="right">
                            <img onclick="resetsrch();" alt="" src="../images/appbuttons/minibuttons/switch.gif">
                            <asp:ImageButton ID="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                            </asp:ImageButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="txtpg" type="hidden" name="txtpg" runat="server">
    <input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
    <input id="lblret" type="hidden" name="lblret" runat="server">
    <input id="txtsearch" type="hidden" runat="server">
    <input id="lbleqid" type="hidden" runat="server">
    <input id="lblfuid" type="hidden" runat="server">
    <input id="lblcoid" type="hidden" runat="server">
    <input id="lbltyp" type="hidden" runat="server">
    <input id="lblncid" type="hidden" runat="server">
    <input id="lblsid" type="hidden" runat="server">
    <input id="lblcid" type="hidden" runat="server">
    <input id="lbldept" type="hidden" name="lbldept" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server">
    <input id="lbllid" type="hidden" name="lbllid" runat="server"><input id="lblclid"
        type="hidden" name="lblclid" runat="server">
    <input id="lblskillid" type="hidden" name="lblskillid" runat="server">
    <input id="lblsrch" type="hidden" runat="server">
    <input id="lblpchk" type="hidden" runat="server">
    <input id="lbllead" type="hidden" runat="server">
    <input id="lblsup" type="hidden" runat="server"><input id="lblro" type="hidden" runat="server">
    <input id="lbllocstr" type="hidden" runat="server"><input id="lbllog" type="hidden"
        name="lbllog" runat="server">
    <input id="lbltab" type="hidden" runat="server"><input id="lblhide" type="hidden"
        runat="server">
    <input type="hidden" id="lbladm1" runat="server"><input type="hidden" id="lblrefresh"
        runat="server">
    <input type="hidden" id="lblissuper" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblcomid" runat="server" />
    <input type="hidden" id="lblcomp" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lblcell" runat="server" />
    <input type="hidden" id="lblfu" runat="server" />
    <input type="hidden" id="lblloc" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblsupstr" runat="server" />
    <input type="hidden" id="lblleadstr" runat="server" />
    <input type="hidden" id="lbls" runat="server" />
    <input type="hidden" id="lble" runat="server" />
    <input type="hidden" id="lblrt" runat="server" />
    <input type="hidden" id="lblstart" runat="server" />
    <input type="hidden" id="lblnext" runat="server" />
    <input type="hidden" id="lblfiltwo" runat="server" />
    <input type="hidden" id="lblref" runat="server" />
    </form>
</body>
</html>
