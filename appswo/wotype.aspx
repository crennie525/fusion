﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wotype.aspx.vb" Inherits="lucy_r12.wotype" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function handleexit() {
            var ret = document.getElementById("lblretstring").value;
            window.parent.handleexit(ret);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table width="522">
    <tr>
            <td class="thdrsingrt label" colspan="3">
                <asp:Label ID="lang1912a" runat="server">Select Work Types Mini Dialog</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="bluelabel" align="center" width="250">
                <asp:Label ID="aswitch" runat="server">Work Types</asp:Label>
            </td>
           
            <td width="22">
            </td>
            <td class="label" align="center" width="250">
                <asp:Label ID="lang1916" runat="server">Selected Work Types</asp:Label>
            </td>
        </tr>
         <tr>
            <td align="center">
                <asp:ListBox ID="lbwt" runat="server" Width="250px" SelectionMode="Multiple"
                    Height="150px"></asp:ListBox>
                
            </td>
            <td valign="middle" align="center">
                <img src="../images/appbuttons/minibuttons/forwardgraybg.gif" class="details" id="todis"
                    width="20" height="20" runat="server">
                <img src="../images/appbuttons/minibuttons/backgraybg.gif" class="details" id="fromdis"
                    width="20" height="20" runat="server">
                <asp:ImageButton ID="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                </asp:ImageButton>
                <asp:ImageButton ID="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                </asp:ImageButton><img id="btnaddtosite" onmouseover="return overlib('Choose Failure Modes for this Plant Site ')"
                    onclick="getss();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/plusminus.gif"
                    width="20" height="20" class="details" runat="server">
            </td>
            <td align="center">
                <asp:ListBox ID="lbwts" runat="server" Width="250px" SelectionMode="Multiple"
                    Height="150px"></asp:ListBox>
            </td>
        </tr>
         <tr>
            <td align="right" colspan="3">
                <img onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
                    id="ibtnret" runat="server" width="69" height="19">
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblretstring" runat="server" />
    <input type="hidden" id="lblnotstring" runat="server" />
    </form>
</body>
</html>
