

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class woactm
    Inherits System.Web.UI.Page
	Protected WithEvents lang1340 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1339 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1338 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim jpid, wonum, typ, stat, icost, ro, rostr, Login As String
    Dim jpm As New Utilities
    Dim ap As New AppUtils
    Protected WithEvents tdnotes As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgparts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents Label24 As System.Web.UI.WebControls.Label
    Protected WithEvents txttsk As System.Web.UI.WebControls.TextBox
    Protected WithEvents trp As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trt As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trl As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblview As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here

        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                dgparts.Columns(0).Visible = False
            Else
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = jpm.CheckROS(rostr, "wo")
                    lblro.Value = ro
                    If ro = "1" Then
                        dgparts.Columns(0).Visible = False
                    End If
                End If
            End If
            wonum = Request.QueryString("wo").ToString '"1048" '
            typ = Request.QueryString("typ").ToString
            stat = Request.QueryString("stat").ToString
            lblwo.Value = wonum
            lbljpid.Value = jpid
            lblview.Value = typ
            lblstat.Value = stat
            Dim icost As String = ap.InvEntry
            If (stat <> "COMP" And stat <> "CAN" And stat <> "WAPPR") And (icost <> "ext" And icost <> "inv") Then
                dgparts.Columns(0).Visible = True
            Else
                dgparts.Columns(0).Visible = False
            End If
            If icost = "ext" Then
                tdnotes.InnerHtml = "Material Costs\Usage are Entered Externally"
                dgparts.Columns(0).Visible = False
            ElseIf icost = "inv" Then
                tdnotes.InnerHtml = "Material Costs\Usage are Entered via Inventory"
                dgparts.Columns(0).Visible = False
            Else
                tdnotes.InnerHtml = "Material Costs\Usage are Entered Manually"
            End If
            If typ = "p" Then
                trp.Attributes.Add("class", "view")
            ElseIf typ = "t" Then
                trt.Attributes.Add("class", "view")
            ElseIf typ = "l" Then
                trl.Attributes.Add("class", "view")
                dgparts.Columns(7).Visible = False
            End If
            jpm.Open()
            GetParts()
            jpm.Dispose()
        End If
    End Sub
    Private Sub GetParts()
        wonum = lblwo.Value
        jpid = lbljpid.Value
        typ = lblview.Value
        If typ = "p" Then
            sql = "select * from woparts where wonum = '" & wonum & "'"
        ElseIf typ = "t" Then
            sql = "select m.*, m.toolnum as itemnum, m.wotoolid as wopartid, m.rate as cost from wotools m " _
            + " where m.wonum = '" & wonum & "'"
        ElseIf typ = "l" Then
            sql = "select m.*, m.lubenum as itemnum, m.wolubeid as wopartid from wolubes m " _
            + " where m.wonum = '" & wonum & "'"
        End If

        dr = jpm.GetRdrData(sql)
        dgparts.DataSource = dr
        dgparts.DataBind()
        ro = lblro.value
        If ro = "1" Then
            dgparts.Columns(0).Visible = False
        End If

    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If CatID <> "Select" And CatID <> "N/A" Then 'Not IsDBNull(CatID) OrElse  
            If CatID = "PASS" Then
                iL = 0
            ElseIf CatID = "FAIL" Then
                iL = 1
            ElseIf CatID = "Y" Then
                iL = 0
            ElseIf CatID = "N" Then
                iL = 1
            End If
        Else
            iL = -1
        End If
        Return iL
    End Function

    Private Sub dgparts_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.EditCommand
        stat = lblstat.Value
        If stat <> "COMP" And stat <> "CAN" Then
            dgparts.EditItemIndex = e.Item.ItemIndex
            jpm.Open()
            GetParts()
            jpm.Dispose()
        End If
    End Sub

    Private Sub dgparts_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.CancelCommand
        dgparts.EditItemIndex = -1
        jpm.Open()
        GetParts()
        jpm.Dispose()
    End Sub

    Private Sub dgparts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgparts.ItemDataBound
        If e.Item.ItemType = ListItemType.EditItem Then
            If typ = "l" Then
                Dim qbox As TextBox = CType(e.Item.FindControl("txtqty"), TextBox)
                qbox.Enabled = False
            End If
        End If
    End Sub

    Private Sub dgparts_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.UpdateCommand
        Dim qtystr As String = CType(e.Item.FindControl("txtqty"), TextBox).Text
        Dim coststr As String = CType(e.Item.FindControl("txtcost"), TextBox).Text
        Dim pid As String = CType(e.Item.FindControl("lblpid"), Label).Text
        Dim itemid As String = CType(e.Item.FindControl("lblitemid"), Label).Text
        Dim ddu As DropDownList = CType(e.Item.FindControl("ddus"), DropDownList)
        Dim us As String = ddu.SelectedValue
        Dim txtchk As Long
        typ = lblview.Value
        If typ <> "l" Then
            Try
                txtchk = System.Convert.ToDecimal(qtystr)
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr557" , "woactm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        End If
        Try
            txtchk = System.Convert.ToDecimal(coststr)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr558" , "woactm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        jpm.Open()
        wonum = lblwo.Value
        If typ = "p" Then
            sql = "update woparts set qtyused = '" & qtystr & "', cost = '" & coststr & "' " _
            + "used = '" & us & "' where wopartid = '" & pid & "'; update woparts set usedtotal = (qtyused * cost) " _
            + "where wopartid = '" & pid & "'"
            If us = "Y" Then
                sql += "update workorder set actmatcost = (select sum(usedtotal) from woparts where wonum = '" & wonum & "' and used = 'Y') " _
                + "where wonum = '" & wonum & "'; " _
                + "update invbalances set curbal = curbal - '" & qtystr & "', reserved = reserved - '" & qtystr & "' " _
                + "where itemid = '" & itemid & "'"
            End If


        ElseIf typ = "t" Then
            sql = "update wotools set qtyused = '" & qtystr & "', rate = '" & coststr & "' " _
            + "used = '" & us & "' where wotoolid = '" & pid & "'; update wotools set usedtotal = (qtyused * rate) " _
            + "where wotoolid = '" & pid & "'"
            If us = "Y" Then
                sql += "update workorder set acttoolcost = (select sum(usedtotal) from wotools where wonum = '" & wonum & "' and used = 'Y') " _
                + "where wonum = '" & wonum & "'; " _
                + "update invbalances set curbal = curbal - '" & qtystr & "', reserved = reserved - '" & qtystr & "' " _
                + "where itemid = '" & itemid & "'"
            End If

        ElseIf typ = "l" Then
            sql = "update wolubes set cost = '" & coststr & "', " _
            + "used = '" & us & "' where wolubeid = '" & pid & "'; "
            If us = "Y" Then
                sql += "update workorder set actlubecost = (select sum(cost) from wolubes where wonum = '" & wonum & "'  used = 'Y') " _
                + "where wonum = '" & wonum & "'; " _
                + "update invbalances set curbal = curbal - '" & qtystr & "', reserved = reserved - '" & qtystr & "' " _
                + "where itemid = '" & itemid & "'"
            End If

        End If

        jpm.Update(sql)

        dgparts.EditItemIndex = -1
        GetParts()
        jpm.Dispose()
    End Sub
	

	

	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgparts.Columns(0).HeaderText = dlabs.GetDGPage("woactm.aspx","dgparts","0")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(1).HeaderText = dlabs.GetDGPage("woactm.aspx","dgparts","1")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(2).HeaderText = dlabs.GetDGPage("woactm.aspx","dgparts","2")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(4).HeaderText = dlabs.GetDGPage("woactm.aspx","dgparts","4")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(5).HeaderText = dlabs.GetDGPage("woactm.aspx","dgparts","5")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(6).HeaderText = dlabs.GetDGPage("woactm.aspx","dgparts","6")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(7).HeaderText = dlabs.GetDGPage("woactm.aspx","dgparts","7")
		Catch ex As Exception
		End Try

	End Sub

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang1338.Text = axlabs.GetASPXPage("woactm.aspx","lang1338")
		Catch ex As Exception
		End Try
		Try
			lang1339.Text = axlabs.GetASPXPage("woactm.aspx","lang1339")
		Catch ex As Exception
		End Try
		Try
			lang1340.Text = axlabs.GetASPXPage("woactm.aspx","lang1340")
		Catch ex As Exception
		End Try

	End Sub

End Class
