

'********************************************************
'*
'********************************************************



Public Class wojpeditmain
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim wonum, jid, href, sid, cid As String
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifinv As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblhref As System.Web.UI.HtmlControls.HtmlInputHidden

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Dim Login As String
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        If Not IsPostBack Then
            wonum = Request.QueryString("wonum").ToString
            lblwo.Value = wonum
            jid = Request.QueryString("jpid").ToString
            lbljpid.Value = jid
            href = Request.QueryString("href").ToString
            'href = Replace(href, "~", "&")
            '//var harr = href.split("~");
            '//var remwo = harr[1] + "~";
            '//href = href.replace(remwo, "")
            Dim harr() As String = href.Split("~")
            Dim remwo As String = "~" & harr(1) '& "~"
            href = href.Replace(remwo, "~wo=" & wonum)
            'href = href.Replace("~wo=", "~wo=" & wonum)
            lblhref.Value = href
            '"wojpedit.aspx?jpid=" + jpid + "&wonum=" + wo + "&href=" + href + "&ro=" + ro;
            ifinv.Attributes.Add("src", "wojpedit.aspx?jpid=" + jid + "&wonum=" + wonum + "&href=" + href)
        End If
    End Sub

End Class
