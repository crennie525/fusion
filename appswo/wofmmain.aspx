<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wofmmain.aspx.vb" Inherits="lucy_r12.wofmmain" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wofmmain</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/wofmmainaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table style="left: 2px; position: absolute; top: 2px" cellspacing="1" cellpadding="2"
        width="730">
        <tr>
            <td>
                <table width="730">
                    <tr height="22">
                        <td class="label" width="80">
                            <asp:Label ID="lang1397" runat="server">Work Order#</asp:Label>
                        </td>
                        <td class="plainlabel" id="tdwo" width="140" runat="server">
                        </td>
                        <td class="plainlabel" id="tdwod" width="450" runat="server">
                        </td>
                        <td align="right" width="30">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="22">
            <td class="plainlabelblue" id="Td1" runat="server" align="center">
                <asp:Label ID="lang1398" runat="server">Note that Failure Reporting Entries can only be made when Work Order is not Waiting to be Approved, Completed, or Cancelled.</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="2" width="730">
                    <tr height="22">
                        <td class="thdrhov label" id="tdw" onclick="gettab('wo');" width="150" runat="server">
                            <asp:Label ID="lang1399" runat="server">Work Order Details</asp:Label>
                        </td>
                        <td class="thdr label" id="tdj" onclick="gettab('jp');" width="200" runat="server">
                            <asp:Label ID="lang1400" runat="server">Job Plan Details (Failure Modes)</asp:Label>
                        </td>
                        <td class="thdr label" id="tdjpm" onclick="gettab('jpm');" width="200" runat="server">
                            <asp:Label ID="lang1401" runat="server">Job Plan Details (Measurements)</asp:Label>
                        </td>
                        <td width="210">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdborder" id="wo" colspan="4" runat="server">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <iframe id="ifwo" src="wohold.htm" frameborder="no" width="760" scrolling="auto"
                                            height="420" runat="server" style="background-color: transparent" allowtransparency>
                                        </iframe>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="details" id="jp" valign="top" colspan="4" runat="server">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <iframe id="ifjp" src="wohold.htm" frameborder="no" width="760" scrolling="auto"
                                            height="420" runat="server" style="background-color: transparent" allowtransparency>
                                        </iframe>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblwo" type="hidden" runat="server">
    <input type="hidden" id="lbljpid" runat="server">
    <input type="hidden" id="lblupsav" runat="server">
    <input type="hidden" id="lblstat" runat="server">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
