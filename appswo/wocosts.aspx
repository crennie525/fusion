<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wocosts.aspx.vb" Inherits="lucy_r12.wocosts" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wocosts</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/wocostsaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkit();" class="tbg">
    <form id="form1" method="post" runat="server">
    <table cellpadding="2" width="750" style="position: absolute; top: 4px; left: 4px"
        cellspacing="0">
        <tbody>
            <tr>
                <td>
                    <table width="750">
                        <tr height="22">
                            <td class="label" width="80">
                                <asp:Label ID="lang1357" runat="server">Work Order#</asp:Label>
                            </td>
                            <td class="plainlabel" id="tdwo" width="140" runat="server">
                            </td>
                            <td class="plainlabel" id="tdwod" width="430" runat="server">
                            </td>
                            <td align="right" width="100">
                            </td>
                        </tr>
                        <tr height="22">
                            <td class="label" width="80">
                                <asp:Label ID="lang1358" runat="server">Job Plan#</asp:Label>
                            </td>
                            <td class="plainlabel" id="tdjp" width="140" runat="server">
                            </td>
                            <td class="plainlabel" id="tdjpd" width="430" runat="server">
                            </td>
                            <td align="right" width="100">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="24" runat="server">
                <td class="thdrsing label">
                    <asp:Label ID="lang1359" runat="server">Work Order Costs Review</asp:Label>
                </td>
            </tr>
            <tr>
                <td id="tdcosts" runat="server" align="center">
                </td>
            </tr>
            <tr>
                <td id="tdnotes" runat="server" class="plainlabelblue" align="center">
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" id="lblwo" runat="server">
    <input type="hidden" id="lbljpid" runat="server">
    <input type="hidden" id="lblstat" runat="server">
    <input type="hidden" id="lbllog" runat="server" name="lbllog">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
