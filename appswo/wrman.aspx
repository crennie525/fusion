<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wrman.aspx.vb" Inherits="lucy_r12.wrman" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wrman</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/wrmanaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="checkstat();">
    <form id="form1" method="post" runat="server">
    <div id="FreezePane" class="FreezePaneOff" align="center">
        <div id="InnerFreezePane" class="InnerFreezePane">
        </div>
    </div>
    <div style="position: absolute; top: 0px; left: 4px">
        <table cellspacing="0" cellpadding="1" width="740">
            <tr>
                <td width="92">
                </td>
                <td width="180">
                </td>
                <td width="22">
                </td>
                <td width="422">
                </td>
            </tr>
            <tr id="deptdiv" runat="server">
                <td class="label">
                    <asp:Label ID="lang1616" runat="server">Department</asp:Label>
                </td>
                <td class="label">
                    <asp:DropDownList ID="dddepts" runat="server" CssClass="plainlabel" AutoPostBack="True"
                        Width="170px">
                    </asp:DropDownList>
                </td>
                <td class="label">
                    <img id="imgsw" onmouseover="return overlib('Reset values and Refresh Location Information', LEFT)"
                        onmouseout="return nd()" onclick="undoloc();" src="../images/appbuttons/minibuttons/switch.gif"
                        runat="server">
                </td>
                <td class="label">
                    <asp:Label ID="lbldeptdesc" runat="server" CssClass="label" Font-Bold="True" Font-Names="Arial"
                        Font-Size="X-Small"></asp:Label>
                </td>
            </tr>
            <tr id="celldiv" class="details" runat="server">
                <td class="label">
                    <asp:Label ID="lang1617" runat="server">Station/Cell</asp:Label>
                </td>
                <td class="label">
                    <asp:DropDownList ID="ddcells" runat="server" CssClass="plainlabel" AutoPostBack="True"
                        Width="170px">
                    </asp:DropDownList>
                </td>
                <td class="label">
                </td>
                <td class="label">
                    <asp:Label ID="lblcelldesc" runat="server" CssClass="label" Font-Bold="True" Font-Names="Arial"
                        Font-Size="X-Small"></asp:Label>
                </td>
            </tr>
            <tr id="eqdiv" class="details" runat="server">
                <td class="label">
                    <asp:Label ID="lang1618" runat="server">Equipment#</asp:Label>
                </td>
                <td class="label">
                    <asp:DropDownList ID="ddeq" runat="server" CssClass="plainlabel" AutoPostBack="True"
                        Width="170px">
                    </asp:DropDownList>
                </td>
                <td>
                </td>
                <td class="details" id="tdisdown" runat="server">
                    <asp:Label ID="lang1619" runat="server">Is Equipment Down?</asp:Label><input type="checkbox"
                        id="cbisdown" runat="server" onclick="checkdown();"><asp:Label ID="lang1620" runat="server">Yes</asp:Label><input
                            type="checkbox" id="cbisnot" runat="server" checked><asp:Label ID="lang1621" runat="server">No</asp:Label>
                </td>
            </tr>
            <tr id="ncdiv" class="details" runat="server">
                <td class="label">
                    <asp:Label ID="lang1622" runat="server">Misc Asset</asp:Label>
                </td>
                <td class="label">
                    <asp:DropDownList ID="ddnc" runat="server" CssClass="plainlabel" AutoPostBack="True"
                        Width="170px">
                    </asp:DropDownList>
                </td>
                <td class="label">
                </td>
                <td class="label">
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang1623" runat="server">Location</asp:Label>
                </td>
                <td class="label">
                    <asp:Label ID="lblloc" runat="server" CssClass="plainlabel" Width="170px" BorderStyle="Solid"
                        BorderWidth="1px" BorderColor="#8080FF" Height="20px"></asp:Label>
                </td>
                <td class="label">
                    <img onmouseover="return overlib('Use Locations', LEFT)" onmouseout="return nd()"
                        onclick="srchloc();" src="../images/appbuttons/minibuttons/useloc.gif">
                </td>
                <td class="label">
                </td>
            </tr>
        </table>
    </div>
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lbltyp" type="hidden" name="lbltyp" runat="server">
    <input id="lbllid" type="hidden" name="lbllid" runat="server">
    <input id="lbldept" type="hidden" name="lbldept" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server">
    <input id="lblclid" type="hidden" name="lblclid" runat="server">
    <input id="lblpar" type="hidden" name="lblpar" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lblpar2" type="hidden" name="lblpar2" runat="server">
    <input id="lblgetarch" type="hidden" name="lblgetarch" runat="server">
    <input id="lblwo" type="hidden" name="lblwo" runat="server">
    <input id="lblpchk" type="hidden" name="lblpchk" runat="server">
    <input id="lbllog" type="hidden" name="lbllog" runat="server">
    <input id="lblreset" type="hidden" name="lblreset" runat="server"><input id="lblncid"
        type="hidden" name="lblncid" runat="server">
    <input type="hidden" id="lblreqd" runat="server"><input type="hidden" id="lblstatus"
        runat="server">
    <input type="hidden" id="lblisdown" runat="server">
    <input type="hidden" id="lblsubmit" runat="server">
    <input type="hidden" id="lblstat" runat="server">
    <input type="hidden" id="lbluser" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
