

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class woplans
    Inherits System.Web.UI.Page
	Protected WithEvents lang1569 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1568 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1567 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1566 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1565 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1564 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim wo As New Utilities
    Dim cid, wonum, jump, sid, stat, ro, rostr, Login As String
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgaddplan As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbljpref As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblplnrid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblplanner As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbreq As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lbldis As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgeditplan As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblwt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtplan As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtsup As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblgetplan As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdjp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgplans As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdplanner As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgplanner As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdref As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro <> "1" Then
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = wo.CheckROS(rostr, "wo")
                    lblro.Value = ro
                End If
            End If
            Try
                wonum = Request.QueryString("wo").ToString
                lblwo.Value = wonum
                stat = Request.QueryString("stat").ToString
                lblstat.Value = stat
            Catch ex As Exception

            End Try
            wo.Open()
            GetJP()
            wo.Dispose()
            If stat = "COMP" Or stat = "CAN" Then
                txtplan.CssClass = "plainlabelblue"
                txtsup.CssClass = "plainlabelblue"
            End If
        Else
            If Request.Form("lblsubmit") = "aplan" Then
                lblsubmit.Value = ""
                wo.Open()
                UpPlanner()
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "nplan" Then
                lblsubmit.Value = ""
                wo.Open()
                GetJP()
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "rplan" Then
                lblsubmit.Value = ""
                wo.Open()
                RemRef()
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "addplan" Then
                lblsubmit.Value = ""
                wo.Open()
                AddPlan()
                wo.Dispose()
           
            End If
        End If
    End Sub
    Private Sub AddPlan()
        wonum = lblwo.Value
        Dim jpid As String = lbljpid.Value
        If jpid <> "" Then
            sql = "usp_addjptowo '" & jpid & "','" & wonum & "'"
            wo.Update(sql)
            lblsubmit.Value = "upjp"
        End If
    End Sub
    Private Sub RemRef()
        Dim jpid As String = lbljpid.Value
        Dim jpref As String = lbljpref.Value
        wonum = lblwo.Value
        sql = "usp_addremjpref '" & wonum & "','" & jpref & "','','rem'"
        wo.Update(sql)
        GetJP()
        wo.Dispose()
    End Sub
    Private Sub UpPlanner()
        Dim plnrid As String = lblplnrid.Value
        Dim plnr As String = lblplanner.Value
        Dim jpref As String = lbljpref.Value
        wonum = lblwo.Value

        sql = "update pmjpref set plnrid = '" & plnrid & "' where jpref = '" & jpref & "' and wonum = '" & wonum & "'"
        wo.Update(sql)
        GetJP()

    End Sub
    Private Sub GetJP()
        Dim jprefstat As String
        wonum = lblwo.Value
        sql = "select w.worktype, w.jpid, w.jpnum, w.jpref, r.status as jprefstat, p.username from workorder w " _
        + "left join pmjpref r on r.jpref = w.jpref and r.jpref is not null " _
        + "left join pmsysusers p on p.userid = r.plnrid where w.wonum = '" & wonum & "'"

        Dim jpn, jpid, jpref, pname, wt As String
        dr = wo.GetRdrData(sql)
        While dr.Read
            jpid = dr.Item("jpid").ToString
            jpn = dr.Item("jpnum").ToString
            jpref = dr.Item("jpref").ToString
            pname = dr.Item("username").ToString
            jprefstat = dr.Item("jprefstat").ToString
            wt = dr.Item("worktype").ToString
        End While
        dr.Close()
        ro = lblro.Value
        If wt = "PM" Or ro = "1" Then
            imgplanner.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
            imgplanner.Attributes.Add("onclick", "")
            imgplans.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
            imgaddplan.Attributes.Add("src", "../images/appbuttons/minibuttons/addmod.gif")
            imgeditplan.Attributes.Add("src", "../images/appbuttons/minibuttons/pencildis.gif")
            lbldis.Value = "1"
        Else
            If jpref = "" Then
                txtplan.Text = jpn
                lbljpid.Value = jpid
                tdplanner.Attributes.Add("class", "graylabel")
                imgplanner.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
                imgplanner.Attributes.Add("onclick", "")
                tdjp.Attributes.Add("class", "label")

                imgplans.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifier.gif")
                'imgplans.Attributes.Add("onclick", "getplans();")
                imgaddplan.Attributes.Add("src", "../images/appbuttons/minibuttons/addmod.gif")
                'imgaddplan.Attributes.Add("onclick", "gotojp();")
                imgeditplan.Attributes.Add("src", "../images/appbuttons/minibuttons/pencil.gif")
                lbldis.Value = "0"
                tdref.InnerHtml = "None"
                cbreq.Checked = False
            Else
                txtplan.Text = jpn
                lbljpid.Value = jpid
                txtsup.Text = pname
                tdplanner.Attributes.Add("class", "label")
                imgplanner.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifier.gif")
                imgplanner.Attributes.Add("onclick", "getplanner('plan');")
                tdjp.Attributes.Add("class", "graylabel")
                If jprefstat <> "Complete" Then
                    imgplans.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
                    'imgplans.Attributes.Add("onclick", "")
                    imgaddplan.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                    'imgaddplan.Attributes.Add("onclick", "")
                    imgeditplan.Attributes.Add("src", "../images/appbuttons/minibuttons/pencildis.gif")
                    lbldis.Value = "1"
                Else
                    imgplans.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifier.gif")
                    'imgplans.Attributes.Add("onclick", "getplans();")
                    imgaddplan.Attributes.Add("src", "../images/appbuttons/minibuttons/addmod.gif")
                    'imgaddplan.Attributes.Add("onclick", "gotojp();")
                    imgeditplan.Attributes.Add("src", "../images/appbuttons/minibuttons/pencil.gif")
                    lbldis.Value = "0"
                End If

                tdref.InnerHtml = jpref
                lbljpref.Value = jpref
                cbreq.Checked = True
            End If
        End If
        

    End Sub

    Private Sub cbpr_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim jpid As String = lbljpid.Value
        Dim jpref As String = lbljpref.Value
        wonum = lblwo.Value
        Dim ret As Integer
        Dim dir As String
        wo.Open()
        'If cbpr.Checked = True Then
        'sql = "usp_addremjpref '" & wonum & "','" & jpref & "','add'"
        'ret = wo.Scalar(sql)
        'If ret = 1 Then
        'Dim strMessage As String =  tmod.getmsg("cdstr609" , "woplans.aspx.vb")
 
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")

        'End If
        'Else
        'sql = "usp_addremjpref '" & wonum & "','" & jpref & "','rem'"
        'wo.Update(sql)
        'End If
        GetJP()
        wo.Dispose()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1564.Text = axlabs.GetASPXPage("woplans.aspx", "lang1564")
        Catch ex As Exception
        End Try
        Try
            lang1565.Text = axlabs.GetASPXPage("woplans.aspx", "lang1565")
        Catch ex As Exception
        End Try
        Try
            lang1566.Text = axlabs.GetASPXPage("woplans.aspx", "lang1566")
        Catch ex As Exception
        End Try
        Try
            lang1567.Text = axlabs.GetASPXPage("woplans.aspx", "lang1567")
        Catch ex As Exception
        End Try
        Try
            lang1568.Text = axlabs.GetASPXPage("woplans.aspx", "lang1568")
        Catch ex As Exception
        End Try
        Try
            lang1569.Text = axlabs.GetASPXPage("woplans.aspx", "lang1569")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgaddplan.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("woplans.aspx", "imgaddplan") & "', ABOVE, LEFT)")
            imgaddplan.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgeditplan.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("woplans.aspx", "imgeditplan") & "', ABOVE, LEFT)")
            imgeditplan.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgplans.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("woplans.aspx", "imgplans") & "', ABOVE, LEFT)")
            imgplans.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
