Imports System.Data.SqlClient
Public Class pmreporting
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim comp As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim wonum, stat, ro, rostr, pmid, pmhid, sid, eqid, uid As String
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents tdact As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblpmhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lang1393 As System.Web.UI.WebControls.Label
    Protected WithEvents lang1394 As System.Web.UI.WebControls.Label
    Protected WithEvents lang1335a As System.Web.UI.WebControls.Label
    Protected WithEvents lang1336a As System.Web.UI.WebControls.Label
    Protected WithEvents lang1337a As System.Web.UI.WebControls.Label
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents tdf As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdpar As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtoo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlub As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlab As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents wo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifwo As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblupsav As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            ro = Request.QueryString("ro").ToString
            lblro.Value = ro
            If ro <> "1" Then
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = comp.CheckROS(rostr, "wo")
                    lblro.Value = ro
                End If
            End If
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            uid = Request.QueryString("uid").ToString
            lbluid.Value = uid
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = sid
            wonum = Request.QueryString("wo").ToString
            lblwo.Value = wonum
            stat = Request.QueryString("stat").ToString
            lblstat.Value = stat
            pmid = Request.QueryString("pmid").ToString
            lblpmid.Value = pmid
            sql = "select max(p1.pmhid) as pmhid from pmhist p1 where p1.pmid = '" & pmid & "'"
            comp.Open()
            dr = comp.GetRdrData(sql)
            While dr.Read
                pmhid = dr.Item("pmhid").ToString

            End While
            dr.Close()
            comp.Dispose()
            lblpmhid.Value = pmhid
            ifwo.Attributes.Add("src", "../appsman/pmfm.aspx?pmid=" & pmid & "&wo=" + wonum + "&stat=" + stat + "&ro=" + ro)
        End If
    End Sub

    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1393.Text = axlabs.GetASPXPage("wofm.aspx", "lang1393")
        Catch ex As Exception
        End Try
        Try
            lang1394.Text = axlabs.GetASPXPage("wofm.aspx", "lang1394")
        Catch ex As Exception
        End Try

    End Sub

End Class
