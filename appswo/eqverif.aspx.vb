﻿Imports System.Data.SqlClient
Public Class eqverif
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim eqv As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim eqnum, sid, wonum As String
    Dim eqid, eqret, eqdesc, fpc, location, parid, dept, did, cell, clid, who, wpage, usr As String
    Dim errflag As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            wpage = Request.QueryString("wpage").ToString
            lblwpage.Value = wpage
            eqnum = Request.QueryString("eqnum").ToString
            sid = Request.QueryString("sid").ToString
            wonum = Request.QueryString("wonum").ToString
            lblwonum.Value = wonum
            lblsid.Value = sid
            usr = Request.QueryString("usr").ToString
            eqv.Open()
            If wpage = "fs" Then
                addwo()
            End If
            If errflag <> 1 Then
                sql = "usp_eqauto '" & eqnum & "', '" & sid & "'"
                dr = eqv.GetRdrData(sql)
                While dr.Read
                    who = dr.Item("who").ToString
                    If who = "D" Or who = "L" Then
                        eqid = dr.Item("eqid").ToString
                        eqret = dr.Item("eqnum").ToString
                        eqdesc = dr.Item("eqdesc").ToString
                        fpc = dr.Item("fpc").ToString
                        If who = "D" Then
                            did = dr.Item("dept_id").ToString
                            dept = dr.Item("dept_line").ToString
                            cell = dr.Item("cell_name").ToString
                            clid = dr.Item("cellid").ToString
                        ElseIf who = "L" Then
                            location = dr.Item("location").ToString
                            parid = dr.Item("parid").ToString
                        End If
                    End If

                End While
                dr.Close()
                lblwho.Value = who
                If who = "L" Or who = "D" Then
                    lbleqid.Value = eqid
                    lbleqnum.Value = eqret
                    lbleqdesc.Value = eqdesc
                    lblfpc.Value = fpc
                    lbllocation.Value = location
                    lblparid.Value = parid
                    lbldid.Value = did
                    lbldept.Value = dept
                    lblcell.Value = cell
                    lblclid.Value = clid
                    savewo()
                Else
                    lbleqnum.Value = eqnum
                End If
            End If
            eqv.Dispose()
        End If
    End Sub
    Private Sub addwo()
        Dim def As String
        sql = "select wostatus from wostatus where compid = '0' and isdefault = 1"
        dr = eqv.GetRdrData(sql)
        While dr.Read
            def = dr.Item("wostatus").ToString
        End While
        dr.Close()
        sql = "insert into workorder (status, statusdate, changeby, changedate, reportedby, reportdate, " _
        + "estlabhrs, estlabcost, estmatcost, esttoolcost, estlubecost, " _
        + "actlabhrs, actlabcost, actmatcost, acttoolcost, actlubecost, " _
        + "estjplabhrs, estjplabcost, estjpmatcost, estjptoolcost, estjplubecost, " _
        + "actjplabhrs, actjplabcost, actjpmatcost, actjptoolcost, actjplubecost, " _
        + "totlabhrs, totlabcost, totmatcost, tottoolcost, totlubecost, siteid, scomp, lcomp) " _
        + "values ('" & def & "', getDate(),'" & usr & "', getDate(),'" & usr & "', getDate(), 0,0,0,0,0,0,0,0,0, " _
        + "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'" & sid & "',0,0) " _
        + "select @@identity"
        Try
            wonum = eqv.Scalar(sql)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr560", "woadd.aspx.vb")
            errflag = 1
            eqv.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        lblwonum.Value = wonum
        sql = "insert into wohist (wonum, wostatus, statusdate, changeby) values('" & wonum & "','" & def & "',getDate(),'" & usr & "')"
        Try
            eqv.Update(sql)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr561", "woadd.aspx.vb")
            errflag = 1
            eqv.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

    End Sub
    Private Sub savewo()
        sql = "delete from wofailmodes1 where wonum = '" & wonum & "'"
        eqv.Update(sql)
        If who = "L" Then
            Dim cmd As New SqlCommand
            cmd.CommandText = "update workorder set siteid = @sid, " _
            + "eqid = @eqid, eqnum = @eq, " _
            + "locid = @lidi, location = @lid, ld = 'L' where wonum = @wonum"
            Dim param = New SqlParameter("@wonum", SqlDbType.Int)
            param.Value = wonum
            cmd.Parameters.Add(param)
            Dim param01 = New SqlParameter("@sid", SqlDbType.VarChar)
            If sid = "" Then
                param01.Value = System.DBNull.Value
            Else
                param01.Value = sid
            End If
            cmd.Parameters.Add(param01)
            Dim param04 = New SqlParameter("@eqid", SqlDbType.VarChar)
            If eqid = "" Then
                param04.Value = System.DBNull.Value
            Else
                param04.Value = eqid
            End If
            cmd.Parameters.Add(param04)
            Dim param05 = New SqlParameter("@eq", SqlDbType.VarChar)
            If eqret = "" Then
                param05.Value = System.DBNull.Value
            Else
                param05.Value = eqret
            End If
            cmd.Parameters.Add(param05)

            Dim param10 = New SqlParameter("@lidi", SqlDbType.VarChar)
            If parid = "" Then
                param10.Value = System.DBNull.Value
            Else
                param10.Value = parid
            End If
            cmd.Parameters.Add(param10)
            Dim param11 = New SqlParameter("@lid", SqlDbType.VarChar)
            If location = "" Then
                param11.Value = System.DBNull.Value
            Else
                param11.Value = location
            End If
            cmd.Parameters.Add(param11)
            eqv.UpdateHack(cmd)
        Else
            Dim cmd As New SqlCommand
            cmd.CommandText = "update workorder set siteid = @sid, deptid = @did, cellid = @cid, " _
            + "eqid = @eqid, eqnum = @eq, ld = 'D' where wonum = @wonum"
            Dim param = New SqlParameter("@wonum", SqlDbType.VarChar)
            param.Value = wonum
            cmd.Parameters.Add(param)
            Dim param01 = New SqlParameter("@sid", SqlDbType.VarChar)
            If sid = "" Then
                param01.Value = System.DBNull.Value
            Else
                param01.Value = sid
            End If
            cmd.Parameters.Add(param01)
            Dim param02 = New SqlParameter("@did", SqlDbType.VarChar)
            If did = "" Then
                param02.Value = System.DBNull.Value
            Else
                param02.Value = did
            End If
            cmd.Parameters.Add(param02)
            Dim param03 = New SqlParameter("@cid", SqlDbType.VarChar)
            If clid = "0" Or clid = "" Then
                param03.Value = System.DBNull.Value
            Else
                param03.Value = clid
            End If
            cmd.Parameters.Add(param03)
            Dim param04 = New SqlParameter("@eqid", SqlDbType.VarChar)
            If eqid = "" Then
                param04.Value = System.DBNull.Value
            Else
                param04.Value = eqid
            End If
            cmd.Parameters.Add(param04)
            Dim param05 = New SqlParameter("@eq", SqlDbType.VarChar)
            If eqret = "" Then
                param05.Value = System.DBNull.Value
            Else
                param05.Value = eqret
            End If
            cmd.Parameters.Add(param05)
            eqv.UpdateHack(cmd)
        End If
    End Sub
End Class