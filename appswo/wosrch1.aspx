﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wosrch1.aspx.vb" Inherits="lucy_r12.wosrch1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>wosrchd</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
   
    <script language="javascript" type="text/javascript">
    <!--
        function getstat() {

            var wo = document.getElementById("lblwonum").value;
            var eReturn1 = window.showModalDialog("wostatusdialog.aspx?typ=stat&wo=" + wo, "", "dialogHeight:370px; dialogWidth:370px; resizable=yes");
            //alert("getstat")
            //alert(eReturn)
            //if (eReturn == undefined) // We don't know here if undefined is the real result...
            //{
            // So we take no chance, in case this is the Google Chrome bug
            //eReturn = window.returnValue;
            //alert()
            //}

            if (eReturn1) {

                if (eReturn1 != "") {
                    document.getElementById("tdstat").innerHTML = eReturn1;
                    document.getElementById("lblstat").value = eReturn1;
                }
            }
        }
        function retvals() {
            var reti = 0;
            var waid = document.getElementById("lblwaid").value;
            if (waid.length > 0) { reti += 1; }

            var wastr = document.getElementById("lblwaidstr").value;
            //if (wastr.length > 0) { reti += 1; }
            var wpaid = document.getElementById("lblwpaid").value;
            if (wpaid.length > 0) { reti += 1; }
            var wpstr = document.getElementById("lblwpaidstr").value;
            //if (wpstr.length > 0) { reti += 1; }
            var fdate = document.getElementById("lblfromdate").value;
            if (fdate.length > 0) { reti += 1; }
            var tdate = document.getElementById("lbltodate").value;
            if (tdate.length > 0) { reti += 1; }
            var wt = document.getElementById("lblwt").value;
            if (wt.length > 0) { reti += 1; }
            var stat = document.getElementById("lblstat").value;
            if (stat.length > 0) { reti += 1; }
            var wo = document.getElementById("txtwo").value;
            if (wo.length > 0) { reti += 1; }
            var cbua = document.getElementById("cbua")
            var cbt = document.getElementById("cbtarg")
            var cbsa = document.getElementById("cbsa")
            var cbco = document.getElementById("cbco")
            var rbwaw = document.getElementById("rbwoo")
            var rbwal = document.getElementById("rblao")
            var sup = document.getElementById("lblsup").value
            if (sup.length > 0) { reti += 1; }
            var lead = document.getElementById("lbllead").value
            if (lead.length > 0) { reti += 1; }
            var did = document.getElementById("lbldid").value
            if (did.length > 0) { reti += 1; }
            var clid = document.getElementById("lblclid").value
            if (clid.length > 0) { reti += 1; }
            var lid = document.getElementById("lbllid").value
            if (lid.length > 0) { reti += 1; }
            var eqid = document.getElementById("lbleqid").value
            if (eqid.length > 0) { reti += 1; }
            var fuid = document.getElementById("lblfuid").value
            if (fuid.length > 0) { reti += 1; }
            var coid = document.getElementById("lblcoid").value
            if (coid.length > 0) { reti += 1; }
            var rettyp = document.getElementById("lblrettyp").value
            if (rettyp.length > 0) { reti += 1; }
            var level = document.getElementById("lbllevel").value
            if (level.length > 0) { reti += 1; }
            var jl = document.getElementById("lbljustloc").value


            var ski = document.getElementById("lblskillid").value
            if (ski.length > 0) { reti += 1; }

            var rall = document.getElementById("rball");
            var rjl = document.getElementById("rbjl");
            if (rall.checked == true) {
                jl = "0";
            }
            else {
                jl = "1";
            }
            var fpc = document.getElementById("txtfpc").value
            if (fpc.length > 0) { reti += 1; }
            var rt = "0";
            var sa = "0";
            var wa = "0";
            var ua = "0"

            if (cbua.checked == true) {
                ua = "1";
            }
            if (rbwaw.checked == true) {
                wa = "1";
                //reti += 1;
            }
            if (cbt.checked == true) {
                rt = "1";
                reti += 1;
            }
            if (cbsa.checked == true) {
                sa = "1";
                reti += 1;
            }

            var cbs = document.getElementById("cbsched")
            var rs = "0";
            if (cbs.checked == true) {
                rs = "1";
                if (fdate != "" || tdate != "") {
                    reti += 1;
                }

            }

            var cba = document.getElementById("cbact")
            var ra = "0";
            if (cba.checked == true) {
                ra = "1";
                reti += 1;
            }
            var co = "0";
            if (cbco.checked == true) {
                co = "1";
                reti += 1;
                //alert("co")
            }

            if (isNaN(wo)) {
                alert("Work Order Number Must be a Numeric Value")
            }
            else {
                //alert(reti)
                var ret = waid + "~" + wpaid + "~" + fdate + "~" + tdate + "~" + wt + "~" + stat + "~" + wo + "~" + wastr + "~" + wpstr + "~" + rt + "~" + rs + "~" + ra + "~" + sa + "~" + wa + "~" + sup + "~" + lead + "~" + did + "~" + clid + "~" + lid + "~" + eqid + "~" + fuid + "~" + coid + "~" + rettyp + "~" + level + "~" + jl + "~" + fpc + "~" + co + "~" + ski + "~" + reti + "~" + ua;
                //alert(ret)
            }
            window.parent.handlereturn(ret);
        }

        
        
        //-->
    </script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td class="label">
                Work Order#
            </td>
            <td>
                <asp:TextBox ID="txtwo" runat="server" Width="80px" CssClass="plainlabel"></asp:TextBox>
            </td>
            <td colspan="6" align="right">
                <img onclick="resetp('all');" alt="" src="../images/appbuttons/minibuttons/switch2.gif">
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <table>
                    <tr>
                        <td id="tddepts" class="bluelabel" runat="server">
                            Use Departments
                        </td>
                        <td>
                            <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                        <td id="tdlocs1" class="bluelabel" runat="server">
                            Use Locations
                        </td>
                        <td>
                            <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trdepts" runat="server" class="details">
            <td class="label">
                Department
            </td>
            <td colspan="3" class="plainlabel" id="tddept" runat="server">
            </td>
            <td class="label">
                Station/Cell
            </td>
            <td colspan="3" class="plainlabel" id="tdcell" runat="server">
            </td>
        </tr>
        <tr id="trdepts1" runat="server" class="details">
            <td class="label">
                Equipment
            </td>
            <td colspan="3" class="plainlabel" id="tdeq" runat="server">
            </td>
            <td class="label">
                Function
            </td>
            <td colspan="3" class="plainlabel" id="tdfu" runat="server">
            </td>
        </tr>
        <tr id="trdepts2" runat="server" class="details">
            <td class="label">
                Component
            </td>
            <td colspan="7" class="plainlabel" id="tdcomp" runat="server">
            </td>
        </tr>
        <tr id="trloc3" runat="server" class="details">
            <td class="label">
                Location
            </td>
            <td colspan="7" class="plainlabel" id="tdloc3" runat="server">
            </td>
        </tr>
        <tr id="trloc3a" runat="server" class="details">
            <td class="label">
                Equipment
            </td>
            <td colspan="3" class="plainlabel" id="tdeq3" runat="server">
            </td>
            <td class="label">
                Function
            </td>
            <td colspan="3" class="plainlabel" id="tdfu3" runat="server">
            </td>
        </tr>
        <tr id="trloc3b" runat="server" class="details">
            <td class="label">
                Component
            </td>
            <td colspan="7" class="plainlabel" id="tdcomp3" runat="server">
            </td>
        </tr>
        <tr id="trloc3c" runat="server" class="details">
            <td colspan="8" align="center" class="plainlabel">
                <input type="radio" id="rball" name="rbw" />Search All Child Locations&nbsp;<input
                    type="radio" id="rbjl" name="rbw" checked />Just Search This Location
            </td>
        </tr>
        <tr id="trloc3d" runat="server" class="details">
            <td colspan="8" align="center" class="plainlabelblue">
                Selecting Search All Child Locations will return up to 5 Child Levels Only
            </td>
        </tr>
        <tr>
            <td colspan="8">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang874" runat="server">Supervisor</asp:Label>
            </td>
            <td class="plainlabel" id="tdsup" colspan="2" runat="server">
            </td>
            <td>
                <img title="Select Supervisor"
                    alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                    border="0" onclick="getsuper('sup');" />
            </td>
            <td class="label">
                <asp:Label ID="lang875" runat="server">Lead Craft</asp:Label>
            </td>
            <td class="plainlabel" id="tdlead" colspan="2" runat="server">
            </td>
            <td>
                <img title="Select Lead Craft"
                    alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                    border="0" onclick="getsuper('lead');" />
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang1375" runat="server">Skill</asp:Label>
            </td>
            <td id="tdskill" class="plainlabel" colspan="2" runat="server">
            </td>
            <td>
                <img id="Img3" onclick="getsk();" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                    runat="server">
            </td>
        </tr>
        <tr>
            <td class="label">
                Work Area
            </td>
            <td id="tdwa" class="plainlabel" runat="server">
            </td>
            <td>
                <img onclick="getwa('d');" src="../images/appbuttons/minibuttons/magnifier.gif">
            </td>
            <td>
                <img onclick="resetp('wa');" alt="" src="../images/appbuttons/minibuttons/switch2.gif">
            </td>
            <td colspan="4" class="plainlabel">
                <input type="radio" id="rbwoo" name="rbwa" runat="server" onclick="checkjust('jl');" />By
                Work Order<input type="radio" id="rblao" name="rbwa" runat="server" checked onclick="checkjust('al');" />By
                Labor
            </td>
        </tr>
        <tr class="view">
            <td class="label">
                Planner
            </td>
            <td id="tdpa" class="plainlabel" runat="server">
            </td>
            <td>
                <img onclick="getpa();" src="../images/appbuttons/minibuttons/magnifier.gif">
            </td>
            <td>
                <img onclick="resetp('wp');" alt="" src="../images/appbuttons/minibuttons/switch2.gif">
            </td>
        </tr>
        <tr>
            <td class="label">
                Work Status
            </td>
            <td id="tdstat" class="plainlabel" runat="server">
            </td>
            <td>
                <img onclick="getstat();" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
            </td>
            <td>
                <img onclick="resetp('stat');" alt="" src="../images/appbuttons/minibuttons/switch2.gif">
            </td>
            <td class="plainlabel">
                <input type="checkbox" id="cbua" runat="server" />&nbsp;Use Any
            </td>
        </tr>
        <tr>
            <td class="label">
                Work Type
            </td>
            <td id="tdwt" class="plainlabel" runat="server">
            </td>
            <td>
                <img id="imgwt" onclick="getwt();" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                    runat="server">
            </td>
            <td>
                <img onclick="resetp('wt');" alt="" src="../images/appbuttons/minibuttons/switch2.gif">
            </td>
        </tr>
        <tr>
            <td class="label">
                Column
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtfpc" runat="server" CssClass="plainlabel"></asp:TextBox>
            </td>
            <td>
                <img onclick="resetp('fpc');" alt="" src="../images/appbuttons/minibuttons/switch2.gif">
            </td>
        </tr>
        <tr id="trgetdates" runat="server">
            <td class="label">
                <asp:Label ID="lang1543" runat="server">From Date</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtfrom" runat="server" ReadOnly="True" Font-Size="9pt" CssClass="plainlabel"></asp:TextBox>
            </td>
            <td>
                <img onclick="getcal('txtfrom');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg">
            </td>
            <td>
                <img onclick="resetp('dfrom');" alt="" src="../images/appbuttons/minibuttons/switch2.gif">
            </td>
            <td class="label">
                <asp:Label ID="lang1544" runat="server">To Date</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtto" runat="server" ReadOnly="True" Font-Size="9pt" CssClass="plainlabel"></asp:TextBox>
            </td>
            <td>
                <img onclick="getcal('txtto');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                    width="18" height="18">
            </td>
            <td>
                <img onclick="resetp('dto');" alt="" src="../images/appbuttons/minibuttons/switch2.gif">
            </td>
        </tr>
        <tr id="trdpick" class="details" runat="server">
            <td colspan="8" align="center" class="plainlabelblue">
                <input type="radio" id="cbtarg" runat="server" name="rbdt" />Use Target
                <input type="radio" id="cbsched" runat="server" name="rbdt" />Use Scheduled
                <input type="radio" id="cbact" runat="server" name="rbdt" />Use Actual
            </td>
        </tr>
        <tr>
            <td colspan="8" class="plainlabel">
                <input type="checkbox" id="cbco" runat="server" />Critical Equipment Only
            </td>
        </tr>
        <tr id="trsa" runat="server">
            <td colspan="1" class="plainlabel">
                <input type="checkbox" id="cbsa" runat="server" />Show All
            </td>
            <td colspan="7" class="plainlabelblue">
                * Bypasses Labor Log In User ID Filter
            </td>
        </tr>
        <tr>
            <td colspan="8" align="right">
                <img onclick="retvals();" src="../images/appbuttons/minibuttons/savedisk1.gif">
            </td>
        </tr>
        <tr>
            <td colspan="8" class="plainlabelblue">
                <b><u>Date Search Defaults - PM and TPM Records</u></b><br />
                Using Target - From Target Start to Target Start<br />
                Using Scheduled - From Scheduled Start to Scheduled Start<br />
                Using Actual - From Actual Complete to Actual Complete<br />
                <br />
                <b><u>Date Search Defaults - All Other Work Types</u></b><br />
                Using Target - From Target Start to Target Complete<br />
                Using Scheduled - From Scheduled Start to Scheduled Complete<br />
                Using Actual - From Actual Start to Actual Complete<br />
                <br />
                <br />
                <b><u></u></b>
            </td>
        </tr>
    </table>
    <input id="lblwonum" type="hidden" runat="server">
    <input id="lblsid" type="hidden" runat="server">
    <input id="lblwaid" type="hidden" runat="server">
    <input id="lblwpaid" type="hidden" runat="server">
    <input id="lblfromdate" type="hidden" runat="server">
    <input id="lbltodate" type="hidden" runat="server">
    <input id="lblstat" type="hidden" runat="server">
    <input id="lblwt" type="hidden" runat="server">
    <input type="hidden" id="lblfilt" runat="server"><input type="hidden" id="lblwaidstr"
        runat="server">
    <input type="hidden" id="lblwpaidstr" runat="server">
    <input type="hidden" id="lblissaw" runat="server" />
    <input type="hidden" id="lblsup" runat="server" />
    <input type="hidden" id="lbllead" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lblclid" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lbldept" runat="server" />
    <input type="hidden" id="lblcell" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lblfuid" runat="server" />
    <input type="hidden" id="lblfu" runat="server" />
    <input type="hidden" id="lblcoid" runat="server" />
    <input type="hidden" id="lblcomp" runat="server" />
    <input type="hidden" id="lbllid" runat="server" />
    <input type="hidden" id="lblloc" runat="server" />
    <input type="hidden" id="lblrettyp" runat="server" />
    <input type="hidden" id="lbllevel" runat="server" />
    <input type="hidden" id="lbljustloc" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblskillid" runat="server" />
    <input type="hidden" id="lblskill" runat="server" />
    </form>
</body>
</html>
