

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wrdet
    Inherits System.Web.UI.Page
	Protected WithEvents ovid208 As System.Web.UI.HtmlControls.HtmlInputButton

	Protected WithEvents ovid207 As System.Web.UI.HtmlControls.HtmlInputButton

	Protected WithEvents lang1610 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1609 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1608 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1607 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1606 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1605 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1604 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1603 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1602 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1601 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1600 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1599 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1598 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1597 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1596 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1595 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1594 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim wo As New Utilities
    Dim cid, wonum, jump, ro, wostr, rostr, Login, reqd, usr As String
    Protected WithEvents tdsuper As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtwodesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblreqd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstatus As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblexit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdemail As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtcharge As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttstart As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttcomp As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tdreq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdreqdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdstatus As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdstatdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                'btnsavetask.ImageUrl = "../images/appbuttons/minibuttons/savedisk1dis.gif"
                'btnsavetask.Enabled = False
            Else
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = wo.CheckROS(rostr, "wr")
                    lblro.Value = ro
                    If ro = "1" Then
                        'btnsavetask.ImageUrl = "../images/appbuttons/minibuttons/savedisk1dis.gif"
                        'btnsavetask.Enabled = False
                    End If
                End If
            End If
            usr = HttpContext.Current.Session("username").ToString
            lbluser.Value = usr
            cid = "0"
            lblcid.Value = cid
            wo.Open()
            GetLists()
            jump = Request.QueryString("jump").ToString
            If jump = "yes" Then

            Else
                Try
                    wonum = Request.QueryString("wo").ToString
                    lblwo.Value = wonum
                    reqd = Request.QueryString("reqd").ToString
                    lblreqd.Value = reqd
                    GetWo()
                Catch ex As Exception

                End Try
            End If
            wo.Dispose()
        Else
            If Request.Form("lblsubmit") = "draft" Then
                lblsubmit.Value = ""
                wo.Open()
                SaveDraft("draft")
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "save" Then
                lblsubmit.Value = ""
                wo.Open()
                SaveDraft("save")
                wo.Dispose()
            End If
        End If
    End Sub
    Private Sub DisableFields()
        txtwodesc.ReadOnly = True
        ddskill.Enabled = False
        txtwodesc.CssClass = "plainlabelblue"
        txtcharge.CssClass = "plainlabelblue"
        txttstart.CssClass = "plainlabelblue"
        txttcomp.CssClass = "plainlabelblue"
        tdreq.Attributes.Add("class", "plainlabelblue")
        tdreqdate.Attributes.Add("class", "plainlabelblue")
        tdwt.Attributes.Add("class", "plainlabelblue")
        tdstatus.Attributes.Add("class", "plainlabelblue")
        tdstatdate.Attributes.Add("class", "plainlabelblue")

        tdsuper.Attributes.Add("class", "plainlabelblue")
        tdphone.Attributes.Add("class", "plainlabelblue")
        tdemail.Attributes.Add("class", "plainlabelblue")

    End Sub
    Private Sub GetWo()
        cid = lblcid.Value
        wonum = lblwo.Value
        Dim stat As String
        sql = "select *, Convert(char(10),targstartdate,101) as 'tstart', " _
        + "Convert(char(10),targcompdate,101) as 'tcomp' " _
        + "from workorder where wonum = '" & wonum & "'"
        dr = wo.GetRdrData(sql)
        Dim tss, tsc, tas, tac, tts, ttc, se, le, supid, did, lid, sid
        While dr.Read
            tdreq.InnerHtml = dr.Item("reportedby").ToString
            tdreqdate.InnerHtml = dr.Item("reportdate").ToString
            supid = dr.Item("superid").ToString
            txtwodesc.Text = dr.Item("description").ToString
            tdwt.InnerHtml = dr.Item("worktype").ToString
            'Try
            'ddstat.SelectedValue = dr.Item("status").ToString
            'Catch ex As Exception

            'End Try
            did = dr.Item("deptid").ToString
            lid = dr.Item("locid").ToString
            sid = dr.Item("siteid").ToString
            lbldid.Value = did
            lbllid.Value = lid
            lblsid.Value = sid

            stat = dr.Item("status").ToString
            tdstatus.InnerHtml = stat
            lblstatus.Value = stat
            tdstatdate.InnerHtml = dr.Item("statusdate").ToString
            'Try
            'ddtype.SelectedValue = dr.Item("worktype").ToString
            'Catch ex As Exception

            'End Try
            txtcharge.Text = dr.Item("chargenum").ToString
            'txtplan.Text = dr.Item("jpnum").ToString
            lbljpid.Value = dr.Item("jpid").ToString
            tts = dr.Item("tstart").ToString
            If tts <> "" Then
                txttstart.Text = tts
            End If
            ttc = dr.Item("tcomp").ToString
            If ttc <> "" Then
                txttcomp.Text = ttc
            End If

            'tss = dr.Item("schedstart").ToString
            'If tss <> "" Then
            'txtsstart.Text = tss
            'End If
            'tsc = dr.Item("schedfinish").ToString
            'If tsc <> "" Then
            'txtscomp.Text = tsc
            'End If

            'tas = dr.Item("actstart").ToString
            'If tas <> "" Then
            'txtastart.Text = tas
            'End If
            'tac = dr.Item("actfinish").ToString
            'If tac <> "" Then
            'txtacomp.Text = tac
            'End If

            'txtlead.Text = dr.Item("leadcraft").ToString
            'txtsup.Text = dr.Item("supervisor").ToString

            'txtestlh.Text = dr.Item("estlabhrs").ToString

            lblpmid.Value = dr.Item("pmid").ToString

            Try
                ddskill.SelectedValue = dr.Item("skillid").ToString
            Catch ex As Exception

            End Try
            'se = dr.Item("supalert").ToString
            'le = dr.Item("leadalert").ToString
        End While
        dr.Close()
        sql = "select longdesc from wolongdesc where wonum = '" & wonum & "'"
        dr = wo.GetRdrData(sql)
        While dr.Read
            txtwodesc.Text += dr.Item("longdesc").value
        End While
        dr.Close()
        If supid <> "" Then
            sql = "select username, phonenum, email from pmsysusers where userid = '" & supid & "'"
            dr = wo.GetRdrData(sql)
            While dr.Read
                tdsuper.InnerHtml = dr.Item("username").ToString
                tdphone.InnerHtml = dr.Item("phonenum").ToString
                tdemail.InnerHtml = dr.Item("email").ToString
            End While
            dr.Close()
        Else
            sql = "select username, phonenum, email from pmsysusers where dfltps = '" & sid & "' and isdefault = 1"
            dr = wo.GetRdrData(sql)
            While dr.Read
                tdsuper.InnerHtml = dr.Item("username").ToString
                tdphone.InnerHtml = dr.Item("phonenum").ToString
                tdemail.InnerHtml = dr.Item("email").ToString
            End While
            dr.Close()
        End If
        'If se = "1" Then
        'cbsupe.Checked = True
        'End If
        'If le = "1" Then
        'cbleade.Checked = True
        'End If
        If stat <> "HOLD" Then
            DisableFields()
        End If
    End Sub
    Private Sub GetLists()
        'get default

        sql = "select skillid, skill " _
        + "from pmSkills where compid = '" & cid & "'"
        dr = wo.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("Select"))
        ddskill.Items(0).Value = 0

    End Sub
    Private Sub SaveDesc()
        wonum = lblwo.Value
        Dim sh, lg As String
        Dim lgcnt As Integer
        sh = txtwodesc.Text
        'lg = txtlongdesc.Text
        sh = Replace(sh, "'", Chr(180), , , vbTextCompare)
        sh = Replace(sh, "--", "-", , , vbTextCompare)
        sh = Replace(sh, ";", ":", , , vbTextCompare)
        sh = Replace(sh, "/", " ", , , vbTextCompare)
        If Len(sh) > 79 Then
            lg = Mid(sh, 80)
            sh = Mid(sh, 1, 79)

        End If

        Dim cmd As New SqlCommand("exec usp_savewodesc @wonum, @sh, @lg")
        Dim param0 = New SqlParameter("@wonum", SqlDbType.Int)
        param0.Value = wonum
        cmd.Parameters.Add(param0)
        Dim param01 = New SqlParameter("@sh", SqlDbType.VarChar)
        If sh = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = sh
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@lg", SqlDbType.Text)
        If lg = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = lg
        End If
        cmd.Parameters.Add(param02)
        Try
            wo.UpdateHack(cmd)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr617" , "wrdet.aspx.vb")
 
            wo.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        'sql = "update workorder set description = '" & sh & "' where wonum = '" & wonum & "'"
        'wo.Update(sql)
        'lg = Mid(lg, 80)
        'sql = "select count(*) from wolongdesc where wonum = '" & wonum & "'"
        'lgcnt = wo.Scalar(sql)
        'If lgcnt <> 0 Then
        'sql = "update wolongdesc set longdesc = '" & lg & "' where wonum = '" & wonum & "'"
        'wo.Update(sql)
        'Else
        'sql = "insert into wolongdesc (wonum, longdesc) values ('" & wonum & "','" & lg & "')"
        'wo.Update(sql)
        'End If
        'Else
        'sql = "update workorder set description = '" & sh & "' where wonum = '" & wonum & "'" _
        '+ "delete from wolongdesc where wonum = '" & wonum & "'"
        'wo.Update(sql)

        'lblsav.Value = "0"
    End Sub
    Private Sub SaveDesc1()
        wonum = lblwo.Value
        Dim sh, lg As String
        Dim lgcnt As Integer
        sh = txtwodesc.Text
        sh = Replace(sh, "'", Chr(180), , , vbTextCompare)
        sh = Replace(sh, "--", "-", , , vbTextCompare)
        sh = Replace(sh, ";", ":", , , vbTextCompare)
        sh = Replace(sh, "/", " ", , , vbTextCompare)
        'lg = txtlongdesc.Text
        If Len(sh) > 79 Then
            sh = Mid(sh, 1, 79)
            sql = "update workorder set description = '" & sh & "' where wonum = '" & wonum & "'"
            wo.Update(sql)
            lg = Mid(sh, 80)
            sql = "select count(*) from wolongdesc where wonum = '" & wonum & "'"
            lgcnt = wo.Scalar(sql)
            If lgcnt <> 0 Then
                sql = "update wolongdesc set longdesc = '" & lg & "' where wonum = '" & wonum & "'"
                wo.Update(sql)
            Else
                sql = "insert into wolongdesc (wonum, longdesc) values ('" & wonum & "','" & lg & "')"
                wo.Update(sql)
            End If
        Else
            sql = "update workorder set description = '" & sh & "' where wonum = '" & wonum & "'" _
            + "delete from wolongdesc where wonum = '" & wonum & "'"
            wo.Update(sql)
        End If
        'lblsav.Value = "0"
    End Sub
    Private Sub SaveDraft(ByVal savetype As String)
        cid = lblcid.Value
        wonum = lblwo.Value
        reqd = lblreqd.Value
        usr = lbluser.Value

        Dim stat, statusdate, typ, charge, jpnum, tstart, tcomp, sstart, scomp, astart, acomp, lead, sup, hrs, skill, se, le
        Dim statdate As DateTime = wo.CNOW
        charge = txtcharge.Text

        tstart = txttstart.Text
        tcomp = txttcomp.Text

        If ddskill.SelectedIndex <> 0 Then
            skill = ddskill.SelectedValue
        Else
            skill = "0"
        End If
        If savetype = "draft" Then
            stat = "HOLD"
        Else
            sql = "select wostatus from wostatus where compid = '" & cid & "' and isdefault = 1"
            dr = wo.GetRdrData(sql)
            While dr.Read
                stat = dr.Item("wostatus").ToString
            End While
            dr.Close()

        End If
        lblexit.Value = "yes" 'was in else <> draft

        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_updatewr @charge, @tstart, @tcomp, @skillid, @wonum, @stat, @statdate, @uid"

        Dim param = New SqlParameter("@charge", SqlDbType.VarChar)
        If charge = "" Then
            param.Value = System.DBNull.Value
        Else
            param.Value = charge
        End If
        cmd.Parameters.Add(param)
        Dim param01 = New SqlParameter("@tstart", SqlDbType.VarChar)
        If tstart = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = tstart
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@tcomp", SqlDbType.VarChar)
        If tcomp = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = tcomp
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@skillid", SqlDbType.VarChar)
        If skill = "" Then
            param03.Value = System.DBNull.Value
        Else
            param03.Value = skill
        End If
        cmd.Parameters.Add(param03)
        Dim param04 = New SqlParameter("@wonum", SqlDbType.VarChar)
        If wonum = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = wonum
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@stat", SqlDbType.VarChar)
        If stat = "" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = stat
        End If
        cmd.Parameters.Add(param05)
        Dim param06 = New SqlParameter("@statdate", SqlDbType.VarChar)
        If stat = "" Then
            param06.Value = System.DBNull.Value
        Else
            param06.Value = statdate
        End If
        cmd.Parameters.Add(param06)

        Dim param07 = New SqlParameter("@uid", SqlDbType.VarChar)
        If usr = "" Then
            param07.Value = System.DBNull.Value
        Else
            param07.Value = usr
        End If
        cmd.Parameters.Add(param07)

        'wo.Open()
        wo.UpdateHack(cmd)

        SaveDesc()
        If reqd = "ok" Then
            UpdateSuper(savetype)
        End If
        If savetype = "save" Then
            Dim mail As New wrmail
            mail.GetWRMail(wonum)
        End If
        GetWo()


        'wo.Dispose()
    End Sub
    Private Sub btnsavetask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)


    End Sub
    Private Sub UpdateSuper(ByVal savetype As String)
        Dim skill As String = ddskill.SelectedValue.ToString
        Dim supid, sid, did, locid, wonum, super, phone, email As String
        wonum = lblwo.Value
        did = lbldid.Value
        locid = lbllid.Value
        sid = lblsid.Value
        If ddskill.SelectedIndex <> 0 And ddskill.SelectedIndex <> -1 Then
            'sql = "select siteid, deptid, locid from workorder where wonum = '" & wonum & "'"
            'wo.Open()
            'dr = wo.GetRdrData(sql)
            'While dr.Read
            'sid = dr.Item("siteid").ToString
            'did = dr.Item("deptid").ToString
            'locid = dr.Item("locid").ToString
            'End While
            'dr.Close()
            If did <> "" Then
                sql = "select top 1 s.superid from pmsuperlocs sl " _
                + "left join pmsupercrafts sk1 on sk1.superid = sl.superid " _
                + "left join pmsysusers s on s.userid = sl.superid " _
                + "where(sl.siteid = '" & sid & "' and sl.dept_id = '" & did & "' and sk1.skillid = '" & skill & "')"
            ElseIf locid <> "" Then
                sql = "select top 1 s.superid from pmsuperlocs sl " _
                + "left join pmsupercrafts sk1 on sk1.superid = sl.superid " _
                + "left join pmsysusers s on s.userid = sl.superid " _
                + "where(sl.siteid = '" & sid & "' and sl.locid = '" & locid & "' and sk1.skillid = '" & skill & "')"
            Else
                sql = ""
            End If
            If sql <> "" Then
                Try
                    supid = wo.strScalar(sql)
                Catch ex As Exception

                End Try
            End If
            If supid = "" Then
                If did <> "" Then
                    sql = "select top 1 s.superid from pmsuperlocs sl " _
                    + "left join pmsuper s on s.superid = sl.superid " _
                    + "where(sl.siteid = '" & sid & "' and sl.dept_id = '" & did & "')"
                ElseIf locid <> "" Then
                    sql = "select top 1 s.superid from pmsuperlocs sl " _
                    + "left join pmsuper s on s.superid = sl.superid " _
                    + "where(sl.siteid = '" & sid & "' and sl.locid = '" & locid & "')"
                Else
                    sql = ""
                End If
                If sql <> "" Then
                    Try
                        supid = wo.strScalar(sql)
                    Catch ex As Exception

                    End Try

                End If
            End If
            If supid = "" Then
                sql = "select userid from pmsysusers where isdefault = 1 and uid in (select uid from " _
                + "pmusersites where siteid = '" & sid & "')"
                Try
                    supid = wo.strScalar(sql)
                Catch ex As Exception

                End Try
            End If
            If supid <> "" Then
                sql = "update workorder set superid = '" & supid & "', supervisor = (select username from " _
                + "pmsysusers where userid = '" & supid & "') where wonum = '" & wonum & "'"
                wo.Update(sql)


            End If

        Else
            If did <> "" Then
                sql = "select top 1 s.superid from pmsuperlocs sl " _
                + "left join pmsuper s on s.superid = sl.superid " _
                + "where(sl.siteid = '" & sid & "' and sl.dept_id = '" & did & "')"
            ElseIf locid <> "" Then
                sql = "select top 1 s.superid from pmsuperlocs sl " _
                + "left join pmsuper s on s.superid = sl.superid " _
                + "where(sl.siteid = '" & sid & "' and sl.locid = '" & locid & "')"
            Else
                sql = ""
            End If
            If sql <> "" Then
                Try
                    supid = wo.strScalar(sql)
                Catch ex As Exception

                End Try

            End If
            If supid = "" Then
                sql = "select userid from pmsysusers where isdefault = 1 and uid in (select uid from " _
                + "pmusersites where siteid = '" & sid & "')"
                Try
                    supid = wo.strScalar(sql)
                Catch ex As Exception

                End Try

            End If
            If supid <> "" Then
                sql = "update workorder set superid = '" & supid & "', supervisor = (select username from " _
               + "pmsysusers where userid = '" & supid & "') where wonum = '" & wonum & "'"
                wo.Update(sql)


            End If



        End If

    End Sub
    Private Sub ddskill_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddskill.SelectedIndexChanged
        'wo.Open()
        'UpdateSuper()
        'wo.Dispose()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1594.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1594")
        Catch ex As Exception
        End Try
        Try
            lang1595.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1595")
        Catch ex As Exception
        End Try
        Try
            lang1596.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1596")
        Catch ex As Exception
        End Try
        Try
            lang1597.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1597")
        Catch ex As Exception
        End Try
        Try
            lang1598.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1598")
        Catch ex As Exception
        End Try
        Try
            lang1599.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1599")
        Catch ex As Exception
        End Try
        Try
            lang1600.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1600")
        Catch ex As Exception
        End Try
        Try
            lang1601.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1601")
        Catch ex As Exception
        End Try
        Try
            lang1602.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1602")
        Catch ex As Exception
        End Try
        Try
            lang1603.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1603")
        Catch ex As Exception
        End Try
        Try
            lang1604.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1604")
        Catch ex As Exception
        End Try
        Try
            lang1605.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1605")
        Catch ex As Exception
        End Try
        Try
            lang1606.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1606")
        Catch ex As Exception
        End Try
        Try
            lang1607.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1607")
        Catch ex As Exception
        End Try
        Try
            lang1608.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1608")
        Catch ex As Exception
        End Try
        Try
            lang1609.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1609")
        Catch ex As Exception
        End Try
        Try
            lang1610.Text = axlabs.GetASPXPage("wrdet.aspx", "lang1610")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            ovid207.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("wrdet.aspx", "ovid207") & "')")
            ovid207.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid208.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("wrdet.aspx", "ovid208") & "')")
            ovid208.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
