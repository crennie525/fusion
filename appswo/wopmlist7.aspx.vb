Imports System.Data.SqlClient
Imports System.text
Public Class wopmlist7
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim mm As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 12
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim rowcnt As Integer
    Dim sid, typ, ws, we, who, wo, uid, username, islabor, isplanner, issuper, Logged_In, appstr, ms, ro As String
    Protected WithEvents divhdrr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblxCoord_sbr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblyCoord_sbr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwkid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblislabor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblissuper As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisplanner As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblLogged_In As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblms As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divywrr As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents tdhdr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents divhdr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents tdywr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents divywr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblweekstart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblweekend As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblworktype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltret As System.Web.UI.HtmlControls.HtmlInputHidden
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            who = Request.QueryString("who").ToString
            lblwho.Value = who
            Try
                uid = Request.QueryString("uid").ToString
            Catch ex As Exception
                uid = Request.QueryString("userid").ToString
            End Try
            lbluid.Value = uid
            Try
                username = Request.QueryString("usrname").ToString
            Catch ex As Exception
                username = Request.QueryString("username").ToString
            End Try
            lblusername.Value = username
            islabor = Request.QueryString("islabor").ToString
            lblislabor.Value = islabor
            issuper = Request.QueryString("issuper").ToString
            lblissuper.Value = issuper
            isplanner = Request.QueryString("isplanner").ToString
            lblisplanner.Value = isplanner
            who = Request.QueryString("who").ToString
            lblwho.Value = who
            Logged_In = Request.QueryString("Logged_In").ToString
            lblLogged_In.Value = Logged_In
            ro = Request.QueryString("ro").ToString
            lblro.Value = ro
            ms = Request.QueryString("ms").ToString
            lblms.Value = ms
            appstr = Request.QueryString("appstr").ToString
            lblappstr.Value = appstr
            Try
                wo = Request.QueryString("wo").ToString
            Catch ex As Exception
                wo = ""
            End Try

            lblwonum.Value = wo
            typ = "PM"
            lbltyp.Value = typ
            mm.Open()
            GetCurrentWeek()
            BuildMyWR(PageNumber)
            mm.Dispose()
        Else
            If Request.Form("lblsubmit") = "newweek" Then
                lblsubmit.Value = ""
                mm.Open()
                BuildMyWR(PageNumber)
                mm.Dispose()
            End If
            If Request.Form("lblret") = "next" Then
                mm.Open()
                GetNext()
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                mm.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                BuildMyWR(PageNumber)
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                mm.Open()
                GetPrev()
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                mm.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                BuildMyWR(PageNumber)
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "filt" Then
                lblret.Value = ""
                mm.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                who = lblwho.Value
                BuildMyWR(PageNumber)
                mm.Dispose()
            End If
            End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            BuildMyWR(PageNumber)
        Catch ex As Exception
            mm.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr178", "eqlookup.aspx.vb")

            mm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            BuildMyWR(PageNumber)
        Catch ex As Exception
            mm.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr179", "eqlookup.aspx.vb")

            mm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetCurrentWeek()
        sql = "declare @dwk int, @fwk datetime, @ewk datetime " _
        + "set @fwk = getDate() " _
        + "set @dwk = datepart(dw, @fwk) " _
        + "if @dwk <> 1 " _
        + "begin " _
        + "set @fwk = dateadd(dd, (@dwk*-1)+1, @fwk) " _
        + "end " _
        + "set @ewk = dateadd(dd, 6, @fwk) " _
        + "select Convert(char(10),@fwk,101) as ws, Convert(char(10),@ewk,101) as we"
        dr = mm.GetRdrData(sql)
        While dr.Read
            ws = dr.Item("ws").ToString
            we = dr.Item("we").ToString
        End While
        dr.Close()
        lblweekstart.Value = ws
        lblweekend.Value = we
        sid = lblsid.Value
        Dim fd, ld, efd, eld, wkid, wkid2, wkid3, wkid4 As String
        Dim fd2, ld2, fd3, ld3 As String
        sql = "select wkid from labweeks where Convert(char(10),fd,101) = '" & ws & "' and siteid = '" & sid & "'"
        dr = mm.GetRdrData(sql)
        While dr.Read
            wkid = dr.Item("wkid").ToString
        End While
        dr.Close()
        lblwkid.Value = wkid
    End Sub
    Private Function checkloc(ByVal lid As String, ByVal jl As String) As String
        Dim ret As String
        If jl = "0" Then
            sql = "usp_lookup_locs_eq '" & lid & "'"
        Else
            sql = "usp_lookup_locs '" & lid & "'"
        End If

        dr = mm.GetRdrData(sql)
        While dr.Read
            ret = dr.Item("retstring").ToString
        End While
        dr.Close()
        Return ret
    End Function
    Private Sub BuildMyWR(ByVal PageNumber As Integer)
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder
        Dim sbr As New StringBuilder
        Dim sb1r As New StringBuilder
        Dim field, srch As String
        Dim srchi As Integer
        sid = lblsid.Value
        Dim rt, rs, ra, sa, sup, lead, did, clid, lid, rettyp, level, lstring, jl, fpc As String
        Dim waid, wpaid, fdate, tdate, wtret, statret, eqid, fuid, coid, usid As String
        Dim filtret As String = lblfiltret.Value
        Dim woret As String
        Dim datret As String = "0"
        Dim wa As String = "1"
        If filtret <> "" Then
            Dim filtarr() As String = filtret.Split("~")
            waid = filtarr(0).ToString
            wpaid = filtarr(1).ToString
            fdate = filtarr(2).ToString
            tdate = filtarr(3).ToString
            wtret = filtarr(4).ToString
            statret = filtarr(5).ToString
            woret = filtarr(6).ToString

            rt = filtarr(9).ToString
            rs = filtarr(10).ToString
            ra = filtarr(11).ToString
            sa = filtarr(12).ToString
            wa = filtarr(13).ToString
            sup = filtarr(14).ToString
            lead = filtarr(15).ToString
            did = filtarr(16).ToString
            clid = filtarr(17).ToString
            lid = filtarr(18).ToString
            eqid = filtarr(19).ToString
            fuid = filtarr(20).ToString
            coid = filtarr(21).ToString
            rettyp = filtarr(22).ToString
            level = filtarr(23).ToString
            jl = filtarr(24).ToString
            fpc = filtarr(25).ToString
            If rt = "1" Or rs = "1" Or ra = "1" Then
                datret = "1"
            End If
            If eqid = "" Then
                If rettyp = "depts" Then
                    If Filter <> "" Then
                        Filter += " and w.deptid = ''" & did & "'' "
                        FilterCNT += " and w.deptid = '" & did & "' "
                    Else
                        Filter = "w.deptid = ''" & did & "'' "
                        FilterCNT = "w.deptid = '" & did & "' "
                    End If
                    If clid <> "" Then
                        If Filter <> "" Then
                            Filter += " and w.cellid = ''" & clid & "'' "
                            FilterCNT += " and w.cellid = '" & clid & "' "
                        Else
                            Filter = "w.cellid = ''" & clid & "'' "
                            FilterCNT = "w.cellid = '" & clid & "' "
                        End If
                    End If
                ElseIf rettyp = "locs" Then
                    If level <> 1 Then
                        lstring = checkloc(lid, jl)
                        If Filter <> "" Then
                            Filter += " and w.locid in (" & lstring & ") "
                            FilterCNT += " and w.locid in (" & lstring & ") "
                        Else
                            Filter = " w.locid in (" & lstring & ") "
                            FilterCNT = " w.locid in (" & lstring & ") "
                        End If
                    End If

                End If
            End If
            If fpc <> "" Then
                fpc = Replace(fpc, "'", Chr(180), , , vbTextCompare)
                If Filter <> "" Then
                    Filter += " and w.eqid in (select e.eqid from equipment e where e.fpc like ''%" & fpc & "%'') "
                    FilterCNT += " and w.eqid in (select e.eqid from equipment e where e.fpc like '%" & fpc & "%') "
                Else
                    Filter += " w.eqid in (select e.eqid from equipment e where e.fpc like ''%" & fpc & "%'') "
                    FilterCNT += " w.eqid in (select e.eqid from equipment e where e.fpc like '%" & fpc & "%') "
                End If
            End If
            'targ
            If fdate <> "" Then
                If rt = "1" Then
                    If Filter <> "" Then
                        Filter += "and targstartdate >= ''" & fdate & "'' "
                        FilterCNT += "and targstartdate >= '" & fdate & "' "
                    Else
                        Filter = "targstartdate >= ''" & fdate & "'' "
                        FilterCNT = "targstartdate >= '" & fdate & "' "
                    End If
                End If
            End If
            If tdate <> "" Then
                If rt = "1" And who <> "pm" Then
                    If Filter <> "" Then
                        Filter += "and targcompdate <= ''" & tdate & "'' "
                        FilterCNT += "and targcompdate <= '" & tdate & "' "
                    Else
                        Filter = "targcompdate <= ''" & tdate & "'' "
                        FilterCNT = "targcompdate <= '" & tdate & "' "
                    End If
                ElseIf rt = "1" And who = "pm" Then
                    If Filter <> "" Then
                        Filter += "and targstartdate <= ''" & tdate & "'' "
                        FilterCNT += "and targstartdate <= '" & tdate & "' "
                    Else
                        Filter = "targstartdate <= ''" & tdate & "'' "
                        FilterCNT = "targstartdate <= '" & tdate & "' "
                    End If
                End If
            End If

            'sched
            If fdate <> "" Then
                If rs = "1" Then
                    If Filter <> "" Then
                        Filter += "and schedstart >= ''" & fdate & "'' "
                        FilterCNT += "and schedstart >= '" & fdate & "' "
                    Else
                        Filter = "schedstart >= ''" & fdate & "'' "
                        FilterCNT = "schedstart >= '" & fdate & "' "
                    End If
                End If
            End If
            If tdate <> "" Then
                If rs = "1" And who <> "pm" Then
                    If Filter <> "" Then
                        Filter += "and schedfinish <= ''" & tdate & "'' "
                        FilterCNT += "and schedfinish <= '" & tdate & "' "
                    Else
                        Filter = "schedfinish <= ''" & tdate & "'' "
                        FilterCNT = "schedfinish <= '" & tdate & "' "
                    End If
                ElseIf rs = "1" And who = "pm" Then
                    If Filter <> "" Then
                        Filter += "and schedstart <= ''" & tdate & "'' "
                        FilterCNT += "and schedstart <= '" & tdate & "' "
                    Else
                        Filter = "schedstart <= ''" & tdate & "'' "
                        FilterCNT = "schedstart <= '" & tdate & "' "
                    End If
                End If
            End If

            'actual
            If fdate <> "" Then
                If ra = "1" And who <> "pm" Then
                    If Filter <> "" Then
                        Filter += "and actstart >= ''" & fdate & "'' "
                        FilterCNT += "and actstart >= '" & fdate & "' "
                    Else
                        Filter = "actstart >= ''" & fdate & "'' "
                        FilterCNT = "actstart >= '" & fdate & "' "
                    End If
                ElseIf ra = "1" And who = "pm" Then
                    If Filter <> "" Then
                        Filter += "and actfinish >= ''" & fdate & "'' "
                        FilterCNT += "and actfinish >= '" & fdate & "' "
                    Else
                        Filter = "actfinish >= ''" & fdate & "'' "
                        FilterCNT = "actfinish >= '" & fdate & "' "
                    End If
                End If
            End If
            If tdate <> "" Then
                If ra = "1" Then
                    If Filter <> "" Then
                        Filter += "and actfinish <= ''" & tdate & "'' "
                        FilterCNT += "and actfinish <= '" & tdate & "' "
                    Else
                        Filter = "actfinish <= ''" & tdate & "'' "
                        FilterCNT = "actfinish <= '" & tdate & "' "
                    End If
                End If
            End If

            'status
            If statret <> "" Then
                If Filter <> "" Then
                    Filter += " and w.status = ''" & statret & "''"
                    FilterCNT += " and w.status = '" & statret & "'"
                Else
                    Filter = " w.status = ''" & statret & "''"
                    FilterCNT = " w.status = '" & statret & "'"
                End If

            End If
            If woret <> "" Then
                If Filter <> "" Then
                    Filter += " and w.wonum = ''" & woret & "''"
                    FilterCNT += " and w.wonum = '" & woret & "'"
                Else
                    Filter = " w.wonum = ''" & woret & "''"
                    FilterCNT = " w.wonum = '" & woret & "'"
                End If

            End If

            If wpaid <> "" Then
                If Filter <> "" Then
                    Filter += " and w.wpaid = ''" & wpaid & "''"
                    FilterCNT += " and w.wpaid = '" & wpaid & "'"
                Else
                    Filter = " w.wpaid = ''" & wpaid & "''"
                    FilterCNT = " w.wpaid = '" & wpaid & "'"
                End If

            End If

            Dim wtrets1, wtrets2 As String
            If wtret <> "" Then
                Dim wtretarr() As String = wtret.Split(",")
                Dim wi As Integer
                For wi = 0 To wtretarr.Length - 1
                    If wtrets1 = "" Then
                        wtrets1 = "''" & wtretarr(wi) & "''"
                        wtrets2 = "'" & wtretarr(wi) & "'"
                    Else
                        wtrets1 += ",''" & wtretarr(wi) & "''"
                        wtrets2 += ",'" & wtretarr(wi) & "'"
                    End If
                Next
                If Filter <> "" Then
                    'Filter += " and w.worktype = ''" & wtret & "''"
                    'FilterCNT += " and w.worktype = '" & wtret & "'"
                    Filter += " and w.worktype in (" & wtrets1 & ")"
                    FilterCNT += " and w.worktype in (" & wtrets2 & ")"
                Else
                    'Filter = " w.worktype = ''" & wtret & "''"
                    'FilterCNT = " w.worktype = '" & wtret & "'"
                    Filter = " w.worktype in (" & wtrets1 & ")"
                    FilterCNT = " w.worktype in (" & wtrets2 & ")"
                End If

            End If

            If sup <> "" Then
                If Filter <> "" Then
                    Filter += "and w.superid = ''" & sup & "'' "
                    FilterCNT += "and w.superid = '" & sup & "' "
                Else
                    Filter += " w.superid = ''" & sup & "'' "
                    FilterCNT += " w.superid = '" & sup & "' "
                End If
            End If

            If lead <> "" Then
                If Filter <> "" Then
                    Filter += "and w.leadcraftid = ''" & lead & "'' "
                    FilterCNT += "and w.leadcraftid = '" & lead & "' "
                Else
                    Filter += " w.leadcraftid = ''" & lead & "'' "
                    FilterCNT += " w.leadcraftid = '" & lead & "' "
                End If
            End If

            If sa = "0" Then
                If islabor = "1" Then
                    sql = "select userid from pmsysusers where uid = '" & usid & "'"
                    usid = mm.strScalar(sql)
                    'issched = "1" And 
                    If wtret <> "TPM" Then
                        If Filter <> "" Then
                            Filter += " and a.userid = ''" & usid & "''"
                            FilterCNT += " and a.userid = '" & usid & "'"
                        Else
                            Filter = " a.userid = ''" & usid & "''"
                            FilterCNT = " a.userid = '" & usid & "'"
                        End If

                    End If
                ElseIf isplanner = "1" Then

                ElseIf issuper = "1" Then

                End If
            End If

            If waid <> "" Then
                If wa = "1" Then
                    If Filter <> "" Then
                        Filter += " and w.waid = ''" & waid & "''"
                        FilterCNT += " and w.waid = '" & waid & "'"
                    Else
                        Filter = " w.waid = ''" & waid & "''"
                        FilterCNT = " w.waid = '" & waid & "'"
                    End If
                Else
                    If Filter <> "" Then
                        Filter += " and a.userid in (select userid from pmsysusers where waid = ''" & waid & "'')"
                        FilterCNT += " and a.userid in (select userid from pmsysusers where waid = '" & waid & "')"
                    Else
                        Filter = " a.userid in (select userid from pmsysusers where waid = ''" & waid & "'')"
                        FilterCNT = " a.userid in (select userid from pmsysusers where waid = '" & waid & "')"
                    End If
                End If


            End If
        Else
            typ = lbltyp.Value
            Filter = " worktype = ''" & typ & "''"
            FilterCNT = " worktype = '" & typ & "'"

        End If

        ws = lblweekstart.Value
        If ws = "" Then
            ws = "10/01/2010"
        End If
        we = lblweekend.Value
        If we = "" Then
            we = "10/28/2010"
        End If

        If Filter <> "" Then
            Filter += " and (targstartdate >= ''" & ws & "'' and targstartdate <= ''" & we & "'')"
            FilterCNT += " and (targstartdate >= '" & ws & "' and targstartdate <= '" & we & "')"

        Else
            Filter += " (targstartdate >= ''" & ws & "'' and targstartdate <= ''" & we & "'')"
            FilterCNT += " (targstartdate >= '" & ws & "' and targstartdate <= '" & we & "')"
        End If

        Dim intPgNav, intPgCnt As Integer
        'sql = "SELECT Count(*) FROM workorder where " & FilterCNT
        sql = "SELECT Count(distinct w.wonum) FROM workorder w left join woassign a on a.wonum = w.wonum where " & FilterCNT

        intPgCnt = mm.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr588", "wolist.aspx.vb")

            mm.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
        End If
        PageSize = "50"
        intPgNav = mm.PageCountRev(intPgCnt, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If

        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        Sort = "w.wonum asc"

        Dim bg As String
        bg = "thdrsinggtrans1 ptransrow"
        Dim cr, crl As String
        cr = "plainlabel"
        Dim bxbg, bxbgy As String
        bxbg = "thdrplain"
        bxbgy = "thdrplainbgy"

        sql = "declare @fwk datetime, @ewk datetime " _
       + "declare @fwk2 datetime, @ewk2 datetime " _
       + "declare @fwk3 datetime, @ewk3 datetime " _
       + "declare @fwk4 datetime, @ewk4 datetime " _
       + "declare @fwk5 datetime  " _
       + "declare @fwk6 datetime  " _
       + "declare @fwk7 datetime " _
       + "set @fwk = '" & ws & "' " _
       + "set @ewk = dateadd(dd, 6, @fwk) " _
       + "set @fwk2 = dateadd(dd, 1, @fwk) " _
       + "set @ewk2 = dateadd(dd, 13, @fwk) " _
       + "set @fwk3 = dateadd(dd, 1, @fwk2) " _
       + "set @ewk3 = dateadd(dd, 13, @fwk2) " _
       + "set @fwk4 = dateadd(dd, 1, @fwk3) " _
       + "set @ewk4 = dateadd(dd, 13, @fwk3) " _
       + "set @fwk5 = dateadd(dd, 1, @fwk4) " _
       + "set @fwk6 = dateadd(dd, 1, @fwk5) " _
       + "set @fwk7 = dateadd(dd, 1, @fwk6) " _
       + "select Convert(char(10),@fwk,101) as ws1, Convert(char(10),@ewk,101) as we1, " _
       + "Convert(char(10),@fwk2,101) as ws2, Convert(char(10),@ewk2,101) as we2, " _
       + "Convert(char(10),@fwk3,101) as ws3, Convert(char(10),@ewk3,101) as we3, " _
       + "Convert(char(10),@fwk4,101) as ws4, Convert(char(10),@ewk4,101) as we4, " _
       + "Convert(char(10),@fwk5,101) as ws5, Convert(char(10),@fwk6,101) as ws6, Convert(char(10),@fwk7,101) as ws7"
        Dim ws1, ws2, ws3, ws4, ws5, ws6, ws7, we1, we2, we3, we4, we5, we6, we7 As String
        dr = mm.GetRdrData(sql)
        While dr.Read
            ws1 = dr.Item("ws1").ToString
            we1 = dr.Item("we1").ToString
            ws2 = dr.Item("ws2").ToString
            we2 = dr.Item("we2").ToString
            ws3 = dr.Item("ws3").ToString
            we3 = dr.Item("we3").ToString
            ws4 = dr.Item("ws4").ToString
            we4 = dr.Item("we4").ToString
            ws5 = dr.Item("ws5").ToString
            ws6 = dr.Item("ws6").ToString
            ws7 = dr.Item("ws7").ToString
        End While
        dr.Close()
        '
        sb1.Append("<table cellspacing=""2"" cellpadding=""2"" width=""712"" border=""0"" id=""scrollmenul"">" & vbCrLf)
        Dim wkid As String = lblwkid.Value
        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td colspan=""9"" rowspan=""3"" valign=""top"">")
        sb1.Append("<table cellspacing=""2"" border=""0"">")
        sb1.Append("<tr>")
        sb1.Append("<td class=""bigblue"" width=""220"" rowspan=""2"" valign=""top"">Weekly Planner</td>")
        sb1.Append("<td valign=""top"" class=""label"" width=""80"">Start Week</td>")
        sb1.Append("<td valign=""top"" class=""plainlabel"" id=""tdswk"" width=""120""><a href=""#"" onclick=""lookweek('" & wkid & "','" & ws1 & "-" & we1 & "','" & sid & "');"">" & ws1 & "-" & we1 & "</a></td>")
        sb1.Append("<td valign=""top"" width=""30""><img src=""../images/appbuttons/minibuttons/magnifier.gif"" onclick=""getweeks();""></td>")
        sb1.Append("<td valign=""top"" class=""A1"" width=""180"" align=""right""><a href=""#"" class=""A1"" onclick=""goback();"">Return</a></td>")
        sb1.Append("</tr>")
        sb1.Append("<tr>")
        sb1.Append("<td class=""label"">Filter</td>")
        'sb1.Append("<td class=""plainlabel"" id=""tdwt"">" & typ & "</td>")
        sb1.Append("<td class=""plainlabel"" id=""tdwt""><img src=""../images/appbuttons/minibuttons/magnifier.gif"" onclick=""getfilt();""></td>")
        sb1.Append("<td><img src=""../images/appbuttons/minibuttons/magnifier.gif"" onclick=""getwt();"" class=""details""></td>")
        sb1.Append("</tr>")
        sb1.Append("</table>")
        sb1.Append("</td>")
        sb1.Append("<td class=""thdrsinggtrans plainlabel"">&nbsp;</td>")
        sb1.Append("</tr>" & vbCrLf)

        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td></td>")
        sb1.Append("<td></td>")
        sb1.Append("<td></td>")
        sb1.Append("<td class=""thdrplaintrans plainlabel"">&nbsp;</td>")
        sb1.Append("</tr>" & vbCrLf)

        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td></td>")
        sb1.Append("<td></td>")
        sb1.Append("<td></td>")
        sb1.Append("<td class=""thdrplaintrans plainlabel"">&nbsp;</td>")
        sb1.Append("</tr>" & vbCrLf)

        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td colspan=""6""></td>")
        sb1.Append("<td colspan=""3"" class=""thdrplain plainlabel"">Available on each shift</td>")
        sb1.Append("<td class=""thdrplaintrans plainlabel"">&nbsp;</td>")
        sb1.Append("</tr>" & vbCrLf)

        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td colspan=""6""></td>")
        sb1.Append("<td colspan=""3"" class=""thdrplain plainlabel"">Scheduled on each shift</td>")
        sb1.Append("<td class=""thdrplaintrans plainlabel"">&nbsp;</td>")
        sb1.Append("</tr>" & vbCrLf)

        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""50"" height=""20"">WO#</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""70"">Status</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""280"">Work Description</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""50"">Type</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""80"">Target</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""40"">Hours</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""80"">Start</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""40"">Hours</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""22""><img src=""../images/appbuttons/minibuttons/warningtrans.gif""></td>" & vbCrLf)
        sb1.Append("<td class=""thdrsinggtrans plainlabel"" width=""20"">&nbsp;</td>")
        sb1.Append("</tr>" & vbCrLf)

        '***
        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""50"" height=""20"">WO#</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""70"">Status</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""280"">Work Description</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""50"">Type</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""80"">Target</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""40"">Hours</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""80"">Start</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""40"">Hours</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""22""><img src=""../images/appbuttons/minibuttons/warningtrans.gif""></td>" & vbCrLf)
        sb1.Append("<td class=""thdrsinggtrans plainlabel"" width=""20"">&nbsp;</td>")
        sb1.Append("</tr>" & vbCrLf)

        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""50"" >00000000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""70"">0000000000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""280"">0000000000000000000000000000000000000000000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""50"" >00000000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""40"">00.00</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""40"">00.00</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""22"">00</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""20"">&nbsp;</td>")
        sb1.Append("</tr>" & vbCrLf)
        '***




        sb1r.Append("<table cellspacing=""2"" cellpadding=""2"" width=""1732"" border=""0"" id=""scrollmenur"">" & vbCrLf)
        sb1r.Append("<tr>" & vbCrLf)
        sb1r.Append("<td colspan=""6"" class=""thdrsingg plainlabel"">Sunday</td>")
        sb1r.Append("<td colspan=""6"" class=""thdrsingg plainlabel"">Monday</td>")
        sb1r.Append("<td colspan=""6"" class=""thdrsingg plainlabel"">Tuesday</td>")
        sb1r.Append("<td colspan=""6"" class=""thdrsingg plainlabel"">Wednesday</td>")
        sb1r.Append("<td colspan=""6"" class=""thdrsingg plainlabel"">Thursday</td>")
        sb1r.Append("<td colspan=""6"" class=""thdrsingg plainlabel"">Friday</td>")
        sb1r.Append("<td colspan=""6"" class=""thdrsingg plainlabel"">Saturday</td>")
        sb1r.Append("</tr>" & vbCrLf)

        sb1r.Append("<tr>" & vbCrLf)
        sb1r.Append("<td colspan=""6"" class=""thdrplainbgy plainlabelblue"">" & ws1 & "</td>")
        sb1r.Append("<td colspan=""6"" class=""thdrplain plainlabelblue"">" & ws2 & "</td>")
        sb1r.Append("<td colspan=""6"" class=""thdrplainbgy plainlabelblue"">" & ws3 & "</td>")
        sb1r.Append("<td colspan=""6"" class=""thdrplain plainlabelblue"">" & ws4 & "</td>")
        sb1r.Append("<td colspan=""6"" class=""thdrplainbgy plainlabelblue"">" & ws5 & "</td>")
        sb1r.Append("<td colspan=""6"" class=""thdrplain plainlabelblue"">" & ws6 & "</td>")
        sb1r.Append("<td colspan=""6"" class=""thdrplainbgy plainlabelblue"">" & ws7 & "</td>")
        sb1r.Append("</tr>" & vbCrLf)

     

        sb1r.Append("<tr>" & vbCrLf)
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">3rd</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">1st</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">2nd</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">3rd</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">1st</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">2nd</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">3rd</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">1st</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">2nd</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">3rd</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">1st</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">2nd</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">3rd</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">1st</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">2nd</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">3rd</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">1st</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">2nd</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">3rd</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">1st</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">2nd</td>")

        sb1r.Append("</tr>" & vbCrLf)

        sql = "usp_getsevdayplnr '" & ws & "'"
        Dim w11a, w11s, w12a, w12s, w13a, w13s As String
        Dim w21a, w21s, w22a, w22s, w23a, w23s As String
        Dim w31a, w31s, w32a, w32s, w33a, w33s As String
        Dim w41a, w41s, w42a, w42s, w43a, w43s As String
        Dim w51a, w51s, w52a, w52s, w53a, w53s As String
        Dim w61a, w61s, w62a, w62s, w63a, w63s As String
        Dim w71a, w71s, w72a, w72s, w73a, w73s As String
        dr = mm.GetRdrData(sql)
        While dr.Read
            w11a = dr.Item("w11a").ToString
            w11s = dr.Item("w11s").ToString
            w12a = dr.Item("w12a").ToString
            w12s = dr.Item("w12s").ToString
            w13a = dr.Item("w13a").ToString
            w13s = dr.Item("w13s").ToString

            w21a = dr.Item("w21a").ToString
            w21s = dr.Item("w21s").ToString
            w22a = dr.Item("w22a").ToString
            w22s = dr.Item("w22s").ToString
            w23a = dr.Item("w23a").ToString
            w23s = dr.Item("w23s").ToString

            w31a = dr.Item("w31a").ToString
            w31s = dr.Item("w31s").ToString
            w32a = dr.Item("w32a").ToString
            w32s = dr.Item("w32s").ToString
            w33a = dr.Item("w33a").ToString
            w33s = dr.Item("w33s").ToString

            w41a = dr.Item("w41a").ToString
            w41s = dr.Item("w41s").ToString
            w42a = dr.Item("w42a").ToString
            w42s = dr.Item("w42s").ToString
            w43a = dr.Item("w43a").ToString
            w43s = dr.Item("w43s").ToString

            w51a = dr.Item("w51a").ToString
            w51s = dr.Item("w51s").ToString
            w52a = dr.Item("w52a").ToString
            w52s = dr.Item("w52s").ToString
            w53a = dr.Item("w53a").ToString
            w53s = dr.Item("w53s").ToString

            w61a = dr.Item("w61a").ToString
            w61s = dr.Item("w61s").ToString
            w62a = dr.Item("w62a").ToString
            w62s = dr.Item("w62s").ToString
            w63a = dr.Item("w63a").ToString
            w63s = dr.Item("w63s").ToString

            w71a = dr.Item("w71a").ToString
            w71s = dr.Item("w71s").ToString
            w72a = dr.Item("w72a").ToString
            w72s = dr.Item("w72s").ToString
            w73a = dr.Item("w73a").ToString
            w73s = dr.Item("w73s").ToString
        End While
        dr.Close()


        sb1r.Append("<tr>" & vbCrLf)
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w13a & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w11a & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w12a & "</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w23a & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w21a & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w22a & "</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w33a & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w31a & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w32a & "</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w43a & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w41a & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w42a & "</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w53a & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w51a & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w52a & "</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w63a & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w61a & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w62a & "</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w73a & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w71a & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w72a & "</td>")


        sb1r.Append("</tr>" & vbCrLf)

        sb1r.Append("<tr>" & vbCrLf)
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w13s & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w11s & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w12s & "</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w23s & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w21s & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w22s & "</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w33s & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w31s & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w32s & "</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w43s & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w41s & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w42s & "</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w53s & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w51s & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w52s & "</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w63s & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w61s & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w62s & "</td>")

        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w73s & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w71s & "</td>")
        sb1r.Append("<td colspan=""2"" class=""thdrplainbgy plainlabel"">" & w72s & "</td>")
        sb1r.Append("</tr>" & vbCrLf)



        sb1r.Append("<tr>" & vbCrLf)
        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"" height=""20"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"" height=""20"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"" height=""20"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"" height=""20"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"" height=""20"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"" height=""20"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"" height=""20"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1r.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1r.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        '***filler cell
        sb1r.Append("<td width=""482"" class=""thdrsinggtrans plainlabel"">&nbsp;&nbsp;&nbsp;&nbsp;</td>")
        sb1r.Append("</tr>" & vbCrLf)


        'sb.Append("<table cellspacing=""2"" width=""1450"">" & vbCrLf)
        sb.Append("<table cellspacing=""2"" cellpadding=""2"" width=""712"" border=""0"" id=""scrollmenulb"">" & vbCrLf)
        sbr.Append("<table cellspacing=""2"" cellpadding=""2"" width=""1732"" border=""0"" id=""scrollmenurb"" >" & vbCrLf)

        Dim wt, wo, stat, isdown, desc, ss, sc, eq, ts, hrs, ttime As String
        Dim w11, w11h, w12, w12h, w13, w13h, w14, w14h As String
        Dim w21, w21h, w22, w22h, w23, w23h, w24, w24h As String
        Dim w31, w31h, w32, w32h, w33, w33h, w34, w34h As String
        Dim w41, w41h, w42, w42h, w43, w43h, w44, w44h As String
        Dim w51, w51h, w52, w52h, w53, w53h, w54, w54h As String
        Dim w61, w61h, w62, w62h, w63, w63h, w64, w64h As String
        Dim w71, w71h, w72, w72h, w73, w73h, w74, w74h As String
        Dim rowflag As Integer = 0
        Dim sbcnt As Integer = 0
        If typ = "PM" Then
            sql = "usp_getwolist_pmwk '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "','" & ws & "','" & we & "'"
        Else
            sql = "usp_getwolist_wowk '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "','" & ws & "','" & we & "'"
        End If
        Dim wopriority As String
        dr = mm.GetRdrData(sql)
        While dr.Read
            sbcnt += 1
            If rowflag = 0 Then
                bxbg = "thdrplain"
                bxbgy = "thdrplainbgy"
                bg = "thdrsinggtrans1 ptransrow"
                rowflag = 1
            Else
                bxbg = "thdrplainbg"
                bxbgy = "thdrplainbg"
                bg = "thdrsinggtrans1 ptransrowblue"
                rowflag = "0"
            End If
            isdown = "0" 'dr.Item("isdown").ToString

            If isdown = "1" Then
                cr = "plainlabelred"
                crl = "A1R"
            Else
                cr = "plainlabel"
                crl = "A1U"
            End If
            wt = dr.Item("worktype").ToString
            wo = dr.Item("wonum").ToString
            eq = dr.Item("eqnum").ToString
            stat = dr.Item("status").ToString
            desc = dr.Item("description").ToString
            ss = dr.Item("schedstart").ToString
            ts = dr.Item("targstartdate").ToString
            sc = dr.Item("schedfinish").ToString
            hrs = dr.Item("hrs").ToString
            ttime = dr.Item("ttime").ToString

            Dim hrsd As Decimal
            Dim hrsi As Integer
            If Len(hrs) > 4 Then
                Try
                    hrsd = System.Convert.ToDouble(hrsd)
                    hrsd = hrsd.Round(hrsd, 0)
                    hrsi = System.Convert.ToInt32(hrsd)
                    hrs = hrsi
                Catch ex As Exception
                    hrs = "#"
                End Try
            End If

            Dim ttimed As Decimal
            Dim ttimei As Integer

            If Len(ttime) > 4 Then
                Try
                    ttimed = System.Convert.ToDouble(ttimed)
                    ttimed = ttimed.Round(ttimed, 0)
                    ttimei = System.Convert.ToInt32(ttimed)
                    ttime = ttimei
                Catch ex As Exception
                    ttime = "#"
                End Try
            End If

            stat = RTrim(stat)
            If Len(stat) > 10 Then
                stat = stat.Substring(0, 9)
            End If
            'workarea = RTrim(workarea)
            'If Len(workarea) > 10 Then
            'workarea = workarea.Substring(0, 9)
            'End If
            wt = RTrim(wt)
            If Len(wt) > 8 Then
                wt = wt.Substring(0, 7)
            End If
            desc = RTrim(desc)
            If Len(desc) > 45 Then
                desc = desc.Substring(0, 44)
            End If
            ss = RTrim(ss)
            ts = RTrim(ss)
            ts = RTrim(ss)


            w11 = dr.Item("w11").ToString
            w11h = dr.Item("w11h").ToString
            w12 = dr.Item("w12").ToString
            w12h = dr.Item("w12h").ToString
            w13 = dr.Item("w13").ToString
            w13h = dr.Item("w13h").ToString

            w21 = dr.Item("w21").ToString
            w21h = dr.Item("w21h").ToString
            w22 = dr.Item("w22").ToString
            w22h = dr.Item("w22h").ToString
            w23 = dr.Item("w23").ToString
            w23h = dr.Item("w23h").ToString

            w31 = dr.Item("w31").ToString
            w31h = dr.Item("w31h").ToString
            w32 = dr.Item("w32").ToString
            w32h = dr.Item("w32h").ToString
            w33 = dr.Item("w33").ToString
            w33h = dr.Item("w33h").ToString

            w41 = dr.Item("w41").ToString
            w41h = dr.Item("w41h").ToString
            w42 = dr.Item("w42").ToString
            w42h = dr.Item("w42h").ToString
            w43 = dr.Item("w43").ToString
            w43h = dr.Item("w43h").ToString

            w51 = dr.Item("w51").ToString
            w51h = dr.Item("w51h").ToString
            w52 = dr.Item("w52").ToString
            w52h = dr.Item("w52h").ToString
            w53 = dr.Item("w53").ToString
            w53h = dr.Item("w53h").ToString

            w61 = dr.Item("w61").ToString
            w61h = dr.Item("w61h").ToString
            w62 = dr.Item("w62").ToString
            w62h = dr.Item("w62h").ToString
            w63 = dr.Item("w63").ToString
            w63h = dr.Item("w63h").ToString

            w71 = dr.Item("w71").ToString
            w71h = dr.Item("w71h").ToString
            w72 = dr.Item("w72").ToString
            w72h = dr.Item("w72h").ToString
            w73 = dr.Item("w73").ToString
            w73h = dr.Item("w73h").ToString

            '**1
            Dim w11i As Integer
            Dim w11hd As Decimal
            Dim w11hi As Integer
            If Len(w11) > 2 Then
                w11 = "#"
            End If
            If Len(w11h) > 4 Then
                Try
                    w11hd = System.Convert.ToDouble(w11h)
                    w11hd = hrsd.Round(w11hd, 0)
                    w11hi = System.Convert.ToInt32(w11hd)
                    w11h = w11hi
                Catch ex As Exception
                    w11h = "#"
                End Try
            End If
            Dim w12i As Integer
            Dim w12hd As Decimal
            Dim w12hi As Integer
            If Len(w12) > 2 Then
                w12 = "#"
            End If
            If Len(w12h) > 4 Then
                Try
                    w12hd = System.Convert.ToDouble(w12h)
                    w12hd = hrsd.Round(w12hd, 0)
                    w12hi = System.Convert.ToInt32(w12hd)
                    w12h = w12hi
                Catch ex As Exception
                    w12h = "#"
                End Try
            End If
            Dim w13i As Integer
            Dim w13hd As Decimal
            Dim w13hi As Integer
            If Len(w13) > 2 Then
                w13 = "#"
            End If
            If Len(w13h) > 4 Then
                Try
                    w13hd = System.Convert.ToDouble(w13h)
                    w13hd = hrsd.Round(w13hd, 0)
                    w13hi = System.Convert.ToInt32(w13hd)
                    w13h = w13hi
                Catch ex As Exception
                    w13h = "#"
                End Try
            End If

            '**2
            Dim w21i As Integer
            Dim w21hd As Decimal
            Dim w21hi As Integer
            If Len(w21) > 2 Then
                w21 = "#"
            End If
            If Len(w21h) > 4 Then
                Try
                    w21hd = System.Convert.ToDouble(w21h)
                    w21hd = hrsd.Round(w21hd, 0)
                    w21hi = System.Convert.ToInt32(w21hd)
                    w21h = w21hi
                Catch ex As Exception
                    w21h = "#"
                End Try
            End If
            Dim w22i As Integer
            Dim w22hd As Decimal
            Dim w22hi As Integer
            If Len(w22) > 2 Then
                w22 = "#"
            End If
            If Len(w22h) > 4 Then
                Try
                    w22hd = System.Convert.ToDouble(w22h)
                    w22hd = hrsd.Round(w22hd, 0)
                    w22hi = System.Convert.ToInt32(w22hd)
                    w22h = w22hi
                Catch ex As Exception
                    w22h = "#"
                End Try
            End If
            Dim w23i As Integer
            Dim w23hd As Decimal
            Dim w23hi As Integer
            If Len(w23) > 2 Then
                w23 = "#"
            End If
            If Len(w23h) > 4 Then
                Try
                    w23hd = System.Convert.ToDouble(w23h)
                    w23hd = hrsd.Round(w23hd, 0)
                    w23hi = System.Convert.ToInt32(w23hd)
                    w23h = w23hi
                Catch ex As Exception
                    w23h = "#"
                End Try
            End If

            '**3
            Dim w31i As Integer
            Dim w31hd As Decimal
            Dim w31hi As Integer
            If Len(w31) > 2 Then
                w31 = "#"
            End If
            If Len(w31h) > 4 Then
                Try
                    w31hd = System.Convert.ToDouble(w31h)
                    w31hd = hrsd.Round(w31hd, 0)
                    w31hi = System.Convert.ToInt32(w31hd)
                    w31h = w31hi
                Catch ex As Exception
                    w31h = "#"
                End Try
            End If
            Dim w32i As Integer
            Dim w32hd As Decimal
            Dim w32hi As Integer
            If Len(w32) > 2 Then
                w32 = "#"
            End If
            If Len(w32h) > 4 Then
                Try
                    w32hd = System.Convert.ToDouble(w32h)
                    w32hd = hrsd.Round(w32hd, 0)
                    w32hi = System.Convert.ToInt32(w32hd)
                    w32h = w32hi
                Catch ex As Exception
                    w32h = "#"
                End Try
            End If
            Dim w33i As Integer
            Dim w33hd As Decimal
            Dim w33hi As Integer
            If Len(w33) > 2 Then
                w33 = "#"
            End If
            If Len(w33h) > 4 Then
                Try
                    w33hd = System.Convert.ToDouble(w33h)
                    w33hd = hrsd.Round(w33hd, 0)
                    w33hi = System.Convert.ToInt32(w33hd)
                    w33h = w33hi
                Catch ex As Exception
                    w33h = "#"
                End Try
            End If

            '**4
            Dim w41i As Integer
            Dim w41hd As Decimal
            Dim w41hi As Integer
            If Len(w41) > 2 Then
                w41 = "#"
            End If
            If Len(w41h) > 4 Then
                Try
                    w41hd = System.Convert.ToDouble(w41h)
                    w41hd = hrsd.Round(w41hd, 0)
                    w41hi = System.Convert.ToInt32(w41hd)
                    w41h = w41hi
                Catch ex As Exception
                    w41h = "#"
                End Try
            End If
            Dim w42i As Integer
            Dim w42hd As Decimal
            Dim w42hi As Integer
            If Len(w42) > 2 Then
                w42 = "#"
            End If
            If Len(w42h) > 4 Then
                Try
                    w42hd = System.Convert.ToDouble(w42h)
                    w42hd = hrsd.Round(w42hd, 0)
                    w42hi = System.Convert.ToInt32(w42hd)
                    w42h = w42hi
                Catch ex As Exception
                    w42h = "#"
                End Try
            End If
            Dim w43i As Integer
            Dim w43hd As Decimal
            Dim w43hi As Integer
            If Len(w43) > 2 Then
                w43 = "#"
            End If
            If Len(w43h) > 4 Then
                Try
                    w43hd = System.Convert.ToDouble(w43h)
                    w43hd = hrsd.Round(w43hd, 0)
                    w43hi = System.Convert.ToInt32(w43hd)
                    w43h = w43hi
                Catch ex As Exception
                    w43h = "#"
                End Try
            End If

            '**5
            Dim w51i As Integer
            Dim w51hd As Decimal
            Dim w51hi As Integer
            If Len(w51) > 2 Then
                w51 = "#"
            End If
            If Len(w51h) > 4 Then
                Try
                    w51hd = System.Convert.ToDouble(w51h)
                    w51hd = hrsd.Round(w51hd, 0)
                    w51hi = System.Convert.ToInt32(w51hd)
                    w51h = w51hi
                Catch ex As Exception
                    w51h = "#"
                End Try
            End If
            Dim w52i As Integer
            Dim w52hd As Decimal
            Dim w52hi As Integer
            If Len(w52) > 2 Then
                w52 = "#"
            End If
            If Len(w52h) > 4 Then
                Try
                    w52hd = System.Convert.ToDouble(w52h)
                    w52hd = hrsd.Round(w52hd, 0)
                    w52hi = System.Convert.ToInt32(w52hd)
                    w52h = w52hi
                Catch ex As Exception
                    w52h = "#"
                End Try
            End If
            Dim w53i As Integer
            Dim w53hd As Decimal
            Dim w53hi As Integer
            If Len(w53) > 2 Then
                w53 = "#"
            End If
            If Len(w53h) > 4 Then
                Try
                    w53hd = System.Convert.ToDouble(w53h)
                    w53hd = hrsd.Round(w53hd, 0)
                    w53hi = System.Convert.ToInt32(w53hd)
                    w53h = w53hi
                Catch ex As Exception
                    w53h = "#"
                End Try
            End If

            '**6
            Dim w61i As Integer
            Dim w61hd As Decimal
            Dim w61hi As Integer
            If Len(w61) > 2 Then
                w61 = "#"
            End If
            If Len(w61h) > 4 Then
                Try
                    w61hd = System.Convert.ToDouble(w61h)
                    w61hd = hrsd.Round(w61hd, 0)
                    w61hi = System.Convert.ToInt32(w61hd)
                    w61h = w61hi
                Catch ex As Exception
                    w61h = "#"
                End Try
            End If
            Dim w62i As Integer
            Dim w62hd As Decimal
            Dim w62hi As Integer
            If Len(w62) > 2 Then
                w62 = "#"
            End If
            If Len(w62h) > 4 Then
                Try
                    w62hd = System.Convert.ToDouble(w62h)
                    w62hd = hrsd.Round(w62hd, 0)
                    w62hi = System.Convert.ToInt32(w62hd)
                    w62h = w62hi
                Catch ex As Exception
                    w62h = "#"
                End Try
            End If
            Dim w63i As Integer
            Dim w63hd As Decimal
            Dim w63hi As Integer
            If Len(w63) > 2 Then
                w63 = "#"
            End If
            If Len(w63h) > 4 Then
                Try
                    w63hd = System.Convert.ToDouble(w63h)
                    w63hd = hrsd.Round(w63hd, 0)
                    w63hi = System.Convert.ToInt32(w63hd)
                    w63h = w63hi
                Catch ex As Exception
                    w63h = "#"
                End Try
            End If

            '**7
            Dim w71i As Integer
            Dim w71hd As Decimal
            Dim w71hi As Integer
            If Len(w71) > 2 Then
                w71 = "#"
            End If
            If Len(w71h) > 4 Then
                Try
                    w71hd = System.Convert.ToDouble(w71h)
                    w71hd = hrsd.Round(w71hd, 0)
                    w71hi = System.Convert.ToInt32(w71hd)
                    w71h = w71hi
                Catch ex As Exception
                    w71h = "#"
                End Try
            End If
            Dim w72i As Integer
            Dim w72hd As Decimal
            Dim w72hi As Integer
            If Len(w72) > 2 Then
                w72 = "#"
            End If
            If Len(w72h) > 4 Then
                Try
                    w72hd = System.Convert.ToDouble(w72h)
                    w72hd = hrsd.Round(w72hd, 0)
                    w72hi = System.Convert.ToInt32(w72hd)
                    w72h = w72hi
                Catch ex As Exception
                    w72h = "#"
                End Try
            End If
            Dim w73i As Integer
            Dim w73hd As Decimal
            Dim w73hi As Integer
            If Len(w73) > 2 Then
                w73 = "#"
            End If
            If Len(w73h) > 4 Then
                Try
                    w73hd = System.Convert.ToDouble(w73h)
                    w73hd = hrsd.Round(w73hd, 0)
                    w73hi = System.Convert.ToInt32(w73hd)
                    w73h = w73hi
                Catch ex As Exception
                    w73h = "#"
                End Try
            End If

            If w11 = "" Then
                w11 = "&nbsp;"
            End If
            If w11h = "" Then
                w11h = "&nbsp;"
            End If
            If w12 = "" Then
                w12 = "&nbsp;"
            End If
            If w12h = "" Then
                w12h = "&nbsp;"
            End If
            If w13 = "" Then
                w13 = "&nbsp;"
            End If
            If w13h = "" Then
                w13h = "&nbsp;"
            End If

            If w21 = "" Then
                w21 = "&nbsp;"
            End If
            If w21h = "" Then
                w21h = "&nbsp;"
            End If
            If w22 = "" Then
                w22 = "&nbsp;"
            End If
            If w22h = "" Then
                w22h = "&nbsp;"
            End If
            If w23 = "" Then
                w23 = "&nbsp;"
            End If
            If w23h = "" Then
                w23h = "&nbsp;"
            End If

            If w31 = "" Then
                w31 = "&nbsp;"
            End If
            If w31h = "" Then
                w31h = "&nbsp;"
            End If
            If w32 = "" Then
                w32 = "&nbsp;"
            End If
            If w32h = "" Then
                w32h = "&nbsp;"
            End If
            If w33 = "" Then
                w33 = "&nbsp;"
            End If
            If w33h = "" Then
                w33h = "&nbsp;"
            End If

            If w41 = "" Then
                w41 = "&nbsp;"
            End If
            If w41h = "" Then
                w41h = "&nbsp;"
            End If
            If w42 = "" Then
                w42 = "&nbsp;"
            End If
            If w42h = "" Then
                w42h = "&nbsp;"
            End If
            If w43 = "" Then
                w43 = "&nbsp;"
            End If
            If w43h = "" Then
                w43h = "&nbsp;"
            End If

            If w51 = "" Then
                w51 = "&nbsp;"
            End If
            If w51h = "" Then
                w51h = "&nbsp;"
            End If
            If w52 = "" Then
                w52 = "&nbsp;"
            End If
            If w52h = "" Then
                w52h = "&nbsp;"
            End If
            If w53 = "" Then
                w53 = "&nbsp;"
            End If
            If w53h = "" Then
                w53h = "&nbsp;"
            End If

            If w61 = "" Then
                w61 = "&nbsp;"
            End If
            If w61h = "" Then
                w61h = "&nbsp;"
            End If
            If w62 = "" Then
                w62 = "&nbsp;"
            End If
            If w62h = "" Then
                w62h = "&nbsp;"
            End If
            If w63 = "" Then
                w63 = "&nbsp;"
            End If
            If w63h = "" Then
                w63h = "&nbsp;"
            End If

            If w71 = "" Then
                w71 = "&nbsp;"
            End If
            If w71h = "" Then
                w71h = "&nbsp;"
            End If
            If w72 = "" Then
                w72 = "&nbsp;"
            End If
            If w72h = "" Then
                w72h = "&nbsp;"
            End If
            If w73 = "" Then
                w73 = "&nbsp;"
            End If
            If w73h = "" Then
                w73h = "&nbsp;"
            End If

            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""" & bg & """ width=""50""><a class=""" & crl & """ href=""#"" onclick=""gotoreq('" & wo & "')"">" & wo & "</a></td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""70"">" & stat & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""280"">" & desc & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""50"">" & wt & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">" & ts & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""40"">" & ttime & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">" & ss & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""40"">" & hrs & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""22"">" & wopriority & "</td>" & vbCrLf)
            sb.Append("<td class=""thdrplaintrans plainlabel"" width=""20"">&nbsp;</td>")
            sb.Append("</tr>" & vbCrLf)


            sbr.Append("<tr>" & vbCrLf)
            sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">" & w13 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">" & w13h & "</td>")
            sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">" & w11 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">" & w11h & "</td>")
            sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">" & w12 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">" & w12h & "</td>")

            sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">" & w23 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">" & w23h & "</td>")
            sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">" & w21 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">" & w21h & "</td>")
            sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">" & w22 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">" & w22h & "</td>")

            sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">" & w33 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">" & w33h & "</td>")
            sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">" & w31 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">" & w31h & "</td>")
            sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">" & w32 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">" & w32h & "</td>")

            sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">" & w43 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">" & w43h & "</td>")
            sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">" & w41 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">" & w41h & "</td>")
            sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">" & w42 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">" & w42h & "</td>")

            sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">" & w53 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">" & w53h & "</td>")
            sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">" & w51 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">" & w51h & "</td>")
            sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">" & w52 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">" & w52h & "</td>")

            sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">" & w63 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">" & w63h & "</td>")
            sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">" & w61 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">" & w61h & "</td>")
            sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">" & w62 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">" & w62h & "</td>")

            sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">" & w73 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">" & w73h & "</td>")
            sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">" & w71 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">" & w71h & "</td>")
            sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">" & w72 & "</td>")
            sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">" & w72h & "</td>")

            '***filler cell
            sbr.Append("<td width=""482"" class=""thdrsinggtrans plainlabel"">&nbsp;&nbsp;&nbsp;&nbsp;</td>")

            sbr.Append("</tr>" & vbCrLf)

            'sbr.Append("</tr>" & vbCrLf)
        End While
        dr.Close()

        bg = "thdrsinggtrans1 ptransrow"
        cr = "plainlabelwht"

        Dim sbi As Integer
        If sbcnt < 15 Then
            For sbi = 1 To 20
                If rowflag = 0 Then
                    bxbg = "thdrplain"
                    bxbgy = "thdrplainbgy"
                    bg = "thdrsinggtrans1 ptransrow"
                    rowflag = 1
                Else
                    bxbg = "thdrplainbg"
                    bxbgy = "thdrplainbg"
                    bg = "thdrsinggtrans1 ptransrowblue"
                    rowflag = "0"
                End If
                sb.Append("<tr>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""50"" >00000000</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""70"">0000000000</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""280"">0000000000000000000000000000000000000000000</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""50"" >00000000</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""40"">00.00</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""40"">00.00</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""22"">00</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""20"">&nbsp;</td>")
                sb.Append("</tr>" & vbCrLf)

                sbr.Append("<tr>" & vbCrLf)
                sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")

                sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")

                sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")

                sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")

                sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")

                sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""20"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbg & " plainlabel"">&nbsp;</td>")

                sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""20"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")
                sbr.Append("<td width=""40"" class=""" & bxbgy & " plainlabel"">&nbsp;</td>")

                sbr.Append("</tr>" & vbCrLf)
            Next
        End If
        bg = "thdrsinggtrans1 ptransrow"
        cr = "plainlabelwht"
        'pm list
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""50"" height=""20"">WO#</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""70"">Status</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""280"">Description</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""50"">Type</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""80"">Target</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""40"">Hours</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""80"">Start</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""40"">Hours</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""22""><img src=""../images/appbuttons/minibuttons/warningtrans.gif""></td>" & vbCrLf)
        sb.Append("<td class=""thdrsinggtrans plainlabel"" width=""20"">&nbsp;</td>")
        sb.Append("</tr>" & vbCrLf)

        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""50"" >00000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""70"">0000000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""300"">0000000000000000000000000000000000000000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""50"" >00000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""40"">00.00</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""40"">00.00</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""22"">00</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""20"">&nbsp;</td>")
        sb.Append("</tr>" & vbCrLf)

        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""50"" >00000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""70"">0000000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""300"">0000000000000000000000000000000000000000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""50"" >00000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""40"">00.00</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""40"">00.00</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""22"">00</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""20"">&nbsp;</td>")
        sb.Append("</tr>" & vbCrLf)


        sbr.Append("<tr>" & vbCrLf)
        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sbr.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sbr.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sbr.Append("</tr>" & vbCrLf)

        sbr.Append("<tr>" & vbCrLf)
        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")

        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")

        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")

        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")

        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")

        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")

        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sbr.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sbr.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")

        sbr.Append("</tr>" & vbCrLf)

        sb1r.Append("<tr>" & vbCrLf)
        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")

        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")

        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")

        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")

        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")

        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")

        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb1r.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb1r.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb1r.Append("</tr>" & vbCrLf)


        sb1r.Append("</table>")
        divhdrr.InnerHtml = sb1r.ToString
        sb1.Append("</table>")
        divhdr.InnerHtml = sb1.ToString
        sb.Append("</table>")
        divywr.InnerHtml = sb.ToString
        sbr.Append("</table>")
        divywrr.InnerHtml = sbr.ToString
    End Sub
End Class
