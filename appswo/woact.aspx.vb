

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class woact
    Inherits System.Web.UI.Page
    Protected WithEvents lang1337 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1336 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1335 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1334 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1333 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1332 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1331 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1330 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1329 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1328 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1327 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim comp As New Utilities
    Dim pmid As String
    Dim ds As DataSet
    Dim wonum, jpid, stat, ro, rostr, Login As String
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdwos As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblupsav As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdact As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdpar As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtoo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlub As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents wo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifmain As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbls As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblf As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txts As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtf As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtacthours As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdstart As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdactstart As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdactfinish As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdest As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td2 As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro <> "1" Then
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = comp.CheckROS(rostr, "wo")
                    lblro.Value = ro
                End If
            End If
            wonum = Request.QueryString("wo").ToString
            lblwo.Value = wonum
            stat = Request.QueryString("stat").ToString
            lblstat.Value = stat
            comp.Open()
            GetSkills()
            GetWoHead(wonum)
            comp.Dispose()
            'txtacthours.Attributes.Add("onkeyup", "upsav();")
            'ddskill.Attributes.Add("onchange", "upsav();")
            If stat = "COMP" Or stat = "CAN" Or stat = "WAPPR" Then
                'ddskill.Enabled = False
                'txtacthours.ReadOnly = True
            End If
        Else
            If Request.Form("lblsubmit") = "s" Then
                lblsubmit.Value = ""
                comp.Open()
                CompWo("s")
                comp.Dispose()
            ElseIf Request.Form("lblsubmit") = "f" Then
                lblsubmit.Value = ""
                comp.Open()
                CompWo("f")
                comp.Dispose()
            End If
        End If
    End Sub
    Private Sub GetSkills()
        'sql = "select skillid, skill " _
        '+ "from pmSkills"
        'dr = comp.GetRdrData(sql)
        'ddskill.DataSource = dr
        'ddskill.DataTextField = "skill"
        'ddskill.DataValueField = "skillid"
        'ddskill.DataBind()
        'dr.Close()
        'ddskill.Items.Insert(0, New ListItem("Select"))
        'ddskill.Items(0).Value = 0
    End Sub
    Private Sub CompWo(ByVal fld As String)
        wonum = lblwo.Value
        Dim dat As String
        Dim csel As Date
        Dim ssel As Date
        Dim sel As Date
        Dim odat As String = ""
        If fld = "s" Then
            dat = txts.Text
            dat = lbls.Value
            ssel = CDate(dat)
            odat = lblf.Value

            If odat <> "" Then
                sel = lblf.Value
                csel = CDate(sel)
            End If
            If ssel > csel Or odat = "" Then
                sql = "update workorder set actstart = '" & dat & "', actfinish = '" & dat & "' where wonum = '" & wonum & "'"
                txtf.Text = dat
            Else
                sql = "update workorder set actstart = '" & dat & "' where wonum = '" & wonum & "'"
            End If
            comp.Update(sql)
            txts.Text = dat
        ElseIf fld = "f" Then
            dat = txtf.Text
            dat = lbls.Value
            csel = CDate(dat)
            odat = lbls.Value
            If odat <> "" Then
                sel = lbls.Value
                ssel = CDate(sel)
            End If

            If csel < ssel Or odat = "" Then
                sql = "update workorder set actfinish = '" & dat & "', actstart = '" & dat & "' where wonum = '" & wonum & "'"
                txts.Text = dat
            Else
                sql = "update workorder set actfinish = '" & dat & "' where wonum = '" & wonum & "'"
            End If

            comp.Update(sql)
            txtf.Text = dat
        End If
    End Sub
    Private Sub CompWoOld()
        wonum = lblwo.Value
        Dim i As Integer
        Dim yn As String
        'Parts
        Dim pstr As String = lblpstr.Value
        Dim parr() As String = pstr.Split(",")
        Dim p, pu, pid As String
        For i = 0 To parr.Length - 1
            p = parr(i)
            Dim puarr() As String = p.Split("~")
            pu = puarr(1)
            If pu = "yes" Then
                yn = "Y"
            Else
                yn = "N"
            End If
            pid = puarr(0)

            sql = "update woparts set used = '" & yn & "' where wopartid = '" & pid & "'"
            comp.Update(sql)

        Next
        sql = "update workorder set actmatcost = (select sum(total) from woparts where wonum = '" & wonum & "' and used = 'Y') " _
            + "where wonum = '" & wonum & "'"
        comp.Update(sql)

        'Tools
        Dim tstr As String = lbltstr.Value
        Dim tarr() As String = tstr.Split(",")
        Dim t, tu, tid As String
        For i = 0 To tarr.Length - 1
            t = tarr(i)
            Dim tuarr() As String = t.Split("~")
            tu = tuarr(1)
            If tu = "yes" Then
                yn = "Y"
            Else
                yn = "N"
            End If
            tid = tuarr(0)

            sql = "update wotool set used = '" & yn & "' where wotoolid = '" & tid & "'"
            comp.Update(sql)

        Next
        sql = "update workorder set acttoolcost = (select sum(total) from wotools where wonum = '" & wonum & "' and used = 'Y') " _
            + "where wonum = '" & wonum & "'"
        comp.Update(sql)

        'Lubes
        Dim lstr As String = lbllstr.Value
        Dim larr() As String = lstr.Split(",")
        Dim l, lu, lid As String
        For i = 0 To larr.Length - 1
            l = larr(i)
            Dim luarr() As String = l.Split("~")
            lu = luarr(1)
            If lu = "yes" Then
                yn = "Y"
            Else
                yn = "N"
            End If
            lid = luarr(0)

            sql = "update wolubes set used = '" & yn & "' where wolubeid = '" & lid & "'"
            comp.Update(sql)

        Next
        sql = "update workorder set actlubecost = (select sum(total) from wolubes where wonum = '" & wonum & "' and used = 'Y') " _
            + "where wonum = '" & wonum & "'"
        comp.Update(sql)

        'Dim actstart, actcomp, acthours, skid, skill, lc As String
        'actstart = txts.Text
        'actcomp = txtf.Text
        'acthours = txtacthours.Text
        'skid = ddskill.SelectedValue
        'skill = ddskill.SelectedItem.ToString
        'lc = txtlead.Text
        'sql = "update workorder set actstart = '" & actstart & "', " _
        '+ "actfinish = '" & actcomp & "', actlabhrs = '" & acthours & "' where wonum = '" & wonum & "'"

        'sql = "usp_upwoact '" & wonum & "','" & actstart & "','" & actcomp & "','" & acthours & "'"
        'comp.Update(sql)

    End Sub
    Private Sub GetWO()
        wonum = lblwo.Value
        stat = lblstat.Value
        Dim sb As New System.Text.StringBuilder
        Dim fm, fmid, fmstr As String
        Dim start As Integer = 0
        Dim rcnt As Integer = 0
        Dim used As String
        Dim chkd As String
        sql = "select distinct wopartid, itemnum, description, used from woparts where wonum = '" & wonum & "'"
        dr = comp.GetRdrData(sql)
        Dim wpid, pnum, pdesc, pstr As String
        While dr.Read
            If start = 0 Then
                start = 1
                sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""700"">")
                sb.Append("<tr><td colspan=""3"" class=""thdrsingg label"">" & tmod.getlbl("cdlbl154", "woact.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td width=""120""></td><td width=""200""></td><td width=""380""></td></tr>")
            End If
            wpid = dr.Item("wopartid").ToString
            pnum = dr.Item("itemnum").ToString
            pdesc = dr.Item("description").ToString
            used = dr.Item("used").ToString
            If used = "Y" Then
                used = "yes"
                chkd = "Part Used"
            Else
                used = "no"
                chkd = "Part Not Used"
            End If
            sb.Append("<tr><td class=""plainlabel"">" & pnum & "</td>")
            sb.Append("<td class=""plainlabel"">" & pdesc & "</td>")
            If stat = "COMP" Or stat = "CAN" Then
                sb.Append("<td class=""plainlabelblue"">" & chkd & "</td></tr>")
            Else
                sb.Append("<td class=""plainlabel""><input type=""radio"" id=""" & wpid & "-yes"" name=""p-" & wpid & """ ")
                If used = "yes" Then
                    sb.Append("checked onclick=""checkwp('yes', this.name)"">Part Used&nbsp;")
                    sb.Append("<input type=""radio"" id=""" & wpid & "-no"" name=""p-" & wpid & """ onclick=""checkwp('no', this.name)"">Part Not Used</td></tr>")
                Else
                    sb.Append("onclick=""checkwp('yes', this.name)"">Part Used&nbsp;")
                    sb.Append("<input type=""radio"" id=""" & wpid & "-no"" name=""p-" & wpid & """ onclick=""checkwp('no', this.name)"" checked>Part Not Used</td></tr>")
                End If
                If pstr = "" Then
                    pstr = wpid & "~" & used
                Else
                    pstr += "," & wpid & "~" & used
                End If
            End If

        End While

        dr.Close()
        If start = 1 Then
            lblpstr.Value = pstr
            sb.Append("<tr><td colspan=""2""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
            sb.Append("</table>")
        Else
            sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""700"">")
            sb.Append("<tr><td colspan=""2"" class=""thdrsing plainlabel"">" & tmod.getlbl("cdlbl155", "woact.aspx.vb") & "</td></tr>")
            sb.Append("<tr><td colspan=""2"" class=""plainlabelred"">" & tmod.getlbl("cdlbl156", "woact.aspx.vb") & "</td></tr>")
            sb.Append("<tr><td colspan=""2""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
            sb.Append("</table>")
        End If
        start = 0

        sql = "select distinct wotoolid, toolnum, description, used from wotools where wonum = '" & wonum & "'"
        dr = comp.GetRdrData(sql)
        Dim wtid, tnum, tdesc, tstr As String
        While dr.Read
            If start = 0 Then
                start = 1
                sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""700"">")
                sb.Append("<tr><td colspan=""3"" class=""thdrsingg label"">" & tmod.getlbl("cdlbl157", "woact.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td width=""120""></td><td width=""200""></td><td width=""380""></td></tr>")
            End If
            wtid = dr.Item("wotoolid").ToString
            tnum = dr.Item("toolnum").ToString
            tdesc = dr.Item("description").ToString
            used = dr.Item("used").ToString
            If used = "Y" Then
                used = "yes"
                chkd = "Tool Used"
            Else
                used = "no"
                chkd = "Tool Not Used"
            End If
            If stat = "COMP" Or stat = "CAN" Then
                sb.Append("<td class=""plainlabelblue"">" & chkd & "</td></tr>")
            Else
                sb.Append("<tr><td class=""plainlabel"">" & tnum & "</td>")
                sb.Append("<td class=""plainlabel"">" & tdesc & "</td>")
                If used = "yes" Then
                    sb.Append("<td class=""plainlabel""><input type=""radio"" id=""" & wtid & "-yes"" name=""t-" & wtid & """ ")
                    sb.Append("checked onclick=""checkwt('yes', this.name)"">Tool Used&nbsp;")
                    sb.Append("<input type=""radio"" id=""" & wtid & "-no"" name=""t-" & wtid & """ onclick=""checkwt('no', this.name)"">Tool Not Used</td></tr>")
                Else
                    sb.Append("<td class=""plainlabel""><input type=""radio"" id=""" & wtid & "-yes"" name=""t-" & wtid & """ ")
                    sb.Append("onclick=""checkwt('yes', this.name)"">Tool Used&nbsp;")
                    sb.Append("<input type=""radio"" id=""" & wtid & "-no"" name=""t-" & wtid & """ checked onclick=""checkwt('no', this.name)"">Tool Not Used</td></tr>")
                End If
                If tstr = "" Then
                    tstr = wtid & "~" & used
                Else
                    tstr += "," & wtid & "~" & used
                End If
            End If

        End While

        dr.Close()
        If start = 1 Then
            lbltstr.Value = tstr
            sb.Append("<tr><td colspan=""2""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
            sb.Append("</table>")
        Else
            sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""700"">")
            sb.Append("<tr><td colspan=""2"" class=""thdrsing plainlabel"">" & tmod.getlbl("cdlbl158", "woact.aspx.vb") & "</td></tr>")
            sb.Append("<tr><td colspan=""2"" class=""plainlabelred"">" & tmod.getlbl("cdlbl159", "woact.aspx.vb") & "</td></tr>")
            sb.Append("<tr><td colspan=""2""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
            sb.Append("</table>")
        End If
        start = 0

        sql = "select distinct wolubeid, lubenum, description, used from wolubes where wonum = '" & wonum & "'"
        dr = comp.GetRdrData(sql)
        Dim wlid, lnum, ldesc, lstr As String
        While dr.Read
            If start = 0 Then
                start = 1
                sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""700"">")
                sb.Append("<tr><td colspan=""3"" class=""thdrsingg label"">" & tmod.getlbl("cdlbl160", "woact.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td width=""120""></td><td width=""200""></td><td width=""380""></td></tr>")
            End If
            wlid = dr.Item("wolubeid").ToString
            lnum = dr.Item("lubenum").ToString
            ldesc = dr.Item("description").ToString
            used = dr.Item("used").ToString
            If used = "Y" Then
                used = "yes"
                chkd = "Lube Used"
            Else
                used = "no"
                chkd = "Lube Not Used"
            End If
            sb.Append("<tr><td class=""plainlabel"">" & lnum & "</td>")
            sb.Append("<td class=""plainlabel"">" & ldesc & "</td>")
            If stat = "COMP" Or stat = "CAN" Then
                sb.Append("<td class=""plainlabelblue"">" & chkd & "</td></tr>")
            Else
                If used = "yes" Then
                    sb.Append("<td class=""plainlabel""><input type=""radio"" id=""" & wlid & "-yes"" name=""l-" & wlid & """ ")
                    sb.Append("checked onclick=""checkwl('yes', this.name)"">Lube Used&nbsp;")
                    sb.Append("<input type=""radio"" id=""" & wlid & "-no"" name=""l-" & wlid & """ onclick=""checkwl('no', this.name)"">Lube Not Used</td></tr>")
                Else
                    sb.Append("<td class=""plainlabel""><input type=""radio"" id=""" & wlid & "-yes"" name=""l-" & wlid & """ ")
                    sb.Append("onclick=""checkwl('yes', this.name)"">Lube Used&nbsp;")
                    sb.Append("<input type=""radio"" id=""" & wlid & "-no"" name=""l-" & wlid & """ checked onclick=""checkwl('no', this.name)"">Lube Not Used</td></tr>")
                End If

                If lstr = "" Then
                    lstr = wlid & "~yes"
                Else
                    lstr += "," & wlid & "~yes"
                End If
            End If

        End While
        dr.Close()
        If start = 1 Then
            lbllstr.Value = lstr
            sb.Append("<tr><td colspan=""2""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
            sb.Append("</table>")
        Else
            sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""700"">")
            sb.Append("<tr><td colspan=""2"" class=""thdrsing plainlabel"">" & tmod.getlbl("cdlbl161", "woact.aspx.vb") & "</td></tr>")
            sb.Append("<tr><td colspan=""2"" class=""plainlabelred"">" & tmod.getlbl("cdlbl162", "woact.aspx.vb") & "</td></tr>")
            sb.Append("<tr><td colspan=""2""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
            sb.Append("</table>")
        End If
        start = 0

        tdwos.InnerHtml = sb.ToString


    End Sub
    Private Sub GetWoHead(ByVal wonum As String)
        Dim lc, skid As String
        wonum = lblwo.Value
        ro = lblro.Value
        sql = "select wonum, description, targstartdate, actstart, actfinish, estlabhrs, actlabhrs, jpid, leadcraft, skillid from workorder where wonum = '" & wonum & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdstart.InnerHtml = dr.Item("targstartdate").ToString
            txts.Text = dr.Item("actstart").ToString
            txtf.Text = dr.Item("actfinish").ToString
            lbls.Value = dr.Item("actstart").ToString
            lblf.Value = dr.Item("actfinish").ToString
            tdest.InnerHtml = dr.Item("estlabhrs").ToString
            tdact.InnerHtml = dr.Item("actlabhrs").ToString
            'txtlead.Text = dr.Item("leadcraft").ToString
            skid = dr.Item("skillid").ToString
        End While
        dr.Close()
        Try
            'ddskill.SelectedValue = skid
        Catch ex As Exception

        End Try
        'GetWO()
        ifmain.Attributes.Add("src", "woactm.aspx?typ=p&wo=" + wonum + "&stat=" + stat + "&sav=no&ro=" + ro)
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1327.Text = axlabs.GetASPXPage("woact.aspx", "lang1327")
        Catch ex As Exception
        End Try
        Try
            lang1328.Text = axlabs.GetASPXPage("woact.aspx", "lang1328")
        Catch ex As Exception
        End Try
        Try
            lang1329.Text = axlabs.GetASPXPage("woact.aspx", "lang1329")
        Catch ex As Exception
        End Try
        Try
            lang1330.Text = axlabs.GetASPXPage("woact.aspx", "lang1330")
        Catch ex As Exception
        End Try
        Try
            lang1331.Text = axlabs.GetASPXPage("woact.aspx", "lang1331")
        Catch ex As Exception
        End Try
        Try
            lang1332.Text = axlabs.GetASPXPage("woact.aspx", "lang1332")
        Catch ex As Exception
        End Try
        Try
            lang1333.Text = axlabs.GetASPXPage("woact.aspx", "lang1333")
        Catch ex As Exception
        End Try
        Try
            lang1334.Text = axlabs.GetASPXPage("woact.aspx", "lang1334")
        Catch ex As Exception
        End Try
        Try
            lang1335.Text = axlabs.GetASPXPage("woact.aspx", "lang1335")
        Catch ex As Exception
        End Try
        Try
            lang1336.Text = axlabs.GetASPXPage("woact.aspx", "lang1336")
        Catch ex As Exception
        End Try
        Try
            lang1337.Text = axlabs.GetASPXPage("woact.aspx", "lang1337")
        Catch ex As Exception
        End Try

    End Sub

End Class
