<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="woact.aspx.vb" Inherits="lucy_r12.woact" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>woact</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/woactaspx.js"></script>
    <script language="javascript" type="text/javascript">
        <!--
        function getcal(fld) {
            var wo = document.getElementById("lblwo").value;
            if (wo != "") {
                var stat = document.getElementById("lblstat").value;
                var ro = document.getElementById("lblro").value;
                if (stat != "COMP" && stat != "CAN" && stat != "WAPPR" && ro != "1") {
                    window.parent.setref();
                    var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
                    if (eReturn) {
                        fld = "txt" + fld;
                        var lfld = "lbl" + fld
                        document.getElementById(fld).value = eReturn;
                        document.getElementById(lfld).value = eReturn;
                        //document.getElementById("lblsubmit").value = "savedates";
                        //document.getElementById("form1").submit();
                        measalert(fld);
                    }
                }
            }
        }
            //-->
    </script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="scrolltop();checkit();" onunload="measalert();" class="tbg">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" cellspacing="0" cellpadding="2" width="700">
        <tr height="24">
            <td class="thdrsing label" colspan="7">
                <asp:Label ID="lang1327" runat="server">Work Order Details</asp:Label>
            </td>
        </tr>
        <tr height="22">
            <td class="plainlabelblue" id="Td1" runat="server" align="center" colspan="7">
                <asp:Label ID="lang1328" runat="server">Note that Work Order Detail Entries can only be made when Work Order is not Waiting to be Approved, Completed, or Cancelled.</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label" width="120">
                <asp:Label ID="lang1329" runat="server">Target Start:</asp:Label>
            </td>
            <td class="plainlabel" id="tdstart" width="100" runat="server">
            </td>
            <td width="30">
            </td>
            <td class="label" width="120">
                <asp:Label ID="lang1330" runat="server">Target Complete:</asp:Label>
            </td>
            <td class="plainlabel" id="Td2" width="100" runat="server">
            </td>
            <td width="30">
            </td>
            <td id="tdcomp" align="right" width="200" runat="server">
                <img class="details" id="imgcomp" onclick="measalert();" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif">
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang1331" runat="server">Actual Start:</asp:Label>
            </td>
            <td class="plainlabel" id="tdactstart" runat="server">
                <asp:TextBox ID="txts" runat="server" Width="100px" CssClass="plainlabel"></asp:TextBox>
            </td>
            <td>
                <img onclick="getcal('s');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg">
            </td>
            <td class="label">
                <asp:Label ID="lang1332" runat="server">Actual Complete:</asp:Label>
            </td>
            <td class="plainlabel" id="tdactfinish" runat="server">
                <asp:TextBox ID="txtf" runat="server" Width="100px" CssClass="plainlabel"></asp:TextBox>
            </td>
            <td>
                <img onclick="getcal('f');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg">
            </td>
        </tr>
        <tr height="24">
            <td class="label">
                <asp:Label ID="lang1333" runat="server">Est Labor Hours:</asp:Label>
            </td>
            <td class="plainlabel" id="tdest" runat="server" colspan="2">
            </td>
            <td class="label">
                <asp:Label ID="lang1334" runat="server">Actual Labor Hours:</asp:Label>
            </td>
            <td class="plainlabel" id="tdact" runat="server">
            </td>
        </tr>
        <tr>
            <td id="tdwos" colspan="7" runat="server">
                <table cellspacing="0" cellpadding="2" width="720">
                    <tr height="22">
                        <td class="thdrhov label" id="tdpar" onclick="gettab('par');" width="150" runat="server">
                            <asp:Label ID="lang1335" runat="server">Work Order Parts</asp:Label>
                        </td>
                        <td class="thdr label" id="tdtoo" onclick="gettab('too');" width="150" runat="server">
                            <asp:Label ID="lang1336" runat="server">Work Order Tools</asp:Label>
                        </td>
                        <td class="thdr label" id="tdlub" onclick="gettab('lub');" width="150" runat="server">
                            <asp:Label ID="lang1337" runat="server">Work Order Lubes</asp:Label>
                        </td>
                        <td width="270">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdborder" id="wo" colspan="4" runat="server">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <iframe id="ifmain" src="wohold.htm" frameborder="no" width="700" scrolling="auto"
                                            height="260" runat="server" style="background-color: transparent" allowtransparency>
                                        </iframe>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblwo" type="hidden" runat="server">
    <input id="lblsubmit" type="hidden" runat="server">
    <input id="lblstat" type="hidden" runat="server">
    <input id="lblpstr" type="hidden" runat="server">
    <input id="lbltstr" type="hidden" runat="server">
    <input id="lbllstr" type="hidden" runat="server">
    <input id="lblupsav" type="hidden" runat="server">
    <input id="xCoord" type="hidden" name="xCoord" runat="server">
    <input id="yCoord" type="hidden" name="yCoord" runat="server">
    <input type="hidden" id="lblpmid" runat="server">
    <input type="hidden" id="lblpmhid" runat="server"><input type="hidden" id="lblro"
        runat="server">
    <input type="hidden" id="lbllog" runat="server" name="lbllog">
    <input type="hidden" id="lblf" runat="server" />
    <input type="hidden" id="lbls" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
