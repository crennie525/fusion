<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wofmme2.aspx.vb" Inherits="lucy_r12.wofmme2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wofmme2</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/wofmme2aspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="scrolltop();checkit();" class="tbg">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" cellspacing="1" cellpadding="2" width="700">
        <tbody>
            <tr height="22">
                <td class="thdrsing label">
                    <asp:Label ID="lang1402" runat="server">Work order Level Measurements</asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <img src="../images/appbuttons/minibuttons/6PX.gif">
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                    <asp:DataGrid ID="dgmeas" runat="server" AutoGenerateColumns="False" Width="460px"
                        CellSpacing="1" AllowCustomPaging="True" AllowPaging="True" GridLines="None"
                        CellPadding="0" ShowFooter="True" BackColor="transparent">
                        <FooterStyle BackColor="transparent"></FooterStyle>
                        <EditItemStyle Height="15px"></EditItemStyle>
                        <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                        <ItemStyle CssClass="ptransrow"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Width="50px" Height="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="Imagebutton5" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Type">
                                <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Hi">
                                <HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="Label21" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Lo">
                                <HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label22" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="Label23" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Spec">
                                <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label25" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="Label26" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Measurement">
                                <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label27" runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.womeas") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtmeas" runat="server" Width="80px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.womeas") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbltmdid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.womid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lbltmdida" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.womid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
                            ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
                    </asp:DataGrid>
                </td>
            </tr>
        </tbody>
    </table>
    <input id="lbljpid" type="hidden" name="lbljpid" runat="server">
    <input id="lblwo" type="hidden" name="lblwo" runat="server">
    <input id="lbltaskcnt" type="hidden" name="lbltaskcnt" runat="server">
    <input id="lbltaskcurrcnt" type="hidden" name="lbltaskcurrcnt" runat="server">
    <input id="lblrow" type="hidden" name="lblrow" runat="server">
    <input id="lblfmcnt" type="hidden" name="lblfmcnt" runat="server">
    <input id="lbltasknum" type="hidden" name="lbltasknum" runat="server">
    <input id="lblcurrcnt" type="hidden" name="lblcurrcnt" runat="server">
    <input id="xCoord" type="hidden" name="xCoord" runat="server">
    <input id="yCoord" type="hidden" name="yCoord" runat="server"><input id="lblsubmit"
        type="hidden" name="lblsubmit" runat="server">
    <input id="lblfmstr" type="hidden" name="lblfmstr" runat="server">
    <input id="lblmstr" type="hidden" name="lblmstr" runat="server">
    <input id="lblmstrvals" type="hidden" name="lblmstrvals" runat="server">
    <input id="lblpstr" type="hidden" name="lblpstr" runat="server">
    <input id="lbltstr" type="hidden" name="lbltstr" runat="server">
    <input id="lbllstr" type="hidden" name="lbllstr" runat="server">
    <input id="lblmflg" type="hidden" name="lblmflg" runat="server"><input id="lblmcomp"
        type="hidden" name="lblmcomp" runat="server">
    <input type="hidden" id="lblstat" runat="server" name="lblstat"><input type="hidden"
        id="lblro" runat="server">
    <input type="hidden" id="lbllog" runat="server" name="lbllog">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    </form>
</body>
</html>
