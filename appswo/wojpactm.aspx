<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wojpactm.aspx.vb" Inherits="lucy_r12.wojpactm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wojpactm</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/wojpactmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="scrolltop();checkit();" class="tbg">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" cellspacing="0" cellpadding="2" width="700">
        <tbody>
            <tr class="details" id="trp" height="24" runat="server">
                <td class="thdrsing label">
                    <asp:Label ID="lang1423" runat="server">Work Order Job Plan Parts Usage</asp:Label>
                </td>
            </tr>
            <tr class="details" id="trt" height="24" runat="server">
                <td class="thdrsing label">
                    <asp:Label ID="lang1424" runat="server">Work Order Job Plan Tools Usage</asp:Label>
                </td>
            </tr>
            <tr class="details" id="trl" height="24" runat="server">
                <td class="thdrsing label">
                    <asp:Label ID="lang1425" runat="server">Work Order Job Plan Lube Usage</asp:Label>
                </td>
            </tr>
            <tr>
                <td class="plainlabelblue" id="tdnotes" align="center" runat="server">
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DataGrid ID="dgparts" runat="server" AutoGenerateColumns="False" BackColor="transparent">
                        <FooterStyle BackColor="transparent"></FooterStyle>
                        <EditItemStyle Height="15px"></EditItemStyle>
                        <AlternatingItemStyle CssClass="prowtransblue"></AlternatingItemStyle>
                        <ItemStyle CssClass="prowtrans"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgeditfm" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Task#">
                                <HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif">
                                <HeaderStyle Width="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <img src="../images/appbuttons/minibuttons/magnifier.gif" id="imgti" runat="server">
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <img src="../images/appbuttons/minibuttons/magnifier.gif" id="imgte" runat="server">
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Item#">
                                <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Description">
                                <HeaderStyle Width="190px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Width="190px" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Width="190px" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Qty">
                                <HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblqty" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Cost\Rate">
                                <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="txtcost" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Total">
                                <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label9" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.total") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="Label10" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.total") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Used">
                                <HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label11" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.used") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddus" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("used")) %>'>
                                        <asp:ListItem Value="Y">Yes</asp:ListItem>
                                        <asp:ListItem Value="N">No</asp:ListItem>
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Qty Used">
                                <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label14" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qtyused") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtqty" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qtyused") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Task#" Visible="False">
                                <HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label13" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.wotskpartid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblpid" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.wotskpartid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="details" id="tskdiv" style="border-right: black 1px solid; border-top: black 1px solid;
        z-index: 999; border-left: black 1px solid; width: 450px; border-bottom: black 1px solid;
        position: absolute; height: 180px">
        <table cellspacing="0" cellpadding="0" width="450" bgcolor="white">
            <tr bgcolor="blue">
                <td>
                    &nbsp;
                    <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
                        ForeColor="White">Task Description</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closetsk();" alt="" src="../images/appbuttons/minibuttons/close.gif"><br>
                </td>
            </tr>
            <tr class="tbg" height="30">
                <td style="padding-right: 3px; padding-left: 3px; padding-bottom: 3px; padding-top: 3px"
                    colspan="2">
                    <asp:TextBox ID="txttsk" runat="server" TextMode="MultiLine" ReadOnly="True" Width="440px"
                        CssClass="plainlabel" Height="170px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblwo" type="hidden" runat="server">
    <input id="lbljpid" type="hidden" runat="server">
    <input id="xCoord" type="hidden" name="xCoord" runat="server">
    <input id="yCoord" type="hidden" name="yCoord" runat="server">
    <input id="lblview" type="hidden" runat="server"><input id="lblstat" type="hidden"
        runat="server">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lbllog" runat="server" name="lbllog">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
