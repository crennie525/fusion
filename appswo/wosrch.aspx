<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wosrch.aspx.vb" Inherits="lucy_r12.wosrch" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wosrch</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
   
    <script language="javascript" type="text/javascript">
    <!--
        function retvals() {
            var reti = 0;
            var waid = document.getElementById("lblwaid").value;
            if (waid.length > 0) { reti += 1; }

            var wastr = document.getElementById("lblwaidstr").value;
            //if (wastr.length > 0) { reti += 1; }
            var wpaid = document.getElementById("lblwpaid").value;
            if (wpaid.length > 0) { reti += 1; }
            var wpstr = document.getElementById("lblwpaidstr").value;
            //if (wpstr.length > 0) { reti += 1; }
            var fdate = document.getElementById("lblfromdate").value;
            if (fdate.length > 0) { reti += 1; }
            var tdate = document.getElementById("lbltodate").value;
            if (tdate.length > 0) { reti += 1; }
            var wt = document.getElementById("lblwt").value;
            if (wt.length > 0) { reti += 1; }
            var stat = document.getElementById("lblstat").value;
            if (stat.length > 0) { reti += 1; }
            var wo = document.getElementById("txtwo").value;
            if (wo.length > 0) { reti += 1; }
            var cbua = document.getElementById("cbua")
            var cbt = document.getElementById("cbtarg")
            var cbsa = document.getElementById("cbsa")
            var cbco = document.getElementById("cbco")
            var rbwaw = document.getElementById("rbwoo")
            var rbwal = document.getElementById("rblao")
            var sup = document.getElementById("lblsup").value
            if (sup.length > 0) { reti += 1; }
            var lead = document.getElementById("lbllead").value
            if (lead.length > 0) { reti += 1; }
            var did = document.getElementById("lbldid").value
            if (did.length > 0) { reti += 1; }
            var clid = document.getElementById("lblclid").value
            if (clid.length > 0) { reti += 1; }
            var lid = document.getElementById("lbllid").value
            if (lid.length > 0) { reti += 1; }
            var eqid = document.getElementById("lbleqid").value
            if (eqid.length > 0) { reti += 1; }
            var fuid = document.getElementById("lblfuid").value
            if (fuid.length > 0) { reti += 1; }
            var coid = document.getElementById("lblcoid").value
            if (coid.length > 0) { reti += 1; }
            var rettyp = document.getElementById("lblrettyp").value
            if (rettyp.length > 0) { reti += 1; }
            var level = document.getElementById("lbllevel").value
            if (level.length > 0) { reti += 1; }
            var jl = document.getElementById("lbljustloc").value


            var ski = document.getElementById("lblskillid").value
            if (ski.length > 0) { reti += 1; }

            var rall = document.getElementById("rball");
            var rjl = document.getElementById("rbjl");
            if (rall.checked == true) {
                jl = "0";
            }
            else {
                jl = "1";
            }
            var fpc = document.getElementById("txtfpc").value
            if (fpc.length > 0) { reti += 1; }
            var rt = "0";
            var sa = "0";
            var wa = "0";
            var ua = "0"

            if (cbua.checked == true) {
                ua = "1";
            }
            if (rbwaw.checked == true) {
                wa = "1";
                //reti += 1;
            }
            if (cbt.checked == true) {
                rt = "1";
                reti += 1;
            }
            if (cbsa.checked == true) {
                sa = "1";
                reti += 1;
            }

            var cbs = document.getElementById("cbsched")
            var rs = "0";
            if (cbs.checked == true) {
                rs = "1";
                if (fdate != "" || tdate != "") {
                    reti += 1;
                }

            }

            var cba = document.getElementById("cbact")
            var ra = "0";
            if (cba.checked == true) {
                ra = "1";
                reti += 1;
            }
            var co = "0";
            if (cbco.checked == true) {
                co = "1";
                reti += 1;
                //alert("co")
            }

            var requestor = document.getElementById("txtRequestor").value;

            if (isNaN(wo)) {
                alert("Work Order Number Must be a Numeric Value")
            }
            else {
                //alert(reti)
                //var ret = waid + "~" + wpaid + "~" + fdate + "~" + tdate + "~" + wt + "~" + stat + "~" + wo + "~" + wastr + "~" + wpstr + "~" + rt + "~" + rs + "~" + ra + "~" + sa + "~" + wa + "~" + sup + "~" + lead + "~" + did + "~" + clid + "~" + lid + "~" + eqid + "~" + fuid + "~" + coid + "~" + rettyp + "~" + level + "~" + jl + "~" + fpc + "~" + co + "~" + ski + "~" + reti + "~" + ua;
                var ret = waid + "~" + wpaid + "~" + fdate + "~" + tdate + "~" + wt + "~" + stat + "~" + wo + "~" + wastr + "~" + wpstr + "~" + rt + "~" + rs + "~" + ra + "~" + sa + "~" + wa + "~" + sup + "~" + lead + "~" + did + "~" + clid + "~" + lid + "~" + eqid + "~" + fuid + "~" + coid + "~" + rettyp + "~" + level + "~" + jl + "~" + fpc + "~" + co + "~" + ski + "~" + reti + "~" + ua + "~" + requestor;
                //alert(ret)
            }
            window.parent.handlereturn(ret);
        }
        function getsk() {

            var eReturn = window.showModalDialog("../labor/labskillsdialog.aspx?lab=wo&sid=&wo=", "", "dialogHeight:500px; dialogWidth:360px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    var ret = eReturn.split("~")

                    //document.getElementById(who).value = "yes";
                    document.getElementById("tdskill").innerHTML = ret[1];
                    document.getElementById("lblskill").value = ret[1];
                    document.getElementById("lblskillid").value = ret[0];

                }
            }
        }
        function getwa(who) {
            var wo = document.getElementById("lblwonum").value;
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/workareaselectdialog.aspx?typ=srch&sid=" + sid + "&wo=" + wo, "", "dialogHeight:500px; dialogWidth:820px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    var ret = eReturn.split("~~")
                    document.getElementById("lblwaid").value = ret[0];
                    document.getElementById("tdwa").innerHTML = ret[1];
                    document.getElementById("lblwaidstr").value = ret[1];
                }
            }
        }
        function getpa() {
            var skill = "";
            var typ = "wplan";
            var ro = "0";
            var wo = document.getElementById("lblwonum").value;
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&wo=" + wo + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    var retarr = eReturn.split(",")

                    document.getElementById("lblwpaid").value = retarr[0];
                    document.getElementById("lblwpaidstr").value = retarr[1];
                    document.getElementById("tdpa").innerHTML = retarr[1];
                    //document.getElementById("lblsubmit").value = "go";
                    //document.getElementById("form1").submit();

                    //var ret = eReturn.split("~~")
                    //document.getElementById("lblwpaid").value = ret[0];
                    // document.getElementById("tdpa").innerHTML = ret[1];
                    //document.getElementById("lblwpaidstr").value = ret[1];
                }
            }
        }
        function getcomp() {
            var eReturn = window.showModalDialog("admindialog.aspx?list=eq&typ=all", "", "dialogHeight:500px; dialogWidth:640px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    jumpto2(eReturn);
                }
            }
        }
        function getcal(fld) {
            //var dt = document.getElementById("lblorig").value;
            var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
            if (eReturn) {
                document.getElementById(fld).value = eReturn;
                if (fld == "txtto") {
                    document.getElementById("lbltodate").value = eReturn;
                }
                else {
                    document.getElementById("lblfromdate").value = eReturn;
                }
                document.getElementById("trdpick").className = "view";
            }
        }
        function getstat() {

            var wo = document.getElementById("lblwonum").value;
            var eReturn = window.showModalDialog("wostatusdialog.aspx?typ=stat&wo=" + wo, "", "dialogHeight:370px; dialogWidth:370px; resizable=yes");
            //alert("getstat")
            //alert(eReturn)
            if (eReturn) {
                
                if (eReturn != "") {
                    document.getElementById("tdstat").innerHTML = eReturn;
                    document.getElementById("lblstat").value = eReturn;
                }
            }
        }
        function getwt() {
            var wo = document.getElementById("lblwonum").value;
            var eReturn = window.showModalDialog("wotypedialog.aspx?typ=wt&wo=" + wo, "", "dialogHeight:400px; dialogWidth:600px; resizable=yes");

            if (eReturn) {
                if (eReturn != "") {
                    document.getElementById("tdwt").innerHTML = eReturn;
                    document.getElementById("lblwt").value = eReturn;
                }
            }
        }
        function clearall() {
            document.getElementById("lbldid").value = "";
            document.getElementById("lbldept").value = "";
            document.getElementById("tddept").innerHTML = "";
            document.getElementById("lblclid").value = "";
            document.getElementById("lblcell").value = "";
            document.getElementById("tdcell").innerHTML = "";
            document.getElementById("lbleqid").value = "";
            document.getElementById("lbleq").value = "";
            document.getElementById("tdeq").innerHTML = "";
            document.getElementById("lblfuid").value = "";
            document.getElementById("lblfu").value = "";
            document.getElementById("tdfu").innerHTML = "";
            document.getElementById("tdcomp").innerHTML = "";
            document.getElementById("tdcomp3").innerHTML = "";
            document.getElementById("lblcoid").value = "";
            document.getElementById("lbllid").value = "";
            document.getElementById("lblloc").value = "";
            document.getElementById("tdloc3").innerHTML = "";
            document.getElementById("tdeq3").innerHTML = "";
            document.getElementById("tdfu3").innerHTML = "";
            document.getElementById("trdepts").className = "details";
            document.getElementById("trdepts1").className = "details";
            document.getElementById("trdepts2").className = "details";
            document.getElementById("trloc3").className = "details";
            document.getElementById("trloc3a").className = "details";
            document.getElementById("trloc3b").className = "details";
            document.getElementById("lblrettyp").value = "";

            document.getElementById("lbllevel").value = "";

            document.getElementById("lblskillid").value = "";
            document.getElementById("lblskill").value = "";
            document.getElementById("tdskill").innerHTML = "";
        }
        function resetp(who) {
            if (who == "all") {
                clearall();
                document.getElementById("lblwaid").value = "";
                document.getElementById("lblwaidstr").value = "";
                document.getElementById("tdwa").innerHTML = "";
                document.getElementById("lblwpaid").value = "";
                document.getElementById("lblwpaidstr").value = "";
                document.getElementById("tdpa").innerHTML = "";
                document.getElementById("lblfromdate").value = "";
                document.getElementById("txtfrom").value = "";
                document.getElementById("lbltodate").value = "";
                document.getElementById("txtto").value = "";
                document.getElementById("lblwt").value = "";
                document.getElementById("tdwt").innerHTML = "";
                document.getElementById("lblstat").value = "";
                document.getElementById("tdstat").innerHTML = "";
                document.getElementById("txtwo").value = "";
                document.getElementById("trdpick").className = "details";
                document.getElementById("lbllead").value = "";
                document.getElementById("tdlead").innerHTML = "";
                document.getElementById("lblsup").value = "";
                document.getElementById("tdsup").innerHTML = "";
                document.getElementById("lblskillid").value = "";
                document.getElementById("lblskill").value = "";
                document.getElementById("tdskill").innerHTML = "";
            }
            else if (who == "wa") {
                document.getElementById("lblwaid").value = "";
                document.getElementById("lblwaidstr").value = "";
                document.getElementById("tdwa").innerHTML = "";
            }
            else if (who == "wp") {
                document.getElementById("lblwpaid").value = "";
                document.getElementById("lblwpaidstr").value = "";
                document.getElementById("tdpa").innerHTML = "";
            }
            else if (who == "dfrom") {
                document.getElementById("lblfromdate").value = "";
                document.getElementById("txtfrom").value = "";
                if (document.getElementById("lbltodate").value == "") {
                    document.getElementById("trdpick").className = "details";
                }
            }
            else if (who == "dto") {
                document.getElementById("lbltodate").value = "";
                document.getElementById("txtto").value = "";
                if (document.getElementById("lblfromdate").value == "") {
                    document.getElementById("trdpick").className = "details";
                }
            }
            else if (who == "wt") {
                document.getElementById("lblwt").value = "";
                document.getElementById("tdwt").innerHTML = "";
            }
            else if (who == "stat") {
                document.getElementById("lblstat").value = "";
                document.getElementById("tdstat").innerHTML = "";
            }
            else if (who == "fpc") {
                document.getElementById("txtfpc").value = "";
            }
            else if (who == "ski") {
                document.getElementById("lblskillid").value = "";
                document.getElementById("lblskill").value = "";
                document.getElementById("tdskill").innerHTML = "";
            }

        }
        function getsuper(typ) {
            var skill = ""; // document.getElementById("lblskillid").value;
            var ro = "0"; // document.getElementById("lblro").value;
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    var retarr = eReturn.split(",")
                    if (typ == "sup") {
                        document.getElementById("lblsup").value = retarr[0];
                        document.getElementById("tdsup").innerHTML = retarr[1];
                        // document.getElementById("lblsupstr").value = retarr[1];
                    }
                    else {
                        document.getElementById("lbllead").value = retarr[0];
                        document.getElementById("tdlead").innerHTML = retarr[1];
                        var issaw = document.getElementById("lblissaw").value;
                        if (issaw == "1") {
                            document.getElementById("cbsa").checked = true;
                        }
                        //document.getElementById("lblleadstr").value = retarr[1];
                    }
                }
            }
        }
        function getminsrch() {
            var did = document.getElementById("lbldid").value;
            //alert(did)
            if (did != "") {
                retminsrch();
            }
            else {
                var sid = document.getElementById("lblsid").value;
                var wo = "";
                var typ;
                if (wo == "") {
                    typ = "lu";
                }
                else {
                    typ = "wo";
                }
                var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    clearall();
                    var ret = eReturn.split("~");
                    document.getElementById("lbldid").value = ret[0];
                    document.getElementById("lbldept").value = ret[1];
                    document.getElementById("tddept").innerHTML = ret[1];
                    document.getElementById("lblclid").value = ret[2];
                    document.getElementById("lblcell").value = ret[3];
                    document.getElementById("tdcell").innerHTML = ret[3];
                    document.getElementById("lbleqid").value = ret[4];
                    document.getElementById("lbleq").value = ret[5];
                    document.getElementById("tdeq").innerHTML = ret[5];
                    document.getElementById("lblfuid").value = ret[6];
                    document.getElementById("lblfu").value = ret[7];
                    document.getElementById("tdfu").innerHTML = ret[7];
                    document.getElementById("lblcoid").value = ret[8];
                    document.getElementById("lblcomp").value = ret[9];
                    document.getElementById("tdcomp").innerHTML = ret[9];
                    document.getElementById("lbllid").value = ret[12];
                    document.getElementById("lblloc").value = ret[13];
                    document.getElementById("trdepts").className = "view";
                    document.getElementById("trdepts1").className = "view";
                    document.getElementById("trdepts2").className = "view";
                    document.getElementById("lblrettyp").value = "depts";
                    var did = ret[0];
                    var eqid = ret[4];
                    var clid = ret[2];
                    var fuid = ret[6];
                    var coid = ret[8];
                    var lid = ret[12];
                    var typ;
                    if (lid == "") {
                        typ = "reg";
                    }
                    else {
                        typ = "dloc";
                    }
                    var task = "";
                    //window.location = "pmget.aspx?jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;
                }
            }
        }
        function retminsrch() {
            //alert()
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "ret";
            var did = document.getElementById("lbldid").value;
            var dept = document.getElementById("lbldept").value;
            var clid = document.getElementById("lblclid").value;
            var cell = document.getElementById("lblcell").value;
            var eqid = document.getElementById("lbleqid").value;
            var eq = document.getElementById("lbleq").value;
            var fuid = document.getElementById("lblfuid").value;
            var fu = document.getElementById("lblfu").value;
            var coid = document.getElementById("lblcoid").value;
            var comp = document.getElementById("lblcomp").value;
            var lid = document.getElementById("lbllid").value;
            var loc = document.getElementById("lblloc").value;
            //if (cell == "" && who != "checkfu" && who != "checkeq") {
            who = "deptret";
            //}

            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("lbldept").value = ret[1];
                document.getElementById("tddept").innerHTML = ret[1];
                document.getElementById("lblclid").value = ret[2];
                document.getElementById("lblcell").value = ret[3];
                document.getElementById("tdcell").innerHTML = ret[3];
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("lbleq").value = ret[5];
                document.getElementById("tdeq").innerHTML = ret[5];
                document.getElementById("lblfuid").value = ret[6];
                document.getElementById("lblfu").value = ret[7];
                document.getElementById("tdfu").innerHTML = ret[7];
                document.getElementById("lblcoid").value = ret[8];
                document.getElementById("lblcomp").value = ret[9];
                document.getElementById("lbllid").value = ret[12];
                document.getElementById("lblloc").value = ret[13];
                document.getElementById("trdepts").className = "view";
                document.getElementById("lblrettyp").value = "depts";
                var did = ret[0];
                var eqid = ret[4];
                var clid = ret[2];
                var fuid = ret[6];
                var coid = ret[8];
                var lid = ret[12];
                var typ;
                //if(lid=="") {
                //typ = "reg";
                //}
                //else {
                //typ = "dloc";
                //}
                typ = "reg"
                var task = "";
                //window.location = "pmget.aspx?jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;
            }
        }

        function getlocs1() {
            var lid = document.getElementById("lbllid").value;
            var typ = "lu"
            //alert(lid)
            if (lid != "") {
                typ = "retloc"
            }
            var sid = document.getElementById("lblsid").value;
            var eqid = document.getElementById("lbleqid").value;
            var fuid = document.getElementById("lblfuid").value;
            var coid = document.getElementById("lblcoid").value;
            //alert(fuid)
            var wo = "";
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                clearall();
                //alert(eReturn)
                var ret = eReturn.split("~");
                //ret = lidi + "~" + lid + "~" + loc + "~" + eq + "~" + eqnum + "~" + fu + "~" + func + "~" + co + "~" + comp + "~" + lev;
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("lblloc").value = ret[1];
                document.getElementById("tdloc3").innerHTML = ret[2];

                document.getElementById("lbleq").value = ret[4];
                document.getElementById("tdeq3").innerHTML = ret[4];
                document.getElementById("lbleqid").value = ret[3];

                document.getElementById("lblfuid").value = ret[5];
                document.getElementById("lblfu").value = ret[6];
                document.getElementById("tdfu3").innerHTML = ret[6];

                document.getElementById("lblcoid").value = ret[7];
                document.getElementById("lblcomp").value = ret[8];
                document.getElementById("tdcomp3").innerHTML = ret[8];

                document.getElementById("lbllevel").value = ret[9];

                document.getElementById("trloc3").className = "view";
                document.getElementById("trloc3a").className = "view";
                document.getElementById("trloc3b").className = "view";
                document.getElementById("lblrettyp").value = "locs";



                var did = "";
                var eqid = ret[3];
                var clid = "";
                var fuid = ret[5];
                var coid = ret[7];
                var lid = ret[0];
                var typ;
                typ = "loc";
                var task = "";
                if (eqid == "") {
                    document.getElementById("trloc3c").className = "view";
                }
                //window.location = "pmget.aspx?jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;

            }
        }
        function getlocsret() {
            var sid = document.getElementById("lblsid").value;
            var lid = document.getElementById("lbllid").value;
            var wo = "";
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=retloc&sid=" + sid + "&wo=" + wo + "&rlid=" + lid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                //ret = lidi + "~" + lid + "~" + loc + "~" + eq + "~" + eqnum + "~" + fu + "~" + func + "~" + co + "~" + comp + "~" + lev;
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("lblloc").value = ret[1];
                document.getElementById("tdloc3").innerHTML = ret[1];

                document.getElementById("lbleq").value = ret[4];
                document.getElementById("tdeq3").innerHTML = ret[4];
                document.getElementById("lbleqid").value = ret[3];

                document.getElementById("lblfuid").value = ret[5];
                document.getElementById("lblfu").value = ret[6];
                document.getElementById("tdfu3").innerHTML = ret[6];

                document.getElementById("lblcoid").value = ret[7];
                document.getElementById("lblcomp").value = ret[8];
                document.getElementById("tdcomp3").innerHTML = ret[8];

                document.getElementById("lbllevel").value = ret[9];

                document.getElementById("trloc3").className = "view";
                document.getElementById("trloc3a").className = "view";
                document.getElementById("trloc3b").className = "view";
                document.getElementById("lblrettyp").value = "locs";

                var did = "";
                var eqid = ret[3];
                var clid = "";
                var fuid = ret[5];
                var coid = ret[7];
                var lid = ret[0];
                var typ;
                typ = "loc";
                var task = "";
                if (eqid == "") {
                    document.getElementById("trloc3c").className = "view";
                }
                //window.location = "pmget.aspx?jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;

            }
        }
        function checkjust(who) {
            if (who == "al") {
                document.getElementById("trloc3d").className = "view";
            }
            else {
                document.getElementById("trloc3d").className = "details";
            }
        }
        
        //-->
    </script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td class="label">
                Work Order#
            </td>
            <td>
                <asp:TextBox ID="txtwo" runat="server" Width="80px" CssClass="plainlabel"></asp:TextBox>
            </td>
            <td colspan="6" align="right">
                <img onclick="resetp('all');" alt="" src="../images/appbuttons/minibuttons/switch2.gif">
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <table>
                    <tr>
                        <td id="tddepts" class="bluelabel" runat="server">
                            Use Departments
                        </td>
                        <td>
                            <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                        <td id="tdlocs1" class="bluelabel" runat="server">
                            Use Locations
                        </td>
                        <td>
                            <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trdepts" runat="server" class="details">
            <td class="label">
                Department
            </td>
            <td colspan="3" class="plainlabel" id="tddept" runat="server">
            </td>
            <td class="label">
                Station/Cell
            </td>
            <td colspan="3" class="plainlabel" id="tdcell" runat="server">
            </td>
        </tr>
        <tr id="trdepts1" runat="server" class="details">
            <td class="label">
                Equipment
            </td>
            <td colspan="3" class="plainlabel" id="tdeq" runat="server">
            </td>
            <td class="label">
                Function
            </td>
            <td colspan="3" class="plainlabel" id="tdfu" runat="server">
            </td>
        </tr>
        <tr id="trdepts2" runat="server" class="details">
            <td class="label">
                Component
            </td>
            <td colspan="7" class="plainlabel" id="tdcomp" runat="server">
            </td>
        </tr>
        <tr id="trloc3" runat="server" class="details">
            <td class="label">
                Location
            </td>
            <td colspan="7" class="plainlabel" id="tdloc3" runat="server">
            </td>
        </tr>
        <tr id="trloc3a" runat="server" class="details">
            <td class="label">
                Equipment
            </td>
            <td colspan="3" class="plainlabel" id="tdeq3" runat="server">
            </td>
            <td class="label">
                Function
            </td>
            <td colspan="3" class="plainlabel" id="tdfu3" runat="server">
            </td>
        </tr>
        <tr id="trloc3b" runat="server" class="details">
            <td class="label">
                Component
            </td>
            <td colspan="7" class="plainlabel" id="tdcomp3" runat="server">
            </td>
        </tr>
        <tr id="trloc3c" runat="server" class="details">
            <td colspan="8" align="center" class="plainlabel">
                <input type="radio" id="rball" name="rbw" />Search All Child Locations&nbsp;<input
                    type="radio" id="rbjl" name="rbw" checked />Just Search This Location
            </td>
        </tr>
        <tr id="trloc3d" runat="server" class="details">
            <td colspan="8" align="center" class="plainlabelblue">
                Selecting Search All Child Locations will return up to 5 Child Levels Only
            </td>
        </tr>
        <tr>
            <td colspan="8">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang874" runat="server">Supervisor</asp:Label>
            </td>
            <td class="plainlabel" id="tdsup" colspan="2" runat="server">
            </td>
            <td>
                <img title="Select Supervisor"
                    alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                    border="0" onclick="getsuper('sup');" />
            </td>
            <td class="label">
                <asp:Label ID="lang875" runat="server">Lead Craft</asp:Label>
            </td>
            <td class="plainlabel" id="tdlead" colspan="2" runat="server">
            </td>
            <td>
                <img title="Select Lead Craft"
                    alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                    border="0" onclick="getsuper('lead');" />
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang1375" runat="server">Skill</asp:Label>
            </td>
            <td id="tdskill" class="plainlabel" colspan="2" runat="server">
            </td>
            <td>
                <img id="Img3" onclick="getsk();" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                    runat="server">
            </td>
        </tr>
        <tr>
            <td class="label">
                Work Area
            </td>
            <td id="tdwa" class="plainlabel" runat="server">
            </td>
            <td>
                <img onclick="getwa('d');" src="../images/appbuttons/minibuttons/magnifier.gif">
            </td>
            <td>
                <img onclick="resetp('wa');" alt="" src="../images/appbuttons/minibuttons/switch2.gif">
            </td>
            <td colspan="4" class="plainlabel">
                <input type="radio" id="rbwoo" name="rbwa" runat="server" onclick="checkjust('jl');" />By
                Work Order<input type="radio" id="rblao" name="rbwa" runat="server" checked onclick="checkjust('al');" />By
                Labor
            </td>
        </tr>
        <tr class="view">
            <td class="label">
                Planner
            </td>
            <td id="tdpa" class="plainlabel" runat="server">
            </td>
            <td>
                <img onclick="getpa();" src="../images/appbuttons/minibuttons/magnifier.gif">
            </td>
            <td>
                <img onclick="resetp('wp');" alt="" src="../images/appbuttons/minibuttons/switch2.gif">
            </td>
        </tr>
        <tr>
            <td class="label">
                Work Status
            </td>
            <td id="tdstat" class="plainlabel" runat="server">
            </td>
            <td>
                <img onclick="getstat();" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
            </td>
            <td>
                <img onclick="resetp('stat');" alt="" src="../images/appbuttons/minibuttons/switch2.gif">
            </td>
            <td class="plainlabel">
                <input type="checkbox" id="cbua" runat="server" />&nbsp;Use Any
            </td>
        </tr>
        <tr>
            <td class="label">
                Work Type
            </td>
            <td id="tdwt" class="plainlabel" runat="server">
            </td>
            <td>
                <img id="imgwt" onclick="getwt();" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                    runat="server">
            </td>
            <td>
                <img onclick="resetp('wt');" alt="" src="../images/appbuttons/minibuttons/switch2.gif">
            </td>
        </tr>
        <tr>
            <td class="label">
                Column
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtfpc" runat="server" CssClass="plainlabel"></asp:TextBox>
            </td>
            <td>
                <img onclick="resetp('fpc');" alt="" src="../images/appbuttons/minibuttons/switch2.gif">
            </td>
        </tr>
        <tr id="trgetdates" runat="server">
            <td class="label">
                <asp:Label ID="lang1543" runat="server">From Date</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtfrom" runat="server" ReadOnly="True" Font-Size="9pt" CssClass="plainlabel"></asp:TextBox>
            </td>
            <td>
                <img onclick="getcal('txtfrom');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg">
            </td>
            <td>
                <img onclick="resetp('dfrom');" alt="" src="../images/appbuttons/minibuttons/switch2.gif">
            </td>
            <td class="label">
                <asp:Label ID="lang1544" runat="server">To Date</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtto" runat="server" ReadOnly="True" Font-Size="9pt" CssClass="plainlabel"></asp:TextBox>
            </td>
            <td>
                <img onclick="getcal('txtto');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                    width="18" height="18">
            </td>
            <td>
                <img onclick="resetp('dto');" alt="" src="../images/appbuttons/minibuttons/switch2.gif">
            </td>
        </tr>
        <tr>
			<td class="label">
				<asp:Label ID="Label1" runat="server">Requestor</asp:Label>
			</td>
			<td>
				<input type="text" id="txtRequestor" class="plainlabel" style="font-size: 9pt;" />
			</td>
		</tr>
        <tr id="trdpick" class="details" runat="server">
            <td colspan="8" align="center" class="plainlabelblue">
                <input type="radio" id="cbtarg" runat="server" name="rbdt" />Use Target
                <input type="radio" id="cbsched" runat="server" name="rbdt" />Use Scheduled
                <input type="radio" id="cbact" runat="server" name="rbdt" />Use Actual
            </td>
        </tr>
        <tr>
            <td colspan="8" class="plainlabel">
                <input type="checkbox" id="cbco" runat="server" />Critical Equipment Only
            </td>
        </tr>
        <tr id="trsa" runat="server">
            <td colspan="1" class="plainlabel">
                <input type="checkbox" id="cbsa" runat="server" />Show All
            </td>
            <td colspan="7" class="plainlabelblue">
                * Bypasses Labor Log In User ID Filter
            </td>
        </tr>
        <tr>
            <td colspan="8" align="right">
                <img onclick="retvals();" src="../images/appbuttons/minibuttons/savedisk1.gif">
            </td>
        </tr>
        <tr>
            <td colspan="8" class="plainlabelblue">
                <b><u>Date Search Defaults - PM and TPM Records</u></b><br />
                Using Target - From Target Start to Target Start<br />
                Using Scheduled - From Scheduled Start to Scheduled Start<br />
                Using Actual - From Actual Complete to Actual Complete<br />
                <br />
                <b><u>Date Search Defaults - All Other Work Types</u></b><br />
                Using Target - From Target Start to Target Complete<br />
                Using Scheduled - From Scheduled Start to Scheduled Complete<br />
                Using Actual - From Actual Start to Actual Complete<br />
                <br />
                <br />
                <b><u></u></b>
            </td>
        </tr>
    </table>
    <input id="lblwonum" type="hidden" runat="server">
    <input id="lblsid" type="hidden" runat="server">
    <input id="lblwaid" type="hidden" runat="server">
    <input id="lblwpaid" type="hidden" runat="server">
    <input id="lblfromdate" type="hidden" runat="server">
    <input id="lbltodate" type="hidden" runat="server">
    <input id="lblstat" type="hidden" runat="server">
    <input id="lblwt" type="hidden" runat="server">
    <input type="hidden" id="lblfilt" runat="server"><input type="hidden" id="lblwaidstr"
        runat="server">
    <input type="hidden" id="lblwpaidstr" runat="server">
    <input type="hidden" id="lblissaw" runat="server" />
    <input type="hidden" id="lblsup" runat="server" />
    <input type="hidden" id="lbllead" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lblclid" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lbldept" runat="server" />
    <input type="hidden" id="lblcell" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lblfuid" runat="server" />
    <input type="hidden" id="lblfu" runat="server" />
    <input type="hidden" id="lblcoid" runat="server" />
    <input type="hidden" id="lblcomp" runat="server" />
    <input type="hidden" id="lbllid" runat="server" />
    <input type="hidden" id="lblloc" runat="server" />
    <input type="hidden" id="lblrettyp" runat="server" />
    <input type="hidden" id="lbllevel" runat="server" />
    <input type="hidden" id="lbljustloc" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblskillid" runat="server" />
    <input type="hidden" id="lblskill" runat="server" />
    </form>
</body>
</html>
