

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wojpfm
    Inherits System.Web.UI.Page
	Protected WithEvents lang1511 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1510 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1509 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1508 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1507 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1506 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1505 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1504 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1503 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1502 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1501 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1500 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1499 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1498 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1497 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1496 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1495 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1494 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1493 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1492 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1491 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1490 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1489 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1488 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1487 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1486 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1485 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1484 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1483 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1482 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1481 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1480 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1479 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1478 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1477 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim comp As New Utilities
    Dim pmid As String
    Dim ds As DataSet
    Dim wonum, jpid, stat As String
    Dim taskhold As String = "0"
    Protected WithEvents tdnofail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents dgmeas As System.Web.UI.WebControls.DataList
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim headhold As String = "0"
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txts As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtf As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtacthours As System.Web.UI.WebControls.TextBox
    Protected WithEvents dltasks As System.Web.UI.WebControls.DataList
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwod As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdstart As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdactstart As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdactfinish As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdest As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwos As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdjpn As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdjpd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcurrcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrow As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfmcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmstrvals As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmflg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmcomp As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            wonum = Request.QueryString("wo").ToString
            jpid = Request.QueryString("jpid").ToString
            lblwo.Value = wonum
            lbljpid.Value = jpid
            stat = Request.QueryString("stat").ToString
            lblstat.Value = stat
            comp.Open()
            GetJPHead(jpid, wonum)
            PopDL(jpid, wonum)
            GetMeas(jpid, wonum)
            comp.Dispose()
        End If
    End Sub

    Private Sub GetJPHead(ByVal jpid As String, ByVal wonum As String)
        sql = "select jpnum, description from wojobplans where wonum = '" & wonum & "' and jpid = '" & jpid & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdjpn.InnerHtml = dr.Item("jpnum").ToString
            tdjpd.InnerHtml = dr.Item("description").ToString
        End While
        dr.Close()
    End Sub
    Private Sub PopDL(ByVal jpid As String, ByVal wonum As String)
        sql = "usp_getwojpfail '" & jpid & "','" & wonum & "'"
        ds = comp.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        dltasks.DataSource = dv
        dltasks.DataBind()

        If dv.Count = 0 Then
            tdnofail.InnerHtml = "No Job Plan Level Failure Modes Found"
        End If
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If CatID <> "Select" And CatID <> "N/A" Then 'Not IsDBNull(CatID) OrElse  
            If CatID = "Pass" Then
                iL = 0
            ElseIf CatID = "Fail" Then
                iL = 1
            ElseIf CatID = "YES" Then
                iL = 0
            ElseIf CatID = "NO" Then
                iL = 1
            End If
        Else
            iL = -1
        End If
        Return iL
    End Function

    Private Sub dltasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dltasks.EditCommand
        lblrow.Value = CType(e.Item.FindControl("A1"), Label).Text 'e.Item.ItemIndex
        lbltasknum.Value = CType(e.Item.FindControl("lbltasknume"), Label).Text 'e.Item.ItemIndex
        comp.Open()
        jpid = lbljpid.Value
        wonum = lblwo.Value
        dltasks.EditItemIndex = e.Item.ItemIndex

        PopDL(jpid, wonum)
        comp.Dispose()
    End Sub
    Private Sub GetMeas(ByVal jpid As String, ByVal wonum As String)
        sql = "select m.*, j.tasknum, j.taskdesc from wojptaskmeasdetails m " _
        + "left join wojobtasks j on j.jpid = m.jpid and j.pmtskid = m.pmtskid " _
        + "where m.jpid = '" & jpid & "' and j.wonum = '" & wonum & "'"
        dr = comp.GetRdrData(sql)
        dgmeas.DataSource = dr
        dgmeas.DataBind()


    End Sub

    Private Sub dltasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dltasks.ItemDataBound
        Dim trs, trf, trt, trfm, trhd As HtmlTableRow
        Dim pcnt, tcnt, lcnt, task As String
        Dim mea As String
        Dim fcnt As Integer = 0
        'Dim pcnt As Integer
        
        Dim fm1, fm2, fm3, fm4, fm5
        Dim rb1, rb2, rb3, rb4, rb5 As RadioButtonList
        Dim ibfm As ImageButton
        Dim rb1tc As RadioButtonList
        Dim rb1tci As String
        Dim rb1i, rb2i, rb3i, rb4i, rb5i As String
        Dim pmtid, pmfid, fm, fmid, comid As String

        If e.Item.ItemType = ListItemType.Item Then
            stat = lblstat.Value
            If stat = "COMP" Or stat = "CAN" Then
                ibfm = CType(e.Item.FindControl("imgeditfm"), ImageButton)
                ibfm.Attributes.Add("class", "details")

            End If


        End If

        If e.Item.ItemType = ListItemType.EditItem Then


            fcnt = 0
            fm1 = DataBinder.Eval(e.Item.DataItem, "fm1id").ToString 'e.Item.DataItem("fm1id").ToString
            If fm1 = 0 Then
                rb1 = CType(e.Item.FindControl("rb1"), RadioButtonList)
                rb1.Enabled = False
            Else
                If DataBinder.Eval(e.Item.DataItem, "fm1dd").ToString <> "Select" Then
                    fcnt = fcnt + 1
                End If

            End If
            fm2 = DataBinder.Eval(e.Item.DataItem, "fm2id").ToString 'e.Item.DataItem("fm2id").ToString
            If fm2 = 0 Then
                rb2 = CType(e.Item.FindControl("rb2"), RadioButtonList)
                rb2.Enabled = False
            Else
                If DataBinder.Eval(e.Item.DataItem, "fm2dd").ToString <> "Select" Then
                    fcnt = fcnt + 1
                End If

            End If
            fm3 = DataBinder.Eval(e.Item.DataItem, "fm3id").ToString 'e.Item.DataItem("fm3id").ToString
            If fm3 = 0 Then
                rb3 = CType(e.Item.FindControl("rb3"), RadioButtonList)
                rb3.Enabled = False
            Else
                If DataBinder.Eval(e.Item.DataItem, "fm3dd").ToString <> "Select" Then
                    fcnt = fcnt + 1
                End If

            End If
            fm4 = DataBinder.Eval(e.Item.DataItem, "fm4id").ToString 'e.Item.DataItem("fm4id").ToString
            If fm4 = 0 Then
                rb4 = CType(e.Item.FindControl("rb4"), RadioButtonList)
                rb4.Enabled = False
            Else
                If DataBinder.Eval(e.Item.DataItem, "fm4dd").ToString <> "Select" Then
                    fcnt = fcnt + 1
                End If

            End If
            fm5 = DataBinder.Eval(e.Item.DataItem, "fm5id").ToString 'e.Item.DataItem("fm5id").ToString
            If fm5 = 0 Then
                rb5 = CType(e.Item.FindControl("rb5"), RadioButtonList)
                rb5.Enabled = False
            Else
                If DataBinder.Eval(e.Item.DataItem, "fm5dd").ToString <> "Select" Then
                    fcnt = fcnt + 1
                End If

            End If

            rb1tc = CType(e.Item.FindControl("rbtc"), RadioButtonList)
            rb1tci = rb1tc.ClientID.ToString
            rb1tc.Attributes.Add("onclick", "checkcnt('" & rb1tci & "', '" & fcnt & "');")
            lblcurrcnt.Value = fcnt
        End If
    End Sub

    Private Sub dltasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dltasks.UpdateCommand
        Dim ret, typ, fail, pass, task, atime As String
        Dim tsk, cnt, adj As String
        wonum = lblwo.Value
        jpid = lbljpid.Value
        Dim pmtskid As String = lblrow.Value
        Dim rb1, rb2, rb3, rb4, rb5, rbc As RadioButtonList
        Dim sel1, sel2, sel3, sel4, sel5, selc As String
        rb1 = CType(e.Item.FindControl("rb1"), RadioButtonList)
        sel1 = rb1.SelectedValue.ToString
        If sel1 = "" Then sel1 = "0"
        rb2 = CType(e.Item.FindControl("rb2"), RadioButtonList)
        sel2 = rb2.SelectedValue.ToString
        If sel2 = "" Then sel2 = "0"
        rb3 = CType(e.Item.FindControl("rb3"), RadioButtonList)
        sel3 = rb3.SelectedValue.ToString
        If sel3 = "" Then sel3 = "0"
        rb4 = CType(e.Item.FindControl("rb4"), RadioButtonList)
        sel4 = rb4.SelectedValue.ToString
        If sel4 = "" Then sel4 = "0"
        rb5 = CType(e.Item.FindControl("rb5"), RadioButtonList)
        sel5 = rb5.SelectedValue.ToString
        If sel5 = "" Then sel5 = "0"
        
        sql = "usp_upjptskfm '" & sel1 & "', '" & sel2 & "', '" & sel3 & "', '" & sel4 & "', '" & sel5 & "', '" & pmtskid & "', '" & jpid & "', '" & wonum & "'"

        comp.Open()
        comp.Update(sql)
        wonum = lblwo.Value
        jpid = lbljpid.Value
        dltasks.EditItemIndex = -1
        PopDL(jpid, wonum)
        comp.Dispose()
    End Sub

    Private Sub dltasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dltasks.CancelCommand
        dltasks.EditItemIndex = -1
        comp.Open()
        wonum = lblwo.Value
        jpid = lbljpid.Value
        PopDL(jpid, wonum)
        comp.Dispose()
    End Sub

    Private Sub dgmeas_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dgmeas.EditCommand

        dgmeas.EditItemIndex = e.Item.ItemIndex
        jpid = lbljpid.Value
        wonum = lblwo.Value
        comp.Open()
        GetMeas(jpid, wonum)
        comp.Dispose()
    End Sub

    Private Sub dgmeas_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dgmeas.CancelCommand
        dgmeas.EditItemIndex = -1
        jpid = lbljpid.Value
        wonum = lblwo.Value
        comp.Open()
        GetMeas(jpid, wonum)
        comp.Dispose()
    End Sub

    Private Sub dgmeas_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dgmeas.UpdateCommand
        Dim mup As Integer
        Dim hi, lo As String
        Dim mstr As String = CType(e.Item.FindControl("txtmeas"), TextBox).Text
        Dim tmdid As String = CType(e.Item.FindControl("lbltmdida"), Label).Text
        Dim pmtskid As String = CType(e.Item.FindControl("lblpmtskida"), Label).Text
        Dim txtchk As Long
        Try
            txtchk = System.Convert.ToDecimal(mstr)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr582" , "wojpfm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If mstr <> "" Then
            'sql = "update pmTaskMeasDetMan set measurement = '" & mstr & "' where pmtskid = '" & pmtskid & "'"
            sql = "usp_upwojpmeas '" & tmdid & "', '" & pmtskid & "', '" & mstr & "'"
            comp.Open()
            'meas.Update(sql)
            'sql = "select count(*) from pmTaskMeasDetManHist where pmtskid = '" & pmtskid & "' and measurement is not null"
            'mup = meas.Scalar(sql)
            'dr = meas.GetRdrData(sql)
            'While dr.Read
            'mup = dr.Item("mup").ToString
            'hi = dr.Item("hi").ToString
            'mup = dr.Item("lo").ToString
            'End While
            'dr.Close()
            'lblmup.Value = mup
            'dgmeas.EditItemIndex = -1
            comp.Update(sql)
            jpid = lbljpid.Value
            wonum = lblwo.Value
            GetMeas(jpid, wonum)
            comp.Dispose()
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1477.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1477")
        Catch ex As Exception
        End Try
        Try
            lang1478.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1478")
        Catch ex As Exception
        End Try
        Try
            lang1479.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1479")
        Catch ex As Exception
        End Try
        Try
            lang1480.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1480")
        Catch ex As Exception
        End Try
        Try
            lang1481.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1481")
        Catch ex As Exception
        End Try
        Try
            lang1482.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1482")
        Catch ex As Exception
        End Try
        Try
            lang1483.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1483")
        Catch ex As Exception
        End Try
        Try
            lang1484.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1484")
        Catch ex As Exception
        End Try
        Try
            lang1485.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1485")
        Catch ex As Exception
        End Try
        Try
            lang1486.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1486")
        Catch ex As Exception
        End Try
        Try
            lang1487.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1487")
        Catch ex As Exception
        End Try
        Try
            lang1488.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1488")
        Catch ex As Exception
        End Try
        Try
            lang1489.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1489")
        Catch ex As Exception
        End Try
        Try
            lang1490.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1490")
        Catch ex As Exception
        End Try
        Try
            lang1491.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1491")
        Catch ex As Exception
        End Try
        Try
            lang1492.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1492")
        Catch ex As Exception
        End Try
        Try
            lang1493.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1493")
        Catch ex As Exception
        End Try
        Try
            lang1494.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1494")
        Catch ex As Exception
        End Try
        Try
            lang1495.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1495")
        Catch ex As Exception
        End Try
        Try
            lang1496.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1496")
        Catch ex As Exception
        End Try
        Try
            lang1497.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1497")
        Catch ex As Exception
        End Try
        Try
            lang1498.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1498")
        Catch ex As Exception
        End Try
        Try
            lang1499.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1499")
        Catch ex As Exception
        End Try
        Try
            lang1500.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1500")
        Catch ex As Exception
        End Try
        Try
            lang1501.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1501")
        Catch ex As Exception
        End Try
        Try
            lang1502.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1502")
        Catch ex As Exception
        End Try
        Try
            lang1503.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1503")
        Catch ex As Exception
        End Try
        Try
            lang1504.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1504")
        Catch ex As Exception
        End Try
        Try
            lang1505.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1505")
        Catch ex As Exception
        End Try
        Try
            lang1506.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1506")
        Catch ex As Exception
        End Try
        Try
            lang1507.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1507")
        Catch ex As Exception
        End Try
        Try
            lang1508.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1508")
        Catch ex As Exception
        End Try
        Try
            lang1509.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1509")
        Catch ex As Exception
        End Try
        Try
            lang1510.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1510")
        Catch ex As Exception
        End Try
        Try
            lang1511.Text = axlabs.GetASPXPage("wojpfm.aspx", "lang1511")
        Catch ex As Exception
        End Try

    End Sub

End Class
