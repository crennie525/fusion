

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wofail
    Inherits System.Web.UI.Page
	Protected WithEvents lang1386 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1385 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim fail As New Utilities
    Dim jump, wonum, comp, cd, stat, wt, ro, rostr, Login As String
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btntowo As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblcd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblwt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imggetfm As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbCompFM As System.Web.UI.WebControls.ListBox
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            fail.Open()
            'PopFail(0)
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro <> "1" Then
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = fail.CheckROS(rostr, "wo")
                    'lblro.Value = ro
                End If
            End If
            jump = Request.QueryString("jump").ToString
            If jump = "yes" Then
                Try

                    wonum = Request.QueryString("wo").ToString
                    lblwo.Value = wonum
                    Try
                        CheckWT()
                    Catch ex As Exception

                    End Try

                    If lblwt.Value <> "PM" And ro <> "1" Then
                        PopSelected()
                        'CheckComp()
                        Try
                            comp = Request.QueryString("co").ToString
                            lblco.Value = comp
                            cd = Request.QueryString("cd").ToString
                            lblcd.Value = cd
                            PopComp()
                        Catch ex As Exception

                        End Try
                    Else
                        PopSelected()
                        'CheckComp()
                        Try
                            comp = Request.QueryString("co").ToString
                            lblco.Value = comp
                            cd = Request.QueryString("cd").ToString
                            lblcd.Value = cd
                            PopComp()
                        Catch ex As Exception

                        End Try
                        lbCompFM.CssClass = "plainlabelblue"
                        lbfailmodes.CssClass = "plainlabelblue"
                        btntowo.Visible = False
                        btnfromcomp.Visible = False
                        todis.Attributes.Add("class", "view")
                        fromdis.Attributes.Add("class", "view")
                        imggetfm.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
                    End If

                Catch ex As Exception

                End Try
            Else
                Try
                    wonum = Request.QueryString("wo").ToString
                    lblwo.Value = wonum
                    CheckWT()
                    stat = Request.QueryString("stat").ToString
                    lblstat.Value = stat
                    If lblwt.Value <> "PM" And ro <> "1" Then
                        PopSelected()
                        CheckComp()
                        If stat = "COMP" Or stat = "CAN" Then
                            lbCompFM.CssClass = "plainlabelblue"
                            lbfailmodes.CssClass = "plainlabelblue"
                            btntowo.Visible = False
                            btnfromcomp.Visible = False
                            todis.Attributes.Add("class", "view")
                            fromdis.Attributes.Add("class", "view")
                            imggetfm.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
                        End If
                    Else
                        PopSelected()
                        'CheckComp()
                        Try
                            comp = Request.QueryString("co").ToString
                            lblco.Value = comp
                            cd = Request.QueryString("cd").ToString
                            lblcd.Value = cd
                            PopComp()
                        Catch ex As Exception

                        End Try
                        lbCompFM.CssClass = "plainlabelblue"
                        lbfailmodes.CssClass = "plainlabelblue"
                        btntowo.Visible = False
                        btnfromcomp.Visible = False
                        todis.Attributes.Add("class", "view")
                        fromdis.Attributes.Add("class", "view")
                        imggetfm.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
                    End If

                Catch ex As Exception

                End Try

            End If
            fail.Dispose()
        Else
            If Request.Form("lblsubmit") = "1" Then
                lblsubmit.Value = ""
                fail.Open()
                PopSelected()
                fail.Dispose()
            End If
        End If

    End Sub
    Private Sub CheckComp()
        wonum = lblwo.Value
        sql = "select comid from workorder where wonum = '" & wonum & "'"
        comp = fail.Scalar(sql)
        If comp <> "" Then
            lblco.Value = comp
            PopComp()
        End If

    End Sub

    Private Sub PopComp()
        comp = lblco.Value
        wonum = lblwo.Value
        sql = "select compfailid, failuremode " _
         + "from componentfailmodes where comid = '" & comp & "' and failid not in (" _
         + "select failid from wofailmodes1 where wonum = '" & wonum & "') "
        Try
            dr = fail.GetRdrData(sql)
        Catch ex As Exception

        End Try
        lbCompFM.DataSource = dr
        lbCompFM.DataTextField = "failuremode"
        lbCompFM.DataValueField = "compfailid"
        lbCompFM.DataBind()
        dr.Close()
    End Sub
    Private Sub CheckWT()
        wonum = lblwo.Value
        sql = "select worktype from workorder where wonum = '" & wonum & "'"
        wt = fail.strScalar(sql)
        lblwt.value = wt
    End Sub
    Private Sub PopSelected()
        wonum = lblwo.Value
        sql = "select failid, failuremode from wofailmodes1 where wonum = '" & wonum & "' order by failuremode"
        Try
            dr = fail.GetRdrData(sql)
        Catch ex As Exception
        End Try
        lbfailmodes.DataSource = dr
        lbfailmodes.DataTextField = "failuremode"
        lbfailmodes.DataValueField = "failid"
        lbfailmodes.DataBind()
        dr.Close()
    End Sub
    Private Sub PopFail(ByVal cid As String)
        Dim scnt As Integer
        wonum = lblwo.Value
        comp = lblco.Value
        Dim dt, val, filt As String
        dt = "FailureModes"
        val = "failid, failuremode"
        If comp <> "" Then
            filt = " where compid = '" & cid & "' and failid not in (" _
            + "select failid from wofailmodes1 where wonum = '" & wonum & "') "
        Else
            filt = " where compid = '" & cid & "' and (failid not in (" _
            + "select failid from wofailmodes1 where wonum = '" & wonum & "') and failid not in (" _
            + "select failid from componentfailmodes where comid = '" & comp & "')) "
        End If

        dr = fail.GetList(dt, val, filt)
        'lbfailmaster.DataSource = dr
        'lbfailmaster.DataTextField = "failuremode"
        'lbfailmaster.DataValueField = "failid"
        'lbfailmaster.DataBind()
        dr.Close()
    End Sub
    Private Sub GetItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal typ As String)
        wonum = lblwo.Value
        Dim fcnt, fcnt2 As Integer
        sql = "select count(*) from wofailmodes1 where wonum = '" & wonum & "' and failid = '" & fi & "'"
        fcnt = fail.Scalar(sql)
        If fcnt > 0 Then
            Dim strMessage As String =  tmod.getmsg("cdstr565" , "wofail.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Try
                If comp <> "na" Then
                    sql = "usp_addWoFailureMode " & wonum & ", " & fi & ", '" & f & "', '" & comp & "','" & typ & "'"
                Else
                    sql = "insert into wofailmodes1 (wonum, failid, failuremode) values " _
                    + "('" & wonum & "','" & fi & "','" & f & "')"
                End If
                fail.Update(sql)
                Dim Item As ListItem
                Dim fm As String
                For Each Item In lbfailmodes.Items
                    f = Item.Text.ToString
                    If Len(fm) = 0 Then
                        fm = f & "(___)"
                    Else
                        fm += " " & f & "(___)"
                    End If
                Next
                fm = Replace(fm, "'", Chr(180), , , vbTextCompare)
                If fcnt <> 1 Then
                    sql = "update wofailout set failout = '" & fm & "' where wonum = '" & wonum & "'"
                Else
                    sql = "insert into wofailout (wonum, failout) values " _
                    + "('" & wonum & "', '" & fm & "')"
                End If
                fail.Update(sql)
            Catch ex As Exception

            End Try
        End If
    End Sub


    Private Sub btntowo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntowo.Click
        Dim Item As ListItem
        Dim f, fi As String
        wonum = lblwo.Value
        comp = lblco.Value
        fail.Open()
        For Each Item In lbCompFM.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                GetItems(f, fi, comp, "hascomp")
            End If
        Next
        Try
            PopFail(0)
            PopComp()
            PopSelected()
        Catch ex As Exception
        End Try
        fail.Dispose()
    End Sub

    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        Dim Item As ListItem
        Dim f, fi As String
        wonum = lblwo.Value
        comp = lblco.Value
        fail.Open()
        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                RemItems(f, fi)
            End If
        Next
        Try
            PopFail(0)
            PopComp()
            PopSelected()
        Catch ex As Exception
        End Try
        fail.Dispose()
    End Sub
    Private Sub RemItems(ByVal f As String, ByVal fi As String)
        wonum = lblwo.Value
        comp = lblco.Value
        Try
            'If comp <> "" Then
            'sql = "sp_delWoFailureMode '" & wonum & "', '" & fi & "', '" & comp & "'"
            'Else
            sql = "delete from wofailmodes1 where wonum = '" & wonum & "' and failid = '" & fi & "'"
            'End If

            fail.Update(sql)
            Dim fm As String
            Dim Item As ListItem
            For Each Item In lbfailmodes.Items
                f = Item.Text.ToString
                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
            Next
            fm = Replace(fm, "'", Chr(180), , , vbTextCompare)
            sql = "update wofailout set failout = '" & fm & "' where wonum = '" & wonum & "'"
            fail.Update(sql)
        Catch ex As Exception

        End Try
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1385.Text = axlabs.GetASPXPage("wofail.aspx", "lang1385")
        Catch ex As Exception
        End Try
        Try
            lang1386.Text = axlabs.GetASPXPage("wofail.aspx", "lang1386")
        Catch ex As Exception
        End Try

    End Sub

End Class
