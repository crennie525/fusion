<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wojpcompman.aspx.vb" Inherits="lucy_r12.wojpcompman" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wojpcompman</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/wojpcompmanaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="scrolltop();">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" width="880">
        <tbody>
            <tr>
                <td>
                    <table width="880">
                        <tr height="26">
                            <td class="label">
                                <asp:Label ID="lang1431" runat="server">Work Order#</asp:Label>
                            </td>
                            <td class="plainlabel" id="tdwo" runat="server">
                            </td>
                            <td class="plainlabel" id="tdwod" colspan="7" runat="server">
                            </td>
                        </tr>
                        <tr height="24">
                            <td class="thdrsing label" colspan="9">
                                <asp:Label ID="lang1432" runat="server">Work Order Details</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="100">
                                <asp:Label ID="lang1433" runat="server">Target Start:</asp:Label>
                            </td>
                            <td class="plainlabel" id="tdstart" width="100" runat="server">
                            </td>
                            <td class="label" width="100">
                                <asp:Label ID="lang1434" runat="server">Actual Start:</asp:Label>
                            </td>
                            <td class="plainlabel" id="tdactstart" width="100" runat="server">
                                <asp:TextBox ID="txts" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td width="25">
                                <img onclick="getcal('s');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                    width="19">
                            </td>
                            <td class="label" width="120">
                                <asp:Label ID="lang1435" runat="server">Actual Complete:</asp:Label>
                            </td>
                            <td class="plainlabel" id="tdactfinish" width="100" runat="server">
                                <asp:TextBox ID="txtf" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td width="25">
                                <img onclick="getcal('f');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                    width="19">
                            </td>
                            <td id="tdcomp" align="right" width="130" runat="server">
                                <img id="imgcomp" onclick="measalert();" alt="" src="../images/appbuttons/bgbuttons/submit.gif">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td class="label">
                                <asp:Label ID="lang1436" runat="server">Est Labor Hours:</asp:Label>
                            </td>
                            <td class="plainlabel" id="tdest" runat="server">
                            </td>
                            <td>
                            </td>
                            <td class="label">
                                <asp:Label ID="lang1437" runat="server">Actual Labor Hours:</asp:Label>
                            </td>
                            <td class="plainlabel" runat="server">
                                <asp:TextBox ID="txtacthours" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td id="tdwos" runat="server">
                </td>
            </tr>
            <tr height="24">
                <td class="thdrsing label">
                    <asp:Label ID="lang1438" runat="server">Work Order Job Plan Details</asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="880">
                        <tr height="26">
                            <td class="label" width="80">
                                <asp:Label ID="lang1439" runat="server">Job Plan:</asp:Label>
                            </td>
                            <td class="plainlabel" id="tdjpn" width="120" runat="server">
                            </td>
                            <td class="plainlabel" id="tdjpd" width="410" colspan="6" runat="server">
                            </td>
                            <td class="label" align="right" width="250">
                                <asp:CheckBox ID="cball" runat="server" AutoPostBack="True" Text="Mark All As Complete?">
                                </asp:CheckBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tbody>
                <tr>
                    <td>
                        <asp:DataList ID="dltasks" runat="server">
                            <HeaderTemplate>
                                <table width="880">
                                    <tr>
                                        <td width="60">
                                        </td>
                                        <td width="60">
                                        </td>
                                        <td width="120">
                                        </td>
                                        <td width="60">
                                        </td>
                                        <td width="120">
                                        </td>
                                        <td width="60">
                                        </td>
                                        <td width="120">
                                        </td>
                                        <td width="60">
                                        </td>
                                        <td width="120">
                                        </td>
                                        <td width="60">
                                        </td>
                                        <td width="80">
                                        </td>
                                        <td width="40">
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr id="trfunc" runat="server">
                                    <td>
                                        <asp:Label ID="A1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'
                                            CssClass="details">
                                        </asp:Label>
                                        <asp:Label ID="A2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fmcnt") %>'
                                            CssClass="details">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr id="trhdr" runat="server">
                                    <td class="thdrsing plainlabel">
                                        <asp:Label ID="lang1440" runat="server">Edit</asp:Label>
                                    </td>
                                    <td class="thdrsing plainlabel">
                                        <asp:Label ID="lang1441" runat="server">Task#</asp:Label>
                                    </td>
                                    <td class="thdrsing plainlabel" colspan="8">
                                        <asp:Label ID="lang1442" runat="server">Task Description</asp:Label>
                                    </td>
                                    <td class="thdrsing plainlabel">
                                        <asp:Label ID="lang1443" runat="server">Complete?</asp:Label>
                                    </td>
                                    <td class="thdrsing plainlabel">
                                        <asp:Label ID="lang1444" runat="server">Meas?</asp:Label>
                                    </td>
                                </tr>
                                <tr id="trtask" runat="server">
                                    <td rowspan="4">
                                        <asp:ImageButton ID="Imagebutton1" runat="server" ToolTip="Edit Record" CommandName="Edit"
                                            ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"></asp:ImageButton>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label ID="lbltasknume" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'
                                            CssClass="plainlabel">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel" colspan="8">
                                        <%# DataBinder.Eval(Container, "DataItem.task") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.tc") %>
                                    </td>
                                    <td>
                                        <img src="../images/appbuttons/minibuttons/measure.gif" id="imgme" runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang1445" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM1
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang1446" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM2
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang1447" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM3
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang1448" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM4
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang1449" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel" colspan="2">
                                        FM5
                                    </td>
                                </tr>
                                <tr id="trfm" runat="server">
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm1dd") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm1s") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm2dd") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm2s") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm3dd") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm3s") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm4dd") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm4s") %>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm5dd") %>
                                    </td>
                                    <td class="plainlabel" colspan="2">
                                        <%# DataBinder.Eval(Container, "DataItem.fm5s") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="11">
                                        <table>
                                            <tr>
                                                <td class="thdrsing plainlabel" width="180">
                                                    <asp:Label ID="lang1450" runat="server">Skill Required</asp:Label>
                                                </td>
                                                <td class="thdrsing plainlabel" width="50">
                                                    Qty
                                                </td>
                                                <td class="thdrsing plainlabel" width="100">
                                                    <asp:Label ID="lang1451" runat="server">Time</asp:Label>
                                                </td>
                                                <td class="thdrsing plainlabel" width="100">
                                                    <asp:Label ID="lang1452" runat="server">Actual Time</asp:Label>
                                                </td>
                                                <td class="thdrsing plainlabel" width="120">
                                                    <asp:Label ID="lang1453" runat="server">Parts\Tools\Lubes</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.skill") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.qty") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.ttime") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.acttime") %>
                                                </td>
                                                <td>
                                                    <img src="../images/appbuttons/minibuttons/parttrans.gif" id="imgp" runat="server">
                                                    <img src="../images/appbuttons/minibuttons/tooltrans.gif" id="imgt" runat="server">
                                                    <img src="../images/appbuttons/minibuttons/lubetrans.gif" id="imgl" runat="server">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <tr id="Tr2" runat="server">
                                    <td class="thdrsing plainlabel">
                                        <asp:Label ID="lang1454" runat="server">Edit</asp:Label>
                                    </td>
                                    <td class="thdrsing plainlabel">
                                        <asp:Label ID="lang1455" runat="server">Task#</asp:Label>
                                    </td>
                                    <td class="thdrsing plainlabel" colspan="8">
                                        <asp:Label ID="lang1456" runat="server">Task Description</asp:Label>
                                    </td>
                                    <td class="thdrsing plainlabel">
                                        <asp:Label ID="lang1457" runat="server">Complete?</asp:Label>
                                    </td>
                                    <td class="thdrsing plainlabel">
                                        <asp:Label ID="lang1458" runat="server">Meas?</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="4">
                                        <asp:ImageButton ID="Imagebutton2" runat="server" ToolTip="Save Changes" CommandName="Update"
                                            ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:ImageButton>
                                        <asp:ImageButton ID="Imagebutton3" runat="server" ToolTip="Cancel Changes" CommandName="Cancel"
                                            ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"></asp:ImageButton>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.tasknum") %>
                                    </td>
                                    <td class="plainlabel" colspan="8">
                                        <%# DataBinder.Eval(Container, "DataItem.task") %>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rbtc" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("tc")) %>'>
                                            <asp:ListItem Value="1">YES</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        <img src="../images/appbuttons/minibuttons/measure.gif" id="imgm" runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang1459" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM1
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang1460" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM2
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang1461" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM3
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang1462" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        FM4
                                    </td>
                                    <td class="thdrsingg plainlabel">
                                        <asp:Label ID="lang1463" runat="server">Pass/Fail?</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel" colspan="2">
                                        FM5
                                    </td>
                                </tr>
                                <tr>
                                    <td class="plainlabel">
                                        <asp:RadioButtonList ID="rb1" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm1dd")) %>'>
                                            <asp:ListItem Value="1">PASS</asp:ListItem>
                                            <asp:ListItem Value="2">FAIL</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm1s") %>
                                    </td>
                                    <td class="plainlabel" bgcolor="#E7F1FD">
                                        <asp:RadioButtonList ID="rb2" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm2dd")) %>'>
                                            <asp:ListItem Value="1">PASS</asp:ListItem>
                                            <asp:ListItem Value="2">FAIL</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td class="plainlabel" bgcolor="#E7F1FD">
                                        <%# DataBinder.Eval(Container, "DataItem.fm2s") %>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:RadioButtonList ID="rb3" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm3dd")) %>'>
                                            <asp:ListItem Value="1">PASS</asp:ListItem>
                                            <asp:ListItem Value="2">FAIL</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td class="plainlabel">
                                        <%# DataBinder.Eval(Container, "DataItem.fm3s") %>
                                    </td>
                                    <td class="plainlabel" bgcolor="#E7F1FD">
                                        <asp:RadioButtonList ID="rb4" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm4dd")) %>'>
                                            <asp:ListItem Value="1">PASS</asp:ListItem>
                                            <asp:ListItem Value="2">FAIL</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td class="plainlabel" bgcolor="#E7F1FD">
                                        <%# DataBinder.Eval(Container, "DataItem.fm4s") %>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:RadioButtonList ID="rb5" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm5dd")) %>'>
                                            <asp:ListItem Value="1">PASS</asp:ListItem>
                                            <asp:ListItem Value="2">FAIL</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td class="plainlabel" colspan="2">
                                        <%# DataBinder.Eval(Container, "DataItem.fm5s") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="11">
                                        <table>
                                            <tr>
                                                <td class="thdrsing plainlabel" width="180">
                                                    <asp:Label ID="lang1464" runat="server">Skill Required</asp:Label>
                                                </td>
                                                <td class="thdrsing plainlabel" width="50">
                                                    Qty
                                                </td>
                                                <td class="thdrsing plainlabel" width="100">
                                                    <asp:Label ID="lang1465" runat="server">Time</asp:Label>
                                                </td>
                                                <td class="thdrsing plainlabel" width="100">
                                                    <asp:Label ID="lang1466" runat="server">Actual Time</asp:Label>
                                                </td>
                                                <td class="thdrsing plainlabel" width="120">
                                                    <asp:Label ID="lang1467" runat="server">Parts\Tools\Lubes</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.skill") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.qty") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.ttime") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <asp:TextBox ID="txtatime" CssClass="plainlabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.acttime") %>'></asp:TextBox>
                                                </td>
                                                <td>
                                                    <img src="../images/appbuttons/minibuttons/parttrans.gif" id="imgpe" runat="server">
                                                    <img src="../images/appbuttons/minibuttons/tooltrans.gif" id="imgte" runat="server">
                                                    <img src="../images/appbuttons/minibuttons/lubetrans.gif" id="imgle" runat="server">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </EditItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:DataList>
                    </td>
                </tr>
            </tbody>
    </table>
    <input id="lbljpid" type="hidden" runat="server">
    <input id="lblwo" type="hidden" runat="server">
    <input id="lbltaskcnt" type="hidden" runat="server">
    <input id="lbltaskcurrcnt" type="hidden" runat="server">
    <input id="lblrow" type="hidden" runat="server">
    <input id="lblfmcnt" type="hidden" runat="server">
    <input id="lbltasknum" type="hidden" runat="server">
    <input id="lblcurrcnt" type="hidden" runat="server">
    <input id="xCoord" type="hidden" runat="server">
    <input id="yCoord" type="hidden" runat="server"><input id="lblsubmit" type="hidden"
        runat="server">
    <input id="lblfmstr" type="hidden" runat="server">
    <input id="lblmstr" type="hidden" runat="server">
    <input id="lblmstrvals" type="hidden" runat="server">
    <input id="lblpstr" type="hidden" runat="server">
    <input id="lbltstr" type="hidden" runat="server">
    <input id="lbllstr" type="hidden" runat="server">
    <input id="lblmflg" type="hidden" runat="server"><input id="lblmcomp" type="hidden"
        runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
