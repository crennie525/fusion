﻿Public Class woexpdchartdialog
    Inherits System.Web.UI.Page
    Dim eqid, wonum, wt, pmid, pmhid, who As String
    Dim usr, isdown, stat, isdownp, typ As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try

        If Not IsPostBack Then
            typ = Request.QueryString("typ").ToString
            who = Request.QueryString("who").ToString
            wt = Request.QueryString("wt").ToString
            pmid = Request.QueryString("pmid").ToString
            pmhid = Request.QueryString("pmhid").ToString
            eqid = Request.QueryString("eqid").ToString
            wonum = Request.QueryString("wo").ToString

            If who = "prod" Then
                If typ = "simp" Then
                    ifexpd.Attributes.Add("src", "woexpdcharts.aspx?wo=" + wonum + "&eqid=" + eqid + "&usr=" + usr + "&isdown=" + isdown + "&stat=" + stat + "&isdownp=" + isdownp + "&wt=" + wt + "&pmid=" + pmid + "&pmhid=" + pmhid)
                Else
                    ifexpd.Attributes.Add("src", "woexpdchart.aspx?wo=" + wonum + "&eqid=" + eqid + "&usr=" + usr + "&isdown=" + isdown + "&stat=" + stat + "&isdownp=" + isdownp + "&wt=" + wt + "&pmid=" + pmid + "&pmhid=" + pmhid)
                End If

            ElseIf who = "eq" Then
                If typ = "simp" Then
                    ifexpd.Attributes.Add("src", "woexpdchart1s.aspx?wo=" + wonum + "&eqid=" + eqid + "&usr=" + usr + "&isdown=" + isdown + "&stat=" + stat + "&isdownp=" + isdownp + "&wt=" + wt + "&pmid=" + pmid + "&pmhid=" + pmhid)
                Else
                    ifexpd.Attributes.Add("src", "woexpdchart1.aspx?wo=" + wonum + "&eqid=" + eqid + "&usr=" + usr + "&isdown=" + isdown + "&stat=" + stat + "&isdownp=" + isdownp + "&wt=" + wt + "&pmid=" + pmid + "&pmhid=" + pmhid)
                End If

            End If

        End If

    End Sub

End Class