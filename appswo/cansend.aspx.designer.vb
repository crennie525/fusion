﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class cansend

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''ddsites control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddsites As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtsearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtsearch As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''tdwo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdwo As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdrwo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdrwo As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdywr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdywr As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''divywr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divywr As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''td1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents td1 As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''divret control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divret As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ifirst control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ifirst As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''iprev control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents iprev As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''lblpg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblpg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''inext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents inext As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''ilast control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ilast As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''tdgo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdgo As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdmode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdmode As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdservice control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdservice As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdservice2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdservice2 As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''iftd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents iftd As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''dgout control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dgout As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''dgouta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dgouta As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''lblret control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblret As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''txtpg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpg As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''txtpgcnt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpgcnt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblwo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblwo As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblMWNO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMWNO As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblpopstring control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblpopstring As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblerrcnt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblerrcnt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblisact control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblisact As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbllcnt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbllcnt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblserv1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblserv1 As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblserv2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblserv2 As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsmode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsmode As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsTransactionID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsTransactionID As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsConfigPosition control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsConfigPosition As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsEquipment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsEquipment As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsStructureType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsStructureType As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsService control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsService As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsResponsible control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsResponsible As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbliOperation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbliOperation As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsDescription control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsDescription As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsPlanningGroup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsPlanningGroup As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsPriority control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsPriority As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsApprovedBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsApprovedBy As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsReasonCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsReasonCode As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsStartDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsStartDate As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsFinishDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsFinishDate As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsTxt1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsTxt1 As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsTxt2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsTxt2 As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsErrorCode1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsErrorCode1 As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsErrorCode2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsErrorCode2 As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsErrorCode3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsErrorCode3 As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsComplaintType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsComplaintType As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbliRetroFlag control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbliRetroFlag As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsItemNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsItemNumber As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsLotNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsLotNumber As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbliPlannedQuantity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbliPlannedQuantity As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsFacility control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsFacility As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsRequestedStartDte control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsRequestedStartDte As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsRequestedFinishDte control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsRequestedFinishDte As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbliSetupTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbliSetupTime As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbliRuntime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbliRuntime As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbliPlannedNumWkrs control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbliPlannedNumWkrs As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsTextBlock control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsTextBlock As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblurl1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblurl1 As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblurl2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblurl2 As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblwoecd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblwoecd As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblwnums control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblwnums As Global.System.Web.UI.HtmlControls.HtmlInputHidden
End Class
