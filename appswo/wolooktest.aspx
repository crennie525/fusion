﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wolooktest.aspx.vb" Inherits="lucy_r12.wolooktest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <style type="text/css">
        .listbox 
        {
            border-bottom: 1px solid black;
            border-top: 1px solid black;
            border-left: 1px solid black;
            border-right: 1px solid black;
            background-color: White;
            overflow: auto;
            width: 150px;
            height: 300px;
            font-family:MS Sans Serif, arial, sans-serif, Verdana;
	font-size:12px;
	text-decoration:none;
	color:black;
	list-style-type: none;
 padding: 3px;
 margin: 10px;
 
        }
    .listitem 
    {
        list-style-type: none;
 padding: 0px;
 margin: 0px;
 font-family:MS Sans Serif, arial, sans-serif, Verdana;
	font-size:12px;
	text-decoration:none;
	color:black;
 
    }
    .listhigh 
    {
        cursor: pointer;
        background-color: #67BAF9;
        list-style-type: none;
 padding: 0px;
 margin: 0px;
 font-family:MS Sans Serif, arial, sans-serif, Verdana;
	font-size:12px;
	text-decoration:none;
	color:black;
 
    }
    </style>
    <script language="javascript" type="text/javascript">
        function checkent() {
            if (window.event) { e = window.event; }
            if (e.keyCode == 13) {
                alert("hello")
            } 

        
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Services>
        <asp:ServiceReference Path="eqauto.asmx" />
        </Services>
        </asp:ScriptManager>
    <div>
    
        <asp:TextBox ID="txteqauto" runat="server" CssClass="plainlabel"></asp:TextBox>

        <cc1:AutoCompleteExtender 
        ID="AutoCompleteExtender1" 
        TargetControlID="txteqauto"
        runat="server"
        ServicePath="eqauto.asmx" 
        ServiceMethod="geteq"
        MinimumPrefixLength="1" 
        CompletionInterval="1000"
        EnableCaching="true"
        CompletionSetCount= "12"
        CompletionListCssClass="listbox" 
        CompletionListItemCssClass="listitem" 
        CompletionListHighlightedItemCssClass="listhigh">
        </cc1:AutoCompleteExtender>

    </div>
    </form>
</body>
</html>
