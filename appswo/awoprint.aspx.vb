﻿Imports System.Data.SqlClient
Public Class awoprint
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim ap As New AppUtils
    Dim sql As String
    Dim mail As New Utilities
    Dim dr As SqlDataReader
    Dim wonum, pm, jp, typ As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            wonum = Request.QueryString("wonum").ToString '"1856" '
            
            mail.Open()
            getafter()
            mail.Dispose()

        End If
    End Sub
    Private Sub getafter()
        Dim desc, astart, sstart, tstart, wtyp, comp As String
        sql = "select actstart, schedstart, targstartdate, actfinish, worktype, description, pmid, jpid from workorder where wonum = '" & wonum & "'"
        dr = mail.GetRdrData(sql)
        While dr.Read
            desc = ""
            tstart = dr.Item("targstartdate").ToString
            sstart = dr.Item("schedstart").ToString
            astart = dr.Item("actstart").ToString
            wtyp = dr.Item("worktype").ToString
            comp = dr.Item("actfinish").ToString
            pm = dr.Item("pmid").ToString
            jp = dr.Item("jpid").ToString
        End While
        dr.Close()
        Dim sd, ld, lc, su, eq, dp, cl, lo, col, dt As String
        Dim sqty As Integer
        Dim esthr, estjphr, acthr, actjphr As Decimal
        Dim estcost, estjpcost, actcost, actjpcost As Decimal
        Dim estmatcost, estjpmatcost, actmatcost, actjpmatcost As Decimal
        Dim esttoolcost, estjptoolcost, acttoolcost, actjptoolcost As Decimal
        Dim estlubecost, estjplubecost, actlubecost, actjplubecost As Decimal
        Dim icost As String = ap.InvEntry()
        sql = "select w.description, l.longdesc, w.qty, w.estlabhrs, w.estlabcost, w.estmatcost, w.esttoolcost, w.estlubecost, isnull(w.actlabhrs,0) as actlabhrs, " _
        + "isnull(w.actlabcost,0) as actlabcost, " _
        + "isnull(w.actmatcost,0) as actmatcost, isnull(w.acttoolcost,0) as acttoolcost, isnull(w.actlubecost,0) as actlubecost, " _
         + "isnull(w.leadcraft, 'Not Provided') as 'leadcraft', isnull(w.supervisor, 'Not Provided') as 'supervisor', " _
         + "w.eqnum, d.dept_line, c.cell_name, l1.location, e.fpc, isnull(h.totaldown, '0') as totaldown " _
        + "from workorder w " _
        + "left join wolongdesc l on l.wonum = w.wonum " _
        + "left join dept d on d.dept_id = w.deptid " _
        + "left join cells c on c.cellid = d.dept_id " _
        + "left join equipment e on e.eqid = w.eqid " _
        + "left join eqhist h on h.wonum = w.wonum " _
        + "left join pmlocations l1 on l1.locid = w.locid where w.wonum = '" & wonum & "'"
        dr = mail.GetRdrData(sql)
        While dr.Read
            eq = dr.Item("eqnum").ToString
            dp = dr.Item("dept_line").ToString
            cl = dr.Item("cell_name").ToString
            lo = dr.Item("location").ToString
            col = dr.Item("fpc").ToString
            dt = dr.Item("totaldown").ToString

            sd = dr.Item("description").ToString
            ld = dr.Item("longdesc").ToString
            su = dr.Item("supervisor").ToString
            lc = dr.Item("leadcraft").ToString
            esthr = dr.Item("estlabhrs").ToString
            acthr = dr.Item("actlabhrs").ToString
            estcost = dr.Item("estlabcost").ToString
            actcost = dr.Item("actlabcost").ToString
            estmatcost = dr.Item("estmatcost").ToString
            actmatcost = dr.Item("actmatcost").ToString
            esttoolcost = dr.Item("esttoolcost").ToString
            acttoolcost = dr.Item("acttoolcost").ToString
            estlubecost = dr.Item("estlubecost").ToString
            actlubecost = dr.Item("actlubecost").ToString
            Try
                sqty = dr.Item("qty").ToString
            Catch ex As Exception
                sqty = 1
            End Try
        End While
        dr.Close()
        desc = sd & ld
        Dim body As String
        body = "<table width='800px' style='font-size:10pt; font-family:Verdana;'>"
        body &= "<tr><td colspan=""2"" class=""plainlabel"" align=""right""><a href=""#"" onclick=""printwo('" & wtyp & "','" & wonum & "','" & pm & "','" & jp & "');"">Print Standard Work Order</a></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2"" class=""bigbold1"" align=""center"">After Work Order Report</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2"" align=""center"">" & Now & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2"">&nbsp;</td></tr>" & vbCrLf & vbCrLf

        body &= "<tr><td colspan=""2""><b><u>Work Order Details</u></b></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td width=""120px"">" & tmod.getxlbl("xlb103", "pmmail.vb") & "</td><td width=""680px"">" & wonum & "</td></tr>" & vbCrLf
        body &= "<tr><td>" & tmod.getxlbl("xlb104", "pmmail.vb") & "</td><td>" & wtyp & "</td></tr>" & vbCrLf
        body &= "<tr><td>Equipment#</td><td>" & eq & "</td></tr>" & vbCrLf
        If lo <> "" Then
            body &= "<tr><td>Location</td><td>" & lo & "</td></tr>" & vbCrLf
        ElseIf dp <> "" Then
            If cl <> "" Then
                body &= "<tr><td>Department</td><td>" & dp & "</td></tr>" & vbCrLf
                body &= "<tr><td>Cell</td><td>" & cl & "</td></tr>" & vbCrLf
            Else
                body &= "<tr><td>Department</td><td>" & dp & "</td></tr>" & vbCrLf
            End If
        End If
        If col <> "" Then
            body &= "<tr><td>Column</td><td>" & col & "</td></tr>" & vbCrLf
        End If

        body &= "<tr><td colspan=""2"">&nbsp;</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2""><u>" & tmod.getxlbl("xlb105", "pmmail.vb") & "</u></td><tr><td colspan=""2"">" & desc & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2"">&nbsp;</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Target Start</td><td>" & tstart & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Scheduled Start</td><td>" & sstart & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Actual Start</td><td>" & astart & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Actual Complete</td><td>" & comp & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
        body &= "<tr><td>Supervisor</td><td>" & su & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Lead Craft</td><td>" & lc & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2"">&nbsp;</td></tr>" & vbCrLf & vbCrLf

        

        Dim comcnt, rootcnt, failcnt As Integer
        sql = "select count(*) from wonotes where wonum = '" & wonum & "'"
        Try
            comcnt = mail.Scalar(sql)
        Catch ex As Exception
            comcnt = 0
        End Try
        sql = "select count(*) from worootcause where wonum = '" & wonum & "'"
        Try
            rootcnt = mail.Scalar(sql)
        Catch ex As Exception
            rootcnt = 0
        End Try
        'Downtime
        body &= "<tr><td>Down Time</td><td>" & dt & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2"">&nbsp;</td></tr>" & vbCrLf & vbCrLf

        Dim fcnt As Integer = 0
        Dim mcnt As Integer = 0
        Dim fu, co, fm, pr, cr, cw As String
        Dim ty, ms, hi, low, sp, m2, munder, mover, mw As String
        If wtyp = "PM" Then
            sql = "select f.pmhid, f1.func, c.compnum, f.failuremode, isnull(f.problem, 'Not Provided') as 'problem', " _
                + "isnull(f.corraction, 'Not Provided') as 'corraction', f.wonum " _
                + "from pmfailhist f " _
                + "left join pmhist h on h.pmid = f.pmid " _
                + "left join components c on c.comid = f.comid " _
                + "left join functions f1 on f1.func_id = c.func_id " _
                + "where h.wonum = '" & wonum & "'"
            dr = mail.GetRdrData(sql)
            While dr.Read
                fcnt += 1
                fu = dr.Item("func").ToString
                co = dr.Item("compnum").ToString
                fm = dr.Item("failuremode").ToString
                pr = dr.Item("problem").ToString
                cr = dr.Item("corraction").ToString
                cw = dr.Item("wonum").ToString
                If fcnt = 1 Then
                    body &= "<tr><td colspan=""2""><b><u>Failures</u></b></td></tr>" & vbCrLf & vbCrLf

                    body &= "<tr><td colspan=""2""><table>" & vbCrLf & vbCrLf

                    body &= "<tr><td width=""100px""><u>Function</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""120px""><u>Component</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""100px""><u>Failure Mode</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""150px""><u>Problem</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""150px""><u>Corrective Action</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""100px""><u>Work Order#</u></td></tr>" & vbCrLf & vbCrLf
                End If
                body &= "<tr><td width=""100px"" valign=""top"">" & fu & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""120px"" valign=""top"">" & co & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""100px"" valign=""top"">" & fm & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""150px"" valign=""top"">" & pr & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""150px"" valign=""top"">" & cr & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""100px"" valign=""top"">" & cw & "</td></tr>" & vbCrLf & vbCrLf
            End While
            dr.Close()
            If fcnt = 0 Then
                body &= "<tr><td colspan=""2"">No Failures Reported</td></tr>" & vbCrLf & vbCrLf
                body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
            Else
                body &= "</table></td></tr>" & vbCrLf & vbCrLf
                body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
            End If

            sql = "select f.pmhid, f1.func, c.compnum, f.[type], f.measure, " _
                + "f.hii, f.loi, f.speci, f.measurement2, isnull(f.mover, '0') as 'mover', isnull(f.munder, '0') as 'munder'," _
                + "isnull(f.problem, 'Not Provided') as 'problem', isnull(f.corraction, 'Not Provided') as 'corraction',  " _
                + "f.wonum " _
                + "from pmTaskMeasDetManHIST f " _
                + "left join PMTrackArch a on a.pmhid = f.pmhid " _
                + "left join pmhist h on h.pmhid = f.pmhid " _
                + "left join components c on c.comid = a.comid  " _
                + "left join functions f1 on f1.func_id = c.func_id " _
                + "where h.wonum = '" & wonum & "' and ((isnull(mover, 0) <> 0 or isnull(munder, 0) <> 0) or f.wonum is not null)"
            dr = mail.GetRdrData(sql)
            While dr.Read
                mcnt += 1
                fu = dr.Item("func").ToString
                co = dr.Item("compnum").ToString
                ty = dr.Item("type").ToString
                ms = dr.Item("measure").ToString
                hi = dr.Item("hii").ToString
                low = dr.Item("loi").ToString
                sp = dr.Item("speci").ToString
                m2 = dr.Item("measurement2").ToString
                munder = dr.Item("munder").ToString
                mover = dr.Item("mover").ToString
                mw = dr.Item("wonum").ToString
                If mcnt = 1 Then
                    body &= "<tr><td colspan=""2""><b><u>Measurement Failures</u></b></td></tr>" & vbCrLf & vbCrLf

                    body &= "<tr><td colspan=""2""><table>" & vbCrLf & vbCrLf

                    body &= "<tr><td width=""100px""><u>Function</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""120px""><u>Component</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""80px""><u>Type</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""80px""><u>Measure</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""40px""><u>Hi</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""40px""><u>Lo</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""40px""><u>Spec</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""80px""><u>Measurement</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""100px""><u>Work Order#</u></td></tr>" & vbCrLf & vbCrLf
                End If
                body &= "<tr><td valign=""top"">" & fu & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & co & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & ty & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & ms & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & hi & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & low & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & sp & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & m2 & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & munder & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & mover & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & mw & "</td></tr>" & vbCrLf & vbCrLf
            End While
            dr.Close()
            If fcnt = 0 Then
                body &= "<tr><td colspan=""2"">No Measurement Failures Reported</td></tr>" & vbCrLf & vbCrLf
                body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
            Else
                body &= "</table></td></tr>" & vbCrLf & vbCrLf
                body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
            End If
        ElseIf wtyp = "TPM" Then
            fcnt = 0
            mcnt = 0
            sql = "select f.pmhid, f1.func, c.compnum, f.failuremode, isnull(f.problem, 'Not Provided') as 'problem', " _
                + "isnull(f.corraction, 'Not Provided') as 'corraction', f.wonum " _
                + "from pmfailhisttpm f " _
                + "left join pmhisttpm h on h.pmhid = f.pmhid " _
                + "left join components c on c.comid = f.comid " _
                + "left join functions f1 on f1.func_id = c.func_id " _
                + "where h.wonum = '" & wonum & "'"
            dr = mail.GetRdrData(sql)
            While dr.Read
                fcnt += 1
                fu = dr.Item("func").ToString
                co = dr.Item("compnum").ToString
                fm = dr.Item("failuremode").ToString
                pr = dr.Item("problem").ToString
                cr = dr.Item("corraction").ToString
                cw = dr.Item("wonum").ToString
                If fcnt = 1 Then
                    body &= "<tr><td colspan=""2""><b><u>Failures</u></b></td></tr>" & vbCrLf & vbCrLf

                    body &= "<tr><td colspan=""2""><table>" & vbCrLf & vbCrLf

                    body &= "<tr><td width=""100px""><u>Function</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""120px""><u>Component</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""100px""><u>Failure Mode</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""150px""><u>Problem</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""150px""><u>Corrective Action</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""100px""><u>Work Order#</u></td></tr>" & vbCrLf & vbCrLf
                End If
                body &= "<tr><td width=""100px"" valign=""top"">" & fu & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""120px"" valign=""top"">" & co & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""100px"" valign=""top"">" & fm & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""150px"" valign=""top"">" & pr & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""150px"" valign=""top"">" & cr & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""100px"" valign=""top"">" & cw & "</td></tr>" & vbCrLf & vbCrLf
            End While
            dr.Close()
            If fcnt = 0 Then
                body &= "<tr><td colspan=""2"">No Failures Reported</td></tr>" & vbCrLf & vbCrLf
                body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
            Else
                body &= "</table></td></tr>" & vbCrLf & vbCrLf
                body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
            End If

            sql = "select f.pmhid, f1.func, c.compnum, f.[type], f.measure, " _
                + "f.hi, f.lo, f.spec, f.measurement, isnull(f.mover, '0') as 'mover', isnull(f.munder, '0') as 'munder'," _
                + "isnull(f.problem, 'Not Provided') as 'problem', isnull(f.corraction, 'Not Provided') as 'corraction',  " _
                + "f.wonum " _
                + "from pmTaskMeasDetManHISTtpm f " _
                + "left join PMTrackArchtpm a on a.pmhid = f.pmhid " _
                + "left join pmhisttpm h on h.pmhid = f.pmhid " _
                + "left join components c on c.comid = a.comid  " _
                + "left join functions f1 on f1.func_id = c.func_id " _
                + "where h.wonum = '" & wonum & "' and ((isnull(mover, 0) <> 0 or isnull(munder, 0) <> 0) or f.wonum is not null)"
            dr = mail.GetRdrData(sql)
            While dr.Read
                mcnt += 1
                fu = dr.Item("func").ToString
                co = dr.Item("compnum").ToString
                ty = dr.Item("type").ToString
                ms = dr.Item("measure").ToString
                hi = dr.Item("hi").ToString
                low = dr.Item("lo").ToString
                sp = dr.Item("speci").ToString
                m2 = dr.Item("measurement2").ToString
                munder = dr.Item("munder").ToString
                mover = dr.Item("mover").ToString
                mw = dr.Item("wonum").ToString
                If mcnt = 1 Then
                    body &= "<tr><td colspan=""2""><b><u>Measurement Failures</u></b></td></tr>" & vbCrLf & vbCrLf

                    body &= "<tr><td colspan=""2""><table>" & vbCrLf & vbCrLf

                    body &= "<tr><td width=""100px""><u>Function</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""120px""><u>Component</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""80px""><u>Type</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""80px""><u>Measure</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""40px""><u>Hi</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""40px""><u>Lo</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""40px""><u>Spec</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""80px""><u>Measurement</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""100px""><u>Work Order#</u></td></tr>" & vbCrLf & vbCrLf
                End If
                body &= "<tr><td valign=""top"">" & fu & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & co & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & ty & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & ms & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & hi & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & low & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & sp & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & m2 & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & munder & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & mover & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & mw & "</td></tr>" & vbCrLf & vbCrLf
            End While
            dr.Close()
            If fcnt = 0 Then
                body &= "<tr><td colspan=""2"">No Measurement Failures Reported</td></tr>" & vbCrLf & vbCrLf
                body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
            Else
                body &= "</table></td></tr>" & vbCrLf & vbCrLf
                body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
            End If
        Else

        End If

        Dim rootid, comments, enterdate, user, moddate, modby As String
        If comcnt > 0 Then
            sql = "select * from wonotes where wonum = '" & wonum & "'"
            dr = mail.GetRdrData(sql)
            While dr.Read
                rootid = dr.Item("noteid").ToString
                comments = dr.Item("note").ToString
                enterdate = dr.Item("notedate").ToString
                user = dr.Item("noteby").ToString
                moddate = dr.Item("modifieddate").ToString
                modby = dr.Item("modifiedby").ToString
            End While
            dr.Close()
            body &= "<tr><td colspan=""2""><b><u>Work Order Notes</u></b></td></tr>" & vbCrLf & vbCrLf
            body &= "<tr><td>Entered By</td><td>" & user & "</td></tr>" & vbCrLf
            body &= "<tr><td>Enter Date</td><td>" & enterdate & "</td></tr>" & vbCrLf
            body &= "<tr><td>Modified By</td><td>" & modby & "</td></tr>" & vbCrLf
            body &= "<tr><td>Modified Date</td><td>" & moddate & "</td></tr>" & vbCrLf
            body &= "<tr><td colspan=""2"">" & comments & "</td><td></tr>" & vbCrLf
            body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
        Else
            body &= "<tr><td colspan=""2"">No Work Order Notes Provided</td></tr>" & vbCrLf & vbCrLf
            body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
        End If
        Dim rootcause, corraction As String
        If rootcnt > 0 Then
            sql = "select * from worootcause where wonum = '" & wonum & "'"
            dr = mail.GetRdrData(sql)
            While dr.Read
                rootid = dr.Item("rootid").ToString
                rootcause = dr.Item("rootcause").ToString
                corraction = dr.Item("corraction").ToString
                comments = dr.Item("comments").ToString
                enterdate = dr.Item("enterdate").ToString
                user = dr.Item("enterby").ToString
                moddate = dr.Item("modifieddate").ToString
                modby = dr.Item("modifiedby").ToString
            End While
            dr.Close()
            body &= "<tr><td colspan=""2""><b><u>Root Cause Details</u></b></td></tr>" & vbCrLf & vbCrLf
            body &= "<tr><td>Entered By</td><td>" & user & "</td></tr>" & vbCrLf
            body &= "<tr><td>Enter Date</td><td>" & enterdate & "</td></tr>" & vbCrLf
            body &= "<tr><td>Modified By</td><td>" & modby & "</td></tr>" & vbCrLf
            body &= "<tr><td>Modified Date</td><td>" & moddate & "</td></tr>" & vbCrLf
            body &= "<tr><td>Root Cause</td><td>" & rootcause & "</td></tr>" & vbCrLf
            body &= "<tr><td colspan=""2""><u>Corrective Action</u></td></tr>" & vbCrLf & vbCrLf
            body &= "<tr><td colspan=""2"">" & corraction & "</td><td></tr>" & vbCrLf
            body &= "<tr><td colspan=""2""><u>Comments</u></td></tr>" & vbCrLf & vbCrLf
            body &= "<tr><td colspan=""2"">" & comments & "</td><td></tr>" & vbCrLf
            body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
        Else
            body &= "<tr><td colspan=""2"">No Root Cause Provided</td></tr>" & vbCrLf & vbCrLf
            body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
        End If

        'Costs
        body &= "<tr><td colspan=""2""><u>Work Order Costs</u></td></tr>" & vbCrLf & vbCrLf
        'body &= "<tr><td colspan=""2"">&nbsp;</td></tr>" & vbCrLf & vbCrLf

        body &= "<tr><td colspan=""2""><table>" & vbCrLf & vbCrLf

        body &= "<tr><td width=""120px""><u>Item</u></td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px""><u>Estimated</u></td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px""><u>Actual</u></td></tr>" & vbCrLf & vbCrLf
        If esthr > 0 Or acthr > 0 Then
            sqty = 1
        End If
        esthr = esthr * sqty
        acthr = acthr * sqty
        body &= "<tr><td width=""120px"">Labor Hours</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(esthr, "##############0.00") & "</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(acthr, "##############0.00") & "</td></tr>" & vbCrLf & vbCrLf

        estcost = estcost * sqty
        actcost = actcost * sqty
        body &= "<tr><td width=""120px"">Labor Costs</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(estcost, "##############0.00") & "</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(actcost, "##############0.00") & "</td></tr>" & vbCrLf & vbCrLf


        body &= "<tr><td width=""120px"">Parts</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(estmatcost, "##############0.00") & "</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(actmatcost, "##############0.00") & "</td></tr>" & vbCrLf & vbCrLf

        body &= "<tr><td width=""120px"">Tools</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(esttoolcost, "##############0.00") & "</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(acttoolcost, "##############0.00") & "</td></tr>" & vbCrLf & vbCrLf

        body &= "<tr><td width=""120px"">Lubricants</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(estlubecost, "##############0.00") & "</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(actlubecost, "##############0.00") & "</td></tr>" & vbCrLf & vbCrLf

        body &= "</table></td></tr>" & vbCrLf & vbCrLf

        body &= "<tr><td colspan=""2""><br><u>Labor Entries</u></td></tr>" & vbCrLf & vbCrLf
        sql = "select name, Convert(char(10),transdate,101) as 'transdate', minutes_man from wolabtrans where wonum = '" & wonum & "'"

        body &= "<tr><td colspan=""2""><table>" & vbCrLf & vbCrLf

        body &= "<tr><td width=""160px""><u>Name</u></td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px""><u>Date</u></td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px""><u>Minutes</u></td></tr>" & vbCrLf & vbCrLf

        dr = mail.GetRdrData(sql)
        While dr.Read
            body &= "<tr><td>" & dr.Item("name").ToString & "</td>"
            body &= "<td>" & dr.Item("transdate").ToString & "</td>"
            body &= "<td>" & dr.Item("minutes_man").ToString & "</td></tr>"
        End While
        dr.Close()

        body &= "</table></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
        'End Costs

        body &= "</table>"
        dvawo.InnerHtml = body
    End Sub
End Class