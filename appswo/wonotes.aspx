﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wonotes.aspx.vb" Inherits="lucy_r12.wonotes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script type="text/javascript" language="javascript">
    <!--
        function checkvals() {
            
            var co = document.getElementById("txtcorraction").value;
            if (co == "") {
                alert("Work Order Notes Required")
            }
            else {
                document.getElementById("lblsubmit").value = "go";
                document.getElementById("form1").submit();
            }
        }
        function checkdat() {
            //var id = document.getElementById("lblrootid").value;
            //alert(id)
            //window.parent.handleroot(id);
            var nid = document.getElementById("lblnoteid").value;
            //alert(id)
            window.parent.handlenote(nid);
        }
    //-->
    </script>
</head>
<body onload="checkdat();">
    <form id="form1" runat="server">
    <table width="620px">
    
    <tr>
    <td colspan="2" class="label" height="22px" id="tdwon" runat="server">Work Order Notes</td>
    <td>&nbsp;</td>
    <td class="label" width="100px" id="tdeb" runat="server">Entered By:</td>
   <td class="plainlabel" width="175px" id="tdenterby" runat="server"></td>
   
    </tr>
    <tr>

    <td colspan="2" rowspan="6" class="plainlabel"><asp:TextBox ID="txtcorraction" runat="server" Width="340" Height="140" TextMode="MultiLine"></asp:TextBox></td>
    <td>&nbsp;</td>
    <td class="label" id="tded" runat="server">Enter Date:</td>
   <td class="plainlabel" id="tdenterdate" runat="server"></td>
    
    </tr>
    <tr>
    <td>&nbsp;</td>
   <td class="label" height="22" id="tdmb" runat="server">Modified By:</td>
   <td class="plainlabel" id="tdmodby" runat="server"></td>
    </tr>
    <tr>
    <td>&nbsp;</td>
   <td class="label" height="22" id="tdmd" runat="server">Modified Date:</td>
   <td class="plainlabel" id="tdmoddate" runat="server"></td>
    </tr>
    <tr>
    <td colspan="5" height="22px">&nbsp;</td>
    </tr>
    <tr>
    <td colspan="5" height="22px">&nbsp;</td>
    </tr>
    <tr>
    <td colspan="5" height="22px">&nbsp;</td>
    </tr>
    
    <tr>
    <td colspan="2" align="right"><img id="imgsav" onclick="checkvals();" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                runat="server" alt="" /></td>
    </tr>
    
    </table>
    <input type="hidden" id="lblrootid" runat="server" />
    <input type="hidden" id="lblorootid" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblwo" runat="server" />
    <input type="hidden" id="lblcuser" runat="server" />
    <input type="hidden" id="lblcuserid" runat="server" />
    <input type="hidden" id="lbluser" runat="server" />
    <input type="hidden" id="lbluserid" runat="server" />
    <input type="hidden" id="lblenterdate" runat="server" />
    <input type="hidden" id="lblmodbyid" runat="server" />
    <input type="hidden" id="lblmodby" runat="server" />
    <input type="hidden" id="lblmoddate" runat="server" />
    <input type="hidden" id="lblnoteid" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />

    </form>
</body>
</html>
