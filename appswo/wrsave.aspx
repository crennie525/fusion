﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wrsave.aspx.vb" Inherits="lucy_r12.wrsave" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
    <!--
        function checkit() {
            var err = document.getElementById("lblerr").value;
            window.returnValue = err;
            window.close();
        }
        //-->
    </script>
</head>
<body onload="checkit();">
    <form id="form1" runat="server">
    <div>
        <table align="center" height="100%" width="100%" id="Table1">
            <tr>
                <td class="bluelabel" align="center" valign="middle" height="100%">
                    Saving Data...
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="lblerr" runat="server">
    </form>
</body>
</html>
