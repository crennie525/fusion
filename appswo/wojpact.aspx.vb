

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wojpact
    Inherits System.Web.UI.Page
	Protected WithEvents lang1418 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1417 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1416 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1415 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1414 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1413 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1412 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1411 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1410 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1409 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1408 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1407 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1406 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1405 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1404 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1403 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim comp As New Utilities
    Dim pmid As String
    Dim ds As DataSet
    Dim wonum, jpid, stat As String
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrow As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dltasks As System.Web.UI.WebControls.DataList
    Protected WithEvents tdjpn As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdjpd As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            wonum = Request.QueryString("wo").ToString
            jpid = Request.QueryString("jpid").ToString
            stat = Request.QueryString("stat").ToString
            lblwo.Value = wonum
            lbljpid.Value = jpid
            lblstat.Value = stat
            comp.Open()
            GetJPHead(jpid, wonum)
            PopDL(jpid, wonum)
            comp.Dispose()
        End If
    End Sub
    Private Sub GetJPHead(ByVal jpid As String, ByVal wonum As String)
        sql = "select jpnum, description from wojobplans where wonum = '" & wonum & "' and jpid = '" & jpid & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdjpn.InnerHtml = dr.Item("jpnum").ToString
            tdjpd.InnerHtml = dr.Item("description").ToString
        End While
        dr.Close()
    End Sub
    Private Sub PopDL(ByVal jpid As String, ByVal wonum As String)
        sql = "usp_getwojplab '" & jpid & "','" & wonum & "'"
        ds = comp.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        'Try
        dltasks.DataSource = dv
        dltasks.DataBind()
    End Sub

    Private Sub dltasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dltasks.EditCommand
        lblrow.Value = CType(e.Item.FindControl("A1"), Label).Text 'e.Item.ItemIndex

        comp.Open()
        jpid = lbljpid.Value
        wonum = lblwo.Value
        dltasks.EditItemIndex = e.Item.ItemIndex

        PopDL(jpid, wonum)
        comp.Dispose()
    End Sub

    Private Sub dltasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dltasks.CancelCommand
        dltasks.EditItemIndex = -1
        comp.Open()
        wonum = lblwo.Value
        jpid = lbljpid.Value
        PopDL(jpid, wonum)
        comp.Dispose()
    End Sub

    Private Sub dltasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dltasks.UpdateCommand
        Dim ret, typ, fail, pass, task, atime, lc As String
        Dim tsk, cnt, adj As String
        wonum = lblwo.Value
        jpid = lbljpid.Value
        Dim pmtskid As String = lblrow.Value
        
        atime = CType(e.Item.FindControl("txtatime"), TextBox).Text
        lc = CType(e.Item.FindControl("txtalead"), TextBox).Text
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToDecimal(atime)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr569" , "wojpact.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        sql = "usp_upjplab '" & lc & "','" & atime & "', '" & pmtskid & "', '" & jpid & "', '" & wonum & "'"

        comp.Open()
        comp.Update(sql)
        wonum = lblwo.Value
        jpid = lbljpid.Value
        dltasks.EditItemIndex = -1
        PopDL(jpid, wonum)
        comp.Dispose()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1403.Text = axlabs.GetASPXPage("wojpact.aspx", "lang1403")
        Catch ex As Exception
        End Try
        Try
            lang1404.Text = axlabs.GetASPXPage("wojpact.aspx", "lang1404")
        Catch ex As Exception
        End Try
        Try
            lang1405.Text = axlabs.GetASPXPage("wojpact.aspx", "lang1405")
        Catch ex As Exception
        End Try
        Try
            lang1406.Text = axlabs.GetASPXPage("wojpact.aspx", "lang1406")
        Catch ex As Exception
        End Try
        Try
            lang1407.Text = axlabs.GetASPXPage("wojpact.aspx", "lang1407")
        Catch ex As Exception
        End Try
        Try
            lang1408.Text = axlabs.GetASPXPage("wojpact.aspx", "lang1408")
        Catch ex As Exception
        End Try
        Try
            lang1409.Text = axlabs.GetASPXPage("wojpact.aspx", "lang1409")
        Catch ex As Exception
        End Try
        Try
            lang1410.Text = axlabs.GetASPXPage("wojpact.aspx", "lang1410")
        Catch ex As Exception
        End Try
        Try
            lang1411.Text = axlabs.GetASPXPage("wojpact.aspx", "lang1411")
        Catch ex As Exception
        End Try
        Try
            lang1412.Text = axlabs.GetASPXPage("wojpact.aspx", "lang1412")
        Catch ex As Exception
        End Try
        Try
            lang1413.Text = axlabs.GetASPXPage("wojpact.aspx", "lang1413")
        Catch ex As Exception
        End Try
        Try
            lang1414.Text = axlabs.GetASPXPage("wojpact.aspx", "lang1414")
        Catch ex As Exception
        End Try
        Try
            lang1415.Text = axlabs.GetASPXPage("wojpact.aspx", "lang1415")
        Catch ex As Exception
        End Try
        Try
            lang1416.Text = axlabs.GetASPXPage("wojpact.aspx", "lang1416")
        Catch ex As Exception
        End Try
        Try
            lang1417.Text = axlabs.GetASPXPage("wojpact.aspx", "lang1417")
        Catch ex As Exception
        End Try
        Try
            lang1418.Text = axlabs.GetASPXPage("wojpact.aspx", "lang1418")
        Catch ex As Exception
        End Try

    End Sub

End Class
