

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class wocomp
    Inherits System.Web.UI.Page
	Protected WithEvents lang1356 As System.Web.UI.WebControls.Label

    Protected WithEvents imgcomp As System.Web.UI.HtmlControls.HtmlImage
	Protected WithEvents lang1355 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1354 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim won As New Utilities
    Dim wonum, pmnum, wotype, jpnum, psite, jpid As String
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txts As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtf As System.Web.UI.WebControls.TextBox
    Protected WithEvents cball As System.Web.UI.WebControls.CheckBox
    Protected WithEvents tdstart As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdactstart As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdactfinish As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdjp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblfmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmeasstring As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load



        GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            wonum = Request.QueryString("wo").ToString
            lblwo.Value = wonum
            wotype = "JP" 'Request.QueryString("typ").ToString
            lbltyp.Value = wotype
            won.Open()
            GetWO()
            'GetJP()
            won.Dispose()

        End If
    End Sub
    Private Sub GetJP()
        Dim sb As New System.Text.StringBuilder
        wonum = lblwo.Value
        sql = "select jpid from workorder where wonum = '" & wonum & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            jpid = dr.Item("jpid").ToString
        End While
        dr.Close()
        If jpid <> "" Then
            sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""720"">")
            sb.Append("<tr><td colspan=""2"" class=""thdrsingg label"">" & tmod.getlbl("cdlbl163" , "wocomp.aspx.vb") & "</td></tr>")
            sb.Append("<tr><td width=""40""></td><td width=""680""></td></tr>")
            sb.Append("<tr><td class=""label""><u>Task#</u></td><td class=""label""><u>Description</u></td></tr>")
        End If
        Dim pmtskid, tnum, tdesc, skill, qty, ttime As String
        If jpid <> "" Then
            sql = "select pmtskid, tasknum, taskdesc, skill, qty, ttime from pmjobtasks where jpid = '" & jpid & "'"
            Dim ds As New DataSet
            Dim dt As New DataTable
            ds = won.GetDSData(sql)
            dt = New DataTable
            dt = ds.Tables(0)
            Dim row As DataRow
            For Each row In dt.Rows
                pmtskid = row("pmtskid").ToString
                tnum = row("tasknum").ToString
                tdesc = row("taskdesc").ToString
                sb.Append("<tr><td class=""plainlabel"">" & tnum & "</td>")
                sb.Append("<td class=""plainlabel"">" & tdesc & "</td></tr>")

                sb.Append("<tr><td colspan=""2""><table>")
                sb.Append("<tr><td class=""plainlabel"">Skill:&nbsp;&nbsp;" & skill & "&nbsp;&nbsp;Qty:&nbsp;&nbsp;" & qty & "&nbsp;&nbsp;Time:&nbsp;&nbsp;" & ttime & "&nbsp;&nbsp;</td>")
                sb.Append("<td class=""plainlabel"">" & tmod.getlbl("cdlbl164" , "wocomp.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel""><input type=""text"" id=""t-" & pmtskid & """></td></tr>")
            Next
            sb.Append("<tr><td colspan=""2""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
            sb.Append("</table>")
            tdjp.InnerHtml = sb.ToString
        End If
    End Sub
    Private Sub GetWO()
        wonum = lblwo.Value
        sql = "select targstartdate, actstart, actfinish from workorder where wonum = '" & wonum & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            tdstart.InnerHtml = dr.Item("targstartdate").ToString
            txts.Text = dr.Item("actstart").ToString
            txtf.Text = dr.Item("actfinish").ToString
        End While
        dr.Close()
        Dim sb As New System.Text.StringBuilder
        Dim fm, fmid, fmstr As String
        Dim start As Integer = 0
        Dim rcnt As Integer = 0
        sql = "select distinct wofailid, failuremode from wofailmodes1 where wonum = '" & wonum & "' order by failuremode"
        dr = won.GetRdrData(sql)

        While dr.Read
            If start = 0 Then
                start = 1
                sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""720"">")
                sb.Append("<tr><td colspan=""2"" class=""thdrsingg label"">" & tmod.getlbl("cdlbl165" , "wocomp.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td width=""120""></td><td width=""600""></td></tr>")
                sb.Append("<tr><td colspan=""2"" class=""plainlabelblue"">")
                sb.Append("Note that a Failure Mode Status of ""Fail"" is assumed if included in a Work Order</td></tr>")
                sb.Append("<tr><td colspan=""2""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
                sb.Append("<tr><td colspan=""2"" class=""plainlabelblue"">")
                sb.Append("To aid in future Optimization efforts Please Do Not Change a Failure Mode Status to ""Pass"" Unless ")
                sb.Append("the Indicated Failure Condition was Not Evident when Performing this Work Order</td></tr>")
                sb.Append("<tr><td colspan=""2""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
            End If
            fm = dr.Item("failuremode").ToString
            fmid = dr.Item("wofailid").ToString
            sb.Append("<tr><td class=""plainlabel"">" & fm & "</td>")
            sb.Append("<td class=""plainlabel""><input type=""radio"" id=""" & fmid & "-pass"" name=""f-" & fmid & """ ")
            sb.Append(" onclick=""checkfm('pass', this.name)"">Pass&nbsp;")
            sb.Append("<input type=""radio"" id=""" & fmid & "-fail"" name=""f-" & fmid & """ onclick=""checkfm('fail', this.name)"" checked>Fail</td></tr>")
            If fmstr = "" Then
                fmstr = fmid & "-pass"
            Else
                fmstr += "," & fmid & "-pass"
            End If
        End While
        sb.Append("<tr><td colspan=""2""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
        sb.Append("</table>")
        dr.Close()
        lblfmstr.Value = fmstr
        start = 0

        sql = "select distinct tmdid, measurement from pmtaskmeasdetails where wonum = '" & wonum & "'"
        dr = won.GetRdrData(sql)
        Dim tmdid, meas, mstr As String
        While dr.Read
            If start = 0 Then
                start = 1
                sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""720"">")
                sb.Append("<tr><td colspan=""2"" class=""thdrsingg label"">" & tmod.getlbl("cdlbl166" , "wocomp.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td width=""150""></td><td width=""630""></td></tr>")

            End If
            meas = dr.Item("measurement").ToString
            tmdid = dr.Item("tmdid").ToString
            meas = Replace(meas, "(___)", "")
            meas = Replace(meas, ";", "")
            sb.Append("<tr><td class=""plainlabel"">" & meas & "</td>")
            sb.Append("<td class=""plainlabel""><input type=""text"" id=""m-" & tmdid & """></td></tr>")
            If mstr = "" Then
                mstr = tmdid & "-nr"
            Else
                mstr += "," & tmdid & "-nr"
            End If
        End While
        sb.Append("<tr><td colspan=""2""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
        sb.Append("</table>")
        dr.Close()
        lblfmstr.Value = fmstr
        start = 0


        sql = "select distinct wopartid, itemnum, description from woparts where wonum = '" & wonum & "'"
        dr = won.GetRdrData(sql)
        Dim wpid, pnum, pdesc, pstr As String
        While dr.Read
            If start = 0 Then
                start = 1
                sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""720"">")
                sb.Append("<td colspan=""3"" class=""thdrsingg label"">" & tmod.getlbl("cdlbl167" , "wocomp.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td width=""120""></td><td width=""200""></td><td width=""400""></td></tr>")
            End If
            wpid = dr.Item("wopartid").ToString
            pnum = dr.Item("itemnum").ToString
            pdesc = dr.Item("description").ToString
            sb.Append("<tr><td class=""plainlabel"">" & pnum & "</td>")
            sb.Append("<td class=""plainlabel"">" & pdesc & "</td>")
            sb.Append("<td class=""plainlabel""><input type=""radio"" id=""" & wpid & "-yes"" name=""p-" & wpid & """ ")
            sb.Append("checked onclick=""checkwp('yes', this.name)"">Part Used&nbsp;")
            sb.Append("<input type=""radio"" id=""" & wpid & "-no"" name=""p-" & wpid & """ onclick=""checkwp('no', this.name)"">Part Not Used</td></tr>")
            If pstr = "" Then
                pstr = wpid & "-yes"
            Else
                pstr += "," & wpid & "-yes"
            End If
        End While
        sb.Append("<tr><td colspan=""3""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
        sb.Append("</table>")
        dr.Close()
        lblpstr.Value = pstr
        start = 0

        sql = "select distinct wotoolid, toolnum, description from wotools where wonum = '" & wonum & "'"
        dr = won.GetRdrData(sql)
        Dim wtid, tnum, tdesc, tstr As String
        While dr.Read
            If start = 0 Then
                start = 1
                sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""720"">")
                sb.Append("<td colspan=""3"" class=""thdrsingg label"">" & tmod.getlbl("cdlbl168" , "wocomp.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td width=""120""></td><td width=""200""></td><td width=""400""></td></tr>")
            End If
            wtid = dr.Item("wotoolid").ToString
            tnum = dr.Item("toolnum").ToString
            tdesc = dr.Item("description").ToString
            sb.Append("<tr><td class=""plainlabel"">" & tnum & "</td>")
            sb.Append("<td class=""plainlabel"">" & tdesc & "</td>")
            sb.Append("<td class=""plainlabel""><input type=""radio"" id=""" & wtid & "-yes"" name=""t-" & wtid & """ ")
            sb.Append("checked onclick=""checkwt('yes', this.name)"">Tool Used&nbsp;")
            sb.Append("<input type=""radio"" id=""" & wtid & "-no"" name=""t-" & wtid & """ onclick=""checkwt('no', this.name)"">Tool Not Used</td></tr>")
            If tstr = "" Then
                tstr = wtid & "-yes"
            Else
                tstr += "," & wtid & "-yes"
            End If
        End While
        sb.Append("<tr><td colspan=""3""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
        sb.Append("</table>")
        dr.Close()
        lbltstr.Value = tstr
        start = 0

        sql = "select distinct wolubeid, lubenum, description from wolubes where wonum = '" & wonum & "'"
        dr = won.GetRdrData(sql)
        Dim wlid, lnum, ldesc, lstr As String
        While dr.Read
            If start = 0 Then
                start = 1
                sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""720"">")
                sb.Append("<td colspan=""3"" class=""thdrsingg label"">" & tmod.getlbl("cdlbl169" , "wocomp.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td width=""120""></td><td width=""200""></td><td width=""400""></td></tr>")
            End If
            wlid = dr.Item("wolubeid").ToString
            lnum = dr.Item("lubenum").ToString
            ldesc = dr.Item("description").ToString
            sb.Append("<tr><td class=""plainlabel"">" & lnum & "</td>")
            sb.Append("<td class=""plainlabel"">" & ldesc & "</td>")
            sb.Append("<td class=""plainlabel""><input type=""radio"" id=""" & wlid & "-yes"" name=""l-" & wlid & """ ")
            sb.Append("checked onclick=""checkwl('yes', this.name)"">Lube Used&nbsp;")
            sb.Append("<input type=""radio"" id=""" & wlid & "-no"" name=""l-" & wlid & """ onclick=""checkwl('no', this.name)"">Lube Not Used</td></tr>")
            If lstr = "" Then
                lstr = wlid & "-yes"
            Else
                lstr += "," & wlid & "-yes"
            End If
        End While
        sb.Append("<tr><td colspan=""3""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
        sb.Append("</table>")
        dr.Close()
        lbllstr.Value = lstr
        start = 0

        tdwo.InnerHtml = sb.ToString


    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1354.Text = axlabs.GetASPXPage("wocomp.aspx", "lang1354")
        Catch ex As Exception
        End Try
        Try
            lang1355.Text = axlabs.GetASPXPage("wocomp.aspx", "lang1355")
        Catch ex As Exception
        End Try
        Try
            lang1356.Text = axlabs.GetASPXPage("wocomp.aspx", "lang1356")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                imgcomp.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                imgcomp.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                imgcomp.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                imgcomp.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "spa" Then
                imgcomp.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
