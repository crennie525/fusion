﻿Imports System.Data.SqlClient
Imports System.Text
Public Class womanfsns
    Inherits System.Web.UI.Page
    'Dim nsws As New nisscan_te
    Dim tmod As New transmod
    Dim mu As New mmenu_utils_a
    Dim ap As New AppUtils
    Dim wonum, sid, jump As String
    Dim wo As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim eid, loc, did, clid, chk, fid, cid, nid, stat, isdown, lid, eqnum, wt, ro, sdays, jpid, isact As String
    Dim usr, def, eqid, fuid, ncid, coid, typ, sched, appstr, islabor, who As String
    Dim exd, exdp As String
    Dim issched As Integer
    Dim hasmisc As Integer = 0
    Dim coi As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            coi = mu.COMPI
            lblcoi.Value = coi
            If coi = "EMD" Then
                lbliscol.Value = "yes"
                txtreq.CssClass = "plainlabel"

            End If
            If coi = "NISS" Then
                Try
                    isact = System.Configuration.ConfigurationManager.AppSettings("servicemode")
                    If isact = "active" Then
                        lblisactive.Value = "yes"
                    End If
                Catch ex As Exception

                End Try

            End If

            Try
                appstr = HttpContext.Current.Session("appstr").ToString()
                lblappstr.Value = appstr
                CheckApps(appstr)
            Catch ex As Exception

            End Try

            Try
                usr = Request.QueryString("usrname").ToString
                lbluser.Value = usr
            Catch ex As Exception

            End Try

            sched = mu.Sched
            lblusesched.Value = sched
            issched = mu.Is_Sched
            lblissched.Value = issched
            If issched = 1 Then
                trgrp.Attributes.Add("class", "view")
            Else
                trgrp.Attributes.Add("class", "view")
            End If
            sid = Request.QueryString("sid").ToString '"12"
            AutoCompleteExtender1.CompletionSetCount = sid
            AutoCompleteExtender2.CompletionSetCount = sid
            AutoCompleteExtender3.CompletionSetCount = sid
            txteqauto.Attributes.Add("onkeydown", "checkent();")
            txtsuperauto.Attributes.Add("onkeydown", "checkents();")
            txtleadauto.Attributes.Add("onkeydown", "checkentl();")
            wonum = Request.QueryString("wo").ToString '"1000"
            lblsid.Value = sid
            lblwonum.Value = wonum
            lblcid.Value = "0"
            jump = "yes"
            Try
                who = Request.QueryString("who").ToString
                lblwho.Value = who
            Catch ex As Exception
                who = ""
                lblwho.Value = who
            End Try

            If wonum <> "" Then
                wo.Open()
                'poppri()
                JumpWo(wonum)
                If usr <> "" Then
                    sql = "select islabor from pmsysusers where username = '" & usr & "'"
                    Try
                        islabor = wo.strScalar(sql)
                    Catch ex As Exception
                        islabor = "0"
                    End Try
                    wo.Dispose()
                End If
            ElseIf who = "ad" Then
                wo.Open()
                'poppri()
                GetWo()
                lbljump.Value = "no"
                wo.Dispose()

            End If
            If islabor = "" Then
                islabor = "0"
            End If
            If islabor <> "0" Then
                'imgleadcraft.Attributes.Add("onclick", "")
                'cbleade.Disabled = True
                If coi = "EMD" Then
                    'imgleadcraft.Attributes.Add("class", "view")
                Else
                    'imgleadcraft.Attributes.Add("class", "details")
                End If

                'cbleade.Attributes.Add("class", "details")
                'txtleadtime.Attributes.Add("class", "details")
            Else
                'imgleadcraft.Attributes.Add("onclick", "getsuper('lead');")
                'cbleade.Disabled = False
                'imgleadcraft.Attributes.Add("class", "view")
                'cbleade.Attributes.Add("class", "view")
                'txtleadtime.Attributes.Add("class", "view")
            End If
            'txtwodesc.Attributes.Add("onKeyPress", "return keychk(this,event)")
            txtleadauto.Attributes.Add("onBlur", "get_leadauto();")
            txtsuperauto.Attributes.Add("onBlur", "get_superauto();")

            txtwodesc.Attributes.Add("onBlur", "dosave();")
            txtwodesc.Attributes.Add("onKeyUp", "checksave();")

            txtestlh.Attributes.Add("onBlur", "dosave();")
            txtestlh.Attributes.Add("onKeyUp", "checksave();")

            txtestmp.Attributes.Add("onBlur", "dosave();")
            txtestmp.Attributes.Add("onKeyUp", "checksave();")

            txtestjp.Attributes.Add("onBlur", "dosave();")
            txtestjp.Attributes.Add("onKeyUp", "checksave();")

            ddpri.Attributes.Add("onBlur", "dosave();")
            ddpri.Attributes.Add("onclick", "checksave();")
            ddpri.Attributes.Add("onchange", "uppri();")

            txteqauto.Attributes.Add("onmouseover", "return overlib('Type in Equipment Number, Select from list, Press Enter or Look up button', BELOW, LEFT, WIDTH, 270)")
            txteqauto.Attributes.Add("onmouseout", "return nd()")
            imgeqauto.Attributes.Add("onmouseover", "return overlib('Create Work Order from Equipment Number Entry', ABOVE, LEFT)")
            imgeqauto.Attributes.Add("onmouseout", "return nd()")

        Else
            If Request.Form("lblsubmit") = "aplan" Then
                lblsubmit.Value = ""
                wo.Open()
                UpPlanner()
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "refresh" Then
                lblsubmit.Value = ""
                wonum = lblwonum.Value
                wo.Open()
                JumpWo(wonum)
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "nplan" Then
                lblsubmit.Value = ""
                wo.Open()
                GetJP()
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "rplan" Then
                lblsubmit.Value = ""
                wo.Open()
                RemRef()
                wonum = lblwonum.Value
                JumpWo(wonum)
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "addplan" Then
                lblsubmit.Value = ""
                wo.Open()
                AddPlan()
                wonum = lblwonum.Value
                JumpWo(wonum)
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "new" Then
                lblsubmit.Value = ""
                wo.Open()
                GetWo()
                lbljump.Value = "no"
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "dupwo" Then
                lblsubmit.Value = ""
                wo.Open()
                DupWO()
                lbljump.Value = "no"
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "go" Then
                lblsubmit.Value = ""
                wo.Open()
                SaveWo()
                'SaveDesc()
                wonum = lblwonum.Value
                JumpWo(wonum)
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "go1" Then
                lblsubmit.Value = ""
                wo.Open()
                SaveWo("remlead")
                'SaveDesc()
                wonum = lblwonum.Value
                JumpWo(wonum)
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "go2" Then
                lblsubmit.Value = ""
                wo.Open()
                SaveWo("remsup")
                'SaveDesc()
                wonum = lblwonum.Value
                JumpWo(wonum)
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "go3" Then
                lblsubmit.Value = ""
                wo.Open()
                SaveWo("nopri")
                'SaveDesc()
                wonum = lblwonum.Value
                JumpWo(wonum)
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "checkcomp" Then
                lblsubmit.Value = ""
                wo.Open()
                SaveWo()
                SaveDesc()
                CheckWOComp()
                wonum = lblwonum.Value
                JumpWo(wonum)
                stat = lblstat.Value
                If stat = "COMP" Or stat = "CAN" Or lblwt.Value = "PM" Or lblwt.Value = "TPM" Or lblwt.Value = "MAXPM" Then
                    txtwo.CssClass = "plainlabelblue"
                    txtwodesc.CssClass = "plainlabelblue"
                    txtwodesc.ReadOnly = True
                    imgcomp.Attributes.Add("class", "details")
                End If
                lbljump.Value = "no"
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "checkpmcomp" Then
                lblsubmit.Value = ""
                wo.Open()
                'SaveWo()
                'SaveDesc()
                CheckPMComp()
                wonum = lblwonum.Value
                JumpWo(wonum)
                stat = lblstat.Value
                If (stat = "COMP" Or stat = "CAN") And (lblwt.Value = "PM" Or lblwt.Value = "TPM" Or lblwt.Value = "MAXPM") Then
                    txtwo.CssClass = "plainlabelblue"
                    txtwodesc.CssClass = "plainlabelblue"
                    txtwodesc.ReadOnly = True
                    imgcomp.Attributes.Add("class", "details")
                End If
                lbljump.Value = "no"
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "checktpmcomp" Then
                lblsubmit.Value = ""
                wo.Open()
                'SaveWo()
                'SaveDesc()
                CheckTPMComp()
                wonum = lblwonum.Value
                JumpWo(wonum)
                stat = lblstat.Value
                If (stat = "COMP" Or stat = "CAN") And (lblwt.Value = "PM" Or lblwt.Value = "TPM" Or lblwt.Value = "MAXPM") Then '
                    txtwo.CssClass = "plainlabelblue"
                    txtwodesc.CssClass = "plainlabelblue"
                    txtwodesc.ReadOnly = True
                    imgcomp.Attributes.Add("class", "details")
                End If
                lbljump.Value = "no"
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "checkpmjpcomp" Then
                lblsubmit.Value = ""
                wo.Open()
                'SaveWo()
                'SaveDesc()
                CheckPMJPComp()
                wonum = lblwonum.Value
                JumpWo(wonum)
                stat = lblstat.Value
                If (stat = "COMP" Or stat = "CAN") And (lblwt.Value = "PM" Or lblwt.Value = "TPM" Or lblwt.Value = "MAXPM") Then '
                    txtwo.CssClass = "plainlabelblue"
                    txtwodesc.CssClass = "plainlabelblue"
                    txtwodesc.ReadOnly = True
                    imgcomp.Attributes.Add("class", "details")
                End If
                lbljump.Value = "no"
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "checkstat" Then
                lblsubmit.Value = ""
                wo.Open()
                SaveWo()
                SaveDesc()
                CheckStat()
                wonum = lblwonum.Value
                JumpWo(wonum)
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "updatepri" Then
                lblsubmit.Value = ""
                wo.Open()

                updatepri()
                SaveWo()
                wonum = lblwonum.Value
                JumpWo(wonum)
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "undopri" Then
                lblsubmit.Value = ""
                wo.Open()

                undopri()
                SaveWo()
                wonum = lblwonum.Value
                JumpWo(wonum)
                wo.Dispose()
            End If
            appstr = lblappstr.Value
            CheckApps(appstr)
        End If

    End Sub
    Private Sub CheckApps(ByVal appstr As String)
        Dim apparr() As String = appstr.Split(",")
        Dim o As Integer = 1
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                Select Case apparr(i)
                    Case "sch"
                        o = 0
                End Select
            Next
        Else
            o = 0
        End If
        If o <> 0 Then
            'jts.Attributes.Add("class", "details")
        End If
    End Sub
    Private Sub undopri()
        Dim wonum As String = lblwonum.Value
        sql = "update workorder set wopri = 0, wopriority = 0 where wonum = '" & wonum & "'"
        wo.Update(sql)
        lblwoprid.Value = "0"
        ddpri.Items.Clear()
        ddpri.Items.Insert(0, New ListItem("Select"))
        ddpri.Items.Insert(1, New ListItem("1"))
        ddpri.Items.Insert(2, New ListItem("2"))
        ddpri.Items.Insert(3, New ListItem("3"))
        ddpri.Items.Insert(4, New ListItem("4"))
        ddpri.Items.Insert(5, New ListItem("5"))
        ddpri.Items.Insert(6, New ListItem("6"))
        ddpri.Items.Insert(7, New ListItem("7"))
        ddpri.Items.Insert(8, New ListItem("8"))
        ddpri.Items.Insert(9, New ListItem("9"))
        ddpri.SelectedIndex = -1
    End Sub
    Private Sub updatepri()
        Dim wonum As String = lblwonum.Value
        sql = "update workorder set wopri = 0, wopriority = 0 where wonum = '" & wonum & "'"
        wo.Update(sql)
        poppri()

    End Sub
    Private Sub poppri()
        sql = "select woprid, (cast(wopri as varchar(10)) + ' - ' + wopridesc) as wopri from wopriority order by wopri"
        dr = wo.GetRdrData(sql)
        ddpri.DataSource = dr
        ddpri.DataTextField = "wopri"
        ddpri.DataValueField = "woprid"
        ddpri.DataBind()
        dr.Close()
        ddpri.Items.Insert(0, New ListItem("Select"))
    End Sub
    Private Sub CheckPMJPComp()
        Dim chkalert As Integer = 0
        Dim mchk As String = lblmalert.Value
        lblmalert.Value = ""
        Dim won As String
        won = lblwonum.Value
        wt = lblwt.Value
        Dim wtstr As String = lblwtstr.Value
        Dim wtar() As String = wtstr.Split(",")
        Dim i As Integer
        Dim rtflg As Integer = 0
        Dim wts As String
        For i = 0 To wtar.Length - 1
            wts = wtar(i)
            If wts = wt Then
                rtflg = 0
                Dim strMessage As String = "Root Cause Entry Required"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        Next

        'Check Labor Entry
        Dim esthrs, acthrs As Decimal
        Dim esthrsj As Decimal
        Dim esthrsw As Decimal

        sql = "select isnull(actlabhrs, 0) as acthrs from workorder where wonum = '" & won & "'"
        dr = wo.GetRdrData(sql)
        While dr.Read
            acthrs = dr.Item("acthrs").ToString
        End While
        dr.Close()
        If acthrs = 0 Then
            If mchk <> "ok" Then
                chkalert += 1
                If lblmalert.Value = "" Then
                    lblmalert.Value = "l"
                Else
                    lblmalert.Value += ",l"
                End If
            End If
        End If

        If chkalert = 0 Then
            CheckActualsPMJP(won)
            CompWOPMJP()
        Else
            Exit Sub
        End If
    End Sub
    Private Sub CheckActualsPMJP(ByVal wonum As String)
        Dim icost As String = ap.InvEntry
    End Sub
    Private Sub CompWOPMJP()
        Dim pmid, pmhid, pdt, ttt As String
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        pdt = lblusetdt.Value
        ttt = lblusetotal.Value
        Dim wonum As String = lblwonum.Value
        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        Dim cndate As Date = wo.CNOW
        issched = lblissched.Value

        If pmhid = "" And pmid <> "" Then
            Try
                sql = "select max(pmhid) from pmhistmax where pmid = '" & pmid & "'"
                pmhid = wo.strScalar(sql)
                lblpmhid.Value = pmhid
            Catch ex As Exception
                sql = "insert into pmhistmax (pmid) values ('" & pmid & "')"
                wo.Update(sql)
                Try
                    sql = "select max(pmhid) from pmhistmax where pmid = '" & pmid & "'"
                    pmhid = wo.strScalar(sql)
                    lblpmhid.Value = pmhid
                Catch ex1 As Exception

                End Try
            End Try

        End If
        Dim pmnum As String = lblpmnum.Value 'txtwodesc.Text
        If pmid <> "" And pmhid <> "" Then
            sql = "usp_comppmmax '" & pmid & "', '" & pmhid & "', '" & pmnum & "', '" & wonum & "','" & cndate & "','" & ustr & "','" & issched & "'"
            'sql = "usp_comppmmax '" & pmid & "', '" & pmhid & "', '" & pmnum & "', '" & wonum & "','" & cndate & "','" &  ustr & "','" & issched & "'"
            'sql = "usp_comppmmax '" & pmid & "', '" & pmhid & "', '" & pmnum & "', '" & wonum & "','" & cndate & "','" & ttt & "', " _
            ' + "'" & pdt & "','" & ustr & "','" & issched & "'"
            'pmi.Update(sql)
            Dim newdate As String '= sql
            Try
                newdate = wo.strScalar(sql)
                newdate = CType(newdate, DateTime)
            Catch ex As Exception
                'this is temp for nissan
                Try
                    Dim sql1 As String
                    sql1 = "insert into err_tmp (edate, sql) values (getdate(), '" & sql & "')"
                    wo.Update(sql1)
                    Dim strMessage As String = "Problem Completing PM - Please Contact Your System Administrator"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                Catch ex1 As Exception

                End Try
            End Try

            'If newdate < Now Then
            'lblalert.Value = "4"
            'Else
            'lblalert.Value = "3"
            'End If
            Dim won As String = lblwonum.Value
            sql = "usp_upreserved '" & won & "'"
            wo.Update(sql)


            isact = lblisactive.Value
            'If isact = "yes" Then
            'Try
            'Dim ps As String = nsws.SendCant(won)
            'If ps = "1" Then
            'Dim strMessage As String = "Problem with OpenWorkOrder or TimeEntry Transfer - Please Contact Your System Administrator"
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'End If
            'Catch ex As Exception

            'End Try
            'End If
            Dim superid As String = lblsup.Value
            If superid <> "" Then
                Try
                    Dim mail1 As New pmmail
                    mail1.CheckIt("comp", superid, wonum)
                Catch ex As Exception

                End Try
            End If
        Else
            Dim strMessage As String = "Problem Completing PM - Please Contact Your System Administrator\nNote: Missing PM ID"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If


    End Sub
    Private Sub CheckTPMComp()
        Dim pmid, pmhid As String
        pmid = lbltpmid.Value
        pmhid = lblpmhid.Value
        If pmhid = "" And pmid <> "" Then
            Try
                sql = "select max(pmhid) from pmhisttpm where pmid = '" & pmid & "'"
                pmhid = wo.strScalar(sql)
                lblpmhid.Value = pmhid
            Catch ex As Exception
                sql = "insert into pmhisttpm (pmid) values ('" & pmid & "')"
                wo.Update(sql)
                Try
                    sql = "select max(pmhid) from pmhisttpm where pmid = '" & pmid & "'"
                    pmhid = wo.strScalar(sql)
                    lblpmhid.Value = pmhid
                Catch ex1 As Exception

                End Try
            End Try

        End If
        Dim won As String = lblwonum.Value
        Dim wom, womc, wojpm, wojpmc As Integer
        Dim chkalert As Integer = 0
        Dim mchk As String = lblmalert.Value
        lblmalert.Value = ""

        wt = lblwt.Value
        Dim wtstr As String = lblwtstr.Value
        Dim wtar() As String = wtstr.Split(",")
        Dim i As Integer
        Dim rtflg As Integer = 0
        Dim wts As String
        For i = 0 To wtar.Length - 1
            wts = wtar(i)
            If wts = wt Then
                rtflg = 0
                Dim strMessage As String = "Root Cause Entry Required"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        Next

        'Check Measurements
        If mchk <> "ok" Then
            sql = "select womc = (select count(*) from pmTaskMeasDetMantpm where pmid = '" & pmid & "' and measurement2 is null), " _
                    + "wom = (select count(*) from pmTaskMeasDetMantpm where pmid = '" & pmid & "')"
            dr = wo.GetRdrData(sql)
            While dr.Read
                wom = dr.Item("wom").ToString
                womc = dr.Item("womc").ToString
            End While
            dr.Close()

            If (womc > 0 And wom <> 0) Then
                chkalert += 1
                If lblmalert.Value = "" Then
                    lblmalert.Value = "m"
                Else
                    lblmalert.Value += ",m"
                End If
                'lblmalert.Value = "m"
            End If
        End If

        'Check Total Time
        Dim esthrsstr, acthrsstr
        esthrsstr = lblttime.Value
        acthrsstr = lblacttime.Value
        If esthrsstr <> "0" Then
            If acthrsstr = "0" Then
                If mchk <> "ok" Then
                    chkalert += 1
                    If lblmalert.Value = "" Then
                        lblmalert.Value = "h"
                    Else
                        lblmalert.Value += ",h"
                    End If

                Else
                    SaveTPMTTime(esthrsstr, esthrsstr)
                End If

            End If
        End If

        'Check Labor Entry
        Dim esthrs, acthrs As Decimal
        Dim esthrsj As Decimal
        Dim esthrsw As Decimal
        sql = "select isnull(actlabhrs, 0) as acthrs from workorder where wonum = '" & won & "'"
        dr = wo.GetRdrData(sql)
        While dr.Read
            acthrs = dr.Item("acthrs").ToString
        End While
        dr.Close()
        If acthrs = 0 Then
            If mchk <> "ok" Then
                chkalert += 1
                If lblmalert.Value = "" Then
                    lblmalert.Value = "l"
                Else
                    lblmalert.Value += ",l"
                End If
            End If
        End If

        'Check Total Down Time
        Dim tdstart, tdstop, tdown As String
        Dim tdstartp, tdstopp, tdownp As String

        sql = "select startdown, stopdown, totaldown, startdownp, stopdownp, totaldownp from eqhist where wonum = '" & won & "'"
        dr = wo.GetRdrData(sql)
        While dr.Read
            tdstart = dr.Item("startdown").ToString
            tdstop = dr.Item("stopdown").ToString
            tdown = dr.Item("totaldown").ToString

            tdstartp = dr.Item("startdownp").ToString
            tdstopp = dr.Item("stopdownp").ToString
            tdownp = dr.Item("totaldownp").ToString
        End While
        dr.Close()
        If tdstart <> "" Then
            exd = "yes"
        End If
        If tdstartp <> "" Then
            exdp = "yes"
        End If
        'Save for force DT complete option
        'If exdp = "yes" And tdownp = "" Then
        'If exd = "yes" And tdown = "" Then
        'Dim strMessage As String = "Your Production and Equipment Down Time Entries need to be Completed"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'Else
        'Dim strMessage As String = "Your Production Down Time Entry needs to be Completed"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End If
        'ElseIf exd = "yes" And tdown = "" Then
        'Dim strMessage As String = "Your Equipment Down Time Entry needs to be Completed"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'Else
        Dim esthrsstrd, acthrsstrd
        esthrsstrd = lbldtime.Value
        acthrsstrd = lblactdtime.Value
        If esthrsstrd <> "0" And esthrsstrd <> "0.00" And esthrsstrd <> "" Then
            If acthrsstrd = "0" Or acthrsstrd = "0.00" Or acthrsstrd = "" Then
                If mchk <> "ok" Then
                    chkalert += 1
                    If lblmalert.Value = "" Then
                        lblmalert.Value = "d"
                    Else
                        lblmalert.Value += ",d"
                    End If

                Else
                    SaveDTime(esthrsstrd, exd, exdp)
                End If

            End If
        End If
        'End If



        If chkalert = 0 Then
            CheckActuals(won)
            'CompWO()
            CompTPMWO()
        Else
            Exit Sub
        End If


    End Sub
    Private Sub SaveTPMDTime(ByVal ard As String, ByVal exd As String, ByVal expd As String)
        Dim currdt As Date = wo.CNOW
        Dim pmid, pmhid As String
        Dim usetdt As String = lblusetdt.Value
        pmid = lbltpmid.Value
        pmhid = lblpmhid.Value
        Dim eqid As String = lbleqid.Value
        Dim wonum As String = lblwonum.Value
        If usetdt = "no" Then
            Dim ardchk As Long
            Try
                ardchk = System.Convert.ToDecimal(ard)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr371", "PMDivMantpm.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        End If
        If exd = "yes" And expd = "yes" Then
            sql = "update eqhist set stopdown = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update workorder set isdown = '0' where wonum = '" & wonum & "'; " _
        + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
        + "select @stopdate = stopdown, @downdate = startdown from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "set @dmins = datediff(mi, @downdate, @stopdate); " _
        + "set @dhrs = @dmins / 60; " _
        + "update tpm set actdtime = @dmins where pmid = '" & pmid & "'; " _
        + "update pmhisttpm set actdtime = @dmins where pmhid = '" & pmhid & "'; " _
        + "update eqhist set totaldown = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
        + "where eqid = '" & eqid & "'"
            wo.Update(sql)
            sql = "update eqhist set stopdownp = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update workorder set isdownp = '0' where wonum = '" & wonum & "'; " _
        + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
        + "select @stopdate = stopdownp, @downdate = startdownp from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "set @dmins = datediff(mi, @downdate, @stopdate); " _
        + "set @dhrs = @dmins / 60; " _
        + "update tpm set actdptime = @dmins where pmid = '" & pmid & "'; " _
        + "update pmhisttpm set actdptime = @dmins where pmhid = '" & pmhid & "'; " _
        + "update eqhist set totaldownp = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update equipment set totaldownp = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
        + "where eqid = '" & eqid & "'"
            wo.Update(sql)
        ElseIf exd = "yes" Then
            sql = "update eqhist set stopdown = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update workorder set isdown = '0' where wonum = '" & wonum & "'; " _
        + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
        + "select @stopdate = stopdown, @downdate = startdown from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "set @dmins = datediff(mi, @downdate, @stopdate); " _
        + "set @dhrs = @dmins / 60; " _
        + "update tpm set actdtime = @dmins where pmid = '" & pmid & "'; " _
        + "update pmhisttpm set actdtime = @dmins where pmhid = '" & pmhid & "'; " _
        + "update eqhist set totaldown = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
        + "where eqid = '" & eqid & "'"
            wo.Update(sql)
        ElseIf expd = "yes" Then
            sql = "update eqhist set stopdownp = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
       + "update workorder set isdownp = '0' where wonum = '" & wonum & "'; " _
       + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
       + "select @stopdate = stopdownp, @downdate = startdownp from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
       + "set @dmins = datediff(mi, @downdate, @stopdate); " _
       + "set @dhrs = @dmins / 60; " _
       + "update tpm set actdptime = @dmins where pmid = '" & pmid & "'; " _
       + "update pmhisttpm set actdptime = @dmins where pmhid = '" & pmhid & "'; " _
       + "update eqhist set totaldownp = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
       + "update equipment set totaldownp = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
       + "where eqid = '" & eqid & "'"
            wo.Update(sql)
        Else
            sql = "update tpm set actdtime = '" & ard & "' where pmid = '" & pmid & "'; " _
        + "declare @dmins decimal(10,2), @dhrs decimal(10,2), @eqhid int; " _
        + "set @dmins = '" & ard & "'; " _
        + "set @dhrs = @dmins / 60; " _
        + "select @eqhid = eqhid from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "if @eqhid is null " _
        + "begin " _
        + "insert into eqhist (eqid, wonum, ispm, pmid, totaldown)" _
        + "values ('" & eqid & "', '" & wonum & "', 1, '" & pmid & "', @dhrs)" _
        + "end " _
        + "else " _
        + "begin " _
        + "update eqhist set totaldown = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "' " _
        + "end; " _
        + "update pmhisttpm set actdtime = @dmins where pmhid = '" & pmhid & "'; " _
        + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
        + "where eqid = '" & eqid & "'"
            wo.Update(sql)
        End If


    End Sub
    Private Sub CheckPMComp()
        Dim pmid, pmhid As String
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        Dim won As String = lblwonum.Value
        Dim wom, womc, wojpm, wojpmc As Integer
        Dim chkalert As Integer = 0
        Dim mchk As String = lblmalert.Value
        lblmalert.Value = ""
        If pmhid = "" And pmid <> "" Then
            Try
                sql = "select max(pmhid) from pmhist where pmid = '" & pmid & "'"
                pmhid = wo.strScalar(sql)
                lblpmhid.Value = pmhid
            Catch ex As Exception
                sql = "insert into pmhist (pmid) values ('" & pmid & "')"
                wo.Update(sql)
                Try
                    sql = "select max(pmhid) from pmhist where pmid = '" & pmid & "'"
                    pmhid = wo.strScalar(sql)
                    lblpmhid.Value = pmhid
                Catch ex1 As Exception

                End Try
            End Try

        End If

        wt = lblwt.Value
        Dim wtstr As String = lblwtstr.Value
        Dim wtar() As String = wtstr.Split(",")
        Dim i As Integer
        Dim rtflg As Integer = 0
        Dim wts As String
        For i = 0 To wtar.Length - 1
            wts = wtar(i)
            If wts = wt Then
                rtflg = 0
                Dim strMessage As String = "Root Cause Entry Required"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        Next

        'Check Measurements
        If mchk <> "ok" Then
            sql = "select womc = (select count(*) from pmTaskMeasDetMan where pmid = '" & pmid & "' and measurement2 is null), " _
                    + "wom = (select count(*) from pmTaskMeasDetMan where pmid = '" & pmid & "')"
            dr = wo.GetRdrData(sql)
            While dr.Read
                wom = dr.Item("wom").ToString
                womc = dr.Item("womc").ToString
            End While
            dr.Close()

            If (womc > 0 And wom <> 0) Then
                chkalert += 1
                If lblmalert.Value = "" Then
                    lblmalert.Value = "m"
                Else
                    lblmalert.Value += ",m"
                End If
                'lblmalert.Value = "m"
            End If
        End If

        'Check Total Time
        Dim esthrsstr, acthrsstr
        esthrsstr = lblttime.Value
        acthrsstr = lblacttime.Value
        'And esthrsstr <> "0.00"
        If esthrsstr <> "0" Then
            'Or acthrsstr = "0.00"
            If acthrsstr = "0" Then
                If mchk <> "ok" Then
                    chkalert += 1
                    If lblmalert.Value = "" Then
                        lblmalert.Value = "h"
                    Else
                        lblmalert.Value += ",h"
                    End If

                Else
                    SavePMTTime(esthrsstr, esthrsstr)
                End If

            End If
        End If

        'Check Labor Entry
        Dim esthrs, acthrs As Decimal
        Dim esthrsj As Decimal
        Dim esthrsw As Decimal
        sql = "select isnull(actlabhrs, 0) as acthrs from workorder where wonum = '" & won & "'"
        dr = wo.GetRdrData(sql)
        While dr.Read
            acthrs = dr.Item("acthrs").ToString
        End While
        dr.Close()
        If acthrs = 0 Then
            If mchk <> "ok" Then
                chkalert += 1
                If lblmalert.Value = "" Then
                    lblmalert.Value = "l"
                Else
                    lblmalert.Value += ",l"
                End If
            End If
        End If

        'Check Total Down Time
        Dim tdstart, tdstop, tdown As String
        Dim tdstartp, tdstopp, tdownp As String
        Dim exd, exdp As String
        sql = "select startdown, stopdown, totaldown, startdownp, stopdownp, totaldownp from eqhist where wonum = '" & won & "'"
        dr = wo.GetRdrData(sql)
        While dr.Read
            tdstart = dr.Item("startdown").ToString
            tdstop = dr.Item("stopdown").ToString
            tdown = dr.Item("totaldown").ToString

            tdstartp = dr.Item("startdownp").ToString
            tdstopp = dr.Item("stopdownp").ToString
            tdownp = dr.Item("totaldownp").ToString
        End While
        dr.Close()
        If tdstart <> "" Then
            exd = "yes"
        End If
        If tdstartp <> "" Then
            exdp = "yes"
        End If
        'save for forced DT entry
        'If exdp = "yes" And tdownp = "" Then
        'If exd = "yes" And tdown = "" Then
        'Dim strMessage As String = "Your Production and Equipment Down Time Entries need to be Completed"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'Else
        'Dim strMessage As String = "Your Production Down Time Entry needs to be Completed"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End If
        'ElseIf exd = "yes" And tdown = "" Then
        'Dim strMessage As String = "Your Equipment Down Time Entry needs to be Completed"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'Else
        Dim esthrsstrd, acthrsstrd
        esthrsstrd = lbldtime.Value
        acthrsstrd = lblactdtime.Value
        Dim s As String = acthrsstrd
        If esthrsstrd <> "0" And esthrsstrd <> "0.00" And esthrsstrd <> "" Then
            If acthrsstrd = "0" Or acthrsstrd = "0.00" Or acthrsstrd = "" Then
                If mchk <> "ok" Then
                    chkalert += 1
                    If lblmalert.Value = "" Then
                        lblmalert.Value = "d"
                    Else
                        lblmalert.Value += ",d"
                    End If

                Else
                    SaveDTime(esthrsstrd, exd, exdp)
                End If

            End If
        End If
        'End If



        If chkalert = 0 Then
            CheckPMActuals(won)
            'CompWO()
            CompPMWO()
        Else
            Exit Sub
        End If

    End Sub
    Private Sub SaveDTime(ByVal ard As String, ByVal exd As String, ByVal exdp As String)
        Dim currdt As Date = wo.CNOW
        Dim pmid, pmhid As String
        Dim usetdt As String = lblusetdt.Value
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        Dim eqid As String = lbleqid.Value
        Dim wonum As String = lblwonum.Value
        If usetdt = "no" Then
            Dim ardchk As Long
            Try
                ardchk = System.Convert.ToDecimal(ard)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr333", "PMDivMan.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        End If
        If exd = "yes" And exdp = "yes" Then
            sql = "update eqhist set stopdown = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update workorder set isdown = '0' where wonum = '" & wonum & "'; " _
        + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
        + "select @stopdate = stopdown, @downdate = startdown from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "set @dmins = datediff(mi, @downdate, @stopdate); " _
        + "set @dhrs = @dmins / 60; " _
        + "update pm set actdtime = @dmins where pmid = '" & pmid & "'; " _
        + "update pmhist set actdtime = @dmins where pmhid = '" & pmhid & "'; " _
        + "update eqhist set totaldown = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
        + "where eqid = '" & eqid & "'"
            wo.Update(sql)
            sql = "update eqhist set stopdownp = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update workorder set isdownp = '0' where wonum = '" & wonum & "'; " _
        + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
        + "select @stopdate = stopdownp, @downdate = startdownp from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "set @dmins = datediff(mi, @downdate, @stopdate); " _
        + "set @dhrs = @dmins / 60; " _
        + "update pm set actdptime = @dmins where pmid = '" & pmid & "'; " _
        + "update pmhist set actdptime = @dmins where pmhid = '" & pmhid & "'; " _
        + "update eqhist set totaldownp = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update equipment set totaldownp = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
        + "where eqid = '" & eqid & "'"
            wo.Update(sql)
        ElseIf exd = "yes" Then
            sql = "update eqhist set stopdownp = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update workorder set isdownp = '0' where wonum = '" & wonum & "'; " _
        + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
        + "select @stopdate = stopdownp, @downdate = startdownp from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "set @dmins = datediff(mi, @downdate, @stopdate); " _
        + "set @dhrs = @dmins / 60; " _
        + "update pm set actdptime = @dmins where pmid = '" & pmid & "'; " _
        + "update pmhist set actdptime = @dmins where pmhid = '" & pmhid & "'; " _
        + "update eqhist set totaldownp = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update equipment set totaldownp = (select sum(totaldownp) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
        + "where eqid = '" & eqid & "'"
            wo.Update(sql)
        ElseIf exdp = "yes" Then
            sql = "update eqhist set stopdownp = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update workorder set isdownp = '0' where wonum = '" & wonum & "'; " _
        + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
        + "select @stopdate = stopdownp, @downdate = startdownp from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "set @dmins = datediff(mi, @downdate, @stopdate); " _
        + "set @dhrs = @dmins / 60; " _
        + "update pm set actdptime = @dmins where pmid = '" & pmid & "'; " _
        + "update pmhist set actdptime = @dmins where pmhid = '" & pmhid & "'; " _
        + "update eqhist set totaldownp = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update equipment set totaldownp = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
        + "where eqid = '" & eqid & "'"
            wo.Update(sql)
        Else
            sql = "update pm set actdtime = '" & ard & "' where pmid = '" & pmid & "'; " _
                    + "declare @dmins decimal(10,2), @dhrs decimal(10,2), @eqhid int; " _
                    + "set @dmins = '" & ard & "'; " _
                    + "set @dhrs = @dmins / 60; " _
                    + "select @eqhid = eqhid from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
                    + "if @eqhid is null " _
                    + "begin " _
                    + "insert into eqhist (eqid, wonum, ispm, pmid, totaldown)" _
                    + "values ('" & eqid & "', '" & wonum & "', 1, '" & pmid & "', @dhrs)" _
                    + "end " _
                    + "else " _
                    + "begin " _
                    + "update eqhist set totaldown = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "' " _
                    + "end; " _
                    + "update pmhist set actdtime = @dmins where pmhid = '" & pmhid & "'; " _
                    + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
                    + "where eqid = '" & eqid & "'"
            wo.Update(sql)
        End If


    End Sub
    Private Sub CheckPMActuals(ByVal wonum As String)
        Dim icost As String = ap.InvEntry

        Dim runs, cruns As String
        runs = lblruns.Value
        cruns = lblworuns.Value
        Dim runsi As Integer
        Try
            runsi = CType(runs, Integer)
        Catch ex As Exception
            runs = "1"
            runsi = 1
        End Try
        Dim crunsi As Integer
        Try
            crunsi = CType(cruns, Integer)
        Catch ex As Exception
            cruns = "0"
            crunsi = 0
        End Try

        If icost <> "ext" And icost <> "inv" Then
            If runs = "1" Then
                sql = "update workorder set actmatcost = (select isnull(sum(total),isnull(sum(usedtotal),0)) from wopmparts where wonum = '" & wonum & "' and used = 'Y') " _
                + "where wonum = '" & wonum & "'; update workorder set acttoolcost = (select isnull(sum(total),isnull(sum(usedtotal),0)) from wopmtools where wonum = '" & wonum & "' and used = 'Y') " _
                + "where wonum = '" & wonum & "'; update workorder set actlubecost = (select isnull(sum(cost),0) from wopmlubes where wonum = '" & wonum & "' and used = 'Y') " _
                + "where wonum = '" & wonum & "'"
                wo.Update(sql)
            Else
                crunsi = crunsi + 1
                sql = "update workorder set actmatcost = isnull(actmatcost, 0) + (select isnull(sum(total),isnull(sum(usedtotal),0)) from wopmparts where wonum = '" & wonum & "' and used = 'Y') " _
                + "where wonum = '" & wonum & "'; update workorder set acttoolcost = isnull(acttoolcost, 0) + (select isnull(sum(total),isnull(sum(usedtotal),0)) from wopmtools where wonum = '" & wonum & "' and used = 'Y') " _
                + "where wonum = '" & wonum & "'; update workorder set actlubecost = isnull(actlubecost, 0) + (select isnull(sum(cost),0) from wopmlubes where wonum = '" & wonum & "' and used = 'Y') " _
                + "where wonum = '" & wonum & "'; update womultidates set actmatcost = (select isnull(sum(total),isnull(sum(usedtotal),0)) from wopmparts where wonum = '" & wonum & "' and used = 'Y') " _
                + "where wonum = '" & wonum & "' and wocnt = '" & crunsi & "'; update womultidates set acttoolcost = (select isnull(sum(total),isnull(sum(usedtotal),0)) from wopmtools where wonum = '" & wonum & "' and used = 'Y') " _
                + "where wonum = '" & wonum & "' and wocnt = '" & crunsi & "'; update womultidates set actlubecost = (select isnull(sum(cost),0) from wopmlubes where wonum = '" & wonum & "' and used = 'Y') " _
                + "where wonum = '" & wonum & "' and wocnt = '" & crunsi & "'"
                wo.Update(sql)
            End If

        End If
    End Sub


    Private Sub LoadMLabor(ByVal wonum As String)
        sql = "select userid, username, wonum, sum(assigned) as assigned " _
        + "from woassign where wonum = '" & wonum & "' and (islead <> '1' or islead is null) " _
        + "group by userid, username, wonum"
        Dim sb As New StringBuilder
        sb.Append("<table>")
        Dim user, hrs As String
        dr = wo.GetRdrData(sql)
        While dr.Read
            user = dr.Item("username").ToString
            hrs = dr.Item("assigned").ToString
            sb.Append("<tr><td class=""plainlabel"">" & user & "</td>")
            sb.Append("<td class=""plainlabel"">" & hrs & "</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        divlg.InnerHtml = sb.ToString
    End Sub
    Private Sub LoadFail(ByVal wonum As String, ByVal typ As String)
        Dim pmid As String = lblpmid.Value
        Dim tpmid As String = lbltpmid.Value
        If pmid <> "" Then
            
            sql = "select distinct failuremode, ocarea from wofailmodes1 where wonum = '" & wonum & "' " _
           + "and failuremode in (select fm1s from pmtrack where pmid = '" & pmid & "') " _
+ "or failuremode in (select fm2s from pmtrack where pmid = '" & pmid & "') " _
+ "or failuremode in (select fm3s from pmtrack where pmid = '" & pmid & "') " _
+ "or failuremode in (select fm4s from pmtrack where pmid = '" & pmid & "') " _
+ "or failuremode in (select fm5s from pmtrack where pmid = '" & pmid & "')"
            sql = "select distinct f.failuremode, o.ocarea from pmtaskfailmodesman1 f left join ocareas o on o.oaid = f.oaid where f.pmid = '" & pmid & "' order by f.failuremode"
        ElseIf tpmid <> "" Then
            sql = "select distinct failuremode, ocarea from wofailmodes1 where wonum = '" & wonum & "' " _
           + "and failuremode in (select fm1s from pmtracktpm where pmid = '" & tpmid & "') " _
+ "or failuremode in (select fm2s from pmtracktpm where pmid = '" & tpmid & "') " _
+ "or failuremode in (select fm3s from pmtracktpm where pmid = '" & tpmid & "') " _
+ "or failuremode in (select fm4s from pmtracktpm where pmid = '" & tpmid & "') " _
+ "or failuremode in (select fm5s from pmtracktpm where pmid = '" & tpmid & "')"
            sql = "select distinct f.failuremode, o.ocarea from pmtaskfailmodestpmman1 f left join ocareas o on o.oaid = f.oaid where f.pmid = '" & tpmid & "' order by f.failuremode"
        Else
            sql = "select distinct failuremode, ocarea from wofailmodes1 where wonum = '" & wonum & "' "
        End If

        Dim sb As String
        Dim fm As String
        Dim oc As String
        dr = wo.GetRdrData(sql)
        While dr.Read
            oc = dr.Item("ocarea").ToString
            fm = dr.Item("failuremode").ToString
            If oc <> "" Then
                fm = fm & "(" & oc & ")"
            End If
            If sb = "" Then
                sb = fm
            Else
                sb += "<br>" & fm
            End If
        End While
        dr.Close()
        Dim dv As New StringBuilder
        If hasmisc = 0 Then
            dv.Append("<div style=""OVERFLOW: auto; WIDTH: 160px;  HEIGHT: 80px;"">")
            dv.Append(sb)
            dv.Append("</div>")
        Else
            dv.Append("<div style=""OVERFLOW: auto; WIDTH: 160px;  HEIGHT: 60px;"">")
            dv.Append(sb)
            dv.Append("</div>")
        End If

        If typ = "D" Then
            tdfail.InnerHtml = dv.ToString
        Else
            tdfail3.InnerHtml = dv.ToString
        End If

    End Sub
    Private Sub JumpWo(ByVal wonum As String)
        'need custom pri check here
        sql = "select [default] from wovars where [column] = 'wopriority'"
        Dim prichk As String
        Try
            prichk = wo.strScalar(sql)
        Catch ex As Exception
            prichk = "no"
        End Try

        Dim dept, cell, func, comid, comp, err1, err2, err3, waid, wa, nc, ld, desc, ldesc, runs, cruns, jp, rtid As String
        Dim skill, skillid, statdate, tts, ttc, tss, tsc, tas, tac, se, le, ldt, pmid, skid, tpmid, pri, priid, ch, jpref, qty, ehrs, jhrs, cstat As String
        Dim repby, repdate, phone, lcomp, scomp, col, eqdesc, isdownp, planner, plannerid As String
        Dim note, root As String
        sql = "select w.*, d.dept_line, cl.cell_name, f.func, c.compnum, wa.workarea, l.longdesc, e.eqdesc, " _
            + "isnull(u.phonenum, '') as phonenum, round(isnull(estlabhrs,0),2) as 'ehrs', round(isnull(estjplabhrs,0),2) as jhrs, " _
        + "Convert(char(10),targstartdate,101) as 'tstart', " _
        + "Convert(char(10),targcompdate,101) as 'tcomp', " _
        + "Convert(char(10),schedstart,101) as 'sstart', " _
        + "Convert(char(10),schedfinish,101) as 'scomp1', " _
        + "Convert(char(10),actstart,101) as 'astart', " _
        + "Convert(char(10),actfinish,101) as 'acomp', " _
        + "isnull(datediff(day, schedstart, schedfinish),0) as sdays, e.fpc, r.rootid, n.noteid, p.rtid " _
        + "from workorder w " _
        + "left join dept d on d.dept_id = w.deptid " _
        + "left join cells cl on cl.cellid = w.cellid " _
        + "left join equipment e on e.eqid = w.eqid " _
        + "left join functions f on f.func_id = w.funcid " _
        + "left join components c on c.comid = w.comid " _
        + "left join workareas wa on wa.waid = w.waid " _
        + "left join wolongdesc l on l.wonum = w.wonum " _
        + "left join pmsysusers u on u.username = w.reportedby " _
        + "left join worootcause r on r.wonum = w.wonum " _
        + "left join wonotes n on n.wonum = w.wonum " _
        + "left join pm p on p.pmid = w.pmid " _
        + "where w.wonum = '" & wonum & "'"
        txtwo.Text = wonum
        Dim coi As String = lbliscol.Value
        dr = wo.GetRdrData(sql)
        While dr.Read
            rtid = dr.Item("rtid").ToString
            planner = dr.Item("planner").ToString
            plannerid = dr.Item("plannerid").ToString
            tdwoplanner.InnerHtml = planner
            lblwplanner.Value = planner
            lblwplannerid.Value = plannerid
            note = dr.Item("noteid").ToString
            root = dr.Item("rootid").ToString
            eqdesc = dr.Item("eqdesc").ToString
            col = dr.Item("fpc").ToString
            lcomp = dr.Item("lcomp").ToString
            scomp = dr.Item("scomp").ToString
            qty = dr.Item("qty").ToString
            eid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            nid = dr.Item("ncid").ToString
            sid = dr.Item("siteid").ToString
            lid = dr.Item("locid").ToString
            loc = dr.Item("location").ToString
            did = dr.Item("deptid").ToString
            dept = dr.Item("dept_line").ToString
            clid = dr.Item("cellid").ToString
            cell = dr.Item("cell_name").ToString
            cid = dr.Item("comid").ToString
            comp = dr.Item("compnum").ToString
            fid = dr.Item("funcid").ToString
            func = dr.Item("func").ToString
            wt = dr.Item("worktype").ToString
            err1 = dr.Item("errorcode1").ToString
            err2 = dr.Item("errorcode2").ToString
            err3 = dr.Item("errorcode3").ToString
            waid = dr.Item("waid").ToString
            wa = dr.Item("workarea").ToString
            ld = dr.Item("ld").ToString
            isdown = dr.Item("isdown").ToString
            isdownp = dr.Item("isdownp").ToString
            If ld = "" Then
                If lid <> "" Then
                    ld = "L"
                ElseIf did <> "" Then
                    ld = "D"
                End If
            End If

            ch = dr.Item("chargenum").ToString
            pri = dr.Item("wopriority").ToString
            priid = dr.Item("wopri").ToString
            Dim cr, crl As String
            If isdown = "1" Then
                cr = "plainlabelred"
                crl = "A1R"
            Else
                cr = "plainlabel"
                crl = "A1U"
            End If
            stat = dr.Item("status").ToString
            'statdate = dr.Item("statusdate").ToString
            Dim typ, pm As String
            typ = dr.Item("worktype").ToString
            jp = dr.Item("jpid").ToString
            jpref = dr.Item("jpref").ToString
            pm = dr.Item("pmid").ToString
            skill = dr.Item("skill").ToString
            skillid = dr.Item("skillid").ToString
            If clid <> "" Then
                chk = "yes"
            Else
                chk = "no"
            End If
            stat = dr.Item("status").ToString
            desc = dr.Item("description").ToString
            ldesc = dr.Item("longdesc").ToString
            Dim reptst As String = dr.Item("reportedby").ToString
            Dim reptst1 As String = reptst
            'from wodet
            If coi = "yes" Then
                txtreq.Text = dr.Item("reportedby").ToString
            Else
                tdreq.InnerHtml = dr.Item("reportedby").ToString
            End If
            lblreq.Value = dr.Item("reportedby").ToString
            tdreqdate.InnerHtml = dr.Item("reportdate").ToString
            sdays = dr.Item("sdays").ToString
            lblsdays.Value = sdays
            tdstat.InnerHtml = dr.Item("status").ToString
            lblstat.Value = dr.Item("status").ToString
            statdate = dr.Item("statusdate").ToString
            If statdate <> "" Then
                tdstatdate.InnerHtml = statdate
            End If
            'txtcontract.Text = dr.Item("contract").ToString
            tdcontract.InnerHtml = dr.Item("contract").ToString
            tdwt.InnerHtml = dr.Item("worktype").ToString
            jpid = dr.Item("jpid").ToString
            lbljpid.Value = dr.Item("jpid").ToString
            tts = dr.Item("tstart").ToString
            If tts <> "" Then
                'txttstart.Text = tts
                tdtstart.InnerHtml = tts
            Else
                'txttstart.Text = ""
                tdtstart.InnerHtml = ""
            End If
            ttc = dr.Item("tcomp").ToString
            If ttc <> "" Then
                'txttcomp.Text = ttc
                tdtcomp.InnerHtml = ttc
            Else
                'txttcomp.Text = ""
                tdtcomp.InnerHtml = ""
            End If

            tss = dr.Item("sstart").ToString
            If tss <> "" Then
                'txtsstart.Text = tss
                tdsstart.InnerHtml = tss
            Else
                'txtsstart.Text = ""
                tdsstart.InnerHtml = ""
                trsched.Attributes.Add("class", "bgyl")
                tdschlab.Attributes.Add("class", "redlabel")

            End If
            tsc = dr.Item("scomp1").ToString
            If tsc <> "" Then
                'txtscomp.Text = tsc
                tdscomp.InnerHtml = tsc
            Else
                'txtscomp.Text = ""
                tdscomp.InnerHtml = ""
            End If

            tas = dr.Item("astart").ToString
            If tas <> "" Then
                'txtastart.Text = tas
                tdastart.InnerHtml = tas
            Else
                'txtastart.Text = ""
                tdastart.InnerHtml = ""
            End If
            tac = dr.Item("acomp").ToString
            If tac <> "" Then
                'txtacomp.Text = tac
                tdacomp.InnerHtml = tac
                lblacomp.Value = tac
            Else
                'txtacomp.Text = ""
                tdacomp.InnerHtml = ""
                lblacomp.Value = ""
            End If

            txtleadauto.Text = dr.Item("leadcraft").ToString
            txtsuperauto.Text = dr.Item("supervisor").ToString
            'tdlead.InnerHtml = dr.Item("leadcraft").ToString
            'tdsup.InnerHtml = dr.Item("supervisor").ToString

            ehrs = dr.Item("ehrs").ToString
            jhrs = dr.Item("jhrs").ToString
            'estjplabhrs
            se = dr.Item("supealert").ToString
            lblse.Value = se
            le = dr.Item("leadealert").ToString
            lblle.Value = le

            ldt = dr.Item("elead").ToString
            txtleadtime.Text = ldt
            lbloldelead.Value = ldt

            pmid = dr.Item("pmid").ToString
            lblpmid.Value = pmid
            tpmid = dr.Item("tpmid").ToString
            lbltpmid.Value = tpmid
            eqid = dr.Item("eqid").ToString
            lbleqid.Value = eqid
            skid = dr.Item("skillid").ToString
            lblskillid.Value = skid
            lblskill.Value = dr.Item("skill").ToString
            tdskill.InnerHtml = dr.Item("skill").ToString
            runs = dr.Item("multidates").ToString
            lblruns.Value = runs
            cruns = dr.Item("multicnt").ToString
            lblworuns.Value = cruns

            lbllead.Value = dr.Item("leadcraftid").ToString
            lblsup.Value = dr.Item("superid").ToString

            'tdreq.InnerHtml = dr.Item("reportedby").ToString
            'tdreqdate.InnerHtml = dr.Item("reportdate").ToString

            tdphone.InnerHtml = dr.Item("phonenum").ToString
        End While
        dr.Close()
        If pmid <> "" Then
            GetDocs()
        End If
        lblrtid.Value = rtid
        lblnoteid.Value = note
        lblrootid.Value = root
        Dim wtstr As String = ""
        sql = "select [default] from wovars where [column] = 'root'"
        Try
            wtstr = wo.strScalar(sql)
        Catch ex As Exception
            wtstr = ""
        End Try
        lblwtstr.Value = wtstr
        If isdown = "" Then
            isdown = "0"
        End If
        If isdownp = "" Then
            isdownp = "0"
        End If
        lblisdown.Value = isdown
        lblisdownp.Value = isdownp

        lblscomp.Value = scomp
        lbllcomp.Value = lcomp
        If scomp = "1" Then
            tdsupe.Attributes.Add("class", "waiton")
        Else
            tdsupe.Attributes.Add("class", "waitoff")
        End If
        If lcomp = "1" Then
            tdleade.Attributes.Add("class", "waiton")
        Else
            tdleade.Attributes.Add("class", "waitoff")
        End If

        txtestmp.Text = qty

        txtestlh.Text = ehrs
        txtestjp.Text = jhrs
        If se = "1" Then
            cbsupe.Checked = True
        Else
            cbsupe.Checked = False
        End If
        If le = "1" Then
            cbleade.Checked = True
        Else
            cbleade.Checked = False
        End If
        trlocs.Attributes.Add("class", "details")
        trdepts.Attributes.Add("class", "details")
        trlocs3.Attributes.Add("class", "details")
        lblld.Value = ld
        If ld = "M" Then
            lbltyp.Value = "mod"
            trlocs.Attributes.Add("class", "view")
            trdepts.Attributes.Add("class", "details")
            trlocs3.Attributes.Add("class", "details")
            lbllid.Value = lid
            lblloc.Value = loc
            tdloc2.InnerHtml = loc
            lbleq.Value = eqnum
            lbleqid.Value = eid
            tdeq2.InnerHtml = eqnum
            tderr.InnerHtml = "Error Code 1: " & err1 & "<br>Error Code 2: " & err2 & "<br>Error Code 3: " & err3
            lblwaid.Value = waid
            lblworkarea.Value = wa

            tdcharge2.InnerHtml = ch
            'tdwa1.InnerHtml = wa
            'If isdown = "1" Then
            '    cbisdown.Checked = True
            'tdisdown1.Attributes.Add("class", "redlabel")
            'Else
            'cbisdown.Checked = False
            'tdisdown1.Attributes.Add("class", "label")
            'End If
        ElseIf ld = "L" Then
            lbltyp.Value = "locs"
            trlocs.Attributes.Add("class", "details")
            trdepts.Attributes.Add("class", "details")
            trlocs3.Attributes.Add("class", "view")
            lbllid.Value = lid
            lblloc.Value = loc
            tdloc3.InnerHtml = loc
            lbleq.Value = eqnum
            lbleqid.Value = eid
            tdeq3.InnerHtml = eqnum
            lblfuid.Value = fid
            lblfu.Value = func
            tdfu3.InnerHtml = func
            lblcomid.Value = cid
            lblcomp.Value = comp
            tdco3.InnerHtml = comp
            If nc <> "" Then
                lblncid.Value = ncid
                lblnc.Value = nc
                tdmisc3.InnerHtml = nc
            Else
                trmisc3.Attributes.Add("class", "details")
                hasmisc = 1
            End If


            lblwaid.Value = waid
            lblworkarea.Value = wa
            If coi = "yes" Then
                imgch.Attributes.Add("class", "details")
                Label5.Text = "Column"
                tdcharge3.InnerHtml = col
            Else
                tdcharge3.InnerHtml = ch
            End If
            tdeqdesc3.InnerHtml = eqdesc
            LoadFail(wonum, ld)

            If isdown = "1" Or isdownp = "1" Then
                cbisdown3.Checked = True
                tdisdown3.Attributes.Add("class", "redlabel")
                tdeq3.Attributes.Add("class", "plainlabelred")
                tdeq3lbl.Attributes.Add("class", "redlabel")
                tdeqdesc3.Attributes.Add("class", "plainlabelred")
            Else
                cbisdown3.Checked = False
                tdisdown3.Attributes.Add("class", "label")
                tdeq3.Attributes.Add("class", "plainlabel")
                tdeq3lbl.Attributes.Add("class", "label")
                tdeqdesc3.Attributes.Add("class", "plainlabel")
            End If
        ElseIf ld = "D" Then
            lbltyp.Value = "depts"
            trlocs.Attributes.Add("class", "details")
            trdepts.Attributes.Add("class", "view")
            trlocs3.Attributes.Add("class", "details")
            lbllid.Value = lid
            lblloc.Value = loc
            tdloc.InnerHtml = loc
            lbleq.Value = eqnum
            lbleqid.Value = eid
            tdeq.InnerHtml = eqnum
            '*
            lbldid.Value = did
            lbldept.Value = dept
            tddept.InnerHtml = dept
            lblclid.Value = clid
            lblcell.Value = cell
            tdcell.InnerHtml = cell
            lblfuid.Value = fid
            lblfu.Value = func
            tdfu.InnerHtml = func
            lblcomid.Value = cid
            lblcomp.Value = comp
            tdco.InnerHtml = comp
            If nc <> "" Then
                lblncid.Value = ncid
                lblnc.Value = nc
                tdmisc.InnerHtml = nc
            Else
                trmisc.Attributes.Add("class", "details")
                hasmisc = 1
            End If
            lbllid.Value = lid
            lblloc.Value = loc
            tdloc.InnerHtml = loc
            lbltyp.Value = "depts"
            '*
            lblwaid.Value = waid
            lblworkarea.Value = wa
            If coi = "yes" Then
                imgchd.Attributes.Add("class", "details")
                Label3.Text = "Column"
                tdcharge1.InnerHtml = col
            Else
                tdcharge1.InnerHtml = ch
            End If
            tdeqdesc.InnerHtml = eqdesc

            LoadFail(wonum, ld)

            If isdown = "1" Or isdownp = "1" Then
                cbisdown.Checked = True
                tddown.Attributes.Add("class", "redlabel")
                tdeq.Attributes.Add("class", "plainlabelred")
                tdeqlbl.Attributes.Add("class", "redlabel")
                tdeqdesc.Attributes.Add("class", "plainlabelred")
            Else
                cbisdown.Checked = False
                tddown.Attributes.Add("class", "label")
                tdeq.Attributes.Add("class", "plainlabel")
                tdeqlbl.Attributes.Add("class", "label")
                tdeqdesc.Attributes.Add("class", "plainlabel")
            End If
        End If
        'need pri check here
        If prichk = "yes" Then

            If (pri <> "" And pri <> "0") And (priid = "" Or priid = "0") Then
                If stat <> "COMP" And stat <> "CAN" And stat <> "CLOSE" Then
                    imgpri.Attributes.Add("class", "viewcell")
                    lblprimsg.Value = "usecust"
                End If
                If pri <> "" And pri <> "0" Then
                    Try
                        ddpri.SelectedValue = pri
                    Catch ex As Exception

                    End Try
                End If
            Else
                poppri()
                imgpri.Attributes.Add("class", "details")
                lblwoprid.Value = priid
                lblusepri.Value = "yes"
                If priid <> "" And priid <> "0" Then
                    Try
                        ddpri.SelectedValue = priid
                    Catch ex As Exception

                    End Try
                End If
            End If
        ElseIf prichk = "no" Then
            If priid <> "" And priid <> "0" Then
                poppri()
                lblwoprid.Value = priid
                lblusepri.Value = "yes"
                If stat <> "COMP" And stat <> "CAN" And stat <> "CLOSE" Then
                    imgpri.Attributes.Add("class", "viewcell")
                    lblprimsg.Value = "usestand"
                    If priid <> "" And priid <> "0" Then
                        Try
                            ddpri.SelectedValue = priid
                        Catch ex As Exception

                        End Try
                    End If
                Else
                    If priid <> "" And priid <> "0" Then
                        Try
                            ddpri.SelectedValue = priid
                        Catch ex As Exception

                        End Try
                    End If
                End If
            Else
                imgpri.Attributes.Add("class", "details")
                If pri <> "" And pri <> "0" Then
                    Try
                        ddpri.SelectedValue = pri
                    Catch ex As Exception

                    End Try
                End If
            End If
        Else
            If pri <> "" And pri <> "0" Then
                Try
                    ddpri.SelectedValue = pri
                Catch ex As Exception

                End Try
            End If

        End If

        tdwa.InnerHtml = wa

        lblskill.Value = skill
        lblskillid.Value = skillid
        tdwt.InnerHtml = wt
        lblwt.Value = wt
        tdstat.InnerHtml = stat
        'tdstatdate.InnerHtml = statdate
        If stat = "COMP" Or stat = "CAN" Then
            'txtplan.CssClass = "plainlabelblue"
            'txtsup.CssClass = "plainlabelblue"
        End If
        If ldesc <> "" Then
            'txtlongdesc.Text = desc + ldesc
            txtwodesc.Text = desc + ldesc
        Else
            txtwodesc.Text = desc
        End If
        LoadMLabor(wonum)
        If stat = "COMP" Or stat = "CAN" Or lblwt.Value = "PM" Or lblwt.Value = "TPM" Or lblwt.Value = "MAXPM" Then
            'need proc to make all blue

            GoBlue()
        End If
        If wt = "PM" Then
            GetPMNums(pmid)
        End If
        If wt = "TPM" Then
            GetTPMNums(tpmid)
        End If
        If wt = "MAXPM" Then
            GetMAXMNums(pmid)
        End If
        If jp <> "" Or jpref <> "" Then
            GetJP()

        End If
        'document.getElementById("ifnotes").src = "wonotesdialog.aspx?who=fs&wonum=" + wonum + "&rootid=" + root + "&noteid=" + note + "&user=" + user + "&userid=" + userid
        'document.getElementById("ifwo").src = "wolabtransfs.aspx?sid=" + sid + "&wo=" + wonum + "&jpid=" + jp + "&stat=" + stat + "&date=" + Now + "&ro=" + ro;
        Dim user As String = lbluser.Value
        Dim userid As String = ""
        sid = lblsid.Value
        issched = lblissched.Value
        coi = lblcoi.Value
        If issched = 0 And coi = "EMD" Then
            ifnotes.Attributes.Add("src", "wonotesdialog.aspx?who=fs&wonum=" + wonum + "&rootid=" + root + "&noteid=" + note + "&user=" + user + "&userid=" + userid)
            ifwo.Attributes.Add("src", "wolabtransfs.aspx?sid=" + sid + "&wo=" + wonum + "&jpid=" + jp + "&stat=" + stat + "&date=" + Now + "&ro=" + ro)
        End If
        
    End Sub
    Private Sub GetDocs()
        Dim doc, docs, fn, ty, pmid As String
        pmid = lblpmid.Value
        Dim sbc As Integer

        sql = "select count(*) from pmManAttach where pmid = '" & pmid & "'"
        sbc = wo.Scalar(sql)
        If sbc > 0 Then ' should be using has rows instead of count
            sql = "select * from pmManAttach where pmid = '" & pmid & "'"
            dr = wo.GetRdrData(sql)
            While dr.Read
                fn = dr.Item("filename").ToString
                ty = dr.Item("doctype").ToString
                doc = ty & "~" & fn
                If docs = "" Then
                    docs = doc
                Else
                    docs += ";" & doc
                End If
            End While
            dr.Close()
            lbldocs.Value = docs
        End If


    End Sub
    Private Sub GetMAXMNums(ByVal pmid As String)
        sql = "select pmnum from pmmax where pmid = '" & pmid & "'"
        Dim pmnum As String
        Try
            pmnum = wo.strScalar(sql)
        Catch ex As Exception

        End Try
        lblpmnum.Value = pmnum

    End Sub
    Private Sub GetPMNums(ByVal pmid As String)
        sql = "select isnull(p.ttime,0) as 'ttime', isnull(p.dtime,0) as 'dtime', isnull(p.acttime,0) as 'acttime', " _
        + "isnull(p.actdtime,0) as 'actdtime' from pm p where p.pmid = '" & pmid & "'"
        dr = wo.GetRdrData(sql)
        While dr.Read
            lblttime.Value = dr.Item("ttime").ToString
            lbldtime.Value = dr.Item("dtime").ToString

            lblacttime.Value = dr.Item("acttime").ToString
            lblactdtime.Value = dr.Item("actdtime").ToString
        End While
        dr.Close()

        Dim pmhid As String
        Try
            sql = "select max(pmhid) from pmhist where pmid = '" & pmid & "'"
            pmhid = wo.strScalar(sql)
            lblpmhid.Value = pmhid
        Catch ex As Exception
            sql = "insert into pmhist (pmid) values ('" & pmid & "')"
            wo.Update(sql)
            Try
                sql = "select max(pmhid) from pmhist where pmid = '" & pmid & "'"
                pmhid = wo.strScalar(sql)
                lblpmhid.Value = pmhid
            Catch ex1 As Exception

            End Try
        End Try


        Dim pdt, ttt As String
        pdt = ap.PDTEntry
        If pdt = "lvdtt" Then
            lblusetdt.Value = "no"

        Else
            lblusetdt.Value = "yes"

        End If
        ttt = ap.TTTEntry
        If ttt = "lvtpt" Then
            lblusetotal.Value = "no"

        Else
            lblusetotal.Value = "yes"

        End If
    End Sub
    Private Sub GetTPMNums(ByVal pmid As String)
        sql = "select fcust, nextday, days, nextdate, isnull(p.ttime,0) as 'ttime', isnull(p.dtime,0) as 'dtime', isnull(p.acttime,0) as 'acttime', " _
        + "isnull(p.actdtime,0) as 'actdtime' from tpm p where p.pmid = '" & pmid & "'"
        dr = wo.GetRdrData(sql)
        While dr.Read
            lblttime.Value = dr.Item("ttime").ToString
            lbldtime.Value = dr.Item("dtime").ToString

            lblacttime.Value = dr.Item("acttime").ToString
            lblactdtime.Value = dr.Item("actdtime").ToString

            lblfcust.Value = dr.Item("fcust").ToString
            lblretday.Value = dr.Item("nextday").ToString
            lbldays.Value = dr.Item("days").ToString
            txtn.Value = dr.Item("nextdate").ToString
        End While
        dr.Close()

        Dim pmhid As String
        Try
            sql = "select max(pmhid) from pmhisttpm where pmid = '" & pmid & "'"
            pmhid = wo.strScalar(sql)
            lblpmhid.Value = pmhid
        Catch ex As Exception
            sql = "insert into pmhisttpm (pmid) values ('" & pmid & "')"
            wo.Update(sql)
            Try
                sql = "select max(pmhid) from pmhisttpm where pmid = '" & pmid & "'"
                pmhid = wo.strScalar(sql)
                lblpmhid.Value = pmhid
            Catch ex1 As Exception

            End Try
        End Try


        Dim pdt, ttt As String
        pdt = ap.PDTTEntry
        If pdt = "lvdttt" Then
            lblusetdt.Value = "no"

        Else
            lblusetdt.Value = "yes"

        End If
        ttt = ap.TTTTEntry
        If ttt = "lvtptt" Then
            lblusetotal.Value = "no"

        Else
            lblusetotal.Value = "yes"

        End If
    End Sub
    Private Sub GoBlue()
        txtwo.CssClass = "plainlabelblue"
        txtwodesc.CssClass = "plainlabelblue"
        txtwodesc.ReadOnly = True
        stat = lblstat.Value
        If stat = "COMP" Or stat = "CAN" Then 'Or lblwt.Value = "PM" Or lblwt.Value = "TPM"
            imgcomp.Attributes.Add("class", "details")
        End If

        tdstat.Attributes.Add("class", "plainlabelblue")
        tdstatdate.Attributes.Add("class", "plainlabelblue")
        tdphone.Attributes.Add("class", "plainlabelblue")

        'cbsupe.Disabled = True
        'cbleade.Disabled = True

        'txttstart.CssClass = "plainlabelblue"
        'txttcomp.CssClass = "plainlabelblue"
        'txtsstart.CssClass = "plainlabelblue"
        'txtacomp.CssClass = "plainlabelblue"
        'txtsstart.CssClass = "plainlabelblue"
        'txtacomp.CssClass = "plainlabelblue"

        'txtlead.CssClass = "plainlabelblue"
        'txtsup.CssClass = "plainlabelblue"

        txtleadtime.CssClass = "plainlabelblue"
        txtestlh.CssClass = "plainlabelblue"
        txtestmp.CssClass = "plainlabelblue"
        '***
        'txtstat.CssClass = "plainlabelblue"
    End Sub
    Private Sub GetJP()
        Dim jprefstat As String
        wonum = lblwonum.Value
        sql = "select w.worktype, w.jpid, w.jpnum, w.jpref, r.status as jprefstat, p.username from workorder w " _
        + "left join pmjpref r on r.jpref = w.jpref and r.jpref is not null " _
        + "left join pmsysusers p on p.userid = r.plnrid where w.wonum = '" & wonum & "'"

        Dim jpn, jpid, jpref, pname, wt As String
        dr = wo.GetRdrData(sql)
        While dr.Read
            jpid = dr.Item("jpid").ToString
            jpn = dr.Item("jpnum").ToString
            jpref = dr.Item("jpref").ToString
            pname = dr.Item("username").ToString
            jprefstat = dr.Item("jprefstat").ToString
            wt = dr.Item("worktype").ToString
        End While
        dr.Close()
        ro = lblro.Value
        If wt = "PM" Or wt = "MAXPM" Or ro = "1" Then
            imgplanner.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
            imgplanner.Attributes.Add("onclick", "")
            imgplans.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
            imgaddplan.Attributes.Add("src", "../images/appbuttons/minibuttons/addmod.gif")
            imgeditplan.Attributes.Add("src", "../images/appbuttons/minibuttons/pencildis.gif")
            lbldis.Value = "1"
        Else
            If jpref = "" Then
                lblplan.Value = jpn
                tdplan.InnerHtml = jpn
                lbljpid.Value = jpid
                tdplanner.Attributes.Add("class", "graylabel")
                imgplanner.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
                imgplanner.Attributes.Add("onclick", "")
                tdjp.Attributes.Add("class", "label")

                imgplans.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifier.gif")
                'imgplans.Attributes.Add("onclick", "getplans();")
                imgaddplan.Attributes.Add("src", "../images/appbuttons/minibuttons/addmod.gif")
                'imgaddplan.Attributes.Add("onclick", "gotojp();")
                imgeditplan.Attributes.Add("src", "../images/appbuttons/minibuttons/pencil.gif")
                lbldis.Value = "0"
                tdref.InnerHtml = "None"
                cbreq.Checked = False
            Else
                lblplan.Value = jpn
                tdplan.InnerHtml = jpn
                lbljpid.Value = jpid
                lblplanner2.Value = pname
                tdplanner2.InnerHtml = pname
                tdplanner.Attributes.Add("class", "label")
                imgplanner.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifier.gif")
                imgplanner.Attributes.Add("onclick", "getplanner('plan');")
                tdjp.Attributes.Add("class", "graylabel")
                If jprefstat <> "Complete" Then
                    imgplans.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
                    'imgplans.Attributes.Add("onclick", "")
                    imgaddplan.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                    'imgaddplan.Attributes.Add("onclick", "")
                    imgeditplan.Attributes.Add("src", "../images/appbuttons/minibuttons/pencildis.gif")
                    lbldis.Value = "1"
                Else
                    imgplans.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifier.gif")
                    'imgplans.Attributes.Add("onclick", "getplans();")
                    imgaddplan.Attributes.Add("src", "../images/appbuttons/minibuttons/addmod.gif")
                    'imgaddplan.Attributes.Add("onclick", "gotojp();")
                    imgeditplan.Attributes.Add("src", "../images/appbuttons/minibuttons/pencil.gif")
                    lbldis.Value = "0"
                End If

                tdref.InnerHtml = jpref
                lbljpref.Value = jpref
                cbreq.Checked = True
            End If
        End If


    End Sub
    Private Sub AddPlan()
        wonum = lblwonum.Value
        Dim jpid As String = lbljpid.Value
        If jpid <> "" Then
            sql = "usp_addjptowo '" & jpid & "','" & wonum & "'"
            wo.Update(sql)
            lblsubmit.Value = "upjp"
        End If
    End Sub
    Private Sub RemRef()
        Dim jpid As String = lbljpid.Value
        Dim jpref As String = lbljpref.Value
        wonum = lblwonum.Value
        sql = "usp_addremjpref '" & wonum & "','" & jpref & "','','rem'"
        wo.Update(sql)
        GetJP()
        wo.Dispose()
    End Sub
    Private Sub UpPlanner()
        Dim plnrid As String = lblplnrid.Value
        Dim plnr As String = lblplanner.Value
        Dim jpref As String = lbljpref.Value
        wonum = lblwonum.Value

        sql = "update pmjpref set plnrid = '" & plnrid & "' where jpref = '" & jpref & "' and wonum = '" & wonum & "'"
        wo.Update(sql)
        GetJP()

    End Sub
    Private Sub GetWo()
        cid = lblcid.Value
        usr = lbluser.Value
        sid = lblsid.Value
        sql = "select wostatus from wostatus where compid = '" & cid & "' and isdefault = 1"
        dr = wo.GetRdrData(sql)
        While dr.Read
            def = dr.Item("wostatus").ToString
        End While
        dr.Close()
        sql = "insert into workorder (status, statusdate, changeby, changedate, reportedby, reportdate, " _
        + "estlabhrs, estlabcost, estmatcost, esttoolcost, estlubecost, " _
        + "actlabhrs, actlabcost, actmatcost, acttoolcost, actlubecost, " _
        + "estjplabhrs, estjplabcost, estjpmatcost, estjptoolcost, estjplubecost, " _
        + "actjplabhrs, actjplabcost, actjpmatcost, actjptoolcost, actjplubecost, " _
        + "totlabhrs, totlabcost, totmatcost, tottoolcost, totlubecost, siteid, scomp, lcomp) " _
        + "values ('" & def & "', getDate(),'" & usr & "', getDate(),'" & usr & "', getDate(), 0,0,0,0,0,0,0,0,0, " _
        + "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'" & sid & "',0,0) " _
        + "select @@identity"
        Try
            wonum = wo.Scalar(sql)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr560", "woadd.aspx.vb")

            wo.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        txtwo.Text = wonum
        sql = "insert into wohist (wonum, wostatus, statusdate, changeby) values('" & wonum & "','" & def & "',getDate(),'" & usr & "')"
        Try
            wo.Update(sql)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr561", "woadd.aspx.vb")

            wo.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        lblwonum.Value = wonum
        lbljump.Value = "no"
        Dim sh As String
        sh = txtwodesc.Text
        If sh <> "" Then
            'SaveDesc()
        Else
            lblsav.Value = ""
            lblcnt.Value = ""
        End If
        JumpWo(wonum)
        lblsubmit.Value = "upwo"
    End Sub
    Private Sub DupWO()
        Dim won As Integer
        wonum = lblwonum.Value
        usr = lbluser.Value
        sql = "usp_dupwo '" & wonum & "','" & usr & "'"
        won = wo.Scalar(sql)
        sql = "update workorder set scomp = 0, lcomp = 0 where wonum = '" & wonum & "'"
        wo.Update(sql)
        lblwonum.Value = won
        txtwo.Text = won
        lbljump.Value = "yes"
        lblsav.Value = "0"
        lblstat.Value = "WAPPR"
        txtwo.CssClass = "plainlabel"
        txtwodesc.CssClass = "plainlabel"
        txtwodesc.ReadOnly = False
        imgcomp.Attributes.Add("class", "view")
        'lblmalert.Value = "dupwo"
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        chk = lblchk.Value
        eqid = lbleqid.Value
        ncid = lblncid.Value
        fuid = lblfuid.Value
        coid = lblcomid.Value
        lid = lbllid.Value
        typ = lbltyp.Value
        stat = lblstat.Value
        'lblgetarch.Value = "yes"
        usr = lbluser.Value
        JumpWo(won)

    End Sub
    Private Sub SaveWo(Optional ByVal who As String = "reg")
        sql = "select [default] from wovars where [column] = 'wopriority'"
        Dim prichk As String
        Try
            prichk = wo.strScalar(sql)
        Catch ex As Exception
            prichk = "no"
        End Try

        Dim coi As String = lbliscol.Value
        Dim lead, sup, leads, sups, hrs, pri, priid, se, le, qty, req, oreq, scomp, lcomp, plannerid, planner, supx, leadx As String
        wt = lblwt.Value
        wonum = lblwonum.Value
        sup = lblsup.Value
        lead = lbllead.Value
        supx = txtsuperauto.Text
        leadx = txtleadauto.Text
        sups = "0" 'txtsup.Text
        leads = "0" 'txtlead.Text
        hrs = txtestlh.Text
        qty = txtestmp.Text
        plannerid = lblwplannerid.Value
        planner = lblwplanner.Value
        priid = lblwoprid.Value
        If plannerid <> "" Then
            sql = "update workorder set plannerid = '" & plannerid & "', planner = '" & planner & "' where wonum = '" & wonum & "'"
            wo.Update(sql)
        End If
        If ddpri.SelectedIndex <> 0 And ddpri.SelectedIndex <> -1 Then
            pri = ddpri.SelectedValue
        Else
            pri = "0"
        End If
        If prichk = "yes" Then
            If (pri <> "" And pri <> "0") And (priid = "" Or priid = "0") Then
                priid = "0"
            Else
                Dim priar() As String = pri.Split(" - ")
                Try
                    pri = priar(0)
                    Dim pritst As Integer = pri
                Catch ex As Exception
                    pri = "0"
                    priid = "0"
                End Try
            End If
        ElseIf prichk = "no" Then
            If priid <> "" And priid <> "0" Then
                Dim priar() As String = pri.Split(" - ")
                Try
                    pri = priar(0)
                Catch ex As Exception
                    pri = "0"
                    priid = "0"
                End Try
            Else
                priid = "0"
            End If
        End If

        Dim reqi As Integer
        If coi = "yes" Then
            req = txtreq.Text
            oreq = lblreq.Value
            If req <> oreq Then
                reqi = req.Length
                If reqi > 50 Then
                    Dim strMessage As String = "Requested By Field Limited to 50 Characters"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                Else
                    req = wo.ModString1(req)
                    sql = "update workorder set reportedby = '" & req & "' where wonum = '" & wonum & "'"
                    req = wo.Update(sql)
                End If
            End If
        End If
        'If wt <> "PM" Then
        If cbsupe.Checked = True Then
            se = "1"
        Else
            se = "0"
        End If
        If cbleade.Checked = True Then
            le = "1"
        Else
            le = "0"
        End If
        'Else
        'se = lblse.Value
        'le = lblle.Value
        'End If
        Dim lc, sc As String
        If se = "1" Then
            sc = "1"
        Else
            sc = "0"
        End If
        If le = "1" Then
            lc = "1"
        Else
            lc = "0"
        End If
        If who = "remsup" Then
            sql = "update workorder set supervisor = null, " _
            + "superid = null " _
            + "where wonum = '" & wonum & "'"
            wo.Update(sql)
        End If
        If who = "remlead" Then
            sql = "update workorder set leadcraft = null, " _
            + "leadcraftid = null " _
            + "where wonum = '" & wonum & "'"
            wo.Update(sql)
        End If
        Dim tnchk As Long
        Try
            tnchk = System.Convert.ToInt64(hrs)
        Catch ex As Exception
            Try
                tnchk = System.Convert.ToDecimal(hrs)
            Catch ex1 As Exception
                Dim strMessage As String = tmod.getmsg("cdstr238", "PMTaskDivFunc.aspx.vb")
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
                Exit Sub
            End Try

        End Try
        Try
            tnchk = System.Convert.ToInt64(qty)
        Catch ex As Exception
            qty = "0"
        End Try

        Dim elead As String = txtleadtime.Text
        Try
            tnchk = System.Convert.ToInt64(elead)
        Catch ex As Exception
            elead = "0"
        End Try

        Dim olead As String = lbloldelead.Value

        SaveDesc()

        'If wt <> "PM" And wt <> "TPM" Then
        If elead = "0" Then
            If who = "reg" Then
                sql = "update workorder set estlabhrs = '" & hrs & "', wopri = '" & priid & "', wopriority = '" & pri & "', qty = '" & qty & "' where wonum = '" & wonum & "'"
                wo.Update(sql)
            Else
                sql = "update workorder set estlabhrs = '" & hrs & "', qty = '" & qty & "' where wonum = '" & wonum & "'"
                wo.Update(sql)
            End If
            'sql = "update workorder set leadealert = '" & le & "', supealert = '" & se & "', estlabhrs = '" & hrs & "', wopriority = '" & pri & "', qty = '" & qty & "' where wonum = '" & wonum & "'"

            'put send mail here, use se and le
            sql = "select scomp, lcomp from workorder where wonum = '" & wonum & "'"
            dr = wo.GetRdrData(sql)
            While dr.Read
                scomp = dr.Item("scomp").ToString
                lcomp = dr.Item("lcomp").ToString
            End While
            dr.Close()
            Try
                If cbsupe.Checked = True Then
                    If scomp <> "1" Then
                        Dim mail1 As New pmmail
                        mail1.CheckIt("sup", sup, wonum)
                        sql = "update workorder set scomp = '1' where wonum = '" & wonum & "'"
                        wo.Update(sql)
                    End If
                    'SendIt("sup", superid, wonum, desc, start)

                End If
                If cbleade.Checked = True Then
                    If lcomp <> "1" Then
                        Dim mail2 As New pmmail
                        mail2.CheckIt("lead", lead, wonum)
                        sql = "update workorder set lcomp = '1' where wonum = '" & wonum & "'"
                        wo.Update(sql)
                    End If
                    'SendIt("lead", leadid, wonum, desc, start)

                End If
            Catch ex As Exception

            End Try

        Else
            If olead = elead Then
                If who = "reg" Then
                    sql = "update workorder set leadealert = '" & le & "', supealert = '" & se & "', estlabhrs = '" & hrs & "', wopri = '" & priid & "', wopriority = '" & pri & "', qty = '" & qty & "', elead = '" & elead & "' where wonum = '" & wonum & "'"
                Else
                    sql = "update workorder set leadealert = '" & le & "', supealert = '" & se & "', estlabhrs = '" & hrs & "', qty = '" & qty & "', elead = '" & elead & "' where wonum = '" & wonum & "'"
                End If

            Else
                If who = "reg" Then
                    sql = "update workorder set leadealert = '" & le & "', supealert = '" & se & "', estlabhrs = '" & hrs & "', wopri = '" & priid & "', wopriority = '" & pri & "', qty = '" & qty & "', elead = '" & elead & "', scomp = 0, lcomp = 0 where wonum = '" & wonum & "'"
                Else
                    sql = "update workorder set leadealert = '" & le & "', supealert = '" & se & "', estlabhrs = '" & hrs & "', qty = '" & qty & "', elead = '" & elead & "', scomp = 0, lcomp = 0 where wonum = '" & wonum & "'"
                End If

            End If
            wo.Update(sql)
        End If
        'Else
        'sql = "update workorder set wopriority = '" & pri & "' where wonum = '" & wonum & "'"
        'End If

        sid = lblsid.Value
        Dim ncnt As Integer = 0
        Dim lcnt As Integer = 0
        Dim wcnt As Integer = 0
        Dim ecnt As Integer = 0
        If leadx <> "" Then
            sql = "select count(*) from pmsysusers where islabor = 1 and dfltps = '" & sid & "' and username = '" & leadx & "'"
            ncnt = wo.Scalar(sql)
            If ncnt = 1 Then
                sql = "select userid from pmsysusers where username = '" & leadx & "'"
                lcnt = wo.Scalar(sql)
                sql = "select count(*) from woassign where wonum = '" & wonum & "' and userid = '" & lcnt & "'"
                wcnt = wo.Scalar(sql)
                sql = "update workorder set leadcraftid = '" & lcnt & "', leadcraft = '" & leadx & "' where wonum = '" & wonum & "'"
                wo.Update(sql)
                If wcnt = 0 Then
                    sql = "update woassign set islead = 0 where wonum = '" & wonum & "'; " _
                        + "insert into woassign (wonum, userid, username, islead) values ('" & wonum & "','" & lcnt & "','" & eqnum & "','1')"
                    wo.Update(sql)
                Else
                    sql = "update woassign set islead = 0 where wonum = '" & wonum & "'; " _
                        + "update woassign set islead = 1 where wonum = '" & wonum & "' and userid = '" & lcnt & "'"
                    wo.Update(sql)
                End If
            Else
                Dim strMessage As String = "The Lead Craft Name Entry was Not Found"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        End If
        If supx <> "" Then
            sql = "select count(*) from pmsysusers where issuper = 1 and dfltps = '" & sid & "' and username = '" & supx & "'"
            ncnt = wo.Scalar(sql)
            If ncnt = 1 Then
                sql = "select userid from pmsysusers where username = '" & supx & "'"
                lcnt = wo.Scalar(sql)
                sql = "update workorder set superid = '" & lcnt & "', supervisor = '" & supx & "' where wonum = '" & wonum & "'"
                wo.Update(sql)
            Else
                Dim strMessage As String = "Supervisor Name Entry was Not Found"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        End If




    End Sub
    Private Sub SaveDesc()
        wonum = lblwonum.Value
        Dim sh, lg As String
        Dim lgcnt As Integer
        sh = txtwodesc.Text
        sh = Replace(sh, "'", Chr(180), , , vbTextCompare)
        sh = Replace(sh, "--", "-", , , vbTextCompare)
        sh = Replace(sh, ";", ":", , , vbTextCompare)
        sh = Replace(sh, "/", " ", , , vbTextCompare)
        If Len(sh) > 79 Then
            lg = Mid(sh, 80)
            sh = Mid(sh, 1, 79)
        End If
        Dim cmd As New SqlCommand("exec usp_savewodesc @wonum, @sh, @lg")
        Dim param0 = New SqlParameter("@wonum", SqlDbType.Int)
        param0.Value = wonum
        cmd.Parameters.Add(param0)
        Dim param01 = New SqlParameter("@sh", SqlDbType.VarChar)
        If sh = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = sh
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@lg", SqlDbType.Text)
        If lg = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = lg
        End If
        cmd.Parameters.Add(param02)
        Try
            wo.UpdateHack(cmd)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr562", "woadd.aspx.vb")

            wo.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        lblsav.Value = "0"
    End Sub
    Private Sub CheckWOComp()
        Dim pmid, pmhid As String
        wonum = lblwonum.Value
        Dim wt As String
        sql = "select w.worktype, w.pmid, pmhid = (select max(p1.pmhid) from pmhist p1 where p1.pmid = w.pmid) " _
        + "from workorder w where w.wonum = '" & wonum & "'"
        dr = wo.GetRdrData(sql)
        While dr.Read
            wt = dr.Item("worktype").ToString
            pmid = dr.Item("pmid").ToString
            lblpmid.Value = dr.Item("pmid").ToString
            pmhid = "" 'dr.Item("pmhid").ToString
            lblpmhid.Value = dr.Item("pmhid").ToString
        End While
        dr.Close()
        If pmhid = "" And pmid <> "" And wt = "PM" Then
            Try
                sql = "select max(pmhid) from pmhist where pmid = '" & pmid & "'"
                pmhid = wo.strScalar(sql)
                lblpmhid.Value = pmhid
            Catch ex As Exception
                sql = "insert into pmhist (pmid) values ('" & pmid & "')"
                wo.Update(sql)
                Try
                    sql = "select max(pmhid) from pmhist where pmid = '" & pmid & "'"
                    pmhid = wo.strScalar(sql)
                    lblpmhid.Value = pmhid
                Catch ex1 As Exception

                End Try
            End Try

        ElseIf pmhid = "" And pmid <> "" And wt = "TPM" Then
            Try
                sql = "select max(pmhid) from pmhisttpm where pmid = '" & pmid & "'"
                pmhid = wo.strScalar(sql)
                lblpmhid.Value = pmhid
            Catch ex As Exception
                sql = "insert into pmhisttpm (pmid) values ('" & pmid & "')"
                wo.Update(sql)
                Try
                    sql = "select max(pmhid) from pmhisttpm where pmid = '" & pmid & "'"
                    pmhid = wo.strScalar(sql)
                    lblpmhid.Value = pmhid
                Catch ex1 As Exception

                End Try
            End Try

        End If
        Dim chkalert As Integer = 0
        Dim mchk As String = lblmalert.Value
        lblmalert.Value = ""

        Dim wtstr As String = lblwtstr.Value
        Dim wtar() As String = wtstr.Split(",")
        Dim i As Integer
        Dim rtflg As Integer = 0
        Dim wts As String
        Dim rootid As String = lblrootid.Value
        For i = 0 To wtar.Length - 1
            wts = wtar(i)
            If wts = wt Then
                If rootid = "" Then
                    rtflg = 0
                    Dim strMessage As String = "Root Cause Entry Required"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If

            End If
        Next

        If wt <> "PM" And wt <> "TPM" Then
            'Check Measurements
            Dim wom, womc, wojpm, wojpmc As Integer

            If mchk <> "ok" Then
                sql = "select womc = (select count(*) from pmtaskmeasdetails where wonum = '" & wonum & "'), " _
                + "wom = (select count(*) from pmtaskmeasdetails where wonum = '" & wonum & "' and womeas is null), " _
                + "wojpmc = (select count(*) from wojptaskmeasdetails where wonum = '" & wonum & "'), " _
                + "wojpm = (select count(*) from wojptaskmeasdetails where wonum = '" & wonum & "' and womeas is null)"
                dr = wo.GetRdrData(sql)
                While dr.Read
                    wom = dr.Item("wom").ToString
                    womc = dr.Item("womc").ToString
                    wojpm = dr.Item("wojpm").ToString
                    wojpmc = dr.Item("wojpmc").ToString
                End While
                dr.Close()
                If (womc > 0 And wom = 0) Or (wojpmc > 0 And wojpm = 0) Then
                    chkalert += 1
                    If lblmalert.Value = "" Then
                        lblmalert.Value = "m"
                    Else
                        lblmalert.Value += ",m"
                    End If
                    'lblmalert.Value = "m"
                End If
            End If

            'Check Total Time
            Dim esthrs, acthrs As Decimal
            Dim esthrsj As Decimal
            Dim esthrsw As Decimal
            sql = "select isnull(estlabhrs, 0) + isnull(estjplabhrs, 0) as esthrs, isnull(actlabhrs, 0) as acthrs, " _
            + "isnull(estlabhrs, 0) as 'estlabhrs', isnull(estjplabhrs, 0) as 'estjplabhrs' " _
            + "from workorder where wonum = '" & wonum & "'"
            dr = wo.GetRdrData(sql)
            While dr.Read
                esthrs = dr.Item("esthrs").ToString
                acthrs = dr.Item("acthrs").ToString

                esthrsj = dr.Item("estjplabhrs").ToString
                esthrsw = dr.Item("estlabhrs").ToString
            End While
            dr.Close()
            If esthrs <> 0 Then
                If mchk <> "ok" And acthrs = 0 Then
                    chkalert += 1
                    If lblmalert.Value = "" Then
                        lblmalert.Value = "l"
                    Else
                        lblmalert.Value += ",l"
                    End If
                Else
                    SaveTTime(esthrsw, esthrsj)
                End If

            End If

            'Check Total Down Time
            Dim tdstart, tdstop, tdown As String
            Dim tdstartp, tdstopp, tdownp As String
            Dim exd, exdp As String
            sql = "select startdown, stopdown, totaldown, startdownp, stopdownp, totaldownp from eqhist where wonum = '" & wonum & "'"
            dr = wo.GetRdrData(sql)
            While dr.Read
                tdstart = dr.Item("startdown").ToString
                tdstop = dr.Item("stopdown").ToString
                tdown = dr.Item("totaldown").ToString

                tdstartp = dr.Item("startdownp").ToString
                tdstopp = dr.Item("stopdownp").ToString
                tdownp = dr.Item("totaldownp").ToString
            End While
            dr.Close()
            If tdstart <> "" Then
                exd = "yes"
            End If
            If tdstartp <> "" Then
                exdp = "yes"
            End If
            Dim coi As String = lblcoi.Value
            wt = lblwt.Value
            Dim tdn, tdnp As String
            Dim bmf As Integer = 0
            If coi = "NISS" And wt = "BM" Then
                If exd = "yes" And exdp = "yes" Then
                    bmf = 0
                Else
                    bmf = 1
                    lblmalert.Value = ""
                    Dim strMessage As String = "Production Equipment Downtime Entries are Both Required for the BM Work Type"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
            End If

            If bmf = 0 Then
                Dim isdown, isdownp As String
                sql = "select isnull(isdown,0) as isdown, isnull(isdownp,0) as isdownp from workorder where wonum = '" & wonum & "'"
                Try
                    'isdown = wo.strScalar(sql)
                    dr = wo.GetRdrData(sql)
                    While dr.Read
                        isdown = dr.Item("isdown").ToString
                        isdownp = dr.Item("isdownp").ToString
                    End While
                    dr.Close()
                Catch ex As Exception
                    isdown = "0"
                    isdownp = "0"
                End Try
                If isdown <> "0" Or isdownp <> "0" Then
                    If mchk <> "ok" Then
                        chkalert += 1
                        If lblmalert.Value = "" Then
                            lblmalert.Value = "d"
                        Else
                            lblmalert.Value += ",d"
                        End If

                    Else
                        Dim who As String
                        If isdown <> "0" Then
                            who = "d"
                        End If
                        If isdownp <> "0" Then
                            If who = "" Then
                                who = "p"
                            Else
                                who = "dp"
                            End If
                        End If
                        SaveStop(who)
                    End If
                End If
            End If


            If chkalert = 0 Then
                CheckActuals(wonum)
                CompWO()
            Else
                Exit Sub
            End If

        End If

    End Sub
    Private Sub SaveStop(ByVal who As String)
        Dim pstr, ph, pm, pa, currt, currd As String

        Dim currdt As Date = wo.CNOW
        Dim eqid As String = lbleqid.Value
        wonum = lblwonum.Value
        sql = "update equipment set isdown = null, wonum = null where wonum = '" & wonum & "' and eqid = '" & eqid & "'; " _
            + "update equipment set isdownp = null, wonump = null where wonump = '" & wonum & "' and eqid = '" & eqid & "'; "
        wo.Update(sql)
        If who = "d" Then
            sql = "update eqhist set stopdown = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update workorder set isdown = '0' where wonum = '" & wonum & "'; " _
        + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
        + "select @stopdate = stopdown, @downdate = startdown from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "set @dmins = datediff(mi, @downdate, @stopdate); " _
        + "set @dhrs = @dmins / 60; " _
        + "update eqhist set totaldown = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
        + "where eqid = '" & eqid & "'"
            Try
                wo.Update(sql)
            Catch ex As Exception

            End Try
        ElseIf who = "p" Then
            sql = "update eqhist set stopdownp = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
            + "update workorder set isdownp = '0' where wonum = '" & wonum & "'; " _
            + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
            + "select @stopdate = stopdownp, @downdate = startdownp from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
            + "set @dmins = datediff(mi, @downdate, @stopdate); " _
            + "set @dhrs = @dmins / 60; " _
            + "update eqhist set totaldownp = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
            + "update equipment set totaldownp = (select sum(totaldownp) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
            + "where eqid = '" & eqid & "'"
            Try
                wo.Update(sql)
            Catch ex As Exception

            End Try
        ElseIf who = "dp" Then
            sql = "update eqhist set stopdown = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update workorder set isdown = '0' where wonum = '" & wonum & "'; " _
        + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
        + "select @stopdate = stopdown, @downdate = startdown from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "set @dmins = datediff(mi, @downdate, @stopdate); " _
        + "set @dhrs = @dmins / 60; " _
        + "update eqhist set totaldown = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
        + "where eqid = '" & eqid & "'"
            Try
                wo.Update(sql)
            Catch ex As Exception

            End Try
            sql = "update eqhist set stopdownp = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
            + "update workorder set isdownp = '0' where wonum = '" & wonum & "'; " _
            + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
            + "select @stopdate = stopdownp, @downdate = startdownp from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
            + "set @dmins = datediff(mi, @downdate, @stopdate); " _
            + "set @dhrs = @dmins / 60; " _
            + "update eqhist set totaldownp = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
            + "update equipment set totaldownp = (select sum(totaldownp) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
            + "where eqid = '" & eqid & "'"
            Try
                wo.Update(sql)
            Catch ex As Exception

            End Try
        End If




    End Sub
    Private Sub CompTPMWO()
        Dim pmid, pmhid, fcust As String

        pmid = lbltpmid.Value
        pmhid = lblpmhid.Value
        fcust = lblfcust.Value
        Dim wonum As String = lblwonum.Value
        coi = lblcoi.Value
        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        Dim cndate As Date = wo.CNOW
        Dim nd As String = lblretday.Value
        Dim cd As Integer
        Dim nnd, days As String
        days = lbldays.Value
        Dim cndt As String = txtn.Value
        Dim cndy As String
        Dim i, n, nn, nnn, tn As Integer
        If days <> "no" Then
            If nd = "" Then
                Try
                    Dim tndi As Integer = System.Convert.ToDateTime(cndt).DayOfWeek
                    Select Case tndi
                        Case "1"
                            nd = "M"
                        Case "2"
                            nd = "Tu"
                        Case "3"
                            nd = "W"
                        Case "4"
                            nd = "Th"
                        Case "5"
                            nd = "F"
                        Case "6"
                            nd = "Sa"
                        Case "0"
                            nd = "Su"
                        Case Else
                            Dim strMessage As String = tmod.getmsg("cdstr365", "PMDivMantpm.aspx.vb")

                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            Exit Sub
                    End Select
                Catch ex As Exception
                    Dim strMessage As String = tmod.getmsg("cdstr366", "PMDivMantpm.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try

            End If
            Select Case nd
                Case "M"
                    cd = 1
                Case "Tu"
                    cd = 2
                Case "T"
                    cd = 2
                Case "W"
                    cd = 3
                Case "Th"
                    cd = 4
                Case "F"
                    cd = 5
                Case "Sa"
                    cd = 6
                Case "Su"
                    cd = 7
                Case Else
                    cd = -1
            End Select

            If cd <> -1 Then
                Dim daysarr() As String = days.Split(",")
                For i = 0 To daysarr.Length - 1
                    If daysarr(i) = nd Then
                        If i < daysarr.Length - 1 Then
                            n = i + 1
                        Else
                            n = 0
                        End If
                        cndy = daysarr(n)
                        Select Case cndy
                            Case "M"
                                nn = 1
                            Case "Tu"
                                nn = 2
                            Case "T"
                                nn = 2
                            Case "W"
                                nn = 3
                            Case "Th"
                                nn = 4
                            Case "F"
                                nn = 5
                            Case "Sa"
                                nn = 6
                            Case "Su"
                                nn = 7
                            Case Else
                                nn = -1
                        End Select
                        If nn <> -1 Then
                            If nn > cd Then
                                nnn = nn - cd 'nnn = days to add
                            ElseIf nn < cd Then
                                tn = 7 - nn
                                nnn = cd + nn
                                nnn = (7 - cd) + nn
                            ElseIf nn = cd Then
                                nnn = 7
                            End If
                            Try
                                nnd = DateAdd(DateInterval.Day, nnn, System.Convert.ToDateTime(cndt))
                            Catch ex As Exception
                                Dim strMessage As String = tmod.getmsg("cdstr367", "PMDivMantpm.aspx.vb")

                                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                                Exit Sub
                            End Try

                        Else
                            'error msg
                            Dim strMessage As String = tmod.getmsg("cdstr368", "PMDivMantpm.aspx.vb")

                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            Exit Sub
                        End If
                        Exit For
                    End If
                Next
            Else
                'error msg
                Dim strMessage As String = tmod.getmsg("cdstr369", "PMDivMantpm.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If

        End If
        Dim pdt, ttt As String
        pdt = lblusetdt.Value
        ttt = lblusetotal.Value
        sql = "usp_comppm1tpm3 '" & pmid & "', '" & pmhid & "', '" & fcust & "', '" & nd & "', '" & cndy & "', '" & nnd & "', '" & wonum & "','" & cndate & "','" & ttt & "', " _
        + "'" & pdt & "','" & ustr & "','" & coi & "'"
        'pmi.Update(sql)
        Dim newdate As String
        'newdate = Now
        Try
            newdate = wo.strScalar(sql)
            newdate = CType(newdate, DateTime)
        Catch ex As Exception
            'this is temp for nissan
            Try
                Dim sql1 As String
                sql1 = "insert into err_tmp (edate, sql) values (getdate(), '" & sql & "')"
                wo.Update(sql1)
                Dim strMessage As String = "Problem Completing PM - Please Contact Your System Administrator"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Catch ex1 As Exception

            End Try
        End Try
        'newdate = CType(newdate, DateTime)

        'added for nissan
        Dim actfin, actsta, actdiff As String
        sql = "select isnull(Convert(char(10),actfinish,101), Convert(char(10),'01/01/1900',101)) as actfin, " _
            + "isnull(Convert(char(10),actstart,101), Convert(char(10),'01/01/1900',101)) as actsta " _
            + "from pmhisttpm where wonum = '" & wonum & "' and pmhid = '" & pmhid & "' and pmid = '" & pmid & "'"
        dr = wo.GetRdrData(sql)
        While dr.Read
            actfin = dr.Item("actfin").ToString
            actsta = dr.Item("actsta").ToString
        End While
        dr.Close()
        'If actfin = "01/01/1900" Then
        'sql = "select isnull(Convert(char(10),actfinish,101), Convert(char(10),'01/01/1900',101)) from workorder where wonum = '" & wonum & "'"
        'Try
        'actfin = pmi.strScalar(sql)
        'Catch ex As Exception
        'actfin = "01/01/1900"
        'End Try
        'End If
        If actfin = "01/01/1900" Then
            sql = "select max(isnull(Convert(char(10),transdate,101),'')) from wolabtrans where wonum = '" & wonum & "'"
            Try
                actfin = wo.strScalar(sql)
            Catch ex As Exception
                actfin = ""
            End Try
            If actfin <> "" Then
                sql = "update pmhisttpm set actfinish = '" & actfin & "' where wonum = '" & wonum & "' and pmhid = '" & pmhid & "' and pmid = '" & pmid & "'"
                wo.Update(sql)
            End If
        Else
            sql = "update pmhisttpm set actfinish = '" & actfin & "' where wonum = '" & wonum & "' and pmhid = '" & pmhid & "' and pmid = '" & pmid & "'"
            wo.Update(sql)
        End If
        If actsta = "01/01/1900" Then
            sql = "select min(isnull(Convert(char(10),transdate,101),'')) from wolabtrans where wonum = '" & wonum & "'"
            Try
                actsta = wo.strScalar(sql)
            Catch ex As Exception
                actsta = ""
            End Try
            If actfin <> "" Then
                sql = "update pmhisttpm set actstart = '" & actsta & "' where wonum = '" & wonum & "' and pmhid = '" & pmhid & "' and pmid = '" & pmid & "'"
                wo.Update(sql)
            End If
        Else
            sql = "update pmhisttpm set actstart = '" & actsta & "' where wonum = '" & wonum & "' and pmhid = '" & pmhid & "' and pmid = '" & pmid & "'"
            wo.Update(sql)
        End If
        'end added


        If coi = "NISS" Then
            Dim nwo As String
            sql = "select max(isnull(wonum, '0')) from workorder where tpmid = '" & pmid & "' and status not in ('COMP','CLOSE','CAN','CANCEL')"
            Try
                nwo = wo.strScalar(sql)
            Catch ex As Exception
                nwo = "0"
            End Try
            If nwo <> "0" Then
                sql = "update workorder set wopriority = 9 where wonum = '" & nwo & "'"
                wo.Update(sql)
            End If
        End If

        'If newdate < Now Then
        'lblalert.Value = "4"
        'Else
        '    lblalert.Value = "3"
        'End If
        Dim won As String = lblwonum.Value
        sql = "usp_upreserved '" & won & "'"
        wo.Update(sql)
        isact = lblisactive.Value
        'If isact = "yes" Then
        'Try
        'Dim ps As String = nsws.SendCant(won)
        'If ps = "1" Then
        'Dim strMessage As String = "Problem with OpenWorkOrder or TimeEntry Transfer - Please Contact Your System Administrator"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'End If
        'Catch ex As Exception

        'End Try
        'End If
        Dim superid As String = lblsup.Value
        If superid <> "" Then
            Try
                Dim mail1 As New pmmail
                mail1.CheckIt("comp", superid, wonum)
            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Sub CompPMWO()
        Dim pmid, pmhid, pdt, ttt As String
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        pdt = lblusetdt.Value
        ttt = lblusetotal.Value
        coi = lblcoi.Value
        Dim wonum As String = lblwonum.Value
        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        Dim cndate As Date = wo.CNOW
        issched = lblissched.Value
        If pmhid = "" And pmid <> "" Then
            Try
                sql = "select max(pmhid) from pmhist where pmid = '" & pmid & "'"
                pmhid = wo.strScalar(sql)
                lblpmhid.Value = pmhid
            Catch ex As Exception
                sql = "insert into pmhist (pmid) values ('" & pmid & "')"
                wo.Update(sql)
                Try
                    sql = "select max(pmhid) from pmhist where pmid = '" & pmid & "'"
                    pmhid = wo.strScalar(sql)
                    lblpmhid.Value = pmhid
                Catch ex1 As Exception

                End Try
            End Try

        End If
        If pmid <> "" And pmhid <> "" Then
            sql = "usp_comppm4 '" & pmid & "', '" & pmhid & "', '" & wonum & "','" & cndate & "','" & ttt & "', " _
            + "'" & pdt & "','" & ustr & "','" & issched & "','" & coi & "'"
            'pmi.Update(sql)
            Dim tst As String = sql
            Dim newdate As String
            Try
                newdate = wo.strScalar(sql)
                newdate = CType(newdate, DateTime)
            Catch ex As Exception
                'this is temp for nissan
                Try

                    Dim sql1 As String
                    sql1 = "insert into err_tmp (edate, sql) values (getdate(), '" & sql & "')"
                    wo.Update(sql1)
                    Dim strMessage As String = "Problem Completing PM - Please Contact Your System Administrator"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Catch ex1 As Exception

                End Try
            End Try

            'added for nissan
            Dim actfin, actsta, actdiff As String
            sql = "select isnull(Convert(char(10),actfinish,101), Convert(char(10),'01/01/1900',101)) as actfin, " _
                + "isnull(Convert(char(10),actstart,101), Convert(char(10),'01/01/1900',101)) as actsta " _
                + "from pmhist where wonum = '" & wonum & "' and pmhid = '" & pmhid & "' and pmid = '" & pmid & "'"
            dr = wo.GetRdrData(sql)
            While dr.Read
                actfin = dr.Item("actfin").ToString
                actsta = dr.Item("actsta").ToString
            End While
            dr.Close()
            'If actfin = "01/01/1900" Then
            'sql = "select isnull(Convert(char(10),actfinish,101), Convert(char(10),'01/01/1900',101)) from workorder where wonum = '" & wonum & "'"
            'Try
            'actfin = pmi.strScalar(sql)
            'Catch ex As Exception
            'actfin = "01/01/1900"
            'End Try
            'End If
            If actfin = "01/01/1900" Then
                sql = "select max(isnull(Convert(char(10),transdate,101),'')) from wolabtrans where wonum = '" & wonum & "'"
                Try
                    actfin = wo.strScalar(sql)
                Catch ex As Exception
                    actfin = ""
                End Try
                If actfin <> "" Then
                    sql = "update pmhist set actfinish = '" & actfin & "' where wonum = '" & wonum & "' and pmhid = '" & pmhid & "' and pmid = '" & pmid & "'"
                    wo.Update(sql)
                End If
            Else
                sql = "update pmhist set actfinish = '" & actfin & "' where wonum = '" & wonum & "' and pmhid = '" & pmhid & "' and pmid = '" & pmid & "'"
                wo.Update(sql)
            End If
            If actsta = "01/01/1900" Then
                sql = "select min(isnull(Convert(char(10),transdate,101),'')) from wolabtrans where wonum = '" & wonum & "'"
                Try
                    actsta = wo.strScalar(sql)
                Catch ex As Exception
                    actsta = ""
                End Try
                If actfin <> "" Then
                    sql = "update pmhist set actstart = '" & actsta & "' where wonum = '" & wonum & "' and pmhid = '" & pmhid & "' and pmid = '" & pmid & "'"
                    wo.Update(sql)
                End If
            Else
                sql = "update pmhist set actstart = '" & actsta & "' where wonum = '" & wonum & "' and pmhid = '" & pmhid & "' and pmid = '" & pmid & "'"
                wo.Update(sql)
            End If
            'end added


            If coi = "NISS" Then
                Dim nwo As String
                sql = "select max(isnull(wonum, '0')) from workorder where pmid = '" & pmid & "' and status not in ('COMP','CLOSE','CAN','CANCEL')"
                Try
                    nwo = wo.strScalar(sql)
                Catch ex As Exception
                    nwo = "0"
                End Try
                If nwo <> "0" Then
                    sql = "update workorder set wopriority = 9 where wonum = '" & nwo & "'"
                    wo.Update(sql)
                End If
            End If

            'If newdate < Now Then
            'lblalert.Value = "4"
            'Else
            '    lblalert.Value = "3"
            'End If
            Dim won As String = lblwonum.Value
            sql = "usp_upreserved '" & won & "'"
            wo.Update(sql)
            isact = lblisactive.Value
            'If isact = "yes" Then
            'Try
            'Dim ps As String = nsws.SendCant(won)
            'If ps = "1" Then
            'Dim strMessage As String = "Problem with OpenWorkOrder or TimeEntry Transfer - Please Contact Your System Administrator"
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'End If
            'Catch ex As Exception

            'End Try
            'End If
            Dim superid As String = lblsup.Value
            If superid <> "" Then
                Try
                    Dim mail1 As New pmmail
                    mail1.CheckIt("comp", superid, wonum)
                Catch ex As Exception

                End Try
            End If
        Else
            Dim strMessage As String = "Problem Completing PM - Please Contact Your System Administrator\nNote: Missing PM ID"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Private Sub SaveTPMTTime(ByVal atime As String, ByVal etime As String)
        Dim pmid, pmhid As String
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        If pmhid = "" And pmid <> "" Then
            Try
                sql = "select max(pmhid) from pmhisttpm where pmid = '" & pmid & "'"
                pmhid = wo.strScalar(sql)
                lblpmhid.Value = pmhid
            Catch ex As Exception
                sql = "insert into pmhisttpm (pmid) values ('" & pmid & "')"
                wo.Update(sql)
                Try
                    sql = "select max(pmhid) from pmhisttpm where pmid = '" & pmid & "'"
                    pmhid = wo.strScalar(sql)
                    lblpmhid.Value = pmhid
                Catch ex1 As Exception

                End Try
            End Try

        End If
        Dim wonum As String = lblwonum.Value
        Dim usetotal As String = lblusetotal.Value
        If usetotal = "yes" Then
            Dim qtychk As Long
            Try
                qtychk = System.Convert.ToDecimal(atime)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr370", "PMDivMantpm.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            sql = "update tpm set acttime = '" & atime & "' where pmid = '" & pmid & "'; " _
            + "update pmhisttpm set acttime = '" & atime & "' where pmhid = '" & pmhid & "'; " _
            + "declare @dmins decimal(10,2), @dhrs decimal(10,2), @emins decimal(10,2), @ehrs decimal(10,2); " _
            + "set @dmins = '" & atime & "'; " _
            + "set @dhrs = @dmins / 60; " _
            + "set @emins = '" & etime & "'; " _
            + "set @ehrs = @emins / 60; " _
            + "update workorder set actlabhrs = @dhrs, estlabhrs = @ehrs where wonum = '" & wonum & "'; "
            wo.Update(sql)
        Else
            Exit Sub
        End If
    End Sub
    Private Sub SavePMTTime(ByVal atime As String, ByVal etime As String)
        Dim pmid, pmhid As String
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        Dim wonum As String = lblwonum.Value
        Dim usetotal As String = lblusetotal.Value
        If usetotal = "yes" Then
            Dim qtychk As Long
            Try
                qtychk = System.Convert.ToDecimal(atime)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr332", "PMDivMan.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            sql = "update pm set acttime = '" & atime & "' where pmid = '" & pmid & "'; " _
            + "update pmhist set acttime = '" & atime & "' where pmhid = '" & pmhid & "'; " _
            + "declare @dmins decimal(10,2), @dhrs decimal(10,2), @emins decimal(10,2), @ehrs decimal(10,2); " _
            + "set @dmins = '" & etime & "'; " _
            + "set @dhrs = @dmins / 60; " _
            + "set @emins = '" & etime & "'; " _
            + "set @ehrs = @emins / 60; " _
            + "update workorder set actlabhrs = @dhrs, estlabhrs = @ehrs where wonum = '" & wonum & "'; "
            wo.Update(sql)
        Else
            Exit Sub
        End If
    End Sub
    Private Sub SaveTTime(ByVal atime As String, ByVal etime As String)
        Dim pmid, pmhid As String
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        Dim wonum As String = lblwonum.Value

        Dim runs, cruns As String
        runs = lblruns.Value
        cruns = lblworuns.Value
        Dim runsi As Integer
        Try
            runsi = CType(runs, Integer)
        Catch ex As Exception
            runs = "1"
            runsi = 1
        End Try
        Dim crunsi As Integer
        Try
            crunsi = CType(cruns, Integer)
        Catch ex As Exception
            cruns = "0"
            crunsi = 0
        End Try

        Dim usetotal As String = lblusetotal.Value
        If usetotal = "yes" Then
            Dim qtychk As Long
            Try
                qtychk = System.Convert.ToDecimal(atime)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr332", "PMDivMan.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            If runs = "1" Then
                sql = "update pm set acttime = '" & atime & "' where pmid = '" & pmid & "'; " _
                + "update pmhist set acttime = '" & atime & "' where pmhid = '" & pmhid & "'; " _
                + "declare @dmins decimal(10,2), @dhrs decimal(10,2), @emins decimal(10,2), @ehrs decimal(10,2); " _
                + "set @dmins = '" & etime & "'; " _
                + "set @dhrs = @dmins / 60; " _
                + "set @emins = '" & etime & "'; " _
                + "set @ehrs = @emins / 60; " _
                + "update workorder set actlabhrspmt = @dhrs, estlabhrs = @ehrs where wonum = '" & wonum & "'; "
                wo.Update(sql)
            Else
                crunsi = crunsi + 1
                sql = "update pm set acttime = '" & atime & "' where pmid = '" & pmid & "'; " _
                + "update pmhist set acttime = '" & atime & "' where pmhid = '" & pmhid & "'; " _
                + "declare @dmins decimal(10,2), @dhrs decimal(10,2), @emins decimal(10,2), @ehrs decimal(10,2); " _
                + "set @dmins = '" & etime & "'; " _
                + "set @dhrs = @dmins / 60; " _
                + "set @emins = '" & etime & "'; " _
                + "set @ehrs = @emins / 60; " _
                + "update workorder set actlabhrspmt = isnull(actlabhrs, 0) + @dhrs, estlabhrs = isnull(estlabhrs, 0) + @ehrs where wonum = '" & wonum & "'; " _
                + "update womultidates set actlabhrspmt = @dhrs, estlabhrs = @ehrs where wonum = '" & wonum & "' and wocnt = '" & crunsi & "'; "
                wo.Update(sql)
            End If

        Else
            Exit Sub
        End If
    End Sub

    Private Sub CheckActuals(ByVal wonum As String)
        Dim icost As String = ap.InvEntry
        If icost <> "ext" And icost <> "inv" Then
            sql = "update workorder set actmatcost = (select isnull(sum(total),0) from woparts where wonum = '" & wonum & "' and used = 'Y') " _
            + "where wonum = '" & wonum & "'; update workorder set acttoolcost = (select isnull(sum(total),0) from wotools where wonum = '" & wonum & "' and used = 'Y') " _
            + "where wonum = '" & wonum & "'; update workorder set actlubecost = (select isnull(sum(cost),0) from wolubes where wonum = '" & wonum & "' and used = 'Y') " _
            + "where wonum = '" & wonum & "'; update workorder set actjpmatcost = (select isnull(sum(total),0) from wojpparts where wonum = '" & wonum & "' " _
            + "and used = 'Y') where wonum = '" & wonum & "'; update workorder set actjptoolcost = (select isnull(sum(total),0) from wojptools where wonum = '" & wonum & "' " _
            + "and used = 'Y') where wonum = '" & wonum & "'; update workorder set actjplubecost = (select isnull(sum(total),0) from wojplubes where wonum = '" & wonum & "' " _
            + "and used = 'Y') where wonum = '" & wonum & "'"
            wo.Update(sql)
        End If
    End Sub
    Private Sub CompWO()
        wonum = lblwonum.Value
        usr = lbluser.Value
        lblstat.Value = "COMP"
        stat = "COMP"
        Dim acomp As String = lblacomp.Value
        Dim currdt As Date = wo.CNOW
        sql = "update workorder set status = 'COMP' where wonum = '" & wonum & "'; " _
            + "insert into wohist (wonum, wostatus, statusdate, changeby) values('" & wonum & "','" & stat & "','" & currdt & "','" & usr & "')"
        wo.Update(sql)
        If acomp = "" Then
            sql = "update workorder set actfinish = '" & currdt & "' where wonum = '" & wonum & "'"
            wo.Update(sql)
        End If
        sql = "usp_upreserved '" & wonum & "'"
        wo.Update(sql)

        isact = lblisactive.Value
        'If isact = "yes" Then
        'Try
        'Dim retwonum As String
        'sql = "select retwonum from workorder where wonum = '" & wonum & "'"
        'dr = wo.GetRdrData(sql)
        'While dr.Read
        'retwonum = dr.Item("retwonum").ToString
        'End While
        'dr.Close()
        'Dim ps As String = ""
        'If retwonum = "" Then
        'ps = nsws.SendCant(wonum)
        'End If
        'If ps = "1" Then
        'Dim strMessage As String = "Problem with OpenWorkOrder or TimeEntry Transfer - Please Contact Your System Administrator"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'End If
        'Catch ex As Exception

        'End Try
        'End If

        Dim superid As String = lblsup.Value
        If superid <> "" Then
            Try
                Dim mail1 As New pmmail
                mail1.CheckIt("comp", superid, wonum)
            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Sub CheckStat()
        wonum = lblwonum.Value
        usr = lbluser.Value
        Dim cstat As String = lblstat.Value
        stat = lblstat.Value
        If stat <> cstat Then
            If stat = "COMP" Then
                CheckWOComp()
            Else
                sql = "update workorder set status = '" & stat & "' where wonum = '" & wonum & "'; " _
                + "insert into wohist (wonum, wostatus, statusdate, changeby) values('" & wonum & "','" & stat & "',getDate(),'" & usr & "')"
                wo.Update(sql)
                lblstat.Value = stat
                tdstat.InnerHtml = stat
                lblmalert.Value = "statchk"
                'SaveWo()
            End If
        End If

    End Sub

End Class