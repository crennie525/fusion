<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wocomp.aspx.vb" Inherits="lucy_r12.wocomp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wocomp</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/wocompaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" width="720">
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="label" width="80">
                            <asp:Label ID="lang1354" runat="server">Target Start:</asp:Label>
                        </td>
                        <td class="plainlabel" id="tdstart" width="100" runat="server">
                        </td>
                        <td class="label" width="80">
                            <asp:Label ID="lang1355" runat="server">Actual Start:</asp:Label>
                        </td>
                        <td class="plainlabel" id="tdactstart" width="100" runat="server">
                            <asp:TextBox ID="txts" runat="server" Width="100px"></asp:TextBox>
                        </td>
                        <td width="30">
                            <img onclick="getcal('s');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                width="19">
                        </td>
                        <td class="label" width="100">
                            <asp:Label ID="lang1356" runat="server">Actual Complete:</asp:Label>
                        </td>
                        <td class="plainlabel" id="tdactfinish" width="100" runat="server">
                            <asp:TextBox ID="txtf" runat="server" Width="100px"></asp:TextBox>
                        </td>
                        <td style="width: 22px" width="22">
                            <img onclick="getcal('f');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                width="19">
                        </td>
                        <td id="tdcomp" align="right" width="80" runat="server">
                            <img id="imgcomp" onclick="measalert();" alt="" src="../images/appbuttons/bgbuttons/submit.gif">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td id="tdwo" runat="server">
            </td>
        </tr>
        <tr>
            <td id="tdjp" runat="server">
            </td>
        </tr>
    </table>
    <input id="lblwo" type="hidden" runat="server">
    <input type="hidden" id="lbltyp" runat="server">
    <input type="hidden" id="lblsubmit" runat="server">
    <input type="hidden" id="lblfmstr" runat="server">
    <input type="hidden" id="lblpstr" runat="server">
    <input type="hidden" id="lbltstr" runat="server">
    <input type="hidden" id="lbllstr" runat="server"><input type="hidden" id="lblmeasstring"
        runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
