<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wradd.aspx.vb" Inherits="lucy_r12.wradd" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wradd</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/wraddaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="checkwo();checkit();">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 0px" cellspacing="0" width="710">
        <tr>
            <td colspan="5">
                <table>
                    <tr>
                        <td class="blue label">
                            <asp:Label ID="lang1589" runat="server">Work Order#</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtwo" runat="server" Width="104px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td>
                            <img onmouseover="return overlib('Create New Work Order With Auto Number', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="getwo();" src="../images/appbuttons/minibuttons/addnew.gif">
                        </td>
                        <td>
                            <asp:ImageButton ID="ibtnsearch" runat="server" CssClass="details" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                            </asp:ImageButton>
                        </td>
                        <td class="details">
                            <asp:Label ID="lang1590" runat="server">Description</asp:Label>
                        </td>
                        <td class="details">
                            <asp:TextBox ID="txtwodesc" runat="server" Width="398px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td class="details">
                            <img onclick="getlong();" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="thdrsinglft">
                <img border="0" src="../images/appbuttons/minibuttons/wohdr.gif">
            </td>
            <td class="thdrsingrt label" colspan="4" align="left">
                <asp:Label ID="lang1591" runat="server">Location\Equipment Information</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <iframe style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                    border-top-style: none; border-left-style: none" id="geteq" height="114" src="wrman.aspx?jump=no&amp;wo=&amp;stat=HOLD&usr="
                    frameborder="no" width="700" allowtransparency scrolling="no" runat="server">
                </iframe>
            </td>
        </tr>
        <tr>
            <td class="thdrsinglft">
                <img border="0" src="../images/appbuttons/minibuttons/wohdr.gif">
            </td>
            <td class="thdrsingrt label" colspan="4" align="left">
                <asp:Label ID="lang1592" runat="server">Work Order Details</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="5" align="center">
                <iframe style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                    border-top-style: none; border-left-style: none" id="ifdet" height="300" src="wrdet.aspx?jump=no&amp;stat=HOLD"
                    frameborder="no" width="700" allowtransparency scrolling="no" runat="server">
                </iframe>
            </td>
        </tr>
        <tr>
            <td width="26">
            </td>
            <td width="144">
            </td>
            <td width="300">
            </td>
            <td width="20">
            </td>
            <td width="200">
            </td>
        </tr>
    </table>
    <div style="z-index: 9999; border-bottom: black 1px solid; border-left: black 1px solid;
        width: 500px; height: 140px; border-top: black 1px solid; border-right: black 1px solid"
        id="subdiv" class="details">
        <table cellspacing="0" cellpadding="0" width="500" bgcolor="white">
            <tr height="20" bgcolor="blue">
                <td class="labelwht">
                    <asp:Label ID="lang1593" runat="server">Long Description</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closelong();" alt="" src="../images/close.gif" width="18" height="18"><br>
                </td>
            </tr>
            <tr class="tbg">
                <td colspan="2">
                    <asp:TextBox ID="txtlongdesc" runat="server" Width="500px" Height="124px" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr class="tbg">
                <td colspan="2" align="right">
                    <asp:ImageButton ID="btnsavetask" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif">
                    </asp:ImageButton>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblwo" type="hidden" name="lblwo" runat="server"><input id="lblcid" type="hidden"
        name="lblcid" runat="server">
    <input id="lblsubmit" type="hidden" name="lblsubmit" runat="server">
    <input id="lbluser" type="hidden" name="lbluser" runat="server">
    <input id="lbljump" type="hidden" name="lbljump" runat="server">
    <input id="lblcnt" type="hidden" name="lblcnt" runat="server">
    <input id="lblsav" type="hidden" name="lblsav" runat="server">
    <input id="lblhref" type="hidden" name="lblhref" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lbldid" type="hidden" name="lbldid" runat="server">
    <input id="lblclid" type="hidden" name="lblclid" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="Hidden1" type="hidden" name="lblcid" runat="server">
    <input id="lblgetarch" type="hidden" name="lblgetarch" runat="server">
    <input id="lbltyp" type="hidden" name="lbltyp" runat="server">
    <input id="lbllid" type="hidden" name="lbllid" runat="server">
    <input id="lblncid" type="hidden" name="lblncid" runat="server">
    <input id="lblro" type="hidden" runat="server"><input id="lblreqd" type="hidden"
        runat="server">
    <input id="lblstatus" type="hidden" runat="server"><input id="lblstart" type="hidden"
        runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
