

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wjsub
    Inherits System.Web.UI.Page
	Protected WithEvents lang1326 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1325 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1324 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1323 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1322 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1321 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1320 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1319 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1318 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1317 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1316 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1315 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sid, cid, fuid, tid, sql, username, eqid, jpid, wonum, ro, rostr As String
    Dim Filter, SubVal As String
    Dim ds As DataSet
    Dim dslev As DataSet
    Dim dr As SqlDataReader
    Dim gtasks As New Utilities
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Login As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dlhd As System.Web.UI.WebControls.DataList
    Protected WithEvents addtask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dgtasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents tdtnum As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tddesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdskill As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdqty As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmin As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltool As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllube As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnote As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
            username = HttpContext.Current.Session("username").ToString()
            lblusername.Value = username
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            'Try
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro <> "1" Then
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = gtasks.CheckROS(rostr, "wo")
                    lblro.Value = ro
                End If
            End If
            tid = Request.QueryString("tid").ToString
            lbltid.Value = tid
            If Len(tid) <> 0 AndAlso tid <> "" AndAlso tid <> "0" Then
                jpid = Request.QueryString("jpid").ToString
                lbljpid.Value = jpid
                wonum = Request.QueryString("wo").ToString
                lblwo.Value = wonum
                Filter = "jpid = '" & jpid & "' and tasknum = '" & tid & "'"
                lblfilt.Value = Filter
                SubVal = "(compid, funcid, tasknum, subtask) values ('" & cid & "', '" & fuid & "', "
                lblsubval.Value = SubVal
                gtasks.Open()
                BindTaskHead()
                BindHead()
                BindGrid()
                Dim lock, lockby As String
                Dim user As String = lblusername.Value
                lock = "0" 'CheckLock(eqid)
                If lock = "1" Then
                    lockby = lbllockedby.Value
                    If lockby = user Then
                        lbllock.Value = "0"
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr549", "wjsub.aspx.vb") & " " & lockby & "."
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        addtask.Attributes.Add("class", "details")
                    End If
                ElseIf lock = "0" Then
                    'LockRecord(user, eq)
                End If
                gtasks.Dispose()
                lbltool.Value = "no"
                lblpart.Value = "no"
                lbllube.Value = "no"
                lblnote.Value = "no"
            Else
                Dim strMessage As String = tmod.getmsg("cdstr550" , "wjsub.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "noeqid"
            End If

            'Catch ex As Exception
            'Dim strMessage As String =  tmod.getmsg("cdstr551" , "wjsub.aspx.vb")
 
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'lbllog.Value = "noeqid"
            'End Try

        End If

        'btnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'btnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Function CheckLock(ByVal eqid As String) As String
        Dim lock As String
        sql = "select locked, lockedby from equipment where eqid = '" & eqid & "'"
        Try
            dr = gtasks.GetRdrData(sql)
            While dr.Read
                lock = dr.Item("locked").ToString
                lbllock.Value = dr.Item("locked").ToString
                lbllockedby.Value = dr.Item("lockedby").ToString
            End While
            dr.Close()
        Catch ex As Exception

        End Try
        Return lock
    End Function
    Private Sub BindHead()
        jpid = lbljpid.Value
        tid = lbltid.Value
        wonum = lblwo.Value
        sql = "select p.deptid, d.dept_line, p.cellid, l.cell_name, p.eqid, " _
        + "e.eqnum, p.funcid, f.func, c.compnum from wojobtasks t " _
        + "left join wojobplans p on p.jpid = t.jpid " _
        + "right join dept d on d.dept_id = p.deptid " _
        + "right join cells l on l.cellid = p.cellid " _
        + "right join equipment e on e.eqid = p.eqid " _
        + "right join functions f on f.func_id = p.funcid " _
        + "right join components c on c.comid = p.comid " _
        + "where t.jpid = '" & jpid & "' and t.tasknum = '" & tid & "' and t.subtask = 0 and t.wonum = '" & wonum & "'"
        dr = gtasks.GetRdrData(sql)
        dlhd.DataSource = dr
        dlhd.DataBind()
        dr.Close()
    End Sub
    Private Sub BindTaskHead()
        jpid = lbljpid.Value
        tid = lbltid.Value
        wonum = lblwo.Value
        sql = "select * from wojobtasks where jpid = '" & jpid & "' and tasknum = '" & tid & "' and subtask = 0 and wonum = '" & wonum & "'"
        dr = gtasks.GetRdrData(sql)
        While dr.Read
            tdtnum.InnerHtml = dr.Item("tasknum").ToString
            tddesc.InnerHtml = dr.Item("taskdesc").ToString
            tdskill.InnerHtml = dr.Item("skill").ToString
            tdqty.InnerHtml = dr.Item("qty").ToString
            tdmin.InnerHtml = dr.Item("ttime").ToString
            'tdrd.InnerHtml = dr.Item("rd").ToString
            'tdrdt.InnerHtml = dr.Item("rdt").ToString

        End While
        dr.Close()
    End Sub
    Private Sub BindGrid()
        jpid = lbljpid.Value
        tid = lbltid.Value
        wonum = lblwo.Value
        sql = "select * from wojobtasks where jpid = '" & jpid & "' and tasknum = '" & tid & "' and subtask <> 0 and wonum = '" & wonum & "' order by subtask"
        ds = gtasks.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Try
            dgtasks.DataSource = dv
            dgtasks.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Public Function PopulateFail(ByVal comid As String) As DataSet
        cid = lblcid.Value
        sql = "select failid, failuremode from componentfailmodes where comid = '" & comid & "' or failindex = '0' order by failuremode"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Public Function PopulatePreTech() As DataSet
        cid = lblcid.Value
        sql = "select ptid, pretech, ptindex from pmPreTech where compid = '" & cid & "' or ptindex = '0' order by ptindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Public Function PopulateComp() As DataSet
        fuid = lblfuid.Value
        sql = "select * from components where func_id = '" & fuid & "' or compindex = '0' order by compindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Public Function PopulateTaskTypes() As DataSet
        cid = lblcid.Value
        sql = "select ttid, tasktype, taskindex from pmTaskTypes where compid = '" & cid & "' or taskindex = '0' order by taskindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function

    Public Function PopulateStatus() As DataSet
        cid = lblcid.Value
        sql = "select statid, status, statusindex from pmStatus where compid = '" & cid & "' or statusindex = '0' order by statusindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    
    Public Function PopulateSkills() As DataSet
        cid = lblcid.Value
        sid = lblsid.Value
        sql = "select skillid, skill, skillindex from pmSiteSkills where (compid = '" & cid & "' and siteid = '" & sid & "') or skillindex = '0' order by skillindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function
    Function PopulateCompFM(ByVal comp As String)
        If comp <> "0" Then
            sql = "select failid, failuremode " _
                     + "from componentfailmodes where comid = '" & comp & "'"
            dslev = gtasks.GetDSData(sql)
            Return dslev
        End If

    End Function
    Function PopulateFL(ByVal comp As String)
        If comp <> "0" Then
            sql = "select failid, failuremode " _
                    + "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
                    + "select failid from wojpfailmodes where comid = '" & comp & "')"
            dslev = gtasks.GetDSData(sql)
            Return dslev
        End If

    End Function
    Function PopulateTaskFM(ByVal comp As String, ByVal ttid As String)
        If comp <> "0" Then
            sql = "select * from wojpfailmodes where wojtid = '" & ttid & "'"
            dslev = gtasks.GetDSData(sql)
            Return dslev
        End If
    End Function

    Private Sub addtask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles addtask.Click
        jpid = lbljpid.Value
        tid = tdtnum.InnerHtml
        wonum = lblwo.Value
        gtasks.Open()
        Dim stcnt As Integer
        sql = "Select count(*) from wojobTasks " _
        + "where jpid = '" & jpid & "' and tasknum = '" & tid & "' and subtask <> 0 and wonum = '" & wonum & "'"
        stcnt = gtasks.Scalar(sql)
        Dim newtst As String = stcnt + 1

        
        If wonum <> "" Then
            sql = "insert into wojobtasks (jpid, wonum, tasknum, subtask, " _
            + "skillid, skillindex, skill, rdid, rd, rdindex) select distinct " _
            + "jpid, wonum, '" & tid & "', '" & newtst & "', " _
            + "skillid, skillindex, skill, rdid, rd, rdindex from wojobtasks " _
            + "where jpid = '" & jpid & "' and tasknum = '" & tid & "' and wonum = '" & wonum & "' and subtask = 0"
            gtasks.Update(sql)
        End If
        BindGrid()
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.EditCommand
        Dim lock As String = lbllock.Value
        If lock <> "1" Then
            addtask.Enabled = False
            gtasks.Open()
            lbloldtask.Value = CType(e.Item.FindControl("lblsubt"), Label).Text
            dgtasks.EditItemIndex = e.Item.ItemIndex
            BindGrid()
            gtasks.Dispose()
        End If
    End Sub

    Private Sub dgtasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.CancelCommand
        addtask.Enabled = True
        gtasks.Open()
        dgtasks.EditItemIndex = -1
        BindGrid()
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.UpdateCommand
        gtasks.Open()
        addtask.Enabled = True
        Dim tn, otn, txt, qty, dt, desc, st, tid As String
        qty = CType(e.Item.FindControl("txtqty"), TextBox).Text
        txt = CType(e.Item.FindControl("txttr"), TextBox).Text
        dt = CType(e.Item.FindControl("txtdt"), TextBox).Text
        desc = CType(e.Item.FindControl("txtdesc"), TextBox).Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", ":", , , vbTextCompare)
        st = CType(e.Item.FindControl("lblst"), TextBox).Text
        tid = CType(e.Item.FindControl("lblttid"), Label).Text
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToInt64(qty)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr552" , "wjsub.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        Dim txtchk As Long
        Try
            txtchk = System.Convert.ToDecimal(txt)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr553" , "wjsub.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        txt = qtychk * txtchk
        Dim dtchk As Long
        'Try
        'dtchk = System.Convert.ToDecimal(dt)
        'Catch ex As Exception
        'Dim strMessage As String =  tmod.getmsg("cdstr554" , "wjsub.aspx.vb")
 
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ' Sub
        'End Try

        wonum = lblwo.Value
        '+ "rdt = '" & dt & "', " _
        If wonum <> "" Then
            sql = "update wojobtasks set qty = '" & qty & "', " _
            + "ttime = '" & txt & "', " _
            + "taskdesc = '" & desc & "' " _
            + "where wojtid = '" & tid & "'"
            gtasks.Update(sql)
        End If
        tn = tdtnum.InnerHtml
        otn = lbloldtask.Value
        If otn <> st Then
            jpid = lbljpid.Value
            sql = "usp_reorderwJPSubTasks '" & jpid & "', '" & tn & "', '" & st & "', '" & otn & "', ' & wonum & " '"
            gtasks.Update(sql)
        End If
        eqid = lbleqid.Value
        'gtasks.UpMod(eqid)
        dgtasks.EditItemIndex = -1
        BindGrid()
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.DeleteCommand
        Dim tnum, snum
        'ListItemType.SelectedItem()
        Try
            tnum = CType(e.Item.FindControl("lblta"), Label).Text
            snum = CType(e.Item.FindControl("lblsubt"), Label).Text
        Catch ex As Exception
            tnum = CType(e.Item.FindControl("lblt"), TextBox).Text
            snum = CType(e.Item.FindControl("lblst"), TextBox).Text
        End Try
        'If e.Item.ItemType = ListItemType.Item Then

        'ElseIf e.Item.ItemType = ListItemType.EditItem Then

        'End If
        wonum = lblwo.Value
        jpid = lbljpid.Value
        sql = "usp_delwJPTask '" & jpid & "', '" & tnum & "', '" & wonum & "','" & snum & "'"
        gtasks.Open()
        gtasks.Update(sql)
        eqid = lbleqid.Value
        'gtasks.UpMod(eqid)
        Try
            dgtasks.EditItemIndex = -1
        Catch ex As Exception

        End Try
        BindGrid()
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.ItemCommand
        If e.CommandName = "Tool" Then
            lbltool.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If

        End If
        If e.CommandName = "Part" Then
            lblpart.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        If e.CommandName = "Lube" Then
            lbllube.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgtasks.Columns(0).HeaderText = dlabs.GetDGPage("wjsub.aspx", "dgtasks", "0")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(2).HeaderText = dlabs.GetDGPage("wjsub.aspx", "dgtasks", "2")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(3).HeaderText = dlabs.GetDGPage("wjsub.aspx", "dgtasks", "3")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(4).HeaderText = dlabs.GetDGPage("wjsub.aspx", "dgtasks", "4")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(5).HeaderText = dlabs.GetDGPage("wjsub.aspx", "dgtasks", "5")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(7).HeaderText = dlabs.GetDGPage("wjsub.aspx", "dgtasks", "7")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1315.Text = axlabs.GetASPXPage("wjsub.aspx", "lang1315")
        Catch ex As Exception
        End Try
        Try
            lang1316.Text = axlabs.GetASPXPage("wjsub.aspx", "lang1316")
        Catch ex As Exception
        End Try
        Try
            lang1317.Text = axlabs.GetASPXPage("wjsub.aspx", "lang1317")
        Catch ex As Exception
        End Try
        Try
            lang1318.Text = axlabs.GetASPXPage("wjsub.aspx", "lang1318")
        Catch ex As Exception
        End Try
        Try
            lang1319.Text = axlabs.GetASPXPage("wjsub.aspx", "lang1319")
        Catch ex As Exception
        End Try
        Try
            lang1320.Text = axlabs.GetASPXPage("wjsub.aspx", "lang1320")
        Catch ex As Exception
        End Try
        Try
            lang1321.Text = axlabs.GetASPXPage("wjsub.aspx", "lang1321")
        Catch ex As Exception
        End Try
        Try
            lang1322.Text = axlabs.GetASPXPage("wjsub.aspx", "lang1322")
        Catch ex As Exception
        End Try
        Try
            lang1323.Text = axlabs.GetASPXPage("wjsub.aspx", "lang1323")
        Catch ex As Exception
        End Try
        Try
            lang1324.Text = axlabs.GetASPXPage("wjsub.aspx", "lang1324")
        Catch ex As Exception
        End Try
        Try
            lang1325.Text = axlabs.GetASPXPage("wjsub.aspx", "lang1325")
        Catch ex As Exception
        End Try
        Try
            lang1326.Text = axlabs.GetASPXPage("wjsub.aspx", "lang1326")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                addtask.Attributes.Add("src", "../images2/eng/bgbuttons/addtask.gif")
            ElseIf lang = "fre" Then
                addtask.Attributes.Add("src", "../images2/fre/bgbuttons/addtask.gif")
            ElseIf lang = "ger" Then
                addtask.Attributes.Add("src", "../images2/ger/bgbuttons/addtask.gif")
            ElseIf lang = "ita" Then
                addtask.Attributes.Add("src", "../images2/ita/bgbuttons/addtask.gif")
            ElseIf lang = "spa" Then
                addtask.Attributes.Add("src", "../images2/spa/bgbuttons/addtask.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
