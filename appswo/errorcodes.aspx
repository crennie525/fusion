<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="errorcodes.aspx.vb" Inherits="lucy_r12.errorcodes" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>errorcodes</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        <!--
        function getecd1(ecc) {
            document.getElementById("lblecd1").value = ecc;
            document.getElementById("lblsubmit").value = "ecd1";
            document.getElementById("form1").submit();
        }
        function getecd2(ecc) {
            document.getElementById("lblecd2").value = ecc;
            document.getElementById("lblsubmit").value = "ecd2";
            document.getElementById("form1").submit();
        }
        function getecd3(ecc) {
            document.getElementById("lblecd3").value = ecc;
            document.getElementById("tdecd3").innerHTML = ecc;
            document.getElementById("lblsubmit").value = "ecd3";
            document.getElementById("trsave").className = "view";
            //document.getElementById("form1").submit();
        }
        function refit() {
            window.location = "errorcodes.aspx";
        }
        function retit() {
            var ret1 = document.getElementById("lblecd1").value;
            var ret2 = document.getElementById("lblecd2").value;
            var ret3 = document.getElementById("lblecd3").value;
            if (ret1 != "" && ret2 != "" && ret3 != "") {
                document.getElementById("lblsubmit").value = "savewo";
                document.getElementById("form1").submit();
            }
        }
        function checkit() {
            var chk = document.getElementById("lblsubmit").value;
            if (chk == "return") {
                goback();
            }
        }
        function goback() {
            var ret;
            var ret1 = document.getElementById("lblecd1").value;
            var ret2 = document.getElementById("lblecd2").value;
            var ret3 = document.getElementById("lblecd3").value;
            ret = ret1 + "~" + ret2 + "~" + ret3;
            window.parent.handlereturn(ret);
            //alert(ret)
        }
        //-->
    </script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td id="tdec" runat="server">
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="bluelabel" height="20" width="80">
                            Error Code 1
                        </td>
                        <td class="plainlabel" id="tdecd1" runat="server" width="150">
                        </td>
                        <td width="20">
                            <img src="../images/appbuttons/minibuttons/refreshit.gif" onclick="refit();">
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel" height="20">
                            Error Code 2
                        </td>
                        <td class="plainlabel" id="tdecd2" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel" height="20">
                            Error Code 3
                        </td>
                        <td class="plainlabel" id="tdecd3" runat="server">
                        </td>
                    </tr>
                    <tr id="trsave" runat="server" class="details">
                        <td colspan="3" align="right">
                            <img src="../images/appbuttons/minibuttons/savedisk1.gif" onclick="retit();">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblsubmit" runat="server">
    <input type="hidden" id="lblecd1" runat="server">
    <input type="hidden" id="lblecd2" runat="server">
    <input type="hidden" id="lblecd3" runat="server">
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lblwonum" runat="server">
    <input type="hidden" id="lbltyp" runat="server">
    </form>
</body>
</html>
