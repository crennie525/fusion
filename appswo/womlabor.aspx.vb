Imports System.Data.SqlClient
Imports System.Text
Public Class womlabor
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim sql As String
    Dim dr As SqlDataReader
    Dim assn As New Utilities
    Dim supid, sup, cid, typ, sid, ro, skill, srch, wonum, estlh, labhrs, shift, name, estart, eend, las, issched, pmmode As String
    Dim islead, waid, workarea, skillid, worktype, pmid As String
    Dim isflg As Integer
    Dim Filter As String
    Protected WithEvents lang2986 As System.Web.UI.WebControls.Label
    Protected WithEvents ddshift As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lang2988 As System.Web.UI.WebControls.Label
    Protected WithEvents lblawo As System.Web.UI.WebControls.Label
    Protected WithEvents btntocomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnfromcomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblopt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsupid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lang2984 As System.Web.UI.WebControls.Label
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lang2985 As System.Web.UI.WebControls.Label
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents trsrch As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trskill As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblestlh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdwonum As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdest As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txthrs As System.Web.UI.WebControls.TextBox
    Protected WithEvents cbsup As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents tdass As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblstart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents supdiv As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents supdiv1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents txtsup As System.Web.UI.WebControls.TextBox
    Protected WithEvents imgsuper As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblass As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdwa As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblworkarea As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwaid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdeststart As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdestcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblvals As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblreturn As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblvalsvar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrvals As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrvalsvar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbwa As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents tdschedstart As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdschedcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldtyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisflg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdmsg1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnoemps As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdestm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbusemin As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmsg2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgsstart As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgscomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblworktype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblestup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblissched As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllabordate As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbusestart As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbusecomp As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents tdlabordate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trnosched As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lbluseown As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdpmswitch As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trpmswitch As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblmode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdacwa As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdacs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblfbs As System.Web.UI.WebControls.Label
    Protected WithEvents lblfbwa As System.Web.UI.WebControls.Label
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdelh As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdelm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdalh As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlhe As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblum As System.Web.UI.WebControls.Label
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        Dim lang As String = lblfslang.Value
        poplang(lang)

        If Not IsPostBack Then

            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            Try
                pmmode = Request.QueryString("mode").ToString
                lblmode.Value = pmmode
            Catch ex As Exception
                pmmode = ""
            End Try
            lblro.Value = ro
            If ro = "1" Then
                'cbopts.Enabled = False
                btntocomp.Visible = False
                btnfromcomp.Visible = False
                todis.Attributes.Add("class", "view")
                fromdis.Attributes.Add("class", "view")
            End If
            issched = Request.QueryString("issched").ToString
            lblissched.Value = issched
            If issched = "0" Then
                cbusestart.Attributes.Add("class", "view")
                cbusecomp.Attributes.Add("class", "view")
                trnosched.Attributes.Add("class", "view")
                cbusestart.Checked = True
            Else
                cbusestart.Attributes.Add("class", "details")
                cbusecomp.Attributes.Add("class", "details")
                trnosched.Attributes.Add("class", "details")
            End If
            lblsid.Value = Request.QueryString("sid").ToString '"12" ' 'HttpContext.Current.Session("dfltps").ToString
            typ = Request.QueryString("typ").ToString '"wo" '
            lbltyp.Value = typ
            wonum = Request.QueryString("wo").ToString '"1000"
            lblwonum.Value = wonum
            tdwonum.InnerHtml = wonum
            cid = "0"
            lblcid.Value = cid
            lblopt.Value = "0"
            If typ = "pm" Then
                pmid = Request.QueryString("pmid").ToString
                lblpmid.Value = pmid
                imgsstart.Attributes.Add("class", "details")
                imgscomp.Attributes.Add("class", "details")
                trnosched.Attributes.Add("class", "details")
            End If
            assn.Open()
            GetWo(wonum)

            assn.Dispose()
        Else
            If Request.Form("lblsubmit") = "remsup" Then
                lblsubmit.Value = ""
                wonum = lblwonum.Value
                assn.Open()
                remsup(wonum)
                PopUnAssigned(estart, eend)
                PopSuperAssigned(wonum)
                PopFields()
                assn.Dispose()
            ElseIf Request.Form("lblsubmit") = "reload" Then
                lblsubmit.Value = ""
                wonum = lblwonum.Value
                assn.Open()
                GetWo(wonum)
                PopUnAssigned(estart, eend)
                PopSuperAssigned(wonum)
                PopFields()
                assn.Dispose()
            ElseIf Request.Form("lblsubmit") = "remwa" Then
                lblsubmit.Value = ""
                wonum = lblwonum.Value
                assn.Open()
                remwa(wonum)
                PopUnAssigned(estart, eend)
                PopSuperAssigned(wonum)
                PopFields()
                assn.Dispose()
            ElseIf Request.Form("lblsubmit") = "changesup" Then
                lblsubmit.Value = ""
                wonum = lblwonum.Value
                estart = lblstart.Value
                eend = lblcomp.Value
                assn.Open()
                changesup(wonum)
                PopUnAssigned(estart, eend)
                PopSuperAssigned(wonum)
                PopFields()
                assn.Dispose()
            ElseIf Request.Form("lblsubmit") = "changewa" Then
                lblsubmit.Value = ""
                wonum = lblwonum.Value
                estart = lblstart.Value
                eend = lblcomp.Value
                assn.Open()
                changewa(wonum)
                PopUnAssigned(estart, eend)
                PopSuperAssigned(wonum)
                PopFields()
                assn.Dispose()
            ElseIf Request.Form("lblsubmit") = "towo" Then
                lblsubmit.Value = ""
                wonum = lblwonum.Value
                estart = lblstart.Value
                eend = lblcomp.Value
                assn.Open()
                towo(wonum)
                PopUnAssigned(estart, eend)
                PopSuperAssigned(wonum)
                PopFields()
                GetAssigned(wonum)
                assn.Dispose()
            ElseIf Request.Form("lblsubmit") = "fromwo" Then
                lblsubmit.Value = ""
                wonum = lblwonum.Value
                estart = lblstart.Value
                eend = lblcomp.Value
                assn.Open()
                fromwo(wonum)
                PopUnAssigned(estart, eend)
                PopSuperAssigned(wonum)
                PopFields()
                GetAssigned(wonum)
                assn.Dispose()
            ElseIf Request.Form("lblsubmit") = "upemps" Then
                lblsubmit.Value = ""
                wonum = lblwonum.Value
                estart = lblstart.Value
                eend = lblcomp.Value
                assn.Open()
                PopUnAssigned(estart, eend)
                PopSuperAssigned(wonum)
                PopFields()
                GetAssigned(wonum)
                assn.Dispose()
            End If
        End If
    End Sub
    Private Sub poplang(ByVal lang As String)
        If lang = "fre" Then
            lang2984.Text = "Chercher par nom"
            lang2985.Text = "Filtrer par comp�tence"
            lang2986.Text = "Filtrer par quart de travail"
            tdacwa.InnerHtml = "Assigner/Changer d'aire de travail"
            tdacs.InnerHtml = "Assigner/Changer de superviseur"
            lblfbs.Text = "Filtrer par superviseur"
            lblfbwa.Text = "Filtrer par aire de travail"
            tdwo.InnerHtml = "# de Bon de Travail"
            tdelh.InnerHtml = "Heures de travail estim�es"
            tdelm.InnerHtml = "Minutes de travail estim�es"
            tdalh.InnerHtml = "Heures de travail assign�es"
            tdlhe.InnerHtml = "Entr�e des heures de travail"
            lblum.Text = "Utiliser des minutes"
            Td1.InnerHtml = "Entr�e de temps de travail non obligatoire pour assigner un bon de travail. Peut �tre �dit� plus tard"
            lang2988.Text = "Disponible"
            lblawo.Text = "Assign� au Bon de Travail"




        End If
    End Sub

    Private Sub GetWo(ByVal wonum As String)
        'supid, sup, estlh, skill, skillid, estart, eend, waid, workarea)
        typ = lbltyp.Value
        pmid = lblpmid.Value
        pmmode = lblmode.Value
        wonum = lblwonum.Value
        If wonum <> "" And pmmode <> "pm" Then
            sql = "select w.superid, w.supervisor, w.skill, w.skillid, w.waid, a.workarea, w.estlabhrs, w.worktype, w.pmid, " _
        + "Convert(char(10),w.schedstart,101) as schedstart, Convert(char(10),w.schedfinish,101) as schedfinish, " _
        + "Convert(char(10),w.targstartdate,101) as targstartdate, Convert(char(10),w.targcompdate,101) as targcompdate " _
        + "from workorder w left join workareas a on a.waid = w.waid " _
        + "where wonum = '" & wonum & "'"
        Else
            sql = "select w.superid, w.super as supervisor, w.skill, w.skillid, '' as waid, '' as workarea, w.ttime as estlabhrs, 'PM' as worktype, w.pmid, " _
            + "'' as schedstart, '' as schedfinish, " _
            + "Convert(char(10),w.nextdate,101) as targstartdate, Convert(char(10),w.nextdate,101) as targcompdate " _
            + "from pm w where pmid = '" & pmid & "'"
        End If
        Dim sstart, send, tstart, tend, astart, aend As String
        dr = assn.GetRdrData(sql)
        While dr.Read
            supid = dr.Item("superid").ToString
            sup = dr.Item("supervisor").ToString

            skill = dr.Item("skill").ToString
            skillid = dr.Item("skillid").ToString

            waid = dr.Item("waid").ToString
            workarea = dr.Item("workarea").ToString

            estlh = dr.Item("estlabhrs").ToString

            sstart = dr.Item("schedstart").ToString
            send = dr.Item("schedfinish").ToString

            tstart = dr.Item("targstartdate").ToString
            tend = dr.Item("targcompdate").ToString

            pmid = dr.Item("pmid").ToString
            worktype = dr.Item("worktype").ToString

        End While
        dr.Close()

        lblpmid.Value = pmid
        If pmid <> "" And wonum = "" Then
            trpmswitch.Attributes.Add("class", "view")
            Dim chk As String = lblmode.Value
            If chk = "" Then
                lblmode.Value = "pm"
            End If
        ElseIf pmid <> "" And wonum <> "" Then
            trpmswitch.Attributes.Add("class", "view")
            Dim chk As String = lblmode.Value
            If chk = "" Then
                lblmode.Value = "wo"
            End If
        Else
            trpmswitch.Attributes.Add("class", "details")
            lblmode.Value = "woonly"
        End If
        lblworktype.Value = worktype
        lblsupid.Value = supid
        lblsup.Value = sup
        txtsup.Text = sup

        'lblestlh.Value = estlh
        'tdest.InnerHtml = estlh
        Dim estlh1 As Decimal
        Try
            estlh1 = CType(estlh, Decimal)
        Catch ex As Exception
            estlh1 = 0
        End Try

        If typ = "pm" Then 'And wonum = ""
            Try
                estlh1 = Math.Round(estlh / 60, 2)
                lblestlh.Value = estlh1
                tdest.InnerHtml = estlh1
            Catch ex As Exception

            End Try
        Else
            lblestlh.Value = Math.Round(estlh1, 2)
            tdest.InnerHtml = Math.Round(estlh1, 2)
        End If

        Dim estlm As Decimal
        Try
            If typ = "pm" Then 'And wonum = ""
                estlm = CType(estlh1, Decimal)
                estlm = estlh 'estlm * 60
                tdestm.InnerHtml = estlh 'estlm
            Else
                estlm = CType(estlh, Decimal)
                estlm = Math.Round(estlm * 60, 2)
                tdestm.InnerHtml = estlm
            End If

        Catch ex As Exception

        End Try

        lblwaid.Value = waid
        lblworkarea.Value = workarea
        tdwa.InnerHtml = workarea

        If supid <> "" And supid <> "0" Then
            cbsup.Checked = True
        Else
            cbsup.Checked = False
        End If
        If waid <> "" Then
            cbwa.Checked = True
        Else
            cbwa.Checked = False
        End If
        Dim lang As String
        lang = lblfslang.Value
        If sstart = "" Then
            tdeststart.InnerHtml = tstart
            lblstart.Value = tstart
            tdschedstart.Attributes.Add("class", "bgyl redlabel")
            If lang = "fre" Then
                tdschedstart.InnerHtml = "Date de d�but pr�vue"
            Else
                tdschedstart.InnerHtml = "Target Start Date"
            End If

            lbldtyp.Value = "t"
            sstart = tstart
        Else
            tdeststart.InnerHtml = sstart
            lblstart.Value = sstart
            tdschedstart.Attributes.Add("class", "bluelabel")
            If lang = "fre" Then
                tdschedstart.InnerHtml = "Date de d�but pr�vue"
            Else
                tdschedstart.InnerHtml = "Scheduled Start Date"
            End If

            lbldtyp.Value = "s"
        End If

        If send = "" Then
            tdestcomp.InnerHtml = tend
            lblcomp.Value = tend
            tdschedcomp.Attributes.Add("class", "bgyl redlabel")
            If lang = "fre" Then
                tdschedcomp.InnerHtml = "Date de fin pr�vue"
            Else
                tdschedcomp.InnerHtml = "Target Complete Date"
            End If

            send = tend
        Else
            tdestcomp.InnerHtml = send
            lblcomp.Value = send
            tdschedcomp.Attributes.Add("class", "bluelabel")
            If lang = "fre" Then
                tdschedcomp.InnerHtml = "Date de fin pr�vue"
            Else
                tdschedcomp.InnerHtml = "Scheduled Complete Date"
            End If

        End If
        Dim useown As String = lbluseown.Value
        If useown <> "yes" Then
            If cbusestart.Checked = True Then
                lbllabordate.Value = sstart
                tdlabordate.InnerHtml = sstart
            ElseIf cbusecomp.Checked = True Then
                lbllabordate.Value = send
                tdlabordate.InnerHtml = send
            Else
                lbllabordate.Value = sstart
                tdlabordate.InnerHtml = sstart
            End If
        Else
            cbusestart.Checked = False
            cbusecomp.Checked = False
            tdlabordate.InnerHtml = lbllabordate.Value
        End If


        estart = sstart
        eend = send
        'End If


        LoadSkills()
        GetAssigned(wonum)
        PopUnAssigned(estart, eend)
        PopSuperAssigned(wonum)

    End Sub
    Private Sub fromwo(ByVal wonum As String)
        pmid = lblpmid.Value
        issched = lblissched.Value
        Dim vals As String = lblrvals.Value
        Dim valsar() As String = vals.Split("~")
        Dim vals1 As String
        Dim i As Integer
        'remsup('" & supid & "', '" & name & "','" & wdate & "','" & assigned & "','" & islead & "','" & trnme & "','" & wid & "','" & shift & "
        For i = 0 To valsar.Length - 1
            vals1 = valsar(i).ToString
            Dim vals1ar() As String = vals1.Split(",")
            Dim t As String = vals1ar(0)
            Dim t1 As String = vals1ar(1)
            Dim t2 As String = vals1ar(2)
            Dim t3 As String = vals1ar(3)
            Dim t4 As String = vals1ar(4)
            Dim t5 As String = vals1ar(5)
            Dim t6 As String = vals1ar(6)
            Dim t7 As String = vals1ar(7)
            If t3 = "" Then
                t3 = "0"
            End If
            '" & supid & "', '" & name & "','" & wdate & "','" & assigned & "','" & islead & "','" & trnme & "','" & wid & "','" & shift & "'
            Dim tsql As String = ""
            pmmode = lblmode.Value
            If t4 = "0" Or issched = "0" Then
                If wonum <> "" And pmmode <> "pm" Then
                    sql = "update workorder set leadcraft = null, leadcraftid = null " _
               + "where wonum = '" & wonum & "'"
                    assn.Update(sql)
                ElseIf pmmode = "pm" Then
                    sql = "update pm set lead = null, leadid = null " _
               + "where pmid = '" & pmid & "'"
                    assn.Update(sql)
                End If

            End If
            If wonum <> "" And pmmode <> "pm" Then
                sql = "delete from woassign " _
               + "where woassignid = '" & t6 & "'"
                assn.Update(sql)
            Else
                sql = "delete from pmassign " _
               + "where pmassignid = '" & t6 & "'"
                assn.Update(sql)
                sql = "select woassignid from woassign where wonum = '" & wonum & "' and userid = '" & t & "'"
                t6 = assn.strScalar(sql)
                sql = "delete from woassign " _
               + "where woassignid = '" & t6 & "'"
                assn.Update(sql)
            End If
            Dim cmd As New SqlCommand

            If issched = "1" Then
                If t2 <> "" Then 't2 <> "N/A" And t2 <> "" Then
                    'Dim time2 As DateTime = DateTime.Parse(t2)
                    cmd.CommandText = "exec usp_addlabavail_bk @t3, @t, @t2"
                    '"update labavail set assigned = assigned - @t3, " _
                    ' + "avail = avail + @t3 where " _
                    '+ "userid = @t " _
                    '+ "and Convert(char(10),wdate,101) = Convert(char(10),@t2,101); "
                    Dim param = New SqlParameter("@t3", SqlDbType.VarChar)
                    param.Value = t3
                    cmd.Parameters.Add(param)
                    Dim param1 = New SqlParameter("@t", SqlDbType.VarChar)
                    param1.Value = t
                    cmd.Parameters.Add(param1)
                    Dim param3 = New SqlParameter("@t2", SqlDbType.Date)
                    If t2 <> "" And t2 <> "N/A" Then
                        param3.Value = t2
                    Else
                        param3.Value = System.DBNull.Value
                    End If
                    'param3.Value = t2
                    cmd.Parameters.Add(param3)

                    assn.UpdateHack(cmd)
                Else
                    Dim strMessage As String = "No Work Order Date Found"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If


            End If
            If wonum <> "" And pmmode <> "pm" Then
                If t7 = "1" Then
                    sql = "update workorder set woa1 = isnull(woa1,0) - 1, woa1hrs = isnull(woa1hrs,0) - '" & t3 & "' " _
                    + "where wonum = '" & wonum & "'"
                    assn.Update(sql)
                ElseIf t7 = "2" Then
                    sql = "update workorder set woa2 = isnull(woa2,0) - 1, woa2hrs = isnull(woa2hrs,0) - '" & t3 & "' " _
                    + "where wonum = '" & wonum & "'"
                    assn.Update(sql)
                ElseIf t7 = "3" Then
                    sql = "update workorder set woa3 = isnull(woa3,0) - 1, woa3hrs = isnull(woa3hrs,0) - '" & t3 & "' " _
                    + "where wonum = '" & wonum & "'"
                    assn.Update(sql)
                End If
            End If


        Next
        lblrvals.Value = ""
    End Sub
    Private Sub towo(ByVal wonum As String)
        pmid = lblpmid.Value
        worktype = lblworktype.Value

        Dim vals As String = lblvals.Value
        Dim valsar() As String = vals.Split("~")
        Dim vals1 As String
        Dim labhrs As String = txthrs.Text
        If labhrs = "" Then
            labhrs = "0"
        Else
            Dim tnchk As Decimal
            Try
                tnchk = System.Convert.ToDecimal(labhrs)
                If cbusemin.Checked = True Then
                    tnchk = tnchk / 60
                    labhrs = tnchk
                End If
            Catch ex As Exception
                labhrs = "0"
            End Try
        End If
        Dim i As Integer
        Dim isflgstr = lblisflg.Value
        Dim ispm As String = "0"
        typ = lbltyp.Value
        If typ = "pm" Then
            ispm = "1"
        End If
        issched = lblissched.Value
        Dim labordate As String = lbllabordate.Value
        For i = 0 To valsar.Length - 1
            vals1 = valsar(i).ToString
            Dim vals1ar() As String = vals1.Split(",")
            Dim t As String = vals1ar(0)
            Dim t1 As String = vals1ar(1)
            Dim t2 As String = vals1ar(2)
            Dim t3 As String = vals1ar(3)
            Dim t4 As String = vals1ar(4)
            Dim t5 As String = vals1ar(5)
            Dim t6 As String '= vals1ar(6)
            sql = "select isnull(leadid, 0) from pm where pmid = '" & pmid & "'"
            t6 = assn.strScalar(sql)
            If t6 <> vals1ar(1) Then
                t6 = "0"
            Else
                t6 = "1"
            End If
            Dim tsql As String = "exec usp_addlabavail '" & t & "', '" & t1 & "', '" & wonum & "', " _
            + "'" & t2 & "', '" & t3 & "', '" & t4 & "', '" & t5 & "','" & isflg & "','" & ispm & "','" & pmid & "','" & issched & "'"
            Dim cmd As New SqlCommand
            cmd.CommandText = "exec usp_addlabavail @uid, @user, @wonum, @wdate, @avail, @labhrs, @shift, @isflg, @ispm, @pmid, @issched"
            Dim param = New SqlParameter("@uid", SqlDbType.VarChar)
            param.Value = vals1ar(0)
            cmd.Parameters.Add(param)
            Dim param04 = New SqlParameter("@user", SqlDbType.VarChar)
            If vals1ar(1) = "" Then
                param04.Value = System.DBNull.Value
            Else
                param04.Value = vals1ar(1)
            End If
            cmd.Parameters.Add(param04)
            Dim param05 = New SqlParameter("@wonum", SqlDbType.VarChar)
            If wonum = "" Then
                param05.Value = System.DBNull.Value
            Else
                param05.Value = wonum
            End If
            cmd.Parameters.Add(param05)
            Dim param10 = New SqlParameter("@wdate", SqlDbType.VarChar)
            If vals1ar(2) = "" Then
                If issched = "0" Then
                    If labordate <> "" Then
                        param10.Value = labordate
                    Else
                        param10.Value = System.DBNull.Value
                    End If
                Else
                    param10.Value = System.DBNull.Value
                End If

            Else
                param10.Value = vals1ar(2)
            End If
            cmd.Parameters.Add(param10)
            Dim param11 = New SqlParameter("@avail", SqlDbType.VarChar)
            If vals1ar(3) = "" Then
                param11.Value = System.DBNull.Value
            Else
                param11.Value = vals1ar(3)
            End If
            cmd.Parameters.Add(param11)
            Dim param12 = New SqlParameter("@labhrs", SqlDbType.VarChar)
            If labhrs = "" Then
                param12.Value = System.DBNull.Value
            Else
                param12.Value = labhrs
            End If
            cmd.Parameters.Add(param12)
            Dim param14 = New SqlParameter("@shift", SqlDbType.VarChar)
            If t5 = "" Then
                param14.Value = System.DBNull.Value
            Else
                param14.Value = t5
            End If
            cmd.Parameters.Add(param14)
            Dim param13 = New SqlParameter("@isflg", SqlDbType.VarChar)
            If isflgstr = "" Then
                param13.Value = System.DBNull.Value
            Else
                param13.Value = t6 'isflgstr
            End If
            cmd.Parameters.Add(param13)
            Dim param15 = New SqlParameter("@ispm", SqlDbType.VarChar)
            If ispm = "" Then
                param15.Value = System.DBNull.Value
            Else
                param15.Value = ispm
            End If
            cmd.Parameters.Add(param15)
            Dim param16 = New SqlParameter("@pmid", SqlDbType.VarChar)
            If pmid = "" Then
                param16.Value = System.DBNull.Value
            Else
                param16.Value = pmid
            End If
            cmd.Parameters.Add(param16)

            Dim param17 = New SqlParameter("@issched", SqlDbType.VarChar)
            If issched = "" Then
                param17.Value = System.DBNull.Value
            Else
                param17.Value = issched
            End If
            cmd.Parameters.Add(param17)
            assn.UpdateHack(cmd)
            If isflgstr = "0" Then
                isflgstr = "1"
            End If

        Next
        lblvals.Value = ""
    End Sub
    Private Sub PopFields()
        wonum = lblwonum.Value
        estlh = lblestlh.Value
        las = lblass.Value

        tdwonum.InnerHtml = wonum
        tdest.InnerHtml = estlh
        tdass.InnerHtml = las

        workarea = lblworkarea.Value
        sup = lblsup.Value
        txtsup.Text = sup
        tdwa.InnerHtml = workarea
    End Sub
    Private Sub changewa(ByVal wonum As String)
        Dim waid, wa As String
        wonum = lblwonum.Value
        sid = lblsid.Value
        Dim err1, err2, err3 As String
        waid = lblwaid.Value
        wa = lblworkarea.Value
        Dim cmd As New SqlCommand
        cmd.CommandText = "update workorder set waid = @waid " _
        + "where wonum = @wonum"
        Dim param = New SqlParameter("@wonum", SqlDbType.Int)
        param.Value = wonum
        cmd.Parameters.Add(param)
        Dim param04 = New SqlParameter("@waid", SqlDbType.VarChar)
        If waid = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = waid
        End If
        cmd.Parameters.Add(param04)


        assn.UpdateHack(cmd)
        'lblsubmit.Value = "return"
    End Sub
    Private Sub changesup(ByVal wonum As String)
        supid = lblsupid.Value
        sup = lblsup.Value
        sql = "update workorder set supervisor = '" & sup & "', " _
                   + "superid = '" & supid & "' " _
                   + "where wonum = '" & wonum & "'"
        assn.Update(sql)
    End Sub
    Private Sub remsup(ByVal wonum As String)
        sql = "update workorder set supervisor = null, " _
           + "superid = null " _
           + "where wonum = '" & wonum & "'"
        assn.Update(sql)
    End Sub
    Private Sub remwa(ByVal wonum As String)
        sql = "update workorder set waid = null " _
           + "where wonum = '" & wonum & "'"
        assn.Update(sql)
    End Sub
    Private Sub GetAssigned(ByVal wonum As String)
        sql = "select sum(assigned) from woassign where wonum = '" & wonum & "'"
        Dim lass As Integer
        Try
            lass = assn.Scalar(sql)
        Catch ex As Exception
            lass = 0
        End Try
        tdass.InnerHtml = lass
        lblass.Value = lass
    End Sub
    Private Sub PopSuperAssigned(ByVal wonum As String)
        Dim lang As String
        lang = lblfslang.Value

        pmmode = lblmode.Value
        If wonum <> "" And pmmode <> "pm" Then
            sql = "select w.woassignid, w.userid, w.username, Convert(char(10),w.assigndate,101) as assigndate, " _
        + "w.assigned, w.islead, s.shift from woassign w left join pmsysusers s on s.userid = w.userid where w.wonum = '" & wonum & "'"
        Else
            pmid = lblpmid.Value
            sql = "select w.pmassignid as woassignid, w.userid, w.username, 'N/A' as assigndate, " _
        + "w.assigned, w.islead, s.shift from pmassign w left join pmsysusers s on s.userid = w.userid where w.pmid = '" & pmid & "'"
        End If

        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        sb.Append("<tr>")
        If lang = "fre" Then
            sb.Append("<td class=""thdrsingg plainlabel"">Employ�</td>")
            sb.Append("<td class=""thdrsingg plainlabel"">Responsable?</td>")
            sb.Append("<td class=""thdrsingg plainlabel"">Date du Bon de Travail</td>")
            sb.Append("<td class=""thdrsingg plainlabel"">Assign�</td>")
            sb.Append("<td class=""thdrsingg plainlabel"">�diter</td>")
        Else
            sb.Append("<td class=""thdrsingg plainlabel"">Employee</td>")
            sb.Append("<td class=""thdrsingg plainlabel"">Is Lead?</td>")
            sb.Append("<td class=""thdrsingg plainlabel"">Work Order Date</td>")
            sb.Append("<td class=""thdrsingg plainlabel"">Assigned</td>")
            sb.Append("<td class=""thdrsingg plainlabel"">Edit</td>")
        End If
        
        sb.Append("</tr>")
        Dim wdate, assigned As String
        Dim trid As Integer = 0
        Dim trnme As String
        Dim isleadstr As String
        Dim shift As String
        Dim wid As String
        isflg = 0
        dr = assn.GetRdrData(sql)
        While dr.Read
            trid += 1
            trnme = "trr" & trid
            wid = dr.Item("woassignid").ToString
            supid = dr.Item("userid").ToString
            name = dr.Item("username").ToString
            wdate = dr.Item("assigndate").ToString
            assigned = dr.Item("assigned").ToString
            islead = dr.Item("islead").ToString
            shift = dr.Item("shift").ToString
            If shift = "" Or shift = 0 Then
                shift = "1"
            End If
            If islead = "1" Then
                isleadstr = "Yes"
                isflg += 1
            Else
                isleadstr = "No"
            End If
            sb.Append("<tr id=""" & trnme & """><td class=""linklabel""><a href=""#"" onclick=""remsup('" & supid & "', '" & name & "','" & wdate & "','" & assigned & "','" & islead & "','" & trnme & "','" & wid & "','" & shift & "');"">" & name & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & isleadstr & "</td>")
            sb.Append("<td class=""plainlabel"">" & wdate & "</td>")
            sb.Append("<td class=""plainlabel"">" & assigned & "</td>")
            sb.Append("<td><img src=""../images/appbuttons/minibuttons/pencil12.gif"" onclick=""getemp('" & supid & "', '" & name & "','" & wdate & "','" & assigned & "','" & islead & "','" & wid & "','" & shift & "');""></td></tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        supdiv1.InnerHtml = sb.ToString
        lblisflg.Value = isflg
        If isflg = "0" Then
            If lang = "fre" Then
                tdmsg1.InnerHtml = "Attention, aucun responsable s�lectionn�."
            Else
                tdmsg1.InnerHtml = "Warning. No Lead Craft Selected."
            End If

        End If
    End Sub
    Private Sub PopUnAssigned(ByVal estart As String, ByVal eend As String)
        Dim lang As String
        lang = lblfslang.Value

        issched = lblissched.Value
        sid = lblsid.Value
        supid = lblsupid.Value
        waid = lblwaid.Value
        typ = lbltyp.Value
        wonum = lblwonum.Value
        pmid = lblpmid.Value
        Dim srchflg As Integer = 0
        sql = "select u.userid, u.username, u.shift from pmsysusers u where "
        pmmode = lblmode.Value
        If wonum <> "" And pmmode <> "pm" Then
            Filter = "u.islabor = 1 and u.userid not in (select userid from woassign where wonum = '" & wonum & "')"
        Else
            Filter = "u.islabor = 1 and u.userid not in (select userid from pmassign where pmid = '" & pmid & "')"
        End If

        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        If Len(srch) > 0 Then
            srchflg = 1
            srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
            srch = Replace(srch, "--", "-", , , vbTextCompare)
            srch = Replace(srch, ";", ":", , , vbTextCompare)
            Filter += " and username like '%" & srch & "%'"
        End If
        If ddskill.SelectedIndex <> 0 And ddskill.SelectedIndex <> -1 Then
            srchflg = 1
            skill = ddskill.SelectedValue
            Filter += " and u.skillid = '" & skill & "'"
        End If
        If ddshift.SelectedIndex <> 0 And ddshift.SelectedIndex <> -1 Then
            srchflg = 1
            shift = ddshift.SelectedValue
            Filter += " and u.shift = '" & shift & "'"
        End If
        If cbsup.Checked = True And supid <> "" And supid <> "0" Then
            Filter += " and u.superid = '" & supid & "'"
        End If
        If cbwa.Checked = True And waid <> "" Then
            Filter += " and u.waid = '" & waid & "'"
        End If
        If srchflg <> 1 Then
            Filter += " and u.dfltps = '" & sid & "'"
        End If
        If typ = "pm" Or eend = "" Then
            If issched = "1" And typ <> "pm" Then
                Filter += " and (l.wdate = Convert(char(10),'" & estart & "',101))"
            End If

        Else
            If issched = "1" And typ <> "pm" Then
                Filter += " and (l.wdate >= Convert(char(10),'" & estart & "',101) and l.wdate <= Convert(char(10),'" & eend & "',101))"
            End If

        End If
        '                + "tblid int IDENTITY(1,1) NOT NULL, " _
        If issched = "1" And typ <> "pm" Then
            sql = "declare @tbl table ( " _
                + "userid int, " _
                + "username varchar(50), " _
                + "shift varchar(50), " _
                + "wdate varchar(50), " _
                + "avail decimal(10,2), " _
                + "assigned decimal(10,2) ) " _
                + "declare @tbl2 table ( " _
                + "tblid int, " _
                + "userid int, " _
                + "username varchar(50), " _
                + "shift varchar(50), " _
                + "wdate varchar(50), " _
                + "avail decimal(10,2), " _
                + "assigned decimal(10,2) ) " _
                + "insert into @tbl (userid, username, shift, wdate, avail, assigned) " _
                + "select distinct u.userid, u.username, u.shift, Convert(char(10),l.wdate,101) as wdate, l.avail, l.assigned from labavail l " _
                + "left join pmsysusers u on u.userid = l.userid " _
                + "where " & Filter & " " _
                + "insert into @tbl2 (userid, username, shift, wdate) " _
                + "select distinct userid, username, shift, wdate from @tbl " _
                + "declare @userid int, @username varchar(50), @shift varchar(50), @wdate varchar(50), @avail decimal(10,2), @assigned decimal(10,2) " _
                + "declare d_cursor cursor for " _
                + "select userid, username, shift, wdate from @tbl " _
                + "open d_cursor " _
                + "fetch next from d_cursor into @userid, @username, @shift, @wdate " _
                + "while @@fetch_status = 0 " _
                + "begin " _
                + "set @avail = (select sum(avail) from @tbl where userid = @userid and username = @username and shift = @shift and wdate = @wdate) " _
                + "set @assigned = (select sum(assigned) from @tbl where userid = @userid and username = @username and shift = @shift and wdate = @wdate)" _
                + "update @tbl2 set avail = @avail, assigned = @assigned " _
                + "where userid = @userid and username = @username and shift = @shift and wdate = @wdate " _
                + "fetch next from d_cursor into @userid, @username, @shift, @wdate " _
                + "end " _
                + "close d_cursor " _
                + "deallocate d_cursor " _
                + "select * from @tbl2"
        Else
            sql = "select u.userid, u.username, u.shift, '' as wdate, '' as avail, '' as assigned from pmsysusers u where " & Filter
        End If


        Dim wdate, avail, assigned As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        sb.Append("<tr>")
        If lang = "fre" Then
            sb.Append("<td class=""thdrsingg plainlabel"">Employ�</td>")
            sb.Append("<td class=""thdrsingg plainlabel"">Date du Bon de Travail</td>")
            sb.Append("<td class=""thdrsingg plainlabel"">Disponible</td>")
            sb.Append("<td class=""thdrsingg plainlabel"">Assign�</td>")
        Else
            sb.Append("<td class=""thdrsingg plainlabel"">Employee</td>")
            sb.Append("<td class=""thdrsingg plainlabel"">Work Order Date</td>")
            sb.Append("<td class=""thdrsingg plainlabel"">Available</td>")
            sb.Append("<td class=""thdrsingg plainlabel"">Assigned</td>")
        End If
        
        sb.Append("</tr>")
        Dim trid As Integer = 0
        Dim trnme As String
        Dim ushift As String
        Dim avchk As Long
        Dim ovflg As Integer = 0
        dr = assn.GetRdrData(sql)
        While dr.Read
            trid += 1
            trnme = "tr" & trid
            supid = dr.Item("userid").ToString
            name = dr.Item("username").ToString
            wdate = dr.Item("wdate").ToString
            avail = dr.Item("avail").ToString
            ushift = dr.Item("shift").ToString
            assigned = dr.Item("assigned").ToString
            Try
                avchk = System.Convert.ToDouble(assigned)
            Catch ex As Exception
                avchk = 0
            End Try
            If avchk < 0 Then
                sb.Append("<tr id=""" & trnme & """><td class=""redlink""><a href=""#"" onclick=""getsup('" & supid & "', '" & name & "','" & wdate & "','" & avail & "','" & trnme & "','" & ushift & "');"">" & name & "</a></td>")
                sb.Append("<td class=""plainlabelred"">" & wdate & "</td>")
                sb.Append("<td class=""plainlabelred"">" & avail & "</td>")
                sb.Append("<td class=""plainlabelred"">" & assigned & "</td></tr>")
            Else
                sb.Append("<tr id=""" & trnme & """><td class=""linklabel""><a href=""#"" onclick=""getsup('" & supid & "', '" & name & "','" & wdate & "','" & avail & "','" & trnme & "','" & ushift & "');"">" & name & "</a></td>")
                sb.Append("<td class=""plainlabel"">" & wdate & "</td>")
                sb.Append("<td class=""plainlabel"">" & avail & "</td>")
                sb.Append("<td class=""plainlabel"">" & assigned & "</td></tr>")
            End If


        End While
        dr.Close()
        sb.Append("</table>")
        supdiv.InnerHtml = sb.ToString
        If trid = "0" Then
            If lang = "fre" Then
                tdnoemps.InnerHtml = "Aucun employ� disponible pour les dates choisies"
            Else
                tdnoemps.InnerHtml = "No Labor Records Available for the Specified Date Range"
            End If


        End If
    End Sub
    Private Sub LoadSkills()
        cid = "0"
        wonum = lblwonum.Value
        sql = "select skillid, skill " _
        + "from pmSkills where compid = '" & cid & "'"
        dr = assn.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("All Skills"))
        ddskill.Items(0).Value = 0

    End Sub

    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ToLoc()
    End Sub

    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FromLoc()
    End Sub
    Private Sub ToLoc()
        cid = lblcid.Value
        wonum = lblwonum.Value
        labhrs = txthrs.Text
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToDecimal(labhrs)
        Catch ex As Exception
            Dim strMessage As String = "Labor Hours Must be a Numeric Value"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim Item As ListItem
        Dim f, fi As String
        assn.Open()
        'For Each Item In lbunassigned.Items
        If Item.Selected Then
            f = Item.Text.ToString
            fi = Item.Value.ToString
            GetItems(fi, f)
        End If
        'Next
        GetAssigned(wonum)
        PopSuperAssigned(wonum)
        'PopUnAssigned()
        assn.Dispose()
    End Sub
    Private Sub GetItems(ByVal fi As String, ByVal f As String)
        wonum = lblwonum.Value
        labhrs = txthrs.Text

        If labhrs <> "" Then
            sql = "insert into woassign (wonum, userid, username, assigned) values ('" & wonum & "','" & fi & "', " _
            + "'" & f & "','" & labhrs & "');"
        Else
            sql = "insert into woassign (wonum, userid, username) values ('" & wonum & "','" & fi & "','" & f & "')"
        End If
        assn.Update(sql)
    End Sub
    Private Sub FromLoc()
        cid = lblcid.Value
        wonum = lblwonum.Value
        Dim Item As ListItem
        Dim f, fi As String
        assn.Open()
        'For Each Item In 2=q   192999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999911111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111119999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999lblocations.Items
        If Item.Selected Then
            f = Item.Text.ToString
            fi = Item.Value.ToString
            RemItems(fi, f)
        End If
        'Next
        GetAssigned(wonum)
        PopSuperAssigned(wonum)
        'PopUnAssigned()
        assn.Dispose()
    End Sub
    Private Sub RemItems(ByVal fi As String, ByVal f As String)
        wonum = lblwonum.Value
        sql = "delete from woassign where wonum = '" & wonum & "' and userid = '" & fi & "'"
        assn.Update(sql)
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        assn.Open()
        estart = lblstart.Value
        eend = lblcomp.Value
        PopUnAssigned(estart, eend)
        PopSuperAssigned(wonum)
        PopFields()
        assn.Dispose()
    End Sub
End Class
