﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wolabtransfs.aspx.vb" Inherits="lucy_r12.wolabtransfs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />

    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/wolabtransaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        <!--
        function getcal(fld) {

            var stat = document.getElementById("lblstat").value;
            if (stat != "COMP" && stat != "CAN" && stat != "WAPPR") {
                //window.parent.setref();
                var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
                if (eReturn) {
                    var fldret = fld; //"txt" + 
                    document.getElementById(fldret).value = eReturn;
                    document.getElementById("lblwd").value = eReturn;
                }
            }
        }
        function getsuper(typ) {

            var stat = document.getElementById("lblstat").value;
            if (stat != "COMP" && stat != "CAN" && stat != "WAPPR") {
                //window.parent.setref();
                var skill = ""; //document.getElementById("lblskillid").value;
                var ro = document.getElementById("lblro").value;
                var sid = document.getElementById("lblsid").value;
                var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
                if (eReturn) {
                    if (eReturn != "") {
                        var retarr = eReturn.split(",")
                        if (typ == "sup") {
                            document.getElementById("lblsup").value = retarr[0];
                            document.getElementById("txtsup").value = retarr[1];
                        }
                        else {
                            document.getElementById("lbllead").value = retarr[0];
                            document.getElementById("lblname").value = retarr[1];
                            document.getElementById("tdlead").innerHTML = retarr[1];
                        }
                    }
                }
            }
        }
        function addnew1() {
            var wo = document.getElementById("lblwo").value;
            if (wo != "") {
                var stat = document.getElementById("lblstat").value;
                if (stat != "COMP" && stat != "CAN" && stat != "WAPPR") {
                    var wd = document.getElementById("txtwd").value;
                    var ld = document.getElementById("txtleadauto").value;
                    if (ld == "") {
                        ld = document.getElementById("ddlead").value;
                        //alert(document.getElementById("ddlead").value)
                    }

                    if (ld == "" || ld == "0") {
                        alert("Lead Craft Required")
                    }
                    else if (wd == "") {
                        alert("Work Date Required")
                    }
                    else {
                        document.getElementById("lblsubmit").value = "addnew";
                        document.getElementById("form1").submit();
                    }
                }
            }
            else {
                alert("No Work Order Created")
            }
        }
        
            //-->
    </script>
    <style type="text/css">
        .thdrsinglft
        {
            /*cursor:hand;*/
            padding: 1px 1px 1px 1px;
            margin: 1em;
            text-indent: 3px;
            text-align: left;
            vertical-align: middle;
            border-top: solid #6b6c6c 1px;
            border-left: solid #6b6c6c 2px;
            border-bottom: solid #6b6c6c 1px;
            background-image: url(../images/appbuttons/minibuttons/gradient1.gif);
            background-repeat: repeat-x;
            background-repeat: repeat;
            background-position: top left;
        }
        .thdrsingrt
        {
            padding: 1px 1px 1px 1px;
            margin: 1em;
            text-indent: 3px;
            text-align: left;
            vertical-align: middle;
            border-top: solid #6b6c6c 1px;
            border-right: solid #6b6c6c 1px;
            border-bottom: solid #6b6c6c 1px;
            background-image: url(../images/appbuttons/minibuttons/gradient1.gif);
            background-repeat: repeat-x;
            background-repeat: repeat;
            background-position: top left;
        }
        .listbox
        {
            border-bottom: 1px solid black;
            border-top: 1px solid black;
            border-left: 1px solid black;
            border-right: 1px solid black;
            background-color: White;
            overflow: auto;
            width: 150px;
            height: 300px;
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
            list-style-type: none;
            padding: 3px;
            margin: 0px;
        }
        .listboxshort
        {
            border-bottom: 1px solid black;
            border-top: 1px solid black;
            border-left: 1px solid black;
            border-right: 1px solid black;
            background-color: White;
            overflow: auto;
            width: 150px;
            height: 80px;
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
            list-style-type: none;
            padding: 3px;
            margin: 0px;
        }
        .listitem
        {
            list-style-type: none;
            padding: 0px;
            margin: 0px;
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
        }
        .listhigh
        {
            cursor: pointer;
            background-color: #67BAF9;
            list-style-type: none;
            padding: 0px;
            margin: 0px;
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
        }
    </style>
</head>
<body class="tbg" >
    <form id="form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="leadauto.asmx" />
        </Services>
    </asp:ScriptManager>
    <table id="scrollmenu" cellspacing="0" cellpadding="2" width="540" style="position: absolute; top: 0px;">
        <tr id="trp" height="24" runat="server">
            <td class="thdrsingg plainlabel">
                <asp:Label ID="lang1518" runat="server">Work Order Labor Reporting</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="bluelabel">
                            <asp:Label ID="lang1519" runat="server">Add Craft</asp:Label>
                        </td>
                        <td id="tdlead" runat="server" class="plainlabel">
                        <asp:TextBox ID="txtleadauto" runat="server" CssClass="plainlabel" Width="180"></asp:TextBox>
                        </td>
                        <td class="details">
                            <img onclick="getsuper('lead');" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                        <td class="bluelabel">
                            <asp:Label ID="lang1520" runat="server">Work Date</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtwd" runat="server" ReadOnly="True" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcal('txtwd');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg">
                        </td>
                        <td>
                            <img id="addnew" onclick="addnew1();" alt="" src="../images/appbuttons/minibuttons/addmod.gif"
                                runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel">
                            Select Craft
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlead" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="22">
            <td id="tdstat" class="plainlabelblue" align="center" runat="server">
                <asp:Label ID="lang1521" runat="server">Default is Start Time \ Stop Time Entry</asp:Label>
            </td>
        </tr>
        <tr height="22">
            <td id="Td1" class="plainlabelblue" align="center" runat="server">
                <asp:Label ID="lang1522" runat="server">Note that Time Entries can only be made when Work Order is not Waiting to be Approved, Completed, or Cancelled.</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DataGrid ID="dgparts" runat="server" BackColor="transparent" AutoGenerateColumns="False" Width="540">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <EditItemStyle Height="15px"></EditItemStyle>
                    <AlternatingItemStyle CssClass="prowtransblue  plainlabel"></AlternatingItemStyle>
                    <ItemStyle CssClass="prowtrans  plainlabel"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit" Visible="False">
                            <HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgeditfm" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Name">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label3" runat="server" Width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif"
                            Visible="False">
                            <HeaderStyle Width="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <img src="../images/appbuttons/minibuttons/magnifier.gif" id="imgti" runat="server">
                            </ItemTemplate>
                            <EditItemTemplate>
                                <img src="../images/appbuttons/minibuttons/magnifier.gif" id="imgte" runat="server">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Work Date">
                            <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.transdate") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lbltransdate" runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.transdate") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Start Time" Visible="False">
                            <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.starttime") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddhrs" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("hrs")) %>'>
                                                <asp:ListItem Value="NA">NA</asp:ListItem>
                                                <asp:ListItem Value="01">01</asp:ListItem>
                                                <asp:ListItem Value="02">02</asp:ListItem>
                                                <asp:ListItem Value="03">03</asp:ListItem>
                                                <asp:ListItem Value="04">04</asp:ListItem>
                                                <asp:ListItem Value="05">05</asp:ListItem>
                                                <asp:ListItem Value="06">06</asp:ListItem>
                                                <asp:ListItem Value="07">07</asp:ListItem>
                                                <asp:ListItem Value="08">08</asp:ListItem>
                                                <asp:ListItem Value="09">09</asp:ListItem>
                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                <asp:ListItem Value="12">12</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="bluelabel">
                                            :
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddmins" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndexM(Container.DataItem("mins")) %>'>
                                                <asp:ListItem Value="00">00</asp:ListItem>
                                                <asp:ListItem Value="01">01</asp:ListItem>
                                                <asp:ListItem Value="02">02</asp:ListItem>
                                                <asp:ListItem Value="03">03</asp:ListItem>
                                                <asp:ListItem Value="04">04</asp:ListItem>
                                                <asp:ListItem Value="05">05</asp:ListItem>
                                                <asp:ListItem Value="06">06</asp:ListItem>
                                                <asp:ListItem Value="07">07</asp:ListItem>
                                                <asp:ListItem Value="08">08</asp:ListItem>
                                                <asp:ListItem Value="09">09</asp:ListItem>
                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                <asp:ListItem Value="12">12</asp:ListItem>
                                                <asp:ListItem Value="13">13</asp:ListItem>
                                                <asp:ListItem Value="14">14</asp:ListItem>
                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                <asp:ListItem Value="16">16</asp:ListItem>
                                                <asp:ListItem Value="17">17</asp:ListItem>
                                                <asp:ListItem Value="18">18</asp:ListItem>
                                                <asp:ListItem Value="19">19</asp:ListItem>
                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                <asp:ListItem Value="21">21</asp:ListItem>
                                                <asp:ListItem Value="22">22</asp:ListItem>
                                                <asp:ListItem Value="23">23</asp:ListItem>
                                                <asp:ListItem Value="24">24</asp:ListItem>
                                                <asp:ListItem Value="25">25</asp:ListItem>
                                                <asp:ListItem Value="26">26</asp:ListItem>
                                                <asp:ListItem Value="27">27</asp:ListItem>
                                                <asp:ListItem Value="28">28</asp:ListItem>
                                                <asp:ListItem Value="29">29</asp:ListItem>
                                                <asp:ListItem Value="30">30</asp:ListItem>
                                                <asp:ListItem Value="31">31</asp:ListItem>
                                                <asp:ListItem Value="32">32</asp:ListItem>
                                                <asp:ListItem Value="33">33</asp:ListItem>
                                                <asp:ListItem Value="34">34</asp:ListItem>
                                                <asp:ListItem Value="35">35</asp:ListItem>
                                                <asp:ListItem Value="36">36</asp:ListItem>
                                                <asp:ListItem Value="37">37</asp:ListItem>
                                                <asp:ListItem Value="38">38</asp:ListItem>
                                                <asp:ListItem Value="39">39</asp:ListItem>
                                                <asp:ListItem Value="40">40</asp:ListItem>
                                                <asp:ListItem Value="41">41</asp:ListItem>
                                                <asp:ListItem Value="42">42</asp:ListItem>
                                                <asp:ListItem Value="43">43</asp:ListItem>
                                                <asp:ListItem Value="44">44</asp:ListItem>
                                                <asp:ListItem Value="45">45</asp:ListItem>
                                                <asp:ListItem Value="46">46</asp:ListItem>
                                                <asp:ListItem Value="47">47</asp:ListItem>
                                                <asp:ListItem Value="48">48</asp:ListItem>
                                                <asp:ListItem Value="49">49</asp:ListItem>
                                                <asp:ListItem Value="50">50</asp:ListItem>
                                                <asp:ListItem Value="51">51</asp:ListItem>
                                                <asp:ListItem Value="52">52</asp:ListItem>
                                                <asp:ListItem Value="53">53</asp:ListItem>
                                                <asp:ListItem Value="54">54</asp:ListItem>
                                                <asp:ListItem Value="55">55</asp:ListItem>
                                                <asp:ListItem Value="56">56</asp:ListItem>
                                                <asp:ListItem Value="57">57</asp:ListItem>
                                                <asp:ListItem Value="58">58</asp:ListItem>
                                                <asp:ListItem Value="59">59</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddaps" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndexAP(Container.DataItem("aps")) %>'>
                                                <asp:ListItem Value="AM">AM</asp:ListItem>
                                                <asp:ListItem Value="PM">PM</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Stop Time" Visible="False">
                            <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.stoptime") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddhre" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("hre")) %>'>
                                                <asp:ListItem Value="NA">NA</asp:ListItem>
                                                <asp:ListItem Value="01">01</asp:ListItem>
                                                <asp:ListItem Value="02">02</asp:ListItem>
                                                <asp:ListItem Value="03">03</asp:ListItem>
                                                <asp:ListItem Value="04">04</asp:ListItem>
                                                <asp:ListItem Value="05">05</asp:ListItem>
                                                <asp:ListItem Value="06">06</asp:ListItem>
                                                <asp:ListItem Value="07">07</asp:ListItem>
                                                <asp:ListItem Value="08">08</asp:ListItem>
                                                <asp:ListItem Value="09">09</asp:ListItem>
                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                <asp:ListItem Value="12">12</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="bluelabel">
                                            :
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddmine" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndexM(Container.DataItem("mine")) %>'>
                                                <asp:ListItem Value="00">00</asp:ListItem>
                                                <asp:ListItem Value="01">01</asp:ListItem>
                                                <asp:ListItem Value="02">02</asp:ListItem>
                                                <asp:ListItem Value="03">03</asp:ListItem>
                                                <asp:ListItem Value="04">04</asp:ListItem>
                                                <asp:ListItem Value="05">05</asp:ListItem>
                                                <asp:ListItem Value="06">06</asp:ListItem>
                                                <asp:ListItem Value="07">07</asp:ListItem>
                                                <asp:ListItem Value="08">08</asp:ListItem>
                                                <asp:ListItem Value="09">09</asp:ListItem>
                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                <asp:ListItem Value="12">12</asp:ListItem>
                                                <asp:ListItem Value="13">13</asp:ListItem>
                                                <asp:ListItem Value="14">14</asp:ListItem>
                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                <asp:ListItem Value="16">16</asp:ListItem>
                                                <asp:ListItem Value="17">17</asp:ListItem>
                                                <asp:ListItem Value="18">18</asp:ListItem>
                                                <asp:ListItem Value="19">19</asp:ListItem>
                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                <asp:ListItem Value="21">21</asp:ListItem>
                                                <asp:ListItem Value="22">22</asp:ListItem>
                                                <asp:ListItem Value="23">23</asp:ListItem>
                                                <asp:ListItem Value="24">24</asp:ListItem>
                                                <asp:ListItem Value="25">25</asp:ListItem>
                                                <asp:ListItem Value="26">26</asp:ListItem>
                                                <asp:ListItem Value="27">27</asp:ListItem>
                                                <asp:ListItem Value="28">28</asp:ListItem>
                                                <asp:ListItem Value="29">29</asp:ListItem>
                                                <asp:ListItem Value="30">30</asp:ListItem>
                                                <asp:ListItem Value="31">31</asp:ListItem>
                                                <asp:ListItem Value="32">32</asp:ListItem>
                                                <asp:ListItem Value="33">33</asp:ListItem>
                                                <asp:ListItem Value="34">34</asp:ListItem>
                                                <asp:ListItem Value="35">35</asp:ListItem>
                                                <asp:ListItem Value="36">36</asp:ListItem>
                                                <asp:ListItem Value="37">37</asp:ListItem>
                                                <asp:ListItem Value="38">38</asp:ListItem>
                                                <asp:ListItem Value="39">39</asp:ListItem>
                                                <asp:ListItem Value="40">40</asp:ListItem>
                                                <asp:ListItem Value="41">41</asp:ListItem>
                                                <asp:ListItem Value="42">42</asp:ListItem>
                                                <asp:ListItem Value="43">43</asp:ListItem>
                                                <asp:ListItem Value="44">44</asp:ListItem>
                                                <asp:ListItem Value="45">45</asp:ListItem>
                                                <asp:ListItem Value="46">46</asp:ListItem>
                                                <asp:ListItem Value="47">47</asp:ListItem>
                                                <asp:ListItem Value="48">48</asp:ListItem>
                                                <asp:ListItem Value="49">49</asp:ListItem>
                                                <asp:ListItem Value="50">50</asp:ListItem>
                                                <asp:ListItem Value="51">51</asp:ListItem>
                                                <asp:ListItem Value="52">52</asp:ListItem>
                                                <asp:ListItem Value="53">53</asp:ListItem>
                                                <asp:ListItem Value="54">54</asp:ListItem>
                                                <asp:ListItem Value="55">55</asp:ListItem>
                                                <asp:ListItem Value="56">56</asp:ListItem>
                                                <asp:ListItem Value="57">57</asp:ListItem>
                                                <asp:ListItem Value="58">58</asp:ListItem>
                                                <asp:ListItem Value="59">59</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddape" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndexAP(Container.DataItem("ape")) %>'>
                                                <asp:ListItem Value="AM">AM</asp:ListItem>
                                                <asp:ListItem Value="PM">PM</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Minutes" Visible="False">
                            <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.minutes") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="txtcost" runat="server" ReadOnly="True" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.minutes") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Minutes" Visible="False">
                            <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.minutes_man") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtcostm" runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.minutes_man") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="woltid" Visible="False">
                            <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblwoltidi" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.woltid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblwoltid" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.woltid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="labrate" Visible="False">
                            <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.laborrate") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lbllrate" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.laborrate") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="skillrate" Visible="False">
                            <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label10" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.skillrate") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblsrate" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.skillrate") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Remove" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton16" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <input id="lblwo" type="hidden" runat="server"><input id="lbllead" type="hidden"
        runat="server">
    <input id="lblsubmit" type="hidden" runat="server">
    <input id="lblstat" type="hidden" runat="server">
    <input id="lbltentry" type="hidden" runat="server">
    <input id="lblacost" type="hidden" runat="server">
    <input id="lblro" type="hidden" runat="server"><input id="lbllog" type="hidden" name="lbllog"
        runat="server">
    <input id="lblfslang" type="hidden" runat="server">
    <input id="lblruns" type="hidden" runat="server">
    <input id="lblworuns" type="hidden" runat="server">
    <input id="lblworktype" type="hidden" runat="server">
    <input id="lblsid" type="hidden" runat="server">
    <input type="hidden" id="lblwd" runat="server" />
    <input type="hidden" id="lblname" runat="server" />
    <input id="xCoord" type="hidden" runat="server" /><input id="yCoord" type="hidden" runat="server" />

     <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" TargetControlID="txtleadauto"
        runat="server" ServicePath="leadauto.asmx" ServiceMethod="geteq" MinimumPrefixLength="1"
        CompletionInterval="1000" EnableCaching="true" CompletionSetCount="12" CompletionListCssClass="listboxshort"
        CompletionListItemCssClass="listitem" CompletionListHighlightedItemCssClass="listhigh">
    </cc1:AutoCompleteExtender>
    </form>
</body>
</html>

