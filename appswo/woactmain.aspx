<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="woactmain.aspx.vb" Inherits="lucy_r12.woactmain" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>woactmain</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/woactmainaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table style="left: 2px; position: absolute; top: 2px" cellspacing="1" cellpadding="2"
        width="750">
        <tr>
            <td>
                <table width="750">
                    <tr height="22">
                        <td class="label" width="80">
                            <asp:Label ID="lang1341" runat="server">Work Order#</asp:Label>
                        </td>
                        <td class="plainlabel" id="tdwo" width="140" runat="server">
                        </td>
                        <td class="plainlabel" id="tdwod" width="450" runat="server">
                        </td>
                        <td align="right" width="50">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="2" width="750">
                    <tr height="22">
                        <td class="thdrhov label" id="tdw" onclick="gettab('wo');" width="130" runat="server">
                            <asp:Label ID="lang1342" runat="server">Work Order Details</asp:Label>
                        </td>
                        <td class="thdr label" id="tdj" onclick="gettab('jp');" width="150" runat="server">
                            <asp:Label ID="lang1343" runat="server">Job Plan Details (Labor)</asp:Label>
                        </td>
                        <td class="thdr label" id="tdm" onclick="gettab('jpm');" width="170" runat="server">
                            <asp:Label ID="lang1344" runat="server">Job Plan Details (Materials)</asp:Label>
                        </td>
                        <td class="thdr label" id="tdl" onclick="gettab('lab');" width="130" runat="server">
                            <asp:Label ID="lang1345" runat="server">Labor Reporting</asp:Label>
                        </td>
                        <td width="200">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdborder" id="wo" colspan="5" runat="server">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <iframe id="ifwo" src="wohold.htm" frameborder="no" width="760" scrolling="auto"
                                            height="440" runat="server" style="background-color: transparent" allowtransparency>
                                        </iframe>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="details" id="jp" valign="top" colspan="5" runat="server">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <iframe id="ifjp" src="wohold.htm" frameborder="no" width="760" scrolling="auto"
                                            height="440" runat="server" style="background-color: transparent" allowtransparency>
                                        </iframe>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblwo" type="hidden" runat="server" name="lblwo">
    <input type="hidden" id="lbljpid" runat="server" name="lbljpid">
    <input type="hidden" id="lblupsav" runat="server" name="lblupsav">
    <input type="hidden" id="lblstat" runat="server" name="lblstat">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
