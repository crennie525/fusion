<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="woreporting.aspx.vb" Inherits="lucy_r12.woreporting" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Work Order Reporting</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="javascript" type="text/javascript">
        <!--
        function gettab(name) {
            var wo = document.getElementById("lblwo").value;
            var ro = document.getElementById("lblro").value;
            var stat = document.getElementById("lblstat").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            if (wo != "") {
                if (name == "f") {
                    closeall();
                    document.getElementById("tdf").className = "thdrhov label";
                    document.getElementById("wo").className = 'tdborder view';
                    document.getElementById("ifwo").src = "wofm2.aspx?wo=" + wo + "&stat=" + stat + "&date=" + Date() + "&ro=" + ro + "&uid=" + uid;
                }
                else if (name == "m") {
                    closeall();
                    document.getElementById("tdm").className = "thdrhov label";
                    document.getElementById("wo").className = 'tdborder view';
                    document.getElementById("ifwo").src = "wofmme2.aspx?wo=" + wo + "&stat=" + stat + "&date=" + Date() + "&ro=" + ro;
                }
                else if (name == "par") {
                    closeall();
                    document.getElementById("tdpar").className = "thdrhov label";
                    document.getElementById("wo").className = 'tdborder view';
                    document.getElementById("ifwo").src = "woactm.aspx?typ=p&wo=" + wo + "&stat=" + stat + "&sav=no&date=" + Date() + "&ro=" + ro;
                }
                else if (name == "too") {
                    closeall();
                    document.getElementById("tdtoo").className = "thdrhov label";
                    document.getElementById("wo").className = 'tdborder view';
                    document.getElementById("ifwo").src = "woactm.aspx?typ=t&wo=" + wo + "&stat=" + stat + "&sav=no&date=" + Date() + "&ro=" + ro;
                }
                else if (name == "lub") {
                    closeall();
                    document.getElementById("tdlub").className = "thdrhov label";
                    document.getElementById("wo").className = 'tdborder view';
                    document.getElementById("ifwo").src = "woactm.aspx?typ=l&wo=" + wo + "&stat=" + stat + "&sav=no&date=" + Date() + "&ro=" + ro;
                }
                else if (name == "lab") {
                    closeall();
                    var jp;
                    jp = "";
                    document.getElementById("tdlab").className = "thdrhov label";
                    document.getElementById("wo").className = 'tdborder view';
                    document.getElementById("ifwo").src = "wolabtrans.aspx?sid=" + sid + "&wo=" + wo + "&jpid=" + jp + "&stat=" + stat + "&date=" + Date() + "&ro=" + ro + "&uid=" + uid;
                }
            }
        }
        function closeall() {
            document.getElementById("tdf").className = "thdr label";
            document.getElementById("tdm").className = "thdr label";
            document.getElementById("tdpar").className = "thdr label";
            document.getElementById("tdtoo").className = "thdr label";
            document.getElementById("tdlub").className = "thdr label";
            document.getElementById("tdlab").className = "thdr label";
            document.getElementById("wo").className = 'details';
        }
        function setref() {
            //window.parent.setref();
        }
        function doref() {
            //window.parent.doref();
        }
        //-->
    </script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" width="800" style="position: absolute; top: 2px; left: 0px"
        cellspacing="0" cellpadding="2">
        <tr>
            <td>
                <img src="../images/appbuttons/minibuttons/6PX.gif">
            </td>
        </tr>
        <tr height="22">
            <td class="thdrhov label" id="tdf" onclick="gettab('f');" width="100" runat="server">
                <asp:Label ID="lang1393" runat="server">Failure Modes</asp:Label>
            </td>
            <td class="thdr label" id="tdm" onclick="gettab('m');" width="100" runat="server">
                <asp:Label ID="lang1394" runat="server">Measurements</asp:Label>
            </td>
            <td class="thdr label" id="tdpar" onclick="gettab('par');" width="100" runat="server">
                <asp:Label ID="lang1335a" runat="server">Parts</asp:Label>
            </td>
            <td class="thdr label" id="tdtoo" onclick="gettab('too');" width="100" runat="server">
                <asp:Label ID="lang1336a" runat="server">Tools</asp:Label>
            </td>
            <td class="thdr label" id="tdlub" onclick="gettab('lub');" width="100" runat="server">
                <asp:Label ID="lang1337a" runat="server">Lubricants</asp:Label>
            </td>
            <td class="thdr label" id="tdlab" onclick="gettab('lab');" width="120" runat="server">
                <asp:Label ID="Label1" runat="server">Labor Reporting</asp:Label>
            </td>
            <td width="180">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="tdborder" id="wo" colspan="7" runat="server">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <iframe id="ifwo" src="wohold.htm" frameborder="no" width="800" height="420" runat="server">
                            </iframe>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblwo" runat="server" name="lblwo">
    <input type="hidden" id="lblfmstr" runat="server" name="lblfmstr">
    <input type="hidden" id="lblmstr" runat="server" name="lblmstr">
    <input type="hidden" id="lblsubmit" runat="server" name="lblsubmit">
    <input type="hidden" id="lblupsav" runat="server" name="lblupsav">
    <input type="hidden" id="lblstat" runat="server" name="lblstat">
    <input id="xCoord" type="hidden" name="xCoord" runat="server">
    <input id="yCoord" type="hidden" name="yCoord" runat="server">
    <input type="hidden" id="lblro" runat="server" name="lblro">
    <input type="hidden" id="lblfslang" runat="server" name="lblfslang">
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lbluid" runat="server" />
    </form>
</body>
</html>
