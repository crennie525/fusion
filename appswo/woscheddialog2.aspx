<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="woscheddialog2.aspx.vb"
    Inherits="lucy_r12.woscheddialog2_aspx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title runat="server" id="pgtitle">woscheddialog2_aspx</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" type="text/javascript" src="../scripts/sessrefdialog.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/woscheddialog2aspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onunload="handleexit();" onload="resetsess();">
    <form id="form1" method="post" runat="server">
    <iframe id="ifsched" runat="server" src="" width="600" height="350" frameborder="no">
    </iframe>
    <iframe id="ifsession" class="details" src="" frameborder="no" runat="server" style="z-index: 0">
    </iframe>
    <input type="hidden" id="lblsessrefresh" runat="server" name="lblsessrefresh">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
