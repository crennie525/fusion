

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wrmain
    Inherits System.Web.UI.Page
	Protected WithEvents lang1615 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1614 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1613 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1612 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim wonum, jump, href, sql, won As String
    Dim dr As SqlDataReader
    Dim wo As New Utilities
    Dim did, sid, clid, chk, eqid, fuid, coid, lid, typ, ncid, wr, Login As String
    Dim usid, nme, ro, rostr, tab, issuper, Logged_In, ms, appstr As String
    Dim islabor, isplanner, waid, wpaid, fdate, tdate, wtret, statret, who As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifnew As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifmain As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifarch As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblhref As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchng As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldesc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgetarch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljump As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblLogged_In As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblms As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblislabor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisplanner As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblissuper As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnme As System.Web.UI.HtmlControls.HtmlInputHidden
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)

        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            Response.Redirect(url)
        End If
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("wrmain.aspx?jump=no")
        Catch ex As Exception

        End Try
        If Not IsPostBack Then
            wr = "yes" 'System.Configuration.ConfigurationManager.AppSettings("WR").ToString
            sid = Request.QueryString("sid").ToString
            usid = Request.QueryString("uid").ToString
            nme = Request.QueryString("usrname").ToString
            islabor = Request.QueryString("islabor").ToString
            issuper = Request.QueryString("issuper").ToString
            isplanner = Request.QueryString("isplanner").ToString

            Logged_In = Request.QueryString("Logged_In").ToString
            lblLogged_In.Value = Logged_In
            MS = Request.QueryString("ms").ToString
            lblms.Value = MS
            appstr = Request.QueryString("appstr").ToString
            lblappstr.Value = appstr

            lblsid.Value = sid
            lblusid.Value = usid
            lblnme.Value = nme
            lblislabor.Value = islabor
            lblisplanner.Value = isplanner
            lblissuper.Value = issuper
            If wr = "yes" Then
                Try

                    jump = Request.QueryString("jump").ToString
                    If jump = "yes" Then
                        won = Request.QueryString("wo").ToString
                        lblwo.Value = won
                        sid = Request.QueryString("sid").ToString
                        lblsid.Value = sid
                        did = Request.QueryString("did").ToString
                        lbldid.Value = did
                        clid = Request.QueryString("clid").ToString
                        lblclid.Value = clid
                        chk = Request.QueryString("chk").ToString
                        lblchk.Value = chk
                        eqid = Request.QueryString("eqid").ToString
                        lbleqid.Value = eqid
                        fuid = Request.QueryString("fuid").ToString
                        lblfuid.Value = fuid
                        coid = Request.QueryString("coid").ToString
                        lblcoid.Value = coid
                        lid = Request.QueryString("lid").ToString
                        lbllid.Value = lid
                        typ = Request.QueryString("typ").ToString
                        lbltyp.Value = typ
                        ncid = Request.QueryString("nid").ToString
                        lblncid.Value = ncid
                        '"jump=yes&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eid + "&fuid=" + fid + "&coid=" + cid + "&lid=" + lid + "&typ=" + typ
                        href = "jump=yes&wo=" + won + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eqid + "&nid=" + ncid + "&fuid=" + fuid + "&coid=" + coid + "&lid=" + lid + "&typ=" + typ
                        lblhref.Value = href
                        'If did <> "" Or lid <> "" Then
                        'lblgetarch.Value = "yes"
                        'End If
                    Else
                        sid = HttpContext.Current.Session("dfltps").ToString
                        lblsid.Value = sid
                    End If
                Catch ex As Exception
                    'sid = "12" 'HttpContext.Current.Session("dfltps").ToString
                    'lblsid.Value = sid
                End Try
            End If
        Else
        End If

    End Sub
    Private Sub SaveDesc()
        wonum = lblwo.Value
        Dim sh, lg As String
        Dim lgcnt As Integer
        lg = lbldesc.Value
        If Len(lg) > 79 Then
            sh = Mid(lg, 1, 79)
            sql = "update workorder set description = '" & sh & "' where wonum = '" & wonum & "'"
            wo.Update(sql)
            lg = Mid(lg, 80)
            sql = "select count(*) from wolongdesc where wonum = '" & wonum & "'"
            lgcnt = wo.Scalar(sql)
            If lgcnt <> 0 Then
                sql = "update wolongdesc set longdesc = '" & lg & "' where wonum = '" & wonum & "'"
                wo.Update(sql)
            Else
                sql = "insert into wolongdesc (wonum, longdesc) values ('" & wonum & "','" & lg & "')"
                wo.Update(sql)
            End If
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1612.Text = axlabs.GetASPXPage("wrmain.aspx", "lang1612")
        Catch ex As Exception
        End Try
        Try
            lang1613.Text = axlabs.GetASPXPage("wrmain.aspx", "lang1613")
        Catch ex As Exception
        End Try
        Try
            lang1614.Text = axlabs.GetASPXPage("wrmain.aspx", "lang1614")
        Catch ex As Exception
        End Try
        Try
            lang1615.Text = axlabs.GetASPXPage("wrmain.aspx", "lang1615")
        Catch ex As Exception
        End Try

    End Sub

End Class