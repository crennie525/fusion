﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Collections.Generic
Imports System.Data.SqlClient

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class eqauto
    Inherits System.Web.Services.WebService
    Dim sql As String
    Dim eqa As New Utilities
    Dim dr As SqlDataReader
    Dim eq, eqdesc, ed As String
    <WebMethod()> _
    Public Function geteq(ByVal prefixText As String, ByVal count As Integer) As String()
        sql = "select top 100 eqnum, eqdesc from equipment where (eqnum like '" & prefixText & "%' or eqdesc like '" & prefixText & "%') and siteid = '" & count & "'"
        Dim items As New List(Of String)
        eqa.Open()
        dr = eqa.GetRdrData(sql)
        While dr.Read
            eq = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            ed = eq & "~~" & eqdesc
            items.Add(ed)
        End While
        dr.Close()
        eqa.Dispose()
        Return items.ToArray()
    End Function
    <WebMethod()> _
    Public Function getwo(ByVal prefixText As String, ByVal count As Integer) As String()
        sql = "select top 100 wonum from workorder where wonum like '" & prefixText & "%' and siteid = '" & count & "'"
        Dim items As New List(Of String)
        eqa.Open()
        dr = eqa.GetRdrData(sql)
        While dr.Read
            eq = dr.Item("wonum").ToString
            items.Add(eq)
        End While
        dr.Close()
        eqa.Dispose()
        Return items.ToArray()
    End Function
End Class