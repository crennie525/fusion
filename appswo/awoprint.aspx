﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="awoprint.aspx.vb" Inherits="lucy_r12.awoprint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>After Work Order Report</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
    <!--
        var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
        function printwo(typ, wo, pm, jp) {
            if (jp != "" && typ != "MAXPM") {
                typ = "JP"
            }
            window.open("woprint.aspx?typ=" + typ + "&pm=" + pm + "&wo=" + wo + "&jp=" + jp, "repWin", popwin);
        }
    //-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="dvawo" runat="server">
    
    </div>
    </form>
</body>
</html>
