<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wofaillist.aspx.vb" Inherits="lucy_r12.wofaillist" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wofaillist</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/wofaillistaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table width="422">
        <tr>
            <td class="thdrsingrt label" colspan="2">
                <asp:Label ID="lang1387" runat="server">Add Failure Modes from Master List Mini Dialog</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label" width="172" height="26">
                <asp:Label ID="lang1388" runat="server">Current Component:</asp:Label>
            </td>
            <td class="labellt" id="tdcomp" width="250" height="26" runat="server">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr style="border-right: #0000ff 1px solid; border-top: #0000ff 1px solid; border-left: #0000ff 1px solid;
                    border-bottom: #0000ff 1px solid">
            </td>
        </tr>
    </table>
    <table width="422">
        <tr>
            <td class="label" align="center">
                <asp:Label ID="lang1389" runat="server">Available Failure Modes</asp:Label>
            </td>
            <td>
            </td>
            <td class="label" align="center">
                <asp:Label ID="lang1390" runat="server">Selected Failure Modes</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" width="200">
                <asp:ListBox ID="lbfailmaster" runat="server" Width="170px" SelectionMode="Multiple"
                    Height="150px"></asp:ListBox>
            </td>
            <td valign="middle" align="center" width="22">
                <img src="../images/appbuttons/minibuttons/forwardgraybg.gif" class="details" id="todis"
                    width="20" height="20">
                <img src="../images/appbuttons/minibuttons/backgraybg.gif" class="details" id="fromdis"
                    width="20" height="20">
                <asp:ImageButton ID="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                </asp:ImageButton>
                <asp:ImageButton ID="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                </asp:ImageButton><img id="btnaddtosite" onmouseover="return overlib('Choose Failure Modes for this Plant Site ')"
                    onclick="getss();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/plusminus.gif"
                    width="20" height="20" class="details">
            </td>
            <td align="center" width="200">
                <asp:ListBox ID="lbfailmodes" runat="server" Width="170px" SelectionMode="Multiple"
                    Height="150px"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="3">
                <img onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
                    id="ibtnret" runat="server" width="69" height="19">
            </td>
        </tr>
        <tr>
            <td class="bluelabel" colspan="3">
                <asp:Label ID="lang1391" runat="server">List Box Options</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label" colspan="3">
                <asp:RadioButtonList ID="cbopts" runat="server" Width="400px" AutoPostBack="True"
                    CssClass="labellt">
                    <asp:ListItem Value="0" Selected="True">Multi-Select (use arrows to move single or multiple selections)</asp:ListItem>
                    <asp:ListItem Value="1">Click-Once (move selected item by clicking that item)</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="plainlabelblue" colspan="3">
                <asp:Label ID="lang1392" runat="server">* Please note that the Multi-Select Mode must be used to remove an item from the Selected Failure Modes list if only one item is present.</asp:Label>
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" runat="server" name="lblcid">
    <input id="lbleqid" type="hidden" runat="server" name="lbleqid">
    <input id="lblfuid" type="hidden" runat="server" name="lblfuid">
    <input id="lblcoid" type="hidden" runat="server" name="lblcoid">
    <input id="lblco" type="hidden" runat="server" name="lblco"><input id="lblapp" type="hidden"
        runat="server" name="lblapp">
    <input type="hidden" id="lblopt" runat="server" name="lblopt"><input type="hidden"
        id="lblfailchk" runat="server" name="lblfailchk">
    <input type="hidden" id="lblsid" runat="server" name="lblsid"><input type="hidden"
        id="lbllog" runat="server" name="lbllog">
    <input type="hidden" id="lblwo" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
