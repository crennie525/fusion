

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wrman
    Inherits System.Web.UI.Page
	Protected WithEvents ovid209 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang1623 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1622 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1621 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1620 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1619 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1618 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1617 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1616 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim tasks As New Utilities
    Dim sql, cid, sid, eq, eqid, fu, fuid, did, clid, jump, typ, lid, coid, ncid, stat, usr As String
    Dim Filter, filt, dt, df, tl, val, tli, wonum As String
    Dim pmid, type As String
    Protected WithEvents ddnc As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ncdiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgetarch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblreset As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblreqd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstatus As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdisdown As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbisdown As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbisnot As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblisdown As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblncid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dddepts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lbldeptdesc As System.Web.UI.WebControls.Label
    Protected WithEvents ddcells As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblcelldesc As System.Web.UI.WebControls.Label
    Protected WithEvents ddeq As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblloc As System.Web.UI.WebControls.Label
    Protected WithEvents deptdiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents imgsw As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents celldiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents eqdiv As System.Web.UI.HtmlControls.HtmlTableRow

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Dim login As String
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If lbllog.Value <> "no" Then
            Dim pmchk As String = lblpchk.Value
            If Request.Form("lblpchk") = "loc" Then
                lblpchk.Value = ""
                tasks.Open()
                lid = lbllid.Value
                lblgetarch.Value = "yes"
                PopEq(lid, "l")
                PopNC(lid, "l")
                tasks.Dispose()
            ElseIf Request.Form("lblpchk") = "eq" Then
                tasks.Open()
                'a note
                If lblclid.Value = "0" Then
                    Dim dept As String = lbldept.Value
                    PopEq(dept, "d")
                Else
                    Dim cell As String = lblclid.Value
                    PopEq(cell, "c")
                End If
                eqid = lbleqid.Value
                ddeq.SelectedValue = eqid
                lblpchk.Value = ""
                tasks.Dispose()
            End If

            If Not IsPostBack Then

                CleanFields()
                cid = HttpContext.Current.Session("comp").ToString
                lblcid.Value = cid
                sid = HttpContext.Current.Session("dfltps").ToString
                lblsid.Value = sid
                usr = Request.QueryString("usr").ToString
                lbluser.Value = usr
                tasks.Open()
                GetDef()
                PopDepts(sid)
                jump = Request.QueryString("jump").ToString
                wonum = Request.QueryString("wo").ToString
                lblwo.Value = wonum
                stat = Request.QueryString("stat").ToString

                If stat = "" Then
                    stat = CheckStat(wonum)
                End If

                lblstatus.Value = stat

                Dim deptcheck As Integer = 0
                Dim fuidcheck As Integer = 0
                If jump = "yes" Then
                    lblgetarch.Value = "yes"
                    wonum = Request.QueryString("wo").ToString
                    lblwo.Value = wonum
                    typ = Request.QueryString("typ").ToString
                    lbltyp.Value = typ
                    Dim eqcheck As Integer = 0
                    If typ = "loc" Or typ = "dloc" Then
                        lid = Request.QueryString("lid").ToString
                        lbllid.Value = lid
                        did = Request.QueryString("did").ToString
                        If did = "" Then
                            imgsw.Visible = True
                            dddepts.Enabled = False
                        Else
                            Try
                                dddepts.SelectedValue = did ' check
                                lbldept.Value = did
                                Filter = did
                            Catch ex As Exception
                                Try
                                    dddepts.SelectedIndex = 0
                                Catch ex1 As Exception

                                End Try

                                deptcheck = 1
                            End Try
                            If deptcheck <> 1 Then
                                Dim cellchk As Integer = 0
                                Dim chk As String = CellCheck(Filter)
                                If chk = "no" Then
                                    lblchk.Value = "no"
                                Else
                                    lblchk.Value = "yes"
                                    PopCells(did)
                                    clid = Request.QueryString("clid").ToString
                                    Try
                                        ddcells.SelectedValue = clid ' check
                                        lblclid.Value = clid
                                        lblpar.Value = "cell"
                                    Catch ex As Exception
                                        Try
                                            ddcells.SelectedIndex = 0
                                        Catch ex1 As Exception

                                        End Try

                                        cellchk = 1
                                    End Try
                                End If
                            End If
                        End If
                        PopEq(lid, "l")
                        PopNC(lid, "l")
                        eqid = Request.QueryString("eqid").ToString
                        ncid = Request.QueryString("nid").ToString
                        Try
                            lbleqid.Value = eqid
                            ddeq.SelectedValue = eqid
                            tdisdown.Attributes.Add("class", "label")
                            CheckDown()

                        Catch ex As Exception
                            Try
                                ddeq.SelectedIndex = 0
                            Catch ex1 As Exception

                            End Try

                            eqcheck = 1
                        End Try
                        Try
                            lblncid.Value = ncid
                            ddnc.SelectedValue = ncid
                        Catch ex As Exception
                            Try
                                ddnc.SelectedIndex = 0
                            Catch ex1 As Exception

                            End Try

                        End Try
                    Else
                        imgsw.Visible = False
                        dddepts.Enabled = True
                        did = Request.QueryString("did").ToString
                        Try
                            dddepts.SelectedValue = did ' check
                            lbldept.Value = did
                            Filter = did
                        Catch ex As Exception
                            Try
                                dddepts.SelectedIndex = 0
                            Catch ex1 As Exception

                            End Try

                            deptcheck = 1
                        End Try
                        If deptcheck <> 1 Then
                            Dim cellchk As Integer = 0
                            Dim chk As String = CellCheck(Filter)
                            If chk = "no" Then
                                lblchk.Value = "no"
                            Else
                                lblchk.Value = "yes"
                                PopCells(did)
                                clid = Request.QueryString("clid").ToString
                                Try
                                    ddcells.SelectedValue = clid ' check
                                    lblclid.Value = clid
                                    lblpar.Value = "cell"
                                Catch ex As Exception
                                    Try
                                        ddcells.SelectedIndex = 0
                                    Catch ex1 As Exception

                                    End Try

                                    cellchk = 1
                                End Try
                            End If
                            If cellchk <> 1 Then
                                'Dim eqcheck As Integer = 0
                                PopEq(did, "d")
                                PopNC(did, "d")
                                eqid = Request.QueryString("eqid").ToString
                                ncid = Request.QueryString("nid").ToString
                                Try
                                    lbleqid.Value = eqid
                                    ddeq.SelectedValue = eqid
                                    tdisdown.Attributes.Add("class", "label")
                                    CheckDown()

                                Catch ex As Exception
                                    Try
                                        ddeq.SelectedIndex = 0
                                    Catch ex1 As Exception

                                    End Try

                                    eqcheck = 1
                                End Try
                                Try
                                    lblncid.Value = ncid
                                    ddnc.SelectedValue = ncid
                                Catch ex As Exception
                                    Try
                                        ddnc.SelectedIndex = 0
                                    Catch ex1 As Exception

                                    End Try

                                End Try
                            End If
                        End If
                        tasks.Dispose()
                    End If
                    If stat <> "HOLD" Then
                        DisableFields()
                    End If
                Else
                    Try
                        wonum = Request.QueryString("wo").ToString
                        lblwo.Value = wonum
                        If stat <> "HOLD" Then
                            DisableFields()
                        End If
                    Catch ex As Exception
                        If stat <> "HOLD" Then
                            DisableFields()
                        End If
                    End Try
                End If
            End If
        Else

        End If

    End Sub

    Private Function CheckStat(ByVal wonum As String) As String
        Dim stat As String
        sql = "select status from workorder where wonum = '" & wonum & "'"
        dr = tasks.GetRdrData(sql)
        While dr.Read
            stat = dr.Item("status").ToString
        End While
        dr.Close()
        Return stat
    End Function
    Private Sub GetDef()
        Dim cid As String = "0"
        Dim stat As String
        sql = "select wostatus from wostatus where compid = '" & cid & "' and isdefault = 1"
        dr = tasks.GetRdrData(sql)
        While dr.Read
            stat = dr.Item("wostatus").ToString
        End While
        dr.Close()
        lblstat.Value = stat
    End Sub
    Private Sub CheckDown()
        Dim isdown As String
        wonum = lblwo.Value
        sql = "select isdown from workorder where wonum = '" & wonum & "'"
        dr = tasks.GetRdrData(sql)
        While dr.Read
            isdown = dr.Item("isdown").ToString

        End While
        dr.Close()
        lblisdown.Value = isdown
        If isdown = "1" Then
            cbisdown.Checked = True
            cbisnot.Checked = False
        Else
            cbisnot.Checked = True
            cbisdown.Checked = False
        End If

    End Sub
    Private Sub DisableFields()
        dddepts.Enabled = False
        ddcells.Enabled = False
        ddeq.Enabled = False
        ddnc.Enabled = False
        cbisdown.Disabled = True
        cbisnot.Disabled = True
    End Sub
    Private Sub CleanFields()
        lblsid.Value = ""
        lbldept.Value = ""
        lblclid.Value = ""
        lblchk.Value = ""
        lbleqid.Value = ""
        lblfuid.Value = ""
        lblcoid.Value = ""
        lbllid.Value = ""
        lbltyp.Value = ""
        lblgetarch.Value = "yes"
    End Sub
    Private Sub PopDepts(ByVal sid As String)
        filt = " where siteid = '" & sid & "'"
        dt = "Dept"
        val = "dept_id , dept_line "
        dr = tasks.GetList(dt, val, filt)
        dddepts.DataSource = dr
        dddepts.DataTextField = "dept_line"
        dddepts.DataValueField = "dept_id"
        dddepts.DataBind()
        dr.Close()
        dddepts.Items.Insert(0, "Select Department")
    End Sub
    Public Function CellCheck(ByVal filt As String) As String
        Dim cells As String
        Dim cellcnt As Integer
        sql = "select count(*) from Cells where Dept_ID = '" & filt & "'"
        cellcnt = tasks.Scalar(sql)
        Dim nocellcnt As Integer
        If cellcnt = 0 Then
            cells = "no"
        ElseIf cellcnt = 1 Then
            sql = "select count(*) from Cells where Dept_ID = '" & filt & "' and cell_name = 'No Cells'"
            nocellcnt = tasks.Scalar(sql)
            If nocellcnt = 1 Then
                cells = "no"
            Else
                cells = "yes"
            End If
        Else
            cells = "yes"
        End If
        Return cells
    End Function
    Private Sub PopCells(ByVal dept As String)
        filt = " where dept_id = " & dept
        dt = "Cells"
        val = "cellid , cell_name "
        dr = tasks.GetList(dt, val, filt)
        ddcells.DataSource = dr
        ddcells.DataTextField = "cell_name"
        ddcells.DataValueField = "cellid"
        ddcells.DataBind()
        dr.Close()
        ddcells.Items.Insert(0, "Select Station/Cell")
        celldiv.Style.Add("display", "block")
        celldiv.Style.Add("visibility", "visible")
    End Sub
    Private Sub PopEq(ByVal var As String, ByVal typ As String)
        lid = lbllid.Value
        did = lbldept.Value
        clid = lblclid.Value
        sid = lblsid.Value
        Dim eqcnt As Integer
        If typ = "d" Then
            If lid = "" Then
                sql = "select count(*) from equipment where dept_id = '" & did & "'"
                filt = " where dept_id = '" & did & "'"
            Else
                sql = "select count(*) from equipment where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            End If

        ElseIf typ = "c" Then
            If lid = "" Then
                sql = "select count(*) from equipment where cellid = '" & clid & "'"
                filt = " where cellid = '" & clid & "'"
            Else
                sql = "select count(*) from equipment where cellid = '" & clid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & clid & "' and locid = '" & clid & "'"
            End If

        ElseIf typ = "l" Then
            If clid <> "" Then
                sql = "select count(*) from equipment where cellid = '" & clid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & clid & "' and locid = '" & lid & "'"
            ElseIf did <> "" Then
                sql = "select count(*) from equipment where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            Else
                sql = "select count(*) from equipment where locid = '" & lid & "' and siteid = '" & sid & "'"
                filt = " where locid = '" & lid & "' and siteid = '" & sid & "'"
            End If
            lblreqd.Value = "change"
        End If
        eqcnt = tasks.Scalar(sql)
        If eqcnt > 0 Then
            dt = "equipment"
            val = "eqid, eqnum "

            dr = tasks.GetList(dt, val, filt)
            ddeq.DataSource = dr
            ddeq.DataTextField = "eqnum"
            ddeq.DataValueField = "eqid"
            ddeq.DataBind()
            dr.Close()
            ddeq.Items.Insert(0, "Select Equipment")
            eqdiv.Style.Add("display", "block")
            eqdiv.Style.Add("visibility", "visible")
            ddeq.Enabled = True
            lblgetarch.Value = "yes"
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr619" , "wrman.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ddeq.Items.Insert(0, "Select Equipment")
            eqdiv.Style.Add("display", "block")
            eqdiv.Style.Add("visibility", "visible")
            ddeq.Enabled = False
            If typ = "l" Then
                lblloc.Text = ""
                lbllid.Value = ""
            End If
        End If
        GetLoc()
    End Sub
    Private Sub PopNC(ByVal var As String, ByVal typ As String)
        lid = lbllid.Value
        did = lbldept.Value
        clid = lblclid.Value
        sid = lblsid.Value
        Dim eqcnt As Integer
        If typ = "d" Then
            If lid = "" Then
                sql = "select count(*) from noncritical where dept_id = '" & did & "'"
                filt = " where dept_id = '" & did & "'"
            Else
                sql = "select count(*) from noncritical where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            End If

        ElseIf typ = "c" Then
            If lid = "" Then
                sql = "select count(*) from noncritical where cellid = '" & clid & "'"
                filt = " where cellid = '" & clid & "'"
            Else
                sql = "select count(*) from noncritical where cellid = '" & clid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & clid & "' and locid = '" & clid & "'"
            End If

        ElseIf typ = "l" Then
            If clid <> "" Then
                sql = "select count(*) from noncritical where cellid = '" & clid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & clid & "' and locid = '" & lid & "'"
            ElseIf did <> "" Then
                sql = "select count(*) from noncritical where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            Else
                sql = "select count(*) from noncritical where locid = '" & lid & "' and siteid = '" & sid & "'"
                filt = " where locid = '" & lid & "' and siteid = '" & sid & "'"
                lblreqd.Value = "change"
            End If

        End If
        eqcnt = tasks.Scalar(sql)
        If eqcnt > 0 Then
            dt = "noncritical"
            val = "ncid, ncnum "

            dr = tasks.GetList(dt, val, filt)
            ddnc.DataSource = dr
            ddnc.DataTextField = "ncnum"
            ddnc.DataValueField = "ncid"
            ddnc.DataBind()
            dr.Close()
            ddnc.Items.Insert(0, "Select Asset")
            ncdiv.Style.Add("display", "block")
            ncdiv.Style.Add("visibility", "visible")
            ddnc.Enabled = True
            'lblgetarch.Value = "yes"
        Else
            'Dim strMessage As String =  tmod.getmsg("cdstr620" , "wrman.aspx.vb")
 
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ddnc.Items.Insert(0, "Select Asset")
            ncdiv.Style.Add("display", "block")
            ncdiv.Style.Add("visibility", "visible")
            ddnc.Enabled = False
        End If
        GetLoc()
    End Sub
    Private Sub GetLoc()
        lid = lbllid.Value
        If lid <> "" And lid <> "0" Then
            sql = "select location, description from pmlocations where locid = '" & lid & "'"
            dr = tasks.GetRdrData(sql)
            While dr.Read
                lblloc.Text = dr.Item("location").ToString
            End While
            dr.Close()
        End If

    End Sub

    Private Sub dddepts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dddepts.SelectedIndexChanged
        Dim dept As String = dddepts.SelectedValue.ToString
        lblreqd.Value = "change"
        tasks.Open()
        If dddepts.SelectedIndex <> 0 Then
            lbldept.Value = dept

            celldiv.Style.Add("display", "none")
            celldiv.Style.Add("visibility", "hidden")
            lblclid.Value = ""
            eqdiv.Style.Add("display", "none")
            eqdiv.Style.Add("visibility", "hidden")
            lbleqid.Value = ""
            ncdiv.Style.Add("display", "none")
            ncdiv.Style.Add("visibility", "hidden")
            lblncid.Value = ""

            CheckTasks("cefc")
            Dim deptname As String = dddepts.SelectedItem.ToString
            wonum = lblwo.Value
            sql = "update workorder set deptid = '" & dept & "' where wonum = '" & wonum & "'"
            tasks.Update(sql)
            lblgetarch.Value = "yes"
            If tl = "2" Or deptname = "No Dept/Line" Then
                'GoToTasks()
            Else
                Filter = dept
                Dim chk As String = CellCheck(Filter)
                If chk = "no" And tl = "3" Then
                    lblchk.Value = "no"
                    lblpar2.Value = "chk"
                    Dim strMessage As String =  tmod.getmsg("cdstr621" , "wrman.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")

                ElseIf chk = "no" Then
                    lblchk.Value = "no"
                    lblclid.Value = "0"
                    lblgetarch.Value = "yes"
                    eqdiv.Style.Add("display", "none")
                    eqdiv.Style.Add("visibility", "hidden")

                    PopEq(dept, "d")
                Else
                    lblchk.Value = "yes"
                    lblpar2.Value = "chk"
                    PopCells(dept)
                End If
            End If
        Else
            celldiv.Style.Add("display", "none")
            celldiv.Style.Add("visibility", "hidden")
            lblclid.Value = ""
            eqdiv.Style.Add("display", "none")
            eqdiv.Style.Add("visibility", "hidden")
            lbleqid.Value = ""

            'CheckTasks("cefc")
        End If
        tasks.Dispose()

    End Sub
    Private Sub CheckTasks(ByVal which As String)
        Dim wo As String = lblwo.Value
        sql = "usp_resetwo '" & which & "', '" & wo & "'"
        tasks.Update(sql)
        lblreset.Value = "fail"
    End Sub

    Private Sub ddcells_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcells.SelectedIndexChanged
        wonum = lblwo.Value
        Dim cell As String = ddcells.SelectedValue.ToString
        If ddcells.SelectedIndex <> 0 Then
            tasks.Open()
            Try

                lblclid.Value = cell
                lblpar.Value = "cell"
                lblgetarch.Value = "yes"
                wonum = lblwo.Value
                sql = "update workorder set cellid = '" & cell & "' where wonum = '" & wonum & "'"
                tasks.Update(sql)
                PopEq(cell, "c")
                PopNC(cell, "c")

            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr622" , "wrman.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try
        End If
        lbleqid.Value = ""
        lblncid.Value = ""
        'CheckTasks("efc")
        tasks.Dispose()
    End Sub

    Private Sub ddeq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddeq.SelectedIndexChanged
        eq = ddeq.SelectedValue.ToString
        lbleqid.Value = eq
        lblpar.Value = "eq"
        wonum = lblwo.Value
        'lblgetarch.Value = "yes"
        tasks.Open()
        Try
            If ddeq.SelectedIndex <> 0 Then
                wonum = lblwo.Value
                sql = "update workorder set eqid = '" & eq & "' where wonum = '" & wonum & "'"
                tasks.Update(sql)
                did = lbldept.Value
                clid = lblclid.Value
                If did = "" Then
                    CheckDept(eq)
                End If
                tdisdown.Attributes.Add("class", "label")
            End If
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr623" , "wrman.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
        'CheckTasks("fc")
        tasks.Dispose()
    End Sub
    Private Sub CheckDept(ByVal eqid As String)
        Dim deptcheck As Integer
        sql = "select dept_id, cellid from equipment where eqid = '" & eqid & "'"
        dr = tasks.GetRdrData(sql)
        While dr.Read
            did = dr.Item("dept_id").ToString
            clid = dr.Item("cellid").ToString
        End While
        dr.Close()
        lbldept.Value = did
        lblclid.Value = clid
        If did <> "" Then
            Try
                dddepts.SelectedValue = did
                Filter = did
            Catch ex As Exception
                Try
                    dddepts.SelectedIndex = 0
                Catch ex1 As Exception

                End Try

                deptcheck = 1
            End Try
            If deptcheck <> 1 Then
                Dim cellchk As Integer = 0
                Dim chk As String = CellCheck(Filter)
                If chk = "no" Then
                    lblchk.Value = "no"
                Else
                    lblchk.Value = "yes"
                    PopCells(did)
                    Try
                        ddcells.SelectedValue = clid ' check
                        lblclid.Value = clid
                        lblpar.Value = "cell"
                    Catch ex As Exception
                        Try
                            ddcells.SelectedIndex = 0
                        Catch ex1 As Exception

                        End Try

                        cellchk = 1
                    End Try
                End If
            End If
        End If

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1616.Text = axlabs.GetASPXPage("wrman.aspx", "lang1616")
        Catch ex As Exception
        End Try
        Try
            lang1617.Text = axlabs.GetASPXPage("wrman.aspx", "lang1617")
        Catch ex As Exception
        End Try
        Try
            lang1618.Text = axlabs.GetASPXPage("wrman.aspx", "lang1618")
        Catch ex As Exception
        End Try
        Try
            lang1619.Text = axlabs.GetASPXPage("wrman.aspx", "lang1619")
        Catch ex As Exception
        End Try
        Try
            lang1620.Text = axlabs.GetASPXPage("wrman.aspx", "lang1620")
        Catch ex As Exception
        End Try
        Try
            lang1621.Text = axlabs.GetASPXPage("wrman.aspx", "lang1621")
        Catch ex As Exception
        End Try
        Try
            lang1622.Text = axlabs.GetASPXPage("wrman.aspx", "lang1622")
        Catch ex As Exception
        End Try
        Try
            lang1623.Text = axlabs.GetASPXPage("wrman.aspx", "lang1623")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgsw.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("wrman.aspx", "imgsw") & "')")
            imgsw.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid209.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("wrman.aspx", "ovid209") & "')")
            ovid209.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
