﻿Imports System.Data.SqlClient
Public Class woroot
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim rt As New Utilities
    Dim wonum, user, userid, rootid, rootcause, corraction, enterdate, modbyid, modby, moddate, comments As String
    Dim orootid, cuserid, cuser As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        Dim lang As String = lblfslang.Value
        poplang(lang)

        If Not IsPostBack Then
            wonum = Request.QueryString("wonum").ToString
            cuser = Request.QueryString("user").ToString
            cuserid = Request.QueryString("userid").ToString
            orootid = Request.QueryString("rootid").ToString
            lblwo.Value = wonum
            lblcuser.Value = cuser
            lblcuserid.Value = cuserid
            lblorootid.Value = orootid
            lblrootid.Value = orootid
            If orootid <> "" Then
                rt.Open()
                getdata()
                rt.Dispose()
            End If
        Else
            If Request.Form("lblsubmit") = "go" Then
                rt.Open()
                savedata()
                rt.Dispose()
            End If
        End If
    End Sub
    Private Sub poplang(ByVal lang As String)
        If lang = "fre" Then
            tdroot.InnerHtml = "Cause Fondamentale"
            tdenter.InnerHtml = "Entré par"
            tdedate.InnerHtml = "Date d'entrée"
            tdmodbyl.InnerHtml = "Modifié par"
            tdmodd.InnerHtml = "Date de modification"
            tdmsg1.InnerHtml = "Écrire dans la boite de texte des Causes Racine pour filtrer les champs"
            tdmsg2.InnerHtml = "Écrire % dans la boite de texte des Causes Racine pour tout voir"
            tdcorrac.InnerHtml = "Action corrective"
            tdcomm.InnerHtml = "Commentaires"
        End If
    End Sub
    Private Sub getdata(Optional ByVal sd As Integer = 0)
        Dim rcnt As Integer = 0
        wonum = lblwo.Value
        sql = "select * from worootcause where wonum = '" & wonum & "'"
        dr = rt.GetRdrData(sql)
        While dr.Read
            rcnt = 1
            rootid = dr.Item("rootid").ToString
            rootcause = dr.Item("rootcause").ToString
            corraction = dr.Item("corraction").ToString
            comments = dr.Item("comments").ToString

            enterdate = dr.Item("enterdate").ToString
            user = dr.Item("enterby").ToString
            userid = dr.Item("enterbyid").ToString

            moddate = dr.Item("modifieddate").ToString
            modby = dr.Item("modifiedby").ToString
            modbyid = dr.Item("modifiedbyid").ToString

        End While
        dr.Close()
        If rcnt <> 0 Then
            lblorootid.Value = rootid
            lblrootid.Value = rootid
            txtrootauto.Text = rootcause
            txtcorraction.Text = corraction
            txtcomments.Text = comments
            lbluser.Value = user
            lbluserid.Value = userid
            lblmodby.Value = modby
            lblmodbyid.Value = modbyid
            lblenterdate.Value = enterdate
            lblmoddate.Value = moddate
            tdenterby.InnerHtml = user
            tdenterdate.InnerHtml = enterdate
            tdmodby.InnerHtml = modby
            tdmoddate.InnerHtml = moddate

        End If
    End Sub
    Private Sub savedata()
        orootid = lblorootid.Value
        wonum = lblwo.Value
        Dim sh, si As String
        rootcause = txtrootauto.Text
        sql = "select rootid from rootcause where rootcause = '" & rootcause & "'"
        Try
            rootid = rt.strScalar(sql)
        Catch ex As Exception
            rootid = ""
        End Try

        If rootid = "" Then
            Dim strMessage As String = "Root Cause Not Found"
            rt.CreateMessageAlert(Me, strMessage, "strKey1")
            getdata()
            Exit Sub
        Else
            lblrootid.Value = rootid
            sh = txtcorraction.Text
            sh = Replace(sh, "'", Chr(180), , , vbTextCompare)
            sh = Replace(sh, "--", "-", , , vbTextCompare)
            sh = Replace(sh, ";", ":", , , vbTextCompare)
            sh = Replace(sh, "/", " ", , , vbTextCompare)
            If Len(sh) > 1000 Then
                Dim strMessage As String = "Corrective Action Limited to 1000 Characters"
                rt.CreateMessageAlert(Me, strMessage, "strKey1")
                getdata()
                Exit Sub
            End If
            si = txtcomments.Text
            si = Replace(si, "'", Chr(180), , , vbTextCompare)
            si = Replace(si, "--", "-", , , vbTextCompare)
            si = Replace(si, ";", ":", , , vbTextCompare)
            si = Replace(si, "/", " ", , , vbTextCompare)
            If Len(si) > 1000 Then
                Dim strMessage As String = "Comments Limited to 1000 Characters"
                rt.CreateMessageAlert(Me, strMessage, "strKey1")
                getdata()
                Exit Sub
            End If
            user = lblcuser.Value
            userid = lblcuserid.Value
            modby = lblcuser.Value
            modbyid = lblcuserid.Value
            If orootid = "" Then
                enterdate = rt.CNOW
                
                Dim cmd As New SqlCommand("insert into worootcause (wonum, enterdate, enterbyid, enterby, rootid, rootcause, corraction, comments) values " _
                                          + "(@wonum, @enterdate, @userid, @user, @rootid, @rootcause, @sh, @si)")
                Dim param0 = New SqlParameter("@wonum", SqlDbType.VarChar)
                param0.Value = wonum
                cmd.Parameters.Add(param0)
                Dim param02 = New SqlParameter("@enterdate", SqlDbType.VarChar)
                If enterdate = "" Then
                    param02.Value = System.DBNull.Value
                Else
                    param02.Value = enterdate
                End If
                cmd.Parameters.Add(param02)
                Dim param03 = New SqlParameter("@userid", SqlDbType.VarChar)
                If userid = "" Then
                    param03.Value = System.DBNull.Value
                Else
                    param03.Value = userid
                End If
                cmd.Parameters.Add(param03)
                Dim param04 = New SqlParameter("@user", SqlDbType.VarChar)
                If user = "" Then
                    param04.Value = System.DBNull.Value
                Else
                    param04.Value = user
                End If
                cmd.Parameters.Add(param04)
                Dim param05 = New SqlParameter("@rootid", SqlDbType.VarChar)
                If rootid = "" Then
                    param05.Value = System.DBNull.Value
                Else
                    param05.Value = rootid
                End If
                cmd.Parameters.Add(param05)
                Dim param06 = New SqlParameter("@rootcause", SqlDbType.VarChar)
                If rootcause = "" Then
                    param06.Value = System.DBNull.Value
                Else
                    param06.Value = rootcause
                End If
                cmd.Parameters.Add(param06)
                Dim param07 = New SqlParameter("@sh", SqlDbType.VarChar)
                If sh = "" Then
                    param07.Value = System.DBNull.Value
                Else
                    param07.Value = sh
                End If
                cmd.Parameters.Add(param07)
                Dim param08 = New SqlParameter("@si", SqlDbType.VarChar)
                If si = "" Then
                    param08.Value = System.DBNull.Value
                Else
                    param08.Value = si
                End If
                cmd.Parameters.Add(param08)
                Try
                    rt.UpdateHack(cmd)
                Catch ex As Exception
                    Dim strMessage As String = "Problem Saving Record"
                    rt.CreateMessageAlert(Me, strMessage, "strKey1")
                    getdata()
                    Exit Sub
                End Try
            Else
                moddate = rt.CNOW
                Dim cmd As New SqlCommand("update worootcause set modifieddate = @moddate, modifiedbyid = @modbyid, modifiedby = @modby, " _
                                          + "rootid = @rootid, rootcause = @rootcause, corraction = @sh, comments = @si " _
                                          + "where wonum = @wonum")
                Dim param0 = New SqlParameter("@wonum", SqlDbType.VarChar)
                param0.Value = wonum
                cmd.Parameters.Add(param0)
                Dim param02 = New SqlParameter("@moddate", SqlDbType.VarChar)
                If moddate = "" Then
                    param02.Value = System.DBNull.Value
                Else
                    param02.Value = moddate
                End If
                cmd.Parameters.Add(param02)
                Dim param03 = New SqlParameter("@modbyid", SqlDbType.VarChar)
                If modbyid = "" Then
                    param03.Value = System.DBNull.Value
                Else
                    param03.Value = modbyid
                End If
                cmd.Parameters.Add(param03)
                Dim param04 = New SqlParameter("@modby", SqlDbType.VarChar)
                If modby = "" Then
                    param04.Value = System.DBNull.Value
                Else
                    param04.Value = modby
                End If
                cmd.Parameters.Add(param04)
                Dim param05 = New SqlParameter("@rootid", SqlDbType.VarChar)
                If rootid = "" Then
                    param05.Value = System.DBNull.Value
                Else
                    param05.Value = rootid
                End If
                cmd.Parameters.Add(param05)
                Dim param06 = New SqlParameter("@rootcause", SqlDbType.VarChar)
                If rootcause = "" Then
                    param06.Value = System.DBNull.Value
                Else
                    param06.Value = rootcause
                End If
                cmd.Parameters.Add(param06)
                Dim param07 = New SqlParameter("@sh", SqlDbType.VarChar)
                If sh = "" Then
                    param07.Value = System.DBNull.Value
                Else
                    param07.Value = sh
                End If
                cmd.Parameters.Add(param07)
                Dim param08 = New SqlParameter("@si", SqlDbType.VarChar)
                If si = "" Then
                    param08.Value = System.DBNull.Value
                Else
                    param08.Value = si
                End If
                cmd.Parameters.Add(param08)
                Try
                    rt.UpdateHack(cmd)
                Catch ex As Exception
                    Dim strMessage As String = "Problem Saving Record\n" & ex.ToString
                    rt.CreateMessageAlert(Me, strMessage, "strKey1")
                    getdata()
                    Exit Sub
                End Try
            End If
            getdata(1)
        End If
    End Sub
End Class