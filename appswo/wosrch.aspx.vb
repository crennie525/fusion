Imports System.Data.SqlClient
Public Class wosrch
    Inherits System.Web.UI.Page
    Dim filt, waid, wpaid, fdate, tdate, wtret, statret, wo, sa, sid, sup, lead As String
    Dim did, clid, lid, eqid, fuid, coid, rettyp, level, jl, who, co, ski, fpc, ua
    Dim supn, leadn As String
    Dim wastr, wpstr As String
    Dim wos As New mmenu_utils_a
    Dim srch As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwaidstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwpaidstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trdpick As System.Web.UI.HtmlControls.HtmlTableRow

    Protected WithEvents trdepts As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trdepts1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trdepts2 As System.Web.UI.HtmlControls.HtmlTableRow

    Protected WithEvents trloc3 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trloc3a As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trloc3b As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trloc3c As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trloc3d As System.Web.UI.HtmlControls.HtmlTableRow

    Protected WithEvents cbua As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbtarg As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents cbsched As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents cbact As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents cbsa As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbco As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents trsa As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblissaw As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcell As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrettyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllevel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljustloc As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdskill As System.Web.UI.HtmlControls.HtmlTableCell
    
    Protected WithEvents tddept As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcell As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents tdloc3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcomp3 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents tdlead As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsup As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trgetdates As System.Web.UI.HtmlControls.HtmlTableRow

    Protected WithEvents txtfpc As Global.System.Web.UI.WebControls.TextBox
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwaid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdwa As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdpa As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblwpaid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lang1543 As System.Web.UI.WebControls.Label
    Protected WithEvents txtfrom As System.Web.UI.WebControls.TextBox
    Protected WithEvents lang1544 As System.Web.UI.WebControls.Label
    Protected WithEvents txtto As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblfromdate As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltodate As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdstat As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgwt As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtwo As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                who = Request.QueryString("who").ToString
                lblwho.Value = who
                If who = "sch" Then
                    trgetdates.Attributes.Add("class", "details")
                End If
            Catch ex As Exception

            End Try
            Dim issaw As Integer = wos.SAW
            lblissaw.Value = issaw
            If issaw = 1 Then
                trsa.Attributes.Add("class", "view")
            Else
                trsa.Attributes.Add("class", "details")
            End If
            filt = Request.QueryString("filt").ToString
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            'waid + "~" + wpaid + "~" + fdate + "~" + tdate + "~" + wt + "~" + stat + "~" + wo;
            If Len(filt) > 0 Then
                Dim filtarr() As String = filt.Split("~")
                waid = filtarr(0).ToString
                wpaid = filtarr(1).ToString
                fdate = filtarr(2).ToString
                tdate = filtarr(3).ToString
                wtret = filtarr(4).ToString
                statret = filtarr(5).ToString
                wo = filtarr(6).ToString
                wastr = filtarr(7).ToString
                wpstr = filtarr(8).ToString
                Dim rt, rs, ra As String
                rt = filtarr(9).ToString
                rs = filtarr(10).ToString
                ra = filtarr(11).ToString
                sa = filtarr(12).ToString
                sup = filtarr(14).ToString
                lead = filtarr(15).ToString
                did = filtarr(16).ToString
                clid = filtarr(17).ToString
                lid = filtarr(18).ToString
                eqid = filtarr(19).ToString
                fuid = filtarr(20).ToString
                coid = filtarr(21).ToString
                rettyp = filtarr(22).ToString
                level = filtarr(23).ToString
                jl = filtarr(24).ToString
                fpc = filtarr(25).ToString
                txtfpc.Text = fpc
                co = filtarr(26).ToString
                ski = filtarr(27).ToString
                ua = filtarr(29).ToString
                If ua = "1" Then
                    cbua.Checked = True
                Else
                    cbua.Checked = False
                End If
                lblskillid.Value = ski
                lbljustloc.Value = jl
                If rt = "1" Or rs = "1" Or ra = "1" Then
                    trdpick.Attributes.Add("class", "view")
                End If
                If rt = "1" Then
                    cbtarg.Checked = True
                Else
                    cbtarg.Checked = False
                End If
                If rs = "1" Then
                    cbsched.Checked = True
                Else
                    cbsched.Checked = False
                End If
                If ra = "1" Then
                    cbact.Checked = True
                Else
                    cbact.Checked = False
                End If
                If sa = "1" Then
                    cbsa.Checked = True
                Else
                    cbsa.Checked = False
                End If
                If co = "1" Then
                    cbco.Checked = True
                Else
                    cbco.Checked = False
                End If
                If rt = "0" And rs = "0" And ra = "0" Then
                    cbtarg.Checked = True
                End If
                lblwaid.Value = waid
                lblwaidstr.Value = wastr
                lblwpaid.Value = wpaid
                lblwpaidstr.Value = wpstr
                tdpa.InnerHtml = wpstr
                tdwa.InnerHtml = wastr
                lblfromdate.Value = fdate
                txtfrom.Text = fdate
                lbltodate.Value = tdate
                txtto.Text = tdate
                lblwt.Value = wtret
                tdwt.InnerHtml = wtret
                lblstat.Value = statret
                tdstat.InnerHtml = statret
                txtwo.Text = wo
                lblsup.Value = sup
                lbllead.Value = lead
                lbldid.Value = did
                lblclid.Value = clid
                lbllid.Value = lid
                lbleqid.Value = eqid
                lblfuid.Value = fuid
                lblcoid.Value = coid
                lblrettyp.Value = rettyp
                lbllevel.Value = level
                srch.Open()
                If sup <> "" Then
                    getsup(sup)
                End If
                If lead <> "" Then
                    getlead(lead)
                End If
                If ski <> "" Then
                    getski(ski)
                End If
                If rettyp = "depts" Then
                    getdepts(did, clid, eqid, fuid, coid)
                ElseIf rettyp = "locs" Then
                    getlocs(lid, eqid, fuid, coid)
                End If
                srch.Dispose()
            Else
                cbtarg.Checked = False
                cbsched.Checked = True
                cbact.Checked = False
                cbsa.Checked = False
                cbco.Checked = False
                lbljustloc.Value = "0"
            End If

        End If
    End Sub
    Private Sub getski(ByVal ski As String)
        Dim skill As String
        sql = "select skill from pmskills where skillid = '" & ski & "'"
        dr = srch.GetRdrData(sql)
        While dr.Read
            skill = dr.Item("skill").ToString
        End While
        dr.Close()
        lblskill.Value = skill
        tdskill.InnerHtml = skill
    End Sub
    Private Sub getdepts(ByVal did As String, ByVal clid As String, ByVal eqid As String, ByVal fuid As String, ByVal coid As String)
        Dim dept, cell, eqnum, func, comp As String

        sql = "declare @dept varchar(100), @cell varchar(100), @eqnum varchar(100), @func varchar(100), @comp varchar(100) " _
            + "set @dept = (select dept_line from dept where dept_id = '" & did & "') " _
            + "set @cell = (select cell_name from cells where cellid = '" & clid & "') " _
            + "set @eqnum = (select eqnum from equipment where eqid = '" & eqid & "') " _
            + "set @func = (select func from functions where func_id = '" & fuid & "')" _
            + "set @comp = (select compnum from components where comid = '" & coid & "') " _
            + "select @dept as dept_line, @cell as cell_name, @eqnum as eqnum, @func as func, @comp as compnum"

        dr = srch.GetRdrData(sql)
        While dr.Read
            dept = dr.Item("dept_line").ToString
            cell = dr.Item("cell_name").ToString
            eqnum = dr.Item("eqnum").ToString
            func = dr.Item("func").ToString
            comp = dr.Item("compnum").ToString
        End While
        dr.Close()
        trdepts.Attributes.Add("class", "view")
        trdepts1.Attributes.Add("class", "view")
        trdepts2.Attributes.Add("class", "view")
        tddept.InnerHtml = dept
        lbldept.Value = dept
        tdcell.InnerHtml = cell
        lblcell.Value = cell
        tdeq.InnerHtml = eqnum
        lbleq.Value = eqnum
        tdfu.InnerHtml = func
        lblfu.Value = func
        tdcomp.InnerHtml = comp
        lblcomp.Value = comp
    End Sub
    Private Sub getlocs(ByVal lid As String, ByVal eqid As String, ByVal fuid As String, ByVal coid As String)
        Dim loc, eqnum, func, comp As String

        sql = "declare @loc varchar(100), @eqnum varchar(100), @func varchar(100), @comp varchar(100) " _
            + "set @loc = (select location from pmlocations where locid = '" & lid & "') " _
            + "set @eqnum = (select eqnum from equipment where eqid = '" & eqid & "') " _
            + "set @func = (select func from functions where func_id = '" & fuid & "')" _
            + "set @comp = (select compnum from components where comid = '" & coid & "') " _
            + "select @loc as loc, @eqnum as eqnum, @func as func, @comp as compnum"

        dr = srch.GetRdrData(sql)
        While dr.Read
            loc = dr.Item("loc").ToString
            eqnum = dr.Item("eqnum").ToString
            func = dr.Item("func").ToString
            comp = dr.Item("compnum").ToString
        End While
        dr.Close()
        trloc3.Attributes.Add("class", "view")
        trloc3a.Attributes.Add("class", "view")
        trloc3b.Attributes.Add("class", "view")
        tdloc3.InnerHtml = loc
        lblloc.Value = loc
       
        tdeq3.InnerHtml = eqnum
        lbleq.Value = eqnum
        tdfu3.InnerHtml = func
        lblfu.Value = func
        tdcomp3.InnerHtml = comp
        lblcomp.Value = comp
        lbljustloc.Value = "1"
    End Sub
    Private Sub getlead(ByVal lead As String)
        sql = "select username from pmsysusers where userid = '" & lead & "'"
        Try
            leadn = srch.strScalar(sql)
            lbllead.Value = lead
            tdlead.InnerHtml = leadn
        Catch ex As Exception

        End Try


    End Sub
    Private Sub getsup(ByVal sup As String)
        sql = "select username from pmsysusers where userid = '" & sup & "'"
        Try
            supn = srch.strScalar(sql)
            lblsup.Value = sup
            tdsup.InnerHtml = supn
        Catch ex As Exception

        End Try
    End Sub
End Class
