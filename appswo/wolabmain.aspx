<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wolabmain.aspx.vb" Inherits="lucy_r12.wolabmain" %>

<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wolabmain</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/sessrefdialog.js"></script>
    <script language="javascript" type="text/javascript">
        <!--
        function gotoreq(href, wt, wo, sid) {
            //alert(href)
            //alert(document.getElementById("lblhref").value)
            document.getElementById("lblwt").value = wt;
            document.getElementById("lblwo").value = wo;
            //document.getElementById("lblhref").value=href;
            document.getElementById("ifwo").src = "woman3.aspx?" + href;
        }

        var timer = window.setTimeout("doref();", 1205000);
        function doref() {
            resetsess();
        }
        function setref() {
            window.clearTimeout(timer);
            timer = window.setTimeout("doref();", 1205000);
        }
        function handlejp() {
            //window.parent.
            setref();
            var won = document.getElementById("lblwo").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            //alert("../appsrt/jobplanmain2.aspx?start=wo&jid=0&wo=" + won + "&sid=" + sid + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper)
            window.location = "../appsrt/jobplanmain2.aspx?start=wo&jid=0&wo=" + won + "&sid=" + sid + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }

        function getfwp1() {
            var won = document.getElementById("lblwo").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            //alert("wopmlist3.aspx?who=wo&wo=" + won + "&sid=" + sid + "&uid=" + userid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper)
            window.location = "wopmlist3.aspx?who=wo&wo=" + won + "&sid=" + sid + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        function getsdp1() {
            var won = document.getElementById("lblwo").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            window.location = "../appswo/wopmlist7.aspx?who=wo&wo=" + won + "&sid=" + sid + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        function getwob1() {
            var won = document.getElementById("lblwo").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            window.location = "../appswo/wobacklog.aspx?who=wo&wo=" + won + "&sid=" + sid + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }

        function handlesched() {
            var href = document.getElementById("lblhref").value;
            var sid = document.getElementById("lblsid").value;
            var ro = "0"; //document.getElementById("lblro").value;
            var eqid = "0";
            var won = document.getElementById("lblwo").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            href = href.replace(/&/g, "~")
            window.location = "../appsman/PMScheduling2.aspx?jump=yes&typ=wo&href=" + href + "&ro=" + ro + "&sid=" + sid + "&eqid=" + eqid + "&wo=" + won + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }

        function handlewo(wo) {
            document.getElementById("lblwo").value = wo;
        }
        function handleswjplan(jpid) {
            setref();
            var href = document.getElementById("lblhref").value;

            var wo = document.getElementById("lblwo").value
            var ro = document.getElementById("lblro").value;
            href = href.replace(/&/g, "~")
            //var harr = href.split("~");
            //var remwo = harr[1] + "~";
            //href = href.replace(remwo, "")
            //alert(href)
            //alert("from wolabmain wojpedit.aspx?jpid=" + jpid + "&wonum=" + wo + "&href=" + href)
            window.location = "wojpeditmain.aspx?jpid=" + jpid + "&wonum=" + wo + "&href=" + href + "&ro=" + ro;
        }
        function pageScroll() {

        }
        //-->
    </script>
</head>
<body onload="resetsess();">
    <uc1:mmenu1 ID="Mmenu11" runat="server"></uc1:mmenu1>
    <form id="form1" method="post" runat="server">
    <table cellpadding="0" cellspacing="0" style="position: absolute; top: 78px; left: 0px">
        <tr>
            <td valign="top">
                <iframe id="ifwo" runat="server" src="" width="650" height="560" frameborder="no">
                </iframe>
            </td>
            <td valign="top">
                <iframe id="iflist" runat="server" src="" width="605" height="560" frameborder="no">
                </iframe>
            </td>
        </tr>
        <tr>
        </tr>
    </table>
    <iframe id="ifsession" class="details" src="" frameborder="no" runat="server" style="z-index: 0">
    </iframe>
    <input type="hidden" id="lblsessrefresh" runat="server" name="lblsessrefresh">
    <input type="hidden" id="lblwt" runat="server">
    <input type="hidden" id="lblwo" runat="server">
    <input type="hidden" id="lblhref" runat="server">
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lblwonum" runat="server">
    <input type="hidden" id="lblusername" runat="server">
    <input type="hidden" id="lbluid" runat="server">
    <input type="hidden" id="lblislabor" runat="server">
    <input type="hidden" id="lblissuper" runat="server">
    <input type="hidden" id="lblisplanner" runat="server">
    <input type="hidden" id="lblLogged_In" runat="server" name="lblLogged_In">
    <input type="hidden" id="lblro" runat="server" name="lblro">
    <input type="hidden" id="lblms" runat="server" name="lblms">
    <input type="hidden" id="lblappstr" runat="server" name="lblappstr">
    </form>
</body>
</html>
