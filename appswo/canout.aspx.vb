﻿Imports System.Data.SqlClient
Public Class canout
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim sc As New Utilities
    Dim mu As New mmenu_utils_a
    Dim tmod As New transmod
    Dim PageNumber As String
    Dim PageSize As String = "50"
    Dim Filter, FilterCNT, Sort As String
    Dim sConfigPosition, sEquipment, sStructureType, sService, sResponsible, sDescription, sPlanningGroup, sPriority As String
    Dim sApprovedBy, sReasonCode, sStartDate, sFinishDate, sTxt1, sTxt2, sErrorCode1, sErrorCode2, sErrorCode3, sComplaintType As String
    Dim sItemNumber, iPlannedQuantity, sRequestedStartDte, sRequestedFinishDte, iSetupTime, iRuntime As String
    Dim iPlannedNumWkrs, sTextBlock, iOperation, sTransactionID, pmid, tpmid, wonum, sup, superid, wa, waid, lc, lcid As String
    Dim iRetroFlag As String = "1"
    Dim sLotNumber As String = "1"
    Dim sFacility As String = "CAN"
    Dim MWNO, OPNO, RPRE, UMAT, PCTP, REND As String
    Dim RPDT As String
    Dim UMAS, DOWT, DLY1, DLY2 As String
    Dim EMNO, FCLA, FCL2, FCL3 As String
    Dim retval As String
    Dim isact As String
    Dim CONO As String = "1"
    Dim errcnt As Integer = 0
    Dim labflag As Integer = 0
    Dim s1flag, s2flag As String
    Dim s3flag, s4flag As String
    Dim woecd, wnums As String
    Dim ldkey, ldesc, reportedby, reportedbyid As String
    Dim lcnt As String '= lbllcnt.Value
    Dim rwo As String '= lblMWNO.Value
    Dim nodate As Date '= sc.CNOW
    Dim coid, wt As String
    Dim retwonum_chk As String
    Dim ed2id, ed3id, oid As String
    Dim wofailid As String = ""
    Dim compfailid As String = ""
    Dim ed1id As String = ""
    Dim ecd1id, ecd2id, ecd3id As String
    Dim cansend As New Service1
    Dim retstring As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Private Function SendCant(ByVal wonum As String, ByVal rwo As String) As String

        'second check for existing Lawson work order number
        If rwo = "" Then
            sql = "select retwonum from workorder where wonum = '" & wonum & "'"
            retwonum_chk = sc.strScalar(sql)
            dr.Close()
            If retwonum_chk <> "" Then
                rwo = retwonum_chk
            End If
        End If

        If rwo = "" Then
            'get transaction id
            sql = "select seed from pmseed where pmtable = 'service'; update pmseed set seed = seed + 1 where pmtable = 'service'"
            sTransactionID = sc.strScalar(sql)

            'get general data
            sql = "select e.eqnum, w.targstartdate, w.targcompdate, cast(isnull(w.actlabhrs, 0) as decimal(10,2)) as actlabhrs, pnw = (select count(*) from woassign where wonum = '" & wonum & "'), " _
            + "x.POSEQIP, x.RESPONSIBLE, isnull(w.actstart, w.schedstart) as actstart, w.actfinish, w.description, w.worktype, w.wopriority, w.reportedby, wa.workarea, w.wopriority, w.superid, w.leadcraftid, " _
            + "w.targstartdate, w.targcompdate, cast(isnull(w.estlabhrs, 0) as decimal(10,2)) as estlabhrs, cast(isnull(w.estjplabhrs, 0) as decimal(10,2)) as estjplabhrs, w.qty, t.xcol, t.xcol2, w.pmid, w.tpmid, l.longdesc, w.comid " _
            + "from workorder w " _
            + "left join equipment e on e.eqid = w.eqid " _
            + "left join equipment_xtra x on x.eqid = e.eqid " _
            + "left join workareas wa on wa.waid = w.waid " _
            + "left join wotype t on t.wotype = w.worktype " _
            + "left join wolongdesc l on l.wonum = w.wonum " _
            + "where w.wonum = '" & wonum & "'"
            dr = sc.GetRdrData(sql)
            While dr.Read
                coid = dr.Item("comid").ToString
                wt = dr.Item("worktype").ToString
                superid = dr.Item("superid").ToString
                lcid = dr.Item("leadcraftid").ToString
                sConfigPosition = dr.Item("POSEQIP").ToString
                sEquipment = dr.Item("eqnum").ToString
                sService = dr.Item("xcol2").ToString
                reportedby = dr.Item("reportedby").ToString
                sDescription = dr.Item("description").ToString
                sDescription = wonum & " - " & sDescription
                sDescription = Mid(sDescription, 1, 39)
                sPlanningGroup = dr.Item("workarea").ToString
                sPriority = dr.Item("wopriority").ToString
                sItemNumber = dr.Item("eqnum").ToString
                iSetupTime = dr.Item("estjplabhrs").ToString
                iRuntime = dr.Item("estlabhrs").ToString
                iPlannedNumWkrs = dr.Item("qty").ToString
                sTextBlock = dr.Item("description").ToString
                ldesc = dr.Item("longdesc").ToString
                iOperation = dr.Item("xcol").ToString
                pmid = dr.Item("pmid").ToString
                tpmid = dr.Item("tpmid").ToString

                sStartDate = dr.Item("actstart").ToString
                sFinishDate = dr.Item("actfinish").ToString
                sRequestedStartDte = dr.Item("targstartdate").ToString
                sRequestedFinishDte = dr.Item("targcompdate").ToString
            End While
            dr.Close()

            'format dates for Lawson
            Dim sdate As Date = sStartDate
            sStartDate = sdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
            Dim fdate As Date = sFinishDate
            sFinishDate = fdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
            Dim rsfdate As Date = sRequestedStartDte
            sRequestedStartDte = rsfdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
            Dim rfdate As Date = sRequestedFinishDte
            sRequestedFinishDte = rfdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

            'get error code id values based on work type
            If wt <> "PM" And wt <> "TPM" Then
                sql = "select top 1 wofailid, compfailid from wofailmodes1 where wonum = '" & wonum & "' and ecd1id is null"
                dr = sc.GetRdrData(sql)
                While dr.Read
                    wofailid = dr.Item("wofailid").ToString
                    compfailid = dr.Item("compfailid").ToString
                End While
                dr.Close()
                If wofailid <> "" Then
                    sql = "select ecd1id, ecd2id, ecd3id, oaid from componentfailmodes where compfailid = '" & compfailid & "'"
                    dr = sc.GetRdrData(sql)
                    While dr.Read
                        ed1id = dr.Item("ecd1id").ToString
                        ed2id = dr.Item("ecd2id").ToString
                        ed3id = dr.Item("ecd3id").ToString
                        oid = dr.Item("oaid").ToString
                    End While
                    dr.Close()
                End If
            Else
                sql = "select top 1 ecd1id, ecd2id, ecd3id from wofailmodes1 where ecd1id in (select ecd1id from ecd1) " _
                    + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = wofailmodes1.ecd1id) " _
                    + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = wofailmodes1.ecd2id) " _
                    + "and wonum = '" & wonum & "'"
                dr = sc.GetRdrData(sql)
                While dr.Read
                    ecd1id = dr.Item("ecd1id").ToString
                    ecd2id = dr.Item("ecd2id").ToString
                    ecd3id = dr.Item("ecd3id").ToString
                End While
                dr.Close()
            End If

            'get actual error code values
            sql = "select top 1 e1.ecd1, e2.ecd2, e3.ecd3 " _
                    + "from ecd1 e1 " _
                    + "left join ecd2 e2 on e2.ecd1id = e1.ecd1id " _
                    + "left join ecd3 e3 on e3.ecd2id = e2.ecd2id " _
                    + "where e1.ecd1id = '" & ecd1id & "' and e2.ecd2id = '" & ecd2id & "' and e3.ecd3id = '" & ecd3id & "'"
            dr = sc.GetRdrData(sql)
            While dr.Read
                sErrorCode1 = dr.Item("ecd1").ToString
                sErrorCode2 = dr.Item("ecd2").ToString
                sErrorCode3 = dr.Item("ecd3").ToString
            End While
            dr.Close()

            'check for responsible
            Dim res As String = getres(wonum, reportedby, superid)
            Dim retarr() As String = res.Split(",")
            sResponsible = retarr(1)
            reportedbyid = retarr(2)
            If sResponsible = "" Then
                retstring = "NORES"
            Else
                sPlanningGroup = getgroup(reportedbyid)

                cansend.Url = System.Configuration.ConfigurationManager.AppSettings("service1")

                retstring = cansend.OpenWorkOrder(sTransactionID, sConfigPosition, sEquipment, sStructureType, sService, sResponsible, iOperation, sDescription, _
                sPlanningGroup, sPriority, sApprovedBy, sReasonCode, sStartDate, sFinishDate, _
                sTxt1, sTxt2, sErrorCode1, sErrorCode2, sErrorCode3, sComplaintType, iRetroFlag, _
                sItemNumber, sLotNumber, iPlannedQuantity, sFacility, sRequestedStartDte, sRequestedFinishDte, _
                iSetupTime, iRuntime, iPlannedNumWkrs, sTextBlock)
            End If
        Else
            retstring = "LWOE"
        End If

        Return retstring
    End Function

    Private Function getres(ByVal wonum As String, ByVal reportedby As String, ByVal superid As String) As String
        sql = "select userid from pmsysusers where username = '" & reportedby & "'"
        reportedbyid = sc.strScalar(sql)
    End Function
    Private Function getgroup(ByVal reportedbyid As String)

    End Function

End Class