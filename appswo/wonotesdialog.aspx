﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wonotesdialog.aspx.vb" Inherits="lucy_r12.wonotesdialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Work Order Notes Dialog</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script type="text/javascript" language="javascript">
    <!--
        function gettab(who) {
        //alert(who)
            var req = document.getElementById("lblreq").value;
            var wonum = document.getElementById("lblwo").value;
            var noteid = document.getElementById("lblnoteid").value;
            var rootid = document.getElementById("lblrootid").value;
            var userid = document.getElementById("lblcuserid").value;
            var user = document.getElementById("lblcuser").value;
            var wh = document.getElementById("lblwho").value;
            if (who == "rc") {
                if (req = "yes") {
                    document.getElementById("tdrc").className = "thdrhovmini plainlabel";
                    document.getElementById("tdwn").className = "thdrmini plainlabel";
                    if (wh == "fs") {
                        document.getElementById("ifwo").src = "worootfs.aspx?wonum=" + wonum + "&rootid=" + rootid + "&noteid=" + noteid + "&userid=" + userid + "&user=" + user;
                    }
                    else {
                        document.getElementById("ifwo").src = "woroot.aspx?wonum=" + wonum + "&rootid=" + rootid + "&noteid=" + noteid + "&userid=" + userid + "&user=" + user;
                    }
                }
            }
            else if (who == "wn") {
                document.getElementById("tdrc").className = "thdrmini plainlabel";
                document.getElementById("tdwn").className = "thdrhovmini plainlabel";
                if (wh == "fs") {
                    document.getElementById("ifwo").src = "wonotesfs.aspx?wonum=" + wonum + "&noteid=" + noteid + "&rootid=" + rootid + "&userid=" + userid + "&user=" + user;
                }
                else {
                    document.getElementById("ifwo").src = "wonotes.aspx?wonum=" + wonum + "&noteid=" + noteid + "&rootid=" + rootid + "&userid=" + userid + "&user=" + user;
                }
            }
        }
        function handlereturn() {
            //alert("test")
            var who = document.getElementById("lblwho").value;
            if (who != "fs") {
                var root = document.getElementById("lblrootid").value;
                var note = document.getElementById("lblnoteid").value;
                var ret = root + "," + note;
                window.returnValue = ret;
                window.close();
            }
        }
        function handleroot(id) {
        //alert(id)
            document.getElementById("lblrootid").value = id;
        }
        function handlenote(id) {
            document.getElementById("lblnoteid").value = id;
        }
        function getret() {

        }
    //-->
    </script>
</head>
<body onunload="handlereturn();">
    <form id="form1" runat="server">
    <table id="tbl1" runat="server" border="0">
    <tr id="tr1" runat="server">
    <td height="22" class="label" width="120px" id="tdwon" runat="server">Work Order#</td>
    <td class="plainlabel" id="tdwo" runat="server" width="500px"></td>
    <td width="40" class="plainlabel"><a href="#" onclick="handlereturn();" id="tdret" runat="server">Return</a></td>
    </tr>
    <tr id="tr2" runat="server">
    <td height="22" class="label" width="120px" id="tdwtl" runat="server">Work Type:</td>
    <td class="plainlabel" id="tdwt" runat="server" colspan="2"></td>
    </tr>
    <tr id="tr3" runat="server">
    <td height="22" class="label" width="120px" id = "tddescl" runat="server">Description</td>
    <td rowspan="2" class="plainlabel" id="tddesc" runat="server" colspan="2"></td>
    </tr>
    <tr id="tr4" runat="server">
    <td height="22">&nbsp;</td>
    </tr>
    <tr>
    <td colspan="3">
    <table width="640px" id="tbl2" runat="server">
    <tr>
    <td class="thdrhovmini plainlabel" id="tdwn" onclick="gettab('wn');" width="160"
                            runat="server" height="22px">
                            <asp:Label ID="lang1548a" runat="server">Work Order Notes</asp:Label>
                        </td>
                        <td class="thdrmini plainlabel" id="tdrc" onclick="gettab('rc');" width="130" runat="server">
                            <asp:Label ID="lang1549" runat="server">Root Cause</asp:Label>
                        </td>
                        <td width="350px" id="tdmsg" runat="server" class="plainlabelblue"></td>
    </tr>
    
    </table>
    </td>
    </tr>
    <tr>
    <td colspan="3">
    <div id="wodiv" runat="server" >
        <iframe id="ifwo" 
            name="ifwo" src="../genhold.htm" frameborder="0" runat="server"></iframe>
    </div>
    </td>
    </tr>
    </table>
    <input type="hidden" id="lblreq" runat="server" />
    <input type="hidden" id="lbltab" runat="server" />
    <input type="hidden" id="lblrootid" runat="server" />
    <input type="hidden" id="lblnoteid" runat="server" />
    <input type="hidden" id="lblwo" runat="server" />
    <input type="hidden" id="lblwt" runat="server" />
    <input type="hidden" id="lblcuser" runat="server" />
    <input type="hidden" id="lblcuserid" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
