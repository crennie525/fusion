

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class woprint_pmpics
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim won As New Utilities
    Dim wonum, pmnum, wotype, jpnum, psite, routeid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdwi As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblwotype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpsite As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            wonum = Request.QueryString("wo").ToString
            pmnum = Request.QueryString("pm").ToString
            won.Open()
            PopPMReport(wonum, pmnum)
            won.Dispose()

        End If
    End Sub
    Private Sub PopPMReport(ByVal wo As String, ByVal pm As String)
        Dim flag As Integer = 0
        Dim subcnt As Integer = 0
        Dim subcnth As Integer = 0
        sql = "select w.siteid, s.sitename from workorder w left join sites s on s.siteid = w.siteid where w.wonum = '" & wo & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            psite = dr.Item("sitename").ToString
        End While
        dr.Close()
        Dim func, funcchk, task, tasknum, subtask, pics, picshold As String
        Dim sb As New System.Text.StringBuilder

        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
        sb.Append("<tr><td width=""120""></td><td width=""160""></td><td width=""130""></td><td width=""170""></td><td width=""90""></td></tr>")



        sql = "usp_getwoprint '" & wo & "'"

        dr = won.GetRdrData(sql)
        Dim headhold As String = "0"
        While dr.Read
            Dim pms As String = dr.Item("pm").ToString
            sb.Append("<tr height=""30"" valign=""top""><td class=""bigbold1"" colspan=""5"" align=""center"">" & psite & " Preventive Maintenance Work Request</td></tr>")
            sb.Append("<tr height=""20"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center"">" & Now & "</td></tr>")
            sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""5"" align=""center"">" & pms & "</td></tr>")
            sb.Append("<tr height=""20"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center""><b>Work Order#</b>&nbsp;&nbsp;" & wo & "</td></tr>")
            'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
            sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl318" , "woprint_pmpics.aspx.vb") & "</td></tr>")
            Dim start As String = dr.Item("schedstart").ToString
            If start = "" Then
                start = dr.Item("targstartdate").ToString
                If start = "" Then
                    start = Now
                End If
            End If
            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl319" , "woprint_pmpics.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & start & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl320" , "woprint_pmpics.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("actfinish").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl321" , "woprint_pmpics.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("reportedby").ToString & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl322" , "woprint_pmpics.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("reportdate").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl323" , "woprint_pmpics.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("status").ToString & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl324" , "woprint_pmpics.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("chargenum").ToString & "</td></tr>")

            'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")

            sb.Append("<tr height=""30""><td class=""label"">" & tmod.getlbl("cdlbl325" , "woprint_pmpics.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("supervisor").ToString & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl326" , "woprint_pmpics.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("leadcraft").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl327" , "woprint_pmpics.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""4"">" & dr.Item("description").ToString & "</td></tr>")

            sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
            sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl328" , "woprint_pmpics.aspx.vb") & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl329" , "woprint_pmpics.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("dept_line").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("dept_desc").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl330" , "woprint_pmpics.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("cell_name").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("cell_desc").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl331" , "woprint_pmpics.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("ldesc").ToString & "</td></tr>")

            If dr.Item("eqnum").ToString <> "" Then
                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl332" , "woprint_pmpics.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("eqnum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("eqdesc").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        'sb.Append("<tr><td>&nbsp;</td></tr>")
        Dim pflag As Integer = 0
        Dim isubcnt As Integer = 0
        sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
        sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""640""></td></tr>")

        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/pmimages/"
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        Dim ThisPage1 As String = Request.ServerVariables("HTTP_HOST")
        Dim ns1i As Integer = nsimage.IndexOf("//")
        Dim nsstr As String = Mid(nsimage, ns1i + 3)
        Dim ns2i As Integer = nsstr.IndexOf("/")
        nsstr = Mid(nsstr, 1, ns2i)
        Dim hostflag As Integer = 0
        If nsstr <> ThisPage1 Then
            nsimage = nsimage.Replace(nsstr, ThisPage1)
            hostflag = 1
        End If
        sql = "usp_getpmmanpics '" & pm & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            If headhold = "0" Then
                headhold = "1"
                pm = dr.Item("pm").ToString
                sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl333" , "woprint_pmpics.aspx.vb") & "</td></tr>")

            End If

            func = dr.Item("func").ToString
            If funcchk <> func Then 'flag = 0 Then
                If flag = 0 Then
                    flag = 1
                Else
                    'sb.Append("<tr><td>&nbsp;</td></tr>")
                End If

                funcchk = func
                sb.Append("<tr height=""20""><td class=""label"" colspan=""3""><u>Function: " & func & "</u></td>")
                'sb.Append("<td class=""plainlabel"" colspan=""2"">" & func & "</td></tr>")
                'sb.Append("<tr><td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl334" , "woprint_pmpics.aspx.vb") & "</td>")
                'sb.Append("<td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl335" , "woprint_pmpics.aspx.vb") & "</td>")
                'sb.Append("<td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl336" , "woprint_pmpics.aspx.vb") & "</td></tr>")
            End If
            task = dr.Item("task").ToString
            tasknum = dr.Item("tasknum").ToString
            subtask = dr.Item("subtask").ToString
            subcnt = dr.Item("subcnt").ToString

            Dim biw, bih As Integer
            biw = 320
            bih = 320

            If subtask = "0" Then
                'sb.Append("<tr height=""20""><td></td><td class=""label"">" & subcnt & "</td></tr>")
                sb.Append("<tr><td class=""plainlabel"">[" & tasknum & "]</td>")
                If Len(dr.Item("task").ToString) > 0 Then
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Task</b> :" & task & "</td></tr>")
                Else
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Task:</b> No Task Description Provided</td></tr>")
                End If
                'sb.Append("<tr><td>&nbsp;</td></tr>")
                sb.Append("<tr height=""20""><td></td>")
                sb.Append("<td colspan=""2"" class=""plainlabel"">Check: OK(___) " & dr.Item("fm1").ToString & "</td></tr>")
                If dr.Item("lube") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Lubes: " & dr.Item("lube").ToString & "</td></tr>")
                End If
                If dr.Item("tool") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Tools: " & dr.Item("tool").ToString & "</td></tr>")
                End If
                If dr.Item("part") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Parts: " & dr.Item("part").ToString & "</td></tr>")
                End If
                If dr.Item("meas") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Measurements: " & dr.Item("meas").ToString & "</td></tr>")
                End If
                pics = dr.Item("pics").ToString
                If subcnt = 0 Then
                    'add pics here
                    Dim pt, ptr, ptd As Integer
                    ptr = 0
                    ptd = 0
                    If pics <> "" Then
                        Dim picstr() As String = pics.Split(",")
                        sb.Append("<tr><td colspan=""3"" align=""center""><table>")
                        For pt = 0 To picstr.Length - 1
                            Dim ptst As String = picstr(pt)
                            If hostflag = 1 Then
                                ptst = ptst.Replace(nsstr, ThisPage1)
                            End If
                            
                            If ptr = 0 And ptd = 0 Then
                                ptr = 1
                                ptd = 1
                                sb.Append("<tr><td width=""500"" align=""center""><img src=""" + picstr(pt) + """ class=""big""></td></tr>")
                                'sb.Append("<td width=""10""></td>")
                            ElseIf ptr = 1 And ptd = 1 Then
                                ptr = 0
                                ptd = 0
                                sb.Append("<tr><td width=""500"" align=""center""><img src=""" + ptst + """ class=""big""></td></tr>")
                            End If

                        Next
                        sb.Append("</table></td></tr>")
                    End If
                    sb.Append("<tr><td>&nbsp;</td></tr>")
                Else
                    If subcnt <> 0 Then
                        If pics <> "" Then
                            picshold = pics
                            pflag = 1
                        End If
                        sb.Append("<tr height=""20""><td></td><td class=""label"" colspan=""2""><u>Sub Tasks</u></td></tr>")
                    End If
                End If

            Else
                isubcnt += 1

                'sb.Append("<tr><td>" & subcnt & "</td></tr>")
                sb.Append("<tr height=""20""><td></td><td class=""plainlabel"">[" & subtask & "]</td>")
                If Len(dr.Item("subt").ToString) > 0 Then
                    sb.Append("<td class=""plainlabel""><b>Task</b>: " & dr.Item("subt").ToString & "</td></tr>")
                Else
                    sb.Append("<td class=""plainlabel""><b>Task</b>: No Sub Task Description Provided</td></tr>")
                End If
                sb.Append("<tr><td></td><td></td>")
                sb.Append("<td class=""plainlabel"">Check: OK(___)</td></tr>")
                sb.Append("<tr><td>&nbsp;</td></tr>")
                If isubcnt = subcnt Then
                    isubcnt = 0
                    If pflag = 1 Then
                        'add pics here
                        pflag = 0
                        pics = picshold 'dr.Item("pics").ToString
                        Dim pt, ptr, ptd As Integer
                        ptr = 0
                        ptd = 0
                        If pics <> "" Then
                            Dim picstr() As String = pics.Split(",")
                            sb.Append("<tr><td colspan=""3"" align=""center""><table border=""0"">")
                            For pt = 0 To picstr.Length - 1
                                Dim ptst As String = picstr(pt)
                                If ptr = 0 And ptd = 0 Then
                                    ptr = 1
                                    ptd = 1
                                    sb.Append("<tr><td width=""500"" align=""center""><img src=""" + picstr(pt) + """ class=""big""></td></tr>")
                                    'sb.Append("<td width=""10""></td>")
                                ElseIf ptr = 1 And ptd = 1 Then
                                    ptr = 0
                                    ptd = 0
                                    sb.Append("<tr><td width=""500"" align=""center""><img src=""" + picstr(pt) + """ class=""big""></td></tr>")
                                End If

                            Next
                            sb.Append("</table></td></tr>")
                        End If
                    End If
                    
                End If
            End If
        End While
        dr.Close()
        sb.Append("</table></td></tr>")
        sb.Append("</table>")
        tdwi.InnerHtml = sb.ToString
    End Sub
End Class
