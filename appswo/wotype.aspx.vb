﻿Imports System.Data.SqlClient
Public Class wotype
    Inherits System.Web.UI.Page
    Dim wt As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim notstring As String = ""
    Dim retstring As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            popwt()
        End If
    End Sub
    Private Sub popwt()
        Dim pwt As New Utilities
        pwt.Open()
        If notstring = "" Then
            sql = "select wotype, wotype + ' - ' + description as 'description' from wotype where active = 1"
        Else
            sql = "select wotype, wotype + ' - ' + description as 'description' from wotype where active = 1 and wotype not in (" & notstring & ")"
        End If

        dr = pwt.GetRdrData(sql)
        lbwt.DataSource = dr
        lbwt.DataValueField = "wotype"
        lbwt.DataTextField = "description"


        lbwt.DataBind()
        dr.Close()
        pwt.Dispose()
    End Sub

    Protected Sub btntocomp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        notstring = lblnotstring.Value
        retstring = lblretstring.Value
        Dim Item As ListItem
        Dim f, fi As String
        For Each Item In lbwt.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                GetItems(fi, f)
            End If
        Next
        popwt()
        lblnotstring.Value = notstring
        lblretstring.Value = retstring
    End Sub
    Private Sub GetItems(ByVal fi As String, ByVal f As String)
        Dim item As ListItem = New ListItem(f, fi)
        If Not lbwts.Items.Contains(item) Then
            lbwts.Items.Add(item)
            If notstring = "" Then
                notstring = "'" + fi + "'"
                retstring = fi
            Else
                notstring += ",'" + fi + "'"
                retstring += "," + fi
            End If
        End If
    End Sub

    Protected Sub btnfromcomp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        notstring = ""
        retstring = ""
        Dim remstring As String = ""
        Dim Item As ListItem
        Dim f, fi As String
        For Each Item In lbwts.Items
            If Item.Selected Then
                'do nothing
            Else
                f = Item.Text.ToString
                fi = Item.Value.ToString
                'If Not lbwts.Items.Contains(Item) Then

                If notstring = "" Then
                    notstring = "'" + fi + "'"
                    retstring = fi
                Else
                    notstring += ",'" + fi + "'"
                    retstring += "," + fi
                End If
                'End If
            End If
        Next
        If notstring <> "" Then
            RemItems(notstring)
        End If
        popwt()
        lblnotstring.Value = notstring
        lblretstring.Value = retstring
    End Sub
    Private Sub RemItems(ByVal notstring As String)
        Dim pwts As New Utilities
        pwts.Open()
        sql = "select wotype, wotype + ' - ' + description as 'description' from wotype where active = 1 and wotype in (" & notstring & ")"
        dr = pwts.GetRdrData(sql)
        lbwts.DataSource = dr
        lbwts.DataValueField = "wotype"
        lbwts.DataTextField = "description"
        lbwts.DataBind()
        dr.Close()
        pwts.Dispose()
    End Sub
End Class