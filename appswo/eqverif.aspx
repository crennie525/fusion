﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="eqverif.aspx.vb" Inherits="lucy_r12.eqverif" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javescript" type="text/javascript">
        function checkwho() {
            var ret;
            var who = document.getElementById("lblwho").value;
            var eqnum = document.getElementById("lbleqnum").value;
            //alert(who)
            if (who == "M" || who == "N") {
                ret = who + "~" + eqnum;
                window.parent.handle_eqauto(ret);
            }
            else {
                var eqid = document.getElementById("lbleqid").value;
                var eqdesc = document.getElementById("lbleqdesc").value;
                var fpc = document.getElementById("lblfpc").value;
                var loc = document.getElementById("lbllocation").value;
                var parid = document.getElementById("lblparid").value;
                var dept = document.getElementById("lbldept").value;
                var did = document.getElementById("lbldid").value;
                var cell = document.getElementById("lblcell").value;
                var clid = document.getElementById("lblclid").value;
                var wo = document.getElementById("lblwonum").value;
                ret = who + "~" + eqnum + "~" + eqid + "~" + eqdesc + "~" + fpc + "~" + loc + "~" + parid + "~" + dept + "~" + did + "~" + cell + "~" + clid + "~" + wo;
                //alert(ret)
                window.parent.handle_eqauto(ret);
            }

        }
    </script>
</head>
<body onload="checkwho();">
    <form id="form1" runat="server">
    <div>
    
    </div>
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblwonum" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lbleqdesc" runat="server" />
    <input type="hidden" id="lblfpc" runat="server" />
    <input type="hidden" id="lbllocation" runat="server" />
    <input type="hidden" id="lblparid" runat="server" />
    <input type="hidden" id="lbldept" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lblcell" runat="server" />
    <input type="hidden" id="lblclid" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblwpage" runat="server" />
    </form>
</body>
</html>
