<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wjsub.aspx.vb" Inherits="lucy_r12.wjsub"
    EnableEventValidation="false" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wjsub</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/wjsubaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
   <script language="javascript" type="text/javascript">
       function GetToolDiv() {
           cid = document.getElementById("lblcid").value;
           ptid = document.getElementById("lblpmtid").value;
           jp = document.getElementById("lbljpid").value;
           tnum = document.getElementById("lbltasknum").value;
           wo = document.getElementById("lblwo").value;
           ro = document.getElementById("lblro").value;
           //if(wo=="") wo="0";
           if (ptid != "") {
               window.open("../inv/devTaskToolList.aspx?typ=wjp&ptid=" + ptid + "&cid=" + cid + "&jp=" + jp + "&tnum=" + tnum + "&wo=" + wo + "&ro=" + ro, "repWin", popwin);
           }
       }
       function GetPartDiv() {
           cid = document.getElementById("lblcid").value;
           ptid = document.getElementById("lblpmtid").value;
           jp = document.getElementById("lbljpid").value;
           tnum = document.getElementById("lbltasknum").value;
           wo = document.getElementById("lblwo").value;
           ro = document.getElementById("lblro").value;
           //if(wo=="") wo="0";
           if (ptid != "") {
               window.open("../inv/devTaskPartList.aspx?typ=wjp&ptid=" + ptid + "&cid=" + cid + "&wjp=" + jp + "&tnum=" + tnum + "&wo=" + wo + "&ro=" + ro, "repWin", popwin);
           }
       }
       function GetLubeDiv() {
           cid = document.getElementById("lblcid").value;
           ptid = document.getElementById("lblpmtid").value;
           jp = document.getElementById("lbljpid").value;
           tnum = document.getElementById("lbltasknum").value;
           wo = document.getElementById("lblwo").value;
           ro = document.getElementById("lblro").value;
           //if(wo=="") wo="0";
           if (ptid != "") {
               window.open("../inv/devTaskLubeList.aspx?typ=wjp&ptid=" + ptid + "&cid=" + cid + "&jp=" + jp + "&tnum=" + tnum + "&wo=" + wo + "&ro=" + ro, "repWin", popwin);
           }
       }
   </script>
</head>
<body onload="checkinv();">
    <form id="form1" method="post" runat="server">
    <table cellspacing="1">
        <tr>
            <td class="thdrsing label" colspan="7">
                <asp:Label ID="lang1315" runat="server">Sub Task View - Details</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <asp:DataList ID="dlhd" runat="server">
                    <HeaderTemplate>
                        <table width="700">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr height="20">
                            <td class="label">
                                <asp:Label ID="lang1316" runat="server">Department</asp:Label>
                            </td>
                            <td>
                                <asp:Label CssClass="plainlabel" ID="lbldept" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dept_line") %>'>
                                </asp:Label>
                            </td>
                            <td class="label">
                                <asp:Label ID="lang1317" runat="server">Station/Cell</asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:Label CssClass="plainlabel" ID="lblcell" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cell_name") %>'>
                                </asp:Label>
                            </td>
                        </tr>
                        <tr height="20">
                            <td class="label">
                                <asp:Label ID="lang1318" runat="server">Equipment</asp:Label>
                            </td>
                            <td>
                                <asp:Label CssClass="plainlabel" ID="lbleq" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
                                </asp:Label>
                            </td>
                            <td class="label">
                                <asp:Label ID="lang1319" runat="server">Function</asp:Label>
                            </td>
                            <td>
                                <asp:Label CssClass="plainlabel" ID="lblfunc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
                                </asp:Label>
                            </td>
                        </tr>
                        <tr height="20">
                            <td class="label">
                                <asp:Label ID="lang1320" runat="server">Component</asp:Label>
                            </td>
                            <td>
                                <asp:Label CssClass="plainlabel" ID="lblcompon" runat="server" ForeColor="Black"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td width="100">
                            </td>
                            <td width="200">
                            </td>
                            <td width="100">
                            </td>
                            <td width="200">
                            </td>
                            <td width="100">
                            </td>
                            <td width="200">
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
    </table>
    <table cellspacing="1">
        <tr>
            <td class="thdrsing label" colspan="7">
                <asp:Label ID="lang1321" runat="server">Current Task</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="btmmenu plainlabel" width="60">
                <asp:Label ID="lang1322" runat="server">Task#</asp:Label>
            </td>
            <td class="btmmenu plainlabel" width="420">
                <asp:Label ID="lang1323" runat="server">Top Level Task Description</asp:Label>
            </td>
            <td class="btmmenu plainlabel" width="100">
                <asp:Label ID="lang1324" runat="server">Skill Required</asp:Label>
            </td>
            <td class="btmmenu plainlabel" width="60">
                Qty
            </td>
            <td class="btmmenu plainlabel" width="60">
                <asp:Label ID="lang1325" runat="server">Min Ea</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="plainlabel" id="tdtnum" runat="server">
            </td>
            <td class="plainlabel" id="tddesc" runat="server">
            </td>
            <td class="plainlabel" id="tdskill" runat="server">
            </td>
            <td class="plainlabel" id="tdqty" runat="server">
            </td>
            <td class="plainlabel" id="tdmin" runat="server">
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="thdrsing label" colspan="7">
                <asp:Label ID="lang1326" runat="server">Sub Tasks</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:ImageButton ID="addtask" runat="server" ImageUrl="../images/appbuttons/bgbuttons/addtask.gif">
                </asp:ImageButton>&nbsp;<img id="btnreturn" onclick="handlereturn();" height="19"
                    src="../images/appbuttons/bgbuttons/return.gif" width="69" runat="server">
            </td>
        </tr>
        <tr>
            <td>
                <asp:DataGrid ID="dgtasks" runat="server" CellSpacing="1" AutoGenerateColumns="False"
                    AllowSorting="True" GridLines="None">
                    <AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="Task#">
                            <HeaderStyle CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblta" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="lblt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'
                                    Width="40px">
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Sub Task#">
                            <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblsubt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="lblst" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>'
                                    Width="40px">
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Sub Task Description">
                            <HeaderStyle Width="280px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>'
                                    Width="270px">
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtdesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>'
                                    Height="70px" TextMode="MultiLine" Width="260px" MaxLength="1000">
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Skill Qty">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'
                                    ID="Label10" NAME="Label8">
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtqty" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Skill Min Ea">
                            <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'
                                    ID="Label12">
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txttr" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="Down Time">
                            <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rdt") %>'
                                    ID="Label2">
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtdt" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.rdt") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Parts/Tools/Lubes">
                            <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton23" runat="server" ImageUrl="../images/appbuttons/minibuttons/parttrans.gif"
                                    ToolTip="Add Parts" CommandName="Part"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton23a" runat="server" ImageUrl="../images/appbuttons/minibuttons/tooltrans.gif"
                                    ToolTip="Add Tools" CommandName="Tool"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton23b" runat="server" ImageUrl="../images/appbuttons/minibuttons/lubetrans.gif"
                                    ToolTip="Add Lubricants" CommandName="Lube"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="pmtskid">
                            <ItemTemplate>
                                <asp:Label ID="lbltida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wojtid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblttid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wojtid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Delete">
                            <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="ibDel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <input id="lbljpid" type="hidden" name="lbljpid" runat="server">
    <input id="lbltid" type="hidden" name="lbltid" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblfilt" type="hidden" name="lblfilt" runat="server">
    <input id="lblsubval" type="hidden" name="lblsubval" runat="server"><input id="lblsid"
        type="hidden" name="lblsid" runat="server">
    <input id="lbloldtask" type="hidden" name="lbloldtask" runat="server"><input id="lblpart"
        type="hidden" name="lblpart" runat="server">
    <input id="lbltool" type="hidden" name="lbltool" runat="server"><input id="lbllube"
        type="hidden" name="lbllube" runat="server">
    <input id="lblnote" type="hidden" name="lblnote" runat="server"><input id="lbltasknum"
        type="hidden" name="lbltasknum" runat="server">
    <input id="lblpmtid" type="hidden" name="lblpmtid" runat="server"><input id="lbllog"
        type="hidden" name="lbllog" runat="server">
    <input id="lbllock" type="hidden" name="lbllock" runat="server">
    <input id="lbllockedby" type="hidden" name="lbllockedby" runat="server">
    <input id="lblusername" type="hidden" name="lblusername" runat="server"><input id="lbleqid"
        type="hidden" name="lbleqid" runat="server">
    <input id="lblwo" type="hidden" runat="server" name="lblwo">
    <input type="hidden" id="lblcid" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
    <input type="hidden" id="lblro" runat="server">
</body>
</html>
