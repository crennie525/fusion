﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wonotesfs.aspx.vb" Inherits="lucy_r12.wonotesfs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script type="text/javascript" language="javascript">
    <!--
        function checkvals() {

            var co = document.getElementById("txtcorraction").value;
            if (co == "") {
                alert("Work Order Notes Required")
            }
            else {
                document.getElementById("lblsubmit").value = "go";
                document.getElementById("form1").submit();
            }
        }
        function checkdat() {
            //var id = document.getElementById("lblrootid").value;
            //alert(id)
            //window.parent.handleroot(id);
            var nid = document.getElementById("lblnoteid").value;
            //alert(id)
            //window.parent.handlenote(nid);
        }
    //-->
    </script>
</head>
<body onload="checkdat();">
    <form id="form1" runat="server">
    <table width="540px">
    
    <tr>
    <td colspan="2" class="label" height="22px">Work Order Notes</td>
   
   
    </tr>
    <tr>

    <td colspan="2" class="plainlabel"><asp:TextBox ID="txtcorraction" runat="server" Width="530" Height="60" TextMode="MultiLine"></asp:TextBox></td>
    
    
    </tr>

   
    <tr>
    <td colspan="2">
    <table>
    <tr>
     <td class="label" width="100px" height="22px">Entered By:</td>
   <td class="plainlabel" width="170px" id="tdenterby" runat="server"></td>
   <td class="label" width="100px">Enter Date:</td>
   <td class="plainlabel" id="tdenterdate" runat="server" width="170px"></td>
    </tr>
    <tr>
    <td class="label" height="22">Modified By:</td>
   <td class="plainlabel" id="tdmodby" runat="server"></td>
   <td class="label" height="22">Modified Date:</td>
   <td class="plainlabel" id="tdmoddate" runat="server"></td>
    </tr>
    </table>
    </td>
    </tr>
    
    
    <tr>
    <td colspan="2" align="right"><img id="imgsav" onclick="checkvals();" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                runat="server" alt="" /></td>
    </tr>
    
    </table>
    <input type="hidden" id="lblrootid" runat="server" />
    <input type="hidden" id="lblorootid" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblwo" runat="server" />
    <input type="hidden" id="lblcuser" runat="server" />
    <input type="hidden" id="lblcuserid" runat="server" />
    <input type="hidden" id="lbluser" runat="server" />
    <input type="hidden" id="lbluserid" runat="server" />
    <input type="hidden" id="lblenterdate" runat="server" />
    <input type="hidden" id="lblmodbyid" runat="server" />
    <input type="hidden" id="lblmodby" runat="server" />
    <input type="hidden" id="lblmoddate" runat="server" />
    <input type="hidden" id="lblnoteid" runat="server" />
    </form>
</body>
</html>

