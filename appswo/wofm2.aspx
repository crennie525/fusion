<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wofm2.aspx.vb" Inherits="lucy_r12.wofm2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wofm2</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/wofm2aspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="scrolltop();checkit();" class="tbg">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" cellspacing="1" cellpadding="2" width="700">
        <tbody>
            <tr height="22">
                <td class="thdrsing label">
                    <asp:Label ID="lang1395" runat="server">Work Order Level Failure Modes</asp:Label>
                </td>
            </tr>
            <tr>
                <td class="plainlabelblue" align="center">
                    <asp:Label ID="lang1396" runat="server">To aid in future Optimization efforts Please Do Not Change a Failure Mode Status to "Pass" Unless the Indicated Failure Condition was Not Evident when Performing this Work Order</asp:Label>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                    <asp:DataGrid ID="dltasks" runat="server" AutoGenerateColumns="False" BackColor="transparent">
                        <FooterStyle BackColor="transparent"></FooterStyle>
                        <EditItemStyle Height="15px"></EditItemStyle>
                        <AlternatingItemStyle CssClass="ptransblue"></AlternatingItemStyle>
                        <ItemStyle CssClass="ptransrow"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgeditfm" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Failure Mode">
                                <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label6" CssClass="plainlabel" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.failuremode") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="Label7" CssClass="plainlabel" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.failuremode") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Pass\Fail">
                                <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label8" CssClass="plainlabel" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.pass") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddfm1" CssClass="plainlabel" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("pass")) %>'>
                                        <asp:ListItem Value="Y">Pass</asp:ListItem>
                                        <asp:ListItem Value="N">Fail</asp:ListItem>
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Failure Mode" Visible="False">
                                <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" CssClass="plainlabel" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.wofailid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblwofailid" CssClass="plainlabel" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.wofailid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </tbody>
    </table>
    <input id="lbljpid" type="hidden" name="lbljpid" runat="server">
    <input id="lblwo" type="hidden" name="lblwo" runat="server">
    <input id="lbltaskcnt" type="hidden" name="lbltaskcnt" runat="server">
    <input id="lbltaskcurrcnt" type="hidden" name="lbltaskcurrcnt" runat="server">
    <input id="lblrow" type="hidden" name="lblrow" runat="server">
    <input id="lblfmcnt" type="hidden" name="lblfmcnt" runat="server">
    <input id="lbltasknum" type="hidden" name="lbltasknum" runat="server">
    <input id="lblcurrcnt" type="hidden" name="lblcurrcnt" runat="server">
    <input id="xCoord" type="hidden" name="xCoord" runat="server">
    <input id="yCoord" type="hidden" name="yCoord" runat="server"><input id="lblsubmit"
        type="hidden" name="lblsubmit" runat="server">
    <input id="lblfmstr" type="hidden" name="lblfmstr" runat="server">
    <input id="lblmstr" type="hidden" name="lblmstr" runat="server">
    <input id="lblmstrvals" type="hidden" name="lblmstrvals" runat="server">
    <input id="lblpstr" type="hidden" name="lblpstr" runat="server">
    <input id="lbltstr" type="hidden" name="lbltstr" runat="server">
    <input id="lbllstr" type="hidden" name="lbllstr" runat="server">
    <input id="lblmflg" type="hidden" name="lblmflg" runat="server"><input id="lblmcomp"
        type="hidden" name="lblmcomp" runat="server">
    <input type="hidden" id="lblstat" runat="server" name="lblstat"><input type="hidden"
        id="lblro" runat="server">
    <input type="hidden" id="lbllog" runat="server" name="lbllog">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblcadm" runat="server" />
    </form>
</body>
</html>
