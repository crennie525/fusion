

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wofmme2
    Inherits System.Web.UI.Page
	Protected WithEvents lang1402 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim comp As New Utilities
    Dim wonum, stat, ro, rostr, Login, sid As String
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim ds As DataSet
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgmeas As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcurrcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrow As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfmcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmstrvals As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmflg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here

        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                dgmeas.Columns(0).Visible = False
            Else
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = comp.CheckROS(rostr, "wo")
                    lblro.Value = ro
                    If ro = "1" Then
                        dgmeas.Columns(0).Visible = False
                    End If
                End If
            End If
            wonum = Request.QueryString("wo").ToString
            lblwo.Value = wonum
            stat = Request.QueryString("stat").ToString
            lblstat.Value = stat
            If (stat <> "COMP" And stat <> "CAN" And stat <> "WAPPR") Then
                dgmeas.Columns(0).Visible = True
            Else
                dgmeas.Columns(0).Visible = False
            End If
            comp.Open()
            GetMeas(wonum)
            comp.Dispose()
        End If
    End Sub
    Private Sub GetMeas(ByVal wonum As String)
        sql = "select * from womeasdetails where wonum = '" & wonum & "'" 'distinct womid, measurement, womeas
        dr = comp.GetRdrData(sql)
        dgmeas.DataSource = dr
        dgmeas.DataBind()
        ro = lblro.Value
        If ro = "1" Then
            dgmeas.Columns(0).Visible = False
        End If
    End Sub

    Private Sub dgmeas_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.EditCommand
        dgmeas.EditItemIndex = e.Item.ItemIndex
        wonum = lblwo.Value
        comp.Open()
        GetMeas(wonum)
        comp.Dispose()
    End Sub

    Private Sub dgmeas_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.CancelCommand
        dgmeas.EditItemIndex = -1
        wonum = lblwo.Value
        comp.Open()
        GetMeas(wonum)
        comp.Dispose()
    End Sub

    Private Sub dgmeas_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.UpdateCommand
        Dim mup As Integer
        Dim hi, lo As String
        Dim mstr As String = CType(e.Item.FindControl("txtmeas"), TextBox).Text
        Dim tmdid As String = CType(e.Item.FindControl("lbltmdida"), Label).Text
        'Dim pmtskid As String = CType(e.Item.FindControl("lblpmtskida"), Label).Text
        Dim txtchk As Long
        Try
            txtchk = System.Convert.ToDecimal(mstr)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr568" , "wofmme2.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If mstr <> "" Then
            sql = "usp_upwomeas '" & tmdid & "', '" & mstr & "'"
            comp.Open()
            dgmeas.EditItemIndex = -1
            comp.Update(sql)
            wonum = lblwo.Value
            GetMeas(wonum)
            comp.Dispose()
        End If
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgmeas.Columns(0).HeaderText = dlabs.GetDGPage("wofmme2.aspx", "dgmeas", "0")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(1).HeaderText = dlabs.GetDGPage("wofmme2.aspx", "dgmeas", "1")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(2).HeaderText = dlabs.GetDGPage("wofmme2.aspx", "dgmeas", "2")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(3).HeaderText = dlabs.GetDGPage("wofmme2.aspx", "dgmeas", "3")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(4).HeaderText = dlabs.GetDGPage("wofmme2.aspx", "dgmeas", "4")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(5).HeaderText = dlabs.GetDGPage("wofmme2.aspx", "dgmeas", "5")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1402.Text = axlabs.GetASPXPage("wofmme2.aspx", "lang1402")
        Catch ex As Exception
        End Try

    End Sub

End Class
