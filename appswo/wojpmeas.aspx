<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wojpmeas.aspx.vb" Inherits="lucy_r12.wojpmeas" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wojpmeas</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/wojpmeasaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="domup();">
    <form id="form1" method="post" runat="server">
    <table width="560">
        <tr>
            <td>
                <asp:DataGrid ID="dgmeas" runat="server" Width="540px" ShowFooter="True" CellPadding="0"
                    GridLines="None" AllowPaging="True" AllowCustomPaging="True" AutoGenerateColumns="False"
                    CellSpacing="1">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <EditItemStyle Height="15px"></EditItemStyle>
                    <AlternatingItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="#E7F1FD">
                    </AlternatingItemStyle>
                    <ItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="ControlLightLight">
                    </ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Type">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label3" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Hi">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label6" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Lo">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label8" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Spec">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label10" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Measurement">
                            <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Width="90px" Text='<%# DataBinder.Eval(Container, "DataItem.womeas") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtmeas" runat="server" Width="90px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.womeas") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <HeaderStyle></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblpmtskid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblpmtskida" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <HeaderStyle></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lbltmdid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.tmdid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lbltmdida" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.tmdid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
                        ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <input id="lbltid" type="hidden" runat="server" name="lbltid">
    <input type="hidden" id="lblmcnt" runat="server" name="lblmcnt">
    <input type="hidden" id="lblmup" runat="server" name="lblmup">
    <input type="hidden" id="lblpmid" runat="server" name="lblpmid">
    <input type="hidden" id="lblpmtid" runat="server" name="lblpmtid">
    <input type="hidden" id="lbltmid" runat="server" name="lbltmid"><input type="hidden"
        id="lblcid" runat="server" name="lblcid">
    <input type="hidden" id="lblsup" runat="server" name="lblsup">
    <input type="hidden" id="lbllead" runat="server" name="lbllead">
    <input type="hidden" id="lblwo" runat="server" name="lblwo"><input type="hidden"
        id="lblcomid" runat="server" name="lblcomid">
    <input type="hidden" id="lbljpid" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
