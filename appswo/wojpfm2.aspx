<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wojpfm2.aspx.vb" Inherits="lucy_r12.wojpfm2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wojpfm2</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/wojpfm2aspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="scrolltop();checkit();" class="tbg">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" cellspacing="0" cellpadding="2" width="700">
        <tbody>
            <tr>
                <td>
                    <table width="700">
                        <tr height="26">
                            <td class="label" width="80">
                                <asp:Label ID="lang1512" runat="server">Job Plan:</asp:Label>
                            </td>
                            <td class="plainlabel" id="tdjpn" width="120" runat="server">
                            </td>
                            <td class="plainlabel" id="tdjpd" width="410" colspan="6" runat="server">
                            </td>
                            <td class="label" align="right" width="250">
                            </td>
                        </tr>
                        <tr>
                            <td class="plainlabelred" id="tdnofail" colspan="9" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="22">
                <td class="thdrsing label">
                    <asp:Label ID="lang1513" runat="server">Job Plan Level Failure Modes</asp:Label>
                </td>
            </tr>
            <tbody>
                <tr>
                    <td align="center">
                        <asp:DataGrid ID="dltasks" runat="server" AutoGenerateColumns="False" BackColor="transparent">
                            <FooterStyle BackColor="transparent"></FooterStyle>
                            <EditItemStyle Height="15px"></EditItemStyle>
                            <AlternatingItemStyle CssClass="prowtransblue"></AlternatingItemStyle>
                            <ItemStyle CssClass="prowtrans"></ItemStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="Edit">
                                    <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgeditfm" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                            CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:ImageButton ID="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                            CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                        <asp:ImageButton ID="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                            CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Task#">
                                    <HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label4" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="Label5" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                        </asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif">
                                    <HeaderStyle Width="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <ItemTemplate>
                                        <img src="../images/appbuttons/minibuttons/magnifier.gif" id="imgti" runat="server">
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <img src="../images/appbuttons/minibuttons/magnifier.gif" id="imgte" runat="server">
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="FM1">
                                    <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label6" CssClass="plainlabel" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.fm1s") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="Label7" CssClass="plainlabel" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.fm1s") %>'>
                                        </asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Pass\Fail">
                                    <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label8" CssClass="plainlabel" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.fm1dd") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddfm1" CssClass="plainlabel" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm1dd")) %>'>
                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="FM2">
                                    <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label9" CssClass="plainlabel" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.fm2s") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="Label10" CssClass="plainlabel" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.fm2s") %>'>
                                        </asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Pass\Fail">
                                    <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label11" CssClass="plainlabel" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.fm2dd") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddfm2" CssClass="plainlabel" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm2dd")) %>'>
                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="FM3">
                                    <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label12" CssClass="plainlabel" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.fm3s") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="Label13" CssClass="plainlabel" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.fm3s") %>'>
                                        </asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Pass\Fail">
                                    <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label14" CssClass="plainlabel" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.fm3dd") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddfm3" CssClass="plainlabel" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm3dd")) %>'>
                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="FM4">
                                    <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label15" CssClass="plainlabel" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.fm4s") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="Label16" CssClass="plainlabel" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.fm4s") %>'>
                                        </asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Pass\Fail">
                                    <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label17" CssClass="plainlabel" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.fm4dd") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddfm4" CssClass="plainlabel" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm4dd")) %>'>
                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="FM5">
                                    <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label18" CssClass="plainlabel" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.fm5s") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="Label19" CssClass="plainlabel" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.fm5s") %>'>
                                        </asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Pass\Fail">
                                    <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label20" CssClass="plainlabel" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.fm5dd") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddfm5" CssClass="plainlabel" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm5dd")) %>'>
                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
            </tbody>
    </table>
    <div class="details" id="tskdiv" style="border-right: black 1px solid; border-top: black 1px solid;
        z-index: 999; border-left: black 1px solid; width: 450px; border-bottom: black 1px solid;
        position: absolute; height: 180px">
        <table cellspacing="0" cellpadding="0" width="450" bgcolor="white">
            <tr bgcolor="blue">
                <td>
                    &nbsp;
                    <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
                        ForeColor="White">Task Description</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closetsk();" alt="" src="../images/appbuttons/minibuttons/close.gif"><br>
                </td>
            </tr>
            <tr class="tbg" height="30">
                <td style="padding-right: 3px; padding-left: 3px; padding-bottom: 3px; padding-top: 3px"
                    colspan="2">
                    <asp:TextBox ID="txttsk" runat="server" TextMode="MultiLine" ReadOnly="True" Width="440px"
                        CssClass="plainlabel" Height="170px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <input id="lbljpid" type="hidden" name="lbljpid" runat="server">
    <input id="lblwo" type="hidden" name="lblwo" runat="server">
    <input id="lbltaskcnt" type="hidden" name="lbltaskcnt" runat="server">
    <input id="lbltaskcurrcnt" type="hidden" name="lbltaskcurrcnt" runat="server">
    <input id="lblrow" type="hidden" name="lblrow" runat="server">
    <input id="lblfmcnt" type="hidden" name="lblfmcnt" runat="server">
    <input id="lbltasknum" type="hidden" name="lbltasknum" runat="server">
    <input id="lblcurrcnt" type="hidden" name="lblcurrcnt" runat="server">
    <input id="xCoord" type="hidden" name="xCoord" runat="server">
    <input id="yCoord" type="hidden" name="yCoord" runat="server"><input id="lblsubmit"
        type="hidden" name="lblsubmit" runat="server">
    <input id="lblfmstr" type="hidden" name="lblfmstr" runat="server">
    <input id="lblmstr" type="hidden" name="lblmstr" runat="server">
    <input id="lblmstrvals" type="hidden" name="lblmstrvals" runat="server">
    <input id="lblpstr" type="hidden" name="lblpstr" runat="server">
    <input id="lbltstr" type="hidden" name="lbltstr" runat="server">
    <input id="lbllstr" type="hidden" name="lbllstr" runat="server">
    <input id="lblmflg" type="hidden" name="lblmflg" runat="server"><input id="lblmcomp"
        type="hidden" name="lblmcomp" runat="server">
    <input type="hidden" id="lblstat" runat="server" name="lblstat">
    <input type="hidden" id="lbllog" runat="server" name="lbllog">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
