<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="woprint_pmpics.aspx.vb"
    Inherits="lucy_r12.woprint_pmpics" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>woprint_pmpics</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/woprint_pmpicsaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <style type="text/css">
    img.big {
        width: 90%;
    }
    </style>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table width="816">
        <tr class="details">
            <td width="93">
                &nbsp;
            </td>
            <td class="details">
            </td>
            <td width="93">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdwi" runat="server" align="center">
            </td>
            <td>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblwotype" runat="server" name="lblwotype">
    <input type="hidden" id="lblpsite" runat="server" name="lblpsite">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
