<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wosave.aspx.vb" Inherits="lucy_r12.wosave" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wosave</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        <!--
        function checkit() {
            var err = document.getElementById("lblerr").value;
            //window.returnValue = err;
            //window.close();
            window.parent.handleifsave(err);
        }
        //-->
    </script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table align="center" height="100%" width="100%" id="Table1">
        <tr>
            <td class="bluelabel" align="center" valign="middle" height="100%">
                Saving Data...
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblerr" runat="server">
    </form>
</body>
</html>
