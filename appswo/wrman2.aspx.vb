﻿Imports System.Data.SqlClient
Public Class wrman2
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim mu As New mmenu_utils_a
    Dim ap As New AppUtils
    Dim wonum, sid, jump As String
    Dim wo As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim eid, loc, did, clid, chk, fid, cid, nid, stat, isdown, lid, eqnum, wt, ro, sdays, jpid, reqd As String
    Dim usr, def, eqid, fuid, ncid, coid, typ, sched As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString '
            wonum = Request.QueryString("wo").ToString '1013 1020
            lblsid.Value = sid
            lblwonum.Value = wonum
            If sid = "" Then
                sid = HttpContext.Current.Session("dfltps").ToString
                lblsid.Value = sid
            End If
            usr = HttpContext.Current.Session("username").ToString
            lbluser.Value = usr
            lblcid.Value = "0"
            'reqd = Request.QueryString("reqd").ToString
            'lblreqd.Value = reqd
            jump = "yes"
            If wonum <> "" Then
                wo.Open()
                JumpWo(wonum)
                wo.Dispose()
            End If
        Else
            If Request.Form("lblsubmit") = "new" Then
                lblsubmit.Value = ""
                wo.Open()
                clearall()
                GetWo()
                'lbljump.Value = "no"
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "draft" Then
                lblsubmit.Value = ""
                wo.Open()
                SaveDraft("draft")
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "save" Then
                lblsubmit.Value = ""
                wo.Open()
                SaveDraft("save")
                wo.Dispose()
            ElseIf Request.Form("lblsubmit") = "ref" Then
                lblsubmit.Value = ""
                wo.Open()
                wonum = lblwonum.Value
                SaveDesc()
                UpdateSuper("draft")
                JumpWo(wonum)
                wo.Dispose()
            End If
        End If
        txtwodesc.Attributes.Add("onBlur", "dosave();")
        txtwodesc.Attributes.Add("onKeyUp", "checksave();")

    End Sub
    Private Sub SaveDraft(ByVal savetype As String)
        cid = lblcid.Value
        wonum = lblwonum.Value
        reqd = lblreqd.Value
        usr = lbluser.Value
        Dim stat, statusdate, typ, charge, jpnum, tstart, tcomp, sstart, scomp, astart, acomp, lead, sup, hrs, skill, se, le
        Dim statdate As DateTime = wo.CNOW
        If savetype = "draft" Then 'And stat <> "WAPPR"
            stat = "HOLD"
        Else
            sql = "select wostatus from wostatus where compid = '" & cid & "' and isdefault = 1"
            dr = wo.GetRdrData(sql)
            While dr.Read
                stat = dr.Item("wostatus").ToString
            End While
            dr.Close()
            If stat = "" Then
                stat = "WAPPR"
            End If
        End If
        sql = "update workorder set status = '" & stat & "' where wonum = '" & wonum & "'"
        wo.Update(sql)

        lblexit.Value = "yes" 'was in else <> draft
        SaveDesc()
        UpdateSuper(savetype)
        If reqd = "ok" Then
            If stat <> "HOLD" Then
                sql = "insert into wohist (wonum, wostatus, statusdate, changeby) " _
               + "values ('" & wonum & "','" & stat & "','" & statdate & "','" & usr & "')"
                wo.Update(sql)
            End If
            UpdateSuper(savetype)
        End If
        If savetype = "save" Then
            Dim mail As New wrmail
            mail.GetWRMail(wonum)
        End If
        'GetWo()
    End Sub
    Private Sub SaveDesc()
        wonum = lblwonum.Value
        Dim sh, lg As String
        Dim lgcnt As Integer
        sh = txtwodesc.Text
        sh = Replace(sh, "'", Chr(180), , , vbTextCompare)
        sh = Replace(sh, "--", "-", , , vbTextCompare)
        sh = Replace(sh, ";", ":", , , vbTextCompare)
        sh = Replace(sh, "/", " ", , , vbTextCompare)
        If Len(sh) > 79 Then
            lg = Mid(sh, 80)
            sh = Mid(sh, 1, 79)
        End If
        Dim cmd As New SqlCommand("exec usp_savewodesc @wonum, @sh, @lg")
        Dim param0 = New SqlParameter("@wonum", SqlDbType.Int)
        param0.Value = wonum
        cmd.Parameters.Add(param0)
        Dim param01 = New SqlParameter("@sh", SqlDbType.VarChar)
        If sh = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = sh
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@lg", SqlDbType.Text)
        If lg = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = lg
        End If
        cmd.Parameters.Add(param02)
        Try
            wo.UpdateHack(cmd)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr617", "wrdet.aspx.vb")

            wo.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
    End Sub
    Private Sub UpdateSuper(ByVal savetype As String)
        Dim skill As String = lblskillid.Value
        Dim supid, sid, did, locid, wonum, super, phone, email As String
        wonum = lblwonum.Value
        did = lbldid.Value
        locid = lbllid.Value
        sid = lblsid.Value
        If skill <> "" Then
            If did <> "" Then
                sql = "select top 1 s.superid from pmsuperlocs sl " _
                + "left join pmsupercrafts sk1 on sk1.superid = sl.superid " _
                + "left join pmsysusers s on s.userid = sl.superid " _
                + "where(sl.siteid = '" & sid & "' and sl.dept_id = '" & did & "' and sk1.skillid = '" & skill & "')"
            ElseIf locid <> "" Then
                sql = "select top 1 s.superid from pmsuperlocs sl " _
                + "left join pmsupercrafts sk1 on sk1.superid = sl.superid " _
                + "left join pmsysusers s on s.userid = sl.superid " _
                + "where(sl.siteid = '" & sid & "' and sl.locid = '" & locid & "' and sk1.skillid = '" & skill & "')"
            Else
                sql = ""
            End If
            If sql <> "" Then
                Try
                    supid = wo.strScalar(sql)
                Catch ex As Exception

                End Try
            End If
            If supid = "" Then
                If did <> "" Then
                    sql = "select top 1 s.superid from pmsuperlocs sl " _
                    + "left join pmsuper s on s.superid = sl.superid " _
                    + "where(sl.siteid = '" & sid & "' and sl.dept_id = '" & did & "')"
                ElseIf locid <> "" Then
                    sql = "select top 1 s.superid from pmsuperlocs sl " _
                    + "left join pmsuper s on s.superid = sl.superid " _
                    + "where(sl.siteid = '" & sid & "' and sl.locid = '" & locid & "')"
                Else
                    sql = ""
                End If
                If sql <> "" Then
                    Try
                        supid = wo.strScalar(sql)
                    Catch ex As Exception

                    End Try
                End If
            End If
            If supid = "" Then
                sql = "select userid from pmsysusers where isdefault = 1 and uid in (select uid from " _
                + "pmusersites where siteid = '" & sid & "')"
                Try
                    supid = wo.strScalar(sql)
                Catch ex As Exception

                End Try
            End If
            If supid <> "" Then
                sql = "update workorder set superid = '" & supid & "', supervisor = (select username from " _
                + "pmsysusers where userid = '" & supid & "') where wonum = '" & wonum & "'"
                wo.Update(sql)
            End If
        Else
            If did <> "" Then
                sql = "select top 1 s.superid from pmsuperlocs sl " _
                + "left join pmsuper s on s.superid = sl.superid " _
                + "where(sl.siteid = '" & sid & "' and sl.dept_id = '" & did & "')"
            ElseIf locid <> "" Then
                sql = "select top 1 s.superid from pmsuperlocs sl " _
                + "left join pmsuper s on s.superid = sl.superid " _
                + "where(sl.siteid = '" & sid & "' and sl.locid = '" & locid & "')"
            Else
                sql = ""
            End If
            If sql <> "" Then
                Try
                    supid = wo.strScalar(sql)
                Catch ex As Exception

                End Try
            End If
            If supid = "" Then
                sql = "select userid from pmsysusers where isdefault = 1 and dfltps = '" & sid & "'"
                ' uid in (select uid from " _
                '+ "pmusersites where siteid = '" & sid & "')"
                Try
                    supid = wo.strScalar(sql)
                Catch ex As Exception

                End Try
            End If
            If supid <> "" Then
                sql = "update workorder set superid = '" & supid & "', supervisor = (select username from " _
               + "pmsysusers where userid = '" & supid & "') where wonum = '" & wonum & "'"
                wo.Update(sql)
            End If
        End If

    End Sub
    Private Sub GetWo()
        cid = lblcid.Value
        sid = lblsid.Value
        usr = lbluser.Value
        def = "HOLD"
        Dim statdate As DateTime = wo.CNOW

        sql = "insert into workorder (worktype, siteid, status, statusdate, changeby, changedate, reportedby, reportdate, lcomp, scomp) " _
        + "values ('WR','" & sid & "','" & def & "', getDate(),'" & usr & "', '" & statdate & "','" & usr & "', getDate(), 0, 0) " _
        + "select @@identity"
        Try
            wonum = wo.Scalar(sql)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdst615", "wradd.aspx.vb")

            wo.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Try
            sql = "insert into wohist (wonum, wostatus, statusdate, changeby) values ('" & wonum & "','HOLD', '" & statdate & "', '" & usr & "')"
            wo.Update(sql)
        Catch ex As Exception

        End Try

        txtwo.Text = wonum
        txtwodesc.Text = ""
        lblwonum.Value = wonum
        lbljump.Value = "no"
        lblstat.Value = "HOLD"
        lblreqd.Value = "no"
        JumpWo(wonum)
    End Sub
    Private Sub clearall()
        trlocs.Attributes.Add("class", "details")
        trdepts.Attributes.Add("class", "details")
        tdphone.InnerHtml = ""
        tdreq.InnerHtml = ""
        tdreqdate.InnerHtml = ""

        txtwodesc.Text = ""
        tdwt.InnerHtml = ""

        lbldid.Value = ""
        lbllid.Value = ""
        'lblsid.Value = ""

        tdstatus.InnerHtml = ""
        lblstat.Value = ""
        tdstatdate.InnerHtml = ""

        txtcharge.Text = ""

        txttstart.Text = ""

        txttcomp.Text = ""

        lblpmid.Value = ""
        tdskill.InnerHtml = ""
        lblskillid.Value = ""

        lbltyp.Value = ""

        lbllid.Value = ""
        lblloc.Value = ""
        lbleq.Value = ""
        lbleqid.Value = ""
        tdeq.InnerHtml = ""
        '*
        lbldid.Value = ""
        lbldept.Value = ""
        tddept.InnerHtml = ""
        lblclid.Value = ""
        lblcell.Value = ""
        tdcell.InnerHtml = ""

        lblreqd.Value = ""

        lbltyp.Value = ""
        lbllid.Value = ""
        lblloc.Value = ""
        tdloc3.InnerHtml = ""
        lbleq.Value = ""
        lbleqid.Value = ""
        tdeq3.InnerHtml = ""

        tdsuper.InnerHtml = ""
        tdsphone.InnerHtml = ""
        tdemail.InnerHtml = ""

        txtwodesc.ReadOnly = False

        txtwodesc.CssClass = "plainlabel"
        txtcharge.CssClass = "plainlabel"
        txttstart.CssClass = "plainlabel"
        txttcomp.CssClass = "plainlabelblue"
        tdreq.Attributes.Add("class", "plainlabel")
        tdreqdate.Attributes.Add("class", "plainlabel")
        tdwt.Attributes.Add("class", "plainlabel")
        tdstatus.Attributes.Add("class", "plainlabel")
        tdstatdate.Attributes.Add("class", "plainlabel")
        tdskill.Attributes.Add("class", "plainlabel")
        tdsuper.Attributes.Add("class", "plainlabel")
        tdphone.Attributes.Add("class", "plainlabel")
        tdsphone.Attributes.Add("class", "plainlabel")
        tdemail.Attributes.Add("class", "plainlabel")
    End Sub
    Private Sub JumpWo(ByVal wonum As String)

        cid = lblcid.Value
        wonum = lblwonum.Value
        txtwo.Text = wonum
        Dim stat, ld, skill, cell, dept As String
        sql = "select w.*, d.dept_line, cl.cell_name, Convert(char(10),w.targstartdate,101) as 'tstart', " _
            + "isnull(u.phonenum, '') as phonenum, " _
        + "Convert(char(10),w.targcompdate,101) as 'tcomp' " _
        + "from workorder w " _
        + "left join dept d on d.dept_id = w.deptid " _
        + "left join cells cl on cl.cellid = w.cellid " _
        + "left join pmsysusers u on u.username = w.reportedby " _
        + "where w.wonum = '" & wonum & "'"
        dr = wo.GetRdrData(sql)
        Dim tss, tsc, tas, tac, tts, ttc, se, le, supid, did, lid, sid
        While dr.Read
            tdphone.InnerHtml = dr.Item("phonenum").ToString
            tdreq.InnerHtml = dr.Item("reportedby").ToString
            tdreqdate.InnerHtml = dr.Item("reportdate").ToString
            supid = dr.Item("superid").ToString
            txtwodesc.Text = dr.Item("description").ToString
            tdwt.InnerHtml = dr.Item("worktype").ToString
            loc = dr.Item("location").ToString
            eqnum = dr.Item("eqnum").ToString
            eid = dr.Item("eqid").ToString
            ld = dr.Item("ld").ToString
            did = dr.Item("deptid").ToString
            dept = dr.Item("dept_line").ToString
            clid = dr.Item("cellid").ToString
            cell = dr.Item("cell_name").ToString
            lid = dr.Item("locid").ToString
            sid = dr.Item("siteid").ToString
            lbldid.Value = did
            lbllid.Value = lid
            lblsid.Value = sid

            stat = dr.Item("status").ToString
            tdstatus.InnerHtml = stat
            lblstat.Value = stat
            tdstatdate.InnerHtml = dr.Item("statusdate").ToString

            txtcharge.Text = dr.Item("chargenum").ToString

            tts = dr.Item("tstart").ToString
            If tts <> "" Then
                txttstart.Text = tts
            End If
            ttc = dr.Item("tcomp").ToString
            If ttc <> "" Then
                txttcomp.Text = ttc
            End If
            lblpmid.Value = dr.Item("pmid").ToString
            tdskill.InnerHtml = dr.Item("skill").ToString
            lblskillid.Value = dr.Item("skillid").ToString
        End While
        dr.Close()
        sql = "select longdesc from wolongdesc where wonum = '" & wonum & "'"
        dr = wo.GetRdrData(sql)
        While dr.Read
            txtwodesc.Text += dr.Item("longdesc").ToString
        End While
        dr.Close()
        If supid <> "" Then
            sql = "select username, phonenum, email from pmsysusers where userid = '" & supid & "'"
            dr = wo.GetRdrData(sql)
            While dr.Read
                tdsuper.InnerHtml = dr.Item("username").ToString
                tdsphone.InnerHtml = dr.Item("phonenum").ToString
                tdemail.InnerHtml = dr.Item("email").ToString
            End While
            dr.Close()
        End If
        If ld = "D" Then
            trdepts.Attributes.Add("class", "view")

            lbltyp.Value = "depts"
            trlocs.Attributes.Add("class", "details")
            trdepts.Attributes.Add("class", "view")
            lbllid.Value = lid
            lblloc.Value = loc
            lbleq.Value = eqnum
            lbleqid.Value = eid
            tdeq.InnerHtml = eqnum
            '*
            lbldid.Value = did
            lbldept.Value = dept
            tddept.InnerHtml = dept
            lblclid.Value = clid
            lblcell.Value = cell
            tdcell.InnerHtml = cell

            lblreqd.Value = "ok"
        ElseIf ld = "L" Then
            trlocs.Attributes.Add("class", "view")
            trdepts.Attributes.Add("class", "details")
            lbltyp.Value = "locs"
            lbllid.Value = lid
            lblloc.Value = loc
            tdloc3.InnerHtml = loc
            lbleq.Value = eqnum
            lbleqid.Value = eid
            tdeq3.InnerHtml = eqnum
            lblreqd.Value = "ok"
        End If
        If stat <> "HOLD" Then
            DisableFields()
        End If
    End Sub
    Private Sub DisableFields()
        txtwodesc.ReadOnly = True
        txtwodesc.CssClass = "plainlabelblue"
        txtcharge.CssClass = "plainlabelblue"
        txttstart.CssClass = "plainlabelblue"
        txttcomp.CssClass = "plainlabelblue"
        tdreq.Attributes.Add("class", "plainlabelblue")
        tdreqdate.Attributes.Add("class", "plainlabelblue")
        tdwt.Attributes.Add("class", "plainlabelblue")
        tdstatus.Attributes.Add("class", "plainlabelblue")
        tdstatdate.Attributes.Add("class", "plainlabelblue")
        tdskill.Attributes.Add("class", "plainlabelblue")
        tdsuper.Attributes.Add("class", "plainlabelblue")
        tdphone.Attributes.Add("class", "plainlabelblue")
        tdsphone.Attributes.Add("class", "plainlabelblue")
        tdemail.Attributes.Add("class", "plainlabelblue")

    End Sub
End Class