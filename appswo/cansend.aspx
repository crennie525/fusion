﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="cansend.aspx.vb" Inherits="lucy_r12.cansend" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lawson Interface Test Screen</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="javascript" type="text/javascript">
    <!--
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }
        function gotoreq(wo) {
            document.getElementById("lblwo").value = wo;
            document.getElementById("lblret").value = "getvals"
            document.getElementById("form1").submit();
        }

        function excel() {
            document.getElementById("lblret").value = "excel"
            document.getElementById("form1").submit();
        }
        function excela() {
            document.getElementById("lblret").value = "excela"
            document.getElementById("form1").submit();
        }

        function sendit() {
            document.getElementById("lblret").value = "sendit"
            document.getElementById("form1").submit();
        }

        function senditnoecd() {
            document.getElementById("lblwoecd").value = "1"
            document.getElementById("lblret").value = "sendit"
            document.getElementById("form1").submit();
        }
        function senditwnums() {
            document.getElementById("lblwnums").value = "1"
            document.getElementById("lblret").value = "sendit"
            document.getElementById("form1").submit();
        }
        function senditnoecdwnums() {
            document.getElementById("lblwoecd").value = "1"
            document.getElementById("lblwnums").value = "1"
            document.getElementById("lblret").value = "sendit"
            document.getElementById("form1").submit();
        }

        function sendit2() {
            var sTransactionID = document.getElementById("lblsTransactionID").value;
            var sConfigPosition = document.getElementById("lblsConfigPosition").value;
            var sEquipment = document.getElementById("lblsEquipment").value;
            var sStructureType = document.getElementById("lblsStructureType").value;
            var sService = document.getElementById("lblsService").value;
            var sResponsible = document.getElementById("lblsResponsible").value;
            var iOperation = document.getElementById("lbliOperation").value;
            var sDescription = document.getElementById("lblsDescription").value;
            var sPlanningGroup = document.getElementById("lblsPlanningGroup").value;
            var sPriority = document.getElementById("lblsPriority").value;
            var sApprovedBy = document.getElementById("lblsApprovedBy").value;
            var sReasonCode = document.getElementById("lblsReasonCode").value;
            var sStartDate = document.getElementById("lblsStartDate").value;
            var sFinishDate = document.getElementById("lblsFinishDate").value;
            var sTxt1 = document.getElementById("lblsTxt1").value;
            var sTxt2 = document.getElementById("lblsTxt2").value;
            var sErrorCode1 = document.getElementById("lblsErrorCode1").value;
            var sErrorCode2 = document.getElementById("lblsErrorCode2").value;
            var sErrorCode3 = document.getElementById("lblsErrorCode3").value;
            var sComplaintType = document.getElementById("lblsComplaintType").value;
            var iRetroFlag = document.getElementById("lbliRetroFlag").value;
            var sItemNumber = document.getElementById("lblsItemNumber").value;
            var sLotNumber = document.getElementById("lblsLotNumber").value;
            var iPlannedQuantity = document.getElementById("lbliPlannedQuantity").value;
            var sFacility = document.getElementById("lblsFacility").value;
            var sRequestedStartDte = document.getElementById("lblsRequestedStartDte").value;
            var sRequestedFinishDte = document.getElementById("lblsRequestedFinishDte").value;
            var iSetupTime = document.getElementById("lbliSetupTime").value;
            var iRuntime = document.getElementById("lbliRuntime").value;
            var iPlannedNumWkrs = document.getElementById("lbliPlannedNumWkrs").value;
            var sTextBlock = document.getElementById("lblsTextBlock").value;

            var eReturn = window.showModalDialog("cansendclient.aspx?sTransactionID=" + sTransactionID +
                                                "&sConfigPosition=" + sConfigPosition +
                                                "&sEquipment=" + sEquipment +
                                                "&sStructureType=" + sStructureType +
                                                "&sService=" + sService +
                                                "&sResponsible=" + sResponsible +
                                                "&iOperation=" + iOperation +
                                                "&sDescription=" + sDescription +
                                                "&sPlanningGroup=" + sPlanningGroup +
                                                "&sPriority=" + sPriority +
                                                "&sApprovedBy=" + sApprovedBy +
                                                "&sReasonCode=" + sReasonCode +
                                                "&sStartDate=" + sStartDate +
                                                "&sFinishDate=" + sFinishDate +
                                                "&sTxt1=" + sTxt1 +
                                                "&sTxt2=" + sTxt2 +
                                                "&sErrorCode1=" + sErrorCode1 +
                                                "&sErrorCode2=" + sErrorCode2 +
                                                "&sErrorCode3=" + sErrorCode3 +
                                                "&sComplaintType=" + sComplaintType +
                                                "&iRetroFlag=" + iRetroFlag +
                                                "&sItemNumber=" + sItemNumber +
                                                "&sLotNumber=" + sLotNumber +
                                                "&iPlannedQuantity=" + iPlannedQuantity +
                                                "&sFacility=" + sFacility +
                                                "&sRequestedStartDte=" + sRequestedStartDte +
                                                "&sRequestedFinishDte=" + sRequestedFinishDte +
                                                "&iSetupTime=" + iSetupTime +
                                                "&iRuntime=" + iRuntime +
                                                "&iPlannedNumWkrs=" + iPlannedNumWkrs +
                                                "&sTextBlock=" + sTextBlock, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
            if (eReturn) {

            }
            document.getElementById("lblret").value = "sendit"

        }
        function checkwo() {
            document.getElementById("lblret").value = "first"
            document.getElementById("form1").submit();
        }
        function checkit() {
            //alert(document.getElementById("lblret").value)
        }
        //-->
    </script>
    <style type="text/css">
        .hdr
        {
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 14px;
            font-weight: bold;
            text-decoration: none;
            color: black;
            background: #f2f2f2;
            vertical-align: middle;
            height: 36px;
        }
        .hdrsing
        {
            padding-left: .5em;
            background: #B9D6F8;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            font-weight: bold;
            text-decoration: none;
            height: 24px;
        }
        .bgy
        {
            background: Yellow;
        }
        .gbgy
        {
            background: Yellow;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
        }
        .wolistsmall2a
        {
            width: 600px;
            height: 410px;
            overflow: auto;
        }
        .wolistsmall3a
        {
            width: 420px;
            height: 410px;
            overflow: auto;
        }
    </style>
</head>
<body onload="checkit();">
    <form id="form1" runat="server">
    <div>
        <table width="600" cellpadding="0" cellspacing="1" border="0">
            <tr>
                <td class="hdr" colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;Lawson Interface Test Screen
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <img alt="" src="../images/appbuttons/minibuttons/2PX.gif" height="3px" />
                </td>
            </tr>
            <tr>
                <td colspan="3" class="plainlabelblue">
                    Please Note that the Work Order Number Search option will only return work orders
                    with a status of "COMP"
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <img alt="" src="../images/appbuttons/minibuttons/2PX.gif" height="3px" />
                </td>
            </tr>
            <tr>
                <td class="hdrsing">
                    Work Orders to Transmit
                </td>
                <td width="5px">
                    &nbsp;
                </td>
                <td class="hdrsing">
                    Transmit Details
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="plainlabel">
                                Plant Site
                            </td>
                            <td>
                                <asp:DropDownList ID="ddsites" runat="server" AutoPostBack="True">
                                    <asp:ListItem Value="12" Selected="True">Practice</asp:ListItem>
                                    <asp:ListItem Value="13">Canton</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class="plainlabel">
                                Search by Work Order Number
                            </td>
                            <td>
                                <asp:TextBox ID="txtsearch" runat="server" class="plainlabel"></asp:TextBox>
                            </td>
                            <td>
                                <img alt="" src="../images/appbuttons/minibuttons/srchsm.gif" onclick="checkwo();" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
                <td class="plainlabel">
                    <table>
                        <tr>
                            <td class="plainlabel" height="20px" width="100">
                                Selected WO:
                            </td>
                            <td class="plainlabel" id="tdwo" runat="server" width="110">
                            </td>
                            <td class="plainlabel" height="20px" width="100">
                                Return WO:
                            </td>
                            <td class="plainlabel" id="tdrwo" runat="server" width="110">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td id="tdywr" runat="server">
                    <div id="divywr" class="wolistsmall2a" runat="server">
                    </div>
                </td>
                <td>
                </td>
                <td id="td1" runat="server">
                    <div id="divret" class="wolistsmall3a" runat="server">
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" valign="top">
                    <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                        border-right: blue 1px solid" cellspacing="0" cellpadding="0" width="300">
                        <tr>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
                                <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                    runat="server">
                            </td>
                            <td width="20">
                                <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                    runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
                <td rowspan="2" align="center" valign="top" class="plainlabelred" id="tdgo" runat="server">
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="3">
                    <table border="0">
                        <tr>
                            <td width="80">
                            </td>
                            <td width="300">
                            </td>
                            <td width="380">
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                Mode
                            </td>
                            <td class="plainlabelblue" id="tdmode" runat="server">
                            </td>
                            <td>
                            </td>
                            <td class="plainlabelblue" align="left">
                                <a href="cansendout2.aspx">Review Output</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                Service 1
                            </td>
                            <td class="plainlabelblue" id="tdservice" runat="server">
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                Service 2
                            </td>
                            <td class="plainlabelblue" id="tdservice2" runat="server">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="top">
                                <iframe id="iftd" src="cansendeout.aspx" frameborder="0" width="100%" runat="server">
                                </iframe>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
    <asp:DataGrid ID="dgout" runat="server" ShowFooter="True">
    </asp:DataGrid>
    <asp:DataGrid ID="dgouta" runat="server" ShowFooter="True">
    </asp:DataGrid>
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="txtpg" runat="server" />
    <input type="hidden" id="txtpgcnt" runat="server" />
    <input type="hidden" id="lblwo" runat="server" />
    <input type="hidden" id="lblMWNO" runat="server" />
    <input type="hidden" id="lblpopstring" runat="server" />
    <input type="hidden" id="lblerrcnt" runat="server" />
    <input type="hidden" id="lblisact" runat="server" />
    <input type="hidden" id="lbllcnt" runat="server" />
    <input type="hidden" id="lblserv1" runat="server" />
    <input type="hidden" id="lblserv2" runat="server" />
    <input type="hidden" id="lblsmode" runat="server" />
    <input type="hidden" id="lblsTransactionID" runat="server" />
    <input type="hidden" id="lblsConfigPosition" runat="server" />
    <input type="hidden" id="lblsEquipment" runat="server" />
    <input type="hidden" id="lblsStructureType" runat="server" />
    <input type="hidden" id="lblsService" runat="server" />
    <input type="hidden" id="lblsResponsible" runat="server" />
    <input type="hidden" id="lbliOperation" runat="server" />
    <input type="hidden" id="lblsDescription" runat="server" />
    <input type="hidden" id="lblsPlanningGroup" runat="server" />
    <input type="hidden" id="lblsPriority" runat="server" />
    <input type="hidden" id="lblsApprovedBy" runat="server" />
    <input type="hidden" id="lblsReasonCode" runat="server" />
    <input type="hidden" id="lblsStartDate" runat="server" />
    <input type="hidden" id="lblsFinishDate" runat="server" />
    <input type="hidden" id="lblsTxt1" runat="server" />
    <input type="hidden" id="lblsTxt2" runat="server" />
    <input type="hidden" id="lblsErrorCode1" runat="server" />
    <input type="hidden" id="lblsErrorCode2" runat="server" />
    <input type="hidden" id="lblsErrorCode3" runat="server" />
    <input type="hidden" id="lblsComplaintType" runat="server" />
    <input type="hidden" id="lbliRetroFlag" runat="server" />
    <input type="hidden" id="lblsItemNumber" runat="server" />
    <input type="hidden" id="lblsLotNumber" runat="server" />
    <input type="hidden" id="lbliPlannedQuantity" runat="server" />
    <input type="hidden" id="lblsFacility" runat="server" />
    <input type="hidden" id="lblsRequestedStartDte" runat="server" />
    <input type="hidden" id="lblsRequestedFinishDte" runat="server" />
    <input type="hidden" id="lbliSetupTime" runat="server" />
    <input type="hidden" id="lbliRuntime" runat="server" />
    <input type="hidden" id="lbliPlannedNumWkrs" runat="server" />
    <input type="hidden" id="lblsTextBlock" runat="server" />
    <input type="hidden" id="lblurl1" runat="server" />
    <input type="hidden" id="lblurl2" runat="server" />
    <input type="hidden" id="lblwoecd" runat="server" />
    <input type="hidden" id="lblwnums" runat="server" />
    </form>
</body>
</html>
