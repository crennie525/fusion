<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wofail.aspx.vb" Inherits="lucy_r12.wofail" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wofail</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/wofailaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkstat();" class="tbg">
    <form id="form1" method="post" runat="server">
    <div class="FreezePaneOff" id="FreezePane" align="center">
        <div class="InnerFreezePane" id="InnerFreezePane">
        </div>
    </div>
    <table style="left: 0px; position: absolute; top: 0px" cellspacing="0" cellpadding="1"
        width="380">
        <tr>
            <td width="180">
            </td>
            <td width="20">
            </td>
            <td width="180">
            </td>
        </tr>
        <tr>
            <td class="label" align="center">
                <asp:Label ID="lang1385" runat="server">Component Failure Modes</asp:Label>
            </td>
            <td>
            </td>
            <td class="label" align="center">
                <asp:Label ID="lang1386" runat="server">Selected Failure Modes</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:ListBox ID="lbCompFM" runat="server" CssClass="plainlabel" Width="170px" SelectionMode="Multiple"
                    Height="80px"></asp:ListBox>
            </td>
            <td valign="middle" align="center">
                <img id="imggetfm" runat="server" onclick="getfm();" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                    border="0"><br>
                <img class="details" id="todis" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                    width="20" runat="server">
                <img class="details" id="fromdis" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
                    width="20" runat="server">
                <asp:ImageButton ID="btntowo" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                </asp:ImageButton><br>
                <asp:ImageButton ID="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                </asp:ImageButton>
            </td>
            <td align="center">
                <asp:ListBox ID="lbfailmodes" runat="server" CssClass="plainlabel" Width="170px"
                    SelectionMode="Multiple" Height="80px"></asp:ListBox>
            </td>
        </tr>
    </table>
    <input id="lblwo" type="hidden" runat="server"><input id="lblcid" type="hidden" runat="server">
    <input id="lblsubmit" type="hidden" runat="server">
    <input id="lblco" type="hidden" runat="server">
    <input type="hidden" id="lblcd" runat="server">
    <input type="hidden" id="lblstat" runat="server">
    <input type="hidden" id="lblwt" runat="server">
    <input type="hidden" id="lbllog" runat="server" name="lbllog">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
