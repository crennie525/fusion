<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wopmlist.aspx.vb" Inherits="lucy_r12.wopmlist" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wopmlist</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        <!--
        function getweeks(id) {
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/labweekselectdialog.aspx?sid=" + sid, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
            if (eReturn) {
                var ret = eReturn.split("~")
                document.getElementById("lblweekstart").value = ret[0];
                document.getElementById("lblweekend").value = ret[3];
                document.getElementById("lblsubmit").value = "newweek";
                document.getElementById("form1").submit();
            }
        }
        function getwt() {
            var wo = ""; //document.getElementById("lblwonum").value;
            var eReturn = window.showModalDialog("wostatusdialog.aspx?typ=wt&wo=" + wo, "", "dialogHeight:370px; dialogWidth:370px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    document.getElementById("tdwt").innerHTML = eReturn;
                    document.getElementById("lbltyp").value = eReturn;
                    document.getElementById("lblsubmit").value = "newweek";
                    document.getElementById("form1").submit();
                }
            }
        }
        //-->
    </script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 0px; left: 0px" width="1450" cellpadding="0"
        cellspacing="0">
        <tr>
            <td id="tdhdr" runat="server">
                <div id="divhdr" class="schedlistlnghdr" runat="server">
                </div>
            </td>
        </tr>
        <tr>
            <td id="tdywr" runat="server">
                <div id="divywr" class="schedlistlng" runat="server">
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                    border-right: blue 1px solid" cellspacing="0" cellpadding="0" width="300">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="txtpg" type="hidden" name="txtpg" runat="server">
    <input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
    <input type="hidden" id="lblfslang" runat="server" name="lblfslang">
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lbltyp" runat="server">
    <input type="hidden" id="lblweekstart" runat="server">
    <input type="hidden" id="lblweekend" runat="server">
    <input type="hidden" id="lblsubmit" runat="server">
    <input type="hidden" id="lblworktype" runat="server">
    </form>
</body>
</html>
