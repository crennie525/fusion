Imports System.Data.SqlClient
Imports System.Text
Public Class wobacklog
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim mm As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 12
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim rowcnt As Integer
    Dim sid, typ, ws, we, who, wo, uid, username, islabor, isplanner, issuper, Logged_In, appstr, ms, ro As String
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblislabor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblissuper As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisplanner As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblLogged_In As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblms As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dgout As System.Web.UI.WebControls.DataGrid

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents tdhdr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents divhdr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divhdrr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divywr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divywrr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblweekstart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblweekend As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblworktype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblxCoord_sbr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblyCoord_sbr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwkid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            who = Request.QueryString("who").ToString
            lblwho.Value = who
            Try
                uid = Request.QueryString("uid").ToString
            Catch ex As Exception
                uid = Request.QueryString("userid").ToString
            End Try
            lbluid.Value = uid
            Try
                username = Request.QueryString("usrname").ToString
            Catch ex As Exception
                username = Request.QueryString("username").ToString
            End Try
            lblusername.Value = username
            islabor = Request.QueryString("islabor").ToString
            lblislabor.Value = islabor
            issuper = Request.QueryString("issuper").ToString
            lblissuper.Value = issuper
            isplanner = Request.QueryString("isplanner").ToString
            lblisplanner.Value = isplanner
            who = Request.QueryString("who").ToString
            lblwho.Value = who
            Logged_In = Request.QueryString("Logged_In").ToString
            lblLogged_In.Value = Logged_In
            ro = Request.QueryString("ro").ToString
            lblro.Value = ro
            ms = Request.QueryString("ms").ToString
            lblms.Value = ms
            appstr = Request.QueryString("appstr").ToString
            lblappstr.Value = appstr
            Try
                wo = Request.QueryString("wo").ToString
            Catch ex As Exception
                wo = ""
            End Try

            lblwonum.Value = wo
            typ = "ALL"
            lbltyp.Value = typ
            mm.Open()
            BuildMyWR(PageNumber)
            mm.Dispose()
        Else
            If Request.Form("lblsubmit") = "newweek" Then
                lblsubmit.Value = ""
                mm.Open()
                BuildMyWR(PageNumber)
                mm.Dispose()
            End If
            If Request.Form("lblret") = "next" Then
                mm.Open()
                GetNext()
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                mm.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                BuildMyWR(PageNumber)
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                mm.Open()
                GetPrev()
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                mm.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                BuildMyWR(PageNumber)
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "filt" Then
                lblret.Value = ""
                mm.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                who = lblwho.Value
                BuildMyWR(PageNumber)
                mm.Dispose()
            ElseIf Request.Form("lblret") = "excel" Then
                lblret.Value = ""
                mm.Open()
                BindExport()
                mm.Dispose()
            End If
        End If
    End Sub
    Private Sub BindExport()
        Dim field, srch As String
        Dim srchi As Integer
        sid = lblsid.Value
        Dim rt, rs, ra, sa, sup, lead, did, clid, lid, rettyp, level, lstring, jl, fpc As String
        Dim waid, wpaid, fdate, tdate, wtret, statret, eqid, fuid, coid, usid As String
        Dim filtret As String = lblfiltret.Value
        Dim woret As String
        Dim datret As String = "0"
        Dim wa As String = "1"
        If filtret <> "" Then
            Dim filtarr() As String = filtret.Split("~")
            waid = filtarr(0).ToString
            wpaid = filtarr(1).ToString
            fdate = filtarr(2).ToString
            tdate = filtarr(3).ToString
            wtret = filtarr(4).ToString
            statret = filtarr(5).ToString
            woret = filtarr(6).ToString

            rt = filtarr(9).ToString
            rs = filtarr(10).ToString
            ra = filtarr(11).ToString
            sa = filtarr(12).ToString
            wa = filtarr(13).ToString
            sup = filtarr(14).ToString
            lead = filtarr(15).ToString
            did = filtarr(16).ToString
            clid = filtarr(17).ToString
            lid = filtarr(18).ToString
            eqid = filtarr(19).ToString
            fuid = filtarr(20).ToString
            coid = filtarr(21).ToString
            rettyp = filtarr(22).ToString
            level = filtarr(23).ToString
            jl = filtarr(24).ToString
            fpc = filtarr(25).ToString
            If rt = "1" Or rs = "1" Or ra = "1" Then
                datret = "1"
            End If
            If eqid = "" Then
                If rettyp = "depts" Then
                    If Filter <> "" Then
                        Filter += " and w.deptid = ''" & did & "'' "
                        FilterCNT += " and w.deptid = '" & did & "' "
                    Else
                        Filter = "w.deptid = ''" & did & "'' "
                        FilterCNT = "w.deptid = '" & did & "' "
                    End If
                    If clid <> "" Then
                        If Filter <> "" Then
                            Filter += " and w.cellid = ''" & clid & "'' "
                            FilterCNT += " and w.cellid = '" & clid & "' "
                        Else
                            Filter = "w.cellid = ''" & clid & "'' "
                            FilterCNT = "w.cellid = '" & clid & "' "
                        End If
                    End If
                ElseIf rettyp = "locs" Then
                    If level <> 1 Then
                        lstring = checkloc(lid, jl)
                        If Filter <> "" Then
                            Filter += " and w.locid in (" & lstring & ") "
                            FilterCNT += " and w.locid in (" & lstring & ") "
                        Else
                            Filter = " w.locid in (" & lstring & ") "
                            FilterCNT = " w.locid in (" & lstring & ") "
                        End If
                    End If

                End If
            End If
            If fpc <> "" Then
                fpc = Replace(fpc, "'", Chr(180), , , vbTextCompare)
                If Filter <> "" Then
                    Filter += " and w.eqid in (select e.eqid from equipment e where e.fpc like ''%" & fpc & "%'') "
                    FilterCNT += " and w.eqid in (select e.eqid from equipment e where e.fpc like '%" & fpc & "%') "
                Else
                    Filter += " w.eqid in (select e.eqid from equipment e where e.fpc like ''%" & fpc & "%'') "
                    FilterCNT += " w.eqid in (select e.eqid from equipment e where e.fpc like '%" & fpc & "%') "
                End If
            End If
            'targ
            If fdate <> "" Then
                If rt = "1" Then
                    If Filter <> "" Then
                        Filter += "and targstartdate >= ''" & fdate & "'' "
                        FilterCNT += "and targstartdate >= '" & fdate & "' "
                    Else
                        Filter = "targstartdate >= ''" & fdate & "'' "
                        FilterCNT = "targstartdate >= '" & fdate & "' "
                    End If
                End If
            End If
            If tdate <> "" Then
                If rt = "1" And who <> "pm" Then
                    If Filter <> "" Then
                        Filter += "and targcompdate <= ''" & tdate & "'' "
                        FilterCNT += "and targcompdate <= '" & tdate & "' "
                    Else
                        Filter = "targcompdate <= ''" & tdate & "'' "
                        FilterCNT = "targcompdate <= '" & tdate & "' "
                    End If
                ElseIf rt = "1" And who = "pm" Then
                    If Filter <> "" Then
                        Filter += "and targstartdate <= ''" & tdate & "'' "
                        FilterCNT += "and targstartdate <= '" & tdate & "' "
                    Else
                        Filter = "targstartdate <= ''" & tdate & "'' "
                        FilterCNT = "targstartdate <= '" & tdate & "' "
                    End If
                End If
            End If

            'sched
            If fdate <> "" Then
                If rs = "1" Then
                    If Filter <> "" Then
                        Filter += "and schedstart >= ''" & fdate & "'' "
                        FilterCNT += "and schedstart >= '" & fdate & "' "
                    Else
                        Filter = "schedstart >= ''" & fdate & "'' "
                        FilterCNT = "schedstart >= '" & fdate & "' "
                    End If
                End If
            End If
            If tdate <> "" Then
                If rs = "1" And who <> "pm" Then
                    If Filter <> "" Then
                        Filter += "and schedfinish <= ''" & tdate & "'' "
                        FilterCNT += "and schedfinish <= '" & tdate & "' "
                    Else
                        Filter = "schedfinish <= ''" & tdate & "'' "
                        FilterCNT = "schedfinish <= '" & tdate & "' "
                    End If
                ElseIf rs = "1" And who = "pm" Then
                    If Filter <> "" Then
                        Filter += "and schedstart <= ''" & tdate & "'' "
                        FilterCNT += "and schedstart <= '" & tdate & "' "
                    Else
                        Filter = "schedstart <= ''" & tdate & "'' "
                        FilterCNT = "schedstart <= '" & tdate & "' "
                    End If
                End If
            End If

            'actual
            If fdate <> "" Then
                If ra = "1" And who <> "pm" Then
                    If Filter <> "" Then
                        Filter += "and actstart >= ''" & fdate & "'' "
                        FilterCNT += "and actstart >= '" & fdate & "' "
                    Else
                        Filter = "actstart >= ''" & fdate & "'' "
                        FilterCNT = "actstart >= '" & fdate & "' "
                    End If
                ElseIf ra = "1" And who = "pm" Then
                    If Filter <> "" Then
                        Filter += "and actfinish >= ''" & fdate & "'' "
                        FilterCNT += "and actfinish >= '" & fdate & "' "
                    Else
                        Filter = "actfinish >= ''" & fdate & "'' "
                        FilterCNT = "actfinish >= '" & fdate & "' "
                    End If
                End If
            End If
            If tdate <> "" Then
                If ra = "1" Then
                    If Filter <> "" Then
                        Filter += "and actfinish <= ''" & tdate & "'' "
                        FilterCNT += "and actfinish <= '" & tdate & "' "
                    Else
                        Filter = "actfinish <= ''" & tdate & "'' "
                        FilterCNT = "actfinish <= '" & tdate & "' "
                    End If
                End If
            End If

            'status
            If statret <> "" Then
                If Filter <> "" Then
                    Filter += " and w.status = ''" & statret & "''"
                    FilterCNT += " and w.status = '" & statret & "'"
                Else
                    Filter = " w.status = ''" & statret & "''"
                    FilterCNT = " w.status = '" & statret & "'"
                End If

            End If
            If woret <> "" Then
                If Filter <> "" Then
                    Filter += " and w.wonum = ''" & woret & "''"
                    FilterCNT += " and w.wonum = '" & woret & "'"
                Else
                    Filter = " w.wonum = ''" & woret & "''"
                    FilterCNT = " w.wonum = '" & woret & "'"
                End If

            End If

            If wpaid <> "" Then
                If Filter <> "" Then
                    Filter += " and w.wpaid = ''" & wpaid & "''"
                    FilterCNT += " and w.wpaid = '" & wpaid & "'"
                Else
                    Filter = " w.wpaid = ''" & wpaid & "''"
                    FilterCNT = " w.wpaid = '" & wpaid & "'"
                End If

            End If

            Dim wtrets1, wtrets2 As String
            If wtret <> "" Then
                Dim wtretarr() As String = wtret.Split(",")
                Dim wi As Integer
                For wi = 0 To wtretarr.Length - 1
                    If wtrets1 = "" Then
                        wtrets1 = "''" & wtretarr(wi) & "''"
                        wtrets2 = "'" & wtretarr(wi) & "'"
                    Else
                        wtrets1 += ",''" & wtretarr(wi) & "''"
                        wtrets2 += ",'" & wtretarr(wi) & "'"
                    End If
                Next
                If Filter <> "" Then
                    'Filter += " and w.worktype = ''" & wtret & "''"
                    'FilterCNT += " and w.worktype = '" & wtret & "'"
                    Filter += " and w.worktype in (" & wtrets1 & ")"
                    FilterCNT += " and w.worktype in (" & wtrets2 & ")"
                Else
                    'Filter = " w.worktype = ''" & wtret & "''"
                    'FilterCNT = " w.worktype = '" & wtret & "'"
                    Filter = " w.worktype in (" & wtrets1 & ")"
                    FilterCNT = " w.worktype in (" & wtrets2 & ")"
                End If

            End If

            If sup <> "" Then
                If Filter <> "" Then
                    Filter += "and w.superid = ''" & sup & "'' "
                    FilterCNT += "and w.superid = '" & sup & "' "
                Else
                    Filter += " w.superid = ''" & sup & "'' "
                    FilterCNT += " w.superid = '" & sup & "' "
                End If
            End If

            If lead <> "" Then
                If Filter <> "" Then
                    Filter += "and w.leadcraftid = ''" & lead & "'' "
                    FilterCNT += "and w.leadcraftid = '" & lead & "' "
                Else
                    Filter += " w.leadcraftid = ''" & lead & "'' "
                    FilterCNT += " w.leadcraftid = '" & lead & "' "
                End If
            End If

            If sa = "0" Then
                If islabor = "1" Then
                    sql = "select userid from pmsysusers where uid = '" & usid & "'"
                    usid = mm.strScalar(sql)
                    'issched = "1" And 
                    If wtret <> "TPM" Then
                        If Filter <> "" Then
                            Filter += " and a.userid = ''" & usid & "''"
                            FilterCNT += " and a.userid = '" & usid & "'"
                        Else
                            Filter = " a.userid = ''" & usid & "''"
                            FilterCNT = " a.userid = '" & usid & "'"
                        End If

                    End If
                ElseIf isplanner = "1" Then

                ElseIf issuper = "1" Then

                End If
            End If

            If waid <> "" Then
                If wa = "1" Then
                    If Filter <> "" Then
                        Filter += " and w.waid = ''" & waid & "''"
                        FilterCNT += " and w.waid = '" & waid & "'"
                    Else
                        Filter = " w.waid = ''" & waid & "''"
                        FilterCNT = " w.waid = '" & waid & "'"
                    End If
                Else
                    If Filter <> "" Then
                        Filter += " and a.userid in (select userid from pmsysusers where waid = ''" & waid & "'')"
                        FilterCNT += " and a.userid in (select userid from pmsysusers where waid = '" & waid & "')"
                    Else
                        Filter = " a.userid in (select userid from pmsysusers where waid = ''" & waid & "'')"
                        FilterCNT = " a.userid in (select userid from pmsysusers where waid = '" & waid & "')"
                    End If
                End If


            End If
        Else
            typ = lbltyp.Value
            Filter = " worktype = ''" & typ & "''"
            FilterCNT = " worktype = '" & typ & "'"

        End If

        If Filter = "" Then
            Filter = " (Convert(char(10),w.targstartdate,101) < Convert(char(10),getDate(),101) " _
            + "or Convert(char(10),w.reportdate,101) < Convert(char(10),getDate(),101) " _
            + "or Convert(char(10),w.schedstart,101) < Convert(char(10),getDate(),101)) " _
            + " and w.status not in (''COMP'', ''CLOSE'') and w.siteid = ''" & sid & "''"
            FilterCNT = " (Convert(char(10),w.targstartdate,101) < Convert(char(10),getDate(),101) " _
            + "or Convert(char(10),w.reportdate,101) < Convert(char(10),getDate(),101) " _
            + "or Convert(char(10),w.schedstart,101) < Convert(char(10),getDate(),101)) " _
            + " and w.status not in ('COMP', 'CLOSE') and w.siteid = '" & sid & "'"
        Else
            'Filter += " and (Convert(char(10),w.targstartdate,101) < Convert(char(10),getDate(),101) " _
            '+ "or Convert(char(10),w.reportdate,101) < Convert(char(10),getDate(),101) " _
            '+ "or Convert(char(10),w.schedstart,101) < Convert(char(10),getDate(),101)) " _
            '+ " and w.status not in (''COMP'', ''CLOSE'') and w.siteid = ''" & sid & "''"
            'FilterCNT += " and (Convert(char(10),w.targstartdate,101) < Convert(char(10),getDate(),101) " _
            '+ "or Convert(char(10),w.reportdate,101) < Convert(char(10),getDate(),101) " _
            '+ "or Convert(char(10),w.schedstart,101) < Convert(char(10),getDate(),101)) " _
            '+ " and w.status not in ('COMP', 'CLOSE') and w.siteid = '" & sid & "'"
        End If


        Sort = "w.wonum asc"
        sql = "usp_getwolist_wo2_exp '1','2000','" & Filter & "','" & Sort & "','" & ws & "','" & we & "'"

        'sql = "select top 600 * from lawson_track order by sTransactionID desc"
        Dim ds As New DataSet
        ds = mm.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
        'lblret.Value = ""
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            BuildMyWR(PageNumber)
        Catch ex As Exception
            mm.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr178", "eqlookup.aspx.vb")

            mm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            BuildMyWR(PageNumber)
        Catch ex As Exception
            mm.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr179", "eqlookup.aspx.vb")

            mm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Function checkloc(ByVal lid As String, ByVal jl As String) As String
        Dim ret As String
        If jl = "0" Then
            sql = "usp_lookup_locs_eq '" & lid & "'"
        Else
            sql = "usp_lookup_locs '" & lid & "'"
        End If

        dr = mm.GetRdrData(sql)
        While dr.Read
            ret = dr.Item("retstring").ToString
        End While
        dr.Close()
        Return ret
    End Function
    Private Sub BuildMyWR(ByVal PageNumber As Integer)
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder
        Dim sbr As New StringBuilder
        Dim sb1r As New StringBuilder
        Dim field, srch As String
        Dim srchi As Integer
        sid = lblsid.Value
        Dim rt, rs, ra, sa, sup, lead, did, clid, lid, rettyp, level, lstring, jl, fpc As String
        Dim waid, wpaid, fdate, tdate, wtret, statret, eqid, fuid, coid, usid As String
        Dim filtret As String = lblfiltret.Value
        Dim woret As String
        Dim datret As String = "0"
        Dim wa As String = "1"
        If filtret <> "" Then
            Dim filtarr() As String = filtret.Split("~")
            waid = filtarr(0).ToString
            wpaid = filtarr(1).ToString
            fdate = filtarr(2).ToString
            tdate = filtarr(3).ToString
            wtret = filtarr(4).ToString
            statret = filtarr(5).ToString
            woret = filtarr(6).ToString

            rt = filtarr(9).ToString
            rs = filtarr(10).ToString
            ra = filtarr(11).ToString
            sa = filtarr(12).ToString
            wa = filtarr(13).ToString
            sup = filtarr(14).ToString
            lead = filtarr(15).ToString
            did = filtarr(16).ToString
            clid = filtarr(17).ToString
            lid = filtarr(18).ToString
            eqid = filtarr(19).ToString
            fuid = filtarr(20).ToString
            coid = filtarr(21).ToString
            rettyp = filtarr(22).ToString
            level = filtarr(23).ToString
            jl = filtarr(24).ToString
            fpc = filtarr(25).ToString
            If rt = "1" Or rs = "1" Or ra = "1" Then
                datret = "1"
            End If
            If eqid = "" Then
                If rettyp = "depts" Then
                    If Filter <> "" Then
                        Filter += " and w.deptid = ''" & did & "'' "
                        FilterCNT += " and w.deptid = '" & did & "' "
                    Else
                        Filter = "w.deptid = ''" & did & "'' "
                        FilterCNT = "w.deptid = '" & did & "' "
                    End If
                    If clid <> "" Then
                        If Filter <> "" Then
                            Filter += " and w.cellid = ''" & clid & "'' "
                            FilterCNT += " and w.cellid = '" & clid & "' "
                        Else
                            Filter = "w.cellid = ''" & clid & "'' "
                            FilterCNT = "w.cellid = '" & clid & "' "
                        End If
                    End If
                ElseIf rettyp = "locs" Then
                    If level <> 1 Then
                        lstring = checkloc(lid, jl)
                        If Filter <> "" Then
                            Filter += " and w.locid in (" & lstring & ") "
                            FilterCNT += " and w.locid in (" & lstring & ") "
                        Else
                            Filter = " w.locid in (" & lstring & ") "
                            FilterCNT = " w.locid in (" & lstring & ") "
                        End If
                    End If

                End If
            End If
            If fpc <> "" Then
                fpc = Replace(fpc, "'", Chr(180), , , vbTextCompare)
                If Filter <> "" Then
                    Filter += " and w.eqid in (select e.eqid from equipment e where e.fpc like ''%" & fpc & "%'') "
                    FilterCNT += " and w.eqid in (select e.eqid from equipment e where e.fpc like '%" & fpc & "%') "
                Else
                    Filter += " w.eqid in (select e.eqid from equipment e where e.fpc like ''%" & fpc & "%'') "
                    FilterCNT += " w.eqid in (select e.eqid from equipment e where e.fpc like '%" & fpc & "%') "
                End If
            End If
            'targ
            If fdate <> "" Then
                If rt = "1" Then
                    If Filter <> "" Then
                        Filter += "and targstartdate >= ''" & fdate & "'' "
                        FilterCNT += "and targstartdate >= '" & fdate & "' "
                    Else
                        Filter = "targstartdate >= ''" & fdate & "'' "
                        FilterCNT = "targstartdate >= '" & fdate & "' "
                    End If
                End If
            End If
            If tdate <> "" Then
                If rt = "1" And who <> "pm" Then
                    If Filter <> "" Then
                        Filter += "and targcompdate <= ''" & tdate & "'' "
                        FilterCNT += "and targcompdate <= '" & tdate & "' "
                    Else
                        Filter = "targcompdate <= ''" & tdate & "'' "
                        FilterCNT = "targcompdate <= '" & tdate & "' "
                    End If
                ElseIf rt = "1" And who = "pm" Then
                    If Filter <> "" Then
                        Filter += "and targstartdate <= ''" & tdate & "'' "
                        FilterCNT += "and targstartdate <= '" & tdate & "' "
                    Else
                        Filter = "targstartdate <= ''" & tdate & "'' "
                        FilterCNT = "targstartdate <= '" & tdate & "' "
                    End If
                End If
            End If

            'sched
            If fdate <> "" Then
                If rs = "1" Then
                    If Filter <> "" Then
                        Filter += "and schedstart >= ''" & fdate & "'' "
                        FilterCNT += "and schedstart >= '" & fdate & "' "
                    Else
                        Filter = "schedstart >= ''" & fdate & "'' "
                        FilterCNT = "schedstart >= '" & fdate & "' "
                    End If
                End If
            End If
            If tdate <> "" Then
                If rs = "1" And who <> "pm" Then
                    If Filter <> "" Then
                        Filter += "and schedfinish <= ''" & tdate & "'' "
                        FilterCNT += "and schedfinish <= '" & tdate & "' "
                    Else
                        Filter = "schedfinish <= ''" & tdate & "'' "
                        FilterCNT = "schedfinish <= '" & tdate & "' "
                    End If
                ElseIf rs = "1" And who = "pm" Then
                    If Filter <> "" Then
                        Filter += "and schedstart <= ''" & tdate & "'' "
                        FilterCNT += "and schedstart <= '" & tdate & "' "
                    Else
                        Filter = "schedstart <= ''" & tdate & "'' "
                        FilterCNT = "schedstart <= '" & tdate & "' "
                    End If
                End If
            End If

            'actual
            If fdate <> "" Then
                If ra = "1" And who <> "pm" Then
                    If Filter <> "" Then
                        Filter += "and actstart >= ''" & fdate & "'' "
                        FilterCNT += "and actstart >= '" & fdate & "' "
                    Else
                        Filter = "actstart >= ''" & fdate & "'' "
                        FilterCNT = "actstart >= '" & fdate & "' "
                    End If
                ElseIf ra = "1" And who = "pm" Then
                    If Filter <> "" Then
                        Filter += "and actfinish >= ''" & fdate & "'' "
                        FilterCNT += "and actfinish >= '" & fdate & "' "
                    Else
                        Filter = "actfinish >= ''" & fdate & "'' "
                        FilterCNT = "actfinish >= '" & fdate & "' "
                    End If
                End If
            End If
            If tdate <> "" Then
                If ra = "1" Then
                    If Filter <> "" Then
                        Filter += "and actfinish <= ''" & tdate & "'' "
                        FilterCNT += "and actfinish <= '" & tdate & "' "
                    Else
                        Filter = "actfinish <= ''" & tdate & "'' "
                        FilterCNT = "actfinish <= '" & tdate & "' "
                    End If
                End If
            End If

            'status
            If statret <> "" Then
                If Filter <> "" Then
                    Filter += " and w.status = ''" & statret & "''"
                    FilterCNT += " and w.status = '" & statret & "'"
                Else
                    Filter = " w.status = ''" & statret & "''"
                    FilterCNT = " w.status = '" & statret & "'"
                End If

            End If
            If woret <> "" Then
                If Filter <> "" Then
                    Filter += " and w.wonum = ''" & woret & "''"
                    FilterCNT += " and w.wonum = '" & woret & "'"
                Else
                    Filter = " w.wonum = ''" & woret & "''"
                    FilterCNT = " w.wonum = '" & woret & "'"
                End If

            End If

            If wpaid <> "" Then
                If Filter <> "" Then
                    Filter += " and w.wpaid = ''" & wpaid & "''"
                    FilterCNT += " and w.wpaid = '" & wpaid & "'"
                Else
                    Filter = " w.wpaid = ''" & wpaid & "''"
                    FilterCNT = " w.wpaid = '" & wpaid & "'"
                End If

            End If

            Dim wtrets1, wtrets2 As String
            If wtret <> "" Then
                Dim wtretarr() As String = wtret.Split(",")
                Dim wi As Integer
                For wi = 0 To wtretarr.Length - 1
                    If wtrets1 = "" Then
                        wtrets1 = "''" & wtretarr(wi) & "''"
                        wtrets2 = "'" & wtretarr(wi) & "'"
                    Else
                        wtrets1 += ",''" & wtretarr(wi) & "''"
                        wtrets2 += ",'" & wtretarr(wi) & "'"
                    End If
                Next
                If Filter <> "" Then
                    'Filter += " and w.worktype = ''" & wtret & "''"
                    'FilterCNT += " and w.worktype = '" & wtret & "'"
                    Filter += " and w.worktype in (" & wtrets1 & ")"
                    FilterCNT += " and w.worktype in (" & wtrets2 & ")"
                Else
                    'Filter = " w.worktype = ''" & wtret & "''"
                    'FilterCNT = " w.worktype = '" & wtret & "'"
                    Filter = " w.worktype in (" & wtrets1 & ")"
                    FilterCNT = " w.worktype in (" & wtrets2 & ")"
                End If

            End If

            If sup <> "" Then
                If Filter <> "" Then
                    Filter += "and w.superid = ''" & sup & "'' "
                    FilterCNT += "and w.superid = '" & sup & "' "
                Else
                    Filter += " w.superid = ''" & sup & "'' "
                    FilterCNT += " w.superid = '" & sup & "' "
                End If
            End If

            If lead <> "" Then
                If Filter <> "" Then
                    Filter += "and w.leadcraftid = ''" & lead & "'' "
                    FilterCNT += "and w.leadcraftid = '" & lead & "' "
                Else
                    Filter += " w.leadcraftid = ''" & lead & "'' "
                    FilterCNT += " w.leadcraftid = '" & lead & "' "
                End If
            End If

            If sa = "0" Then
                If islabor = "1" Then
                    sql = "select userid from pmsysusers where uid = '" & usid & "'"
                    usid = mm.strScalar(sql)
                    'issched = "1" And 
                    If wtret <> "TPM" Then
                        If Filter <> "" Then
                            Filter += " and a.userid = ''" & usid & "''"
                            FilterCNT += " and a.userid = '" & usid & "'"
                        Else
                            Filter = " a.userid = ''" & usid & "''"
                            FilterCNT = " a.userid = '" & usid & "'"
                        End If

                    End If
                ElseIf isplanner = "1" Then

                ElseIf issuper = "1" Then

                End If
            End If

            If waid <> "" Then
                If wa = "1" Then
                    If Filter <> "" Then
                        Filter += " and w.waid = ''" & waid & "''"
                        FilterCNT += " and w.waid = '" & waid & "'"
                    Else
                        Filter = " w.waid = ''" & waid & "''"
                        FilterCNT = " w.waid = '" & waid & "'"
                    End If
                Else
                    If Filter <> "" Then
                        Filter += " and a.userid in (select userid from pmsysusers where waid = ''" & waid & "'')"
                        FilterCNT += " and a.userid in (select userid from pmsysusers where waid = '" & waid & "')"
                    Else
                        Filter = " a.userid in (select userid from pmsysusers where waid = ''" & waid & "'')"
                        FilterCNT = " a.userid in (select userid from pmsysusers where waid = '" & waid & "')"
                    End If
                End If


            End If
        Else
            typ = lbltyp.Value
            Filter = " worktype = ''" & typ & "''"
            FilterCNT = " worktype = '" & typ & "'"

        End If

        If Filter = "" Then
            Filter = " (Convert(char(10),w.targstartdate,101) < Convert(char(10),getDate(),101) " _
            + "or Convert(char(10),w.reportdate,101) < Convert(char(10),getDate(),101) " _
            + "or Convert(char(10),w.schedstart,101) < Convert(char(10),getDate(),101)) " _
            + " and w.status not in (''COMP'', ''CLOSE'') and w.siteid = ''" & sid & "''"
            FilterCNT = " (Convert(char(10),w.targstartdate,101) < Convert(char(10),getDate(),101) " _
            + "or Convert(char(10),w.reportdate,101) < Convert(char(10),getDate(),101) " _
            + "or Convert(char(10),w.schedstart,101) < Convert(char(10),getDate(),101)) " _
            + " and w.status not in ('COMP', 'CLOSE') and w.siteid = '" & sid & "'"
        Else
            'Filter += " and (Convert(char(10),w.targstartdate,101) < Convert(char(10),getDate(),101) " _
            '+ "or Convert(char(10),w.reportdate,101) < Convert(char(10),getDate(),101) " _
            '+ "or Convert(char(10),w.schedstart,101) < Convert(char(10),getDate(),101)) " _
            '+ " and w.status not in (''COMP'', ''CLOSE'') and w.siteid = ''" & sid & "''"
            'FilterCNT += " and (Convert(char(10),w.targstartdate,101) < Convert(char(10),getDate(),101) " _
            '+ "or Convert(char(10),w.reportdate,101) < Convert(char(10),getDate(),101) " _
            '+ "or Convert(char(10),w.schedstart,101) < Convert(char(10),getDate(),101)) " _
            '+ " and w.status not in ('COMP', 'CLOSE') and w.siteid = '" & sid & "'"
        End If


        Dim intPgNav, intPgCnt As Integer
        'sql = "SELECT Count(distinct wonum) FROM workorder w where " & FilterCNT
        sql = "SELECT Count(distinct w.wonum) FROM workorder w left join woassign a on a.wonum = w.wonum where " & FilterCNT
        intPgCnt = mm.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr588", "wolist.aspx.vb")

            mm.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
        End If
        PageSize = "50"
        intPgNav = mm.PageCountRev(intPgCnt, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If

        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        Sort = "w.wonum asc"

        Dim bg As String
        bg = "thdrsinggtrans1 ptransrow"
        Dim cr, crl As String
        cr = "plainlabelwt"
        Dim bxbg, bxbgy As String
        bxbg = "thdrplain"
        bxbgy = "thdrplainbgy"


        sb1.Append("<table cellspacing=""2"" cellpadding=""2"" width=""1220"" border=""0"" id=""scrollmenul"">" & vbCrLf)
        Dim wkid As String = lblwkid.Value
        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td colspan=""4"" rowspan=""3"" valign=""top"">")
        sb1.Append("<table cellspacing=""2"" border=""0"">")
        sb1.Append("<tr>")
        sb1.Append("<td class=""bigblue"" width=""220"" rowspan=""2"" valign=""top"">Work Order Backlog</td>")
        sb1.Append("<td class=""label"">Filter</td>")
        'sb1.Append("<td class=""plainlabel"" id=""tdwt"">" & typ & "</td>")
        'sb1.Append("<td><img src=""../images/appbuttons/minibuttons/magnifier.gif"" onclick=""getwt();""></td>")
        sb1.Append("<td class=""plainlabel"" id=""tdwt""><img src=""../images/appbuttons/minibuttons/magnifier.gif"" onclick=""getfilt();""></td>")
        sb1.Append("<td><IMG onmouseover=""return overlib('Export Results to Excel', ABOVE, LEFT)"" onclick=""excel();"" onmouseout=""return nd()"" src=""../images/appbuttons/minibuttons/excelexp.gif""></td>")

        sb1.Append("</tr>")
        sb1.Append("</table>")
        sb1.Append("</td>")
        sb1.Append("<td></td>")
        sb1.Append("<td></td>")
        sb1.Append("<td valign=""top"" class=""A1"" align=""right""><a href=""#"" class=""A1"" onclick=""goback();"">Return</a></td>")
        sb1.Append("<td class=""thdrsinggtrans plainlabel"">&nbsp;</td>")
        sb1.Append("</tr>" & vbCrLf)

        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td></td>")
        sb1.Append("<td></td>")
        sb1.Append("<td></td>")
        sb1.Append("<td class=""thdrplaintrans plainlabel"">&nbsp;</td>")
        sb1.Append("</tr>" & vbCrLf)

        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td></td>")
        sb1.Append("<td></td>")
        sb1.Append("<td></td>")
        sb1.Append("<td class=""thdrplaintrans plainlabel"">&nbsp;</td>")
        sb1.Append("</tr>" & vbCrLf)

        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""52"" height=""20"">WO#</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""70"">Status</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""140"">Equipment#</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""166"">Equipment Description</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""260"">Work Description</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""50"">Type</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""80"">Reported</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""80"">Target</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""40"">Hours</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""30"">Qty</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""80"">Start</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""40"">Hours</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""100"">Work Area</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""22""><img src=""../images/appbuttons/minibuttons/warningtrans.gif""></td>" & vbCrLf)
        'sb1.Append("<td class=""thdrsinggtrans plainlabel"" width=""20"">&nbsp;</td>")
        sb1.Append("</tr>" & vbCrLf)

        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""52"" >00000000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""70"">0000000000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""140"">00000000000000000000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""166"">00000000000000000000000000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""260"">000000000000000000000000000000000000000000000000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""50"" >00000000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""40"">00.00</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""30"">00</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""40"">00.00</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""100"">00000000000000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """ width=""22"">00</td>" & vbCrLf)
        'sb1.Append("<td class=""" & bg & " " & cr & """ width=""20"">&nbsp;</td>")
        sb1.Append("</tr>" & vbCrLf)

        'sb.Append("<table cellspacing=""2"" width=""1450"">" & vbCrLf)
        sb.Append("<table cellspacing=""2"" cellpadding=""2"" width=""1220"" border=""0"" id=""scrollmenulb"">" & vbCrLf)
        'sbr.Append("<table cellspacing=""2"" cellpadding=""2"" width=""1714"" border=""0"" id=""scrollmenurb"" >" & vbCrLf)


        'sb.Append("<tr>" & vbCrLf)
        'sb.Append("<td class=""" & bg & " " & cr & """ width=""52"" >00000000</td>" & vbCrLf)
        'sb.Append("<td class=""" & bg & " " & cr & """ width=""70"">0000000000</td>" & vbCrLf)
        'sb.Append("<td class=""" & bg & " " & cr & """ width=""140"">00000000000000000000</td>" & vbCrLf)
        'sb.Append("<td class=""" & bg & " " & cr & """ width=""166"">00000000000000000000000000</td>" & vbCrLf)
        'sb.Append("<td class=""" & bg & " " & cr & """ width=""300"">000000000000000000000000000000000000000000000000</td>" & vbCrLf)
        'sb.Append("<td class=""" & bg & " " & cr & """ width=""50"" >00000000</td>" & vbCrLf)
        'sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
        'sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
        'sb.Append("<td class=""" & bg & " " & cr & """ width=""40"">00.00</td>" & vbCrLf)
        'sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
        'sb.Append("<td class=""" & bg & " " & cr & """ width=""40"">00.00</td>" & vbCrLf)
        'sb.Append("<td class=""" & bg & " " & cr & """ width=""100"">00000000000000</td>" & vbCrLf)
        'sb.Append("<td class=""" & bg & " " & cr & """ width=""22"">00</td>" & vbCrLf)
        'sb.Append("<td class=""" & bg & " " & cr & """ width=""20"">&nbsp;</td>")
        'sb.Append("</tr>" & vbCrLf)


        Dim wt, wo, stat, isdown, desc, ss, sc, eq, ts, hrs, ttime, qty As String
        Dim w11, w11h, w12, w12h, w13, w13h, w14, w14h As String
        Dim w21, w21h, w22, w22h, w23, w23h, w24, w24h As String
        Dim w31, w31h, w32, w32h, w33, w33h, w34, w34h As String
        Dim w41, w41h, w42, w42h, w43, w43h, w44, w44h As String
        Dim w51, w51h, w52, w52h, w53, w53h, w54, w54h As String
        Dim w61, w61h, w62, w62h, w63, w63h, w64, w64h As String
        Dim w71, w71h, w72, w72h, w73, w73h, w74, w74h As String
        Dim rowflag As Integer = 0
        Dim sbcnt As Integer = 0
        'If typ = "PM" Then
        'sql = "usp_getwolist_pm '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "','" & ws & "','" & we & "'"
        'Else
        'If intPgCnt > 0 Then
        sql = "usp_getwolist_wo2 '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "','" & ws & "','" & we & "'"
        'End If
        Dim eqdesc, workarea, wopriority, reportdate As String

        dr = mm.GetRdrData(sql)
        While dr.Read
            sbcnt += 1
            If rowflag = 0 Then
                bxbg = "thdrplain"
                bxbgy = "thdrplainbgy"
                bg = "thdrsinggtrans1 ptransrow"
                rowflag = 1
            Else
                bxbg = "thdrplainbg"
                bxbgy = "thdrplainbg"
                bg = "thdrsinggtrans1 ptransrowblue"
                rowflag = "0"
            End If
            isdown = "0" 'dr.Item("isdown").ToString

            If isdown = "1" Then
                cr = "plainlabelred"
                crl = "A1R"
            Else
                cr = "plainlabel"
                crl = "A1U"
            End If
            wt = dr.Item("worktype").ToString
            wo = dr.Item("wonum").ToString
            eq = dr.Item("eqnum").ToString
            stat = dr.Item("status").ToString
            desc = dr.Item("description").ToString
            ss = dr.Item("schedstart").ToString
            ts = dr.Item("targstartdate").ToString
            sc = dr.Item("schedfinish").ToString
            hrs = dr.Item("hrs").ToString
            ttime = dr.Item("ttime").ToString
            qty = dr.Item("qty").ToString

            eqdesc = dr.Item("eqdesc").ToString
            workarea = dr.Item("workarea").ToString
            wopriority = dr.Item("wopriority").ToString
            reportdate = dr.Item("reportdate").ToString
            Dim hrsd As Decimal
            Dim hrsi As Integer
            If Len(hrs) > 4 Then
                Try
                    hrsd = System.Convert.ToDecimal(hrsd)
                    hrsd = hrsd.Round(hrsd, 0)
                    hrsi = System.Convert.ToInt32(hrsd)
                    hrs = hrsi
                Catch ex As Exception
                    hrs = "#"
                End Try
            End If

            Dim ttimed As Decimal
            Dim ttimei As Integer

            If Len(ttime) > 4 Then
                Try
                    ttimed = System.Convert.ToDecimal(ttimed)
                    ttimed = ttimed.Round(ttimed, 0)
                    ttimei = System.Convert.ToInt32(ttimed)
                    ttime = ttimei
                Catch ex As Exception
                    ttime = "#"
                End Try
            End If

            stat = RTrim(stat)
            If Len(stat) > 10 Then
                stat = stat.Substring(0, 9)
            End If
            workarea = RTrim(workarea)
            If Len(workarea) > 10 Then
                workarea = workarea.Substring(0, 9)
            End If
            wt = RTrim(wt)
            If Len(wt) > 8 Then
                wt = wt.Substring(0, 7)
            End If
            desc = RTrim(desc)
            If Len(desc) > 50 Then
                desc = desc.Substring(0, 49)
            End If
            ss = RTrim(ss)
            ts = RTrim(ss)
            ts = RTrim(ss)
            reportdate = RTrim(reportdate)
            If Len(eq) > 20 Then
                eq = eq.Substring(1, 19)
            End If
            eqdesc = RTrim(eqdesc)
            If Len(eqdesc) > 26 Then
                eqdesc = eqdesc.Substring(0, 25)
            End If

            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""" & bg & """ width=""52""><a class=""" & crl & """ href=""#"" onclick=""gotoreq('" & wo & "')"">" & wo & "</a></td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""70"">" & stat & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""140"">" & eq & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""166"">" & eqdesc & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""260"">" & desc & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""50"">" & wt & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">" & reportdate & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">" & ts & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""40"">" & ttime & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""30"">" & qty & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">" & ss & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""40"">" & hrs & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""100"">" & workarea & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ width=""22"">" & wopriority & "</td>" & vbCrLf)
            'sb.Append("<td class=""thdrplaintrans plainlabel"">&nbsp;</td>")
            sb.Append("</tr>" & vbCrLf)

            'sb1.Append("<tr>" & vbCrLf)
            'sb1.Append("<td class=""" & bg & """><a class=""" & crl & """ href=""#"" onclick=""gotoreq('" & wo & "')"">" & wo & "</a></td>" & vbCrLf)
            'sb1.Append("<td class=""" & bg & " " & cr & """>" & stat & "</td>" & vbCrLf)
            'sb1.Append("<td class=""" & bg & " " & cr & """>" & desc & "</td>" & vbCrLf)
            'sb1.Append("<td class=""" & bg & " " & cr & """>" & ts & "</td>" & vbCrLf)
            'sb1.Append("<td class=""" & bg & " " & cr & """>" & ttime & "</td>" & vbCrLf)
            'sb1.Append("<td class=""" & bg & " " & cr & """>" & ss & "</td>" & vbCrLf)
            'sb1.Append("<td class=""" & bg & " " & cr & """>" & hrs & "</td>" & vbCrLf)
            'sb1.Append("<td class=""thdrplaintrans plainlabel"">&nbsp;</td>")
            'sb1.Append("</tr>" & vbCrLf)



        End While
        dr.Close()
        'End If




        bg = "thdrsinggtrans1 ptransrow"
        cr = "plainlabelwht"

        Dim sbi As Integer
        If sbcnt < 15 Then
            For sbi = 1 To 20
                If rowflag = 0 Then
                    bxbg = "thdrplain"
                    bxbgy = "thdrplainbgy"
                    bg = "thdrsinggtrans1 ptransrow"
                    rowflag = 1
                Else
                    bxbg = "thdrplainbg"
                    bxbgy = "thdrplainbg"
                    bg = "thdrsinggtrans1 ptransrowblue"
                    rowflag = "0"
                End If
                sb.Append("<tr>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""52"" >00000000</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""70"">0000000000</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""140"">00000000000000000000</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""166"">00000000000000000000000000</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""260"">000000000000000000000000000000000000000000000000</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""50"" >00000000</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""40"">00.00</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""30"">00</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""40"">00.00</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""100"">00000000000000</td>" & vbCrLf)
                sb.Append("<td class=""" & bg & " " & cr & """ width=""22"">00</td>" & vbCrLf)
                'sb.Append("<td class=""" & bg & " " & cr & """ width=""20"">&nbsp;</td>")
                sb.Append("</tr>" & vbCrLf)



            Next
        End If
        'pm list
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""52"" height=""20"">WO#</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""70"">Status</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""140"">Equipment#</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""166"">Equipment Description</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""260"">Description</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""50"">Type</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""80"">Reported</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""80"">Target</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""40"">Hours</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""30"">Qty</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""80"">Start</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""40"">Hours</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""100"">Work Area</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""22""><img src=""../images/appbuttons/minibuttons/warningtrans.gif""></td>" & vbCrLf)
        'sb.Append("<td class=""thdrsinggtrans plainlabel"" width=""20"">&nbsp;</td>")
        sb.Append("</tr>" & vbCrLf)

        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""52"" >00000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""70"">0000000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""140"">00000000000000000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""166"">00000000000000000000000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""260"">000000000000000000000000000000000000000000000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""50"" >00000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""40"">00.00</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""30"">00</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""80"">00/00/0000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""40"">00.00</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""100"">00000000000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """ width=""22"">00</td>" & vbCrLf)
        'sb.Append("<td class=""" & bg & " " & cr & """ width=""20"">&nbsp;</td>")
        sb.Append("</tr>" & vbCrLf)



        sb1.Append("</table>")
        divhdr.InnerHtml = sb1.ToString
        sb.Append("</table>")
        divywr.InnerHtml = sb.ToString

    End Sub
End Class
