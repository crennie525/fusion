<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wojpfm.aspx.vb" Inherits="lucy_r12.wojpfm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wojpfm</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/wojpfmaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="scrolltop();">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" cellspacing="0" cellpadding="2" width="700">
        <tbody>
            <tr>
                <td>
                    <table width="700">
                        <tr height="26">
                            <td class="label" width="80">
                                <asp:Label ID="lang1477" runat="server">Job Plan:</asp:Label>
                            </td>
                            <td class="plainlabel" id="tdjpn" width="120" runat="server">
                            </td>
                            <td class="plainlabel" id="tdjpd" width="410" colspan="6" runat="server">
                            </td>
                            <td class="label" align="right" width="250">
                            </td>
                        </tr>
                        <tr>
                            <td class="plainlabelred" id="tdnofail" colspan="9" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="22">
                <td class="thdrsing label">
                    <asp:Label ID="lang1478" runat="server">Job Plan Level Failure Modes</asp:Label>
                </td>
            </tr>
            <tbody>
                <tr>
                    <td>
                        <asp:DataList ID="dltasks" runat="server" CssClass="plainlabelblue">
                            <HeaderTemplate>
                                <table width="700">
                                    <tr>
                                        <td width="700">
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr id="trfunc" runat="server">
                                    <td>
                                        <asp:Label ID="A1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'
                                            CssClass="details">
                                        </asp:Label>
                                        <asp:Label ID="A2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fmcnt") %>'
                                            CssClass="details">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr id="trhdr" runat="server">
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="thdrsing plainlabel" width="60">
                                                    <asp:Label ID="lang1479" runat="server">Edit</asp:Label>
                                                </td>
                                                <td class="thdrsing plainlabel" width="60">
                                                    <asp:Label ID="lang1480" runat="server">Task#</asp:Label>
                                                </td>
                                                <td class="thdrsing plainlabel" width="580">
                                                    <asp:Label ID="lang1481" runat="server">Task Description</asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trtask" runat="server">
                                                <td>
                                                    <asp:ImageButton ID="imgeditfm" runat="server" ToolTip="Edit Record" CommandName="Edit"
                                                        ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"></asp:ImageButton>
                                                </td>
                                                <td class="plainlabel">
                                                    <asp:Label ID="lbltasknume" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'
                                                        CssClass="plainlabel">
                                                    </asp:Label>
                                                </td>
                                                <td class="plainlabel" id="tdtdesc" runat="server">
                                                    <%# DataBinder.Eval(Container, "DataItem.task") %>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="thdrsingg plainlabel" width="40">
                                                    <asp:Label ID="lang1482" runat="server">Pass/Fail</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="100">
                                                    FM1
                                                </td>
                                                <td class="thdrsingg plainlabel" width="40">
                                                    <asp:Label ID="lang1483" runat="server">Pass/Fail</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="100">
                                                    FM2
                                                </td>
                                                <td class="thdrsingg plainlabel" width="40">
                                                    <asp:Label ID="lang1484" runat="server">Pass/Fail</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="100">
                                                    FM3
                                                </td>
                                                <td class="thdrsingg plainlabel" width="40">
                                                    <asp:Label ID="lang1485" runat="server">Pass/Fail</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="100">
                                                    FM4
                                                </td>
                                                <td class="thdrsingg plainlabel" width="40">
                                                    <asp:Label ID="lang1486" runat="server">Pass/Fail</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="100">
                                                    FM5
                                                </td>
                                            </tr>
                                            <tr id="trfm" runat="server">
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.fm1dd") %>
                                                </td>
                                                <td class="plainlabel" id="tdfm1" runat="Server">
                                                    <%# DataBinder.Eval(Container, "DataItem.fm1s") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.fm2dd") %>
                                                </td>
                                                <td class="plainlabel" id="tdfm2" runat="Server">
                                                    <%# DataBinder.Eval(Container, "DataItem.fm2s") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.fm3dd") %>
                                                </td>
                                                <td class="plainlabel" id="tdfm3" runat="Server">
                                                    <%# DataBinder.Eval(Container, "DataItem.fm3s") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.fm4dd") %>
                                                </td>
                                                <td class="plainlabel" id="tdfm4" runat="Server">
                                                    <%# DataBinder.Eval(Container, "DataItem.fm4s") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.fm5dd") %>
                                                </td>
                                                <td class="plainlabel" id="tdfm5" runat="Server">
                                                    <%# DataBinder.Eval(Container, "DataItem.fm5s") %>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <tr id="Tr2" runat="server">
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="thdrsing plainlabel" width="60">
                                                    <asp:Label ID="lang1487" runat="server">Edit</asp:Label>
                                                </td>
                                                <td class="thdrsing plainlabel" width="60">
                                                    <asp:Label ID="lang1488" runat="server">Task#</asp:Label>
                                                </td>
                                                <td class="thdrsing plainlabel" width="580">
                                                    <asp:Label ID="lang1489" runat="server">Task Description</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="Imagebutton2" runat="server" ToolTip="Save Changes" CommandName="Update"
                                                        ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:ImageButton>
                                                    <asp:ImageButton ID="Imagebutton3" runat="server" ToolTip="Cancel Changes" CommandName="Cancel"
                                                        ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"></asp:ImageButton>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.tasknum") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.task") %>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="thdrsingg plainlabel" width="40">
                                                    <asp:Label ID="lang1490" runat="server">Pass/Fail</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="100">
                                                    FM1
                                                </td>
                                                <td class="thdrsingg plainlabel" width="40">
                                                    <asp:Label ID="lang1491" runat="server">Pass/Fail</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="100">
                                                    FM2
                                                </td>
                                                <td class="thdrsingg plainlabel" width="40">
                                                    <asp:Label ID="lang1492" runat="server">Pass/Fail</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="100">
                                                    FM3
                                                </td>
                                                <td class="thdrsingg plainlabel" width="40">
                                                    <asp:Label ID="lang1493" runat="server">Pass/Fail</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="100">
                                                    FM4
                                                </td>
                                                <td class="thdrsingg plainlabel" width="40">
                                                    <asp:Label ID="lang1494" runat="server">Pass/Fail</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="100">
                                                    FM5
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="plainlabel">
                                                    <asp:RadioButtonList ID="rb1" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm1dd")) %>'>
                                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.fm1s") %>
                                                </td>
                                                <td class="plainlabel" bgcolor="#E7F1FD">
                                                    <asp:RadioButtonList ID="rb2" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm2dd")) %>'>
                                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td class="plainlabel" bgcolor="#E7F1FD">
                                                    <%# DataBinder.Eval(Container, "DataItem.fm2s") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <asp:RadioButtonList ID="rb3" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm3dd")) %>'>
                                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.fm3s") %>
                                                </td>
                                                <td class="plainlabel" bgcolor="#E7F1FD">
                                                    <asp:RadioButtonList ID="rb4" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm4dd")) %>'>
                                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td class="plainlabel" bgcolor="#E7F1FD">
                                                    <%# DataBinder.Eval(Container, "DataItem.fm4s") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <asp:RadioButtonList ID="rb5" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("fm5dd")) %>'>
                                                        <asp:ListItem Value="1">Pass</asp:ListItem>
                                                        <asp:ListItem Value="2">Fail</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.fm5s") %>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </EditItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:DataList>
                    </td>
                </tr>
                <tr height="22">
                    <td class="thdrsing label">
                        <asp:Label ID="lang1495" runat="server">Job Plan Level Measurements</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DataList ID="dgmeas" runat="server">
                            <HeaderTemplate>
                                <table width="700">
                                    <tr>
                                        <td width="700">
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr id="Tr1" runat="server">
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'
                                            CssClass="details">
                                        </asp:Label>
                                        <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tmdid") %>'
                                            CssClass="details">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr id="Tr3" runat="server">
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="thdrsing plainlabel" width="60">
                                                    <asp:Label ID="lang1496" runat="server">Edit</asp:Label>
                                                </td>
                                                <td class="thdrsing plainlabel" width="60">
                                                    <asp:Label ID="lang1497" runat="server">Task#</asp:Label>
                                                </td>
                                                <td class="thdrsing plainlabel" width="580">
                                                    <asp:Label ID="lang1498" runat="server">Task Description</asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="Tr4" runat="server">
                                                <td>
                                                    <asp:ImageButton ID="imgeditm" runat="server" ToolTip="Edit Record" CommandName="Edit"
                                                        ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"></asp:ImageButton>
                                                </td>
                                                <td class="plainlabel">
                                                    <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'
                                                        CssClass="plainlabel">
                                                    </asp:Label>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.taskdesc") %>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td width="60">
                                                    &nbsp;
                                                </td>
                                                <td class="thdrsingg plainlabel" width="120">
                                                    <asp:Label ID="lang1499" runat="server">Type</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="50">
                                                    <asp:Label ID="lang1500" runat="server">Hi</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="50">
                                                    <asp:Label ID="lang1501" runat="server">Lo</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="50">
                                                    <asp:Label ID="lang1502" runat="server">Spec</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="100">
                                                    <asp:Label ID="lang1503" runat="server">Measurement</asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="Tr5" runat="server">
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.type") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.hi") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.lo") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.spec") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.womeas") %>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <tr id="Tr8" runat="server">
                                    <td>
                                        <asp:Label ID="lblpmtskida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'
                                            CssClass="details">
                                        </asp:Label>
                                        <asp:Label ID="lbltmdida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tmdid") %>'
                                            CssClass="details">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr id="Tr6" runat="server">
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="thdrsing plainlabel" width="60">
                                                    <asp:Label ID="lang1504" runat="server">Edit</asp:Label>
                                                </td>
                                                <td class="thdrsing plainlabel" width="60">
                                                    <asp:Label ID="lang1505" runat="server">Task#</asp:Label>
                                                </td>
                                                <td class="thdrsing plainlabel" width="550">
                                                    <asp:Label ID="lang1506" runat="server">Task Description</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="Imagebutton5" runat="server" ToolTip="Save Changes" CommandName="Update"
                                                        ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:ImageButton>
                                                    <asp:ImageButton ID="Imagebutton6" runat="server" ToolTip="Cancel Changes" CommandName="Cancel"
                                                        ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"></asp:ImageButton>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.tasknum") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.taskdesc") %>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td width="60">
                                                    &nbsp;
                                                </td>
                                                <td class="thdrsingg plainlabel" width="120">
                                                    <asp:Label ID="lang1507" runat="server">Type</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="50">
                                                    <asp:Label ID="lang1508" runat="server">Hi</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="50">
                                                    <asp:Label ID="lang1509" runat="server">Lo</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="50">
                                                    <asp:Label ID="lang1510" runat="server">Spec</asp:Label>
                                                </td>
                                                <td class="thdrsingg plainlabel" width="100">
                                                    <asp:Label ID="lang1511" runat="server">Measurement</asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="Tr7" runat="server">
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.type") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.hi") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.lo") %>
                                                </td>
                                                <td class="plainlabel">
                                                    <%# DataBinder.Eval(Container, "DataItem.spec") %>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtmeas" runat="server" Width="90px" MaxLength="50" CssClass="plainlabel"
                                                        Text='<%# DataBinder.Eval(Container, "DataItem.womeas") %>'>
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </EditItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:DataList>
                    </td>
                </tr>
            </tbody>
    </table>
    <input id="lbljpid" type="hidden" name="lbljpid" runat="server">
    <input id="lblwo" type="hidden" name="lblwo" runat="server">
    <input id="lbltaskcnt" type="hidden" name="lbltaskcnt" runat="server">
    <input id="lbltaskcurrcnt" type="hidden" name="lbltaskcurrcnt" runat="server">
    <input id="lblrow" type="hidden" name="lblrow" runat="server">
    <input id="lblfmcnt" type="hidden" name="lblfmcnt" runat="server">
    <input id="lbltasknum" type="hidden" name="lbltasknum" runat="server">
    <input id="lblcurrcnt" type="hidden" name="lblcurrcnt" runat="server">
    <input id="xCoord" type="hidden" name="xCoord" runat="server">
    <input id="yCoord" type="hidden" name="yCoord" runat="server"><input id="lblsubmit"
        type="hidden" name="lblsubmit" runat="server">
    <input id="lblfmstr" type="hidden" name="lblfmstr" runat="server">
    <input id="lblmstr" type="hidden" name="lblmstr" runat="server">
    <input id="lblmstrvals" type="hidden" name="lblmstrvals" runat="server">
    <input id="lblpstr" type="hidden" name="lblpstr" runat="server">
    <input id="lbltstr" type="hidden" name="lbltstr" runat="server">
    <input id="lbllstr" type="hidden" name="lbllstr" runat="server">
    <input id="lblmflg" type="hidden" name="lblmflg" runat="server"><input id="lblmcomp"
        type="hidden" name="lblmcomp" runat="server">
    <input type="hidden" id="lblstat" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
