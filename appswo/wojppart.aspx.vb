

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wojppart
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim comp As New Utilities
    Dim typ, pmtskid, wo As String
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdpart As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            typ = Request.QueryString("typ").ToString
            lbltyp.Value = typ
            pmtskid = Request.QueryString("pmtskid").ToString
            lblpmtskid.Value = pmtskid
            wo = Request.QueryString("wo").ToString
            lblwo.Value = wo
            comp.Open()
            GetPart(typ, pmtskid, wo)
            comp.Dispose()
        Else
            If Request.Form("lblsubmit") = "savepart" Then
                comp.Open()
                SavePart()
                comp.Dispose()
                lblsubmit.Value = "go"
            End If
        End If
    End Sub
    Private Sub SavePart()
        Dim i As Integer
        typ = lbltyp.Value
        If typ = "p" Then
            'Parts
            Dim pstr As String = lblpstr.Value
            Dim parr() As String = pstr.Split(",")
            Dim p, pu, pid As String
            For i = 0 To parr.Length - 1
                p = parr(i)
                Dim puarr() As String = p.Split("~")
                pu = puarr(1)
                pid = puarr(0)
                If pu = "no" Then
                    sql = "update wojpparts set used = 'N' where wotskpartid = '" & pid & "'"
                    comp.Update(sql)
                End If
            Next
        ElseIf typ = "t" Then
            'Tools
            Dim tstr As String = lbltstr.Value
            Dim tarr() As String = tstr.Split(",")
            Dim t, tu, tid As String
            For i = 0 To tarr.Length - 1
                t = tarr(i)
                Dim tuarr() As String = t.Split("~")
                tu = tuarr(1)
                tid = tuarr(0)
                If tu = "no" Then
                    sql = "update wojptool set used = 'N' where wotsktoolid = '" & tid & "'"
                    comp.Update(sql)
                End If
            Next
        ElseIf typ = "l" Then
            'Lubes
            Dim lstr As String = lbllstr.Value
            Dim larr() As String = lstr.Split(",")
            Dim l, lu, lid As String
            For i = 0 To larr.Length - 1
                l = larr(i)
                Dim luarr() As String = l.Split("~")
                lu = luarr(1)
                lid = luarr(0)
                If lu = "no" Then
                    sql = "update wojplubes set used = 'N' where wotsklubeid = '" & lid & "'"
                    comp.Update(sql)
                End If
            Next
        End If


       
    End Sub
    Private Sub GetPart(ByVal typ As String, ByVal pmtskid As String, ByVal wo As String)
        Dim start As Integer = 0
        Dim sb As New System.Text.StringBuilder

        If typ = "p" Then
            sql = "select distinct wotskpartid, itemnum, description from wojpparts where wonum = '" & wo & "' and pmtskid = '" & pmtskid & "'"
            dr = comp.GetRdrData(sql)
            Dim wpid, pnum, pdesc, pstr As String
            While dr.Read
                If start = 0 Then
                    start = 1
                    sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""440"">")
                    sb.Append("<tr><td colspan=""3"" class=""thdrsingg label"">" & tmod.getlbl("cdlbl206" , "wojppart.aspx.vb") & "</td></tr>")
                    sb.Append("<tr><td width=""120""></td><td width=""200""></td><td width=""120""></td></tr>")
                End If
                wpid = dr.Item("wopartid").ToString
                pnum = dr.Item("itemnum").ToString
                pdesc = dr.Item("description").ToString
                sb.Append("<tr><td class=""plainlabel"">" & pnum & "</td>")
                sb.Append("<td class=""plainlabel"">" & pdesc & "</td>")
                sb.Append("<td class=""plainlabel""><input type=""radio"" id=""" & wpid & "-yes"" name=""p-" & wpid & """ ")
                sb.Append("checked onclick=""checkwp('yes', this.name)"">Part Used&nbsp;")
                sb.Append("<input type=""radio"" id=""" & wpid & "-no"" name=""p-" & wpid & """ onclick=""checkwp('no', this.name)"">Part Not Used</td></tr>")
                If pstr = "" Then
                    pstr = wpid & "~yes"
                Else
                    pstr += "," & wpid & "~yes"
                End If
            End While
            dr.Close()
            lblpstr.Value = pstr
        ElseIf typ = "t" Then
            sql = "select distinct wotsktoolid, toolnum, description from wojptools where wonum = '" & wo & "' and pmtskid = '" & pmtskid & "'"
            dr = comp.GetRdrData(sql)
            Dim wtid, tnum, tdesc, tstr As String
            While dr.Read
                If start = 0 Then
                    start = 1
                    sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""440"">")
                    sb.Append("<tr><td colspan=""3"" class=""thdrsingg label"">" & tmod.getlbl("cdlbl207" , "wojppart.aspx.vb") & "</td></tr>")
                    sb.Append("<tr><td width=""120""></td><td width=""200""></td><td width=""120""></td></tr>")
                End If
                wtid = dr.Item("wotoolid").ToString
                tnum = dr.Item("toolnum").ToString
                tdesc = dr.Item("description").ToString
                sb.Append("<tr><td class=""plainlabel"">" & tnum & "</td>")
                sb.Append("<td class=""plainlabel"">" & tdesc & "</td>")
                sb.Append("<td class=""plainlabel""><input type=""radio"" id=""" & wtid & "-yes"" name=""t-" & wtid & """ ")
                sb.Append("checked onclick=""checkwt('yes', this.name)"">Tool Used&nbsp;")
                sb.Append("<input type=""radio"" id=""" & wtid & "-no"" name=""t-" & wtid & """ onclick=""checkwt('no', this.name)"">Tool Not Used</td></tr>")
                If tstr = "" Then
                    tstr = wtid & "~yes"
                Else
                    tstr += "," & wtid & "~yes"
                End If
            End While

            dr.Close()
            lbltstr.Value = tstr
        ElseIf typ = "l" Then
            sql = "select distinct wotsklubeid, lubenum, description from wojplubes where wonum = '" & wo & "' and pmtskid = '" & pmtskid & "'"
            dr = comp.GetRdrData(sql)
            Dim wlid, lnum, ldesc, lstr As String
            While dr.Read
                If start = 0 Then
                    start = 1
                    sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""440"">")
                    sb.Append("<tr><td colspan=""3"" class=""thdrsingg label"">" & tmod.getlbl("cdlbl208" , "wojppart.aspx.vb") & "</td></tr>")
                    sb.Append("<tr><td width=""120""></td><td width=""200""></td><td width=""120""></td></tr>")
                End If
                wlid = dr.Item("wolubeid").ToString
                lnum = dr.Item("lubenum").ToString
                ldesc = dr.Item("description").ToString
                sb.Append("<tr><td class=""plainlabel"">" & lnum & "</td>")
                sb.Append("<td class=""plainlabel"">" & ldesc & "</td>")
                sb.Append("<td class=""plainlabel""><input type=""radio"" id=""" & wlid & "-yes"" name=""l-" & wlid & """ ")
                sb.Append("checked onclick=""checkwl('yes', this.name)"">Lube Used&nbsp;")
                sb.Append("<input type=""radio"" id=""" & wlid & "-no"" name=""l-" & wlid & """ onclick=""checkwl('no', this.name)"">Lube Not Used</td></tr>")
                If lstr = "" Then
                    lstr = wlid & "~yes"
                Else
                    lstr += "," & wlid & "~yes"
                End If
            End While
            dr.Close()
            lbllstr.Value = lstr
        End If

        sb.Append("<tr><td colspan=""3"" align=""right"">")
        sb.Append("<IMG onclick=""savepart();""  src=""../images/appbuttons/minibuttons/savedisk1.gif""")
        sb.Append("onmouseover=""return overlib('" & tmod.getov("cov173" , "wojppart.aspx.vb") & "')"" onmouseout=""return nd()""></td></tr></table>")
        tdpart.InnerHtml = sb.ToString
    End Sub
End Class
