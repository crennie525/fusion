<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wopmlist7.aspx.vb" Inherits="lucy_r12.wopmlist7" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Weekly Planner</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        <!--
        function lookweek(wkid, wkof, sid) {
            var eReturn = window.showModalDialog("../labor/labweekscheddialog.aspx?wkid=" + wkid + "&wkof=" + wkof + "&sid=" + sid, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
            if (eReturn) {

            }
        }
        function getweeks(id) {
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/labweekselectdialog.aspx?sid=" + sid, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
            if (eReturn) {
                ////fd,ld,efd,eld, fd2, ld2, fd3, ld3, wkid, wkid2, wkid3, wkid4
                var ret = eReturn.split("~")
                document.getElementById("lblweekstart").value = ret[0];
                document.getElementById("lblweekend").value = ret[1];
                document.getElementById("lblwkid").value = ret[8];
                document.getElementById("lblsubmit").value = "newweek";
                document.getElementById("form1").submit();
            }
        }
        function getwt() {
            var wo = ""; //document.getElementById("lblwonum").value;
            var eReturn = window.showModalDialog("wostatusdialog.aspx?typ=wt&wo=" + wo, "", "dialogHeight:370px; dialogWidth:370px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    document.getElementById("tdwt").innerHTML = eReturn;
                    document.getElementById("lbltyp").value = eReturn;
                    document.getElementById("lblsubmit").value = "newweek";
                    document.getElementById("form1").submit();
                }
            }
        }
        function setsb1pos() {
            var intX = document.getElementById("divywrr").scrollLeft;
            document.getElementById("divhdrr").scrollLeft = intX;
            var intY = document.getElementById("divywrr").scrollTop;
            document.getElementById("divywr").scrollTop = intY;
        }
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }
        function goback() {
            var who = document.getElementById("lblwho").value;
            var wo = document.getElementById("lblwonum").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            if (who == "main") {
                window.location = "../mainmenu/NewMainMenu2.aspx?sid=" + sid + "&wo=" + wo + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
            }
            else if (who == "wo") {
                window.location = "wolabmain.aspx?sid=" + sid + "&wo=" + wo + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
            }
            else {
                window.location = "wolist1_fs.aspx?sid=" + sid + "&wo=" + wo + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
            }
        }
        function gotoreq(wo) {
            var who = document.getElementById("lblwho").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            window.location = "wolabmain.aspx?sid=" + sid + "&wo=" + wo + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        function getfilt() {
            var sid = document.getElementById("lblsid").value;
            var filt = document.getElementById("lblfiltret").value;
            var eReturn = window.showModalDialog("wosrchdialog.aspx?filt=" + filt + "&sid=" + sid + "&who=sch", "", "dialogHeight:550px; dialogWidth:650px; resizable=yes");
            if (eReturn) {
                //alert(eReturn)
                if (eReturn != "") {
                    document.getElementById("lblfiltret").value = eReturn;
                    document.getElementById("lblret").value = "filt"
                    document.getElementById("form1").submit();
                }
            }
        }
        //-->
    </script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 0px; left: 0px" cellspacing="0" cellpadding="0"
        width="1232">
        <tr>
            <td id="tdhdr" valign="top" runat="server">
                <div id="divhdr" class="schedlistlnghdrlb" runat="server">
                </div>
            </td>
            <td valign="top">
                <div id="divhdrr" class="schedlistlnghdr1b" runat="server">
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <div id="divywr" class="schedlistlngl" runat="server">
                </div>
            </td>
            <td id="sbr" valign="top">
                <div onscroll="setsb1pos();" id="divywrr" class="schedlistlng1" runat="server">
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <img src="../images/appbuttons/minibuttons/4PX.gif">
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                    border-right: blue 1px solid" cellspacing="0" cellpadding="0" width="300">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <iframe id="ifsession" class="details" src="../session.aspx?who=pln" frameborder="no"
        runat="server" style="z-index: 0"></iframe>
    <input id="txtpg" type="hidden" name="txtpg" runat="server">
    <input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
    <input id="lblfslang" type="hidden" name="lblfslang" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lbltyp" type="hidden" name="lbltyp" runat="server">
    <input id="lblweekstart" type="hidden" name="lblweekstart" runat="server">
    <input id="lblweekend" type="hidden" name="lblweekend" runat="server">
    <input id="lblsubmit" type="hidden" name="lblsubmit" runat="server">
    <input id="lblworktype" type="hidden" name="lblworktype" runat="server">
    <input id="lblxCoord_sbr" type="hidden" runat="server">
    <input id="lblyCoord_sbr" type="hidden" runat="server">
    <input type="hidden" id="lblret" runat="server">
    <input type="hidden" id="lblwkid" runat="server"><input type="hidden" id="lblwho"
        runat="server"><input type="hidden" id="lblwonum" runat="server" name="lblwonum">
    <input type="hidden" id="lblusername" runat="server" name="lblusername">
    <input type="hidden" id="lbluid" runat="server" name="lbluid">
    <input type="hidden" id="lblislabor" runat="server" name="lblislabor">
    <input type="hidden" id="lblissuper" runat="server" name="lblissuper">
    <input type="hidden" id="lblisplanner" runat="server" name="lblisplanner"><input
        type="hidden" id="lblLogged_In" runat="server" name="lblLogged_In">
    <input type="hidden" id="lblro" runat="server" name="lblro">
    <input type="hidden" id="lblms" runat="server" name="lblms">
    <input type="hidden" id="lblappstr" runat="server" name="lblappstr">
    <input type="hidden" id="lblfiltret" runat="server" />
    </form>
</body>
</html>
