

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class woarch
    Inherits System.Web.UI.Page
	Protected WithEvents lang1353 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1352 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim main As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim chk, did, clid, start, Login, sid, lid, rostr, ro As String
    Dim appstr As String
    Protected WithEvents tdarch As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents rb1 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rb2 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            If Request.QueryString("start").ToString = "yes" Then
                appstr = HttpContext.Current.Session("appstr").ToString()
                CheckApps(appstr)
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = main.CheckROS(rostr, "wo")
                    'lblro.Value = ro
                    If ro = "1" Then
                        rb2.Disabled = True
                    End If
                End If
                'sid = Request.QueryString("sid").ToString
                sid = HttpContext.Current.Session("dfltps").ToString
                lblsid.Value = sid
                main.Open()
                'GetArch()
                main.Dispose()
            Else
                tdarch.InnerHtml = "Waiting for location..."
            End If

        End If
    End Sub
    Private Sub CheckApps(ByVal appstr As String)
        Dim apparr() As String = appstr.Split(",")
        'eq,dev,opt,inv
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                Select Case apparr(i)
                    Case "wo"
                        rb2.Disabled = False
                End Select
            Next
            If rb1.Disabled = True Then
                rb2.Checked = True
            End If
        Else
            rb2.Disabled = False
        End If

    End Sub
    Private Sub GetArch()
        Dim sb As StringBuilder = New StringBuilder
        sid = lblsid.Value
        Dim eqnum, eqdesc As String
        sb.Append("<table cellspacing=""0"" border=""0""><tr>")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""135""></tr>" & vbCrLf)
        Dim comp As String = "0" 

        sql = "select distinct e.siteid, e.locid, e.dept_id, " _
        + "e.cellid, e.eqid, e.eqnum, isnull(e.eqdesc, 'No Description') as eqdesc, isnull(f.func_id, 0) as func_id, " _
        + "e.locked, e.lockedby, e.trans, e.transstatus, " _
        + "f.func, isnull(c.comid, 0) as comid, c.compnum, cnt = (select count(c.compnum) " _
        + "from components c where c.func_id = f.func_id), " _
         + "epcnt = (select count(*) from pmpictures p where p.eqid = e.eqid and p.funcid is null and p.comid is null), " _
        + "fpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid is null), " _
        + "cpcnt = (select count(*) from pmpictures p where p.funcid = f.func_id and p.comid = c.comid) " _
        + "from equipment e left outer join functions f on f.eqid = e.eqid " _
        + "left outer join components c on c.func_id = f.func_id " _
        + "where e.siteid = '" & sid & "'"

        dr = main.GetRdrData(sql)
        '*** Multi Add ***
        Dim trans As String = "0"
        Dim tstat As String = "0"
        '*** End Multi Add ***
        Dim locby As String
        Dim lock As String = "0"
        Dim fid As String = "0"
        Dim cid As String = "0"
        Dim eid As String = "0"
        Dim cidhold As Integer = 0

        Dim epcnt As Integer = 0
        Dim fpcnt As Integer = 0
        Dim cpcnt As Integer = 0

        Dim cnt As Integer = 0
        While dr.Read
            If dr.Item("eqid") <> eid Then
                If eid <> 0 Then
                    sb.Append("</table></td></tr>")
                End If
                eid = dr.Item("eqid").ToString
                sid = dr.Item("siteid").ToString
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                lid = dr.Item("locid").ToString
                epcnt = dr.Item("epcnt").ToString
                If clid <> "" Then
                    chk = "yes"
                Else
                    chk = "no"
                End If
                eqnum = dr.Item("eqnum").ToString
                eqdesc = dr.Item("eqdesc").ToString
                lock = dr.Item("locked").ToString
                locby = dr.Item("lockedby").ToString
                sb.Append("<tr><td><img id='i" + eid + "' ")
                sb.Append("onclick=""fclose('t" + eid + "', 'i" + eid + "');""")
                sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>")
                If epcnt <> 0 Then
                    sb.Append("<td><img src=""../images/appbuttons/minibuttons/gridpic.gif"" onclick=""geteqport('" + eid + "')""></td>" & vbCrLf)
                Else
                    sb.Append("<td><img src=""../images/appbuttons/minibuttons/gridpicdis.gif""></td>" & vbCrLf)
                End If
                sb.Append("<td colspan=""2"" class=""plainlabel""><a href=""#"" onclick=""gotoeq('" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & lid & "')"" class=""linklabel"">" & eqnum & "</a> - " & eqdesc)
                '*** Multi Add ***
                trans = dr.Item("trans").ToString
                If trans = "0" OrElse Len(trans) = 0 Then
                    'sb.Append("</td></tr>" & vbCrLf)
                    'If lock = "0" OrElse Len(lock) = 0 Then
                    sb.Append("</td></tr>" & vbCrLf)
                    'Else
                    'sb.Append("&nbsp;<img src='../images/appbuttons/minibuttons/lillock.gif' ")
                    'sb.Append("onmouseover=""return overlib('" & tmod.getov("cov170" , "woarch.aspx.vb") & ": " &  locby & "')"" ")
                    'sb.Append("onmouseout=""return nd()""></td></tr>" & vbCrLf)
                    'End If
                Else
                    tstat = dr.Item("transstatus").ToString
                    If tstat <> "4" Then
                        sb.Append("&nbsp;<img src='../images/appbuttons/minibuttons/warning.gif' ")
                        sb.Append("onmouseover=""return overlib('" & tmod.getov("cov171" , "woarch.aspx.vb") & "')"" ")
                        sb.Append("onmouseout=""return nd()""></td></tr>" & vbCrLf)
                    End If

                End If
                '*** End Multi Add ***
                'If lock = "0" OrElse Len(lock) = 0 Then
                'sb.Append("</td></tr>" & vbCrLf)
                'Else
                'sb.Append("&nbsp;<img src='../images/appbuttons/minibuttons/lillock.gif' ")
                'sb.Append("onmouseover=""return overlib('" & tmod.getov("cov172" , "woarch.aspx.vb") & ": " &  locby & "')"" ")
                'sb.Append("onmouseout=""return nd()""></td></tr>" & vbCrLf)
                'End If

                sb.Append("<tr><td></td><td colspan=""3""><table class=""details"" cellspacing=""0"" id='t" + eid + "' border=""0"">")
                sb.Append("<tr><td width==""15""></td><td width==""15""></td><td width==""135""></td></tr>")
            End If


            If dr.Item("func_id").ToString <> fid Then
                If dr.Item("func_id").ToString <> "0" Then
                    eid = dr.Item("eqid").ToString
                    fid = dr.Item("func_id").ToString
                    'sid = dr.Item("siteid").ToString
                    'did = dr.Item("dept_id").ToString
                    'clid = dr.Item("cellid").ToString
                    cidhold = dr.Item("cnt").ToString
                    fpcnt = dr.Item("fpcnt").ToString
                    sb.Append("<tr>" & vbCrLf & "<td colspan=""2""><table cellspacing=""0"" border=""0"">" & vbCrLf)
                    sb.Append("<tr><td><img id='i" + fid + "' onclick=""fclose('t" + fid + "', 'i" + fid + "');"" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                    If fpcnt <> 0 Then
                        sb.Append("<td><img src=""../images/appbuttons/minibuttons/gridpic.gif"" onclick=""getfuport('" + eid + "','" + fid + "')""></td>" & vbCrLf)
                    Else
                        sb.Append("<td><img src=""../images/appbuttons/minibuttons/gridpicdis.gif""></td>" & vbCrLf)
                    End If
                    sb.Append("<td><a href=""#"" onclick=""gotofu('" & fid & "', '" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & lid & "')"" class=""linklabelblk"">" & dr.Item("func").ToString & "</a></td></tr></table></td></tr>" & vbCrLf)
                    If dr.Item("comid").ToString <> cid Then
                        If dr.Item("comid").ToString <> "0" Then
                            cid = dr.Item("comid").ToString
                            eid = dr.Item("eqid").ToString
                            fid = dr.Item("func_id").ToString
                            'sid = dr.Item("siteid").ToString
                            'did = dr.Item("dept_id").ToString
                            'clid = dr.Item("cellid").ToString
                            cpcnt = dr.Item("cpcnt").ToString
                            If cnt = 0 Then
                                cnt = cnt + 1
                                sb.Append("<tr><td width=""15""></td>" & vbCrLf & "<td width=""135""><table border=""0"" class=""details"" cellspacing=""0"" id=""t" + fid + """>" & vbCrLf)
                                If cpcnt <> 0 Then
                                    sb.Append("<tr><td width=""15""><img src=""../images/appbuttons/minibuttons/gridpic.gif"" onclick=""getcoport('" + eid + "','" + fid + "','" + cid + "')""></td>" & vbCrLf)
                                Else
                                    sb.Append("<tr><td width=""15""><img src=""../images/appbuttons/minibuttons/gridpicdis.gif""></td>" & vbCrLf)
                                End If
                                sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & cid & "', '" & fid & "', '" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & lid & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                                If cnt = cidhold Then
                                    cnt = 0
                                    sb.Append("</table></td></tr>")
                                End If
                            End If
                        Else
                            'cnt = 0
                            'sb.Append("</table></td></tr>")
                        End If

                    End If
                Else
                    fid = "0"
                End If

            ElseIf dr.Item("comid").ToString <> cid Then
                If fid <> "0" Then
                    cid = dr.Item("comid").ToString
                    cpcnt = dr.Item("cpcnt").ToString
                    If cnt = 0 Then
                        cnt = cnt + 1
                        sb.Append("<tr><td></td><td></td>" & vbCrLf & "<td><table cellspacing=""0"" id=""t" + fid + """>" & vbCrLf)
                        If cpcnt <> 0 Then
                            sb.Append("<tr><td width=""15""><img src=""../images/appbuttons/minibuttons/gridpic.gif"" onclick=""getcoport('" + eid + "','" + fid + "','" + cid + "')""></td>" & vbCrLf)
                        Else
                            sb.Append("<tr><td width=""15""><img src=""../images/appbuttons/minibuttons/gridpicdis.gif""></td>" & vbCrLf)
                        End If
                        sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & cid & "', '" & fid & "', '" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & lid & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                    Else
                        cnt = cnt + 1
                        If cpcnt <> 0 Then
                            sb.Append("<tr><td width=""15""><img src=""../images/appbuttons/minibuttons/gridpic.gif"" onclick=""getcoport('" + eid + "','" + fid + "','" + cid + "')""></td>" & vbCrLf)
                        Else
                            sb.Append("<tr><td width=""15""><img src=""../images/appbuttons/minibuttons/gridpicdis.gif""></td>" & vbCrLf)
                        End If
                        sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & cid & "', '" & fid & "', '" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & lid & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                    End If
                    If cnt = cidhold Then
                        cnt = 0
                        sb.Append("</table></td></tr>")
                    End If
                    'Else
                    'cid = "0"
                    'cnt = 0
                    'sb.Append("</table></td></tr>")
                End If

            End If

        End While
        dr.Close()
        'h.Dispose()
        sb.Append("</td></tr></table></td></tr></table>")
        'Response.Write(sb.ToString)
        tdarch.InnerHtml = sb.ToString
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1352.Text = axlabs.GetASPXPage("woarch.aspx", "lang1352")
        Catch ex As Exception
        End Try
        Try
            lang1353.Text = axlabs.GetASPXPage("woarch.aspx", "lang1353")
        Catch ex As Exception
        End Try

    End Sub

End Class