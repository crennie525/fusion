﻿Imports System.Data.SqlClient
Public Class woexpds
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim sql As String
    Dim dr As SqlDataReader
    Dim ed As New Utilities
    Dim mu As New mmenu_utils_a
    Dim usr, eqid, wonum, isdown, stat, adm, isdownp, wt, coi, pmid, ispm, pmhid, currdt_both, dalert, dpalert, usedate, issched As String
    Dim downcnt As Integer
    Dim totaldown, totaldownp As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetFSLangs()

        Try
            lblfslang.value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            coi = mu.COMPI
            lblcoi.Value = coi
            issched = mu.Is_Sched
            If coi = "OK" Then
                'tdptop.Attributes.Add("class", "view")
                'tdpbot.Attributes.Add("class", "view")
            Else
                'tdptop.Attributes.Add("class", "details")
                'tdpbot.Attributes.Add("class", "details")
            End If
            wt = Request.QueryString("wt").ToString
            lblwt.Value = wt
            pmid = Request.QueryString("pmid").ToString
            lblpmid.Value = pmid
            pmhid = Request.QueryString("pmhid").ToString
            lblpmhid.Value = pmhid
            usr = Request.QueryString("usr").ToString
            eqid = Request.QueryString("eqid").ToString
            wonum = Request.QueryString("wo").ToString
            isdown = Request.QueryString("isdown").ToString
            isdownp = Request.QueryString("isdownp").ToString
            stat = Request.QueryString("stat").ToString
            lbluser.Value = usr
            lbleqid.Value = eqid
            lblwo.Value = wonum
            If isdown = "" Then
                isdown = "0"
            End If
            If isdownp = "" Then
                isdownp = "0"
            End If

            lblisdown.Value = isdown
            lblisdownp.Value = isdownp
            lblstatus.Value = stat
            If stat = "COMP" Or stat = "CLOSE" Then
                Try
                    adm = HttpContext.Current.Session("cadm").ToString()
                    lbladmin.Value = adm
                Catch ex As Exception
                    adm = "0"
                    lbladmin.Value = adm
                End Try
                If adm <> "1" Then
                    ddhrsstart.Enabled = False
                    ddminsstart.Enabled = False
                    ddapsstart.Enabled = False
                    'ddhrsstop.Enabled = False
                    'ddminsstop.Enabled = False
                    'ddapsstop.Enabled = False
                    txtdate.CssClass = "plainlabelblue"
                    txtstop.CssClass = "plainlabelblue"

                    ddhrsstartp.Enabled = False
                    ddminsstartp.Enabled = False
                    ddapsstartp.Enabled = False
                    'ddhrsstopp.Enabled = False
                    'ddminsstopp.Enabled = False
                    'ddapsstopp.Enabled = False
                    txtdatep.CssClass = "plainlabelblue"
                    txtstopp.CssClass = "plainlabelblue"

                    lblhold.Value = "0"
                Else
                    lblhold.Value = "0"
                End If
            Else
                lblhold.Value = "0"
            End If

            ed.Open()
            If coi = "EMD" And issched = "0" Then
                If (isdown = "0" And isdownp = "0") Then
                    usedate = Now
                    SaveStart("auto", usedate)
                    SaveStartp("auto", usedate)
                End If
            End If
            checkdown(eqid, wonum)
            If isdown = "1" Then
                'Just go to GetDown with last?
                GetDown(eqid, wonum)
            Else
                'Add new?
                Dim ehc As Integer
                sql = "select count(*) from eqhist where wonum = '" & wonum & "' and startdown is not null and totaldown is not null"
                Try
                    ehc = ed.strScalar(sql)
                Catch ex As Exception
                    ehc = "0"
                End Try
                'Dim totaldown As String
                If ehc <> 0 Then
                    '"<tr><td class=""plainlabelsm""><A href=""#"" onclick=""gettask('" & dr.Item("funcid").ToString & "','" & coid & "','" & dr.Item("compnum").ToString & "','" & task & "');"">" & task & "</A></td>")
                    tdeqedit.InnerHtml = "<a href=""#"" onclick=""getedit('eq');"">Edit Previous</a>"
                    sql = "select sum(totaldownm) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'"
                    Try
                        totaldown = ed.strScalar(sql)
                        tdeqtot.InnerHtml = "Total Recorded: " & totaldown & " minutes"
                    Catch ex As Exception
                        tdeqtot.InnerHtml = ""
                    End Try
                Else
                    tdeqedit.InnerHtml = ""
                    tdeqtot.InnerHtml = ""
                End If

            End If
            If isdownp = "1" Then
                'Just go to GetDown with last?
                GetDownP(eqid, wonum)
            Else
                'Add new?
                Dim ehc As Integer
                sql = "select count(*) from eqhist where wonum = '" & wonum & "' and startdownp is not null and totaldownp is not null"
                Try
                    ehc = ed.strScalar(sql)
                Catch ex As Exception
                    ehc = "0"
                End Try
                'Dim totaldownp As String
                If ehc <> 0 Then
                    '"<tr><td class=""plainlabelsm""><A href=""#"" onclick=""gettask('" & dr.Item("funcid").ToString & "','" & coid & "','" & dr.Item("compnum").ToString & "','" & task & "');"">" & task & "</A></td>")
                    tdprodedit.InnerHtml = "<a href=""#"" onclick=""getedit('prod');"">Edit Previous</a>"
                    sql = "select sum(totaldownpm) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'"
                    Try
                        totaldownp = ed.strScalar(sql)
                        tdprodtot.InnerHtml = "Total Recorded: " & totaldownp & " minutes"
                    Catch ex As Exception
                        tdprodtot.InnerHtml = ""
                    End Try
                Else
                    tdprodedit.InnerHtml = ""
                    tdprodtot.InnerHtml = ""
                End If
            End If
            ed.Dispose()
        Else
            Dim s As String = lblsubmit.Value
            Dim sq As String = s
            If Request.Form("lblsubmit") = "savestart" Then
                lblsubmit.Value = ""
                ed.Open()
                SaveStart("1")
                ed.Dispose()
            ElseIf Request.Form("lblsubmit") = "savestartboth" Then
                lblsubmit.Value = ""
                ed.Open()
                SaveStart("both")
                ed.Dispose()
            ElseIf Request.Form("lblsubmit") = "savestop" Then
                lblsubmit.Value = ""
                ed.Open()
                SaveStop("1")
                sql = "select sum(totaldownm) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'"
                Try
                    totaldown = ed.strScalar(sql)
                    tdeqtot.InnerHtml = "Total Recorded: " & totaldown & " minutes"
                Catch ex As Exception
                    tdeqtot.InnerHtml = ""
                End Try
                ed.Dispose()
            ElseIf Request.Form("lblsubmit") = "savestopboth" Then
                lblsubmit.Value = ""
                ed.Open()
                SaveStop("both")
                ed.Dispose()
            ElseIf Request.Form("lblsubmit") = "savestartp" Then
                lblsubmit.Value = ""
                ed.Open()
                SaveStartp("1")
                ed.Dispose()
            ElseIf Request.Form("lblsubmit") = "savestartpboth" Then
                lblsubmit.Value = ""
                ed.Open()
                SaveStartp("both")
                ed.Dispose()
            ElseIf Request.Form("lblsubmit") = "savestopp" Then
                lblsubmit.Value = ""
                ed.Open()
                SaveStopp("1")
                sql = "select sum(totaldownpm) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'"
                Try
                    totaldownp = ed.strScalar(sql)
                    tdprodtot.InnerHtml = "Total Recorded: " & totaldownp & " minutes"
                Catch ex As Exception
                    tdprodtot.InnerHtml = ""
                End Try
                ed.Dispose()
            ElseIf Request.Form("lblsubmit") = "savestoppboth" Then
                lblsubmit.Value = ""
                ed.Open()
                SaveStopp("both")
                ed.Dispose()
            ElseIf Request.Form("lblsubmit") = "reload" Then
                lblsubmit.Value = ""
                ed.Open()
                eqid = lbleqid.Value
                wonum = lblwo.Value
                'checkdown(eqid, wonum)
                'GetDown(eqid, wonum)
                'GetDownP(eqid, wonum)
                tdeqedit.InnerHtml = "<a href=""#"" onclick=""getedit('eq');"">Edit Previous</a>"
                sql = "select sum(totaldownm) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'"
                Try
                    totaldown = ed.strScalar(sql)
                    tdeqtot.InnerHtml = "Total Recorded: " & totaldown & " minutes"
                Catch ex As Exception
                    tdeqtot.InnerHtml = ""
                    tdeqedit.InnerHtml = ""
                End Try
                ed.Dispose()
            ElseIf Request.Form("lblsubmit") = "reloadp" Then
                lblsubmit.Value = ""
                ed.Open()
                eqid = lbleqid.Value
                wonum = lblwo.Value
                'checkdown(eqid, wonum)
                'GetDown(eqid, wonum)
                'GetDownP(eqid, wonum)
                tdprodedit.InnerHtml = "<a href=""#"" onclick=""getedit('prod');"">Edit Previous</a>"
                sql = "select sum(totaldownpm) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'"
                Try
                    totaldownp = ed.strScalar(sql)
                    tdprodtot.InnerHtml = "Total Recorded: " & totaldownp & " minutes"
                Catch ex As Exception
                    tdprodtot.InnerHtml = ""
                    tdprodedit.InnerHtml = ""
                End Try
                ed.Dispose()
                


            ElseIf Request.Form("lblsubmit") = "getdown" Then
                lblsubmit.Value = ""
                ed.Open()
                eqid = lbleqid.Value
                wonum = lblwo.Value
                GetDown(eqid, wonum)
                tdeqedit.InnerHtml = "<a href=""#"" onclick=""getedit('eq');"">Edit Previous</a>"
                sql = "select sum(totaldownm) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'"
                Try
                    totaldown = ed.strScalar(sql)
                    tdeqtot.InnerHtml = "Total Recorded: " & totaldown & " minutes"
                Catch ex As Exception
                    tdeqtot.InnerHtml = ""
                End Try
                ed.Dispose()

                txtdate.Text = ""
                txtstop.Text = ""
                txttote.Text = ""

                ddhrsstart.SelectedValue = "NA"
                ddminsstart.SelectedValue = "00"
                ddapsstart.SelectedValue = "AM"
                lbleqhid.Value = "0"
                tdeqadd.InnerHtml = ""

            ElseIf Request.Form("lblsubmit") = "getdownp" Then
                lblsubmit.Value = ""
                eqid = lbleqid.Value
                wonum = lblwo.Value
                ed.Open()
                GetDownP(eqid, wonum)
                tdprodedit.InnerHtml = "<a href=""#"" onclick=""getedit('prod');"">Edit Previous</a>"
                sql = "select sum(totaldownpm) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'"
                Try
                    totaldownp = ed.strScalar(sql)
                    tdprodtot.InnerHtml = "Total Recorded: " & totaldownp & " minutes"
                Catch ex As Exception
                    tdprodtot.InnerHtml = ""
                End Try
                ed.Dispose()

                txtdatep.Text = ""
                txtstopp.Text = ""
                txttotp.Text = ""

                ddhrsstartp.SelectedValue = "NA"
                ddminsstartp.SelectedValue = "00"
                ddapsstartp.SelectedValue = "AM"
                lbleqhidp.Value = "0"
                tdprodadd.InnerHtml = ""
            End If

        End If
    End Sub
    Private Sub GetDownP(ByVal eqid As String, ByVal wonum As String)

        txtdatep.Text = ""
        txtstopp.Text = ""
        txttotp.Text = ""

        ddhrsstartp.SelectedValue = "NA"
        ddminsstartp.SelectedValue = "00"
        ddapsstartp.SelectedValue = "AM"

        'ddapsstopp.SelectedValue = "AM"
        'ddminsstopp.SelectedValue = "00"
        'ddhrsstopp.SelectedValue = "NA"

        Dim eqhidp As String
        sql = "select isnull(max(eqhid), 0) from eqhist where wonum = '" & wonum & "' and startdownp is not null and totaldownp is null"
        Try
            eqhidp = ed.strScalar(sql)
            lbleqhidp.Value = eqhidp
        Catch ex As Exception
            eqhidp = "0"

        End Try

        Dim ehc As Integer
        sql = "select count(*) from eqhist where wonum = '" & wonum & "' and startdownp is not null and totaldownp is null"
        Try
            ehc = ed.strScalar(sql)
        Catch ex As Exception
            ehc = "0"
        End Try
        If ehc = 0 Then
            isdownp = "0"
            lblisdownp.Value = isdownp

            sql = "update equipment set isdownp = 0, wonum = '" & wonum & "' where eqid = '" & eqid & "';update workorder set isdownp = '0' where wonum = '" & wonum & "';"
            ed.Update(sql)
        End If

        wt = lblwt.Value
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        eqid = lbleqid.Value
        wonum = lblwo.Value

        Dim ehc1 As Integer
        sql = "select count(*) from eqhist where wonum = '" & wonum & "' and startdownp is not null and totaldownp is not null"
        Try
            ehc1 = ed.strScalar(sql)
        Catch ex As Exception
            ehc1 = "0"
        End Try



        Dim repdown, startdown, stopdown, totaldown, repdate, startdate, stopdate As String
        Dim repdownp, startdownp, stopdownp, totaldownp, repdatep, startdatep, stopdatep, totaldownpm As String
        If eqhidp <> "0" Then
            sql = "select eqid, repdown, startdown, stopdown, repdownp, startdownp, stopdownp, wonum, ispm, totaldown, " _
                + "totaldownpm = (select sum(totaldownpm) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'), " _
                + "totaldownp = (select sum(totaldownp) * 60 from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'), " _
        + "Convert(char(10),repdown, 101) as repdate, " _
        + "Convert(char(10),repdownp, 101) as repdatep, " _
        + "Convert(char(10),startdown, 101) as startdate, " _
        + "Convert(char(10),stopdown, 101) as stopdate, " _
        + "Convert(char(10),startdownp, 101) as startdatep, " _
        + "Convert(char(10),stopdownp, 101) as stopdatep " _
        + "from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "' and eqhid = '" & eqhidp & "'"
            dr = ed.GetRdrData(sql)
            While dr.Read
                repdown = dr.Item("repdown").ToString
                repdownp = dr.Item("repdownp").ToString
                'startdown = dr.Item("startdown").ToString
                'stopdown = dr.Item("stopdown").ToString

                startdownp = dr.Item("startdownp").ToString
                stopdownp = dr.Item("stopdownp").ToString

                'repdate = dr.Item("repdate").ToString
                'startdate = dr.Item("startdate").ToString
                'stopdate = dr.Item("stopdate").ToString

                repdatep = dr.Item("repdatep").ToString
                startdatep = dr.Item("startdatep").ToString
                stopdatep = dr.Item("stopdatep").ToString

                'totaldown = dr.Item("totaldown").ToString
                totaldownp = dr.Item("totaldownp").ToString
                totaldownpm = dr.Item("totaldownpm").ToString
            End While
            dr.Close()

            If ehc1 <> 0 Then
                '"<tr><td class=""plainlabelsm""><A href=""#"" onclick=""gettask('" & dr.Item("funcid").ToString & "','" & coid & "','" & dr.Item("compnum").ToString & "','" & task & "');"">" & task & "</A></td>")
                tdprodedit.InnerHtml = "<a href=""#"" onclick=""getedit('prod');"">Edit Previous</a>"
                tdprodtot.InnerHtml = "Total Recorded: " & totaldownpm & " minutes"
            Else
                tdprodedit.InnerHtml = ""
                tdprodtot.InnerHtml = ""
            End If
            'If repdown <> "" Then
            'tdrepdate.InnerHtml = repdown
            'Else

            'tdrepdate.InnerHtml = startdown
            'End If
            If repdownp <> "" Then
                'tdrepdatep.InnerHtml = repdownp
            Else

                tdrepdatep.InnerHtml = startdownp
            End If

            'If startdown <> "" Then
            'txtdate.Text = startdate
            'lblstartdown.Value = startdate
            'trmsg.Attributes.Add("class", "details")
            'Else
            'trmsg.Attributes.Add("class", "view")
            'End If
            If startdownp <> "" Then
                txtdatep.Text = startdatep
                lblstartdownp.Value = startdatep
                trmsg2.Attributes.Add("class", "details")
            Else
                trmsg2.Attributes.Add("class", "view")
                txtdatep.Text = ""
                lblstartdownp.Value = ""
            End If
            'If stopdown <> "" Then
            'txtstop.Text = stopdate
            'End If
            If stopdownp <> "" Then
                txtstopp.Text = stopdatep
            Else
                txtstopp.Text = ""
            End If
            Dim starttime As Date
            Dim shr, smin, saps, thr, tmin, taps As Integer
            Dim sam, spm, tam, tpm, shrs, thrs, smins, tmins As String
            Dim starttimep As Date
            Dim shrp, sminp, sapsp, thrp, tminp, tapsp As Integer
            Dim samp, spmp, tamp, tpmp, shrsp, thrsp, sminsp, tminsp As String
            'If startdown <> "" Then
            'starttime = startdown
            'saps = startdown.IndexOf("AM")
            'If saps <> -1 Then
            'sam = "AM"
            'Else
            'sam = "PM"
            'End If
            'Else
            'If repdown <> "" And startdown <> "" Then
            'starttime = repdown
            'saps = repdown.IndexOf("AM")
            'If saps <> -1 Then
            'sam = "AM"
            'Else
            'sam = "PM"
            'End If
            'End If

            'End If

            If startdownp <> "" Then
                starttimep = startdownp
                sapsp = startdownp.IndexOf("AM")
                If sapsp <> -1 Then
                    samp = "AM"
                Else
                    samp = "PM"
                End If
            Else
                If repdownp <> "" Then
                    starttimep = repdownp
                    sapsp = repdownp.IndexOf("AM")
                    If sapsp <> -1 Then
                        samp = "AM"
                    Else
                        samp = "PM"
                    End If
                End If

            End If

            'Try
            'shr = DatePart(DateInterval.Hour, starttime)
            'If shr > 12 Then
            'shr = shr - 12
            'End If
            'If shr < 10 And shr <> 0 Then
            'shrs = "0" & shr
            'Else
            'If shr = "0" Then
            'shrs = "12"
            'Else
            'shrs = shr
            'End If
            'End If
            'smin = DatePart(DateInterval.Minute, starttime)
            'If smin < 10 Then
            'smins = "0" & smin
            'Else
            'smins = smin
            'End If
            'ddapsstart.SelectedValue = sam
            'ddminsstart.SelectedValue = smins
            'ddhrsstart.SelectedValue = shrs
            'Catch ex As Exception

            'End Try

            Try
                shrp = DatePart(DateInterval.Hour, starttimep)
                If shrp > 12 Then
                    shrp = shrp - 12
                End If
                If shrp < 10 And shrp <> 0 Then
                    shrsp = "0" & shrp
                Else
                    If shrp = "0" Then
                        shrsp = "12"
                    Else
                        shrsp = shrp
                    End If
                    'shrsp = shrp
                End If
                sminp = DatePart(DateInterval.Minute, starttimep)
                If sminp < 10 Then
                    sminsp = "0" & sminp
                Else
                    sminsp = smin
                End If
                ddapsstartp.SelectedValue = samp
                ddminsstartp.SelectedValue = sminsp
                ddhrsstartp.SelectedValue = shrsp
            Catch ex As Exception

            End Try

            lblstartdownp.Value = startdatep & " " & shrp & ":" & sminsp & " " & samp


            Dim stoptime As Date
            'If stopdown <> "" Then
            'stoptime = stopdown
            'taps = stopdown.IndexOf("AM")
            'If taps <> -1 Then
            'tam = "AM"
            'Else
            'tam = "PM"
            'End If
            'Try
            'thr = DatePart(DateInterval.Hour, stoptime)
            'If thr > 12 Then
            'thr = thr - 12
            'End If
            'If thr < 10 And thr <> 0 Then
            'thrs = "0" & thr
            'Else
            'If thr = "0" Then
            'thrs = "12"
            'Else
            'thrs = thr
            'End If
            'thrs = thr
            'End If
            'tmin = DatePart(DateInterval.Minute, stoptime)
            'If tmin < 10 Then
            'tmins = "0" & tmin
            'Else
            'tmins = tmin
            'End If
            'ddapsstop.SelectedValue = tam
            'ddminsstop.SelectedValue = tmins
            'ddhrsstop.SelectedValue = thrs
            'Catch ex As Exception

            'End Try
            'End If

            Dim stoptimep As Date
            If stopdownp <> "" Then
                stoptimep = stopdownp
                tapsp = stopdownp.IndexOf("AM")
                If tapsp <> -1 Then
                    tamp = "AM"
                Else
                    tamp = "PM"
                End If
                Try
                    thrp = DatePart(DateInterval.Hour, stoptimep)
                    If thrp > 12 Then
                        thrp = thrp - 12
                    End If
                    If thrp < 10 And thrp <> 0 Then
                        thrsp = "0" & thrp
                    Else
                        If thrp = "0" Then
                            thrsp = "12"
                        Else
                            thrsp = thrp
                        End If
                        'thrsp = thrp
                    End If
                    tminp = DatePart(DateInterval.Minute, stoptimep)
                    If tminp < 10 Then
                        tminsp = "0" & tminp
                    Else
                        tminsp = tminp
                    End If
                    'ddapsstopp.SelectedValue = tamp
                    'ddminsstopp.SelectedValue = tminsp
                    'ddhrsstopp.SelectedValue = thrsp
                Catch ex As Exception

                End Try
            End If
        End If

    End Sub
    Private Sub GetDown(ByVal eqid As String, ByVal wonum As String)


        txtdate.Text = ""
        txtstop.Text = ""
        txttote.Text = ""

        ddhrsstart.SelectedValue = "NA"
        ddminsstart.SelectedValue = "00"
        ddapsstart.SelectedValue = "AM"

        'ddapsstop.SelectedValue = "AM"
        'ddminsstop.SelectedValue = "00"
        'ddhrsstop.SelectedValue = "NA"

        Dim eqhid As String
        sql = "select isnull(max(eqhid), 0) from eqhist where wonum = '" & wonum & "' and startdown is not null and totaldown is null"

        Try
            eqhid = ed.strScalar(sql)
            lbleqhid.Value = eqhid
        Catch ex As Exception
            eqhid = "0"
        End Try

        Dim ehc As Integer
        sql = "select count(*) from eqhist where wonum = '" & wonum & "' and startdown is not null and totaldown is null"
        Try
            ehc = ed.strScalar(sql)
        Catch ex As Exception
            ehc = "0"
        End Try

        If ehc = 0 Then
            isdown = "0"
            lblisdown.Value = isdown

            sql = "update equipment set isdown = 0, wonum = '" & wonum & "' where eqid = '" & eqid & "';update workorder set isdown = '0' where wonum = '" & wonum & "';"
            ed.Update(sql)
        End If

        Dim ehc1 As Integer
        sql = "select count(*) from eqhist where wonum = '" & wonum & "' and startdown is not null and stopdown is not null"
        Try
            ehc1 = ed.strScalar(sql)
        Catch ex As Exception
            ehc1 = "0"
        End Try



        Dim repdown, startdown, stopdown, totaldown, repdate, startdate, stopdate, totaldownm As String
        Dim repdownp, startdownp, stopdownp, totaldownp, repdatep, startdatep, stopdatep As String
        If eqhid <> "0" Then
            sql = "select eqid, repdown, startdown, stopdown, repdownp, startdownp, stopdownp, wonum, ispm, " _
                + "totaldownm = (select sum(totaldownm) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'), " _
                + "totaldown = (select sum(totaldown) * 60 from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'), " _
        + "Convert(char(10),repdown, 101) as repdate, " _
        + "Convert(char(10),repdownp, 101) as repdatep, " _
        + "Convert(char(10),startdown, 101) as startdate, " _
        + "Convert(char(10),stopdown, 101) as stopdate, " _
        + "Convert(char(10),startdownp, 101) as startdatep, " _
        + "Convert(char(10),stopdownp, 101) as stopdatep " _
        + "from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "' and eqhid = '" & eqhid & "'"
            dr = ed.GetRdrData(sql)
            While dr.Read
                repdown = dr.Item("repdown").ToString
                'repdownp = dr.Item("repdownp").ToString
                startdown = dr.Item("startdown").ToString
                stopdown = dr.Item("stopdown").ToString

                'startdownp = dr.Item("startdownp").ToString
                'stopdownp = dr.Item("stopdownp").ToString

                repdate = dr.Item("repdate").ToString
                startdate = dr.Item("startdate").ToString
                stopdate = dr.Item("stopdate").ToString

                'repdatep = dr.Item("repdatep").ToString
                'startdatep = dr.Item("startdatep").ToString
                'stopdatep = dr.Item("stopdatep").ToString

                totaldown = dr.Item("totaldown").ToString
                totaldownm = dr.Item("totaldownm").ToString
                'totaldownp = dr.Item("totaldownp").ToString
            End While
            dr.Close()

            If ehc1 <> 0 Then
                '"<tr><td class=""plainlabelsm""><A href=""#"" onclick=""gettask('" & dr.Item("funcid").ToString & "','" & coid & "','" & dr.Item("compnum").ToString & "','" & task & "');"">" & task & "</A></td>")
                tdeqedit.InnerHtml = "<a href=""#"" onclick=""getedit('eq');"">Edit Previous</a>"
                tdeqtot.InnerHtml = "Total Recorded: " & totaldownm & " minutes"
            Else
                tdeqedit.InnerHtml = ""
                tdeqtot.InnerHtml = ""
            End If

            If repdown <> "" Then
                tdrepdate.InnerHtml = repdown
            Else

                tdrepdate.InnerHtml = startdown
            End If
            'If repdownp <> "" Then
            'tdrepdatep.InnerHtml = repdownp
            'Else

            'tdrepdatep.InnerHtml = startdownp
            'End If

            If startdown <> "" Then
                txtdate.Text = startdate
                lblstartdown.Value = startdate
                trmsg.Attributes.Add("class", "details")
            Else
                trmsg.Attributes.Add("class", "view")
            End If
            'If startdownp <> "" Then
            'txtdatep.Text = startdatep
            'lblstartdownp.Value = startdatep
            'trmsg2.Attributes.Add("class", "details")
            'Else
            'trmsg2.Attributes.Add("class", "view")
            'End If
            If stopdown <> "" Then
                txtstop.Text = stopdate
            End If
            'If stopdownp <> "" Then
            'txtstopp.Text = stopdatep
            'End If
            Dim starttime As Date
            Dim shr, smin, saps, thr, tmin, taps As Integer
            Dim sam, spm, tam, tpm, shrs, thrs, smins, tmins As String
            Dim starttimep As Date
            Dim shrp, sminp, sapsp, thrp, tminp, tapsp As Integer
            Dim samp, spmp, tamp, tpmp, shrsp, thrsp, sminsp, tminsp As String
            If startdown <> "" Then
                starttime = startdown
                saps = startdown.IndexOf("AM")
                If saps <> -1 Then
                    sam = "AM"
                Else
                    sam = "PM"
                End If
            Else
                If repdown <> "" And startdown <> "" Then
                    starttime = repdown
                    saps = repdown.IndexOf("AM")
                    If saps <> -1 Then
                        sam = "AM"
                    Else
                        sam = "PM"
                    End If
                End If

            End If

            'If startdownp <> "" Then
            'starttimep = startdownp
            'sapsp = startdownp.IndexOf("AM")
            'If sapsp <> -1 Then
            'samp = "AM"
            'Else
            'samp = "PM"
            'End If
            'Else
            'If repdownp <> "" Then
            'starttimep = repdownp
            'sapsp = repdownp.IndexOf("AM")
            'If sapsp <> -1 Then
            'samp = "AM"
            'Else
            'samp = "PM"
            'End If
            'End If

            'End If

            Try
                shr = DatePart(DateInterval.Hour, starttime)
                If shr > 12 Then
                    shr = shr - 12
                End If
                If shr < 10 And shr <> 0 Then
                    shrs = "0" & shr
                Else
                    If shr = "0" Then
                        shrs = "12"
                    Else
                        shrs = shr
                    End If
                End If
                smin = DatePart(DateInterval.Minute, starttime)
                If smin < 10 Then
                    smins = "0" & smin
                Else
                    smins = smin
                End If
                ddapsstart.SelectedValue = sam
                ddminsstart.SelectedValue = smins
                ddhrsstart.SelectedValue = shrs
            Catch ex As Exception

            End Try

            'Try
            'shrp = DatePart(DateInterval.Hour, starttimep)
            'If shrp > 12 Then
            'shrp = shrp - 12
            'End If
            'If shrp < 10 And shrp <> 0 Then
            'shrsp = "0" & shrp
            'Else
            'If shrp = "0" Then
            'shrsp = "12"
            'Else
            'shrsp = shrp
            'End If
            'shrsp = shrp
            'End If
            'sminp = DatePart(DateInterval.Minute, starttimep)
            'If sminp < 10 Then
            'sminsp = "0" & sminp
            'Else
            'sminsp = smin
            'End If
            'ddapsstartp.SelectedValue = samp
            'ddminsstartp.SelectedValue = sminsp
            'ddhrsstartp.SelectedValue = shrsp
            'Catch ex As Exception

            'End Try

            lblstartdown.Value = startdate & " " & shr & ":" & smins & " " & sam

            Dim stoptime As Date
            If stopdown <> "" Then
                stoptime = stopdown
                taps = stopdown.IndexOf("AM")
                If taps <> -1 Then
                    tam = "AM"
                Else
                    tam = "PM"
                End If
                Try
                    thr = DatePart(DateInterval.Hour, stoptime)
                    If thr > 12 Then
                        thr = thr - 12
                    End If
                    If thr < 10 And thr <> 0 Then
                        thrs = "0" & thr
                    Else
                        If thr = "0" Then
                            thrs = "12"
                        Else
                            thrs = thr
                        End If
                        'thrs = thr
                    End If
                    tmin = DatePart(DateInterval.Minute, stoptime)
                    If tmin < 10 Then
                        tmins = "0" & tmin
                    Else
                        tmins = tmin
                    End If
                    'ddapsstop.SelectedValue = tam
                    'ddminsstop.SelectedValue = tmins
                    'ddhrsstop.SelectedValue = thrs
                Catch ex As Exception

                End Try
            End If

            Dim stoptimep As Date
            'If stopdownp <> "" Then
            'stoptimep = stopdownp
            'tapsp = stopdownp.IndexOf("AM")
            'If tapsp <> -1 Then
            'tamp = "AM"
            'Else
            'tamp = "PM"
            'End If
            'Try
            'thrp = DatePart(DateInterval.Hour, stoptimep)
            'If thrp > 12 Then
            'thrp = thrp - 12
            'End If
            'If thrp < 10 And thrp <> 0 Then
            'thrsp = "0" & thrp
            ' Else
            'If thrp = "0" Then
            'thrsp = "12"
            'Else
            'thrsp = thrp
            'End If
            'thrsp = thrp
            'End If
            'tminp = DatePart(DateInterval.Minute, stoptimep)
            'If tminp < 10 Then
            'tminsp = "0" & tminp
            'Else
            'tminsp = tminp
            'End If
            'ddapsstopp.SelectedValue = tamp
            'ddminsstopp.SelectedValue = tminsp
            'ddhrsstopp.SelectedValue = thrsp
            'Catch ex As Exception

            'End Try
            'End If
        End If



    End Sub
    Private Sub checkdown(ByVal eqid As String, ByVal wonum As String)
        sql = "select isdown, isdownp, wonum, wonump from equipment where eqid = '" & eqid & "'"
        Dim d, dp, w, wp As String
        Dim dint As Integer = 0
        Dim dpint As Integer = 0
        dr = ed.GetRdrData(sql)
        While dr.Read
            d = dr.Item("isdown").ToString
            dp = dr.Item("isdownp").ToString
            w = dr.Item("wonum").ToString
            wp = dr.Item("wonump").ToString
        End While
        dr.Close()
        If d = "" Then
            d = "0"
        End If
        If dp = "" Then
            dp = "0"
        End If
        isdown = d
        isdownp = dp
        lblisdown.Value = d
        lblisdownp.Value = dp


    End Sub
    Private Sub checkdown(ByVal eqid As String, ByVal wonum As String, ByVal wt As String)
        sql = "select isdown, isdownp, wonum, wonump from equipment where eqid = '" & eqid & "'"
        Dim d, dp, w, wp As String
        Dim dint As Integer = 0
        Dim dpint As Integer = 0
        dr = ed.GetRdrData(sql)
        While dr.Read
            d = dr.Item("isdown").ToString
            dp = dr.Item("isdownp").ToString
            w = dr.Item("wonum").ToString
            wp = dr.Item("wonump").ToString
        End While
        dr.Close()
        If w <> wonum And w <> "" Then
            If d <> "" Then
                dint += 1
                lbldalert.Value = "1"

            End If
        End If
        If wp <> wonum And wp <> "" Then
            If dp <> "" Then
                dpint += 1
                lbldpalert.Value = "1"

            End If
        End If
        If wt <> "PM" And wt <> "TPM" And wt <> "MAXPM" Then
            If dint = 1 And dpint = 1 Then
                Dim strMessage As String = "Warning!\nThis Equipment is designated as Down for\nEquipment and Production in another Work Order"
                ed.CreateMessageAlert(Me, strMessage, "strKey1")
            ElseIf dint = 1 Then
                Dim strMessage As String = "Warning!\nThis Equipment is designated as Down for\nEquipment in another Work Order"
                ed.CreateMessageAlert(Me, strMessage, "strKey1")
            ElseIf dpint = 1 Then
                Dim strMessage As String = "Warning!\nThis Equipment is designated as Down for\nProduction in another Work Order"
                ed.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If

    End Sub
    Private Sub SaveStart(ByVal who As String, Optional ByVal usedate As String = "")
        wt = lblwt.Value
        dalert = lbldalert.Value
        wonum = lblwo.Value
        isdown = lblisdown.Value
        isdownp = lblisdownp.Value
        Dim pstr, ph, pm, pa, currt, currd As String
        Dim currdt As Date
        If who = "dup" Or who = "auto" Then
            currdt = usedate
        Else
            ph = ddhrsstart.SelectedValue
            pm = ddminsstart.SelectedValue
            pa = ddapsstart.SelectedValue
            currt = ph & ":" & pm & " " & pa
            currd = txtdate.Text
            currdt = currd & " " & currt
        End If
        eqid = lbleqid.Value
        wonum = lblwo.Value
        pmid = lblpmid.Value
        wt = lblwt.Value



        Try
            Dim gdate As Date = Now
            Dim idate As Date = currdt
            If idate > gdate Then
                Dim strMessage As String = "Start Date and Time greater than Current Date and Time"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        Catch ex As Exception
            Dim strMessage As String = "Problem saving Start Date and Time"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        



        Dim eqhid As String
        eqhid = lbleqhid.Value
        Dim eqhidp As String
        eqhidp = lbleqhidp.Value

        If pmid <> "" And (wt = "PM" Or wt = "TPM" Or wt = "MAXPM") Then
            ispm = "1"
        Else
            If dalert <> "1" Then
                sql = "update equipment set isdown = 1, wonum = '" & wonum & "' where eqid = '" & eqid & "'"
                ed.Update(sql)
            End If
        End If
        If isdown = "0" And who <> "dup" Then ' And isdownp = "0"
            sql = "insert into eqhist(eqid, repdown, startdown, wonum, ispm, pmid) values " _
                + "('" & eqid & "','" & currdt & "','" & currdt & "','" & wonum & "','" & ispm & "','" & pmid & "'); select @@identity"
            lblisdown.Value = "1"
            trmsg.Attributes.Add("class", "details")
            eqhid = ed.Scalar(sql)
            lbleqhid.Value = eqhid
            sql = "update workorder set isdown = '1' where wonum = '" & wonum & "'"
            ed.Update(sql)
        Else
            If who <> "dup" Then
                sql = "update eqhist set startdown = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "' and eqhid = '" & eqhid & "';update workorder set isdown = '1' where wonum = '" & wonum & "';"
                trmsg.Attributes.Add("class", "details")
                ed.Update(sql)
            
            End If

        End If
        lblstartdown.Value = currdt
        tdmsg.InnerHtml = ""
        If who = "both" Then
            SaveStartp("dup", currdt)
        End If
        'checkdown(eqid, wonum)
        'GetDown(eqid, wonum)
        'GetDownP(eqid, wonum)
    End Sub
    Private Sub SaveStartp(ByVal who As String, Optional ByVal usedate As String = "")
        wt = lblwt.Value
        dpalert = lbldpalert.Value
        wonum = lblwo.Value
        isdown = lblisdown.Value
        isdownp = lblisdownp.Value
        Dim pstr, ph, pm, pa, currt, currd As String
        Dim currdt As Date
        If who = "dup" Or who = "auto" Then
            currdt = usedate
        Else
            ph = ddhrsstartp.SelectedValue
            pm = ddminsstartp.SelectedValue
            pa = ddapsstartp.SelectedValue
            currt = ph & ":" & pm & " " & pa
            currd = txtdatep.Text
            currdt = currd & " " & currt
        End If

        Try
            Dim gdate As Date = Now
            Dim idate As Date = currdt
            If idate > gdate Then
                Dim strMessage As String = "Start Date and Time greater than Current Date and Time"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        Catch ex As Exception
            Dim strMessage As String = "Problem saving Start Date and Time"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        Dim eqhidp As String
        eqhidp = lbleqhidp.Value
        Dim eqhid As String
        eqhid = lbleqhid.Value

        eqid = lbleqid.Value
        wonum = lblwo.Value
        pmid = lblpmid.Value
        wt = lblwt.Value
        If pmid <> "" And (wt = "PM" Or wt = "TPM" Or wt = "MAXPM") Then
            ispm = "1"
        Else
            If dpalert <> "1" Then
                sql = "update equipment set isdownp = 1, wonump = '" & wonum & "' where eqid = '" & eqid & "'"
                ed.Update(sql)
            End If
        End If
        If isdownp = "0" And who <> "dup" Then 'isdown = "0" And 
            sql = "insert into eqhist(eqid, repdownp, startdownp, wonum, ispm, pmid) values ('" & eqid & "','" & currdt & "','" & currdt & "','" & wonum & "','" & ispm & "','" & pmid & "'); select @@identity"
            lblisdownp.Value = "1"
            trmsg2.Attributes.Add("class", "details")
            eqhidp = ed.Scalar(sql)
            lbleqhidp.Value = eqhidp
            sql = "update workorder set isdownp = '1' where wonum = '" & wonum & "'"
            ed.Update(sql)
        Else
            If who <> "dup" Then
                sql = "update eqhist set startdownp = '" & currdt & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "' and eqhid = '" & eqhidp & "';update workorder set isdownp = '1' where wonum = '" & wonum & "';"
                trmsg2.Attributes.Add("class", "details")
                ed.Update(sql)
            End If

        End If
        lblstartdownp.Value = currdt
        tdmsg2.InnerHtml = ""
        If who = "both" Then
            SaveStart("dup", currdt)
        End If
        'checkdown(eqid, wonum)
        'GetDownP(eqid, wonum)
        'GetDown(eqid, wonum)
    End Sub
    Private Sub SaveStop(ByVal who As String, Optional ByVal usedate As String = "")
        wt = lblwt.Value
        dalert = lbldalert.Value
        wonum = lblwo.Value
        isdown = lblisdown.Value
        isdownp = lblisdownp.Value
        Dim pstr, ph, pm, pa, currt As String
        Dim currd As Decimal 'Date
        currd = txttote.Text

        eqid = lbleqid.Value
        wonum = lblwo.Value
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        wt = lblwt.Value

        Dim startdt As Date = txtdate.Text 'lblstartdown.Value
        Dim dthrs As String
        Dim dtint As Integer
        Dim dtmins As Decimal 'Integer

        Try
            If rdmhem.Checked = True Then
                dthrs = currd / 60
                dtmins = currd
            Else
                dthrs = currd
                dtmins = currd * 60
            End If
        Catch ex As Exception
            Dim strMessage As String = "Total Down Time for Equipment Could Not Be Calculated"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        Try
            dtint = CType(dthrs, Integer)
        Catch ex As Exception
            Dim strMessage As String = "Total Down Time for Equipment Could Not Be Calculated"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        If dtint < 0 Then
            Dim strMessage As String = "Total Down Time for Equipment Would Result in a Negative Value: Update Aborted"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If

        Dim eqhid As String
        eqhid = lbleqhid.Value
        Dim eqhidp As String
        eqhidp = lbleqhidp.Value

        If who <> "dup" Then
            If pmid = "" And wt <> "MAXPM" Then
                sql = "update eqhist set totaldown = '" & dthrs & "', totaldownm = '" & dtmins & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "' and eqhid = '" & eqhid & "'; " _
            + "update workorder set isdown = '0' where wonum = '" & wonum & "'; " _
            + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "') " _
            + "where eqid = '" & eqid & "'"
            Else
                If wt = "PM" Then
                    sql = "update eqhist set totaldown = '" & dthrs & "', totaldownm = '" & dtmins & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "' and eqhid = '" & eqhid & "'; " _
            + "update workorder set isdown = '0' where wonum = '" & wonum & "'; " _
            + "update pm set actdtime = '" & dtmins & "' where pmid = '" & pmid & "'; " _
            + "update pmhist set actdtime = '" & dtmins & "' where pmhid = '" & pmhid & "'; " _
            + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "') " _
            + "where eqid = '" & eqid & "'"
                ElseIf wt = "TPM" Then
                    sql = "update eqhist set totaldown = '" & dthrs & "', totaldownm = '" & dtmins & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "' nd eqhid = '" & eqhid & "'; " _
            + "update workorder set isdown = '0' where wonum = '" & wonum & "'; " _
            + "update tpm set actdtime = '" & dtmins & "' where pmid = '" & pmid & "'; " _
            + "update pmhisttpm set actdtime = '" & dtmins & "' where pmhid = '" & pmhid & "'; " _
            + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "') " _
            + "where eqid = '" & eqid & "'"
                End If
            End If
        
        End If



        Try
            ed.Update(sql)
            lblisdown.Value = "0"
        Catch ex As Exception

        End Try
        If wt <> "PM" And wt <> "TPM" And wt <> "MAXPM" Then
            If dalert <> "1" Then
                sql = "update equipment set isdown = null, wonum = null where eqid = '" & eqid & "'"
                ed.Update(sql)
            End If
        End If
        If who = "both" Then
            'SaveStopp("dup", currdt)
        Else
            tdeqadd.InnerHtml = "<a href=""#"" onclick=""getdown();"">Add New Record</ a>"
            'GetDown(eqid, wonum)
            'GetDownP(eqid, wonum)
        End If

    End Sub
    Private Sub SaveStopp(ByVal who As String, Optional ByVal usedate As String = "")
        wt = lblwt.Value
        dpalert = lbldpalert.Value
        wonum = lblwo.Value
        Dim pstr, ph, pm, pa, currt As String
        Dim currd As Decimal 'Date
        currd = txttotp.Text

        eqid = lbleqid.Value
        wonum = lblwo.Value
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        wt = lblwt.Value

        Dim startdt As Date = txtdatep.Text 'lblstartdownp.Value
        Dim dthrs As String
        Dim dtint As Integer
        Dim dtmins As Decimal 'Integer

        Try
            If rdmhpm.Checked = True Then
                dthrs = currd / 60
                dtmins = currd
            Else
                dthrs = currd
                dtmins = currd * 60
            End If
        Catch ex As Exception
            Dim strMessage As String = "Total Down Time for Production Could Not Be Calculated"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        Try
            dtint = CType(dthrs, Integer)
        Catch ex As Exception
            Dim strMessage As String = "Total Down Time for Production Could Not Be Calculated"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        If dtint < 0 Then
            Dim strMessage As String = "Total Down Time for Production Would Result in a Negative Value: Update Aborted"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If

        Dim eqhidp As String
        eqhidp = lbleqhidp.Value
        Dim eqhid As String
        eqhid = lbleqhid.Value

        If who <> "dup" Then
            If pmid = "" And wt <> "MAXPM" Then
                sql = "update eqhist set totaldownp = '" & dthrs & "', totaldownpm = '" & dtmins & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "' and eqhid = '" & eqhidp & "'; " _
                + "update workorder set isdownp = '0' where wonum = '" & wonum & "'; " _
                + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "') " _
                + "where eqid = '" & eqid & "'"
            Else
                If wt = "PM" Then
                    sql = "update eqhist set totaldownp = '" & dthrs & "', totaldownpm = '" & dtmins & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "' and eqhid = '" & eqhidp & "'; " _
         + "update workorder set isdownp = '0' where wonum = '" & wonum & "'; " _
         + "update pm set actdptime = '" & dtmins & "' where pmid = '" & pmid & "'; " _
         + "update pmhist set actdptime = '" & dtmins & "' where pmhid = '" & pmhid & "'; " _
         + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "') " _
         + "where eqid = '" & eqid & "'"
                ElseIf wt = "TPM" Then
                    sql = "update eqhist set totaldown = '" & dthrs & "', totaldownpm = '" & dtmins & "' where eqid = '" & eqid & "' and wonum = '" & wonum & "' nd eqhid = '" & eqhidp & "'; " _
    + "update workorder set isdownp = '0' where wonum = '" & wonum & "'; " _
    + "update tpm set actdptime = '" & dtmins & "' where pmid = '" & pmid & "'; " _
    + "update pmhisttpm set actdptime = '" & dtmins & "' where pmhid = '" & pmhid & "'; " _
    + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "') " _
    + "where eqid = '" & eqid & "'"
                End If
            End If
        
        End If



        Try
            ed.Update(sql)
            lblisdownp.Value = "0"
        Catch ex As Exception

        End Try
        If wt <> "PM" And wt <> "TPM" And wt <> "MAXPM" Then
            If dpalert <> "1" Then
                sql = "update equipment set isdownp = null, wonump = null where eqid = '" & eqid & "'"
                ed.Update(sql)
            End If
        End If
        If who = "both" Then
            'SaveStop("dup", currdt)
        Else
            tdprodadd.InnerHtml = "<a href=""#"" onclick=""getdownp();"">Add New Record</ a>"
            'GetDownP(eqid, wonum)
            'GetDown(eqid, wonum)
        End If

    End Sub












    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            'lang1379.Text = axlabs.GetASPXPage("woexpd.aspx", "lang1379")
        Catch ex As Exception
        End Try
        Try
            'lang1380.Text = axlabs.GetASPXPage("woexpd.aspx", "lang1380")
        Catch ex As Exception
        End Try
        Try
            'lang1381.Text = axlabs.GetASPXPage("woexpd.aspx", "lang1381")
        Catch ex As Exception
        End Try
        Try
            'lang1382.Text = axlabs.GetASPXPage("woexpd.aspx", "lang1382")
        Catch ex As Exception
        End Try
        Try
            'lang1383.Text = axlabs.GetASPXPage("woexpd.aspx", "lang1383")
        Catch ex As Exception
        End Try
        Try
            'lang1384.Text = axlabs.GetASPXPage("woexpd.aspx", "lang1384")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                bgbreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                bgbreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                bgbreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                bgbreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                bgbreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
