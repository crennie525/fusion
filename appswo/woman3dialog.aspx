﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="woman3dialog.aspx.vb" Inherits="lucy_r12.woman3dialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add/Edit Work Orders</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        <!--
        function handlereturn(ret) {
        //alert(ret)
            window.returnValue = ret;
            window.close();
        }
        function pageScroll() {
            window.scrollTo(0, top);
            //window.scrollTo(0, 0);
            //scrolldelay = setTimeout('pageScroll()', 200);
            upsize('1');
            document.getElementById("ifsession").src = "../session.aspx?who=mm";
        }
        var timer = window.setTimeout("doref();", 1205000);
        function doref() {
            resetsess();
        }
        function setref() {
            window.clearTimeout(timer);
            timer = window.setTimeout("doref();", 1205000);
        }
        function handlejp() {
            //window.parent.setref();
            var won = document.getElementById("lblwo").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            //alert("../appsrt/jobplanmain2.aspx?start=wo&jid=0&wo=" + won + "&sid=" + sid + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper)
            window.location = "../appsrt/jobplanmain2.aspx?start=wo&jid=0&wo=" + won + "&sid=" + sid + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        function handlesched() {
            var href = document.getElementById("lblhref").value;
            var sid = document.getElementById("lblsid").value;
            var ro = "0"; //document.getElementById("lblro").value;
            var eqid = "0";
            var won = document.getElementById("lblwo").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            href = href.replace(/&/g, "~")
            window.location = "../appsman/PMScheduling2.aspx?jump=yes&typ=wo&href=" + href + "&ro=" + ro + "&sid=" + sid + "&eqid=" + eqid + "&wo=" + won + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        function handleswjplan(jpid) {
            setref();
            var href = document.getElementById("lblhref").value;

            var wo = document.getElementById("lblwo").value
            var ro = document.getElementById("lblro").value;
            href = href.replace(/&/g, "~")
            //var harr = href.split("~");
            //var remwo = harr[1] + "~";
            //href = href.replace(remwo, "")
            //alert(href)
            //alert("from wolabmain wojpedit.aspx?jpid=" + jpid + "&wonum=" + wo + "&href=" + href)
            window.location = "wojpeditmain.aspx?jpid=" + jpid + "&wonum=" + wo + "&href=" + href + "&ro=" + ro;
        }
        function handlewo(wo) {
            document.getElementById("lblwo").value = wo;
        }
        function GetWidth() {
            var x = 0;
            if (self.innerHeight) {
                x = self.innerWidth;
            }
            else if (document.documentElement && document.documentElement.clientHeight) {
                x = document.documentElement.clientWidth;
            }
            else if (document.body) {
                x = document.body.clientWidth;
            }
            return x;
        }

        function GetHeight() {
            var y = 0;
            if (self.innerHeight) {
                y = self.innerHeight;
            }
            else if (document.documentElement && document.documentElement.clientHeight) {
                y = document.documentElement.clientHeight;
            }
            else if (document.body) {
                y = document.body.clientHeight;
            }
            return y;
        }
        var flag = 5
        function upsize(who) {
            //alert(who)
            if (who != flag) {
                flag = who;
                document.getElementById("iftd").height = GetHeight();
                document.getElementById("iftd").width = GetWidth();
                window.scrollTo(0, top);
                //window.scrollTo(0, 0);
            }
        }
//-->
    </script>
</head>
<body onunload="handlereturn('ok');" onload="pageScroll();">
    <form id="form1" runat="server">
    <script type="text/javascript">
        document.body.onresize = function () {
            upsize('2');
        }
        self.onresize = function () {
            upsize('3');
        }
        document.documentElement.onresize = function () {
            upsize('4');
        }
        
    </script>
    <table style="position: absolute; top: 4px; left: 4px" cellspacing="0" cellpadding="0">
    <tr>
    <td></td>
    </tr>
    <tr>
    <td><iframe id="iftd" width="100%" height="600" src="" frameborder="0" runat="server">
    </iframe></td>
    </tr>
    </table>
     
    <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
    
    
     <input type="hidden" id="lblsessrefresh" runat="server" name="lblsessrefresh" />
    <input type="hidden" id="lblwt" runat="server" />
    <input type="hidden" id="lblwo" runat="server" />
    <input type="hidden" id="lblhref" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblwonum" runat="server" />
    <input type="hidden" id="lblusername" runat="server" />
    <input type="hidden" id="lbluid" runat="server" />
    <input type="hidden" id="lblislabor" runat="server" />
    <input type="hidden" id="lblissuper" runat="server" />
    <input type="hidden" id="lblisplanner" runat="server" />
    <input type="hidden" id="lblLogged_In" runat="server" name="lblLogged_In" />
    <input type="hidden" id="lblro" runat="server" name="lblro" />
    <input type="hidden" id="lblms" runat="server" name="lblms" />
    <input type="hidden" id="lblappstr" runat="server" name="lblappstr" />
    </form>
</body>
</html>
