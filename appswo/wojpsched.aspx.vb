

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wojpsched
    Inherits System.Web.UI.Page
	Protected WithEvents lang1517 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1516 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim comp As New Utilities
    Dim pmid As String
    Dim ds As DataSet
    Dim wonum, jpid, stat, ro, rostr As String
    Protected WithEvents lbltool As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllube As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsdateret As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dghours As System.Web.UI.WebControls.DataGrid
    Protected WithEvents Label24 As System.Web.UI.WebControls.Label
    Protected WithEvents txttsk As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdjpn As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdjpd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrow As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtlead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwojtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblindx As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            wonum = Request.QueryString("wo").ToString '"1407" '
            jpid = Request.QueryString("jpid").ToString '"5" '
            stat = Request.QueryString("stat").ToString '"INPRG" '
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblwo.Value = wonum
            lbljpid.Value = jpid
            lblstat.Value = stat
            lblro.Value = ro
            If ro <> "1" Then
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = comp.CheckROS(rostr, "wo")
                    lblro.Value = ro
                End If
            End If
            If (stat <> "COMP" And stat <> "CAN" And stat <> "WAPPR") Then
                dghours.Columns(9).Visible = True
            Else
                dghours.Columns(9).Visible = False
            End If
            comp.Open()
            GetJPHead(jpid)
            PopDL(jpid, wonum)
            comp.Dispose()
        Else
            If Request.Form("lblsubmit") = "getlead" Then
                lblsubmit.Value = ""
                comp.Open()
                GetLead()
                comp.Dispose()
            ElseIf Request.Form("lblsubmit") = "upsdate" Then
                lblsubmit.Value = ""
                comp.Open()
                UpSDate()
                comp.Dispose()
            End If
        End If
    End Sub
    Private Sub UpSDate()
        Dim wojtid, sdate As String
        wojtid = lblwojtid.Value
        sdate = lblsdateret.Value
        sql = "update wojobtasks set sdate = '" & sdate & "' where wojtid = '" & wojtid & "'"
        comp.Update(sql)
        wonum = lblwo.Value
        jpid = lbljpid.Value
        sql = "usp_checksdates '" & wonum & "'"
        comp.Update(sql)
        PopDL(jpid, wonum)
    End Sub
    Private Sub GetLead()
        Dim wojtid As String = lblwojtid.Value
        Dim lci As String = lbllead.Value
        Dim lc As String = txtlead.Value
        Dim indx As String = lblindx.Value
        sql = "update wojobtasks set leadcraft = '" & lc & "' where wojtid = '" & wojtid & "'"
        comp.Update(sql)
        If indx <> "N" Then
            jpid = lbljpid.Value
            wonum = lblwo.Value
            dghours.EditItemIndex = indx
            PopDL(jpid, wonum)
        Else
            jpid = lbljpid.Value
            wonum = lblwo.Value
            PopDL(jpid, wonum)
        End If
    End Sub
    Private Sub GetJPHead(ByVal jpid As String)
        sql = "select jpnum, description from wojobplans where wonum = '" & wonum & "' and jpid = '" & jpid & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdjpn.InnerHtml = dr.Item("jpnum").ToString
            tdjpd.InnerHtml = dr.Item("description").ToString
        End While
        dr.Close()
    End Sub
    Private Sub PopDL(ByVal pmid As String, ByVal wonum As String)
        sql = "usp_getwojplab '" & jpid & "','" & wonum & "'"
        ds = comp.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        'Try
        dghours.DataSource = dv
        dghours.DataBind()
    End Sub

    Private Sub dghours_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dghours.CancelCommand
        dghours.EditItemIndex = -1
        comp.Open()
        wonum = lblwo.Value
        jpid = lbljpid.Value
        PopDL(jpid, wonum)
        comp.Dispose()
    End Sub

    Private Sub dghours_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dghours.EditCommand
        comp.Open()
        jpid = lbljpid.Value
        wonum = lblwo.Value
        dghours.EditItemIndex = e.Item.ItemIndex
        PopDL(jpid, wonum)
        comp.Dispose()
    End Sub

    Private Sub dghours_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dghours.ItemDataBound
        Dim ibfm As ImageButton
        stat = lblstat.Value

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            If stat = "COMP" Or stat = "CAN" Or stat = "WAPPR" Then
                ibfm = CType(e.Item.FindControl("imgeditfm"), ImageButton)
                ibfm.Attributes.Add("class", "details")

            End If
            Dim img As HtmlImage = CType(e.Item.FindControl("imgti"), HtmlImage)
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "task").ToString
            'tdtsk.InnerHtml = tsk
            img.Attributes.Add("onclick", "gettsk('" & tsk & "');")

            Dim img1 As HtmlImage = CType(e.Item.FindControl("imglci"), HtmlImage)
            Dim wojtid As String = DataBinder.Eval(e.Item.DataItem, "wojtid").ToString
            Dim indx As String = "N"
            img1.Attributes.Add("onclick", "getsuper('" & indx & "','" & wojtid & "');")

            Dim lid As String = CType(e.Item.FindControl("lblsdate"), Label).ClientID
            Dim imgs As HtmlImage = CType(e.Item.FindControl("imgsdate"), HtmlImage)
            imgs.Attributes.Add("onclick", "getcal('" & lid & "','" & wojtid & "');")
        End If
        If e.Item.ItemType = ListItemType.EditItem Then
            Dim img As HtmlImage = CType(e.Item.FindControl("imgte"), HtmlImage)
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "task").ToString
            'tdtsk.InnerHtml = tsk
            img.Attributes.Add("onclick", "gettsk('" & tsk & "');")

            Dim img1 As HtmlImage = CType(e.Item.FindControl("imglce"), HtmlImage)
            Dim wojtid As String = DataBinder.Eval(e.Item.DataItem, "wojtid").ToString
            Dim indx As String = e.Item.ItemIndex
            img1.Attributes.Add("onclick", "getsuper('" & indx & "','" & wojtid & "');")

        End If

    End Sub

    Private Sub dghours_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dghours.ItemCommand
        If e.CommandName = "Tool" Then
            lbltool.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                'lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                'lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If

        End If
        If e.CommandName = "Part" Then
            lblpart.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                'lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                'lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        If e.CommandName = "Lube" Then
            lbllube.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                'lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                'lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dghours.Columns(1).HeaderText = dlabs.GetDGPage("wojpsched.aspx", "dghours", "1")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(2).HeaderText = dlabs.GetDGPage("wojpsched.aspx", "dghours", "2")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(3).HeaderText = dlabs.GetDGPage("wojpsched.aspx", "dghours", "3")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(5).HeaderText = dlabs.GetDGPage("wojpsched.aspx", "dghours", "5")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(8).HeaderText = dlabs.GetDGPage("wojpsched.aspx", "dghours", "8")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(9).HeaderText = dlabs.GetDGPage("wojpsched.aspx", "dghours", "9")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(10).HeaderText = dlabs.GetDGPage("wojpsched.aspx", "dghours", "10")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label24.Text = axlabs.GetASPXPage("wojpsched.aspx", "Label24")
        Catch ex As Exception
        End Try
        Try
            lang1516.Text = axlabs.GetASPXPage("wojpsched.aspx", "lang1516")
        Catch ex As Exception
        End Try
        Try
            lang1517.Text = axlabs.GetASPXPage("wojpsched.aspx", "lang1517")
        Catch ex As Exception
        End Try

    End Sub

End Class
