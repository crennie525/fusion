Imports System.Data.SqlClient
Imports System.Text
Public Class wolist1
    Inherits System.Web.UI.Page
    Dim mu As New mmenu_utils_a
    Dim coi As String
    Protected WithEvents lblcoi As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tmod As New transmod
    Dim mm As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim rowcnt As Integer

    Dim usid, nme, typ, eqid, fuid, coid, sid, cid, lid, did, clid, ro, rostr, Login, tab, issuper, Logged_In, ms, appstr As String
    Dim islabor, wonum, isplanner, waid, wpaid, fdate, tdate, wtret, statret, who, jump, ski As String
    Dim issched As Integer
    Protected WithEvents lblfiltret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblissched As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnme As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisplanner As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lang1548 As System.Web.UI.WebControls.Label
    Protected WithEvents lang1549 As System.Web.UI.WebControls.Label
    Protected WithEvents tdpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblhref As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents woonly As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblLogged_In As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblms As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblislabor As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lblwosort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstatsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltypesort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblschedsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllasttab As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents afwp As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents asdp As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents awob As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents dgout As System.Web.UI.WebControls.DataGrid

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lang1523 As System.Web.UI.WebControls.Label
    Protected WithEvents lang1524 As System.Web.UI.WebControls.Label
    Protected WithEvents lblrf As System.Web.UI.WebControls.Label
    Protected WithEvents lang1525 As System.Web.UI.WebControls.Label
    Protected WithEvents ddref As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lang1526 As System.Web.UI.WebControls.Label
    Protected WithEvents lang1527 As System.Web.UI.WebControls.Label
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents lang1528 As System.Web.UI.WebControls.Label
    Protected WithEvents lang1529 As System.Web.UI.WebControls.Label
    Protected WithEvents dddepts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lang1530 As System.Web.UI.WebControls.Label
    Protected WithEvents ddcells As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lang1531 As System.Web.UI.WebControls.Label
    Protected WithEvents lblloc As System.Web.UI.WebControls.Label
    Protected WithEvents lang1532 As System.Web.UI.WebControls.Label
    Protected WithEvents ddeq As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lang1533 As System.Web.UI.WebControls.Label
    Protected WithEvents ddfunc As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lang1534 As System.Web.UI.WebControls.Label
    Protected WithEvents ddcomp As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lang1535 As System.Web.UI.WebControls.Label
    Protected WithEvents txtsrchwo As System.Web.UI.WebControls.TextBox
    Protected WithEvents lang1536 As System.Web.UI.WebControls.Label
    Protected WithEvents txtsrchdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lang1537 As System.Web.UI.WebControls.Label
    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lang1538 As System.Web.UI.WebControls.Label
    Protected WithEvents ddstatus As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lang1539 As System.Web.UI.WebControls.Label
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lang1540 As System.Web.UI.WebControls.Label
    Protected WithEvents txtsup As System.Web.UI.WebControls.TextBox
    Protected WithEvents lang1541 As System.Web.UI.WebControls.Label
    Protected WithEvents txtlead As System.Web.UI.WebControls.TextBox
    Protected WithEvents lang1542 As System.Web.UI.WebControls.Label
    Protected WithEvents ddnc As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lang1543 As System.Web.UI.WebControls.Label
    Protected WithEvents txtfrom As System.Web.UI.WebControls.TextBox
    Protected WithEvents lang1544 As System.Web.UI.WebControls.Label
    Protected WithEvents txtto As System.Web.UI.WebControls.TextBox
    Protected WithEvents lang1545 As System.Web.UI.WebControls.Label
    Protected WithEvents txtstart As System.Web.UI.WebControls.TextBox
    Protected WithEvents lang1546 As System.Web.UI.WebControls.Label
    Protected WithEvents txtcomp As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents trsa As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdssa As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbssa As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents tdsa As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbsa As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents tdrf As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbrf As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents tdr1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdr2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdr4 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnext As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trsa1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbsa1 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents tdywr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents divywr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents jts As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tbwrs As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdwrs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trhide1 As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents trhide2 As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsearch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsrch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhide As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladm1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblissuper As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here


        If Not IsPostBack Then

            lblwosort.Value = "desc"
            lblstatsort.Value = "desc"
            lbltypesort.Value = "desc"
            lbleqsort.Value = "desc"
            lblschedsort.Value = "desc"
            lblsort.Value = "w.schedstart desc"

            issched = mu.Is_Sched
            lblissched.Value = issched
            coi = mu.COMPI
            lblcoi.Value = coi
            If issched = 1 And coi = "SMY" Then
                woonly.Attributes.Add("class", "plainlabel")
                afwp.Attributes.Add("onclick", "getfwp();")
                asdp.Attributes.Add("onclick", "getsdp();")
                awob.Attributes.Add("onclick", "getwob();")
            Else
                woonly.Attributes.Add("class", "details") 'plainlabelgray
                'afwp.Attributes.Add("onclick", "")
                'asdp.Attributes.Add("onclick", "")
                'awob.Attributes.Add("onclick", "")
                'afwp.Attributes.Add("class", "plainlabelgray")
                'asdp.Attributes.Add("class", "plainlabelgray")
                'awob.Attributes.Add("class", "plainlabelgray")
            End If
            Try
                ro = "1" 'HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            Try
                who = Request.QueryString("who").ToString
                lblwho.Value = who
            Catch ex As Exception
                who = ""
            End Try
            If who = "mm" Then
                divywr.Attributes.Add("class", "wolistsmall2mm")
                woonly.Attributes.Add("class", "details")
                eqid = Request.QueryString("eqid").ToString
                fuid = Request.QueryString("fuid").ToString
                coid = Request.QueryString("coid").ToString
                lbleqid.Value = eqid
                lblfuid.Value = fuid
                lblcoid.Value = coid
            End If
            If ro <> "1" Then
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = mm.CheckROS(rostr, "wo")
                    lblro.Value = ro
                End If
            End If
            sid = Request.QueryString("sid").ToString
            usid = Request.QueryString("uid").ToString
            nme = Request.QueryString("usrname").ToString
            islabor = Request.QueryString("islabor").ToString
            issuper = Request.QueryString("issuper").ToString
            isplanner = Request.QueryString("isplanner").ToString

            Logged_In = Request.QueryString("Logged_In").ToString
            lblLogged_In.Value = Logged_In
            ms = Request.QueryString("ms").ToString
            lblms.Value = ms
            appstr = Request.QueryString("appstr").ToString
            lblappstr.Value = appstr
            CheckApps(appstr)
            lblsid.Value = sid
            lblusid.Value = usid
            lblnme.Value = nme
            lblislabor.Value = islabor
            lblisplanner.Value = isplanner
            lblissuper.Value = issuper
            lbltab.Value = "wo"
            lbllasttab.Value = "wo"
            mm.Open()
            'temp
            'sql = "update workorder set leadcraftid = null, leadcraft = null where leadcraftid is not null and (leadcraft is null or len(leadcraft) = 0)"
            'mm.Update(sql)
            'end temp
            
            If coi = "NISS_SYM" Or coi = "NISS" Then
                sql = "exec usp_smy_pmwoclean"
                mm.Update(sql)
            End If

            BuildMyWR(PageNumber)
            mm.Dispose()
            ddref.Attributes.Add("onchange", "refref();")
        Else
            If Request.Form("lblret") = "filt" Then
                lblret.Value = ""
                mm.Open()
                tab = lbltab.Value
                BuildMyWR(PageNumber, tab)
                mm.Dispose()
            ElseIf Request.Form("lblret") = "pm" Then
                mm.Open()
                BuildMyWR(PageNumber, "pm")
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "wo" Then
                mm.Open()
                BuildMyWR(PageNumber, "wo")
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "next" Then
                mm.Open()
                GetNext()
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                mm.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                tab = lbltab.Value
                BuildMyWR(PageNumber, tab)
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                mm.Open()
                GetPrev()
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                mm.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                tab = lbltab.Value
                BuildMyWR(PageNumber, tab)
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "dosort" Then
                mm.Open()
                PageNumber = txtpg.Value
                tab = lbltab.Value
                BuildMyWR(PageNumber, tab)
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "excel" Then
                mm.Open()
                BindExport()
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "sa" Then
                mm.Open()
                PageNumber = txtpg.Value
                tab = lbltab.Value
                BuildMyWR(PageNumber, tab)
                mm.Dispose()
                lblret.Value = ""
            End If
            appstr = lblappstr.Value
            CheckApps(appstr)
        End If
    End Sub
    Private Sub BindExport()
        Filter = lblfilt.Value
        Sort = "w.wonum"
        sql = "usp_getwolist_excel '" & Filter & "','" & Sort & "'"
        Dim ds As New DataSet
        ds = mm.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
    Private Sub CheckApps(ByVal appstr As String)
        Dim apparr() As String = appstr.Split(",")
        Dim o As Integer = 1
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                Select Case apparr(i)
                    Case "sch"
                        o = 0
                End Select
            Next
        Else
            o = 0
        End If
        If o <> 0 Then
            jts.Attributes.Add("class", "details")
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            tab = lbltab.Value
            BuildMyWR(PageNumber, tab)
        Catch ex As Exception
            mm.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "AppSetAssetClass.aspx.vb")

            mm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            tab = lbltab.Value
            BuildMyWR(PageNumber, tab)
        Catch ex As Exception
            mm.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "AppSetAssetClass.aspx.vb")

            mm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Function checkloc(ByVal lid As String, ByVal jl As String) As String
        Dim ret As String
        If jl = "0" Then
            sql = "usp_lookup_locs_eq '" & lid & "'"
        Else
            sql = "usp_lookup_locs '" & lid & "'"
        End If

        dr = mm.GetRdrData(sql)
        While dr.Read
            ret = dr.Item("retstring").ToString
        End While
        dr.Close()
        Return ret
    End Function
    Private Sub CheckRF()
        typ = lbltyp.Value
        tab = lbltab.Value

        If typ <> "wr" And tab <> "yes" Then
            If cbrf.Checked = True Then
                lblrefresh.Value = "yes"
                tdr1.Attributes.Add("class", "bluelabel")
                tdr4.Attributes.Add("class", "bluelabel")
                ddref.Enabled = True
                lblrf.Text = "Stop Refresh"
            Else
                lblrefresh.Value = "no"
                tdr1.Attributes.Add("class", "graylabel")
                tdr4.Attributes.Add("class", "graylabel")
                ddref.Enabled = False
                lblrf.Text = "Start Refresh"
            End If
        End If
    End Sub
    Private Sub BuildMyWR(ByVal PageNumber As Integer, Optional ByVal who As String = "wo")
        Dim sb As New StringBuilder
        Dim field, srch As String
        Dim srchi As Integer
        usid = lblusid.Value '"124" 'HttpContext.Current.Session("userid").ToString()

        nme = lblnme.Value '"Lab Sched1" 'HttpContext.Current.Session("username").ToString()

        typ = lbltyp.Value
        tab = lbltab.Value
        islabor = lblislabor.Value '"0" 'HttpContext.Current.Session("islabor").ToString()
        isplanner = lblisplanner.Value '"1" 'HttpContext.Current.Session("islabor").ToString()
        issuper = lblissuper.Value
        issched = lblissched.Value
        'did + "~" + clid + "~" + lid + "~" + eqid + "~" + fuid + "~" + coid + "~" + rettyp + "~" + level;
        'waid + "~" + wpaid + "~" + fdate + "~" + tdate + "~" + wt + "~" + stat + "~" + wo + "~" + wastr + "~" + wpstr + "~" + rt + "~" + rs + "~" + ra;
        Dim rt, rs, ra, sa, sup, lead, did, clid, lid, rettyp, level, lstring, jl, fpc, co, reti, woreti, ua, rtid As String
        Dim filtret As String = lblfiltret.Value
        Dim woret As String
        Dim datret As String = "0"
        woreti = "0"
        Dim wa As String = "1"
        If filtret <> "" Then
            Dim filtarr() As String = filtret.Split("~")
            waid = filtarr(0).ToString
            wpaid = filtarr(1).ToString
            fdate = filtarr(2).ToString
            tdate = filtarr(3).ToString
            wtret = filtarr(4).ToString
            statret = filtarr(5).ToString
            woret = filtarr(6).ToString

            rt = filtarr(9).ToString
            rs = filtarr(10).ToString
            ra = filtarr(11).ToString
            sa = filtarr(12).ToString
            wa = filtarr(13).ToString
            sup = filtarr(14).ToString
            lead = filtarr(15).ToString
            did = filtarr(16).ToString
            clid = filtarr(17).ToString
            lid = filtarr(18).ToString
            eqid = filtarr(19).ToString
            fuid = filtarr(20).ToString
            coid = filtarr(21).ToString
            rettyp = filtarr(22).ToString
            level = filtarr(23).ToString
            jl = filtarr(24).ToString
            fpc = filtarr(25).ToString
            co = filtarr(26).ToString
            ski = filtarr(27).ToString
            reti = filtarr(28).ToString
            ua = filtarr(29).ToString
            If eqid = "" Then
                If rettyp = "depts" Then
                    If Filter <> "" Then
                        Filter += " and w.deptid = ''" & did & "'' "
                        FilterCNT += " and w.deptid = '" & did & "' "
                    Else
                        Filter = "w.deptid = ''" & did & "'' "
                        FilterCNT = "w.deptid = '" & did & "' "
                    End If
                    If clid <> "" Then
                        If Filter <> "" Then
                            Filter += " and w.cellid = ''" & clid & "'' "
                            FilterCNT += " and w.cellid = '" & clid & "' "
                        Else
                            Filter = "w.cellid = ''" & clid & "'' "
                            FilterCNT = "w.cellid = '" & clid & "' "
                        End If
                    End If
                ElseIf rettyp = "locs" Then
                    If level <> 1 Then
                        lstring = checkloc(lid, jl)
                        If Filter <> "" Then
                            Filter += " and w.locid in (" & lstring & ") "
                            FilterCNT += " and w.locid in (" & lstring & ") "
                        Else
                            Filter = " w.locid in (" & lstring & ") "
                            FilterCNT = " w.locid in (" & lstring & ") "
                        End If
                    End If

                End If
            End If
            If fpc <> "" Then
                fpc = Replace(fpc, "'", Chr(180), , , vbTextCompare)
                If Filter <> "" Then
                    Filter += " and w.eqid in (select e.eqid from equipment e where e.fpc like ''%" & fpc & "%'') "
                    FilterCNT += " and w.eqid in (select e.eqid from equipment e where e.fpc like '%" & fpc & "%') "
                Else
                    Filter += " w.eqid in (select e.eqid from equipment e where e.fpc like ''%" & fpc & "%'') "
                    FilterCNT += " w.eqid in (select e.eqid from equipment e where e.fpc like '%" & fpc & "%') "
                End If

            End If
            If co <> "" And co <> "0" Then
                If Filter <> "" Then
                    Filter += " and w.eqid in (select e.eqid from equipment e where e.critical = ''1'') "
                    FilterCNT += " and w.eqid in (select e.eqid from equipment e where  e.critical = '1') "
                Else
                    Filter += " w.eqid in (select e.eqid from equipment e where  e.critical = ''1'') "
                    FilterCNT += " w.eqid in (select e.eqid from equipment e where  e.critical = '1') "
                End If
            End If

            If rt = "1" Or rs = "1" Or ra = "1" Then
                datret = "1"
            End If
            'put OR flag here for multile date type selections
            Dim mdates As Integer = 0
            If rt = "1" And rs = "1" And ra = "1" Then
                mdates = 3
            ElseIf rt = "1" And rs = "1" And ra = "0" Then
                mdates = 12
            ElseIf rt = "1" And rs = "0" And ra = "1" Then
                mdates = 13
            ElseIf rt = "0" And rs = "1" And ra = "1" Then
                mdates = 23
            End If

            'targ
            If fdate <> "" Then
                If rt = "1" Then
                    If Filter <> "" Then
                        Filter += "and targstartdate >= ''" & fdate & "'' "
                        FilterCNT += "and targstartdate >= '" & fdate & "' "
                    Else
                        Filter = "targstartdate >= ''" & fdate & "'' "
                        FilterCNT = "targstartdate >= '" & fdate & "' "
                    End If
                End If
            End If
            If tdate <> "" Then
                If rt = "1" And who <> "pm" Then
                    If Filter <> "" Then
                        Filter += "and targcompdate <= ''" & tdate & "'' "
                        FilterCNT += "and targcompdate <= '" & tdate & "' "
                    Else
                        Filter = "targcompdate <= ''" & tdate & "'' "
                        FilterCNT = "targcompdate <= '" & tdate & "' "
                    End If
                ElseIf rt = "1" And who = "pm" Then
                    If Filter <> "" Then
                        Filter += "and targstartdate <= ''" & tdate & "'' "
                        FilterCNT += "and targstartdate <= '" & tdate & "' "
                    Else
                        Filter = "targstartdate <= ''" & tdate & "'' "
                        FilterCNT = "targstartdate <= '" & tdate & "' "
                    End If
                End If
            End If

            'sched
            If fdate <> "" Then
                If rs = "1" Then
                    If Filter <> "" Then
                        Filter += "and schedstart >= ''" & fdate & "'' "
                        FilterCNT += "and schedstart >= '" & fdate & "' "
                    Else
                        Filter = "schedstart >= ''" & fdate & "'' "
                        FilterCNT = "schedstart >= '" & fdate & "' "
                    End If
                End If
            End If
            If tdate <> "" Then
                If rs = "1" And who <> "pm" Then
                    If Filter <> "" Then
                        Filter += "and schedfinish <= ''" & tdate & "'' "
                        FilterCNT += "and schedfinish <= '" & tdate & "' "
                    Else
                        Filter = "schedfinish <= ''" & tdate & "'' "
                        FilterCNT = "schedfinish <= '" & tdate & "' "
                    End If
                ElseIf rs = "1" And who = "pm" Then
                    If Filter <> "" Then
                        Filter += "and schedstart <= ''" & tdate & "'' "
                        FilterCNT += "and schedstart <= '" & tdate & "' "
                    Else
                        Filter = "schedstart <= ''" & tdate & "'' "
                        FilterCNT = "schedstart <= '" & tdate & "' "
                    End If
                End If
            End If

            'actual
            If fdate <> "" Then
                If ra = "1" And who <> "pm" Then
                    If Filter <> "" Then
                        Filter += "and actstart >= ''" & fdate & "'' "
                        FilterCNT += "and actstart >= '" & fdate & "' "
                    Else
                        Filter = "actstart >= ''" & fdate & "'' "
                        FilterCNT = "actstart >= '" & fdate & "' "
                    End If
                ElseIf ra = "1" And who = "pm" Then
                    If Filter <> "" Then
                        Filter += "and actfinish >= ''" & fdate & "'' "
                        FilterCNT += "and actfinish >= '" & fdate & "' "
                    Else
                        Filter = "actfinish >= ''" & fdate & "'' "
                        FilterCNT = "actfinish >= '" & fdate & "' "
                    End If
                End If
            End If
            If tdate <> "" Then
                If ra = "1" Then
                    If Filter <> "" Then
                        Filter += "and actfinish <= ''" & tdate & "'' "
                        FilterCNT += "and actfinish <= '" & tdate & "' "
                    Else
                        Filter = "actfinish <= ''" & tdate & "'' "
                        FilterCNT = "actfinish <= '" & tdate & "' "
                    End If
                End If
            End If

            'status
            If statret <> "" Then
                If Filter <> "" Then
                    Filter += " and w.status = ''" & statret & "''"
                    FilterCNT += " and w.status = '" & statret & "'"
                Else
                    Filter = " w.status = ''" & statret & "''"
                    FilterCNT = " w.status = '" & statret & "'"
                End If

            End If


            If woret <> "" Then
                If Filter <> "" Then
                    Filter += " and w.wonum = ''" & woret & "''"
                    FilterCNT += " and w.wonum = '" & woret & "'"
                Else
                    Filter = " w.wonum = ''" & woret & "''"
                    FilterCNT = " w.wonum = '" & woret & "'"
                End If
                woreti = "1"
            Else
                If ua = "1" Then
                    woreti = "1"
                End If
            End If

            '***change from wpaid to plannerid
            If wpaid <> "" Then
                If Filter <> "" Then
                    Filter += " and w.plannerid = ''" & wpaid & "''"
                    FilterCNT += " and w.plannerid = '" & wpaid & "'"
                Else
                    Filter = " w.plannerid = ''" & wpaid & "''"
                    FilterCNT = " w.plannerid = '" & wpaid & "'"
                End If

            End If

            Dim wtrets1, wtrets2 As String
            If wtret <> "" Then
                Dim wtretarr() As String = wtret.Split(",")
                Dim wi As Integer
                For wi = 0 To wtretarr.Length - 1
                    If wtrets1 = "" Then
                        wtrets1 = "''" & wtretarr(wi) & "''"
                        wtrets2 = "'" & wtretarr(wi) & "'"
                    Else
                        wtrets1 += ",''" & wtretarr(wi) & "''"
                        wtrets2 += ",'" & wtretarr(wi) & "'"
                    End If
                Next
                If Filter <> "" Then
                    'Filter += " and w.worktype = ''" & wtret & "''"
                    'FilterCNT += " and w.worktype = '" & wtret & "'"
                    Filter += " and w.worktype in (" & wtrets1 & ")"
                    FilterCNT += " and w.worktype in (" & wtrets2 & ")"
                Else
                    'Filter = " w.worktype = ''" & wtret & "''"
                    'FilterCNT = " w.worktype = '" & wtret & "'"
                    Filter = " w.worktype in (" & wtrets1 & ")"
                    FilterCNT = " w.worktype in (" & wtrets2 & ")"
                End If

            End If

            If sup <> "" Then
                If Filter <> "" Then
                    Filter += "and w.superid = ''" & sup & "'' "
                    FilterCNT += "and w.superid = '" & sup & "' "
                Else
                    Filter += " w.superid = ''" & sup & "'' "
                    FilterCNT += " w.superid = '" & sup & "' "
                End If
            End If

            If lead <> "" Then
                If Filter <> "" Then
                    Filter += "and w.leadcraftid = ''" & lead & "'' "
                    FilterCNT += "and w.leadcraftid = '" & lead & "' "
                Else
                    Filter += " w.leadcraftid = ''" & lead & "'' "
                    FilterCNT += " w.leadcraftid = '" & lead & "' "
                End If
            End If

            If ski <> "" Then
                If Filter <> "" Then
                    Filter += "and w.skillid = ''" & ski & "'' "
                    FilterCNT += "and w.skillid = '" & ski & "' "
                Else
                    Filter += " w.skillid = ''" & ski & "'' "
                    FilterCNT += " w.skillid = '" & ski & "' "
                End If
            End If

            If sa = "0" Then
                If islabor = "1" Then
                    sql = "select userid from pmsysusers where uid = '" & usid & "'"
                    usid = mm.strScalar(sql)
                    If issched = "1" And wtret <> "TPM" Then
                        If Filter <> "" Then
                            Filter += " and a.userid = ''" & usid & "''"
                            FilterCNT += " and a.userid = '" & usid & "'"
                        Else
                            Filter = " a.userid = ''" & usid & "''"
                            FilterCNT = " a.userid = '" & usid & "'"
                        End If

                    End If
                ElseIf isplanner = "1" Then

                ElseIf issuper = "1" Then

                End If
            End If

            If waid <> "" Then
                If wa = "1" Then
                    If Filter <> "" Then
                        Filter += " and w.waid = ''" & waid & "''"
                        FilterCNT += " and w.waid = '" & waid & "'"
                    Else
                        Filter = " w.waid = ''" & waid & "''"
                        FilterCNT = " w.waid = '" & waid & "'"
                    End If
                Else
                    If Filter <> "" Then
                        'Filter += " and a.userid in (select userid from pmsysusers where waid = ''" & waid & "'')"
                        'FilterCNT += " and a.userid in (select userid from pmsysusers where waid = '" & waid & "')"
                    Else
                        'Filter = " a.userid in (select userid from pmsysusers where waid = ''" & waid & "'')"
                        'FilterCNT = " a.userid in (select userid from pmsysusers where waid = '" & waid & "')"
                    End If
                End If


            End If
        Else
            sa = "0"
            If islabor = "1" Then
                sql = "select userid from pmsysusers where uid = '" & usid & "'"
                usid = mm.strScalar(sql)
                If issched = "1" Then
                    Filter = " a.userid = ''" & usid & "''"
                    FilterCNT = " a.userid = '" & usid & "'"
                End If
            ElseIf isplanner = "1" Then

            ElseIf issuper = "1" Then

            End If

        End If
        sid = lblsid.Value
        If Filter = "" Or (filtret = "" And islabor = 1) Then
            If who = "wo" Then
                If Filter = "" Then
                    If woreti = "0" Then
                        Filter = " w.status not in (''COMP'',''CLOSE'',''CANCEL'') and ((w.worktype <> ''PM'' and w.worktype <> ''TPM'' and w.worktype <> ''MAXPM'') or w.worktype is null) and w.siteid = ''" & sid & "''"
                        FilterCNT = " w.status not in ('COMP','CLOSE','CANCEL') and ((w.worktype <> 'PM' and w.worktype <> 'TPM' and w.worktype <> 'MAXPM') or w.worktype is null) and w.siteid = '" & sid & "'"
                    Else
                        Filter = " ((w.worktype <> ''PM'' and w.worktype <> ''TPM'' and w.worktype <> ''MAXPM'') or w.worktype is null) and w.siteid = ''" & sid & "''"
                        FilterCNT = " ((w.worktype <> 'PM' and w.worktype <> 'TPM' and w.worktype <> 'MAXPM') or w.worktype is null) and w.siteid = '" & sid & "'"
                    End If
                    
                Else
                    If woreti = "0" Then
                        Filter += " and w.status not in (''COMP'',''CLOSE'',''CANCEL'') and ((w.worktype <> ''PM'' and w.worktype <> ''TPM'' and w.worktype <> ''MAXPM'') or w.worktype is null) and w.siteid = ''" & sid & "''"
                        FilterCNT += " and w.status not in ('COMP','CLOSE','CANCEL') and ((w.worktype <> 'PM' and w.worktype <> 'TPM' and w.worktype <> 'MAXPM') or w.worktype is null) and w.siteid = '" & sid & "'"
                    Else
                        Filter += " and ((w.worktype <> ''PM'' and w.worktype <> ''TPM'' and w.worktype <> ''MAXPM'') or w.worktype is null) and w.siteid = ''" & sid & "''"
                        FilterCNT += " and ((w.worktype <> 'PM' and w.worktype <> 'TPM' and w.worktype <> 'MAXPM') or w.worktype is null) and w.siteid = '" & sid & "'"
                    End If
                    
                End If

                tdpm.Attributes.Add("class", "thdrhovmini plainlabel")
                tdeq.Attributes.Add("class", "thdrmini plainlabel")
            ElseIf who = "pm" Then
                If Filter = "" Then
                    If woreti = "0" Then
                        Filter = " w.status not in (''COMP'',''CLOSE'',''CANCEL'') and (w.worktype = ''PM'' or w.worktype = ''TPM'' or w.worktype = ''MAXPM'') and (w.pmid is not null or w.tpmid is not null) and w.siteid = ''" & sid & "''"
                        FilterCNT = " w.status not in ('COMP','CLOSE','CANCEL') and (w.worktype = 'PM' or w.worktype = 'TPM' or w.worktype = 'MAXPM') and (w.pmid is not null or w.tpmid is not null) and w.siteid = '" & sid & "'"
                    Else
                        Filter = " (w.worktype = ''PM'' or w.worktype = ''TPM'' or w.worktype = ''MAXPM'') and (w.pmid is not null or w.tpmid is not null) and w.siteid = ''" & sid & "''"
                        FilterCNT = " (w.worktype = 'PM' or w.worktype = 'TPM' or w.worktype = 'MAXPM') and (w.pmid is not null or w.tpmid is not null) and w.siteid = '" & sid & "'"
                    End If
                    
                Else
                    If woreti = "0" Then
                        Filter += " and w.status not in (''COMP'',''CLOSE'',''CANCEL'') and (w.worktype = ''PM'' or w.worktype = ''TPM'' or w.worktype = ''MAXPM'') and (w.pmid is not null or w.tpmid is not null) and w.siteid = ''" & sid & "''"
                        FilterCNT += " and w.status not in ('COMP','CLOSE','CANCEL') and (w.worktype = 'PM' or w.worktype = 'TPM' or w.worktype = 'MAXPM') and (w.pmid is not null or w.tpmid is not null) and w.siteid = '" & sid & "'"
                    Else
                        Filter += " and (w.worktype = ''PM'' or w.worktype = ''TPM'' or w.worktype = ''MAXPM'') and (w.pmid is not null or w.tpmid is not null) and w.siteid = ''" & sid & "''"
                        FilterCNT += " and (w.worktype = 'PM' or w.worktype = 'TPM' or w.worktype = 'MAXPM') and (w.pmid is not null or w.tpmid is not null) and w.siteid = '" & sid & "'"
                    End If
                    
                End If

                tdpm.Attributes.Add("class", "thdrmini plainlabel")
                tdeq.Attributes.Add("class", "thdrhovmini plainlabel")
            End If
        Else

            If who = "wo" Then
                Filter += " and ((w.worktype <> ''PM'' and w.worktype <> ''TPM'' and w.worktype <> ''MAXPM'') or w.worktype is null) and w.siteid = ''" & sid & "''"
                FilterCNT += " and ((w.worktype <> 'PM' and w.worktype <> 'TPM' and w.worktype <> 'MAXPM') or w.worktype is null) and w.siteid = '" & sid & "'"
                tdpm.Attributes.Add("class", "thdrhovmini plainlabel")
                tdeq.Attributes.Add("class", "thdrmini plainlabel")
                If filtret <> "" Then
                    If statret = "" Then
                        If woreti = "0" Then
                            Filter += " and w.status not in (''COMP'',''CLOSE'',''CANCEL'')"
                            FilterCNT += " and w.status not in ('COMP','CLOSE','CANCEL')"
                        End If

                    End If
                End If
            ElseIf who = "pm" Then
                If wtret = "" Or (wtret <> "TPM" And wtret <> "PM") Then
                    Filter += " and (w.worktype = ''PM'' or w.worktype = ''TPM'' or w.worktype = ''MAXPM'') and (w.pmid is not null or w.tpmid is not null) and w.siteid = ''" & sid & "''"
                    FilterCNT += " and (w.worktype = 'PM' or w.worktype = 'TPM' or w.worktype = 'MAXPM') and (w.pmid is not null or w.tpmid is not null) and w.siteid = '" & sid & "'"
                    tdpm.Attributes.Add("class", "thdrmini plainlabel")
                    tdeq.Attributes.Add("class", "thdrhovmini plainlabel")
                    If filtret <> "" Then
                        If statret = "" Then
                            If woreti = "0" Then
                                Filter += " and w.status not in (''COMP'',''CLOSE'',''CANCEL'')"
                                FilterCNT += " and w.status not in ('COMP','CLOSE','CANCEL')"
                            End If
                        End If
                    End If
                End If

            End If


        End If
        'eqid = lbleqid.Value
        'fuid = lblfuid.Value
        'coid = lblcoid.Value
        If eqid <> "" Then
            If Filter <> "" Then
                Filter += " and w.eqid = ''" & eqid & "''"
                FilterCNT += " and w.eqid = '" & eqid & "'"
            Else
                Filter = " w.eqid= ''" & eqid & "''"
                FilterCNT = " w.eqid = '" & eqid & "'"
            End If
        End If
        If fuid <> "" Then
            If Filter <> "" Then
                Filter += " and (w.funcid = ''" & fuid & "'' or w.eqid in (select t1.eqid from pmtrack t1 where t1.funcid = ''" & fuid & "'' and t1.pmid = w.pmid) or w.eqid in (select t1.eqid from pmtracktpm t1 where t1.funcid = ''" & fuid & "'' and t1.pmid = w.tpmid))"
                FilterCNT += " and (w.funcid = '" & fuid & "' or w.eqid in (select t1.eqid from pmtrack t1 where t1.funcid = '" & fuid & "' and t1.pmid = w.pmid) or w.eqid in (select t1.eqid from pmtracktpm t1 where t1.funcid = '" & fuid & "' and t1.pmid = w.tpmid))"
            Else
                Filter = " (w.funcid = ''" & fuid & "'' or w.eqid in (select t1.eqid from pmtrack t1 where t1.funcid = ''" & fuid & "'' and t1.pmid = w.pmid) or w.eqid in (select t1.eqid from pmtracktpm t1 where t1.funcid = ''" & fuid & "'' and t1.pmid = w.tpmid))"
                FilterCNT = " (w.funcid = '" & fuid & "' or w.eqid in (select t1.eqid from pmtrack t1 where t1.funcid = '" & fuid & "' and t1.pmid = w.pmid) or w.eqid in (select t1.eqid from pmtracktpm t1 where t1.funcid = '" & fuid & "' and t1.pmid = w.pmid))"
            End If
        End If
        If coid <> "" Then
            If Filter <> "" Then
                Filter += " and (w.comid = ''" & coid & "'' or w.eqid in (select t1.eqid from pmtrack t1 where t1.comid = ''" & coid & "'' and t1.pmid = w.pmid) or w.eqid in (select t1.eqid from pmtracktpm t1 where t1.comid = ''" & coid & "'' and t1.pmid = w.tpmid))"
                FilterCNT += " and (w.comid = '" & coid & "' or w.eqid in (select t1.eqid from pmtrack t1 where t1.comid = '" & coid & "' and t1.pmid = w.pmid) or w.eqid in (select t1.eqid from pmtracktpm t1 where t1.comid = '" & coid & "' and t1.pmid = w.pmid))"
            Else
                Filter = " (w.comid = ''" & coid & "'' or w.eqid in (select t1.eqid from pmtrack t1 where t1.comid = ''" & coid & "'' and t1.pmid = w.pmid) or w.eqid in (select t1.eqid from pmtracktpm t1 where t1.comid = ''" & coid & "'' and t1.pmid = w.tpmid))"
                FilterCNT = " (w.comid = '" & coid & "' or w.eqid in (select t1.eqid from pmtrack t1 where t1.comid = '" & coid & "' and t1.pmid = w.pmid) or w.eqid in (select t1.eqid from pmtracktpm t1 where t1.comid = '" & coid & "' and t1.pmid = w.tpmid))"
            End If
        End If
        'If Filter <> "" Then
        'Filter += " and w.eqnum <> ''DELETED''"
        'FilterCNT += " and w.eqnum <> 'DELETED'"
        'Else
        '    Filter += " w.eqnum <> ''DELETED''"
        '    FilterCNT += " w.eqnum <> 'DELETED'"
        'End If

        'If Filter <> "" Then
        'Filter += " and w.status <> ''HOLD''"
        'FilterCNT += " and w.status <> 'HOLD'"
        'Else
        'Filter += "w.status <> ''HOLD''"
        'FilterCNT += "w.status <> 'HOLD'"
        'End If

        If islabor = "1" And issched = "0" Then
            If Filter <> "" Then
                Filter += " and (w.eqid in (select eqid from pmlaborlocs where laborid = ''" & usid & "'') or " _
                    + "w.eqid in (select eqid from workorder where leadcraftid = ''" & usid & "''))"
                FilterCNT += " and (w.eqid in (select eqid from pmlaborlocs where laborid = '" & usid & "') or " _
                    + "w.eqid in (select eqid from workorder where leadcraftid = '" & usid & "'))"
            Else
                Filter += " (w.eqid in (select eqid from pmlaborlocs where laborid = ''" & usid & "'') or " _
                   + "w.eqid in (select eqid from workorder where leadcraftid = ''" & usid & "''))"
                FilterCNT += " (w.eqid in (select eqid from pmlaborlocs where laborid = '" & usid & "') or " _
                    + "w.eqid in (select eqid from workorder where leadcraftid = '" & usid & "'))"
            End If
        ElseIf islabor = "1" And issched = "1" And sa <> "1" And wa = "1" Then
            'w.worktype = ''PM'' or 
            'w.worktype = 'PM' or 
            If who = "wo" Then
                'Filter += " and w.status not in (''COMP'',''CLOSE'',''CANCEL'')"
                'FilterCNT += " and w.status not in ('COMP','CLOSE','CANCEL')"
                'Filter += " or (w.eqid in (select eqid from pmlaborlocs where laborid = ''" & usid & "'') or " _
                '   + "w.eqid in (select eqid from workorder where leadcraftid = ''" & usid & "'') and ((w.worktype <> ''PM'' and w.worktype <> ''TPM'') or w.worktype is null))"
                'FilterCNT += " or (w.eqid in (select eqid from pmlaborlocs where laborid = '" & usid & "') or " _
                '    + "w.eqid in (select eqid from workorder where leadcraftid = '" & usid & "') and ((w.worktype <> 'PM' and w.worktype <> 'TPM') or w.worktype is null))"
            ElseIf who = "pm" Then
                'select wonum, status, worktype, eqid, leadcraftid from workorder 
                'where ((leadcraftid = '17' or eqid in (select eqid from pmlaborlocs where laborid = '17')) and worktype = 'TPM')
                'and status <> 'HOLD' and status not in ('COMP','CLOSE','CANCEL')
                If wtret = "TPM" Then
                    Filter += " and (((w.leadcraftid = ''" & usid & "'' or w.eqid in (select eqid from pmlaborlocs where laborid = ''" & usid & "'')) and worktype = ''TPM''))"
                    FilterCNT += " and (((w.leadcraftid = '" & usid & "' or w.eqid in (select eqid from pmlaborlocs where laborid = '" & usid & "')) and worktype = 'TPM'))"
                    If filtret <> "" Then
                        If statret = "" Then
                            If woreti = "0" Then
                                Filter += " and w.status not in (''COMP'',''CLOSE'',''CANCEL'')"
                                FilterCNT += " and w.status not in ('COMP','CLOSE','CANCEL')"
                            End If
                        End If
                    End If
                ElseIf wtret = "PM" Then
                    If filtret <> "" Then
                        If statret = "" Then
                            If woreti = "0" Then
                                Filter += " and w.status not in (''COMP'',''CLOSE'',''CANCEL'')"
                                FilterCNT += " and w.status not in ('COMP','CLOSE','CANCEL')"
                            End If
                        End If
                    End If
                ElseIf wtret = "MAXPM" Then
                    If filtret <> "" Then
                        If statret = "" Then
                            If woreti = "0" Then
                                Filter += " and w.status not in (''COMP'',''CLOSE'',''CANCEL'')"
                                FilterCNT += " and w.status not in ('COMP','CLOSE','CANCEL')"
                            End If
                        End If
                    End If
                Else
                    If wtret = "" Then
                        If datret = "0" Then
                            Filter += " or (((w.leadcraftid = ''" & usid & "'' or w.eqid in (select eqid from pmlaborlocs where laborid = ''" & usid & "'')) and worktype = ''TPM''))"
                        Else
                            Filter += " or (((w.leadcraftid = ''" & usid & "'' or w.eqid in (select eqid from pmlaborlocs where laborid = ''" & usid & "'')) and worktype = ''TPM'' "
                            'rem1  

                        End If
                        If statret = "" Then
                            If woreti = "0" Then
                                FilterCNT += " and w.status not in ('COMP','CLOSE','CANCEL')"
                            End If
                        End If

                    End If

                End If

            End If

            'FilterCNT += " or (w.eqid in (select eqid from pmlaborlocs where laborid = '" & usid & "') or " _
            '    + "w.eqid in (select eqid from workorder where leadcraftid = '" & usid & "') and ((w.worktype = 'TPM') and (w.pmid is not null or w.tpmid is not null)) and w.status not in ('COMP','CLOSE','CANCEL'))"
        End If


        lblfilt.Value = Filter



        'SELECT w.wonum, w.status, w.worktype, w.description, w.schedstart, w.schedfinish
        '<a href=""#"" onclick=""sortstart();"">Scheduled</a></td> KEEP THIS FOR FUTURE SORT OPTION
        sb.Append("<table cellpadding=""2"" cellspacing=""2"" width=""580"">" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""60"" height=""20px""><a href=""#"" onclick=""getsort('wo');"">WO#</a></td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""50"" ><a href=""#"" onclick=""getsort('stat');"">Status</a></td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""60"" ><a href=""#"" onclick=""getsort('type');"" onclick=""getsort('type');"">Type</a></td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""140""><a href=""#"" onclick=""getsort('eq');"" onclick=""getsort('eq');"">Equipment</a></td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""200"">Description</td>" & vbCrLf)

        If filtret <> "" And datret = "1" Then
            If rt = "1" Then
                sb.Append("<td class=""thdrsingg plainlabel"" width=""70""><a href=""#"" onclick=""getsort('targ');"">Target</a></td>" & vbCrLf)
            ElseIf ra = "1" Then
                sb.Append("<td class=""thdrsingg plainlabel"" width=""70""><a href=""#"" onclick=""getsort('act');"">Actual</a></td>" & vbCrLf)
            Else
                sb.Append("<td class=""thdrsingg plainlabel"" width=""70""><a href=""#"" onclick=""getsort('sched');"">Scheduled</a></td>" & vbCrLf)
            End If
        Else
            sb.Append("<td class=""thdrsingg plainlabel"" width=""70""><a href=""#"" onclick=""getsort('sched');"">Scheduled</a></td>" & vbCrLf)
        End If

        sb.Append("<td class=""thdrsingg plainlabel"" width=""20""><img src=""../images/appbuttons/minibuttons/printx.gif"" border=""0""></td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        Dim wt, wo, stat, bg, isdown, isdownp, desc, ss, sc, eq, tm As String
        Dim tsd, asd, tcd, acd As String
        Dim rowflag As Integer = 0
        'NEED SORT SELECTION
        Dim wosort, statsort, typesort, eqsort, schedsort, dosort As String
        dosort = lblsort.Value
        Sort = dosort

        '*****
        Dim intPgNav, intPgCnt As Integer
        If issched = "0" Then
            sql = "SELECT Count(distinct w.wonum) FROM workorder w where " & FilterCNT
        Else
            sql = "SELECT Count(distinct w.wonum) FROM workorder w left join woassign a on a.wonum = w.wonum where " & FilterCNT 'left join woassign a on a.wonum = w.wonum
        End If
        Dim str As String
        Try
            intPgCnt = mm.Scalar(sql)
            'str = Replace(sql, "'", Chr(180), , , vbTextCompare)
            'Dim strMessage As String = str
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Catch ex As Exception
            str = Replace(sql, "'", Chr(180), , , vbTextCompare)
            Dim strMessage As String = str
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try

        PageSize = "50"
        intPgNav = mm.PageCountRev(intPgCnt, PageSize)
        'added for refresh
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If
        '*****
        Try
            If islabor = "1" And sa = "0" Then
                If issched = "1" Then
                    sql = "usp_getwolist_labor '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "'"
                Else
                    sql = "usp_getwolist_labor_ns '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "'"
                End If
            ElseIf isplanner = "1" Then
                If wa = "0" Then
                    sql = "usp_getwolist_plnr '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "'"
                Else
                    sql = "usp_getwolist_plnra '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "'"
                End If
            Else
                If wa = "1" Then
                    sql = "usp_getwolist_plnr '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "'"
                Else
                    sql = "usp_getwolist_plnra '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "'"
                End If

            End If
        Catch ex As Exception
            str = Replace(sql, "'", Chr(180), , , vbTextCompare)
            Dim strMessage As String = str
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try

        Dim jp, pm, lc As String
        Dim pgadj As Integer = 0
        Try
            dr = mm.GetRdrData(sql)
        Catch ex As Exception
            str = Replace(sql, "'", Chr(180), , , vbTextCompare)
            Dim strMessage As String = str
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try

        If dr.HasRows Then
            While dr.Read
                If rowflag = 0 Then
                    bg = "ptransrow"
                    rowflag = 1
                Else
                    bg = "ptransrowblue"
                    rowflag = "0"
                End If

                isdown = dr.Item("isdown").ToString
                isdownp = dr.Item("isdownp").ToString
                rtid = dr.Item("rtid").ToString
                Dim cr, crl As String
                If isdown = "1" Or isdownp = "1" Then
                    cr = "plainlabelred"
                    crl = "A1R"
                Else
                    cr = "plainlabel"
                    crl = "A1U"
                End If
                Dim hnd As String = "hand"
                tsd = dr.Item("targstartdate").ToString
                asd = dr.Item("actstart").ToString
                tcd = dr.Item("targcompdate").ToString
                acd = dr.Item("actfinish").ToString

                wt = dr.Item("worktype").ToString
                wo = dr.Item("wonum").ToString
                eq = dr.Item("eqnum").ToString
                eqid = dr.Item("eqid").ToString
                stat = dr.Item("status").ToString
                desc = dr.Item("description").ToString
                ss = dr.Item("schedstart").ToString
                sc = dr.Item("schedfinish").ToString
                jp = dr.Item("jpid").ToString
                pm = dr.Item("pmid").ToString
                tm = dr.Item("tpmid").ToString
                lc = dr.Item("leadcraftid").ToString
                Dim tsdd As DateTime
                Try
                    tsdd = tsd
                    tsdd = tsdd.ToShortDateString
                    tsd = tsdd
                Catch ex As Exception

                End Try
                Dim asdd As DateTime
                Try
                    asdd = asd
                    asdd = asdd.ToShortDateString
                    asd = asdd
                Catch ex As Exception

                End Try
                Dim tcdd As DateTime
                Try
                    tcdd = tcd
                    tcdd = tcdd.ToShortDateString
                    tcd = tcdd
                Catch ex As Exception

                End Try
                Dim acdd As DateTime
                Try
                    acdd = acd
                    acdd = acdd.ToShortDateString
                    acd = acdd
                Catch ex As Exception

                End Try
                Dim ssd As DateTime
                Try
                    ssd = ss
                    ssd = ssd.ToShortDateString
                    ss = ssd
                Catch ex As Exception

                End Try
                Dim scd As DateTime
                Try
                    scd = sc
                    scd = scd.ToShortDateString
                    sc = scd
                Catch ex As Exception

                End Try
                If Len(desc) > 50 Then
                    desc = desc.Substring(0, 50)
                End If
                If eq = "" Then
                    If eqid <> "" Then
                        eq = "Equipment ID# " & eqid
                    End If
                End If
                Dim omo As String = """return overlib('Target Start: " & tsd & "<br />Target Comnplete: " & tcd & "<br />Scheduled Start: " & ss & "<br />Scheduled Complete: " & sc & "<br />Actual Start: " & asd & "<br />Actual Complete: " & acd & "', ABOVE, LEFT)"" onmouseout=""return nd()"""
                If filtret = "" And islabor = "1" And wt = "TPM" Then
                    If usid = lc Then
                        sb.Append("<tr>" & vbCrLf)
                        sb.Append("<td class=""" & bg & """><a class=""" & crl & """ href=""#"" onclick=""gotoreq('" & wo & "','" & eqid & "')"">" & wo & "</a></td>" & vbCrLf)
                        'sb.Append("<td class=""" & bg & " " & cr & """>" & wo & "</td>" & vbCrLf)
                        sb.Append("<td class=""" & bg & " " & cr & """>" & stat & "</td>" & vbCrLf)
                        If wt <> "0" Then
                            sb.Append("<td class=""" & bg & " " & cr & """>" & wt & "</td>" & vbCrLf)
                        Else
                            sb.Append("<td class=""" & bg & " " & cr & """></td>" & vbCrLf)
                        End If
                        sb.Append("<td class=""" & bg & " " & cr & """>" & eq & "</td>" & vbCrLf)
                        sb.Append("<td class=""" & bg & " " & cr & """>" & desc & "</td>" & vbCrLf)

                        If filtret <> "" And datret = "1" Then
                            If rt = "1" Then
                                sb.Append("<td class=""" & bg & " " & cr & """>" & tsd & "</td>" & vbCrLf)
                            ElseIf ra = "1" Then
                                sb.Append("<td class=""" & bg & " " & cr & """>" & asd & "</td>" & vbCrLf)
                            Else
                                sb.Append("<td class=""" & bg & " " & cr & """>" & ss & "</td>" & vbCrLf)
                            End If
                        Else
                            sb.Append("<td class=""" & bg & " " & cr & """>" & ss & "</td>" & vbCrLf)
                        End If

                        If wt = "TPM" Then
                            sb.Append("<td class=""" & bg & " " & cr & """ align=""center""><a href=""#"" onclick=""printwo('" & wo & "','" & wt & "','" & jp & "','" & tm & "','" & stat & "','" & rtid & "')"" onmouseover=""return overlib('" & tmod.getov("cov174", "wolist.aspx.vb") & "', ABOVE, LEFT)"" onmouseout=""return nd()""><img src=""../images/appbuttons/minibuttons/printx.gif"" border=""0""></a></td>" & vbCrLf)
                        Else
                            sb.Append("<td class=""" & bg & " " & cr & """ align=""center""><a href=""#"" onclick=""printwo('" & wo & "','" & wt & "','" & jp & "','" & pm & "','" & stat & "','" & rtid & "')"" onmouseover=""return overlib('" & tmod.getov("cov174", "wolist.aspx.vb") & "', ABOVE, LEFT)"" onmouseout=""return nd()""><img src=""../images/appbuttons/minibuttons/printx.gif"" border=""0""></a></td>" & vbCrLf)
                        End If

                        sb.Append("</tr>" & vbCrLf)
                    Else
                        pgadj += 1
                    End If
                Else
                    sb.Append("<tr>" & vbCrLf)
                    sb.Append("<td class=""" & bg & """><a class=""" & crl & """ href=""#"" onclick=""gotoreq('" & wo & "','" & eqid & "')"">" & wo & "</a></td>" & vbCrLf)
                    'sb.Append("<td class=""" & bg & " " & cr & """>" & wo & "</td>" & vbCrLf)
                    sb.Append("<td class=""" & bg & " " & cr & """>" & stat & "</td>" & vbCrLf)
                    If wt <> "0" Then
                        sb.Append("<td class=""" & bg & " " & cr & """>" & wt & "</td>" & vbCrLf)
                    Else
                        sb.Append("<td class=""" & bg & " " & cr & """></td>" & vbCrLf)
                    End If
                    sb.Append("<td class=""" & bg & " " & cr & """>" & eq & "</td>" & vbCrLf)
                    sb.Append("<td class=""" & bg & " " & cr & """>" & desc & "</td>" & vbCrLf)

                    If filtret <> "" And datret = "1" Then
                        If rt = "1" Then
                            sb.Append("<td class=""" & bg & " " & cr & """>" & tsd & "</td>" & vbCrLf)
                        ElseIf ra = "1" Then
                            sb.Append("<td class=""" & bg & " " & cr & """>" & asd & "</td>" & vbCrLf)
                        Else
                            sb.Append("<td class=""" & bg & " " & cr & """>" & ss & "</td>" & vbCrLf)
                        End If
                    Else
                        sb.Append("<td class=""" & bg & " " & cr & """ onmouseover=" & omo & ">" & ss & "</td>" & vbCrLf)
                    End If

                    If wt = "TPM" Then
                        sb.Append("<td class=""" & bg & " " & cr & """ align=""center""><a href=""#"" onclick=""printwo('" & wo & "','" & wt & "','" & jp & "','" & tm & "','" & stat & "','" & rtid & "')"" onmouseover=""return overlib('" & tmod.getov("cov174", "wolist.aspx.vb") & "', ABOVE, LEFT)"" onmouseout=""return nd()""><img src=""../images/appbuttons/minibuttons/printx.gif"" border=""0""></a></td>" & vbCrLf)
                    Else
                        sb.Append("<td class=""" & bg & " " & cr & """ align=""center""><a href=""#"" onclick=""printwo('" & wo & "','" & wt & "','" & jp & "','" & pm & "','" & stat & "','" & rtid & "')"" onmouseover=""return overlib('" & tmod.getov("cov174", "wolist.aspx.vb") & "', ABOVE, LEFT)"" onmouseout=""return nd()""><img src=""../images/appbuttons/minibuttons/printx.gif"" border=""0""></a></td>" & vbCrLf)
                    End If
                    sb.Append("</tr>" & vbCrLf)
                End If



            End While
        End If
        
        dr.Close()
        sb.Append("</table>")

        'Dim intPgNav, intPgCnt As Integer
        If issched = "0" Then
            sql = "SELECT Count(distinct w.wonum) FROM workorder w where " & FilterCNT
        Else
            sql = "SELECT Count(distinct w.wonum) FROM workorder w left join woassign a on a.wonum = w.wonum where " & FilterCNT 'left join woassign a on a.wonum = w.wonum
        End If

        intPgCnt = mm.Scalar(sql)
        intPgCnt = intPgCnt - pgadj
        'add adjust here

        If intPgCnt = 0 Then
            Dim ltab As String = lbllasttab.Value
            If ltab = "wo" Then
                tdpm.Attributes.Add("class", "thdrhovmini plainlabel")
                tdeq.Attributes.Add("class", "thdrmini plainlabel")
            ElseIf ltab = "pm" Then
                tdpm.Attributes.Add("class", "thdrmini plainlabel")
                tdeq.Attributes.Add("class", "thdrhovmini plainlabel")
            End If
            lbllasttab.Value = ltab
            lbltab.Value = ltab
            Dim strMessage As String = tmod.getmsg("cdstr588", "wolist.aspx.vb")
            mm.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If

        PageSize = "50"
        intPgNav = mm.PageCountRev(intPgCnt, PageSize)
        'added for refresh
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If

        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If


        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        divywr.InnerHtml = sb.ToString
        CheckRF()
    End Sub
End Class
