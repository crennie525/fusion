﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="cansendclient.aspx.vb"
    Inherits="lucy_r12.cansendclient" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Client Side Test</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript" src="../scripts/soapclient.js"></script>
    <script language="javascript" type="text/javascript">
    <!--
        function getparams() {
            var ids = new Array('lblsTransactionID', 'lblsConfigPosition', 'lblsEquipment', 'lblsStructureType', 'lblsService',
                                'lblsResponsible', 'lbliOperation', 'lblsDescription', 'lblsPlanningGroup', 'lblsPriority',
                                'lblsApprovedBy', 'lblsReasonCode', 'lblsStartDate', 'lblsFinishDate', 'lblsTxt1',
                                'lblsTxt2', 'lblsErrorCode1', 'lblsErrorCode2', 'lblsErrorCode3', 'lblsComplaintType',
                                'lbliRetroFlag', 'lblsItemNumber', 'lblsLotNumber', 'lbliPlannedQuantity', 'lblsFacility',
                                'lblsRequestedStartDte', 'lblsRequestedFinishDte', 'lbliSetupTime', 'lbliRuntime', 'lbliPlannedNumWkrs', 
                                'lblsTextBlock');
            var params = new Array('sTransactionID', 'sConfigPosition', 'sEquipment', 'sStructureType', 'sService',
                                'sResponsible', 'iOperation', 'sDescription', 'sPlanningGroup', 'sPriority',
                                'sApprovedBy', 'sReasonCode', 'sStartDate', 'sFinishDate', 'sTxt1',
                                'sTxt2', 'sErrorCode1', 'sErrorCode2', 'sErrorCode3', 'sComplaintType',
                                'iRetroFlag', 'sItemNumber', 'sLotNumber', 'iPlannedQuantity', 'sFacility',
                                'sRequestedStartDte', 'sRequestedFinishDte', 'iSetupTime', 'iRuntime', 'iPlannedNumWkrs',
                                'sTextBlock');
            var pl = new SOAPClientParameters();
            for (var i = 0; i < ids.length; i++) {
                var elem = document.getElementById(ids[i]);
                if (elem) {
                    pl.add(params[i], elem)
                }
            }
            var wsurl = document.getElementById("lblurl1").value;
            alert(wsurl + "?wsdl")
            SOAPClient.invoke(wsurl, "OpenWorkOrder", pl, true, OpenWorkOrder_callBack);
        }
        function OpenWorkOrder_callBack(r) {
            document.getElementById("tdoworesp").innerHTML = r;
        }
    //-->
    </script>
</head>
<body onload="getparams();">
    <form id="form1" runat="server">
    <table>
    <tr>
    <td class="label">OpenWorkOrder Response</td>
    </tr>
    <tr>
    <td class="plainlabel" id="tdoworesp"></td>
    </tr>
    </table>
    <input type="hidden" id="lblsTransactionID" runat="server" />
    <input type="hidden" id="lblsConfigPosition" runat="server" />
    <input type="hidden" id="lblsEquipment" runat="server" />
    <input type="hidden" id="lblsStructureType" runat="server" />
    <input type="hidden" id="lblsService" runat="server" />
    <input type="hidden" id="lblsResponsible" runat="server" />
    <input type="hidden" id="lbliOperation" runat="server" />
    <input type="hidden" id="lblsDescription" runat="server" />
    <input type="hidden" id="lblsPlanningGroup" runat="server" />
    <input type="hidden" id="lblsPriority" runat="server" />
    <input type="hidden" id="lblsApprovedBy" runat="server" />
    <input type="hidden" id="lblsReasonCode" runat="server" />
    <input type="hidden" id="lblsStartDate" runat="server" />
    <input type="hidden" id="lblsFinishDate" runat="server" />
    <input type="hidden" id="lblsTxt1" runat="server" />
    <input type="hidden" id="lblsTxt2" runat="server" />
    <input type="hidden" id="lblsErrorCode1" runat="server" />
    <input type="hidden" id="lblsErrorCode2" runat="server" />
    <input type="hidden" id="lblsErrorCode3" runat="server" />
    <input type="hidden" id="lblsComplaintType" runat="server" />
    <input type="hidden" id="lbliRetroFlag" runat="server" />
    <input type="hidden" id="lblsItemNumber" runat="server" />
    <input type="hidden" id="lblsLotNumber" runat="server" />
    <input type="hidden" id="lbliPlannedQuantity" runat="server" />
    <input type="hidden" id="lblsFacility" runat="server" />
    <input type="hidden" id="lblsRequestedStartDte" runat="server" />
    <input type="hidden" id="lblsRequestedFinishDte" runat="server" />
    <input type="hidden" id="lbliSetupTime" runat="server" />
    <input type="hidden" id="lbliRuntime" runat="server" />
    <input type="hidden" id="lbliPlannedNumWkrs" runat="server" />
    <input type="hidden" id="lblsTextBlock" runat="server" />
    <input type="hidden" id="lblurl1" runat="server" />
    <input type="hidden" id="lblurl2" runat="server" />
    </form>
</body>
</html>
