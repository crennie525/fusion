﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wotypedialog.aspx.vb" Inherits="lucy_r12.wotypedialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Work Type Selection</title>
    <script language="javascript" type="text/javascript">
        <!--
        function handleexit(ret) {
            window.returnValue = ret;
            window.close();
        }
        function pageScroll() {
            window.scrollTo(0, top);
            //window.scrollTo(0, 0);
            //scrolldelay = setTimeout('pageScroll()', 200);
            upsize('1');
            document.getElementById("ifsession").src = "../session.aspx?who=mm";
        }
        var timer = window.setTimeout("doref();", 1205000);
        function doref() {
            resetsess();
        }
        function setref() {
            window.clearTimeout(timer);
            timer = window.setTimeout("doref();", 1205000);
        }
        function GetWidth() {
            var x = 0;
            if (self.innerHeight) {
                x = self.innerWidth;
            }
            else if (document.documentElement && document.documentElement.clientHeight) {
                x = document.documentElement.clientWidth;
            }
            else if (document.body) {
                x = document.body.clientWidth;
            }
            return x;
        }

        function GetHeight() {
            var y = 0;
            if (self.innerHeight) {
                y = self.innerHeight;
            }
            else if (document.documentElement && document.documentElement.clientHeight) {
                y = document.documentElement.clientHeight;
            }
            else if (document.body) {
                y = document.body.clientHeight;
            }
            return y;
        }
        var flag = 5
        function upsize(who) {
            //alert(who)
            if (who != flag) {
                flag = who;
                document.getElementById("iftd").height = GetHeight();
                document.getElementById("iftd").width = GetWidth();
                window.scrollTo(0, top);
                //window.scrollTo(0, 0);
            }
        }
//-->
    </script>
</head>
<body onload="pageScroll();">
    <form id="form1" runat="server">
    <script type="text/javascript">
        document.body.onresize = function () {
            upsize('2');
        }
        self.onresize = function () {
            upsize('3');
        }
        document.documentElement.onresize = function () {
            upsize('4');
        }
        
    </script>
    <table style="position: absolute; top: 4px; left: 4px" cellspacing="0" cellpadding="0">
    <tr>
    <td><iframe id="iftd" width="100%" height="600" src="" frameborder="0" runat="server">
    </iframe></td>
    </tr>
    </table>
     
    <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
    </form>
</body>
</html>
