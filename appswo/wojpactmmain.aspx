<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wojpactmmain.aspx.vb"
    Inherits="lucy_r12.wojpactmmain" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wojpactmmain</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/wojpactmmainaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table style="left: 2px; position: absolute; top: 2px" cellspacing="1" cellpadding="2"
        width="730">
        <tr>
            <td>
                <table width="730">
                    <tr height="22">
                        <td class="label" width="80">
                            <asp:Label ID="lang1426" runat="server">Job Plan#</asp:Label>
                        </td>
                        <td class="plainlabelblue" id="tdjp" width="140" runat="server">
                        </td>
                        <td class="plainlabelblue" id="tdjpd" width="450" runat="server">
                        </td>
                        <td align="right" width="50">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="22">
            <td class="plainlabelblue" id="Td1" runat="server" align="center">
                <asp:Label ID="lang1427" runat="server">Note that Material Entries can only be made when Work Order is not Waiting to be Approved, Completed, or Cancelled.</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="2" width="720">
                    <tr height="22">
                        <td class="thdrhov label" id="tdpar" onclick="gettab('par');" width="150" runat="server">
                            <asp:Label ID="lang1428" runat="server">Job Plan Parts</asp:Label>
                        </td>
                        <td class="thdr label" id="tdtoo" onclick="gettab('too');" width="150" runat="server">
                            <asp:Label ID="lang1429" runat="server">Job Plan Tools</asp:Label>
                        </td>
                        <td class="thdr label" id="tdlub" onclick="gettab('lub');" width="150" runat="server">
                            <asp:Label ID="lang1430" runat="server">Job Plan Lubes</asp:Label>
                        </td>
                        <td width="270">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdborder" id="wo" colspan="4" runat="server">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <iframe id="ifmain" src="wohold.htm" frameborder="no" width="730" scrolling="auto"
                                            height="350" runat="server" style="background-color: transparent" allowtransparency>
                                        </iframe>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblwo" type="hidden" name="lblwo" runat="server">
    <input id="lbljpid" type="hidden" name="lbljpid" runat="server">
    <input id="lblupsav" type="hidden" name="lblupsav" runat="server">
    <input id="lblstat" type="hidden" name="lblstat" runat="server">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
