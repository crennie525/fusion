

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wojpact2
    Inherits System.Web.UI.Page
    Protected WithEvents lang1422 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1421 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1420 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1419 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim comp As New Utilities
    Dim pmid As String
    Dim ds As DataSet
    Dim wonum, jpid, stat, ro, rostr, Login As String
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtlead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwojtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblindx As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Label24 As System.Web.UI.WebControls.Label
    Protected WithEvents txttsk As System.Web.UI.WebControls.TextBox
    Protected WithEvents Td2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrow As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdjpn As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdjpd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents dghours As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetDGLangs()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            wonum = Request.QueryString("wo").ToString
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dghours.Columns(0).Visible = False
            Else
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = comp.CheckROS(rostr, "wo")
                    lblro.Value = ro
                    If ro = "1" Then
                        dghours.Columns(0).Visible = False
                    End If
                End If

            End If
            jpid = Request.QueryString("jpid").ToString
            stat = Request.QueryString("stat").ToString
            lblwo.Value = wonum
            lbljpid.Value = jpid
            lblstat.Value = stat
            If (stat <> "COMP" And stat <> "CAN" And stat <> "WAPPR") Then
                dghours.Columns(0).Visible = True
            Else
                dghours.Columns(0).Visible = False
            End If
            comp.Open()
            GetJPHead(jpid, wonum)
            PopDL(jpid, wonum)
            comp.Dispose()
        Else
            If Request.Form("lblsubmit") = "getlead" Then
                lblsubmit.Value = ""
                comp.Open()
                GetLead()
                comp.Dispose()
            End If
        End If
    End Sub
    Private Sub GetLead()
        Dim wojtid As String = lblwojtid.Value
        Dim lci As String = lbllead.Value
        Dim lc As String = txtlead.Value
        Dim indx As String = lblindx.Value
        sql = "update wojobtasks set leadcraft = '" & lc & "' where wojtid = '" & wojtid & "'"
        comp.Update(sql)
        If indx <> "N" Then
            jpid = lbljpid.Value
            wonum = lblwo.Value
            dghours.EditItemIndex = indx
            PopDL(jpid, wonum)
        Else
            jpid = lbljpid.Value
            wonum = lblwo.Value
            PopDL(jpid, wonum)
        End If
    End Sub
    Private Sub GetJPHead(ByVal jpid As String, ByVal wonum As String)
        sql = "select jpnum, description from wojobplans where wonum = '" & wonum & "' and jpid = '" & jpid & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdjpn.InnerHtml = dr.Item("jpnum").ToString
            tdjpd.InnerHtml = dr.Item("description").ToString
        End While
        dr.Close()
    End Sub
    Private Sub PopDL(ByVal jpid As String, ByVal wonum As String)
        sql = "usp_getwojplab '" & jpid & "','" & wonum & "'"
        ds = comp.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        'Try
        dghours.DataSource = dv
        dghours.DataBind()
    End Sub

    Private Sub dghours_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dghours.EditCommand
        'lblrow.Value = CType(e.Item.FindControl("A1"), Label).Text 'e.Item.ItemIndex

        comp.Open()
        jpid = lbljpid.Value
        wonum = lblwo.Value
        dghours.EditItemIndex = e.Item.ItemIndex
        PopDL(jpid, wonum)
        comp.Dispose()
    End Sub

    Private Sub dghours_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dghours.CancelCommand
        dghours.EditItemIndex = -1
        comp.Open()
        wonum = lblwo.Value
        jpid = lbljpid.Value
        PopDL(jpid, wonum)
        comp.Dispose()
    End Sub

    Private Sub dghours_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dghours.UpdateCommand
        Dim ret, typ, fail, pass, task, atime, lc As String
        Dim tsk, cnt, adj As String
        wonum = lblwo.Value
        jpid = lbljpid.Value
        Dim pmtskid As String = CType(e.Item.FindControl("lblwjtid"), Label).Text

        atime = CType(e.Item.FindControl("txtatime"), TextBox).Text
        lc = CType(e.Item.FindControl("txtalead"), TextBox).Text
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToDecimal(atime)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr570", "wojpact2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        sql = "usp_upjplab '" & lc & "','" & atime & "', '" & pmtskid & "', '" & jpid & "', '" & wonum & "'"

        comp.Open()
        comp.Update(sql)
        wonum = lblwo.Value
        jpid = lbljpid.Value
        dghours.EditItemIndex = -1
        PopDL(jpid, wonum)
        comp.Dispose()
    End Sub

    Private Sub dghours_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dghours.ItemDataBound
        Dim ibfm As ImageButton
        stat = lblstat.Value

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            If stat = "COMP" Or stat = "CAN" Or stat = "WAPPR" Then
                ibfm = CType(e.Item.FindControl("imgeditfm"), ImageButton)
                ibfm.Attributes.Add("class", "details")

            End If
            Dim img As HtmlImage = CType(e.Item.FindControl("imgti"), HtmlImage)
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "task").ToString
            'tdtsk.InnerHtml = tsk
            tsk = comp.ModString1(tsk)
            img.Attributes.Add("onclick", "gettsk('" & tsk & "');")

            Dim img1 As HtmlImage = CType(e.Item.FindControl("imglci"), HtmlImage)
            Dim wojtid As String = DataBinder.Eval(e.Item.DataItem, "wojtid").ToString
            Dim indx As String = "N"
            img1.Attributes.Add("onclick", "getsuper('" & indx & "','" & wojtid & "');")
        End If
        If e.Item.ItemType = ListItemType.EditItem Then
            Dim img As HtmlImage = CType(e.Item.FindControl("imgte"), HtmlImage)
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "task").ToString
            'tdtsk.InnerHtml = tsk
            img.Attributes.Add("onclick", "gettsk('" & tsk & "');")

            Dim img1 As HtmlImage = CType(e.Item.FindControl("imglce"), HtmlImage)
            Dim wojtid As String = DataBinder.Eval(e.Item.DataItem, "wojtid").ToString
            Dim indx As String = e.Item.ItemIndex
            img1.Attributes.Add("onclick", "getsuper('" & indx & "','" & wojtid & "');")

        End If
    End Sub




    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dghours.Columns(0).HeaderText = dlabs.GetDGPage("wojpact2.aspx", "dghours", "0")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(1).HeaderText = dlabs.GetDGPage("wojpact2.aspx", "dghours", "1")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(2).HeaderText = dlabs.GetDGPage("wojpact2.aspx", "dghours", "2")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(3).HeaderText = dlabs.GetDGPage("wojpact2.aspx", "dghours", "3")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(5).HeaderText = dlabs.GetDGPage("wojpact2.aspx", "dghours", "5")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label24.Text = axlabs.GetASPXPage("wojpact2.aspx", "Label24")
        Catch ex As Exception
        End Try
        Try
            lang1419.Text = axlabs.GetASPXPage("wojpact2.aspx", "lang1419")
        Catch ex As Exception
        End Try
        Try
            lang1420.Text = axlabs.GetASPXPage("wojpact2.aspx", "lang1420")
        Catch ex As Exception
        End Try
        Try
            lang1421.Text = axlabs.GetASPXPage("wojpact2.aspx", "lang1421")
        Catch ex As Exception
        End Try
        Try
            lang1422.Text = axlabs.GetASPXPage("wojpact2.aspx", "lang1422")
        Catch ex As Exception
        End Try

    End Sub

End Class
