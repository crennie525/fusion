<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wojpact.aspx.vb" Inherits="lucy_r12.wojpact" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wojpact</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
</head>
<body onload="scrolltop();">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" cellspacing="0" cellpadding="2" width="700">
        <tbody>
            <tr>
                <td>
                    <table width="700">
                        <tr height="26">
                            <td class="label" width="80">
                                <asp:Label ID="lang1403" runat="server">Job Plan:</asp:Label>
                            </td>
                            <td class="plainlabel" id="tdjpn" width="120" runat="server">
                            </td>
                            <td class="plainlabel" id="tdjpd" width="410" colspan="6" runat="server">
                            </td>
                            <td class="label" align="right" width="250">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="24">
                <td class="thdrsing label">
                    <asp:Label ID="lang1404" runat="server">Work Order Job Plan Labor Usage</asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DataList ID="dltasks" runat="server">
                        <HeaderTemplate>
                            <table width="700">
                                <tr>
                                    <td width="700">
                                    </td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr id="trfunc" runat="server">
                                <td>
                                    <asp:Label ID="A1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'
                                        CssClass="details">
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr id="trhdr" runat="server">
                                <td>
                                    <table>
                                        <tr>
                                            <td class="thdrsing plainlabel" width="60">
                                                <asp:Label ID="lang1405" runat="server">Edit</asp:Label>
                                            </td>
                                            <td class="thdrsing plainlabel" width="60">
                                                <asp:Label ID="lang1406" runat="server">Task#</asp:Label>
                                            </td>
                                            <td class="thdrsing plainlabel" width="580">
                                                <asp:Label ID="lang1407" runat="server">Task Description</asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="Tr1" runat="server">
                                            <td>
                                                <asp:ImageButton ID="imgeditfm" runat="server" ToolTip="Edit Record" CommandName="Edit"
                                                    ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"></asp:ImageButton>
                                            </td>
                                            <td class="plainlabel">
                                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'
                                                    CssClass="plainlabel">
                                                </asp:Label>
                                            </td>
                                            <td class="plainlabel" id="tdtdesc" runat="server">
                                                <%# DataBinder.Eval(Container, "DataItem.task") %>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="11">
                                    <table>
                                        <tr>
                                            <td class="thdrsing plainlabel" width="180">
                                                <asp:Label ID="lang1408" runat="server">Skill Required</asp:Label>
                                            </td>
                                            <td class="thdrsing plainlabel" width="50">
                                                Qty
                                            </td>
                                            <td class="thdrsing plainlabel" width="80">
                                                <asp:Label ID="lang1409" runat="server">Time</asp:Label>
                                            </td>
                                            <td class="thdrsing plainlabel" width="180">
                                                <asp:Label ID="lang1410" runat="server">Lead Craft</asp:Label>
                                            </td>
                                            <td class="thdrsing plainlabel" width="30">
                                                <img src="../images/appbuttons/minibuttons/magnifierdg.gif">
                                            </td>
                                            <td class="thdrsing plainlabel" width="80">
                                                <asp:Label ID="lang1411" runat="server">Actual Time</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="plainlabel">
                                                <%# DataBinder.Eval(Container, "DataItem.skill") %>
                                            </td>
                                            <td class="plainlabel">
                                                <%# DataBinder.Eval(Container, "DataItem.qty") %>
                                            </td>
                                            <td class="plainlabel">
                                                <%# DataBinder.Eval(Container, "DataItem.ttime") %>
                                            </td>
                                            <td class="plainlabel">
                                                <%# DataBinder.Eval(Container, "DataItem.leadcraft") %>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="plainlabel">
                                                <%# DataBinder.Eval(Container, "DataItem.acttime") %>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <SeparatorTemplate>
                            <tr>
                                <td>
                                    <hr>
                                </td>
                            </tr>
                        </SeparatorTemplate>
                        <EditItemTemplate>
                            <tr id="Tr2" runat="server">
                                <td>
                                    <table>
                                        <tr>
                                            <td class="thdrsing plainlabel" width="60">
                                                <asp:Label ID="lang1412" runat="server">Edit</asp:Label>
                                            </td>
                                            <td class="thdrsing plainlabel" width="60">
                                                <asp:Label ID="lang1413" runat="server">Task#</asp:Label>
                                            </td>
                                            <td class="thdrsing plainlabel" width="580">
                                                <asp:Label ID="lang1414" runat="server">Task Description</asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="Tr3" runat="server">
                                            <td>
                                                <asp:ImageButton ID="Imagebutton2" runat="server" ToolTip="Save Changes" CommandName="Update"
                                                    ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:ImageButton>
                                                <asp:ImageButton ID="Imagebutton3" runat="server" ToolTip="Cancel Changes" CommandName="Cancel"
                                                    ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"></asp:ImageButton>
                                            </td>
                                            <td class="plainlabel">
                                                <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'
                                                    CssClass="plainlabel">
                                                </asp:Label>
                                            </td>
                                            <td class="plainlabel" id="Td1" runat="server">
                                                <%# DataBinder.Eval(Container, "DataItem.task") %>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="11">
                                    <table>
                                        <tr>
                                            <td class="thdrsing plainlabel" width="180">
                                                <asp:Label ID="lang1415" runat="server">Skill Required</asp:Label>
                                            </td>
                                            <td class="thdrsing plainlabel" width="50">
                                                Qty
                                            </td>
                                            <td class="thdrsing plainlabel" width="80">
                                                <asp:Label ID="lang1416" runat="server">Time</asp:Label>
                                            </td>
                                            <td class="thdrsing plainlabel" width="180">
                                                <asp:Label ID="lang1417" runat="server">Lead Craft</asp:Label>
                                            </td>
                                            <td class="thdrsing plainlabel" width="30">
                                                <img src="../images/appbuttons/minibuttons/magnifierdg.gif">
                                            </td>
                                            <td class="thdrsing plainlabel" width="80">
                                                <asp:Label ID="lang1418" runat="server">Actual Time</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="plainlabel">
                                                <%# DataBinder.Eval(Container, "DataItem.skill") %>
                                            </td>
                                            <td class="plainlabel">
                                                <%# DataBinder.Eval(Container, "DataItem.qty") %>
                                            </td>
                                            <td class="plainlabel">
                                                <%# DataBinder.Eval(Container, "DataItem.ttime") %>
                                            </td>
                                            <td class="plainlabel">
                                                <asp:TextBox ID="txtalead" Width="160" ReadOnly="True" CssClass="plainlabel" runat="server"
                                                    Text='<%# DataBinder.Eval(Container, "DataItem.leadcraft") %>'></asp:TextBox>
                                            </td>
                                            <td width="30">
                                                <img src="../images/appbuttons/minibuttons/magnifierdg.gif" id="imglclu" runat="server">
                                            </td>
                                            <td class="plainlabel">
                                                <asp:TextBox ID="txtatime" CssClass="plainlabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.acttime") %>'></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </EditItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:DataList>
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" id="lblwo" runat="server">
    <input type="hidden" id="lbljpid" runat="server">
    <input type="hidden" id="lblstat" runat="server">
    <input id="xCoord" type="hidden" runat="server" name="xCoord">
    <input id="yCoord" type="hidden" runat="server" name="yCoord">
    <input type="hidden" id="lblrow" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
