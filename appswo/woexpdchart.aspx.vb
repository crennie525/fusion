﻿Imports System.Data.SqlClient
Public Class woexpdchart
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim ed As New Utilities
    Dim eqid, wonum, wt, pmid, pmhid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            wt = Request.QueryString("wt").ToString
            pmid = Request.QueryString("pmid").ToString
            pmhid = Request.QueryString("pmhid").ToString
            eqid = Request.QueryString("eqid").ToString
            wonum = Request.QueryString("wo").ToString
            lblwonum.Value = wonum
            lbleqid.Value = eqid

            lblwt.Value = wt
            lblpmid.Value = pmid
            lblpmhid.Value = pmhid

            ed.Open()
            GetDown(wonum, eqid)
            ed.Dispose()

        End If
    End Sub
    Private Sub GetDown(ByVal wonum As String, ByVal eqid As String)
        'sql = "select eqid, repdown, startdown, stopdown, repdownp, startdownp, stopdownp, wonum, ispm, totaldown, totaldownp, " _
        '+ "12 as hours, 0 as mins, 'AM' as aps, 12 as hre, 0 as mine, 'AM' as ape " _
        '+ "from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'"
        sql = "exec usp_geteqdownp '" & wonum & "', '" & eqid & "'"
        dr = ed.GetRdrData(sql)
        dgparts.DataSource = dr
        dgparts.DataBind()

    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        Dim cati As Integer
        If Len(CatID) <> 0 Then
            cati = CType(CatID, Integer)
            iL = CatID '- 1
        Else
            iL = 0
        End If
        Return iL
    End Function
    Function GetSelIndexM(ByVal CatID As String) As Integer
        Dim iL As Integer
        Dim cati As Integer
        If Len(CatID) <> 0 Then
            cati = CType(CatID, Integer)
            If CatID < 15 Then
                iL = 0
            ElseIf CatID < 30 Then
                iL = 1
            ElseIf CatID < 45 Then
                iL = 2
            Else
                iL = 3
            End If
        Else
            iL = 0
        End If
        Return iL
    End Function
    Function GetSelIndexAP(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Len(CatID) <> 0 Then
            If CatID = "AM" Then
                iL = 0
            Else
                iL = 1
            End If
        Else
            iL = 0
        End If
        Return iL
    End Function

    Private Sub dgparts_CancelCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.CancelCommand
        wonum = lblwonum.Value
        eqid = lbleqid.Value
        dgparts.EditItemIndex = -1
        ed.Open()
        GetDown(wonum, eqid)
        ed.Dispose()
    End Sub

    Private Sub dgparts_DeleteCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.DeleteCommand
        Dim id As String
        Try
            id = CType(e.Item.FindControl("lbleqhid"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lbleqhidi"), Label).Text
        End Try

        wonum = lblwonum.Value
        eqid = lbleqid.Value

        sql = "update eqhist set startdownp = null, stopdownp = null, repdownp = null, totaldownp = null where eqhid = '" & id & "' " _
        + "update equipment set totaldownp = (select sum(totaldownp) from eqhist where eqid = '" & eqid & "') " _
        + "where eqid = '" & eqid & "'"

        

        ed.Open()
        ed.Update(sql)
        GetDown(wonum, eqid)
        ed.Dispose()
    End Sub

    Private Sub dgparts_EditCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.EditCommand
        wonum = lblwonum.Value
        eqid = lbleqid.Value

        dgparts.EditItemIndex = e.Item.ItemIndex
        ed.Open()
        GetDown(wonum, eqid)
        ed.Dispose()
    End Sub

    Private Sub dgparts_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgparts.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            Dim deleteButton As ImageButton = CType(e.Item.FindControl("ibDel"), ImageButton)
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to Delete this Down Time Entry?')"
        End If

        If e.Item.ItemType = ListItemType.EditItem Then
            Dim lblr1 As Label = CType(e.Item.FindControl("lblstartdate"), Label)
            Dim lblr2 As Label = CType(e.Item.FindControl("lblstopdate"), Label)
            Dim r1 As String
            r1 = lblr1.ClientID.ToString
            Dim r2 As String
            r2 = lblr2.ClientID.ToString
            Dim imgstart As HtmlImage = CType(e.Item.FindControl("imgstart"), HtmlImage)
            Dim imgstop As HtmlImage = CType(e.Item.FindControl("imgstop"), HtmlImage)
            imgstart.Attributes.Add("onclick", "getcal('start', '" & r1 & "');")
            imgstop.Attributes.Add("onclick", "getcal('stop', '" & r2 & "');")
        End If


    End Sub

    Private Sub dgparts_UpdateCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.UpdateCommand
        Dim dhrs, dmins, daps, dhre, dmine, dape As DropDownList
        wonum = lblwonum.Value
        eqid = lbleqid.Value
        wt = lblwt.Value
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value

        dhrs = CType(e.Item.FindControl("ddhrs"), DropDownList)
        dmins = CType(e.Item.FindControl("ddmins"), DropDownList)
        daps = CType(e.Item.FindControl("ddaps"), DropDownList)
        dhre = CType(e.Item.FindControl("ddhre"), DropDownList)
        dmine = CType(e.Item.FindControl("ddmine"), DropDownList)
        dape = CType(e.Item.FindControl("ddape"), DropDownList)

        Dim shrs, smins, saps, shre, smine, sape
        shrs = dhrs.SelectedValue
        smins = dmins.SelectedValue
        saps = daps.SelectedValue
        shre = dhre.SelectedValue
        smine = dmine.SelectedValue
        sape = dape.SelectedValue

        Dim id As String = CType(e.Item.FindControl("lbleqhid"), Label).Text
        Dim sad As String = CType(e.Item.FindControl("lblstartdate"), Label).Text
        Dim std As String = CType(e.Item.FindControl("lblstopdate"), Label).Text

        Dim sdate, sedate, mins As String
        Dim ndate, nedate As Date

        If shrs <> "NA" Then

            sdate = sad & " " & shrs & ":" & smins & ":00 " & saps
            ndate = CType(sdate, Date)
        End If

        If shre <> "NA" Then

            sedate = std & " " & shre & ":" & smine & ":00 " & sape
            nedate = CType(sedate, Date)
        End If

        Dim dthrs As String
        Dim dtint As Integer

        If shrs <> "NA" And shre <> "NA" Then
            Try
                ed.Open()
                sql = "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
                    + "select @stopdate = cast('" & nedate & "' as datetime), @downdate = cast('" & ndate & "' as datetime); " _
            + "set @dmins = datediff(mi, @downdate, @stopdate); " _
            + "set @dhrs = @dmins / 60; " _
            + "select @dhrs"
                dthrs = ed.strScalar(sql)
            Catch ex As Exception
                Dim strMessage As String = "Total Down Time Could Not Be Calculated"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try

            Try
                dtint = CType(dthrs, Integer)
            Catch ex As Exception
                Dim strMessage As String = "Total Down Time Could Not Be Calculated"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            If dtint < 0 Then
                Dim strMessage As String = "Total Down Time Would Result in a Negative Value: Update Aborted"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub

            Else
                If pmid = "" And wt <> "MAXPM" Then
                    sql = "update eqhist set startdownp = '" & ndate & "', stopdownp = '" & nedate & "' where eqhid = '" & id & "'; " _
                    + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
                    + "select @stopdate = stopdownp, @downdate = startdownp from eqhist where eqhid = '" & id & "'; " _
                    + "set @dmins = datediff(mi, @downdate, @stopdate); " _
                    + "set @dhrs = @dmins / 60; " _
                    + "update eqhist set totaldownp = @dhrs where eqhid = '" & id & "'; " _
                    + "update equipment set totaldownp = (select sum(totaldownp) from eqhist where eqid = '" & eqid & "') where eqid = '" & eqid & "'"
                Else
                    If wt = "PM" Then
                        sql = "update eqhist set startdownp = '" & ndate & "', stopdownp = '" & nedate & "' where eqhid = '" & id & "'; " _
                        + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
                        + "select @stopdate = stopdownp, @downdate = startdownp from eqhist where eqhid = '" & id & "'; " _
                        + "set @dmins = datediff(mi, @downdate, @stopdate); " _
                        + "set @dhrs = @dmins / 60; " _
                        + "update eqhist set totaldownp = @dhrs where eqhid = '" & id & "'; " _
                        + "update equipment set totaldownp = (select sum(totaldownp) from eqhist where eqid = '" & eqid & "') where eqid = '" & eqid & "'" _
                        + "update pmhist set actdtime = (select sum(totaldownp) from eqhist where eqid = '" & eqid & "' and pmid = '" & pmid & "' and wonum = '" & wonum & "') where pmhid = '" & pmhid & "'; "
                    ElseIf wt = "TPM" Then
                        sql = "update eqhist set startdownp = '" & ndate & "', stopdownp = '" & nedate & "' where eqhid = '" & id & "'; " _
                        + "declare @stopdate datetime, @downdate datetime, @dmins decimal(10,2), @changedate datetime, @dsum int, @dhrs decimal(10,2);" _
                        + "select @stopdate = stopdownp, @downdate = startdownp from eqhist where eqhid = '" & id & "'; " _
                        + "set @dmins = datediff(mi, @downdate, @stopdate); " _
                        + "set @dhrs = @dmins / 60; " _
                        + "update eqhist set totaldownp = @dhrs where eqhid = '" & id & "'; " _
                        + "update equipment set totaldownp = (select sum(totaldownp) from eqhist where eqid = '" & eqid & "') where eqid = '" & eqid & "'" _
                        + "update pmhisttpm set actdtime = (select sum(totaldownp) from eqhist where eqid = '" & eqid & "' and pmid = '" & pmid & "' and wonum = '" & wonum & "') where pmhid = '" & pmhid & "'; "
                    End If
                End If


                ed.Update(sql)
                dgparts.EditItemIndex = -1
                GetDown(wonum, eqid)
                ed.Dispose()
            End If


        ElseIf shrs <> "NA" And shre = "NA" Then
            Dim strMessage As String = "No Stop Time Entered"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        ElseIf shrs = "NA" & shre <> "NA" Then
            Dim strMessage As String = "No Start Time Entered"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If

    End Sub
End Class