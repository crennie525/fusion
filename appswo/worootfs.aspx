﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="worootfs.aspx.vb" Inherits="lucy_r12.worootfs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script type="text/javascript" language="javascript">
    <!--
        function checkvals() {
            var rc = document.getElementById("txtrootauto").value;
            var co = document.getElementById("txtcorraction").value;
            if (rc == "") {
                alert("Root Cause Required")
            }
            else if (co == "") {
                alert("Corrective Action Required")
            }
            else {
                document.getElementById("lblsubmit").value = "go";
                document.getElementById("form1").submit();
            }
        }
        function checkchange() {

        }
        function checkdat() {
            var id = document.getElementById("lblrootid").value;
            //window.parent.handleroot(id);
        }
    //-->
    </script>
    <style type="text/css">
        .thdrsinglft
        {
            /*cursor:hand;*/
            padding: 1px 1px 1px 1px;
            margin: 1em;
            text-indent: 3px;
            text-align: left;
            vertical-align: middle;
            border-top: solid #6b6c6c 1px;
            border-left: solid #6b6c6c 2px;
            border-bottom: solid #6b6c6c 1px;
            background-image: url(../images/appbuttons/minibuttons/gradient1.gif);
            background-repeat: repeat-x;
            background-repeat: repeat;
            background-position: top left;
        }
        .thdrsingrt
        {
            padding: 1px 1px 1px 1px;
            margin: 1em;
            text-indent: 3px;
            text-align: left;
            vertical-align: middle;
            border-top: solid #6b6c6c 1px;
            border-right: solid #6b6c6c 1px;
            border-bottom: solid #6b6c6c 1px;
            background-image: url(../images/appbuttons/minibuttons/gradient1.gif);
            background-repeat: repeat-x;
            background-repeat: repeat;
            background-position: top left;
        }
        .listbox
        {
            border-bottom: 1px solid black;
            border-top: 1px solid black;
            border-left: 1px solid black;
            border-right: 1px solid black;
            background-color: White;
            overflow: auto;
            width: 240px;
            height: 300px;
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
            list-style-type: none;
            padding: 3px;
            margin: 10px;
        }
        .listitem
        {
            list-style-type: none;
            padding: 0px;
            margin: 0px;
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
        }
        .listhigh
        {
            cursor: pointer;
            background-color: #67BAF9;
            list-style-type: none;
            padding: 0px;
            margin: 0px;
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
        }
    </style>
</head>
<body onload="checkdat();">
    <form id="form1" runat="server">
    <cc2:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="rootauto.asmx" />
        </Services>
    </cc2:ToolkitScriptManager>
    <table width="540px" cellpadding="0" cellspacing="1">
        <tr>
            <td class="plainlabelblue" colspan="2" align="center">
                *Start typing in the Root Cause Text Box to Filter Root Cause values
            </td>
        </tr>
        <tr>
            <td class="plainlabelblue" colspan="2" align="center">
                *To see all values type % in the Root Cause Text Box
            </td>
        </tr>
        <tr>
            <td class="label" width="100px">
                Root Cause
            </td>
            <td class="plainlabel" width="440px">
                <asp:TextBox ID="txtrootauto" runat="server" Width="240"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="label" height="20px">
                Corrective Action
            </td>
        </tr>
        <tr>
            <td colspan="2" class="plainlabel">
                <asp:TextBox ID="txtcorraction" runat="server" Width="530" Height="40" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table>
                    <tr>
                        <td class="label" width="100px" height="22px">
                            Entered By:
                        </td>
                        <td class="plainlabel" width="170px" id="tdenterby" runat="server">
                        </td>
                        <td class="label" width="100px">
                            Enter Date:
                        </td>
                        <td class="plainlabel" id="tdenterdate" runat="server" width="170px">
                        </td>
                    </tr>
                    <tr>
                        <td class="label" height="22">
                            Modified By:
                        </td>
                        <td class="plainlabel" id="tdmodby" runat="server">
                        </td>
                        <td class="label" height="22">
                            Modified Date:
                        </td>
                        <td class="plainlabel" id="tdmoddate" runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <img id="imgsav" onclick="checkvals();" src="../images/appbuttons/minibuttons/savedisk1.gif"
                    runat="server" alt="" />
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblrootid" runat="server" />
    <input type="hidden" id="lblorootid" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblwo" runat="server" />
    <input type="hidden" id="lblcuser" runat="server" />
    <input type="hidden" id="lblcuserid" runat="server" />
    <input type="hidden" id="lbluser" runat="server" />
    <input type="hidden" id="lbluserid" runat="server" />
    <input type="hidden" id="lblenterdate" runat="server" />
    <input type="hidden" id="lblmodbyid" runat="server" />
    <input type="hidden" id="lblmodby" runat="server" />
    <input type="hidden" id="lblmoddate" runat="server" />
    <cc2:AutoCompleteExtender ID="AutoCompleteExtender1" TargetControlID="txtrootauto"
        runat="server" ServicePath="rootauto.asmx" ServiceMethod="geteq" MinimumPrefixLength="1"
        CompletionInterval="1000" EnableCaching="true" CompletionSetCount="12" CompletionListCssClass="listbox"
        CompletionListItemCssClass="listitem" CompletionListHighlightedItemCssClass="listhigh">
    </cc2:AutoCompleteExtender>
    </form>
</body>
</html>
