Imports System.Data.SqlClient
Imports System.Text
Public Class wopmlist
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim mm As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 12
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim rowcnt As Integer
    Dim sid, typ, ws, we As String
    Protected WithEvents tdhdr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblweekstart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblweekend As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblworktype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divhdr As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents tdywr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents divywr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            typ = "PM"
            lbltyp.Value = typ
            mm.Open()
            GetCurrentWeek()
            BuildMyWR(PageNumber)
            mm.Dispose()
        Else
            If Request.Form("lblsubmit") = "newweek" Then
                lblsubmit.Value = ""
                mm.Open()
                BuildMyWR(PageNumber)
                mm.Dispose()
            End If
        End If
    End Sub
    Private Sub GetCurrentWeek()
        sql = "declare @dwk int, @fwk datetime, @ewk datetime " _
        + "set @fwk = getDate() " _
        + "set @dwk = datepart(dw, @fwk) " _
        + "if @dwk <> 1 " _
        + "begin " _
        + "set @fwk = dateadd(dd, (@dwk*-1)+1, @fwk) " _
        + "end " _
        + "set @ewk = dateadd(dd, 27, @fwk) " _
        + "select Convert(char(10),@fwk,101) as ws, Convert(char(10),@ewk,101) as we"
        dr = mm.GetRdrData(sql)
        While dr.Read
            ws = dr.Item("ws").ToString
            we = dr.Item("we").ToString
        End While
        dr.Close()
        lblweekstart.Value = ws
        lblweekend.Value = we
    End Sub
    Private Sub BuildMyWR(ByVal PageNumber As Integer)
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder
        Dim field, srch As String
        Dim srchi As Integer
        sid = lblsid.Value
        typ = lbltyp.Value
        Filter = " worktype = ''" & typ & "''"
        FilterCNT = " worktype = '" & typ & "'"

        ws = lblweekstart.Value
        If ws = "" Then
            ws = "10/01/2010"
        End If
        we = lblweekend.Value
        If we = "" Then
            we = "10/28/2010"
        End If

        Filter += " and (targstartdate >= ''" & ws & "'' and targstartdate <= ''" & we & "'')"
        FilterCNT += " and (targstartdate >= '" & ws & "' and targstartdate <= '" & we & "')"

        Dim intPgNav, intPgCnt As Integer
        sql = "SELECT Count(*) FROM workorder where " & FilterCNT
        intPgCnt = mm.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr588", "wolist.aspx.vb")

            mm.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
        End If
        PageSize = "50"
        intPgNav = mm.PageCount1(intPgCnt, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If

        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        Sort = "w.wonum asc"

        sql = "declare @fwk datetime, @ewk datetime " _
       + "declare @fwk2 datetime, @ewk2 datetime " _
       + "declare @fwk3 datetime, @ewk3 datetime " _
       + "declare @fwk4 datetime, @ewk4 datetime " _
       + "set @fwk = '" & ws & "' " _
       + "set @ewk = dateadd(dd, 6, @fwk) " _
       + "set @fwk2 = dateadd(dd, 7, @fwk) " _
       + "set @ewk2 = dateadd(dd, 13, @fwk) " _
       + "set @fwk3 = dateadd(dd, 7, @fwk2) " _
       + "set @ewk3 = dateadd(dd, 13, @fwk2) " _
       + "set @fwk4 = dateadd(dd, 7, @fwk3) " _
       + "set @ewk4 = dateadd(dd, 13, @fwk3) " _
       + "select Convert(char(10),@fwk,101) as ws1, Convert(char(10),@ewk,101) as we1, " _
       + "Convert(char(10),@fwk2,101) as ws2, Convert(char(10),@ewk2,101) as we2, " _
       + "Convert(char(10),@fwk3,101) as ws3, Convert(char(10),@ewk3,101) as we3, " _
       + "Convert(char(10),@fwk4,101) as ws4, Convert(char(10),@ewk4,101) as we4 "
        Dim ws1, ws2, ws3, ws4, we1, we2, we3, we4 As String
        dr = mm.GetRdrData(sql)
        While dr.Read
            ws1 = dr.Item("ws1").ToString
            we1 = dr.Item("we1").ToString
            ws2 = dr.Item("ws2").ToString
            we2 = dr.Item("we2").ToString
            ws3 = dr.Item("ws3").ToString
            we3 = dr.Item("we3").ToString
            ws4 = dr.Item("ws4").ToString
            we4 = dr.Item("we4").ToString
        End While
        dr.Close()
        '
        sb1.Append("<table cellspacing=""2"" width=""1450"">" & vbCrLf)
        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td colspan=""7"" rowspan=""3"" valign=""top"">")
        sb1.Append("<table><tr><td class=""bigblue"" width=""220"" rowspan=""2"">Four Week Planner</td>")
        sb1.Append("<td valign=""top"" class=""label"">Start Week</td>")
        sb1.Append("<td valign=""top"" class=""plainlabel"" id=""tdswk"">" & ws1 & " - " & we1 & "</td>")
        sb1.Append("<td valign=""top""><img src=""../images/appbuttons/minibuttons/magnifier.gif"" onclick=""getweeks();""></td>")
        sb1.Append("</tr><tr><td class=""label"">Work Type</td><td class=""plainlabel"" id=""tdwt"">" & typ & "</td>")
        sb1.Append("<td><img src=""../images/appbuttons/minibuttons/magnifier.gif"" onclick=""getwt();""></td></tr></table></td>")
        sb1.Append("<td colspan=""6"" class=""thdrsingg plainlabel"">Week 1</td>")
        sb1.Append("<td colspan=""6"" class=""thdrsingg plainlabel"">Week 2</td>")
        sb1.Append("<td colspan=""6"" class=""thdrsingg plainlabel"">Week 3</td>")
        sb1.Append("<td colspan=""6"" class=""thdrsingg plainlabel"">Week 4</td>")
        sb1.Append("</tr>" & vbCrLf)

        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td colspan=""6"" class=""thdrplain plainlabelblue"">" & ws1 & " - " & we1 & "</td>")
        sb1.Append("<td colspan=""6"" class=""thdrplain plainlabelblue"">" & ws2 & " - " & we2 & "</td>")
        sb1.Append("<td colspan=""6"" class=""thdrplain plainlabelblue"">" & ws3 & " - " & we3 & "</td>")
        sb1.Append("<td colspan=""6"" class=""thdrplain plainlabelblue"">" & ws4 & " - " & we4 & "</td>")
        sb1.Append("</tr>" & vbCrLf)
        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">3rd</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">1st</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">2nd</td>")

        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">3rd</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">1st</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">2nd</td>")

        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">3rd</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">1st</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">2nd</td>")

        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">3rd</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">1st</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">2nd</td>")
        sb1.Append("</tr>" & vbCrLf)

        sql = "usp_getfourwkplnr '" & ws & "'"
        Dim w11a, w11s, w12a, w12s, w13a, w13s As String
        Dim w21a, w21s, w22a, w22s, w23a, w23s As String
        Dim w31a, w31s, w32a, w32s, w33a, w33s As String
        Dim w41a, w41s, w42a, w42s, w43a, w43s As String
        dr = mm.GetRdrData(sql)
        While dr.Read
            w11a = dr.Item("w11a").ToString
            w11s = dr.Item("w11s").ToString
            w12a = dr.Item("w12a").ToString
            w12s = dr.Item("w12s").ToString
            w13a = dr.Item("w13a").ToString
            w13s = dr.Item("w13s").ToString

            w21a = dr.Item("w21a").ToString
            w21s = dr.Item("w21s").ToString
            w22a = dr.Item("w22a").ToString
            w22s = dr.Item("w22s").ToString
            w23a = dr.Item("w23a").ToString
            w23s = dr.Item("w23s").ToString

            w31a = dr.Item("w31a").ToString
            w31s = dr.Item("w31s").ToString
            w32a = dr.Item("w32a").ToString
            w32s = dr.Item("w32s").ToString
            w33a = dr.Item("w33a").ToString
            w33s = dr.Item("w33s").ToString

            w41a = dr.Item("w41a").ToString
            w41s = dr.Item("w41s").ToString
            w42a = dr.Item("w42a").ToString
            w42s = dr.Item("w42s").ToString
            w43a = dr.Item("w43a").ToString
            w43s = dr.Item("w43s").ToString
        End While
        dr.Close()


        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td colspan=""4"" rowspan=""1""></td>")
        sb1.Append("<td colspan=""3"" class=""thdrplain plainlabel"">Available on each shift</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w13a & "</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w11a & "</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w12a & "</td>")

        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w23a & "</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w21a & "</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w22a & "</td>")

        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w33a & "</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w31a & "</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w32a & "</td>")

        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w43a & "</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w41a & "</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w42a & "</td>")
        sb1.Append("</tr>" & vbCrLf)

        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td colspan=""4"" rowspan=""1""></td>")
        sb1.Append("<td colspan=""3"" class=""thdrplain plainlabel"">Scheduled on each shift</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w13s & "</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w11s & "</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w12s & "</td>")

        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w23s & "</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w21s & "</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w22s & "</td>")

        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w33s & "</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w31s & "</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w32s & "</td>")

        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w43s & "</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w41s & "</td>")
        sb1.Append("<td colspan=""2"" class=""thdrplain plainlabel"">" & w42s & "</td>")
        sb1.Append("</tr>" & vbCrLf)

        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""70"" height=""20"">WO#</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""50"">Status</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""350"">Description</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""80"">Target</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""40"">Hours</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""80"">Start</td>" & vbCrLf)
        sb1.Append("<td class=""thdrsingg plainlabel"" width=""40"">Hours</td>" & vbCrLf)


        sb1.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sb1.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sb1.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sb1.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb1.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb1.Append("</tr>" & vbCrLf)

        Dim bg As String
        bg = "ptransrow"
        Dim cr, crl As String
        cr = "plainlabel"

        sb1.Append("<tr>" & vbCrLf)
        sb1.Append("<td class=""" & bg & """>00000000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """>0000000000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """>0000000000000000000000000000000000000000000000000000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """>00/00/0000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """>00.00</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """>00/00/0000</td>" & vbCrLf)
        sb1.Append("<td class=""" & bg & " " & cr & """>00.00</td>" & vbCrLf)

        sb1.Append("<td width=""20"" class=""thdrplain plainlabel"">00</td>")
        sb1.Append("<td width=""40"" class=""thdrplain plainlabel"">00.00</td>")
        sb1.Append("<td width=""20"" class=""thdrplain plainlabel"">00</td>")
        sb1.Append("<td width=""40"" class=""thdrplain plainlabel"">00.00</td>")
        sb1.Append("<td width=""20"" class=""thdrplain plainlabel"">00</td>")
        sb1.Append("<td width=""40"" class=""thdrplain plainlabel"">00.00</td>")

        sb1.Append("<td width=""20"" class=""thdrplain plainlabel"">00</td>")
        sb1.Append("<td width=""40"" class=""thdrplain plainlabel"">00.00</td>")
        sb1.Append("<td width=""20"" class=""thdrplain plainlabel"">00</td>")
        sb1.Append("<td width=""40"" class=""thdrplain plainlabel"">00.00</td>")
        sb1.Append("<td width=""20"" class=""thdrplain plainlabel"">00</td>")
        sb1.Append("<td width=""40"" class=""thdrplain plainlabel"">00.00</td>")

        sb1.Append("<td width=""20"" class=""thdrplain plainlabel"">00</td>")
        sb1.Append("<td width=""40"" class=""thdrplain plainlabel"">00.00</td>")
        sb1.Append("<td width=""20"" class=""thdrplain plainlabel"">00</td>")
        sb1.Append("<td width=""40"" class=""thdrplain plainlabel"">00.00</td>")
        sb1.Append("<td width=""20"" class=""thdrplain plainlabel"">00</td>")
        sb1.Append("<td width=""40"" class=""thdrplain plainlabel"">00.00</td>")

        sb1.Append("<td width=""20"" class=""thdrplain plainlabel"">00</td>")
        sb1.Append("<td width=""40"" class=""thdrplain plainlabel"">00.00</td>")
        sb1.Append("<td width=""20"" class=""thdrplain plainlabel"">00</td>")
        sb1.Append("<td width=""40"" class=""thdrplain plainlabel"">00.00</td>")
        sb1.Append("<td width=""20"" class=""thdrplain plainlabel"">00</td>")
        sb1.Append("<td width=""40"" class=""thdrplain plainlabel"">00.00</td>")
        sb1.Append("</tr>" & vbCrLf)


        sb1.Append("</table>")
        divhdr.InnerHtml = sb1.ToString


        sb.Append("<table cellspacing=""2"" width=""1450"">" & vbCrLf)

        Dim wt, wo, stat, isdown, desc, ss, sc, eq, ts, hrs, ttime As String
        Dim w11, w11h, w12, w12h, w13, w13h, w14, w14h As String
        Dim w21, w21h, w22, w22h, w23, w23h, w24, w24h As String
        Dim w31, w31h, w32, w32h, w33, w33h, w34, w34h As String
        Dim w41, w41h, w42, w42h, w43, w43h, w44, w44h As String
        Dim rowflag As Integer = 0
        sql = "usp_getwolist_pm '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "','" & ws & "','" & we & "'"
        dr = mm.GetRdrData(sql)
        While dr.Read
            If rowflag = 0 Then
                bg = "ptransrow"
                rowflag = 1
            Else
                bg = "ptransrowblue"
                rowflag = "0"
            End If
            isdown = "0" 'dr.Item("isdown").ToString

            If isdown = "1" Then
                cr = "plainlabelred"
                crl = "A1R"
            Else
                cr = "plainlabel"
                crl = "A1U"
            End If
            wt = dr.Item("worktype").ToString
            wo = dr.Item("wonum").ToString
            eq = dr.Item("eqnum").ToString
            stat = dr.Item("status").ToString
            desc = dr.Item("description").ToString
            ss = dr.Item("schedstart").ToString
            ts = dr.Item("targstartdate").ToString
            sc = dr.Item("schedfinish").ToString
            hrs = dr.Item("hrs").ToString
            ttime = dr.Item("ttime").ToString

            w11 = dr.Item("w11").ToString
            w11h = dr.Item("w11h").ToString
            w12 = dr.Item("w12").ToString
            w12h = dr.Item("w12h").ToString
            w13 = dr.Item("w13").ToString
            w13h = dr.Item("w13h").ToString
            'w14 = dr.Item("w14").ToString
            'w14h = dr.Item("w14h").ToString
            If w11 = "" Then
                w11 = "&nbsp;"
            End If
            If w11h = "" Then
                w11h = "&nbsp;"
            End If
            If w12 = "" Then
                w12 = "&nbsp;"
            End If
            If w12h = "" Then
                w12h = "&nbsp;"
            End If
            If w13 = "" Then
                w13 = "&nbsp;"
            End If
            If w13h = "" Then
                w13h = "&nbsp;"
            End If
            'If w14 = "" Then
            'w14 = "&nbsp;"
            'End If
            'If w14h = "" Then
            'w14h = "&nbsp;"
            'End If

            w21 = dr.Item("w21").ToString
            w21h = dr.Item("w21h").ToString
            w22 = dr.Item("w22").ToString
            w22h = dr.Item("w22h").ToString
            w23 = dr.Item("w23").ToString
            w23h = dr.Item("w23h").ToString
            'w24 = dr.Item("w24").ToString
            'w24h = dr.Item("w24h").ToString
            If w21 = "" Then
                w21 = "&nbsp;"
            End If
            If w21h = "" Then
                w21h = "&nbsp;"
            End If
            If w22 = "" Then
                w22 = "&nbsp;"
            End If
            If w22h = "" Then
                w22h = "&nbsp;"
            End If
            If w23 = "" Then
                w23 = "&nbsp;"
            End If
            If w23h = "" Then
                w23h = "&nbsp;"
            End If
            'If w24 = "" Then
            'w24 = "&nbsp;"
            'End If
            'If w24h = "" Then
            'w24h = "&nbsp;"
            'End If

            w31 = dr.Item("w31").ToString
            w31h = dr.Item("w31h").ToString
            w32 = dr.Item("w32").ToString
            w32h = dr.Item("w32h").ToString
            w33 = dr.Item("w33").ToString
            w33h = dr.Item("w33h").ToString
            'w34 = dr.Item("w34").ToString
            'w34h = dr.Item("w34h").ToString
            If w31 = "" Then
                w31 = "&nbsp;"
            End If
            If w31h = "" Then
                w31h = "&nbsp;"
            End If
            If w32 = "" Then
                w32 = "&nbsp;"
            End If
            If w32h = "" Then
                w32h = "&nbsp;"
            End If
            If w33 = "" Then
                w33 = "&nbsp;"
            End If
            If w33h = "" Then
                w33h = "&nbsp;"
            End If
            'If w34 = "" Then
            'w34 = "&nbsp;"
            'End If
            'If w34h = "" Then
            'w34h = "&nbsp;"
            'End If

            w41 = dr.Item("w41").ToString
            w41h = dr.Item("w41h").ToString
            w42 = dr.Item("w42").ToString
            w42h = dr.Item("w42h").ToString
            w43 = dr.Item("w43").ToString
            w43h = dr.Item("w43h").ToString
            'w44 = dr.Item("w44").ToString
            'w44h = dr.Item("w44h").ToString
            If w41 = "" Then
                w41 = "&nbsp;"
            End If
            If w41h = "" Then
                w41h = "&nbsp;"
            End If
            If w42 = "" Then
                w42 = "&nbsp;"
            End If
            If w42h = "" Then
                w42h = "&nbsp;"
            End If
            If w43 = "" Then
                w43 = "&nbsp;"
            End If
            If w43h = "" Then
                w43h = "&nbsp;"
            End If
            'If w44 = "" Then
            'w44 = "&nbsp;"
            'End If
            'If w44h = "" Then
            'w44h = "&nbsp;"
            'End If

            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""" & bg & """><a class=""" & crl & """ href=""#"" onclick=""gotoreq('" & wo & "')"">" & wo & "</a></td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """>" & stat & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """>" & desc & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """>" & ts & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """>" & ttime & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """>" & ss & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """>" & hrs & "</td>" & vbCrLf)

            sb.Append("<td width=""20"" class=""thdrplain plainlabel"">" & w13 & "</td>")
            sb.Append("<td width=""40"" class=""thdrplain plainlabel"">" & w13h & "</td>")
            sb.Append("<td width=""20"" class=""thdrplain plainlabel"">" & w11 & "</td>")
            sb.Append("<td width=""40"" class=""thdrplain plainlabel"">" & w11h & "</td>")
            sb.Append("<td width=""20"" class=""thdrplain plainlabel"">" & w12 & "</td>")
            sb.Append("<td width=""40"" class=""thdrplain plainlabel"">" & w12h & "</td>")

            sb.Append("<td width=""20"" class=""thdrplain plainlabel"">" & w23 & "</td>")
            sb.Append("<td width=""40"" class=""thdrplain plainlabel"">" & w23h & "</td>")
            sb.Append("<td width=""20"" class=""thdrplain plainlabel"">" & w21 & "</td>")
            sb.Append("<td width=""40"" class=""thdrplain plainlabel"">" & w21h & "</td>")
            sb.Append("<td width=""20"" class=""thdrplain plainlabel"">" & w22 & "</td>")
            sb.Append("<td width=""40"" class=""thdrplain plainlabel"">" & w22h & "</td>")

            sb.Append("<td width=""20"" class=""thdrplain plainlabel"">" & w33 & "</td>")
            sb.Append("<td width=""40"" class=""thdrplain plainlabel"">" & w33h & "</td>")
            sb.Append("<td width=""20"" class=""thdrplain plainlabel"">" & w31 & "</td>")
            sb.Append("<td width=""40"" class=""thdrplain plainlabel"">" & w31h & "</td>")
            sb.Append("<td width=""20"" class=""thdrplain plainlabel"">" & w32 & "</td>")
            sb.Append("<td width=""40"" class=""thdrplain plainlabel"">" & w32h & "</td>")

            sb.Append("<td width=""20"" class=""thdrplain plainlabel"">" & w43 & "</td>")
            sb.Append("<td width=""40"" class=""thdrplain plainlabel"">" & w43h & "</td>")
            sb.Append("<td width=""20"" class=""thdrplain plainlabel"">" & w41 & "</td>")
            sb.Append("<td width=""40"" class=""thdrplain plainlabel"">" & w41h & "</td>")
            sb.Append("<td width=""20"" class=""thdrplain plainlabel"">" & w42 & "</td>")
            sb.Append("<td width=""40"" class=""thdrplain plainlabel"">" & w42h & "</td>")
            sb.Append("</tr>" & vbCrLf)
        End While
        dr.Close()

        'pm list
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""70"" height=""20"">WO#</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""50"">Status</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""350"">Description</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""80"">Target</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""40"">Hours</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""80""><a href=""#"" onclick=""sortstart();"">Start</a></td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""40"">Hours</td>" & vbCrLf)

        bg = "ptransrow"
        cr = "plainlabelwht"

        sb.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sb.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sb.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")

        sb.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb.Append("<td width=""20"" class=""thdrsingg plainlabel"">#</td>")
        sb.Append("<td width=""40"" class=""thdrsingg plainlabel"">Hrs</td>")
        sb.Append("</tr>" & vbCrLf)

        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """>00000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """>0000000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """>0000000000000000000000000000000000000000000000000000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """>00/00/0000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """>00.00</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """>00/00/0000</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & " " & cr & """>00.00</td>" & vbCrLf)

        sb.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")

        sb.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")

        sb.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")

        sb.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb.Append("<td width=""20"" class=""thdrplainwht plainlabelwht"">00</td>")
        sb.Append("<td width=""40"" class=""thdrplainwht plainlabelwht"">00.00</td>")
        sb.Append("</tr>" & vbCrLf)

        sb.Append("</table>")
        divywr.InnerHtml = sb.ToString
    End Sub
End Class