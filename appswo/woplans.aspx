<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="woplans.aspx.vb" Inherits="lucy_r12.woplans" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>woplans</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/woplansaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkstat();" class="tbg">
    <form id="form1" method="post" runat="server">
    <div class="FreezePaneOff" id="FreezePane" align="center">
        <div class="InnerFreezePane" id="InnerFreezePane">
        </div>
    </div>
    <table style="left: 0px; position: absolute; top: 0px" cellspacing="0" cellpadding="1">
        <tr>
            <td class="label" id="tdjp" width="100" runat="server">
                <asp:Label ID="lang1564" runat="server">Select Job Plan</asp:Label>
            </td>
            <td width="150">
                <asp:TextBox ID="txtplan" runat="server" CssClass="plainlabel" Width="150px" ReadOnly="True"></asp:TextBox>
            </td>
            <td width="40">
                <img id="imgplans" onclick="getjplans();" src="../images/appbuttons/minibuttons/magnifier.gif"
                    border="0" runat="server" onmouseover="return overlib('View Existing Job Plans', ABOVE, LEFT)"
                    onmouseout="return nd()"><img id="imgaddplan" onclick="gotojp();" src="../images/appbuttons/minibuttons/addmod.gif"
                        runat="server" onmouseover="return overlib('Create a New Job Plan', ABOVE, LEFT)"
                        onmouseout="return nd()">
            </td>
            <td width="20">
                <img id="imgeditplan" runat="server" src="../images/appbuttons/minibuttons/pencil.gif"
                    onmouseover="return overlib('Edit Selected Job Plan (Applies to Work Order Only)', ABOVE, LEFT)"
                    onmouseout="return nd()" onclick="getwjplan();">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <input id="cbreq" onclick="checkreq();" type="checkbox" runat="server"><font class="label"><asp:Label
                    ID="lang1565" runat="server">Planning Required?</asp:Label></font><font class="plainlabel"><asp:Label
                        ID="lang1566" runat="server">(Overrides Job Plan Selection)</asp:Label></font>
            </td>
        </tr>
        <tr>
            <td class="graylabel" id="tdplanner" runat="server">
                <asp:Label ID="lang1567" runat="server">Select Planner</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtsup" runat="server" CssClass="plainlabel" Width="150px" ReadOnly="True"></asp:TextBox>
            </td>
            <td>
                <img id="imgplanner" alt="" src="../images/appbuttons/minibuttons/magnifierdis.gif"
                    border="0" runat="server">
            </td>
        </tr>
        <tr height="24">
            <td class="label">
                <asp:Label ID="lang1568" runat="server">Reference#</asp:Label>
            </td>
            <td class="plainlabel" id="tdref" colspan="2" runat="server">
                <asp:Label ID="lang1569" runat="server">None</asp:Label>
            </td>
        </tr>
    </table>
    <input id="lblgetplan" type="hidden" runat="server">
    <input id="lblwo" type="hidden" runat="server">
    <input id="lbljpid" type="hidden" runat="server">
    <input id="lbljpref" type="hidden" runat="server">
    <input id="lblplnrid" type="hidden" runat="server"><input id="lblplanner" type="hidden"
        runat="server">
    <input id="lblsubmit" type="hidden" runat="server">
    <input type="hidden" id="lbldis" runat="server">
    <input type="hidden" id="lblstat" runat="server">
    <input type="hidden" id="lblwt" runat="server">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lbllog" runat="server" name="lbllog">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
