

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wofmmain
    Inherits System.Web.UI.Page
	Protected WithEvents lang1401 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1400 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1399 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1398 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1397 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim comp As New Utilities
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim wonum, jpid, stat, ro, rostr As String
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdw As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdj As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents wo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifwo As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents jp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblupsav As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdjpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifjp As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwod As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro <> "1" Then
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = comp.CheckROS(rostr, "wo")
                    lblro.Value = ro
                End If
            End If
            wonum = Request.QueryString("wo").ToString
            lblwo.Value = wonum
            stat = Request.QueryString("stat").ToString
            lblstat.Value = stat
            comp.Open()
            GetWoHead(wonum)
            comp.Dispose()


        End If
    End Sub
    Private Sub GetWoHead(ByVal wonum As String)
        wonum = lblwo.Value
        ro = lblro.Value
        sql = "select wonum, description, targstartdate, actstart, actfinish, estlabhrs, actlabhrs, jpid, status from workorder where wonum = '" & wonum & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdwo.InnerHtml = dr.Item("wonum").ToString
            tdwod.InnerHtml = dr.Item("description").ToString
            lbljpid.Value = dr.Item("jpid").ToString
            jpid = dr.Item("jpid").ToString
            stat = dr.Item("status").ToString
        End While
        dr.Close()
        lbljpid.Value = jpid
        lblstat.Value = stat
        ifwo.Attributes.Add("src", "wofm.aspx?wo=" + wonum + "&stat=" + stat + "&ro=" + ro)
        'ifjp.Attributes.Add("src", "wojpfm.aspx?wo=" + wonum + "&jpid=" + jpid + "&stat=" + stat)

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1397.Text = axlabs.GetASPXPage("wofmmain.aspx", "lang1397")
        Catch ex As Exception
        End Try
        Try
            lang1398.Text = axlabs.GetASPXPage("wofmmain.aspx", "lang1398")
        Catch ex As Exception
        End Try
        Try
            lang1399.Text = axlabs.GetASPXPage("wofmmain.aspx", "lang1399")
        Catch ex As Exception
        End Try
        Try
            lang1400.Text = axlabs.GetASPXPage("wofmmain.aspx", "lang1400")
        Catch ex As Exception
        End Try
        Try
            lang1401.Text = axlabs.GetASPXPage("wofmmain.aspx", "lang1401")
        Catch ex As Exception
        End Try

    End Sub

End Class
