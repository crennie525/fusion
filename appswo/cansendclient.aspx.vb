﻿Public Class cansendclient
    Inherits System.Web.UI.Page
    Dim sConfigPosition, sEquipment, sStructureType, sService, sResponsible, sDescription, sPlanningGroup, sPriority As String
    Dim sApprovedBy, sReasonCode, sStartDate, sFinishDate, sTxt1, sTxt2, sErrorCode1, sErrorCode2, sErrorCode3, sComplaintType As String
    Dim iRetroFlag, sItemNumber, sLotNumber, iPlannedQuantity, sFacility, sRequestedStartDte, sRequestedFinishDte, iSetupTime, iRuntime As String
    Dim iPlannedNumWkrs, sTextBlock, iOperation, sTransactionID, pmid, tpmid, wonum As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then


            sTransactionID = Request.QueryString("sTransactionID").ToString
            sConfigPosition = Request.QueryString("sConfigPosition").ToString
            sEquipment = Request.QueryString("sEquipment").ToString
            sStructureType = Request.QueryString("sStructureType").ToString
            sService = Request.QueryString("sService").ToString
            sResponsible = Request.QueryString("sResponsible").ToString
            iOperation = Request.QueryString("iOperation").ToString
            sDescription = Request.QueryString("sDescription").ToString
            sPlanningGroup = Request.QueryString("sPlanningGroup").ToString
            sPriority = Request.QueryString("sPriority").ToString
            sApprovedBy = Request.QueryString("sApprovedBy").ToString
            sReasonCode = Request.QueryString("sReasonCode").ToString
            sStartDate = Request.QueryString("sStartDate").ToString
            sFinishDate = Request.QueryString("sFinishDate").ToString
            sTxt1 = Request.QueryString("sTxt1").ToString
            sTxt2 = Request.QueryString("sTxt2").ToString
            sErrorCode1 = Request.QueryString("sErrorCode1").ToString
            sErrorCode2 = Request.QueryString("sErrorCode2").ToString
            sErrorCode3 = Request.QueryString("sErrorCode3").ToString
            sComplaintType = Request.QueryString("sComplaintType").ToString
            iRetroFlag = Request.QueryString("iRetroFlag").ToString
            sItemNumber = Request.QueryString("sItemNumber").ToString
            sLotNumber = Request.QueryString("sLotNumber").ToString
            iPlannedQuantity = Request.QueryString("iPlannedQuantity").ToString
            sFacility = Request.QueryString("sFacility").ToString
            sRequestedStartDte = Request.QueryString("sRequestedStartDte").ToString
            sRequestedFinishDte = Request.QueryString("sRequestedFinishDte").ToString
            iSetupTime = Request.QueryString("iSetupTime").ToString
            iRuntime = Request.QueryString("iRuntime").ToString
            iPlannedNumWkrs = Request.QueryString("iPlannedNumWkrs").ToString
            sTextBlock = Request.QueryString("sTextBlock").ToString


            lblsTransactionID.Value = sTransactionID
            lblsConfigPosition.Value = sConfigPosition
            lblsEquipment.Value = sEquipment
            lblsStructureType.Value = sStructureType
            lblsService.Value = sService
            lblsResponsible.Value = sResponsible
            lbliOperation.Value = iOperation
            lblsDescription.Value = sDescription
            lblsPlanningGroup.Value = sPlanningGroup
            lblsPriority.Value = sPriority
            lblsApprovedBy.Value = sApprovedBy
            lblsReasonCode.Value = sReasonCode
            lblsStartDate.Value = sStartDate
            lblsFinishDate.Value = sFinishDate
            lblsTxt1.Value = sTxt1
            lblsTxt2.Value = sTxt2
            lblsErrorCode1.Value = sErrorCode1
            lblsErrorCode2.Value = sErrorCode2
            lblsErrorCode3.Value = sErrorCode3
            lblsComplaintType.Value = sComplaintType
            lbliRetroFlag.Value = iRetroFlag
            lblsItemNumber.Value = sItemNumber
            lblsLotNumber.Value = sLotNumber
            lbliPlannedQuantity.Value = iPlannedQuantity
            lblsFacility.Value = sFacility
            lblsRequestedStartDte.Value = sRequestedStartDte
            lblsRequestedFinishDte.Value = sRequestedFinishDte
            lbliSetupTime.Value = iSetupTime
            lbliRuntime.Value = iRuntime
            lbliPlannedNumWkrs.Value = iPlannedNumWkrs
            lblsTextBlock.Value = sTextBlock

            Dim serv1, serv2, smode As String
            Try
                serv1 = System.Configuration.ConfigurationManager.AppSettings("service1")
                lblurl1.Value = serv1
            Catch ex As Exception
                
            End Try
        End If
    End Sub

End Class