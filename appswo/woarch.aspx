<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="woarch.aspx.vb" Inherits="lucy_r12.woarch" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>woarch</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/woarchaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 0px" cellspacing="0" cellpadding="0">
        <tr>
            <td class="labelibl">
                <input type="radio" id="rb1" name="rb" runat="server" checked><asp:Label ID="lang1352"
                    runat="server">Jump to Work Order List</asp:Label><br>
                <input type="radio" id="rb2" name="rb" runat="server" disabled><asp:Label ID="lang1353"
                    runat="server">Create New Work Order</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <div style="height: 1px">
                    <hr size="1" color="gray">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="bluelabel" id="tdarch" style="overflow: auto; width: 230px; height: 155px"
                    runat="server">
                </div>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblsid" runat="server" name="lblsid">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
