﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="cansend2.aspx.vb" Inherits="lucy_r12.cansend2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
    <!--
        function checkent() {
            var empno = document.getElementById("txtnum").value;
            if (empno != "") {
                document.getElementById("lblret").value = "sendemp";
                document.getElementById("form1").submit();
            }
            else {

            }
        }
        function checknext() {
            document.getElementById("lblret").value = "getnext";
            document.getElementById("form1").submit();
        }
        //-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td class="label" height="22" width="120">
                    Entries to Submit
                </td>
                <td class="plainlabel" id="tdnum" runat="server" width="180">
                </td>
            </tr>
            <tr id="trinit" runat="server">
                <td class="label" height="22">
                    Employee Name
                </td>
                <td class="plainlabel" id="tdemp" runat="server">
                </td>
            </tr>
            <tr id="trinit2" runat="server">
                <td class="label" height="22">
                    Employee #
                </td>
                <td class="plainlabel">
                    <asp:TextBox ID="txtnum" runat="server" CssClass="plainlabel" Width="170px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="plainlabelred" id="tdgo" runat="server" height="22">
                </td>
            </tr>
            <tr id="trsub" runat="server">
                <td colspan="2" align="right">
                    <input type="button" onclick="checkent();" value="Submit" />
                </td>
            </tr>
            <tr id="trnext" runat="server" class="details">
                <td colspan="2" align="right">
                    <input type="button" onclick="checknext();" value="Get Next" />
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="lblpopstring" runat="server" />
    <input type="hidden" id="lblerrcnt" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblstat" runat="server" />
    <input type="hidden" id="lblcurremp" runat="server" />
    </form>
</body>
</html>
