

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wojpme2
    Inherits System.Web.UI.Page
	Protected WithEvents lang1515 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1514 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim comp As New Utilities
    Dim pmid As String
    Dim ds As DataSet
    Dim wonum, jpid, stat, ro, rostr, Login As String
    Protected WithEvents tdjpn As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdjpd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnofail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents dgmeas As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcurrcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrow As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfmcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmstrvals As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmflg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Label24 As System.Web.UI.WebControls.Label
    Protected WithEvents txttsk As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here

        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try

        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgmeas.Columns(0).Visible = False
            Else
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = comp.CheckROS(rostr, "wo")
                    'lblro.Value = ro
                    If ro = "1" Then
                        dgmeas.Columns(0).Visible = False
                    End If
                End If
            End If
            wonum = Request.QueryString("wo").ToString
            jpid = Request.QueryString("jpid").ToString
            lblwo.Value = wonum
            lbljpid.Value = jpid
            stat = Request.QueryString("stat").ToString
            lblstat.Value = stat
            If (stat <> "COMP" And stat <> "CAN" And stat <> "WAPPR") Then
                dgmeas.Columns(0).Visible = True
            Else
                dgmeas.Columns(0).Visible = False
            End If
            comp.Open()
            GetJPHead(jpid, wonum)
            GetMeas(jpid, wonum)
            comp.Dispose()
        End If
    End Sub

    Private Sub GetJPHead(ByVal jpid As String, ByVal wonum As String)
        sql = "select jpnum, description from wojobplans where wonum = '" & wonum & "' and jpid = '" & jpid & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdjpn.InnerHtml = dr.Item("jpnum").ToString
            tdjpd.InnerHtml = dr.Item("description").ToString
        End While
        dr.Close()
    End Sub
    Private Sub GetMeas(ByVal jpid As String, ByVal wonum As String)
        sql = "select m.*, j.tasknum, j.taskdesc from wojptaskmeasdetails m " _
        + "left join wojobtasks j on j.jpid = m.jpid and j.pmtskid = m.pmtskid and j.wonum = m.wonum " _
        + "where m.jpid = '" & jpid & "' and m.wonum = '" & wonum & "'"
        dr = comp.GetRdrData(sql)
        dgmeas.DataSource = dr
        dgmeas.DataBind()


    End Sub

    Private Sub dgmeas_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.EditCommand
        dgmeas.EditItemIndex = e.Item.ItemIndex
        jpid = lbljpid.Value
        wonum = lblwo.Value
        comp.Open()
        GetMeas(jpid, wonum)
        comp.Dispose()
    End Sub

    Private Sub dgmeas_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.CancelCommand
        dgmeas.EditItemIndex = -1
        jpid = lbljpid.Value
        wonum = lblwo.Value
        comp.Open()
        GetMeas(jpid, wonum)
        comp.Dispose()
    End Sub

    Private Sub dgmeas_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.UpdateCommand
        Dim mup As Integer
        Dim hi, lo As String
        Dim mstr As String = CType(e.Item.FindControl("txtmeas"), TextBox).Text
        Dim tmdid As String = CType(e.Item.FindControl("lbltmdida"), Label).Text
        Dim pmtskid As String = CType(e.Item.FindControl("lblpmtskida"), Label).Text
        Dim txtchk As Long
        Try
            txtchk = System.Convert.ToDecimal(mstr)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr583" , "wojpme2.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If mstr <> "" Then
            'sql = "update pmTaskMeasDetMan set measurement = '" & mstr & "' where pmtskid = '" & pmtskid & "'"
            sql = "usp_upwojpmeas '" & tmdid & "', '" & pmtskid & "', '" & mstr & "'"
            comp.Open()
            'meas.Update(sql)
            'sql = "select count(*) from pmTaskMeasDetManHist where pmtskid = '" & pmtskid & "' and measurement is not null"
            'mup = meas.Scalar(sql)
            'dr = meas.GetRdrData(sql)
            'While dr.Read
            'mup = dr.Item("mup").ToString
            'hi = dr.Item("hi").ToString
            'mup = dr.Item("lo").ToString
            'End While
            'dr.Close()
            'lblmup.Value = mup
            'dgmeas.EditItemIndex = -1
            comp.Update(sql)
            jpid = lbljpid.Value
            wonum = lblwo.Value
            GetMeas(jpid, wonum)
            comp.Dispose()
        End If
    End Sub

    Private Sub dgmeas_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgmeas.ItemDataBound
        Dim ibfm As ImageButton
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            stat = lblstat.Value
            If stat = "COMP" Or stat = "CAN" Or stat = "WAPPR" Then
                ibfm = CType(e.Item.FindControl("imgeditfm"), ImageButton)
                ibfm.Attributes.Add("class", "details")

            End If

            Dim img As HtmlImage = CType(e.Item.FindControl("imgmi"), HtmlImage)
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "taskdesc").ToString
            img.Attributes.Add("onclick", "gettsk('" & tsk & "');")

        End If
        If e.Item.ItemType = ListItemType.EditItem Then
            Dim img As HtmlImage = CType(e.Item.FindControl("imgme"), HtmlImage)
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "taskdesc").ToString
            img.Attributes.Add("onclick", "gettsk('" & tsk & "');")
        End If
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgmeas.Columns(0).HeaderText = dlabs.GetDGPage("wojpme2.aspx", "dgmeas", "0")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(1).HeaderText = dlabs.GetDGPage("wojpme2.aspx", "dgmeas", "1")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(2).HeaderText = dlabs.GetDGPage("wojpme2.aspx", "dgmeas", "2")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(3).HeaderText = dlabs.GetDGPage("wojpme2.aspx", "dgmeas", "3")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(4).HeaderText = dlabs.GetDGPage("wojpme2.aspx", "dgmeas", "4")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(5).HeaderText = dlabs.GetDGPage("wojpme2.aspx", "dgmeas", "5")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(6).HeaderText = dlabs.GetDGPage("wojpme2.aspx", "dgmeas", "6")
        Catch ex As Exception
        End Try
        Try
            dgmeas.Columns(7).HeaderText = dlabs.GetDGPage("wojpme2.aspx", "dgmeas", "7")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label24.Text = axlabs.GetASPXPage("wojpme2.aspx", "Label24")
        Catch ex As Exception
        End Try
        Try
            lang1514.Text = axlabs.GetASPXPage("wojpme2.aspx", "lang1514")
        Catch ex As Exception
        End Try
        Try
            lang1515.Text = axlabs.GetASPXPage("wojpme2.aspx", "lang1515")
        Catch ex As Exception
        End Try

    End Sub

End Class
