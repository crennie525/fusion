

'********************************************************
'*
'********************************************************



Public Class woscheddialog2_aspx
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim wonum, jpid, stat, typ, skill, skillid, sdays, ro, rostr As String
    Dim wos As New Utilities
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifsched As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try

        If Not IsPostBack Then
            typ = Request.QueryString("typ").ToString
            wonum = Request.QueryString("wo").ToString
            jpid = Request.QueryString("jpid").ToString
            stat = Request.QueryString("stat").ToString
            skill = Request.QueryString("skill").ToString
            skillid = Request.QueryString("skillid").ToString
            sdays = Request.QueryString("sdays").ToString
            'ro = Request.QueryString("ro").ToString
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                'pgtitle.InnerHtml = "Scheduling Dialog (Read Only)"
            Else
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = wos.CheckROS(rostr, "wo")
                    'lblro.Value = ro
                End If
                If ro = "1" Then
                    'pgtitle.InnerHtml = "Scheduling Dialog (Read Only)"
                Else
                    'pgtitle.InnerHtml = "Scheduling Dialog"
                End If

            End If
            If typ = "wo" Then
                ifsched.Attributes.Add("src", "wosched.aspx?wo=" & wonum & "&jpid=" & jpid & "&stat=" & stat & "&skill=" & skill & "&skillid=" & skillid & "&sdays=" & sdays & "&date=" & Now & "&ro=" & ro)
            End If

        End If
    End Sub

End Class
