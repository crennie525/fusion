

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class wocosts
    Inherits System.Web.UI.Page
    Protected WithEvents lang1359 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1358 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1357 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim comp As New Utilities
    Dim ap As New AppUtils
    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim wonum, jpid, stat, icost, Login As String
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwod As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdjp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdjpd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnotes As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents trp As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdcosts As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            wonum = Request.QueryString("wo").ToString '"1384" '
            lblwo.Value = wonum
            If wonum <> "" Then
                comp.Open()
                GetWoHead(wonum)
                GetCosts()
                comp.Dispose()
            End If

        End If
    End Sub
    Private Sub GetWoHead(ByVal wonum As String)
        wonum = lblwo.Value
        sql = "select w.wonum, w.description, w.jpid, w.status, j.jpnum, j.description as jdesc " _
        + "from workorder w left join wojobplans j on j.jpid = w.jpid  where w.wonum = '" & wonum & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdwo.InnerHtml = dr.Item("wonum").ToString
            tdwod.InnerHtml = dr.Item("description").ToString
            lbljpid.Value = dr.Item("jpid").ToString
            jpid = dr.Item("jpid").ToString
            stat = dr.Item("status").ToString
            tdjp.InnerHtml = dr.Item("jpnum").ToString
            tdjpd.InnerHtml = dr.Item("jdesc").ToString
        End While
        dr.Close()
        lbljpid.Value = jpid
        lblstat.Value = stat
    End Sub
    Private Sub GetCosts()
        wonum = lblwo.Value
        jpid = lbljpid.Value
        icost = ap.InvEntry()
        Dim sb As New StringBuilder
        sb.Append("<table cellspacing=""0"" width=""645"" border=""0"">")
        sb.Append("<tr height=""20"">")
        sb.Append("<td width=""100px""></td>" & vbCrLf)
        sb.Append("<td width=""200px""></td>" & vbCrLf)
        sb.Append("<td width=""25px"">&nbsp;</td>")
        sb.Append("<td class=""label"" width=""80px"" align=""center""><u>Estimated</u></td>" & vbCrLf)
        sb.Append("<td width=""25px"">&nbsp;</td>")
        sb.Append("<td class=""label"" width=""80px"" align=""center""><u>Actual</u></td>" & vbCrLf)
        sb.Append("<td width=""25px"">&nbsp;</td>")
        sb.Append("<td class=""label"" width=""120px"" align=""center""><u>Savings (Running)</u></td></tr>" & vbCrLf)
        'sb.Append("<td class=""label"" width=""120px"" align=""center""><u>Total</u></td></tr>" & vbCrLf)

        sql = "select estlabhrs, estlabcost, estmatcost, esttoolcost, estlubecost, " _
        + "isnull(actlabhrs,0) as actlabhrs, " _
        + "isnull(actlabcost,0) as actlabcost, " _
        + "isnull(actmatcost,0) as actmatcost, isnull(acttoolcost,0) as acttoolcost, isnull(actlubecost,0) as actlubecost, " _
        + "isnull(estjplabhrs, 0) as estjplabhrs, isnull(estjplabcost,0) as estjplabcost, isnull(estjpmatcost,0) as estjpmatcost, " _
        + "isnull(estjptoolcost,0) as estjptoolcost, isnull(estjplubecost,0) as estjplubecost, " _
        + "actjplabhrs, actjplabcost, actjpmatcost, actjptoolcost, actjplubecost, " _
        + "totlabhrs, totlabcost, totmatcost, tottoolcost, totlubecost " _
        + "from workorder where wonum = '" & wonum & "'"
        '+ "p.acttime / 60 as patime " _
        '+ "actlabhrs, actlabcost, actmatcost, acttoolcost, actlubecost, " _
        dr = comp.GetRdrData(sql)
        Dim sqty As Integer
        Dim dhr, sve, sva, dte, dta, svt, svte, svta, svle, svla As Decimal
        dte = 0
        dta = 0
        svt = 0
        Dim esthr, estjphr, acthr, actjphr As Decimal
        Dim estcost, estjpcost, actcost, actjpcost As Decimal
        Dim estmatcost, estjpmatcost, actmatcost, actjpmatcost As Decimal
        Dim esttoolcost, estjptoolcost, acttoolcost, actjptoolcost As Decimal
        Dim estlubecost, estjplubecost, actlubecost, actjplubecost As Decimal
        Dim sclass, aster As String
        If icost = "inv" Or icost = "ext" Then
            aster = "*"
        Else
            aster = ""
        End If
        While dr.Read
            Try
                sqty = dr.Item("qty").ToString
            Catch ex As Exception
                sqty = 1
            End Try
            estjphr = dr.Item("estjplabhrs").ToString
            esthr = dr.Item("estlabhrs").ToString
            esthr = sqty * esthr
            estjphr = sqty * estjphr
            acthr = dr.Item("actlabhrs").ToString
            actjphr = dr.Item("actjplabhrs").ToString
            sb.Append("<tr height=""20"" class=""transrowlime"">")
            sb.Append("<td class=""greenlabel transrowlime"" rowspan=""3"" align=""center"" valign=""center"">" & tmod.getlbl("cdlbl170", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""label transrowlime"">" & tmod.getlbl("cdlbl171", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowlime""></td>")
            sb.Append("<td class=""plainlabel transrowlime"" align=""center"">" & Format(esthr, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowlime""></td>")
            sb.Append("<td class=""plainlabel transrowlime"" align=""center"">" & Format(acthr, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowlime""></td>")
            sve = dr.Item("estlabhrs").ToString - dr.Item("actlabhrs").ToString
            sb.Append("<td class=""plainlabel transrowlime"" align=""center""></td></tr>" & vbCrLf) '" & sve & "
            sb.Append("<tr height=""20"" class=""transrowlime"">")
            sb.Append("<td class=""label transrowlime"">" & tmod.getlbl("cdlbl172", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowlime""></td>")
            sb.Append("<td class=""plainlabel transrowlime"" align=""center"">" & Format(estjphr, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowlime""></td>")
            sb.Append("<td class=""plainlabel transrowlime"" align=""center"">" & Format(actjphr, "##############0.00") & "*</td>" & vbCrLf)
            sb.Append("<td class=""transrowlime""></td>")
            sva = dr.Item("estjplabhrs").ToString - dr.Item("actjplabhrs").ToString
            sb.Append("<td class=""plainlabel transrowlime"" align=""center""></td></tr>" & vbCrLf) '" & sve & "

            sb.Append("<tr height=""20"" bgcolor=""#99FB97"">")
            sb.Append("<td class=""greenlabel"">" & tmod.getlbl("cdlbl173", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td></td>")
            dhr = esthr + estjphr 'dr.Item("estlabhrs").ToString + dr.Item("estjplabhrs").ToString
            dhr = sqty * dhr
            dte += dhr
            sb.Append("<td class=""greenlabel"" align=""center"">" & Format(dhr, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td></td>")
            dhr = acthr 'dr.Item("actlabhrs").ToString + dr.Item("actjplabhrs").ToString
            dta += dhr
            sb.Append("<td class=""greenlabel"" align=""center"">" & Format(dhr, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td></td>")

            'If dta <> 0 Then
            dhr = dte - dta
            'Else
            'dhr = 0
            'End If
            svt = 0
            dte = 0
            dta = 0
            'svt += dhr
            If dhr < 0 Then
                sclass = "redlabel"
            Else
                sclass = "greenlabel"
            End If
            sb.Append("<td class=""" & sclass & """ align=""center"">" & Format(dhr, "##############0.00") & "</td></tr>" & vbCrLf)

            sb.Append("<tr><td colspan=""5""><img src=""../images/appbuttons/minibuttons/6PX.gif""></td></tr>" & vbCrLf)
            Try
                estcost = dr.Item("estlabcost").ToString
            Catch ex As Exception
                estcost = 0
            End Try
            Try
                actcost = dr.Item("actlabcost").ToString
            Catch ex As Exception
                actcost = 0
            End Try
            Try
                estjpcost = dr.Item("estjplabcost").ToString
            Catch ex As Exception
                estjpcost = 0
            End Try
            Try
                actjpcost = dr.Item("actjplabcost").ToString
            Catch ex As Exception
                actjpcost = 0
            End Try

            sb.Append("<tr height=""20"" class=""transrowltblue"">")
            sb.Append("<td class=""bluelabel transrowltblue"" rowspan=""3"" align=""center"" valign=""center"">" & tmod.getlbl("cdlbl174", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""label transrowltblue"">" & tmod.getlbl("cdlbl175", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">$" & Format(estcost, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">$" & Format(actcost, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            Dim elc, alc As Decimal
            Try
                elc = dr.Item("estlabcost").ToString
            Catch ex As Exception
                elc = 0
            End Try
            Try
                alc = dr.Item("actlabcost").ToString
            Catch ex As Exception
                alc = 0
            End Try

            sve = elc - alc

            sb.Append("<td class=""plainlabel transrowltblue"" align=""center""></td></tr>" & vbCrLf) '" & sve & "
            sb.Append("<tr height=""20"" class=""transrowltblue"">")
            sb.Append("<td class=""label transrowltblue"">" & tmod.getlbl("cdlbl176", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">$" & Format(estjpcost, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">$" & Format(actjpcost, "##############0.00") & "*</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            Dim ejc, ajc As Decimal
            Try
                ejc = dr.Item("estjplabcost").ToString
            Catch ex As Exception
                ejc = 0
            End Try
            Try
                ajc = dr.Item("actjplabcost").ToString
            Catch ex As Exception
                ajc = 0
            End Try

            sva = ejc - ajc

            sb.Append("<td class=""plainlabel transrowltblue"" align=""center""></td></tr>" & vbCrLf) '" & sva & "
            ' sva = 0

            sb.Append("<tr height=""20""  bgcolor=""#BFDDFB"">")
            sb.Append("<td class=""bluelabel"">" & tmod.getlbl("cdlbl177", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td></td>")
            dhr = estcost + estjpcost 'dr.Item("estlabcost").ToString + dr.Item("estjplabcost").ToString
            dte += dhr
            sb.Append("<td class=""bluelabel"" align=""center"">$" & Format(dhr, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td></td>")
            dhr = actcost 'dr.Item("actlabcost").ToString + dr.Item("actjplabcost").ToString
            dta += dhr
            sb.Append("<td class=""bluelabel"" align=""center"">$" & Format(dhr, "##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td></td>")
            dhr = sve + sva
            svt += dhr
            'If dta <> 0 Then
            dhr = dte - dta
            'Else
            'dhr = 0
            'End If
            If dhr < 0 Then
                sclass = "redlabel"
            Else
                sclass = "bluelabel"
            End If
            sb.Append("<td class=""" & sclass & """ align=""center"">" & Format(dhr, "$##############0.00") & "</td></tr>" & vbCrLf)


            '*******************Materials********************************

            sb.Append("<tr><td colspan=""5""><img src=""../images/appbuttons/minibuttons/6PX.gif""></td></tr>" & vbCrLf)
            estmatcost = dr.Item("estmatcost").ToString
            actmatcost = dr.Item("actmatcost").ToString
            estjpmatcost = dr.Item("estjpmatcost").ToString
            actjpmatcost = dr.Item("actjpmatcost").ToString

            sb.Append("<tr height=""20"" class=""transrowltblue"">")
            sb.Append("<td class=""bluelabel transrowltblue"" rowspan=""3"" align=""center"" valign=""center"">" & tmod.getlbl("cdlbl178", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""label transrowltblue"">" & tmod.getlbl("cdlbl179", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(estmatcost, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(actmatcost, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            'If actmatcost <> 0 Then
            sve = estmatcost - actmatcost 'dr.Item("estmatcost").ToString - dr.Item("actmatcost").ToString
            'Else
            'sve = 0
            'End If
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(sve, "$##############0.00") & "</td></tr>" & vbCrLf)
            sb.Append("<tr height=""20"" class=""transrowltblue"">")
            sb.Append("<td class=""label transrowltblue"">" & tmod.getlbl("cdlbl180", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(estjpmatcost, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(actjpmatcost, "$##############0.00") & aster & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")

            If icost = "use" Then
                'If actjpmatcost <> 0 Then
                sva = estjpmatcost - actjpmatcost 'dr.Item("estjpmatcost").ToString - dr.Item("actjpmatcost").ToString
                'Else
                'sva = 0
                'End If
                sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(sva, "$##############0.00") & "</td></tr>" & vbCrLf)
            Else
                sb.Append("<td class=""plainlabel transrowltblue"" align=""center""></td></tr>" & vbCrLf)
            End If


            sb.Append("<tr height=""20""  bgcolor=""#BFDDFB"">")
            sb.Append("<td class=""bluelabel"">" & tmod.getlbl("cdlbl181", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td></td>")
            dhr = estmatcost + estjpmatcost 'dr.Item("estmatcost").ToString + dr.Item("estjpmatcost").ToString
            dte += dhr
            sb.Append("<td class=""bluelabel"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td></td>")
            If icost = "use" Then
                dhr = actmatcost + actjpmatcost 'dr.Item("actmatcost").ToString + dr.Item("actjpmatcost").ToString
            Else
                dhr = actmatcost
            End If
            sb.Append("<td class=""bluelabel"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td></td>")
            dta += dhr
            dhr = sve + sva
            svt += dhr
            'If dta <> 0 Then
            If icost <> "use" Then
                dhr = (estmatcost + estjpmatcost) - (actmatcost + actjpmatcost) 'dte + dta
            Else
                dhr = (estmatcost + estjpmatcost) - (actmatcost)
            End If
            'Else
            'dhr = 0
            'End If
            If dhr < 0 Then
                sclass = "redlabel"
            Else
                sclass = "bluelabel"
            End If
            sb.Append("<td class=""" & sclass & """ align=""center"">" & Format(dhr, "$##############0.00") & "</td></tr>" & vbCrLf)

            sb.Append("<tr><td colspan=""5""><img src=""../images/appbuttons/minibuttons/6PX.gif""></td></tr>" & vbCrLf)
            esttoolcost = dr.Item("esttoolcost").ToString
            acttoolcost = dr.Item("acttoolcost").ToString
            estjptoolcost = dr.Item("estjptoolcost").ToString
            actjptoolcost = dr.Item("actjptoolcost").ToString

            sb.Append("<tr height=""20"" class=""transrowltblue"">")
            sb.Append("<td class=""bluelabel transrowltblue"" rowspan=""3"" align=""center"" valign=""center"">" & tmod.getlbl("cdlbl182", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""label transrowltblue"">" & tmod.getlbl("cdlbl183", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(esttoolcost, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(acttoolcost, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            svte = esttoolcost - acttoolcost 'dr.Item("esttoolcost").ToString - dr.Item("acttoolcost").ToString
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(svte, "$##############0.00") & "</td></tr>" & vbCrLf)
            sb.Append("<tr height=""20"" class=""transrowltblue"">")
            sb.Append("<td class=""label transrowltblue"">" & tmod.getlbl("cdlbl184", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(estjptoolcost, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(actjptoolcost, "$##############0.00") & aster & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            If icost = "use" Then
                svta = estjptoolcost - actjptoolcost 'dr.Item("estjptoolcost").ToString - dr.Item("actjptoolcost").ToString
                sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(svta, "$##############0.00") & "</td></tr>" & vbCrLf)
            Else
                sb.Append("<td class=""plainlabel transrowltblue"" align=""center""></td></tr>" & vbCrLf)
            End If


            sb.Append("<tr height=""20""  bgcolor=""#BFDDFB"">")
            sb.Append("<td class=""bluelabel"">" & tmod.getlbl("cdlbl185", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td></td>")
            dhr = esttoolcost + estjptoolcost 'dr.Item("esttoolcost").ToString + dr.Item("estjptoolcost").ToString
            dte += dhr
            sb.Append("<td class=""bluelabel"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td></td>")
            dhr = acttoolcost + actjptoolcost 'dr.Item("acttoolcost").ToString + dr.Item("actjptoolcost").ToString
            dta += dhr
            sb.Append("<td class=""bluelabel"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td></td>")
            dhr = svte + svta
            svt += dhr
            'If dta <> 0 Then
            If icost = "use" Then
                dhr = (esttoolcost + estjptoolcost) - (acttoolcost + actjptoolcost) 'dte + dta
            Else
                dhr = (esttoolcost + estjptoolcost) - (acttoolcost)
            End If

            'Else
            'dhr = 0
            'End If
            If dhr < 0 Then
                sclass = "redlabel"
            Else
                sclass = "bluelabel"
            End If
            sb.Append("<td class=""" & sclass & """ align=""center"">" & Format(dhr, "$##############0.00") & "</td></tr>" & vbCrLf)

            sb.Append("<tr><td colspan=""5""><img src=""../images/appbuttons/minibuttons/6PX.gif""></td></tr>" & vbCrLf)
            estlubecost = dr.Item("estlubecost").ToString
            actlubecost = dr.Item("actlubecost").ToString
            estjplubecost = dr.Item("estjplubecost").ToString
            actjplubecost = dr.Item("actjplubecost").ToString

            sb.Append("<tr height=""20"" class=""transrowltblue"">")
            sb.Append("<td class=""bluelabel transrowltblue"" rowspan=""3"" align=""center"" valign=""center"">" & tmod.getlbl("cdlbl186", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""label transrowltblue"">" & tmod.getlbl("cdlbl187", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(estlubecost, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(actlubecost, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            svle = estlubecost - actlubecost 'dr.Item("estlubecost").ToString - dr.Item("actlubecost").ToString
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(svle, "$##############0.00") & "</td></tr>" & vbCrLf)
            sb.Append("<tr height=""20"" class=""transrowltblue"">")
            sb.Append("<td class=""label transrowltblue"">" & tmod.getlbl("cdlbl188", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(estjplubecost, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(actjplubecost, "$##############0.00") & aster & "</td>" & vbCrLf)
            sb.Append("<td class=""transrowltblue""></td>")
            If icost = "use" Then
                svla = estjplubecost - actjplubecost 'dr.Item("estjplubecost").ToString - dr.Item("actjplubecost").ToString
                sb.Append("<td class=""plainlabel transrowltblue"" align=""center"">" & Format(svla, "$##############0.00") & "</td></tr>" & vbCrLf)
            Else
                sb.Append("<td class=""plainlabel transrowltblue"" align=""center""></td></tr>" & vbCrLf)
            End If


            sb.Append("<tr height=""20""  bgcolor=""#BFDDFB"">")
            sb.Append("<td class=""bluelabel"">" & tmod.getlbl("cdlbl189", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td></td>")
            dhr = estlubecost + estjplubecost 'dr.Item("estlubecost").ToString + dr.Item("estjplubecost").ToString
            dte += dhr
            sb.Append("<td class=""bluelabel"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td></td>")
            dhr = actjplubecost + actjplubecost 'dr.Item("actlubecost").ToString + dr.Item("actjplubecost").ToString
            dta += dhr
            sb.Append("<td class=""bluelabel"" align=""center"">" & Format(dhr, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td></td>")
            dhr = sve + sva
            svt += dhr
            'If dta <> 0 Then
            If icost = "use" Then
                dhr = (estlubecost + estjplubecost) - (actjplubecost + actjplubecost) 'dte + dta
            Else
                dhr = (estlubecost + estjplubecost) - (actlubecost)
            End If

            'Else
            'dhr = 0
            'End If
            If dhr < 0 Then
                sclass = "redlabel"
            Else
                sclass = "bluelabel"
            End If
            sb.Append("<td class=""" & sclass & """ align=""center"">" & Format(dhr, "$##############0.00") & "</td></tr>" & vbCrLf)

            sb.Append("<tr><td colspan=""5""><img src=""../images/appbuttons/minibuttons/6PX.gif""></td></tr>" & vbCrLf)

            '***************************************totals********************************************

            sb.Append("<tr height=""20""  >")
            sb.Append("<td></td>")
            sb.Append("<td class=""label"" bgcolor=""#BFDDFB"">" & tmod.getlbl("cdlbl190", "wocosts.aspx.vb") & "</td>" & vbCrLf)
            sb.Append("<td bgcolor=""#BFDDFB""></td>")
            sb.Append("<td class=""label"" align=""center"" bgcolor=""#BFDDFB"">" & Format(dte, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td bgcolor=""#BFDDFB""></td>")
            sb.Append("<td class=""label"" align=""center"" bgcolor=""#BFDDFB"">" & Format(dta, "$##############0.00") & "</td>" & vbCrLf)
            sb.Append("<td bgcolor=""#BFDDFB""></td>")
            'If dta = 0 Then
            'svt = 0
            'Else
            svt = dte - dta
            'End If
            If svt < 0 Then
                sclass = "redlabel"
            Else
                sclass = "label"
            End If
            sb.Append("<td class=""" & sclass & """ align=""center"" bgcolor=""#BFDDFB"">" & Format(svt, "$##############0.00") & "</td></tr>" & vbCrLf)


        End While
        dr.Close()

        sb.Append("</table>")
        tdcosts.InnerHtml = sb.ToString
        tdnotes.InnerHtml = "Job Plan Labor Hours and Costs are Presented for Reference Purposes Only."
        If icost <> "use" Then
            tdnotes.InnerHtml += "<br>Material Usage and Costs are Entered Externally - Job Plan Actual Material Costs are Presented for Reference Purposes Only"
        End If
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1357.Text = axlabs.GetASPXPage("wocosts.aspx", "lang1357")
        Catch ex As Exception
        End Try
        Try
            lang1358.Text = axlabs.GetASPXPage("wocosts.aspx", "lang1358")
        Catch ex As Exception
        End Try
        Try
            lang1359.Text = axlabs.GetASPXPage("wocosts.aspx", "lang1359")
        Catch ex As Exception
        End Try

    End Sub

End Class
