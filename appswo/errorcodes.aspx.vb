Imports System.Data.SqlClient
Imports System.Text
Public Class errorcodes
    Inherits System.Web.UI.Page
    Dim ec As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents lblecd1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblecd2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdecd1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblecd3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trsave As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sql As String
    Dim sid, wonum, typ As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdec As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            sid = "" 'Request.QueryString("sid").ToString
            wonum = "" 'Request.QueryString("wo").ToString
            typ = "" 'Request.QueryString("typ").ToString
            lblsid.Value = sid
            lblwonum.Value = wonum
            lbltyp.Value = typ
            ec.Open()
            getec1()
            ec.Dispose()
        Else
            If Request.Form("lblsubmit") = "ecd1" Then
                lblsubmit.Value = ""
                ec.Open()
                getec2()
                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "ecd2" Then
                lblsubmit.Value = ""
                ec.Open()
                getec3()
                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "savewo" Then
                lblsubmit.Value = ""
                ec.Open()
                savewo()
                ec.Dispose()
            End If
        End If
    End Sub
    Private Sub savewo()
        wonum = lblwonum.Value
        sid = lblsid.Value
        Dim err1, err2, err3 As String
        err1 = lblecd1.Value
        err2 = lblecd2.Value
        err3 = lblecd3.Value
        Dim cmd As New SqlCommand
        cmd.CommandText = "update workorder set errorcode1 = @err1, " _
        + "errorcode2 = @err2, errorcode3 = @err3 " _
        + "where wonum = @wonum"
        Dim param = New SqlParameter("@wonum", SqlDbType.Int)
        param.Value = wonum
        cmd.Parameters.Add(param)
        Dim param04 = New SqlParameter("@err1", SqlDbType.VarChar)
        If err1 = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = err1
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@err2", SqlDbType.VarChar)
        If err2 = "" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = err2
        End If
        cmd.Parameters.Add(param05)
        Dim param10 = New SqlParameter("@err3", SqlDbType.VarChar)
        If err3 = "" Then
            param10.Value = System.DBNull.Value
        Else
            param10.Value = err3
        End If
        cmd.Parameters.Add(param10)
       
        ec.UpdateHack(cmd)
        lblsubmit.Value = "return"
    End Sub
    Private Sub getec3()
        Dim ecc, ecd As String
        Dim ecd2 As String = lblecd2.Value

        sql = "select * from ecd3 where substring(ecd3, 1, 3) = substring('" & ecd2 & "', 1, 3)"
        dr = ec.GetRdrData(sql)
        Dim sb As New StringBuilder
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 250px;  HEIGHT: 280px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
        sb.Append("<table width=""230"" cellpadding=""1"">")
        sb.Append("<tr>")
        sb.Append("<td width=""80""></td>")
        sb.Append("<td width=""130""></td>")
        sb.Append("</tr>")
        While dr.Read
            ecc = dr.Item("ecd3").ToString
            ecd = dr.Item("ecddesc").ToString
            sb.Append("<tr>")
            sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getecd3('" & ecc & "');"">" & ecc & "</td>")
            sb.Append("<td class=""plainlabel"">" & ecd & "</td>")
            sb.Append("</tr>")
        End While
        sb.Append("</table>")
        tdec.InnerHtml = sb.ToString
        tdecd2.InnerHtml = ecd2
    End Sub
    Private Sub getec2()
        Dim ecc, ecd As String
        Dim ecd1 As String = lblecd1.Value

        sql = "select * from ecd2 where substring(ecd2, 1, 2) = substring('" & ecd1 & "', 1, 2)"
        dr = ec.GetRdrData(sql)
        Dim sb As New StringBuilder
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 250px;  HEIGHT: 280px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
        sb.Append("<table width=""230"" cellpadding=""1"">")
        sb.Append("<tr>")
        sb.Append("<td width=""80""></td>")
        sb.Append("<td width=""130""></td>")
        sb.Append("</tr>")
        While dr.Read
            ecc = dr.Item("ecd2").ToString
            ecd = dr.Item("ecddesc").ToString
            sb.Append("<tr>")
            sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getecd2('" & ecc & "');"">" & ecc & "</td>")
            sb.Append("<td class=""plainlabel"">" & ecd & "</td>")
            sb.Append("</tr>")
        End While
        sb.Append("</table>")
        tdec.InnerHtml = sb.ToString
        tdecd1.InnerHtml = ecd1
    End Sub
    Private Sub getec1()
        Dim ecc, ecd As String
        sql = "select * from ecd1"
        dr = ec.GetRdrData(sql)
        Dim sb As New StringBuilder
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 250px;  HEIGHT: 280px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
        sb.Append("<table width=""230"" cellpadding=""1"">")
        sb.Append("<tr>")
        sb.Append("<td width=""80""></td>")
        sb.Append("<td width=""130""></td>")
        sb.Append("</tr>")
        While dr.Read
            ecc = dr.Item("ecd1").ToString
            ecd = dr.Item("ecddesc").ToString
            sb.Append("<tr>")
            sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getecd1('" & ecc & "');"">" & ecc & "</td>")
            sb.Append("<td class=""plainlabel"">" & ecd & "</td>")
            sb.Append("</tr>")
        End While
        sb.Append("</table>")
        tdec.InnerHtml = sb.ToString

    End Sub
End Class
