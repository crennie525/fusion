<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="newssamp.aspx.vb" Inherits="lucy_r12.newssamp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>newssamp</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table style="left: 0px; position: absolute; top: 0px" width="210">
        <tr>
            <td class="plainlabelbluea" align="center">
                <u>
                    <asp:Label ID="lang1313" runat="server">Maintenance Updates</asp:Label></u>
            </td>
        </tr>
        <tr>
            <td class="plainlabela" align="center">
                02/01/2008
            </td>
        </tr>
        <tr>
            <td>
                <img src="../eqimages/newssamp.jpg">
            </td>
        </tr>
        <tr>
            <td class="plainlabela">
                <asp:Label ID="lang1314" runat="server">Please be patient while we are resurfacing the West Entrance Parking Lot. Work is expected to be completed by 02/05/2008</asp:Label>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
