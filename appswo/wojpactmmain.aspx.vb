

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wojpactmmain
    Inherits System.Web.UI.Page
	Protected WithEvents lang1430 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1429 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1428 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1427 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1426 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim comp As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim wonum, jpid, stat, ro, rostr As String
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents wo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblupsav As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdpar As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtoo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlub As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifmain As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents tdjp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdjpd As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            ro = HttpContext.Current.Session("ro").ToString
        Catch ex As Exception
            ro = "0"
        End Try
        lblro.Value = ro
        If ro <> "1" Then
            rostr = HttpContext.Current.Session("rostr").ToString
            If Len(rostr) <> 0 Then
                ro = comp.CheckROS(rostr, "wo")
                lblro.Value = ro
            End If
        End If
        wonum = Request.QueryString("wo").ToString
        lblwo.Value = wonum
        jpid = Request.QueryString("jpid").ToString
        lbljpid.Value = jpid
        stat = Request.QueryString("stat").ToString
        lblstat.Value = stat
        comp.Open()
        GetJPHead(jpid, wonum)
        comp.Dispose()
        ifmain.Attributes.Add("src", "wojpactm.aspx?typ=p&wo=" + wonum + "&jpid=" + jpid + "&stat=" + stat + "&ro=" + ro)
    End Sub
    Private Sub GetJPHead(ByVal jpid As String, ByVal wonum As String)
        sql = "select jpnum, description from wojobplans where wonum = '" & wonum & "' and jpid = '" & jpid & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdjp.InnerHtml = dr.Item("jpnum").ToString
            tdjpd.InnerHtml = dr.Item("description").ToString
        End While
        dr.Close()
    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang1426.Text = axlabs.GetASPXPage("wojpactmmain.aspx","lang1426")
		Catch ex As Exception
		End Try
		Try
			lang1427.Text = axlabs.GetASPXPage("wojpactmmain.aspx","lang1427")
		Catch ex As Exception
		End Try
		Try
			lang1428.Text = axlabs.GetASPXPage("wojpactmmain.aspx","lang1428")
		Catch ex As Exception
		End Try
		Try
			lang1429.Text = axlabs.GetASPXPage("wojpactmmain.aspx","lang1429")
		Catch ex As Exception
		End Try
		Try
			lang1430.Text = axlabs.GetASPXPage("wojpactmmain.aspx","lang1430")
		Catch ex As Exception
		End Try

	End Sub

End Class
