<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wotemp.aspx.vb" Inherits="lucy_r12.wotemp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wotemp</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" method="post" runat="server">
    <asp:DataList ID="dltasks" runat="server">
        <HeaderTemplate>
            <table width="700">
                <tr>
                    <td width="700">
                    </td>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr id="trfunc" runat="server">
                <td>
                    <asp:Label ID="A1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'
                        CssClass="details">
                    </asp:Label>
                    <asp:Label ID="A2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tmdid") %>'
                        CssClass="details">
                    </asp:Label>
                </td>
            </tr>
            <tr id="trhdr" runat="server">
                <td>
                    <table>
                        <tr>
                            <td class="thdrsing plainlabel" width="60">
                                <asp:Label ID="lang1573" runat="server">Edit</asp:Label>
                            </td>
                            <td class="thdrsing plainlabel" width="60">
                                <asp:Label ID="lang1574" runat="server">Task#</asp:Label>
                            </td>
                            <td class="thdrsing plainlabel" width="580">
                                <asp:Label ID="lang1575" runat="server">Task Description</asp:Label>
                            </td>
                        </tr>
                        <tr id="trtask" runat="server">
                            <td>
                                <asp:ImageButton ID="Imagebutton1" runat="server" ToolTip="Edit Record" CommandName="Edit"
                                    ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"></asp:ImageButton>
                            </td>
                            <td class="plainlabel">
                                <asp:Label ID="lbltasknume" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'
                                    CssClass="plainlabel">
                                </asp:Label>
                            </td>
                            <td class="plainlabel">
                                <%# DataBinder.Eval(Container, "DataItem.task") %>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td width="60">
                                &nbsp;
                            </td>
                            <td class="thdrsingg plainlabel" width="120">
                                <asp:Label ID="lang1576" runat="server">Type</asp:Label>
                            </td>
                            <td class="thdrsingg plainlabel" width="50">
                                <asp:Label ID="lang1577" runat="server">Hi</asp:Label>
                            </td>
                            <td class="thdrsingg plainlabel" width="50">
                                <asp:Label ID="lang1578" runat="server">Lo</asp:Label>
                            </td>
                            <td class="thdrsingg plainlabel" width="50">
                                <asp:Label ID="lang1579" runat="server">Spec</asp:Label>
                            </td>
                            <td class="thdrsingg plainlabel" width="100">
                                <asp:Label ID="lang1580" runat="server">Measurement</asp:Label>
                            </td>
                        </tr>
                        <tr id="trfm" runat="server">
                            <td>
                                &nbsp;
                            </td>
                            <td class="plainlabel">
                                <%# DataBinder.Eval(Container, "DataItem.type") %>
                            </td>
                            <td class="plainlabel">
                                <%# DataBinder.Eval(Container, "DataItem.hi") %>
                            </td>
                            <td class="plainlabel">
                                <%# DataBinder.Eval(Container, "DataItem.lo") %>
                            </td>
                            <td class="plainlabel">
                                <%# DataBinder.Eval(Container, "DataItem.spec") %>
                            </td>
                            <td class="plainlabel">
                                <%# DataBinder.Eval(Container, "DataItem.womeas") %>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </ItemTemplate>
        <EditItemTemplate>
            <tr id="Tr2" runat="server">
                <td>
                    <table>
                        <tr>
                            <td class="thdrsing plainlabel" width="60">
                                <asp:Label ID="lang1581" runat="server">Edit</asp:Label>
                            </td>
                            <td class="thdrsing plainlabel" width="60">
                                <asp:Label ID="lang1582" runat="server">Task#</asp:Label>
                            </td>
                            <td class="thdrsing plainlabel" width="550">
                                <asp:Label ID="lang1583" runat="server">Task Description</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="Imagebutton2" runat="server" ToolTip="Save Changes" CommandName="Update"
                                    ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton3" runat="server" ToolTip="Cancel Changes" CommandName="Cancel"
                                    ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"></asp:ImageButton>
                            </td>
                            <td class="plainlabel">
                                <%# DataBinder.Eval(Container, "DataItem.tasknum") %>
                            </td>
                            <td class="plainlabel">
                                <%# DataBinder.Eval(Container, "DataItem.task") %>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td width="60">
                                &nbsp;
                            </td>
                            <td class="thdrsingg plainlabel" width="120">
                                <asp:Label ID="lang1584" runat="server">Type</asp:Label>
                            </td>
                            <td class="thdrsingg plainlabel" width="50">
                                <asp:Label ID="lang1585" runat="server">Hi</asp:Label>
                            </td>
                            <td class="thdrsingg plainlabel" width="50">
                                <asp:Label ID="lang1586" runat="server">Lo</asp:Label>
                            </td>
                            <td class="thdrsingg plainlabel" width="50">
                                <asp:Label ID="lang1587" runat="server">Spec</asp:Label>
                            </td>
                            <td class="thdrsingg plainlabel" width="100">
                                <asp:Label ID="lang1588" runat="server">Measurement</asp:Label>
                            </td>
                        </tr>
                        <tr id="Tr1" runat="server">
                            <td>
                                &nbsp;
                            </td>
                            <td class="plainlabel">
                                <%# DataBinder.Eval(Container, "DataItem.type") %>
                            </td>
                            <td class="plainlabel">
                                <%# DataBinder.Eval(Container, "DataItem.hi") %>
                            </td>
                            <td class="plainlabel">
                                <%# DataBinder.Eval(Container, "DataItem.lo") %>
                            </td>
                            <td class="plainlabel">
                                <%# DataBinder.Eval(Container, "DataItem.spec") %>
                            </td>
                            <td>
                                <asp:TextBox ID="txtmeas" runat="server" Width="90px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.womeas") %>'>
                                </asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </EditItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:DataList>
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
