<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="womlaborpm.aspx.vb" Inherits="lucy_r12.womlaborpm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>womlaborpm</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/transassignaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        <!--
        function getemp(uid, emp, wdate, hrs, islead, wid) {
            var wo = document.getElementById("lblwonum").value;
            var eReturn = window.showModalDialog("getempdialog.aspx?uid=" + uid + "&wo=" + wo + "&emp=" + emp + "&islead=" + islead + "&hrs=" + hrs + "&adate=" + wdate + "&wid=" + wid, "", "dialogHeight:520px; dialogWidth:620px; resizable=yes");
            if (eReturn) {
                document.getElementById("lblsubmit").value = "upemps";
                document.getElementById("form1").submit();
            }
        }
        function getsuper(typ) {
            //var wt = document.getElementById("lblwt").value;
            var wo = document.getElementById("lblwonum").value;
            var skill; // = document.getElementById("lblskillid").value;
            var ro; // = document.getElementById("lblro").value;
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&wo=" + wo + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    var retarr = eReturn.split(",")
                    if (typ == "sup") {
                        document.getElementById("lblsupid").value = retarr[0];
                        document.getElementById("txtsup").value = retarr[1];
                        document.getElementById("lblsup").value = retarr[1];
                        document.getElementById("lblsubmit").value = "changesup";
                        document.getElementById("form1").submit();
                    }
                    else {
                        //alert(retarr[0])
                        document.getElementById("lbllead").value = retarr[0];
                        document.getElementById("txtlead").value = retarr[1];
                    }
                }
            }
        }
        function resetlead() {
            //var wt = document.getElementById("txtwt").value;
            //if(wt!="PM"&&wt!="TPM") {
            document.getElementById("lbllead").value = "";
            document.getElementById("txtlead").value = "";
            document.getElementById("lblsubmit").value = "go";
            document.getElementById("form1").submit();
            //}
        }
        function resetsuper() {
            //var wt = document.getElementById("txtwt").value;
            //if(wt!="PM"&&wt!="TPM") {
            document.getElementById("lblsupid").value = "";
            document.getElementById("lblsup").value = "";
            document.getElementById("txtsup").value = "";
            document.getElementById("lblsubmit").value = "remsup";
            document.getElementById("form1").submit();
            //}
        }
        function resetwa() {
            //var wt = document.getElementById("txtwt").value;
            //if(wt!="PM"&&wt!="TPM") {
            document.getElementById("lblwaid").value = "";
            document.getElementById("lblworkarea").value = "";
            document.getElementById("lblsubmit").value = "remwa";
            document.getElementById("form1").submit();
            //}
        }
        function getwa(who) {
            var wo = document.getElementById("lblwonum").value;
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/workareaselectdialog.aspx?typ=wo&sid=" + sid + "&wo=" + wo, "", "dialogHeight:500px; dialogWidth:820px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    var ret = eReturn.split("~~")
                    document.getElementById("lblwaid").value = ret[0];
                    document.getElementById("lblworkarea").value = ret[1];
                    if (who == "d") {
                        document.getElementById("tdwa").innerHTML = ret[1];
                    }
                    else {
                        document.getElementById("tdwa1").innerHTML = ret[1];
                    }
                    document.getElementById("lblsubmit").value = "changewa";
                    document.getElementById("form1").submit();
                }
            }
        }
        function getsup(uid, uname, wdate, avail, trid, ushift) {
            var vals = document.getElementById("lblvals").value;
            var valsvar = document.getElementById("lblvalsvar").value;
            var chkflg = 0;
            if (vals == "") {
                document.getElementById("lblvals").value = uid + "," + uname + "," + wdate + "," + avail + "," + trid + "," + ushift;
                document.getElementById("lblvalsvar").value = uid;
                document.getElementById(trid).className = "bggr";
            }
            else {
                vals = vals + "~" + uid + "," + uname + "," + wdate + "," + avail + "," + trid;
                valsvar = valsvar + "~" + uid;
                var valsar = vals.split("~");
                var valsvarar = valsvar.split("~");
                var newvals = "";
                var newvalsvar = "";
                var val;
                var val1;
                var ntrid;
                var uidcnt = 0;
                for (i = 0; i < valsvarar.length; i++) {
                    val = valsvarar[i];
                    val1 = valsar[i];
                    var val1ar = val1.split(",")
                    ntrid = val1ar[4];
                    if (val == uid) {
                        uidcnt += 1;
                    }
                    if (val == uid && uidcnt > 1) {
                        chkflg = 1;
                    }
                    else {
                        if (newvalsvar == "") {
                            newvalsvar = val;
                            newvals = val1;
                        }
                        else {
                            newvalsvar = newvalsvar + "~" + val;
                            newvals = newvals + "~" + val1;
                        }
                    }
                    if (chkflg == "1") {
                        document.getElementById(ntrid).className = "bgwt";
                    }
                    else {
                        document.getElementById(ntrid).className = "bggr";
                    }
                    chkflg = 0;

                }
                if (newvals != "") {
                    document.getElementById("lblvals").value = newvals;
                    document.getElementById("lblvalsvar").value = newvalsvar;
                }
            }
        }
        function checkto() {
            var vals = document.getElementById("lblvals").value;
            if (vals != "") {
                document.getElementById("lblsubmit").value = "towo";
                document.getElementById("form1").submit();
            }
        }
        function remsup(uid, uname, wdate, hrs, islead, trid, wid) {
            var vals = document.getElementById("lblrvals").value;
            var valsvar = document.getElementById("lblrvalsvar").value;
            var chkflg = 0;
            if (vals == "") {
                document.getElementById("lblrvals").value = uid + "," + uname + "," + wdate + "," + hrs + "," + islead + "," + trid + "," + wid;
                document.getElementById("lblrvalsvar").value = uid;
                document.getElementById(trid).className = "bggr";
            }
            else {
                vals = vals + "~" + uid + "," + uname + "," + wdate + "," + hrs + "," + islead + "," + trid + "," + wid;
                valsvar = valsvar + "~" + uid;
                var valsar = vals.split("~");
                var valsvarar = valsvar.split("~");
                var newvals = "";
                var newvalsvar = "";
                var val;
                var val1;
                var ntrid;
                var uidcnt = 0;
                for (i = 0; i < valsvarar.length; i++) {
                    val = valsvarar[i];
                    val1 = valsar[i];
                    var val1ar = val1.split(",")
                    ntrid = val1ar[4];
                    if (val == uid) {
                        uidcnt += 1;
                    }
                    if (val == uid && uidcnt > 1) {
                        chkflg = 1;
                    }
                    else {
                        if (newvalsvar == "") {
                            newvalsvar = val;
                            newvals = val1;
                        }
                        else {
                            newvalsvar = newvalsvar + "~" + val;
                            newvals = newvals + "~" + val1;
                        }
                    }
                    if (chkflg == "1") {
                        document.getElementById(ntrid).className = "bgwt";
                    }
                    else {
                        document.getElementById(ntrid).className = "bggr";
                    }
                    chkflg = 0;

                }
                if (newvals != "") {
                    document.getElementById("lblrvals").value = newvals;
                    document.getElementById("lblrvalsvar").value = newvalsvar;
                }
            }
        }
        function checkfrom() {
            var vals = document.getElementById("lblrvals").value;
            if (vals != "") {
                document.getElementById("lblsubmit").value = "fromwo";
                document.getElementById("form1").submit();
            }
        }
        function getcal(fld) {
            var stat = document.getElementById("lblstat").value;
            var wt = document.getElementById("lblwt").value;
            var wo = document.getElementById("lblwonum").value;
            var dtyp = document.getElementById("lbldtyp").value;
            var fldret = "txt" + fld;
            var chk = "";
            var cdt = "";
            var sdt = "";
            //if(fld=="tstart") {
            //chk = document.getElementById("txttcomp").value;
            //}
            if (fld == "sstart") {
                chk = document.getElementById("tdestcomp").innerHTML;
                cdt = chk;
                sdt = document.getElementById("tdeststart").innerHTML;
                fld = "estart"
            }
            if (fld == "scomp") {
                chk = document.getElementById("tdestcomp").innerHTML;
                cdt = chk;
                chk = "no"
                sdt = document.getElementById("tdeststart").innerHTML;
                fld = "ecomp"
            }
            if (chk == "") {
                chk = "yes";
            }
            else {
                chk = "no";
            }
            //if(stat!="COMP"&&stat!="CAN"&&wt!="PM"&&wt!="TPM") {
            //window.parent.setref();
            var eReturn = window.showModalDialog("../controls/caldialog2.aspx?typ=wo&who=" + fld + "&chk=" + chk + "&wo=" + wo + "&cdt=" + cdt + "&sdt=" + sdt + "&dtyp=" + dtyp, "", "dialogHeight:425px; dialogWidth:425px; resizable=yes");
            if (eReturn) {
                /*
                var fldret= "txt" + fld;	
                document.getElementById(fldret).value = eReturn;
                if(fld=="tstart") {
                document.getElementById("txttcomp").value = eReturn;
                }
                if(fld=="sstart") {
                document.getElementById("txtscomp").value = eReturn;
                }
                if(fld=="astart") {
                document.getElementById("txtacomp").value = eReturn;
                }
                */
                document.getElementById("lblsubmit").value = "reload";
                document.getElementById("form1").submit();
            }
            //}
        }
        //-->
    </script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 0px; left: 0px" width="720">
        <tr>
            <td colspan="3">
                <table>
                    <tr id="trsrch" runat="server">
                        <td class="bluelabel" width="120">
                            <asp:Label ID="lang2984" runat="server">Search By Name</asp:Label>
                        </td>
                        <td width="120">
                            <asp:TextBox ID="txtsrch" runat="server" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td width="44">
                            <asp:ImageButton ID="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                            </asp:ImageButton><img onclick="resetpage();" alt="" src="../images/appbuttons/minibuttons/switch.gif">
                        </td>
                        <td class="bluelabel" width="140">
                            Work Order#
                        </td>
                        <td id="tdwonum" class="plainlabel" runat="server">
                        </td>
                    </tr>
                    <tr id="trskill" runat="server">
                        <td class="bluelabel">
                            <asp:Label ID="lang2985" runat="server">Filter By Skill</asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:DropDownList ID="ddskill" runat="server" CssClass="plainlabel" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="bluelabel">
                            Est Labor Hours
                        </td>
                        <td id="tdest" class="plainlabel" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel" height="20">
                            <asp:Label ID="lang2986" runat="server">Filter by Shift</asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:DropDownList ID="ddshift" runat="server" CssClass="plainlabel" AutoPostBack="True">
                                <asp:ListItem Value="0" Selected="True">All</asp:ListItem>
                                <asp:ListItem Value="1">1st Shift</asp:ListItem>
                                <asp:ListItem Value="2">2nd Shift</asp:ListItem>
                                <asp:ListItem Value="3">3rd Shift</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="bluelabel">
                            Assigned Labor Hours
                        </td>
                        <td id="tdass" class="plainlabel" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel">
                            Assign\Change Work Area
                        </td>
                        <td id="tdwa" class="plainlabel" runat="server">
                        </td>
                        <td>
                            <img onclick="getwa('d');" src="../images/appbuttons/minibuttons/magnifier.gif"><img
                                onclick="resetwa();" alt="" src="../images/appbuttons/minibuttons/switch.gif">
                        </td>
                        <td id="tdschedstart" class="bluelabel" runat="server">
                            Scheduled Start Date
                        </td>
                        <td id="tdeststart" class="plainlabel" runat="server">
                        </td>
                        <td>
                            <img onclick="getcal('sstart');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                width="19" height="19">
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel">
                            Assign\Change Supervisor
                        </td>
                        <td>
                            <asp:TextBox ID="txtsup" runat="server" CssClass="plainlabel" Width="160px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <img id="imgsuper" onclick="getsuper('sup');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                runat="server"><img onclick="resetsuper();" alt="" src="../images/appbuttons/minibuttons/switch.gif">
                        </td>
                        <td id="tdschedcomp" class="bluelabel" runat="server">
                            Scheduled Complete Date
                        </td>
                        <td id="tdestcomp" class="plainlabel" runat="server">
                        </td>
                        <td>
                            <img onclick="getcal('scomp');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                width="19" height="19">
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel" colspan="3">
                            <input id="cbsup" checked type="checkbox" runat="server" name="cbsup">Filter By
                            Supervisor
                        </td>
                        <td class="redlabel">
                            Labor Hours Entry*
                        </td>
                        <td>
                            <asp:TextBox ID="txthrs" runat="server" CssClass="plainlabel" Width="48px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel" colspan="3">
                            <input id="cbwa" checked type="checkbox" name="Checkbox1" runat="server">Filter
                            By Work Area
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="20">
            <td class="label" align="center">
                <asp:Label ID="lang2988" runat="server">Available</asp:Label>
            </td>
            <td>
            </td>
            <td class="label" align="center">
                <asp:Label ID="lblawo" runat="server">Assigned to Work Order</asp:Label>
            </td>
        </tr>
        <tr>
            <td width="350" align="center">
                <div style="border-bottom: black 1px solid; border-left: black 1px solid; width: 350px;
                    height: 200px; overflow: auto; border-top: black 1px solid; border-right: black 1px solid"
                    id="supdiv" runat="server">
                </div>
            </td>
            <td valign="middle" width="30" align="center">
                <img id="todis" class="details" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                    width="20" height="20" runat="server">
                <img id="fromdis" class="details" src="../images/appbuttons/minibuttons/backgraybg.gif"
                    width="20" height="20" runat="server">
                <img id="btntocomp" onclick="checkto();" src="../images/appbuttons/minibuttons/forwardgbg.gif"
                    runat="server">
                <img id="btnfromcomp" onclick="checkfrom();" src="../images/appbuttons/minibuttons/backgbg.gif"
                    runat="server">
            </td>
            <td width="350" align="center">
                <div style="border-bottom: black 1px solid; border-left: black 1px solid; width: 350px;
                    height: 200px; overflow: auto; border-top: black 1px solid; border-right: black 1px solid"
                    id="supdiv1" runat="server">
                </div>
            </td>
        </tr>
        <tr>
            <td id="tdmsg1" class="plainlabelred" colspan="3" align="center" runat="server">
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lbllhid" type="hidden" name="lbllhid" runat="server">
    <input id="lbllid" type="hidden" name="lbllid" runat="server">
    <input id="lblapp" type="hidden" name="lblapp" runat="server">
    <input id="lblopt" type="hidden" name="lblopt" runat="server"><input id="lblfailchk"
        type="hidden" name="lblfailchk" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server"><input id="lbllog"
        type="hidden" name="lbllog" runat="server">
    <input id="lblsupid" type="hidden" name="lblsupid" runat="server"><input id="lbltyp"
        type="hidden" name="lbltyp" runat="server">
    <input id="lblup" type="hidden" name="lblup" runat="server"><input id="lblro" type="hidden"
        name="lblro" runat="server">
    <input id="lblskill" type="hidden" name="lblskill" runat="server">
    <input id="lblfilt" type="hidden" name="lblfilt" runat="server">
    <input id="lblwonum" type="hidden" runat="server" name="lblwonum">
    <input id="lblestlh" type="hidden" runat="server" name="lblestlh">
    <input id="lblstart" type="hidden" runat="server" name="lblstart">
    <input id="lblcomp" type="hidden" runat="server" name="lblcomp">
    <input id="lblsubmit" type="hidden" runat="server" name="lblsubmit">
    <input id="lblsup" type="hidden" runat="server" name="lblsup">
    <input id="lblass" type="hidden" runat="server" name="lblass">
    <input id="lblworkarea" type="hidden" runat="server" name="lblworkarea">
    <input id="lblwaid" type="hidden" runat="server" name="lblwaid">
    <input id="lblvals" type="hidden" runat="server" name="lblvals">
    <input id="lblreturn" type="hidden" runat="server" name="lblreturn">
    <input id="lblvalsvar" type="hidden" runat="server" name="lblvalsvar">
    <input id="lblrvals" type="hidden" name="lblrvals" runat="server">
    <input id="lblrvalsvar" type="hidden" name="lblrvalsvar" runat="server">
    <input id="lblstat" type="hidden" runat="server" name="lblstat">
    <input id="lblwt" type="hidden" runat="server" name="lblwt">
    <input id="lbldtyp" type="hidden" runat="server" name="lbldtyp">
    <input type="hidden" id="lblisflg" runat="server" name="lblisflg">
    <input type="hidden" id="lblpmid" runat="server">
    </form>
</body>
</html>
