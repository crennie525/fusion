﻿Imports System.Data.SqlClient
Public Class wolabtransfs
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim trans As New Utilities
    Dim ap As New AppUtils
    Dim sql As String
    Dim dr As SqlDataReader
    Dim wonum, stat, ro, rostr, Login, sid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetDGLangs()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            AutoCompleteExtender3.CompletionSetCount = sid
            wonum = Request.QueryString("wo").ToString
            lblwo.Value = wonum
            stat = Request.QueryString("stat").ToString
            lblstat.Value = stat
            Dim acost As String = ap.ActCost()
            lblacost.Value = acost
            If (stat <> "COMP" And stat <> "CAN" And stat <> "WAPPR") And acost <> "lcaext" Then
                dgparts.Columns(0).Visible = True
                dgparts.Columns(11).Visible = True
            Else
                dgparts.Columns(0).Visible = False
                dgparts.Columns(11).Visible = False
            End If
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                dgparts.Columns(0).Visible = False
                dgparts.Columns(11).Visible = False
                addnew.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                addnew.Attributes.Add("onclick", "")
            Else
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = trans.CheckROS(rostr, "wo")
                    lblro.Value = ro
                    If ro = "1" Then
                        dgparts.Columns(0).Visible = False
                        addnew.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                        addnew.Attributes.Add("onclick", "")
                    Else
                        addnew.Attributes.Add("onclick", "addnew1();")
                    End If
                End If

            End If
            Dim tentry As String = ap.TimeEntry()
            lbltentry.Value = tentry
            If acost <> "lcaext" Then
                If tentry = "lvst" Then
                    dgparts.Columns(4).Visible = True
                    dgparts.Columns(5).Visible = True
                    dgparts.Columns(6).Visible = True
                    dgparts.Columns(7).Visible = False
                    tdstat.InnerHtml = "Default is Start Time \ Stop Time Entry"
                Else
                    dgparts.Columns(4).Visible = False
                    dgparts.Columns(5).Visible = False
                    dgparts.Columns(6).Visible = False
                    dgparts.Columns(7).Visible = True
                    tdstat.InnerHtml = "Default is Time Entry Only"
                End If
            Else
                dgparts.Columns(4).Visible = True
                dgparts.Columns(5).Visible = True
                dgparts.Columns(6).Visible = False
                dgparts.Columns(7).Visible = True
                tdstat.InnerHtml = "Default is External Entry"
            End If

            trans.Open()
            PopLead(wonum)
            GetLabTrans()
            GetWorkType()
            trans.Dispose()
            If wonum = "" Then
                dgparts.Columns(0).Visible = False
            End If
            Try
                ddlead.SelectedIndex = 0
            Catch ex As Exception

            End Try

        Else
            If Request.Form("lblsubmit") = "addnew" Then
                lblsubmit.Value = ""
                trans.Open()
                AddTrans()
                trans.Dispose()
            End If
        End If
    End Sub
    Private Sub PopLead(ByVal wonum As String)
        sql = "select count(*) from woassign where wonum = '" & wonum & "'"
        Dim woacnt As Integer = 0
        woacnt = trans.Scalar(sql)
        If woacnt = 0 Then
            sql = "select leadcraftid as userid, leadcraft as username from workorder where wonum = '" & wonum & "'"
        Else
            sql = "select userid, username from woassign where wonum = '" & wonum & "'"
        End If

        dr = trans.GetRdrData(sql)
        ddlead.DataSource = dr
        ddlead.DataValueField = "userid"
        ddlead.DataTextField = "username"
        ddlead.DataBind()
        dr.Close()
        ddlead.Items.Insert("0", "Select")
    End Sub

    Private Sub AddTrans()
        wonum = lblwo.Value
        Dim ld, ldi, wd As String
        Dim echk As Integer = 0
        ld = txtleadauto.Text
        'ldi = lbllead.Value
        Dim ncnt As Integer = 0
        sql = "select userid from pmsysusers where username = '" & ld & "'"
        ncnt = trans.Scalar(sql)
        wd = txtwd.Text
        wd = lblwd.Value
        If ld <> "" And wd <> "" And ncnt <> 0 Then
            echk = 1
            sql = "insert into wolabtrans (laborid, name, transdate, wonum) values " _
            + "('" & ldi & "','" & ld & "','" & wd & "','" & wonum & "')"
            trans.Update(sql)
        End If
        If ddlead.SelectedValue <> "0" And ddlead.SelectedIndex <> 0 And ddlead.SelectedIndex <> -1 Then
            echk = 1
            ldi = ddlead.SelectedValue
            ld = ddlead.SelectedItem.ToString
            sql = "insert into wolabtrans (laborid, name, transdate, wonum) values " _
            + "('" & ldi & "','" & ld & "','" & wd & "','" & wonum & "')"
            trans.Update(sql)
        End If
        If echk = 1 Then
            Dim strMessage As String = "Lead Craft Name Entry was Not Found"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        GetLabTrans()
        Try
            ddlead.SelectedIndex = 0
        Catch ex As Exception

        End Try

        lblname.Value = ""
        lbllead.Value = ""
        tdlead.InnerHtml = ""
    End Sub
    Private Sub GetLabTrans()

        wonum = lblwo.Value
        sql = "usp_getwolabtrans '" & wonum & "'"
        dr = trans.GetRdrData(sql)
        dgparts.DataSource = dr
        dgparts.DataBind()
        dr.Close()


    End Sub
    Private Sub GetWorkType()
        sql = "select worktype, isnull(multidates, 1) as multidates, isnull(multicnt, 0) as multicnt from workorder"
        dr = trans.GetRdrData(sql)
        While dr.Read
            lblworktype.Value = dr.Item("worktype").ToString
            lblruns.Value = dr.Item("multidates").ToString
            lblworuns.Value = dr.Item("multicnt").ToString
        End While
        dr.Close()
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        Dim cati As Integer
        If Len(CatID) <> 0 Then
            cati = CType(CatID, Integer)
            iL = CatID '- 1
        Else
            iL = 0
        End If
        Return iL
    End Function
    Function GetSelIndexM(ByVal CatID As String) As Integer
        Dim iL As Integer
        Dim cati As Integer
        If Len(CatID) <> 0 Then
            cati = CType(CatID, Integer)
            If CatID < 15 Then
                iL = 0
            ElseIf CatID < 30 Then
                iL = 1
            ElseIf CatID < 45 Then
                iL = 2
            Else
                iL = 3
            End If
        Else
            iL = 0
        End If
        Return iL
    End Function
    Function GetSelIndexAP(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Len(CatID) <> 0 Then
            If CatID = "AM" Then
                iL = 0
            Else
                iL = 1
            End If
        Else
            iL = 0
        End If
        Return iL
    End Function

    Private Sub dgparts_CancelCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.CancelCommand
        dgparts.EditItemIndex = -1
        trans.Open()
        GetLabTrans()
        trans.Dispose()
    End Sub

    Private Sub dgparts_DeleteCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.DeleteCommand
        Dim id As String
        Try
            id = CType(e.Item.FindControl("lblwoltid"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lblwoltidi"), Label).Text
        End Try

        sql = "delete from wolabtrans where woltid = '" & id & "'; " _
        + "update workorder set actlabhrs = (select (((sum(minutes) * 100) / 60) * .01) from wolabtrans where wonum = '" & wonum & "'), " _
        + "actlabcost = (select sum(cost) from wolabtrans where wonum = '" & wonum & "') " _
        + "where wonum = '" & wonum & "'"
        trans.Open()
        trans.Update(sql)
        trans.Dispose()
    End Sub

    Private Sub dgparts_EditCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.EditCommand
        dgparts.EditItemIndex = e.Item.ItemIndex
        trans.Open()
        GetLabTrans()
        trans.Dispose()
    End Sub

    Private Sub dgparts_UpdateCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.UpdateCommand
        Dim dhrs, dmins, daps, dhre, dmine, dape As DropDownList
        wonum = lblwo.Value
        Dim tentry As String = lbltentry.Value
        Dim acost As String = lblacost.Value

        dhrs = CType(e.Item.FindControl("ddhrs"), DropDownList)
        dmins = CType(e.Item.FindControl("ddmins"), DropDownList)
        daps = CType(e.Item.FindControl("ddaps"), DropDownList)
        dhre = CType(e.Item.FindControl("ddhre"), DropDownList)
        dmine = CType(e.Item.FindControl("ddmine"), DropDownList)
        dape = CType(e.Item.FindControl("ddape"), DropDownList)

        Dim shrs, smins, saps, shre, smine, sape
        shrs = dhrs.SelectedValue
        smins = dmins.SelectedValue
        saps = daps.SelectedValue
        shre = dhre.SelectedValue
        smine = dmine.SelectedValue
        sape = dape.SelectedValue

        Dim id As String = CType(e.Item.FindControl("lblwoltid"), Label).Text 'DataBinder.Eval(e.Item.DataItem, "woltid").ToString
        Dim wd As String = CType(e.Item.FindControl("lbltransdate"), Label).Text 'DataBinder.Eval(e.Item.DataItem, "transdate").ToString
        Dim rate As String
        If acost = "lcask" Then
            rate = CType(e.Item.FindControl("lblsrate"), Label).Text
        Else
            rate = CType(e.Item.FindControl("lbllrate"), Label).Text
        End If

        'Dim strMessage1 As String = rate
        'Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")

        Dim sdate, sedate, mins As String
        Dim ndate, nedate As Date

        If tentry = "lvst" Then
            If shrs <> "NA" Then

                sdate = wd & " " & shrs & ":" & smins & ":00 " & saps
                ndate = CType(sdate, Date)
            End If

            If shre <> "NA" Then

                sedate = wd & " " & shre & ":" & smine & ":00 " & sape
                nedate = CType(sedate, Date)
            End If

            If shrs <> "NA" And shre <> "NA" Then
                sql = "update wolabtrans set starttime = cast('" & ndate & "' as datetime), stoptime = cast('" & nedate & "' as datetime) " _
                + "where woltid = '" & id & "'; " _
                + "update wolabtrans set cost = " & rate & " * (((minutes * 100) / 60) * .01) where woltid = '" & id & "'; " _
                + "update workorder set actlabhrs = (select (((sum(minutes) * 100) / 60) * .01) from wolabtrans where wonum = '" & wonum & "'), " _
                + "actlabcost = (select sum(cost) from wolabtrans where wonum = '" & wonum & "') " _
                + "where wonum = '" & wonum & "'"
            ElseIf shrs <> "NA" And shre = "NA" Then
                sql = "update wolabtrans set starttime = '" & ndate & "' where woltid = '" & id & "'"
            ElseIf shrs = "NA" & shre <> "NA" Then
                Dim strMessage As String = tmod.getmsg("cdstr585", "wolabtrans.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        Else
            mins = CType(e.Item.FindControl("txtcostm"), TextBox).Text
            If mins = "" Then
                mins = "0"
            End If
            Dim minchk As Decimal

            Try
                minchk = System.Convert.ToDecimal(mins)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr586", "wolabtrans.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            sql = "update wolabtrans set minutes_man = '" & mins & "' " _
            + "where woltid = '" & id & "'; " _
            + "update wolabtrans set cost = " & rate & " * (((minutes_man * 100) / 60) * .01) where woltid = '" & id & "'; " _
            + "update workorder set actlabhrs = (select (((sum(minutes_man) * 100) / 60) * .01) from wolabtrans where wonum = '" & wonum & "'), " _
            + "actlabcost = (select sum(cost) from wolabtrans where wonum = '" & wonum & "') " _
            + "where wonum = '" & wonum & "'"
        End If


        trans.Open()
        trans.Update(sql)
        dgparts.EditItemIndex = -1
        GetLabTrans()
        trans.Dispose()
    End Sub
    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgparts.Columns(1).HeaderText = dlabs.GetDGPage("wolabtrans.aspx", "dgparts", "1")
        Catch ex As Exception
        End Try
        Try
            dgparts.Columns(3).HeaderText = dlabs.GetDGPage("wolabtrans.aspx", "dgparts", "3")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1518.Text = axlabs.GetASPXPage("wolabtrans.aspx", "lang1518")
        Catch ex As Exception
        End Try
        Try
            lang1519.Text = axlabs.GetASPXPage("wolabtrans.aspx", "lang1519")
        Catch ex As Exception
        End Try
        Try
            lang1520.Text = axlabs.GetASPXPage("wolabtrans.aspx", "lang1520")
        Catch ex As Exception
        End Try
        Try
            lang1521.Text = axlabs.GetASPXPage("wolabtrans.aspx", "lang1521")
        Catch ex As Exception
        End Try
        Try
            lang1522.Text = axlabs.GetASPXPage("wolabtrans.aspx", "lang1522")
        Catch ex As Exception
        End Try

    End Sub
End Class