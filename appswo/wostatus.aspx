<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wostatus.aspx.vb" Inherits="lucy_r12.wostatus" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Work Order Status Dialog</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/wostatusaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        <!--
        function getstat(stat) {
        
            document.getElementById("lblwostatus").value = stat;
            document.getElementById("lblsubmit").value = "savewo";
            var wo = document.getElementById("lblwonum").value;
            if (wo != "") {
                document.getElementById("form1").submit();
            }
            else {
                //alert(stat)
                window.parent.handlestat(stat);
            }
        }
        function checkit() {
            var chk = document.getElementById("lblsubmit").value;
            if (chk == "return") {
                var stat = document.getElementById("lblwostatus").value;
                //alert(stat)
                window.parent.handlestat(stat);
            }
        }
//-->
    </script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 4px; left: 4px">
        <tr>
            <td>
                <div id="statdiv" style="border-bottom: black 1px solid; border-left: black 1px solid;
                    height: 200px; overflow: auto; border-top: black 1px solid; border-right: black 1px solid"
                    runat="server">
                </div>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblwostatus" runat="server">
    <input type="hidden" id="lblwonum" runat="server">
    <input type="hidden" id="lblsubmit" runat="server">
    <input type="hidden" id="lbltyp" runat="server">
    </form>
</body>
</html>
