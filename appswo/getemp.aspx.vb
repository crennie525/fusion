Imports System.Data.SqlClient

Public Class getemp
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim sav As New Utilities
    Dim uid, emp, islead, hrs, wonum, ohrs, adate, wid, shift, pmid, worktype, issched As String
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhrs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblislead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblemp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tddate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbladate As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblshift As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblworktype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbusemin As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblissched As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdemp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cblead As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents txthrs As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            wid = Request.QueryString("wid").ToString
            uid = Request.QueryString("uid").ToString
            wonum = Request.QueryString("wo").ToString
            emp = Request.QueryString("emp").ToString
            islead = Request.QueryString("islead").ToString
            hrs = Request.QueryString("hrs").ToString
            adate = Request.QueryString("adate").ToString
            shift = Request.QueryString("shift").ToString
            worktype = Request.QueryString("worktype").ToString
            pmid = Request.QueryString("pmid").ToString
            issched = Request.QueryString("issched").ToString
            lblwid.Value = wid
            lbluid.Value = uid
            lblwonum.Value = wonum
            tdwo.InnerHtml = wonum
            tdemp.InnerHtml = emp
            lblemp.Value = emp
            txthrs.Text = hrs
            lblhrs.Value = hrs
            If islead = "1" Then
                cblead.Checked = True
            End If
            lblislead.Value = islead
            tddate.InnerHtml = adate
            lbladate.Value = adate
            lblshift.Value = shift
            lblworktype.Value = worktype
            lblpmid.Value = pmid
            lblissched.Value = issched
        Else
            If Request.Form("lblsubmit") = "savewo" Then
                lblsubmit.Value = ""
                wonum = lblwonum.Value
                uid = lbluid.Value
                emp = lblemp.Value
                adate = lbladate.Value
                wid = lblwid.Value
                sav.Open()
                savewo(uid, wonum, emp, adate, wid)
                sav.Dispose()
            End If
        End If
    End Sub
    Private Sub savewo(ByVal uid As String, ByVal wonum As String, ByVal emp As String, ByVal adate As String, ByVal wid As String)
        issched = lblissched.Value
        If cblead.Checked = True Then
            islead = "1"
        Else
            islead = "0"
        End If
        shift = lblshift.Value
        worktype = lblworktype.Value
        pmid = lblpmid.Value
        If islead = "1" Then
            If wonum <> "" Then
                sql = "update workorder set leadcraft = '" & emp & "', leadcraftid = '" & uid & "' " _
           + "where wonum = '" & wonum & "'; update woassign set islead = null " _
           + "where wonum = '" & wonum & "'; update woassign set islead = '1' " _
           + "where wonum = '" & wonum & "' and userid = '" & uid & "'"
                sav.Update(sql)
                
            End If

            If (worktype = "PM" Or worktype = "TPM" Or worktype = "PMMAX") And pmid <> "" Then
                sql = "update pm set leadid = '" & uid & "', lead = '" & emp & "' where pmid = '" & pmid & "'"
                sav.Update(sql)
                sql = "update pmassign set islead = null " _
          + "where pmid = '" & pmid & "'; update pmassign set islead = '1' " _
          + "where pmid = '" & pmid & "' and userid = '" & uid & "'"
                sav.Update(sql)
            Else
                sql = "update pmassign set islead = null " _
           + "where pmid = '" & pmid & "'; update pmassign set islead = '1' " _
           + "where pmid = '" & pmid & "' and userid = '" & uid & "'"
                sav.Update(sql)
            End If
        Else
            If wonum <> "" Then
                sql = "update workorder set leadcraft = null, leadcraftid = null " _
           + "where wonum = '" & wonum & "'; update woassign set islead = null " _
           + "where wonum = '" & wonum & "'; "
                sav.Update(sql)
                
            End If

            If (worktype = "PM" Or worktype = "TPM" Or worktype = "PMMAX") And pmid <> "" Then
                sql = "update pm set leadid = null, lead = null where pmid = '" & pmid & "'"
                sav.Update(sql)
                sql = "update pmassign set islead = null " _
          + "where pmid = '" & pmid & "'; "
                sav.Update(sql)
            Else
                sql = "update pmassign set islead = null " _
           + "where pmid = '" & pmid & "'; "
                sav.Update(sql)
            End If
        End If
        Dim hrsflg As Integer = 0
        Dim diffflg As Integer = 0
        Dim addflg As Integer = 0
        Dim remflg As Integer = 0
        hrs = txthrs.Text
        ohrs = lblhrs.Value
        Dim am, rm As String
        If hrs <> ohrs Then
            Dim tnchk As Decimal
            Try
                tnchk = System.Convert.ToDouble(hrs)
            Catch ex As Exception
                hrs = ohrs
                hrsflg = 1
            End Try
            Dim onchk As Decimal
            Try
                onchk = System.Convert.ToDouble(ohrs)
            Catch ex As Exception
                ohrs = "0"
            End Try
            Dim remhrs As Decimal
            If tnchk < onchk Then
                remhrs = onchk - tnchk
                remflg = 1
            End If
            Dim addhrs As Decimal
            If tnchk > onchk Then
                addhrs = tnchk - onchk
                remflg = 0
            End If

            Try
                If tnchk < onchk Then
                    diffflg = 1
                ElseIf tnchk > onchk Then
                    diffflg = 2
                Else
                    diffflg = 0
                End If
            Catch ex As Exception
                diffflg = 0
            End Try

            If hrsflg <> 1 And diffflg <> 0 Then
                If cbusemin.Checked = True Then
                    If wonum <> "" Then
                        sql = "update woassign set assigned = (cast(" & hrs & " as decimal(10,2)) / 60) " _
                                    + "where woassignid = '" & wid & "'"
                    Else
                        sql = "update pmassign set assigned = (cast(" & hrs & " as decimal(10,2)) / 60) " _
                                    + "where pmassignid = '" & wid & "'"
                    End If

                Else
                    If wonum <> "" Then
                        sql = "update woassign set assigned = '" & hrs & "' " _
                                    + "where woassignid = '" & wid & "'; update pmassign set assigned = '" & hrs & "' " _
                                    + "where pmassignid = '" & wid & "'"
                    Else
                        sql = "update pmassign set assigned = '" & hrs & "' " _
                                    + "where pmassignid = '" & wid & "'"
                    End If

                End If

                sav.Update(sql)
                If diffflg = 1 Then
                    If cbusemin.Checked = True Then
                        If issched = "1" Then
                            If adate <> "" And adate <> "N/A" Then
                                sql = "update labavail set assigned = assigned - (cast(" & remhrs & " as decimal(10,2)) / 60), " _
                           + "avail = avail + (cast(" & remhrs & " / 60) where " _
                           + "userid = '" & uid & "' " _
                           + "and Convert(char(10),wdate,101) = Convert(char(10),cast(" & adate & " as datetime),101)"
                                sav.Update(sql)
                            End If
                        End If

                        If wonum <> "" Then
                            If shift = "1" Then
                                sql = "update workorder set woa1hrs = isnull(woa1hrs,0) - (cast(" & remhrs & " as decimal(10,2)) / 60) " _
                                + "where wonum = '" & wonum & "'"
                            ElseIf shift = "2" Then
                                sql = "update workorder set woa2hrs = isnull(woa2hrs,0) - (cast(" & remhrs & " as decimal(10,2)) / 60) " _
                                + "where wonum = '" & wonum & "'"
                            ElseIf shift = "3" Then
                                sql = "update workorder set woa3hrs = isnull(woa3hrs,0) - (cast(" & remhrs & " as decimal(10,2)) / 60) " _
                                + "where wonum = '" & wonum & "'"
                            End If
                            sav.Update(sql)
                        End If

                    Else
                        If issched = "1" Then
                            If adate <> "" And adate <> "N/A" Then
                                sql = "update labavail set assigned = assigned - cast(" & remhrs & " as decimal(10,2)), " _
                           + "avail = avail + cast(" & remhrs & " as decimal(10,2)) where " _
                           + "userid = '" & uid & "' " _
                           + "and Convert(char(10),wdate,101) = Convert(char(10),cast(" & adate & " as datetime),101)"
                                sav.Update(sql)
                            End If
                        End If

                        If wonum <> "" Then
                            If shift = "1" Then
                                sql = "update workorder set woa1hrs = isnull(woa1hrs,0) - cast(" & remhrs & " as decimal(10,2)) " _
                                + "where wonum = '" & wonum & "'"
                            ElseIf shift = "2" Then
                                sql = "update workorder set woa2hrs = isnull(woa2hrs,0) - cast(" & remhrs & " as decimal(10,2)) " _
                                + "where wonum = '" & wonum & "'"
                            ElseIf shift = "3" Then
                                sql = "update workorder set woa3hrs = isnull(woa3hrs,0) - cast(" & remhrs & " as decimal(10,2)) " _
                                + "where wonum = '" & wonum & "'"
                            End If
                            sav.Update(sql)
                        End If

                        End If

                Else
                        If cbusemin.Checked = True Then
                            If issched = "1" Then
                                If adate <> "" And adate <> "N/A" Then

                                    sql = "update labavail set assigned = assigned + (cast(" & addhrs & " as decimal(10,2)) / 60), " _
                               + "avail = avail - (cast(" & addhrs & " as decimal(10,2)) / 60) where " _
                               + "userid = '" & uid & "' " _
                               + "and Convert(char(10),wdate,101) = Convert(char(10),cast('" & adate & "' as datetime),101)"
                                    sav.Update(sql)
                                End If
                            End If

                            If wonum <> "" Then
                                If shift = "1" Then
                                    sql = "update workorder set woa1hrs = isnull(woa1hrs,0) + (cast(" & addhrs & " as decimal(10,2)) / 60) " _
                                    + "where wonum = '" & wonum & "'"
                                ElseIf shift = "2" Then
                                    sql = "update workorder set woa2hrs = isnull(woa2hrs,0) + (cast(" & addhrs & " as decimal(10,2)) / 60) " _
                                    + "where wonum = '" & wonum & "'"
                                ElseIf shift = "3" Then
                                    sql = "update workorder set woa3hrs = isnull(woa3hrs,0) + (cast(" & addhrs & " as decimal(10,2)) / 60) " _
                                    + "where wonum = '" & wonum & "'"
                                End If
                                sav.Update(sql)
                            End If


                        Else
                            If issched = "1" Then
                                If adate <> "" And adate <> "N/A" Then
                                    sql = "update labavail set assigned = assigned + cast(" & addhrs & " as decimal(10,2)), " _
                               + "avail = avail - cast(" & addhrs & " as decimal(10,2)) where " _
                               + "userid = '" & uid & "' " _
                               + "and Convert(char(10),wdate,101) = Convert(char(10),cast('" & adate & "' as datetime),101)"
                                    sav.Update(sql)
                                End If
                            End If


                            If wonum <> "" Then
                                If shift = "1" Then
                                    sql = "update workorder set woa1hrs = isnull(woa1hrs,0) + cast(" & addhrs & " as decimal(10,2)) " _
                                    + "where wonum = '" & wonum & "'"
                                ElseIf shift = "2" Then
                                    sql = "update workorder set woa2hrs = isnull(woa2hrs,0) + cast(" & addhrs & " as decimal(10,2)) " _
                                    + "where wonum = '" & wonum & "'"
                                ElseIf shift = "3" Then
                                    sql = "update workorder set woa3hrs = isnull(woa3hrs,0) + cast(" & addhrs & " as decimal(10,2)) " _
                                    + "where wonum = '" & wonum & "'"
                                End If
                                sav.Update(sql)
                            End If

                        End If
                End If
            End If
        End If
        lblsubmit.Value = "return"

    End Sub
End Class
