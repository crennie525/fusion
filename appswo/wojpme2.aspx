<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wojpme2.aspx.vb" Inherits="lucy_r12.wojpme2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wojpme2</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/wojpme2aspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="scrolltop();checkit();" class="tbg">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" cellspacing="0" cellpadding="2" width="700">
        <tr>
            <td>
                <table width="700">
                    <tr height="26">
                        <td class="label" width="80">
                            <asp:Label ID="lang1514" runat="server">Job Plan:</asp:Label>
                        </td>
                        <td class="plainlabel" id="tdjpn" width="120" runat="server">
                        </td>
                        <td class="plainlabel" id="tdjpd" width="410" colspan="6" runat="server">
                        </td>
                        <td class="label" align="right" width="250">
                        </td>
                    </tr>
                    <tr>
                        <td class="plainlabelred" id="tdnofail" colspan="9" runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="22">
            <td class="thdrsing label">
                <asp:Label ID="lang1515" runat="server">Job Plan Level Measurements</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DataGrid ID="dgmeas" runat="server" AutoGenerateColumns="False" Width="540px"
                    CellSpacing="1" AllowCustomPaging="True" AllowPaging="True" GridLines="None"
                    CellPadding="0" ShowFooter="True" BackColor="transparent">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <EditItemStyle Height="15px"></EditItemStyle>
                    <AlternatingItemStyle CssClass="prowtransblue plainlabel"></AlternatingItemStyle>
                    <ItemStyle CssClass="prowtrans plainlabel"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgeditfm" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="Imagebutton5" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Task#">
                            <HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label28" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label29" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif">
                            <HeaderStyle Width="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <img src="../images/appbuttons/minibuttons/magnifier.gif" id="imgmi" runat="server">
                            </ItemTemplate>
                            <EditItemTemplate>
                                <img src="../images/appbuttons/minibuttons/magnifier.gif" id="imgme" runat="server">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Type">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label3" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Hi">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label21" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Lo">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label22" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label23" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Spec">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label25" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label26" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Measurement">
                            <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label27" runat="server" Width="90px" Text='<%# DataBinder.Eval(Container, "DataItem.womeas") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtmeas" runat="server" Width="90px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.womeas") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <HeaderStyle></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblpmtskid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblpmtskida" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <HeaderStyle></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lbltmdid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.tmdid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lbltmdida" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.tmdid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
                        ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <div class="details" id="tskdiv" style="border-right: black 1px solid; border-top: black 1px solid;
        z-index: 999; border-left: black 1px solid; width: 450px; border-bottom: black 1px solid;
        position: absolute; height: 180px">
        <table cellspacing="0" cellpadding="0" width="450" bgcolor="white">
            <tr bgcolor="blue">
                <td>
                    &nbsp;
                    <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
                        ForeColor="White">Task Description</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closetsk();" alt="" src="../images/appbuttons/minibuttons/close.gif"><br>
                </td>
            </tr>
            <tr class="tbg" height="30">
                <td style="padding-right: 3px; padding-left: 3px; padding-bottom: 3px; padding-top: 3px"
                    colspan="2">
                    <asp:TextBox ID="txttsk" runat="server" TextMode="MultiLine" ReadOnly="True" Width="440px"
                        CssClass="plainlabel" Height="170px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <input id="lbljpid" type="hidden" name="lbljpid" runat="server">
    <input id="lblwo" type="hidden" name="lblwo" runat="server">
    <input id="lbltaskcnt" type="hidden" name="lbltaskcnt" runat="server">
    <input id="lbltaskcurrcnt" type="hidden" name="lbltaskcurrcnt" runat="server">
    <input id="lblrow" type="hidden" name="lblrow" runat="server">
    <input id="lblfmcnt" type="hidden" name="lblfmcnt" runat="server">
    <input id="lbltasknum" type="hidden" name="lbltasknum" runat="server">
    <input id="lblcurrcnt" type="hidden" name="lblcurrcnt" runat="server">
    <input id="xCoord" type="hidden" name="xCoord" runat="server">
    <input id="yCoord" type="hidden" name="yCoord" runat="server"><input id="lblsubmit"
        type="hidden" name="lblsubmit" runat="server">
    <input id="lblfmstr" type="hidden" name="lblfmstr" runat="server">
    <input id="lblmstr" type="hidden" name="lblmstr" runat="server">
    <input id="lblmstrvals" type="hidden" name="lblmstrvals" runat="server">
    <input id="lblpstr" type="hidden" name="lblpstr" runat="server">
    <input id="lbltstr" type="hidden" name="lbltstr" runat="server">
    <input id="lbllstr" type="hidden" name="lbllstr" runat="server">
    <input id="lblmflg" type="hidden" name="lblmflg" runat="server"><input id="lblmcomp"
        type="hidden" name="lblmcomp" runat="server">
    <input type="hidden" id="lblstat" runat="server" name="lblstat">
    <input type="hidden" id="lbllog" runat="server" name="lbllog">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
