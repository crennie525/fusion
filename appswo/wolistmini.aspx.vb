

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class wolistmini
    Inherits System.Web.UI.Page
	Protected WithEvents lang1547 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim mm As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 12
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim rowcnt As Integer
    Dim usid, nme, typ, eqid, fuid, coid, sid, cid, lid, did, clid As String
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtwo As System.Web.UI.WebControls.TextBox
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtsrchwo As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents tdywr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            mm.Open()
            BuildMyWR(PageNumber)
            mm.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                mm.Open()
                GetNext()
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                mm.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                BuildMyWR(PageNumber)
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                mm.Open()
                GetPrev()
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                mm.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                BuildMyWR(PageNumber)
                mm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "srchwo" Then
                mm.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                BuildMyWR(PageNumber)
                mm.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub BuildMyWR(ByVal PageNumber As Integer)
        Dim sb As New StringBuilder
        Dim field, srch As String
        Dim srchi As Integer
        'usid = HttpContext.Current.Session("uid").ToString()
        'Dim uid1 As String = usid
        'uid1 = Replace(uid1, "'", "''")
        'nme = HttpContext.Current.Session("username").ToString()
        'Dim nme1 As String = nme
        'nme1 = Replace(nme, "'", "''")
        Filter = "(status <> ''COMP'' and status <> ''CAN'' and status <> ''WAPPR'') "
        FilterCNT = "(status <> 'COMP' and status <> 'CAN' and status <> 'WAPPR') "

        If txtsrchwo.Text <> "" Then
            Filter += "and wonum = ''" & txtsrchwo.Text & "'' "
            FilterCNT += "and wonum = '" & txtsrchwo.Text & "' "
        End If


        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 720px;  HEIGHT: 200px"">")
        sb.Append("<table cellspacing=""2"" width=""700""><tr height=""20"">" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""80px"">" & tmod.getlbl("cdlbl224" , "wolistmini.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""60px"">" & tmod.getlbl("cdlbl225" , "wolistmini.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""140px"">" & tmod.getlbl("cdlbl226" , "wolistmini.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""420px"">" & tmod.getlbl("cdlbl227" , "wolistmini.aspx.vb") & "</td></tr>" & vbCrLf)

        Dim intPgNav, intPgCnt As Integer

        sql = "SELECT Count(*) FROM workorder where " & FilterCNT



        intPgCnt = mm.Scalar(sql)
        PageSize = "100"
        intPgNav = mm.PageCount1(intPgCnt, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        Fields = "*, Convert(char(10),targstartdate, 101) as targstart, Convert(char(10),targcompdate, 101) as targcomp"
        Tables = "workorder"
        PK = "wonum"
        Sort = "reportdate desc"
        dr = mm.GetPageHack(Tables, PK, Sort, PageNumber, PageSize, Fields, FilterCNT, Group)

        Dim wo, eid, sid, loc, did, clid, chk, fid, cid, nid, stat, desc As String
        Dim rowflag As Integer = 0
        Dim bg As String
        'Dim stat As String

        While dr.Read
            If rowflag = 0 Then
                bg = "white"
                rowflag = 1
            Else
                bg = "#e7f1fd"
                rowflag = "0"
            End If

            eid = dr.Item("eqid").ToString
            nid = dr.Item("ncid").ToString
            sid = dr.Item("siteid").ToString
            loc = dr.Item("locid").ToString
            did = dr.Item("deptid").ToString
            clid = dr.Item("cellid").ToString
            cid = dr.Item("comid").ToString
            fid = dr.Item("funcid").ToString

            stat = dr.Item("status").ToString

            Dim typ, jp, pm As String
            typ = dr.Item("worktype").ToString
            jp = dr.Item("jpid").ToString
            If jp = "" Then
                jp = "0"
            End If
            pm = dr.Item("pmid").ToString
            If pm = "" Then
                pm = "0"
            End If
            If clid <> "" Then
                chk = "yes"
            Else
                chk = "no"
            End If

            wo = dr.Item("wonum").ToString
            stat = dr.Item("status").ToString
            desc = dr.Item("description").ToString
            desc = Replace(desc, "/", "//", , , vbTextCompare)
            sb.Append("<tr bgcolor=""" & bg & """><td class=""plainlabel""><a href=""#"" onclick=""gotoreq('" & wo & "','" & desc & "','" & jp & "','" & pm & "')"">" & wo & "</a></td>" & vbCrLf)
            sb.Append("<td class=""plainlabel"">" & typ & "</td>" & vbCrLf)
            sb.Append("<td class=""plainlabel"">" & dr.Item("chargenum").ToString & "</td>" & vbCrLf)
            sb.Append("<td class=""plainlabel"">" & dr.Item("description").ToString & "</td></tr>" & vbCrLf)


        End While
        dr.Close()
        sb.Append("</table></div>")
        tdywr.InnerHtml = sb.ToString
        'lblpg.Text = "Page " & dgwork.CurrentPageIndex + 1 & " of " & dgwork.PageCount
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            BuildMyWR(PageNumber)
        Catch ex As Exception
            mm.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr600" , "wolistmini.aspx.vb")
 
            mm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            BuildMyWR(PageNumber)
        Catch ex As Exception
            mm.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr601" , "wolistmini.aspx.vb")
 
            mm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1547.Text = axlabs.GetASPXPage("wolistmini.aspx", "lang1547")
        Catch ex As Exception
        End Try

    End Sub

End Class
