<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wojppart.aspx.vb" Inherits="lucy_r12.wojppart" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wojppart</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/wojppartaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td id="tdpart" runat="server" class="plainlabel">
            </td>
        </tr>
    </table>
    <input type="hidden" id="lbltyp" runat="server">
    <input type="hidden" id="lblpmtskid" runat="server">
    <input type="hidden" id="lblwo" runat="server">
    <input type="hidden" id="lblpstr" runat="server">
    <input type="hidden" id="lbltstr" runat="server">
    <input type="hidden" id="lbllstr" runat="server">
    <input type="hidden" id="lblsubmit" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
