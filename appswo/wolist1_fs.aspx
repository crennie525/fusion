﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wolist1_fs.aspx.vb" Inherits="lucy_r12.wolist1_fs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../menu/mmenu1.ascx" TagName="mmenu1" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Work Order Manager - Full Screen</title>
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
   
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts/sessrefdialog.js"></script>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
    <!--
        function checkent(event) {
            event = event || window.event 
        if (event.keyCode == 13) {
                var eq = document.getElementById("txteqauto").value;
                if (eq != "") {
                    //alert(eq)
                    get_eqauto();
                }
            }
        }

        function get_eqauto() {
            var coi = document.getElementById("lblcoi").value;
            var wo = ""; //document.getElementById("lblwonum").value; // 
            var sid = document.getElementById("lblsid").value;
            var eq = document.getElementById("txteqauto").value;
            var usr = document.getElementById("lblnme").value;
            if (coi == "SW") {
                if (eq != "" && sid != "") {
                    //document.getElementById("ifeqauto").src = "eqverif.aspx?eqnum=" + eq + "&sid=" + sid;
                    wo = document.getElementById("txteqauto").value;
                    document.getElementById("lblfiltret").value = "~~~~~~" + wo + "~~~~~~~~~~~~~~~~" + "na" + "~~~~~~~~";
                    document.getElementById("lblret").value = "filt"
                    document.getElementById("form1").submit();
                }
                else if (eq == "" && sid != "") {
                    newwo();
                }
            }
            else {
                if (eq != "" && sid != "") {
                    document.getElementById("ifeqauto").src = "eqverif.aspx?wpage=fs&eqnum=" + eq + "&sid=" + sid + "&wonum=" + wo + "&usr=" + usr;
                }
                else {
                    newwo();
                }
            }
        }
        function handle_eqauto(ret) {
            //var coi = document.getElementById("lbliscol").value;
            //ret = who + "~" + eqnum + "~" + eqid + "~" + eqdesc + "~" + fpc + "~" + loc + "~" + parid + "~" + dept + "~" + did + "~" + cell + "~" + clid + "~" + wo;
            //wo, eid, fid, cid, sid, did, clid, chk, lid, nid, stat, wt
            var retarr = ret.split("~");
            var who = retarr[0];
            //alert(who)
            if (who == "W") {
                var wo = retarr[1];

                document.getElementById("lblfiltret").value = "~~~~~~" + wo + "~~~~~~~~~~~~~~~~" + "na" + "~~~~~~~";
                document.getElementById("lblret").value = "filt"
                document.getElementById("form1").submit();
            }
            else if (who == "D" || who == "L") {
                var wo = retarr[11];
                var eid = retarr[2];
                var fid = "";
                var cid = "";
                var sid = document.getElementById("lblsid").value;
                var did = retarr[8];
                var clid = retarr[10];
                var chk = "";
                var lid = retarr[6];
                var nid = "";
                var stat = "";
                var wt = "";
                //gotoreq(wo, eid, fid, cid, sid, did, clid, chk, lid, nid, stat, wt);
                addwo(wo)
            }
            else if (who == "N") {
                alert("No Record Found")
            }
            else if (who == "M") {
                alert("Search Returned Multiple Records")
            }

        }
        function checkit1() {
            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                //window.parent.doref();
                doref();
            }
            var chk = document.getElementById("lbladm1").value;
            if (chk == "no") {
                document.getElementById("trsa").className = "details";
                document.getElementById("lblrefresh").value = "no";
            }
            var ref = document.getElementById("lblrefresh").value;
            if (ref == "yes") {
                var rat = document.getElementById("ddref").value;
                //alert(rat)
                if (rat == "5") {
                    window.setTimeout("refref();", 300000);
                    var now = new Date();
                    now.setSeconds(now.getSeconds() + 300);
                    document.getElementById("tdnext").innerHTML = now;
                }
                else if (rat == "4") {
                    window.setTimeout("refref();", 240000);
                    var now = new Date();
                    now.setSeconds(now.getSeconds() + 240);
                    document.getElementById("tdnext").innerHTML = now;
                }
                else if (rat == "3") {
                    window.setTimeout("refref();", 180000);
                    var now = new Date();
                    now.setSeconds(now.getSeconds() + 180);
                    document.getElementById("tdnext").innerHTML = now;
                }
                else if (rat == "2") {
                    window.setTimeout("refref();", 120000);
                    var now = new Date();
                    now.setSeconds(now.getSeconds() + 120);
                    document.getElementById("tdnext").innerHTML = now;
                }
                else if (rat == "1") {
                    window.setTimeout("refref();", 60000);
                    var now = new Date();
                    now.setSeconds(now.getSeconds() + 60);
                    document.getElementById("tdnext").innerHTML = now;
                }
                else {
                    window.parent.setref();
                }
            }
            var retchk = document.getElementById("lblret").value;
            if (retchk == "dash") {
                document.getElementById("lblret").value = "";
                var w = document.getElementById("lblwonum").value;
                addwo(w);
            }
        }
        function refref() {
            //window.parent.setref();
            setref();
            document.getElementById("lblret").value = "sa"
            document.getElementById("form1").submit();
        }
        var timer = window.setTimeout("doref();", 1205000);
        function doref() {
            resetsess();
        }
        function setref() {
            window.clearTimeout(timer);
            timer = window.setTimeout("doref();", 1205000);
        }
        function handledt(dt, who) {
            closedt();
            document.getElementById(who).value = dt;
        }
        function getcal(fld) {
            //var dt = document.getElementById("lblorig").value;
            var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:300px; dialogWidth:300px; resizable=yes");
            if (eReturn) {
                //var fldret= "txt" + fld;	
                document.getElementById(fld).value = eReturn;
                //document.getElementById("lblschk").value = "1";
                if (fld == "txtto" || fld == "txtfrom") {
                    document.getElementById("txtstart").value = "";
                    document.getElementById("txtcomp").value = "";
                }
                else if (fld == "txtstart" || fld == "txtcomp") {
                    document.getElementById("txtto").value = "";
                    document.getElementById("txtfrom").value = "";
                }
            }
        }
        function closedt() {
            document.getElementById("dtdiv").className = "Details";
        }
        function gotoreq(wo, eid, fid, cid, sid, did, clid, chk, lid, nid, stat, wt) {
            var typ = "reg";
            if (did != "") {
                if (lid != "" && lid != "0") {
                    typ = "dloc"
                }
                else {
                    typ = "reg"
                }
            }
            else {
                if (lid != "" && lid != "0") {
                    typ = "loc"
                }
            }
            if (typ == "") {
                typ = "reg";
            }
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lblusid").value;
            var username = document.getElementById("lblnme").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            var hr;
            //alert(hr + "," + wt)
            var who = document.getElementById("lblwho").value;
            //alert(who)
            if (who == "mm") {
                hr = "sid=" + sid + "&wo=" + wo + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&eqid=" + eid + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;

                window.parent.gotoreq(hr);
            }
            else {
                //alert("wolist" + "," + hr)
                hr = "jump=yes&wo=" + wo + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eid + "&fuid=" + fid + "&coid=" + cid + "&lid=" + lid + "&typ=" + typ + "&nid=" + nid + "&stat=" + stat + "&wt=" + wt + "&usrname=" + username;
                window.parent.gotoreq(hr, wt, wo, sid);
            }
        }
        function fclose(td, img) {
            //alert(td + ", " + img)
            var str = document.getElementById(img).src
            var lst = str.lastIndexOf("/") + 1
            var loc = str.substr(lst)
            if (loc == 'minus.gif') {
                document.getElementById(img).src = '../images/appbuttons/bgbuttons/plus.gif';
                try {
                    document.getElementById(td).className = "details";
                }
                catch (err) {

                }
            }
            else {
                document.getElementById(img).src = '../images/appbuttons/bgbuttons/minus.gif';
                try {
                    document.getElementById(td).className = "view";
                }
                catch (err) {

                }
            }
        }
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }

        var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";

        function printwo(wo, typ, jp, pm, stat, rtid) {
            //alert(wo + "," + typ + "," + jp + "," + pm + "," + stat + "," + rtid)
            if (wo != "0") {
                if (typ == "PM") {
                    //getdocs();
                    getroute(rtid, pm);
                }
                if (jp != "" && typ != "MAXPM") {
                    typ = "JP"
                }

                if (stat == "COMP" || stat == "CLOSE") {
                    window.open("awoprint.aspx?&wonum=" + wo, "repWin", popwin);
                }
                else {
                    window.open("woprint.aspx?typ=" + typ + "&pm=" + pm + "&wo=" + wo + "&jp=" + jp, "repWin", popwin);
                }
            }
        }
        function getroute(rtid, pm) {
            var popwin = "directories=0,height=520,width=820,location=1,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
            //var rtid = document.getElementById("lblrtid").value;
            //alert(rtid)
            var typ = "RBASM3";
            //var pm = document.getElementById("lblpmid").value;
            if (rtid != "") {
                window.open("../reports/pmrhtml7_new.aspx?rid=" + rtid + "&typ=" + typ + "&pmid=" + pm, "repWin2", popwin);
            }
        }
        function resetsrch() {
            //document.getElementById("txtstart").value="";
            //document.getElementById("txtcomp").value="";
            //document.getElementById("txtsearch").value="";
            var url = window.location.href;
            var nohttp = url.split('//')[1];
            var hostPort = nohttp.split('/')[1]
            //alert (hostPort)
            window.location = "/" + hostPort + "/appswo/wolist1.aspx?jump=no";


        }
        function srchloc() {
            //alert("Coming Soon!")
            //var lock = document.getElementById("lbllock").value;
            //if(lock!="1") {
            var eReturn = window.showModalDialog("../locs/LocDialog.aspx?typ=eq", "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                var ret = eReturn.split(",");
                document.getElementById("lbllid").value = ret[5];
                document.getElementById("lbltyp").value = ret[1];
                document.getElementById("lblloc").innerHTML = ret[2];
                document.getElementById("lbllocstr").value = ret[2];
                var lid = ret[5];
                //document.getElementById("lblpchk").value = "loc"
                //document.getElementById("form1").submit();

            }
            //}
        }
        function getsuper(typ) {
            var skill = document.getElementById("lblskillid").value;
            var ro = document.getElementById("lblro").value;
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    var retarr = eReturn.split(",")
                    if (typ == "sup") {
                        document.getElementById("lblsup").value = retarr[0];
                        document.getElementById("txtsup").value = retarr[1];
                    }
                    else {
                        document.getElementById("lbllead").value = retarr[0];
                        document.getElementById("txtlead").value = retarr[1];
                    }
                }
            }
        }
        function getfilt() {
            var sid = document.getElementById("lblsid").value;
            var filt = document.getElementById("lblfiltret").value;
            var eReturn = window.showModalDialog("wosrchdialog.aspx?filt=" + filt + "&sid=" + sid, "", "dialogHeight:550px; dialogWidth:650px; resizable=yes");
            if (eReturn) {
                //alert(eReturn)
                if (eReturn != "") {
                    document.getElementById("lblfiltret").value = eReturn;
                    document.getElementById("lblret").value = "filt"
                    document.getElementById("form1").submit();
                }
            }
        }

        function showall() {
            document.getElementById("txtstart").value = "";
            document.getElementById("txtcomp").value = "";
            document.getElementById("txtsearch").value = "";
            document.getElementById("lblret").value = "sa"
            document.getElementById("form1").submit();
        }

        function getsrch() {
            var chk = document.getElementById("lblhide").value;
            var tab = document.getElementById("lbltab").value;
            if (chk == "no") {
                document.getElementById("lblhide").value = "yes";
                document.getElementById("trhide1").className = "details";
                document.getElementById("trhide2").className = "details";
                if (tab == "yes") {
                    document.getElementById("divywr").className = "wolistbig1";
                }
                else {
                    document.getElementById("divywr").className = "wolistbig";
                }
            }
            else {
                document.getElementById("lblhide").value = "no";
                //document.getElementById("trhide1").className="view";
                //document.getElementById("trhide2").className="view";
                var tab = document.getElementById("lbltab").value;
                if (tab == "yes") {
                    document.getElementById("divywr").className = "wolistsmall1";
                }
                else {
                    document.getElementById("divywr").className = "wolistsmall";
                }
            }
        }
        function startrf() {
            var rf = document.getElementById("cbrf");
            if (rf.checked == true) {
                document.getElementById("lblrefresh").value = "yes";
                document.getElementById("tdr1").className = "bluelabel";
                document.getElementById("tdr4").className = "bluelabel";
                document.getElementById("ddref").disabled = false;
            }
            else {
                document.getElementById("lblrefresh").value = "no";
                document.getElementById("tdr1").className = "graylabel";
                document.getElementById("tdr4").className = "graylabel";
                document.getElementById("tdnext").innerHTML = "N\A";
                document.getElementById("ddref").disabled = true;
            }
            document.getElementById("lblret").value = "sa"
            document.getElementById("form1").submit();
        }
        function showsuper() {
            var chk = document.getElementById("lblissuper").value;
            if (chk == "1") {
                document.getElementById("lblret").value = "sa"
                document.getElementById("form1").submit();
            }
        }
        //onload="getsrch();"
        function gettab(who) {
            var coi = document.getElementById("lblcoi").value;
            var ctab = document.getElementById("lbltab").value;
            document.getElementById("lbllasttab").value = ctab;
            document.getElementById("lbltab").value = who;
            document.getElementById("lblret").value = who;
            document.getElementById("form1").submit();
        }
        function refreshit() {
            document.getElementById("lblfiltret").value = "";
            document.getElementById("lblret").value = "filt"
            document.getElementById("form1").submit();
        }
        function getfwp1() {

            var won = ""; // document.getElementById("lblwo").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lblusid").value;
            var username = document.getElementById("lblnme").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            //alert("wopmlist3.aspx?who=wo&wo=" + won + "&sid=" + sid + "&uid=" + userid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper)
            window.location = "wopmlist3.aspx?who=wofs&wo=" + won + "&sid=" + sid + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        function getsdp1() {
            var won = ""; //document.getElementById("lblwo").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lblusid").value;
            var username = document.getElementById("lblnme").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            window.location = "wopmlist7.aspx?who=wofs&wo=" + won + "&sid=" + sid + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        function getwob1() {
            var won = ""; //document.getElementById("lblwo").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lblusid").value;
            var username = document.getElementById("lblnme").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            window.location = "wobacklog.aspx?who=wofs&wo=" + won + "&sid=" + sid + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        function addwo(wo) {
            //alert()
            //"woman3.aspx?jump=yes&wo=" + wo + "&sid=" + sid + "&usrname=" + usr;
            var sid = document.getElementById("lblsid").value;
            var usr = document.getElementById("lblnme").value;
            var uid = document.getElementById("lblusid").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            var fs = document.getElementById("lblfs").value;
            var eReturn;
            if (fs == "fs") {
                eReturn = window.showModalDialog("woman3dialog.aspx?fs=fs&who=ed&sid=" + sid + "&wo=" + wo + "&uid=" + uid + "&usrname=" + usr + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr, "", "dialogHeight:600px; dialogWidth:1260px; resizable=yes");
            }
            else {
                eReturn = window.showModalDialog("woman3dialog.aspx?fs=&who=ed&sid=" + sid + "&wo=" + wo + "&uid=" + uid + "&usrname=" + usr + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr, "", "dialogHeight:600px; dialogWidth:740px; resizable=yes");
            }
            
            if (eReturn) {
                document.getElementById("lblret").value = "filtret"
                document.getElementById("form1").submit();
            }
        }
        function newwo() {
            //"woman3.aspx?jump=yes&wo=" + wo + "&sid=" + sid + "&usrname=" + usr;
            var sid = document.getElementById("lblsid").value;
            var usr = document.getElementById("lblnme").value;
            var wo = "";
            var uid = document.getElementById("lblusid").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            var fs = document.getElementById("lblfs").value;
            var eReturn;
            if (fs == "fs") {
                eReturn = window.showModalDialog("woman3dialog.aspx?fs=fs&who=ad&sid=" + sid + "&wo=" + wo + "&uid=" + uid + "&usrname=" + usr + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr, "", "dialogHeight:600px; dialogWidth:1260px; resizable=yes");
            }
            else {
                eReturn = window.showModalDialog("woman3dialog.aspx?fs=&who=ad&sid=" + sid + "&wo=" + wo + "&uid=" + uid + "&usrname=" + usr + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr, "", "dialogHeight:600px; dialogWidth:700px; resizable=yes");
            }
            if (eReturn) {
                document.getElementById("lblret").value = "filtret"
                document.getElementById("form1").submit();
            }
        }
        function getsched() {
            window.parent.handlesched();
        }
        function getsort(who) {
            //alert(who)
            var wo = document.getElementById("lblwosort").value;
            var stat = document.getElementById("lblstatsort").value;
            var typ = document.getElementById("lbltypesort").value;
            var eq = document.getElementById("lbleqsort").value;
            var sch = document.getElementById("lblschedsort").value;
            if (who == "wo") {
                if (wo == "asc") {
                    clearsort();
                    document.getElementById("lblwosort").value = "desc";
                    document.getElementById("lblsort").value = "w.wonum desc";
                }
                else {
                    clearsort();
                    document.getElementById("lblwosort").value = "asc";
                    document.getElementById("lblsort").value = "w.wonum asc";
                }
            }
            else if (who == "stat") {
                if (stat == "asc") {
                    clearsort();
                    document.getElementById("lblstatsort").value = "desc";
                    document.getElementById("lblsort").value = "w.status desc";
                }
                else {
                    clearsort();
                    document.getElementById("lblstatsort").value = "asc";
                    document.getElementById("lblsort").value = "w.status asc";
                }
            }
            else if (who == "type") {
                if (typ == "asc") {
                    clearsort();
                    document.getElementById("lbltypesort").value = "desc";
                    document.getElementById("lblsort").value = "w.worktype desc";
                }
                else {
                    clearsort();
                    document.getElementById("lbltypesort").value = "asc";
                    document.getElementById("lblsort").value = "w.worktype asc";
                }
            }
            else if (who == "eq") {
                if (eq == "asc") {
                    clearsort();
                    document.getElementById("lbleqsort").value = "desc";
                    document.getElementById("lblsort").value = "w.eqnum desc";
                }
                else {
                    clearsort();
                    document.getElementById("lbleqsort").value = "asc";
                    document.getElementById("lblsort").value = "w.eqnum asc";
                }
            }
            else if (who == "sched") {
                if (sch == "asc") {
                    clearsort();
                    document.getElementById("lblschedsort").value = "desc";
                    document.getElementById("lblsort").value = "w.schedstart desc";
                }
                else {
                    clearsort();
                    document.getElementById("lblschedsort").value = "asc";
                    document.getElementById("lblsort").value = "w.schedstart asc";
                }
            }
            else if (who == "targ") {
                if (sch == "asc") {
                    clearsort();
                    document.getElementById("lblschedsort").value = "desc";
                    document.getElementById("lblsort").value = "w.targstartdate desc";
                }
                else {
                    clearsort();
                    document.getElementById("lblschedsort").value = "asc";
                    document.getElementById("lblsort").value = "w.targstartdate asc";
                }
            }
            else if (who == "act") {
                if (sch == "asc") {
                    clearsort();
                    document.getElementById("lblschedsort").value = "desc";
                    document.getElementById("lblsort").value = "w.actstart desc";
                }
                else {
                    clearsort();
                    document.getElementById("lblschedsort").value = "asc";
                    document.getElementById("lblsort").value = "w.actstart asc";
                }
            }
            else if (who == "loc") {
                if (sch == "asc") {
                    clearsort();
                    document.getElementById("lbllocsort").value = "desc";
                    document.getElementById("lblsort").value = "w.location desc";
                }
                else {
                    clearsort();
                    document.getElementById("lbllocsort").value = "asc";
                    document.getElementById("lblsort").value = "w.location asc";
                }
            }
            document.getElementById("lblret").value = "dosort"
            document.getElementById("form1").submit();
        }
        function clearsort() {
            document.getElementById("lblwosort").value = "desc";
            document.getElementById("lblstatsort").value = "desc";
            document.getElementById("lbltypesort").value = "desc";
            document.getElementById("lbleqsort").value = "desc";
            document.getElementById("lblschedsort").value = "desc";
            document.getElementById("lbllocsort").value = "desc";
        }
        function excel() {
            document.getElementById("lblret").value = "excel"
            document.getElementById("form1").submit();
        }
        function excela() {
            document.getElementById("lblret").value = "excela"
            document.getElementById("form1").submit();
        }
        function excelc() {
            document.getElementById("lblret").value = "excelc"
            document.getElementById("form1").submit();
        }
        function checknopm() {
            var cb = document.getElementById("cbnopm");
            if (cb.checked == true) {
                document.getElementById("lblnopm").value = "yes";
            }
            else {
                document.getElementById("lblnopm").value = "no";
            }
            document.getElementById("lblret").value = "nopm"
            document.getElementById("form1").submit();
        }
        //-->
    </script>
    <style type="text/css">
        .wolistlarge
        {
            width: 1060px;
            height: 510px;
            overflow: auto;
        }
        .listbox
        {
            border-bottom: 1px solid black;
            border-top: 1px solid black;
            border-left: 1px solid black;
            border-right: 1px solid black;
            background-color: White;
            overflow: auto;
            width: 150px;
            height: 300px;
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
            list-style-type: none;
            padding: 3px;
            margin: 0px;
        }
        .listitem
        {
            list-style-type: none;
            padding: 0px;
            margin: 0px;
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
        }
        .listhigh
        {
            cursor: pointer;
            background-color: #67BAF9;
            list-style-type: none;
            padding: 0px;
            margin: 0px;
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
        }
    </style>
</head>
<body onload="checkit1();">
    <uc1:mmenu1 ID="mmenu11" runat="server" />
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="eqauto.asmx" />
        </Services>
    </asp:ScriptManager>
    <div>
        <table style="position: absolute; top: 80px; left: 8px" width="1060" cellpadding="0"
            cellspacing="0">
            <tr>
                <td>
                    <table width="1060" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td class="thdrhovmini plainlabel" id="tdpm" onclick="gettab('wo');" width="95" runat="server">
                                <asp:Label ID="lang1548" runat="server">Work Orders</asp:Label>
                            </td>
                            <td class="thdrmini plainlabel" id="tdeq" onclick="gettab('pm');" width="100" runat="server">
                                <asp:Label ID="lang1549" runat="server">PM Work Orders</asp:Label>
                            </td>
                            <td class="thdrmini plainlabel" id="tdeqd" onclick="gettab('eqd');" width="120" runat="server">
                                <asp:Label ID="Label1" runat="server">Equipment Down</asp:Label>
                            </td>
                            <td width="145">
                                <img src="../images/appbuttons/minibuttons/magnifier.gif" onclick="getfilt();">
                                <img src="../images/appbuttons/minibuttons/refreshit.gif" onclick="refreshit();">
                                <img onmouseover="return overlib('Jump to PM Scheduling', ABOVE, LEFT)" onclick="getsched();"
                                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/sched.gif" id="jts"
                                    runat="server" class="details">
                                <img onmouseover="return overlib('Export Results to Excel (600 Max)', ABOVE, LEFT)"
                                    onclick="excel();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/excelexp.gif">
                                <img onmouseover="return overlib('Export Current Down (ALL) to Excel', ABOVE, LEFT)"
                                    onclick="excela();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/excelexp.gif">
                                <img onmouseover="return overlib('Export Current Down (Critical) to Excel', ABOVE, LEFT)"
                                    onclick="excelc();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/excelexp.gif" id="imgcdown" runat="server">
                            </td>
                            <td>
                                <asp:TextBox ID="txteqauto" runat="server" CssClass="plainlabel" Width="120"></asp:TextBox>
                            </td>
                            <td>
                                <img src="../images/appbuttons/minibuttons/addnew.gif" onclick="get_eqauto();" id="imgeqauto" runat="server">
                            </td>
                            <td width="10">
                                <input id="cbrf" onclick="startrf();" type="checkbox" name="cbrf" runat="server">
                            </td>
                            <td id="tdrf" class="bluelabel" width="90" align="left" runat="server">
                                <asp:Label ID="lblrf" runat="server">Start Refresh</asp:Label>
                            </td>
                            <td id="tdr1" class="graylabel" width="35" runat="server">
                                <asp:Label ID="lang1525" runat="server">Rate</asp:Label>
                            </td>
                            <td id="tdr2" width="90" runat="server">
                                <asp:DropDownList ID="ddref" runat="server">
                                    <asp:ListItem Value="5" Selected="True">5 minutes</asp:ListItem>
                                    <asp:ListItem Value="4">4 minutes</asp:ListItem>
                                    <asp:ListItem Value="3">3 minutes</asp:ListItem>
                                    <asp:ListItem Value="2">2 minutes</asp:ListItem>
                                    <asp:ListItem Value="1">1 minute</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td id="tdr4" class="graylabel" width="35" runat="server">
                                <asp:Label ID="lang1526" runat="server">Next</asp:Label>
                            </td>
                            <td id="tdnext" class="plainlabel" width="140" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td id="tdywr" runat="server">
                    <div id="divywr" class="wolistlarge" runat="server">
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                                    border-right: blue 1px solid" cellspacing="0" cellpadding="0" width="300">
                                    <tr>
                                        <td style="border-right: blue 1px solid" width="20">
                                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                                runat="server">
                                        </td>
                                        <td style="border-right: blue 1px solid" width="20">
                                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                                runat="server">
                                        </td>
                                        <td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
                                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                                        </td>
                                        <td style="border-right: blue 1px solid" width="20">
                                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                                runat="server">
                                        </td>
                                        <td width="20">
                                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                                runat="server">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="20">
                                &nbsp;
                            </td>
                            <td width="295" id="woonly" runat="server" align="right">
                                <a href="#" id="afwp" runat="server" class="A1">4 Week Planner</a><img src="../images/appbuttons/minibuttons/4PX.gif"
                                    width="2px" />
                                <a href="#" id="asdp" runat="server" class="A1">Weekly Planner</a><img src="../images/appbuttons/minibuttons/4PX.gif"
                                    width="2px" />
                                <a href="#" id="awob" runat="server" class="A1">Backlog</a>
                            </td>
                            <td width="20">
                                &nbsp;
                            </td>
                            <td id="tdnopm" runat="server" class="plainlabelblue">
                            <input type="checkbox" id="cbnopm" onclick="checknopm();" />Filter Out PM Record Types
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:DataGrid ID="dgout" runat="server" ShowFooter="True">
        </asp:DataGrid>
        <asp:DataGrid ID="dgouta" runat="server" ShowFooter="True">
        </asp:DataGrid>
        <asp:DataGrid ID="dgoutc" runat="server" ShowFooter="True">
        </asp:DataGrid>
        <iframe id="ifsession" class="details" src="" frameborder="no" runat="server" style="z-index: 0">
        </iframe>
        <iframe id="ifeqauto" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
        </iframe>
        <input id="txtpg" type="hidden" name="txtpg" runat="server" />
        <input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server" />
        <input id="lblret" type="hidden" name="lblret" runat="server" />
        <input id="txtsearch" type="hidden" runat="server" name="txtsearch" />
        <input id="lbleqid" type="hidden" runat="server" name="lbleqid" />
        <input id="lblfuid" type="hidden" runat="server" name="lblfuid" />
        <input id="lblcoid" type="hidden" runat="server" name="lblcoid" />
        <input id="lbltyp" type="hidden" runat="server" name="lbltyp" />
        <input id="lblncid" type="hidden" runat="server" name="lblncid" />
        <input id="lblsid" type="hidden" runat="server" name="lblsid" />
        <input id="lblcid" type="hidden" runat="server" name="lblcid" />
        <input id="lbldept" type="hidden" name="lbldept" runat="server" />
        <input id="lblchk" type="hidden" name="lblchk" runat="server" />
        <input id="lbllid" type="hidden" name="lbllid" runat="server" /><input id="lblclid"
            type="hidden" name="lblclid" runat="server" />
        <input id="lblskillid" type="hidden" name="lblskillid" runat="server" />
        <input id="lblsrch" type="hidden" runat="server" name="lblsrch" />
        <input id="lblpchk" type="hidden" runat="server" name="lblpchk" />
        <input id="lbllead" type="hidden" runat="server" name="lbllead" />
        <input id="lblsup" type="hidden" runat="server" name="lblsup" /><input id="lblro"
            type="hidden" runat="server" name="lblro" />
        <input id="lbllocstr" type="hidden" runat="server" name="lbllocstr" /><input id="lbllog"
            type="hidden" name="lbllog" runat="server" />
        <input id="lbltab" type="hidden" runat="server" name="lbltab" /><input id="lblhide"
            type="hidden" runat="server" name="lblhide" />
        <input type="hidden" id="lbladm1" runat="server" name="lbladm1" /><input type="hidden"
            id="lblrefresh" runat="server" name="lblrefresh" />
        <input type="hidden" id="lblissuper" runat="server" name="lblissuper" />
        <input type="hidden" id="lblfslang" runat="server" name="lblfslang" />
        <input type="hidden" id="lblislabor" runat="server" />
        <input type="hidden" id="lblfiltret" runat="server" />
        <input type="hidden" id="lblusid" runat="server" />
        <input type="hidden" id="lblnme" runat="server" />
        <input type="hidden" id="lblisplanner" runat="server" />
        <input type="hidden" id="lblhref" runat="server" />
        <input type="hidden" id="lblwho" runat="server" /><input type="hidden" id="lblLogged_In"
            runat="server" name="lblLogged_In" />
        <input type="hidden" id="lblms" runat="server" name="lblms" />
        <input type="hidden" id="lblappstr" runat="server" name="lblappstr" />
        <input type="hidden" id="lblissched" runat="server" />
        <input type="hidden" id="lblwosort" runat="server" />
        <input type="hidden" id="lblstatsort" runat="server" />
        <input type="hidden" id="lbltypesort" runat="server" />
        <input type="hidden" id="lbleqsort" runat="server" />
        <input type="hidden" id="lblschedsort" runat="server" />
        <input type="hidden" id="lblsort" runat="server" />
        <input type="hidden" id="lblissaw" runat="server" />
        <input type="hidden" id="lblfilt" runat="server" />
        <input type="hidden" id="lbllocsort" runat="server" />
        <input type="hidden" id="lblnopm" runat="server" />
        <input type="hidden" id="lbllasttab" runat="server" />
        <input type="hidden" id="lbldnum" runat="server" />
        <input type="hidden" id="lbloref" runat="server" />
        <input type="hidden" id="lblwonum" runat="server" />
        <input type="hidden" id="lblfs" runat="server" />
        <input type="hidden" id="lblcoi" runat="server" />
        
    </div>
    <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" TargetControlID="txteqauto"
        runat="server" ServicePath="eqauto.asmx" ServiceMethod="geteq" MinimumPrefixLength="1"
        CompletionInterval="1000" EnableCaching="true" CompletionSetCount="12" CompletionListCssClass="listbox"
        CompletionListItemCssClass="listitem" CompletionListHighlightedItemCssClass="listhigh">
    </cc1:AutoCompleteExtender>
    </form>
</body>
</html>
