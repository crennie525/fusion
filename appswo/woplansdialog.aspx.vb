Public Class woplansdialog
    Inherits System.Web.UI.Page
    Dim sid, wonum, typ, stat, ro, wt As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents iftd As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            wonum = Request.QueryString("wo").ToString
            stat = Request.QueryString("stat").ToString
            ro = Request.QueryString("ro").ToString
            wt = Request.QueryString("wt").ToString
            iftd.Attributes.Add("src", "woplans.aspx?jump=no&wo=" + wonum + "&stat=" + stat + "&ro=" + ro + "&wt=" + wt + "&date=" & Now)

        End If
    End Sub

End Class
