﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wrman2.aspx.vb" Inherits="lucy_r12.wrman2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <style type="text/css">
        .FreezePaneOn6
        {
            position: absolute;
            top: 52px;
            left: 0px;
            visibility: visible;
            display: block;
            width: 106%;
            height: 90%;
            background-color: #eeeded;
            z-index: 999;
            filter: alpha(opacity=35);
            -moz-opacity: 0.35;
            padding-top: 3%;
        }
    </style>
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="javascript" type="text/javascript">
    <!--
        function handlesav(typ) {
            var stat = document.getElementById("lblstat").value;
            if (stat == "HOLD") {
                if (typ == "save") {
                    var reqd = document.getElementById("lblreqd").value;
                    var did = document.getElementById("lbldid").value;
                    var lid = document.getElementById("lbllid").value;
                    var desc = document.getElementById("txtwodesc").value;
                    var tstart = document.getElementById("txttstart").value;
                    var tcomp = document.getElementById("txttcomp").value;

                    if (reqd != "ok") {

                        alert("Department or Location Information Required")
                    }
                    else if (did == "" && lid == "") {

                        alert("Department or Location Information Required")
                    }
                    else if (desc == "") {
                        alert("Work Description Required")
                    }
                    else if (tstart == "") {
                        alert("Requested Start Date Required")
                    }
                    else if (tcomp == "") {
                        alert("Requested Complete Date Required")
                    }
                    else {
                        document.getElementById("lblsubmit").value = typ;
                        document.getElementById("form1").submit();
                    }
                }
                else {
                    document.getElementById("lblsubmit").value = typ;
                    document.getElementById("form1").submit();
                }
            }
        }
        function checksave() {
            //alert()
            document.getElementById("lblchecksave").value = "yes";
            //document.getElementById("tdsave").innerHTML = "Please Save Changes Before Exiting";
        }

        function dosave() {
            var chk = document.getElementById("lblchecksave").value;
            if (chk == "yes") {
                var timer = window.setTimeout("impsave();", 5000);
            }
        }

        function impsave() {
            var wonum = document.getElementById("lblwonum").value;
            var wodesc = document.getElementById("txtwodesc").value;

            var eReturn = window.showModalDialog("wrsave.aspx?wo=" + wonum + "&wodesc=" + wodesc, "", "dialogHeight:30px; dialogWidth:120px; resizable=no");
            if (eReturn) {
                var err = eReturn;
                if (err == "ok") {
                    //document.getElementById("tdsave").innerHTML = "";
                    //document.getElementById("tdsave").innerHTML="Please Save Changes Before Exiting";
                }
                else {

                }
            }
        }
        function retminsrch(who) {
            var sid = document.getElementById("lblsid").value;
            //var wo = document.getElementById("lblwonum").value;
            //alert(wo)
            var typ = "wrret";
            var did = document.getElementById("lbldid").value;
            var dept = document.getElementById("lbldept").value;
            var clid = document.getElementById("lblclid").value;
            var cell = document.getElementById("lblcell").value;
            var eqid = document.getElementById("lbleqid").value;
            var eq = document.getElementById("lbleq").value;
            var fuid = document.getElementById("lblfuid").value;
            var fu = document.getElementById("lblfu").value;
            var coid = ""; // document.getElementById("lblcoid").value;
            var comp = ""; // document.getElementById("lblcomp").value;
            var lid = document.getElementById("lbllid").value;
            var loc = document.getElementById("lblloc").value;
            var wonum = document.getElementById("lblwonum").value;

            //if (cell == "" && who != "checkfu" && who != "checkeq") {
            who = "deptret";
            //}
            //alert("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wonum=" + wonum + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who)
            //alert("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who)
            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wonum=" + wonum + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("lbldept").value = ret[1];
                document.getElementById("tddept").innerHTML = ret[1];
                document.getElementById("lblclid").value = ret[2];
                document.getElementById("lblcell").value = ret[3];
                document.getElementById("tdcell").innerHTML = ret[3];
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("lbleq").value = ret[5];
                document.getElementById("tdeq").innerHTML = ret[5];
                document.getElementById("lblfuid").value = ret[6];
                document.getElementById("lblfu").value = ret[7];
                //document.getElementById("tdfu").innerHTML = ret[7];
                //document.getElementById("lblcoid").value = ret[8];
                //document.getElementById("lblcomp").value = ret[9];
                document.getElementById("lbllid").value = ret[12];
                document.getElementById("lblloc").value = ret[13];
                document.getElementById("trdepts").className = "view";
                //document.getElementById("lblrettyp").value = "depts";
                var did = ret[0];
                var eqid = ret[4];
                var clid = ret[2];
                var fuid = ret[6];
                var coid = ret[8];
                var lid = ret[12];
                var typ;
                //if(lid=="") {
                //typ = "reg";
                //}
                //else {
                //typ = "dloc";
                //}
                //typ = "reg"
                //var task = "";
                //window.location = "pmgetopt.aspx?jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;
            }
        }
        function getminsrch() {
            var did = document.getElementById("lbldid").value;
            //alert(did)
            if (did != "") {
                retminsrch();
            }
            else {
                var stat = document.getElementById("lblstat").value;
                var sid = document.getElementById("lblsid").value;
                var wo = document.getElementById("lblwonum").value;
                var typ;
                if (wo == "") {
                    typ = "lu";
                }
                else {
                    typ = "wr";
                }
                var wt = document.getElementById("lblwt").value;
                if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
                    var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
                    if (eReturn) {
                        var ret = eReturn.split("~");
                        document.getElementById("lbldid").value = ret[0];
                        document.getElementById("lbldept").value = ret[1];
                        document.getElementById("tddept").innerHTML = ret[1];
                        document.getElementById("lblclid").value = ret[2];
                        document.getElementById("lblcell").value = ret[3];
                        document.getElementById("tdcell").innerHTML = ret[3];
                        document.getElementById("lbleqid").value = ret[4];
                        document.getElementById("lbleq").value = ret[5];
                        document.getElementById("tdeq").innerHTML = ret[5];
                        document.getElementById("lblfuid").value = ret[6];
                        document.getElementById("lblfu").value = ret[7];
                        //document.getElementById("tdfu").innerHTML = ret[7];
                        document.getElementById("lblcomid").value = ret[8];
                        document.getElementById("lblcomp").value = ret[9];
                        //document.getElementById("tdco").innerHTML = ret[9];
                        document.getElementById("lblncid").value = ret[10];
                        document.getElementById("lblnc").value = ret[11];
                        //document.getElementById("tdmisc").innerHTML = ret[11];
                        document.getElementById("lbllid").value = ret[12];
                        document.getElementById("lblloc").value = ret[13];
                        //document.getElementById("tdloc").innerHTML = ret[13];
                        document.getElementById("trdepts").className = "view";
                        document.getElementById("lbltyp").value = "depts";
                        if (ret[0] != "") {
                            document.getElementById("lblreqd").value = "ok";
                        }
                        else {
                            document.getElementById("lblreqd").value = "no";
                        }
                    }
                }
            }
        }

        function getlocs1() {
            var wt = document.getElementById("lblwt").value;
            if (wt != "PM" && wt != "TPM") {
                var stat = document.getElementById("lblstat").value;
                var sid = document.getElementById("lblsid").value;
                var wo = document.getElementById("lblwonum").value;
                var lid = document.getElementById("lbllid").value;
                var typ = "wo"
                //alert(lid)
                if (lid != "") {
                    typ = "woret"
                }
                var eqid = document.getElementById("lbleqid").value;
                var fuid = document.getElementById("lblfuid").value;
                var coid = document.getElementById("lblcomid").value;
                if (stat != "COMP" && stat != "CAN") {
                    //alert("../locs/locget3dialog.aspx?typ=wo&sid=" + sid + "&wo=" + wo)
                    var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
                    if (eReturn) {
                        //alert(eReturn)
                        //ret = lidi + "~" + lid + "~" + loc + "~" + eq + "~" eqnum + "~" + fu + "~" + func + "~" + co + "~" + comp;
                        var ret = eReturn.split("~");
                        document.getElementById("lbllid").value = ret[0];
                        document.getElementById("lblloc").value = ret[1];
                        document.getElementById("tdloc3").innerHTML = ret[2];

                        document.getElementById("lbleq").value = ret[4];
                        document.getElementById("tdeq3").innerHTML = ret[4];
                        document.getElementById("lbleqid").value = ret[3];

                        document.getElementById("lblfuid").value = ret[5];
                        document.getElementById("lblfu").value = ret[6];
                        //document.getElementById("tdfu3").innerHTML = ret[6];

                        document.getElementById("lblcomid").value = ret[7];
                        document.getElementById("lblcomp").value = ret[8];
                        //document.getElementById("tdco3").innerHTML = ret[8];

                        document.getElementById("trlocs").className = "view";
                        document.getElementById("lbltyp").value = "locs";
                        //var lid = ret[5];
                        //document.getElementById("lblpchk").value = "loc"
                        //document.getElementById("form1").submit();
                        if (ret[0] != "") {
                            document.getElementById("lblreqd").value = "ok";
                        }
                        else {
                            document.getElementById("lblreqd").value = "no";
                        }

                    }
                }
            }
        }
        function clearall() {
            document.getElementById("lbldid").value = "";
            document.getElementById("lbldept").value = "";
            document.getElementById("tddept").innerHTML = "";
            document.getElementById("lblclid").value = "";
            document.getElementById("lblcell").value = "";
            document.getElementById("tdcell").innerHTML = "";
            document.getElementById("lbleqid").value = "";
            document.getElementById("lbleq").value = "";
            document.getElementById("tdeq").innerHTML = "";
            document.getElementById("lblfuid").value = "";
            document.getElementById("lblfu").value = "";
            //document.getElementById("tdfu").innerHTML = "";
            //document.getElementById("lblcoid").value = "";
            document.getElementById("lbllid").value = "";
            document.getElementById("lblloc").value = "";
            document.getElementById("tdloc3").innerHTML = "";
            document.getElementById("tdeq3").innerHTML = "";
            //document.getElementById("tdfu3").innerHTML = "";
            document.getElementById("trdepts").className = "details";
            document.getElementById("trlocs").className = "details";
            //document.getElementById("lblrettyp").value = "";

            //document.getElementById("lbllevel").value = "";
            document.getElementById("lblreqd").value = "no";
        }
        function getwo() {
            var ro = document.getElementById("lblro").value;
            if (ro != "1") {
                document.getElementById("lblsubmit").value = "new"
                document.getElementById("form1").submit();
            }
        }
        function getcal(fld) {
            var stat = document.getElementById("lblstat").value;
            var wt = document.getElementById("lblwt").value;
            var wo = document.getElementById("lblwonum").value;
            var fldret = "txt" + fld;
            var chk;
            var cdt = "";
            var sdt = "";
            if (fld == "tstart" || fld == "tcomp") {
                //chk = document.getElementById("tdtcomp").innerHTML;
                chk = document.getElementById("txttcomp").value;
                cdt = chk;
                //sdt = document.getElementById("tdtstart").innerHTML;
                sdt = document.getElementById("txttstart").value;
                dtyp = "t";
            }
            if (chk == "") {
                chk = "yes";
            }
            else {
                chk = "no";
            }

            if (stat != "COMP" && stat != "CAN" && wt != "PM" && wt != "TPM") {
                window.parent.setref();
                var eReturn = window.showModalDialog("../controls/caldialog2.aspx?typ=wo&who=" + fld + "&chk=" + chk + "&wo=" + wo + "&cdt=" + cdt + "&sdt=" + sdt + "&dtyp=" + dtyp, "", "dialogHeight:425px; dialogWidth:425px; resizable=yes");
                if (eReturn) {
                    var fldret = "txt" + fld;
                    document.getElementById(fldret).value = eReturn;

                    if (fld == "tstart") {
                        if (cdt != "") {
                            if (cdt < eReturn) {
                                document.getElementById("txttcomp").value = eReturn;
                            }
                        }
                    }
                    else if (fld == "tcomp") {
                        if (sdt == "") {
                            document.getElementById("txttstart").value = eReturn;
                        }
                    }
                    document.getElementById("lblsubmit").value = "ref";
                    document.getElementById("form1").submit();
                }
            }
        }
        function getcharge() {
            var wo = document.getElementById("lblwonum").value;
            if (wo != "") {
                var eReturn = window.showModalDialog("../admin/ChargeDialog.aspx?who=wo&wo=" + wo, "", "dialogHeight:350px; dialogWidth:350px; resizable=yes");
                if (eReturn) {
                    //document.getElementById("tdcharge1").innerHTML = eReturn;
                    document.getElementById("txtcharge").value = eReturn;
                }
            }
        }

        function getsk() {
            wo = document.getElementById("lblwonum").value;
            sid = document.getElementById("lblsid").value;
            var stat = document.getElementById("lblstat").value;
            var wt = document.getElementById("lblwt").value;
            if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
                var eReturn = window.showModalDialog("../labor/labskillsdialog.aspx?who=wo&sid=" + sid + "&wo=" + wo, "", "dialogHeight:500px; dialogWidth:360px; resizable=yes");
                if (eReturn) {
                    if (eReturn != "") {
                        var ret = eReturn.split("~")

                        //document.getElementById(who).value = "yes";
                        document.getElementById("tdskill").innerHTML = ret[1];
                        document.getElementById("lblskill").value = ret[1];
                        document.getElementById("lblskillid").value = ret[0];

                    }
                }
            }
        }
        function checkstat() {
            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                window.parent.doref();
            }
            else {
                //window.parent.setref();
            }
            var chk = document.getElementById("lblwonum").value;
            if (chk == "") {
                FreezeScreen('<br><br>No Work Request Selected');
            }
            var leav = document.getElementById("lblexit").value;
            if (leav == "yes") {
                window.parent.gettab("pm");
                //window.parent.handleexit();
            }
        }
        function FreezeScreen(msg) {
            var outerPane = document.getElementById('FreezePane');
            var innerPane = document.getElementById('InnerFreezePane');
            outerPane.className = 'FreezePaneOn6';
            innerPane.innerHTML = msg;
        }
        //-->
    </script>
</head>
<body onload="checkstat();">
    <form id="form1" runat="server">
    <div id="FreezePane" class="FreezePaneOff" align="center">
        <div id="InnerFreezePane" class="InnerFreezePane">
        </div>
    </div>
    <div>
        <table style="position: absolute; top: 0px" cellspacing="0" width="710">
            <tr>
                <td>
                    <table>
                        <td class="blue label">
                            <asp:Label ID="lang1589" runat="server">Work Order#</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtwo" runat="server" Width="104px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td>
                            <img onmouseover="return overlib('Create New Work Order With Auto Number', ABOVE, RIGHT)"
                                onmouseout="return nd()" onclick="getwo();" src="../images/appbuttons/minibuttons/addnew.gif">
                        </td>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" width="710">
                        <tr>
                            <td class="thdrsinglft" width="30">
                                <img border="0" src="../images/appbuttons/minibuttons/wohdr.gif">
                            </td>
                            <td class="thdrsingrt label" align="left" width="680">
                                <asp:Label ID="lang1591" runat="server">Location\Equipment Information</asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td id="tddepts" class="bluelabel" runat="server">
                                Use Departments
                            </td>
                            <td>
                                <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                            <td id="tdlocs1" class="bluelabel" runat="server">
                                Use Locations
                            </td>
                            <td>
                                <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trdepts" class="details" runat="server">
                <td>
                    <table>
                        <tr>
                            <td class="label" width="110">
                                Department
                            </td>
                            <td id="tddept" class="plainlabel" width="170" runat="server">
                            </td>
                            <td>
                                <img onclick="retminsrch('deptret');" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                            <td class="label">
                                Equipment
                            </td>
                            <td id="tdeq" class="plainlabel" runat="server">
                            </td>
                            <td>
                                <img onclick="retminsrch('checkeq');" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                            <td class="label">
                            </td>
                            <td style="height: 1px" class="label">
                            </td>
                            <td class="label">
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                Station\Cell
                            </td>
                            <td id="tdcell" class="plainlabel" runat="server">
                            </td>
                            <td>
                                <img onclick="retminsrch('checkcell');" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trlocs" class="details" runat="server">
                <td>
                    <table>
                        <tr>
                            <td class="label" width="110">
                                Location
                            </td>
                            <td id="tdloc3" class="plainlabel" width="170" runat="server">
                            </td>
                            <td class="label">
                                Equipment
                            </td>
                            <td id="tdeq3" class="plainlabel" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" width="710">
                        <tr>
                            <td class="thdrsinglft" width="30">
                                <img border="0" src="../images/appbuttons/minibuttons/wohdr.gif">
                            </td>
                            <td class="thdrsingrt label" width="680" align="left">
                                <asp:Label ID="Label1" runat="server">Work Order Details</asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="1" cellpadding="1" width="700">
                        <tr>
                            <td width="90">
                            </td>
                            <td width="160">
                            </td>
                            <td width="70">
                            </td>
                            <td width="20">
                            </td>
                            <td width="60">
                            </td>
                            <td width="50">
                            </td>
                            <td width="30">
                            </td>
                            <td width="70">
                            </td>
                            <td width="170">
                            </td>
                        </tr>
                        <tr height="22">
                            <td class="label" colspan="6">
                                <asp:Label ID="lang1594" runat="server">Work Description</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="9" style="height: 53px">
                                <asp:TextBox ID="txtwodesc" runat="server" Width="688px" TextMode="MultiLine" Height="50px"
                                    CssClass="plainlabel"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" height="22">
                                <asp:Label ID="lang1595" runat="server">Requestor</asp:Label>
                            </td>
                            <td id="tdreq" class="plainlabel" runat="server">
                            </td>
                            <td class="label" colspan="2">
                                <asp:Label ID="lang1596" runat="server">Request Date</asp:Label>
                            </td>
                            <td id="tdreqdate" class="plainlabel" colspan="3" runat="server">
                            </td>
                            <td class="label">
                                <asp:Label ID="lang1597" runat="server">Phone</asp:Label>
                            </td>
                            <td class="plainlabel" id="tdphone" runat="server">
                            </td>
                        </tr>
                        <tr>
                            <td class="label" height="22">
                                <asp:Label ID="lang1598" runat="server">Status</asp:Label>
                            </td>
                            <td id="tdstatus" class="plainlabel" runat="server">
                                <asp:Label ID="lang1599" runat="server">WAPPR</asp:Label>
                            </td>
                            <td class="label" colspan="2">
                                <asp:Label ID="lang1600" runat="server">Status Date</asp:Label>
                            </td>
                            <td id="tdstatdate" class="plainlabel" colspan="3" runat="server">
                            </td>
                            <td class="label">
                                <asp:Label ID="lang1601" runat="server">Work Type</asp:Label>
                            </td>
                            <td id="tdwt" class="plainlabel" runat="server">
                                TBD
                            </td>
                        </tr>
                        <tr height="22">
                            <td class="label">
                                <asp:Label ID="lang1602" runat="server">Charge#</asp:Label>
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtcharge" runat="server" Width="220px" CssClass="plainlabel"></asp:TextBox>
                            </td>
                            <td>
                                <img onclick="getcharge('wo');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                        </tr>
                        <tr>
                            <td class="thdrsingg plainlabel" colspan="4">
                                <asp:Label ID="lang1603" runat="server">Scheduling Details</asp:Label>
                            </td>
                            <td class="thdrsingg plainlabel" colspan="5">
                                <asp:Label ID="lang1604" runat="server">Responsibility</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="4">
                                <table cellspacing="1" cellpadding="1">
                                    <tr>
                                        <td class="label" colspan="3">
                                            <asp:Label ID="lang1605" runat="server">Requested Start Date</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txttstart" runat="server" Width="100px" CssClass="plainlabel"></asp:TextBox>
                                        </td>
                                        <td>
                                            <img onclick="getcal('tstart');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                                width="19" height="19">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label" colspan="3">
                                            <asp:Label ID="lang1606" runat="server">Requested Completion Date</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txttcomp" runat="server" Width="100px" CssClass="plainlabel"></asp:TextBox>
                                        </td>
                                        <td>
                                            <img onclick="getcal('tcomp');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                                width="19" height="19">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="60">
                                        </td>
                                        <td width="60">
                                        </td>
                                        <td width="20">
                                        </td>
                                        <td width="60">
                                        </td>
                                        <td width="20">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" colspan="5">
                                <table cellspacing="1" cellpadding="1" width="360">
                                    <tr height="22">
                                        <td class="label">
                                            <asp:Label ID="lang1607" runat="server">Skill Required</asp:Label>
                                        </td>
                                        <td id="tdskill" class="plainlabel" colspan="5" runat="server">
                                        </td>
                                        <td>
                                            <img id="Img3" onclick="getsk();" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                                runat="server">
                                        </td>
                                    </tr>
                                    <tr height="22">
                                        <td class="label">
                                            <asp:Label ID="lang1608" runat="server">Supervisor</asp:Label>
                                        </td>
                                        <td id="tdsuper" class="plainlabel" colspan="6" runat="server">
                                        </td>
                                    </tr>
                                    <tr height="22">
                                        <td class="label">
                                            <asp:Label ID="lang1609" runat="server">Phone</asp:Label>
                                        </td>
                                        <td id="tdsphone" class="plainlabel" colspan="6" runat="server">
                                        </td>
                                    </tr>
                                    <tr height="22">
                                        <td class="label">
                                            <asp:Label ID="lang1610" runat="server">Email</asp:Label>
                                        </td>
                                        <td id="tdemail" class="plainlabel" colspan="6" runat="server">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100">
                                        </td>
                                        <td width="60">
                                        </td>
                                        <td width="50">
                                        </td>
                                        <td width="60">
                                        </td>
                                        <td width="20">
                                        </td>
                                        <td width="20">
                                        </td>
                                        <td width="50">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-top: #7ba4e0 thin solid" colspan="10" align="right">
                                <input onmouseover="return overlib('Submit This Work Request and Exit')" onmouseout="return nd()"
                                    onclick="handlesav('save');" value="Submit" type="button">&nbsp;<input onmouseover="return overlib('Save Changes to This Work Request Without Submitting and Exit. This Work Order Will Not Be Available in the Work Manager.')"
                                        onmouseout="return nd()" onclick="handlesav('draft');" value="Save Draft" type="button">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblwt" type="hidden" name="lblwt" runat="server">
    <input id="lblstat" type="hidden" name="lblstat" runat="server">
    <input id="lbltyp" type="hidden" name="lbltyp" runat="server">
    <input id="lbllid" type="hidden" name="lbllid" runat="server">
    <input id="lblloc" type="hidden" name="lblloc" runat="server">
    <input id="lblpchk" type="hidden" name="lblpchk" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lblwonum" type="hidden" name="lblwonum" runat="server">
    <input id="lbldid" type="hidden" name="lbldid" runat="server">
    <input id="lbldept" type="hidden" name="lbldept" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lblcell" type="hidden" name="lblcell" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server">
    <input id="lblpar2" type="hidden" name="lblpar2" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lbleq" type="hidden" name="lbleq" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblfu" type="hidden" name="lblfu" runat="server"><input id="lblcomp" type="hidden"
        name="lblcomp" runat="server">
    <input id="lblcomid" type="hidden" name="lblcomid" runat="server"><input id="lbltabs"
        type="hidden" name="lbltabs" runat="server">
    <input id="lblwaid" type="hidden" name="Hidden1" runat="server">
    <input id="lblworkarea" type="hidden" name="Hidden2" runat="server">
    <input id="lblncid" type="hidden" name="lblncid" runat="server">
    <input id="lblnc" type="hidden" name="lblnc" runat="server">
    <input id="lblld" type="hidden" name="lblld" runat="server">
    <input id="lblisdown" type="hidden" name="lblisdown" runat="server">
    <input id="lbluser" type="hidden" name="lbluser" runat="server">
    <input id="lblskillid" type="hidden" name="lblskillid" runat="server">
    <input id="lblskill" type="hidden" name="lblskill" runat="server">
    <input id="lblro" type="hidden" name="lblro" runat="server">
    <input id="lbldis" type="hidden" name="lbldis" runat="server">
    <input id="lbljpref" type="hidden" name="lbljpref" runat="server">
    <input id="lbljpid" type="hidden" name="lbljpid" runat="server"><input id="lblsubmit"
        type="hidden" name="lblsubmit" runat="server">
    <input id="lblplnrid" type="hidden" name="lblplnrid" runat="server">
    <input id="lblplanner" type="hidden" name="lblplanner" runat="server">
    <input id="lbljump" type="hidden" name="lbljump" runat="server"><input id="lblsav"
        type="hidden" name="lblsav" runat="server">
    <input id="lblcnt" type="hidden" name="lblcnt" runat="server"><input id="lblclid"
        type="hidden" name="lblclid" runat="server">
    <input id="lblsdays" type="hidden" name="lblsdays" runat="server">
    <input id="lblse" type="hidden" name="lblse" runat="server">
    <input id="lblle" type="hidden" name="lblle" runat="server">
    <input id="lblpmid" type="hidden" name="lblpmid" runat="server">
    <input id="lbllead" type="hidden" name="lbllead" runat="server">
    <input id="lblsup" type="hidden" name="lblsup" runat="server">
    <input id="lblchecksave" type="hidden" name="lblchecksave" runat="server">
    <input id="lbllog" type="hidden" name="lbllog" runat="server">
    <input id="lblpmhid" type="hidden" name="lblpmhid" runat="server">
    <input id="lblmalert" type="hidden" name="lblmalert" runat="server">
    <input id="lblusesched" type="hidden" name="lblusesched" runat="server">
    <input id="lbltpmid" type="hidden" runat="server">
    <input id="lblttime" type="hidden" name="lblttime" runat="server">
    <input id="lblacttime" type="hidden" name="lblacttime" runat="server">
    <input id="lbldtime" type="hidden" name="Hidden1" runat="server">
    <input id="lblactdtime" type="hidden" name="lblactdtime" runat="server">
    <input id="lblusetdt" type="hidden" name="lblusetdt" runat="server">
    <input id="lblusetotal" type="hidden" name="lblusetotal" runat="server">
    <input id="lblfcust" type="hidden" name="lblfcust" runat="server">
    <input id="lblretday" type="hidden" name="lblretday" runat="server">
    <input id="lbldays" type="hidden" name="lbldays" runat="server">
    <input id="txtn" type="hidden" name="txtn" runat="server">
    <input id="lblworuns" type="hidden" runat="server">
    <input id="lblruns" type="hidden" runat="server">
    <input type="hidden" id="lblplan" runat="server">
    <input type="hidden" id="lblplanner2" runat="server">
    <input type="hidden" id="lbltstart" runat="server">
    <input type="hidden" id="lbltcomp" runat="server">
    <input type="hidden" id="lblreqd" runat="server" />
    <input type="hidden" id="lblexit" runat="server" />
    </form>
</body>
</html>
