

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class wofm2
    Inherits System.Web.UI.Page
	Protected WithEvents lang1396 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1395 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim comp As New Utilities
    Dim wonum, stat, ro, rostr, Login, uid, cadm As String
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcadm As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim ds As DataSet
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dltasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcurrcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrow As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfmcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmstrvals As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmflg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                dltasks.Columns(0).Visible = False
            Else
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = comp.CheckROS(rostr, "wo")
                    lblro.Value = ro
                    If ro = "1" Then
                        dltasks.Columns(0).Visible = False
                    End If
                End If
            End If
            uid = Request.QueryString("uid").ToString
            wonum = Request.QueryString("wo").ToString '"1025" '
            lblwo.Value = wonum
            stat = Request.QueryString("stat").ToString
            lblstat.Value = stat
            If (stat <> "COMP" And stat <> "CAN" And stat <> "WAPPR") Then
                dltasks.Columns(0).Visible = True
            Else
                dltasks.Columns(0).Visible = False
            End If
            comp.Open()
            sql = "select admin from pmsysusers where userid = '" & uid & "'"
            Try
                cadm = comp.strScalar(sql)
            Catch ex As Exception
                cadm = "0"
            End Try
            Try
                cadm = HttpContext.Current.Session("cadm").ToString()
            Catch ex1 As Exception
                cadm = "0"
            End Try
            lblcadm.Value = cadm
            GetFail(wonum)
            comp.Dispose()
        End If
    End Sub
    Private Sub GetFail(ByVal wonum As String)
        sql = "select distinct wofailid, failuremode, pass from wofailmodes1 where wonum = '" & wonum & "'"
        ds = comp.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        dltasks.DataSource = dv
        dltasks.DataBind()
        ro = lblro.value
        If ro = "1" Then
            dltasks.Columns(0).Visible = False
        End If
        'If dv.Count = 0 Then
        'tdnofail.InnerHtml = "No Job Plan Level Failure Modes Found"
        'End If
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If CatID <> "Select" And CatID <> "N/A" Then 'Not IsDBNull(CatID) OrElse  
            If CatID = "Y" Then
                iL = 0
            ElseIf CatID = "N" Then
                iL = 1
            ElseIf CatID = "YES" Then
                iL = 0
            ElseIf CatID = "NO" Then
                iL = 1
            End If
        Else
            iL = -1
        End If
        Return iL
    End Function
    Private Sub dltasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dltasks.EditCommand
        comp.Open()
        wonum = lblwo.Value
        dltasks.EditItemIndex = e.Item.ItemIndex
        GetFail(wonum)
        comp.Dispose()
    End Sub

    Private Sub dltasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dltasks.CancelCommand
        dltasks.EditItemIndex = -1
        wonum = lblwo.Value
        comp.Open()
        GetFail(wonum)
        comp.Dispose()
    End Sub

    Private Sub dltasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dltasks.UpdateCommand
        wonum = lblwo.Value
        Dim rb1, rb2, rb3, rb4, rb5, rbc As DropDownList
        Dim sel1, sel2, sel3, sel4, sel5, selc As String
        rb1 = CType(e.Item.FindControl("ddfm1"), DropDownList)
        sel1 = rb1.SelectedValue.ToString
        Dim fid As String = CType(e.Item.FindControl("lblwofailid"), Label).Text
        comp.Open()
        sql = "update wofailmodes1 set pass = '" & sel1 & "' where wofailid = '" & fid & "' and wonum = '" & wonum & "'"
        comp.Update(sql)
        dltasks.EditItemIndex = -1
        GetFail(wonum)
        comp.Dispose()
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dltasks.Columns(0).HeaderText = dlabs.GetDGPage("wofm2.aspx", "dltasks", "0")
        Catch ex As Exception
        End Try
        Try
            dltasks.Columns(1).HeaderText = dlabs.GetDGPage("wofm2.aspx", "dltasks", "1")
        Catch ex As Exception
        End Try
        Try
            dltasks.Columns(2).HeaderText = dlabs.GetDGPage("wofm2.aspx", "dltasks", "2")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1395.Text = axlabs.GetASPXPage("wofm2.aspx", "lang1395")
        Catch ex As Exception
        End Try
        Try
            lang1396.Text = axlabs.GetASPXPage("wofm2.aspx", "lang1396")
        Catch ex As Exception
        End Try

    End Sub

End Class
