<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wojppartdialog.aspx.vb"
    Inherits="lucy_r12.wojppartdialog" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Inventory Dialog</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/sessrefdialog.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/wojppartdialogaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="resetsess();pageScroll();">
    <form id="form1" method="post" runat="server">
    <iframe id="ifpart" runat="server" src="" width="100%" height="100%" frameborder="no">
    </iframe>
    <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
    <script type="text/javascript">
        document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script>
    <input type="hidden" id="lblsessrefresh" runat="server" name="lblsessrefresh">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
