<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wolistmini.aspx.vb" Inherits="lucy_r12.wolistmini" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wolistmini</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/wolistminiaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
     <!--
        function srchwo() {
            var wo = document.getElementById("txtsrchwo").value;
            if (wo != "") {
                document.getElementById("lblret").value = "srchwo";
                document.getElementById("form1").submit();
            }
        }
         //-->
    </script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table style="left: 0px; position: absolute; top: 0px">
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="label">
                            <asp:Label ID="lang1547" runat="server">Work Order#</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtsrchwo" runat="server" Width="120px" CssClass="plainlabel" ReadOnly="False"></asp:TextBox>
                        </td>
                        <td>
                            <img id="Img3" onclick="srchwo();" src="../images/appbuttons/minibuttons/srchsm.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td id="tdywr" runat="server">
            </td>
        </tr>
        <tr>
            <td align="center">
                <table style="border-right: blue 1px solid; border-top: blue 1px solid; border-left: blue 1px solid;
                    border-bottom: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="txtpg" type="hidden" name="txtpg" runat="server">
    <input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
    <input type="hidden" id="lblret" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
