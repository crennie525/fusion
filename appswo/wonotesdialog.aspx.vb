﻿Imports System.Data.SqlClient
Public Class wonotesdialog
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim wo As New Utilities
    Dim rootid, noteid, userid, user, wonum, wt, desc, ld, who As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        Dim lang As String = lblfslang.Value
        poplang(lang)

        If Not IsPostBack Then
            Try
                who = Request.QueryString("who").ToString
            Catch ex As Exception

            End Try
            rootid = Request.QueryString("rootid").ToString
            noteid = Request.QueryString("noteid").ToString
            userid = Request.QueryString("userid").ToString
            user = Request.QueryString("user").ToString
            wonum = Request.QueryString("wonum").ToString
            lblwho.Value = who
            lblrootid.Value = rootid
            lblnoteid.Value = noteid
            lblcuser.Value = user
            lblcuserid.Value = userid
            lblwo.Value = wonum
            wo.Open()
            getwo()
            wo.Dispose()
            If who = "fs" Then
                tr1.Attributes.Add("class", "details")
                tr2.Attributes.Add("class", "details")
                tr3.Attributes.Add("class", "details")
                tr4.Attributes.Add("class", "details")
                tbl1.Attributes.Add("style", "width: 560px; position: absolute; top: 0px;")
                tbl2.Attributes.Add("width", "560")
                tdwo.Attributes.Add("width", "420")
                tdmsg.Attributes.Add("width", "300")
                ifwo.Attributes.Add("style", "background-color: transparent; width: 560px; height: 220px")
                wodiv.Attributes.Add("style", "border-bottom: 1px groove; border-left: 1px groove; width: 560px; height: 220px; border-top: 1px groove; border-right: 1px groove;")
                ifwo.Attributes.Add("src", "wonotesfs.aspx?wonum=" + wonum + "&rootid=" + rootid + "&noteid=" + noteid + "&userid=" + userid + "&user=" + user)
            Else
                tbl1.Attributes.Add("width", "640")
                tbl2.Attributes.Add("width", "640")
                tdwo.Attributes.Add("width", "500")
                tdmsg.Attributes.Add("width", "380")
                ifwo.Attributes.Add("style", "background-color: transparent; width: 650px; height: 410px")
                wodiv.Attributes.Add("style", "border-bottom: 1px groove; border-left: 1px groove; width: 650px; height: 410px; border-top: 1px groove; border-right: 1px groove;")
                ifwo.Attributes.Add("src", "wonotes.aspx?wonum=" + wonum + "&rootid=" + rootid + "&noteid=" + noteid + "&userid=" + userid + "&user=" + user)
            End If


        End If
    End Sub
    Private Sub poplang(ByVal lang As String)
        If lang = "fre" Then
            tdwon.InnerHtml = "# de Bon de Travail"
            tdwtl.InnerHtml = "Type de travail"
            tddescl.InnerHtml = "Description"
            lang1548a.Text = "Notes de Bon de Travail"
            lang1549.Text = "Cause Fondamentale"
            tdret.InnerHtml = "Retour"

        End If
    End Sub
    Private Sub getwo()
        Dim lang As String = lblfslang.Value

        sql = "select w.worktype, w.description, l.longdesc from workorder w left join wolongdesc l on l.wonum = w.wonum where w.wonum = '" & wonum & "'"
        dr = wo.GetRdrData(sql)
        While dr.Read
            wt = dr.Item("worktype").ToString
            desc = dr.Item("description").ToString
            ld = dr.Item("longdesc").ToString

        End While
        dr.Close()
        tdwo.InnerHtml = wonum
        tdwt.InnerHtml = wt
        tddesc.InnerHtml = desc & ld
        Dim wtstr As String = ""
        sql = "select [default] from wovars where [column] = 'root'"
        Try
            wtstr = wo.strScalar(sql)
        Catch ex As Exception
            wtstr = ""
        End Try
        If wtstr <> "" Then
            Dim wtar() As String = wtstr.Split(",")
            Dim i As Integer
            Dim a As Integer = 0
            Dim wts As String
            For i = 0 To wtar.Length - 1
                wts = wtar(i)
                If wts = wt Then
                    a = 1
                    Exit For
                End If
            Next
            If a = 0 Then
                If lang = "fre" Then
                    tdmsg.InnerHtml = "Entrée de cause fondamentale non requise"
                Else
                    tdmsg.InnerHtml = "Root Cause Entry is Not Required"
                End If

                lblreq.Value = "no"
            Else
                lblreq.Value = "yes"
            End If
        Else
            If lang = "fre" Then
                tdmsg.InnerHtml = "Entrée de cause fondamentale non requise"
            Else
                tdmsg.InnerHtml = "Root Cause Entry is Not Required"
            End If

            lblreq.Value = "no"
        End If
    End Sub
End Class