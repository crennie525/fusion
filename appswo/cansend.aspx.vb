﻿Imports System.Data.SqlClient
Public Class cansend
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim sc As New Utilities
    Dim mu As New mmenu_utils_a
    Dim tmod As New transmod
    Dim PageNumber As String
    Dim PageSize As String = "50"
    Dim Filter, FilterCNT, Sort As String
    Dim sConfigPosition, sEquipment, sStructureType, sService, sResponsible, sDescription, sPlanningGroup, sPriority As String
    Dim sApprovedBy, sReasonCode, sStartDate, sFinishDate, sTxt1, sTxt2, sErrorCode1, sErrorCode2, sErrorCode3, sComplaintType As String
    Dim iRetroFlag, sItemNumber, sLotNumber, iPlannedQuantity, sFacility, sRequestedStartDte, sRequestedFinishDte, iSetupTime, iRuntime As String
    Dim iPlannedNumWkrs, sTextBlock, iOperation, sTransactionID, pmid, tpmid, wonum, sup, superid, wa, waid, lc, lcid As String
    '{sTransactionID, sConfigPosition, sEquipment, sStructureType, sService, sResponsible, iOperation, sDescription, 
    'sPlanningGroup, sPriority, sApprovedBy, sReasonCode, sStartDate, sFinishDate, 
    'sTxt1, sTxt2, sErrorCode1, sErrorCode2, sErrorCode3, sComplaintType, iRetroFlag, 
    'sItemNumber, sLotNumber, iPlannedQuantity, sFacility, sRequestedStartDte, sRequestedFinishDte, 
    'iSetupTime, iRuntime, iPlannedNumWkrs, sTextBlock}
    Dim MWNO, OPNO, RPRE, UMAT, PCTP, REND As String
    Dim RPDT As String
    Dim UMAS, DOWT, DLY1, DLY2 As String
    Dim EMNO, FCLA, FCL2, FCL3 As String
    Dim retval As String
    Dim isact As String
    Dim CONO As String = "1"
    Dim errcnt As Integer = 0
    Dim labflag As Integer = 0
    '***
    Dim s1flag, s2flag As String

    Dim s3flag, s4flag As String

    Dim woecd, wnums As String
    '***
    'we need to fill the woout table with the wonum and return value if it's ok
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Dim coi As String = mu.COMPI
            If coi = "NISS" Then
                Try
                    isact = System.Configuration.ConfigurationManager.AppSettings("servicemode")
                    lblisact.Value = isact
                Catch ex As Exception
                    isact = "test"
                    lblisact.Value = isact
                End Try

            End If
            lblwoecd.Value = "0"
            lblwnums.Value = "1"
            sc.Open()
            'SendCant("1086")
            getwr("1")
            getdummy()
            sc.Dispose()
            Dim serv1, serv2, smode As String
            Try
                serv1 = System.Configuration.ConfigurationManager.AppSettings("service1")
                lblserv1.Value = serv1
                tdservice.InnerHtml = serv1
            Catch ex As Exception
                serv1 = "http://ncsmiwta/Services/CMMS/OpenWorkOrder/Service.asmx"
                lblserv1.Value = serv1
                tdservice.InnerHtml = serv1
                tdservice.Attributes.Add("class", "plainlabelred")
            End Try
            Try
                serv2 = System.Configuration.ConfigurationManager.AppSettings("service2")
                lblserv2.Value = serv2
                tdservice2.InnerHtml = serv2
            Catch ex As Exception
                serv2 = "http://ncsmiwta/Services/CMMS/TimeEntry/TimeEntry/Service.asmx"
                lblserv2.Value = serv2
                tdservice2.InnerHtml = serv2
                tdservice2.Attributes.Add("class", "plainlabelred")
            End Try
            Try
                smode = System.Configuration.ConfigurationManager.AppSettings("servicemode")
                lblsmode.Value = smode
                tdmode.InnerHtml = smode
            Catch ex As Exception
                smode = "test"
                lblsmode.Value = smode
                tdmode.InnerHtml = smode
                tdmode.Attributes.Add("class", "plainlabelred")
            End Try

        Else
            If Request.Form("lblret") = "next" Then
                sc.Open()
                GetNext()
                sc.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                sc.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                getwr(PageNumber)
                sc.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                sc.Open()
                GetPrev()
                sc.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                sc.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                getwr(PageNumber)
                sc.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "getvals" Then
                wonum = lblwo.Value
                sc.Open()
                GetVals(wonum)
                sc.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "sendit" Then
                sc.Open()
                'Dim retwonum As String
                'sql = "select retwonum from workorder where wonum = '" & wonum & "'"
                'dr = sc.GetRdrData(sql)
                'While dr.Read
                'retwonum = dr.Item("retwonum").ToString
                'End While
                'dr.Close()

                'If retwonum = "" Then
                wonum = lblwo.Value
                SendCant(wonum)
                'End If
                sc.Dispose()
                lblret.Value = ""
                lblwoecd.Value = ""
                'lblwnums.Value = ""
            ElseIf Request.Form("lblret") = "excel" Then
                lblret.Value = ""
                sc.Open()
                BindExport()
                sc.Dispose()

            ElseIf Request.Form("lblret") = "excela" Then
                lblret.Value = ""
                sc.Open()
                BindExporta()
                sc.Dispose()

            ElseIf Request.Form("lblret") = "sendemp" Then
                'CONO, MWNO, OPNO, RPDT, RPRE, EMNO, UMAT, PCTP, REND, UMAS, FCLA, FCL2, FCL3, DOWT, DLY1, DLY2

            End If
        End If

    End Sub
    Private Sub BindExport()
        sql = "select top 50 * from trans_temp order by sTransactionID desc"
        Dim ds As New DataSet
        ds = sc.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
        lblret.Value = ""
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
    Private Sub BindExporta()
        sql = "select top 50 * from trans_temp_time order by sTransactionID desc"
        Dim ds As New DataSet
        ds = sc.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgouta.DataSource = dv
        dgouta.DataBind()
        lblret.Value = ""
        cmpDataGridToExcel.DataGridToExcelNHD(dgouta, Response)
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            getwr(PageNumber)
        Catch ex As Exception
            sc.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "AppSetAssetClass.aspx.vb")

            sc.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            getwr(PageNumber)
        Catch ex As Exception
            sc.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "AppSetAssetClass.aspx.vb")

            sc.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub getdummy()
        Dim sb As New StringBuilder
        Dim bg As String
        bg = "ptransrow plainlabelred"
        sb.Append("<table cellpadding=""2"" cellspacing=""2"" width=""400"">" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""120"" height=""20px"">Field</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""200"" >Value</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""80"" >Required?</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)

        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""" & bg & """ width=""120"" height=""20px"">configPosition</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & """ width=""200"" >" & sConfigPosition & "</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & """ width=""200"" align=""center"" >Y</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)

        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""120"" height=""20px"">Equipment</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" >" & sEquipment & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)

        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""120"" height=""20px"">Service</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" >" & sService & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)

        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""120"" height=""20px"">Responsible</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" >" & sResponsible & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)

        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">Description</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sDescription & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">planningGroup</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sPlanningGroup & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">Priority</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sPriority & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">startDate</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sStartDate & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">finishDate</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sFinishDate & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">errorCode1</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sErrorCode1 & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">errorCode2</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sErrorCode2 & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">errorCode3</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sErrorCode3 & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""120"" height=""20px"">retroFlag</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" >" & iRetroFlag & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)

        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">itemNumber</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sItemNumber & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">lotNumber</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sLotNumber & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""120"" height=""20px"">Facility</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" >" & sFacility & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)

        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">requestedStartDte</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sRequestedStartDte & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">requestedFinishDte</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sRequestedFinishDte & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">setupTime</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & iSetupTime & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">Runtime</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & iRuntime & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">plannedNumWkrs</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & iPlannedNumWkrs & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">textBlock</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sTextBlock & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""120"" height=""20px"">Operation</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" >" & iOperation & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)

        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow label"" height=""20px"" colspan=""3"" align=""center"">Time Entry Values (EMNO and UMAT Only)</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        Dim username As String
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">User Name</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & username & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N/A</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)

        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""120"" height=""20px"">EMNO</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" >" & EMNO & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)

        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">UMAT</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & UMAT & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)

        sb.Append("</table>" & vbCrLf)

        divret.InnerHtml = sb.ToString
    End Sub
    Private Sub GetVals(ByVal wonum As String)
        Dim ldkey, ldesc, reportedby, reportedbyid As String
        Dim rwo As String
        sql = "select e.eqnum, w.targstartdate, w.targcompdate, cast(isnull(w.actlabhrs, 0) as decimal(10,2)) as actlabhrs, pnw = (select count(*) from woassign where wonum = '" & wonum & "'), " _
            + "x.POSEQIP, x.RESPONSIBLE, isnull(w.actstart, w.schedstart) as actstart, w.actfinish, w.description, w.worktype, w.wopriority, w.reportedby, wa.workarea, w.wopriority, w.superid, w.leadcraftid, w.retwonum, " _
            + "w.targstartdate, w.targcompdate, cast(isnull(w.estlabhrs, 0) as decimal(10,2)) as estlabhrs, cast(isnull(w.estjplabhrs, 0) as decimal(10,2)) as estjplabhrs, w.qty, t.xcol, t.xcol2, w.pmid, w.tpmid, l.longdesc " _
            + "from workorder w " _
            + "left join equipment e on e.eqid = w.eqid " _
            + "left join equipment_xtra x on x.eqid = e.eqid " _
            + "left join workareas wa on wa.waid = w.waid " _
            + "left join wotype t on t.wotype = w.worktype " _
            + "left join wolongdesc l on l.wonum = w.wonum " _
            + "where w.wonum = '" & wonum & "'"
        dr = sc.GetRdrData(sql)
        While dr.Read
            superid = dr.Item("superid").ToString
            lcid = dr.Item("leadcraftid").ToString
            sConfigPosition = dr.Item("POSEQIP").ToString
            sEquipment = dr.Item("eqnum").ToString
            'sService = dr.Item("worktype").ToString
            sService = dr.Item("xcol2").ToString
            'sService = "1"
            reportedby = dr.Item("reportedby").ToString
            'sResponsible = dr.Item("reportedby").ToString
            'sResponsible = 1
            sDescription = dr.Item("description").ToString
            sDescription = wonum & " - " & sDescription
            sDescription = Mid(sDescription, 1, 39)
            sPlanningGroup = dr.Item("workarea").ToString
            sPriority = dr.Item("wopriority").ToString
            sStartDate = dr.Item("actstart").ToString
            sFinishDate = dr.Item("actfinish").ToString
            'errorCode1 = dr.Item("worktype").ToString
            'errorCode2 = dr.Item("worktype").ToString
            'errorCode3 = dr.Item("worktype").ToString
            iRetroFlag = "1"
            sItemNumber = dr.Item("eqnum").ToString
            sLotNumber = "1"
            sFacility = "CAN"
            sRequestedStartDte = dr.Item("targstartdate").ToString
            sRequestedFinishDte = dr.Item("targcompdate").ToString
            iSetupTime = dr.Item("estjplabhrs").ToString
            iRuntime = dr.Item("estlabhrs").ToString
            iPlannedNumWkrs = dr.Item("qty").ToString
            sTextBlock = dr.Item("description").ToString
            ldesc = dr.Item("longdesc").ToString
            If ldesc <> "" Then
                sTextBlock = wonum & " - " & sTextBlock & ldesc
            Else
                sTextBlock = wonum & " - " & sTextBlock
            End If
            iOperation = dr.Item("xcol").ToString
            pmid = dr.Item("pmid").ToString
            tpmid = dr.Item("tpmid").ToString
            rwo = dr.Item("retwonum").ToString
            rwo = RTrim(rwo)
            rwo = LTrim(rwo)
        End While
        dr.Close()

        sql = "select userid from pmsysusers where username = '" & reportedby & "'"
        Try
            reportedbyid = sc.strScalar(sql)
        Catch ex As Exception
            reportedbyid = ""
        End Try

        Dim lcnt As Integer
        If rwo <> "" Then
            lblMWNO.Value = rwo
            tdrwo.InnerHtml = rwo

            sql = "select distinct count(*) from wolabtrans where wonum = '" & wonum & "' and retwonum is null"
            lcnt = sc.Scalar(sql)
            lbllcnt.Value = lcnt
        Else
            lblMWNO.Value = ""
            lbllcnt.Value = "0"
        End If

        Dim ecd1id, ecd2id, ecd3id As String
        If pmid <> "" Then
            
            sql = "select top 1 ecd1id, ecd2id, ecd3id from pmtaskfailmodesman1 where ecd1id in (select ecd1id from ecd1) " _
                + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = pmtaskfailmodesman1.ecd1id) " _
                + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = pmtaskfailmodesman1.ecd2id) " _
                + "and pmid = '" & pmid & "'"
            'dr = sc.GetRdrData(sql)
            'While dr.Read
            'ecd1id = dr.Item("ecd1id").ToString
            'ecd2id = dr.Item("ecd2id").ToString
            'ecd3id = dr.Item("ecd3id").ToString
            'End While
            'dr.Close()

        ElseIf tpmid <> "" Then
            sql = "select top 1 ecd1id, ecd2id, ecd3id from pmtaskfailmodestpmman where ecd1id in (select ecd1id from ecd1) " _
                + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = pmtaskfailmodestpmman.ecd1id) " _
                + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = pmtaskfailmodestpmman.ecd2id) " _
                + "and pmid = '" & pmid & "'"
            'dr = sc.GetRdrData(sql)
            'While dr.Read
            'ecd1id = dr.Item("ecd1id").ToString
            'ecd2id = dr.Item("ecd2id").ToString
            'ecd3id = dr.Item("ecd3id").ToString
            'End While
            'dr.Close()

        Else
            sql = "select top 1 ecd1id, ecd2id, ecd3id from wofailmodes1 where ecd1id in (select ecd1id from ecd1) " _
                + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = wofailmodes1.ecd1id) " _
                + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = wofailmodes1.ecd2id) " _
            + "and wonum = '" & wonum & "'"
            dr = sc.GetRdrData(sql)
            While dr.Read
                ecd1id = dr.Item("ecd1id").ToString
                ecd2id = dr.Item("ecd2id").ToString
                ecd3id = dr.Item("ecd3id").ToString
            End While
            dr.Close()

        End If
        If ecd1id <> "" Then
            sql = "select top 1 e1.ecd1, e2.ecd2, e3.ecd3 " _
                    + "from ecd1 e1 " _
                    + "left join ecd2 e2 on e2.ecd1id = e1.ecd1id " _
                    + "left join ecd3 e3 on e3.ecd2id = e2.ecd2id " _
                    + "where e1.ecd1id = '" & ecd1id & "' and e2.ecd2id = '" & ecd2id & "' and e3.ecd3id = '" & ecd3id & "'"
            dr = sc.GetRdrData(sql)
            While dr.Read
                sErrorCode1 = dr.Item("ecd1").ToString
                sErrorCode2 = dr.Item("ecd2").ToString
                sErrorCode3 = dr.Item("ecd3").ToString
            End While
            dr.Close()
        End If

        ''added for sResponsible and possible sPlanningGroup
        If sResponsible = "" Then
            If pmid = "" And tpmid = "" Then
                If reportedbyid <> "" Then
                    sql = "select top 1 empno, waid, workarea from pmsysusers where userid = '" & reportedbyid & "'"
                    dr = sc.GetRdrData(sql)
                    While dr.Read
                        sup = dr.Item("empno").ToString
                        waid = dr.Item("waid").ToString
                        wa = dr.Item("workarea").ToString
                    End While
                    dr.Close()
                    If sup <> "" Then
                        sResponsible = sup
                        If wa <> "" Then
                            If sPlanningGroup = "" Then
                                sPlanningGroup = wa
                            End If
                        Else
                            If waid <> "" And sPlanningGroup = "" Then
                                sql = "select workarea from workareas where waid = '" & waid & "'"
                                Try
                                    wa = sc.strScalar(sql)
                                Catch ex As Exception
                                    wa = ""
                                End Try
                            End If
                            If wa <> "" And sPlanningGroup = "" Then
                                sPlanningGroup = wa
                            End If
                        End If
                    End If
                End If
            Else
                If superid <> "" Then
                    sql = "select top 1 empno, waid, workarea from pmsysusers where userid = '" & superid & "'"
                    dr = sc.GetRdrData(sql)
                    While dr.Read
                        sup = dr.Item("empno").ToString
                        waid = dr.Item("waid").ToString
                        wa = dr.Item("workarea").ToString
                    End While
                    dr.Close()
                    If sup <> "" Then
                        sResponsible = sup
                        If wa <> "" Then
                            If sPlanningGroup = "" Then
                                sPlanningGroup = wa
                            End If
                        Else
                            If waid <> "" And sPlanningGroup = "" Then
                                sql = "select workarea from workareas where waid = '" & waid & "'"
                                Try
                                    wa = sc.strScalar(sql)
                                Catch ex As Exception
                                    wa = ""
                                End Try
                            End If
                            If wa <> "" And sPlanningGroup = "" Then
                                sPlanningGroup = wa
                            End If
                        End If
                    End If
                End If
            End If
        End If


        If sResponsible = "" Then

            Dim strMessage As String = "No Supervisor or Labor Entry Found to Use for sResponsible"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim cant As Integer = 0
        Dim bg As String
        Dim sb As New StringBuilder
        sb.Append("<table cellpadding=""2"" cellspacing=""2"" width=""400"">" & vbCrLf)

        'sb.Append("<tr>" & vbCrLf)
        'sb.Append("<td class=""ptransrow plainlabel"" width=""120"" height=""20px"">Selected Wo</td>" & vbCrLf)
        'sb.Append("<td class=""ptransrow plainlabel"" width=""200"" >" & wonum & "</td>" & vbCrLf)

        'sb.Append("</tr>" & vbCrLf)
        tdwo.InnerHtml = wonum
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""120"" height=""20px"">Field</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""200"" >Value</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""80"" >Required?</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        '.btmmenuy
        If sConfigPosition = "" Then
            cant = 1
            bg = "waitony plainlabelred"
        Else
            bg = "ptransrow plainlabelred"
        End If
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""" & bg & """ width=""120"" height=""20px"">configPosition</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & """ width=""200"" >" & sConfigPosition & "</td>" & vbCrLf)
        sb.Append("<td class=""" & bg & """ width=""200"" align=""center"" >Y</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)


        If sEquipment = "" Then
            cant = 1
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""120"" height=""20px"">Equipment</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""200"" >" & sEquipment & "</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
        Else
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""120"" height=""20px"">Equipment</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" >" & sEquipment & "</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
        End If

        If sService = "" Then
            cant = 1
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""120"" height=""20px"">Service</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""200"" >" & sService & "</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
        Else
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""120"" height=""20px"">Service</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" >" & sService & "</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
        End If

        If sResponsible = "" Then
            cant = 1
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""120"" height=""20px"">Responsible</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""200"" >" & sResponsible & "</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
        Else
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""120"" height=""20px"">Responsible</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" >" & sResponsible & "</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
        End If
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">Description</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sDescription & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">planningGroup</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sPlanningGroup & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">Priority</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sPriority & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">startDate</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sStartDate & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">finishDate</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sFinishDate & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">errorCode1</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sErrorCode1 & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">errorCode2</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sErrorCode2 & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">errorCode3</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sErrorCode3 & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)

        If iRetroFlag = "" Then
            cant = 1
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""120"" height=""20px"">retroFlag</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""200"" >" & iRetroFlag & "</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
        Else
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""120"" height=""20px"">retroFlag</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" >" & iRetroFlag & "</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
        End If
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">itemNumber</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sItemNumber & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">lotNumber</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sLotNumber & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)

        If sFacility = "" Then
            cant = 1
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""120"" height=""20px"">Facility</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""200"" >" & sFacility & "</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
        Else
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""120"" height=""20px"">Facility</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" >" & sFacility & "</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
        End If
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">requestedStartDte</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sRequestedStartDte & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">requestedFinishDte</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sRequestedFinishDte & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">setupTime</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & iSetupTime & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">Runtime</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & iRuntime & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">plannedNumWkrs</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & iPlannedNumWkrs & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">textBlock</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" >" & sTextBlock & "</td>" & vbCrLf)
        sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)

        If iOperation = "" Then
            cant = 1
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""120"" height=""20px"">Operation</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""200"" >" & iOperation & "</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
        Else
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""120"" height=""20px"">Operation</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" >" & iOperation & "</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
        End If



        Dim username, laborid As String
        sql = "select cast(sum(isnull((w.minutes_man / 60),0)) as decimal(10,2)) as minutes_man, w.laborid, u.username, u.empno from wolabtrans w left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "' group by w.laborid, u.username, u.empno"
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds = sc.GetDSData(sql)
        dt = New DataTable
        dt = ds.Tables(0)
        Dim dti As Integer = dt.Rows.Count
        If dti > 0 Then
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""ptransrow label"" height=""20px"" colspan=""3"" align=""center"">Time Entry Values (EMNO and UMAT Only)</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
        End If
        Dim row As DataRow
        For Each row In dt.Rows
            username = row("username").ToString
            EMNO = row("empno").ToString
            'EMNO = "1"
            laborid = row("laborid").ToString
            UMAT = row("minutes_man").ToString
            'Dim UMAT_n As Long
            'Try
            'UMAT_n = System.Convert.ToDecimal(UMAT)
            'UMAT_n = Math.Round(UMAT_n, 2)
            'UMAT = UMAT_n

            'Catch ex As Exception

            'End Try
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">User Name</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow"" width=""200"" >" & username & "</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N/A</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)

            If EMNO = "" Then
                cant = 1
                sb.Append("<tr>" & vbCrLf)
                sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""120"" height=""20px"">EMNO</td>" & vbCrLf)
                sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""200"" >" & EMNO & "</td>" & vbCrLf)
                sb.Append("<td class=""ptransrow plainlabelred bgy"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
                sb.Append("</tr>" & vbCrLf)
            Else
                sb.Append("<tr>" & vbCrLf)
                sb.Append("<td class=""ptransrow plainlabelred"" width=""120"" height=""20px"">EMNO</td>" & vbCrLf)
                sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" >" & EMNO & "</td>" & vbCrLf)
                sb.Append("<td class=""ptransrow plainlabelred"" width=""200"" align=""center"" >Y</td>" & vbCrLf)
                sb.Append("</tr>" & vbCrLf)
            End If
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""ptransrow"" width=""120"" height=""20px"">UMAT</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow"" width=""200"" >" & UMAT & "</td>" & vbCrLf)
            sb.Append("<td class=""ptransrow"" width=""200"" align=""center"" >N</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
        Next

        sb.Append("</table>" & vbCrLf)

        divret.InnerHtml = sb.ToString



        '***TEST SECTION***
        '****
        Dim nodate As Date = sc.CNOW
        If sEquipment = "" Then
            sEquipment = "0"
        End If
        '****
        '***
        If sDescription = "" Then
            sDescription = "NONE"
        End If
        '***
        '***
        If sPlanningGroup = "" Then
            sPlanningGroup = "0"
        End If
        '***
        '***
        If sPriority = "" Then
            sPriority = "0"
        End If
        '***
        '****ADDED FOR TEST****
        If sStartDate = "" Then
            If sFinishDate <> "" Then
                sStartDate = sFinishDate
            Else
                sStartDate = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                sFinishDate = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
            End If
        End If
        If sFinishDate = "" Then
            sFinishDate = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
        End If
        '****END ADDED FOR TEST****
        '***
        If sItemNumber = "" Then
            sItemNumber = "0"
        End If
        '***
        '****ADDED FOR TEST****
        If sRequestedStartDte = "" Then
            If sRequestedFinishDte <> "" Then
                sRequestedFinishDte = sRequestedFinishDte
            Else
                sRequestedStartDte = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                sRequestedFinishDte = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
            End If
        End If
        If sRequestedFinishDte = "" Then
            sRequestedFinishDte = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
        End If
        '****END ADDED FOR TEST****
        '***
        If sTextBlock = "" Then
            sTextBlock = "NONE"
        End If
        '***
        '***ADDED FOR TEST***
        If sErrorCode1 = "" Then
            sErrorCode1 = "0"
        End If
        If sErrorCode2 = "" Then
            sErrorCode2 = "0"
        End If
        If sErrorCode2 = "" Then
            sErrorCode2 = "0"
        End If

        If sComplaintType = "" Then
            sComplaintType = "0"
        End If
        '***END***
        lblsTransactionID.Value = sTransactionID
        lblsConfigPosition.Value = sConfigPosition
        lblsEquipment.Value = sEquipment
        lblsStructureType.Value = sStructureType
        lblsService.Value = sService
        lblsResponsible.Value = sResponsible
        lbliOperation.Value = iOperation
        lblsDescription.Value = sDescription
        lblsPlanningGroup.Value = sPlanningGroup
        lblsPriority.Value = sPriority
        lblsApprovedBy.Value = sApprovedBy
        lblsReasonCode.Value = sReasonCode
        lblsStartDate.Value = sStartDate
        lblsFinishDate.Value = sFinishDate
        lblsTxt1.Value = sTxt1
        lblsTxt2.Value = sTxt2
        lblsErrorCode1.Value = sErrorCode1
        lblsErrorCode2.Value = sErrorCode2
        lblsErrorCode3.Value = sErrorCode3
        lblsComplaintType.Value = sComplaintType
        lbliRetroFlag.Value = iRetroFlag
        lblsItemNumber.Value = sItemNumber
        lblsLotNumber.Value = sLotNumber
        lbliPlannedQuantity.Value = iPlannedQuantity
        lblsFacility.Value = sFacility
        lblsRequestedStartDte.Value = sRequestedStartDte
        lblsRequestedFinishDte.Value = sRequestedFinishDte
        lbliSetupTime.Value = iSetupTime
        lbliRuntime.Value = iRuntime
        lbliPlannedNumWkrs.Value = iPlannedNumWkrs
        lblsTextBlock.Value = sTextBlock
        '***END TEST SECTION***

        Dim sb1 As New StringBuilder
        sb1.Append("<table cellpadding=""2"" cellspacing=""2"" width=""400"">" & vbCrLf)
        If cant = 1 Then
            sb1.Append("Required Value(s) Missing (Highlighted in Yellow)" & vbCrLf)

        Else
            If rwo <> "" Then
                If lcnt <> "0" Then
                    sb1.Append(lcnt & " Time Entry Records to Submit<br>")
                    sb1.Append("<input type=""button"" onclick=""sendit();"" value=""Send Data"">" & vbCrLf)
                    sb1.Append("<input type=""button"" onclick=""senditnoecd();"" value=""Send Data with no ECD"">" & vbCrLf)
                    'sb1.Append("<input type=""button"" onclick=""senditwnums();"" value=""Send Data with Numbers"">" & vbCrLf)
                    'sb1.Append("<input type=""button"" onclick=""senditnoecdwnums();"" value=""Send Data with no ECD and with Numbers"">" & vbCrLf)

                Else
                    sb1.Append("No Records to Transmit")
                End If
            Else
                sb1.Append("<input type=""button"" onclick=""sendit();"" value=""Send Data"">" & vbCrLf)
                sb1.Append("<input type=""button"" onclick=""senditnoecd();"" value=""Send Data with no ECD"">" & vbCrLf)
                'sb1.Append("<input type=""button"" onclick=""senditwnums();"" value=""Send Data with Numbers"">" & vbCrLf)
                'sb1.Append("<input type=""button"" onclick=""senditnoecdwnums();"" value=""Send Data with no ECD and with Numbers"">" & vbCrLf)
                'sb1.Append("<input type=""button"" onclick=""senditnoecdnonums();"" value=""Send Data with no ECD and with No Numbers"">" & vbCrLf)
                'sb1.Append("<input type=""button"" onclick=""senditwecdwnums();"" value=""Send Data with ECD and with Numbers"">" & vbCrLf)
                'sb1.Append("<input type=""button"" onclick=""sendit2();"" value=""Send Data 1 (Client)"">" & vbCrLf)
                'senditnoecd()
                'senditwnums()
                'senditnoecdwnums()
                'senditnoecdnonums()
                'senditwecdwnums()
            End If

        End If
        sb1.Append("</table>" & vbCrLf)
        tdgo.InnerHtml = sb1.ToString
    End Sub
    Private Sub SendCant(ByVal wonum As String)
        Dim ldkey, ldesc, reportedby, reportedbyid As String
        Dim lcnt As String = lbllcnt.Value
        Dim rwo As String = lblMWNO.Value
        Dim nodate As Date = sc.CNOW

        Dim retwonum_chk As String
        Try
            If rwo = "" Then
                sql = "select retwonum from workorder where wonum = '" & wonum & "'"
                dr = sc.GetRdrData(sql)
                While dr.Read
                    retwonum_chk = dr.Item("retwonum").ToString
                End While
                dr.Close()
                If retwonum_chk <> "" Then
                    rwo = retwonum_chk
                End If
            End If
        Catch ex As Exception

        End Try



        '***
        s1flag = "0"
        s2flag = "0"
        s3flag = "0"
        s4flag = "0"
        woecd = lblwoecd.Value
        wnums = lblwnums.Value

        '***

        If rwo = "" Then
            sql = "select seed from pmseed where pmtable = 'service'; update pmseed set seed = seed + 1 where pmtable = 'service'"
            sTransactionID = sc.strScalar(sql)
            'SendCant_test(wonum)
            'sTransactionID = ""
            sConfigPosition = ""
            sEquipment = ""
            sStructureType = ""
            sService = ""
            sResponsible = ""
            iOperation = ""
            sDescription = ""
            sPlanningGroup = ""
            sPriority = ""
            sApprovedBy = ""
            sReasonCode = ""
            sStartDate = ""
            sFinishDate = ""
            sTxt1 = ""
            sTxt2 = ""
            sErrorCode1 = ""
            sErrorCode2 = ""
            sErrorCode3 = ""
            sComplaintType = ""
            iRetroFlag = ""
            sItemNumber = ""
            sLotNumber = ""
            iPlannedQuantity = ""
            sFacility = ""
            sRequestedStartDte = ""
            sRequestedFinishDte = ""
            iSetupTime = ""
            iRuntime = ""
            iPlannedNumWkrs = ""
            sTextBlock = ""

            Dim coid, wt As String

            'need xtra col in wotype for Operation value
            sql = "select e.eqnum, w.targstartdate, w.targcompdate, cast(isnull(w.actlabhrs, 0) as decimal(10,2)) as actlabhrs, pnw = (select count(*) from woassign where wonum = '" & wonum & "'), " _
            + "x.POSEQIP, x.RESPONSIBLE, isnull(w.actstart, w.schedstart) as actstart, w.actfinish, w.description, w.worktype, w.wopriority, w.reportedby, wa.workarea, w.wopriority, w.superid, w.leadcraftid, " _
            + "w.targstartdate, w.targcompdate, cast(isnull(w.estlabhrs, 0) as decimal(10,2)) as estlabhrs, cast(isnull(w.estjplabhrs, 0) as decimal(10,2)) as estjplabhrs, w.qty, t.xcol, t.xcol2, w.pmid, w.tpmid, l.longdesc, w.comid " _
            + "from workorder w " _
            + "left join equipment e on e.eqid = w.eqid " _
            + "left join equipment_xtra x on x.eqid = e.eqid " _
            + "left join workareas wa on wa.waid = w.waid " _
            + "left join wotype t on t.wotype = w.worktype " _
            + "left join wolongdesc l on l.wonum = w.wonum " _
            + "where w.wonum = '" & wonum & "'"
            dr = sc.GetRdrData(sql)
            While dr.Read
                coid = dr.Item("comid").ToString
                wt = dr.Item("worktype").ToString

                superid = dr.Item("superid").ToString
                lcid = dr.Item("leadcraftid").ToString
                sConfigPosition = dr.Item("POSEQIP").ToString
                sEquipment = dr.Item("eqnum").ToString
                '****
                'If sEquipment = "" Then
                'sEquipment = "0"
                'End If
                '****
                'sService = dr.Item("worktype").ToString
                sService = dr.Item("xcol2").ToString
                'sService = "1"
                reportedby = dr.Item("reportedby").ToString
                'sResponsible = "1"
                sDescription = dr.Item("description").ToString
                '***
                'If sDescription = "" Then
                'sDescription = "NONE"
                'End If
                '***
                sDescription = wonum & " - " & sDescription
                sDescription = Mid(sDescription, 1, 39)
                sPlanningGroup = dr.Item("workarea").ToString
                '***
                'If sPlanningGroup = "" Then
                'sPlanningGroup = "0"
                'End If
                '***
                sPriority = dr.Item("wopriority").ToString
                '***
                'If sPriority = "" Then
                'sPriority = "0"
                'End If
                '***
                sStartDate = dr.Item("actstart").ToString
                If sStartDate <> "" Then
                    Try
                        Dim sdate As Date = sStartDate
                        sStartDate = sdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                    Catch ex As Exception

                    End Try
                End If

                sFinishDate = dr.Item("actfinish").ToString
                RPDT = dr.Item("actfinish").ToString
                If sFinishDate <> "" Then
                    Try
                        Dim fdate As Date = sFinishDate
                        Dim rdate As Date = sFinishDate
                        sFinishDate = fdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                        RPDT = rdate.ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                    Catch ex As Exception

                    End Try
                End If

                '****ADDED FOR TEST****

                'If sStartDate = "" Then
                'If sFinishDate <> "" Then
                'sStartDate = sFinishDate
                's3flag = "1"
                'Else
                'sStartDate = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                'sFinishDate = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                's3flag = "1"
                's4flag = "1"
                'End If
                'End If
                'If sFinishDate = "" Then
                'sFinishDate = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                's4flag = "1"
                'End If
                '****END ADDED FOR TEST****

                'errorCode1 = dr.Item("worktype").ToString
                'errorCode2 = dr.Item("worktype").ToString
                'errorCode3 = dr.Item("worktype").ToString
                iRetroFlag = "1"
                sItemNumber = dr.Item("eqnum").ToString
                '***
                'If sItemNumber = "" Then
                'sItemNumber = "0"
                'End If
                '***
                sLotNumber = "1"
                sFacility = "CAN"
                sRequestedStartDte = dr.Item("targstartdate").ToString
                If sRequestedStartDte <> "" Then
                    Try
                        Dim rsfdate As Date = sRequestedStartDte
                        sRequestedStartDte = rsfdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                    Catch ex As Exception

                    End Try
                End If
                sRequestedFinishDte = dr.Item("targcompdate").ToString
                If sRequestedFinishDte <> "" Then
                    Try
                        Dim rfdate As Date = sRequestedFinishDte
                        sRequestedFinishDte = rfdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                    Catch ex As Exception

                    End Try
                End If
                '****ADDED FOR TEST****

                'If sRequestedStartDte = "" Then
                'If sRequestedFinishDte <> "" Then
                'sRequestedStartDte = sRequestedFinishDte
                's1flag = "1"
                'Else
                'sRequestedStartDte = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                'sRequestedFinishDte = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                's1flag = "1"
                's2flag = "1"
                'End If
                'End If
                'If sRequestedFinishDte = "" Then
                'sRequestedFinishDte = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                's2flag = "1"
                'End If
                '****END ADDED FOR TEST****
                iSetupTime = dr.Item("estjplabhrs").ToString
                UMAS = dr.Item("estjplabhrs").ToString
                '***
                If wnums = "1" Then
                    If UMAS = "" Then
                        UMAS = "0"
                    End If
                End If

                '***
                iRuntime = dr.Item("estlabhrs").ToString
                UMAT = dr.Item("estlabhrs").ToString
                '***
                If wnums = "1" Then
                    If UMAT = "" Then
                        UMAT = "0"
                    End If
                End If

                '***
                iPlannedNumWkrs = dr.Item("qty").ToString
                sTextBlock = dr.Item("description").ToString
                ldesc = dr.Item("longdesc").ToString
                If ldesc <> "" Then
                    sTextBlock = wonum & " - " & sTextBlock & ldesc
                Else
                    sTextBlock = wonum & " - " & sTextBlock
                End If
                '***
                'If sTextBlock = "" Then
                'sTextBlock = "NONE"
                'End If
                '***
                iOperation = dr.Item("xcol").ToString
                OPNO = dr.Item("xcol").ToString
                pmid = dr.Item("pmid").ToString
                tpmid = dr.Item("tpmid").ToString
            End While
            dr.Close()

            'Temp fix for Nissan until single fail mode ecd values catch up



            Dim wofailid, compfailid, ed1id, ed2id, ed3id, oid As String
            wofailid = ""
            compfailid = ""
            ed1id = ""
            Try
                If wt <> "PM" And wt <> "TPM" Then
                    sql = "select top 1 wofailid, compfailid from wofailmodes1 where wonum = '" & wonum & "' and ecd1id is null"
                    dr = sc.GetRdrData(sql)
                    While dr.Read
                        wofailid = dr.Item("wofailid").ToString
                        compfailid = dr.Item("compfailid").ToString
                    End While
                    dr.Close()
                    If wofailid <> "" Then
                        sql = "select ecd1id, ecd2id, ecd3id, oaid from componentfailmodes where compfailid = '" & compfailid & "'"
                        dr = sc.GetRdrData(sql)
                        While dr.Read
                            ed1id = dr.Item("ecd1id").ToString
                            ed2id = dr.Item("ecd2id").ToString
                            ed3id = dr.Item("ecd3id").ToString
                            oid = dr.Item("oaid").ToString
                        End While
                        dr.Close()
                        If ed1id <> "" Then
                            sql = "update wofailmodes1 set oaid = '" & oid & "', ecd1id = '" & ed1id & "', ecd2id = '" & ed2id & "', ecd3id = '" & ed3id & "' where wofailid = '" & wofailid & "'"
                            sc.Update(sql)
                        End If
                    End If
                End If
            Catch ex As Exception

            End Try

            'End temp fix for Nissan

            sql = "select userid from pmsysusers where username = '" & reportedby & "'"
            Try
                reportedbyid = sc.strScalar(sql)
            Catch ex As Exception
                reportedbyid = ""
            End Try

            If wnums = "1" Then
                iPlannedQuantity = "1"
                If iSetupTime = "" Then
                    iSetupTime = "0"
                End If
                If iRuntime = "" Then
                    iRuntime = "0"
                End If
                If iPlannedNumWkrs = "" Then
                    iPlannedNumWkrs = "0"
                End If
            End If

            Dim ecd1id, ecd2id, ecd3id As String
            If pmid <> "" Then
                sql = "select top 1 ecd1id, ecd2id, ecd3id from pmtaskfailmodesman1 where ecd1id in (select ecd1id from ecd1) " _
                    + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = pmtaskfailmodesman1.ecd1id) " _
                    + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = pmtaskfailmodesman1.ecd2id) " _
                    + "and pmid = '" & pmid & "'"
                'dr = sc.GetRdrData(sql)
                'While dr.Read
                'ecd1id = dr.Item("ecd1id").ToString
                'ecd2id = dr.Item("ecd2id").ToString
                'ecd3id = dr.Item("ecd3id").ToString

                'End While
                'dr.Close()

            ElseIf tpmid <> "" Then
                sql = "select top 1 ecd1id, ecd2id, ecd3id from pmtaskfailmodestpmman where ecd1id in (select ecd1id from ecd1) " _
                    + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = pmtaskfailmodestpmman.ecd1id) " _
                    + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = pmtaskfailmodestpmman.ecd2id) " _
                    + "and pmid = '" & pmid & "'"
                'dr = sc.GetRdrData(sql)
                'While dr.Read
                'ecd1id = dr.Item("ecd1id").ToString
                'ecd2id = dr.Item("ecd2id").ToString
                'ecd3id = dr.Item("ecd3id").ToString

                'End While
                'dr.Close()

            Else
                sql = "select top 1 ecd1id, ecd2id, ecd3id from wofailmodes1 where ecd1id in (select ecd1id from ecd1) " _
                    + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = wofailmodes1.ecd1id) " _
                    + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = wofailmodes1.ecd2id) " _
                    + "and wonum = '" & wonum & "'"
                dr = sc.GetRdrData(sql)
                While dr.Read
                    ecd1id = dr.Item("ecd1id").ToString
                    ecd2id = dr.Item("ecd2id").ToString
                    ecd3id = dr.Item("ecd3id").ToString

                End While
                dr.Close()

            End If

            sql = "select top 1 e1.ecd1, e2.ecd2, e3.ecd3 " _
                    + "from ecd1 e1 " _
                    + "left join ecd2 e2 on e2.ecd1id = e1.ecd1id " _
                    + "left join ecd3 e3 on e3.ecd2id = e2.ecd2id " _
                    + "where e1.ecd1id = '" & ecd1id & "' and e2.ecd2id = '" & ecd2id & "' and e3.ecd3id = '" & ecd3id & "'"
            dr = sc.GetRdrData(sql)
            While dr.Read
                sErrorCode1 = dr.Item("ecd1").ToString
                sErrorCode2 = dr.Item("ecd2").ToString
                sErrorCode3 = dr.Item("ecd3").ToString
                FCLA = dr.Item("ecd1").ToString
                FCL2 = dr.Item("ecd2").ToString
                FCL3 = dr.Item("ecd3").ToString
            End While
            dr.Close()

            '***ADDED FOR TEST***
            'If sErrorCode1 = "" Then
            'sErrorCode1 = "0"
            'End If
            'If sErrorCode2 = "" Then
            'sErrorCode2 = "0"
            'End If
            'If sErrorCode3 = "" Then
            'sErrorCode3 = "0"
            'End If

            'If sComplaintType = "" Then
            'sComplaintType = "0"
            'End If
            '***END***

            If woecd = "1" Then
                If sErrorCode1 <> "" Then
                    sErrorCode1 = ""
                End If
                If sErrorCode2 <> "" Then
                    sErrorCode2 = ""
                End If
                If sErrorCode3 <> "" Then
                    sErrorCode3 = ""
                End If
            End If


            'sReasonCode = "0"
            'sTxt1 = "0"
            'sTxt2 = "0"
            'sApprovedBy = sResponsible
            'sStructureType = "0"
            If sResponsible = "" Then
                If pmid = "" And tpmid = "" Then
                    If reportedbyid <> "" Then
                        sql = "select top 1 empno, waid, workarea from pmsysusers where userid = '" & reportedbyid & "'"
                        dr = sc.GetRdrData(sql)
                        While dr.Read
                            sup = dr.Item("empno").ToString
                            waid = dr.Item("waid").ToString
                            wa = dr.Item("workarea").ToString
                        End While
                        dr.Close()
                        If sup <> "" Then
                            sResponsible = sup
                            If wa <> "" Then
                                If sPlanningGroup = "" Then
                                    sPlanningGroup = wa
                                End If
                            Else
                                If waid <> "" And sPlanningGroup = "" Then
                                    sql = "select workarea from workareas where waid = '" & waid & "'"
                                    Try
                                        wa = sc.strScalar(sql)
                                    Catch ex As Exception
                                        wa = ""
                                    End Try
                                End If
                                If wa <> "" And sPlanningGroup = "" Then
                                    sPlanningGroup = wa
                                End If
                            End If
                        End If
                    End If
                Else
                    If superid <> "" Then
                        sql = "select top 1 empno, waid, workarea from pmsysusers where userid = '" & superid & "'"
                        dr = sc.GetRdrData(sql)
                        While dr.Read
                            sup = dr.Item("empno").ToString
                            waid = dr.Item("waid").ToString
                            wa = dr.Item("workarea").ToString
                        End While
                        dr.Close()
                        If sup <> "" Then
                            sResponsible = sup
                            If wa <> "" Then
                                If sPlanningGroup = "" Then
                                    sPlanningGroup = wa
                                End If
                            Else
                                If waid <> "" And sPlanningGroup = "" Then
                                    sql = "select workarea from workareas where waid = '" & waid & "'"
                                    Try
                                        wa = sc.strScalar(sql)
                                    Catch ex As Exception
                                        wa = ""
                                    End Try
                                End If
                                If wa <> "" And sPlanningGroup = "" Then
                                    sPlanningGroup = wa
                                End If
                            End If
                        End If
                    End If
                End If
            End If


            If sResponsible = "" Then

                Dim strMessage As String = "No Supervisor or Labor Entry Found to Use for sResponsible"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If

            sql = "insert into trans_temp (sTransactionID, sConfigPosition, sEquipment, sStructureType, sService, sResponsible, iOperation, sDescription, " _
                                        + "sPlanningGroup, sPriority, sApprovedBy, sReasonCode, sStartDate, sFinishDate, " _
                                        + "sTxt1, sTxt2, sErrorCode1, sErrorCode2, sErrorCode3, sComplaintType, iRetroFlag, " _
                                        + "sItemNumber, sLotNumber, iPlannedQuantity, sFacility, sRequestedStartDte, sRequestedFinishDte, " _
                                        + "iSetupTime, iRuntime, iPlannedNumWkrs, sTextBlock) " _
                    + "values('" & sTransactionID & "','" & sConfigPosition & "','" & sEquipment & "','" & sStructureType & "','" & sService & "','" & sResponsible & "','" _
                    + iOperation & "','" & sDescription & "','" _
                    + sPlanningGroup & "','" & sPriority & "','" & sApprovedBy & "','" & sReasonCode & "','" & sStartDate & "','" & sFinishDate & "','" _
                    + sTxt1 & "','" & sTxt2 & "','" & sErrorCode1 & "','" & sErrorCode2 & "','" & sErrorCode3 & "','" & sComplaintType & "','" & iRetroFlag & "','" _
                    + sItemNumber & "','" & sLotNumber & "','" & iPlannedQuantity & "','" & sFacility & "','" & sRequestedStartDte & "','" & sRequestedFinishDte & "','" _
                    + iSetupTime & "','" & iRuntime & "','" & iPlannedNumWkrs & "','" & sTextBlock & "')"
            sc.Update(sql)





            Dim cansend As New Service1
            Try
                cansend.Url = System.Configuration.ConfigurationManager.AppSettings("service1")

            Catch ex As Exception
                Dim strMessage As String = "URL Not Found - Check Web Config File"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            Dim retstring As String

            Try
                sql = "select retwonum from workorder where wonum = '" & wonum & "'"
                dr = sc.GetRdrData(sql)
                While dr.Read
                    retwonum_chk = dr.Item("retwonum").ToString
                End While
                dr.Close()
            Catch ex As Exception
                retwonum_chk = ""
            End Try
            

            If retwonum_chk = "" Then
                retstring = cansend.OpenWorkOrder(sTransactionID, sConfigPosition, sEquipment, sStructureType, sService, sResponsible, iOperation, sDescription, _
            sPlanningGroup, sPriority, sApprovedBy, sReasonCode, sStartDate, sFinishDate, _
            sTxt1, sTxt2, sErrorCode1, sErrorCode2, sErrorCode3, sComplaintType, iRetroFlag, _
            sItemNumber, sLotNumber, iPlannedQuantity, sFacility, sRequestedStartDte, sRequestedFinishDte, _
            iSetupTime, iRuntime, iPlannedNumWkrs, sTextBlock)
                tdgo.InnerHtml = retstring
                Dim retarr() As String = retstring.Split(",")
                Dim retwonum, retval, errmsg As String
                isact = lblisact.Value
                'TransactionID : 001  , Work Order Number : 2766814  , ReturnValue : 0  , Error Message 
                Try
                    Dim woarr() As String = retarr(1).Split(":")
                    retwonum = woarr(1)
                    Dim valarr() As String = retarr(2).Split(":")
                    retval = valarr(1)
                    Dim msgarr() As String = retarr(3).Split(":")
                    errmsg = msgarr(1)
                    MWNO = RTrim(retwonum)
                    MWNO = LTrim(MWNO)
                    retwonum = MWNO
                    If retval = 1 Then

                        sql = "insert into woout (fuswonum, sdate, retwonum, retval, errmsg) " _
                        + "values ('" & wonum & "',getdate(),'" & retwonum & "','" & retval & "','" & errmsg & "')"
                        sc.Update(sql)
                        sendtime(wonum, MWNO, OPNO, RPDT, UMAS, FCLA, FCL2, FCL3)
                        lcnt -= 1
                    Else
                        If isact = "run" Or isact = "active" Then
                            sql = "update workorder set retwonum = '" & retwonum & "' where wonum = '" & wonum & "'"
                            sc.Update(sql)

                            sql = "insert into lawson_track (sTransactionID, sConfigPosition, sEquipment, sStructureType, sService, sResponsible, iOperation, sDescription, " _
                                            + "sPlanningGroup, sPriority, sApprovedBy, sReasonCode, sStartDate, sFinishDate, " _
                                            + "sTxt1, sTxt2, sErrorCode1, sErrorCode2, sErrorCode3, sComplaintType, iRetroFlag, " _
                                            + "sItemNumber, sLotNumber, iPlannedQuantity, sFacility, sRequestedStartDte, sRequestedFinishDte, " _
                                            + "iSetupTime, iRuntime, iPlannedNumWkrs, sTextBlock, retwonum, fuswonum) " _
                        + "values('" & sTransactionID & "','" & sConfigPosition & "','" & sEquipment & "','" & sStructureType & "','" & sService & "','" & sResponsible & "','" _
                        + iOperation & "','" & sDescription & "','" _
                        + sPlanningGroup & "','" & sPriority & "','" & sApprovedBy & "','" & sReasonCode & "','" & sStartDate & "','" & sFinishDate & "','" _
                        + sTxt1 & "','" & sTxt2 & "','" & sErrorCode1 & "','" & sErrorCode2 & "','" & sErrorCode3 & "','" & sComplaintType & "','" & iRetroFlag & "','" _
                        + sItemNumber & "','" & sLotNumber & "','" & iPlannedQuantity & "','" & sFacility & "','" & sRequestedStartDte & "','" & sRequestedFinishDte & "','" _
                        + iSetupTime & "','" & iRuntime & "','" & iPlannedNumWkrs & "','" & sTextBlock & "','" & retwonum & "','" & wonum & "')"
                            sc.Update(sql)
                        End If
                        sendtime(wonum, MWNO, OPNO, RPDT, UMAS, FCLA, FCL2, FCL3)
                        lcnt -= 1
                    End If

                    lblMWNO.Value = retwonum
                Catch ex As Exception
                    'Try
                    'sql = "update workorder set retwonum = '" & retwonum & "' where wonum = '" & wonum & "'"
                    'sc.Update(sql)
                    'Catch ex0 As Exception

                    'End Try
                    Try
                        sql = "insert into woout (fuswonum, sdate, errovr) " _
                        + "values ('" & wonum & "',getdate(),'" & retstring & "')"
                        sc.Update(sql)
                    Catch ex1 As Exception

                    End Try
                    Dim strMessage As String = "Problem with TimeEntry Service - Please Stop and Contact LAI"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End Try

            End If
            
        Else
            If lcnt <> "0" Then
                labflag = 1
                MWNO = lblMWNO.Value
                sql = "select e.eqnum, w.targstartdate, w.targcompdate, w.actlabhrs, pnw = (select count(*) from woassign where wonum = '" & wonum & "'), " _
               + "x.POSEQIP, x.RESPONSIBLE, w.actstart, w.actfinish, w.description, w.worktype, w.wopriority, w.reportedby, wa.workarea, w.wopriority, " _
               + "w.targstartdate, w.targcompdate, w.estlabhrs, w.estjplabhrs, w.qty, t.xcol, w.pmid, w.tpmid " _
               + "from workorder w " _
               + "left join equipment e on e.eqid = w.eqid " _
               + "left join equipment_xtra x on x.eqid = e.eqid " _
               + "left join workareas wa on wa.waid = w.waid " _
               + "left join wotype t on t.wotype = w.worktype " _
               + "where w.wonum = '" & wonum & "'"
                dr = sc.GetRdrData(sql)
                While dr.Read
                    RPDT = dr.Item("actfinish").ToString
                    If sFinishDate <> "" Then
                        Try
                            Dim fdate As Date = sFinishDate
                            Dim rdate As Date = sFinishDate
                            sFinishDate = fdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                            RPDT = rdate.ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                        Catch ex As Exception

                        End Try
                    Else
                        Try
                            Dim rdate As Date = RPDT
                            RPDT = rdate.ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                        Catch ex As Exception

                        End Try
                    End If
                    'If RPDT = "" Then
                    'RPDT = sFinishDate
                    'End If
                    iSetupTime = dr.Item("estjplabhrs").ToString
                    UMAS = dr.Item("estjplabhrs").ToString
                    UMAT = dr.Item("estlabhrs").ToString
                    OPNO = dr.Item("xcol").ToString
                    tpmid = dr.Item("tpmid").ToString
                    pmid = dr.Item("pmid").ToString
                End While
                dr.Close()
                '***
                If wnums = "1" Then
                    If UMAS = "" Then
                        UMAS = "0"
                    End If
                    If UMAT = "" Then
                        UMAT = "0"
                    End If
                End If
                'If iSetupTime = "" Then
                'iSetupTime = "0"
                'End If

                '***
                Dim ecd1id, ecd2id, ecd3id As String
                If pmid <> "" Then
                    sql = "select top 1 ecd1id, ecd2id, ecd3id from pmtaskfailmodesman1 where ecd1id in (select ecd1id from ecd1) " _
                        + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = pmtaskfailmodesman1.ecd1id) " _
                        + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = pmtaskfailmodesman1.ecd2id) " _
                        + "and pmid = '" & pmid & "'"
                    'dr = sc.GetRdrData(sql)
                    'While dr.Read
                    'ecd1id = dr.Item("ecd1id").ToString
                    'ecd2id = dr.Item("ecd2id").ToString
                    'ecd3id = dr.Item("ecd3id").ToString

                    'End While
                    'dr.Close()

                ElseIf tpmid <> "" Then
                    sql = "select top 1 ecd1id, ecd2id, ecd3id from pmtaskfailmodestpmman where ecd1id in (select ecd1id from ecd1) " _
                        + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = pmtaskfailmodestpmman.ecd1id) " _
                        + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = pmtaskfailmodestpmman.ecd2id) " _
                        + "and pmid = '" & pmid & "'"
                    'dr = sc.GetRdrData(sql)
                    'While dr.Read
                    'ecd1id = dr.Item("ecd1id").ToString
                    'ecd2id = dr.Item("ecd2id").ToString
                    'ecd3id = dr.Item("ecd3id").ToString

                    'End While
                    'dr.Close()

                Else
                    'sql = "select top 1 ecd1id, ecd2id, ecd3id from wofailmodes1 where ecd1id in (select ecd1id from ecd1) " _
                    '+ "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = wofailmodes1.ecd1id) " _
                    '+ "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = wofailmodes1.ecd2id) " _
                    '+ "and pmid = '" & pmid & "'"
                    sql = "select top 1 ecd1id, ecd2id, ecd3id from wofailmodes1 where ecd1id in (select ecd1id from ecd1) " _
                    + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = wofailmodes1.ecd1id) " _
                    + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = wofailmodes1.ecd2id) " _
                    + "and wonum = '" & wonum & "'"
                    dr = sc.GetRdrData(sql)
                    While dr.Read
                        ecd1id = dr.Item("ecd1id").ToString
                        ecd2id = dr.Item("ecd2id").ToString
                        ecd3id = dr.Item("ecd3id").ToString

                    End While
                    dr.Close()

                End If
                If ecd1id <> "" Then
                    sql = "select top 1 e1.ecd1, e2.ecd2, e3.ecd3 " _
                    + "from ecd1 e1 " _
                    + "left join ecd2 e2 on e2.ecd1id = e1.ecd1id " _
                    + "left join ecd3 e3 on e3.ecd2id = e2.ecd2id " _
                    + "where e1.ecd1id = '" & ecd1id & "' and e2.ecd2id = '" & ecd2id & "' and e3.ecd3id = '" & ecd3id & "'"
                    dr = sc.GetRdrData(sql)
                    While dr.Read
                        sErrorCode1 = dr.Item("ecd1").ToString
                        sErrorCode2 = dr.Item("ecd2").ToString
                        sErrorCode3 = dr.Item("ecd3").ToString
                        FCLA = dr.Item("ecd1").ToString
                        FCL2 = dr.Item("ecd2").ToString
                        FCL3 = dr.Item("ecd3").ToString
                    End While
                    dr.Close()
                End If



                If woecd = "1" Then
                    If FCLA <> "" Then
                        FCLA = ""
                    End If
                    If FCL2 <> "" Then
                        FCL2 = ""
                    End If
                    If FCL3 <> "" Then
                        FCL3 = ""
                    End If
                End If
                sendtime(wonum, MWNO, OPNO, RPDT, UMAS, FCLA, FCL2, FCL3)
            End If
        End If


        PageNumber = txtpg.Value
        getwr(PageNumber)
        '[POSEQIP],[ITEMGRP],[STATUS], [SUBPROCESS], [RESPONSIBLE], [FIXEDASSET], [PRIORITY], [CRITICALITYCLS],[PLANNINGPOSITION], [EQID], [structureType]
        'configPosition [POSEQIP]
        'Equipment [eqnum] 
        'structureType ??? ***added to equipment_xtra  [structureType]
        'Service ***is this work type??? [worktype]
        'Responsible [RESPONSIBLE]
        'Description ***first 40 characters of work description??? [description] 
        'planningGroup ***[PLANNINGPOSITION] or planning area??? 
        'Priority [wopriority]
        'approvedBy ***not in Fusion - can we use a default??? 
        'reasonCode ??? 
        'startDate [actstart] 
        'finishDate [actfinish] 
        'txt1 ??? 
        'txt2 ??? 
        '***Nissan has many PM records with multiple error code values
        '***Will use top 1 for testing
        'errorCode1 [ecd1] 
        'errorCode2 [ecd2] 
        'errorCode3 [ecd3] 
        'complaintType ???
        'retroFlag ***always 1??? 
        'itemNumber ??? 
        'lotNumber ??? 
        'plannedQuantity ??? 
        'Facility ***always CAN 
        'requestedStartDte [targstartdate] 
        'requestedFinishDte [targcompdate]
        'setupTime ??? 
        'Runtime ***total time??? [actlabhrs]
        'plannedNumWkrs [pnw] ***calculated field 
        'textBlock ??? 
        'Operation ???
    End Sub
    Private Sub SendCant_test(ByVal wonum As String)

        Dim ldkey, ldesc As String
        Dim lcnt As String = lbllcnt.Value
        Dim rwo As String = lblMWNO.Value
        Dim nodate As Date = sc.CNOW
        sql = "select seed from pmseed where pmtable = 'service'; update pmseed set seed = seed + 1 where pmtable = 'service'"
        sTransactionID = sc.strScalar(sql)
        'need xtra col in wotype for Operation value
        sql = "select e.eqnum, w.targstartdate, w.targcompdate, w.actlabhrs, pnw = (select count(*) from woassign where wonum = '" & wonum & "'), " _
            + "x.POSEQIP, x.RESPONSIBLE, w.actstart, w.actfinish, w.description, w.worktype, w.wopriority, w.reportedby, wa.workarea, w.wopriority, " _
            + "w.targstartdate, w.targcompdate, w.estlabhrs, w.estjplabhrs, w.qty, t.xcol, w.pmid, w.tpmid " _
            + "from workorder w " _
            + "left join equipment e on e.eqid = w.eqid " _
            + "left join equipment_xtra x on x.eqid = e.eqid " _
            + "left join workareas wa on wa.waid = w.waid " _
            + "left join wotype t on t.wotype = w.worktype " _
            + "where w.wonum = '" & wonum & "'"
        dr = sc.GetRdrData(sql)
        While dr.Read
            sConfigPosition = dr.Item("POSEQIP").ToString
            sEquipment = dr.Item("eqnum").ToString

            sService = dr.Item("worktype").ToString
            'sService = "1"
            sResponsible = dr.Item("reportedby").ToString
            'sResponsible = "1"
            sDescription = dr.Item("description").ToString
            sDescription = wonum & " - " & sDescription
            sDescription = Mid(sDescription, 1, 39)
            sPlanningGroup = dr.Item("workarea").ToString

            sPriority = dr.Item("wopriority").ToString

            sStartDate = dr.Item("actstart").ToString
            If sStartDate <> "" Then
                Try
                    Dim sdate As Date = sStartDate
                    sStartDate = sdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                Catch ex As Exception

                End Try
            End If

            sFinishDate = dr.Item("actfinish").ToString
            RPDT = dr.Item("actfinish").ToString
            If sFinishDate <> "" Then
                Try
                    Dim fdate As Date = sFinishDate
                    Dim rdate As Date = sFinishDate
                    sFinishDate = fdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                    RPDT = rdate.ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                Catch ex As Exception

                End Try
            Else

            End If


            iRetroFlag = "1"
            sItemNumber = dr.Item("eqnum").ToString

            sLotNumber = "1"
            sFacility = "CAN"
            sRequestedStartDte = dr.Item("targstartdate").ToString
            If sRequestedStartDte <> "" Then
                Try
                    Dim rsfdate As Date = sRequestedStartDte
                    sRequestedStartDte = rsfdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                Catch ex As Exception

                End Try
            End If
            sRequestedFinishDte = dr.Item("targcompdate").ToString
            If sRequestedFinishDte <> "" Then
                Try
                    Dim rfdate As Date = sRequestedFinishDte
                    sRequestedFinishDte = rfdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                Catch ex As Exception

                End Try
            End If

            iSetupTime = dr.Item("estjplabhrs").ToString
            UMAS = dr.Item("estjplabhrs").ToString
            iRuntime = dr.Item("estlabhrs").ToString
            UMAT = dr.Item("estlabhrs").ToString
            iPlannedNumWkrs = dr.Item("qty").ToString
            sTextBlock = dr.Item("description").ToString

            iOperation = dr.Item("xcol").ToString
            OPNO = dr.Item("xcol").ToString
            pmid = dr.Item("pmid").ToString
            tpmid = dr.Item("tpmid").ToString
        End While
        dr.Close()
        iPlannedQuantity = "1"
        If iSetupTime = "" Then
            iSetupTime = "0"
        End If
        If iRuntime = "" Then
            iRuntime = "0"
        End If
        If iPlannedNumWkrs = "" Then
            iPlannedNumWkrs = "0"
        End If
        Dim ecd1id, ecd2id, ecd3id As String
        If pmid <> "" Then
            sql = "select top 1 ecd1id, ecd2id, ecd3id from pmtaskfailmodesman1 where ecd1id in (select ecd1id from ecd1) " _
                + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = pmtaskfailmodesman1.ecd1id) " _
                + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = pmtaskfailmodesman1.ecd2id) " _
                + "and pmid = '" & pmid & "'"
            dr = sc.GetRdrData(sql)
            While dr.Read
                ecd1id = dr.Item("ecd1id").ToString
                ecd2id = dr.Item("ecd2id").ToString
                ecd3id = dr.Item("ecd3id").ToString

            End While
            dr.Close()

        ElseIf tpmid <> "" Then
            sql = "select top 1 ecd1id, ecd2id, ecd3id from pmtaskfailmodestpmman where ecd1id in (select ecd1id from ecd1) " _
                + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = pmtaskfailmodestpmman.ecd1id) " _
                + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = pmtaskfailmodestpmman.ecd2id) " _
                + "and pmid = '" & pmid & "'"
            dr = sc.GetRdrData(sql)
            While dr.Read
                ecd1id = dr.Item("ecd1id").ToString
                ecd2id = dr.Item("ecd2id").ToString
                ecd3id = dr.Item("ecd3id").ToString

            End While
            dr.Close()

        Else
            sql = "select top 1 ecd1id, ecd2id, ecd3id from wofailmodes1 where ecd1id in (select ecd1id from ecd1) " _
                + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = wofailmodes1.ecd1id) " _
                + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = wofailmodes1.ecd2id) " _
                + "and pmid = '" & pmid & "'"
            dr = sc.GetRdrData(sql)
            While dr.Read
                ecd1id = dr.Item("ecd1id").ToString
                ecd2id = dr.Item("ecd2id").ToString
                ecd3id = dr.Item("ecd3id").ToString

            End While
            dr.Close()

        End If

        sql = "select top 1 e1.ecd1, e2.ecd2, e3.ecd3 " _
                + "from ecd1 e1 " _
                + "left join ecd2 e2 on e2.ecd1id = e1.ecd1id " _
                + "left join ecd3 e3 on e3.ecd2id = e2.ecd2id " _
                + "where e1.ecd1id = '" & ecd1id & "' and e2.ecd2id = '" & ecd2id & "' and e3.ecd3id = '" & ecd3id & "'"
        dr = sc.GetRdrData(sql)
        While dr.Read
            sErrorCode1 = dr.Item("ecd1").ToString
            sErrorCode2 = dr.Item("ecd2").ToString
            sErrorCode3 = dr.Item("ecd3").ToString
            FCLA = dr.Item("ecd1").ToString
            FCL2 = dr.Item("ecd2").ToString
            FCL3 = dr.Item("ecd3").ToString
        End While
        dr.Close()



        sql = "insert into trans_temp (sTransactionID, sConfigPosition, sEquipment, sStructureType, sService, sResponsible, iOperation, sDescription, " _
                                    + "sPlanningGroup, sPriority, sApprovedBy, sReasonCode, sStartDate, sFinishDate, " _
                                    + "sTxt1, sTxt2, sErrorCode1, sErrorCode2, sErrorCode3, sComplaintType, iRetroFlag, " _
                                    + "sItemNumber, sLotNumber, iPlannedQuantity, sFacility, sRequestedStartDte, sRequestedFinishDte, " _
                                    + "iSetupTime, iRuntime, iPlannedNumWkrs, sTextBlock) " _
                + "values('" & sTransactionID & "','" & sConfigPosition & "','" & sEquipment & "','" & sStructureType & "','" & sService & "','" & sResponsible & "','" _
                + iOperation & "','" & sDescription & "','" _
                + sPlanningGroup & "','" & sPriority & "','" & sApprovedBy & "','" & sReasonCode & "','" & sStartDate & "','" & sFinishDate & "','" _
                + sTxt1 & "','" & sTxt2 & "','" & sErrorCode1 & "','" & sErrorCode2 & "','" & sErrorCode3 & "','" & sComplaintType & "','" & iRetroFlag & "','" _
                + sItemNumber & "','" & sLotNumber & "','" & iPlannedQuantity & "','" & sFacility & "','" & sRequestedStartDte & "','" & sRequestedFinishDte & "','" _
                + iSetupTime & "','" & iRuntime & "','" & iPlannedNumWkrs & "','" & sTextBlock & "')"
        sc.Update(sql)

    End Sub
    Private Sub sendtime(ByVal wonum As String, ByVal MWNO As String, ByVal OPNO As String, ByVal RPDT As String, ByVal UMAS As String, _
                         ByVal FCLA As String, ByVal FCL2 As String, ByVal FCL3 As String)
        'need RPRE, EMNO, DOWT (responsible for reporting, emp no, down time 
        '? PCTP (reg or over), REND (manual completion flag)
        '? DLY1 = 0.00, DLY2 = 0.00 (should be numeric, no more info)
        'default PCTP as 3 for now
        'CONO set at top
        wnums = lblwnums.Value
        If labflag = 1 Then
            tdgo.InnerHtml = ""
        End If
        Dim DOWTP As String
        sql = "select cast(isnull((totaldown * 60), '0.00') as decimal(10,2)) as 'dowt', cast(isnull((totaldownp * 60), '0.00') as decimal(10,2)) as 'dowtp' from eqhist where wonum = '" & wonum & "'"
        'DOWT = sc.strScalar(sql)
        dr = sc.GetRdrData(sql)
        While dr.Read
            DOWT = dr.Item("dowt").ToString
            DOWTP = dr.Item("dowtp").ToString
        End While
        dr.Close()
        DLY1 = DOWTP

        If DOWT = "" Then
            DOWT = "0"
        End If
        If DOWTP = "" Then
            DOWTP = "0"
            DLY1 = "0"
        End If

        sql = "select cast(sum(isnull((w.minutes_man / 60),0)) as decimal(10,2)) as minutes_man, w.laborid, u.username, u.empno, u.uid, u.islabor from wolabtrans w " _
            + "left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "' and w.retwonum is null group by w.laborid, u.username, u.empno, u.uid, u.islabor "
        Dim empcnt As Integer = 0
        errcnt = 0
        lblerrcnt.Value = errcnt
        Dim laborid, username, popstring, uid, islabor As String
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds = sc.GetDSData(sql)
        dt = New DataTable
        dt = ds.Tables(0)
        Dim row As DataRow
        For Each row In dt.Rows
            empcnt += 1
            uid = row("uid").ToString
            islabor = row("islabor").ToString
            username = row("username").ToString
            EMNO = row("empno").ToString
            If islabor = "0" Then
                EMNO = uid
            End If
            'EMNO = "1"
            laborid = row("laborid").ToString
            UMAT = row("minutes_man").ToString
            If UMAT = "" Then
                UMAT = "0"
            End If
            'Dim UMAT_n As Long
            'Try

            'UMAT_n = System.Convert.ToDecimal(UMAT)
            'UMAT_n = Math.Round(UMAT_n, 2)
            'UMAT = UMAT_n

            'Catch ex As Exception
            '***set UMAT to 0 for test
            ' If wnums = "1" Then
            'UMAT = "0"
            'End If

            'End Try
            If EMNO = "" Then
                errcnt += 1
                lblerrcnt.Value = errcnt
                'need to collect data for pop-up here
                If popstring = "" Then
                    popstring = empcnt + "~" + username + "~" + laborid + "~" + wonum + "~" + DOWT + "~" + MWNO + "~" + OPNO + "~" + RPDT + "~" + UMAT + "~" + UMAS + "~" + FCLA + "~" + FCL2 + "~" + FCL3 + "~" _
                        + EMNO
                Else
                    popstring += "," + empcnt + "~" + username + "~" + laborid + "~" + wonum + "~" + DOWT + "~" + "~" + MWNO + "~" + OPNO + "~" + RPDT + "~" + UMAT + "~" + UMAS + "~" + FCLA + "~" + FCL2 + "~" + FCL3 + "~" _
                        + EMNO
                End If
            Else
                PCTP = "3"
                If wnums = "1" Then
                    '*** set REND to 0 for test
                    REND = "0"
                    'DLY1 = "0.00"
                    DLY2 = "0.00"
                    '***set RPRE to 0 for test
                    'RPRE = "0"
                    sql = "select top 1 u.empno from wolabtrans w " _
            + "left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "' and w.retwonum is null"
                    Try
                        RPRE = "" 'sc.strScalar(sql)
                    Catch ex As Exception
                        RPRE = ""
                        'Dim strMessage As String = "No Labor Entry Found to Use for sResponsible"
                        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        'Exit Sub
                    End Try
                    If RPRE = "" Then
                        'Dim strMessage As String = "No Labor Entry Found to Use for sResponsible"
                        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        'Exit Sub
                    End If
                End If


                'sWorkOrdNum,sOpNum,sWorkDate,sUserId,sEmpNumbr,sLabrHrs,sCostTyp,sCloseOperat,sLabrSetupTim,
                'sFailurCls, sErrCde2, sErrCde3, sDownTim, sDelyTim1, sDelyTim2
                timeentry(wonum, laborid, MWNO, OPNO, RPDT, RPRE, EMNO, UMAT, PCTP, REND, UMAS, FCLA, FCL2, FCL3, DOWT, DLY1, DLY2)

            End If
            'check data and send - use else in above or separate proc?
            'error checking on return?

        Next
        'errcnt = lblerrcnt.Value
        If errcnt <> 0 Then
            tdgo.InnerHtml += "<br />" & errcnt & " Time Entry Transfers Failed"

        End If
    End Sub

    Private Sub timeentry(ByVal wonum As String, ByVal laborid As String, _
        ByVal MWNO As String, _
        ByVal OPNO As String, _
        ByVal RPDT As String, _
        ByVal RPRE As String, _
        ByVal EMNO As String, _
        ByVal UMAT As String, _
        ByVal PCTP As String, _
        ByVal REND As String, _
        ByVal UMAS As String, _
        ByVal FCLA As String, _
        ByVal FCL2 As String, _
        ByVal FCL3 As String, _
        ByVal DOWT As String, _
        ByVal DLY1 As String, _
        ByVal DLY2 As String)
        '***TEST***
        REM 04/30/2012 per Nissan
        'FCLA = ""
        'FCL2 = ""
        'FCL3 = ""
        '*********
        wnums = lblwnums.Value
        If wnums = "1" Then
            REND = "1"
            'DLY1 = "0.00"
            DLY2 = "0.00"
        End If
        PCTP = "3"

        Dim errcnt As Integer
        Try
            errcnt = lblerrcnt.Value
        Catch ex As Exception
            errcnt = 0
            lblerrcnt.Value = "0"
        End Try


        Dim cansend As New Service2
        Try
            cansend.Url = System.Configuration.ConfigurationManager.AppSettings("service2")

        Catch ex As Exception
            Dim strMessage As String = "Time Entry URL Not Found - Check Web Config File"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub

        End Try
        Dim retstring As String
        Dim ds As New DataSet

        Dim te(0) As TimeEntryInputVar
        te(0).sOpNum = OPNO
        te(0).sWorkDate = RPDT
        te(0).sUserId = RPRE
        te(0).sEmpNumbr = EMNO
        te(0).sLabrHrs = UMAT
        te(0).sCostTyp = PCTP
        te(0).sCloseOperat = REND
        te(0).sLabrSetupTim = UMAS
        te(0).sFailurCls = FCLA
        te(0).sErrCde2 = FCL2
        te(0).sErrCde3 = FCL3
        te(0).sDownTim = DOWT
        te(0).sDelyTim1 = DLY1
        te(0).sDelyTim2 = DLY2

        If sTransactionID = "" Then
            sql = "select seed from pmseed where pmtable = 'service'; update pmseed set seed = seed + 1 where pmtable = 'service'"
            sTransactionID = sc.strScalar(sql)
        End If

        Dim sWorkOrdNum As String = MWNO
        sWorkOrdNum = RTrim(sWorkOrdNum)
        sWorkOrdNum = LTrim(sWorkOrdNum)
        'redundant check !!! won't work in test
        'Dim rwochk As String
        'sql = "select retwonum from workorder where wonum = '" & wonum & "'"
        'dr = sc.GetRdrData(sql)
        'While dr.Read
        'rwochk = dr.Item("retwonum").ToString
        'End While
        'dr.Close()
        'If sWorkOrdNum <> rwochk Then
        'sWorkOrdNum = rwochk
        'End If
        '***
        sql = "insert into trans_temp_time (sTransactionID, sWorkOrdNum,sOpNum,sWorkDate,sUserId,sEmpNumbr,sLabrHrs,sCostTyp,sCloseOperat,sLabrSetupTim,sFailurCls,sErrCde2,sErrCde3,sDownTim,sDelyTim1,sDelyTim2) " _
            + "values ('" & sTransactionID & "','" & sWorkOrdNum & "','" & OPNO & "','" & RPDT & "','" & RPRE & "','" & EMNO & "','" & UMAT & "','" & PCTP & "','" & REND & "','" & UMAS & "','" & FCLA & "','" & FCL2 & "','" & FCL3 & "','" & DOWT & "','" & DLY1 & "','" & DLY2 & "')"
        sc.Update(sql)

        ds = cansend.TimeEntry(sWorkOrdNum, te)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        Dim drM As DataRow
        Dim errval, errval1, errval2 As String
        For Each drM In dt.Select()
            errval = drM(0).ToString
            errval1 = drM(1).ToString
            Try
                errval2 = drM(2).ToString
            Catch ex As Exception
                errval2 = ""
            End Try

            If retstring = "" Then
                If errval2 <> "" Then
                    retstring = errval + " - " + errval1 + " - " + errval2
                Else
                    retstring = errval + " - " + errval1
                End If

            Else
                If errval2 <> "" Then
                    retstring += ", " + errval + " - " + errval1 + " - " + errval2
                Else
                    retstring += ", " + errval + " - " + errval1
                End If

            End If
        Next
        Dim rtst As String = "<br /><br />Time Entry Return - Lawson Work Order Number as sent: " & sWorkOrdNum & " - Time Entry Return Message: " & retstring
        tdgo.InnerHtml += "<br /><br />Time Entry Return - Lawson Work Order Number as sent: " & sWorkOrdNum & " - Time Entry Return Message: " & retstring & "<br /><br /><br /><br />"
        Dim retval, errmsg As String
        isact = lblisact.Value
        Try
            'Dim retarr() As String = retstring.Split(",")
            'Dim valarr() As String = retarr(0).Split(":")
            'retval = valarr(1)
            'Dim msgarr() As String = retarr(1).Split(":")
            'errmsg = msgarr(1)
            If errval <> "0" Then
                sql = "insert into woout_te (fuswonum, sdate, retwonum, errmsg) " _
                + "values ('" & wonum & "',getdate(),'" & sWorkOrdNum & "','" & retstring & "')"
                sc.Update(sql)
            Else
                If isact = "run" Or isact = "active" Then
                    'sql = "select sum(isnull(w.minutes_man,0)) as minutes_man, w.laborid, u.username, u.empno from wolabtrans w left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "'"
                    sql = "update wolabtrans set retwonum = '" & sWorkOrdNum & "' where wonum = '" & wonum & "' and laborid = '" & laborid & "'"
                    sc.Update(sql)
                End If

            End If
        Catch ex As Exception
            'Try
            'sql = "update wolabtrans set retwonum = '" & MWNO & "' where wonum = '" & wonum & "' and laborid = '" & laborid & "'"
            'sc.Update(sql)
            'Catch ex0 As Exception

            'End Try
            Try
                sql = "insert into woout_te (fuswonum, sdate, errovr) " _
                    + "values ('" & wonum & "',getdate(),'" & retstring & "')"
                sc.Update(sql)
            Catch ex1 As Exception

            End Try
            errcnt += 1
            lblerrcnt.Value = errcnt
            'Dim strMessage As String = "Problem with TimeEntry Return Value - Please Stop and Contact LAI"
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub getwr(ByVal PageNumber As String)
        Dim sid As String = ddsites.SelectedValue.ToString
        Dim sb As New StringBuilder
        sb.Append("<table cellpadding=""2"" cellspacing=""2"" width=""580"">" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""60"" height=""20px"">WO#</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""50"" >Status</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""60"" >Type</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""140""><Equipment</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""200"">Description</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""70"">Completed</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        Dim srch As String = txtsearch.Text
        srch = sc.ModString1(srch)

        sql = "select wonum, status, worktype, description, actfinish, retwonum from workorder where status = 'COMP' and siteid = '" & sid & "' order by wonum"
        If srch <> "" Then
            Filter = " status = ''COMP'' and siteid = ''" & sid & "'' and wonum = ''" & srch & "''" 'wonum not in (select w.fuswonum from woout w where w.retval = 1)"
            FilterCNT = " status = 'COMP' and siteid = '" & sid & "' and wonum = '" & srch & "'" ' wonum not in (select w.fuswonum from woout w where w.retval = 1)"
        Else
            Filter = " status = ''COMP'' and siteid = ''" & sid & "'' and ((retwonum is null or len(retwonum) = 0) or wonum in (select w.wonum from wolabtrans w where w.wonum = workorder.wonum and w.retwonum is null))" 'wonum not in (select w.fuswonum from woout w where w.retval = 1)"
            FilterCNT = " status = 'COMP' and siteid = '" & sid & "' and ((retwonum is null or len(retwonum) = 0) or wonum in (select w.wonum from wolabtrans w where w.wonum = workorder.wonum and w.retwonum is null))" ' wonum not in (select w.fuswonum from woout w where w.retval = 1)"
        End If

        'need to change filter to look for retwonum in workorder table once we establish that it can be filtered from return string

        Dim intPgCnt, intPgNav As Integer
        sql = "SELECT Count(distinct wonum) FROM workorder where " & FilterCNT
        intPgCnt = sc.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr588", "wolist.aspx.vb")

            sc.CreateMessageAlert(Me, strMessage, "strKey1")

            Exit Sub
        End If
        PageSize = "50"
        intPgNav = sc.PageCountRev(intPgCnt, PageSize)
        If PageNumber > intPgNav Then
            PageNumber = intPgNav

        End If

        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If


        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav

        Sort = "wonum asc"
        sql = "update workorder set retwonum = rtrim(retwonum);update workorder set retwonum = ltrim(retwonum);"
        sc.Update(sql)
        sql = "usp_getwolist_comp '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "'"
        Dim rowflag As Integer = 0
        Dim bg, wo, st, wt, dn, eq, af, rwo As String
        Dim crl As String = "A1U"

        dr = sc.GetRdrData(sql)
        While dr.Read
            rwo = dr.Item("retwonum").ToString
            If rowflag = 0 Then
                If rwo = "" Then
                    bg = "ptransrow"
                Else
                    bg = "gbgy"
                End If

                rowflag = 1
            Else
                If rwo = "" Then
                    bg = "ptransrowblue"
                Else
                    bg = "gbgy"
                End If

                rowflag = "0"
            End If
            wo = dr.Item("wonum").ToString
            st = dr.Item("status").ToString
            wt = dr.Item("worktype").ToString
            dn = dr.Item("description").ToString
            eq = dr.Item("eqnum").ToString
            af = dr.Item("actfinish").ToString
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""" & bg & """><a class=""" & crl & """ href=""#"" onclick=""gotoreq('" & wo & "')"">" & wo & "</a></td>" & vbCrLf)
            sb.Append("<td class=""" & bg & """>" & st & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & """>" & wt & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & """>" & eq & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & """>" & dn & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & """>" & af & "</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
        End While
        dr.Close()




        sb.Append("</table>" & vbCrLf)
        divywr.InnerHtml = sb.ToString

    End Sub

    Private Sub ddsites_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsites.SelectedIndexChanged
        sc.Open()
        'SendCant("1086")
        divret.InnerHtml = ""
        tdgo.InnerHtml = ""
        tdwo.InnerHtml = ""
        getwr("1")
        getdummy()
        sc.Dispose()
    End Sub
End Class