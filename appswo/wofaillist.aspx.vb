

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wofaillist
    Inherits System.Web.UI.Page
	Protected WithEvents btnaddtosite As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang1392 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1391 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1390 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1389 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1388 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1387 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim fail As New Utilities
    Dim cid, eqid, fuid, co, cod, coid, cvalu, app, sid, Login, wo As String
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbfailmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents cbopts As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ibtnret As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblopt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

      
	GetFSOVLIBS()
  GetFSLangs()



Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try

        Page.EnableViewState = True
        If Not IsPostBack Then
            Try
                cid = "0" 'Request.QueryString("cid").ToString
                lblcid.Value = cid
                'Session("comp") = cid
                lblsid.Value = HttpContext.Current.Session("dfltps").ToString
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                coid = Request.QueryString("coid").ToString
                lblcoid.Value = coid
                'Session("coid") = coid
                cvalu = Request.QueryString("cvalu").ToString
                tdcomp.InnerHtml = cvalu
                wo = Request.QueryString("wo").ToString
                lblwo.Value = wo
                lblopt.Value = "0"
                lbfailmodes.AutoPostBack = False
                lbfailmaster.AutoPostBack = False
                fail.Open()
                PopFail(cid, coid, wo)
                PopFailList(cid, coid, wo)
                fail.Dispose()
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr566" , "wofaillist.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try

        End If
        'ibtnret.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ibtnret.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
        'btntocomp.Attributes.Add("onclick", "DisableButton(this);")
        'btnfromcomp.Attributes.Add("onclick", "DisableButton(this);")

    End Sub
    Private Sub PopFailList(ByVal cid As String, ByVal comid As String, ByVal wonum As String)

        sql = "select compfailid, failuremode from componentfailmodes where compid = '" & cid & "' and " _
        + "comid = '" & coid & "' and comid <> '0' order by failuremode"

        sql = "select failid, failuremode from wofailmodes1 where wonum = '" & wonum & "' " _
        + "and failid not in (" _
         + "select failid from componentfailmodes where comid = '" & comid & "') order by failuremode"
        dr = fail.GetRdrData(sql)
        lbfailmodes.DataSource = dr
        lbfailmodes.DataValueField = "failid"
        lbfailmodes.DataTextField = "failuremode"
        lbfailmodes.DataBind()
        dr.Close()
        Try
            lbfailmodes.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub

    Private Sub PopFail(ByVal cid As String, ByVal comid As String, ByVal wonum As String)
        Dim chk As String = lblfailchk.Value
        Dim scnt As Integer
        sid = lblsid.Value
        If chk = "" Then
            sql = "select count(*) from pmSiteFM where siteid = '" & sid & "'"
            scnt = fail.Scalar(sql)
            If scnt = 0 Then
                lblfailchk.Value = "open"
                chk = "open"
            Else
                lblfailchk.Value = "site"
                chk = "site"
            End If
        End If
        Dim dt, val, filt As String
        If chk = "open" Then
            dt = "FailureModes"
        ElseIf chk = "site" Then
            dt = "pmSiteFM"
        Else
            Exit Sub
        End If

        val = "failid, failuremode"
        If chk = "site" Then
            filt = " where siteid = '" & sid & "' and failid not in (" _
        + "select failid from componentfailmodes where comid = '" & comid & "') and " _
        + "failid not in (" _
        + "select failid from wofailmodes1 where wonum = '" & wonum & "') " _
        + "order by failuremode asc"
        Else
            filt = " where failid not in (" _
        + "select failid from componentfailmodes where comid = '" & comid & "') and " _
        + "failid not in (" _
        + "select failid from wofailmodes1 where wonum = '" & wonum & "') " _
        + "order by failuremode asc"
        End If
       
        dr = fail.GetList(dt, val, filt) 'removed compid = '" & cid & "' and 
        lbfailmaster.DataSource = dr
        lbfailmaster.DataTextField = "failuremode"
        lbfailmaster.DataValueField = "failid"
        lbfailmaster.DataBind()
        dr.Close()
    End Sub

    Private Sub GetItems(ByVal fi As String, ByVal f As String, ByVal comp As String, ByVal typ As String)
        Dim wonum As String = lblwo.Value
        Dim fcnt, fcnt2 As Integer
        sql = "select count(*) from wofailmodes1 where wonum = '" & wonum & "' and failid = '" & fi & "'"
        fcnt = fail.Scalar(sql)
        If fcnt > 0 Then
            Dim strMessage As String =  tmod.getmsg("cdstr567" , "wofaillist.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Try
                If comp <> "na" Then
                    sql = "usp_addWoFailureMode " & wonum & ", " & fi & ", '" & f & "', '" & comp & "','" & typ & "'"
                Else
                    sql = "insert into wofailmodes1 (wonum, failid, failuremode) values " _
                    + "('" & wonum & "','" & fi & "','" & f & "')"

                End If
                fail.Update(sql)
                Dim Item As ListItem
                Dim fm As String
                For Each Item In lbfailmodes.Items
                    f = Item.Text.ToString
                    If Len(fm) = 0 Then
                        fm = f & "(___)"
                    Else
                        fm += " " & f & "(___)"
                    End If
                Next
                fm = Replace(fm, "'", Chr(180), , , vbTextCompare)
                If fcnt <> 1 Then
                    sql = "update wofailout set failout = '" & fm & "' where wonum = '" & wonum & "'"
                Else
                    sql = "insert into wofailout (wonum, failout) values " _
                    + "('" & wonum & "', '" & fm & "')"
                End If
                fail.Update(sql)
            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToComp()
    End Sub
    Private Sub ToComp()
        'fail.Dispose()
        wo = lblwo.Value
        cid = lblcid.Value
        'cid = "0"
        coid = lblcoid.Value
        'coid = HttpContext.Current.Session("coid").ToString()
        Dim Item As ListItem
        Dim f, fi As String
        'coid = lblcoid.Value
        'coid = HttpContext.Current.Session("coid").ToString()
        fail.Open()
        For Each Item In lbfailmaster.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                GetItems(fi, f, coid, "na")
            End If
        Next
        PopFailList(cid, coid, wo)
        PopFail(cid, coid, wo)
        fail.Dispose()
    End Sub
    Private Sub RemItems(ByVal fi As String, ByVal f As String)
        Dim wonum As String = lblwo.Value
        Dim comp As String = lblco.Value

        Try
            'If comp <> "" Then
            'sql = "sp_delWoFailureMode '" & wonum & "', '" & fi & "', '" & comp & "'"
            'Else
            sql = "delete from wofailmodes1 where wonum = '" & wonum & "' and failid = '" & fi & "'"
            'End If

            fail.Update(sql)
            Dim fm As String
            Dim Item As ListItem
            For Each Item In lbfailmodes.Items
                f = Item.Text.ToString
                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
            Next
            fm = Replace(fm, "'", Chr(180), , , vbTextCompare)
            sql = "update wofailout set failout = '" & fm & "' where wonum = '" & wonum & "'"
            fail.Update(sql)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        FromComp()
    End Sub
    Private Sub FromComp()
        Dim wonum As String = lblwo.Value
        cid = lblcid.Value
        'cid = "0"
        coid = lblcoid.Value
        'coid = HttpContext.Current.Session("coid").ToString()
        Dim Item As ListItem
        Dim f, fi As String
        coid = lblcoid.Value
        'coid = HttpContext.Current.Session("coid").ToString()
        fail.Open()
        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                RemItems(fi, f)
            End If
        Next
        PopFailList(cid, coid, wo)
        PopFail(cid, coid, wo)
        fail.Dispose()
    End Sub
    Private Sub lbfailmodes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbfailmodes.SelectedIndexChanged
        If lblopt.Value = "1" Then
            FromComp()
        End If

    End Sub
    Private Sub cbopts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbopts.SelectedIndexChanged
        Dim opt As String = cbopts.SelectedValue.ToString
        lblopt.Value = opt
        If opt = "0" Then
            lbfailmodes.AutoPostBack = False
            lbfailmaster.AutoPostBack = False
        Else
            lbfailmodes.AutoPostBack = True
            lbfailmaster.AutoPostBack = True
        End If
    End Sub

    Private Sub lbfailmaster_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbfailmaster.SelectedIndexChanged
        If lblopt.Value = "1" Then
            ToComp()
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1387.Text = axlabs.GetASPXPage("wofaillist.aspx", "lang1387")
        Catch ex As Exception
        End Try
        Try
            lang1388.Text = axlabs.GetASPXPage("wofaillist.aspx", "lang1388")
        Catch ex As Exception
        End Try
        Try
            lang1389.Text = axlabs.GetASPXPage("wofaillist.aspx", "lang1389")
        Catch ex As Exception
        End Try
        Try
            lang1390.Text = axlabs.GetASPXPage("wofaillist.aspx", "lang1390")
        Catch ex As Exception
        End Try
        Try
            lang1391.Text = axlabs.GetASPXPage("wofaillist.aspx", "lang1391")
        Catch ex As Exception
        End Try
        Try
            lang1392.Text = axlabs.GetASPXPage("wofaillist.aspx", "lang1392")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                ibtnret.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtnret.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtnret.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtnret.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtnret.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddtosite.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("wofaillist.aspx", "btnaddtosite") & "')")
            btnaddtosite.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class

