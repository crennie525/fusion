<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wosched.aspx.vb" Inherits="lucy_r12.wosched" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wosched</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/woschedaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkinv();">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" cellspacing="0" cellpadding="2" width="580">
        <tr>
            <td>
                <table width="320">
                    <tr height="26">
                        <td class="label" width="70">
                            <asp:Label ID="lang1570" runat="server">Work Order:</asp:Label>
                        </td>
                        <td class="plainlabel" id="tdwo" width="120" runat="server">
                        </td>
                    </tr>
                    <tr height="26">
                        <td class="label">
                            <asp:Label ID="lang1571" runat="server">Skill:</asp:Label>
                        </td>
                        <td class="plainlabel" id="tdskill" runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="24">
            <td class="thdrsing label">
                <asp:Label ID="lang1572" runat="server">Work Order Schedule Dates</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:DataGrid ID="dghours" runat="server" AutoGenerateColumns="False" ShowFooter="True">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <EditItemStyle Height="15px"></EditItemStyle>
                    <AlternatingItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="#E7F1FD">
                    </AlternatingItemStyle>
                    <ItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="ControlLightLight">
                    </ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgeditfm" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                &nbsp;
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Activity (Reference)">
                            <HeaderStyle Width="240px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Width="240px" Text='<%# DataBinder.Eval(Container, "DataItem.activity") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtactf" runat="server" Width="240px"></asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtacte" runat="server" Width="240px" Text='<%# DataBinder.Eval(Container, "DataItem.activity") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Schedule Date">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:TextBox ID="lblsdatei" ReadOnly="True" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.sdate") %>'>
                                </asp:TextBox>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="lblsdatef" ReadOnly="True" runat="server" Width="110px"></asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="lblsdatee" ReadOnly="True" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.sdate") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/btn_calendar.jpg">
                            <HeaderStyle Width="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <img src="../images/appbuttons/minibuttons/btn_calendar.jpg" id="imgsdate" runat="server">
                            </ItemTemplate>
                            <FooterTemplate>
                                <img src="../images/appbuttons/minibuttons/btn_calendar.jpg" id="imgsdatef" runat="server">
                            </FooterTemplate>
                            <EditItemTemplate>
                                <img src="../images/appbuttons/minibuttons/btn_calendar.jpg" id="imgsdatee" runat="server">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblwosidi" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.wosid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblwosid" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.wosid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Minutes">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.minutes") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtatimef" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.minutes") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtatime" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.minutes") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Parts/Tools/Lubes" Visible="False">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton23" runat="server" ImageUrl="../images/appbuttons/minibuttons/parttrans.gif"
                                    ToolTip="Add Parts" CommandName="Part"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton23a" runat="server" ImageUrl="../images/appbuttons/minibuttons/tooltrans.gif"
                                    ToolTip="Add Tools" CommandName="Tool"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton23b" runat="server" ImageUrl="../images/appbuttons/minibuttons/lubetrans.gif"
                                    ToolTip="Add Lubricants" CommandName="Lube"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/del.gif">
                            <HeaderStyle Width="24px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton25" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <input id="lblwo" type="hidden" runat="server">
    <input id="lblstat" type="hidden" runat="server">
    <input id="lblskill" type="hidden" runat="server">
    <input id="lblskillid" type="hidden" runat="server">
    <input id="lblsdays" type="hidden" runat="server">
    <input id="lblsdateret" type="hidden" name="lblsdateret" runat="server">
    <input id="lblwosidret" type="hidden" runat="server">
    <input id="lblsubmit" type="hidden" runat="Server">
    <input type="hidden" id="lblindx" runat="server">
    <input type="hidden" id="lbltool" runat="server" name="lbltool">
    <input type="hidden" id="lbllube" runat="server" name="lbllube">
    <input type="hidden" id="lblpart" runat="server" name="lblpart">
    <input type="hidden" id="lblpmtid" runat="server" name="lblpmtid">
    <input type="hidden" id="lbltasknum" runat="server" name="lbltasknum">
    <input type="hidden" id="lbllog" runat="server"><input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
