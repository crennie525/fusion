﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="woexpdchartdialog.aspx.vb" Inherits="lucy_r12.woexpdchartdialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Down Time History</title>
    <script language="JavaScript" type="text/javascript" src="../scripts/sessrefdialog.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/woexpddialogaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language ="javascript" type="text/javascript">
        function handleexit() {
            window.returnValue = "1";
            window.close();
        }
    </script>
</head>
<body  onload="resetsess();pageScroll();" onunload="handleexit();">
    <form id="form1" runat="server">
    <iframe id="ifexpd" runat="server" src="" width="100%" height="500" frameborder="0">
    </iframe>
    <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
    <script type="text/javascript">
        document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script>
    <input type="hidden" id="lblsessrefresh" runat="server" name="lblsessrefresh" />
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
