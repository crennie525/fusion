

'********************************************************
'*
'********************************************************



Public Class woexpddialog
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim mu As New mmenu_utils_a
    Dim coi As String
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim usr, eqid, wonum, isdown, stat, wt, isdownp, pmid, pmhid As String
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifexpd As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try

        coi = mu.COMPI

        If Not IsPostBack Then
            usr = Request.QueryString("usr").ToString
            eqid = Request.QueryString("eqid").ToString
            wonum = Request.QueryString("wo").ToString
            isdown = Request.QueryString("isdown").ToString
            isdownp = Request.QueryString("isdownp").ToString
            wt = Request.QueryString("wt").ToString
            stat = Request.QueryString("stat").ToString
            pmid = Request.QueryString("pmid").ToString
            pmhid = Request.QueryString("pmhid").ToString
            If coi <> "NISS_OLD" Then
                ifexpd.Attributes.Add("src", "woexpds.aspx?wo=" + wonum + "&eqid=" + eqid + "&usr=" + usr + "&isdown=" + isdown + "&stat=" + stat + "&isdownp=" + isdownp + "&wt=" + wt + "&pmid=" + pmid + "&pmhid=" + pmhid)
            Else
                ifexpd.Attributes.Add("src", "woexpd.aspx?wo=" + wonum + "&eqid=" + eqid + "&usr=" + usr + "&isdown=" + isdown + "&stat=" + stat + "&isdownp=" + isdownp + "&wt=" + wt + "&pmid=" + pmid + "&pmhid=" + pmhid)
            End If

        End If

    End Sub

End Class
