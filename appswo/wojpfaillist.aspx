<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wojpfaillist.aspx.vb"
    Inherits="lucy_r12.wojpfaillist" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wojpfaillist</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/wojpfaillistaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table width="422">
        <tr>
            <td class="thdrsingrt label" colspan="2">
                <asp:Label ID="lang1471" runat="server">Add Failure Modes from Master List Mini Dialog</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label" width="172" height="26">
                <asp:Label ID="lang1472" runat="server">Current Component:</asp:Label>
            </td>
            <td class="labellt" id="tdcomp" width="250" height="26" runat="server">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr style="border-right: #0000ff 1px solid; border-top: #0000ff 1px solid; border-left: #0000ff 1px solid;
                    border-bottom: #0000ff 1px solid">
            </td>
        </tr>
    </table>
    <table width="422">
        <tr>
            <td class="label" align="center">
                <asp:Label ID="lang1473" runat="server">Available Failure Modes</asp:Label>
            </td>
            <td>
            </td>
            <td class="label" align="center">
                <asp:Label ID="lang1474" runat="server">Selected Failure Modes</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" width="200">
                <asp:ListBox ID="lbfailmaster" runat="server" Height="150px" SelectionMode="Multiple"
                    Width="170px"></asp:ListBox>
            </td>
            <td valign="middle" align="center" width="22">
                <img class="details" id="todis" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                    width="20">
                <img class="details" id="fromdis" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
                    width="20">
                <asp:ImageButton ID="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                </asp:ImageButton><asp:ImageButton ID="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                </asp:ImageButton><img class="details" id="btnaddtosite" onmouseover="return overlib('Choose Failure Modes for this Plant Site ')"
                    onclick="getss();" onmouseout="return nd()" height="20" src="../images/appbuttons/minibuttons/plusminus.gif"
                    width="20">
            </td>
            <td align="center" width="200">
                <asp:ListBox ID="lbfailmodes" runat="server" Height="150px" SelectionMode="Multiple"
                    Width="170px"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="3">
                <img id="ibtnret" onclick="handleexit();" height="19" alt="" src="../images/appbuttons/bgbuttons/return.gif"
                    width="69" runat="server">
            </td>
        </tr>
        <tr>
            <td class="bluelabel" colspan="3">
                <asp:Label ID="lang1475" runat="server">List Box Options</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label" colspan="3">
                <asp:RadioButtonList ID="cbopts" runat="server" Width="400px" CssClass="labellt"
                    AutoPostBack="True">
                    <asp:ListItem Value="0" Selected="True">Multi-Select (use arrows to move single or multiple selections)</asp:ListItem>
                    <asp:ListItem Value="1">Click-Once (move selected item by clicking that item)</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="plainlabelblue" colspan="3">
                <asp:Label ID="lang1476" runat="server">* Please note that the Multi-Select Mode must be used to remove an item from the Selected Failure Modes list if only one item is present.</asp:Label>
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lblco" type="hidden" name="lblco" runat="server"><input id="lblapp" type="hidden"
        name="lblapp" runat="server">
    <input id="lblopt" type="hidden" name="lblopt" runat="server"><input id="lblfailchk"
        type="hidden" name="lblfailchk" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server"><input id="lbllog"
        type="hidden" name="lbllog" runat="server">
    <input id="lblwo" type="hidden" name="lblwo" runat="server">
    <input id="lblpmtskid" type="hidden" runat="server" name="lblpmtskid">
    <input type="hidden" id="lbljpid" runat="server" name="lbljpid">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
