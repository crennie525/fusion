

'********************************************************
'*
'********************************************************



Public Class woimage
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents imgeq As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdcnt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents pr As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ne As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents po As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblpic As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcur As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpareqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblurl As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetImgEq()
        'Put user code to initialize the page here
    End Sub
    Private Sub GetImgEq()
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgeq.Attributes.Add("src", "../images2/eng/eqimg1.gif")
        ElseIf lang = "fre" Then
            imgeq.Attributes.Add("src", "../images2/fre/eqimg1.gif")
        ElseIf lang = "ger" Then
            imgeq.Attributes.Add("src", "../images2/ger/eqimg1.gif")
        ElseIf lang = "ita" Then
            imgeq.Attributes.Add("src", "../images2/ita/eqimg1.gif")
        ElseIf lang = "spa" Then
            imgeq.Attributes.Add("src", "../images2/spa/eqimg1.gif")
        End If
    End Sub
End Class
