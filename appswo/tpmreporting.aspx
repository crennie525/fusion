<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="tpmreporting.aspx.vb"
    Inherits="lucy_r12.tpmreporting" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>tpmreporting</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="javascript" type="text/javascript">
    <!--
        function gettab(name) {
            var wo = document.getElementById("lblwo").value;
            var ro = document.getElementById("lblro").value;
            var stat = document.getElementById("lblstat").value;
            var pm = document.getElementById("lblpmid").value;
            var pmhid = document.getElementById("lblpmhid").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            if (wo != "") {
                if (name == "f") {
                    closeall();
                    document.getElementById("tdf").className = "thdrhov label";
                    document.getElementById("wo").className = 'tdborder view';
                    document.getElementById("ifwo").src = "../appsmantpm/pmfmtpm.aspx?pmid=" + pm + "&date=" + Date() + "&ro=" + ro + "&wo=" + wo;
                }
                else if (name == "m") {
                    closeall();
                    document.getElementById("tdm").className = "thdrhov label";
                    document.getElementById("wo").className = 'tdborder view';
                    document.getElementById("ifwo").src = "../appsmantpm/pmmetpm.aspx?pmid=" + pm + "&date=" + Date() + "&ro=" + ro + "&wo=" + wo;
                }
                else if (name == "par") {
                    closeall();
                    document.getElementById("tdpar").className = "thdrhov label";
                    document.getElementById("wo").className = 'tdborder view';
                    document.getElementById("ifwo").src = "../appsmantpm/pmactmtpm.aspx?typ=p&wo=" + wo + "&pmid=" + pm + "&date=" + Date() + "&ro=" + ro;
                }
                else if (name == "too") {
                    closeall();
                    document.getElementById("tdtoo").className = "thdrhov label";
                    document.getElementById("wo").className = 'tdborder view';
                    document.getElementById("ifwo").src = "../appsmantpm/pmactmtpm.aspx?typ=t&wo=" + wo + "&pmid=" + pm + "&date=" + Date() + "&ro=" + ro;
                }
                else if (name == "lub") {
                    closeall();
                    document.getElementById("tdlub").className = "thdrhov label";
                    document.getElementById("wo").className = 'tdborder view';
                    document.getElementById("ifwo").src = "../appsmantpm/pmactmtpm.aspx?typ=l&wo=" + wo + "&pmid=" + pm + "&date=" + Date() + "&ro=" + ro;
                }
                else if (name == "act") {
                    closeall();
                    var jp;
                    jp = "";
                    document.getElementById("tdact").className = "thdrhov label";
                    document.getElementById("wo").className = 'tdborder view';
                    document.getElementById("ifwo").src = "../appsmantpm/pmacttpm.aspx?wo=" + wo + "&pmid=" + pm + "&pmhid=" + pmhid + "&date=" + Date() + "&ro=" + ro;
                }
                else if (name == "lab") {
                    closeall();
                    var jp;
                    jp = "";
                    document.getElementById("tdlab").className = "thdrhov label";
                    document.getElementById("wo").className = 'tdborder view';
                    document.getElementById("ifwo").src = "wolabtrans.aspx?sid=" + sid + "&wo=" + wo + "&jpid=" + jp + "&stat=" + stat + "&date=" + Date() + "&ro=" + ro + "&uid=" + uid;
                }
            }
        }
        function closeall() {
            document.getElementById("tdf").className = "thdr label";
            document.getElementById("tdm").className = "thdr label";
            document.getElementById("tdpar").className = "thdr label";
            document.getElementById("tdtoo").className = "thdr label";
            document.getElementById("tdlub").className = "thdr label";
            document.getElementById("tdlab").className = "thdr label";
            document.getElementById("tdact").className = "thdr label";
            document.getElementById("wo").className = 'details';
        }
        function setref() {
            //window.parent.setref();
        }
        function doref() {
            //window.parent.doref();
        }
        //-->
    </script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 2px; left: 0px" id="scrollmenu" cellspacing="0"
        cellpadding="2" width="900">
        <tr>
            <td>
                <img src="../images/appbuttons/minibuttons/6PX.gif">
            </td>
        </tr>
        <tr height="22">
            <td id="tdf" class="thdrhov label" onclick="gettab('f');" width="100" runat="server">
                <asp:Label ID="lang1393" runat="server">Failure Modes</asp:Label>
            </td>
            <td id="tdm" class="thdr label" onclick="gettab('m');" width="100" runat="server">
                <asp:Label ID="lang1394" runat="server">Measurements</asp:Label>
            </td>
            <td id="tdpar" class="thdr label" onclick="gettab('par');" width="100" runat="server">
                <asp:Label ID="lang1335a" runat="server">Parts</asp:Label>
            </td>
            <td id="tdtoo" class="thdr label" onclick="gettab('too');" width="100" runat="server">
                <asp:Label ID="lang1336a" runat="server">Tools</asp:Label>
            </td>
            <td id="tdlub" class="thdr label" onclick="gettab('lub');" width="100" runat="server">
                <asp:Label ID="lang1337a" runat="server">Lubricants</asp:Label>
            </td>
            <td id="tdact" class="thdr label" onclick="gettab('act');" width="160" runat="server">
                <asp:Label ID="Label2" runat="server">Task Details (Labor)</asp:Label>
            </td>
            <td id="tdlab" class="thdr label" onclick="gettab('lab');" width="120" runat="server">
                <asp:Label ID="Label1" runat="server">Labor Reporting</asp:Label>
            </td>
            <td width="20">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td id="wo" class="tdborder" colspan="7" runat="server">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <iframe id="ifwo" height="420" src="wohold.htm" frameborder="no" width="1020" runat="server">
                            </iframe>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblwo" type="hidden" name="lblwo" runat="server">
    <input id="lblfmstr" type="hidden" name="lblfmstr" runat="server">
    <input id="lblmstr" type="hidden" name="lblmstr" runat="server">
    <input id="lblsubmit" type="hidden" name="lblsubmit" runat="server">
    <input id="lblupsav" type="hidden" name="lblupsav" runat="server">
    <input id="lblstat" type="hidden" name="lblstat" runat="server">
    <input id="xCoord" type="hidden" name="xCoord" runat="server">
    <input id="yCoord" type="hidden" name="yCoord" runat="server">
    <input id="lblro" type="hidden" name="lblro" runat="server">
    <input id="lblfslang" type="hidden" name="lblfslang" runat="server">
    <input id="lblpmid" type="hidden" runat="server" name="lblpmid"><input type="hidden"
        id="lblpmhid" runat="server" name="lblpmhid">
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id = "lbluid" runat="server" />
    </form>
</body>
</html>
