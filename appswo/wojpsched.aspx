<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wojpsched.aspx.vb" Inherits="lucy_r12.wojpsched" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>wojpsched</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/wojpschedaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="scrolltop();checkinv();">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" cellspacing="0" cellpadding="2" width="600">
        <tr>
            <td>
                <table width="600">
                    <tr height="26">
                        <td class="label" width="70">
                            <asp:Label ID="lang1516" runat="server">Job Plan:</asp:Label>
                        </td>
                        <td class="plainlabel" id="tdjpn" width="120" runat="server">
                        </td>
                        <td class="plainlabel" id="tdjpd" width="410" colspan="6" runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="24">
            <td class="thdrsing label">
                <asp:Label ID="lang1517" runat="server">Job Plan Scheduled Dates</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:DataGrid ID="dghours" runat="server" AutoGenerateColumns="False">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <EditItemStyle Height="15px"></EditItemStyle>
                    <AlternatingItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="#E7F1FD">
                    </AlternatingItemStyle>
                    <ItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="ControlLightLight">
                    </ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit" Visible="False">
                            <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgeditfm" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Task#">
                            <HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label3" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif">
                            <HeaderStyle Width="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <img src="../images/appbuttons/minibuttons/magnifier.gif" id="imgti" runat="server">
                            </ItemTemplate>
                            <EditItemTemplate>
                                <img src="../images/appbuttons/minibuttons/magnifier.gif" id="imgte" runat="server">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Skill Required">
                            <HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label2" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Qty">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label6" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Skill Min Ea">
                            <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="txtqty" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Lead Craft" Visible="False">
                            <HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.leadcraft") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtalead" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.leadcraft") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif"
                            Visible="False">
                            <HeaderStyle Width="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <img src="../images/appbuttons/minibuttons/magnifierdis.gif" id="imglci" runat="server">
                            </ItemTemplate>
                            <EditItemTemplate>
                                <img src="../images/appbuttons/minibuttons/magnifier.gif" id="imglce" runat="server">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Schedule Date">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblsdate" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.sdate") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtatime" runat="server" Width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.sdate") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/btn_calendar.jpg">
                            <HeaderStyle Width="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <img src="../images/appbuttons/minibuttons/btn_calendar.jpg" id="imgsdate" runat="server">
                            </ItemTemplate>
                            <EditItemTemplate>
                                <img src="../images/appbuttons/minibuttons/btn_calendar.jpg" id="Img2" runat="server">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Parts/Tools/Lubes" Visible="False">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton23" runat="server" ImageUrl="../images/appbuttons/minibuttons/parttrans.gif"
                                    ToolTip="Add Parts" CommandName="Part"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton23a" runat="server" ImageUrl="../images/appbuttons/minibuttons/tooltrans.gif"
                                    ToolTip="Add Tools" CommandName="Tool"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton23b" runat="server" ImageUrl="../images/appbuttons/minibuttons/lubetrans.gif"
                                    ToolTip="Add Lubricants" CommandName="Lube"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Task#" Visible="False">
                            <HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lbltida" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.wojtid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblttid" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.wojtid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <div class="details" id="tskdiv" style="border-right: black 1px solid; border-top: black 1px solid;
        z-index: 999; border-left: black 1px solid; width: 450px; border-bottom: black 1px solid;
        position: absolute; height: 180px">
        <table cellspacing="0" cellpadding="0" width="450" bgcolor="white">
            <tr bgcolor="blue">
                <td>
                    &nbsp;
                    <asp:Label ID="Label24" runat="server" ForeColor="White" Font-Size="10pt" Font-Names="Arial"
                        Font-Bold="True">Task Description</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closetsk();" alt="" src="../images/appbuttons/minibuttons/close.gif"><br>
                </td>
            </tr>
            <tr class="tbg" height="30">
                <td style="padding-right: 3px; padding-left: 3px; padding-bottom: 3px; padding-top: 3px"
                    colspan="2">
                    <asp:TextBox ID="txttsk" runat="server" Width="440px" Height="170px" TextMode="MultiLine"
                        ReadOnly="True" CssClass="plainlabel"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblwo" type="hidden" name="lblwo" runat="server">
    <input id="lbljpid" type="hidden" name="lbljpid" runat="server">
    <input id="lblstat" type="hidden" name="lblstat" runat="server">
    <input id="xCoord" type="hidden" name="xCoord" runat="server">
    <input id="yCoord" type="hidden" name="yCoord" runat="server">
    <input id="lblrow" type="hidden" name="lblrow" runat="server">
    <input id="lbllead" type="hidden" name="lbllead" runat="server">
    <input id="txtlead" type="hidden" name="txtlead" runat="server">
    <input id="lblwojtid" type="hidden" runat="server" name="lblwojtid">
    <input id="lblindx" type="hidden" runat="server" name="lblindx">
    <input id="lblsubmit" type="hidden" runat="server" name="lblsubmit">
    <input type="hidden" id="lblsdateret" runat="server">
    <input type="hidden" id="lbltool" runat="server" name="lbltool">
    <input type="hidden" id="lbllube" runat="server" name="lbllube">
    <input type="hidden" id="lblpart" runat="server" name="lblpart">
    <input type="hidden" id="lblpmtid" runat="server" name="lblpmtid">
    <input type="hidden" id="lbltasknum" runat="server" name="lbltasknum">
    <input type="hidden" id="lbllog" runat="server" name="lbllog">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
