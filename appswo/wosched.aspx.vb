

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wosched
    Inherits System.Web.UI.Page
	Protected WithEvents lang1572 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1571 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1570 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim comp As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim ds As DataSet
    Dim wonum, stat, skillid, skill, sdays, ro, rostr As String
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsdays As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsdateret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwosidret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblindx As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltool As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllube As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dghours As System.Web.UI.WebControls.DataGrid
    Protected WithEvents tdskill As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                dghours.Columns(0).Visible = False
                dghours.Columns(7).Visible = False
            Else
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = comp.CheckROS(rostr, "wo")
                    lblro.Value = ro
                    If ro = "1" Then
                        dghours.Columns(0).Visible = False
                        dghours.Columns(7).Visible = False
                    End If
                End If
            End If

            wonum = Request.QueryString("wo").ToString
            tdwo.InnerHtml = wonum
            stat = Request.QueryString("stat").ToString
            skill = Request.QueryString("skill").ToString
            lblskill.Value = skill
            tdskill.InnerHtml = skill
            sdays = Request.QueryString("sdays").ToString
            lblsdays.Value = sdays
            skillid = Request.QueryString("skillid").ToString
            lblskillid.Value = skill
            lblwo.Value = wonum
            lblstat.Value = stat
            If (stat <> "COMP" And stat <> "CAN") Then  'And stat <> "WAPPR"
                dghours.Columns(0).Visible = True
                dghours.Columns(7).Visible = True
            Else
                dghours.Columns(0).Visible = False
                dghours.Columns(7).Visible = False
            End If
            comp.Open()
            CheckWOSched()
            PopDL(wonum)
            comp.Dispose()
        Else
            If Request.Form("lblsubmit") = "upsdate" Then
                comp.Open()
                UpSDate()
                wonum = lblwo.Value
                PopDL(wonum)
                comp.Dispose()
            End If
        End If
    End Sub
    Private Sub UpSDate()
        Dim wosid As String = lblwosidret.Value
        Dim sdate As String = lblsdateret.Value
        sql = "update wosched set sdate = '" & sdate & "' where wosid = '" & wosid & "'"
        comp.Update(sql)
        Dim wonum As String = lblwo.Value
        sql = "usp_checksdates '" & wonum & "'"
        comp.Update(sql)
    End Sub
    Private Sub CheckWOSched()
        wonum = lblwo.Value
        sdays = lblsdays.Value
        Dim scnt, sdaysi, i As Integer
        Dim sdate As Date
        sdaysi = CType(sdays, Integer)
        sql = "select count(*) from wosched where wonum = '" & wonum & "'"
        scnt = comp.Scalar(sql)
        If scnt = 0 Then
            Try
                sql = "select schedstart from workorder where wonum = '" & wonum & "'"
                sdate = comp.strScalar(sql)
                For i = 0 To sdaysi
                    sql = "insert into wosched (wonum, minutes, sdate) values ('" & wonum & "','0', " _
                    + "dateadd(dd, " & i & ", '" & sdate & "'))"
                    comp.Update(sql)
                Next
            Catch ex As Exception

            End Try
           
        End If

    End Sub
    Private Sub PopDL(ByVal wonum As String)
        If lblindx.Value = "" Then
            dghours.EditItemIndex = -1
        Else
            dghours.EditItemIndex = lblindx.Value

        End If
        sql = "select wosid, Convert(char(10),sdate,101) as sdate, minutes, activity from wosched where wonum = '" & wonum & "'"
        ds = comp.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        'Try
        dghours.DataSource = dv
        dghours.DataBind()
    End Sub

    Private Sub dghours_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dghours.EditCommand
        comp.Open()
        wonum = lblwo.Value
        dghours.EditItemIndex = e.Item.ItemIndex
        lblindx.Value = e.Item.ItemIndex
        PopDL(wonum)
        comp.Dispose()
    End Sub

    Private Sub dghours_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dghours.CancelCommand
        dghours.EditItemIndex = -1
        lblindx.Value = -1
        comp.Open()
        wonum = lblwo.Value
        PopDL(wonum)
        comp.Dispose()
    End Sub

    Private Sub dghours_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dghours.UpdateCommand
        Dim ret, typ, fail, pass, task, atime, lc, wosid, act As String
        Dim tsk, cnt, adj As String
        wonum = lblwo.Value
        wosid = CType(e.Item.FindControl("lblwosid"), Label).Text
        act = CType(e.Item.FindControl("txtacte"), TextBox).Text
        act = Replace(act, "'", Chr(180), , , vbTextCompare)
        act = Replace(act, "--", "-", , , vbTextCompare)
        act = Replace(act, ";", ":", , , vbTextCompare)
        If Len(act) > 500 Then
            Dim strMessage As String =  tmod.getmsg("cdstr610" , "wosched.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        atime = CType(e.Item.FindControl("txtatime"), TextBox).Text
        'lc = CType(e.Item.FindControl("txtalead"), TextBox).Text
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToDecimal(atime)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr611" , "wosched.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        sql = "update wosched set minutes = '" & atime & "', activity = '" & act & "' where wosid = '" & wosid & "' and wonum = '" & wonum & "'; " _
        + "update workorder set estlabhrs = (select sum(minutes) / 60 from wosched where wonum = '" & wonum & "') " _
        + "where wonum = '" & wonum & "'"

        comp.Open()
        comp.Update(sql)
        sql = "usp_checksdates '" & wonum & "'"
        comp.Update(sql)
        wonum = lblwo.Value
        dghours.EditItemIndex = -1
        lblindx.Value = ""
        PopDL(wonum)
        comp.Dispose()
    End Sub

    Private Sub dghours_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dghours.ItemDataBound

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim wosid As String = DataBinder.Eval(e.Item.DataItem, "wosid").ToString
            Dim lid As String = CType(e.Item.FindControl("lblsdatei"), TextBox).ClientID
            Dim imgs As HtmlImage = CType(e.Item.FindControl("imgsdate"), HtmlImage)
            imgs.Attributes.Add("onclick", "getcal('" & lid & "','" & wosid & "');")
        End If
        If e.Item.ItemType = ListItemType.EditItem Then
            Dim wosid As String = DataBinder.Eval(e.Item.DataItem, "wosid").ToString
            Dim lid As String = CType(e.Item.FindControl("lblsdatee"), TextBox).ClientID
            Dim imgs As HtmlImage = CType(e.Item.FindControl("imgsdatee"), HtmlImage)
            imgs.Attributes.Add("onclick", "getcal('" & lid & "','" & wosid & "');")

        End If
        If e.Item.ItemType = ListItemType.Footer Then
            Dim lid As String = CType(e.Item.FindControl("lblsdatef"), TextBox).ClientID
            Dim imgs As HtmlImage = CType(e.Item.FindControl("imgsdatef"), HtmlImage)
            imgs.Attributes.Add("onclick", "getcal('" & lid & "','0');")
        End If
    End Sub

    Private Sub dghours_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dghours.DeleteCommand
        Dim wosid As String = CType(e.Item.FindControl("lblwosidi"), Label).Text
        Dim wonum As String = lblwo.Value
        sql = "delete from wosched where wosid = '" & wosid & "'; " _
        + "update workorder set estlabhrs = (select sum(minutes) / 60 from wosched where wonum = '" & wonum & "') " _
        + "where wonum = '" & wonum & "'"
        comp.Open()
        comp.Update(sql)
        sql = "usp_checksdates '" & wonum & "'"
        comp.Update(sql)
        lblindx.Value = ""
        PopDL(wonum)
        comp.Dispose()
    End Sub

    Private Sub dghours_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dghours.ItemCommand
        If e.CommandName = "Add" Then
            Dim ret, typ, fail, pass, task, atime, lc, wosid, act, sdate As String
            Dim tsk, cnt, adj As String
            wonum = lblwo.Value
            act = CType(e.Item.FindControl("txtactf"), TextBox).Text
            act = Replace(act, "'", Chr(180), , , vbTextCompare)
            act = Replace(act, "--", "-", , , vbTextCompare)
            act = Replace(act, ";", ":", , , vbTextCompare)
            If Len(act) > 500 Then
                Dim strMessage As String =  tmod.getmsg("cdstr612" , "wosched.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            atime = CType(e.Item.FindControl("txtatimef"), TextBox).Text
            Dim qtychk As Long
            Try
                qtychk = System.Convert.ToDecimal(atime)
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr613" , "wosched.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            sdate = CType(e.Item.FindControl("lblsdatef"), TextBox).Text
            If Len(sdate) = 0 Then
                Dim strMessage As String =  tmod.getmsg("cdstr614" , "wosched.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            sql = "insert into wosched (minutes, activity, sdate) values ('" & atime & "','" & act & "','" & sdate & "'); " _
            + "update workorder set estlabhrs = (select sum(minutes) / 60 from wosched where wonum = '" & wonum & "') " _
            + "where wonum = '" & wonum & "'"
            comp.Open()
            comp.Update(sql)
            sql = "usp_checksdates '" & wonum & "'"
            comp.Update(sql)
            lblindx.Value = ""
            PopDL(wonum)
            comp.Dispose()
        End If
        If e.CommandName = "Tool" Then
            lbltool.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = "0" 'CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = "0" 'CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = "0" 'CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = "0" 'CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If

        End If
        If e.CommandName = "Part" Then
            lblpart.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = "0" 'CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = "0" 'CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = "0" 'CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = "0" 'CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        If e.CommandName = "Lube" Then
            lbllube.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = "0" 'CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = "0" 'CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = "0" 'CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = "0" 'CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dghours.Columns(0).HeaderText = dlabs.GetDGPage("wosched.aspx", "dghours", "0")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(1).HeaderText = dlabs.GetDGPage("wosched.aspx", "dghours", "1")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(2).HeaderText = dlabs.GetDGPage("wosched.aspx", "dghours", "2")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(3).HeaderText = dlabs.GetDGPage("wosched.aspx", "dghours", "3")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(5).HeaderText = dlabs.GetDGPage("wosched.aspx", "dghours", "5")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1570.Text = axlabs.GetASPXPage("wosched.aspx", "lang1570")
        Catch ex As Exception
        End Try
        Try
            lang1571.Text = axlabs.GetASPXPage("wosched.aspx", "lang1571")
        Catch ex As Exception
        End Try
        Try
            lang1572.Text = axlabs.GetASPXPage("wosched.aspx", "lang1572")
        Catch ex As Exception
        End Try

    End Sub

End Class
