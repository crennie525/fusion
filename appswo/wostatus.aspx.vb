

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wostatus
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim stat As New Utilities
    Protected WithEvents lblwostatus As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim typ, wonum, val As String
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents statdiv As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            typ = Request.QueryString("typ").ToString
            wonum = Request.QueryString("wo").ToString
            lbltyp.Value = typ
            lblwonum.Value = wonum
            stat.Open()
            If typ = "stat" Then
                GetStatus()
            Else
                GetWOType()
            End If
            stat.Dispose()
        Else
            If Request.Form("lblsubmit") = "savewo" Then
                stat.Open()
                savewo()
                stat.Dispose()
            End If
        End If
    End Sub
    Private Sub savewo()
        wonum = lblwonum.Value
        typ = lbltyp.Value
        val = lblwostatus.Value
        Dim worts1, reportedby As String
        sql = "select worts1, reportedby from workorder where wonum = '" & wonum & "'"
        'Dim dr As New SqlDataReader
        dr = stat.GetRdrData(sql)
        While dr.Read
            worts1 = dr.Item("worts1").ToString
            reportedby = dr.Item("reportedby").ToString
        End While
        dr.Close()
        If worts1 = "Y" Then
            Try
                Dim mail1 As New pmmail
                mail1.CheckIt("reqr", reportedby, wonum)
            Catch ex As Exception

            End Try
        End If
        Dim wstat, pri, priid, prichk, usepri As String
        If wonum <> "" Then
            If typ = "stat" Then
                sql = "update workorder set status = '" & val & "', statusdate = getDate() where wonum = '" & wonum & "'"
                stat.Update(sql)
            Else
                'need pri and stat check here
                sql = "update workorder set worktype = '" & val & "' where wonum = '" & wonum & "'"
                stat.Update(sql)
                sql = "select [default] from wovars where [column] = 'wopriority'"
                Try
                    prichk = stat.strScalar(sql)
                Catch ex As Exception
                    prichk = "no"
                End Try
                sql = "select dfltpri, dfltpriid, dfltstat, usepri from wotype where wotype = '" & val & "'"
                dr = stat.GetRdrData(sql)
                While dr.Read
                    pri = dr.Item("dfltpri").ToString
                    priid = dr.Item("dfltpriid").ToString
                    wstat = dr.Item("dfltstat").ToString
                    usepri = dr.Item("usepri").ToString
                End While
                dr.Close()
                Dim priarr() As String = pri.Split(" - ")
                If prichk = "yes" Then
                    If priid <> "" And prichk = "yes" Then
                        pri = priarr(0)
                        sql = "update workorder set wopri = '" & priid & "', wopriority = '" & pri & "' where wonum = '" & wonum & "'"
                        stat.Update(sql)
                    Else
                        If pri <> "" And pri <> "" Then
                            sql = "update workorder set wopri = '" & pri & "' where wonum = '" & wonum & "'"
                            stat.Update(sql)
                        End If
                    End If
                Else
                    If priid <> "" And usepri = "1" Then
                        pri = priarr(0)
                        sql = "update workorder set wopri = '" & priid & "', wopriority = '" & pri & "' where wonum = '" & wonum & "'"
                        stat.Update(sql)
                    Else
                        If pri <> "" Then
                            sql = "update workorder set wopri = null, wopriority = '" & pri & "' where wonum = '" & wonum & "'"
                            stat.Update(sql)
                        End If
                    End If
                End If
                If wstat <> "" Then
                    sql = "update workorder set status = '" & wstat & "', statusdate = getDate() where wonum = '" & wonum & "'"
                    stat.Update(sql)
                End If
            End If

        End If

        lblsubmit.Value = "return"
    End Sub
    Private Sub GetWOType()
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        sql = "select * from wotype where active = 1 and wotype not in ('PM','TPM','MAXPM')"
        Dim wotype, wodesc As String
        dr = stat.GetRdrData(sql)
        While dr.Read
            wotype = dr.Item("wotype").ToString
            wodesc = dr.Item("description").ToString
            sb.Append("<tr><td class=""linklabel"" width=""100""><a href=""#"" onclick=""getstat('" & wotype & "');"">" & wotype & "</a></td>")
            sb.Append("<td class=""plainlabel"" width=""200"">" & wodesc & "</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        statdiv.InnerHtml = sb.ToString
    End Sub
    Private Sub GetStatus()
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        sql = "select * from wostatus where active = 1"
        Dim wostatus, wodesc As String
        dr = stat.GetRdrData(sql)
        While dr.Read
            wostatus = dr.Item("wostatus").ToString
            wodesc = dr.Item("description").ToString
            sb.Append("<tr><td class=""linklabel"" width=""100""><a href=""#"" onclick=""getstat('" & wostatus & "');"">" & wostatus & "</a></td>")
            sb.Append("<td class=""plainlabel"" width=""200"">" & wodesc & "</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        statdiv.InnerHtml = sb.ToString
    End Sub
End Class
