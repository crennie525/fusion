﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="cansendout2.aspx.vb" Inherits="lucy_r12.cansendout2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function excel() {
            document.getElementById("lblret").value = "excel"
            document.getElementById("form1").submit();
        }
    </script>
    <style type="text/css">
        .hdr
        {
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 14px;
            font-weight: bold;
            text-decoration: none;
            color: black;
            background: #f2f2f2;
            vertical-align: middle;
            height: 36px;
        }
        .hdrsing
        {
            padding-left: .5em;
            background: #B9D6F8;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            font-weight: bold;
            text-decoration: none;
            height: 24px;
        }
        .bgy
        {
            background: Yellow;
        }
        .gbgy
        {
            background: Yellow;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
        }
        .wolistsmall2a
        {
            width: 600px;
            height: 410px;
            overflow: auto;
        }
        .wolistsmall3a
        {
            width: 420px;
            height: 410px;
            overflow: auto;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td class="plainlabelblue"><a href="#" onclick="excel();">Export to Excel (Top 600)</a></td>
    </tr>
    <tr>
    <td>
    <asp:datagrid id="dgoutvw" runat="server" ShowFooter="False" CellPadding="4" 
            ForeColor="#333333">
        <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
        <EditItemStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle CssClass="thdrsingg plainlabel" />
        <ItemStyle CssClass="ptransrow" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        </asp:datagrid>
    </td>
    </tr>
    </table>
    </div>
    <asp:datagrid id="dgout" runat="server" ShowFooter="True"></asp:datagrid>
    <input type="hidden" id="lblret" runat="server" />
    </form>
</body>
</html>
