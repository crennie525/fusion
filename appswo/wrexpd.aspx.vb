

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wrexpd
    Inherits System.Web.UI.Page
	Protected WithEvents lang1611 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim wr As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents txtwodesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim wonum, usr, eqid As String
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            wonum = Request.QueryString("wo").ToString
            lblwo.Value = wonum
            usr = Request.QueryString("usr").ToString
            lbluser.Value = usr
            eqid = Request.QueryString("eqid").ToString
            lbleqid.value = eqid
            wr.Open()
            GetDesc(wonum)
            wr.Dispose()
        Else
            If Request.Form("lblsubmit") = "updesc" Then
                lblsubmit.Value = ""
                wr.Open()
                SaveDesc()
                wr.Dispose()

            End If
        End If
    End Sub
    Private Sub GetDesc(ByVal wonum As String)
        sql = "select description from workorder where wonum = '" & wonum & "'"
        dr = wr.GetRdrData(sql)
        While dr.Read
            txtwodesc.Text = dr.Item("description").ToString
        End While
        dr.Close()
        sql = "select longdesc from wolongdesc where wonum = '" & wonum & "'"
        dr = wr.GetRdrData(sql)
        While dr.Read
            txtwodesc.Text += dr.Item("longdesc").value
        End While
        dr.Close()

    End Sub
    Private Sub SaveDesc()
        wonum = lblwo.Value
        eqid = lbleqid.Value
        Dim sh, lg As String
        Dim lgcnt As Integer
        sh = txtwodesc.Text
        'lg = txtlongdesc.Text
        sh = Replace(sh, "'", Chr(180), , , vbTextCompare)
        sh = Replace(sh, "--", "-", , , vbTextCompare)
        sh = Replace(sh, ";", ":", , , vbTextCompare)
        sh = Replace(sh, "/", " ", , , vbTextCompare)
        If Len(sh) > 79 Then
            lg = Mid(sh, 80)
            sh = Mid(sh, 1, 79)

        End If

        Dim cmd As New SqlCommand("exec usp_savewodesc @wonum, @sh, @lg")
        Dim param0 = New SqlParameter("@wonum", SqlDbType.Int)
        param0.Value = wonum
        cmd.Parameters.Add(param0)
        Dim param01 = New SqlParameter("@sh", SqlDbType.VarChar)
        If sh = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = sh
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@lg", SqlDbType.Text)
        If lg = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = lg
        End If
        cmd.Parameters.Add(param02)
        Try
            wr.UpdateHack(cmd)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr618" , "wrexpd.aspx.vb")
 
            wr.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        
        Dim cid As String = "0"
        Dim stat As String
        sql = "select wostatus from wostatus where compid = '" & cid & "' and isdefault = 1"
        dr = wr.GetRdrData(sql)
        While dr.Read
            stat = dr.Item("wostatus").ToString
        End While
        dr.Close()
        Dim statdate As DateTime = wr.CNOW
        lblstat.Value = stat
        usr = lbluser.Value
        sql = "update workorder set status = '" & stat & "', statusdate = '" & statdate & "', isdown = 1 " _
        + "where wonum = '" & wonum & "'; " _
        + "insert into wohist (wonum, wostatus, statusdate, changeby) values ('" & wonum & "','" & stat & "', '" & statdate & "', '" & usr & "') " _
        + "insert into eqhist(eqid, repdown, startdown, wonum, ispm) values ('" & eqid & "','" & statdate & "','" & statdate & "','" & wonum & "','0')"
        wr.Update(sql)

        Dim mail As New wrdown
        Dim mcnt As Integer
        mcnt = mail.GetDownMail(wonum)

        lblret.Value = mcnt
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1611.Text = axlabs.GetASPXPage("wrexpd.aspx", "lang1611")
        Catch ex As Exception
        End Try

    End Sub

End Class
