﻿Imports System.Data.SqlClient
Public Class cansendout2
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim sc As New Utilities
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sc.Open()
            ViewExport()
            sc.Dispose()
        Else
            If Request.Form("lblret") = "excel" Then
                lblret.Value = ""
                sc.Open()
                BindExport()
                sc.Dispose()
            End If
            End If
    End Sub
    Private Sub BindExport()
        sql = "select top 600 * from lawson_track order by sTransactionID desc"
        Dim ds As New DataSet
        ds = sc.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
        'lblret.Value = ""
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
    Private Sub ViewExport()
        sql = "select top 600 * from lawson_track order by sTransactionID desc"
        Dim ds As New DataSet
        ds = sc.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgoutvw.DataSource = dv
        dgoutvw.DataBind()
        'lblret.Value = ""
        'cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
End Class