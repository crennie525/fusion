﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmwoswitch.aspx.vb" Inherits="lucy_r12.pmwoswitch" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PM/Work Order Switch Help</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td class="redlabel">YOU ARE IN WORK ORDER MODE<br /></td>
    </tr>
    <tr>
    <td class="plainlabelblue">
    When in Work Order Mode your changes will only be applied to the current PM Work Order.<br /><br />
    </td>
    </tr>
    <tr>
    <td class="redlabel">YOU ARE IN PM MODE<br /></td>
    </tr>
    <tr>
    <td class="plainlabelblue">
    When in PM Mode your changes will only be applied to the Master Settings for labor in the current PM Record.<br /><br />
    Any changes implemented in this mode will be applied to all future PM Work Orders.
    </td>
    </tr>
    </table>
    </div>
    </form>
</body>
</html>
