﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="cansendeout.aspx.vb" Inherits="lucy_r12.cansendeout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
     
    <script language="javascript" type="text/javascript">
        function excel() {
            document.getElementById("lblret").value = "excel"
            document.getElementById("form1").submit();
        }
        function excela() {
            document.getElementById("lblret").value = "excela"
            document.getElementById("form1").submit();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td class="plainlabel"><a href="#" onclick="excel();">Export OpenWorkOrder Sent Data to Excel (Top 50 Descending)</a></td>
    </tr>
    <tr>
    <td class="plainlabel"><a href="#" onclick="excela();">Export TimeEntry Sent Data to Excel (Top 50 Descending)</a></td>
    
    </tr>
    </table>
    <asp:datagrid id="dgout" runat="server" ShowFooter="True"></asp:datagrid>
    <asp:datagrid id="dgouta" runat="server" ShowFooter="True"></asp:datagrid>
    </div>
    <input type="hidden" id="lblret" runat="server" />
    </form>
</body>
</html>
