﻿Imports System.Data.SqlClient
Public Class wrsave
    Inherits System.Web.UI.Page
    Dim wo, wodesc As String
    Dim sql As String
    Dim swo As New Utilities
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            wo = Request.QueryString("wo").ToString
            wodesc = Request.QueryString("wodesc").ToString
            swo.Open()
            If wodesc <> "" Then
                Try
                    SaveDesc(wo, wodesc)
                Catch ex As Exception
                    lblerr.value = "yes"
                End Try

            End If
            swo.Dispose()
        End If
    End Sub
    Private Sub SaveDesc(ByVal wonum As String, ByVal wodesc As String)
        Dim sh, lg As String
        Dim lgcnt As Integer
        sh = wodesc
        sh = Replace(sh, "'", Chr(180), , , vbTextCompare)
        sh = Replace(sh, "--", "-", , , vbTextCompare)
        sh = Replace(sh, ";", ":", , , vbTextCompare)
        sh = Replace(sh, "/", " ", , , vbTextCompare)
        If Len(sh) > 79 Then
            lg = Mid(sh, 80)
            sh = Mid(sh, 1, 79)
        End If
        Dim cmd As New SqlCommand("exec usp_savewodesc @wonum, @sh, @lg")
        Dim param0 = New SqlParameter("@wonum", SqlDbType.Int)
        param0.Value = wonum
        cmd.Parameters.Add(param0)
        Dim param01 = New SqlParameter("@sh", SqlDbType.VarChar)
        If sh = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = sh
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@lg", SqlDbType.Text)
        If lg = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = lg
        End If
        cmd.Parameters.Add(param02)
        Try
            swo.UpdateHack(cmd)
        Catch ex As Exception
            Exit Sub
        End Try
        lblerr.Value = "ok"
    End Sub
End Class