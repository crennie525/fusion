﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="woexpds.aspx.vb" Inherits="lucy_r12.woexpds" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    
    <script language="javascript" type="text/javascript">
        function getedit(who) {
            var wt = document.getElementById("lblwt").value;
            var pmid = document.getElementById("lblpmid").value;
            var pmhid = document.getElementById("lblpmhid").value;
            var eqid = document.getElementById("lbleqid").value;
            var wonum = document.getElementById("lblwo").value;
            var typ = "simp";

            var eReturn = window.showModalDialog("woexpdchartdialog.aspx?typ=" + typ + "&who=" + who + "&wt=" + wt + "&pmid=" + pmid + "&pmhid=" + pmhid + "&eqid=" + eqid + "&wo=" + wonum + "&date=" + Date(), "", "dialogHeight:325px; dialogWidth:725px; resizable=yes");
            if (eReturn) {
            //alert(who)
                if (who == "eq") {
                    document.getElementById("lblsubmit").value = "reload";
                }
                else if (who == "prod") {
                    document.getElementById("lblsubmit").value = "reloadp";
                }
                
                document.getElementById("form1").submit();
            }
        }
        function handlereturn() {
            var isdown = document.getElementById("lblisdown").value;
            var isdownp = document.getElementById("lblisdownp").value;
            var ret = isdown + "," + isdownp;
            //alert(ret)
            window.parent.handlereturn(ret);
        }
        function getcal(fld) {
            var isdown = document.getElementById("lblisdown").value;
            var startdate = document.getElementById("lblstartdown").value;
            var isdownp = document.getElementById("lblisdownp").value;
            var startdatep = document.getElementById("lblstartdownp").value;
            if (fld == "txtstop") {
                if (startdate != "") {
                    var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
                    if (eReturn) {
                        var fldret = fld; //"txt" + 
                        document.getElementById(fldret).value = eReturn;
                    }
                }
                else {
                    alert("Start Date and Time Required")
                }
            }
            else if (fld == "txtstopp") {
                if (startdatep != "") {
                    var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
                    if (eReturn) {
                        var fldret = fld; //"txt" + 
                        document.getElementById(fldret).value = eReturn;
                    }
                }
                else {
                    alert("Start Date and Time Required")
                }
            }
            else {
                var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
                if (eReturn) {
                    var fldret = fld; //"txt" + 
                    document.getElementById(fldret).value = eReturn;
                    /*if (fldret == "txtdate") {
                        document.getElementById("tdmsg").innerHTML = "Start Date and Time Not Saved";
                    }
                    else {
                        document.getElementById("tdmsg2").innerHTML = "Start Date and Time Not Saved";
                    }*/
                }
            }
        }
        function getdown() {
        //alert("getdown")
            document.getElementById("lblsubmit").value = "getdown";
            document.getElementById("form1").submit();
        }
        function getdownp() {
        //alert("getdownp")
            document.getElementById("lblsubmit").value = "getdownp";
            document.getElementById("form1").submit();
        }
        function savestart(who) {
            var chk = document.getElementById("lblhold").value;
            if (chk != "0") {
                var startdate = document.getElementById("txtdate").value;
                var starthr = document.getElementById("ddhrsstart").value;
                if (startdate == "") {
                    alert("Start Date Required")
                }
                else if (starthr == "NA") {
                    alert("Start Time Hour Required")
                }
            }
            else {
                if (who == "1") {
                    document.getElementById("lblsubmit").value = "savestart";
                }
                else {
                    document.getElementById("lblsubmit").value = "savestartboth";
                }
                document.getElementById("form1").submit();
            }
        }
        function savestartp(who) {

            var chk = document.getElementById("lblhold").value;
            //alert(chk)

            if (chk != "0") {
                var startdate = document.getElementById("txtdatep").value;
                var starthr = document.getElementById("ddhrsstartp").value;
                if (startdate == "") {
                    alert("Production Start Date Required")
                }
                else if (starthr == "NA") {
                    alert("Production Start Time Hour Required")
                }
            }
            else {
                //alert(who)
                if (who == "1") {
                    document.getElementById("lblsubmit").value = "savestartp";
                }
                else {

                    document.getElementById("lblsubmit").value = "savestartpboth";
                }
                document.getElementById("form1").submit();
            }
        }


        function savestop(who) {
            var chk = document.getElementById("lblhold").value;
            if (chk != "1") {
                //var startdate = document.getElementById("txtdate").value;
                var startdate = document.getElementById("lblstartdown").value;
                if (startdate == "") {
                    alert("Start Date Required")
                    document.getElementById("txttote").value = "";
                }
                else {
                    var stopdate = document.getElementById("txtstop").value;
                    var tote = document.getElementById("txttote").value;
                    //var stophr = document.getElementById("ddhrsstop").value;
                    //else if (stopdate == "") {
                    //    alert("Stop Date Required")
                    //}
                    if (isNaN(parseInt(tote))) {
                        alert("Total Down Is Not a Number")
                    }
                    else if (tote == "") {
                        alert("Equipment Total Down Required")
                    }
                    //else if (stophr == "NA") {
                    //alert("Stop Time Hour Required")
                    //}
                    else {
                        if (who == "1") {
                            document.getElementById("lblsubmit").value = "savestop";
                        }
                        else {
                            document.getElementById("lblsubmit").value = "savestopboth";
                        }
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        function savestopp(who) {
            var chk = document.getElementById("lblhold").value;
            //alert(chk)
            if (chk != "1") {
                //var startdate = document.getElementById("txtdatep").value;
                var startdate = document.getElementById("lblstartdownp").value;
                if (startdate == "") {
                    alert("Production Start Date Required")
                    document.getElementById("txttotp").value = "";
                }
                else {
                    var stopdate = document.getElementById("txtstopp").value;
                    var totp = document.getElementById("txttotp").value;
                    //var stophr = document.getElementById("ddhrsstopp").value;
                    //else if (stopdate == "") {
                    //    alert("Production Stop Date Required")
                    //}

                    if (isNaN(parseInt(totp))) {
                        alert("Total Down Is Not a Number")
                    }
                    else if (totp == "") {
                        alert("Production Total Down Required")
                    }
                    //else if (stophr == "NA") {
                    //    alert("Production Stop Time Hour Required")
                    //}
                    else {
                        if (who == "1") {
                            document.getElementById("lblsubmit").value = "savestopp";
                        }
                        else {
                            document.getElementById("lblsubmit").value = "savestoppboth";
                        }
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
    </script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td class="bigbold1" id="tdptop" runat="server">
                Production Downtime
            </td>
            <td>
                &nbsp;
            </td>
            <td class="bigbold1">
                Equipment Downtime
            </td>
        </tr>
        <tr>
            <td id="tdpbot" runat="server">
                <table>
                    <tr class="details">
                        <td class="bluelabel" height="24">
                            <asp:Label ID="Label1" runat="server">Date\Time Reported</asp:Label>
                        </td>
                        <td id="tdrepdatep" class="plainlabel" colspan="3" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <hr style="border-bottom: #0000ff 2px solid; border-left: #0000ff 2px solid; border-top: #0000ff 2px solid;
                                border-right: #0000ff 2px solid">
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel">
                            <asp:Label ID="Label2" runat="server">Start Date</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtdatep" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcal('txtdatep');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                width="19" height="19">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel">
                            <asp:Label ID="Label3" runat="server">Start Time</asp:Label>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddhrsstartp" runat="server" CssClass="plainlabel">
                                            <asp:ListItem Value="NA">NA</asp:ListItem>
                                            <asp:ListItem Value="01">01</asp:ListItem>
                                            <asp:ListItem Value="02">02</asp:ListItem>
                                            <asp:ListItem Value="03">03</asp:ListItem>
                                            <asp:ListItem Value="04">04</asp:ListItem>
                                            <asp:ListItem Value="05">05</asp:ListItem>
                                            <asp:ListItem Value="06">06</asp:ListItem>
                                            <asp:ListItem Value="07">07</asp:ListItem>
                                            <asp:ListItem Value="08">08</asp:ListItem>
                                            <asp:ListItem Value="09">09</asp:ListItem>
                                            <asp:ListItem Value="10">10</asp:ListItem>
                                            <asp:ListItem Value="11">11</asp:ListItem>
                                            <asp:ListItem Value="12">12</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="bluelabel">
                                        :
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddminsstartp" runat="server" CssClass="plainlabel">
                                            <asp:ListItem Value="00">00</asp:ListItem>
                                            <asp:ListItem Value="01">01</asp:ListItem>
                                            <asp:ListItem Value="02">02</asp:ListItem>
                                            <asp:ListItem Value="03">03</asp:ListItem>
                                            <asp:ListItem Value="04">04</asp:ListItem>
                                            <asp:ListItem Value="05">05</asp:ListItem>
                                            <asp:ListItem Value="06">06</asp:ListItem>
                                            <asp:ListItem Value="07">07</asp:ListItem>
                                            <asp:ListItem Value="08">08</asp:ListItem>
                                            <asp:ListItem Value="09">09</asp:ListItem>
                                            <asp:ListItem Value="10">10</asp:ListItem>
                                            <asp:ListItem Value="11">11</asp:ListItem>
                                            <asp:ListItem Value="12">12</asp:ListItem>
                                            <asp:ListItem Value="13">13</asp:ListItem>
                                            <asp:ListItem Value="14">14</asp:ListItem>
                                            <asp:ListItem Value="15">15</asp:ListItem>
                                            <asp:ListItem Value="16">16</asp:ListItem>
                                            <asp:ListItem Value="17">17</asp:ListItem>
                                            <asp:ListItem Value="18">18</asp:ListItem>
                                            <asp:ListItem Value="19">19</asp:ListItem>
                                            <asp:ListItem Value="20">20</asp:ListItem>
                                            <asp:ListItem Value="21">21</asp:ListItem>
                                            <asp:ListItem Value="22">22</asp:ListItem>
                                            <asp:ListItem Value="23">23</asp:ListItem>
                                            <asp:ListItem Value="24">24</asp:ListItem>
                                            <asp:ListItem Value="25">25</asp:ListItem>
                                            <asp:ListItem Value="26">26</asp:ListItem>
                                            <asp:ListItem Value="27">27</asp:ListItem>
                                            <asp:ListItem Value="28">28</asp:ListItem>
                                            <asp:ListItem Value="29">29</asp:ListItem>
                                            <asp:ListItem Value="30">30</asp:ListItem>
                                            <asp:ListItem Value="31">31</asp:ListItem>
                                            <asp:ListItem Value="32">32</asp:ListItem>
                                            <asp:ListItem Value="33">33</asp:ListItem>
                                            <asp:ListItem Value="34">34</asp:ListItem>
                                            <asp:ListItem Value="35">35</asp:ListItem>
                                            <asp:ListItem Value="36">36</asp:ListItem>
                                            <asp:ListItem Value="37">37</asp:ListItem>
                                            <asp:ListItem Value="38">38</asp:ListItem>
                                            <asp:ListItem Value="39">39</asp:ListItem>
                                            <asp:ListItem Value="40">40</asp:ListItem>
                                            <asp:ListItem Value="41">41</asp:ListItem>
                                            <asp:ListItem Value="42">42</asp:ListItem>
                                            <asp:ListItem Value="43">43</asp:ListItem>
                                            <asp:ListItem Value="44">44</asp:ListItem>
                                            <asp:ListItem Value="45">45</asp:ListItem>
                                            <asp:ListItem Value="46">46</asp:ListItem>
                                            <asp:ListItem Value="47">47</asp:ListItem>
                                            <asp:ListItem Value="48">48</asp:ListItem>
                                            <asp:ListItem Value="49">49</asp:ListItem>
                                            <asp:ListItem Value="50">50</asp:ListItem>
                                            <asp:ListItem Value="51">51</asp:ListItem>
                                            <asp:ListItem Value="52">52</asp:ListItem>
                                            <asp:ListItem Value="53">53</asp:ListItem>
                                            <asp:ListItem Value="54">54</asp:ListItem>
                                            <asp:ListItem Value="55">55</asp:ListItem>
                                            <asp:ListItem Value="56">56</asp:ListItem>
                                            <asp:ListItem Value="57">57</asp:ListItem>
                                            <asp:ListItem Value="58">58</asp:ListItem>
                                            <asp:ListItem Value="59">59</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddapsstartp" runat="server" CssClass="plainlabel">
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <img onclick="savestartp('1');" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                alt="" title="Save Production Down Time Start"
                                 />
                        </td>
                        <td>
                            <img class="details" onclick="savestartp('2');" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                alt="" title="Save Production Down Time Start and use value for Equipment Down Time Start"
                                 />
                        </td>
                    </tr>
                    <tr id="trmsg2" runat="server">
                        <td id="tdmsg2" class="plainlabelred" colspan="4" align="center" runat="server">
                            
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <hr style="border-bottom: #0000ff 2px solid; border-left: #0000ff 2px solid; border-top: #0000ff 2px solid;
                                border-right: #0000ff 2px solid">
                        </td>
                    </tr>
                    <tr class="details">
                        <td class="bluelabel">
                            <asp:Label ID="Label5" runat="server">Stop Date</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtstopp" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcal('txtstopp');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                width="19" height="19">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="plainlabel" align="center">
                            Minutes&nbsp;&nbsp;<input type="radio" name="rdmhe" id="rdmhpm" checked="true" runat="server" />&nbsp;&nbsp;
                            Hours&nbsp;&nbsp;<input type="radio" name="rdmhe" id="rdmhph" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel">
                            <asp:Label ID="Label6" runat="server">Total Down</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txttotp" runat="server" Width="64px"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="savestopp('1');" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                alt="" title="Save Production Down Time Stop"
                                 />
                        </td>
                        <td>
                            <img class="details" onclick="savestopp('2');" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                title="Save Production Down Time Stop and use value for Equipment Down Time Stop"
                                 />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="plainlabelblue" id="tdprodadd" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="plainlabelblue" id="tdprodedit" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="plainlabel" id="tdprodtot" runat="server">
                        </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
            <td>
                <table>
                    <tr class="details">
                        <td class="bluelabel" height="24">
                            <asp:Label ID="lang1379" runat="server">Date\Time Reported</asp:Label>
                        </td>
                        <td id="tdrepdate" class="plainlabel" colspan="3" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <hr style="border-bottom: #0000ff 2px solid; border-left: #0000ff 2px solid; border-top: #0000ff 2px solid;
                                border-right: #0000ff 2px solid">
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel">
                            <asp:Label ID="lang1380" runat="server">Start Date</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtdate" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcal('txtdate');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                width="19" height="19">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel">
                            <asp:Label ID="lang1381" runat="server">Start Time</asp:Label>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddhrsstart" runat="server" CssClass="plainlabel">
                                            <asp:ListItem Value="NA">NA</asp:ListItem>
                                            <asp:ListItem Value="01">01</asp:ListItem>
                                            <asp:ListItem Value="02">02</asp:ListItem>
                                            <asp:ListItem Value="03">03</asp:ListItem>
                                            <asp:ListItem Value="04">04</asp:ListItem>
                                            <asp:ListItem Value="05">05</asp:ListItem>
                                            <asp:ListItem Value="06">06</asp:ListItem>
                                            <asp:ListItem Value="07">07</asp:ListItem>
                                            <asp:ListItem Value="08">08</asp:ListItem>
                                            <asp:ListItem Value="09">09</asp:ListItem>
                                            <asp:ListItem Value="10">10</asp:ListItem>
                                            <asp:ListItem Value="11">11</asp:ListItem>
                                            <asp:ListItem Value="12">12</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="bluelabel">
                                        :
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddminsstart" runat="server" CssClass="plainlabel">
                                            <asp:ListItem Value="00">00</asp:ListItem>
                                            <asp:ListItem Value="01">01</asp:ListItem>
                                            <asp:ListItem Value="02">02</asp:ListItem>
                                            <asp:ListItem Value="03">03</asp:ListItem>
                                            <asp:ListItem Value="04">04</asp:ListItem>
                                            <asp:ListItem Value="05">05</asp:ListItem>
                                            <asp:ListItem Value="06">06</asp:ListItem>
                                            <asp:ListItem Value="07">07</asp:ListItem>
                                            <asp:ListItem Value="08">08</asp:ListItem>
                                            <asp:ListItem Value="09">09</asp:ListItem>
                                            <asp:ListItem Value="10">10</asp:ListItem>
                                            <asp:ListItem Value="11">11</asp:ListItem>
                                            <asp:ListItem Value="12">12</asp:ListItem>
                                            <asp:ListItem Value="13">13</asp:ListItem>
                                            <asp:ListItem Value="14">14</asp:ListItem>
                                            <asp:ListItem Value="15">15</asp:ListItem>
                                            <asp:ListItem Value="16">16</asp:ListItem>
                                            <asp:ListItem Value="17">17</asp:ListItem>
                                            <asp:ListItem Value="18">18</asp:ListItem>
                                            <asp:ListItem Value="19">19</asp:ListItem>
                                            <asp:ListItem Value="20">20</asp:ListItem>
                                            <asp:ListItem Value="21">21</asp:ListItem>
                                            <asp:ListItem Value="22">22</asp:ListItem>
                                            <asp:ListItem Value="23">23</asp:ListItem>
                                            <asp:ListItem Value="24">24</asp:ListItem>
                                            <asp:ListItem Value="25">25</asp:ListItem>
                                            <asp:ListItem Value="26">26</asp:ListItem>
                                            <asp:ListItem Value="27">27</asp:ListItem>
                                            <asp:ListItem Value="28">28</asp:ListItem>
                                            <asp:ListItem Value="29">29</asp:ListItem>
                                            <asp:ListItem Value="30">30</asp:ListItem>
                                            <asp:ListItem Value="31">31</asp:ListItem>
                                            <asp:ListItem Value="32">32</asp:ListItem>
                                            <asp:ListItem Value="33">33</asp:ListItem>
                                            <asp:ListItem Value="34">34</asp:ListItem>
                                            <asp:ListItem Value="35">35</asp:ListItem>
                                            <asp:ListItem Value="36">36</asp:ListItem>
                                            <asp:ListItem Value="37">37</asp:ListItem>
                                            <asp:ListItem Value="38">38</asp:ListItem>
                                            <asp:ListItem Value="39">39</asp:ListItem>
                                            <asp:ListItem Value="40">40</asp:ListItem>
                                            <asp:ListItem Value="41">41</asp:ListItem>
                                            <asp:ListItem Value="42">42</asp:ListItem>
                                            <asp:ListItem Value="43">43</asp:ListItem>
                                            <asp:ListItem Value="44">44</asp:ListItem>
                                            <asp:ListItem Value="45">45</asp:ListItem>
                                            <asp:ListItem Value="46">46</asp:ListItem>
                                            <asp:ListItem Value="47">47</asp:ListItem>
                                            <asp:ListItem Value="48">48</asp:ListItem>
                                            <asp:ListItem Value="49">49</asp:ListItem>
                                            <asp:ListItem Value="50">50</asp:ListItem>
                                            <asp:ListItem Value="51">51</asp:ListItem>
                                            <asp:ListItem Value="52">52</asp:ListItem>
                                            <asp:ListItem Value="53">53</asp:ListItem>
                                            <asp:ListItem Value="54">54</asp:ListItem>
                                            <asp:ListItem Value="55">55</asp:ListItem>
                                            <asp:ListItem Value="56">56</asp:ListItem>
                                            <asp:ListItem Value="57">57</asp:ListItem>
                                            <asp:ListItem Value="58">58</asp:ListItem>
                                            <asp:ListItem Value="59">59</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddapsstart" runat="server" CssClass="plainlabel">
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <img onclick="savestart('1');" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                alt="" title="Save Equipment Down Time Start"
                                 />
                        </td>
                        <td>
                            <img class="details" onclick="savestart('2');" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                title="Save Equipment Down Time Start and use value for Production Down Time Start"
                                 />
                        </td>
                    </tr>
                    <tr id="trmsg" runat="server">
                        <td id="tdmsg" class="plainlabelred" colspan="4" align="center" runat="server">
                            
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <hr style="border-bottom: #0000ff 2px solid; border-left: #0000ff 2px solid; border-top: #0000ff 2px solid;
                                border-right: #0000ff 2px solid">
                        </td>
                    </tr>
                    <tr class="details">
                        <td class="bluelabel">
                            <asp:Label ID="lang1383" runat="server">Stop Date</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtstop" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcal('txtstop');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                width="19" height="19">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="plainlabel" align="center">
                            Minutes&nbsp;&nbsp;<input type="radio" name="rdmhp" id="rdmhem" checked="true" runat="server" />&nbsp;&nbsp;
                            Hours&nbsp;&nbsp;<input type="radio" name="rdmhp" id="rdmheh" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel">
                            <asp:Label ID="lang1384" runat="server">Total Down</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txttote" runat="server" Width="64px"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="savestop('1');" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                alt="" title="Save Equipment Down Time Stop"
                                 />
                        </td>
                        <td>
                            <img class="details" onclick="savestop('2');" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                title="Save Equipment Down Time Stop and use value for Production Down Time Stop"
                                 />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="plainlabelblue" id="tdeqadd" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="plainlabelblue" id="tdeqedit" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="plainlabel" id="tdeqtot" runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="right">
                <img onclick="handlereturn();" id="bgbreturn" runat="server" src="../images/appbuttons/bgbuttons/return.gif">
            </td>
        </tr>
    </table>
    <input id="lbluser" type="hidden" runat="server" />
    <input id="lblwo" type="hidden" runat="server" />
    <input id="lbleqid" type="hidden" runat="server" />
    <input id="lblsubmit" type="hidden" runat="server" />
    <input id="lblrepdown" type="hidden" runat="server" />
    <input id="lblstartdown" type="hidden" name="lblstartdown" runat="server" />
    <input id="lblstopdown" type="hidden" name="lblstopdown" runat="server" />
    <input id="lblisdown" type="hidden" runat="server" />
    <input id="lblstatus" type="hidden" runat="server" />
    <input type="hidden" id="lbladmin" runat="server" />
    <input type="hidden" id="lblhold" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    <input id="lblstartdownp" type="hidden" runat="server" />
    <input id="lblstopdownp" type="hidden" runat="server" />
    <input id="lblisdownp" type="hidden" runat="server" />
    <input id="lblwt" type="hidden" runat="server" />
    <input id="lblcoi" type="hidden" runat="server" />
    <input type="hidden" id="lblpmid" runat="server" />
    <input type="hidden" id="lblpmhid" runat="server" />
    <input type="hidden" id="lbldalert" runat="server" />
    <input type="hidden" id="lbldpalert" runat="server" />
    <input type="hidden" id="lbldowncnt" runat="server" />
    <input type="hidden" id="lbleqhid" runat="server" />
    <input type="hidden" id="lbleqhidp" runat="server" />
    </form>
</body>
</html>
