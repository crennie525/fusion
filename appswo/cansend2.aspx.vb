﻿Imports System.Data.SqlClient
Public Class cansend2
    Inherits System.Web.UI.Page
    Dim popstring As String
    Dim errcnt As Integer
    Dim sql As String
    Dim te As New Utilities

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            popstring = Request.QueryString("pop").ToString
            errcnt = Request.QueryString("err").ToString
            lblpopstring.Value = popstring
            lblerrcnt.Value = errcnt
            'empcnt + "~" + username + "~" + wonum + "~" + MWNO + "~" + OPNO + "~" + RPDT + "~" + UMAT + "~" + UMAS + "~" + FCLA + "~" + FCL2 + "~" + FCL3 + "~" _
            Dim poparr() As String = popstring.Split(",")
            Dim poparr1() As String = poparr(0).Split("~")
            Dim emp As String = poparr1(1)
            tdnum.InnerHtml = errcnt
            tdemp.InnerHtml = emp
            trnext.Attributes.Add("class", "details")
            trsub.Attributes.Add("class", "view")
            lblcurremp.Value = "1"
        Else
            If Request.Form("lblret") = "sendemp" Then

            ElseIf Request.Form("lblret") = "getnext" Then
                getnext()
            End If
            lblret.Value = ""
        End If
    End Sub
    Public Structure TimeEntryInputVar1
        Public sOpNum As String
        Public sWorkDate As String
        Public sUserId As String
        Public sEmpNumbr As String
        Public sLabrHrs As String
        Public sCostTyp As String
        Public sCloseOperat As String
        Public sLabrSetupTim As String
        Public sFailurCls As String
        Public sErrCde2 As String
        Public sErrCde3 As String
        Public sDownTim As String
        Public sDelyTim1 As String
        Public sDelyTim2 As String

    End Structure
    Private Sub sendemp()
        Dim curremp As Integer = lblcurremp.Value
        popstring = lblpopstring.Value
        Dim poparr() As String = popstring.Split(",")
        Dim poparr1() As String = poparr(curremp - 1).Split("~")
        'empcnt + "~" + username + "~" + laborid + "~" + wonum + "~" + DOWT + "~" + MWNO + "~" + OPNO + "~" + RPDT + "~" + UMAT + "~" + UMAS + "~" + FCLA + "~" + FCL2 + "~" + FCL3 + "~" _
        Dim CONO, MWNO, OPNO, RPDT, RPRE, EMNO, UMAT, PCTP, REND, UMAS, FCLA, FCL2, FCL3, DOWT, DLY1, DLY2, wonum, laborid As String
        CONO = "1"
        wonum = poparr1(3)
        DOWT = poparr1(4)
        MWNO = poparr1(5)
        OPNO = poparr1(6)
        RPDT = poparr1(7)
        UMAT = poparr1(8)
        UMAS = poparr1(9)
        FCLA = poparr1(10)
        FCL2 = poparr1(11)
        FCL3 = poparr1(12)
        PCTP = "3"
        REND = ""
        DLY1 = "0.00"
        DLY2 = "0.00"
        EMNO = txtnum.Text
        RPRE = EMNO
        Dim cansend As New Service2
        cansend.Url = System.Configuration.ConfigurationManager.AppSettings("service2")
        Dim retstring As String
        Dim ds As New DataSet
        Dim ter() As TimeEntryInputVar
        ds = cansend.TimeEntry(MWNO, ter)
        tdgo.InnerHtml += EMNO & " - " & retstring
        Dim retval, errmsg As String
        Dim errcnt As Integer = 0
        Try
            Dim retarr() As String = retstring.Split(",")
            Dim valarr() As String = retarr(0).Split(":")
            retval = valarr(1)
            Dim msgarr() As String = retarr(1).Split(":")
            errmsg = msgarr(1)
            If errmsg = "1" Then
                errcnt += 1
                sql = "insert into woout_te (fuswonum, sdate, retwonum, retval, errmsg) " _
                + "values ('" & wonum & "',getdate(),'" & MWNO & "','" & retval & "','" & errmsg & "')"
                te.Update(sql)
                'Dim strMessage As String = "The Return Value contained an Error Message - Please Stop and Contact LAI"
                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Else
                'sql = "select sum(isnull(w.minutes_man,0)) as minutes_man, w.laborid, u.username, u.empno from wolabtrans w left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "'"
                sql = "update wolabtrans set retwonum = '" & MWNO & "' where wonum = '" & wonum & "' and laborid = '" & laborid & "'"
                te.Update(sql)
            End If
        Catch ex As Exception
            Try
                sql = "update wolabtrans set retwonum = '" & MWNO & "' where wonum = '" & wonum & "' and laborid = '" & laborid & "'"
                te.Update(sql)
            Catch ex0 As Exception

            End Try
            Try
                sql = "insert into woout_te (fuswonum, sdate, errovr) " _
                    + "values ('" & wonum & "',getdate(),'" & retstring & "')"
                te.Update(sql)
            Catch ex1 As Exception

            End Try
            Dim strMessage As String = "Problem with Return Value - Please Stop and Contact LAI"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
        If errcnt <> 0 Then
            curremp -= 1
            lblcurremp.Value = curremp
            If curremp > 0 Then
                trnext.Attributes.Add("class", "details")
                trsub.Attributes.Add("class", "view")
            Else

            End If
        Else
            'trnext.Attributes.Add("class", "details")
            'trsub.Attributes.Add("class", "view")
        End If
    End Sub
    Private Sub getnext()
        Dim curremp As Integer = lblcurremp.Value
        popstring = lblpopstring.Value
        Dim poparr() As String = popstring.Split(",")
        Dim poparr1() As String = poparr(curremp - 1).Split("~")
        Dim emp As String = poparr1(1)
        tdnum.InnerHtml = curremp
        tdemp.InnerHtml = emp
        trnext.Attributes.Add("class", "details")
        trsub.Attributes.Add("class", "view")
    End Sub
End Class