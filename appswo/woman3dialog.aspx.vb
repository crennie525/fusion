﻿Public Class woman3dialog
    Inherits System.Web.UI.Page
    Dim appstr, usrname, sid, wonum As String
    Dim uid, islabor, issuper, Logged_In, ro, ms, href, isplanner, who, fs As String
    Protected WithEvents tdwon As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwtl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tddescl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lang1548a As Global.System.Web.UI.WebControls.Label
    Protected WithEvents lang1549 As Global.System.Web.UI.WebControls.Label

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        

        If Not IsPostBack Then
            fs = Request.QueryString("fs").ToString

            uid = Request.QueryString("uid").ToString
            lbluid.Value = uid
            islabor = Request.QueryString("islabor").ToString
            lblislabor.Value = islabor
            issuper = Request.QueryString("issuper").ToString
            lblissuper.Value = issuper
            Logged_In = Request.QueryString("Logged_In").ToString
            lblLogged_In.Value = Logged_In
            ro = Request.QueryString("ro").ToString
            lblro.Value = ro
            ms = Request.QueryString("ms").ToString
            lblms.Value = ms
            isplanner = Request.QueryString("isplanner").ToString
            lblisplanner.Value = isplanner

            appstr = Request.QueryString("appstr").ToString
            lblappstr.Value = appstr
            usrname = Request.QueryString("usrname").ToString
            lblusername.Value = usrname
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            wonum = Request.QueryString("wo").ToString
            lblwo.Value = wonum
            who = Request.QueryString("who").ToString
            If fs = "fs" Then
                lblhref.Value = "woman3dialog.aspx?fs=fs&who=" + who + "&sid=" + sid + "&wo=" + wonum + "&uid=" _
            + "" + uid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr
                iftd.Attributes.Add("src", "womanfsns.aspx?who=" + who + "&appstr=" + appstr + "&usrname=" + usrname + "&sid=" + sid + "&wo=" + wonum)
            Else
                lblhref.Value = "woman3dialog.aspx?fs=&who=" + who + "&sid=" + sid + "&wo=" + wonum + "&uid=" _
            + "" + uid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr
                iftd.Attributes.Add("src", "woman3.aspx?who=" + who + "&appstr=" + appstr + "&usrname=" + usrname + "&sid=" + sid + "&wo=" + wonum)
            End If


            '

        End If
    End Sub
    
End Class