

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class woprint
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim won As New Utilities
    Dim wonum, pmnum, wotype, jpnum, psite, routeid, prichk, priid, pri As String
    Protected WithEvents lblwotype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpsite As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdwi As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        lblcomi.value = comi
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            wonum = Request.QueryString("wo").ToString
            Try
                wotype = Request.QueryString("typ").ToString
                lblwotype.Value = wotype
            Catch ex As Exception
                'use for testing
                wotype = "JP"
                lblwotype.Value = wotype
            End Try
            won.Open()
            checkpri()
            If wotype = "PM" Then
                pmnum = Request.QueryString("pm").ToString
                PopPMReport(wonum, pmnum)
            ElseIf wotype = "PMMAX" Or wotype = "MAXPM" Then
                pmnum = Request.QueryString("pm").ToString
                PopPMReportMax(wonum, pmnum)
            ElseIf wotype = "TPM" Then
                pmnum = Request.QueryString("pm").ToString
                PopTPMReport(wonum, pmnum)
            ElseIf wotype = "JP" Then
                jpnum = Request.QueryString("jp").ToString
                PopTaskReport(wonum, jpnum)
            ElseIf wotype = "RTE" Then
                routeid = Request.QueryString("routeid").ToString
                PopMRouteReport(wonum, routeid)
            Else
                PopWOReport(wonum)
            End If
            won.Dispose()
        End If
    End Sub
    Private Sub checkpri()
        sql = "select [default] from wovars where [column] = 'wopriority'"
        Try
            prichk = won.strScalar(sql)
        Catch ex As Exception
            prichk = "no"
        End Try
    End Sub
    Private Function getpri(ByVal priid As String) As String
        Dim ret As String
        Dim wo As New Utilities
        sql = "select (cast(wopri as varchar(10)) + ' - ' + wopridesc) as wopri from wopriority where woprid = '" & priid & "'"
        Try
            wo.Open()
            Try
                ret = wo.strScalar(sql)
            Catch ex As Exception
                ret = "~oops~"
            End Try
            wo.Dispose()
        Catch ex As Exception
            ret = "~oops~"
        End Try
        
        Return ret
    End Function
    Private Sub PopMRouteReport(ByVal wo As String, ByVal routeid As String)

    End Sub
    Private Sub PopTPMReport(ByVal wo As String, ByVal pm As String)
        Dim comi As String = lblcomi.Value
        Dim flag As Integer = 0
        Dim subcnt As Integer = 0
        Dim subcnth As Integer = 0
        sql = "select w.siteid, s.sitename from workorder w left join sites s on s.siteid = w.siteid where w.wonum = '" & wo & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            psite = dr.Item("sitename").ToString
        End While
        dr.Close()
        Dim func, funcchk, task, tasknum, subtask As String
        Dim sb As New System.Text.StringBuilder

        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
        sb.Append("<tr><td width=""120""></td><td width=""160""></td><td width=""130""></td><td width=""170""></td><td width=""90""></td></tr>")

        sql = "usp_getwoprint '" & wo & "'"

        dr = won.GetRdrData(sql)
        Dim headhold As String = "0"
        While dr.Read
            Dim pms As String = dr.Item("pm").ToString
            sb.Append("<tr height=""30"" valign=""top""><td class=""bigbold1"" colspan=""5"" align=""center"">" & psite & " TPM Work Request</td></tr>")
            sb.Append("<tr height=""20"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center"">" & Now & "</td></tr>")
            sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""5"" align=""center"">" & pms & "</td></tr>")
            sb.Append("<tr height=""20"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center""><b>Work Order#</b>&nbsp;&nbsp;" & wo & "</td></tr>")
            'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
            sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl228", "woprint.aspx.vb") & "</td></tr>")
            Dim start As String = dr.Item("schedstart").ToString
            If start = "" Then
                start = dr.Item("targstartdate").ToString
                If start = "" Then
                    start = Now
                End If
            End If
            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl229", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & start & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl230", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("actfinish").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl231", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("reportedby").ToString & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl232", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("reportdate").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl297", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("status").ToString & "</td>")
            sb.Append("<td class=""label"">Work Type</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("worktype").ToString & "</td></tr>")

            pri = dr.Item("wopriority").ToString
            priid = dr.Item("wopri").ToString

            If prichk = "yes" Then

                If (pri <> "" And pri <> "0") And (priid = "" Or priid = "0") Then
                    If pri <> "" And pri <> "0" Then
                        pri = pri
                    Else
                        pri = ""
                    End If
                Else
                    If priid <> "" And priid <> "0" Then
                        pri = getpri(priid)
                        If pri = "~oops~" Then
                            pri = ""
                        End If
                    End If
                End If
            ElseIf prichk = "no" Then
                If priid <> "" And priid <> "0" Then
                    pri = getpri(priid)
                    If pri = "~oops~" Then
                        pri = ""
                    End If
                Else
                    If pri <> "" And pri <> "0" Then
                        pri = pri
                    Else
                        pri = ""
                    End If
                End If
            Else
                If pri <> "" And pri <> "0" Then
                    pri = pri
                Else
                    pri = ""
                End If
            End If

            sb.Append("<tr><td class=""label"">WO Priority</td><td class=""plainlabel"">" & pri & "</td><td class=""label"">" & tmod.getlbl("cdlbl298", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("chargenum").ToString & "</td></tr>")

            'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")

            sb.Append("<tr height=""30""><td class=""label"">" & tmod.getlbl("cdlbl235", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("supervisor").ToString & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl236", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("leadcraft").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl237", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""4"">" & dr.Item("description").ToString & dr.Item("longdesc").ToString & "</td></tr>")

            sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
            sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl238", "woprint.aspx.vb") & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl239", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("dept_line").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("dept_desc").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl240", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("cell_name").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("cell_desc").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl241", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("ldesc").ToString & "</td></tr>")

            If dr.Item("eqnum").ToString <> "" Then
                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl242", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("eqnum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("eqdesc").ToString & "</td></tr>")
            End If
            If dr.Item("fpc").ToString <> "" Then
                sb.Append("<tr height=""20""><td class=""label"">Column</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("fpc").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3""></td></tr>")
            End If
        End While
        dr.Close()
        'sb.Append("<tr><td>&nbsp;</td></tr>")

        sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
        sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""640""></td></tr>")
        sql = "usp_getpmmantpm '" & pm & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            If headhold = "0" Then
                headhold = "1"
                pm = dr.Item("pm").ToString
                sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl243", "woprint.aspx.vb") & "</td></tr>")

            End If

            func = dr.Item("func").ToString
            If funcchk <> func Then 'flag = 0 Then
                If flag = 0 Then
                    flag = 1
                Else
                    'sb.Append("<tr><td>&nbsp;</td></tr>")
                End If

                funcchk = func
                sb.Append("<tr height=""20""><td class=""label"" colspan=""3""><u>Function: " & func & "</u></td>")
                'sb.Append("<td class=""plainlabel"" colspan=""2"">" & func & "</td></tr>")
                'sb.Append("<tr><td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl244" , "woprint.aspx.vb") & "</td>")
                'sb.Append("<td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl245" , "woprint.aspx.vb") & "</td>")
                'sb.Append("<td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl246" , "woprint.aspx.vb") & "</td></tr>")
            End If
            task = dr.Item("task").ToString
            tasknum = dr.Item("tasknum").ToString
            subtask = dr.Item("subtask").ToString
            subcnt = dr.Item("subcnt").ToString


            If subtask = "0" Then
                'sb.Append("<tr height=""20""><td></td><td class=""label"">" & subcnt & "</td></tr>")
                sb.Append("<tr><td class=""plainlabel"">[" & tasknum & "]</td>")
                If Len(dr.Item("task").ToString) > 0 Then
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Task</b> :" & task & "</td></tr>")
                Else
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Task:</b> No Task Description Provided</td></tr>")
                End If
                'sb.Append("<tr><td>&nbsp;</td></tr>")
                sb.Append("<tr height=""20""><td></td>")
                sb.Append("<td colspan=""2"" class=""plainlabel"">Check: OK(___) " & dr.Item("fm1").ToString & "</td></tr>")
                If dr.Item("lube") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Lubes: " & dr.Item("lube").ToString & "</td></tr>")
                End If
                If dr.Item("tool") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Tools: " & dr.Item("tool").ToString & "</td></tr>")
                End If
                If dr.Item("part") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Parts: " & dr.Item("part").ToString & "</td></tr>")
                End If
                If dr.Item("meas") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Measurements: " & dr.Item("meas").ToString & "</td></tr>")
                End If
                If subcnt = 0 Then
                    sb.Append("<tr><td>&nbsp;</td></tr>")
                Else
                    If subcnt <> 0 Then

                        sb.Append("<tr height=""20""><td></td><td class=""label"" colspan=""2""><u>Sub Tasks</u></td></tr>")
                    End If

                End If

            Else
                'sb.Append("<tr><td>" & subcnt & "</td></tr>")
                sb.Append("<tr height=""20""><td></td><td class=""plainlabel"">[" & subtask & "]</td>")
                If Len(dr.Item("subt").ToString) > 0 Then
                    sb.Append("<td class=""plainlabel""><b>Task</b>: " & dr.Item("subt").ToString & "</td></tr>")
                Else
                    sb.Append("<td class=""plainlabel""><b>Task</b>: No Sub Task Description Provided</td></tr>")
                End If
                sb.Append("<tr><td></td><td></td>")
                sb.Append("<td class=""plainlabel"">Check: OK(___)</td></tr>")
                sb.Append("<tr><td>&nbsp;</td></tr>")
            End If
        End While
        dr.Close()
        If comi <> "SW" Then
            sb.Append("<tr><td colspan=""5"" class=""plainlabel"">Work Performed by:  ________________________   Date: __________</td></tr>")
            sb.Append("<tr><td colspan=""5"">")
            sb.Append("<table border=""0"">")
            sb.Append("<tr><td class=""plainlabel"">Time&nbsp;(min)</td>")
            sb.Append("<td height=""24"" width=""100"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">REG</td>")
            sb.Append("<td width=""100"" align=""center"" class=""plainlabel"">OT</td></tr>")
            sb.Append("<tr><td class=""plainlabel"">Actual:</td>")
            sb.Append("<td height=""24"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">________</td>")
            sb.Append("<td align=""center"" class=""plainlabel"">________</td></tr>")
            sb.Append("<tr><td class=""plainlabel"">Other:</td>")
            sb.Append("<td height=""24"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">________</td>")
            sb.Append("<td align=""center"" class=""plainlabel"">________</td></tr>")
            sb.Append("</table>")
            sb.Append("</td></tr>")
        End If


        sb.Append("</table></td></tr>")
        sb.Append("</table>")
        tdwi.InnerHtml = sb.ToString
    End Sub
    Private Sub PopPMReport(ByVal wo As String, ByVal pm As String)
        Dim comi As String = lblcomi.Value
        Dim flag As Integer = 0
        Dim subcnt As Integer = 0
        Dim subcnth As Integer = 0
        sql = "select w.siteid, s.sitename from workorder w left join sites s on s.siteid = w.siteid where w.wonum = '" & wo & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            psite = dr.Item("sitename").ToString
        End While
        dr.Close()
        Dim func, funcchk, task, tasknum, subtask As String
        Dim sb As New System.Text.StringBuilder

        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
        sb.Append("<tr><td width=""120""></td><td width=""160""></td><td width=""130""></td><td width=""170""></td><td width=""90""></td></tr>")



        sql = "usp_getwoprint '" & wo & "'"

        dr = won.GetRdrData(sql)
        Dim headhold As String = "0"
        While dr.Read
            Dim pms As String = dr.Item("pm").ToString
            sb.Append("<tr height=""30"" valign=""top""><td class=""bigbold1"" colspan=""5"" align=""center"">" & psite & " Preventive Maintenance Work Request</td></tr>")
            sb.Append("<tr height=""20"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center"">" & Now & "</td></tr>")
            sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""5"" align=""center"">" & pms & "</td></tr>")
            sb.Append("<tr height=""20"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center""><b>Work Order#</b>&nbsp;&nbsp;" & wo & "</td></tr>")
            'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
            sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl247", "woprint.aspx.vb") & "</td></tr>")
            Dim start As String = dr.Item("schedstart").ToString
            If start = "" Then
                start = dr.Item("targstartdate").ToString
                If start = "" Then
                    start = Now
                End If
            End If
            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl248", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & start & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl249", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("actfinish").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl250", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("reportedby").ToString & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl251", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("reportdate").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl297", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("status").ToString & "</td>")
            sb.Append("<td class=""label"">Work Type</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("worktype").ToString & "</td></tr>")

            pri = dr.Item("wopriority").ToString
            priid = dr.Item("wopri").ToString

            If prichk = "yes" Then

                If (pri <> "" And pri <> "0") And (priid = "" Or priid = "0") Then
                    If pri <> "" And pri <> "0" Then
                        pri = pri
                    Else
                        pri = ""
                    End If
                Else
                    If priid <> "" And priid <> "0" Then
                        pri = getpri(priid)
                        If pri = "~oops~" Then
                            pri = ""
                        End If
                    End If
                End If
            ElseIf prichk = "no" Then
                If priid <> "" And priid <> "0" Then
                    pri = getpri(priid)
                    If pri = "~oops~" Then
                        pri = ""
                    End If
                Else
                    If pri <> "" And pri <> "0" Then
                        pri = pri
                    Else
                        pri = ""
                    End If
                End If
            Else
                If pri <> "" And pri <> "0" Then
                    pri = pri
                Else
                    pri = ""
                End If
            End If

            sb.Append("<tr><td class=""label"">WO Priority</td><td class=""plainlabel"">" & pri & "</td><td class=""label"">" & tmod.getlbl("cdlbl298", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("chargenum").ToString & "</td></tr>")

            'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")

            sb.Append("<tr height=""30""><td class=""label"">" & tmod.getlbl("cdlbl254", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("supervisor").ToString & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl255", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("leadcraft").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl256", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""4"">" & dr.Item("description").ToString & dr.Item("longdesc").ToString & "</td></tr>")

            sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
            sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl257", "woprint.aspx.vb") & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl258", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("dept_line").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("dept_desc").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl259", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("cell_name").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("cell_desc").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl260", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("ldesc").ToString & "</td></tr>")

            If dr.Item("eqnum").ToString <> "" Then
                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl261", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("eqnum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("eqdesc").ToString & "</td></tr>")
            End If
            If dr.Item("fpc").ToString <> "" Then
                sb.Append("<tr height=""20""><td class=""label"">Column</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("fpc").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3""></td></tr>")
            End If
            If dr.Item("fpc").ToString <> "" Then
                sb.Append("<tr height=""20""><td class=""label"">Column</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("fpc").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3""></td></tr>")
            End If
        End While
        dr.Close()
        'sb.Append("<tr><td>&nbsp;</td></tr>")

        sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
        sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""640""></td></tr>")
        sql = "usp_getpmman '" & pm & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            If headhold = "0" Then
                headhold = "1"
                pm = dr.Item("pm").ToString
                sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl262", "woprint.aspx.vb") & "</td></tr>")

            End If

            func = dr.Item("func").ToString
            If funcchk <> func Then 'flag = 0 Then
                If flag = 0 Then
                    flag = 1
                Else
                    'sb.Append("<tr><td>&nbsp;</td></tr>")
                End If

                funcchk = func
                sb.Append("<tr height=""20""><td class=""label"" colspan=""3""><u>Function: " & func & "</u></td>")
                'sb.Append("<td class=""plainlabel"" colspan=""2"">" & func & "</td></tr>")
                'sb.Append("<tr><td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl263" , "woprint.aspx.vb") & "</td>")
                'sb.Append("<td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl264" , "woprint.aspx.vb") & "</td>")
                'sb.Append("<td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl265" , "woprint.aspx.vb") & "</td></tr>")
            End If
            task = dr.Item("task").ToString
            tasknum = dr.Item("tasknum").ToString
            subtask = dr.Item("subtask").ToString
            subcnt = dr.Item("subcnt").ToString


            If subtask = "0" Then
                'sb.Append("<tr height=""20""><td></td><td class=""label"">" & subcnt & "</td></tr>")
                sb.Append("<tr><td class=""plainlabel"">[" & tasknum & "]</td>")
                If Len(dr.Item("task").ToString) > 0 Then
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Task</b> :" & task & "</td></tr>")
                Else
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Task:</b> No Task Description Provided</td></tr>")
                End If
                'sb.Append("<tr><td>&nbsp;</td></tr>")
                sb.Append("<tr height=""20""><td></td>")
                sb.Append("<td colspan=""2"" class=""plainlabel"">Check: OK(___) " & dr.Item("fm1").ToString & "</td></tr>")
                If dr.Item("lube") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Lubes: " & dr.Item("lube").ToString & "</td></tr>")
                End If
                If dr.Item("tool") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Tools: " & dr.Item("tool").ToString & "</td></tr>")
                End If
                If dr.Item("part") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Parts: " & dr.Item("part").ToString & "</td></tr>")
                End If
                If dr.Item("meas") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Measurements: " & dr.Item("meas").ToString & "</td></tr>")
                End If
                If subcnt = 0 Then
                    sb.Append("<tr><td>&nbsp;</td></tr>")
                Else
                    If subcnt <> 0 Then

                        sb.Append("<tr height=""20""><td></td><td class=""label"" colspan=""2""><u>Sub Tasks</u></td></tr>")
                    End If

                End If

            Else
                'sb.Append("<tr><td>" & subcnt & "</td></tr>")
                sb.Append("<tr height=""20""><td></td><td class=""plainlabel"">[" & subtask & "]</td>")
                If Len(dr.Item("subt").ToString) > 0 Then
                    sb.Append("<td class=""plainlabel""><b>Task</b>: " & dr.Item("subt").ToString & "</td></tr>")
                Else
                    sb.Append("<td class=""plainlabel""><b>Task</b>: No Sub Task Description Provided</td></tr>")
                End If
                sb.Append("<tr><td></td><td></td>")
                sb.Append("<td class=""plainlabel"">Check: OK(___)</td></tr>")
                sb.Append("<tr><td>&nbsp;</td></tr>")
            End If
        End While
        dr.Close()
        If comi <> "SW" Then
            sb.Append("<tr><td colspan=""5"" class=""plainlabel"">Work Performed by:  ________________________   Date: __________</td></tr>")
            sb.Append("<tr><td colspan=""5"">")
            sb.Append("<table border=""0"">")
            sb.Append("<tr><td class=""plainlabel"">Time&nbsp;(min)</td>")
            sb.Append("<td height=""24"" width=""100"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">REG</td>")
            sb.Append("<td width=""100"" align=""center"" class=""plainlabel"">OT</td></tr>")
            sb.Append("<tr><td class=""plainlabel"">Actual:</td>")
            sb.Append("<td height=""24"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">________</td>")
            sb.Append("<td align=""center"" class=""plainlabel"">________</td></tr>")
            sb.Append("<tr><td class=""plainlabel"">Other:</td>")
            sb.Append("<td height=""24"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">________</td>")
            sb.Append("<td align=""center"" class=""plainlabel"">________</td></tr>")
            sb.Append("</table>")
            sb.Append("</td></tr>")
        End If


        sb.Append("</table></td></tr>")
        sb.Append("</table>")
        tdwi.InnerHtml = sb.ToString
    End Sub
    Private Sub PopPMReportMax(ByVal wo As String, ByVal pm As String)
        Dim comi As String = lblcomi.Value
        Dim flag As Integer = 0
        Dim subcnt As Integer = 0
        Dim subcnth As Integer = 0
        sql = "select w.siteid, s.sitename from workorder w left join sites s on s.siteid = w.siteid where w.wonum = '" & wo & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            psite = dr.Item("sitename").ToString
        End While
        dr.Close()
        Dim func, funcchk, task, tasknum, subtask As String
        Dim sb As New System.Text.StringBuilder

        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
        sb.Append("<tr><td width=""120""></td><td width=""160""></td><td width=""130""></td><td width=""170""></td><td width=""90""></td></tr>")



        sql = "usp_getwoprintmax '" & wo & "'"

        dr = won.GetRdrData(sql)
        Dim headhold As String = "0"
        While dr.Read
            Dim pms As String = dr.Item("pm").ToString
            sb.Append("<tr height=""30"" valign=""top""><td class=""bigbold1"" colspan=""5"" align=""center"">" & psite & " Preventive Maintenance Work Request</td></tr>")
            sb.Append("<tr height=""20"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center"">" & Now & "</td></tr>")
            sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""5"" align=""center"">" & pms & "</td></tr>")
            sb.Append("<tr height=""20"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center""><b>Work Order#</b>&nbsp;&nbsp;" & wo & "</td></tr>")
            'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
            sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl247", "woprint.aspx.vb") & "</td></tr>")
            Dim start As String = dr.Item("schedstart").ToString
            If start = "" Then
                start = dr.Item("targstartdate").ToString
                If start = "" Then
                    start = Now
                End If
            End If
            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl248", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & start & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl249", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("actfinish").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl250", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("reportedby").ToString & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl251", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("reportdate").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl297", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("status").ToString & "</td>")
            sb.Append("<td class=""label"">Work Type</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("worktype").ToString & "</td></tr>")

            pri = dr.Item("wopriority").ToString
            priid = dr.Item("wopri").ToString

            If prichk = "yes" Then

                If (pri <> "" And pri <> "0") And (priid = "" Or priid = "0") Then
                    If pri <> "" And pri <> "0" Then
                        pri = pri
                    Else
                        pri = ""
                    End If
                Else
                    If priid <> "" And priid <> "0" Then
                        pri = getpri(priid)
                        If pri = "~oops~" Then
                            pri = ""
                        End If
                    End If
                End If
            ElseIf prichk = "no" Then
                If priid <> "" And priid <> "0" Then
                    pri = getpri(priid)
                    If pri = "~oops~" Then
                        pri = ""
                    End If
                Else
                    If pri <> "" And pri <> "0" Then
                        pri = pri
                    Else
                        pri = ""
                    End If
                End If
            Else
                If pri <> "" And pri <> "0" Then
                    pri = pri
                Else
                    pri = ""
                End If
            End If

            sb.Append("<tr><td class=""label"">WO Priority</td><td class=""plainlabel"">" & pri & "</td><td class=""label"">" & tmod.getlbl("cdlbl298", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("chargenum").ToString & "</td></tr>")

            'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")

            sb.Append("<tr height=""30""><td class=""label"">" & tmod.getlbl("cdlbl254", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("supervisor").ToString & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl255", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("leadcraft").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl256", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""4"">" & dr.Item("description").ToString & "</td></tr>")

            sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
            sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl257", "woprint.aspx.vb") & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl258", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("dept_line").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("dept_desc").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl259", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("cell_name").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("cell_desc").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl260", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("ldesc").ToString & "</td></tr>")

            If dr.Item("eqnum").ToString <> "" Then
                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl261", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("eqnum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("eqdesc").ToString & "</td></tr>")
            End If
            If dr.Item("fpc").ToString <> "" Then
                sb.Append("<tr height=""20""><td class=""label"">Column</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("fpc").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3""></td></tr>")
            End If
        End While
        dr.Close()
        'sb.Append("<tr><td>&nbsp;</td></tr>")

        Dim tnum, td, fm, sk, qt, st, lube, tool, part, stnum, jpstr, mea, jp As String
        Try
            sql = "select pmjp1 from pmmax where pmid = '" & pm & "'"
            jp = won.strScalar(sql)
        Catch ex As Exception
            jp = "-1"
        End Try

        If jp <> "-1" Then
            sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
            sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""580""></td></tr>")
            sql = "usp_getjpwo '" & jp & "','" & wo & "'"
            sql = "select distinct t.tasknum, t.subtask, t.taskdesc, t.failuremode, t.skill, t.qty, t.ttime, " _
           + "cast(l.output as varchar(3000)) as lout, cast(tl.output as varchar(3000)) as tout, cast(p.output as varchar(3000)) as pout, w.jpnum, w.description, m.measurement " _
           + "from wojobtasks t " _
           + "left join wojplubesout l on t.wojtid = l.wojtid " _
           + "left join wojptoolsout tl on t.wojtid = tl.wojtid " _
           + "left join wojppartsout p on t.wojtid = p.wojtid " _
           + "left join wojobplans w on w.jpid = t.jpid " _
           + "left join wojptaskmeasoutput m on m.wojtid = t.wojtid " _
           + "where t.jpid = '" & jp & "' and t.wonum = '" & wo & "' order by t.tasknum"
            Dim jpdesc As String
            dr = won.GetRdrData(sql)
            While dr.Read
                jpstr = dr.Item("jpnum").ToString
                jpdesc = dr.Item("description").ToString
                If headhold = "0" Then
                    headhold = "1"
                    'jp = dr.Item("pm").ToString
                    sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""bigbold graybot"" colspan=""4"">Job Plan Details</td></tr>")
                    sb.Append("<tr><td class=""label"" colspan=""3"">Job Plan</td><td class=""plainlabel"">" & jpstr & "</tr>")
                    sb.Append("<tr><td class=""label"" colspan=""3"">Description</td><td class=""plainlabel"">" & jpdesc & "</tr>")
                    sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")

                End If

                tnum = dr.Item("tasknum").ToString
                stnum = dr.Item("subtask").ToString
                td = dr.Item("taskdesc").ToString
                sk = dr.Item("skill").ToString
                qt = dr.Item("qty").ToString
                st = dr.Item("ttime").ToString
                fm = dr.Item("failuremode").ToString
                lube = dr.Item("lout").ToString
                tool = dr.Item("tout").ToString
                part = dr.Item("pout").ToString
                mea = dr.Item("measurement").ToString

                If stnum = "0" Then
                    sb.Append("<tr><td class=""plainlabel"">[" & tnum & "]</td>")
                    If Len(td) > 0 Then
                        sb.Append("<td colspan=""3"" class=""plainlabel"">" & td & "</td></tr>")
                    Else
                        sb.Append("<td colspan=""3"" class=""plainlabel"">" & tmod.getlbl("cdlbl286", "woprint.aspx.vb") & "</td></tr>")
                    End If
                    If sk <> "" And sk <> "Select" Then
                        sb.Append("<tr><td></td>")
                        sb.Append("<td colspan=""3"" class=""plainlabel""><b>Skill:&nbsp;&nbsp;</b>" & sk)
                        sb.Append("&nbsp;&nbsp;&nbsp;<b>Qty:&nbsp;&nbsp;</b>" & qt)
                        sb.Append("&nbsp;&nbsp;&nbsp;<b>Time:&nbsp;&nbsp;</b>" & st & "</td></tr>")
                    End If
                    If fm <> "" Then
                        sb.Append("<tr><td></td>")
                        sb.Append("<td colspan=""3"" class=""plainlabel""><b>Failure Modes:&nbsp;&nbsp;</b>" & fm & "</td></tr>")
                    End If
                    If mea <> "" Then
                        sb.Append("<tr><td></td>")
                        sb.Append("<td colspan=""3"" class=""plainlabel""><b>Measurements:&nbsp;&nbsp;</b>" & mea & "</td></tr>")
                    End If
                    If lube <> "" Then
                        sb.Append("<tr><td></td>")
                        sb.Append("<td colspan=""3"" class=""plainlabel""><b>Lubes:&nbsp;&nbsp;</b>" & lube & "</td></tr>")
                    End If
                    If tool <> "" Then
                        sb.Append("<tr><td></td>")
                        sb.Append("<td colspan=""3"" class=""plainlabel""><b>Tools:&nbsp;&nbsp;</b>" & tool & "</td></tr>")
                    End If
                    If part <> "" Then
                        sb.Append("<tr><td></td>")
                        sb.Append("<td colspan=""3"" class=""plainlabel""><b>Parts:&nbsp;&nbsp;</b>" & part & "</td></tr>")
                    End If
                    sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
                Else
                    sb.Append("<tr><td class=""plainlabel"">[" & stnum & "]</td>")
                    If Len(td) > 0 Then
                        sb.Append("<td></td><td colspan=""2"" class=""plainlabel"">" & td & "</td></tr>")
                    Else
                        sb.Append("<td></td><td colspan=""2"" class=""plainlabel"">" & tmod.getlbl("cdlbl287", "woprint.aspx.vb") & "</td></tr>")
                    End If
                    If sk <> "" And sk <> "Select" Then
                        sb.Append("<tr><td></td><td></td>")
                        sb.Append("<td colspan=""2"" class=""plainlabel""><b>Skill:&nbsp;&nbsp;</b>" & sk)
                        sb.Append("&nbsp;&nbsp;&nbsp;<b>Qty:&nbsp;&nbsp;</b>" & qt)
                        sb.Append("&nbsp;&nbsp;&nbsp;<b>Time:&nbsp;&nbsp;</b>" & st & "</td></tr>")
                    End If
                    If fm <> "" Then
                        sb.Append("<tr><td></td><td></td>")
                        sb.Append("<td colspan=""2"" class=""plainlabel""><b>Failure Modes:&nbsp;&nbsp;</b>" & fm & "</td></tr>")
                    End If
                    If lube <> "" Then
                        sb.Append("<tr><td></td><td></td>")
                        sb.Append("<td colspan=""2"" class=""plainlabel""><b>Lubes:&nbsp;&nbsp;</b>" & lube & "</td></tr>")
                    End If
                    If tool <> "" Then
                        sb.Append("<tr><td></td><td></td>")
                        sb.Append("<td colspan=""2"" class=""plainlabel""><b>Tools:&nbsp;&nbsp;</b>" & tool & "</td></tr>")
                    End If
                    If part <> "" Then
                        sb.Append("<tr><td></td><td></td>")
                        sb.Append("<td colspan=""2"" class=""plainlabel""><b>Parts:&nbsp;&nbsp;</b>" & part & "</td></tr>")
                    End If
                    sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
                End If
            End While
            dr.Close()
        End If


        '*** Parts
        Dim pflg As String = "0"
        headhold = 0
        sql = "select itemnum, description, location, qty from woparts where wonum = '" & wo & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            If headhold = 0 Then
                headhold = "1"

                sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
                sb.Append("<tr><td width=""150""></td><td width=""30""></td><td width=""200""></td><td width=""400""></td></tr>")
                'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""4"">" & tmod.getlbl("cdlbl288", "woprint.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td class=""label""><u>Part#</u></td>")
                sb.Append("<td class=""label""><u>Qty</u></td>")
                sb.Append("<td class=""label""><u>Description</u></td>")
                sb.Append("<td class=""label""><u>Location</u></td></tr>")
            End If
            If Len(dr.Item("itemnum").ToString) > 0 Then
                pflg = "1"
                sb.Append("<tr><td class=""label"">" & dr.Item("itemnum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("qty").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("description").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        If pflg = "1" Then
            sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")
            sb.Append("</Table></td></tr>")
        End If

        '*** Tools
        Dim tflg As String = "0"
        headhold = 0
        sql = "select toolnum, description, location, qty from wotools where wonum = '" & wo & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            If headhold = 0 Then
                headhold = "1"

                sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
                sb.Append("<tr><td width=""150""></td><td width=""30""></td><td width=""200""></td><td width=""400""></td></tr>")
                'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""4"">" & tmod.getlbl("cdlbl289", "woprint.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td class=""label""><u>Tool#</u></td>")
                sb.Append("<td class=""label""><u>Qty</u></td>")
                sb.Append("<td class=""label""><u>Description</u></td>")
                sb.Append("<td class=""label""><u>Location</u></td></tr>")
            End If
            If Len(dr.Item("toolnum").ToString) > 0 Then
                tflg = "1"
                sb.Append("<tr><td class=""label"">" & dr.Item("toolnum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("qty").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("description").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        If tflg = "1" Then
            sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")
            sb.Append("</Table></td></tr>")
        End If

        '*** Lubes
        Dim lflg As String = "0"
        headhold = 0
        sql = "select lubenum, description, location, qty from wolubes where wonum = '" & wo & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            If headhold = 0 Then
                headhold = "1"
                sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
                sb.Append("<tr><td width=""150""></td><td width=""30""></td><td width=""200""></td><td width=""400""></td></tr>")
                'sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl290" , "woprint.aspx.vb") & "</td></tr>")
                'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""4"">" & tmod.getlbl("cdlbl291", "woprint.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td class=""label""><u>Lubricant#</u></td>")
                sb.Append("<td class=""graylabel""><u>Qty</u></td>")
                sb.Append("<td class=""label""><u>Description</u></td>")
                sb.Append("<td class=""label""><u>Location</u></td></tr>")
            End If
            If Len(dr.Item("lubenum").ToString) > 0 Then
                lflg = "1"
                sb.Append("<tr><td class=""label"">" & dr.Item("lubenum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">N/A</td>") '" & dr.Item("qty").ToString & "
                sb.Append("<td class=""plainlabel"">" & dr.Item("description").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        If lflg = "1" Then
            sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")
            sb.Append("</Table></td></tr>")
        End If
        If comi <> "SW" Then
            sb.Append("<tr><td colspan=""5"" class=""plainlabel"">Work Performed by:  ________________________   Date: __________</td></tr>")
            sb.Append("<tr><td colspan=""5"">")
            sb.Append("<table border=""0"">")
            sb.Append("<tr><td class=""plainlabel"">Time&nbsp;(min)</td>")
            sb.Append("<td height=""24"" width=""100"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">REG</td>")
            sb.Append("<td width=""100"" align=""center"" class=""plainlabel"">OT</td></tr>")
            sb.Append("<tr><td class=""plainlabel"">Actual:</td>")
            sb.Append("<td height=""24"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">________</td>")
            sb.Append("<td align=""center"" class=""plainlabel"">________</td></tr>")
            sb.Append("<tr><td class=""plainlabel"">Other:</td>")
            sb.Append("<td height=""24"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">________</td>")
            sb.Append("<td align=""center"" class=""plainlabel"">________</td></tr>")
            sb.Append("</table>")
            sb.Append("</td></tr>")
        End If


        sb.Append("</table></td></tr>")
        sb.Append("</table>")
        tdwi.InnerHtml = sb.ToString
    End Sub
    Private Sub PopTaskReport(ByVal wo As String, ByVal jp As String)
        Dim comi As String = lblcomi.Value
        Dim flag As Integer = 0
        Dim subcnt As Integer = 0
        Dim subcnth As Integer = 0
        'psite = HttpContext.Current.Session("psite").ToString()
        sql = "select w.siteid, s.sitename from workorder w left join sites s on s.siteid = w.siteid where w.wonum = '" & wo & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            psite = dr.Item("sitename").ToString
        End While
        dr.Close()
        Dim func, funcchk, task, tasknum, subtask As String
        Dim sb As New System.Text.StringBuilder

        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
        sb.Append("<tr><td width=""120""></td><td width=""160""></td><td width=""130""></td><td width=""170""></td><td width=""90""></td></tr>")

        sql = "usp_getwoprint '" & wo & "'"

        dr = won.GetRdrData(sql)
        Dim headhold As String = "0"
        While dr.Read
            Dim pms As String = dr.Item("jpnum").ToString
            sb.Append("<tr height=""30"" valign=""top""><td class=""bigbold1"" colspan=""5"" align=""center"">" & psite & " Work Request with Job Plan</td></tr>")
            sb.Append("<tr height=""20"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center"">" & Now & "</td></tr>")
            sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""5"" align=""center"">" & pms & "</td></tr>")
            sb.Append("<tr height=""20"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center""><b>Work Order#</b>&nbsp;&nbsp;" & wo & "</td></tr>")
            'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
            sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl266", "woprint.aspx.vb") & "</td></tr>")
            Dim start As String = dr.Item("schedstart").ToString
            If start = "" Then
                start = dr.Item("targstartdate").ToString
                If start = "" Then
                    start = Now
                End If
            End If
            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl267", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & start & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl268", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("actfinish").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl269", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("reportedby").ToString & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl270", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("reportdate").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl297", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("status").ToString & "</td>")
            sb.Append("<td class=""label"">Work Type</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("worktype").ToString & "</td></tr>")

            pri = dr.Item("wopriority").ToString
            priid = dr.Item("wopri").ToString

            If prichk = "yes" Then

                If (pri <> "" And pri <> "0") And (priid = "" Or priid = "0") Then
                    If pri <> "" And pri <> "0" Then
                        pri = pri
                    Else
                        pri = ""
                    End If
                Else
                    If priid <> "" And priid <> "0" Then
                        pri = getpri(priid)
                        If pri = "~oops~" Then
                            pri = ""
                        End If
                    End If
                End If
            ElseIf prichk = "no" Then
                If priid <> "" And priid <> "0" Then
                    pri = getpri(priid)
                    If pri = "~oops~" Then
                        pri = ""
                    End If
                Else
                    If pri <> "" And pri <> "0" Then
                        pri = pri
                    Else
                        pri = ""
                    End If
                End If
            Else
                If pri <> "" And pri <> "0" Then
                    pri = pri
                Else
                    pri = ""
                End If
            End If

            sb.Append("<tr><td class=""label"">WO Priority</td><td class=""plainlabel"">" & pri & "</td><td class=""label"">" & tmod.getlbl("cdlbl298", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("chargenum").ToString & "</td></tr>")

            sb.Append("<tr height=""30""><td class=""label"">" & tmod.getlbl("cdlbl273", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("supervisor").ToString & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl274", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("leadcraft").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl275", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""4"">" & dr.Item("description").ToString & dr.Item("longdesc").ToString & "</td></tr>")

            sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
            sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl276", "woprint.aspx.vb") & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl277", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("dept_line").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("dept_desc").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl278", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("cell_name").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("cell_desc").ToString & "</td></tr>")

            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl279", "woprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("ldesc").ToString & "</td></tr>")

            If dr.Item("eqnum").ToString <> "" Then
                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl280", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("eqnum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("eqdesc").ToString & "</td></tr>")
            End If
            If dr.Item("fpc").ToString <> "" Then
                sb.Append("<tr height=""20""><td class=""label"">Column</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("fpc").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3""></td></tr>")
            End If
            If dr.Item("func").ToString <> "" Then
                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl281", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("func").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3""></td></tr>")
            End If

            If dr.Item("compnum").ToString <> "" Then
                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl282", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("compnum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("compdesc").ToString & " - " & dr.Item("spl").ToString & " - " & dr.Item("desig").ToString & "</td></tr>")
            End If

            If dr.Item("fm").ToString <> "" Then
                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl283", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("fm").ToString & "</td></tr>")
            End If

            If dr.Item("measurement").ToString <> "" Then
                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl284", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("measurement").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        'sb.Append("<tr><td>&nbsp;</td></tr>")

        Dim tnum, td, fm, sk, qt, st, lube, tool, part, stnum, jpstr, mea As String

        sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""580""></td></tr>")
        sql = "usp_getjpwo '" & jp & "','" & wo & "'"
        sql = "select distinct t.tasknum, t.subtask, t.taskdesc, t.failuremode, t.skill, t.qty, t.ttime, " _
       + "cast(l.output as varchar(3000)) as lout, cast(tl.output as varchar(3000)) as tout, cast(p.output as varchar(3000)) as pout, w.jpnum, m.measurement " _
       + "from wojobtasks t " _
       + "left join wojplubesout l on t.wojtid = l.wojtid " _
       + "left join wojptoolsout tl on t.wojtid = tl.wojtid " _
       + "left join wojppartsout p on t.wojtid = p.wojtid " _
       + "left join wojobplans w on w.jpid = t.jpid " _
       + "left join wojptaskmeasoutput m on m.wojtid = t.wojtid " _
       + "where t.jpid = '" & jp & "' and t.wonum = '" & wo & "' order by t.tasknum"

        dr = won.GetRdrData(sql)
        While dr.Read
            jpstr = dr.Item("jpnum").ToString
            If headhold = "0" Then
                headhold = "1"
                'jp = dr.Item("pm").ToString
                sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""4"">" & tmod.getlbl("cdlbl285", "woprint.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td class=""label"" colspan=""3"">Job Plan</td><td class=""plainlabel"">" & jpstr & "</tr>")

            End If

            tnum = dr.Item("tasknum").ToString
            stnum = dr.Item("subtask").ToString
            td = dr.Item("taskdesc").ToString
            sk = dr.Item("skill").ToString
            qt = dr.Item("qty").ToString
            st = dr.Item("ttime").ToString
            fm = dr.Item("failuremode").ToString
            lube = dr.Item("lout").ToString
            tool = dr.Item("tout").ToString
            part = dr.Item("pout").ToString
            mea = dr.Item("measurement").ToString

            If stnum = "0" Then
                sb.Append("<tr><td class=""plainlabel"">[" & tnum & "]</td>")
                If Len(td) > 0 Then
                    sb.Append("<td colspan=""3"" class=""plainlabel"">" & td & "</td></tr>")
                Else
                    sb.Append("<td colspan=""3"" class=""plainlabel"">" & tmod.getlbl("cdlbl286", "woprint.aspx.vb") & "</td></tr>")
                End If
                If sk <> "" And sk <> "Select" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>Skill:&nbsp;&nbsp;</b>" & sk)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Qty:&nbsp;&nbsp;</b>" & qt)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Time:&nbsp;&nbsp;</b>" & st & "</td></tr>")
                End If
                If fm <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>Failure Modes:&nbsp;&nbsp;</b>" & fm & "</td></tr>")
                End If
                If mea <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>Measurements:&nbsp;&nbsp;</b>" & mea & "</td></tr>")
                End If
                If lube <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>Lubes:&nbsp;&nbsp;</b>" & lube & "</td></tr>")
                End If
                If tool <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>Tools:&nbsp;&nbsp;</b>" & tool & "</td></tr>")
                End If
                If part <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>Parts:&nbsp;&nbsp;</b>" & part & "</td></tr>")
                End If
                sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
            Else
                sb.Append("<tr><td class=""plainlabel"">[" & stnum & "]</td>")
                If Len(td) > 0 Then
                    sb.Append("<td></td><td colspan=""2"" class=""plainlabel"">" & td & "</td></tr>")
                Else
                    sb.Append("<td></td><td colspan=""2"" class=""plainlabel"">" & tmod.getlbl("cdlbl287", "woprint.aspx.vb") & "</td></tr>")
                End If
                If sk <> "" And sk <> "Select" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Skill:&nbsp;&nbsp;</b>" & sk)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Qty:&nbsp;&nbsp;</b>" & qt)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Time:&nbsp;&nbsp;</b>" & st & "</td></tr>")
                End If
                If fm <> "" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Failure Modes:&nbsp;&nbsp;</b>" & fm & "</td></tr>")
                End If
                If lube <> "" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Lubes:&nbsp;&nbsp;</b>" & lube & "</td></tr>")
                End If
                If tool <> "" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Tools:&nbsp;&nbsp;</b>" & tool & "</td></tr>")
                End If
                If part <> "" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Parts:&nbsp;&nbsp;</b>" & part & "</td></tr>")
                End If
                sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
            End If
        End While
        dr.Close()

        '*** Parts
        Dim pflg As String = "0"
        headhold = 0
        sql = "select itemnum, description, location, qty from woparts where wonum = '" & wo & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            If headhold = 0 Then
                headhold = "1"

                sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
                sb.Append("<tr><td width=""150""></td><td width=""30""></td><td width=""200""></td><td width=""400""></td></tr>")
                'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""4"">" & tmod.getlbl("cdlbl288", "woprint.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td class=""label""><u>Part#</u></td>")
                sb.Append("<td class=""label""><u>Qty</u></td>")
                sb.Append("<td class=""label""><u>Description</u></td>")
                sb.Append("<td class=""label""><u>Location</u></td></tr>")
            End If
            If Len(dr.Item("itemnum").ToString) > 0 Then
                pflg = "1"
                sb.Append("<tr><td class=""label"">" & dr.Item("itemnum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("qty").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("description").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        If pflg = "1" Then
            sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")
            sb.Append("</Table></td></tr>")
        End If

        '*** Tools
        Dim tflg As String = "0"
        headhold = 0
        sql = "select toolnum, description, location, qty from wotools where wonum = '" & wo & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            If headhold = 0 Then
                headhold = "1"

                sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
                sb.Append("<tr><td width=""150""></td><td width=""30""></td><td width=""200""></td><td width=""400""></td></tr>")
                'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""4"">" & tmod.getlbl("cdlbl289", "woprint.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td class=""label""><u>Tool#</u></td>")
                sb.Append("<td class=""label""><u>Qty</u></td>")
                sb.Append("<td class=""label""><u>Description</u></td>")
                sb.Append("<td class=""label""><u>Location</u></td></tr>")
            End If
            If Len(dr.Item("toolnum").ToString) > 0 Then
                tflg = "1"
                sb.Append("<tr><td class=""label"">" & dr.Item("toolnum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("qty").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("description").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        If tflg = "1" Then
            sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")
            sb.Append("</Table></td></tr>")
        End If

        '*** Lubes
        Dim lflg As String = "0"
        headhold = 0
        sql = "select lubenum, description, location, qty from wolubes where wonum = '" & wo & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            If headhold = 0 Then
                headhold = "1"
                sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
                sb.Append("<tr><td width=""150""></td><td width=""30""></td><td width=""200""></td><td width=""400""></td></tr>")
                'sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl290" , "woprint.aspx.vb") & "</td></tr>")
                'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""4"">" & tmod.getlbl("cdlbl291", "woprint.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td class=""label""><u>Lubricant#</u></td>")
                sb.Append("<td class=""graylabel""><u>Qty</u></td>")
                sb.Append("<td class=""label""><u>Description</u></td>")
                sb.Append("<td class=""label""><u>Location</u></td></tr>")
            End If
            If Len(dr.Item("lubenum").ToString) > 0 Then
                lflg = "1"
                sb.Append("<tr><td class=""label"">" & dr.Item("lubenum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">N/A</td>") '" & dr.Item("qty").ToString & "
                sb.Append("<td class=""plainlabel"">" & dr.Item("description").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        If lflg = "1" Then
            sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")
            sb.Append("</Table></td></tr>")
        End If
        If comi <> "SW" Then
            sb.Append("<tr><td colspan=""5"" class=""plainlabel"">Work Performed by:  ________________________   Date: __________</td></tr>")
            sb.Append("<tr><td colspan=""5"">")
            sb.Append("<table border=""0"">")
            sb.Append("<tr><td class=""plainlabel"">Time&nbsp;(min)</td>")
            sb.Append("<td height=""24"" width=""100"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">REG</td>")
            sb.Append("<td width=""100"" align=""center"" class=""plainlabel"">OT</td></tr>")
            sb.Append("<tr><td class=""plainlabel"">Actual:</td>")
            sb.Append("<td height=""24"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">________</td>")
            sb.Append("<td align=""center"" class=""plainlabel"">________</td></tr>")
            sb.Append("<tr><td class=""plainlabel"">Other:</td>")
            sb.Append("<td height=""24"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">________</td>")
            sb.Append("<td align=""center"" class=""plainlabel"">________</td></tr>")
            sb.Append("</table>")
            sb.Append("</td></tr>")
        End If


        sb.Append("</table></td></tr>")
        sb.Append("</table>")
        tdwi.InnerHtml = sb.ToString
    End Sub
    Private Sub PopWOReport(ByVal wo As String)
        Dim typ As String '= lblwotype.Value
        Dim comi As String = lblcomi.Value
        Dim flag As Integer = 0
        Dim fmcnt As Integer
        Dim func, funcchk, task, tasknum, pm, pmtskid, wt As String

        'psite = HttpContext.Current.Session("psite").ToString()
        sql = "select w.siteid, s.sitename from workorder w left join sites s on s.siteid = w.siteid where w.wonum = '" & wo & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            psite = dr.Item("sitename").ToString
        End While
        dr.Close()
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
        sb.Append("<tr><td width=""120""></td><td width=""160""></td><td width=""130""></td><td width=""170""></td><td width=""90""></td></tr>")
        sql = "usp_getwoprint '" & wo & "'"

        dr = won.GetRdrData(sql)
        Dim headhold As String = "0"
        comi = lblcomi.Value
        While dr.Read
            If headhold = "0" Then
                headhold = "1"
                typ = dr.Item("worktype").ToString
                'pm = dr.Item("pm").ToString
                If typ = "CM" Or typ = "PM4" Then
                    If comi = "GLA" Then
                        sb.Append("<tr height=""30"" valign=""top""><td><img src=""../menu/mimages/logo_glassine.jpg""></td><td class=""bigbold1"" colspan=""4"" align=""left"">&nbsp;&nbsp;" & psite & " Corrective Maintenance Work Request</td></tr>")
                    Else
                        sb.Append("<tr height=""30"" valign=""top""><td><img src=""../menu/mimages/custcomp.gif""></td><td class=""bigbold1"" colspan=""4"" align=""left"">&nbsp;&nbsp;" & psite & " Corrective Maintenance Work Request</td></tr>")
                    End If

                ElseIf typ = "EM" Then
                    If comi = "GLA" Then
                        sb.Append("<tr height=""30"" valign=""top""><td><img src=""../menu/mimages/logo_glassine.jpg""></td><td class=""bigbold1"" colspan=""4"" align=""left"">&nbsp;&nbsp;" & psite & " Emergency Maintenance Work Request</td></tr>")
                    Else
                        sb.Append("<tr height=""30"" valign=""top""><td><img src=""../menu/mimages/custcomp.gif""></td><td class=""bigbold1"" colspan=""4"" align=""left"">&nbsp;&nbsp;" & psite & " Emergency Maintenance Work Request</td></tr>")
                    End If

                Else
                    If comi = "GLA" Then
                        sb.Append("<tr height=""30"" valign=""top""><td><img src=""../menu/mimages/logo_glassine.jpg""></td><td class=""bigbold1"" colspan=""4"" align=""left"">&nbsp;&nbsp;" & psite & " Work Request</td></tr>")
                    Else
                        sb.Append("<tr height=""30"" vlaign=""top""><td><img src=""../menu/mimages/custcomp.gif""></td><td class=""bigbold1"" colspan=""4"" align=""left"">&nbsp;&nbsp;" & psite & " Work Request</td></tr>")
                    End If

                End If

                sb.Append("<tr height=""20"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center"">" & Now & "</td></tr>")
                sb.Append("<tr height=""20"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center""><b>Work Order#</b>&nbsp;&nbsp;" & wo & "</td></tr>")
                'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl292", "woprint.aspx.vb") & "</td></tr>")
                Dim start As String = dr.Item("schedstart").ToString
                If start = "" Then
                    start = dr.Item("targstartdate").ToString
                    If start = "" Then
                        start = Now
                    End If
                End If
                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl293", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & start & "</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl294", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("actfinish").ToString & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl295", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("reportedby").ToString & "</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl296", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("reportdate").ToString & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl297", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("status").ToString & "</td>")
                sb.Append("<td class=""label"">Work Type</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("worktype").ToString & "</td></tr>")

                pri = dr.Item("wopriority").ToString
                priid = dr.Item("wopri").ToString

                If prichk = "yes" Then

                    If (pri <> "" And pri <> "0") And (priid = "" Or priid = "0") Then
                        If pri <> "" And pri <> "0" Then
                            pri = pri
                        Else
                            pri = ""
                        End If
                    Else
                        If priid <> "" And priid <> "0" Then
                            pri = getpri(priid)
                            If pri = "~oops~" Then
                                pri = ""
                            End If
                        End If
                    End If
                ElseIf prichk = "no" Then
                    If priid <> "" And priid <> "0" Then
                         pri = getpri(priid)
                        If pri = "~oops~" Then
                            pri = ""
                        End If
                    Else
                        If pri <> "" And pri <> "0" Then
                            pri = pri
                        Else
                            pri = ""
                        End If
                    End If
                Else
                    If pri <> "" And pri <> "0" Then
                        pri = pri
                    Else
                        pri = ""
                    End If
                End If

                sb.Append("<tr><td class=""label"">WO Priority</td><td class=""plainlabel"">" & pri & "</td><td class=""label"">" & tmod.getlbl("cdlbl298", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("chargenum").ToString & "</td></tr>")

                

                'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")

                sb.Append("<tr height=""30""><td class=""label"">" & tmod.getlbl("cdlbl299", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("supervisor").ToString & "</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl300", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("leadcraft").ToString & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl301", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""4"">" & dr.Item("description").ToString & dr.Item("longdesc").ToString & "</td></tr>")

                sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl302", "woprint.aspx.vb") & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl303", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("dept_line").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("dept_desc").ToString & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl304", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("cell_name").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("cell_desc").ToString & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl305", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("ldesc").ToString & "</td></tr>")

                If dr.Item("eqnum").ToString <> "" Then
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl306", "woprint.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("eqnum").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("eqdesc").ToString & "</td></tr>")
                End If
                If dr.Item("fpc").ToString <> "" Then
                    sb.Append("<tr height=""20""><td class=""label"">Column</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("fpc").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"" colspan=""3""></td></tr>")
                End If

                If dr.Item("func").ToString <> "" Then
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl307", "woprint.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("func").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"" colspan=""3""></td></tr>")
                End If

                If dr.Item("compnum").ToString <> "" Then
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl308", "woprint.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("compnum").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("compdesc").ToString & " - " & dr.Item("spl").ToString & " - " & dr.Item("desig").ToString & "</td></tr>")
                End If

                If dr.Item("fm").ToString <> "" Then
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl309", "woprint.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("fm").ToString & "</td></tr>")
                End If

                If dr.Item("measurement").ToString <> "" Then
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl310", "woprint.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("measurement").ToString & "</td></tr>")
                End If
            End If
            sb.Append("<tr><td>&nbsp;</td></tr>")
            If typ = "CM" Or typ = "EM" Or typ = "PM4" Then
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl311", "woprint.aspx.vb") & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl312", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("problem").ToString & "</td></tr>")
                sb.Append("<tr><td>&nbsp;</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl313", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("corraction").ToString & "</td></tr>")
            End If
            sb.Append("<tr><td>&nbsp;</td></tr>")
        End While
        dr.Close()
        '*** Parts
        Dim pflg As String = "0"
        headhold = 0
        sql = "select itemnum, description, location, qty from woparts where wonum = '" & wo & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            If headhold = 0 Then
                headhold = "1"

                sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
                sb.Append("<tr><td width=""150""></td><td width=""30""></td><td width=""200""></td><td width=""400""></td></tr>")
                'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""4"">" & tmod.getlbl("cdlbl314", "woprint.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td class=""label""><u>Part#</u></td>")
                sb.Append("<td class=""label""><u>Qty</u></td>")
                sb.Append("<td class=""label""><u>Description</u></td>")
                sb.Append("<td class=""label""><u>Location</u></td></tr>")
            End If
            If Len(dr.Item("itemnum").ToString) > 0 Then
                pflg = "1"
                sb.Append("<tr><td class=""label"">" & dr.Item("itemnum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("qty").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("description").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        If pflg = "1" Then
            sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")
            sb.Append("</Table></td></tr>")
        End If

        '*** Tools
        Dim tflg As String = "0"
        headhold = 0
        sql = "select toolnum, description, location, qty from wotools where wonum = '" & wo & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            If headhold = 0 Then
                headhold = "1"

                sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
                sb.Append("<tr><td width=""150""></td><td width=""30""></td><td width=""200""></td><td width=""400""></td></tr>")
                'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""4"">" & tmod.getlbl("cdlbl315", "woprint.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td class=""label""><u>Tool#</u></td>")
                sb.Append("<td class=""label""><u>Qty</u></td>")
                sb.Append("<td class=""label""><u>Description</u></td>")
                sb.Append("<td class=""label""><u>Location</u></td></tr>")
            End If
            If Len(dr.Item("toolnum").ToString) > 0 Then
                tflg = "1"
                sb.Append("<tr><td class=""label"">" & dr.Item("toolnum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("qty").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("description").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        If tflg = "1" Then
            sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")
            sb.Append("</Table></td></tr>")
        End If

        '*** Lubes
        Dim lflg As String = "0"
        headhold = 0
        sql = "select lubenum, description, location, qty from wolubes where wonum = '" & wo & "'"
        dr = won.GetRdrData(sql)
        While dr.Read
            If headhold = 0 Then
                headhold = "1"
                sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
                sb.Append("<tr><td width=""150""></td><td width=""30""></td><td width=""200""></td><td width=""400""></td></tr>")
                'sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl316" , "woprint.aspx.vb") & "</td></tr>")
                'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""4"">" & tmod.getlbl("cdlbl317", "woprint.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td class=""label""><u>Lubricant#</u></td>")
                sb.Append("<td class=""graylabel""><u>Qty</u></td>")
                sb.Append("<td class=""label""><u>Description</u></td>")
                sb.Append("<td class=""label""><u>Location</u></td></tr>")
            End If
            If Len(dr.Item("lubenum").ToString) > 0 Then
                lflg = "1"
                sb.Append("<tr><td class=""label"">" & dr.Item("lubenum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">N/A</td>") '" & dr.Item("qty").ToString & "
                sb.Append("<td class=""plainlabel"">" & dr.Item("description").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        If lflg = "1" Then
            sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")
            sb.Append("</Table></td></tr>")
        End If
        If comi <> "SW" Then
            sb.Append("<tr><td colspan=""5"" class=""plainlabel"">Work Performed by:  ________________________   Date: __________</td></tr>")
            sb.Append("<tr><td colspan=""5"">")
            sb.Append("<table border=""0"">")
            sb.Append("<tr><td class=""plainlabel"">Time&nbsp;(min)</td>")
            sb.Append("<td height=""24"" width=""100"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">REG</td>")
            sb.Append("<td width=""100"" align=""center"" class=""plainlabel"">OT</td></tr>")
            sb.Append("<tr><td class=""plainlabel"">Actual:</td>")
            sb.Append("<td height=""24"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">________</td>")
            sb.Append("<td align=""center"" class=""plainlabel"">________</td></tr>")
            sb.Append("<tr><td class=""plainlabel"">Other:</td>")
            sb.Append("<td height=""24"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">________</td>")
            sb.Append("<td align=""center"" class=""plainlabel"">________</td></tr>")
            sb.Append("</table>")
            sb.Append("</td></tr>")
        Else
            sql = "select wo_image from woimages where wonum = '" & wo & "'"
            Dim wopic, picstr As String
            wopic = won.strScalar(sql)
            Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
            Dim strfrom As String = Server.MapPath("\") + appstr + "/woimages/"
            If wopic <> "" Then
                sb.Append("<tr><td colspan=""5"" align=""center""><img src=""" + wopic + """></td></tr>")
            End If
        End If
        

        sb.Append("</table>")
        tdwi.InnerHtml = sb.ToString
    End Sub


End Class
