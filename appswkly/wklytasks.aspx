﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wklytasks.aspx.vb" Inherits="lucy_r12.wklytasks" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Weekly Tasks</title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td>
    <table cellSpacing="1" cellPadding="1">
											<tr>
												<td class="label" onmouseover="return overlib('Use to indicate shift - Clears and Disables any Day Enries')"
													onmouseout="return nd()" align="center"><asp:Label id="lang1304" runat="server">Shift</asp:Label></td>
												<td class="label" onmouseover="return overlib('Selects All Days for Selected Shift')"
													onmouseout="return nd()" align="center"><asp:Label id="lang1305" runat="server">All</asp:Label></td>
												<td class="label"></td>
												<td class="label" align="center" width="23">M</td>
												<td class="label" align="center" width="23">Tu</td>
												<td class="label" align="center" width="23">W</td>
												<td class="label" align="center" width="23">Th</td>
												<td class="label" align="center" width="23">F</td>
												<td class="label" align="center" width="23">Sa</td>
												<td class="label" align="center" width="23">Su</td>
											</tr>
											<tr>
												<td><INPUT id="cb1o" type="checkbox" runat="server"></td>
												<td><INPUT id="cb1" type="checkbox" runat="server"></td>
												<td class="label">1st Shift</td>
												<td align="center"><INPUT id="cb1mon" type="checkbox" name="Checkbox1" runat="server"></td>
												<td align="center"><INPUT id="cb1tue" type="checkbox" name="Checkbox2" runat="server"></td>
												<td align="center"><INPUT id="cb1wed" type="checkbox" name="Checkbox3" runat="server"></td>
												<td align="center"><INPUT id="cb1thu" type="checkbox" name="Checkbox4" runat="server"></td>
												<td align="center"><INPUT id="cb1fri" type="checkbox" name="Checkbox5" runat="server"></td>
												<td align="center"><INPUT id="cb1sat" type="checkbox" name="Checkbox6" runat="server"></td>
												<td align="center"><INPUT id="cb1sun" type="checkbox" name="Checkbox7" runat="server"></td>
											</tr>
											<tr>
												<td><INPUT id="cb2o" type="checkbox" runat="server"></td>
												<td><INPUT id="cb2" type="checkbox" name="Checkbox1" runat="server"></td>
												<td class="label">2nd Shift</td>
												<td align="center"><INPUT id="cb2mon" type="checkbox" name="Checkbox1" runat="server"></td>
												<td align="center"><INPUT id="cb2tue" type="checkbox" name="Checkbox2" runat="server"></td>
												<td align="center"><INPUT id="cb2wed" type="checkbox" name="Checkbox3" runat="server"></td>
												<td align="center"><INPUT id="cb2thu" type="checkbox" name="Checkbox4" runat="server"></td>
												<td align="center"><INPUT id="cb2fri" type="checkbox" name="Checkbox5" runat="server"></td>
												<td align="center"><INPUT id="cb2sat" type="checkbox" name="Checkbox6" runat="server"></td>
												<td align="center"><INPUT id="cb2sun" type="checkbox" name="Checkbox7" runat="server"></td>
											</tr>
											<tr>
												<td><INPUT id="cb3o" type="checkbox" runat="server"></td>
												<td><INPUT id="cb3" type="checkbox" name="Checkbox1" runat="server"></td>
												<td class="label">3rd Shift</td>
												<td align="center"><INPUT id="cb3mon" type="checkbox" name="Checkbox1" runat="server"></td>
												<td align="center"><INPUT id="cb3tue" type="checkbox" name="Checkbox2" runat="server"></td>
												<td align="center"><INPUT id="cb3wed" type="checkbox" name="Checkbox3" runat="server"></td>
												<td align="center"><INPUT id="cb3thu" type="checkbox" name="Checkbox4" runat="server"></td>
												<td align="center"><INPUT id="cb3fri" type="checkbox" name="Checkbox5" runat="server"></td>
												<td align="center"><INPUT id="cb3sat" type="checkbox" name="Checkbox6" runat="server"></td>
												<td align="center"><INPUT id="cb3sun" type="checkbox" name="Checkbox7" runat="server"></td>
											</tr>
										</table>
    </td>
    </tr>
    </table>
    </div>
    </form>
</body>
</html>
