<%@ Page Language="vb" AutoEventWireup="false" Codebehind="TaskNotestpm.aspx.vb" Inherits="lucy_r12.TaskNotestpm" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>TaskNotes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<script language="JavaScript" src="../scripts1/TaskNotestpmaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td><asp:label id="Label1" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True">Task#</asp:label></td>
					<td><asp:label id="lbltsk" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True"></asp:label></td>
					<td><asp:label id="Label2" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True">Sub-Task#</asp:label></td>
					<td><asp:label id="lblsub" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True"></asp:label></td>
					<td><asp:imagebutton id="ImageButton1" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td colSpan="5"><asp:textbox id="txtnote" runat="server" CssClass="plainlabel" TextMode="MultiLine" Height="168px"
							Width="384px"></asp:textbox></td>
				</tr>
			</table>
			<input id="lblptid" type="hidden" runat="server"><input type="hidden" id="lbllog" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
