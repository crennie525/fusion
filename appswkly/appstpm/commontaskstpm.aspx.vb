

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class commontaskstpm
    Inherits System.Web.UI.Page
	Protected WithEvents ovid179 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid178 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid177 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid176 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid175 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid174 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid173 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang1237 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1236 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1235 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1234 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1233 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1232 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1231 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1230 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1229 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1228 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1227 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1226 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1225 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1224 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1223 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1222 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1221 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim ctasks As New Utilities
    Dim sql As String
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
    Dim cid, taskid, tasktype, pdm, filter, sid, ro As String
    Protected WithEvents lbltasktype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpdm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbc As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbt As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents txtmemo As System.Web.UI.WebControls.TextBox
    Protected WithEvents imgsavecommon As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblmode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtkey As System.Web.UI.WebControls.TextBox

    Protected WithEvents TextBox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcompnum As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcompdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcompdesig As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcompspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfunc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfuncspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbfailmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents tdpm As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents iflst As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblreturn As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblorigtasktype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblorigpdm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblctaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblscreen As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblexit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmemo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbtt As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents rbpt As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskholdid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibaddnew As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ir1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ir2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ir3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdtasks As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddtype As System.Web.UI.WebControls.ListBox
    Protected WithEvents ddpt As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                ibaddnew.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                ibaddnew.Attributes.Add("onclick", "")
                imgsavecommon.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                imgsavecommon.Attributes.Add("onclick", "")

                ir1.Attributes.Add("src", "../images/appbuttons/minibuttons/return2dis.gif")
                ir1.Attributes.Add("onclick", "")
                ir2.Attributes.Add("src", "../images/appbuttons/minibuttons/return2dis.gif")
                ir2.Attributes.Add("onclick", "")
                ir3.Attributes.Add("src", "../images/appbuttons/minibuttons/return2dis.gif")
                ir3.Attributes.Add("onclick", "")
            End If
            sid = Request.QueryString("sid").ToString '"1C - Clean" '
            lblsid.Value = sid
            tasktype = Request.QueryString("typ").ToString '"1C - Clean" '
            lbltasktype.Value = tasktype
            lblorigtasktype.Value = tasktype
            pdm = Request.QueryString("pdm").ToString
            lblpdm.Value = pdm
            lblorigpdm.Value = pdm
            taskid = Request.QueryString("tid").ToString
            lbltaskid.Value = taskid
            ctasks.Open()
            lblcid.Value = "0"
            GetLists()
            If tasktype <> "" Then
                Try
                    ddtype.SelectedValue = lbltasktype.Value
                    Dim cond As String
                    sql = "select tasktype from pmtasktypes where ttid = '" & tasktype & "'"
                    cond = ctasks.strScalar(sql)
                    cond = Mid(cond, 1, 1)
                    If cond = "4" Then
                        Try
                            ddpt.SelectedValue = pdm
                        Catch ex As Exception

                        End Try
                    Else
                        ddpt.Enabled = False
                    End If
                Catch ex As Exception
                    'ddtype.Items.Insert(0, New ListItem(tasktype))
                    'ddtype.SelectedValue = tasktype
                End Try
            End If
            GetTasks()
            PopFail()
            ctasks.Dispose()
        Else
            If Request.Form("lblsubmit") = "refresh" Then
                lblsubmit.Value = ""
                Refreshit()
            ElseIf Request.Form("lblsubmit") = "addtask" Then
                lblsubmit.Value = ""
                lblscreen.Value = ""
                ctasks.Open()
                AddTask()
                ctasks.Dispose()
            ElseIf Request.Form("lblsubmit") = "savetask" Then
                lblsubmit.Value = ""
                lblscreen.Value = ""
                ctasks.Open()
                SaveTask()
                ctasks.Dispose()
            ElseIf Request.Form("lblsubmit") = "returntask" Then
                lblsubmit.Value = ""
                lblscreen.Value = ""
                ctasks.Open()
                ReturnTask()
                ctasks.Dispose()
            End If
        End If
    End Sub
    Private Sub ReturnTask()
        Dim typ As String = lbltyp.Value
        lbltyp.Value = ""
        If typ = "add" Then
            AddTask("yes")
        ElseIf typ = "save" Then
            SaveTask("yes")
        End If
        Dim tid As String = lbltaskid.Value
        tasktype = lbltasktype.Value
        pdm = lblpdm.Value

        Dim desc As String
        If typ <> "reg" Then
            desc = txtmemo.Text
        Else
            desc = lblmemo.Value
        End If
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", ":", , , vbTextCompare)
        Dim tasktypestr, pdmstr As String
        tasktypestr = ddtype.SelectedItem.ToString
        tasktypestr = Replace(tasktypestr, "'", Chr(180), , , vbTextCompare)
        tasktypestr = Replace(tasktypestr, "--", "-", , , vbTextCompare)
        tasktypestr = Replace(tasktypestr, ";", ":", , , vbTextCompare)
        pdmstr = ddpt.SelectedItem.ToString
        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_updateFromCommon1tpm @tid, @desc"
        Dim param0 = New SqlParameter("@tid", SqlDbType.Int)
        param0.Value = tid
        cmd.Parameters.Add(param0)
        Dim param01 = New SqlParameter("@desc", SqlDbType.VarChar)
        param01.Value = desc
        cmd.Parameters.Add(param01)
        ctasks.UpdateHack(cmd)
        'sql = "update pmTasks set taskdesc = '" & desc & "' where pmtskid = '" & tid & "'"
        'Dim thid As Integer
        'sql = "insert into taskhold (taskdesc) values ('" & desc & "') select @@identity"
        'thid = ctasks.Scalar(sql)
        'lbltaskholdid.Value = thid
        If rbtt.Checked = True Then
            sql = "update pmTaskstpm set tasktype = '" & tasktypestr & "', " _
            + "ttid = '" & tasktype & "' where pmtskid = '" & tid & "'"
            'sql = "update taskhold set tasktype = '" & tasktypestr & "', " _
            '+ "ttid = '" & tasktype & "' where taskholdid = '" & thid & "'"
            ctasks.Update(sql)
        End If
        If rbpt.Checked = True Then
            If ddpt.SelectedIndex <> 0 Then
                sql = "update pmTaskstpm set pretech = '" & pdmstr & "', ptid = '" & pdm & "' " _
                + "where pmtskid = '" & tid & "'"
                'sql = "update taskhold set pretech = '" & pdmstr & "', ptid = '" & pdm & "' " _
                '+ "where taskholdid = '" & thid & "'"
                ctasks.Update(sql)
            End If
        End If
        sid = lblsid.Value
        sql = "usp_updateTaskDDIndexestpm '" & taskid & "', '" & sid & "'"
        ctasks.Update(sql)

        lblexit.Value = "yes"
    End Sub
    Private Sub SaveTask(Optional ByVal ret As String = "no")
        Dim tid As String = lblctaskid.Value
        lblctaskid.Value = ""
        Dim desc As String
        Dim cnt As Integer
        tasktype = lbltasktype.Value
        pdm = lblpdm.Value
        desc = txtmemo.Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", ":", , , vbTextCompare)
        sql = "select count(*) from commontasks where description = '" & desc & "' and ttid = '" & tasktype & "' and ptid = '" & pdm & "'"
        cnt = ctasks.Scalar(sql)
        If cnt = 0 Then
            sql = "update commontasks set description = '" & desc & "' where ctaskid = '" & tid & "'"
            ctasks.Update(sql)
            If ret = "no" Then
                Dim strMessage As String =  tmod.getmsg("cdstr505" , "commontaskstpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Else
            If ret = "no" Then
                Dim strMessage As String =  tmod.getmsg("cdstr506" , "commontaskstpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If
    End Sub
    Private Sub AddTask(Optional ByVal ret As String = "no")
        Dim desc As String
        Dim cnt As Integer
        tasktype = lbltasktype.Value
        pdm = lblpdm.Value

        desc = txtmemo.Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", ":", , , vbTextCompare)
        sql = "select count(*) from commontasks where description = '" & desc & "' and ttid = '" & tasktype & "' and ptid = '" & pdm & "'"
        cnt = ctasks.Scalar(sql)
        If cnt = 0 Then
            Dim tasktypestr, pdmstr As String
            tasktypestr = ddtype.SelectedItem.ToString
            pdmstr = ddpt.SelectedItem.ToString
            sql = "insert into commontasks (description, ttid, tasktype, ptid, pretech) values " _
            + "('" & desc & "','" & tasktype & "','" & tasktypestr & "','" & pdm & "','" & pdmstr & "')"
            ctasks.Update(sql)
            If ret = "no" Then
                Dim strMessage As String =  tmod.getmsg("cdstr507" , "commontaskstpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

        Else
            If ret = "no" Then
                Dim strMessage As String =  tmod.getmsg("cdstr508" , "commontaskstpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

        End If


    End Sub
    Private Sub Refreshit()
        txtcompnum.Text = ""
        txtcompdesc.Text = ""
        txtcompdesig.Text = ""
        txtcompspl.Text = ""
        txtfunc.Text = ""
        txtfuncspl.Text = ""
        txtkey.Text = ""
        rbt.Checked = False
        rbc.Checked = True
        tdpm.Attributes.Add("class", "details")
        tasktype = lblorigtasktype.Value
        lbltasktype.Value = lblorigtasktype.Value
        pdm = lblorigpdm.Value
        lblpdm.Value = lblorigpdm.Value
        ctasks.Open()
        If tasktype <> "" Then
            Try
                ddtype.SelectedValue = tasktype
                If Mid(tasktype, 1, 1) = "4" Then
                    Try
                        ddpt.SelectedValue = pdm
                    Catch ex As Exception

                    End Try
                Else
                    ddpt.Enabled = False
                End If
            Catch ex As Exception

            End Try
        End If
        GetTasks()
        PopFail()
        ctasks.Dispose()
    End Sub
    Private Sub PopFail()
        cid = lblcid.Value
        Dim dt, val, filt As String
        dt = "FailureModes"
        val = "failid, failuremode"
        filt = " where compid = '" & cid & "'"
        dr = ctasks.GetList(dt, val, filt)
        lbfailmaster.DataSource = dr
        lbfailmaster.DataTextField = "failuremode"
        lbfailmaster.DataValueField = "failid"
        lbfailmaster.DataBind()
        dr.Close()
    End Sub
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        'lbltasktype.Value = ddtype.SelectedValue
        ctasks.Open()
        GetTasks()
        ctasks.Dispose()
    End Sub
    Private Sub GetTasks()
        Dim locstr, desc, typ, locid, pd, tid As String
        Dim noflag As Integer = 0
        Dim img As String = "../images/appbuttons/minibuttons/magnifier.gif"
        Dim img1 As String = "../images/appbuttons/minibuttons/2PX.gif"
        Dim msg As String = "onmouseover = ""return overlib('" & tmod.getov("cov147" , "commontaskstpm.aspx.vb") & "', ABOVE, LEFT)"" onmouseout = ""return nd()"""
        Dim msg1 As String = "onmouseover = ""return overlib('" & tmod.getov("cov148" , "commontaskstpm.aspx.vb") & "', ABOVE, LEFT)"" onmouseout = ""return nd()"""
        Dim simg As String = "<img src=""" & img & """ onclick=""lookup();""" & msg & ">"
        Dim spimg As String = "<img src=""" & img & """ onclick=""jumpto('" & desc & "');""" & msg1 & ">" 'locstr

        Dim sb As New StringBuilder
        sb.Append("<table width=""660"" cellpadding=""0"">")
        sb.Append("<tr><td>")

        sb.Append("<tr><td colspan=""4"" valign=""top""><div style=""OVERFLOW: auto; WIDTH: 660px;  HEIGHT: 160px"">")
        sb.Append("<table width=""640"" cellpadding=""1"">")
        sb.Append("<tr><td class=""btmmenu plainlabel"" width=""300"">" & tmod.getlbl("cdlbl151" , "commontaskstpm.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""20""><img src=" & img & "></td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""160"">" & tmod.getlbl("cdlbl152" , "commontaskstpm.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""160"">PdM</td></tr>")

        tasktype = lbltasktype.Value
        pdm = lblpdm.Value

        If Mid(tasktype, 1, 1) <> "4" Then
            filter = "t.ttid = '" & tasktype & "' "
        Else
            filter = "t.ttid = '" & tasktype & "' and t.pretech = '" & pdm & "' "
        End If
        Dim keyword As String = txtkey.Text
        If keyword <> "" Then
            filter += "and t.description like '%" & keyword & "%' "
        End If
        Dim compnum, compdesc, compdesig, compspl, func, funcspl, fail, compstr, comstr, tskstr, ftskstr As String
        If rbc.Checked = True Then
            lblmode.Value = "common"
            imgsavecommon.Visible = True
            tdpm.Attributes.Add("class", "details")
            sql = "select * from commontasks t where " & filter
        Else
            tdpm.Attributes.Add("class", "view")
            lblmode.Value = "lookup"
            imgsavecommon.Visible = False
            compnum = txtcompnum.Text
            compdesc = txtcompdesc.Text
            compdesig = txtcompdesig.Text
            compspl = txtcompspl.Text
            func = txtfunc.Text
            funcspl = txtfuncspl.Text
            If compnum <> "" Then
                filter += "and c.compnum like '%" & compnum & "%' "
            End If
            If compdesc <> "" Then
                filter += "and c.compdesc like '%" & compdesc & "%' "
            End If
            If compdesig <> "" Then
                filter += "and c.desig like '%" & compdesig & "%' "
            End If
            If compspl <> "" Then
                filter += "and c.spl like '%" & compspl & "%' "
            End If
            If func <> "" Then
                filter += "and f.func like '%" & func & "%' "
            End If
            If funcspl <> "" Then
                filter += "and f.spl like '%" & funcspl & "%' "
            End If
            Dim fi As String
            Dim Item As ListItem
            For Each Item In lbfailmaster.Items
                If Item.Selected Then
                    fi = Item.Value.ToString
                    If fail = "" Then
                        fail = "'" & fi & "'"
                    Else
                        fail += ",'" & fi & "'"
                    End If
                End If
            Next
            If fail <> "" Then
                If compnum <> "" Then
                    sql = "select distinct t.comid from pmtaskstpm t left join components c on c.compnum = t.compnum " _
                    + "left join functions f on f.func_id = t.funcid where " & filter
                    dr = ctasks.GetRdrData(sql)
                    While dr.Read
                        If compstr = "" Then
                            compstr = "'" & dr.Item("comid").ToString & "'"
                        Else
                            compstr += ",'" & dr.Item("comid").ToString & "'"
                        End If
                    End While
                    dr.Close()
                    If compstr <> "" Then
                        sql = "select distinct comid from componentfailmodes where failid in (" & fail & ") and " _
                        + "comid in (" & compstr & ")"
                        dr = ctasks.GetRdrData(sql)
                        While dr.Read
                            If comstr = "" Then
                                comstr = "'" & dr.Item("comid").ToString & "'"
                            Else
                                comstr += ",'" & dr.Item("comid").ToString & "'"
                            End If
                        End While
                        dr.Close()
                        If comstr <> "" Then
                            filter += "and t.comid in (" & comstr & ") "
                            sql = "select distinct t.taskdesc, t.pmtskid, t.tasktype, t.pretech from pmtaskstpm t left join components c on c.compnum = t.compnum " _
                            + "left join functions f on f.func_id = t.funcid where " & filter
                        Else
                            'Msg?
                            noflag = 1
                        End If
                    Else
                        'Msg?
                        noflag = 1
                    End If
                Else
                    sql = "select distinct t.pmtskid from pmtaskstpm t left join components c on c.compnum = t.compnum " _
                     + "left join functions f on f.func_id = t.funcid where " & filter
                    dr = ctasks.GetRdrData(sql)
                    While dr.Read
                        If tskstr = "" Then
                            tskstr = "'" & dr.Item("pmtskid").ToString & "'"
                        Else
                            tskstr += ",'" & dr.Item("pmtskid").ToString & "'"
                        End If
                    End While
                    dr.Close()
                    If tskstr <> "" Then
                        sql = "select distinct taskid from pmtaskfailmodestpm where parentfailid in (" & fail & ") and " _
                        + "taskid in (" & tskstr & ")"
                        dr = ctasks.GetRdrData(sql)
                        While dr.Read
                            If ftskstr = "" Then
                                ftskstr = "'" & dr.Item("taskid").ToString & "'"
                            Else
                                ftskstr += ",'" & dr.Item("taskid").ToString & "'"
                            End If
                        End While
                        dr.Close()
                        If ftskstr <> "" Then
                            filter += "and t.pmtskid in (" & ftskstr & ") "
                            sql = "select distinct t.taskdesc, t.pmtskid, t.tasktype, t.pretech from pmtaskstpm t left join components c on c.compnum = t.compnum " _
                            + "left join functions f on f.func_id = t.funcid where " & filter
                        Else
                            'Msg?
                            noflag = 1
                        End If
                    Else
                        'Msg?
                        noflag = 1
                    End If
                End If




            Else
                sql = "select distinct t.taskdesc, t.pmtskid, t.tasktype, t.pretech from pmtaskstpm t left join components c on c.compnum = t.compnum " _
                + "left join functions f on f.func_id = t.funcid where " & filter
            End If
        End If
        Dim deschold As String
        If noflag = 0 Then
            If rbc.Checked = True Then
                filter += "and t.description is not null and len(description) <> 0 "
            Else
                filter += "and t.taskdesc is not null and len(taskdesc) <> 0 "
            End If
            dr = ctasks.GetRdrData(sql)
            While dr.Read
                If rbc.Checked = True Then
                    desc = dr.Item("description").ToString
                    tid = dr.Item("ctaskid").ToString
                Else
                    desc = dr.Item("taskdesc").ToString
                    tid = dr.Item("pmtskid").ToString
                End If

                typ = dr.Item("tasktype").ToString
                pd = dr.Item("pretech").ToString
                If desc <> "" Then
                    If deschold <> desc Then
                        deschold = desc
                        ro = lblro.Value
                        If ro = "1" Then
                            sb.Append("<tr><td class=""plainlabel grayborder"" valign=""top""><a href=""#"" class=""A1"">" & desc & "</td>")
                        Else
                            sb.Append("<tr><td class=""plainlabel grayborder"" valign=""top""><a href=""#"" class=""A1"" onclick=""getthis('" & desc & "')"">" & desc & "</td>")
                        End If

                        sb.Append("<td class=""plainlabel grayborder"" valign=""top""><img src=""" & img & """ onclick=""jumpto('" & tid & "','" & desc & "');""" & msg1 & "></td>")
                        sb.Append("<td class=""plainlabel grayborder"" valign=""top"">" & typ & "</td>")
                        If pd <> "" Then
                            sb.Append("<td class=""plainlabel grayborder"" valign=""top"">" & pd & "</td></tr>")
                        Else
                            sb.Append("<td class=""plainlabel grayborder"" valign=""top"">&nbsp;</td></tr>")
                        End If
                    End If
                End If
            End While

            dr.Close()

            Dim i As Integer
            For i = 0 To 5
                sb.Append("<tr><td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")
            Next
        Else
            Dim i As Integer
            For i = 0 To 10
                sb.Append("<tr><td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")
            Next
        End If

        sb.Append("</table></div></td></tr>")
        sb.Append("</table>")
        tdtasks.InnerHtml = sb.ToString

    End Sub
    Private Sub GetLists()
        cid = lblcid.Value




        sql = "select ptid, pretech " _
        + "from pmPreTech where compid = '" & cid & "' or compid = '0' order by compid"
        dr = ctasks.GetRdrData(sql)
        ddpt.DataSource = dr
        ddpt.DataTextField = "pretech"
        ddpt.DataValueField = "ptid"
        ddpt.DataBind()
        dr.Close()
        ddpt.Items.Insert(0, New ListItem("None"))
        ddpt.Items(0).Value = 0

        sql = "select ttid, tasktype " _
      + "from pmTaskTypes where compid = '" & cid & "' or compid = '0' and tasktype <> 'Select' order by compid"
        dr = ctasks.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataTextField = "tasktype"
        ddtype.DataValueField = "ttid"
        ddtype.DataBind()
        dr.Close()
        ddtype.Items.Insert(0, New ListItem("Select"))
        ddtype.Items(0).Value = 0


    End Sub


    Private Sub ddtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddtype.SelectedIndexChanged
        If ddtype.SelectedIndex <> 0 Then
            Dim tasktypestr As String
            tasktype = ddtype.SelectedValue
            lbltasktype.Value = tasktype
            tasktypestr = ddtype.SelectedItem.ToString
            If tasktype <> "" Then
                Try
                    ddtype.SelectedValue = tasktype
                    If Mid(tasktypestr, 1, 1) = "4" Then
                        ddpt.Enabled = True
                    Else
                        ddpt.Enabled = False
                        ctasks.Open()
                        GetTasks()
                        ctasks.Dispose()
                    End If
                Catch ex As Exception

                End Try
            End If
        End If
    End Sub

    Private Sub ddpt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddpt.SelectedIndexChanged
        If ddpt.SelectedIndex <> 0 Then
            ctasks.Open()
            GetTasks()
            ctasks.Dispose()
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1221.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1221")
        Catch ex As Exception
        End Try
        Try
            lang1222.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1222")
        Catch ex As Exception
        End Try
        Try
            lang1223.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1223")
        Catch ex As Exception
        End Try
        Try
            lang1224.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1224")
        Catch ex As Exception
        End Try
        Try
            lang1225.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1225")
        Catch ex As Exception
        End Try
        Try
            lang1226.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1226")
        Catch ex As Exception
        End Try
        Try
            lang1227.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1227")
        Catch ex As Exception
        End Try
        Try
            lang1228.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1228")
        Catch ex As Exception
        End Try
        Try
            lang1229.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1229")
        Catch ex As Exception
        End Try
        Try
            lang1230.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1230")
        Catch ex As Exception
        End Try
        Try
            lang1231.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1231")
        Catch ex As Exception
        End Try
        Try
            lang1232.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1232")
        Catch ex As Exception
        End Try
        Try
            lang1233.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1233")
        Catch ex As Exception
        End Try
        Try
            lang1234.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1234")
        Catch ex As Exception
        End Try
        Try
            lang1235.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1235")
        Catch ex As Exception
        End Try
        Try
            lang1236.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1236")
        Catch ex As Exception
        End Try
        Try
            lang1237.Text = axlabs.GetASPXPage("commontaskstpm.aspx", "lang1237")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            ibaddnew.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontaskstpm.aspx", "ibaddnew") & "', ABOVE, LEFT)")
            ibaddnew.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsavecommon.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontaskstpm.aspx", "imgsavecommon") & "', ABOVE, LEFT)")
            imgsavecommon.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ir1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontaskstpm.aspx", "ir1") & "', ABOVE, LEFT)")
            ir1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ir2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontaskstpm.aspx", "ir2") & "', ABOVE, LEFT)")
            ir2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ir3.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontaskstpm.aspx", "ir3") & "', ABOVE, LEFT)")
            ir3.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid173.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontaskstpm.aspx", "ovid173") & "', ABOVE, LEFT)")
            ovid173.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid174.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontaskstpm.aspx", "ovid174") & "', ABOVE, LEFT)")
            ovid174.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid175.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontaskstpm.aspx", "ovid175") & "', ABOVE, LEFT)")
            ovid175.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid176.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontaskstpm.aspx", "ovid176") & "', ABOVE, LEFT)")
            ovid176.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid177.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontaskstpm.aspx", "ovid177") & "', ABOVE, LEFT)")
            ovid177.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid178.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontaskstpm.aspx", "ovid178") & "', ABOVE, LEFT)")
            ovid178.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid179.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontaskstpm.aspx", "ovid179") & "', ABOVE, LEFT)")
            ovid179.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
