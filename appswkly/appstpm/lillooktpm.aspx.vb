

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class lillooktpm
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim ctasks As New Utilities
    Dim sql As String
    Protected WithEvents tdtasks As System.Web.UI.HtmlControls.HtmlTableCell
    Dim dr As SqlDataReader
    Dim list, col, table, ro As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            list = Request.QueryString("list").ToString
            col = Request.QueryString("col").ToString
            ctasks.Open()
            GetList(list, col)
            ctasks.Dispose()
        End If
    End Sub
    Private Sub GetList(ByVal list As String, ByVal col As String)

        Dim sb As New StringBuilder
        Dim val As String
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 300px;  HEIGHT: 160px"">")
        sb.Append("<table width=""280"" cellpadding=""1"">")
        If list = "comp" Then
            table = "components"
        Else
            table = "functions"
        End If
        sql = "select distinct " & col & " from " & table
        dr = ctasks.GetRdrData(sql)
        While dr.Read
            val = dr.Item(col).ToString
            val = Replace(val, "'", Chr(180), , , vbTextCompare)
            val = Replace(val, """", Chr(180), , , vbTextCompare)
            sb.Append("<tr><td class=""plainlabel""><a href=""#"" class=""A1"" onclick=""getthis('" & val & "')"">" & val & "</td></tr>" & vbCrLf)
        End While
        dr.Close()

        sb.Append("</table></div>")
        tdtasks.InnerHtml = sb.ToString

    End Sub
End Class
