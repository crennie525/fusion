

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class tpmtasks
    Inherits System.Web.UI.Page
	Protected WithEvents ovid193 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid192 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid191 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid190 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid189 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid188 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid187 As System.Web.UI.HtmlControls.HtmlTableCell

	Protected WithEvents ovid186 As System.Web.UI.HtmlControls.HtmlTableCell

	Protected WithEvents ovid185 As System.Web.UI.HtmlControls.HtmlTableCell

	Protected WithEvents ovid184 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid183 As System.Web.UI.HtmlControls.HtmlTableCell

	Protected WithEvents ovid182 As System.Web.UI.HtmlControls.HtmlTableCell

	Protected WithEvents btnpfint As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang1309 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1308 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1307 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1306 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1305 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1304 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1303 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1302 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1301 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1300 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1299 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1298 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1297 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1296 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1295 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1294 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1293 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1292 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1291 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1290 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1289 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1288 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tl, cid, sid, did, clid, eqid, fuid, coid, sql, val, name, field, ttid, tnum, typ, lid, ro, appstr As String
    Dim co, fail, chk As String
    Dim tskcnt As Integer
    Dim tasks As New Utilities
    Dim tasksadd As New Utilities
    Dim taskssav As New Utilities
    Public dr As SqlDataReader
    Dim ds As DataSet
    Dim Tables As String = "pmtaskstpm"
    Dim PK As String = "pmtskid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 1
    Dim Fields As String = "*, haspm = (select e.hastpm from equipment e where e.eqid = pmtaskstpm.eqid)"
    Dim Filter As String = ""
    Dim CntFilter As String = ""
    Dim TaskFilter As String = ""
    Dim Group As String = ""
    Dim Sort As String = "tasknum, subtask asc"
    Dim strScript, login, username As String
    Protected WithEvents cb1mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnoeq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgcopycomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnaddnewfail As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromreusedis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblfreqprev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbcust As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblcbcust As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1mon As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1tue As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1wed As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1thu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1fri As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1sat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1sun As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2o As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2mon As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2tue As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2wed As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2thu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2fri As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2sat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2sun As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3o As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3mon As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3tue As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3wed As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3thu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3fri As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3sat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3sun As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1o As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtfreq As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbltpmhold As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim haspm As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddcomp As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Label25 As System.Web.UI.WebControls.Label
    Protected WithEvents txtcQty As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbCompFM As System.Web.UI.WebControls.ListBox
    Protected WithEvents ibReuse As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfaillist As System.Web.UI.WebControls.ListBox
    Protected WithEvents ibToTask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibFromTask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents txtdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddpt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtqty As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttr As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpfint As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddeqstat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtrdt As System.Web.UI.WebControls.TextBox
    Protected WithEvents cbloto As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cbcs As System.Web.UI.WebControls.CheckBox

    Protected WithEvents imgClock As System.Web.UI.WebControls.Image
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents lblcnt As System.Web.UI.WebControls.Label
    Protected WithEvents Label26 As System.Web.UI.WebControls.Label
    Protected WithEvents lblsubcount As System.Web.UI.WebControls.Label
    Protected WithEvents btnaddtsk As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnaddsubtask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btndeltask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibCancel As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnsavetask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents msglbl As System.Web.UI.WebControls.Label
    Protected WithEvents btnaddcomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnlookup As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnlookup2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img5 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents trh4 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents btnStart As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnPrev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnNext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnEnd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnedittask As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnsav As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ggrid As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents sgrid As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iflst As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblptid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfail As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpgholder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblst As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsvchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblenable As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrsb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lot As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhaspm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
            username = HttpContext.Current.Session("username").ToString()
            lblusername.Value = username
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If lbllog.Value <> "no" Then
            'Dim ftr As Footer
            'ftr = Page.FindControl("Footer1")
            'ftr.loc = Footer.eloc.divpg
            If lblsvchk.Value = "1" Then
                goNext()
            ElseIf lblsvchk.Value = "2" Then
                goPrev()
            ElseIf lblsvchk.Value = "3" Then
                'goSubNext()
            ElseIf lblsvchk.Value = "4" Then
                'goSubPrev()
            ElseIf lblsvchk.Value = "5" Then
                GoFirst()
            ElseIf lblsvchk.Value = "6" Then
                GoLast()
            ElseIf lblsvchk.Value = "7" Then
                GoNav()
            End If
            If Request.Form("lblcompchk") = "1" Then
                lblcompchk.Value = "0"
                tasks.Open()
                PopComp()
                'added in case components with tasks were copied on return
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                'end new
                coid = lblco.Value
                Try
                    ddcomp.SelectedValue = coid
                    SaveTaskComp()
                Catch ex As Exception

                End Try
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "2" Then
                lblcompchk.Value = "0"
                tasks.Open()
                PopComp()
                'added in case components deleted on return
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                'end new
                coid = lblco.Value
                Try
                    ddcomp.SelectedValue = coid
                Catch ex As Exception

                End Try
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "3" Then
                lblcompchk.Value = "0"
                tasksadd.Open()
                SaveTask2()
                tasksadd.Dispose()
            ElseIf Request.Form("lblcompchk") = "4" Then
                lblcompchk.Value = "0"
                tasks.Open()
                GetLists()
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "5" Then
                lblcompchk.Value = "0"
                tasks.Open()
                SubCount(PageNumber, Filter)
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "6" Then
                lblcompchk.Value = "0"
                tasks.Open()
                'LoadCommon()
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            End If
            If Request.Form("lblcompfailchk") = "1" Then
                lblcompfailchk.Value = "0"
                AddFail()
            End If
            Dim start As String
            Try
                start = Request.QueryString("start").ToString
            Catch ex As Exception
                start = "yes"
            End Try

            If start = "no" Then
                lblpg.Text = "0"
                lblcnt.Text = "0"
                'lblspg.Text = "0"
                lblsubcount.Text = "0"
            End If
            If Not IsPostBack Then
                If start = "yes" Then
                    appstr = HttpContext.Current.Session("appstr").ToString()
                    CheckApps(appstr)
                    lblcnt.Text = "0"
                    lblsvchk.Value = "0"
                    lblenable.Value = "1"
                    tl = Request.QueryString("tl").ToString
                    lbltasklev.Value = tl
                    cid = Request.QueryString("cid").ToString
                    lblcid.Value = cid
                    sid = Request.QueryString("sid").ToString
                    lblsid.Value = sid
                    did = Request.QueryString("did").ToString
                    lbldid.Value = did
                    clid = Request.QueryString("clid").ToString
                    lblclid.Value = clid
                    eqid = Request.QueryString("eqid").ToString
                    lbleqid.Value = eqid
                    chk = Request.QueryString("chk").ToString
                    typ = Request.QueryString("typ").ToString
                    lbltyp.Value = typ
                    lid = Request.QueryString("lid").ToString
                    lbllid.Value = lid
                    lblchk.Value = chk
                    tasks.Open()
                    fuid = Request.QueryString("fuid").ToString
                    lblfuid.Value = fuid
                    val = fuid
                    field = "pmtaskstpm.funcid"
                    name = "Function"
                    eqid = Request.QueryString("eqid").ToString
                    lbleqid.Value = eqid
                    GetLists()
                    lblsb.Value = "0"
                    Filter = field & " = " & val & " and pmtaskstpm.subtask = 0" ' and tpm = '1'"
                    CntFilter = field & " = " & val & " and pmtaskstpm.subtask = 0" ' and tpm = '1'"
                    lblfilt.Value = Filter
                    lblfiltcnt.Value = CntFilter
                    Dim tasknum As String
                    Try
                        tasknum = Request.QueryString("task").ToString
                        If tasknum = "" Then
                            tasknum = "0"
                        End If
                        'Filter = field & " = " & val & " and tasknum = " & tasknum
                        PageNumber = tasknum
                    Catch ex As Exception
                        tasknum = "0"
                    End Try


                    LoadPage(PageNumber, Filter)
                    btnaddtsk.Enabled = True
                    Dim lock, lockby As String
                    Dim user As String = lblusername.Value
                    'Read Only
                    Try
                        ro = HttpContext.Current.Session("ro").ToString
                    Catch ex As Exception
                        ro = "0"
                    End Try
                    lblro.Value = ro

                    'End Read Only
                    lock = CheckLock(eqid)
                    If lock = "1" Then
                        lockby = lbllockedby.Value
                        If lockby = user Then
                            lbllock.Value = "0"

                        Else
                            lblro.Value = "1" '***use instead?
                            ro = "1"
                            Dim strMessage As String = tmod.getmsg("cdstr536", "tpmtasks.aspx.vb") & " " & lockby & "."
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        End If
                    ElseIf lock = "0" Then
                        'LockRecord(user, eq)
                    End If
                    If ro = "1" Then
                        ibToTask.Visible = False
                        ibFromTask.Visible = False
                        ibReuse.Visible = False
                        todis.Attributes.Add("class", "view")
                        fromdis.Attributes.Add("class", "view")
                        fromreusedis.Attributes.Add("class", "view")
                        btnedittask.Attributes.Add("src", "../images/appbuttons/minibuttons/lilpendis.gif")
                        btnaddtsk.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                        btnaddtsk.Enabled = False
                        btndeltask.Attributes.Add("src", "../images/appbuttons/minibuttons/deldis.gif")
                        btndeltask.Enabled = False
                        ibCancel.Attributes.Add("src", "../images/appbuttons/minibuttons/candisk1dis.gif")
                        ibCancel.Enabled = False
                        btnsav.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                        btnsav.Attributes.Add("onclick", "")
                    End If
                    tasks.Dispose()

                Else
                    btnaddtsk.Enabled = False
                End If

                Dim locked As String = lbllock.Value
                If locked = "1" Then
                    'btnedittask.Attributes.Add("class", "details")
                    'btnaddtsk.Attributes.Add("class", "details")
                    'btndeltask.Attributes.Add("class", "details")
                    'ibCancel.Attributes.Add("class", "details")
                    'btnsav.Attributes.Add("class", "details")
                    'ggrid.Attributes.Add("class", "details")
                    'btnaddcomp.Attributes.Add("class", "details")
                End If
                'Disable all fields
                If ro <> "1" Then
                    cb1o.Disabled = True
                    cb1.Disabled = True
                    cb1mon.Disabled = True
                    cb1tue.Disabled = True
                    cb1wed.Disabled = True
                    cb1thu.Disabled = True
                    cb1fri.Disabled = True
                    cb1sat.Disabled = True
                    cb1sun.Disabled = True

                    cb2o.Disabled = True
                    cb2.Disabled = True
                    cb2mon.Disabled = True
                    cb2tue.Disabled = True
                    cb2wed.Disabled = True
                    cb2thu.Disabled = True
                    cb2fri.Disabled = True
                    cb2sat.Disabled = True
                    cb2sun.Disabled = True

                    cb3o.Disabled = True
                    cb3.Disabled = True
                    cb3mon.Disabled = True
                    cb3tue.Disabled = True
                    cb3wed.Disabled = True
                    cb3thu.Disabled = True
                    cb3fri.Disabled = True
                    cb3sat.Disabled = True
                    cb3sun.Disabled = True

                    cbcust.Disabled = True

                    ddcomp.Enabled = False
                    ''lbfaillist.Enabled = False
                    ''lbfailmodes.Enabled = False
                    ibToTask.Enabled = False
                    ibFromTask.Enabled = False
                    txtdesc.Enabled = False
                    ddtype.Enabled = False
                    txtfreq.Enabled = False
                    txtpfint.Enabled = False

                    txtqty.Enabled = False
                    txttr.Enabled = False
                    txtrdt.Enabled = False
                    'ddpt.Enabled = False
                    ddeqstat.Enabled = False
                    'cbloto.Enabled = False
                    'cbcs.Enabled = False
                    lblenable.Value = "1"
                    txtcQty.Enabled = False
                    'lbCompFM.Enabled = False
                    'buttons

                    btnaddsubtask.Enabled = False
                    btndeltask.Enabled = False
                    ibCancel.Enabled = False
                    btnsavetask.Enabled = False
                End If

                'end disable
                imgClock.Visible = False
                btnsavetask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov155" , "tpmtasks.aspx.vb") & "')")
                btnsavetask.Attributes.Add("onmouseout", "return nd()")
                btndeltask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov156" , "tpmtasks.aspx.vb") & "')")
                btndeltask.Attributes.Add("onmouseout", "return nd()")
                btnaddsubtask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov157" , "tpmtasks.aspx.vb") & "')")
                btnaddsubtask.Attributes.Add("onmouseout", "return nd()")
                btnaddtsk.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov158" , "tpmtasks.aspx.vb") & "')")
                btnaddtsk.Attributes.Add("onmouseout", "return nd()")
                btnaddtsk.Attributes.Add("onclick", "FreezeScreen('Your Data is Being Processed...');")
                btnedittask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov159" , "tpmtasks.aspx.vb") & "')")
                btnedittask.Attributes.Add("onmouseout", "return nd()")
                ibToTask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov160" , "tpmtasks.aspx.vb") & "')")
                ibToTask.Attributes.Add("onmouseout", "return nd()")
                'ibToTask.Attributes.Add("onclick", "DisableButton(this);")
                ibReuse.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov161" , "tpmtasks.aspx.vb") & "')")
                ibReuse.Attributes.Add("onmouseout", "return nd()")
                'ibReuse.Attributes.Add("onclick", "DisableButton(this);")
                ibCancel.Attributes.Add("onmouseout", "return nd()")
                ibCancel.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov162" , "tpmtasks.aspx.vb") & "')")
                ibFromTask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov163" , "tpmtasks.aspx.vb") & "')")
                ibFromTask.Attributes.Add("onmouseout", "return nd()")
                'ibFromTask.Attributes.Add("onclick", "DisableButton(this);")
                btnPrev.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov164" , "tpmtasks.aspx.vb") & "')")
                btnPrev.Attributes.Add("onmouseout", "return nd()")
                btnPrev.Attributes.Add("onclick", "confirmExit('tb', 'dev');")
                btnStart.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov165" , "tpmtasks.aspx.vb") & "')")
                btnStart.Attributes.Add("onmouseout", "return nd()")
                btnStart.Attributes.Add("onclick", "confirmExit('ts', 'dev');")
                btnNext.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov166" , "tpmtasks.aspx.vb") & "')")
                btnNext.Attributes.Add("onmouseout", "return nd()")
                btnNext.Attributes.Add("onclick", "confirmExit('tf', 'dev');")
                btnEnd.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov167" , "tpmtasks.aspx.vb") & "')")
                btnEnd.Attributes.Add("onmouseout", "return nd()")
                btnEnd.Attributes.Add("onclick", "confirmExit('te', 'dev');")
                btnlookup.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov168" , "tpmtasks.aspx.vb") & "')")
                btnlookup.Attributes.Add("onmouseout", "return nd()")
                btnlookup2.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov169" , "tpmtasks.aspx.vb") & "')")
                btnlookup2.Attributes.Add("onmouseout", "return nd()")
                ddcomp.Attributes.Add("onchange", "chngchk();")
                'ddtype.Attributes.Add("onchange", "GetType('d', this.value);")
                txttr.Attributes.Add("onkeyup", "filldown(this.value);")
                ddeqstat.Attributes.Add("onchange", "zerodt(this.value);")
                txtfreq.Attributes.Add("onchange", "checkfreq();")

                cb1o.Attributes.Add("onclick", "checkcb1o('cb1o');")
                cb2o.Attributes.Add("onclick", "checkcb1o('cb2o');")
                cb3o.Attributes.Add("onclick", "checkcb1o('cb3o');")

                cb1.Attributes.Add("onclick", "checkcb1();")
                cb2.Attributes.Add("onclick", "checkcb2();")
                cb2.Attributes.Add("onclick", "checkcb2();")

                cb1mon.Attributes.Add("onclick", "checkday1('cb1mon');")
                cb1tue.Attributes.Add("onclick", "checkday1('cb1tue');")
                cb1wed.Attributes.Add("onclick", "checkday1('cb1wed');")
                cb1thu.Attributes.Add("onclick", "checkday1('cb1thu');")
                cb1fri.Attributes.Add("onclick", "checkday1('cb1fri');")
                cb1sat.Attributes.Add("onclick", "checkday1('cb1sat');")
                cb1sun.Attributes.Add("onclick", "checkday1('cb1sun');")

                cb2mon.Attributes.Add("onclick", "checkday2('cb2mon');")
                cb2tue.Attributes.Add("onclick", "checkday2('cb2tue');")
                cb2wed.Attributes.Add("onclick", "checkday2('cb2wed');")
                cb2thu.Attributes.Add("onclick", "checkday2('cb2thu');")
                cb2fri.Attributes.Add("onclick", "checkday2('cb2fri');")
                cb2sat.Attributes.Add("onclick", "checkday2('cb2sat');")
                cb2sun.Attributes.Add("onclick", "checkday2('cb2sun');")

                cb3mon.Attributes.Add("onclick", "checkday3('cb3mon');")
                cb3tue.Attributes.Add("onclick", "checkday3('cb3tue');")
                cb3wed.Attributes.Add("onclick", "checkday3('cb3wed');")
                cb3thu.Attributes.Add("onclick", "checkday3('cb3thu');")
                cb3fri.Attributes.Add("onclick", "checkday3('cb3fri');")
                cb3sat.Attributes.Add("onclick", "checkday3('cb3sat');")
                cb3sun.Attributes.Add("onclick", "checkday3('cb3sun');")
                'tasks.Dispose()

                txtfreq.Attributes.Add("onkeyup", "checkfreq17(this.value);")
            End If
        End If
    End Sub
    Private Sub CheckApps(ByVal appstr As String)
        Dim apparr() As String = appstr.Split(",")
        Dim e As String = "0"
        'eq,dev,opt,inv
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                If apparr(i) = "eq" Then
                    e = "1"
                End If
            Next
            If e <> "1" Then
                lblnoeq.Value = "1"
                btnaddcomp.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                btnaddcomp.Attributes.Add("onclick", "")
                btnaddnewfail.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                btnaddnewfail.Attributes.Add("onclick", "")
                imgcopycomp.Attributes.Add("onclick", "")
            End If
        Else
            lblnoeq.Value = "0"
        End If

    End Sub
    Private Function CheckLock(ByVal eqid As String) As String
        Dim lock As String
        sql = "select locked, lockedby from equipment where eqid = '" & eqid & "'"
        Try
            dr = tasks.GetRdrData(sql)
            While dr.Read
                lock = dr.Item("locked").ToString
                lbllock.Value = dr.Item("locked").ToString
                lbllockedby.Value = dr.Item("lockedby").ToString
            End While
            dr.Close()
        Catch ex As Exception

        End Try
        Return lock
    End Function
    Private Sub GoNav()
        Dim pg As String = pgflag.Value
        If pg <> "0" Then
            Filter = lblfilt.Value
            lblsb.Value = "0"
            PageNumber = pg
            tasks.Open()
            LoadPage(PageNumber, Filter)
            tasks.Dispose()
            lblenable.Value = "1"
        End If
    End Sub
    Private Function SubCount(ByVal PageNumber As Integer, ByVal Filter As String) As Integer
        Dim scnt As Integer
        fuid = lblfuid.Value
        sql = "select count(*) from pmtaskstpm where funcid = '" & fuid & "' and tasknum = " & PageNumber & " and subtask <> '0'"
        Try
            scnt = tasks.Scalar(sql)
        Catch ex As Exception
            scnt = tasksadd.Scalar(sql)
        End Try
        lblsubcount.Text = scnt
        Return scnt
    End Function
    Private Sub LoadPage(ByVal PageNumber As Integer, ByVal Filter As String)
        '**** Added for PM Manager
        eqid = lbleqid.Value
        'Try
        'lblhaspm.Value = tasks.HasPM(eqid)
        'Catch ex As Exception
        'lblhaspm.Value = tasksadd.HasPM(eqid)
        'End Try
        Dim thold As String

        If lblsb.Value = "0" Then
            Dim scnt As Integer
            scnt = SubCount(PageNumber, Filter)
            lblsubcount.Text = scnt
            lblpgholder.Value = PageNumber
            cbloto.Checked = False
            cbcs.Checked = False
        End If
        Dim tskcnt, optcnt As Integer
        tskcnt = 0
        If tskcnt = 0 Then
            CntFilter = lblfiltcnt.Value
            sql = "select count(*) from pmtaskstpm where " & CntFilter
            Try
                tskcnt = tasks.Scalar(sql)
            Catch ex As Exception
                tskcnt = tasksadd.Scalar(sql)
            End Try
        End If
        lblcnt.Text = tskcnt
        lblpg.Text = PageNumber

        If tskcnt = 0 Then
            lblpg.Text = "0"
            lblcnt.Text = tskcnt
            Dim strMessage As String =  tmod.getmsg("cdstr537" , "tpmtasks.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Dim JTables, JPK As String
            Tables = "pmtaskstpm"
            JTables = "pmtasksdays"
            PK = "pmtaskstpm.pmtskid"
            JPK = "pmtasksdays.pmtskid"
            fuid = lblfuid.Value
            Try
                dr = tasks.GetPage2(Tables, JTables, PK, JPK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            Catch ex As Exception
                dr = tasksadd.GetPage2(Tables, JTables, PK, JPK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            End Try
            Dim test As String
            test = lbltaskid.Value
            Dim cust As String
            If dr.Read Then
                txtpfint.Text = dr.Item("pfInterval").ToString
                lblt.Value = dr.Item("tasknum").ToString
                ttid = dr.Item("pmtskid").ToString
                lbltaskid.Value = ttid
                lblst.Value = dr.Item("subtask").ToString
                cust = dr.Item("cust").ToString
                thold = dr.Item("tpmhold").ToString
                lbltpmhold.Value = thold
                lblcbcust.Value = cust
                If cust = "1" Then
                    cbcust.Checked = True
                Else
                    cbcust.Checked = False
                End If
                Try
                    ddtype.SelectedValue = dr.Item("ttid").ToString
                Catch ex As Exception
                    Try
                        ddtype.SelectedIndex = 0
                    Catch ex1 As Exception

                    End Try

                End Try
                If cust <> "1" Then
                    Try

                        txtfreq.Text = dr.Item("freq").ToString 'was freq
                    Catch ex As Exception
                        Try
                            txtfreq.Text = ""
                        Catch ex1 As Exception

                        End Try

                    End Try
                Else
                    Try
                        txtfreq.Text = ""
                    Catch ex As Exception

                    End Try

                End If

                Try
                    ddeqstat.SelectedValue = dr.Item("rdid").ToString
                Catch ex As Exception
                    Try
                        ddeqstat.SelectedIndex = 0
                    Catch ex1 As Exception

                    End Try

                End Try
                Dim desc As String = dr.Item("taskdesc").ToString
                desc = Replace(desc, "&#180;", "'")
                txtdesc.Text = desc

                txtqty.Text = dr.Item("qty").ToString
                txttr.Text = dr.Item("tTime").ToString
                txtrdt.Text = dr.Item("rdt").ToString

                If dr.Item("lotoid").ToString = "1" Then
                    cbloto.Checked = True
                Else
                    cbloto.Checked = False
                End If
                If dr.Item("conid").ToString = "1" Then
                    cbcs.Checked = True
                Else
                    cbcs.Checked = False
                End If
                co = dr.Item("comid").ToString
                If Len(co) = 0 Or co = "" Then
                    co = "0"
                End If
                lblco.Value = co
                txtcQty.Text = dr.Item("cqty").ToString
                If txtcQty.Text = "" Then
                    txtcQty.Text = "1"
                End If

                If dr.Item("cb1o").ToString = "1" Then
                    lcb1o.Value = "1"
                    cb1o.Checked = True
                Else
                    lcb1o.Value = "0"
                    cb1o.Checked = False
                End If
                If dr.Item("cb1").ToString = "1" Then
                    cb1.Checked = True
                    cb1mon.Checked = True
                    cb1tue.Checked = True
                    cb1wed.Checked = True
                    cb1thu.Checked = True
                    cb1fri.Checked = True
                    cb1sat.Checked = True
                    cb1sun.Checked = True

                    lcb1.Value = "1"
                    lcb1mon.Value = "1"
                    lcb1tue.Value = "1"
                    lcb1wed.Value = "1"
                    lcb1thu.Value = "1"
                    lcb1fri.Value = "1"
                    lcb1sat.Value = "1"
                    lcb1sun.Value = "1"
                Else
                    lcb1.Value = "0"
                    cb1.Checked = False
                    If dr.Item("cb1mon").ToString = "1" Then
                        lcb1mon.Value = "1"
                        cb1mon.Checked = True
                    Else
                        lcb1mon.Value = "0"
                        cb1mon.Checked = False
                    End If
                    If dr.Item("cb1tue").ToString = "1" Then
                        lcb1tue.Value = "1"
                        cb1tue.Checked = True
                    Else
                        lcb1tue.Value = "0"
                        cb1tue.Checked = False
                    End If
                    If dr.Item("cb1wed").ToString = "1" Then
                        lcb1wed.Value = "1"
                        cb1wed.Checked = True
                    Else
                        lcb1wed.Value = "0"
                        cb1wed.Checked = False
                    End If
                    If dr.Item("cb1thu").ToString = "1" Then
                        lcb1thu.Value = "1"
                        cb1thu.Checked = True
                    Else
                        lcb1thu.Value = "0"
                        cb1thu.Checked = False
                    End If
                    If dr.Item("cb1fri").ToString = "1" Then
                        lcb1fri.Value = "1"
                        cb1fri.Checked = True
                    Else
                        lcb1fri.Value = "0"
                        cb1fri.Checked = False
                    End If
                    If dr.Item("cb1sat").ToString = "1" Then
                        lcb1sat.Value = "1"
                        cb1sat.Checked = True
                    Else
                        lcb1sat.Value = "0"
                        cb1sat.Checked = False
                    End If
                    If dr.Item("cb1sun").ToString = "1" Then
                        lcb1sun.Value = "1"
                        cb1sun.Checked = True
                    Else
                        lcb1sun.Value = "0"
                        cb1sun.Checked = False
                    End If
                End If
                'End If

                If dr.Item("cb2o").ToString = "1" Then
                    lcb2o.Value = "1"
                    cb2o.Checked = True
                Else
                    lcb2o.Value = "0"
                    cb2o.Checked = False
                End If
                If dr.Item("cb2").ToString = "1" Then
                    cb2.Checked = True
                    cb2mon.Checked = True
                    cb2tue.Checked = True
                    cb2wed.Checked = True
                    cb2thu.Checked = True
                    cb2fri.Checked = True
                    cb2sat.Checked = True
                    cb2sun.Checked = True

                    lcb2.Value = "1"
                    lcb2mon.Value = "1"
                    lcb2tue.Value = "1"
                    lcb2wed.Value = "1"
                    lcb2thu.Value = "1"
                    lcb2fri.Value = "1"
                    lcb2sat.Value = "1"
                    lcb2sun.Value = "1"
                Else
                    lcb2.Value = "0"
                    cb2.Checked = False
                    If dr.Item("cb2mon").ToString = "1" Then
                        lcb2mon.Value = "1"
                        cb2mon.Checked = True
                    Else
                        lcb2mon.Value = "0"
                        cb2mon.Checked = False
                    End If
                    If dr.Item("cb2tue").ToString = "1" Then
                        lcb2tue.Value = "1"
                        cb2tue.Checked = True
                    Else
                        lcb2tue.Value = "0"
                        cb2tue.Checked = False
                    End If
                    If dr.Item("cb2wed").ToString = "1" Then
                        lcb2wed.Value = "1"
                        cb2wed.Checked = True
                    Else
                        lcb2wed.Value = "0"
                        cb2wed.Checked = False
                    End If
                    If dr.Item("cb2thu").ToString = "1" Then
                        lcb2thu.Value = "1"
                        cb2thu.Checked = True
                    Else
                        lcb2thu.Value = "0"
                        cb2thu.Checked = False
                    End If
                    If dr.Item("cb2fri").ToString = "1" Then
                        lcb2fri.Value = "1"
                        cb2fri.Checked = True
                    Else
                        lcb2fri.Value = "0"
                        cb2fri.Checked = False
                    End If
                    If dr.Item("cb2sat").ToString = "1" Then
                        lcb2sat.Value = "1"
                        cb2sat.Checked = True
                    Else
                        lcb2sat.Value = "0"
                        cb2sat.Checked = False
                    End If
                    If dr.Item("cb2sun").ToString = "1" Then
                        lcb2sun.Value = "1"
                        cb2sun.Checked = True
                    Else
                        lcb2sun.Value = "0"
                        cb2sun.Checked = False
                    End If
                End If
                'End If
                If dr.Item("cb3o").ToString = "1" Then
                    lcb3o.Value = "1"
                    cb3o.Checked = True
                Else
                    lcb3o.Value = "0"
                    cb3o.Checked = False
                End If
                If dr.Item("cb3").ToString = "1" Then
                    cb3.Checked = True
                    cb3mon.Checked = True
                    cb3tue.Checked = True
                    cb3wed.Checked = True
                    cb3thu.Checked = True
                    cb3fri.Checked = True
                    cb3sat.Checked = True
                    cb3sun.Checked = True

                    lcb3.Value = "1"
                    lcb3mon.Value = "1"
                    lcb3tue.Value = "1"
                    lcb3wed.Value = "1"
                    lcb3thu.Value = "1"
                    lcb3fri.Value = "1"
                    lcb3sat.Value = "1"
                    lcb3sun.Value = "1"
                Else
                    lcb3.Value = "0"
                    cb3.Checked = False
                    If dr.Item("cb3mon").ToString = "1" Then
                        lcb3mon.Value = "1"
                        cb3mon.Checked = True
                    Else
                        lcb3mon.Value = "0"
                        cb3mon.Checked = False
                    End If
                    If dr.Item("cb3tue").ToString = "1" Then
                        lcb3tue.Value = "1"
                        cb3tue.Checked = True
                    Else
                        lcb3tue.Value = "0"
                        cb3tue.Checked = False
                    End If
                    If dr.Item("cb3wed").ToString = "1" Then
                        lcb3wed.Value = "1"
                        cb3wed.Checked = True
                    Else
                        lcb3wed.Value = "0"
                        cb3wed.Checked = False
                    End If
                    If dr.Item("cb3thu").ToString = "1" Then
                        lcb3thu.Value = "1"
                        cb3thu.Checked = True
                    Else
                        lcb3thu.Value = "0"
                        cb3thu.Checked = False
                    End If
                    If dr.Item("cb3fri").ToString = "1" Then
                        lcb3fri.Value = "1"
                        cb3fri.Checked = True
                    Else
                        lcb3fri.Value = "0"
                        cb3fri.Checked = False
                    End If
                    If dr.Item("cb3sat").ToString = "1" Then
                        lcb3sat.Value = "1"
                        cb3sat.Checked = True
                    Else
                        lcb3sat.Value = "0"
                        cb3sat.Checked = False
                    End If
                    If dr.Item("cb3sun").ToString = "1" Then
                        lcb3sun.Value = "1"
                        cb3sun.Checked = True
                    Else
                        lcb3sun.Value = "0"
                        cb3sun.Checked = False
                    End If
                End If
                lblhaspm.Value = dr.Item("haspm").ToString
            End If
            dr.Close()
            If co <> "0" Then
                lblco.Value = co
                Try
                    ddcomp.SelectedValue = co
                Catch ex As Exception

                End Try
                PopCompFailList(co)
                PopFailList(co)
                PopTaskFailModes(co)
                'UpdateFailStats(co)
                lblco.Value = co
                chk = "comp"
            Else
                Try
                    ddcomp.SelectedValue = co
                Catch ex As Exception

                End Try
                lbfaillist.Items.Clear()
                lbfailmodes.Items.Clear()
                lbCompFM.Items.Clear()
                lblco.Value = co
                chk = "comp"
            End If
            lblpar.Value = "comp"
            'UpDateRevs(ttid)
            'If lblsb.Value = "0" Then
            'lblpg.Text = PageNumber
            'Else
            'lblpg.Text = lblpgholder.Value
            'PageNumber = lblpgholder.Value
            'If Len(Filter) = 0 Then
            'Filter = " subtask = 0"
            'Else
            'Filter = Filter & " and subtask = 0"
            'End If
            'sql = "select count(*) from pmtasks where " & Filter
            'Try
            'tskcnt = tasks.Scalar(sql)
            'Catch ex As Exception
            'tskcnt = tasksadd.Scalar(sql)
            'End Try
            'tdtasknav.InnerHtml = "Page# " & PageNumber & " of " & tskcnt
            'End If
            'lblcnt.Text = tskcnt
            'CheckPgNav(tskcnt, PageNumber)
            btnaddsubtask.Enabled = True

        End If
    End Sub
    Private Function SubTaskCnt(ByVal PageNumber As Integer, ByVal Filter As String) As Integer
        Dim tcnt As Integer
        CntFilter = lblfiltcnt.Value
        sql = "select count(*) from pmtaskstpm where " & CntFilter & " and tasknum <= " & PageNumber & " and subtask = '0'"
        tcnt = tasks.Scalar(sql)
        Dim scnt As Integer
        sql = "select count(*) from pmtaskstpm where " & CntFilter & " and tasknum < " & PageNumber & " and subtask <> '0'"
        scnt = tasks.Scalar(sql)
        Dim wcnt = tcnt + scnt
        Return wcnt
    End Function

    Private Sub GoFirst()
        Filter = lblfilt.Value
        lblsb.Value = "0"
        PageNumber = 1
        'Filter = Filter & " and tasknum = " & PageNumber
        tasks.Open()
        LoadPage(PageNumber, Filter)
        tasks.Dispose()
        lblenable.Value = "1"
    End Sub
    Private Sub GoLast()
        Filter = lblfilt.Value
        lblsb.Value = "0"
        PageNumber = lblcnt.Text
        'Filter = Filter & " and tasknum = " & PageNumber
        tasks.Open()
        LoadPage(PageNumber, Filter)
        tasks.Dispose()
        lblenable.Value = "1"
    End Sub
    Private Sub goNext()
        Filter = lblfilt.Value
        If lblsb.Value = "0" Then
            PageNumber = lblpg.Text
        Else
            PageNumber = lblpgholder.Value
        End If

        lblsb.Value = "0"
        PageNumber = PageNumber + 1
        'Filter = Filter & " and tasknum = " & PageNumber
        If PageNumber <= lblcnt.Text Then
            tasks.Open()
            LoadPage(PageNumber, Filter)
            tasks.Dispose()
        Else

            Dim strMessage As String =  tmod.getmsg("cdstr538" , "tpmtasks.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        lblenable.Value = "1"
    End Sub
    Private Sub goPrev()
        Filter = lblfilt.Value
        'If lblsb.Value = "0" Or lblspg.Text = "1" Then
        'PageNumber = lblpg.Text
        'Else
        PageNumber = lblpgholder.Value
        'End If
        lblsb.Value = "0"
        PageNumber = lblpg.Text
        If PageNumber > 1 Then
            If PageNumber = 1 Then
                fuid = lblfuid.Value
                Dim tcnt As Integer
                sql = "select count(*) from pmtaskstpm where funcid = '" & fuid & "' and tasknum = '0'"
                Dim task0 As New Utilities
                task0.Open()
                tcnt = task0.Scalar(sql)
                task0.Dispose()
                If tcnt = 1 Then
                    Try
                        PageNumber = PageNumber - 1
                        'Filter = Filter & " and tasknum = " & PageNumber
                        tasks.Open()
                        LoadPage(PageNumber, Filter)
                    Catch ex As Exception
                        Dim strMessage As String =  tmod.getmsg("cdstr539" , "tpmtasks.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        Exit Sub
                    End Try
                End If
            Else
                Try
                    PageNumber = PageNumber - 1
                    'Filter = Filter & " and tasknum = " & PageNumber
                    tasks.Open()
                    LoadPage(PageNumber, Filter)
                Catch ex As Exception
                    Dim strMessage As String =  tmod.getmsg("cdstr540" , "tpmtasks.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If

            'ElseIf lblspg.Text = "1" Then
            'PageNumber = PageNumber
            'Filter = Filter & " and tasknum = " & PageNumber
            'LoadPage(PageNumber, Filter)
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr541" , "tpmtasks.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        lblenable.Value = "1"
        tasks.Dispose()
    End Sub

    Private Sub UpDateRevs(ByVal Filter As String)
        sql = "select created, revised from pmtaskstpm where pmtskid = '" & Filter & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        Dim cr, rv As String
        While dr.Read
            cr = dr.Item("created").ToString
            rv = dr.Item("revised").ToString
        End While
        dr.Close()
        If Len(rv) = 0 Then
            rv = "NA"
        End If
        imgClock.Attributes.Add("onmouseover", "return overlib('Task Created: " & cr & "<br> Task Revised: " & rv & "')")
        imgClock.Attributes.Add("onmouseout", "return nd()")
    End Sub
    Private Sub CheckPgNav(ByVal tskcnt As Integer, ByVal PageNumber As Integer)
        If PageNumber = 1 Or tskcnt = 1 Then
            'btnPrev.Enabled = False
        Else
            'btnPrev.Enabled = True
        End If
        If PageNumber < tskcnt Then
            'btnNext.Enabled = True
        Else
            'btnNext.Enabled = False
        End If
    End Sub

    Private Sub GetLists()
        cid = lblcid.Value
       

        sql = "select ttid, tasktype " _
        + "from pmTaskTypes where compid = '" & cid & "' or compid = '0' and tasktype <> 'Select' and ttid in (2, 3, 5) order by compid"
        dr = tasks.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataBind()
        dr.Close()
        ddtype.Items.Insert(0, New ListItem("Select"))
        ddtype.Items(0).Value = 0

        sid = lblsid.Value




        sql = "select statid, status " _
        + "from pmStatus where compid = '" & cid & "' or compid = '0' order by compid"
        dr = tasks.GetRdrData(sql)
        ddeqstat.DataSource = dr
        ddeqstat.DataTextField = "status"
        ddeqstat.DataValueField = "statid"
        ddeqstat.DataBind()
        dr.Close()
        ddeqstat.Items.Insert(0, New ListItem("Select"))
        ddeqstat.Items(0).Value = 0
        PopComp()
    End Sub

    Private Sub btnaddtsk_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnaddtsk.Click
        lblenable.Value = "0"
        tl = lbltasklev.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        tl = 5
        tasksadd.Open()

        sql = "usp_AddTaskTPM '" & tl & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & ustr & "'"
        tasksadd.Scalar(sql)
        Filter = lblfilt.Value

        PageNumber = lblcnt.Text
        PageNumber = PageNumber + 1
        lblcnt.Text = PageNumber
        'field = "pmtasks.funcid"
        'val = fuid
        Filter = lblfilt.Value 'field & " = " & val & " and pmtasks.subtask = 0"

        LoadPage(PageNumber, Filter)
        tasksadd.Dispose()
    End Sub
    Private Sub SaveTask2()
        Dim typstr, frestr, rmtstr, ptstr, skistr, eqsstr, cqty, rdt As String
        Dim pf, t, st, tid, typ, fre, des, rmt, pt, ski, qty, tr, eqs, lot, cs, ci, cn, cin As String
        Dim cust As String
        cust = lblcbcust.Value
        If cust <> "1" Then
            
            Try
                fre = "" 'txtfreq.Text
                frestr = txtfreq.Text
            Catch ex As Exception
                fre = "" 'txtfreq.Text
                frestr = lblfreq.Value
            End Try
            If frestr = "" Then
                frestr = lblfreq.Value
            End If
        Else
            fre = "0"
            frestr = ""
        End If
        
        Dim icb1o, icb1, icb1mon, icb1tue, icb1wed, icb1thu, icb1fri, icb1sat, icb1sun As Integer
        Dim firststr, shiftonly1 As String
        Dim icb2o, icb2, icb2mon, icb2tue, icb2wed, icb2thu, icb2fri, icb2sat, icb2sun As Integer
        Dim secondstr, shiftonly2 As String
        Dim icb3o, icb3, icb3mon, icb3tue, icb3wed, icb3thu, icb3fri, icb3sat, icb3sun As Integer
        Dim thirdstr, shiftonly3 As String
        Dim c1cnt, c2cnt, c3cnt As Integer
        c1cnt = 0
        c2cnt = 0
        c3cnt = 0
        If fre <> "0" Or cust = "1" Then
            If lcb1o.Value = "1" Then
                icb1o = 1
                'icb1 = 0
                'icb1mon = 0
                'icb1tue = 0
                'icb1wed = 0
                'icb1thu = 0
                'icb1fri = 0
                'icb1sat = 0
                'icb1sun = 0
                shiftonly1 = "1st shift"
            Else
                icb1o = 0
            End If
            If lcb1.Value = "1" Then
                icb1 = 1
                icb1mon = 1
                icb1tue = 1
                icb1wed = 1
                icb1thu = 1
                icb1fri = 1
                icb1sat = 1
                icb1sun = 1
                'firststr = "M,Tu,W,Th,F,Sa,Su"
                c1cnt = 7
            Else
                icb1 = 0

                If lcb1mon.Value = "1" Then
                    icb1mon = 1
                    firststr = "M"
                    c1cnt += 1
                Else
                    icb1mon = 0
                End If
                If lcb1tue.Value = "1" Then
                    icb1tue = 1
                    If firststr = "" Then
                        firststr = "Tu"
                    Else
                        firststr += ",Tu"
                    End If
                    c1cnt += 1
                Else
                    icb1tue = 0
                End If
                If lcb1wed.Value = "1" Then
                    icb1wed = 1
                    If firststr = "" Then
                        firststr = "W"
                    Else
                        firststr += ",W"
                    End If
                    c1cnt += 1
                Else
                    icb1wed = 0
                End If
                If lcb1thu.Value = "1" Then
                    icb1thu = 1
                    If firststr = "" Then
                        firststr = "Th"
                    Else
                        firststr += ",Th"
                    End If
                    c1cnt += 1
                Else
                    icb1thu = 0
                End If
                If lcb1fri.Value = "1" Then
                    icb1fri = 1
                    If firststr = "" Then
                        firststr = "F"
                    Else
                        firststr += ",F"
                    End If
                    c1cnt += 1
                Else
                    icb1fri = 0
                End If
                If lcb1sat.Value = "1" Then
                    icb1sat = 1
                    If firststr = "" Then
                        firststr = "Sa"
                    Else
                        firststr += ",Sa"
                    End If
                    c1cnt += 1
                Else
                    icb1sat = 0
                End If
                If lcb1sun.Value = "1" Then
                    icb1sun = 1
                    If firststr = "" Then
                        firststr = "Su"
                    Else
                        firststr += ",Su"
                    End If
                    c1cnt += 1
                Else
                    icb1sun = 0
                End If
            End If
            'End If

            If lcb2o.Value = "1" Then
                icb2o = 1
                'icb2 = 0
                'icb2mon = 0
                'icb2tue = 0
                'icb2wed = 0
                'icb2thu = 0
                'icb2fri = 0
                'icb2sat = 0
                'icb2sun = 0
                shiftonly2 = "2nd shift"
            Else
                icb2o = 0
            End If
            If lcb2.Value = "1" Then
                icb2 = 1
                icb2mon = 1
                icb2tue = 1
                icb2wed = 1
                icb2thu = 1
                icb2fri = 1
                icb2sat = 1
                icb2sun = 1
                'secondstr = "M,Tu,W,Th,F,Sa,Su"
                c2cnt = 7
            Else
                icb2 = 0
                If lcb2mon.Value = "1" Then
                    icb2mon = 1

                    secondstr = "M"

                    c2cnt += 1
                Else
                    icb2mon = 0
                End If
                If lcb2tue.Value = "1" Then
                    icb2tue = 1
                    If secondstr = "" Then
                        secondstr = "Tu"
                    Else
                        secondstr += ",Tu"
                    End If
                    c2cnt += 1
                Else
                    icb2tue = 0
                End If
                If lcb2wed.Value = "1" Then
                    icb2wed = 1
                    If secondstr = "" Then
                        secondstr = "W"
                    Else
                        secondstr += ",W"
                    End If
                    c2cnt += 1
                Else
                    icb2wed = 0
                End If
                If lcb2thu.Value = "1" Then
                    icb2thu = 1
                    If secondstr = "" Then
                        secondstr = "Th"
                    Else
                        secondstr += ",Th"
                    End If
                    c2cnt += 1
                Else
                    icb2thu = 0
                End If
                If lcb2fri.Value = "1" Then
                    icb2fri = 1
                    If secondstr = "" Then
                        secondstr = "F"
                    Else
                        secondstr += ",F"
                    End If
                    c2cnt += 1
                Else
                    icb2fri = 0
                End If
                If lcb2sat.Value = "1" Then
                    icb2sat = 1
                    If secondstr = "" Then
                        secondstr = "Sa"
                    Else
                        secondstr += ",Sa"
                    End If
                    c2cnt += 1
                Else
                    icb2sat = 0
                End If
                If lcb2sun.Value = "1" Then
                    icb2sun = 1
                    If secondstr = "" Then
                        secondstr = "Su"
                    Else
                        secondstr += ",Su"
                    End If
                    c2cnt += 1
                Else
                    icb2sun = 0
                End If
            End If
            'End If

            If lcb3o.Value = "1" Then
                icb3o = 1
                'icb3 = 0
                'icb3mon = 0
                'icb3tue = 0
                'icb3wed = 0
                'icb3thu = 0
                'icb3fri = 0
                'icb3sat = 0
                'icb3sun = 0
                shiftonly3 = "3rd shift"
            Else
                icb3o = 0
            End If
            If lcb3.Value = "1" Then
                icb3 = 1
                icb3mon = 1
                icb3tue = 1
                icb3wed = 1
                icb3thu = 1
                icb3fri = 1
                icb3sat = 1
                icb3sun = 1
                'thirdstr = "M,Tu,W,Th,F,Sa,Su"
                c3cnt = 7
            Else
                icb3 = 0
                If lcb3mon.Value = "1" Then
                    icb3mon = 1

                    thirdstr = "M"

                    c3cnt += 1
                Else
                    icb3mon = 0
                End If
                If lcb3tue.Value = "1" Then
                    icb3tue = 1
                    If thirdstr = "" Then
                        thirdstr = "T"
                    Else
                        thirdstr += ",T"
                    End If
                    c3cnt += 1
                Else
                    icb3tue = 0
                End If
                If lcb3wed.Value = "1" Then
                    icb3wed = 1
                    If thirdstr = "" Then
                        thirdstr = "W"
                    Else
                        thirdstr += ",W"
                    End If
                    c3cnt += 1
                Else
                    icb3wed = 0
                End If
                If lcb3thu.Value = "1" Then
                    icb3thu = 1
                    If thirdstr = "" Then
                        thirdstr = "Th"
                    Else
                        thirdstr += ",Th"
                    End If
                    c3cnt += 1
                Else
                    icb3thu = 0
                End If
                If lcb3fri.Value = "1" Then
                    icb3fri = 1
                    If thirdstr = "" Then
                        thirdstr = "F"
                    Else
                        thirdstr += ",F"
                    End If
                    c3cnt += 1
                Else
                    icb3fri = 0
                End If
                If lcb3sat.Value = "1" Then
                    icb3sat = 1
                    If thirdstr = "" Then
                        thirdstr = "Sa"
                    Else
                        thirdstr += ",Sa"
                    End If
                    c3cnt += 1
                Else
                    icb3sat = 0
                End If
                If lcb3sun.Value = "1" Then
                    icb3sun = 1
                    If thirdstr = "" Then
                        thirdstr = "Su"
                    Else
                        thirdstr += ",Su"
                    End If
                    c3cnt += 1
                Else
                    icb3sun = 0
                End If
            End If
            'End If


        End If
        Dim ms As Integer = 0
        If firststr <> "" Then
            ms += 1
        End If
        If secondstr <> "" Then
            ms += 1
        End If
        If thirdstr <> "" Then
            ms += 1
        End If
        If ms < 2 Then
            ms = 0
            If shiftonly1 <> "" Then
                ms += 1
            End If
            If shiftonly2 <> "" Then
                ms += 1
            End If
            If shiftonly3 <> "" Then
                ms += 1
            End If
            If ms > 1 Then
                ms = 1
            Else
                ms = 0
            End If
        Else
            ms = 1
        End If



        Dim fm As String
        Dim Item As ListItem
        Dim f, fi As String
        Dim ipar As Integer = 0
        For Each Item In lbfailmodes.Items
            f = Item.Text.ToString
            ipar = f.LastIndexOf("(")
            If ipar <> -1 Then
                f = Mid(f, 1, ipar)
            End If
            If Len(fm) = 0 Then
                fm = f & "(___)"
            Else
                fm += " " & f & "(___)"
            End If
        Next
        fm = Replace(fm, "'", Chr(180), , , vbTextCompare)
        t = lblt.Value
        st = lblsb.Value
        If ddtype.SelectedIndex = 0 Then
            typ = "0"
        Else
            typ = ddtype.SelectedValue
        End If
        typstr = ddtype.SelectedItem.ToString
        typstr = Replace(typstr, "'", Chr(180), , , vbTextCompare)



        des = txtdesc.Text
        des = Replace(des, "'", Chr(180), , , vbTextCompare)
        des = Replace(des, "--", "-", , , vbTextCompare)
        des = Replace(des, ";", ":", , , vbTextCompare)

        rdt = txtrdt.Text

        qty = txtqty.Text
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToInt32(qty)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr542", "tpmtasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If Len(qty) = 0 Then
            qty = "1"
        End If

        tr = txttr.Text
        If Len(tr) = 0 Then
            tr = "0"
        End If

        If ddeqstat.SelectedIndex = 0 Then
            eqs = "0"
        Else
            eqs = ddeqstat.SelectedValue
        End If
        eqsstr = ddeqstat.SelectedItem.ToString
        eqsstr = Replace(eqsstr, "'", Chr(180), , , vbTextCompare)
        If cbloto.Checked = True Then
            lot = "1"
        Else
            lot = "0"
        End If

        If cbcs.Checked = True Then
            cs = "1"
        Else
            cs = "0"
        End If

        tid = lbltaskid.Value
        Filter = lblfilt.Value
        If ddcomp.SelectedIndex = 0 Then
            ci = "0"
            cin = "0"
        Else
            ci = ddcomp.SelectedValue.ToString
        End If
        fuid = lblfuid.Value

        cqty = txtcQty.Text
        Dim cqtychk As Long
        Try
            cqtychk = System.Convert.ToInt32(cqty)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr543", "tpmtasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        pf = txtpfint.Text

        If cin Is Nothing Then
            cin = "0"
        End If

        cn = ddcomp.SelectedItem.ToString
        cn = Replace(cn, "'", Chr(180), , , vbTextCompare)

        tl = lbltasklev.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value

        Dim ttid As String = lbltaskid.Value

        sid = lblsid.Value

        PageNumber = lblpg.Text
        fuid = lblfuid.Value

        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        eqid = lbleqid.Value
        Dim hpm As String = lblhaspm.Value
        If Len(ttid) <> 0 Then
            'usp_updatepmtaskstpm](@pmtskid int, @des varchar(500), @qty int, @ttime decimal(10,2), @rdid int, @rd varchar(50),
            '@fre int, @freq varchar(50), @lotoid int, @conid int, @ttid int, @tasktype varchar(50), @comid int, 
            '@compnum varchar(200), @fm1 varchar(350), @cqty int, @pfinterval int, @rdt decimal(10,2), @filter varchar(2000),
            '@pagenumber int, @fuid int,
            '@icb1o int, @icb1 int, @icb1mon int, @icb1tue int, @icb1wed int, @icb1thu int, @icb1fri int, @icb1sat int, 
            '@icb1sun int, @icb2o int, @icb2 int, @icb2mon int, @icb2tue int, @icb2wed int, @icb2thu int, @icb2fri int, @icb2sat int, 
            '@icb2sun int, @icb3o int, @icb3 int, @icb3mon int, @icb3tue int, @icb3wed int, @icb3thu int, @icb3fri int, @icb3sat int, 
            '@icb3sun int,
            '@ustr varchar(50), @eqid int, @sid int, @hpm int,
            '@firststr varchar(50), @secondstr varchar(50), @thirdstr varchar(50),
            '@shift1 varchar(50), @shift2 varchar(50), @shift3 varchar(50), @ms int, @cust varchar(10) = null

            Dim cmd As New SqlCommand
            cmd.CommandText = "exec usp_updatepmtaskstpm @pmtskid, @des, @qty, @ttime, @rdid, @rd, " _
            + "@fre, @freq, @lotoid, @conid, @ttid, @tasktype, @comid, " _
            + "@compnum, @fm1, @cqty, @pfinterval, @rdt, @filter, " _
            + "@pagenumber, @fuid, " _
            + "@icb1o, @icb1, @icb1mon, @icb1tue, @icb1wed, @icb1thu, @icb1fri, @icb1sat, " _
            + "@icb1sun, @icb2o, @icb2, @icb2mon, @icb2tue, @icb2wed, @icb2thu, @icb2fri, @icb2sat, " _
            + "@icb2sun, @icb3o, @icb3, @icb3mon, @icb3tue, @icb3wed, @icb3thu, @icb3fri, @icb3sat, " _
            + "@icb3sun, " _
            + "@ustr, @eqid, @sid, @hpm, @first, @second, @third, " _
            + "@shift1, @shift2, @shift3, @ms, @cust, @c1cnt, @c2cnt, @c3cnt"

            Dim param = New SqlParameter("@pmtskid", SqlDbType.Int)
            param.Value = ttid
            cmd.Parameters.Add(param)
            Dim param01 = New SqlParameter("@des", SqlDbType.VarChar)
            If des = "" Then
                param01.Value = System.DBNull.Value
            Else
                param01.Value = des
            End If
            cmd.Parameters.Add(param01)
            Dim param02 = New SqlParameter("@qty", SqlDbType.Int)
            If qty = "" Then
                param02.Value = "1"
            Else
                param02.Value = qty
            End If
            cmd.Parameters.Add(param02)
            Dim param03 = New SqlParameter("@ttime", SqlDbType.Decimal)
            If tr = "" Then
                param03.Value = "0"
            Else
                param03.Value = tr
            End If
            cmd.Parameters.Add(param03)
            Dim param04 = New SqlParameter("@rdid", SqlDbType.Int)
            If eqs = "" Then
                param04.Value = "0"
            Else
                param04.Value = eqs
            End If
            cmd.Parameters.Add(param04)
            Dim param05 = New SqlParameter("@rd", SqlDbType.VarChar)
            If eqsstr = "" Then
                param05.Value = System.DBNull.Value
            Else
                param05.Value = eqsstr
            End If
            cmd.Parameters.Add(param05)
            Dim param06 = New SqlParameter("@fre", SqlDbType.Int)
            If fre = "" Then
                param06.Value = "0"
            Else
                param06.Value = fre
            End If
            cmd.Parameters.Add(param06)
            Dim param07 = New SqlParameter("@freq", SqlDbType.VarChar)
            If frestr = "" Or frestr = "Select" Then
                param07.Value = System.DBNull.Value
            Else
                param07.Value = frestr
            End If
            cmd.Parameters.Add(param07)
            Dim param08 = New SqlParameter("@lotoid", SqlDbType.Int)
            If lot = "" Then
                param08.Value = "0"
            Else
                param08.Value = lot
            End If
            cmd.Parameters.Add(param08)
            Dim param09 = New SqlParameter("@conid", SqlDbType.Int)
            If cs = "" Then
                param09.Value = "0"
            Else
                param09.Value = cs
            End If
            cmd.Parameters.Add(param09)
            Dim param10 = New SqlParameter("@ttid", SqlDbType.Int)
            If typ = "" Then
                param10.Value = "0"
            Else
                param10.Value = typ
            End If
            cmd.Parameters.Add(param10)
            Dim param11 = New SqlParameter("@tasktype", SqlDbType.VarChar)
            If typstr = "" Or typstr = "Select" Then
                param11.Value = System.DBNull.Value
            Else
                param11.Value = typstr
            End If
            cmd.Parameters.Add(param11)
            Dim param12 = New SqlParameter("@comid", SqlDbType.Int)
            If ci = "" Then
                param12.Value = "0"
            Else
                param12.Value = ci
            End If
            cmd.Parameters.Add(param12)
            Dim param13 = New SqlParameter("@compnum", SqlDbType.VarChar)
            If cn = "" Or cn = "Select" Then
                param13.Value = System.DBNull.Value
            Else
                param13.Value = cn
            End If
            cmd.Parameters.Add(param13)
            Dim param14 = New SqlParameter("@fm1", SqlDbType.VarChar)
            If fm = "" Then
                param14.Value = System.DBNull.Value
            Else
                param14.Value = fm
            End If
            cmd.Parameters.Add(param14)
            Dim param15 = New SqlParameter("@cqty", SqlDbType.Int)
            If cqty = "" Then
                param15.Value = "1"
            Else
                param15.Value = cqty
            End If
            cmd.Parameters.Add(param15)
            Dim param16 = New SqlParameter("@pfinterval", SqlDbType.Int)
            If pf = "" Then
                param16.Value = System.DBNull.Value
            Else
                param16.Value = pf
            End If
            cmd.Parameters.Add(param16)
            Dim param17 = New SqlParameter("@rdt", SqlDbType.Decimal)
            If rdt = "" Then
                param17.Value = "0"
            Else
                param17.Value = rdt
            End If
            cmd.Parameters.Add(param17)
            Dim param18 = New SqlParameter("@filter", SqlDbType.VarChar)
            If Filter = "" Then
                param18.Value = System.DBNull.Value
            Else
                param18.Value = Filter
            End If
            cmd.Parameters.Add(param18)
            Dim param19 = New SqlParameter("@pagenumber", SqlDbType.Int)
            param19.Value = lblt.Value
            cmd.Parameters.Add(param19)
            Dim param20 = New SqlParameter("@fuid", SqlDbType.Int)
            If fuid = "" Then
                param20.Value = "0"
            Else
                param20.Value = fuid
            End If
            cmd.Parameters.Add(param20)
            Dim param21 = New SqlParameter("@icb1o", SqlDbType.Int)
            param21.Value = icb1o
            cmd.Parameters.Add(param21)
            Dim param22 = New SqlParameter("@icb1", SqlDbType.Int)
            param22.Value = icb1
            cmd.Parameters.Add(param22)
            Dim param23 = New SqlParameter("@icb1mon", SqlDbType.Int)
            param23.Value = icb1mon
            cmd.Parameters.Add(param23)
            Dim param24 = New SqlParameter("@icb1tue", SqlDbType.Int)
            param24.Value = icb1tue
            cmd.Parameters.Add(param24)
            Dim param25 = New SqlParameter("@icb1wed", SqlDbType.Int)
            param25.Value = icb1wed
            cmd.Parameters.Add(param25)
            Dim param26 = New SqlParameter("@icb1thu", SqlDbType.Int)
            param26.Value = icb1thu
            cmd.Parameters.Add(param26)
            Dim param27 = New SqlParameter("@icb1fri", SqlDbType.Int)
            param27.Value = icb1fri
            cmd.Parameters.Add(param27)
            Dim param28 = New SqlParameter("@icb1sat", SqlDbType.Int)
            param28.Value = icb1sat
            cmd.Parameters.Add(param28)
            Dim param29 = New SqlParameter("@icb1sun", SqlDbType.Int)
            param29.Value = icb1sun
            cmd.Parameters.Add(param29)

            Dim param30 = New SqlParameter("@icb2o", SqlDbType.Int)
            param30.Value = icb2o
            cmd.Parameters.Add(param30)
            Dim param31 = New SqlParameter("@icb2", SqlDbType.Int)
            param31.Value = icb2
            cmd.Parameters.Add(param31)
            Dim param32 = New SqlParameter("@icb2mon", SqlDbType.Int)
            param32.Value = icb2mon
            cmd.Parameters.Add(param32)
            Dim param33 = New SqlParameter("@icb2tue", SqlDbType.Int)
            param33.Value = icb2tue
            cmd.Parameters.Add(param33)
            Dim param34 = New SqlParameter("@icb2wed", SqlDbType.Int)
            param34.Value = icb2wed
            cmd.Parameters.Add(param34)
            Dim param35 = New SqlParameter("@icb2thu", SqlDbType.Int)
            param35.Value = icb2thu
            cmd.Parameters.Add(param35)
            Dim param36 = New SqlParameter("@icb2fri", SqlDbType.Int)
            param36.Value = icb2fri
            cmd.Parameters.Add(param36)
            Dim param37 = New SqlParameter("@icb2sat", SqlDbType.Int)
            param37.Value = icb2sat
            cmd.Parameters.Add(param37)
            Dim param38 = New SqlParameter("@icb2sun", SqlDbType.Int)
            param38.Value = icb2sun
            cmd.Parameters.Add(param38)

            Dim param39 = New SqlParameter("@icb3o", SqlDbType.Int)
            param39.Value = icb3o
            cmd.Parameters.Add(param39)
            Dim param40 = New SqlParameter("@icb3", SqlDbType.Int)
            param40.Value = icb3
            cmd.Parameters.Add(param40)
            Dim param41 = New SqlParameter("@icb3mon", SqlDbType.Int)
            param41.Value = icb3mon
            cmd.Parameters.Add(param41)
            Dim param42 = New SqlParameter("@icb3tue", SqlDbType.Int)
            param42.Value = icb3tue
            cmd.Parameters.Add(param42)
            Dim param43 = New SqlParameter("@icb3wed", SqlDbType.Int)
            param43.Value = icb3wed
            cmd.Parameters.Add(param43)
            Dim param44 = New SqlParameter("@icb3thu", SqlDbType.Int)
            param44.Value = icb3thu
            cmd.Parameters.Add(param44)
            Dim param45 = New SqlParameter("@icb3fri", SqlDbType.Int)
            param45.Value = icb3fri
            cmd.Parameters.Add(param45)
            Dim param46 = New SqlParameter("@icb3sat", SqlDbType.Int)
            param46.Value = icb3sat
            cmd.Parameters.Add(param46)
            Dim param47 = New SqlParameter("@icb3sun", SqlDbType.Int)
            param47.Value = icb3sun
            cmd.Parameters.Add(param47)

            Dim param48 = New SqlParameter("@ustr", SqlDbType.VarChar)
            If ustr = "" Then
                param48.Value = System.DBNull.Value
            Else
                param48.Value = ustr
            End If
            cmd.Parameters.Add(param48)
            Dim param49 = New SqlParameter("@eqid", SqlDbType.Int)
            param49.Value = eqid
            cmd.Parameters.Add(param49)
            Dim param50 = New SqlParameter("@sid", SqlDbType.Int)
            param50.Value = sid
            cmd.Parameters.Add(param50)

            Dim param89 = New SqlParameter("@hpm", SqlDbType.Int)
            If hpm = "" Then
                param89.Value = "0"
            Else
                param89.Value = hpm
            End If
            cmd.Parameters.Add(param89)

            Dim param91 = New SqlParameter("@first", SqlDbType.VarChar)
            If firststr = "" Then
                param91.Value = System.DBNull.Value
            Else
                param91.Value = firststr
            End If
            cmd.Parameters.Add(param91)

            Dim param92 = New SqlParameter("@second", SqlDbType.VarChar)
            If secondstr = "" Then
                param92.Value = System.DBNull.Value
            Else
                param92.Value = secondstr
            End If
            cmd.Parameters.Add(param92)

            Dim param93 = New SqlParameter("@third", SqlDbType.VarChar)
            If thirdstr = "" Then
                param93.Value = System.DBNull.Value
            Else
                param93.Value = thirdstr
            End If
            cmd.Parameters.Add(param93)

            Dim param94 = New SqlParameter("@shift1", SqlDbType.VarChar)
            If shiftonly1 = "" Then
                param94.Value = System.DBNull.Value
            Else
                param94.Value = shiftonly1
            End If
            cmd.Parameters.Add(param94)

            Dim param95 = New SqlParameter("@shift2", SqlDbType.VarChar)
            If shiftonly2 = "" Then
                param95.Value = System.DBNull.Value
            Else
                param95.Value = shiftonly2
            End If
            cmd.Parameters.Add(param95)

            Dim param96 = New SqlParameter("@shift3", SqlDbType.VarChar)
            If shiftonly3 = "" Then
                param96.Value = System.DBNull.Value
            Else
                param96.Value = shiftonly3
            End If
            cmd.Parameters.Add(param96)

            Dim param97 = New SqlParameter("@ms", SqlDbType.VarChar)
            param97.Value = ms
            cmd.Parameters.Add(param97)

            Dim param98 = New SqlParameter("@cust", SqlDbType.VarChar)
            If cust = "" Then
                param98.Value = "0"
            Else
                param98.Value = cust
            End If
            cmd.Parameters.Add(param98)

            Dim param100 = New SqlParameter("@c1cnt", SqlDbType.Int)
            If IsDBNull(c1cnt) Then
                param100.Value = 0
            Else
                param100.Value = c1cnt
            End If
            cmd.Parameters.Add(param100)

            Dim param101 = New SqlParameter("@c2cnt", SqlDbType.Int)
            If IsDBNull(c2cnt) Then
                param101.Value = 0
            Else
                param101.Value = c2cnt
            End If
            cmd.Parameters.Add(param101)

            Dim param102 = New SqlParameter("@c3cnt", SqlDbType.Int)
            If IsDBNull(c3cnt) Then
                param102.Value = 0
            Else
                param102.Value = c3cnt
            End If
            cmd.Parameters.Add(param102)

            Try
                tasks.UpdateHack(cmd)
            Catch ex As Exception
                tasksadd.UpdateHack(cmd)
            End Try

            Dim currcnt As String = lblcurrsb.Value
            Dim cscnt As String = lblcurrcs.Value
            If st = 0 Then
                PageNumber = lblpg.Text
            Else
                PageNumber = currcnt + cscnt
            End If
            'question this
            'field = "pmtasks.funcid"
            'val = fuid
            Filter = lblfilt.Value 'field & " = " & val & " and pmtasks.subtask = 0"
            'end question
            LoadPage(PageNumber, Filter)
            lblenable.Value = "1"
        End If
    End Sub
   

    Private Sub SaveTaskComp()
        Dim comp As String = ddcomp.SelectedValue.ToString
        Dim compd As String = ddcomp.SelectedItem.ToString
        If ddcomp.SelectedIndex <> 0 Then
            lblco.Value = comp
            chk = comp
            ttid = lbltaskid.Value
            fuid = lblfuid.Value
            Try
                tasks.Open()
                sql = "usp_inserttpmComp '" & comp & "', '" & compd & "', '" & ttid & "', '" & fuid & "'"
                tasks.Update(sql)
                PopCompFailList(comp)
                PopFailList(comp)
                PopTaskFailModes(comp)
                UpdateFailStats(comp)
                eqid = lbleqid.Value
                tasks.UpMod(eqid)
            Catch ex As Exception

            End Try
            Try
                tasks.Dispose()
            Catch ex As Exception

            End Try
        Else
            Try
                ddcomp.SelectedValue = lblco.Value
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub ddcomp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcomp.SelectedIndexChanged
        SaveTaskComp()
    End Sub
    Private Sub CheckComp(ByVal comp As String)
        ttid = lblt.Value
    End Sub

    Private Sub PopComp()
        fuid = lblfuid.Value
        sql = "select comid, compnum + ' - ' + compdesc as compnum " _
        + "from components where func_id = '" & fuid & "'"
        dr = tasks.GetRdrData(sql)
        ddcomp.DataSource = dr
        ddcomp.DataTextField = "compnum"
        ddcomp.DataValueField = "comid"
        ddcomp.DataBind()
        dr.Close()
        ddcomp.Items.Insert(0, New ListItem("Select"))
        ddcomp.Items(0).Value = 0
    End Sub
    Private Sub PopFailList(ByVal comp As String)
        ttid = lbltaskid.Value
        'sql = "select count(*) " _
        '+ "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
        '+ "select failid from pmtaskfailmodes where comid = '" & comp & "' " _
        '+ "and parentfailid is not null)" ' and taskid = '" & ttid & "')"
        'Dim ercnt As Integer
        'ercnt = tasks.Scalar(sql)
        'If ercnt = 0 Then
        sql = "select compfailid, failuremode " _
        + "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
        + "select failid from pmtaskfailmodestpm where comid = '" & comp & "')" ' and taskid = '" & ttid & "')"
        sql = "usp_getcfall_tskna_tpm '" & comp & "'"
        'Else
        'sql = "select compfailid, failuremode " _
        '+ "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
        '+ "select failid from pmtaskfailmodes where comid = '" & comp & "' " _
        '+ "and parentfailid is null)" ' and taskid = '" & ttid & "')"
        'End If

        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try
        lbfaillist.DataSource = dr
        lbfaillist.DataTextField = "failuremode"
        lbfaillist.DataValueField = "compfailid"
        lbfaillist.DataBind()
        dr.Close()
    End Sub

    Private Sub PopCompFailList(ByVal comp As String)
        ttid = lbltaskid.Value
        sql = "select compfailid, failuremode " _
         + "from componentfailmodes where comid = '" & comp & "'"
        sql = "usp_getcfall_co '" & comp & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        lbCompFM.DataSource = dr
        lbCompFM.DataTextField = "failuremode"
        lbCompFM.DataValueField = "compfailid"
        lbCompFM.DataBind()
        dr.Close()
    End Sub
    Private Sub PopTaskFailModes(ByVal comp As String)
        ttid = lbltaskid.Value
        'sql = "select * from pmtaskfailmodes where taskid = '" & ttid & "'"
        sql = "select * from pmtaskfailmodestpm where comid = '" & comp & "' and taskid = '" & ttid & "'"
        sql = "usp_getcfall_tsk_tpm '" & comp & "','" & ttid & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try
        lbfailmodes.DataSource = dr
        lbfailmodes.DataTextField = "failuremode"
        lbfailmodes.DataValueField = "failid"
        lbfailmodes.DataBind()
        dr.Close()
    End Sub
    Private Sub GetItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Dim fcnt, fcnt2 As Integer
        If oaid <> "0" Then
            sql = "select count(*) from pmTaskFailModestpm where taskid = '" & ttid & "' and failid = '" & fi & "' and oaid = '" & oaid & "'"
        Else
            sql = "select count(*) from pmTaskFailModestpm where taskid = '" & ttid & "' and failid = '" & fi & "' and  (oaid is null or oaid = '0')"
        End If
        'sql = "select count(*) from pmTaskFailModestpm where taskid = '" & ttid & "' and failid = '" & fi & "'"
        fcnt = tasks.Scalar(sql)
        sql = "select count(*) from pmTaskFailModestpm where taskid = '" & ttid & "'"
        fcnt2 = tasks.Scalar(sql)
        If fcnt2 >= 5 Then
            Dim strMessage As String = tmod.getmsg("cdstr547", "tpmtasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf fcnt > 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr548", "tpmtasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Try
                sql = "usp_addTaskFailureModetpm " & ttid & ", " & fi & ", '" & f & "', '" & comp & "', 'dev','" & oaid & "'"
                tasks.Update(sql)
                Dim Item As ListItem
                Dim fm As String
                Dim ipar As Integer = 0
                For Each Item In lbfailmodes.Items
                    f = Item.Text.ToString
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                    If Len(fm) = 0 Then
                        fm = f & "(___)"
                    Else
                        fm += " " & f & "(___)"
                    End If
                Next
                fm = Replace(fm, "'", Chr(180), , , vbTextCompare)
                sql = "update pmtaskstpm set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
                tasks.Update(sql)
                eqid = lbleqid.Value
                tasks.UpMod(eqid)
            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Sub RemItems(ByVal f As String, ByVal fi As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Try
            sql = "usp_delTaskFailureModetpm '" & ttid & "', '" & fi & "','" & oaid & "'"
            tasks.Update(sql)
            Dim fm As String
            Dim Item As ListItem
            Dim ipar As Integer = 0
            For Each Item In lbfailmodes.Items
                f = Item.Text.ToString
                ipar = f.LastIndexOf("(")
                If ipar <> -1 Then
                    f = Mid(f, 1, ipar)
                End If
                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
            Next
            fm = Replace(fm, "'", Chr(180), , , vbTextCompare)
            sql = "update pmtaskstpm set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
            tasks.Update(sql)
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub UpdateFailStats(ByVal comp As String)
        Dim fmcnt, fmcnta As Integer
        sql = "select count(*) from ComponentFailModes where comid = '" & comp & "'"
        Try
            fmcnt = tasks.Scalar(sql)
        Catch ex As Exception
            fmcnt = tasksadd.Scalar(sql)
        End Try
        sql = "select count(distinct failid) from pmTaskFailModestpm where comid = '" & comp & "'"
        Try
            fmcnta = tasks.Scalar(sql)
        Catch ex As Exception
            fmcnta = tasksadd.Scalar(sql)
        End Try

        fmcnta = fmcnt - fmcnta
    End Sub

    Private Sub ibToTask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibToTask.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        Dim comp As String = ddcomp.SelectedValue.ToString
        tasks.Open()
        'SaveTask()
        'lblenable.Value = "0"
        For Each Item In lbfaillist.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            UpdateFM()
            lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            Filter = lblfilt.Value
            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub
    Private Sub UpdateFM()
        Dim ttid As String = lbltaskid.Value
        sql = "usp_UpdateFM1tpm '" & ttid & "'"
        tasks.Update(sql)
    End Sub

    Private Sub ibReuse_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibReuse.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        Dim comp As String = ddcomp.SelectedValue.ToString
        tasks.Open()
        'SaveTask()
        'lblenable.Value = "0"
        For Each Item In lbCompFM.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            Filter = lblfilt.Value
            'LoadPage(PageNumber, Filter)
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
        Catch ex As Exception

        End Try
        tasks.Dispose()
    End Sub

    Private Sub ibFromTask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibFromTask.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        Dim comp As String = ddcomp.SelectedValue.ToString
        tasks.Open()
        'SaveTask()
        'lblenable.Value = "0"
        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                RemItems(f, fi, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            UpdateFM()
            lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            Filter = lblfilt.Value
            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub
    Private Sub AddFail()
        Dim comp As String = lblco.Value '= colblcoid.Value
        tasks.Open()
        PopCompFailList(comp)
        PopFailList(comp)
        PopTaskFailModes(comp)
        tasks.Dispose()
    End Sub

    Private Sub btndeltask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btndeltask.Click
        PageNumber = lblpg.Text
        Filter = lblfilt.Value
        fuid = lblfuid.Value
        tnum = lblt.Value
        Dim st As Integer = lblsb.Value
        sql = "usp_delTPMTask '" & fuid & "', '" & tnum & "', '" & st & "'"
        tasks.Open()
        tasks.Update(sql)
        tskcnt = lblcnt.Text
        tskcnt = tskcnt - 1
        lblcnt.Text = tskcnt
        If PageNumber <= tskcnt Then
            PageNumber = PageNumber
        Else
            PageNumber = tskcnt
        End If
        If PageNumber = 0 Then
            PageNumber = 1
        End If
        eqid = lbleqid.Value
        tasks.UpMod(eqid)

        LoadPage(PageNumber, Filter)
        tasks.Dispose()
    End Sub

    Private Sub ibCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibCancel.Click
        tasks.Open()
        GetLists()
        Filter = lblfilt.Value
        LoadPage(PageNumber, Filter)
    End Sub

    Private Sub btnsavetask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsavetask.Click

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label25.Text = axlabs.GetASPXPage("tpmtasks.aspx", "Label25")
        Catch ex As Exception
        End Try
        Try
            Label26.Text = axlabs.GetASPXPage("tpmtasks.aspx", "Label26")
        Catch ex As Exception
        End Try
        Try
            lang1288.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1288")
        Catch ex As Exception
        End Try
        Try
            lang1289.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1289")
        Catch ex As Exception
        End Try
        Try
            lang1290.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1290")
        Catch ex As Exception
        End Try
        Try
            lang1291.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1291")
        Catch ex As Exception
        End Try
        Try
            lang1292.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1292")
        Catch ex As Exception
        End Try
        Try
            lang1293.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1293")
        Catch ex As Exception
        End Try
        Try
            lang1294.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1294")
        Catch ex As Exception
        End Try
        Try
            lang1295.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1295")
        Catch ex As Exception
        End Try
        Try
            lang1296.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1296")
        Catch ex As Exception
        End Try
        Try
            lang1297.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1297")
        Catch ex As Exception
        End Try
        Try
            lang1298.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1298")
        Catch ex As Exception
        End Try
        Try
            lang1299.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1299")
        Catch ex As Exception
        End Try
        Try
            lang1300.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1300")
        Catch ex As Exception
        End Try
        Try
            lang1301.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1301")
        Catch ex As Exception
        End Try
        Try
            lang1302.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1302")
        Catch ex As Exception
        End Try
        Try
            lang1303.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1303")
        Catch ex As Exception
        End Try
        Try
            lang1304.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1304")
        Catch ex As Exception
        End Try
        Try
            lang1305.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1305")
        Catch ex As Exception
        End Try
        Try
            lang1306.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1306")
        Catch ex As Exception
        End Try
        Try
            lang1307.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1307")
        Catch ex As Exception
        End Try
        Try
            lang1308.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1308")
        Catch ex As Exception
        End Try
        Try
            lang1309.Text = axlabs.GetASPXPage("tpmtasks.aspx", "lang1309")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddcomp.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "btnaddcomp") & "')")
            btnaddcomp.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            btnaddnewfail.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "btnaddnewfail") & "')")
            btnaddnewfail.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            btnpfint.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "btnpfint") & "')")
            btnpfint.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ggrid.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "ggrid") & "')")
            ggrid.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            img1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "img1") & "')")
            img1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img4.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "Img4") & "', ABOVE, LEFT)")
            Img4.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img5.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "Img5") & "', ABOVE, LEFT)")
            Img5.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgcopycomp.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "imgcopycomp") & "')")
            imgcopycomp.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid182.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "ovid182") & "', ABOVE, LEFT)")
            ovid182.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid183.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "ovid183") & "')")
            ovid183.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid184.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "ovid184") & "')")
            ovid184.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid185.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "ovid185") & "')")
            ovid185.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid186.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "ovid186") & "')")
            ovid186.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid187.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "ovid187") & "')")
            ovid187.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid188.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "ovid188") & "')")
            ovid188.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid189.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "ovid189") & "')")
            ovid189.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid190.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "ovid190") & "')")
            ovid190.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid191.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "ovid191") & "')")
            ovid191.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid192.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "ovid192") & "')")
            ovid192.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid193.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "ovid193") & "')")
            ovid193.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            sgrid.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtasks.aspx", "sgrid") & "')")
            sgrid.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
