<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tpmtasksmain.aspx.vb" Inherits="lucy_r12.tpmtasksmain" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title id="pgtitle">tpmtasksmain</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="javascript" src="../scripts/taskgridtpm.js"></script>
		<script language="JavaScript" src="../scripts1/tpmtasksmainaspx_1.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  class="tbg">
		<form id="form1" method="post" runat="server">
			<table style="Z-INDEX: 1; LEFT: 7px; POSITION: absolute; TOP: 83px" cellSpacing="0" cellPadding="2"
				width="1060">
				<tr>
					<td id="tdqs" runat="server" colspan="2"></td>
				</tr>
				<tr>
					<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/pmgridhdr.gif" border="0"></td>
					<td class="thdrsingrt label" width="758"><asp:Label id="lang1310" runat="server">Location/Equipment/Function Details</asp:Label></td>
					<td width="3">&nbsp;</td>
					<td class="thdrsinglft" id="tdnavtop" width="22"><IMG src="../images/appbuttons/minibuttons/eqarch.gif" border="0"></td>
					<td class="thdrsingrt label" width="251"><asp:Label id="lang1311" runat="server">Asset Hierarchy</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="2">
						<table cellSpacing="0" cellPadding="0">
							<tr>
								<td><iframe id="geteq" style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: transparent; BORDER-BOTTOM-STYLE: none"
										src="tpmget.aspx?jump=no" frameBorder="no" width="780" scrolling="no" height="132"
										runat="server" allowtransparency></iframe>
								</td>
							</tr>
							<tr>
								<td colSpan="7"><iframe id="iftaskdet" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-TOP-STYLE: none; PADDING-TOP: 0px; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: transparent; BORDER-BOTTOM-STYLE: none"
										src="tpmtaskgrid.aspx?start=no" frameBorder="no" width="780" scrolling="no" height="400" runat="server"
										allowtransparency></iframe>
								</td>
							</tr>
						</table>
					</td>
					<td></td>
					<td vAlign="top" colSpan="2" rowSpan="2">
						<table cellSpacing="0">
							<tr>
								<td colSpan="2"><iframe id="ifarch" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-TOP-STYLE: none; PADDING-TOP: 0px; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: transparent; BORDER-BOTTOM-STYLE: none"
										src="DevArchtpm.aspx?start=no" frameBorder="no" width="270" scrolling="no" height="200" runat="server"
										allowtransparency></iframe>
								</td>
							</tr>
							<tr>
								<td class="thdrsinglft" width="22"><IMG src="../images/appbuttons/minibuttons/eqarch.gif" border="0"></td>
								<td class="thdrsingrt label" width="270"><asp:Label id="lang1116" runat="server">Task Images</asp:Label></td>
							</tr>
							<tr>
								<td colSpan="2"><iframe id="ifimg" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-TOP-STYLE: none; PADDING-TOP: 0px; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: transparent; BORDER-BOTTOM-STYLE: none"
										src="../tpmpics/taskimagetpm.aspx?eqid=0" frameBorder="no" width="270" scrolling="no" height="282"
										runat="server" allowtransparency></iframe>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lbltab" type="hidden" name="lbltab" runat="server"> <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lblsid" type="hidden" name="lblsid" runat="server"> <input id="lbldid" type="hidden" name="lbldid" runat="server"><input id="lblclid" type="hidden" name="lblclid" runat="server">
			<input id="lblret" type="hidden" name="lblret" runat="server"><input id="lblchk" type="hidden" name="lblchk" runat="server">
			<input id="lbldchk" type="hidden" name="lbldchk" runat="server"><input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input id="lblcoid" type="hidden" name="lblcoid" runat="server"> <input id="lbltaskid" type="hidden" runat="server" NAME="lbltaskid">
			<input id="lbltasklev" type="hidden" runat="server" NAME="lbltasklev"> <input id="lblcid" type="hidden" runat="server" NAME="lblcid">
			<input id="tasknum" type="hidden" name="tasknum" runat="server"><input id="taskcnt" type="hidden" name="taskcnt" runat="server">
			<input id="lblgetarch" type="hidden" runat="server" NAME="lblgetarch"> <input type="hidden" id="lbltyp" runat="server" NAME="lbltyp">
			<input type="hidden" id="lbllid" runat="server" NAME="lbllid"> <input type="hidden" id="retqs" runat="server" NAME="retqs">
			<input type="hidden" id="lblro" runat="server"><input type="hidden" id="lblsubmit" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
		<uc1:mmenu1 id="Mmenu11" runat="server"></uc1:mmenu1>
	</body>
</HTML>
