

'********************************************************
'*
'********************************************************



Public Class GSubDialogtpm
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, fuid, tid, sid, Login, eqid, ro As String
    Protected WithEvents ifsm As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try

        If Not IsPostBack Then
            Try
                tid = Request.QueryString("tid").ToString
                If Len(tid) <> 0 AndAlso tid <> "" AndAlso tid <> "0" Then
                    cid = Request.QueryString("cid").ToString
                    fuid = Request.QueryString("fuid").ToString
                    eqid = Request.QueryString("eqid").ToString
                    sid = Request.QueryString("sid").ToString
                    'ro = Request.QueryString("ro").ToString
                    Try
                        ro = HttpContext.Current.Session("ro").ToString
                    Catch ex As Exception
                        ro = "0"
                    End Try
                    Try

                        If ro = "1" Then
                            'pgtitle.InnerHtml = "Sub Tasks Dialog (Read Only)"
                        Else
                            'pgtitle.InnerHtml = "Sub Tasks Dialog"
                        End If
                    Catch ex As Exception

                    End Try
                    ifsm.Attributes.Add("src", "GSubtpm.aspx?cid=" & cid & "&sid=" & sid & "&tid=" & tid & "&fuid=" & fuid + "&eqid=" + eqid + "&ro=" + ro)
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr509" , "GSubDialogtpm.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "noeqid"
                End If
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr510" , "GSubDialogtpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "noeqid"
            End Try

        End If

    End Sub

End Class
