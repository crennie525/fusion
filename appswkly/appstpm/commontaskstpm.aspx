<%@ Page Language="vb" AutoEventWireup="false" Codebehind="commontaskstpm.aspx.vb" Inherits="lucy_r12.commontaskstpm" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>commontasks</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="javascript" src="../scripts/pmtaskdivfunctpm_1016_2.js"></script>
		<script language="JavaScript" src="../scripts1/commontaskstpmaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="label" width="90"><asp:Label id="lang1221" runat="server">Task Type</asp:Label></td>
					<td width="170"><asp:listbox id="ddtype" runat="server" Width="160px" CssClass="plainlabel" Rows="1" DataValueField="ttid"
							DataTextField="tasktype" AutoPostBack="True"></asp:listbox></td>
					<td class="plainlabelblue" width="400"><input id="rbtt" type="checkbox" CHECKED runat="server"><asp:Label id="lang1222" runat="server">Return Task Type with Task</asp:Label></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang1223" runat="server">PdM Tech</asp:Label></td>
					<td><asp:dropdownlist id="ddpt" runat="server" Width="160px" CssClass="plainlabel" AutoPostBack="True"></asp:dropdownlist></td>
					<td class="plainlabelblue"><input id="rbpt" type="checkbox" CHECKED runat="server"><asp:Label id="lang1224" runat="server">Return PdM with Task</asp:Label></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang1225" runat="server">Keyword/Phrase</asp:Label></td>
					<td colSpan="2"><asp:textbox id="txtkey" runat="server" Width="300"></asp:textbox></td>
				</tr>
				<tr>
					<td class="plainlabelblue" colSpan="2"><input id="rbc" onclick="hidepm();" type="radio" CHECKED name="rb" runat="server"><asp:Label id="lang1226" runat="server">Search Common Tasks</asp:Label><input id="rbt" onclick="showpm();" type="radio" name="rb" runat="server"><asp:Label id="lang1227" runat="server">Search TPM Tasks</asp:Label></td>
					<td align="right"><IMG onclick="refreshscreen();" src="../images/appbuttons/minibuttons/refreshit.gif"
							onmouseover="return overlib('Reset Screen', ABOVE, LEFT)" onmouseout="return nd()">&nbsp;&nbsp;<asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton>&nbsp;&nbsp;</td>
				</tr>
				<tr class="details" id="tdpm" runat="server">
					<td colSpan="3">
						<table cellspacing="0">
							<tr height="22">
								<td colspan="4" class="btmmenu plainlabel"><asp:Label id="lang1228" runat="server">TPM Task Lookup Options</asp:Label></td>
							</tr>
							<tr>
								<td class="label" width="180"><asp:Label id="lang1229" runat="server">Component Name</asp:Label></td>
								<td width="180"><asp:textbox id="txtcompnum" runat="server"></asp:textbox></td>
								<td width="20"><IMG onmouseover="return overlib('Lookup Component Name', ABOVE, LEFT)" onclick="filtlook('comp','txtcompnum','compnum');"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
								<td class="label" vAlign="top" align="center" width="280" rowSpan="6">
									<table cellSpacing="0" cellPadding="0">
										<tr>
											<td class="label" align="center"><asp:Label id="lang1230" runat="server">Failure Modes</asp:Label></td>
										</tr>
										<tr>
											<td class="label" align="center"><asp:listbox id="lbfailmaster" runat="server" Width="170px" SelectionMode="Multiple" Height="140px"></asp:listbox></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang1231" runat="server">Component Description</asp:Label></td>
								<td><asp:textbox id="txtcompdesc" runat="server"></asp:textbox></td>
								<td><IMG onmouseover="return overlib('Lookup Component Description', ABOVE, LEFT)" onclick="filtlook('comp','txtcompdesc','compdesc');"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang1232" runat="server">Component Designation</asp:Label></td>
								<td><asp:textbox id="txtcompdesig" runat="server"></asp:textbox></td>
								<td><IMG onmouseover="return overlib('Lookup Component Designation', ABOVE, LEFT)" onclick="filtlook('comp','txtcompdesig','desig');"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang1233" runat="server">Component Special Identifier</asp:Label></td>
								<td><asp:textbox id="txtcompspl" runat="server"></asp:textbox></td>
								<td><IMG onmouseover="return overlib('Lookup Component Special Identifier', ABOVE, LEFT)"
										onclick="filtlook('comp','txtcompspl','spl');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang1234" runat="server">Function Name</asp:Label></td>
								<td><asp:textbox id="txtfunc" runat="server"></asp:textbox></td>
								<td><IMG onmouseover="return overlib('Lookup Function Name', ABOVE, LEFT)" onclick="filtlook('func','txtfunc','func');"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang1235" runat="server">Function Special Identifier</asp:Label></td>
								<td><asp:textbox id="txtfuncspl" runat="server"></asp:textbox></td>
								<td><IMG onmouseover="return overlib('Lookup Function Special Identifier', ABOVE, LEFT)"
										onclick="filtlook('func','txtfuncspl','spl');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
				</tr>
				<tr>
					<td id="tdtasks" colSpan="3" runat="server"></td>
				</tr>
			</table>
			<div class="details" id="ptndiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 999; BORDER-LEFT: black 1px solid; WIDTH: 500px; BORDER-BOTTOM: black 1px solid; HEIGHT: 220px">
				<table cellSpacing="0" cellPadding="0" width="500" bgcolor="white">
					<tr bgColor="blue" height="20">
						<td class="labelwht"><asp:Label id="lang1236" runat="server">Add/Edit Description</asp:Label></td>
						<td align="right"><IMG onclick="closeptn();" height="18" alt="" src="../images/close.gif" width="18"><br>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:textbox id="txtmemo" runat="server" Width="500px" CssClass="plainlabel" Height="200px" TextMode="MultiLine"></asp:textbox></td>
					</tr>
					<tr>
						<td align="right" colSpan="2"><IMG onmouseover="return overlib('Add To Common Tasks', ABOVE, LEFT)" onclick="addtask();"
								onmouseout="return nd()" src="../images/appbuttons/minibuttons/addnew.gif" id="ibaddnew" runat="server">
							<IMG id="imgsavecommon" onmouseover="return overlib('Save Changes to This Common Task', ABOVE, LEFT)"
								onclick="savetask();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/savedisk1.gif"
								runat="server"> <IMG id="ir1" runat="server" onmouseover="return overlib('Add Task and Return This Task to my TPM Task Screen', ABOVE, LEFT)"
								onclick="returntask('add');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/return2grn.gif">
							<IMG id="ir2" runat="server" onmouseover="return overlib('Save Changes and Return This Task to my TPM Task Screen', ABOVE, LEFT)"
								onclick="returntask('save');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/return2.gif">
							<IMG id="ir3" runat="server" onmouseover="return overlib('Return This Task to my TPM Task Screen', ABOVE, LEFT)"
								onclick="returntask('return');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/return2red.gif">
						</td>
					</tr>
				</table>
			</div>
			<div class="details" id="lstdiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 999; BORDER-LEFT: black 1px solid; WIDTH: 330px; BORDER-BOTTOM: black 1px solid; HEIGHT: 220px">
				<table cellSpacing="0" cellPadding="0" width="330" bgcolor="white">
					<tr bgColor="blue" height="20">
						<td class="labelwht"><asp:Label id="lang1237" runat="server">List Dialog</asp:Label></td>
						<td align="right"><IMG onclick="closelst();" height="18" alt="" src="../images/close.gif" width="18"><br>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><iframe id="iflst" style="WIDTH: 330px; HEIGHT: 220px" runat="server"></iframe>
						</td>
					</tr>
				</table>
			</div>
			<iframe class="details" id="ifptn" style="WIDTH: 500px; HEIGHT: 246px" src="#"></iframe>
			<input id="lblcid" type="hidden" runat="server"> <input id="lbltasktype" type="hidden" runat="server">
			<input id="lblpdm" type="hidden" runat="server"> <input id="lbltaskid" type="hidden" runat="server">
			<input id="lblmode" type="hidden" runat="server"> <input id="lblreturn" type="hidden" runat="server">
			<input id="lblorigtasktype" type="hidden" runat="server"><input id="lblorigpdm" type="hidden" runat="server">
			<input id="lblsubmit" type="hidden" runat="server"> <input type="hidden" id="lblctaskid" runat="server">
			<input type="hidden" id="lblscreen" runat="server"><input type="hidden" id="lblexit" runat="server">
			<input type="hidden" id="lbltyp" runat="server"><input type="hidden" id="lblmemo" runat="server">
			<input type="hidden" id="lblsid" runat="server"><input type="hidden" id="lbltaskholdid" runat="server">
			<input type="hidden" id="lblro" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
