<%@ Page Language="vb" AutoEventWireup="false" Codebehind="DevArchtpm.aspx.vb" Inherits="lucy_r12.DevArchtpm" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>DevArch</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/DevArchtpmaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  class="tbg">
		<form id="form1" method="post" runat="server">
			<div class="bluelabel" id="tdarch" style="OVERFLOW: auto; WIDTH: 220px; POSITION: absolute; TOP: 0px; HEIGHT: 200px"
				runat="server"></div>
			<input type="hidden" id="lblchk" runat="server"> <input type="hidden" id="lbldid" runat="server">
			<input type="hidden" id="lblclid" runat="server"><input type="hidden" id="lblloc" runat="server">
			<input type="hidden" id="lblsid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
