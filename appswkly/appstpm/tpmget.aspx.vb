Imports System.Data.SqlClient
Public Class tpmget1
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim tasks As New Utilities
    Dim sql, cid, sid, eq, eqid, fu, fuid, did, clid, jump, typ, lid, ro, appstr, task As String
    Dim Filter, filt, dt, df, tl, val, tli As String
    Dim pmid, type, login As String
    Dim tpma As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lang246 As System.Web.UI.WebControls.Label
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents tddepts As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlocs1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trdepts As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tddept As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgaddeq As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgcopyeq As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdcell As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgaddfu As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgcopyfu As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgtpma As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents trlocs As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdloc3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgaddeq1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgcopyeq1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdfu3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgaddfu1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgcopyfu1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img11 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img21 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgtpma1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents trrev As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdrev As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Img5 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img6 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmflg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldoctype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcleantasks As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgototasks As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgetarch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhaspm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnoeq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrettyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcell As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllevel As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            lblgototasks.Value = "0"
            lblcleantasks.Value = "2"
            lbltaskcnt.Value = "2"
            cid = "0" 'HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            'Read Only
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            appstr = HttpContext.Current.Session("appstr").ToString()
            CheckApps(appstr)
            'End Read Only
            tasks.Open()

            Try
                tli = Request.QueryString("tli").ToString
                lbltl.Value = tli
            Catch ex As Exception
                'need to review what the heck this is doing here
            End Try
            jump = Request.QueryString("jump").ToString
            Dim deptcheck As Integer = 0
            Dim fuidcheck As Integer = 0
            If jump = "yes" Then
                Try
                    task = Request.QueryString("task").ToString
                    lbltask.Value = task
                Catch ex As Exception

                End Try
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                Dim eqcheck As Integer = 0
                If typ = "loc" Or typ = "dloc" Then
                    If typ = "loc" Then
                        trdepts.Attributes.Add("class", "details")
                    End If
                    trlocs.Attributes.Add("class", "view")
                    lid = Request.QueryString("lid").ToString
                    lbllid.Value = lid
                    did = Request.QueryString("did").ToString
                    If did = "" Then
                        deptcheck = 1
                    Else
                        Try
                            lbldid.Value = did
                            Filter = did
                        Catch ex As Exception
                            deptcheck = 1
                        End Try
                        If deptcheck <> 1 Then
                            Dim cellchk As Integer = 0
                            Dim chk As String = CellCheck(Filter)
                            If chk = "no" Then
                                lblchk.Value = "no"
                            Else
                                lblchk.Value = "yes"
                                clid = Request.QueryString("clid").ToString
                                Try
                                    lblclid.Value = clid
                                    lblpar.Value = "cell"
                                Catch ex As Exception
                                    cellchk = 1
                                End Try
                            End If
                        End If
                    End If
                    eqid = Request.QueryString("eqid").ToString
                    Try
                        lbleqid.Value = eqid
                        GetRev(eqid)
                    Catch ex As Exception
                        eqcheck = 1
                    End Try
                    If eqcheck <> 1 Then
                        fuid = Request.QueryString("funid").ToString
                        Try
                            lblfuid.Value = fuid
                        Catch ex As Exception
                            fuidcheck = 1
                        End Try
                        If fuid = "" Then
                            fuidcheck = "1"
                        End If
                        If fuidcheck <> 1 Then
                            CheckTasks(fuid)
                            Dim coid As String
                            Try
                                coid = Request.QueryString("comid").ToString
                                lblcoid.Value = coid
                            Catch ex As Exception

                            End Try
                            If lblcoid.Value <> "" AndAlso lblcoid.Value <> "0" Then
                                lbltaskcnt.Value = "1"
                                GoToTask(coid)
                            Else
                                GoToTasks("yes")
                            End If
                        Else
                            lblgototasks.Value = "0"
                        End If
                    End If
                    'Need details here for locs
                    getlocs(lid, eqid, fuid)
                Else
                    trdepts.Attributes.Add("class", "view")
                    did = Request.QueryString("did").ToString
                    Try
                        lbldid.Value = did
                        Filter = did
                    Catch ex As Exception
                        deptcheck = 1
                    End Try
                    If deptcheck <> 1 Then
                        Dim cellchk As Integer = 0
                        Dim chk As String = CellCheck(Filter)
                        If chk = "no" Then
                            lblchk.Value = "no"
                        Else
                            lblchk.Value = "yes"
                            clid = Request.QueryString("clid").ToString
                            Try
                                lblclid.Value = clid
                                lblpar.Value = "cell"
                            Catch ex As Exception
                                cellchk = 1
                            End Try
                        End If
                        If cellchk <> 1 Then
                            eqid = Request.QueryString("eqid").ToString
                            Try
                                lbleqid.Value = eqid
                                GetRev(eqid)
                            Catch ex As Exception
                                eqcheck = 1
                            End Try
                            If tli <> "4" Then
                                If eqcheck <> 1 Then
                                    Try
                                        fuid = Request.QueryString("funid").ToString
                                        lblfuid.Value = fuid
                                    Catch ex As Exception
                                        fuidcheck = 1
                                    End Try
                                    If fuid = "" Then
                                        fuidcheck = "1"
                                    End If
                                    If fuidcheck <> 1 Then
                                        CheckTasks(fuid)
                                        Dim coid As String
                                        Try
                                            coid = Request.QueryString("comid").ToString
                                            lblcoid.Value = coid
                                        Catch ex As Exception

                                        End Try
                                        If lblcoid.Value <> "" AndAlso lblcoid.Value <> "0" Then
                                            lbltaskcnt.Value = "1"
                                            GoToTask(coid)
                                        Else
                                            GoToTasks("yes")
                                        End If
                                    Else
                                        lblgototasks.Value = "0"
                                    End If
                                Else
                                    GoToTasks()
                                End If
                            Else
                                lbltaskcnt.Value = "4"
                                GoToTasks()
                            End If
                        End If
                    End If
                    'need details for depts here
                    getdepts(did, clid, eqid, fuid)
                End If 'locs or depts

            End If

            tasks.Dispose()
        Else
            If Request.Form("lblpchk") = "eq" Then
                tasks.Open()
                eqid = lbleqid.Value
                lblpchk.Value = ""
                lbltaskcnt.Value = "4"
                CleanTasks()
                GoToTasks()
                tasks.Dispose()
            ElseIf Request.Form("lblpchk") = "func" Then
                eqid = lbleqid.Value
                tasks.Open()
                fuid = lblfuid.Value
                If fuid <> "0" AndAlso fuid <> "undefined" Then
                    Try
                        CheckTasks(fuid)
                    Catch ex As Exception
                        lbltaskcnt.Value = "5"
                        lblcleantasks.Value = "0"
                        CleanTasks()
                        GoToTasks()
                    End Try
                Else
                    lbltaskcnt.Value = "5"
                    lblcleantasks.Value = "0"
                    CleanTasks()
                    GoToTasks()
                End If
                lblpchk.Value = ""
                GoToTasks()
                tasks.Dispose()
            ElseIf Request.Form("lblpchk") = "archit" Then
                lblpchk.Value = ""
                tasks.Open()
                ArchiveEq()
                tasks.Dispose()
            End If
        End If

    End Sub

    Private Sub ArchiveEq()
        cid = lblcid.Value
        lid = lbllid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        sid = lblsid.Value
        eqid = lbleqid.Value
        typ = "N"
        Dim newid As Integer
        Dim user As String = HttpContext.Current.Session("username").ToString '"PM Administrator"
        Session("username") = user
        Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)
        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_copyEqArch @cid, @sid, @did, @clid, @eqid, @ustr, @lid, @typ"
        'sql = "usp_copyEqArch '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & ustr & "', '" & lid & "', 'N'"
        Dim param = New SqlParameter("@cid", SqlDbType.Int)
        param.Value = cid
        cmd.Parameters.Add(param)
        Dim param01 = New SqlParameter("@sid", SqlDbType.VarChar)
        If sid = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = sid
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@did", SqlDbType.VarChar)
        If did = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = did
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@clid", SqlDbType.VarChar)
        If clid = "" Then
            param03.Value = System.DBNull.Value
        Else
            param03.Value = clid
        End If
        cmd.Parameters.Add(param03)
        Dim param04 = New SqlParameter("@eqid", SqlDbType.VarChar)
        If eqid = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = eqid
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@ustr", SqlDbType.VarChar)
        If ustr = "" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = ustr
        End If
        cmd.Parameters.Add(param05)
        Dim param06 = New SqlParameter("@lid", SqlDbType.VarChar)
        If lid = "" Then
            param06.Value = System.DBNull.Value
        Else
            param06.Value = lid
        End If
        cmd.Parameters.Add(param06)
        Dim param07 = New SqlParameter("@typ", SqlDbType.VarChar)
        If typ = "" Then
            param07.Value = System.DBNull.Value
        Else
            param07.Value = typ
        End If
        cmd.Parameters.Add(param07)

        newid = tasks.ScalarHack(cmd)
        'tdrev.InnerHtml = newid
        GetRev(eqid)
    End Sub
    Private Sub CleanTasks()
        Dim tskchk As String = lblcleantasks.Value
        If tskchk = "0" Then
            lblcleantasks.Value = "1"
            lblgototasks.Value = "1"
        End If
    End Sub
    Private Sub getlocs(ByVal lid As String, ByVal eqid As String, ByVal fuid As String)
        If fuid = "" Or fuid = "no" Then
            fuid = "0"
        End If
        sql = "declare @loc varchar(50), @eq varchar(50), @fu varchar(50); " _
        + "set @loc = (select location from pmlocations where locid = '" & lid & "'); " _
        + "set @eq = (select eqnum from equipment where eqid = '" & eqid & "'); " _
        + "set @fu = (select func from functions where func_id = '" & fuid & "') " _
        + "select @loc as 'loc', @eq as 'eq', @fu as 'fu'"
        Dim loc, eqnum, func, comp As String
        dr = tasks.GetRdrData(sql)
        While dr.Read
            loc = dr.Item("loc").ToString
            eqnum = dr.Item("eq").ToString
            func = dr.Item("fu").ToString
        End While
        dr.Close()
        tdloc3.InnerHtml = loc
        tdeq3.InnerHtml = eqnum
        tdfu3.InnerHtml = func
    End Sub
    Private Sub getdepts(ByVal did As String, ByVal clid As String, ByVal eqid As String, ByVal fuid As String)
        If fuid = "" Or fuid = "no" Then
            fuid = "0"
        End If
        sql = "declare @dept varchar(50), @cell varchar(50), @eq varchar(50), @fu varchar(50); " _
        + "set @dept = (select dept_line from dept where dept_id = '" & did & "'); " _
        + "set @cell = (select cell_name from cells where cellid = '" & clid & "'); " _
        + "set @eq = (select eqnum from equipment where eqid = '" & eqid & "'); " _
        + "set @fu = (select func from functions where func_id = '" & fuid & "') " _
        + "select @dept as 'dept', @cell as 'cell', @eq as 'eq', @fu as 'fu'"

        Dim dept, cell, eqnum, func, comp As String
        dr = tasks.GetRdrData(sql)
        While dr.Read
            dept = dr.Item("dept").ToString
            cell = dr.Item("cell").ToString
            eqnum = dr.Item("eq").ToString
            func = dr.Item("fu").ToString
        End While
        dr.Close()
        tddept.InnerHtml = dept
        tdcell.InnerHtml = cell
        tdeq.InnerHtml = eqnum
        tdfu.InnerHtml = func
        lbldept.Value = dept
        lblcell.Value = cell
        lbleq.Value = eq
        lblfu.Value = fu
    End Sub
    Private Sub CheckApps(ByVal appstr As String)
        Dim apparr() As String = appstr.Split(",")
        Dim e As String = "0"
        'eq,dev,opt,inv
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                If apparr(i) = "eq" Then
                    e = "1"
                End If
            Next
            If e <> "1" Then
                lblnoeq.Value = "1"
                imgaddeq.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                imgaddeq.Attributes.Add("onclick", "")
                imgaddfu.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                imgaddfu.Attributes.Add("onclick", "")
                imgcopyeq.Attributes.Add("onclick", "")
                imgcopyfu.Attributes.Add("onclick", "")
            End If
        Else
            lblnoeq.Value = "0"
        End If

    End Sub
    Private Sub GetRev(ByVal eqid As String)
        sql = "select rev, usetot from equipment where eqid = '" & eqid & "'"
        Dim rev, tot As String
        'rev = tasks.strScalar(sql)
        dr = tasks.GetRdrData(sql)
        While dr.Read
            rev = dr.Item("rev").ToString

        End While
        dr.Close()
        tdrev.InnerHtml = rev
        trrev.Attributes.Add("class", "view")

    End Sub
    Public Function CellCheck(ByVal filt As String) As String
        Dim cells As String
        Dim cellcnt As Integer
        sql = "select count(*) from Cells where Dept_ID = '" & filt & "'"
        cellcnt = tasks.Scalar(sql)
        Dim nocellcnt As Integer
        If cellcnt = 0 Then
            cells = "no"
        ElseIf cellcnt = 1 Then
            sql = "select count(*) from Cells where Dept_ID = '" & filt & "' and cell_name = 'No Cells'"
            nocellcnt = tasks.Scalar(sql)
            If nocellcnt = 1 Then
                cells = "no"
            Else
                cells = "yes"
            End If
        Else
            cells = "yes"
        End If
        Return cells
    End Function
    Private Sub CheckTasks(ByVal fu As String)
        If fu <> "" And fu <> "no" Then
            sql = "select count(*) from pmtasks where funcid = '" & fu & "'"
            Dim cnt As Integer
            cnt = tasks.Scalar(sql)
            If cnt = 0 Then
                lbltaskcnt.Value = "0"
            Else
                lbltaskcnt.Value = "1"
                'CheckTPA(fu)
            End If
        End If

    End Sub
    Private Sub CheckTPA(ByVal fu As String)
        sql = "exec usp_checktpmfunccnt '" & fu & "'"
        tpma = tasks.Scalar(sql)
        If tpma > 0 Then
            imgtpma.Attributes.Add("class", "visible")
        End If
    End Sub
    Private Sub GoToTask(ByVal coid As String)
        Dim tn As Integer
        Dim tasknum, chk As String
        lblcoid.Value = coid
        tl = "5" 'lbltasklev.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        chk = lblchk.Value
        fuid = lblfuid.Value
        'sql = "select top(1) tasknum from pmtasks where funcid = '" & fuid & "' and comid = '" & coid & "'"
        'tn = tasks.Scalar(sql)
        'tasknum = tn

        lblgototasks.Value = "1"
        lblcleantasks.Value = "0"
        'Response.Redirect("PMTaskDivFuncGrid.aspx?start=yes&tl=5&chk=" & chk & "&cid=" & _
        'cid + "&fuid=" & fuid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & _
        '"&eqid=" & eqid & "&task=" & tasknum & "&comid=" & coid)

    End Sub
    Private Sub GoToTasks(Optional ByVal jump As String = "no")
        tl = "5" 'lbltl.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        Dim pmstr, doctyp As String
        pmstr = lblpmnum.Value
        doctyp = lbldoctype.Value
        Dim chk As String
        chk = lblchk.Value
        lblgototasks.Value = "1"
        lblcleantasks.Value = "0"
    End Sub
End Class

