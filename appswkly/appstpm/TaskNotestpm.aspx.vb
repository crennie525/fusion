

'********************************************************
'*
'********************************************************




Imports System.Data.SqlClient

Public Class TaskNotestpm
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents lbltsk As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents lblsub As System.Web.UI.WebControls.Label
    Protected WithEvents txtnote As System.Web.UI.WebControls.TextBox
    Protected WithEvents ImageButton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblptid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim ptid, Login, ro As String
    Dim dr As SqlDataReader
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim notes As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try

        If Not IsPostBack Then
            Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                If ro = "1" Then
                    ImageButton1.ImageUrl = "../images/appbuttons/bgbuttons/savedis.gif"
                    ImageButton1.Enabled = False
                Else
                    'ImageButton1.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
                    'ImageButton1.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")


                End If
                ptid = Request.QueryString("ptid")
                lblptid.Value = ptid
                If Len(ptid) <> 0 AndAlso ptid <> "" AndAlso ptid <> "0" Then
                    notes.Open()
                    GetTaskNums()
                    GetNote()
                Else
                    Dim strMessage As String = tmod.getmsg("cdstr528" , "TaskNotestpm.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "nodeptid"
                End If
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr529" , "TaskNotestpm.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "nodeptid"
            End Try

        End If

    End Sub
    Private Sub GetTaskNums()
        ptid = lblptid.Value
        sql = "select tasknum, subtask from pmtaskstpm " _
        + "where pmtskid = '" & ptid & "'"
        dr = notes.GetRdrData(sql)
        If dr.Read Then
            lbltsk.Text = dr.Item(0).ToString
            lblsub.Text = dr.Item(1).ToString
        End If
        dr.Close()
    End Sub
    Private Sub GetNote()
        Try
            notes.Open()
        Catch ex As Exception

        End Try
        ptid = lblptid.Value
        sql = "select note from pmtasknotestpm " _
        + "where pmtskid = '" & ptid & "'"
        dr = notes.GetRdrData(sql)
        If dr.Read Then
            txtnote.Text = dr.Item(0).ToString
        End If
        dr.Close()
        notes.Dispose()
    End Sub


    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim notecnt As Integer
        ptid = lblptid.Value
        Dim txt As String = txtnote.Text
        txt = Replace(txt, "'", Chr(180), , , vbTextCompare)
        txt = Replace(txt, "--", "-", , , vbTextCompare)
        txt = Replace(txt, ";", ":", , , vbTextCompare)
        notes.Open()
        sql = "select count (*) from pmtasknotes " _
        + "where pmtskid = '" & ptid & "'"
        notecnt = notes.Scalar(sql)
        If notecnt = 0 Then
            sql = " insert into pmtasknotestpm " _
            + "(pmtskid, note) values ('" & ptid & "', '" & txt & "')"
            notes.Update(sql)
        Else
            sql = "update pmtasknotestpm set " _
                        + "note = '" & txt & "' where pmtskid = '" & ptid & "'"
            notes.Update(sql)
        End If
        notes.Dispose()
    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			Label1.Text = axlabs.GetASPXPage("TaskNotestpm.aspx","Label1")
		Catch ex As Exception
		End Try
		Try
			Label2.Text = axlabs.GetASPXPage("TaskNotestpm.aspx","Label2")
		Catch ex As Exception
		End Try

	End Sub

	

	

	Private Sub GetBGBLangs()
		Dim lang as String = lblfslang.value
		Try
			If lang = "eng" Then
			ImageButton1.Attributes.Add("src" , "../images2/eng/bgbuttons/save.gif")
			ElseIf lang = "fre" Then
			ImageButton1.Attributes.Add("src" , "../images2/fre/bgbuttons/save.gif")
			ElseIf lang = "ger" Then
			ImageButton1.Attributes.Add("src" , "../images2/ger/bgbuttons/save.gif")
			ElseIf lang = "ita" Then
			ImageButton1.Attributes.Add("src" , "../images2/ita/bgbuttons/save.gif")
			ElseIf lang = "spa" Then
			ImageButton1.Attributes.Add("src" , "../images2/spa/bgbuttons/save.gif")
			End If
		Catch ex As Exception
		End Try

	End Sub

End Class

