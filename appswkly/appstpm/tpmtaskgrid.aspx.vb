

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class tpmtaskgrid
    Inherits System.Web.UI.Page
	Protected WithEvents lang1287 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1286 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1285 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1284 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim ds As DataSet
    Dim tasksg As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim start, tl, cid, sid, did, clid, eqid, chk, fuid, Val, field, name, Filter, coid, typ, lid As String
    Dim compnum, optsort, task As String
    Protected WithEvents lang1042 As System.Web.UI.WebControls.Label
    Protected WithEvents tdaddtask As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsort As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcompsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdcomp As System.Web.UI.WebControls.Label
    Protected WithEvents imgaddcomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcompnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim co_only As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rptrtasks As System.Web.UI.WebControls.Repeater
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents taskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here

        If Not IsPostBack Then
            tasksg.Open()
            lblcurrsort.Value = "tasknum"
            start = Request.QueryString("start").ToString
            taskcnt.Value = "0"
            If start = "yes" Then
                tl = Request.QueryString("tl").ToString
                lbltasklev.Value = tl
                cid = Request.QueryString("cid").ToString
                lblcid.Value = cid
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                did = Request.QueryString("did").ToString
                lbldid.Value = did
                clid = Request.QueryString("clid").ToString
                lblclid.Value = clid
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                chk = Request.QueryString("chk").ToString
                lblchk.Value = chk
                fuid = Request.QueryString("fuid").ToString
                lblfuid.Value = fuid
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                lid = Request.QueryString("lid").ToString
                lbllid.Value = lid
                lblfilt.Value = Filter
                Try
                    coid = Request.QueryString("comid").ToString
                Catch ex As Exception
                    coid = ""
                End Try
                Try
                    task = Request.QueryString("task").ToString
                Catch ex As Exception
                    task = ""
                End Try
                lblcoid.Value = coid
                Try
                    optsort = HttpContext.Current.Session("optsort").ToString
                    lblcurrsort.Value = optsort
                Catch ex As Exception
                    lblcurrsort.Value = "tasknum"
                End Try
                If fuid <> "0" Then
                    Try
                        tasksg.Open()
                    Catch ex As Exception

                    End Try

                    If task <> "" Then
                        GoToTPMTask(coid, task)
                        tasksg.Dispose()
                    ElseIf coid <> "" AndAlso coid <> "0" Then
                        GoToTask(coid)
                        tasksg.Dispose()
                    Else
                        LoadPage(fuid)
                        tdaddtask.Attributes.Add("class", "visible")
                    End If
                Else
                    tdaddtask.Attributes.Add("class", "details")
                    fuid = "na"
                    Try
                        tasksg.Open()
                    Catch ex As Exception

                    End Try

                    LoadPage(fuid)
                    tasksg.Dispose()
                End If
            Else
                fuid = "na"
                If fuid <> "0" Then
                    LoadPage(fuid)
                    tdaddtask.Attributes.Add("class", "visible")
                End If
            End If
            Try
                tasksg.Dispose()
            Catch ex As Exception

            End Try
        Else
            If Request.Form("lblsubmit") = "addnew" Then
                tasksg.Open()
                AddTask()
                tasksg.Dispose()
            ElseIf Request.Form("lblsubmit") = "addnewcomp" Then
                tasksg.Open()
                compnum = lblcompnum.Value
                coid = lblcoid.Value
                AddTask2(coid, compnum)
                tasksg.Dispose()
            End If
        End If
       

    End Sub
    Public Sub SortTasks(ByVal sender As Object, ByVal e As System.EventArgs)
        lblcurrsort.Value = "tasknum"
        Session("optsort") = "tasknum"
        fuid = lblfuid.Value
        tasksg.Open()
        LoadPage(fuid)
        tasksg.Dispose()

    End Sub
    Public Sub SortCompTasks(ByVal sender As Object, ByVal e As System.EventArgs)
        lblcurrsort.Value = "compnum"
        Session("optsort") = "compnum"
        fuid = lblfuid.Value
        tasksg.Open()
        LoadPage(fuid)
        tasksg.Dispose()

    End Sub
    Private Sub AddTask()
        If Len(lblfuid.Value) <> 0 Then

            tl = lbltasklev.Value
            cid = lblcid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            eqid = lbleqid.Value
            fuid = lblfuid.Value

            tl = 5
            Dim newtask As Integer
            Dim usr As String = HttpContext.Current.Session("username").ToString()
            Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
            sql = "usp_AddTaskTPM '" & tl & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & ustr & "'"
            'sql = "usp_AddTask '" & tl & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & ustr & "'"
            newtask = tasksg.Scalar(sql)
            sql = "select tasknum from pmtaskstpm where pmtskid = '" & newtask & "'"
            newtask = tasksg.Scalar(sql)
            GoToTPMTask("", newtask)

        End If
    End Sub
    Private Sub AddTask2(ByVal coid As String, ByVal compnum As String)
        If Len(lblfuid.Value) <> 0 And coid <> "" And coid <> "0" Then

            tl = lbltasklev.Value
            cid = lblcid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            eqid = lbleqid.Value
            fuid = lblfuid.Value

            tl = 5
            Dim newid, newtask As Integer
            Dim usr As String = HttpContext.Current.Session("username").ToString()
            Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
            sql = "usp_AddTaskTPM '" & tl & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & ustr & "'"
            'sql = "usp_AddTask '" & tl & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & ustr & "'"
            newid = tasksg.Scalar(sql)
            sql = "usp_inserttpmComp '" & coid & "', '" & compnum & "', '" & newid & "', '" & fuid & "'"
            tasksg.Update(sql)
            sql = "select tasknum from pmtaskstpm where pmtskid = '" & newid & "'"
            newtask = tasksg.Scalar(sql)
            GoToTPMTask(coid, newtask)

        End If
    End Sub
    Private Sub GoToTPMTask(ByVal coid As String, ByVal tasknum As String)
        tl = lbltasklev.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        chk = lblchk.Value
        fuid = lblfuid.Value
        typ = lbltyp.Value
        lid = lbllid.Value
        'Dim strMessage As String = "PMOptTasksGrid"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Response.Redirect("tpmtasks.aspx?start=yes&tl=5&chk=" & chk & "&cid=" & _
        cid + "&fuid=" & fuid & "&comid=" & coid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & _
        "&eqid=" & eqid & "&task=" & tasknum & "&typ=" & typ & "&lid=" & lid)
    End Sub
    Private Sub LoadPage(ByVal fuid As String)
        Dim cnt As Integer
        Dim sqlcnt As String
        If fuid = "na" Then
            cnt = 0
            sql = "select * from pmtaskstpm where pmtskid = 0"
        Else
            cnt = 1
            Dim sort As String = lblcurrsort.Value
            If sort = "tasknum" Then
                tdsort.InnerHtml = "Task# Ascending"
                sql = "select t.pmtskid, t.tasknum, t.taskdesc, tpmhold, " _
                + "isnull(c.compnum + ' - ' + isnull(c.compdesc, ''), 'None Selected') as compnum " _
                + "from pmtaskstpm t left outer join components c on c.comid = t.comid " _
                + "where subtask = 0 and t.funcid = '" & fuid & "' " _
                + "order by t.tasknum"
            Else
                tdsort.InnerHtml = "Component# Ascending"
                sql = "select t.pmtskid, t.tasknum, t.taskdesc, tpmhold, " _
                + "isnull(c.compnum + ' - ' + isnull(c.compdesc, ''), 'None Selected') as compnum " _
                + "from pmtaskstpm t left outer join components c on c.comid = t.comid " _
                + "where subtask = 0 and t.funcid = '" & fuid & "' " _
                + "order by c.compnum, t.tasknum"
            End If

            sqlcnt = "select count(*) from pmtaskstpm where subtask = 0 and funcid = '" & fuid & "'"
        End If
        If cnt <> 0 Then
            cnt = tasksg.Scalar(sqlcnt)
            taskcnt.Value = cnt
        Else
            taskcnt.Value = "0"
        End If
        ds = tasksg.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        rptrtasks.DataSource = dt
        Dim i, eint As Integer
        eint = 15
        For i = cnt To eint
            dt.Rows.InsertAt(dt.NewRow(), i)
        Next
        rptrtasks.DataBind()
    End Sub
    Private Sub GoToTask(ByVal coid As String)
        Dim tn As Integer
        Dim tasknum As String
        tl = lbltasklev.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        chk = lblchk.Value
        fuid = lblfuid.Value
        typ = lbltyp.Value
        lid = lbllid.Value
        sql = "select tasknum from pmtaskstpm where funcid = '" & fuid & "' and comid = '" & coid & "'" ' and tpm = '1'"
        dr = tasksg.GetRdrData(sql)
        While dr.Read
            tasknum = dr.Item("tasknum").ToString
        End While
        dr.Close()
        'tasksg.Dispose()
        If tasknum <> "" OrElse tasknum <> Nothing Then
            Response.Redirect("tpmtasks.aspx?start=yes&tl=5&chk=" & chk & "&cid=" & _
                   cid + "&fuid=" & fuid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & _
                   "&eqid=" & eqid & "&task=" & tasknum & "&comid=" & coid & "&typ=" & typ & "&lid=" & lid)
        Else
            'Response.Redirect("PMTaskDivFunc.aspx?start=no")
            sql = "select compnum + ' - ' + isnull(compdesc, '') as compnum " _
           + "from components where comid = '" & coid & "'"
            Dim cnum As String
            cnum = tasksg.strScalar(sql)
            lblcompnum.Value = cnum
            lbladdcomp.Visible = True
            imgaddcomp.Attributes.Add("class", "view")
            lbladdcomp.Text = "Add Task to Component: " & cnum
            LoadPage(fuid)
        End If


    End Sub
    Private Sub rptrtasks_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrtasks.ItemCommand
        If e.CommandName = "Select" Then
            Dim tasknum, comid As String
            Try
                tasknum = CType(e.Item.FindControl("lbltn"), LinkButton).Text
            Catch ex As Exception
                tasknum = CType(e.Item.FindControl("lbltnalt"), LinkButton).Text
            End Try
            tl = lbltasklev.Value
            cid = lblcid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            eqid = lbleqid.Value
            chk = lblchk.Value
            fuid = lblfuid.Value
            coid = lblcoid.Value
            typ = lbltyp.Value
            lid = lbllid.Value
            Response.Redirect("tpmtasks.aspx?start=yes&tl=5&chk=" & chk & "&cid=" & _
            cid + "&fuid=" & fuid & "&comid=" & coid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & _
            "&eqid=" & eqid & "&task=" & tasknum & "&typ=" & typ & "&lid=" & lid)

        End If
    End Sub

    Private Sub rptrtasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtasks.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Then
            Dim llabel As LinkButton = CType(e.Item.FindControl("lbltn"), LinkButton)
            Dim dlabel As Label = CType(e.Item.FindControl("Label1"), Label)
            Dim clabel As Label = CType(e.Item.FindControl("lbleqdesc"), Label)
            Dim hold As String = DataBinder.Eval(e.Item.DataItem, "tpmhold").ToString
            Dim cpcol As String = "Red"

            If hold = "1" Then
                clabel.ForeColor = System.Drawing.Color.FromName(cpcol)
                dlabel.ForeColor = System.Drawing.Color.FromName(cpcol)
                llabel.ForeColor = System.Drawing.Color.FromName(cpcol)
            End If

        ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim llabel As LinkButton = CType(e.Item.FindControl("lbltnalt"), LinkButton)
            Dim dlabel As Label = CType(e.Item.FindControl("Label3"), Label)
            Dim clabel As Label = CType(e.Item.FindControl("Label2"), Label)
            Dim hold As String = DataBinder.Eval(e.Item.DataItem, "tpmhold").ToString
            Dim cpcol As String = "Red"

            If hold = "1" Then
                clabel.ForeColor = System.Drawing.Color.FromName(cpcol)
                dlabel.ForeColor = System.Drawing.Color.FromName(cpcol)
                llabel.ForeColor = System.Drawing.Color.FromName(cpcol)
            End If
        End If

        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang1284 As Label
                lang1284 = CType(e.Item.FindControl("lang1284"), Label)
                lang1284.Text = axlabs.GetASPXPage("tpmtaskgrid.aspx", "lang1284")
            Catch ex As Exception
            End Try
            Try
                Dim lang1285 As Label
                lang1285 = CType(e.Item.FindControl("lang1285"), Label)
                lang1285.Text = axlabs.GetASPXPage("tpmtaskgrid.aspx", "lang1285")
            Catch ex As Exception
            End Try
            Try
                Dim lang1286 As Label
                lang1286 = CType(e.Item.FindControl("lang1286"), Label)
                lang1286.Text = axlabs.GetASPXPage("tpmtaskgrid.aspx", "lang1286")
            Catch ex As Exception
            End Try
            Try
                Dim lang1287 As Label
                lang1287 = CType(e.Item.FindControl("lang1287"), Label)
                lang1287.Text = axlabs.GetASPXPage("tpmtaskgrid.aspx", "lang1287")
            Catch ex As Exception
            End Try

        End If

    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1284.Text = axlabs.GetASPXPage("tpmtaskgrid.aspx", "lang1284")
        Catch ex As Exception
        End Try
        Try
            lang1285.Text = axlabs.GetASPXPage("tpmtaskgrid.aspx", "lang1285")
        Catch ex As Exception
        End Try
        Try
            lang1286.Text = axlabs.GetASPXPage("tpmtaskgrid.aspx", "lang1286")
        Catch ex As Exception
        End Try
        Try
            lang1287.Text = axlabs.GetASPXPage("tpmtaskgrid.aspx", "lang1287")
        Catch ex As Exception
        End Try

    End Sub

End Class
