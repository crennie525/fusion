

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class nsquery
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim nsq As New Utilities
    Dim dr As SqlDataReader

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Dim sb As New StringBuilder
            sb.Append("<table>")
            Dim dept As String
            sql = "select dept_line from dept"
            nsq.Open()
            dr = nsq.GetRdrData(sql)
            While dr.Read
                dept = dr.Item("dept_line").ToString
                sb.Append("<tr><td>" & dept & "</td></tr>")

            End While
            dr.Close()
            nsq.Dispose()
            sb.Append("</table>")
            Response.Write(sb.ToString)
        End If
    End Sub

End Class
