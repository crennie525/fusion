﻿Public Class PMOStatusDataModel

    Private _site As String
    Private _pmostatusid As Integer = 0
    Private _statuscount As Integer = 0

    Public Property Site As String
        Get
            Return _site
        End Get
        Set(value As String)
            _site = value
        End Set
    End Property

    Public Property PMOStatusID As Integer
        Get
            Return _pmostatusid
        End Get
        Set(value As Integer)
            _pmostatusid = value
        End Set
    End Property

    Public Property StatusCount As Integer
        Get
            Return _statuscount
        End Get
        Set(value As Integer)
            _statuscount = value
        End Set
    End Property

End Class
