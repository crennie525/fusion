﻿
Imports System.IO

Public Class ParentChildReportDownload
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Not HttpContext.Current.Session("Logged_In") Is Nothing) Then

            Dim iSite = -1
            Dim iAssetClass = -1
            Dim sEquipFilter = "-1"

            If (Not Request.QueryString("s") Is Nothing) Then

                iSite = CType(Request.QueryString("s"), Integer)
            End If

            If (Not Request.QueryString("a") Is Nothing) Then

                iAssetClass = CType(Request.QueryString("a"), Integer)
            End If

            If (Not Request.QueryString("e") Is Nothing) Then

                sEquipFilter = Request.QueryString("e").ToString()
            End If

            Dim equipmentData = DashBoardWebData.GetMasterRecordHierarchyDownload(iSite, iAssetClass, sEquipFilter)
            gvParentChild.DataSource = equipmentData
            gvParentChild.DataBind()

            Response.Clear()
            Response.Buffer = True

            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("content-disposition", " filename=MasterRecordHierarchy.xls")
            Dim stringWriter = New StringWriter()

            Dim htmlTextWriter = New HtmlTextWriter(stringWriter)

            Me.Page.RenderControl(htmlTextWriter)
            Response.Write(stringWriter.ToString())
            Response.End()
        End If
    End Sub

    Sub gvParentChild_OnRowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

        If e.Row.RowType = DataControlRowType.DataRow Then

            e.Row.Cells(1).Width = New Unit("700")
        End If

    End Sub

End Class