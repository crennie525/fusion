﻿Public Class PMOTimeSavingsDataModel

    Private _siteName As String
    Private _originalHours As Double
    Private _optimizedHours As Double

    Public Property SiteName As String
        Get
            Return _siteName
        End Get
        Set(value As String)
            _siteName = value
        End Set
    End Property

    Public Property OriginalHours As Double
        Get
            Return _originalHours
        End Get
        Set(value As Double)
            _originalHours = value
        End Set
    End Property

    Public Property OptimizedHours As Double
        Get
            Return _optimizedHours
        End Get
        Set(value As Double)
            _optimizedHours = value
        End Set
    End Property
End Class
