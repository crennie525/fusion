﻿
Public Class dashboardUtil

    Public Shared Sub GetSites(ByRef dropDownList As DropDownList, ByVal bShowSitesTotal As Boolean)
        Dim dbUtil As New Utilities
        dbUtil.Open()

        Dim dbReader = dbUtil.GetRdrData("select siteid, sitename from sites where sitename != 'Practice Site'")
        Dim sSiteId As String
        Dim sSiteName As String

        Dim objAllSites As New ListItem("All Sites", "-1")
        dropDownList.Items.Add(objAllSites)

        If (bShowSitesTotal) Then
            Dim objAllSitesTotal As New ListItem("All Sites Total", "-2")
            dropDownList.Items.Add(objAllSitesTotal)
        End If

        While dbReader.Read

            sSiteId = dbReader("siteid").ToString()

            Select Case dbReader("sitename").ToString()

                Case "JU"
                    sSiteName = "Juncos"
                Case "WG"
                    sSiteName = "Rhode Island"
                Case "TO MFG"
                    sSiteName = "Thousand Oaks"
                Case "DU"
                    sSiteName = "Dublin"
                Case "TO Site/GFO"
                    sSiteName = "Thousand Oaks and GFO Sites"
                Case Else
                    sSiteName = dbReader("sitename").ToString()
            End Select

            Dim objListItem As New ListItem(sSiteName, sSiteId)
            dropDownList.Items.Add(objListItem)
        End While

    End Sub

    Public Shared Sub GetAssetClasses(ByRef dropDownList As DropDownList)
        Dim dbUtil As New Utilities
        dbUtil.Open()

        Dim dbReader = dbUtil.GetRdrData("select assetclass, acid from pmAssetClass")
        dropDownList.DataSource = dbReader
        dropDownList.DataTextField = "assetclass"
        dropDownList.DataValueField = "acid"
        dropDownList.DataBind()

        dbReader.Close()
        dropDownList.Items.Insert(0, New ListItem("", "-1"))
    End Sub

	Public Shared Sub GetEquipmentFilter(ByRef dropDownList As DropDownList)
		Dim dbUtil As New Utilities
		dbUtil.Open()

		Dim dbReader = dbUtil.GetRdrData("exec usp_getEquipmentFilter")
		dropDownList.DataSource = dbReader
		dropDownList.DataTextField = "EquipmentFilter"
		dropDownList.DataValueField = "EquipmentFilter"
		dropDownList.DataBind()

		dbReader.Close()
		dropDownList.Items.Insert(0, New ListItem("", "-1"))
	End Sub

	Public Shared Sub GetEquipmentBySite(ByRef dropDownList As DropDownList)
		Dim dbUtil As New Utilities
		dbUtil.Open()

		Dim dbReader = dbUtil.GetRdrData("select eqid, eqnum from equipment where siteid = 21 order by eqnum")
		dropDownList.DataSource = dbReader
		dropDownList.DataTextField = "eqnum"
		dropDownList.DataValueField = "eqid"
		dropDownList.DataBind()

		dbReader.Close()
		dropDownList.Items.Insert(0, New ListItem("", "0"))
	End Sub

	Public Shared Sub GetWorkOrderStatus(ByRef dropDownList As DropDownList)
		Dim dbUtil As New Utilities
		dbUtil.Open()

		Dim dbReader = dbUtil.GetRdrData("select wostatus from wostatus order by wostatus")
		dropDownList.DataSource = dbReader
		dropDownList.DataTextField = "wostatus"
		dropDownList.DataValueField = "wostatus"
		dropDownList.DataBind()

		dbReader.Close()
		dropDownList.Items.Insert(0, New ListItem("", "0"))

		dropDownList.SelectedValue = "APPR"
	End Sub

	Public Shared Sub GetSupervisors(ByRef dropDownList As DropDownList)
		Dim dbUtil As New Utilities
		dbUtil.Open()

		Dim dbReader = dbUtil.GetRdrData("select distinct supervisor from workorder 
			where siteid = 21 and supervisor != '' order by supervisor")
		dropDownList.DataSource = dbReader
		dropDownList.DataTextField = "supervisor"
		dropDownList.DataValueField = "supervisor"
		dropDownList.DataBind()

		dbReader.Close()
		dropDownList.Items.Insert(0, New ListItem("", "0"))
	End Sub

	Public Shared Sub GetTypes(ByRef dropDownList As DropDownList)
		Dim dbUtil As New Utilities
		dbUtil.Open()

		Dim dbReader = dbUtil.GetRdrData("select wotype from wotype")
		dropDownList.DataSource = dbReader
		dropDownList.DataTextField = "wotype"
		dropDownList.DataValueField = "wotype"
		dropDownList.DataBind()

		dbReader.Close()
		dropDownList.Items.Insert(0, New ListItem("", "0"))
	End Sub
End Class
