﻿
Imports System.Web.Script.Serialization

Public Class PMOStatusWebData
    Inherits System.Web.UI.Page

    <System.Web.Services.WebMethod()>
    Public Shared Function GetPMOStatusCounts(siteId As Integer) As String

        Dim dbUtil As New Utilities
        dbUtil.Open()

        Dim dbReader = dbUtil.GetRdrData("exec usp_getPMOStatCounts " + siteId.ToString())
        Dim index As Integer = 0
        Dim pmoStatusArray(index) As PMOStatusDataModel

        While dbReader.Read

            Dim pmoStatusData As New PMOStatusDataModel
            pmoStatusData.Site = dbReader.Item("Site").ToString
            pmoStatusData.PMOStatusID = dbReader.Item("pmostat")
            pmoStatusData.StatusCount = dbReader.Item("StatusCount")

            ReDim Preserve pmoStatusArray(index)
            pmoStatusArray(index) = pmoStatusData
            index = index + 1
        End While
        dbReader.Close()

        dbUtil.Dispose()

        Dim serializer As New JavaScriptSerializer()
        Dim JsonData As String = serializer.Serialize(pmoStatusArray)

        Return JsonData

    End Function

End Class