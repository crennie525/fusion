﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ParentChildReport.aspx.vb" Inherits="lucy_r12.ParentChildReport" %>

<form id="form1" runat="server">
    <table style="width:90%;float:left;">
        <tr>
            <td colspan="2">
                Site Location:
                <asp:DropDownList runat="server" id="ddParentChildSites" Width="200">
                </asp:DropDownList> 
                
                &nbsp;&nbsp;
                  
                Asset Class: 
                <asp:DropDownList runat="server" id="ddParentChildAssetClasses" Width="200"></asp:DropDownList>
                
                &nbsp;&nbsp;
                  
                Equipment: 
                <asp:DropDownList runat="server" id="ddParentChildEquipmentFilter" Width="200"></asp:DropDownList> 
                
                &nbsp;&nbsp;
                <input type="button" value="Download" onclick="DownloadToExcel()"/>
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td style="vertical-align:top;padding:10px;text-align: left;">
                <table title="Equipment Records" id="MyTreeGrid" style="width:800px;height:500px;font-size:16px;">
			    </table>
            </td>
        </tr>
    </table>
</form>

<script type="text/javascript" src="../jqplot/dashboard.js"></script>
<script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
<link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />
<script type="text/javascript">

    $(document).ready(function () {

        GetEquipmentMasterRecords(-1, -1, '-1');
    });

    function DownloadToExcel() {
        var url = "ParentChildReportDownload.aspx?s=" + $("#ddParentChildSites").val() + "&a=" + $("#ddParentChildAssetClasses").val() + "&e=" + $("#ddParentChildEquipmentFilter").val();
        window.open(url);
    }
</script>

    



