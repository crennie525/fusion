﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMOTimeSavings.aspx.vb" Inherits="lucy_r12.PMOTimeSavings" %>

<form id="form1" runat="server">
    <table style="width:90%;float:left;">
        <tr>
            <td colspan="2">
                Site Location:
                <asp:DropDownList runat="server" id="ddTimeSavedSites" Width="250"></asp:DropDownList> 
                
                &nbsp;&nbsp;
                  
                Asset Class: 
                <asp:DropDownList runat="server" id="ddPMOTimeSavingAssetClasses" Width="250"></asp:DropDownList> 
            </td>
        </tr>
        <tr>
            <td id="tdChartLeft" style="width:50%;vertical-align:top;padding:10px;text-align: left;">
            </td>
            <td id="tdChartRight" style="width:50%;vertical-align:top;padding:10px;text-align: left;">
            </td>
        </tr>
    </table>
</form>
    
<script type="text/javascript" src="../jqplot/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="../jqplot/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="../jqplot/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="../jqplot/jqplot.pointLabels.min.js"></script>
<script type="text/javascript" src="../jqplot/dashboard.js"></script>
<link rel="stylesheet" type="text/css" href="../jqplot/jquery.jqplot.min.css" />

<script type="text/javascript">

    $(document).ready(function () {

        GetPMOHours(-1, -1);
    });
</script>

