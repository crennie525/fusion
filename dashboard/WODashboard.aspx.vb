﻿Public Class WODashboard
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		If (Not HttpContext.Current.Session("Logged_In") Is Nothing) Then


			hSiteId.Value = HttpContext.Current.Session("dfltps").ToString()

			If Not Me.IsPostBack Then
				dashboardUtil.GetEquipmentBySite(ddWOEquipmentFilter)
				dashboardUtil.GetWorkOrderStatus(ddStatus)
				dashboardUtil.GetSupervisors(ddSupervisor)
				dashboardUtil.GetTypes(ddType)
			End If

		End If


	End Sub

End Class