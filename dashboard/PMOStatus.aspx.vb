﻿Public Class PMOStatus
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            dashboardUtil.GetSites(ddPMOStatusSites, False)
            dashboardUtil.GetAssetClasses(ddPMOStatusAssetClasses)
        End If
    End Sub

End Class