﻿Public Class PMOMaterialSavingsDataModel

    Private _siteName As String
    Private _originalMaterials As Double
    Private _optimizedMaterials As Double

    Public Property SiteName As String
        Get
            Return _siteName
        End Get
        Set(value As String)
            _siteName = value
        End Set
    End Property

    Public Property OriginalMaterials As Double
        Get
            Return _originalMaterials
        End Get
        Set(value As Double)
            _originalMaterials = value
        End Set
    End Property

    Public Property OptimizedMaterials As Double
        Get
            Return _optimizedMaterials
        End Get
        Set(value As Double)
            _optimizedMaterials = value
        End Set
    End Property
End Class
