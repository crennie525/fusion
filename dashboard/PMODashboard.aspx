﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMODashboard.aspx.vb" Inherits="lucy_r12.PMODashboard" %>
<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PMO Dashboard</title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="margin-top:100px;">
            <table style="width:100%;">
                <tr>
                    <td id="reports" style="width:20%;vertical-align:top;">
                        <div class="dashboardLinkSelected">
                            <a id="PMOStatus" onclick="DashboardControl('PMOStatus');">Asset Optimization Status</a>
                        </div>
                        <div class="dashboardLink">
                            <a id="LaborSavings" onclick="DashboardControl('LaborSavings');">Annual Labor Savings Summary</a>
                        </div>
                        <div class="dashboardLink">
                            <a id="MaterialSavings" onclick="DashboardControl('MaterialSavings');">Annual Materials Savings Summary</a>
                        </div>
                        <div class="dashboardLink">
                            <a id="ParentChild" onclick="DashboardControl('ParentChild');">Master Record Hierarchy Report</a>
                        </div>
                        <div class="dashboardLink">
                            <a id="EquipmentNotStarted" onclick="DashboardControl('EquipmentNotStarted');">Not Started Child Records</a>
                        </div>
                    </td>
                    <td id="dashboardData" style="width:80%;vertical-align:top;">
                    </td>
                </tr>
            </table>
        </div>
    </form>
    <uc1:mmenu1 runat="server"></uc1:mmenu1>
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../dashboard/dashboard.css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("#dashboardData").load("../dashboard/pmostatus.aspx");
        });

        function DashboardControl(sReport) {
            var dataUrl;
            switch (sReport) {
                case 'PMOStatus':
                    dataUrl = "../dashboard/pmostatus.aspx";
                    break;
                case 'LaborSavings':
                    dataUrl = "../dashboard/pmotimesavings.aspx";
                    break;
                case 'MaterialSavings':
                    dataUrl = "../dashboard/pmomaterialsavings.aspx";
                    break;
                case 'ParentChild':
                    dataUrl = "../dashboard/parentchildreport.aspx";
                    break;
                case 'EquipmentNotStarted':
                    dataUrl = "../dashboard/equipmentnotstarted.aspx";
                    break;
                default:
                    dataUrl = "../dashboard/pmostatus.aspx";
                    break;
            }

            $(".dashboardLinkSelected").attr("class", "dashboardLink");
            $("#" + sReport).parent().attr("class", "dashboardLinkSelected");
            $("#dashboardData").load(dataUrl);
        }
    </script>
</body>
</html>
