﻿Public Class EquipmentDataModel

    Private _id As Integer
    Private _equipmentName As String
    Private _equipmentDesc As String
    Private _childCount As Integer
    Private _optimizedHours As Double
    Private _children As EquipmentLeafDataModel()

    Public Property id As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property

    Public Property name As String
        Get
            Return _equipmentName
        End Get
        Set(value As String)
            _equipmentName = value
        End Set
    End Property

    Public Property EquipmentDesc As String
        Get
            Return _equipmentDesc
        End Get
        Set(value As String)
            _equipmentDesc = value
        End Set
    End Property

    Public Property OptimizedHours As Double
        Get
            Return _optimizedHours
        End Get
        Set(value As Double)
            _optimizedHours = value
        End Set
    End Property

    Public Property ChildCount As Integer
        Get
            Return _childCount
        End Get
        Set(value As Integer)
            _childCount = value
        End Set
    End Property

    Public Property children As EquipmentLeafDataModel()
        Get
            Return _children
        End Get
        Set(value As EquipmentLeafDataModel())
            _children = value
        End Set
    End Property
End Class
