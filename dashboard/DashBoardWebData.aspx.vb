﻿
Imports System.Web.Script.Serialization

Public Class WorkOrderDataModel

	Public Property Display As String
	Public Property Count As Integer

End Class

Public Class WorkOrdersDataModel

	Public Property Displays As WorkOrderDataModel()
	Public Property DisplaysGreaterThan30Days As WorkOrderDataModel()
	Public Property DisplaysGreaterThan60Days As WorkOrderDataModel()

End Class

Public Class DashBoardWebData
	Inherits System.Web.UI.Page

	<System.Web.Services.WebMethod()>
	Public Shared Function GetWorkOrderTypeCounts(siteId As Integer, display As String, status As String, supervisor As String,
												  eqid As Integer, type As String, startdate As String, enddate As String) As WorkOrdersDataModel

		Dim workOrders As New WorkOrdersDataModel

		workOrders.Displays = GetWorkOrderTypeCountsByDays(siteId, display, status, eqid, supervisor, type, startdate, enddate, 0)
		workOrders.DisplaysGreaterThan30Days = GetWorkOrderTypeCountsByDays(siteId, display, status, eqid, supervisor, type, startdate, enddate, 30)
		workOrders.DisplaysGreaterThan60Days = GetWorkOrderTypeCountsByDays(siteId, display, status, eqid, supervisor, type, startdate, enddate, 60)

		Return workOrders

	End Function

	Private Shared Function GetWorkOrderTypeCountsByDays(siteId As Integer, display As String, status As String, eqid As Integer, supervisor As String,
														 type As String, startdate As String, enddate As String, days As Integer) As WorkOrderDataModel()

		Try

			Dim dbUtil As New Utilities
			dbUtil.Open()

			Dim dbReader = dbUtil.GetRdrData("exec usp_GetWorkOrderCounts " & siteId & ",'" & display & "','" & status & "'," & eqid & ",'" & supervisor & "','" & type & "','" & startdate & "','" & enddate & "'," & days)

			Dim index As Integer = 0
			Dim displayData As String
			Dim workOrderArray() As WorkOrderDataModel

			While dbReader.Read

				Dim workOrderData As New WorkOrderDataModel

				displayData = dbReader.Item("Display").ToString
				If display.ToLower() = "supervisor" And displayData <> "" Then
					Dim displayArray = displayData.Split(" ")
					If displayArray.Length > 1 Then
						displayData = displayArray(0).Substring(0, 1) & " " & displayArray(1)
					End If
				End If

				workOrderData.Display = displayData
				workOrderData.Count = dbReader.Item("Count")

				ReDim Preserve workOrderArray(index)
				workOrderArray(index) = workOrderData
				index = index + 1
			End While

			dbReader.Close()
			dbUtil.Dispose()

			Return workOrderArray

		Catch ex As Exception

		End Try
	End Function

	<System.Web.Services.WebMethod()>
	Public Shared Function GetPMOStatusCounts(siteId As Integer, assetClassId As Integer) As String

		Dim dbUtil As New Utilities
		dbUtil.Open()

		Dim dbReader = dbUtil.GetRdrData("exec usp_getPMOStatCounts " + siteId.ToString() + "," + assetClassId.ToString())
		Dim index As Integer = 0
		Dim pmoStatusArray(index) As PMOStatusDataModel

		While dbReader.Read

			Dim pmoStatusData As New PMOStatusDataModel
			pmoStatusData.Site = dbReader.Item("Site").ToString
			pmoStatusData.PMOStatusID = dbReader.Item("pmostat")
			pmoStatusData.StatusCount = dbReader.Item("StatusCount")

			ReDim Preserve pmoStatusArray(index)
			pmoStatusArray(index) = pmoStatusData
			index = index + 1
		End While
		dbReader.Close()

		dbUtil.Dispose()

		Dim serializer As New JavaScriptSerializer()
		Dim JsonData As String = serializer.Serialize(pmoStatusArray)

		Return JsonData

	End Function

	<System.Web.Services.WebMethod()>
	Public Shared Function GetPMOHours(siteId As Integer, assetClassId As Integer) As String

		Dim dbUtil As New Utilities
		dbUtil.Open()

		Dim dbReader = dbUtil.GetRdrData("exec usp_PMHoursSavedBySiteId  " + siteId.ToString() + "," + assetClassId.ToString())
		Dim index As Integer = 0
		Dim pmoHourArray(index) As PMOTimeSavingsDataModel

		While dbReader.Read

			Dim pmoHourData As New PMOTimeSavingsDataModel
			pmoHourData.SiteName = dbReader.Item("SiteName")
			pmoHourData.OriginalHours = dbReader.Item("OriginalHours")
			pmoHourData.OptimizedHours = dbReader.Item("OptimizedHours")

			ReDim Preserve pmoHourArray(index)
			pmoHourArray(index) = pmoHourData
			index = index + 1
		End While
		dbReader.Close()

		dbUtil.Dispose()

		Dim serializer As New JavaScriptSerializer()
		Dim JsonData As String = serializer.Serialize(pmoHourArray)

		Return JsonData

	End Function

	<System.Web.Services.WebMethod()>
	Public Shared Function GetPMOMaterialsSaved(siteId As Integer, assetClassId As Integer) As String

		Dim dbUtil As New Utilities
		dbUtil.Open()

		Dim dbReader = dbUtil.GetRdrData("exec usp_PMMaterialsSavedBySiteId  " + siteId.ToString() + "," + assetClassId.ToString())
		Dim index As Integer = 0
		Dim pmoMaterialArray(index) As PMOMaterialSavingsDataModel

		While dbReader.Read

			Dim pmoMaterialData As New PMOMaterialSavingsDataModel
			pmoMaterialData.SiteName = dbReader.Item("SiteName")
			pmoMaterialData.OriginalMaterials = dbReader.Item("OriginalMaterials")
			pmoMaterialData.OptimizedMaterials = dbReader.Item("OptimizedMaterials")

			ReDim Preserve pmoMaterialArray(index)
			pmoMaterialArray(index) = pmoMaterialData
			index = index + 1
		End While
		dbReader.Close()

		dbUtil.Dispose()

		Dim serializer As New JavaScriptSerializer()
		Dim JsonData As String = serializer.Serialize(pmoMaterialArray)

		Return JsonData

	End Function

	<System.Web.Services.WebMethod()>
	Public Shared Function GetMasterRecordHierarchy(siteId As Integer, assetClassId As Integer, equipmentFilter As String) As EquipmentDataModel()

		Dim dbUtil As New Utilities
		dbUtil.Open()

		Dim dbReader = dbUtil.GetRdrData("exec usp_getEquipmentMasterRecords  " + siteId.ToString() + "," + assetClassId.ToString() + ",'" +
										 equipmentFilter.ToString() + "'")
		Dim index As Integer = 0
		Dim equipmentArray() As EquipmentDataModel

		While dbReader.Read

			Dim equipmentData As New EquipmentDataModel
			equipmentData.id = dbReader.Item("eqid")
			equipmentData.name = dbReader.Item("eqnum")
			equipmentData.EquipmentDesc = dbReader.Item("eqdesc")
			equipmentData.ChildCount = dbReader.Item("ChildCount")

			ReDim Preserve equipmentArray(index)
			equipmentArray(index) = equipmentData
			index = index + 1
		End While
		dbReader.Close()

		If Not equipmentArray Is Nothing Then

			For Each master As EquipmentDataModel In equipmentArray

				If (master.ChildCount > 0) Then

					Dim childArray() As EquipmentLeafDataModel
					childArray = Nothing

					dbReader = dbUtil.GetRdrData("exec usp_getEquipmentChildRecords  " + master.id().ToString())
					index = 0
					While dbReader.Read

						Dim childData As New EquipmentLeafDataModel
						childData.id = dbReader.Item("eqid")
						childData.name = dbReader.Item("eqnum")
						childData.EquipmentDesc = dbReader.Item("eqdesc")
						childData.ChildTotal = dbReader.Item("ChildTotal")

						ReDim Preserve childArray(index)
						childArray(index) = childData
						index = index + 1
					End While
					dbReader.Close()

					If Not childArray Is Nothing Then
						master.children = childArray
					End If
				End If
			Next
			dbUtil.Dispose()

		End If

		Return equipmentArray

	End Function

	<System.Web.Services.WebMethod()>
	Public Shared Function GetNotStartedRecords(siteId As Integer, assetClassId As Integer, equipmentFilter As String) As EquipmentLeafDataModel()
		Dim dbUtil As New Utilities
		dbUtil.Open()

		Dim dbReader = dbUtil.GetRdrData("exec usp_getEquipmentNotStarted  " + siteId.ToString() + "," + assetClassId.ToString() + ",'" +
										 equipmentFilter.ToString() + "'")
		Dim index As Integer = 0
		Dim equipmentArray() As EquipmentLeafDataModel

		While dbReader.Read

			Dim equipmentData As New EquipmentLeafDataModel
			equipmentData.id = dbReader.Item("eqid")
			equipmentData.name = dbReader.Item("eqnum").ToString()
			equipmentData.EquipmentDesc = dbReader.Item("eqdesc").ToString()
			equipmentData.ChildTotal = dbReader.Item("childtot")

			ReDim Preserve equipmentArray(index)
			equipmentArray(index) = equipmentData
			index = index + 1
		End While

		dbReader.Close()
		dbUtil.Dispose()

		Return equipmentArray
	End Function

	Public Shared Function GetMasterRecordHierarchyDownload(siteId As Integer, assetClassId As Integer, equipFilter As String) As ExcelExportDataModel()

		Dim dbUtil As New Utilities
		dbUtil.Open()

		Dim dbReader = dbUtil.GetRdrData("exec usp_getEquipmentMasterRecords  " + siteId.ToString() + "," + assetClassId.ToString() + ",'" + equipFilter + "'")
		Dim index As Integer = 0
		Dim masterArray() As EquipmentDataModel

		While dbReader.Read

			Dim equipmentData As New EquipmentDataModel
			equipmentData.id = dbReader.Item("eqid")
			equipmentData.name = dbReader.Item("eqnum")
			equipmentData.EquipmentDesc = dbReader.Item("eqdesc")
			equipmentData.OptimizedHours = dbReader.Item("OptimizedHours")
			equipmentData.ChildCount = dbReader.Item("ChildCount")

			ReDim Preserve masterArray(index)
			masterArray(index) = equipmentData
			index = index + 1
		End While
		dbReader.Close()

		Dim returnIndex As Integer = 0
		Dim returnArray() As ExcelExportDataModel

		If Not masterArray Is Nothing Then

			For Each master As EquipmentDataModel In masterArray

				Dim returnData As New ExcelExportDataModel
				returnData.id = master.id
				returnData.name = master.name
				returnData.EquipmentDesc = master.EquipmentDesc
				returnData.OptimizedHours = master.OptimizedHours

				ReDim Preserve returnArray(returnIndex)
				returnArray(returnIndex) = returnData
				returnIndex = returnIndex + 1

				If (master.ChildCount > 0) Then

					dbReader = dbUtil.GetRdrData("exec usp_getEquipmentChildRecords  " + master.id().ToString())
					While dbReader.Read

						Dim childData As New ExcelExportDataModel
						childData.id = dbReader.Item("eqid")
						childData.name = "***" & dbReader.Item("eqnum")
						childData.EquipmentDesc = dbReader.Item("eqdesc")
						childData.ChildTotal = dbReader.Item("ChildTotal")

						ReDim Preserve returnArray(returnIndex)
						returnArray(returnIndex) = childData
						returnIndex = returnIndex + 1
					End While
					dbReader.Close()
				End If
			Next
			dbUtil.Dispose()

		End If

		Return returnArray
	End Function

End Class