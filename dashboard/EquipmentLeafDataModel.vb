﻿Public Class EquipmentLeafDataModel

    Private _id As Integer
    Private _equipmentName As String
    Private _equipmentDesc As String
    Private _childtotal As Integer

    Public Property id As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property

    Public Property name As String
        Get
            Return _equipmentName
        End Get
        Set(value As String)
            _equipmentName = value
        End Set
    End Property

    Public Property EquipmentDesc As String
        Get
            Return _equipmentDesc
        End Get
        Set(value As String)
            _equipmentDesc = value
        End Set
    End Property

    Public Property ChildTotal As Integer
        Get
            Return _childtotal
        End Get
        Set(value As Integer)
            _childtotal = value
        End Set
    End Property
End Class
