﻿Public Class PMOTimeSavings
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            dashboardUtil.GetSites(ddTimeSavedSites, True)
            dashboardUtil.GetAssetClasses(ddPMOTimeSavingAssetClasses)
        End If
    End Sub
End Class