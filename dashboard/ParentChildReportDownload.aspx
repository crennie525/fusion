﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ParentChildReportDownload.aspx.vb" Inherits="lucy_r12.ParentChildReportDownload" %>

<form id="form1" runat="server">
    <div>
        <asp:GridView ID="gvParentChild" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvParentChild_OnRowDataBound">
            <Columns>
                <asp:BoundField DataField="name" HeaderText="Equipment Name" />
                <asp:BoundField DataField="EquipmentDesc" HeaderText="Equipment Desc" />
                <asp:BoundField DataField="ChildTotal" HeaderText="Child Total" />
                <asp:BoundField DataField="OptimizedHours" HeaderText="Optimized Hours" />
            </Columns>
        </asp:GridView>
    </div>
</form>


    



