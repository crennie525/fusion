﻿Public Class ParentChildReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Not HttpContext.Current.Session("Logged_In") Is Nothing) Then

            If Not Me.IsPostBack Then
                dashboardUtil.GetSites(ddParentChildSites, False)
                dashboardUtil.GetAssetClasses(ddParentChildAssetClasses)
                dashboardUtil.GetEquipmentFilter(ddParentChildEquipmentFilter)
            End If

        End If
    End Sub
End Class