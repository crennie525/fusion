﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WODashboard.aspx.vb" Inherits="lucy_r12.WODashboard" %>
<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WO Dashboard</title>
</head>
<body>
    
	<form id="formWoDashboard" runat="server">
		<div style="margin-top:120px;margin-right:40px;">
			<table style="margin:0 auto;">
				<tr>
					<td>
						<b>Display</b>
					</td>
					<td>
						<asp:DropDownList ID="ddDisplay" runat="server">
							<asp:ListItem Text="Type" Value="Type"></asp:ListItem>
							<asp:ListItem Text="Status" Value="Status"></asp:ListItem>
							<asp:ListItem Text="Supervisor" Value="Supervisor"></asp:ListItem>
						</asp:DropDownList>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td>
						<b>Status</b>
					</td>	
					<td>
						<asp:DropDownList ID="ddStatus" runat="server"></asp:DropDownList>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td>
						<b>Type</b>
					</td>
					<td>
						<asp:DropDownList ID="ddType" runat="server"></asp:DropDownList>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td>
						<b>Equipment</b>
					</td>	
					<td>
						<asp:DropDownList ID="ddWOEquipmentFilter" runat="server"></asp:DropDownList>
					</td>
				</tr>
				<tr><td style="line-height:5px;">&nbsp;</td></tr>
				<tr>
					<td>
						<b>Supervisor</b>
					</td>
					<td>
						<asp:DropDownList ID="ddSupervisor" runat="server"></asp:DropDownList>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td>
						<b>Start Date</b>
					</td>
					<td>
						<input id="startdate" type="text" class="easyui-datebox" />
					</td>
					<td>&nbsp;&nbsp;</td>
					<td>
						<b>End Date</b>
					</td>
					<td>
						<input id="enddate" type="text" class="easyui-datebox" />
					</td>
				</tr>
			</table>
			<br /><br />
			<table style="margin:0 auto;">
				<tr>
					<td id="chart1">
					</td>
					<td id="chart2">
					</td>
					<td id="chart3">
					</td>
				</tr>
			</table> 
		</div>
    
		<asp:HiddenField id="hSiteId" runat="server" />
		<uc1:mmenu1 runat="server"></uc1:mmenu1>
	</form>
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="../jqplot/jquery.jqplot.min.js"></script>
	<script type="text/javascript" src="../jqplot/jqplot.pieRenderer.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../jqplot/jquery.jqplot.min.css" />
	<script type="text/javascript" src="../jqplot/dashboard.js"></script>
	<link rel="stylesheet" type="text/css" href="../dashboard/dashboard.css" />
	<script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />
    <script type="text/javascript">
    	$(document).ready(function () {
    		GetWorkOrderTypeCounts('Type', 'APPR', 0, '0', '0', '0', '0');

    		$('#startdate').datebox({
    			onSelect: function (date) {
    				var startdate = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
    				GetWorkOrderTypeCounts($("#ddDisplay").val(), $("#ddStatus").val(), $("#ddWOEquipmentFilter").val(), $("#ddSupervisor").val(), $("#ddType").val(), startdate, $("#enddate").val());
    			}
    		});

    		$('#enddate').datebox({
    			onSelect: function (date) {
    				var enddate = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
    				GetWorkOrderTypeCounts($("#ddDisplay").val(), $("#ddStatus").val(), $("#ddWOEquipmentFilter").val(), $("#ddSupervisor").val(), $("#ddType").val(), $("#startdate").val(), enddate);
    			}
    		});
    	});
	</script>
</body>
</html>
