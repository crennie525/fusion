<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMOptRationale2.aspx.vb"
    Inherits="lucy_r12.PMOptRationale2" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>PMOptRationale2</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script  type="text/javascript" src="../scripts1/PMOptRationale2aspx.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="handleexit();">
    <form id="form1" method="post" runat="server">
    <div style="left: 8px; position: absolute; top: 8px">
        <table width="800" cellspacing="1" cellpadding="1">
            <tr>
                <td class="bluelabel" style="width: 80px">
                </td>
                <td width="230" runat="server">
                </td>
                <td class="bluelabel" style="width: 80px">
                </td>
                <td class="label" width="240">
                </td>
                <td class="label" width="200">
                </td>
            </tr>
            <tr height="20">
                <td class="bluelabel">
                    <asp:Label ID="lang993" runat="server">Department</asp:Label>
                </td>
                <td class="label" id="tddept" runat="server">
                </td>
                <td class="bluelabel">
                    <asp:Label ID="lang994" runat="server">Station/Cell</asp:Label>
                </td>
                <td class="label" id="tdcell" runat="server">
                </td>
            </tr>
            <tr height="20">
                <td class="bluelabel">
                    <asp:Label ID="lang995" runat="server">Equipment</asp:Label>
                </td>
                <td class="label" id="tdeq" runat="server">
                </td>
                <td class="bluelabel">
                    <asp:Label ID="lang996" runat="server">Function</asp:Label>
                </td>
                <td class="label" id="tdfunc" runat="server">
                </td>
            </tr>
            <tr height="20">
                <td class="bluelabel">
                    <asp:Label ID="lang997" runat="server">Task#</asp:Label>
                </td>
                <td class="label" id="tdtasknum" runat="server">
                </td>
                <td class="bluelabel">
                    <asp:Label ID="lang998" runat="server">Component</asp:Label>
                </td>
                <td class="label" id="tdcomp" runat="server">
                </td>
                <td class="label" align="right">
                    <asp:ImageButton ID="ibsvrtn" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif">
                    </asp:ImageButton>
                    <img src="../images/appbuttons/minibuttons/printx.gif" onclick="getreport();" onmouseover="return overlib('Print PMO Task Rationale Report', ABOVE, LEFT)"
                        onmouseout="return nd()">
                </td>
            </tr>
        </table>
        <table cellspacing="1" cellpadding="1" width="800" class="tbg">
            <tr>
                <td class="thdrsing label" style="width: 120px">
                    <asp:Label ID="lang999" runat="server">Task Detail</asp:Label>
                </td>
                <td class="thdrsing label" width="220">
                    <asp:Label ID="lang1000" runat="server">Original</asp:Label>
                </td>
                <td class="thdrsing label" width="220">
                    <asp:Label ID="lang1001" runat="server">Revised</asp:Label>
                </td>
                <td class="thdrsing label" width="240">
                    <asp:Label ID="lang1002" runat="server">Rational</asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label cellbrdrend">
                    <asp:Label ID="lang1003" runat="server">Failure Modes</asp:Label>
                </td>
                <td class="labellt cellbrdrcntr">
                    <asp:ListBox ID="lbofm" runat="server" CssClass="plainlabel" Width="210px" Height="52px">
                    </asp:ListBox>
                </td>
                <td class="labellt cellbrdrcntr">
                    <asp:ListBox ID="lbfm" runat="server" CssClass="plainlabel" Width="210px" Height="52px">
                    </asp:ListBox>
                </td>
                <td class="labellt cellbrdrcntr" valign="top">
                    <textarea class="plainlabel" id="txtfm" style="width: 230px; height: 40px" name="txtfm"
                        rows="3" cols="20" runat="server"></textarea>
                </td>
            </tr>
            <tr>
                <td class="label cellbrdrend">
                    <asp:Label ID="lang1004" runat="server">Task Description</asp:Label>
                </td>
                <td class="labellt cellbrdrcntr" id="tdotd" runat="server">
                    <textarea class="plainlabel" id="txtodesc" style="width: 210px; height: 40px" name="Textarea1"
                        rows="3" cols="20" runat="server">						</textarea>
                </td>
                <td class="labellt cellbrdrcntr" id="tdtd" runat="server">
                    <textarea class="plainlabel" id="txtdesc" style="width: 210px; height: 40px" name="Textarea1"
                        rows="3" cols="20" runat="server">						</textarea>
                </td>
                <td class="labellt cellbrdrcntr">
                    <textarea class="plainlabel" id="txttd" style="width: 230px; height: 40px" name="txttd"
                        rows="3" cols="20" runat="server">						</textarea>
                </td>
            </tr>
            <tr>
                <td class="label cellbrdrend">
                    <asp:Label ID="lang1005" runat="server">Task Type</asp:Label>
                </td>
                <td class="labellt cellbrdrcntr plainlabel" id="tdott" runat="server">
                    N/A
                </td>
                <td class="labellt cellbrdrcntr plainlabel" id="tdtt" runat="server">
                    N/A
                </td>
                <td class="labellt cellbrdrcntr">
                    <textarea class="plainlabel" id="txttt" style="width: 230px; height: 40px" name="txttt"
                        rows="3" cols="20" runat="server">						</textarea>
                </td>
            </tr>
            <tr>
                <td class="label cellbrdrend">
                    <asp:Label ID="lang1006" runat="server">PdM Tech</asp:Label>
                </td>
                <td class="labellt cellbrdrcntr plainlabel" id="tdopdm" runat="server">
                    N/A
                </td>
                <td class="labellt cellbrdrcntr plainlabel" id="tdpdm" runat="server">
                    N/A
                </td>
                <td class="labellt cellbrdrcntr">
                    <textarea class="plainlabel" id="txtpdm" style="width: 230px; height: 40px" name="txtpdm"
                        rows="3" cols="20" runat="server">						</textarea>
                </td>
            </tr>
            <tr>
                <td class="label cellbrdrend">
                    <asp:Label ID="lang1007" runat="server">Skill Required</asp:Label>
                </td>
                <td class="labellt cellbrdrcntr plainlabel" id="tdosr" runat="server">
                    N/A
                </td>
                <td class="labellt cellbrdrcntr plainlabel" id="tdsr" runat="server">
                    N/A
                </td>
                <td class="labellt cellbrdrcntr">
                    <textarea class="plainlabel" id="txtsr" style="width: 230px; height: 40px" name="txtsr"
                        rows="3" cols="20" runat="server">						</textarea>
                </td>
            </tr>
            <tr>
                <td class="label cellbrdrend">
                    <asp:Label ID="lang1008" runat="server">Labor Minutes Each</asp:Label>
                </td>
                <td class="labellt cellbrdrcntr plainlabel" id="tdolm" runat="server">
                    N/A
                </td>
                <td class="labellt cellbrdrcntr plainlabel" id="tdlm" runat="server">
                    N/A
                </td>
                <td class="labellt cellbrdrcntr">
                    <textarea class="plainlabel" id="txtlm" style="width: 230px; height: 40px" name="txtlm"
                        rows="3" cols="20" runat="server">						</textarea>
                </td>
            </tr>
            <tr>
                <td class="label cellbrdrend">
                    <asp:Label ID="lang1009" runat="server">Frequency</asp:Label>
                </td>
                <td class="labellt cellbrdrcntr plainlabel" id="tdofreq" runat="server">
                    N/A
                </td>
                <td class="labellt cellbrdrcntr plainlabel" id="tdfreq" runat="server">
                    N/A
                </td>
                <td class="labellt cellbrdrcntr">
                    <textarea class="plainlabel" id="txtfr" style="width: 230px; height: 40px" name="txtfr"
                        rows="3" cols="20" runat="server">						</textarea>
                </td>
            </tr>
            <tr>
                <td class="label cellbrdrend">
                    <asp:Label ID="lang1010" runat="server">EQ Status</asp:Label>
                </td>
                <td class="labellt cellbrdrcntr plainlabel" id="tdoeqs" runat="server">
                    N/A
                </td>
                <td class="labellt cellbrdrcntr plainlabel" id="tdeqs" runat="server">
                    N/A
                </td>
                <td class="labellt cellbrdrcntr">
                    <textarea class="plainlabel" id="txteqs" style="width: 230px; height: 40px" name="txteqs"
                        rows="3" cols="20" runat="server">						</textarea>
                </td>
            </tr>
            <tr>
                <td class="label cellbrdrend">
                    <asp:Label ID="lang1011" runat="server">Down Time</asp:Label>
                </td>
                <td class="labellt cellbrdrcntr plainlabel" id="tdodt" runat="server">
                    N/A
                </td>
                <td class="labellt cellbrdrcntr plainlabel" id="tddt" runat="server">
                    N/A
                </td>
                <td class="labellt cellbrdrcntr">
                    <textarea class="plainlabel" id="txtdt" style="width: 230px; height: 40px" name="txtdt"
                        rows="3" cols="20" runat="server">						</textarea>
                </td>
            </tr>
            <tr>
                <td class="label cellbrdrend">
                    <asp:Label ID="lang1012" runat="server">Parts</asp:Label>
                </td>
                <td class="labellt cellbrdrcntr">
                    <asp:ListBox ID="lboparts" runat="server" CssClass="plainlabel" Width="210px" Height="52px">
                    </asp:ListBox>
                </td>
                <td class="labellt cellbrdrcntr">
                    <asp:ListBox ID="lbparts" runat="server" CssClass="plainlabel" Width="210px" Height="52px">
                    </asp:ListBox>
                </td>
                <td class="labellt cellbrdrcntr">
                    <textarea class="plainlabel" id="txtpart" style="width: 230px; height: 40px" name="txtpart"
                        rows="3" cols="20" runat="server">						</textarea>
                </td>
            </tr>
            <tr>
                <td class="label cellbrdrend">
                    <asp:Label ID="lang1013" runat="server">Tools</asp:Label>
                </td>
                <td class="labellt cellbrdrcntr">
                    <asp:ListBox ID="lbotools" runat="server" CssClass="plainlabel" Width="210px" Height="52px">
                    </asp:ListBox>
                </td>
                <td class="labellt cellbrdrcntr">
                    <asp:ListBox ID="lbtools" runat="server" CssClass="plainlabel" Width="210px" Height="52px">
                    </asp:ListBox>
                </td>
                <td class="labellt cellbrdrcntr">
                    <textarea class="plainlabel" id="txttool" style="width: 230px; height: 40px" name="txttool"
                        rows="3" cols="20" runat="server">						</textarea>
                </td>
            </tr>
            <tr>
                <td class="label cellbrdrend">
                    <asp:Label ID="lang1014" runat="server">Lubricants</asp:Label>
                </td>
                <td class="labellt cellbrdrcntr">
                    <asp:ListBox ID="lbolubes" runat="server" CssClass="plainlabel" Width="210px" Height="52px">
                    </asp:ListBox>
                </td>
                <td class="labellt cellbrdrcntr">
                    <asp:ListBox ID="lblubes" runat="server" CssClass="plainlabel" Width="210px" Height="52px">
                    </asp:ListBox>
                </td>
                <td class="labellt cellbrdrcntr">
                    <textarea class="plainlabel" id="txtlube" style="width: 230px; height: 40px" name="txtlube"
                        rows="3" cols="20" runat="server">						</textarea>
                </td>
            </tr>
        </table>
    </div>
    <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
    <input id="lbldid" type="hidden" name="lbldid" runat="server">
    <input id="lblclid" type="hidden" name="lblclid" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="txtfin" type="hidden" name="txtfin" runat="server"><input id="lbllog"
        type="hidden" name="lbllog" runat="server">
    <input id="lbllock" type="hidden" name="lbllock" runat="server">
    <input id="lbllockedby" type="hidden" name="lbllockedby" runat="server">
    <input id="lblusername" type="hidden" name="lblusername" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
