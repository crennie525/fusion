

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class bulkproc
    Inherits System.Web.UI.Page
	Protected WithEvents lang954 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, srch, eqnum As String
    Dim dr As SqlDataReader
    Dim bulk As New Utilities

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents trsrch As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tr1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents supdiv As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblpost As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsupid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            eqnum = Request.QueryString("eq").ToString
            bulk.Open()
            PopBulk(eqnum)
            bulk.Dispose()
        End If
    End Sub
    Private Sub PopBulk(Optional ByVal eqnum As String = "null")
        Dim sb As New System.Text.StringBuilder
        srch = txtsrch.Text
        srch = bulk.ModString2(srch)
        Dim old, tlt, eq, typ As String
        If eqnum <> "null" Then
            sql = "select * from client_procs where eqnum = '" & eqnum & "'"
            trsrch.Attributes.Add("class", "details")
        Else
            trsrch.Attributes.Add("class", "view")
            If srch = "" Then
                sql = "select * from client_procs"
            Else
                sql = "select * from client_procs where pmtitle like '%" & srch & "%' or eqnum like '%" & srch & "%'"
            End If
        End If

        sb.Append("<table>")
        dr = bulk.GetRdrData(sql)
        While dr.Read
            old = dr.Item("oldid").ToString
            tlt = dr.Item("pmtitle").ToString
            eq = dr.Item("eqnum").ToString
            typ = dr.Item("proctype").ToString
            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getbulk('" & old & "', '" & typ & "');"">" & eq & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & tlt & "</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        supdiv.InnerHtml = sb.ToString


    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        bulk.Open()
        PopBulk()
        bulk.Dispose()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang954.Text = axlabs.GetASPXPage("bulkproc.aspx", "lang954")
        Catch ex As Exception
        End Try

    End Sub

End Class
