<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmgetopt.aspx.vb" Inherits="lucy_r12.pmgetopt" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>pmgetopt</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script  type="text/javascript" src="../scripts/overlib2.js"></script>
    <script  type="text/javascript" src="../scripts/pmtaskdivfunc_1016f.js"></script>
    <script  type="text/javascript" src="../scripts/taskgrid.js"></script>
    <script  type="text/javascript" src="../scripts1/PMOptMainaspx_9.js"></script>
    <link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />
    <script  type="text/javascript">
    <!--
        function getpms() {
            window.parent.setref();
            var chk = document.getElementById("lblusetot").value;
            var eqid = document.getElementById("lbleqid").value;
            var sid = document.getElementById("lblsid").value;
            var eqnum = document.getElementById("lbleqnum").value;
            if (chk == "1" && eqid != "") {
                window.parent.setref();
                var eReturn = window.showModalDialog("../appsdrag/totpmsdialog.aspx?typ=1&eqid=" + eqid + "&sid=" + sid + "&eqnum=" + eqnum, "", "dialogHeight:610px; dialogWidth:860px; resizable=yes");
                if (eReturn) {
                    document.getElementById("lblgototasks").value = "1";
                    checkpg();
                    //checkpm_noalert();
                }
            }
        }
        function GetProcDiv() {

            handleapp();
            eqid = document.getElementById("lbleqid").value;
            //var eqlst = document.getElementById("ddeq");
            //var eqnum = eqlst.options[eqlst.selectedIndex].text
            var eqnum = document.getElementById("lbleq").value;
            if (eqid != "") {
                window.parent.setref();
                var eReturn = window.showModalDialog("../appsopt/OptDialog.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:580px; resizable=yes");
            }
            if (eReturn) {
                var sid = document.getElementById("lblsid").value;
                var did = document.getElementById("lbldid").value;
                var dept = document.getElementById("lbldept").value;
                var clid = document.getElementById("lblclid").value;
                var cell = document.getElementById("lblcell").value;
                var eqid = document.getElementById("lbleqid").value;
                var eq = document.getElementById("lbleq").value;
                var fuid = document.getElementById("lblfuid").value;
                var fu = document.getElementById("lblfu").value;
                var coid = document.getElementById("lblcoid").value;
                var comp = document.getElementById("lblcomp").value;
                var lid = document.getElementById("lbllid").value;
                var loc = document.getElementById("lblloc").value;
                var task = document.getElementById("lbltask").value;
                var who = document.getElementById("lblwho").value;
                var typ;
                if (did == "" && lid != "") {
                    typ = "loc";
                }
                if (did != "") {
                    typ = "reg";
                }
                var rproc = "0";
                window.location = "pmgetopt.aspx?wo=" + who + "&jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task + "&rproc=" + rproc;
            }
        }
        function jumptpm(typ) {

            funid = document.getElementById("lblfuid").value;
            eqid = document.getElementById("lbleqid").value;

            if (eqid == "") {
                alert("No Equipment Record Selected")
            }
            else if (funid == "") {
                //typ = "eq"
                window.parent.tpmjump(eqid, funid, typ);
            }
            else {
                //typ = "fu"
                window.parent.tpmjump(eqid, funid, typ);
            }

        }
        function jumptpm(typ) {

            funid = document.getElementById("lblfuid").value;
            eqid = document.getElementById("lbleqid").value;

            if (eqid == "") {
                alert("No Equipment Record Selected")
            }
            else if (funid == "") {
                //typ = "eq"
                window.parent.tpmjump(eqid, funid, typ);
            }
            else {
                //typ = "fu"
                window.parent.tpmjump(eqid, funid, typ);
            }

        }
        function retfunc() {
            //alert()
            var eqid = document.getElementById("lbleqid").value;
            if (eqid != "") {
                var eReturn = window.showModalDialog("../equip/retfuncdialog.aspx?typ=fu&eqid=" + eqid, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    var sid = document.getElementById("lblsid").value;
                    var wo = "";
                    var typ = "loc";
                    var did = document.getElementById("lbldid").value;
                    var dept = document.getElementById("lbldept").value;
                    var clid = document.getElementById("lblclid").value;
                    var cell = document.getElementById("lblcell").value;
                    var eqid = document.getElementById("lbleqid").value;
                    var eq = document.getElementById("lbleq").value;
                    var fret = eReturn.split(",")
                    var fuid = fret[0]; // eReturn;
                    var coid = document.getElementById("lblcoid").value;
                    var comp = document.getElementById("lblcomp").value;
                    var lid = document.getElementById("lbllid").value;
                    var loc = document.getElementById("lblloc").value;
                    var task = "";
                    try {
                        window.parent.handlearch(did, clid, lid, sid, typ, eqid, fuid, coid, eqnum, dept, cell, loc)
                    }
                    catch (err) {

                    }

                    window.location = "pmgetopt.aspx?jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;
                }
            }
        }
        function retminsrch(who) {
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "ret";
            var did = document.getElementById("lbldid").value;
            var dept = document.getElementById("lbldept").value;
            dept = dept.replace("#", "%23")
            var clid = document.getElementById("lblclid").value;
            var cell = document.getElementById("lblcell").value;
            cell = cell.replace("#", "%23")
            var eqid = document.getElementById("lbleqid").value;
            var eq = document.getElementById("lbleq").value;
            eq = eq.replace("#", "%23")
            var fuid = document.getElementById("lblfuid").value;
            var fu = document.getElementById("lblfu").value;
            var coid = document.getElementById("lblcoid").value;
            var comp = document.getElementById("lblcomp").value;
            var lid = document.getElementById("lbllid").value;
            var loc = document.getElementById("lblloc").value;

            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who, retminsrchCallBack, "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                retminsrchCallBack(eReturn);
            }
        }

        function retminsrchCallBack(eReturn) {
            if (eReturn != "ok") {
                clearall();
                var ret = eReturn.split("~");
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("lbldept").value = ret[1];
                document.getElementById("tddept").innerHTML = ret[1];
                document.getElementById("lblclid").value = ret[2];
                document.getElementById("lblcell").value = ret[3];
                document.getElementById("tdcell").innerHTML = ret[3];
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("lbleq").value = ret[5];
                document.getElementById("tdeq").innerHTML = ret[5];
                document.getElementById("lblfuid").value = ret[6];
                document.getElementById("lblfu").value = ret[7];
                document.getElementById("tdfu").innerHTML = ret[7];
                document.getElementById("lblcoid").value = ret[8];
                document.getElementById("lblcomp").value = ret[9];
                document.getElementById("lbllid").value = ret[12];
                document.getElementById("lblloc").value = ret[13];
                document.getElementById("trdepts").className = "view";
                document.getElementById("lblrettyp").value = "depts";
                var who = document.getElementById("lblwho").value;
                var did = ret[0];
                var eqid = ret[4];
                var clid = ret[2];
                var fuid = ret[6];
                var coid = ret[8];
                var lid = ret[12];

                var eqnum = ret[5];
                var dept = ret[1];
                var cell = ret[3];
                var loc = ret[13];

                var typ;
                typ = "reg";

                try {
                    window.parent.handlearch(did, clid, lid, sid, typ, eqid, fuid, coid, eqnum, dept, cell, loc)
                } catch (err) {

                }
                var task = "";
                window.location = "../appsopt/pmgetopt.aspx?who=" + who + "&jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;
            }
        }

        function getminsrch() {
            var did = document.getElementById("lbldid").value;
            //alert(did)
            if (did != "") {
                retminsrch();
            }
            else {
                var sid = document.getElementById("lblsid").value;
                var wo = "";
                var typ;
                if (wo == "") {
                    typ = "lu";
                }
                else {
                    typ = "wo";
                }

                var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, getminsrchCallBack, "dialogHeight:600px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    getminsrchCallBack(eReturn);
                }
            }
        }

        function getminsrchCallBack(eReturn) {

            if (eReturn != "ok") {
                clearall();
                var ret = eReturn.split("~");
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("lbldept").value = ret[1];
                document.getElementById("tddept").innerHTML = ret[1];
                document.getElementById("lblclid").value = ret[2];
                document.getElementById("lblcell").value = ret[3];
                document.getElementById("tdcell").innerHTML = ret[3];
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("lbleq").value = ret[5];
                document.getElementById("tdeq").innerHTML = ret[5];
                document.getElementById("lblfuid").value = ret[6];
                document.getElementById("lblfu").value = ret[7];
                document.getElementById("tdfu").innerHTML = ret[7];
                document.getElementById("lblcoid").value = ret[8];
                document.getElementById("lblcomp").value = ret[9];
                document.getElementById("lbllid").value = ret[12];
                document.getElementById("lblloc").value = ret[13];
                document.getElementById("trdepts").className = "view";
                document.getElementById("lblrettyp").value = "depts";
                var who = document.getElementById("lblwho").value;
                var did = ret[0];
                var eqid = ret[4];
                var clid = ret[2];
                var fuid = ret[6];
                var coid = ret[8];
                var lid = ret[12];
                var eqnum = ret[5];
                var dept = ret[1];
                var cell = ret[3];
                var loc = ret[13];
                var typ;
                if (lid == "") {
                    typ = "reg";
                } else {
                    typ = "dloc";
                }
                var task = "";
                try {
                    window.parent.handlearch(did, clid, lid, sid, typ, eqid, fuid, coid, eqnum, dept, cell, loc)
                } catch (err) {

                }
                window.location = "../appsopt/pmgetopt.aspx?who=" + who + "&jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;
            }
        }

        function getlocs1() {
            var sid = document.getElementById("lblsid").value;
            var lid = document.getElementById("lbllid").value;
            var typ = "lu"
            //alert(lid)
            if (lid != "") {
                typ = "retloc"
            }
            var eqid = document.getElementById("lbleqid").value;
            var fuid = document.getElementById("lblfuid").value;
            var coid = document.getElementById("lblcoid").value;
            //alert(fuid)
            var wo = "";
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("lblloc").value = ret[1];
                document.getElementById("tdloc3").innerHTML = ret[2];

                document.getElementById("lbleq").value = ret[4];
                document.getElementById("tdeq3").innerHTML = ret[4];
                document.getElementById("lbleqid").value = ret[3];

                document.getElementById("lblfuid").value = ret[5];
                document.getElementById("lblfu").value = ret[6];
                document.getElementById("tdfu3").innerHTML = ret[6];

                document.getElementById("lblcoid").value = ret[7];
                document.getElementById("lblcomp").value = ret[8];

                document.getElementById("lbllevel").value = ret[9];

                document.getElementById("trlocs").className = "view";
                document.getElementById("lblrettyp").value = "locs";
                var who = document.getElementById("lblwho").value;
                var did = "";
                var eqid = ret[3];
                var clid = "";
                var fuid = ret[5];
                var coid = ret[7];
                var lid = ret[0];

                var eqnum = ret[4];
                var dept = "";
                var cell = "";
                var loc = ret[1];

                var typ;
                typ = "loc";
                var task = "";
                //alert(coid)
                try {
                    window.parent.handlearch(did, clid, lid, sid, typ, eqid, fuid, coid, eqnum, dept, cell, loc)
                }
                catch (err) {

                }
                window.location = "pmgetopt.aspx?who=" + who + "&jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;

            }
        }
        function clearall() {
            document.getElementById("lbldid").value = "";
            document.getElementById("lbldept").value = "";
            document.getElementById("tddept").innerHTML = "";
            document.getElementById("lblclid").value = "";
            document.getElementById("lblcell").value = "";
            document.getElementById("tdcell").innerHTML = "";
            document.getElementById("lbleqid").value = "";
            document.getElementById("lbleq").value = "";
            document.getElementById("tdeq").innerHTML = "";
            document.getElementById("lblfuid").value = "";
            document.getElementById("lblfu").value = "";
            document.getElementById("tdfu").innerHTML = "";
            document.getElementById("lblcoid").value = "";
            document.getElementById("lbllid").value = "";
            document.getElementById("lblloc").value = "";
            document.getElementById("tdloc3").innerHTML = "";
            document.getElementById("tdeq3").innerHTML = "";
            document.getElementById("tdfu3").innerHTML = "";
            document.getElementById("trdepts").className = "details";
            document.getElementById("trlocs").className = "details";
            document.getElementById("lblrettyp").value = "";

            document.getElementById("lbllevel").value = "";
        }
        function GetFuncDiv2() {
            window.parent.setref();
            cid = document.getElementById("lblcid").value;
            eqid = document.getElementById("lbleqid").value;
            ro = document.getElementById("lblro").value;
            var eReturn = window.showModalDialog("../equip/AddEditFuncDialog.aspx?cid=" + cid + "&eqid=" + eqid + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:600px; dialogWidth:750px; resizable=yes");
            if (eReturn) {
                //alert(eReturn)
                var fuid = document.getElementById("lblfuid").value = eReturn;
                var sid = document.getElementById("lblsid").value;
                var did = document.getElementById("lbldid").value;
                var dept = document.getElementById("lbldept").value;
                var clid = document.getElementById("lblclid").value;
                var cell = document.getElementById("lblcell").value;
                var eqid = document.getElementById("lbleqid").value;
                var eq = document.getElementById("lbleq").value;
                //var fuid = document.getElementById("lblfuid").value;
                var fu = document.getElementById("lblfu").value;
                var coid = document.getElementById("lblcoid").value;
                var comp = document.getElementById("lblcomp").value;
                var lid = document.getElementById("lbllid").value;
                var loc = document.getElementById("lblloc").value;
                var who = document.getElementById("lblwho").value;
                var typ;
                if (lid == "") {
                    typ = "reg";
                }
                else if (lid != "" && did != "") {
                    typ = "dloc";
                }
                else {
                    typ = "loc"
                }
                var task = "";
                window.location = "pmgetopt.aspx?who=" + who + "&jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;
            }
        }
        function GetFuncCopy2() {

            handleapp();
            cid = document.getElementById("lblcid").value;
            sid = document.getElementById("lblsid").value;
            did = document.getElementById("lbldid").value;
            clid = document.getElementById("lblclid").value;
            eqid = document.getElementById("lbleqid").value;
            ro = document.getElementById("lblro").value;
            if (eqid.length != 0 || eqid != "" || eqid != "0") {
                window.parent.setref();
                var eReturn = window.showModalDialog("../equip/FuncCopyMiniDialog.aspx?cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:660px; dialogWidth:860px; resizable=yes");
                if (eReturn) {
                    if (eReturn == "log") {
                        window.parent.handlelogout();
                    }
                    else if (eReturn != "no") {
                        document.getElementById("lblfuid").value = eReturn;
                        var fuid = document.getElementById("lblfuid").value = eReturn;
                        var sid = document.getElementById("lblsid").value;
                        var did = document.getElementById("lbldid").value;
                        var dept = document.getElementById("lbldept").value;
                        var clid = document.getElementById("lblclid").value;
                        var cell = document.getElementById("lblcell").value;
                        var eqid = document.getElementById("lbleqid").value;
                        var eq = document.getElementById("lbleq").value;
                        //var fuid = document.getElementById("lblfuid").value;
                        var fu = document.getElementById("lblfu").value;
                        var coid = document.getElementById("lblcoid").value;
                        var comp = document.getElementById("lblcomp").value;
                        var lid = document.getElementById("lbllid").value;
                        var loc = document.getElementById("lblloc").value;
                        var who = document.getElementById("lblwho").value;
                        var typ;
                        if (lid == "") {
                            typ = "reg";
                        }
                        else if (lid != "" && did != "") {
                            typ = "dloc";
                        }
                        else {
                            typ = "loc"
                        }
                        var task = "";
                        window.location = "pmgetopt.aspx?who=" + who + "&jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;
                    }

                }
                else {
                    alert("No Equipment Record Selected")
                }
            }
        }
        function GetEqDiv2() {

            handleapp();
            sid = document.getElementById("lblsid").value;
            dept = document.getElementById("lbldid").value;
            cell = document.getElementById("lblclid").value;
            eqid = document.getElementById("lbleqid").value;
            lid = document.getElementById("lbllid").value;
            ro = document.getElementById("lblro").value;
            if (dept.length != 0 || dept != "" || dept != "0" || lid != "") {
                window.parent.setref();
                var eReturn = window.showModalDialog("../equip/EqPopDialog.aspx?sid=" + sid + "&dept=" + dept + "&cell=" + cell + "&eqid=" + eqid + "&lid=" + lid + "&ro=" + ro, GetEqDiv2Callback, "dialogHeight:700px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    GetEqDiv2Callback(eReturn);
                }
            }
            else {
                alert("Error Retrieving Department Data")
            }
        }

        function GetEqDiv2Callback(eReturn) {
            if (eReturn == "log") {
                window.parent.handlelogout();
            }
            else if (eReturn != "no") {
                document.getElementById("lbleqid").value = eReturn;
                var sid = document.getElementById("lblsid").value;
                var did = document.getElementById("lbldid").value;
                var dept = document.getElementById("lbldept").value;
                var clid = document.getElementById("lblclid").value;
                var cell = document.getElementById("lblcell").value;
                var eqid = eReturn;
                var eq = document.getElementById("lbleq").value;
                var fuid = document.getElementById("lblfuid").value;
                var fu = document.getElementById("lblfu").value;
                var coid = document.getElementById("lblcoid").value;
                var comp = document.getElementById("lblcomp").value;
                var lid = document.getElementById("lbllid").value;
                var loc = document.getElementById("lblloc").value;
                var who = document.getElementById("lblwho").value;
                var typ;
                if (lid == "") {
                    typ = "reg";
                }
                else if (lid != "" && did != "") {
                    typ = "dloc";
                }
                else {
                    typ = "loc"
                }
                var task = "";
                window.location = "../appsopt/pmgetopt.aspx?who=" + who + "&jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;
            }
        }

        function GetEqCopy2() {

            handleapp();
            sid = document.getElementById("lblsid").value;
            dept = document.getElementById("lbldid").value;
            cell = document.getElementById("lblclid").value;
            eqid = document.getElementById("lbleqid").value;
            lid = document.getElementById("lbllid").value;
            ro = document.getElementById("lblro").value;
            //alert(clid)
            if (dept.length != 0 || dept != "" || dept != "0") {
                window.parent.setref();
                var eReturn = window.showModalDialog("../equip/EQCopyMiniDialog.aspx?sid=" + sid + "&dept=" + dept + "&cell=" + cell + "&eqid=" + eqid + "&lid=" + lid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:660px; dialogWidth:660px; resizable=yes");
                if (eReturn) {
                    if (eReturn == "log") {
                        window.parent.handlelogout();
                    }
                    else if (eReturn != "no") {
                        document.getElementById("lbleqid").value = eReturn;
                        var sid = document.getElementById("lblsid").value;
                        var did = document.getElementById("lbldid").value;
                        var dept = document.getElementById("lbldept").value;
                        var clid = document.getElementById("lblclid").value;
                        var cell = document.getElementById("lblcell").value;
                        var eqid = eReturn;
                        var eq = document.getElementById("lbleq").value;
                        var fuid = document.getElementById("lblfuid").value;
                        var fu = document.getElementById("lblfu").value;
                        var coid = document.getElementById("lblcoid").value;
                        var comp = document.getElementById("lblcomp").value;
                        var lid = document.getElementById("lbllid").value;
                        var loc = document.getElementById("lblloc").value;
                        var who = document.getElementById("lblwho").value;
                        var typ;
                        if (lid == "") {
                            typ = "reg";
                        }
                        else if (lid != "" && did != "") {
                            typ = "dloc";
                        }
                        else {
                            typ = "loc"
                        }
                        var task = "";
                        window.location = "pmgetopt.aspx?who=" + who + "&jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;
                    }

                }
            }
            else {
                alert("Error Retrieving Department Data")
            }
        }
        function checkpg() {
            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                window.parent.doref();
            }
            else {
                window.parent.setref();
            }
            var dflg = document.getElementById("lbldocflg").value;
            if (dflg != "yes") {
                var gototasks = document.getElementById("lblgototasks").value;
                //alert(gototasks)
                //alert(docflag)
                if (docflag != 1) {
                    //alert("docflag != 1 start")
                    var who = document.getElementById("lblwho").value;
                    //alert(who)
                    chk = document.getElementById("lblpar").value;
                    if (chk == "nopm") {
                        document.getElementById("lblpar").value = "";
                        AddNewPM();
                    }
                    var clean = document.getElementById("lblcleantasks").value;
                    //alert(clean)
                    if (clean == "1") {
                        document.getElementById("lblcleantasks").value = "2";
                        //alert("PMOptMain 1")
                        window.parent.handletask("PMOptTasks.aspx?who=" + who + "&start=no");
                    }
                    var arch = document.getElementById("lblgetarch").value;
                    if (arch == "yes") {
                        document.getElementById("lblgetarch").value = "no";
                        eqid = document.getElementById("lbleqid").value;
                        fuid = document.getElementById("lblfuid").value;
                        did = document.getElementById("lbldept").value;
                        clid = document.getElementById("lblclid").value;
                        chk = document.getElementById("lblchk").value;
                        lid = document.getElementById("lbllid").value;
                        sid = document.getElementById("lblsid").value;
                        window.parent.uparch(eqid, chk, did, clid, fuid, lid, sid);
                    }

                    var tc = document.getElementById("lbltaskcnt").value;
                    //alert(gototasks)
                    if (gototasks == "1") {
                        document.getElementById("lblgototasks").value = "0";
                        document.getElementById("lblcleantasks").value = "0";
                        tl = document.getElementById("lbltl").value;
                        cid = document.getElementById("lblcid").value;
                        sid = document.getElementById("lblsid").value;
                        did = document.getElementById("lbldid").value;
                        clid = document.getElementById("lblclid").value;
                        eqid = document.getElementById("lbleqid").value;
                        var pmstr = document.getElementById("lbldocpmstr").value;
                        var pmid = document.getElementById("lbldocpmid").value;
                        chk = document.getElementById("lblchk").value;
                        coid = document.getElementById("lblcoid").value;
                        fuid = document.getElementById("lblfuid").value;
                        tcnt = document.getElementById("lbltaskcnt").value;
                        typ = document.getElementById("lbltyp").value;
                        lid = document.getElementById("lbllid").value;
                        //alert(tcnt + " - ME")
                        if (tcnt == "1") {
                            window.parent.handletasks("PMOptTasksGrid.aspx?who=" + who + "&comid=" + coid + "&start=yes&tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&typ=" + typ + "&lid=" + lid, "ok")
                        }
                        else if (tcnt == "0") {
                            //alert("PMOptMain 2")
                            window.parent.handletasks("PMOptTasks.aspx?who=" + who + "&start=yes&tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&typ=" + typ + "&lid=" + lid, "ok")
                        }
                        else if (tcnt == "2") {
                            window.parent.handletasks("PMOptTasksGrid.aspx?who=" + who + "&start=no", "dept")
                        }
                        else if (tcnt == "3") {
                            window.parent.handletasks("PMOptTasksGrid.aspx?who=" + who + "&start=no", "cell")
                        }
                        else if (tcnt == "4") {
                            window.parent.handletasks("PMOptTasksGrid.aspx?who=" + who + "&start=no", "eq")
                        }
                        else if (tcnt == "5") {
                            window.parent.handletasks("PMOptTasksGrid.aspx?who=" + who + "&start=no", "fu")
                        }
                    }
                    else {
                        window.parent.handletasks("PMOptTasksGrid.aspx?who=" + who + "&start=no")
                    }


                } //for doc if
                else {
                    docflag = 0;
                    if (gototasks == "1") {
                        document.getElementById("lblgototasks").value = "0";
                        document.getElementById("lblcleantasks").value = "0";
                        tl = document.getElementById("lbltl").value;
                        cid = document.getElementById("lblcid").value;
                        sid = document.getElementById("lblsid").value;
                        did = document.getElementById("lbldid").value;
                        clid = document.getElementById("lblclid").value;
                        eqid = document.getElementById("lbleqid").value;
                        var pmstr = document.getElementById("lbldocpmstr").value;
                        var pmid = document.getElementById("lbldocpmid").value;
                        chk = document.getElementById("lblchk").value;
                        coid = document.getElementById("lblcoid").value;
                        fuid = document.getElementById("lblfuid").value;
                        tcnt = document.getElementById("lbltaskcnt").value;
                        typ = document.getElementById("lbltyp").value;
                        lid = document.getElementById("lbllid").value;
                        task = document.getElementById("lbltask").value;
                        var who = document.getElementById("lblwho").value;
                        //alert(tcnt + " - ME")
                        if (tcnt == "1") {
                            window.parent.handletasks("PMOptTasksGrid.aspx?who=" + who + "&comid=" + coid + "&start=yes&tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&lid=" + lid + "&typ=" + typ + "&task=" + task, "ok")
                        }
                        else if (tcnt == "0") {
                            //alert("PMOptMain 3")
                            window.parent.handletasks("PMOptTasks.aspx?who=" + who + "&start=yes&tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&lid=" + lid + "&typ=" + typ, "ok")
                        }
                        else if (tcnt == "2") {
                            window.parent.handletasks("PMOptTasksGrid.aspx?who=" + who + "&start=no", "dept")
                        }
                        else if (tcnt == "3") {
                            window.parent.handletasks("PMOptTasksGrid.aspx?who=" + who + "&start=no", "cell")
                        }
                        else if (tcnt == "4") {
                            parent.handletasks("PMOptTasksGrid.aspx?who=" + who + "&start=no", "eq")
                        }
                        else if (tcnt == "5") {
                            window.parent.handletasks("PMOptTasksGrid.aspx?who=" + who + "&start=no", "fu")
                        }
                    }
                    else {
                        window.parent.handletasks("PMOptTasksGrid.aspx?who=" + who + "&start=no", "dept")
                    }
                }
            }
            else {
                document.getElementById("lbldocflg").value = "";
            }
            checkpg1()
        }
        function checkpg1() {

            //var arch = document.getElementById("lblgetarch").value; //document.getElementById("lbleqid").value!=""||
            //if (arch == "yes") {
            document.getElementById("lblgetarch").value = "no";
            eqid = document.getElementById("lbleqid").value;
            fuid = document.getElementById("lblfuid").value;
            did = document.getElementById("lbldid").value;
            clid = document.getElementById("lblclid").value;
            chk = document.getElementById("lblchk").value;
            lid = document.getElementById("lbllid").value;
            sid = document.getElementById("lblsid").value;
            window.parent.uparch(eqid, chk, did, clid, fuid, lid, sid);
            //}
            //}
        }
        function getdyk() {
            //closelst();
            //did you know drag and drop message
            var eReturn = window.showModalDialog("pmdocmsg.aspx", "", "dialogHeight:450px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
            }
        }
        //-->
    </script>
</head>
<body onload="checkpg();">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 0px; left: 4px" cellspacing="0" cellpadding="0"
        width="740" border="0">
        <tr>
            <td>
                <table cellspacing="1" cellpadding="1">
                    <tr>
                        <td id="tddepts" class="bluelabel" runat="server" height="20">
                            Use Departments
                        </td>
                        <td>
                            <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                        <td id="tdlocs1" class="bluelabel" runat="server">
                            Use Locations
                        </td>
                        <td>
                            <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trdepts" class="view" runat="server">
            <td>
                <table cellspacing="1" cellpadding="1">
                    <tr>
                        <td class="label" style="width: 110px">
                            Department
                        </td>
                        <td id="tddept" class="plainlabel" width="170" runat="server">
                        </td>
                        <td>
                            <img onclick="retminsrch('deptret');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                width="19" height="19" />
                        </td>
                        <td class="label">
                            Equipment
                        </td>
                        <td id="tdeq" class="plainlabel" runat="server">
                        </td>
                        <td>
                            <img onclick="retminsrch('checkeq');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif" />
                        </td>
                        <td class="label">
                            <img id="imgaddeq" class="imgbutton" onmouseover="return overlib('Add a New Equipment Record')"
                                onmouseout="return nd()" onclick="GetEqDiv2();" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
                                width="19" height="19" runat="server">
                        </td>
                        <td style="height: 1px" class="label">
                            <img id="imgcopyeq" onmouseover="return overlib('Copy an Equipment Record')" onmouseout="return nd()"
                                onclick="GetEqCopy2();" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                                width="19" height="19" runat="server">
                        </td>
                        <td>
                            <img id="imgdrag1" onmouseover="return overlib('Drag and Drop Tasks from Procedures', ABOVE, LEFT)"
                                onclick="GetDrag();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/dnd.jpg"
                                runat="server">
                        </td>
                        <td class="label">
                            &nbsp;<img onmouseover="return overlib('Review\Edit PM Approval Process for this Asset', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="getappr();" alt="" width="22" height="19" src="../images/appbuttons/minibuttons/gauge.gif">
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Station\Cell
                        </td>
                        <td id="tdcell" class="plainlabel" runat="server">
                        </td>
                        <td>
                            <img onclick="retminsrch('checkcell');" src="../images/appbuttons/minibuttons/magnifier.gif"
                                width="19" height="19">
                        </td>
                        <td class="label">
                            Function
                        </td>
                        <td id="tdfu" class="plainlabel" runat="server">
                        </td>
                        <td>
                            <img onclick="retminsrch('checkfu');" src="../images/appbuttons/minibuttons/magnifier.gif"
                                width="19" height="19">
                        </td>
                        <td class="label">
                            <img id="imgaddfu" class="imgbutton" onmouseover="return overlib('Add a New Function Record')"
                                onmouseout="return nd()" onclick="GetFuncDiv2();" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
                                width="19" height="19" runat="server">
                        </td>
                        <td class="label">
                            <img id="imgcopyfu" onmouseover="return overlib('Copy a Function Record')" onmouseout="return nd()"
                                onclick="GetFuncCopy2();" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                                width="19" height="19" runat="server">
                        </td>
                        <td align="center">
                            <img id="Img1" onmouseover="return overlib('Jump to the TPM Optimizer', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="jumptpm('o');" src="../images/appbuttons/minibuttons/compresstpm.gif"
                                runat="server" width="19" height="19">
                        </td>
                        <td>
                            <img id="Img2" onmouseover="return overlib('Jump to the TPM Developer', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="jumptpm('r');" src="../images/appbuttons/minibuttons/3gearstpmtrans.gif"
                                runat="server" width="19" height="19">
                        </td>
                        <td>
                            <img id="imgtpma" class="details" onmouseover="return overlib('You have potential TPM Tasks - Click this icon to Review', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="gettpma();" src="../images/appbuttons/minibuttons/nwarningnbg.gif"
                                runat="server" width="19" height="19">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trlocs" class="details" runat="server">
            <td colspan="2">
                <table cellspacing="1" cellpadding="1">
                    <tr>
                        <td class="label" style="width: 110px">
                            Location
                        </td>
                        <td id="tdloc3" class="plainlabel" width="170" runat="server">
                        </td>
                        <td class="label">
                            Equipment
                        </td>
                        <td id="tdeq3" class="plainlabel" runat="server">
                        </td>
                        <td class="label">
                            <img id="imgaddeq1" class="imgbutton" onmouseover="return overlib('Add a New Equipment Record')"
                                onmouseout="return nd()" onclick="GetEqDiv2();" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
                                style="width: 20px" height="20" runat="server">
                        </td>
                        <td style="height: 1px" class="label">
                            <img id="imgcopyeq1" onmouseover="return overlib('Copy an Equipment Record')" onmouseout="return nd()"
                                onclick="GetEqCopy2();" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                                style="width: 20px" height="20" runat="server">
                        </td>
                        <td>
                            <img id="imgdrag" onmouseover="return overlib('Drag and Drop Tasks from Procedures', ABOVE, LEFT)"
                                onclick="GetDrag();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/dnd.jpg"
                                runat="server">
                        </td>
                        <td class="label">
                            &nbsp;<img onmouseover="return overlib('Review\Edit PM Approval Process for this Asset', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="getappr();" alt="" src="../images/appbuttons/minibuttons/gauge.gif">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                        </td>
                        <td class="label">
                            Function
                        </td>
                        <td id="tdfu3" class="plainlabel" runat="server">
                        </td>
                        <td>
                            <img onclick="retfunc();" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                        <td class="label">
                            <img id="imgaddfu1" class="imgbutton" onmouseover="return overlib('Add a New Function Record')"
                                onmouseout="return nd()" onclick="GetFuncDiv2();" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
                                style="width: 20px" height="20" runat="server">
                        </td>
                        <td class="label">
                            <img id="imgcopyfu1" onmouseover="return overlib('Copy a Function Record')" onmouseout="return nd()"
                                onclick="GetFuncCopy2();" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                                style="width: 20px" height="20" runat="server">
                        </td>
                        <td align="center">
                            <img id="Img11" onmouseover="return overlib('Jump to the TPM Optimizer', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="jumptpm('o');" src="../images/appbuttons/minibuttons/compresstpm.gif"
                                runat="server">
                        </td>
                        <td>
                            <img id="Img21" onmouseover="return overlib('Jump to the TPM Developer', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="jumptpm('r');" src="../images/appbuttons/minibuttons/3gearstpmtrans.gif"
                                runat="server">
                        </td>
                        <td>
                            <img id="imgtpma1" class="details" onmouseover="return overlib('You have potential TPM Tasks - Click this icon to Review', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="gettpma();" src="../images/appbuttons/minibuttons/nwarningnbg.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trrev" class="details" runat="server">
            <td>
                <table cellspacing="1" cellpadding="1">
                    <tr>
                        <td class="label" style="width: 110px">
                            <asp:Label ID="lang246" runat="server">Revision</asp:Label>
                        </td>
                        <td id="tdrev" class="plainlabel" runat="server" style="width: 150px">
                            1234
                        </td>
                        <td class="label">
                            <img id="Img5" onmouseover="return overlib('Archive Current PM Revision and Create a New Revision')"
                                onmouseout="return nd()" onclick="archit();" alt="" src="../images/appbuttons/minibuttons/archive.gif"
                                runat="server">
                        </td>
                        <td class="label">
                            <img id="Img6" onmouseover="return overlib('Archive Current PM Revision and Convert for Optimization')"
                                onmouseout="return nd()" onclick="optit();" alt="" src="../images/appbuttons/minibuttons/optit.gif"
                                runat="server">
                        </td>
                        <td class="label">
                            <asp:Label ID="lang991" runat="server">Procedures</asp:Label>
                        </td>
                        <td class="label">
                            <asp:DropDownList ID="ddproc" runat="server" CssClass="plainlabel" Width="170px"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="label">
                            <img class="imgbutton" id="imgproc" onmouseover="return overlib('Add Procedures to Optimize', ABOVE, LEFT)"
                                onclick="GetProcDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/upload.gif"
                                style="width: 20px" runat="server">
                        </td>
                        <td style="width: 20px">
                            <img alt="" onclick="getdyk();" src="../images/appbuttons/minibuttons/Q.gif" height="18"
                                width="18" />
                        </td>
                        <td class="details">
                            <img class="details" id="imgbulk" onmouseover="return overlib('Choose a Bulk Loaded Procedure to Optimize', ABOVE, LEFT)"
                                onclick="getbulk();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/bulkproc.gif"
                                runat="server">
                        </td>
                        <td class="label">
                            <img class="details" id="imgbulkg" onmouseover="return overlib('Choose a Bulk Loaded Procedure from the Document Library to Optimize', ABOVE, LEFT)"
                                onclick="getbulkg();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/globlist.gif"
                                runat="server">
                        </td>
                    </tr>
                    <tr class="details" id="trtots" runat="server">
                        <td class="redlabel" colspan="8" align="center">
                            <asp:Label ID="lang992" runat="server">Using Totals for Original Planning &amp; Scheduling Details</asp:Label>
                        </td>
                        <td>
                            <img onmouseover="return overlib('View\Edit Original Total Values', ABOVE, LEFT)"
                                onclick="getpms();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/pencil12.gif">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lbldept" type="hidden" name="lbldid" runat="server">
    <input id="lblpar" type="hidden" name="lblpar" runat="server">
    <input id="lbltl" type="hidden" name="lbltl" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server">
    <input id="lblpar2" type="hidden" name="lblpar2" runat="server">
    <input id="lblclid" type="hidden" name="lblclid" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblpmflg" type="hidden" name="lblpmflg" runat="server">
    <input id="lblpmid" type="hidden" name="lblpmid" runat="server">
    <input id="lbldoctype" type="hidden" name="lbldoctype" runat="server">
    <input id="lblpmnum" type="hidden" name="lblpmtype" runat="server">
    <input id="lblpmtype" type="hidden" name="lblpmtype" runat="server">
    <input id="lblpchk" type="hidden" name="lblpchk" runat="server">
    <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
    <input id="tasknum" type="hidden" name="tasknum" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lblcleantasks" type="hidden" name="lblcleantasks" runat="server">
    <input id="lblgototasks" type="hidden" name="lblgototasks" runat="server">
    <input id="lbltaskcnt" type="hidden" name="lbltaskcnt" runat="server">
    <input id="appchk" type="hidden" name="appchk" runat="server"><input id="lbllog"
        type="hidden" name="lbllog" runat="server">
    <input id="lblgetarch" type="hidden" name="lblgetarch" runat="server"><input id="lblpmtyp"
        type="hidden" name="lblpmtyp" runat="server">
    <input id="lblhaspm" type="hidden" name="lblhaspm" runat="server">
    <input id="lbltyp" type="hidden" name="lbltyp" runat="server">
    <input id="lbllid" type="hidden" name="lbllid" runat="server">
    <input id="lblro" type="hidden" name="lblro" runat="server">
    <input id="lblnoeq" type="hidden" name="lblnoeq" runat="server"><input id="lbltask"
        type="hidden" name="lbltask" runat="server">
    <input id="lblfslang" type="hidden" name="lblfslang" runat="server">
    <input id="lblrettyp" type="hidden" name="lblrettyp" runat="server">
    <input id="lblcell" type="hidden" name="lblcell" runat="server">
    <input id="lbldid" type="hidden" name="lbldid" runat="server">
    <input id="lbleq" type="hidden" name="lbleq" runat="server">
    <input id="lblfu" type="hidden" name="lblfu" runat="server">
    <input id="lblloc" type="hidden" name="lblloc" runat="server"><input id="lblcomp"
        type="hidden" name="lblcomp" runat="server">
    <input id="lbllevel" type="hidden" name="lbllevel" runat="server">
    <input id="lbldocpmid" type="hidden" name="lbldocpmid" runat="server"><input id="lbldocpmstr"
        type="hidden" name="lbldocpmstr" runat="server">
    <input id="lblcellchk" type="hidden" name="lblcellchk" runat="server"><input id="Hidden22"
        type="hidden" name="Hidden22" runat="server">
    <input id="lblusetot" type="hidden" name="lblusetot" runat="server">
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lbldocflg" runat="server" />
    </form>
</body>
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../scripts/showModalDialog.js"></script>
</html>
