<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="proclib.aspx.vb" Inherits="lucy_r12.proclib" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>proclib</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script  type="text/javascript" src="../scripts1/proclibaspx.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table style="left: 4px; position: absolute; top: 4px" cellspacing="1" cellpadding="1"
        width="400">
        <tr id="trsrch" runat="server">
            <td class="bluelabel">
                <asp:Label ID="lang1047" runat="server">Search By Document Title</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtsrch" runat="server" CssClass="plainlabel" Width="190px"></asp:TextBox>
            </td>
            <td>
                <asp:ImageButton ID="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                </asp:ImageButton>
            </td>
        </tr>
        <tr id="tr1" runat="server">
            <td colspan="3">
                <div id="supdiv" style="border-right: black 1px solid; border-top: black 1px solid;
                    overflow: auto; border-left: black 1px solid; border-bottom: black 1px solid;
                    height: 200px" runat="server">
                </div>
            </td>
        </tr>
    </table>
    <input id="lblpost" type="hidden" runat="server" name="lblpost"><input id="lbltyp"
        type="hidden" runat="server" name="lbltyp">
    <input id="lblskillid" type="hidden" runat="server" name="lblskillid"><input id="lblsupid"
        type="hidden" runat="server" name="lblsupid">
    <input type="hidden" id="lblsid" runat="server" name="lblsid">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
