<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMOptTasks.aspx.vb" Inherits="lucy_r12.PMOptTasks" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>PMOptTasks</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script  type="text/javascript" src="../scripts/overlib2.js"></script>
    <script  type="text/javascript" src="../scripts/pmtaskdivfuncopt_1016_1.js"></script>
    <script  type="text/javascript" src="../scripts1/PMOptTasksaspx_1016_2.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />   
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../scripts/showModalDialog.js"></script>
    <script  type="text/javascript">
        <!--
        function checkdeltask() {
            var chken = document.getElementById("lblenable").value;
            if (chken != "1") {
                tid = document.getElementById("lbltaskid").value;
                var tpmhold = document.getElementById("lbltpmhold").value;
                if (tid != "" && tid != "0" && tpmhold != "1") {
                    var deltask = confirm("Are you sure you want to Delete this Task?");
                    if (deltask == true) {
                        document.getElementById("lbltpmalert").value = "no";
                        document.getElementById("lblcompchk").value = "7";
                        FreezeScreen('Your Data Is Being Processed...');
                        document.getElementById("form1").submit();
                    }
                    else if (deltask == false) {
                        return false;
                        alert("Action Cancelled")
                    }
                }
            }
        }
        function checkdeltasktpm() {
            var chken = document.getElementById("lblenable").value;
            //if(chken!="1") {
            tid = document.getElementById("lbltaskid").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            //alert(tid + "," + tpmhold)
            if (tid != "" && tid != "0" && tpmhold != "0") {
                //var deltask = confirm("Are you sure you want to Delete this Task from\nthe TPM Module and Enable Editing?");
                var deltask = confirm("Removing this task from TPM will result in the loss of images and scheduling data.\nDo you want to continue?");
                if (deltask == true) {
                    document.getElementById("lblcompchk").value = "deltpm";
                    FreezeScreen('Your Data Is Being Processed...');
                    document.getElementById("form1").submit();
                }
                else if (deltask == false) {
                    return false;
                    alert("Action Cancelled")
                }
            }
            //}
        }
        function tasklookup() {

            var chken = document.getElementById("lblenable").value;
            var ro = document.getElementById("lblro").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            if ((chken != "1" || ro == "1") && tpmhold != "1") {
                
                var str = document.getElementById("txtdesc").value;
                var eReturn = window.showModalDialog("../apps/TaskLookup.aspx?str=" + str, tasklookupCallback, "dialogHeight:500px; dialogWidth:800px; resizable=yes");
                if (eReturn != "no" && eReturn != "~none~") {
                    tasklookupCallback(eReturn);
                }
                else if (eReturn != "~none~") {
                    document.getElementById("form1").submit();
                }
            }
        }

        function tasklookupCallback(eReturn) {
            document.getElementById("txtdesc").value = eReturn;
        }

        function CheckDel(val) {

            if (val == "Delete") {
                var decision = confirm("Are You Sure You Want to Mark this Task as Delete?")
                if (decision == true) {
                    //document.getElementById("submit('ddcomp', '');
                    FreezeScreen('Your Data Is Being Processed...');
                    document.getElementById("lbldel").value = "delr"
                    document.getElementById("form1").submit();
                }
                else {
                    alert("Action Cancelled")
                }

            }
        }
        function DisableButton(b) {
            document.getElementById("ibToTask").className = "details";
            document.getElementById("ibFromTask").className = "details";
            document.getElementById("ibfromo").className = "details";
            document.getElementById("ibtoo").className = "details";
            document.getElementById("ibReuse").className = "details";
            document.getElementById("todis").className = "view";
            document.getElementById("fromdis").className = "view";
            document.getElementById("todis1").className = "view";
            document.getElementById("fromdis1").className = "view";
            document.getElementById("fromreusedis").className = "view";
            document.getElementById("form1").submit();
        }
        function GetType(app, sval) {
            //alert(app + ", " + sval)
            var ghostoff = document.getElementById("lblghostoff").value;
            var xstatus = document.getElementById("lblxstatus").value;
            //alert(ghostoff + ", " + xstatus)
            var xflg = 0;
            if (ghostoff == "yes" && xstatus == "yes") {
                xflg = 1;
            }
            var typlst
            var sstr
            var ptlst
            var ssti
            var ptolst
            if (app == "d") {
                typlst = document.getElementById("ddtype");
                ptlst = document.getElementById("ddpt");
                sstr = typlst.options[typlst.selectedIndex].text;
                ssti = document.getElementById("ddtype").value;
                //sstr == "4 - Cond Monitoring" || sstr == "4 - Verifica e Controllo di Cond"
                if (ssti == "7") {
                    document.getElementById("ddpt").disabled = false;
                }
                else {
                    ptlst.value = "0";
                    document.getElementById("ddpt").disabled = true;
                }
            }
            else if (app == "o") {
                if (xflg == 0) {
                    document.getElementById("ddtype").value = sval;
                }
                typlst = document.getElementById("ddtypeo");
                ptlst = document.getElementById("ddpt");
                ptolst = document.getElementById("ddpto");
                sstr = typlst.options[typlst.selectedIndex].text
                ssti = document.getElementById("ddtypeo").value;
                if (ssti == "7") {
                    if (xflg == 0) {
                        document.getElementById("ddpt").disabled = false;
                    }
                    document.getElementById("ddpto").disabled = false;
                }
                else {
                    if (xflg == 0) {
                        ptlst.value = "0";
                        document.getElementById("ddpt").disabled = true;
                    }
                    ptolst.value = "0";
                    document.getElementById("ddpto").disabled = true;
                }
            }
        }
        function filldown(id, val) {
            var ghostoff = document.getElementById("lblghostoff").value;
            var xstatus = document.getElementById("lblxstatus").value;
            var xflg = 0;
            if (ghostoff == "yes" && xstatus == "yes") {
                xflg = 1;
            }
            if (id == 'o') {
                if (xflg == 0) {
                    document.getElementById('txttr').value = val;
                }
                var dt = document.getElementById("ddeqstato").options[document.getElementById("ddeqstato").options.selectedIndex].text;
                dt = document.getElementById("ddeqstato").value;
                if (dt == "2") {
                    document.getElementById("txtordt").value = val;
                    if (xflg == 0) {
                        document.getElementById("txtrdt").value = val;
                    }
                }
            }
            else {
                document.getElementById("lblcompchng").value = "1";
                var dt = document.getElementById("ddeqstat").options[document.getElementById("ddeqstat").options.selectedIndex].text;
                dt = document.getElementById("ddeqstat").value;
                if (dt == "2") {
                    document.getElementById("txtrdt").value = val;
                }
            }
        }
        function zerodt(id, val) {
            var ghostoff = document.getElementById("lblghostoff").value;
            var xstatus = document.getElementById("lblxstatus").value;
            var xflg = 0;
            if (ghostoff == "yes" && xstatus == "yes") {
                xflg = 1;
            }
            if (id == 'o') {
                var dt = document.getElementById("ddeqstato").options[document.getElementById("ddeqstato").options.selectedIndex].text;
                dt = document.getElementById("ddeqstato").value;
                if (dt != "2") {
                    document.getElementById("txtordt").value = 0;
                    if (xflg == 0) {
                        document.getElementById("txtrdt").value = "0";
                    }
                }
                else {
                    document.getElementById("txtordt").value = document.getElementById("txttro").value;
                    if (xflg == 0) {
                        document.getElementById("txtrdt").value = document.getElementById("txttr").value;
                    }
                }
            }
            else {
                var dt = document.getElementById("ddeqstat").options[document.getElementById("ddeqstat").options.selectedIndex].text;
                dt = document.getElementById("ddeqstat").value;
                if (dt != "2") {
                    document.getElementById("txtrdt").value = "0";
                }
                else {
                    document.getElementById("txtrdt").value = document.getElementById("txttr").value;
                }
            }
        }

        function GetGrid() {
            handleapp();
            document.getElementById("lblgrid").value = "yes";
            chk = document.getElementById("lblchk").value;
            cid = document.getElementById("lblcid").value;
            tid = document.getElementById("lbltaskid").value;
            funid = document.getElementById("lblfuid").value;
            comid = document.getElementById("lblco").value;
            clid = document.getElementById("lblclid").value;
            typ = document.getElementById("lbltyp").value;
            lid = document.getElementById("lbllid").value;
            pmid = "none"//document.getElementById("lbldocpmid").value;
            var ro = document.getElementById("lblro").value;
            var who = document.getElementById("lblwho").value;
            //alert(who)
            var ustr = document.getElementById("lblusername").value;
            if (pmid == "") pmid = "none";
            pmstr = document.getElementById("lbldocpmstr").Value
            if (pmstr == "") pmstr = "none";
            if (tid != "" || tid != "0") {
                window.open("../apps/GTasksFunc2.aspx?app=" + who + "&tli=5a&funid=" + funid + "&comid=no&cid=" + cid + "&chk=" + chk + "&pmid=" + pmid + "&pmstr=" + pmstr + "&lid=" + lid + "&typ=" + typ + "&date=" + Date() + "&ro=" + ro + "&ustr=" + ustr, target = "_top");
            }
            else {
                alert("No Task Records Selected Yet!")
            }
        }
        function getPFDiv() {

            //handleapp();
            var chken = document.getElementById("lblenable").value;
            var ro = document.getElementById("lblro").value;
            ptid = document.getElementById("lbltaskid").value;
            if (chken != "1" || ro == "1") {
                if (ptid != "") {
                    window.parent.setref();
                    var eq = document.getElementById("lbleqid").value;
                    var valu = document.getElementById("lbltaskid").value;
                    //document.getElementById("form1").submit();
                    var eReturn = window.showModalDialog("../equip/PFIntDialog.aspx?tid=" + valu + "&eqid=" + eq + "&ro=" + ro, "", "dialogHeight:560px; dialogWidth:669px; resizable=yes");
                }
                if (eReturn) {
                    alert(eReturn)
                    document.getElementById("txtpfint").value = eReturn;
                    document.getElementById("form1").submit()
                }
            }
        }
        function getmeter() {
            var enable = document.getElementById("lblenable").value;
            var eqid = document.getElementById("lbleqid").value;
            var eqnum = document.getElementById("lbleqnum").value;
            var pmtskid = document.getElementById("lbltaskid").value;
            var fuid = document.getElementById("lblfuid").value;
            var skillid = document.getElementById("lblskillid").value;
            var skillqty = document.getElementById("lblskillqty").value;
            var rdid = document.getElementById("lblrdid").value;
            var func = document.getElementById("lblfunc").value;
            var skill = document.getElementById("lblskill").value;
            var rd = document.getElementById("lblrd").value;
            var meterid = document.getElementById("lblmeterid").value;
            var varflg = 0;
            if (enable == "0" && pmtskid != "") {
                if (skillid == "" || skillid == "0") {
                    skillid = document.getElementById("ddskill").value;
                    if (skillid == "" || skillid == "0") {
                        alert("Skill Required")
                        varflg = 1;
                    }
                    else {
                        var sklist = document.getElementById("ddskill");
                        var skstr = sklist.options[sklist.selectedIndex].text;
                        skill = skstr;
                    }
                }
                else if (skillqty == "") {
                    skillqty = document.getElementById("txtqty").value;
                    if (skillqty == "") {
                        alert("Skill Qty Required")
                        varflg = 1;
                    }
                }
                else if (rdid == "" || rdid == "0") {
                    rdid = document.getElementById("ddeqstat").value;
                    if (rdid == "" || rdid == "0") {
                        alert("Running or Down Value Required")
                        varflg = 1;
                    }
                    else {
                        var rdlist = document.getElementById("ddeqstat");
                        var rdstr = rdlist.options[rdlist.selectedIndex].text;
                        rd = rdstr;
                    }
                }
                if (varflg == 0) {
                    var eReturn = window.showModalDialog("../equip/usemeterdialog.aspx?typ=reg&eqid=" + eqid + "&pmtskid=" + pmtskid + "&fuid=" + fuid + "&skillid=" + skillid
                                   + "&skillqty=" + skillqty + "&rdid=" + rdid + "&func=" + func + "&skill=" + skill + "&rd=" + rd
                                   + "&meterid=" + meterid + "&rdt=" + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:900px;resizable=yes");
                    if (eReturn) {
                        document.getElementById("lblcompchk").value = "2";
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        function getmetero() {
            var enable = document.getElementById("lblenable").value;
            var eqid = document.getElementById("lbleqid").value;
            var eqnum = document.getElementById("lbleqnum").value;
            var pmtskid = document.getElementById("lbltaskid").value;
            var fuid = document.getElementById("lblfuid").value;
            var skillid = document.getElementById("lblskillid").value;
            var skillqty = document.getElementById("lblskillqty").value;
            var rdid = document.getElementById("lblrdid").value;
            var skill = document.getElementById("lblskill").value;
            var rd = document.getElementById("lblrd").value;
            var func = document.getElementById("lblfunc").value;
            var meterid = document.getElementById("lblmeterid").value;
            var varflg = 0;
            if (skillid == "" || skillid == "0") {
                skillid = document.getElementById("ddskill").value;
                if (skillid == "" || skillid == "0") {
                    alert("Revised Skill Required")
                    varflg = 1;
                }
                else {
                    var sklist = document.getElementById("ddskill");
                    var skstr = sklist.options[sklist.selectedIndex].text;
                    skill = skstr;
                }
            }
            else if (skillqty == "") {
                skillqty = document.getElementById("txtqty").value;
                if (skillqty == "") {
                    alert("Revised Skill Qty Required")
                    varflg = 1;
                }
            }
            else if (rdid == "" || rdid == "0") {
                rdid = document.getElementById("ddeqstat").value;
                if (rdid == "" || rdid == "0") {
                    alert("Revised Running or Down Value Required")
                    varflg = 1;
                }
                else {
                    var rdlist = document.getElementById("ddeqstat");
                    var rdstr = sklist.options[rdlist.selectedIndex].text;
                    rd = rdstr;
                }
            }

            //orig
            if (varflg == 0) {
                var skillido = document.getElementById("lblskillido").value;
                var skillqtyo = document.getElementById("lblskillqtyo").value;
                var rdido = document.getElementById("lblrdido").value;
                var skillo = document.getElementById("lblskillo").value;
                var rdo = document.getElementById("lblrdo").value;
                var meterido = document.getElementById("lblmeterido").value;

                if (enable == "0" && pmtskid != "") {
                    if (skillido == "" || skillido == "0") {
                        skillido = document.getElementById("ddskillo").value;
                        if (skillido == "" || skillido == "0") {
                            alert("Original Skill Required")
                            varflg = 1;
                        }
                        else {
                            var sklist = document.getElementById("ddskillo");
                            var skstr = sklist.options[sklist.selectedIndex].text;
                            skillo = skstr;
                        }
                    }
                    else if (skillqtyo == "") {
                        skillqtyo = document.getElementById("txtqtyo").value;
                        if (skillqtyo == "") {
                            alert("Original Skill Qty Required")
                            varflg = 1;
                        }
                    }
                    else if (rdido == "" || rdido == "0") {
                        rdido = document.getElementById("ddeqstato").value;
                        if (rdido == "" || rdido == "0") {
                            alert("Original Running or Down Value Required")
                            varflg = 1;
                        }
                        else {
                            var rdlist = document.getElementById("ddeqstato");
                            var rdstr = sklist.options[rdlist.selectedIndex].text;
                            rdo = rdstr;
                        }
                    }
                    if (varflg == 0) {
                        var eReturn = window.showModalDialog("../equip/usemeterdialog.aspx?typ=orig&eqid=" + eqid + "&pmtskid=" + pmtskid + "&fuid=" + fuid + "&skillid=" + skillid
                                   + "&skillqty=" + skillqty + "&rdid=" + rdid + "&func=" + func + "&skill=" + skill + "&rd=" + rd
                                   + "&meterid=" + meterid + "&rdt=" + "&eqnum=" + eqnum + "&skillido=" + skillido
                                  + "&skillqtyo=" + skillqtyo + "&rdido=" + rdido + "&skillo=" + skillo + "&rdo=" + rdo
                                  + "&meterido=" + meterido + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:900px;resizable=yes");
                        if (eReturn) {
                            document.getElementById("lblcompchk").value = "2";
                            document.getElementById("form1").submit();
                        }
                    }
                }
            }

        }
        function getValueAnalysis() {

            tl = document.getElementById("lbltasklev").value;
            cid = document.getElementById("lblcid").value;
            sid = document.getElementById("lblsid").value;
            did = document.getElementById("lbldid").value;
            clid = document.getElementById("lblclid").value;
            eqid = document.getElementById("lbleqid").value;
            //alert(eqid)
            chk = document.getElementById("lblchk").value;
            fuid = document.getElementById("lblfuid").value;
            if (fuid != "") {
                window.parent.setref();
                var ht = "2000"; //screen.Height - 20;
                var wd = "1000"; //screen.Width - 20;

                var eReturn = window.showModalDialog("../reports/reportdialog.aspx?who=pmo&tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:1000px; dialogWidth:2000px;resizable=yes");
                //var eReturn = window.showModalDialog("../reports/reportdialog.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:610px; dialogWidth:610px");
                if (eReturn) {
                    if (eReturn != "ret") {
                        document.getElementById("lblsvchk").value = "7";
                        document.getElementById("pgflag").value = eReturn;
                        //alert(eReturn)
                        document.getElementById("form1").submit();
                    }
                }
            }
            else {
                alert("No Task Records Selected Yet!")
            }
        }
        function getfail2() {
            //eqid, fuid, coid, sid, pmtskid, eqnum, func, comp, task
            var eqid = document.getElementById("lbleqid").value;
            var fuid = document.getElementById("lblfuid").value;
            var coid = document.getElementById("lblco").value;
            var sid = document.getElementById("lblsid").value;
            var task = document.getElementById("lbltaskid").value;
            var eqnum = "";
            var func = "";
            var comp = "";
            var pmtskid = document.getElementById("lblpg").innerHTML;

            var eReturn = window.showModalDialog("../apps/taskwodialog.aspx?eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&sid=" + sid + "&pmtskid=" + pmtskid + "&eqnum=" + eqnum + "&comp=" + comp + "&func=" + func + "&task=" + task, "", "dialogHeight:600px; dialogWidth:900px; resizable=yes");
            if (eReturn) {

            }
        }
        function checkit() {
            var addchk = document.getElementById("lblcompchk").value;
            var addchk2 = document.getElementById("lblstart").value;
            if (addchk == "yes" || addchk2 == "yes") {
                doEnable();
                document.getElementById("lblcompchk").value = "";
            }

            var tnum = document.getElementById("lblpg").innerHTML;
            window.parent.handletnum(tnum);

            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                window.parent.doref();
            }
            else {
                window.parent.setref();
            }
            var chk = document.getElementById("lblusetot").value;
            if (chk == "1") {
                document.getElementById("ddskillo").disabled = true;
                document.getElementById("txttro").disabled = true;
                document.getElementById("txtfreqo").disabled = true;
                document.getElementById("ddeqstato").disabled = true;
            }
            populateArrays();
            var start = document.getElementById("lblstart").value;
            if (start == "yes1") {
                GetNavGrid();
            }
            document.getElementById("lblstart").value = "no";

            var grid = document.getElementById("lblgrid").value;
            if (grid == "yes") {
                window.location.reload(false);
                document.getElementById("form1").submit();
            }
            var tpm = document.getElementById("lbltpmalert").value;
            var tpmhold = document.getElementById("lbltpmhold").value; //&&tpmhold!="1"
            if (tpm == "yes") {
                var decision = confirm("This Task Meets Your Requirements for TPM\nDo You Want to Transfer this Task to TPM?")
                if (decision == true) {
                    //document.getElementById("submit('ddcomp', '');
                    FreezeScreen('Your Data Is Being Processed...');
                    document.getElementById("lblcompchk").value = "uptpm";
                    document.getElementById("form1").submit();
                }
                else {
                    document.getElementById("lbltpmalert").value = "no";
                    alert("Action Cancelled")
                }
            }
            var chk = document.getElementById("lblpar").value;
            if (chk == "task") {
                valu = document.getElementById("lbltaskid").value;
                var num = document.getElementById("lblt").innerHTML;
                window.parent.handletask(chk, valu, num);
            }
            else if (chk == "comp") {
                cvalu = document.getElementById("lblco").value;
                tvalu = document.getElementById("lbltaskid").value;
                tnum = document.getElementById("lblt").value;
                window.parent.handleco(chk, cvalu, tvalu, tnum);
            }

        }
        function doEnable() {
            var tpmhold = document.getElementById("lbltpmhold").value;
            var ro = document.getElementById("lblro").value;
            var pg = document.getElementById("lblpg").innerHTML;
            //alert(pg + "," + ro + "," + tpmhold)
            if (pg != "0" && ro != "1" && tpmhold != "1") {
                //alert(document.getElementById("lblenable").value)
                var ismeter = document.getElementById("lblusemeter").value;
                //var isfixed = document.getElementById("lblisfixed").value;
                document.getElementById("lblenable").value = "0";

                document.getElementById("ddcomp").disabled = false;
                document.getElementById("lbfaillist").disabled = false;
                document.getElementById("lbfailmodes").disabled = false;
                document.getElementById("lbCompFM").disabled = false;
                document.getElementById("txtcQty").disabled = false;
                document.getElementById("ibReuse").disabled = false;
                document.getElementById("ibToTask").disabled = false;
                document.getElementById("ibFromTask").disabled = false;
                document.getElementById("txtdesc").disabled = false;
                document.getElementById("ddtype").disabled = false;
                if (ismeter != "1") {
                    document.getElementById("txtfreq").disabled = false;
                    document.getElementById("cbfixed").disabled = false;
                }
                document.getElementById("txtpfint").disabled = false;
                document.getElementById("txtqty").disabled = false;
                document.getElementById("txttr").disabled = false;
                document.getElementById("txtrdt").disabled = false;
                //document.getElementById("ddpt.disabled=false;
                var sstr;
                var ssti;
                var typlst;
                typlst = document.getElementById("ddtype");
                sstr = typlst.options[typlst.selectedIndex].text
                ssti = document.getElementById("ddtype").value;
                if (sstr == "7") {
                    document.getElementById("ddpt").disabled = false;
                    try {
                        document.getElementById("ddpto").disabled = false;
                    }
                    catch (err) {

                    }
                }
                document.getElementById("ddeqstat").disabled = false;
                document.getElementById("ddskill").disabled = false;
                document.getElementById("cbloto").disabled = false;

                document.getElementById("cbcs").disabled = false;

                document.getElementById("btnaddtsk").disabled = false;
                document.getElementById("btnaddsubtask").disabled = false;
                try {
                    document.getElementById("btntpm").disabled = false;
                }
                catch (err) {

                }
                document.getElementById("txttaskorder").disabled = false;
                try {
                    document.getElementById("btndeltask").disabled = false;
                }
                catch (err) {
                    //do nothing - button not used in optimizer
                }


                document.getElementById("ddascd").disabled = false;




                document.getElementById("ibCancel").disabled = false;
                //document.getElementById("btnsavetask").disabled=false;
                var ismetero = document.getElementById("lblusemetero").value;
                //var isfixedo = document.getElementById("lblisfixedo").value;
                if (oflag == "1") {
                    document.getElementById("lbofailmodes").disabled = false;
                    document.getElementById("txtodesc").disabled = false;
                    document.getElementById("ddtypeo").disabled = false;
                    if (ismetero != "1") {
                        document.getElementById("txtfreqo").disabled = false;
                        document.getElementById("cbfixedo").disabled = false;
                    }
                    document.getElementById("txtqtyo").disabled = false;
                    document.getElementById("txttro").disabled = false;
                    document.getElementById("txtordt").disabled = false;
                    //document.getElementById("ddpto.disabled=false;
                    document.getElementById("ddeqstato").disabled = false;
                    document.getElementById("ddskillo").disabled = false;
                    document.getElementById("ddtaskstat").disabled = false;
                    var chk = document.getElementById("lblusetot").value;
                    if (chk == "1") {
                        document.getElementById("ddskillo").disabled = true;
                        document.getElementById("txttro").disabled = true;
                        document.getElementById("txtfreqo").disabled = true;
                        document.getElementById("ddeqstato").disabled = true;
                        document.getElementById("txtqtyo").disabled = true;
                        document.getElementById("txtordt").disabled = true;
                    }
                }
            }
            else {
                if (pg == "0") {
                    alert("No Task Records Are Loaded Yet!")
                }
                else if (tpmhold == "1") {
                    alert("This has been designated as a TPM Task and Cannot be Edited!")
                }
            }
        }

        function valpgnums() {
            if (document.getElementById("lblenable").value != "1") {
                var tpmhold = document.getElementById("lbltpmhold").value;
                if (tpmhold != "1") {
                    var desc = document.getElementById("txtdesc").innerHTML;
                    var odesc = document.getElementById("txtodesc").innerHTML;
                    var compchng = document.getElementById("txtcQty").value;
                    if (desc.length > 500) {
                        alert("Maximum Lenth for Task Description is 500 Characters")
                    }
                    else if (odesc.length > 500) {
                        alert("Maximum Lenth for Task Description is 500 Characters")
                    }
                    else if (isNaN(document.getElementById("txtqtyo").value)) {
                        alert("Original Skill Qty is Not a Number")
                    }
                    else if (isNaN(document.getElementById("txtqty").value)) {
                        alert("Revised Skill Qty is Not a Number")
                    }
                    else if (isNaN(document.getElementById("txtfreqo").value)) {
                        alert("Original Frequency is Not a Number")
                    }
                    else if (document.getElementById("txtfreqo").value < 1 && document.getElementById("txtfreqo").value != 0) {
                        alert("Original Frequency is Less Than 1")
                    }
                    else if (isNaN(document.getElementById("txtfreq").value)) {
                        alert("Revised Frequency is Not a Number")
                    }
                    else if (document.getElementById("txtfreq").value < 1) {
                        alert("Revised Frequency is Less Than 1")
                    }
                    else if (isNaN(document.getElementById("txttro").value)) {
                        alert("Original Skill Time is Not a Number")
                    }
                    else if (isNaN(document.getElementById("txttr").value)) {
                        alert("Revised Skill Time is Not a Number")
                    }
                    else if (isNaN(document.getElementById("txtordt").value)) {
                        alert("Original Running/Down Time is Not a Number")
                    }
                    else if (isNaN(document.getElementById("txtrdt").value)) {
                        alert("Revised Running/Down Time is Not a Number")
                    }
                    else if (document.getElementById("ddtaskstat").value == 'Delete') {
                        CheckChanges();
                    }

                    else if (compchng != "1" && compchng != "0" && compchng != "") {
                    vercompchng();
                    }
                    else {
                        document.getElementById("lblcompchk").value = "3";
                        FreezeScreen('Your Data Is Being Processed...');
                        document.getElementById("form1").submit();
                    }

                }
            }
        }

        function vercompchng() {
            var cqtychk = document.getElementById("txtcQty").value;
            var sklchk = document.getElementById("txttr").value;
            //sklchk = Number(sklchk)
            //alert(cqtychk)
            //alert(sklchk)
            //cqtychk = parseInt(cqtychk);
            //sklchk = parseInt(sklchk);
            
            if (cqtychk > 1 && sklchk > -1) {
                //do pop up here
                var eReturn = window.showModalDialog("../appsopt/checkcompqtydialog.aspx", "", "dialogHeight:200px; dialogWidth:400px;resizable=yes")
                if (eReturn) {
                    //alert(eReturn)
                    if (eReturn == "1") {
                        multcomp();
                        document.getElementById("lblcompchk").value = "3";
                        FreezeScreen('Your Data Is Being Processed...');
                        document.getElementById("form1").submit();
                    }
                    else {
                        document.getElementById("lblcompchk").value = "3";
                        FreezeScreen('Your Data Is Being Processed...');
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        function multcomp() {
            if (document.getElementById("lblenable").value != "1") {
                var cqty = document.getElementById("txtcQty").value;
                var cmins = document.getElementById("txttr").value;
                var dmins = document.getElementById("txtrdt").value;
                if (cqty > 1 && cmins > 0) {
                    var newmins = cqty * cmins;
                    document.getElementById("txttr").value = newmins;
                }
                if (cqty > 1 && dmins > 0) {
                    var newdmins = cqty * dmins;
                    document.getElementById("txtrdt").value = newdmins;
                }
            }
        }
        function checkcqty(val) {
            //alert(val)
            //document.getElementById("lblcompchng").value = "1";
            /*
            if (val > 1) {
            var decision = confirm("Update Total Task Time based on new Component Qty value?")
            if (decision == true) {
            //document.getElementById("submit('ddcomp', '');
            //FreezeScreen('Your Data Is Being Processed...');
            multcomp();
            alert("Save Required to record new time values")
            }
            else {
            alert("Action Cancelled")
            }

            }
            */
        }
        function chkqty1() {

        }
            //-->
    </script>
</head>
<body class="tbg" onload="page_maint('o');checkit();">
    <div style="z-index: 1000; position: absolute; visibility: hidden" id="overDiv">
    </div>
    <form id="form1" method="post" runat="server">
    <div id="FreezePane" class="FreezePaneOff" align="center">
        <div id="InnerFreezePane" class="InnerFreezePane">
        </div>
    </div>
    <div style="z-index: 100; position: absolute; top: 0px; left: 0px">
        <table cellspacing="0" cellpadding="3" width="742">
            <tr>
                <td class="thdrsinglft" style="width: 26px" align="left">
                    <img border="0" src="../images/appbuttons/minibuttons/optbg.gif">
                </td>
                <td class="thdrsingrt label" onmouseover="return overlib('Against what is the PM intended to protect?', ABOVE, LEFT)"
                    onmouseout="return nd()" width="714">
                    <asp:Label ID="lang1015" runat="server">Failure Modes, Causes And/Or Effects</asp:Label>
                </td>
            </tr>
        </table>
        <table id="tbeq" cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td class="label" height="20" width="160">
                    <asp:Label ID="lang1016" runat="server">Component Addressed:</asp:Label>
                </td>
                <td class="label" width="240">
                    <asp:DropDownList ID="ddcomp" runat="server" CssClass="plainlabel" Width="240px">
                    </asp:DropDownList>
                </td>
                <td class="label" width="45">
                    <img id="btnaddcomp" onmouseover="return overlib('Add/Edit Components for Selected Function', ABOVE, LEFT)"
                        onmouseout="return nd()" onclick="GetCompDiv();" alt="" src="../images/appbuttons/minibuttons/addmod.gif"
                        runat="server"><img id="imgcopycomp" onmouseover="return overlib('Copy Components for Selected Function', ABOVE, LEFT)"
                            onmouseout="return nd()" onclick="GetCompCopy();" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                            style="width: 20px" height="20" runat="server">
                </td>
                <td class="label" width="30">
                    <asp:Label ID="lang1017" runat="server">Qty:</asp:Label>
                </td>
                <td class="label" width="63">
                    <asp:TextBox ID="txtcQty" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                    <img id="img1" onmouseover="return overlib('Update Total Task Time based on new Component Qty value?', ABOVE, LEFT)"
                        onmouseout="return nd()" onclick="multcomp();" alt="" src="../images/appbuttons/minibuttons/pdown.gif"
                        width="18" height="18" runat="server" class="details">
                </td>
                <td class="plainlabel" width="212">
                    <input id="cbpush" onclick="checkpush();" type="checkbox" runat="server"><asp:Label
                        ID="lang1018" runat="server">Copy to Component Library (Site Only)</asp:Label>
                </td>
            </tr>
        </table>
        <table id="tbeq2" cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="372">
                        <tr>
                            <td style="width: 20px">
                            </td>
                            <td style="width: 150px">
                            </td>
                            <td width="25">
                            </td>
                            <td width="5">
                            </td>
                            <td style="width: 150px">
                            </td>
                            <td width="22">
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowgray" align="center" colspan="3">
                                <asp:Label ID="lang1019" runat="server">Original Failure Modes</asp:Label>
                            </td>
                            <td class="tbg">
                                &nbsp;
                            </td>
                            <td class="bluelabel transrowdrk" align="center" colspan="2">
                                <asp:Label ID="lang1020" runat="server">Revised Failure Modes</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowgray">
                                &nbsp;
                            </td>
                            <td class="transrowgray" align="center">
                                <asp:ListBox ID="lbofailmodes" runat="server" CssClass="plainlabel" style="width: 150px"
                                    SelectionMode="Multiple" Height="50px" ForeColor="Blue"></asp:ListBox>
                            </td>
                            <td class="transrowgray" valign="middle" align="center">
                                <img id="todis1" class="details" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                                    style="width: 20px" height="20" runat="server">
                                <img id="fromdis1" class="details" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                    style="width: 20px" height="20" runat="server">
                                <asp:ImageButton ID="ibfromo" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardbg.gif">
                                </asp:ImageButton><br>
                                <asp:ImageButton ID="ibtoo" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbgdrk.gif">
                                </asp:ImageButton>
                            </td>
                            <td class="tbg">
                                &nbsp;
                            </td>
                            <td class="transrowdrk" align="center">
                                <asp:ListBox ID="lbCompFM" runat="server" CssClass="plainlabel" style="width: 150px" SelectionMode="Multiple"
                                    Height="50px"></asp:ListBox>
                            </td>
                            <td class="transrowdrk" valign="middle" align="center">
                                <img id="btnaddnewfail" onmouseover="return overlib('Add a New Failure Mode to the Component Selected Above')"
                                    onmouseout="return nd()" onclick="GetFailDiv();" alt="" src="../images/appbuttons/minibuttons/addmod.gif"
                                    runat="server"><br>
                                <asp:ImageButton ID="ibReuse" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardblu.gif">
                                </asp:ImageButton><img id="fromreusedis" class="details" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                    style="width: 20px" height="20" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table cellspacing="0" cellpadding="0" width="370">
                        <tr>
                            <td style="width: 150px">
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td style="width: 150px">
                            </td>
                            <td style="width: 50px">
                            </td>
                        </tr>
                        <tr>
                            <td class="redlabel transrowdrk" bgcolor="#bad8fc" align="center">
                                <asp:Label ID="lang1021" runat="server">Not Addressed</asp:Label>
                            </td>
                            <td class="transrowdrk">
                                &nbsp;
                            </td>
                            <td class="bluelabel transrowdrk" bgcolor="#bad8fc" align="center">
                                <asp:Label ID="lang1022" runat="server">Selected</asp:Label>
                            </td>
                            <td class="transrowdrk">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowdrk" align="center">
                                <asp:ListBox ID="lbfaillist" runat="server" CssClass="plainlabel" style="width: 150px" SelectionMode="Multiple"
                                    Height="50px" ForeColor="Red"></asp:ListBox>
                            </td>
                            <td class="transrowdrk" valign="middle" align="center">
                                <img id="todis" class="details" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                                    style="width: 20px" height="20" runat="server">
                                <img id="fromdis" class="details" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                    style="width: 20px" height="20" runat="server">
                                <asp:ImageButton ID="ibToTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardblu.gif">
                                </asp:ImageButton><br>
                                <asp:ImageButton ID="ibFromTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/backblu.gif">
                                </asp:ImageButton>
                            </td>
                            <td class="transrowdrk" align="center">
                                <asp:ListBox ID="lbfailmodes" runat="server" CssClass="plainlabel" style="width: 150px"
                                    SelectionMode="Multiple" Height="50px" ForeColor="Blue"></asp:ListBox>
                            </td>
                            <td class="transrowdrk">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td colspan="2">
                    <img src="../images/appbuttons/minibuttons/2PX.gif" height="2">
                </td>
            </tr>
            <tr>
                <td class="thdrsinglft" style="width: 26px" align="left">
                    <img border="0" src="../images/appbuttons/minibuttons/optbg.gif">
                </td>
                <td class="thdrsingrt label" onmouseover="return overlib('What are you going to do?')"
                    onmouseout="return nd()" width="714" colspan="3">
                    <asp:Label ID="lang1023" runat="server">Task Activity Details</asp:Label>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="2" width="372">
                        <tr>
                            <td width="40">
                            </td>
                            <td width="145">
                            </td>
                            <td width="40">
                            </td>
                            <td width="145">
                            </td>
                            <td width="2">
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowgray" colspan="4">
                                <asp:Label ID="lang1024" runat="server">Original Task Description</asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowgray" colspan="4">
                                <textarea style="width: 296px; font-family: Arial; height: 40px; font-size: 12px"
                                    id="txtodesc" onkeyup="copydesc(this.value);" onchange="copydesc(this.value);"
                                    cols="34" name="txtdesc" runat="server"></textarea>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowgray">
                                <asp:Label ID="lang1025" runat="server">Type</asp:Label>
                            </td>
                            <td class="transrowgray">
                                <asp:DropDownList ID="ddtypeo" runat="server" CssClass="plainlabel" Width="140px"
                                    Rows="1" DataTextField="tasktype" DataValueField="ttid">
                                </asp:DropDownList>
                            </td>
                            <td class="label transrowgray">
                                PdM
                            </td>
                            <td class="transrowgray">
                                <asp:DropDownList ID="ddpto" runat="server" CssClass="plainlabel" Width="140px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <img src="../images/appbuttons/minibuttons/2PX.gif">
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table cellspacing="0" cellpadding="2" width="370" border="0">
                        <tr>
                            <td width="40">
                            </td>
                            <td style="width: 140px">
                            </td>
                            <td width="40">
                            </td>
                            <td style="width: 90px">
                            </td>
                            <td style="width: 50px">
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowdrk" colspan="5">
                                <asp:Label ID="lang1026" runat="server">Revised Task Description</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowdrk" colspan="4">
                                <textarea style="width: 290px; font-family: Arial; height: 40px; font-size: 12px"
                                    id="txtdesc" class="plainlabel" cols="32" name="txtdesc" runat="server"></textarea>
                            </td>
                            <td class="transrowdrk" bgcolor="#bad8fc">
                                <img id="btnlookup" class="details" onclick="tasklookup();" src="../images/appbuttons/minibuttons/lookupblu.gif"
                                    runat="server">
                                <img id="sgrid1" onmouseover="return overlib('Add/Edit Sub Tasks')" onmouseout="return nd()"
                                    onclick="getsgrid();" alt="" src="../images/appbuttons/minibuttons/sgrid1.gif"
                                    style="width: 20px" height="20">
                                <img id="btnlookup2" onclick="tasklookup();" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    runat="server">
                                <br>
                                <img onmouseover="return overlib('Generate Work Order for This Task')" onmouseout="return nd()"
                                    onclick="getfail2();" alt="" src="../images/appbuttons/minibuttons/addmod.gif">
                                <img onmouseover="return overlib('Add/Edit Measurements for this Task')" onmouseout="return nd()"
                                    onclick="getMeasDiv();" alt="" src="../images/appbuttons/minibuttons/measblu.gif"
                                    width="27" height="20">
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowdrk">
                                <asp:Label ID="lang1027" runat="server">Type</asp:Label>
                            </td>
                            <td class="transrowdrk">
                                <asp:DropDownList ID="ddtype" runat="server" CssClass="plainlabel" Width="140px"
                                    Rows="1" DataTextField="tasktype" DataValueField="ttid">
                                </asp:DropDownList>
                            </td>
                            <td class="label transrowdrk">
                                PdM
                            </td>
                            <td class="transrowdrk" colspan="2">
                                <asp:DropDownList ID="ddpt" runat="server" CssClass="plainlabel" Width="140px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="2" width="742">
            <tr>
                <td>
                    <img src="../images/appbuttons/minibuttons/2PX.gif" width="2" height="2">
                </td>
            </tr>
            <tr>
                <td class="thdrsinglft" style="width: 26px" align="left">
                    <img border="0" src="../images/appbuttons/minibuttons/optbg.gif">
                </td>
                <td class="thdrsingrt label" onmouseover="return overlib('How are you going to do it?')"
                    onmouseout="return nd()" width="716" colspan="3">
                    <asp:Label ID="lang1028" runat="server">Planning &amp; Scheduling Details</asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblrte" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td valign="top">
                    <table cellspacing="0" cellpadding="1" width="372" border="0">
                        <tr>
                            <td style="width: 60px">
                            </td>
                            <td width="10">
                            </td>
                            <td style="width: 50px">
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td width="30">
                            </td>
                            <td width="70">
                            </td>
                            <td width="40">
                            </td>
                            <td width="25">
                            </td>
                            <td width="45">
                            </td>
                            <td width="2">
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowgray" bgcolor="#edf0f3" colspan="5">
                                <asp:Label ID="lang1029" runat="server">Original Skill</asp:Label>
                            </td>
                            <td class="label transrowgray" bgcolor="#edf0f3">
                                Qty
                            </td>
                            <td class="label transrowgray" bgcolor="#edf0f3" colspan="4">
                                <asp:Label ID="lang1030" runat="server">Min Ea</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowgray" colspan="5">
                                <asp:DropDownList ID="ddskillo" runat="server" CssClass="plainlabel" Width="144px">
                                </asp:DropDownList>
                            </td>
                            <td class="transrowgray">
                                <asp:TextBox ID="txtqtyo" runat="server" CssClass="plainlabel" Width="24PX"></asp:TextBox>
                            </td>
                            <td class="transrowgray" colspan="4">
                                <asp:TextBox ID="txttro" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowgray">
                                <asp:Label ID="lang1031" runat="server">Frequency</asp:Label>
                            </td>
                            <td class="transrowgray" colspan="2">
                                <asp:TextBox ID="txtfreqo" runat="server" CssClass="plainlabel" Width="55px"></asp:TextBox>
                            </td>
                            <td class="transrowgray">
                                <input id="cbfixedo" runat="server" type="checkbox" onmouseover="return overlib('Check if Frequency is Fixed (Calendar Frequency Only)')"
                                    onmouseout="return nd()" />
                            </td>
                            <td class="transrowgray">
                                <img onclick="getmetero();" onmouseover="return overlib('View, Add, or Edit Original Meter Frequency for this Task', ABOVE, LEFT)"
                                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/meter1.gif">
                            </td>
                            <td class="label transrowgray">
                                <asp:Label ID="lang1032" runat="server">Status</asp:Label>
                            </td>
                            <td class="transrowgray" colspan="2">
                                <asp:DropDownList ID="ddeqstato" runat="server" CssClass="plainlabel" Width="80px">
                                </asp:DropDownList>
                            </td>
                            <td class="label transrowgray">
                                DT
                            </td>
                            <td class="transrowgray">
                                <asp:TextBox ID="txtordt" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabel" colspan="3">
                                <asp:Label ID="lang1033" runat="server">Task Status</asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="ddtaskstat" runat="server" CssClass="plainlabel" AutoPostBack="False">
                                    <asp:ListItem Value="Select">Select</asp:ListItem>
                                    <asp:ListItem Value="UnChanged">UnChanged</asp:ListItem>
                                    <asp:ListItem Value="Add">Add</asp:ListItem>
                                    <asp:ListItem Value="Revised">Revised</asp:ListItem>
                                    <asp:ListItem Value="Delete">Delete</asp:ListItem>
                                    <asp:ListItem Value="Detailed Steps">Detailed Steps</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td id="tdtpmhold" class="plainlabelred" colspan="5" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table border="0" cellspacing="0" cellpadding="1" width="370">
                        <tr>
                            <td style="width: 60px">
                            </td>
                            <td style="width: 60px">
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td width="30">
                            </td>
                            <td width="70">
                            </td>
                            <td width="40">
                            </td>
                            <td width="25">
                            </td>
                            <td width="45">
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowdrk" colspan="4">
                                <asp:Label ID="lang1034" runat="server">Revised Skill</asp:Label>
                            </td>
                            <td class="label transrowdrk">
                                Qty
                            </td>
                            <td class="label transrowdrk">
                                <asp:Label ID="lang1035" runat="server">Min Ea</asp:Label>
                            </td>
                            <td class="label transrowdrk" colspan="3">
                                <asp:Label ID="lang1036" runat="server">P-F Interval</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowdrk" colspan="4">
                                <asp:DropDownList ID="ddskill" runat="server" CssClass="plainlabel" Width="140px">
                                </asp:DropDownList>
                            </td>
                            <td class="transrowdrk">
                                <asp:TextBox ID="txtqty" runat="server" CssClass="plainlabel" Width="30px"></asp:TextBox>
                            </td>
                            <td class="transrowdrk">
                                <asp:TextBox ID="txttr" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                                <a href="#" onclick="multcomp();" class="details" onmouseover="return overlib('Multiply Skill Qty by Component Qty - Save Required', ABOVE, LEFT)"
                                    onmouseout="return nd()">X</a>
                            </td>
                            <td class="transrowdrk">
                                <asp:TextBox ID="txtpfint" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                            </td>
                            <td class="transrowdrk" colspan="2">
                                <img id="btnpfint" onmouseover="return overlib('Estimate Frequency using the PF Interval')"
                                    onmouseout="return nd()" onclick="getPFDiv();" alt="" src="../images/appbuttons/minibuttons/lilcalcblu.gif"
                                    style="width: 20px" height="20">
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowdrk">
                                <asp:Label ID="lang1037" runat="server">Frequency</asp:Label>
                            </td>
                            <td class="transrowdrk">
                                <asp:TextBox ID="txtfreq" runat="server" CssClass="plainlabel" Width="55px"></asp:TextBox>
                            </td>
                            <td class="transrowdrk">
                                <input id="cbfixed" runat="server" type="checkbox" onmouseover="return overlib('Check if Frequency is Fixed (Calendar Frequency Only)')"
                                    onmouseout="return nd()" />
                            </td>
                            <td class="transrowdrk">
                                <img onclick="getmeter();" onmouseover="return overlib('View, Add, or Edit Meter Frequency for this Task', ABOVE, LEFT)"
                                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/meter1.gif">
                            </td>
                            <td class="label transrowdrk">
                                <asp:Label ID="lang1038" runat="server">Status</asp:Label>
                            </td>
                            <td class="transrowdrk" colspan="2">
                                <asp:DropDownList ID="ddeqstat" runat="server" CssClass="plainlabel" Width="80px">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 19px" class="label transrowdrk">
                                DT
                            </td>
                            <td class="transrowdrk">
                                <asp:TextBox ID="txtrdt" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelsm" colspan="6">
                                <asp:CheckBox ID="cbloto" runat="server"></asp:CheckBox><asp:Label ID="lang1039"
                                    runat="server">LOTO</asp:Label><asp:CheckBox ID="cbcs" runat="server"></asp:CheckBox>CS
                                    <asp:CheckBox ID="cbmr" runat="server"></asp:CheckBox>Meter Ready
                            </td>
                            <td class="bluelabel" colspan="5" align="right">
                                <img onmouseover="return overlib('Choose Equipment Spare Parts for this Task')" onmouseout="return nd()"
                                    onclick="GetSpareDiv();" src="../images/appbuttons/minibuttons/spareparts.gif" class="details"><img
                                        id="Img3" onmouseover="return overlib('Add/Edit Parts for this Task')" onmouseout="return nd()"
                                        onclick="GetPartDiv();" alt="" src="../images/appbuttons/minibuttons/parttrans.gif"
                                        width="23" height="19" runat="server"><img onmouseover="return overlib('Add/Edit Tools for this Task')"
                                            onmouseout="return nd()" onclick="GetToolDiv();" alt="" src="../images/appbuttons/minibuttons/tooltrans.gif"
                                            width="23" height="19"><img onmouseover="return overlib('Add/Edit Lubricants for this Task')"
                                                onmouseout="return nd()" onclick="GetLubeDiv();" alt="" src="../images/appbuttons/minibuttons/lubetrans.gif"
                                                width="23" height="19"><img onmouseover="return overlib('Add/Edit Notes for this Task')"
                                                    onmouseout="return nd()" onclick="GetNoteDiv();" alt="" src="../images/appbuttons/minibuttons/notessm.gif"
                                                    width="25" height="20">
                                <img id="Img5" onmouseover="return overlib('Edit Pass/Fail Adjustment Levels - This Task', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="editadj('0');" border="0" alt="" src="../images/appbuttons/minibuttons/screwd.gif"
                                    runat="server" class="details"><img class="details" onmouseover="return overlib('View or Edit Images for this Task')"
                                        onmouseout="return nd()" onclick="getti();" alt="" src="../images/appbuttons/minibuttons/pic.gif"
                                        style="width: 20px" height="20">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table border="0" cellspacing="0" cellpadding="0" width="742">
            <tr id="Tr1" runat="server" name="Tr1">
                <td style="border-top: #7ba4e0 thin solid" align="center">
                    <img id="btnStart" src="../images/appbuttons/minibuttons/tostartbg.gif" width="18"
                        height="18" runat="server"><img id="btnPrev" src="../images/appbuttons/minibuttons/prevarrowbg.gif"
                            width="18" height="18" runat="server">
                </td>
                <td style="border-top: #7ba4e0 thin solid" align="center">
                    <asp:Label ID="Label22" runat="server" CssClass="bluelabel" ForeColor="Blue" Font-Bold="True"
                        Font-Names="Arial" Font-Size="X-Small">Task# </asp:Label><asp:Label ID="lblpg" runat="server"
                            CssClass="bluelabel" ForeColor="Blue" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"></asp:Label><asp:Label
                                ID="Label23" runat="server" CssClass="bluelabel" ForeColor="Blue" Font-Bold="True"
                                Font-Names="Arial" Font-Size="X-Small"> of </asp:Label><asp:Label ID="lblcnt" runat="server"
                                    CssClass="bluelabel" ForeColor="Blue" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"></asp:Label>
                </td>
                <td style="border-top: #7ba4e0 thin solid" align="center">
                    <img id="btnNext" src="../images/appbuttons/minibuttons/nextarrowbg.gif" width="18"
                        height="18" runat="server"><img id="btnEnd" src="../images/appbuttons/minibuttons/tolastbg.gif"
                            width="18" height="18" runat="server">
                </td>
                <td style="border-top: #7ba4e0 thin solid" class="label">
                    <asp:Label ID="Label2" runat="server" CssClass="plainlabel" ForeColor="Black" Font-Bold="True"
                        Font-Names="Arial" Font-Size="X-Small">Task Order </asp:Label>
                </td>
                <td style="border-top: #7ba4e0 thin solid">
                    <asp:TextBox ID="txttaskorder" runat="server" CssClass="plainlabel" Width="35px"
                        Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"></asp:TextBox>
                </td>
                <td style="border-top: #7ba4e0 thin solid" align="center">
                    <asp:Label ID="Label26" runat="server" CssClass="greenlabel" Font-Bold="True" Font-Names="Arial"
                        Font-Size="X-Small">Sub Tasks: </asp:Label><asp:Label ID="lblsubcount" runat="server"
                            CssClass="greenlabel" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"></asp:Label><asp:Label
                                ID="lblspg" runat="server" CssClass="details" ForeColor="Green" Font-Bold="True"
                                Font-Names="Arial" Font-Size="X-Small"></asp:Label><asp:Label ID="Label27" runat="server"
                                    CssClass="details" ForeColor="Green" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"> of </asp:Label><asp:Label
                                        ID="lblscnt" runat="server" CssClass="details" ForeColor="Green" Font-Bold="True"
                                        Font-Names="Arial" Font-Size="X-Small"></asp:Label>
                    &nbsp;&nbsp;
                </td>
                <td style="border-top: #7ba4e0 thin solid" class="labeld">
                    <asp:Label ID="Label1" runat="server" CssClass="label" ForeColor="Black" Font-Bold="True"
                        Font-Names="Arial" Font-Size="X-Small">Assessment Code</asp:Label>
                </td>
                <td style="border-top: #7ba4e0 thin solid">
                    <asp:DropDownList ID="ddascd" runat="server" CssClass="plainlabel" style="width: 40px" ForeColor="Black"
                        Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">
                        <asp:ListItem Value="0">0</asp:ListItem>
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="border-top: #7ba4e0 thin solid" align="right">
                    &nbsp;<img onclick="GetNavGrid();" alt="" src="../images/appbuttons/bgbuttons/navgrid.gif"
                        style="width: 20px" height="20"><img id="btnedittask" onclick="doEnable();" border="0" alt=""
                            src="../images/appbuttons/minibuttons/lilpentrans.gif" width="19" height="19"
                            runat="server"><asp:ImageButton ID="btnaddtsk" runat="server" ImageUrl="../images/appbuttons/minibuttons/addmod.gif">
                            </asp:ImageButton>
                    <asp:ImageButton ID="btntpm" runat="server" ImageUrl="../images/appbuttons/minibuttons/compresstpm.gif">
                    </asp:ImageButton><img id="imgdeltpm" class="details" onmouseover="return overlib('Delete This Task From the TPM Module and Restore Editing', ABOVE, LEFT)"
                        onmouseout="return nd()" onclick="checkdeltasktpm();" border="0" alt="" src="../images/appbuttons/minibuttons/cantpm.gif"
                        runat="server">
                    <asp:ImageButton ID="btnaddsubtask" runat="server" CssClass="details" ImageUrl="../images/appbuttons/minibuttons/subtask.gif">
                    </asp:ImageButton><asp:ImageButton ID="ibCancel" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif">
                    </asp:ImageButton><asp:ImageButton ID="btnsavetask" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                        Visible="False"></asp:ImageButton><img id="btnsav" onclick="valpgnums();" alt=""
                            src="../images/appbuttons/minibuttons/savedisk1.gif" style="width: 20px" height="20" runat="server"><img
                                id="imgrat" onmouseover="return overlib('Add/Edit Rationale for Task Changes')"
                                onmouseout="return nd()" onclick="GetRationale();" alt="" src="../images/appbuttons/minibuttons/rationale.gif"
                                style="width: 20px" height="20" runat="server"><img onmouseover="return overlib('View Reports')"
                                    onmouseout="return nd()" onclick="getValueAnalysis();" alt="" src="../images/appbuttons/minibuttons/report.gif"><img
                                        id="ggrid" onmouseover="return overlib('Review Changes in Grid View')" onmouseout="return nd()"
                                        onclick="GetGrid();" alt="" src="../images/appbuttons/minibuttons/grid1.gif"
                                        style="width: 20px" height="20" runat="server"><img class="details" onmouseover="return overlib('Go to the Centralized PM Library', LEFT, WIDTH, 270)"
                                            onmouseout="return nd()" onclick="GoToPMLib();" alt="" src="../images/appbuttons/minibuttons/gotolibsm.gif"
                                            width="23" height="20"><img id="sgrid" onmouseover="return overlib('Add/Edit Sub Tasks')"
                                                onmouseout="return nd()" onclick="getsgrid();" alt="" src="../images/appbuttons/minibuttons/sgrid1.gif"
                                                style="width: 20px" height="20">
                    <img id="Img4" onmouseover="return overlib('Edit Pass/Fail Adjustment Levels - All Tasks', ABOVE, LEFT)"
                        onmouseout="return nd()" onclick="editadj('pre');" border="0" alt="" src="../images/appbuttons/minibuttons/screwall.gif"
                        runat="server" class="details"><img id="imgdeltask" onmouseover="return overlib('Delete This Task', ABOVE, LEFT)"
                            onmouseout="return nd()" onclick="checkdeltask();" border="0" alt="" src="../images/appbuttons/minibuttons/del.gif"
                            runat="server">
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblsvchk" type="hidden" runat="server">
    <input id="lblcompchk" type="hidden" name="lblcompchk" runat="server">
    <input id="lblsb" type="hidden" name="lblsb" runat="server">
    <input id="lblfilt" type="hidden" name="lblfilt" runat="server">
    <input id="lblenable" type="hidden" name="lblenable" runat="server">
    <input id="lblpgholder" type="hidden" name="lblpgholder" runat="server">
    <input id="lblcompfailchk" type="hidden" name="lblcompfailchk" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblstart" type="hidden" name="lblpmtype" runat="server">
    <input id="lblt" type="hidden" name="lblt" runat="server">
    <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
    <input id="lblst" type="hidden" name="lblst" runat="server">
    <input id="lblco" type="hidden" name="lblco" runat="server">
    <input id="lblpar" type="hidden" name="lblpar" runat="server">
    <input id="lbltabid" type="hidden" name="lbltabid" runat="server">
    <input id="lblpmstr" type="hidden" name="lblpmstr" runat="server">
    <input id="pgflag" type="hidden" name="pgflag" runat="server">
    <input id="lblpmid" type="hidden" name="lblpmid" runat="server">
    <input id="lbldid" type="hidden" runat="server">
    <input id="lblclid" type="hidden" runat="server"><input id="lblsid" type="hidden"
        runat="server">
    <input id="lbldel" type="hidden" runat="server"><input id="lblsave" type="hidden"
        runat="server">
    <input id="lblgrid" type="hidden" runat="server"><input id="lblcind" type="hidden"
        runat="server">
    <input id="lblcurrsb" type="hidden" runat="server"><input id="lblcurrcs" type="hidden"
        runat="server">
    <input id="lbldocpmid" type="hidden" runat="server"><input id="lbldocpmstr" type="hidden"
        runat="server">
    <input id="lblsesscnt" type="hidden" runat="server"><input id="appchk" type="hidden"
        name="appchk" runat="server">
    <input id="lbllog" type="hidden" name="lbllog" runat="server"><input id="lblfiltcnt"
        type="hidden" runat="server">
    <input id="lblusername" type="hidden" runat="server">
    <input id="lbllock" type="hidden" name="lbllock" runat="server">
    <input id="lbllockedby" type="hidden" name="lbllockedby" runat="server">
    <input id="lblhaspm" type="hidden" runat="server">
    <input id="lbltyp" type="hidden" runat="server">
    <input id="lbllid" type="hidden" runat="server">
    <input id="lblnoeq" type="hidden" runat="server">
    <input id="lblro" type="hidden" runat="server">
    <input id="lbltpmalert" type="hidden" name="lbltpmalert" runat="server"><input id="lbltpmhold"
        type="hidden" runat="server">
    <input id="lblusetot" type="hidden" runat="server">
    <input id="lblnewkey" type="hidden" runat="server">
    <input id="lblnewcomp" type="hidden" runat="server">
    <input id="lblnewdesc" type="hidden" runat="server">
    <input id="lblfslang" type="hidden" runat="server">
    <input type="hidden" id="lblskillid" runat="server" />
    <input type="hidden" id="lblskill" runat="server" />
    <input type="hidden" id="lblskillqty" runat="server" />
    <input type="hidden" id="lblmeterid" runat="server" />
    <input type="hidden" id="lblrd" runat="server" />
    <input type="hidden" id="lblrdid" runat="server" />
    <input type="hidden" id="lblfunc" runat="server" />
    <input type="hidden" id="lblusemeter" runat="server" />
    <input type="hidden" id="lblusemetero" runat="server" />
    <input type="hidden" id="lblskillido" runat="server" />
    <input type="hidden" id="lblskillo" runat="server" />
    <input type="hidden" id="lblskillqtyo" runat="server" />
    <input type="hidden" id="lblmeterido" runat="server" />
    <input type="hidden" id="lblrdo" runat="server" />
    <input type="hidden" id="lblrdido" runat="server" />
    <input type="hidden" id="lblmfid" runat="server" />
    <input type="hidden" id="lblmfido" runat="server" />
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblfreq" runat="server" />
    <input type="hidden" id="lblfreqo" runat="server" />
    <input type="hidden" id="lblfixed" runat="server" />
    <input type="hidden" id="lblfixedo" runat="server" />
    <input type="hidden" id="lbltpmpmtskid" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblghostoff" runat="server" />
    <input type="hidden" id="lblxstatus" runat="server" />
    <input type="hidden" id="lblrteid" runat="server" />
    <input type="hidden" id="lblrbskillid" runat="server" />
    <input type="hidden" id="lblrbqty" runat="server" />
    <input type="hidden" id="lblrbfreq" runat="server" />
    <input type="hidden" id="lblrbrdid" runat="server" />
    <input type="hidden" id="lblcompchng" runat="server" />
    <input type="hidden" id="lblcomi" runat="server" />
    </form>
</body>
</html>
