

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMOptTasksGrid
    Inherits System.Web.UI.Page
	Protected WithEvents lang1046 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1045 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1044 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1043 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1042 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1041 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim ds As DataSet
    Dim tasksg As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim start, tl, cid, sid, did, clid, eqid, chk, fuid, Val, field, name, Filter, coid, typ, lid, task, optsort As String
    Dim compnum, who As String
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdaddtask As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcurrsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdsort As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcurrcompsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdcomp As System.Web.UI.WebControls.Label
    Protected WithEvents imgaddcomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcompnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rptrtasks As System.Web.UI.WebControls.Repeater
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents taskcnt As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here

        If Not IsPostBack Then
            Try
                who = Request.QueryString("who").ToString
                lblwho.Value = who
            Catch ex As Exception
                lblwho.Value = "opt"
            End Try
            lblcurrsort.Value = "tasknum"
            start = Request.QueryString("start").ToString
            taskcnt.Value = "0"
            If start = "yes" Then
                tl = Request.QueryString("tl").ToString
                lbltasklev.Value = tl
                cid = Request.QueryString("cid").ToString
                lblcid.Value = cid
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                did = Request.QueryString("did").ToString
                lbldid.Value = did
                clid = Request.QueryString("clid").ToString
                lblclid.Value = clid
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                chk = Request.QueryString("chk").ToString
                lblchk.Value = chk
                fuid = Request.QueryString("fuid").ToString
                lblfuid.Value = fuid
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                lid = Request.QueryString("lid").ToString
                lbllid.Value = lid
                Try
                    coid = Request.QueryString("comid").ToString
                Catch ex As Exception
                    coid = ""
                End Try
                Try
                    task = Request.QueryString("task").ToString
                Catch ex As Exception
                    task = ""
                End Try
                Try
                    optsort = HttpContext.Current.Session("optsort").ToString
                    lblcurrsort.Value = optsort
                Catch ex As Exception
                    lblcurrsort.Value = "tasknum"
                End Try
                lblcoid.Value = coid
                lblfilt.Value = Filter
                If fuid <> "0" Then
                    tasksg.Open()
                    If task <> "" Then
                        GoToTPMTask(coid, task)
                        tasksg.Dispose()
                    ElseIf coid <> "" AndAlso coid <> "0" Then
                        GoToTask(coid)
                        tasksg.Dispose()
                    Else
                        LoadPage(fuid)
                        tdaddtask.Attributes.Add("class", "visible")
                    End If
                Else
                    tdaddtask.Attributes.Add("class", "details")
                    fuid = "na"
                    tasksg.Open()
                    LoadPage(fuid)
                    tasksg.Dispose()
                End If

            Else
                tdaddtask.Attributes.Add("class", "details")
                fuid = "na"
                tasksg.Open()
                LoadPage(fuid)
                tasksg.Dispose()
            End If
        Else
            If Request.Form("lblsubmit") = "addnew" Then
                tasksg.Open()
                AddTask()
                tasksg.Dispose()
            ElseIf Request.Form("lblsubmit") = "addnewcomp" Then
                tasksg.Open()
                compnum = lblcompnum.Value
                coid = lblcoid.Value
                AddTask2(coid, compnum)
                tasksg.Dispose()
            End If
        End If
    End Sub
    Private Sub AddTask()
        If Len(lblfuid.Value) <> 0 Then

            tl = lbltasklev.Value
            cid = lblcid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            eqid = lbleqid.Value
            fuid = lblfuid.Value

            tl = 5
            Dim newtask As Integer
            Dim usr As String = HttpContext.Current.Session("username").ToString()
            Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
            sql = "usp_AddTask '" & tl & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & ustr & "'"
            newtask = tasksg.Scalar(sql)
            sql = "select tasknum from pmtasks where pmtskid = '" & newtask & "'"
            newtask = tasksg.Scalar(sql)
            GoToTPMTask("", newtask)

        End If
    End Sub
    Private Sub AddTask2(ByVal coid As String, ByVal compnum As String)
        If Len(lblfuid.Value) <> 0 And coid <> "" And coid <> "0" Then

            tl = lbltasklev.Value
            cid = lblcid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            eqid = lbleqid.Value
            fuid = lblfuid.Value

            tl = 5
            Dim newid, newtask As Integer
            Dim usr As String = HttpContext.Current.Session("username").ToString()
            Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
            sql = "usp_AddTask '" & tl & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & ustr & "'"
            newid = tasksg.Scalar(sql)
            sql = "sp_insertComp '" & coid & "', '" & compnum & "', '" & newid & "', '" & fuid & "'"
            tasksg.Update(sql)
            sql = "select tasknum from pmtasks where pmtskid = '" & newid & "'"
            newtask = tasksg.Scalar(sql)
            GoToTPMTask(coid, newtask)

        End If
    End Sub
    Private Sub GoToTPMTask(ByVal coid As String, ByVal tasknum As String)
        tl = lbltasklev.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        chk = lblchk.Value
        fuid = lblfuid.Value
        typ = lbltyp.Value
        lid = lbllid.Value
        who = lblwho.Value
        'Dim strMessage As String = "PMOptTasksGrid"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Response.Redirect("PMOptTasks.aspx?who=" & who & "&start=yes&tl=5&chk=" & chk & "&cid=" & _
        cid + "&fuid=" & fuid & "&comid=" & coid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & _
        "&eqid=" & eqid & "&task=" & tasknum & "&typ=" & typ & "&lid=" & lid)
    End Sub
    Private Sub GoToTask(ByVal coid As String)
        Dim tn As Integer
        Dim tasknum As String
        tl = lbltasklev.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        chk = lblchk.Value
        fuid = lblfuid.Value
        typ = lbltyp.Value
        lid = lbllid.Value
        who = lblwho.Value
        sql = "select tasknum from pmtasks where funcid = '" & fuid & "' and comid = '" & coid & "'"
        dr = tasksg.GetRdrData(sql)
        While dr.Read
            tasknum = dr.Item("tasknum").ToString
        End While
        dr.Close()
        'tasksg.Dispose()
        If tasknum <> "" Then
            'Response.Redirect("PMOptTasks.aspx?start=yes&tl=5&chk=" & chk & "&cid=" & _
            'cid + "&fuid=" & fuid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & _
            '"&eqid=" & eqid & "&task=" & tasknum & "&comid=" & coid)
            'Dim strMessage As String = "PMOptTasksGrid 2"
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Response.Redirect("PMOptTasks.aspx?who=" & who & "&start=yes&tl=5&chk=" & chk & "&cid=" & _
           cid + "&fuid=" & fuid & "&comid=" & coid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & _
           "&eqid=" & eqid & "&task=" & tasknum & "&typ=" & typ & "&lid=" & lid)
        Else
            'Response.Redirect("PMOptTasks.aspx?start=no")
            sql = "select compnum + ' - ' + isnull(compdesc, '') as compnum " _
            + "from components where comid = '" & coid & "'"
            Dim cnum As String
            cnum = tasksg.strScalar(sql)
            lblcompnum.Value = cnum
            lbladdcomp.Visible = True
            imgaddcomp.Attributes.Add("class", "view")
            lbladdcomp.Text = "Add Task to Component: " & cnum
            LoadPage(fuid)
        End If


    End Sub
    Private Sub LoadPage(ByVal fuid As String)
        Dim cnt As Integer
        Dim sqlcnt As String
        If fuid = "na" Then
            cnt = 0
            sql = "select *, 'Orig' as 'o', 'Rev' as 'r' from pmtasks where pmtskid = 0"
        Else
            cnt = 1
            'sql = "select t.pmtskid, t.tasknum, t.taskdesc, " _
            '+ "isnull(c.compnum + ' - ' + c.compdesc, 'None Selected') as compnum " _
            '+ "from pmtasks t left outer join components c on c.comid = t.comid " _
            '+ "where subtask = 0 and t.funcid = '" & fuid & "' " _
            '+ "order by tasknum"
            Dim sort As String = lblcurrsort.Value
            If sort = "tasknum" Then
                tdsort.InnerHtml = "Task# Ascending"
                sql = "select t.pmtskid, t.tasknum, isnull(t.otaskdesc, 'Not Provided') as 'otaskdesc', " _
                + "isnull(t.taskdesc, 'Not Provided') as 'taskdesc', " _
                + "'Orig' as 'o', 'Rev' as 'r', isnull(c.compnum, 'None Selected') + ' - ' + isnull(c.compdesc, '') as compnum " _
                + "from pmtasks t left outer join components c on c.comid = t.comid where subtask = 0 and t.funcid = '" & fuid & "' " _
                + "order by t.tasknum"
            ElseIf sort = "compnum" Then
                tdsort.InnerHtml = "Component# Ascending"
                sql = "select t.pmtskid, t.tasknum, isnull(t.otaskdesc, 'Not Provided') as 'otaskdesc', " _
                + "isnull(t.taskdesc, 'Not Provided') as 'taskdesc', " _
                + "'Orig' as 'o', 'Rev' as 'r', isnull(c.compnum, 'None Selected') + ' - ' + isnull(c.compdesc, '') as compnum " _
                + "from pmtasks t left outer join components c on c.comid = t.comid where subtask = 0 and t.funcid = '" & fuid & "' " _
                + "order by c.compnum, c.compdesc, t.tasknum"
            End If
            sqlcnt = "select count(*) from pmtasks where subtask = 0 and funcid = '" & fuid & "'"
        End If
        If cnt <> 0 Then
            cnt = tasksg.Scalar(sqlcnt)
            taskcnt.Value = cnt
        Else
            taskcnt.Value = "0"
        End If
        ds = tasksg.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        rptrtasks.DataSource = dt
        Dim i, eint As Integer
        eint = 8
        For i = cnt To eint
            dt.Rows.InsertAt(dt.NewRow(), i)
        Next
        rptrtasks.DataBind()
    End Sub

    Private Sub rptrtasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtasks.ItemDataBound
        'If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
        'Dim tasknum As String
        'Dim tasklink As HtmlAnchor
        'Dim tdo As HtmlTableCell
        ' If e.Item.ItemType = ListItemType.Item Then
        'tasklink = CType(e.Item.FindControl("lbltn"), HtmlAnchor)
        'tasknum = tasklink.InnerHtml
        ''tdo = CType(e.Item.FindControl("lblo"), HtmlTableCell)
        ''If tasknum <> "" Then
        ''tdo.InnerHtml = "Original"
        ''End If
        'ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
        'tasklink = CType(e.Item.FindControl("lbltnalt"), HtmlAnchor)
        'tasknum = tasklink.InnerHtml
        'End If
        'tasklink.Attributes("onclick") = "checktask(" & tasknum & ")"
        'End If

        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang1041 As Label
                lang1041 = CType(e.Item.FindControl("lang1041"), Label)
                lang1041.Text = axlabs.GetASPXPage("PMOptTasksGrid.aspx", "lang1041")
            Catch ex As Exception
            End Try
            Try
                Dim lang1042 As Label
                lang1042 = CType(e.Item.FindControl("lang1042"), Label)
                lang1042.Text = axlabs.GetASPXPage("PMOptTasksGrid.aspx", "lang1042")
            Catch ex As Exception
            End Try
            Try
                Dim lang1043 As Label
                lang1043 = CType(e.Item.FindControl("lang1043"), Label)
                lang1043.Text = axlabs.GetASPXPage("PMOptTasksGrid.aspx", "lang1043")
            Catch ex As Exception
            End Try
            Try
                Dim lang1044 As Label
                lang1044 = CType(e.Item.FindControl("lang1044"), Label)
                lang1044.Text = axlabs.GetASPXPage("PMOptTasksGrid.aspx", "lang1044")
            Catch ex As Exception
            End Try
            Try
                Dim lang1045 As Label
                lang1045 = CType(e.Item.FindControl("lang1045"), Label)
                lang1045.Text = axlabs.GetASPXPage("PMOptTasksGrid.aspx", "lang1045")
            Catch ex As Exception
            End Try
            Try
                Dim lang1046 As Label
                lang1046 = CType(e.Item.FindControl("lang1046"), Label)
                lang1046.Text = axlabs.GetASPXPage("PMOptTasksGrid.aspx", "lang1046")
            Catch ex As Exception
            End Try

        End If

    End Sub
    Public Sub SortTasks(ByVal sender As Object, ByVal e As System.EventArgs)
        lblcurrsort.Value = "tasknum"
        Session("optsort") = "tasknum"
        fuid = lblfuid.Value
        tasksg.Open()
        LoadPage(fuid)
        tasksg.Dispose()

    End Sub
    Public Sub SortCompTasks(ByVal sender As Object, ByVal e As System.EventArgs)
        lblcurrsort.Value = "compnum"
        Session("optsort") = "compnum"
        fuid = lblfuid.Value
        tasksg.Open()
        LoadPage(fuid)
        tasksg.Dispose()

    End Sub
    Private Sub rptrtasks_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrtasks.ItemCommand
        If e.CommandName = "Select" Then
            Dim tasknum, comid As String
            Try
                tasknum = CType(e.Item.FindControl("lbltn"), LinkButton).Text
            Catch ex As Exception
                tasknum = CType(e.Item.FindControl("lbltnalt"), LinkButton).Text
            End Try
            tl = lbltasklev.Value
            cid = lblcid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            eqid = lbleqid.Value
            chk = lblchk.Value
            fuid = lblfuid.Value
            coid = lblcoid.Value
            typ = lbltyp.Value
            lid = lbllid.Value
            who = lblwho.Value
            'Dim strMessage As String = "PMOptTasksGrid 3"
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Response.Redirect("PMOptTasks.aspx?who=" & who & "&start=yes&tl=5&chk=" & chk & "&cid=" & _
            cid + "&fuid=" & fuid & "&comid=" & coid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & _
            "&eqid=" & eqid & "&task=" & tasknum & "&typ=" & typ & "&lid=" & lid)

        End If
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1041.Text = axlabs.GetASPXPage("PMOptTasksGrid.aspx", "lang1041")
        Catch ex As Exception
        End Try
        Try
            lang1042.Text = axlabs.GetASPXPage("PMOptTasksGrid.aspx", "lang1042")
        Catch ex As Exception
        End Try
        Try
            lang1043.Text = axlabs.GetASPXPage("PMOptTasksGrid.aspx", "lang1043")
        Catch ex As Exception
        End Try
        Try
            lang1044.Text = axlabs.GetASPXPage("PMOptTasksGrid.aspx", "lang1044")
        Catch ex As Exception
        End Try
        Try
            lang1045.Text = axlabs.GetASPXPage("PMOptTasksGrid.aspx", "lang1045")
        Catch ex As Exception
        End Try
        Try
            lang1046.Text = axlabs.GetASPXPage("PMOptTasksGrid.aspx", "lang1046")
        Catch ex As Exception
        End Try

    End Sub

End Class
