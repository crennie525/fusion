<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OptDialog.aspx.vb" Inherits="lucy_r12.OptDialog" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>OptDialog</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script  type="text/javascript" src="../scripts1/OptDialogaspx.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script  type="text/javascript">
     <!--
        function handleentry() {
            pageScroll();
            eqid = document.getElementById("lbleqid").value;
            eqnum = document.getElementById("lbleqdesc").value;
            document.getElementById("ifaddproc").src = "upload.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&date=" + Date();
        }
        
        function pageScroll() {
            window.scrollTo(0, top);
            //scrolldelay = setTimeout('pageScroll()', 200); 
        } 
         //-->
    </script>
</head>
<body class="tbg" onload="handleentry();">
    <form id="form1" method="post" runat="server">
    <iframe id="ifaddproc" src="" width="100%" height="100%" frameborder="no"></iframe>
    <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
    <script type="text/javascript">
        document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script>
    <input id="lbleqid" type="hidden" runat="server" name="lbleqid">
    <input id="lbleqdesc" type="hidden" runat="server" name="lbleqdesc">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
