<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMOptTasksGrid.aspx.vb"
    Inherits="lucy_r12.PMOptTasksGrid" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>PMOptTasksGrid</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script  type="text/javascript" src="../scripts1/PMOptTasksGridaspx.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script  type="text/javascript">
        <!--
        function addtask2() {
            document.getElementById("lblsubmit").value = "addnewcomp";
            document.getElementById("form1").submit();
        }
        //-->
    </script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <div style="z-index: 100; position: absolute; height: 380px; overflow: auto; top: 0px;
        left: 0px">
        <table cellspacing="0" cellpadding="0" width="722">
            <tr>
                <td class="thdrsinglft" style="width: 26px" align="left">
                    <img border="0" src="../images/appbuttons/minibuttons/optbg.gif">
                </td>
                <td class="thdrsingrt label" width="696" colspan="2">
                    <asp:Label ID="lang1041" runat="server">Current Tasks</asp:Label>
                </td>
            </tr>
            <tr>
                <td id="tdaddtask" colspan="3" runat="server">
                    <table>
                        <tr>
                            <td style="width: 20px">
                                <img onclick="addtask();" border="0" src="../images/appbuttons/minibuttons/addnew.gif">
                            </td>
                            <td class="bluelabel" style="width: 80px">
                                <asp:Label ID="lang1042" runat="server">Add Task</asp:Label>
                            </td>
                            <td style="width: 20px">
                                <img id="imgaddcomp" class="details" onclick="addtask2();" border="0" src="../images/appbuttons/minibuttons/addnew.gif"
                                    runat="server">
                            </td>
                            <td class="bluelabel" width="382">
                                <asp:Label ID="lbladdcomp" runat="server" Visible="False">Add Task to Component</asp:Label>
                            </td>
                            <td id="tdsort" class="plainlabelblue" width="200" align="right" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Repeater ID="rptrtasks" runat="server">
                        <HeaderTemplate>
                            <table cellspacing="2">
                                <tr class="tbg" width="722" height="26">
                                    <td class="thdrsingg plainlabel" style="width: 80px">
                                        <asp:LinkButton OnClick="SortTasks" ID="lang1043" runat="server">Task#</asp:LinkButton>
                                    </td>
                                    <td class="thdrsingg plainlabel" style="width: 180px">
                                        <asp:LinkButton OnClick="SortCompTasks" ID="lang1044" runat="server">Component Addressed</asp:LinkButton>
                                    </td>
                                    <td class="thdrsingg plainlabel" width="30">
                                        <asp:Label ID="lang1045" runat="server">Type</asp:Label>
                                    </td>
                                    <td class="thdrsingg plainlabel" width="402">
                                        <asp:Label ID="lang1046" runat="server">Task Description</asp:Label>
                                    </td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="transrowblue" height="20">
                                <td class="plainlabel transrowblue">
                                    &nbsp;
                                    <asp:LinkButton ID="lbltn" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"tasknum")%>'
                                        runat="server">
                                    </asp:LinkButton>
                                </td>
                                <td class="plainlabel transrowblue">
                                    <asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td class="label transrowblue" id="lblo" runat="server">
                                    <%# DataBinder.Eval(Container.DataItem,"o")%>
                                </td>
                                <td class="plainlabel transrowblue">
                                    <asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"otaskdesc")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td class="details">
                                    <asp:Label ID="lbleqiditem" Text='<%# DataBinder.Eval(Container.DataItem,"pmtskid")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr class="tbg">
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                </td>
                                <td class="label">
                                    <%# DataBinder.Eval(Container.DataItem,"r")%>
                                </td>
                                <td class="plainlabel">
                                    <asp:Label ID="Label4" Text='<%# DataBinder.Eval(Container.DataItem,"taskdesc")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="transrowblue" height="20">
                                <td class="plainlabel transrowblue">
                                    &nbsp;
                                    <asp:LinkButton ID="lbltnalt" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"tasknum")%>'
                                        runat="server">
                                    </asp:LinkButton>
                                </td>
                                <td class="plainlabel transrowblue">
                                    <asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td class="label transrowblue">
                                    <%# DataBinder.Eval(Container.DataItem,"o")%>
                                </td>
                                <td class="plainlabel transrowblue">
                                    <asp:Label ID="Label3" Text='<%# DataBinder.Eval(Container.DataItem,"otaskdesc")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td class="details">
                                    <asp:Label ID="lbleqidalt" Text='<%# DataBinder.Eval(Container.DataItem,"pmtskid")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr class="tbg">
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                </td>
                                <td class="label">
                                    <%# DataBinder.Eval(Container.DataItem,"r")%>
                                </td>
                                <td class="plainlabel">
                                    <asp:Label ID="Label5" Text='<%# DataBinder.Eval(Container.DataItem,"taskdesc")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblsid" type="hidden" name="lblsid" runat="server"><input id="lbltaskid"
        type="hidden" name="lbltaskid" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
    <input id="lbldid" type="hidden" name="lbldid" runat="server">
    <input id="lblclid" type="hidden" name="lblclid" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblfilt" type="hidden" name="lblfilt" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server">
    <input id="taskcnt" type="hidden" name="taskcnt" runat="server"><input id="lbltasknum"
        type="hidden" runat="server">
    <input id="lblcoid" type="hidden" runat="server"><input id="lbltyp" type="hidden"
        name="lbltyp" runat="server">
    <input id="lbllid" type="hidden" name="lbllid" runat="server">
    <input type="hidden" id="lblsubmit" runat="server">
    <input type="hidden" id="lblfslang" runat="server" /><input type="hidden" id="lblcurrsort"
        runat="server" name="lblcurrsort">
    <input type="hidden" id="lblcurrcompsort" runat="server" name="lblcurrcompsort">
    <input type="hidden" id="lblcompnum" runat="server">
    <input type="hidden" id="lblwho" runat="server" />
    </form>
</body>
</html>
