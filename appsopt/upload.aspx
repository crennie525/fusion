<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="upload.aspx.vb" Inherits="lucy_r12.upload" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>upload</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  type="text/javascript" src="../scripts1/uploadaspx.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table width="480">
        <tr>
            <td class="thdrsingrt label">
                <asp:Label ID="lang1048" runat="server">Procedure Upload Dialog</asp:Label>
            </td>
        </tr>
        <tr height="26">
            <td style="border-bottom: #7ba4e0 thin solid" colspan="4" class="label" id="tdhdr"
                runat="server">
            </td>
        </tr>
    </table>
    <table width="480">
        <tr>
            <td class="bluelabel">
                <asp:Label ID="lang1049" runat="server">Choose PM´s to Upload</asp:Label>
            </td>
            <td>
                <input class="3DButton" id="btnupload" type="button" value="Upload" name="btnupload"
                    runat="server">
            </td>
            <td class="bluelabel">
                <asp:Label ID="lang1050" runat="server">Select Files to Delete</asp:Label>
            </td>
            <td>
                <asp:ImageButton ID="ibdelproc" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif">
                </asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td class="label" colspan="2">
                <input class="3DButton" id="File1" type="file" size="32" name="File1" runat="server">
            </td>
            <td class="labellt" id="tbfiles" valign="top" colspan="2" rowspan="10" runat="server">
                <asp:CheckBoxList ID="cblproc" CssClass="labellt" runat="server">
                </asp:CheckBoxList>
                <a href="#" onclick="handleexit()">Return</a>
            </td>
            <tr>
                <td class="label" colspan="2">
                    <input class="3DButton" id="File2" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="2">
                    <input class="3DButton" id="File3" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="2">
                    <input class="3DButton" id="File4" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="2">
                    <input class="3DButton" id="File5" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="2" style="height: 1px">
                    <input class="3DButton" id="File6" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="2">
                    <input class="3DButton" id="File7" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="2">
                    <input class="3DButton" id="File8" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="2">
                    <input class="3DButton" id="File9" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="2">
                    <input class="3DButton" id="File10" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
    </table>
    <input id="lbldoctype" type="hidden" runat="server"><input id="lbleqid" type="hidden"
        runat="server">
    <input type="hidden" id="lblsubmit" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script type="text/javascript">
        function handleexit() {

            var checkboxId = $("#tbfiles input:checked").attr("id");
            var filename = $("#tbfiles label[for=" + checkboxId + "]").text();

            if (!window.showModalDialog) {
                window.parent.parent.closeModal(filename);
            } else {
                window.parent.returnValue = filename;
                window.parent.close();
            }
        }
    </script>
</body>
</html>
