<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OptDevArch.aspx.vb" Inherits="lucy_r12.OptDevArch" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>OptDevArch</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script  type="text/javascript" src="../scripts1/OptDevArchaspx.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <div class="bluelabel" id="tdarch" style="overflow: auto; width: 250px; position: absolute;
        top: 0px; height: 170px" runat="server">
    </div>
    <input type="hidden" id="lblchk" runat="server" name="lblchk">
    <input type="hidden" id="lbldid" runat="server" name="lbldid">
    <input type="hidden" id="lblclid" runat="server" name="lblclid"><input type="hidden"
        id="lblloc" runat="server">
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
