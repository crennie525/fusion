﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="checkcompqty.aspx.vb" Inherits="lucy_r12.checkcompqty" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script  type="text/javascript">
    <!--
        function dflt() {
            document.getElementById("lblcheckyn").value = "1";
        }
        function checkyn(who) {
            document.getElementById("lblcheckyn").value = who;
        }
        function doit() {
            var cyn = document.getElementById("lblcheckyn").value;
            window.parent.doit(cyn);
        }
    -->
    </script>
</head>
<body onload="dflt();">
    <form id="form1" runat="server">
    <table>
    <tr><td class="plainlabel">Update Total Task Time based on Component Qty value?</td></tr>
    <tr><td class="plainlabel" align="center">YES&nbsp;<input type="radio" name="rbyn" id="rb1" checked="checked" onclick="checkyn('1');" />
    NO&nbsp;<input type="radio" name="rbyn" id="rb2" onclick="checkyn('2');" /></td></tr>
    <tr><td class="plainlabel" align="center"><input type="button" id="btnyn" value="Submit" onclick="doit();" ></td></tr>
    </table>
    <input type="hidden" id="lblcheckyn" runat="server" />
    </form>
</body>
</html>
