<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMRationaleDialog.aspx.vb"
    Inherits="lucy_r12.PMRationaleDialog" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title runat="server" id="pgtitle">PMRationaleDialog</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script  type="text/javascript" src="../scripts/sessrefdialog.js"></script>
    <script  type="text/javascript" src="../scripts1/PMRationaleDialogaspx.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="handleentry();pageScroll();">
    <form id="form1" method="post" runat="server">
    <iframe id="ifaddrat" src="" width="100%" height="150%" frameborder="no"></iframe>
    <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
    <script type="text/javascript">
        document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script>
    <input type="hidden" id="lblsessrefresh" runat="server" name="lblsessrefresh">
    <input id="lblpg" type="hidden" runat="server" name="lblpg"><input id="lbltaskid"
        type="hidden" runat="server" name="lbltaskid">
    <input id="lbldid" type="hidden" runat="server" name="lbldid">
    <input id="lblclid" type="hidden" runat="server" name="lblclid">
    <input id="lbleqid" type="hidden" runat="server" name="lbleqid">
    <input id="lblfuid" type="hidden" runat="server" name="lblfuid">
    <input id="lblcoid" type="hidden" runat="server" name="lblcoid">
    <input type="hidden" id="lbllog" runat="server">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
