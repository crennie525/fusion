<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="bulkdialog.aspx.vb" Inherits="lucy_r12.bulkdialog" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bulk Procedure Lookup</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script  type="text/javascript" src="../scripts/sessrefdialog.js"></script>
    <script  type="text/javascript" src="../scripts1/bulkdialogaspx.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="resetsess();pageScroll();">
    <form id="form1" method="post" runat="server">
    <iframe id="ifbulk" src="" width="100%" height="100%" frameborder="no" runat="server">
    </iframe>
    <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
    <script type="text/javascript">
        document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script>
    <input type="hidden" id="lblsessrefresh" runat="server" name="lblsessrefresh">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
