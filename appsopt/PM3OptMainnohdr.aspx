﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PM3OptMainnohdr.aspx.vb"
    Inherits="lucy_r12.PM3OptMainnohdr" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script  type="text/javascript" src="../scripts/taskgrid.js"></script>
    <script  type="text/javascript" src="../scripts1/PM3OptMainaspx2.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />
    <script  type="text/javascript">
     <!--
        function handlegetdrag(eqid, eqnum, proc) {
            document.getElementById("tbopt").className = "details";
            document.getElementById("tdshtop").className = "details";
            document.getElementById("tdshbot").className = "details";
            var ustr = document.getElementById("lbluser").value;
            eqnum = eqnum.replace(/#/, "%23")
            proc = proc.replace(/#/, "%23")
            document.getElementById("ifdrag").src = "../appsdrag/dragndropno.aspx?who=revdnd&eqid=" + eqid + "&eqnum=" + eqnum + "&proc=" + proc + "&ustr=" + ustr;
            //alert("2")
            document.getElementById("imgopt").src = "../images/appbuttons/minibuttons/dndbg.gif";
            //alert("3")
            document.getElementById("tdloctop").innerHTML = "Drag and Drop PM Details";
            //alert("4")
            document.getElementById("tbdrag").className = "view";
        }
        function handledoc(str) {
            //alert(str)
            document.getElementById("ifpmdoc").src = str;
        }
        function CheckDrag() {
            var who = document.getElementById("lblwho").value;
            if (who == "drag") {
                var eqid = document.getElementById("lbleqid").value;
                var eqnum = "Something";
                var proc = "";
                handlegetdrag(eqid, eqnum, proc);
            }
            else {
                opennav();
            }

            //after this is checked, show the table
            document.getElementById("tblopt").style.visibility = "visible";
        }
        function handlearch(did, clid, lid, sid, typ, eqid, fuid, coid, eqnum, dept, cell, loc) {
            //alert(eqid + " from arch")
            document.getElementById("lbldid").value = did;
            document.getElementById("lblclid").value = clid;
            document.getElementById("lbllid").value = lid;
            document.getElementById("lblsid").value = sid;
            document.getElementById("lbltyp").value = typ;
            document.getElementById("lbleqid").value = eqid;
            document.getElementById("lblfuid").value = fuid;
            document.getElementById("lblcoid").value = coid;
            document.getElementById("lbleqnum").value = eqnum;

            document.getElementById("lbldept").value = dept;
            document.getElementById("lblcell").value = cell;
            document.getElementById("lblloc").value = loc;

            window.parent.handlearchret(did, clid, lid, sid, typ, eqid, fuid, coid, eqnum, dept, cell, loc);
        }
        var procflag = 0
        var navflag = 0
        function opennav() {
            if (procflag == 0) {
                if (navflag == 0) {
                    navflag = 1;
                    //procflag=1;
                    document.getElementById("tdnav").className = "viewcell";
                    document.getElementById("tdnavtop").className = "thdrsinglft viewcell";
                    document.getElementById("tdnavtoprt").className = "thdrsingrt label viewcell";
                    //document.getElementById("tdnavtop").style.width="305px";
                    document.getElementById("tdproc").className = "details";
                    document.getElementById("tdproctop").className = "details";
                    document.getElementById("tdproctoplft").className = "details";
                    document.getElementById("tdproctoplft1").className = "details";
                    document.getElementById("tdproctop").style.width = "0px";
                    document.getElementById("tblopt").style.width = "915px";
                    document.getElementById("imgnav").src = "../images/appbuttons/minibuttons/close1.gif";
                }
                else {
                    navflag = 0;
                    //procflag=0;
                    document.getElementById("tdnav").className = "details";
                    document.getElementById("tdnavtop").className = "details";
                    document.getElementById("tdnavtoprt").className = "details";
                    document.getElementById("tdnavtop").style.width = "0px";
                    document.getElementById("tdproc").className = "viewcell";
                    document.getElementById("tdproctoplft").className = "thdrsingtbo viewcell";
                    document.getElementById("tdproctop").className = "thdrsingrt label viewcell";
                    document.getElementById("tdproctoplft1").className = "thdrsingtbo viewcell";
                    document.getElementById("tdproctop").style.width = "580px";
                    document.getElementById("tblopt").style.width = "1190px";
                    document.getElementById("imgnav").src = "../images/appbuttons/minibuttons/open.gif";
                }
            }
        }
         //-->
    </script>
</head>
<body onload="checkarch();CheckDrag();">
    <form id="form1" runat="server">
    <div style="z-index: 1; position: absolute; top: 0px; left: 4px">
        <!-- initially hide the table to prevent screen flicker -->
        <table id="tblopt" cellspacing="0" cellpadding="0" width="1312" border="0" style="visibility: hidden;">
            <tr>
                <td id="tdqs" runat="server" colspan="2">
                </td>
            </tr>
            <tr>
                <td class="thdrsinglft" align="left" style="width: 26px">
                    <img src="../images/appbuttons/minibuttons/optbg.gif" border="0" id="imgopt" runat="server">
                </td>
                <td class="thdrsingrt label" align="left" width="716" id="tdloctop">
                    <asp:Label ID="lang980" runat="server">Location/Equipment/Function Details</asp:Label>
                </td>
                <td class="thdrsingtbo viewcell" valign="top" align="left" width="22" id="tdshtop" runat="server">
                    <img onmouseover="return overlib('Show/Hide Asset Information', ABOVE, LEFT)" onmouseout="return nd()"
                        id="imgnav" onclick="opennav();" src="../images/appbuttons/minibuttons/open.gif"
                        runat="server">
                </td>
                <td class="thdrsinglft details" id="tdnavtop" width="22">
                    <img src="../images/appbuttons/minibuttons/eqarch.gif" border="0">
                </td>
                <td class="thdrsingrt label details" id="tdnavtoprt" align="left" width="268">
                    <font class="label">
                        <asp:Label ID="lang981" runat="server">Asset Hierarchy</asp:Label></font>
                </td>
                <td class="thdrsingtbo details" valign="top" align="left" width="22" id="tdproctoplft1">
                    <img onmouseover="return overlib('Expand/Shrink Procedure to Optimize', ABOVE, LEFT)"
                        onmouseout="return nd()" id="imghide" onclick="hiderev();" src="../images/appbuttons/minibuttons/close1.gif"
                        runat="server">
                </td>
                <td class="thdrsingtbo details" align="left" style="width: 26px" id="tdproctoplft">
                    <img src="../images/appbuttons/minibuttons/optbg.gif" border="0">
                </td>
                <td class="thdrsingrt label viewells" id="tdproctop" width="552">
                    <font class="label">
                        <asp:Label ID="lang982" runat="server">PM Procedure to Optimize</asp:Label></font>
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="top">
                    <table cellspacing="0" cellpadding="0" id="tbopt" runat="server">
                        <tr>
                            <td>
                                <iframe id="geteq" style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                                    border-top-style: none; border-left-style: none" src="pmgetopt.aspx?tli=5&amp;jump=no"
                                    frameborder="no" width="742" scrolling="no" height="128" runat="server" >
                                </iframe>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe id="iftaskdet" style="border-bottom-style: none; padding-bottom: 0px; border-right-style: none;
                                    background-color: transparent; margin: 0px; padding-left: 0px; padding-right: 0px;
                                    border-top-style: none; border-left-style: none; padding-top: 0px" src="PMOptTasksGrid.aspx?start=no"
                                    frameborder="no" width="742" scrolling="no" height="500" runat="server" >
                                </iframe>
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" id="tbdrag" runat="server" class="details">
                        <tr>
                            <td>
                                <iframe id="ifdrag" style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                                    border-top-style: none; border-left-style: none" src="OptHolder.aspx" frameborder="no"
                                    width="742" scrolling="yes" height="519" runat="server" ></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top" align="center" width="22" id="tdshbot" runat="server" class="viewcell">
                    <img id="Img1" onclick="opennav();" src="../images/appbuttons/minibuttons/archlabel.gif"
                        runat="server" alt="" />
                </td>
                <td class="details" id="tdnav" colspan="2" valign="top">
                    <table cellspacing="0">
                        <tr>
                            <td colspan="2" valign="top">
                                <iframe id="ifarch" style="border-bottom-style: none; padding-bottom: 0px; border-right-style: none;
                                    background-color: transparent; margin: 0px; padding-left: 0px; padding-right: 0px;
                                    border-top-style: none; border-left-style: none; padding-top: 0px" src="OptDevArch.aspx?start=no"
                                    frameborder="0" width="260" scrolling="no" height="170" runat="server">
                                </iframe>
                            </td>
                        </tr>
                        <tr>
                            <td class="thdrsinglft" width="22">
                                <img src="../images/appbuttons/minibuttons/eqarch.gif" border="0">
                            </td>
                            <td class="thdrsingrt" width="227">
                                <a href="#" onclick="getpics('asset');" class="A1black" id="hassets">
                                    <asp:Label ID="lang983" runat="server">Asset Images</asp:Label></a>&nbsp;&nbsp;&nbsp;&nbsp;<a
                                        href="#" onclick="getpics('task');" class="A1blue" id="htasks"><asp:Label ID="lang984"
                                            runat="server">Task Images</asp:Label></a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <iframe id="ifimg" style="border-bottom-style: none; padding-bottom: 0px; border-right-style: none;
                                    background-color: transparent; margin: 0px; padding-left: 0px; padding-right: 0px;
                                    border-top-style: none; border-left-style: none; padding-top: 0px" src="OptAssetImage.aspx?eqid=0"
                                    frameborder="no" width="260" scrolling="no" height="296" runat="server" >
                                </iframe>
                            </td>
                        </tr>
                    </table>
                </td>
                <td id="tdproc" valign="top" colspan="3" class="viewcell">
                    <iframe id="ifpmdoc" style="padding-bottom: 0px; background-color: transparent; margin: 0px;
                        padding-left: 0px; padding-right: 0px; padding-top: 0px" src="OptHolder.aspx"
                        frameborder="yes" width="600" scrolling="no" height="480" runat="server" >
                    </iframe>
                </td>
            </tr>
        </table>
        <input id="lbltab" type="hidden" name="lbltab" runat="server">
        <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
        <input id="lblsid" type="hidden" name="lblsid" runat="server">
        <input id="lbldid" type="hidden" name="lbldid" runat="server">
        <input id="lblclid" type="hidden" name="lblclid" runat="server">
        <input id="lblchk" type="hidden" name="lblchk" runat="server">
        <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
        <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
        <input id="lblcid" type="hidden" name="lblcid" runat="server">
        <input id="lblgetarch" type="hidden" name="lblgetarch" runat="server">
        <input type="hidden" id="lbltyp" runat="server" name="lbltyp">
        <input type="hidden" id="lbllid" runat="server" name="lbllid">
        <input id="lblret" type="hidden" name="lblret" runat="server">
        <input id="lbldchk" type="hidden" name="lbldchk" runat="server">
        <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
        <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
        <input id="tasknum" type="hidden" name="tasknum" runat="server">
        <input id="taskcnt" type="hidden" name="taskcnt" runat="server">
        <input type="hidden" id="retqs" runat="server">
        <input type="hidden" id="lblsave" runat="server">
        <input type="hidden" id="lblsavetasks" runat="server"><input type="hidden" id="lblsubmit"
            runat="server">
        <input type="hidden" id="lblpicmode" runat="server">
        <input type="hidden" id="lblwho" runat="server" />
        <input type="hidden" id="lbleqnum" runat="server" />
        <input type="hidden" id="lbldept" runat="server" />
        <input type="hidden" id="lblcell" runat="server" />
        <input type="hidden" id="lblloc" runat="server" />
        <input type="hidden" id="lbluser" runat="server" />
        <input type="hidden" id="lblfslang" runat="server" />
    </div>
    </form>
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../scripts/showModalDialog.js"></script>
</body>
</html>
