﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="totpms2.aspx.vb" Inherits="lucy_r12.totpms2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
   
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript">
        function checksave() {
            var chk = document.getElementById("lblsavechk").value;
            if (chk == "1") {
                window.parent.handlesave();
            }
        }
        function getskill(freqid, pmtskid, sid, fld) {
            var eReturn = window.showModalDialog("../apps/freqlistdialog.aspx?pmtskid=" + pmtskid + "&fld=" + fld + "&sid=" + sid, "", "dialogHeight:460px; dialogWidth:460px; resizable=yes");
            if (eReturn) {
                var ret = eReturn.split(",");
                document.getElementById(freqid).innerHTML = ret[1];
                document.getElementById("lblskillret").value = ret[1];
                document.getElementById("lblskillidret").value = ret[0];
            }

        }
        function getrd(freqid, pmtskid, sid, fld) {
            var eReturn = window.showModalDialog("../apps/freqlistdialog.aspx?pmtskid=" + pmtskid + "&fld=" + fld + "&sid=" + sid, "", "dialogHeight:460px; dialogWidth:460px; resizable=yes");
            if (eReturn) {
                var ret = eReturn.split(",");
                document.getElementById(freqid).innerHTML = ret[1];
                document.getElementById("lblrdret").value = ret[1];
                document.getElementById("lblrdidret").value = ret[0];
            }

        }
        function getmeter(pmtid, freqid, eqid, pmtskid, fuid, skillid, skillqty, rdid, meterid, rd, skill) {

        //alert(eqid)
            var eqnum = document.getElementById("lbleqnum").value;
            var func = ""; // document.getElementById("lblfunc").value;
            var varflg = 0;
            if (skillid == "" || skillid == "0") {
                varflg = 1;
            }
            if (rdid == "" || rdid == "0") {
                varflg = 1;
            }
            //typ was dadr
            if (varflg == 0) {
                var eReturn = window.showModalDialog("../equip/usemeterdialog.aspx?typ=dadr&eqid=" + eqid + "&pmtskid=" + pmtskid + "&fuid=" + fuid + "&skillid=" + skillid
                                   + "&skillqty=" + skillqty + "&rdid=" + rdid + "&func=" + func + "&skill=" + skill + "&rd=" + rd
                                   + "&meterid=" + meterid + "&rdt=" + "&eqnum=" + eqnum + "&pmtid=" + pmtid + "&who=tot" + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:900px;resizable=yes");
                if (eReturn) {
                    alert(eReturn)
                    var ret = eReturn.split("~");
                    var del = ret[6];
                    if (del != "yes") {
                        document.getElementById(freqid).value = ret[2];
                        document.getElementById(freqid).disabled = true;
                        document.getElementById("lblmeterid").value = ret[0];
                        document.getElementById("lblmeterfreq").value = ret[1];
                        document.getElementById("lblfreqret").value = ret[2];
                        document.getElementById("lbldayfreq").value = ret[2];
                        //document.getElementById("txtfreqo").value = ret[2];
                        document.getElementById("lblmeterunit").value = ret[3];
                        document.getElementById("lblmaxdays").value = ret[4];
                        document.getElementById("lblmeter").value = ret[5];
                        /*
                        var munit = document.getElementById("lblunit").value;
                        var maxdays = document.getElementById("txtmax").value;
                        var freq = document.getElementById("txtfreq").value;
                        var dayfreq = document.getElementById("lbldayfreq").value;
                        var meterid = document.getElementById("lblmeterid").value;
                        var meter = document.getElementById("lblmeter").value;
                        var del = document.getElementById("lbldel").value;
                        var ret = meterid + "~" + freq + "~" + dayfreq + "~" + munit + "~" + maxdays + "~" + meter + "~" + del;
                        */
                    }
                    else {
                        document.getElementById(freqid).disabled = false;
                        
                    }
                    //document.getElementById("lblcompchk").value = "2";
                    //document.getElementById("form1").submit();
                }
            }
        }
        var winbx = "directories=0,height=600,width=720,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
        function getpmot() {
            var eq = document.getElementById("lbleqid").value;
            window.open("../appsdrag/pmotime.aspx?eqid=" + eq, "winbx", "width=900, height=700, scrollbars=1");

        }
    </script>
</head>
<body onload="checksave();">
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td class="redlabel" align="center" id="tdmsg" runat="server">
                </td>
            </tr>
            <tr>
            <td class="plainlabelblue" align="center" height="20">
            Note: Changes to Skill, Qty, Frequency, and Status will modify all Task Records (Original Only) that were created using these values.
            </td>
            </tr>
            <tr>
            <td class="plainlabelblue" align="left" height="20">
            Note: When Switching from a Status of Running to a Status of Down the Down Time Text Box will not be enabled until after the record is saved.
            </td>
            </tr>
            <tr>
            <td class="plainlabelblue" align="left" height="20">
            Note: Any Changes to Skill, Qty, Frequency, and Status will not be reflected in the PM column until after the record is saved.
            </td>
            </tr>
            <tr>
                <td width="840">
                    <asp:DataGrid ID="dgtots" runat="server" CellSpacing="1" AutoGenerateColumns="False"
                        AllowCustomPaging="True" AllowPaging="True" GridLines="None" CellPadding="0"
                        ShowFooter="False" PageSize="20">
                        <FooterStyle BackColor="White"></FooterStyle>
                        <EditItemStyle Height="15px"></EditItemStyle>
                        <AlternatingItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="#E7F1FD">
                        </AlternatingItemStyle>
                        <ItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="ControlLightLight">
                        </ItemStyle>
                        <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PM" Visible="true">
                                <HeaderStyle Width="300px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container, "DataItem.pm") %>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <%# DataBinder.Eval(Container, "DataItem.pm") %>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="skill desc" HeaderText="Skill">
                                <HeaderStyle Width="180px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblskill" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                    </asp:Label>
                                    <img id="imgskill" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Qty">
                                <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skillqty") %>'
                                        ID="Label10" NAME="Label8">
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtqty" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.skillqty") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Frequency">
                                <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblfreqi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtfreq" runat="server" CssClass="plainlabel" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'></asp:TextBox>
                                    <img id="imgfreq" alt="" runat="server" onmouseover="return overlib('View, Add, or Edit Meter Frequency for this Task', ABOVE, LEFT)"
                                        onmouseout="return nd()" src="../images/appbuttons/minibuttons/meter1.gif" />
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Status">
                                <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label75" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rd") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblrd" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rd") %>'>
                                    </asp:Label>
                                    <img id="imgrd" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Total Time">
                                <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.tottime") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txttottimee" runat="server" Width="50px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.tottime") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Down Time">
                                <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.downtime") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtdowntimee" runat="server" Width="50px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.downtime") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn> 
                            <asp:TemplateColumn HeaderText="Tasks" Visible="True">
                                <HeaderStyle Width="30px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbltasksi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tcnt") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lbltaskse" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tcnt") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="skillid" Visible="False">
                                <HeaderStyle Width="20px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblskillidi" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.skillid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblskillide" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.skillid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="skillqty" Visible="False">
                                <HeaderStyle Width="20px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblsqtyi" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.skillqty") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblsqty" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.skillqty") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="freqid" Visible="False">
                                <HeaderStyle Width="20px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblfreqidi" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblfreqide" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="rdid" Visible="False">
                                <HeaderStyle Width="20px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblrdidi" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.rdid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblrdide" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.rdid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ismeter" Visible="False">
                                <HeaderStyle Width="20px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblismeteri" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.ismeter") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblismetere" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.ismeter") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="meterid" Visible="False">
                                <HeaderStyle Width="20px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblmeteridi" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.meterid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblmeteride" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.meterid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="meter" Visible="False">
                                <HeaderStyle Width="20px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblmeteri" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.meter") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblmetere" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.meter") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="meterunit" Visible="False">
                                <HeaderStyle Width="20px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblmeteruniti" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.meterunit") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblmeterunite" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.meterunit") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="meterfreq" Visible="False">
                                <HeaderStyle Width="20px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblmeterfreqi" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.meterfreq") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblmeterfreqe" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.meterfreq") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="maxdays" Visible="False">
                                <HeaderStyle Width="20px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblmaxdaysi" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.maxdays") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblmaxdayse" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.maxdays") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Delete">
										<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:ImageButton id="ibDel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton>
										</ItemTemplate>
									</asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
                            ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                            <td class="bluelabelu" height="20" align="right">
                                <a id="a24" onclick="getpmot();" href="#">
                                    <asp:Label ID="Label3" runat="server">Open PMO Time Overview</asp:Label></a>
                            </td>
                        </tr>
        </table>
        <input type="hidden" id="lbleqid" runat="server">
        <input type="hidden" id="lblsavechk" runat="server">
        <input type="hidden" id="lblsid" runat="server" />
        <input type="hidden" id="lblskillidret" runat="server" />
        <input type="hidden" id="lblrdidret" runat="server" />
        <input type="hidden" id="lblskillret" runat="server" />
        <input type="hidden" id="lblrdret" runat="server" />
        <input type="hidden" id="lblmeterid" runat="server" />
        <input type="hidden" id="lbldayfreq" runat="server" />
        <input type="hidden" id="lblmeterunit" runat="server" />
        <input type="hidden" id="lblmaxdays" runat="server" />
        <input type="hidden" id="lblmeterfreq" runat="server" />
        <input type="hidden" id="lblmeter" runat="server" />
        <input type="hidden" id="lblfreqret" runat="server" />
        <input type="hidden" id="lbleqnum" runat="server" />
        <input type="hidden" id="lblismeter" runat="server" />
        <input type="hidden" id="lbltaskflag" runat="server" />
        
    </div>
    </form>
</body>
</html>
