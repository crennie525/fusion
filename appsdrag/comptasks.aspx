<%@ Page Language="vb" AutoEventWireup="false" Codebehind="comptasks.aspx.vb" Inherits="lucy_r12.comptasks" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>comptasks</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<LINK href="../styles/pmcss1.css" type="text/css" rel="stylesheet">
		
		<script language="JavaScript" type="text/javascript">
		function changeview(typ) {
		var chk = document.getElementById("lbltyp").value;
		if(chk!=typ) {
		document.getElementById("lbltyp").value = typ;
		document.getElementById("lblsubmit").value = typ;
		document.getElementById("form1").submit();
		}
		}
		</script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 2px; POSITION: absolute; TOP: 2px">
				<tr>
					<td>
						<table>
							<tr>
								<td class="label" width="120">Current Component</td>
								<td class="bluelabellt" id="tdfu" runat="server" width="290"></td>
								<td align="right" width="100"></td>
							</tr>
							<tr>
								<td colspan="3" class="plainlabel">
									<input type="radio" id="rb1" name="rtyp" checked runat="server" onclick="changeview('orig');"
										VALUE="rb1">View Original Tasks <input type="radio" id="rb2" name="rtyp" runat="server" onclick="changeview('rev');" VALUE="rb2">View 
									Revised Tasks
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="thdrsingg plainlabel" width="340">Components &amp; Tasks</td>
				</tr>
				<tr>
					<td class="bluelabel" id="tdcomps" vAlign="top" runat="server">
						<div class="DragContainer1" id="DragContainer1" runat="server">
							<div class="DragBox" id="dummy1" overClass="OverDragBox" dragClass="DragDragBox"></div>
						</div>
					</td>
				</tr>
			</table>
			<input id="lbl1" type="hidden" name="lbl1" runat="server"> <input id="lbl2" type="hidden" name="lbl2" runat="server"><input id="xCoord" type="hidden" runat="server" /><input id="yCoord" type="hidden" runat="server" />
			<input id="lblsubmit" type="hidden" runat="server" NAME="lblsubmit"> <input id="lblcomid" type="hidden" runat="server" NAME="lblfuid">
			<input type="hidden" id="lbltyp" runat="server" NAME="lbltyp">
		</form>
	</body>
</HTML>
