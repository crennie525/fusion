Imports System.Data.SqlClient
Public Class taskedit2
    Inherits System.Web.UI.Page
    Dim tl, cid, sid, did, clid, eqid, fuid, coid, sql, val, name, field, ttid, tnum, username, ro, tchk As String
    Dim co, fail, chk, tc, ghostoff, xstatus As String
    Dim tskcnt As Integer
    Dim tasks As New Utilities
    Dim tasksadd As New Utilities
    Dim comi As New mmenu_utils_a
    Dim tmod As New transmod
    Public dr As SqlDataReader
    Dim ds As DataSet
    Dim Tables As String = "pmtasks"
    Dim PK As String = "pmtskid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 1
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim CntFilter As String = ""
    Dim Group As String = ""
    Protected WithEvents btnedittask As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdcompnum As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdoskill As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdofreq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdostat As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdskill As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfreq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdstat As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Label22 As System.Web.UI.WebControls.Label
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents Label23 As System.Web.UI.WebControls.Label
    Protected WithEvents lblcnt As System.Web.UI.WebControls.Label
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusetot As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusemeter As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btntpm As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbltpmhold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdtpmhold As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbltpmalert As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgdeltpm As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Sort As String = "tasknum, subtask asc"
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtcQty As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbofailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents ibfromo As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibtoo As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbCompFM As System.Web.UI.WebControls.ListBox
    Protected WithEvents ibReuse As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfaillist As System.Web.UI.WebControls.ListBox
    Protected WithEvents ibToTask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibFromTask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents ddtypeo As System.Web.UI.WebControls.ListBox
    Protected WithEvents ddpto As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddtype As System.Web.UI.WebControls.ListBox
    Protected WithEvents ddpt As System.Web.UI.WebControls.DropDownList

    Protected WithEvents txtqtyo As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttro As System.Web.UI.WebControls.TextBox
    
    Protected WithEvents txtordt As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddtaskstat As System.Web.UI.WebControls.DropDownList

    Protected WithEvents txtqty As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttr As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpfint As System.Web.UI.WebControls.TextBox
   
    Protected WithEvents txtrdt As System.Web.UI.WebControls.TextBox
    Protected WithEvents cbloto As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cbcs As System.Web.UI.WebControls.CheckBox
    Protected WithEvents Label26 As System.Web.UI.WebControls.Label
    Protected WithEvents lblsubcount As System.Web.UI.WebControls.Label
    Protected WithEvents lblspg As System.Web.UI.WebControls.Label
    Protected WithEvents Label27 As System.Web.UI.WebControls.Label
    Protected WithEvents lblscnt As System.Web.UI.WebControls.Label
    Protected WithEvents btnaddsubtask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibCancel As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtodesc As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents txtdesc As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents btnlookup As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnlookup2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img5 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Tr1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents IMG2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgrat As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblsvchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblenable As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpgholder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblst As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltabid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsave As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgrid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcind As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrsb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldocpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldocpmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsesscnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhaspm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblghostoff As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblxstatus As System.Web.UI.HtmlControls.HtmlInputHidden
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim coi As String = comi.COMPI

        If coi = "GSK" Then
            lblghostoff.Value = "yes"
        End If

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        If Request.Form("lblcompchk") = "1" Then
            lblcompchk.Value = "0"
            tasks.Open()
            'added in case components with tasks were copied on return
            Filter = lblfilt.Value
            PageNumber = lblpg.Text
            LoadPage(PageNumber, Filter)
            'end new
            coid = lblco.Value
            
            tasks.Dispose()
        ElseIf Request.Form("lblcompchk") = "2" Then
            lblcompchk.Value = "0"
            tasks.Open()

            'added in case components deleted on return
            Filter = lblfilt.Value
            PageNumber = lblpg.Text
            LoadPage(PageNumber, Filter)
            'end new
            tasks.Dispose()
            coid = lblco.Value
           
        ElseIf Request.Form("lblcompchk") = "3" Then
            lblcompchk.Value = "0"
            tasksadd.Open()
            SaveTask()
            tasksadd.Dispose()
        ElseIf Request.Form("lblcompchk") = "4" Then
            lblcompchk.Value = "0"
            tasks.Open()
            GetLists()
            tasks.Dispose()
        ElseIf Request.Form("lblcompchk") = "5" Then
            lblcompchk.Value = "0"
            tasks.Open()
            SubCount(PageNumber, Filter)
            tasks.Dispose()
        ElseIf Request.Form("lblcompchk") = "6" Then
            lblcompchk.Value = "0"
            tasks.Open()
            'LoadCommon()
            Filter = lblfilt.Value
            PageNumber = lblpg.Text
            LoadPage(PageNumber, Filter)
            tasks.Dispose()
        End If

        If Request.Form("lblcompfailchk") = "1" Then
            lblcompfailchk.Value = "0"
            AddFail()
        End If


        If Request.Form("lblgrid") = "yes" Then
            lblgrid.Value = ""
            val = lblfuid.Value
            field = "funcid"
            lblsb.Value = "0"
            Filter = field & " = " & val ' & " and subtask = 0"
            lblfilt.Value = Filter
            tasks.Open()
            LoadPage(PageNumber, Filter)
            tasks.Dispose()
            'GoToGrid()
        End If
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro

            tchk = Request.QueryString("tchk").ToString
            lblusetot.Value = tchk
            fuid = Request.QueryString("fuid").ToString
            lblfuid.Value = fuid
            lblsb.Value = "0"
            Filter = "funcid = " & fuid
            CntFilter = "funcid = " & fuid & " and subtask = 0"
            lblfilt.Value = Filter
            lblfiltcnt.Value = CntFilter
            Dim tasknum As String
            Try
                tasknum = Request.QueryString("tasknum").ToString
                If tasknum = "" Then
                    tasknum = "0"
                End If
                Filter = "funcid = " & fuid
                PageNumber = tasknum
                lblpg.Text = tasknum
            Catch ex As Exception
                tasknum = "0"
            End Try
            tasks.Open()
            GetLists()
            'tasks.Dispose()
            'tasks.Open()
            LoadPage(PageNumber, Filter)
            Dim lock, lockby As String
            Dim user As String = lblusername.Value
            If lock = "1" Then
                lockby = lbllockedby.Value
                If lockby = user Then
                    lbllock.Value = "0"
                Else
                    Dim strMessage As String = "This record is locked for editing by " & lockby & "."
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            ElseIf lock = "0" Then
                'LockRecord(user, eq)
            End If
            tasks.Dispose()

        Else
            'lblpg.Text = "0"
            'lblcnt.Text = "0"
            'lblspg.Text = "0"
            'lblscnt.Text = "0"
        End If
        'lblenable.Value = "0"
        'Disable all fields
        If ro <> "1" Then
            'DisableOptions()
            'txttaskorder.Enabled = False
            ddtaskstat.Enabled = False 'orig
            'ddcomp.Enabled = False
            ''lbfaillist.Enabled = False
            ''lbfailmodes.Enabled = False
            ibToTask.Enabled = False
            ibFromTask.Enabled = False
            ibReuse.Enabled = False
            txtdesc.Disabled = True
            txtrdt.Enabled = False
            txtordt.Enabled = False
            txtodesc.Disabled = True 'orig
            ddtype.Enabled = False
            ddtypeo.Enabled = False 'orig
            'txtfreq.Enabled = False
            'txtfreqo.Enabled = False 'orig

            'cbfixed.Disabled = True
            'cbfixedo.Disabled = True

            txtpfint.Enabled = False
            'ddskill.Enabled = False
            'ddskillo.Enabled = False 'orig
            txtqty.Enabled = False
            txtqtyo.Enabled = False 'orig
            txttr.Enabled = False
            txttro.Enabled = False 'orig
            ddpt.Enabled = False
            ddpto.Enabled = False 'orig
            'ddeqstat.Enabled = False
            'ddeqstato.Enabled = False 'orig
            lblenable.Value = "1"
            txtcQty.Enabled = False
            'lbCompFM.Enabled = False
            ''lbofailmodes.Enabled = False 'orig
            'buttons
            'btnaddtsk.Enabled = True
            btnaddsubtask.Enabled = False
            'btndeltask.Enabled = False
            ibCancel.Enabled = False
            'btnsavetask.Enabled = False
            btntpm.Enabled = False
        End If

        'end disable
        btnaddsubtask.Attributes.Add("onmouseover", "return overlib('Add a Sub-Task to this Task')")
        btnaddsubtask.Attributes.Add("onmouseout", "return nd()")
        ibToTask.Attributes.Add("onmouseover", "return overlib('Add a Failure Mode to this Task')")
        ibToTask.Attributes.Add("onmouseout", "return nd()")
        'ibToTask.Attributes.Add("onclick", "DisableButton(this);")
        ibReuse.Attributes.Add("onmouseover", "return overlib('Add a Failure Mode to this Task that was addessed in another Task')")
        ibReuse.Attributes.Add("onmouseout", "return nd()")
        'ibReuse.Attributes.Add("onclick", "DisableButton(this);")
        ibCancel.Attributes.Add("onmouseout", "return nd()")
        ibCancel.Attributes.Add("onmouseover", "return overlib('Cancel Changes')")
        ibFromTask.Attributes.Add("onmouseover", "return overlib('Remove a Failure Mode from this Task')")
        ibFromTask.Attributes.Add("onmouseout", "return nd()")
        'ibFromTask.Attributes.Add("onclick", "DisableButton(this);")
        btnlookup.Attributes.Add("onmouseover", "return overlib('Get Task Descriptions Similar to this one. - Use keywords for best results')")
        btnlookup.Attributes.Add("onmouseout", "return nd()")
        btnlookup2.Attributes.Add("onmouseover", "return overlib('Open the Common Tasks Dialog')")
        btnlookup2.Attributes.Add("onmouseout", "return nd()")
        ibtoo.Attributes.Add("onmouseover", "return overlib('Add An Original Failure Mode to this Task')")
        ibtoo.Attributes.Add("onmouseout", "return nd()")
        'ibtoo.Attributes.Add("onclick", "DisableButton(this);")
        ibfromo.Attributes.Add("onmouseover", "return overlib('Remove An Original Failure Mode from this Task')")
        ibfromo.Attributes.Add("onmouseout", "return nd()")
        'ibfromo.Attributes.Add("onclick", "DisableButton(this);")
        btnedittask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov101", "PMOptTasks.aspx.vb") & "')")
        btnedittask.Attributes.Add("onmouseout", "return nd()")
    End Sub
    Private Sub LoadDetails(ByVal pmtskid As String)

    End Sub
    Private Sub EnableOptions()
        btnaddsubtask.Enabled = True
        ibCancel.Enabled = True
    End Sub
    Private Function SubCount(ByVal PageNumber As Integer, ByVal Filter As String) As Integer
        Dim scnt As Integer
        fuid = lblfuid.Value
        Try
            sql = "select count(*) from pmTasks where funcid = '" & fuid & "' and tasknum = " & PageNumber & " and subtask <> '0'"
            Try
                scnt = tasks.Scalar(sql)
            Catch ex As Exception
                scnt = tasksadd.Scalar(sql)
            End Try
            lblsubcount.Text = scnt
        Catch ex As Exception
            scnt = 0
        End Try

        Return scnt
    End Function
    Private Function SubTaskCnt(ByVal PageNumber As Integer, ByVal Filter As String) As Integer
        Dim tcnt As Integer
        sql = "select count(*) from pmTasks where " & Filter & " and tasknum <= " & PageNumber & " and subtask = '0'"
        tcnt = tasks.Scalar(sql)
        Dim scnt As Integer
        sql = "select count(*) from pmTasks where " & Filter & " and tasknum < " & PageNumber & " and subtask <> '0'"
        scnt = tasks.Scalar(sql)
        Dim wcnt = tcnt + scnt
        Return wcnt
    End Function
    Private Sub AddFail()
        Dim comp As String = lblco.Value
        tasks.Open()
        PopCompFailList(comp)
        PopFailList(comp)
        tasks.Dispose()
    End Sub
    Private Sub GetLists()
        'msglbl.Text = ""
        'cid = lblcid.Value
        sql = "select top 1 * from pmtasks where " & CntFilter
        dr = tasks.GetRdrData(sql)
        While dr.Read
            cid = dr.Item("compid").ToString
            lblcid.Value = cid
            sid = dr.Item("siteid").ToString
            lblsid.Value = sid
            did = dr.Item("deptid").ToString
            lbldid.Value = did
            clid = dr.Item("cellid").ToString
            lblclid.Value = clid
            eqid = dr.Item("eqid").ToString
            lbleqid.Value = eqid
        End While
        dr.Close()
        If clid <> "0" And clid <> "" Then
            sql = "select cell_name from cells where cellid = '" & clid & "'"
            Dim cellname As String = tasks.strScalar(sql)
            If cellname <> "No Cells" Then
                chk = "no"
            Else
                chk = "yes"
            End If
        Else
            chk = "no"
        End If
        lblchk.Value = chk
        'cid = "0" 
        'lblcid.Value = cid
        sql = "select ttid, tasktype " _
        + "from pmTaskTypes where compid = '" & cid & "' or compid = '0' and tasktype <> 'Select' order by compid"
        dr = tasks.GetRdrData(sql)
        ddtype.DataSource = dr
        Try
            ddtype.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddtype.Items.Insert(0, New ListItem("Select"))
        ddtype.Items(0).Value = 0
        'orig
        dr = tasks.GetRdrData(sql)
        ddtypeo.DataSource = dr
        Try
            ddtypeo.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddtypeo.Items.Insert(0, New ListItem("Select"))
        ddtypeo.Items(0).Value = 0

        sql = "select ptid, pretech " _
        + "from pmPreTech where compid = '" & cid & "' or compid = '0' order by compid"
        dr = tasks.GetRdrData(sql)
        ddpt.DataSource = dr
        ddpt.DataTextField = "pretech"
        ddpt.DataValueField = "ptid"
        Try
            ddpt.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddpt.Items.Insert(0, New ListItem("None"))
        ddpt.Items(0).Value = 0
        'orig
        dr = tasks.GetRdrData(sql)
        ddpto.DataSource = dr
        ddpto.DataTextField = "pretech"
        ddpto.DataValueField = "ptid"
        Try
            ddpto.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddpto.Items.Insert(0, New ListItem("None"))
        ddpto.Items(0).Value = 0
    End Sub
    Private Sub LoadPage(ByVal PageNumber As Integer, ByVal Filter As String)
        'tasks.Open()
        '**** Added for PM Manager
        eqid = lbleqid.Value
        Try
            'lblhaspm.Value = tasks.HasPM(eqid)
            sql = "select haspm from equipment where eqid = '" & eqid & "'"
            dr = tasks.GetRdrData(sql)
            While dr.Read
                lblhaspm.Value = dr.Item("haspm").ToString
            End While
            dr.Close()
        Catch ex As Exception
            'lblhaspm.Value = "0" 'tasksadd.HasPM(eqid)
        End Try

        If lblsb.Value = "0" Then
            Dim scnt As Integer
            scnt = SubCount(PageNumber, Filter)
            lblscnt.Text = scnt
            lblsubcount.Text = scnt
            If Len(Filter) = 0 Then
                Filter = " subtask = 0"
            Else
                Filter = Filter & " and subtask = 0"
            End If

            lblpgholder.Value = PageNumber
            lblspg.Text = "0"
            cbloto.Checked = False
            cbcs.Checked = False
        End If
        Dim tskcnt, optcnt As Integer
        CntFilter = lblfiltcnt.Value
        sql = "select count(*) from pmtasks where " & CntFilter
        Try
            tskcnt = tasks.Scalar(sql)
        Catch ex As Exception
            tskcnt = tasksadd.Scalar(sql)
        End Try
        If PageNumber = 0 And tskcnt > 0 Then
            PageNumber = 1
            lblpg.Text = PageNumber
        End If
        'End If

        If tskcnt = 0 Then
            lblpg.Text = "0"
            lblcnt.Text = tskcnt
            Dim strMessage As String = "No Tasks entered for this Function yet"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Tables = "pmtasks"
            PK = "pmtskid"
            Try
                dr = tasks.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            Catch ex As Exception
                tasks.Open()
                dr = tasksadd.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            End Try
            Dim ismeter As String
            While dr.Read
                ismeter = dr.Item("usemeter").ToString
                lblusemeter.Value = dr.Item("usemeter").ToString
                lblt.Value = dr.Item("tasknum").ToString
                ttid = dr.Item("pmtskid").ToString
                lbltaskid.Value = ttid
                lblst.Value = dr.Item("subtask").ToString
                Try
                    ddtype.SelectedValue = dr.Item("ttid").ToString
                Catch ex As Exception
                    ddtype.SelectedIndex = 0
                End Try
                Try
                    ddtypeo.SelectedValue = dr.Item("origttid").ToString 'orig
                Catch ex As Exception
                End Try

                Try
                    ddpt.SelectedValue = dr.Item("ptid").ToString
                Catch ex As Exception
                    ddpt.SelectedIndex = 0
                End Try
                Try
                    ddpto.SelectedValue = dr.Item("origptid").ToString 'orig
                Catch ex As Exception
                    ddpto.SelectedIndex = 0
                End Try

                txtdesc.Value = dr.Item("taskdesc").ToString
                txtodesc.Value = dr.Item("otaskdesc").ToString 'orig
                txtqty.Text = dr.Item("qty").ToString
                txtqtyo.Text = dr.Item("origqty").ToString 'orig
                txttr.Text = dr.Item("tTime").ToString
                txttro.Text = dr.Item("origtTime").ToString 'orig
                txtordt.Text = dr.Item("origrdt").ToString
                txtrdt.Text = dr.Item("rdt").ToString
                lblcind.Value = dr.Item("compindex").ToString
                'ddtaskstat.SelectedValue = dr.Item("taskstatusid").ToString
                Try
                    ddtaskstat.SelectedValue = dr.Item("taskstatus").ToString
                Catch ex As Exception
                    ddtaskstat.SelectedIndex = 0
                End Try

                If dr.Item("lotoid").ToString = "1" Then
                    cbloto.Checked = True
                Else
                    cbloto.Checked = False
                End If
                If dr.Item("conid").ToString = "1" Then
                    cbcs.Checked = True
                Else
                    cbcs.Checked = False
                End If
                co = dr.Item("comid").ToString
                If Len(co) = 0 Or co = "" Then
                    co = "0"
                End If
                lblco.Value = co
                tdcompnum.InnerHtml = dr.Item("compnum").ToString
                tdoskill.InnerHtml = dr.Item("origskill").ToString
                tdskill.InnerHtml = dr.Item("skill").ToString
                tdostat.InnerHtml = dr.Item("origrd").ToString
                tdstat.InnerHtml = dr.Item("rd").ToString
                tdofreq.InnerHtml = dr.Item("origfreq").ToString
                tdfreq.InnerHtml = dr.Item("freq").ToString
                txtcQty.Text = dr.Item("cqty").ToString

                lbltpmhold.Value = dr.Item("tpmhold").ToString
            End While
            'lblpar.Value = "task"
            dr.Close()
            If lbltpmhold.Value = "1" Then
                tdtpmhold.InnerHtml = "(TPM Task)"
                lbltpmalert.Value = "no"
                imgdeltpm.Attributes.Add("class", "visible")
                btntpm.Visible = False
            Else
                If ismeter = "1" Then
                    lbltpmhold.Value = "0"
                    tdtpmhold.InnerHtml = ""
                    'lbltpmalert.Value = ""
                    imgdeltpm.Attributes.Add("class", "details")
                    btntpm.Visible = False
                Else
                    lbltpmhold.Value = "0"
                    tdtpmhold.InnerHtml = ""
                    'lbltpmalert.Value = ""
                    imgdeltpm.Attributes.Add("class", "details")
                    btntpm.Visible = True
                End If

            End If
            Dim cind As String = lblcind.Value
            If co <> "0" Then
                lblco.Value = co
                'tasks.Open()
                PopCompFailList(co)
                PopFailList(co)
                PopTaskFailModes(co)
                PopoTaskFailModes(co)
                UpdateFailStats(co)
                'tasks.Dispose()
                'PopDesc(co)
                lblco.Value = co
                chk = "comp"

            Else
                lbfaillist.Items.Clear()
                lbfailmodes.Items.Clear()
                lbofailmodes.Items.Clear()
                lbCompFM.Items.Clear()
                lblco.Value = co
                chk = "comp"
            End If
            lblpar.Value = "comp"
            sql = "select xstatus from equipment where eqid = '" & eqid & "'"
            Try
                xstatus = tasks.strScalar(sql)
            Catch ex As Exception
                xstatus = "no"
            End Try
            lblxstatus.Value = xstatus
            ghostoff = lblghostoff.Value
            If xstatus = "yes" And ghostoff = "yes" Then
                ddtypeo.Attributes.Add("onchange", "GetType('o', this.value);")
                ddtaskstat.Attributes.Add("onchange", "CheckDel(this.value);")
                ddtype.Attributes.Add("onchange", "GetType('d', this.value);")
                ddpto.Attributes.Add("onchange", "")
                txtqtyo.Attributes.Add("onchange", "")
                txttro.Attributes.Add("onkeyup", "filldown('o', this.value);")
                txttr.Attributes.Add("onkeyup", "filldown('r', this.value);")
                txtordt.Attributes.Add("onkeyup", "")
            Else
                ddtypeo.Attributes.Add("onchange", "GetType('o', this.value);")
                ddtaskstat.Attributes.Add("onchange", "CheckDel(this.value);")
                ddtype.Attributes.Add("onchange", "GetType('d', this.value);")
                ddpto.Attributes.Add("onchange", "document.getElementById('ddpt').value=this.value;")
                txtqtyo.Attributes.Add("onchange", "document.getElementById('txtqty').value=this.value;")
                txttro.Attributes.Add("onkeyup", "filldown('o', this.value);")
                txttr.Attributes.Add("onkeyup", "filldown('r', this.value);")
                txtordt.Attributes.Add("onkeyup", "document.getElementById('txtrdt').value=this.value;")
            End If
            UpDateRevs(ttid)
            fuid = lblfuid.Value
            If lblsb.Value = "0" Then
                lblpg.Text = PageNumber
                If Len(Filter) = 0 Then
                    Filter = " subtask = 0"
                Else
                    Filter = "funcid = " & fuid & " and subtask = 0"
                End If
                sql = "select count(*) from pmtasks where " & Filter
                Try
                    tskcnt = tasks.Scalar(sql)
                Catch ex As Exception
                    tskcnt = tasksadd.Scalar(sql)
                End Try
            Else
                lblpg.Text = lblpgholder.Value
                PageNumber = lblpgholder.Value
                If Len(Filter) = 0 Then
                    Filter = " subtask = 0"
                Else
                    Filter = "funcid = " & fuid & " and subtask = 0"
                End If
                sql = "select count(*) from pmtasks where " & Filter


            End If
            lblcnt.Text = tskcnt
            'CheckPgNav(tskcnt, PageNumber)
            btnaddsubtask.Enabled = True

        End If
        'tasks.Dispose()
    End Sub
    Private Sub PopFailList(ByVal comp As String)
        Dim lang As String = lblfslang.Value
        ttid = lbltaskid.Value
        sql = "select compfailid, failuremode " _
         + "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
         + "select failid from pmtaskfailmodes where comid = '" & comp & "')" ' and taskid = '" & ttid & "')"
        sql = "usp_getcfall_tskna '" & comp & "','" & lang & "'"
        'Try
        dr = tasks.GetRdrData(sql)
        'Catch ex As Exception
        'dr = tasksadd.GetRdrData(sql)
        'End Try

        lbfaillist.DataSource = dr
        lbfaillist.DataTextField = "failuremode"
        lbfaillist.DataValueField = "compfailid"
        Try
            lbfaillist.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
    End Sub

    Private Sub PopCompFailList(ByVal comp As String)
        Dim lang As String = lblfslang.Value
        ttid = lbltaskid.Value
        sql = "select compfailid, failuremode " _
         + "from componentfailmodes where comid = '" & comp & "'"
        sql = "usp_getcfall_co '" & comp & "','" & lang & "'"
        'Try
        dr = tasks.GetRdrData(sql)
        'Catch ex As Exception
        'dr = tasksadd.GetRdrData(sql)
        'End Try

        lbCompFM.DataSource = dr
        lbCompFM.DataTextField = "failuremode"
        lbCompFM.DataValueField = "compfailid"
        Try
            lbCompFM.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
    End Sub
    Private Sub PopTaskFailModes(ByVal comp As String)
        Dim lang As String = lblfslang.Value
        ttid = lbltaskid.Value
        'sql = "select * from pmtaskfailmodes where taskid = '" & ttid & "'"
        sql = "select * from pmtaskfailmodes where comid = '" & comp & "' and taskid = '" & ttid & "'"
        sql = "usp_getcfall_tsk '" & comp & "','" & ttid & "','" & lang & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        lbfailmodes.DataSource = dr
        lbfailmodes.DataTextField = "failuremode"
        lbfailmodes.DataValueField = "failid"
        Try
            lbfailmodes.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
    End Sub
    Private Sub PopoTaskFailModes(ByVal comp As String)
        Dim lang As String = lblfslang.Value
        ttid = lbltaskid.Value
        sql = "select * from pmotaskfailmodes where taskid = '" & ttid & "'"
        sql = "usp_getcfall_tsko '" & ttid & "','" & lang & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try


        lbofailmodes.DataSource = dr
        lbofailmodes.DataTextField = "failuremode"
        lbofailmodes.DataValueField = "failid"
        Try
            lbofailmodes.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
    End Sub
    Private Sub UpdateFailStats(ByVal comp As String)
        Dim fmcnt, fmcnta As Integer
        sql = "select count(*) from ComponentFailModes where comid = '" & comp & "'"
        Try
            fmcnt = tasks.Scalar(sql)
        Catch ex As Exception
            fmcnt = tasksadd.Scalar(sql)
        End Try

        sql = "select count(distinct failid) from pmTaskFailModes where comid = '" & comp & "'"
        Try
            fmcnta = tasks.Scalar(sql)
        Catch ex As Exception
            fmcnta = tasksadd.Scalar(sql)
        End Try

        fmcnta = fmcnt - fmcnta
    End Sub
    Private Sub UpDateRevs(ByVal Filter As String)
        sql = "select created, revised from pmtasks where pmtskid = '" & Filter & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        Dim cr, rv As String
        While dr.Read
            cr = dr.Item("created").ToString
            rv = dr.Item("revised").ToString
        End While
        dr.Close()
        If Len(rv) = 0 Then
            rv = "NA"
        End If
        'imgClock.Attributes.Add("onmouseover", "return overlib('Task Created: " & cr & "<br> Task Revised: " & rv & "')")
        'imgClock.Attributes.Add("onmouseout", "return nd()")
    End Sub


    Private Sub GetItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Dim fcnt, fcnt2 As Integer
        If oaid <> "0" Then
            sql = "select count(*) from pmTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and oaid = '" & oaid & "'"
        Else
            sql = "select count(*) from pmTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and (oaid is null or oaid = '0')"
        End If
        fcnt = tasks.Scalar(sql)
        sql = "select count(*) from pmTaskFailModes where taskid = '" & ttid & "'"
        fcnt2 = tasks.Scalar(sql)
        If fcnt2 >= 5 Then
            Dim strMessage As String = "Limit is 5 Failure Modes per Task"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf fcnt > 0 Then
            Dim strMessage As String = "Failure Modes Must Be Unique Within a Task"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Try
                sql = "sp_addTaskFailureMode " & ttid & ", " & fi & ", '" & f & "', '" & comp & "', 'dev','" & oaid & "'"
                tasks.Update(sql)
                Dim Item As ListItem
                Dim fm As String
                Dim ipar As Integer = 0
                For Each Item In lbfailmodes.Items
                    f = Item.Text.ToString
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                    If Len(fm) = 0 Then
                        fm = f & "(___)"
                    Else
                        fm += " " & f & "(___)"
                    End If
                Next
                fm = tasks.ModString2(fm)
                sql = "update pmtasks set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
                tasks.Update(sql)

            Catch ex As Exception

            End Try
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
        End If
    End Sub
    Private Sub RemItems(ByVal f As String, ByVal fi As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Try
            sql = "sp_delTaskFailureMode '" & ttid & "', '" & fi & "','" & oaid & "'"
            tasks.Update(sql)
            Dim fm As String
            Dim ipar As Integer = 0
            Dim Item As ListItem
            For Each Item In lbfailmodes.Items
                f = Item.Text.ToString
                ipar = f.LastIndexOf("(")
                If ipar <> -1 Then
                    f = Mid(f, 1, ipar)
                End If
                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
            Next
            fm = tasks.ModString2(fm)
            sql = "update pmtasks set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
            tasks.Update(sql)
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GetoItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Dim fcnt, fcnt2 As Integer
        If oaid <> "0" Then
            sql = "select count(*) from pmoTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and oaid = '" & oaid & "'"
        Else
            sql = "select count(*) from pmoTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and oaid is null"
        End If
        fcnt = tasks.Scalar(sql)
        sql = "select count(*) from pmoTaskFailModes where taskid = '" & ttid & "'"
        fcnt2 = tasks.Scalar(sql)
        If fcnt2 >= 5 Then
            Dim strMessage As String = "Limit is 5 Failure Modes per Task"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf fcnt > 0 Then
            Dim strMessage As String = "Failure Modes Must Be Unique Within a Task"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Try
                sql = "sp_addTaskFailureMode " & ttid & ", " & fi & ", '" & f & "', '" & comp & "', 'opt', '" & oaid & "'"
                tasks.Update(sql)
                Dim Item As ListItem
                Dim ofm, of1, ofi As String
                Dim ipar As Integer = 0
                For Each Item In lbofailmodes.Items
                    of1 = Item.ToString
                    ipar = of1.LastIndexOf("(")
                    If ipar <> -1 Then
                        of1 = Mid(of1, 1, ipar)
                    End If

                    If Len(ofm) = 0 Then
                        ofm = of1 & "(___)"
                    Else
                        ofm += " " & of1 & "(___)"
                    End If
                Next
                ofm = tasks.ModString2(ofm)
                sql = "update pmtasks set ofm1 = '" & ofm & "' where pmtskid = '" & ttid & "'"
                tasks.Update(sql)
            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Sub RemoItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Try
            sql = "sp_deloTaskFailureMode " & ttid & ", " & fi & ", '" & f & "','" & oaid & "'"
            tasks.Update(sql)
            Dim Item As ListItem
            Dim ofm, of1, ofi As String
            Dim ipar As Integer = 0
            For Each Item In lbofailmodes.Items
                of1 = Item.ToString
                ipar = of1.LastIndexOf("(")
                If ipar <> -1 Then
                    of1 = Mid(of1, 1, ipar)
                End If
                If Len(ofm) = 0 Then
                    ofm = of1 & "(___)"
                Else
                    ofm += " " & of1 & "(___)"
                End If
            Next
            ofm = tasks.ModString2(ofm)
            sql = "update pmtasks set ofm1 = '" & ofm & "' where pmtskid = '" & ttid & "'"
            tasks.Update(sql)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ibfromo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibfromo.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        ttid = lbltaskid.Value
        Dim comp As String = lblco.Value
        tasks.Open()

        For Each Item In lbofailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                RemoItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopoTaskFailModes(comp)
            UpdateFailStats(comp)
            lbltaskid.Value = ttid
            'SaveTask()
            'lblenable.Value = "0"
            PageNumber = lblpg.Text
            Filter = lblfilt.Value
            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub
    Private Sub ibReuse_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibReuse.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        ttid = lbltaskid.Value
        Dim comp As String = lblco.Value
        tasks.Open()

        For Each Item In lbCompFM.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            lbltaskid.Value = ttid
            'SaveTask()
            'lblenable.Value = "0"
            PageNumber = lblpg.Text
            Filter = lblfilt.Value
            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try

        tasks.Dispose()

    End Sub

    Private Sub ibtoo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtoo.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        ttid = lbltaskid.Value
        Dim comp As String = lblco.Value
        tasks.Open()
        lblenable.Value = "0"
        For Each Item In lbCompFM.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetoItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            PopoTaskFailModes(comp)
            UpdateFailStats(comp)
            'LoadPage(PageNumber, Filter) 'test

            'SaveTask()
            'lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            'PopComp()
            Filter = lblfilt.Value

            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub

    Private Sub ibToTask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibToTask.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        ttid = lbltaskid.Value
        Dim comp As String = lblco.Value
        tasks.Open()

        For Each Item In lbfaillist.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetItems(f, fi, comp, oa)
            End If
        Next

        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            UpdateFM()
            'LoadPage(PageNumber, Filter) 'test
            'SaveTask()
            'lblenable.Value = "0"
            lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            'PopComp()
            Filter = lblfilt.Value

            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub

    Private Sub UpdateFM()
        Dim ttid As String = lbltaskid.Value
        sql = "usp_UpdateFM1 '" & ttid & "'"
        tasks.Update(sql)
    End Sub

    Private Sub ibFromTask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibFromTask.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        ttid = lbltaskid.Value
        Dim comp As String = lblco.Value
        tasks.Open()

        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                RemItems(f, fi, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            UpdateFM()
            'LoadPage(PageNumber, Filter) 'test
            'SaveTask()
            'lblenable.Value = "0"
            lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            'PopComp()
            Filter = lblfilt.Value

            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub

    Private Sub ibCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibCancel.Click
        If Len(lblfuid.Value) <> 0 Then
            tasks.Open()
            GetLists()
            Filter = lblfilt.Value
            LoadPage(PageNumber, Filter)
            tasks.Dispose()
        Else
            Dim strMessage As String = "No Function Record Loaded Yet!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
    Private Sub SaveTask()
        Dim t, st, tid, typ, fre, des, odes, rmt, pt, ski, qty, tr, eqs, lot, cs, ci, cn, cin As String
        Dim typor, typstror, freor, frestror, ptor, ptstror As String
        Dim skior, skistror, qtyor, tror, eqsor, eqsstror As String
        Dim typstr, frestr, rmtstr, ptstr, skistr, eqsstr, cqty, taskstat, taskstatid As String
        If Len(lblfuid.Value) <> 0 Then
            t = lblt.Value

            st = "0" 'lblsb.Value
            If ddtaskstat.SelectedIndex <> 0 Then
                taskstatid = ddtaskstat.SelectedValue
                taskstat = ddtaskstat.SelectedItem.ToString
                taskstat = Replace(taskstat, "'", Chr(180), , , vbTextCompare)
            Else
                taskstatid = "0"
            End If

            If ddtype.SelectedIndex <> 0 Then
                typ = ddtype.SelectedValue
                typstr = ddtype.SelectedItem.ToString
                typstr = Replace(typstr, "'", Chr(180), , , vbTextCompare)
            Else
                typ = "0"
            End If
            If ddtypeo.SelectedIndex <> 0 Then
                typor = ddtypeo.SelectedValue 'orig +
                typstror = ddtypeo.SelectedItem.ToString
                typstror = Replace(typstror, "'", Chr(180), , , vbTextCompare)
            Else
                typor = "0"
            End If

            If ddpt.SelectedIndex <> 0 Then
                pt = ddpt.SelectedValue
                ptstr = ddpt.SelectedItem.ToString
                ptstr = Replace(ptstr, "'", Chr(180), , , vbTextCompare)
            Else
                pt = "0"
            End If
            If ddpto.SelectedIndex <> 0 Then
                ptor = ddpto.SelectedValue 'orig +
                ptstror = ddpto.SelectedItem.ToString
                ptstror = Replace(ptstror, "'", Chr(180), , , vbTextCompare)
            Else
                ptor = "0"
            End If

            qty = txtqty.Text

            If Len(qty) = 0 Then
                qty = "1"
            End If
            qtyor = txtqtyo.Text 'orig 
            If Len(qtyor) = 0 Then
                qtyor = "1"
            End If
            tr = txttr.Text
            tror = txttro.Text 'orig
            If tror = "" Then
                tror = "0"
            End If

            Dim ordt, rdt As String
            ordt = txtordt.Text
            rdt = txtrdt.Text
            If ordt = "" Then
                ordt = "0"
            End If
            If rdt = "" Then
                rdt = "0"
            End If

            des = txtdesc.Value
            des = tasks.ModString2(des)
            odes = txtodesc.Value 'orig
            odes = tasks.ModString2(odes)
            If cbloto.Checked = True Then
                lot = "1"
            Else
                lot = "0"
            End If
            If cbcs.Checked = True Then
                cs = "1"
            Else
                cs = "0"
            End If
            tid = lbltaskid.Value
            Filter = lblfilt.Value
            fuid = lblfuid.Value
            cqty = txtcQty.Text


            sql = "select compindex from components where func_id = '" & fuid & "' and comid = '" & ci & "'"
            Try
                dr = tasks.GetRdrData(sql)
            Catch ex As Exception
                dr = tasksadd.GetRdrData(sql)
            End Try

            If dr.Read Then
                cin = dr.Item("compindex").ToString
            End If
            dr.Close()
            If Len(cin) = 0 Then
                cin = "0"
            End If

            Dim fm As String
            Dim Item As ListItem
            Dim f, fi As String
            Dim ipar As Integer = 0
            For Each Item In lbfailmodes.Items
                f = Item.Text.ToString
                ipar = f.LastIndexOf("(")
                If ipar <> -1 Then
                    f = Mid(f, 1, ipar)
                End If
                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
            Next
            fm = tasks.ModString2(fm)
            Dim ofm, ofs, ofi As String
            For Each Item In lbofailmodes.Items
                ofs = Item.ToString
                ipar = ofs.LastIndexOf("(")
                If ipar <> -1 Then
                    ofs = Mid(ofs, 1, ipar)
                End If

                If Len(ofm) = 0 Then
                    ofm = ofs & "(___)"
                Else
                    ofm += " " & ofs & "(___)"
                End If
            Next
            ofm = tasks.ModString2(ofm)
            Dim ttid As String = lbltaskid.Value
            tchk = lblusetot.Value
            If Len(ttid) <> 0 Then
                If tchk = "1" Then
                    sql = "update pmTasks set " _
                            + "taskdesc = '" & des & "', " _
                            + "otaskdesc = '" & odes & "', " _
                            + "qty = '" & qty & "', " _
                            + "tTime = '" & tr & "', " _
                            + "rdt = '" & rdt & "', " _
                            + "fm1 = '" & fm & "', " _
                            + "ofm1 = '" & ofm & "', " _
                            + "ptid = '" & pt & "', " _
                            + "pretech = '" & ptstr & "', " _
                            + "origptid = '" & ptor & "', " _
                            + "origpretech = '" & ptstror & "', " _
                            + "lotoid = '" & lot & "', " _
                            + "conid = '" & cs & "', " _
                            + "ttid = '" & typ & "', " _
                            + "tasktype = '" & typstr & "', " _
                            + "taskstatus = '" & taskstat & "', " _
                            + "origttid = '" & typor & "', " _
                            + "origtasktype = '" & typstror & "', " _
                            + "cqty = '" & cqty & "', " _
                            + "revised = getDate() " _
                            + "where " & Filter & " and pmtskid = '" & ttid & "'" 'tasknum = '" & t & "' and subtask = '" & st & "'"
                Else
                    sql = "update pmTasks set " _
                            + "taskdesc = '" & des & "', " _
                            + "otaskdesc = '" & odes & "', " _
                            + "qty = '" & qty & "', " _
                            + "tTime = '" & tr & "', " _
                            + "origqty = '" & qtyor & "', " _
                            + "origtTime = '" & tror & "', " _
                            + "origrdt = '" & ordt & "', " _
                            + "rdt = '" & rdt & "', " _
                            + "fm1 = '" & fm & "', " _
                            + "ofm1 = '" & ofm & "', " _
                            + "ptid = '" & pt & "', " _
                            + "pretech = '" & ptstr & "', " _
                            + "origptid = '" & ptor & "', " _
                            + "origpretech = '" & ptstror & "', " _
                            + "lotoid = '" & lot & "', " _
                            + "conid = '" & cs & "', " _
                            + "ttid = '" & typ & "', " _
                            + "tasktype = '" & typstr & "', " _
                            + "taskstatus = '" & taskstat & "', " _
                            + "origttid = '" & typor & "', " _
                            + "origtasktype = '" & typstror & "', " _
                            + "cqty = '" & cqty & "', " _
                            + "revised = getDate() " _
                            + "where " & Filter & " and pmtskid = '" & ttid & "'" 'tasknum = '" & t & "' and subtask = '" & st & "'"
                End If
                
                'Try
                Try
                    tasks.Update(sql)
                Catch ex As Exception
                    tasksadd.Update(sql)
                End Try
                sid = lblsid.Value
                sql = "usp_updateTaskDDIndexes '" & ttid & "', '" & sid & "'"
                Try
                    tasks.Update(sql)
                Catch ex As Exception
                    Try
                        tasksadd.Update(sql)
                    Catch ex1 As Exception

                    End Try

                End Try
                Dim usr As String = HttpContext.Current.Session("username").ToString()
                Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)

                eqid = lbleqid.Value
                '**** Added for PM Manager
                Dim hpm As String = lblhaspm.Value
                If hpm = "1" Then
                    sql = "update equipment set modifieddate = getDate(), hasmod = 1, modifiedby = '" & ustr & "' " _
                + "where eqid = '" & eqid & "'"
                Else
                    sql = "update equipment set modifieddate = getDate(), modifiedby = '" & ustr & "' " _
                + "where eqid = '" & eqid & "'"
                End If
                Try
                    tasks.Update(sql)
                Catch ex As Exception
                    tasksadd.Update(sql)
                End Try
                Dim currcnt As String = lblcurrsb.Value
                Dim cscnt As String = lblcurrcs.Value
                If st = 0 Then
                    PageNumber = lblpg.Text
                Else
                    PageNumber = currcnt + cscnt
                End If
                field = "funcid"
                val = fuid
                Filter = field & " = " & val '& " and tasknum = " & PageNumber
                Try
                    LoadPage(PageNumber, Filter)
                Catch ex As Exception
                    LoadPage(PageNumber, Filter)
                End Try

                lblenable.Value = "0"
                lblsave.Value = "yes"
                eqid = lbleqid.Value
                Try
                    tasks.UpMod(eqid)
                Catch ex As Exception
                    tasksadd.UpMod(eqid)
                End Try

                'Catch ex As Exception
                'Dim strMessage As String = "Problem Saving Record\nPlease review selections and try again."
                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                'End Try
            Else
                Dim strMessage As String = "Problem Saving Record\nPlease review selections and try again."
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

        Else
            Dim strMessage As String = "No Function Record Loaded Yet!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Private Sub DelRevised()
        Dim tid As String = lbltaskid.Value
        tasks.Open()
        SaveTask()
        sql = "update pmTasks set " _
            + "taskdesc = NULL, " _
            + "qty = '0', " _
            + "tTime = '0', " _
            + "rdid = '', " _
            + "rd = '', " _
            + "rdt = '0', " _
            + "fm1 = '', " _
            + "skillid = '', " _
            + "skill = '', " _
            + "ptid = '', " _
            + "pretech = '', " _
            + "freqid = '', " _
            + "freq = '', " _
            + "lotoid = '', " _
            + "conid = '', " _
            + "ttid = '', " _
            + "tasktype = '', " _
            + "cqty = '', " _
            + "taskstatus = 'Delete', " _
            + "revised = getDate() " _
            + "where pmtskid = '" & tid & "'"
        sid = lblsid.Value
        tasks.Update(sql)
        sql = "usp_updateTaskDDIndexes '" & tid & "', '" & sid & "'"
        tasks.Update(sql)
        PageNumber = lblpg.Text
        Filter = lblfilt.Value
        eqid = lbleqid.Value
        tasks.UpMod(eqid)
        LoadPage(PageNumber, Filter)
        lblenable.Value = "0"
        tasks.Dispose()
    End Sub

    Protected Sub btntpm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntpm.Click
        tasks.Open()
        UpTPM()
        Filter = lblfilt.Value
        PageNumber = lblpg.Text
        LoadPage(PageNumber, Filter)
        tasks.Dispose()
    End Sub
    Private Sub UpTPM()
        Dim usemeter As String = lblusemeter.Value
        If usemeter <> "1" Then
            eqid = lbleqid.Value
            cid = lblcid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            fuid = lblfuid.Value
            ttid = lbltaskid.Value
            Dim ttyp As String = ddtype.SelectedValue.ToString
            'If ttyp = "2" Or ttyp = "3" Or ttyp = "5" Then
            Dim tasknum As String = lblt.Value
            Dim user As String = HttpContext.Current.Session("username").ToString '"PM Administrator"
            'create procedure [dbo].[usp_copyTPMTask] (@cid int, 
            '@sid int, @did int, @clid int, @eqid int , @fuid int, @user varchar(50), @pmtskid int) as
            sql = "usp_copyTPMTask '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & user & "','" & ttid & "','" & tasknum & "'"
            tasks.Update(sql)
            'ddskill.SelectedValue = "2"
            'SaveTask2()
            'Else
            'Dim strMessage As String =  tmod.getmsg("cdstr402" , "PMOptTasks.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'End If
        Else
            Dim strMessage As String = "Can`t Add Task with Meter Based Frequency to TPM"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If


    End Sub
End Class
