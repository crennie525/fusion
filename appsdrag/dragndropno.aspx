<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="dragndropno.aspx.vb" Inherits="lucy_r12.dragndropno" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>dragndropno</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcss1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    <link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll3.js"></script>
    <style type="text/css">
    .plainlabeltmp
{
	font-family:MS Sans Serif, arial, sans-serif, Verdana;
	font-size:8pt;
	text-decoration:none;
	color:black;
}
.plainlabelredtmp
{
	font-family:MS Sans Serif, arial, sans-serif, Verdana;
	font-size:8pt;
	text-decoration:none;
	color:red;
}

    </style>
    <script language="JavaScript" type="text/javascript">
    <!--
        function enableprofile() {
            disbelow();
            document.getElementById("ddskillo").disabled = false;
            document.getElementById("txtqtyo").disabled = false;
            document.getElementById("txtfreqo").disabled = false;
            document.getElementById("ddeqstato").disabled = false;
            document.getElementById("tdskill").className = "label";
            document.getElementById("tdskillqty").className = "label";
            document.getElementById("tdfreq").className = "label";
            document.getElementById("tdstat").className = "label";
            document.getElementById("ddskillo").selectedIndex = 0;
            document.getElementById("txtqtyo").value = "1";
            document.getElementById("txtfreqo").value = "";
            document.getElementById("ddeqstato").selectedIndex = 0;
            document.getElementById("lbladdprof").value = "yes";

        }
        function moveit(cur, eqid, taskid, istpm, tpmtskid) {
            var eReturn = window.showModalDialog("../appsdrag/taskmovedialog.aspx?curr=" + cur + "&eqid=" + eqid + "&pmtskid=" + taskid + "&istpm=" + istpm + "&tpmtskid=" + tpmtskid + "&date=" + Date(), "", "dialogHeight:450px; dialogWidth:450px; resizable=yes");
         if (eReturn) {
             document.getElementById("lblsubmit").value = "reload";
             document.getElementById("form1").submit();
         }
        }
        function copyfunctask(who, pmtskid, fuid, coid) {
            //alert()
            var eqid = document.getElementById("lbleqid").value;
            var sid = document.getElementById("lblsid").value;
            var ustr = document.getElementById("lbluser").value;
            //"pmo123e2copytofunc.aspx?sid=" + sid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&who=" + who + "&pmtskid=" + pmtskid + "&ustr=" + ustr
            var eReturn = window.showModalDialog("../appspmo123grid/pmo123e2copytofuncdialog.aspx?sid=" + sid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&who=" + who + "&pmtskid=" + pmtskid + "&ustr=" + ustr + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:650px; resizable=yes");
            if (eReturn) {
                var ret = eReturn;
                var retarr = ret.split("~");
                var flist = retarr[0];
                var clist = retarr[1];
                //alert(clist)
                var fbox = document.getElementById("lblflist").value;
                var cbox = document.getElementById("lblclist").value;
                //alert(cbox)
                var flst = fbox.split("~");
                for (var i = 0; i < flst.length; i++) {
                    var chk = flst[i];
                    var chkarr = chk.split("^");
                    var chk1 = chkarr[0];
                    //alert(flist + "," + chk1)
                    if (chk != "") {
                        if (flist == chk1) {
                            var chk2 = chkarr[1];
                            //alert(chk2)
                            var chk2arr = chk2.split(",");
                            var chk3 = chk2arr[0];
                            var chk4 = chk2arr[1];
                            fclose(chk3, chk4, "0");
                        }
                        
                    }
                }
                if (clist != "") {
                    var clst = cbox.split("~");
                    for (var i = 0; i < clst.length; i++) {
                        var chk = clst[i];
                        var chkarr = chk.split("^");
                        var chk1 = chkarr[0];
                        //alert(clist + "," + chk1)
                        if (chk != "") {
                            if (clist == chk1) {
                                var chk2 = chkarr[1];
                                //alert(chk2)
                                var chk2arr = chk2.split(",");
                                var chk3 = chk2arr[0];
                                var chk4 = chk2arr[1];
                                fclose(chk3, chk4, "YES");
                            }

                        }
                    }
                }
                //document.getElementById("lbldocopy").value = "yes";
                saveopttasks();
            }

        }
        function getdyk() {
            //closelst();
            //did you know drag and drop message
            var eReturn = window.showModalDialog("../appsopt/pmdocmsg.aspx", "", "dialogHeight:450px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
            }
        }
        function getsgrid(tid, fuid, istpm) {
            if (istpm == "no") {
                var eqid = document.getElementById("lbleqid").value;
                var cid = "0";
                var sid = document.getElementById("lblsid").value;
                var eReturn = window.showModalDialog("../apps/GSubDialog.aspx?sid=" + sid + "&cid=" + cid + "&tid=" + tid + "&fuid=" + fuid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:680px; dialogWidth:850px; resizable=yes");
                if (eReturn) {
                    //document.getElementById("form1").submit();
                }
            }
            else {
                alert("This Task is Designated as a TPM Task")
            }
        }
        function getdt(ttid, eqid, coid, istpm) {
        if (istpm == "no") {
            var who = "revdnd";
            var sid = document.getElementById("lblsid").value;
            var ustr = document.getElementById("lbluser").value;
            var usetot = document.getElementById("lblusetot").value;
            var eReturn = window.showModalDialog("../appspmo123grid/pmo123e2rdetsdialog.aspx?eqid=" + eqid + "&ttid=" + ttid + "&coid=" + coid + "&who=" + who + "&sid=" + sid + "&ustr=" + ustr + "&usetot=" + usetot + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:600px; resizable=yes");
            if (eReturn) {

            }
        }
        else {
            alert("This Task is Designated as a TPM Task")
        }

    }
    function deltask(taskid, fuid, tasknum, istpm) {
        if (istpm == "no") {
            var conf = confirm("Are you sure you want to Delete this Task?")
            if (conf == true) {
                document.getElementById("lbldeltaskid").value = taskid;
                document.getElementById("lbldelfuid").value = fuid;
                document.getElementById("lbldeltasknum").value = tasknum;
                document.getElementById("lbldodel").value = "yes";
                saveopttasks();
            }
            else {
                alert("Action Cancelled")
            }
        }
        else {
            alert("This Task is Designated as a TPM Task")
        }

    }
        function getcompadd(ptid, eqid, fuid) {
            var ro = "0";
            var tli = "4";
            var cid = "0";
            var chk = "";
            var sid = document.getElementById("lblsid").value;
            var schk = document.getElementById("trsave").className;
            if (schk != "details") {
                alert("Please Save Tasks Before Adding a Component");
            }
            else {
                var eReturn = window.showModalDialog("../equip/addcomp2dialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&tli=" + tli + "&chk=" + chk + "&sid=" + sid + "&date=" + Date() + "&ro=" + ro, getcompaddCallback, "dialogHeight:700px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    getcompaddCallback(eReturn);
                }
            }
        }

        function getcompaddCallback(eReturn) {
            document.getElementById("lblsubmit").value = "reload";
            document.getElementById("form1").submit();
        }

        function getcompcopy(ptid, eqid, fuid) {
        var ro = "0";
            var tli = "4";
            var cid = "0";
            var chk = "";
            var sid = document.getElementById("lblsid").value;
            var schk = document.getElementById("trsave").className;
            if (schk == "details") {
                alert("Please Save Tasks Before Adding a Component")
            }
            else {
                var eReturn = window.showModalDialog("CompCopyMiniDialog.aspx?usr=&cid=" + cid + "&sid=" + sid + "&eqid=" + eqid + "&fuid=" + fuid + "&ro=" + ro + "&did=" + did + "&clid=" + clid + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:880px; resizable=yes");
                if (eReturn) {
                    document.getElementById("lblsubmit").value = "reload";
                    document.getElementById("form1").submit();
                }
            }
        }
        function gotoother() {
            var eqid = document.getElementById("lbleqid").value;
            var eqnum = document.getElementById("lbleqnum").value;
            var proc = document.getElementById("lblproc").value;
            var ustr = document.getElementById("lbluser").value;
            var cap = "../appsdrag/dragndropno.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&proc=" + proc + "&ustr=" + ustr;
            window.location = cap;
        }

        function copytask(taskid, fuid, tasknum) {
            document.getElementById("lblcopytaskid").value = taskid;
            document.getElementById("lblcopyfuid").value = fuid;
            document.getElementById("lblcopytasknum").value = tasknum;
            document.getElementById("lbldocopy").value = "yes";
            saveopttasks();
        }
       

        function saveopttasks() {
            var taskids = document.getElementById("lbltaskids").value;
            //alert(taskids)
            var taskidsarr = taskids.split("~");
            var task;
            var lbl1 = document.getElementById("lbl1");
            var dchk = "";
            for (var i = 0; i < taskidsarr.length; i++) {
                var chk = taskidsarr[i];
                if (dchk == "") {
                    dchk = chk;
                    try {
                        task = document.getElementById(chk).value;
                        var chkarr = chk.split("-");
                        if (task != "") {
                            if (lbl1 == "") {
                                lbl1.value = chkarr[1] + ";" + chkarr[2] + ";" + task;
                            }
                            else {
                                lbl1.value += "~" + chkarr[1] + ";" + chkarr[2] + ";" + task;
                            }
                        }
                    }
                    catch (err) {
                        //user removed task box
                    }
                }
                else {
                    if (dchk != chk) {
                        try {
                            task = document.getElementById(chk).value;
                            var chkarr = chk.split("-");
                            if (task != "") {
                                if (lbl1 == "") {
                                    lbl1.value = chkarr[1] + ";" + chkarr[2] + ";" + task;
                                }
                                else {
                                    lbl1.value += "~" + chkarr[1] + ";" + chkarr[2] + ";" + task;
                                }
                            }
                        }
                        catch (err) {
                            //user removed task box
                        }
                    }
                    dchk = chk;
                }


            }
            //if(lbl1.value!="") {
            document.getElementById("lbltaskids").value = "";
            document.getElementById("lblsubmit").value = "savetasks";
            document.getElementById("form1").submit();
            //}
        }
        var newid = 5;
        function addnode(id, img) {
            var idarr = id.split("-")
            var curhtml = document.getElementById(id).innerHTML;
            newid += 1;
            var taskid = newid + "-" + idarr[1] + "-" + idarr[2];
            if (document.getElementById("lbltaskids").value == "") {
                document.getElementById("lbltaskids").value = taskid;
            }
            else {
                document.getElementById("lbltaskids").value += "~" + taskid;
            }
            curhtml += "<TEXTAREA rows='3' style='width: 560px;font-family:MS Sans Serif, arial, sans-serif, Verdana;font-size:12px;text-decoration:none;color:red;' id='" + taskid + "'></TEXTAREA>"
            document.getElementById(id).className = "view";
            document.getElementById(id).innerHTML = curhtml;
            var str = document.getElementById(img).src
            var lst = str.lastIndexOf("/") + 1
            var loc = str.substr(lst)
            if (loc == 'minusgray.gif') {
                loc = 'plus.gif'
                document.getElementById(img).src = '../images/appbuttons/bgbuttons/plus.gif';
        }
            if (loc == 'plus.gif') {
                fclose(id, img);
            }
            document.getElementById("trsave").className = "view";
        }

        function getpm() {
            var eqid = document.getElementById("lbleqid").value;
            var eReturn = window.showModalDialog("../appsdrag/totlookdialog.aspx?typ=1&eqid=" + eqid, getpmCallback, "dialogHeight:410px; dialogWidth:410px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    getpmCallback(eReturn);
                }
            }
        }

        function getpmCallback(eReturn) {
            document.getElementById("lblmeterid").value = "";
            document.getElementById("lblmeterfreq").value = "";
            document.getElementById("lbldayfreq").value = "";
            document.getElementById("lblmeterunit").value = "";
            document.getElementById("lblmaxdays").value = "";
            document.getElementById("lblmeter").value = "";

            var retarr = eReturn.split(",")
            //if (chk=="1") {
            document.getElementById("lblskillid").value = retarr[0];
            document.getElementById("lblskill").value = retarr[1];
            document.getElementById("ddskillo").value = retarr[0];

            document.getElementById("lblfreqid").value = retarr[2];
            document.getElementById("lblfreq").value = retarr[3];
            document.getElementById("txtfreqo").value = retarr[3];

            document.getElementById("lblstatusid").value = retarr[4];
            document.getElementById("lblstatus").value = retarr[5];
            document.getElementById("ddeqstato").value = retarr[4];

            document.getElementById("txtqtyo").value = retarr[6];
            document.getElementById("lblskillqty").value = retarr[6];

            //skillid & "," & skill & "," & freqid & "," & freq & "," & rdid & "," & rd & "," & sqty & "," & ismeter _
            //+"," & meterid & "," & meter & "," & meterunit & "," & meterfreq & "," & maxdays
            var mchk = retarr[7];
            if (mchk == "1") {
                document.getElementById("lblmeterid").value = retarr[8];
                document.getElementById("lblmeterfreq").value = retarr[11];
                document.getElementById("lbldayfreq").value = retarr[3];
                document.getElementById("txtfreqo").value = retarr[3];
                document.getElementById("lblmeterunit").value = retarr[10];
                document.getElementById("lblmaxdays").value = retarr[12];
                document.getElementById("lblmeter").value = retarr[9];
            }
            else {

                document.getElementById("lblmeterid").value = "";
                document.getElementById("lblmeterfreq").value = "";
                document.getElementById("lbldayfreq").value = "";
                document.getElementById("lblmeterunit").value = "";
                document.getElementById("lblmaxdays").value = "";
                document.getElementById("lblmeter").value = "";
            }
            document.getElementById("lbladdprof").value = "yes";
            checkpm();
            //}
        }

        function changepm(who) {
            if (who == "skill") {
                document.getElementById("lblskillid").value = document.getElementById("ddskillo").value;
                var sklst = document.getElementById("ddskillo");
                document.getElementById("lblskill").value = sklst.options[sklst.selectedIndex].text
            }
            if (who == "freq") {
               
                document.getElementById("lblfreq").value = document.getElementById("txtfreqo").value;
            }
            if (who == "stat") {
                document.getElementById("lblstatusid").value = document.getElementById("ddeqstato").value;
                var stlst = document.getElementById("ddeqstato");
                document.getElementById("lblstatus").value = stlst.options[stlst.selectedIndex].text
            }
            disbelow();
        }

        function getmeter() {
            var chk1 = document.getElementById("lbladdprof").value;
            if (chk1 == "yes") {
                var chk = document.getElementById("trsave").className;
                var freq = document.getElementById("txtfreqo").value;
                if (chk == "view") {
                    var conf = confirm("Warning!\nSome New Tasks Might Not Have Been Saved\nDo You Wish To Coninue?")
                    if (conf == true) {

                        document.getElementById("lblmeterid").value = "";
                        document.getElementById("lblmeterfreq").value = "";
                        document.getElementById("lbldayfreq").value = "";
                        document.getElementById("lblmeterunit").value = "";
                        document.getElementById("lblmaxdays").value = "";
                        document.getElementById("lblmeter").value = "";
                        document.getElementById("txtfreqo").value = "";

                        var eqid = document.getElementById("lbleqid").value;
                        var eqnum = document.getElementById("lbleqnum").value;
                        var pmtskid = ""; // document.getElementById("lbltaskid").value;
                        var fuid = ""; // document.getElementById("lblfuid").value;
                        var skillid = document.getElementById("lblskillid").value;
                        var skillqty = document.getElementById("lblskillqty").value;
                        var rdid = document.getElementById("lblstatusid").value;
                        var func = ""//document.getElementById("lblfunc").value;
                        var skill = document.getElementById("lblskill").value;
                        var rd = document.getElementById("lblstatus").value;
                        var meterid = ""; // document.getElementById("lblmeterid").value;
                        var varflg = 0;

                        if (varflg == 0) {
                            var eReturn = window.showModalDialog("../equip/usemeterdialog.aspx?typ=dad&eqid=" + eqid + "&pmtskid=" + pmtskid + "&fuid=" + fuid + "&skillid=" + skillid
                                   + "&skillqty=" + skillqty + "&rdid=" + rdid + "&func=" + func + "&skill=" + skill + "&rd=" + rd
                                   + "&meterid=" + meterid + "&rdt=" + "&eqnum=" + eqnum, addMeterCallBack1, "dialogHeight:600px; dialogWidth:900px;resizable=yes");
                            if (eReturn) {
                                addMeterCallBack1(eReturn);
                            } //eReturn
                        } //varflg
                    } //conf
                    else {
                        alert("Action Cancelled");
                    }
                } //chk
                else {

                    document.getElementById("lblmeterid").value = "";
                    document.getElementById("lblmeterfreq").value = "";
                    document.getElementById("lbldayfreq").value = "";
                    document.getElementById("lblmeterunit").value = "";
                    document.getElementById("lblmaxdays").value = "";
                    document.getElementById("lblmeter").value = "";
                    document.getElementById("txtfreqo").value = "";

                    var eqid = document.getElementById("lbleqid").value;
                    var eqnum = document.getElementById("lbleqnum").value;
                    var pmtskid = ""; // document.getElementById("lbltaskid").value;
                    var fuid = ""; // document.getElementById("lblfuid").value;
                    var skillid = document.getElementById("lblskillid").value;
                    var skillqty = document.getElementById("lblskillqty").value;
                    var rdid = document.getElementById("lblstatusid").value;
                    var func = ""//document.getElementById("lblfunc").value;
                    var skill = document.getElementById("lblskill").value;
                    var rd = document.getElementById("lblstatus").value;
                    var meterid = ""; // document.getElementById("lblmeterid").value;
                    var varflg = 0;

                    if (varflg == 0) {
                        var eReturn = window.showModalDialog("../equip/usemeterdialog.aspx?typ=dad&eqid=" + eqid + "&pmtskid=" + pmtskid + "&fuid=" + fuid + "&skillid=" + skillid
                                   + "&skillqty=" + skillqty + "&rdid=" + rdid + "&func=" + func + "&skill=" + skill + "&rd=" + rd
                                   + "&meterid=" + meterid + "&rdt=" + "&eqnum=" + eqnum, addMeterCallBack2, "dialogHeight:600px; dialogWidth:900px;resizable=yes");
                        if (eReturn) {
                            addMeterCallBack2(eReturn);
                        } 
                    } 
                }
            }
        }

        function addMeterCallBack1(eReturn) {
            var ret = eReturn.split("~");
            if (ret[0] != "0") {
                document.getElementById("lblmeterid").value = ret[0];
                document.getElementById("lblmeterfreq").value = ret[1];
                document.getElementById("lblfreq").value = ret[2];
                document.getElementById("lbldayfreq").value = ret[2];
                document.getElementById("txtfreqo").value = ret[2];
                document.getElementById("lblmeterunit").value = ret[3];
                document.getElementById("lblmaxdays").value = ret[4];
                document.getElementById("lblmeter").value = ret[5];
            } 
            else {
                document.getElementById("txtfreqo").value = freq;
            }
        }

        function addMeterCallBack2(eReturn) {
            var ret = eReturn.split("~");
            if (ret[0] != "0") {
                document.getElementById("lblmeterid").value = ret[0];
                document.getElementById("lblmeterfreq").value = ret[1];
                document.getElementById("lblfreq").value = ret[2];
                document.getElementById("lbldayfreq").value = ret[2];
                document.getElementById("txtfreqo").value = ret[2];
                document.getElementById("lblmeterunit").value = ret[3];
                document.getElementById("lblmaxdays").value = ret[4];
                document.getElementById("lblmeter").value = ret[5];
            } 
            else {
                document.getElementById("txtfreqo").value = freq;
            }
        }

        function checkpm() {
            var chk = document.getElementById("lbladdprof").value;
            if (chk == "yes") {
                document.getElementById("ddskillo").disabled = false;
                document.getElementById("txtqtyo").disabled = false;
                document.getElementById("txtfreqo").disabled = false;
                document.getElementById("ddeqstato").disabled = false;

                var skill = document.getElementById("lblskillid").value;
                var sklst = document.getElementById("ddskillo");
                var sknum = sklst.options[sklst.selectedIndex].text

                var skillqty = document.getElementById("txtqtyo").value;

                var freq = document.getElementById("lblfreq").value;

                var frnum = document.getElementById("txtfreqo").value;

                var stat = document.getElementById("lblstatusid").value;
                var stlst = document.getElementById("ddeqstato");
                var stnum = stlst.options[stlst.selectedIndex].text

                if (skill == "" && skill != "0") {
                    alert("Skill Required")
                }
                else if (skillqty == "" || skillqty == "0") {
                    alert("Skill Quantity Required")
                }
                else if (frnum == "" || frnum == "0") {
                    alert("Frequency Required")
                }
                else if (stat == "" || stat == "0") {
                    alert("Status Required")
                }
                else {
                    document.getElementById("lblsubmit").value = "changepm";
                    document.getElementById("form1").submit();
                }
            }
        }




        function getpms() {
            var chk = document.getElementById("lblusetot").value;
            var eqid = document.getElementById("lbleqid").value;
            var sid = document.getElementById("lblsid").value;
            var eqnum = document.getElementById("lbleqnum").value;
            if (chk == "1") {
                //alert(document.getElementById("lblsavetasks").value)
                var chk1 = document.getElementById("lblsavetasks").value;
                //alert(chk1)
                if (chk1 == "1") {
                //alert("need save")
                    var conf = confirm("Warning!\nThis will save and clear any current Function Tasks from Drag View")
                    if (conf == true) {
                        disbelow();
                        var eReturn = window.showModalDialog("../appsdrag/totpmsdialog.aspx?typ=1&eqid=" + eqid + "&sid=" + sid + "&eqnum=" + eqnum, "", "dialogHeight:610px; dialogWidth:860px; resizable=yes");
                        if (eReturn) {
                            document.getElementById("lblnoedit").value = "yes";
                            document.getElementById("lbladdprof").value = "yes";
                            checkpm_noalert();
                        }
                    }
                    else {
                        alert("Action Cancelled")
                    }
                }
                else {
                    disbelow();
                    var eReturn = window.showModalDialog("../appsdrag/totpmsdialog.aspx?typ=1&eqid=" + eqid + "&sid=" + sid + "&eqnum=" + eqnum, "", "dialogHeight:610px; dialogWidth:860px; resizable=yes");
                    if (eReturn) {
                    document.getElementById("lblnoedit").value = "yes";
                        document.getElementById("lbladdprof").value = "yes";
                        checkpm_noalert();
                    }
                }

            }
            else {
                alert("Use Total Time Not Selected")
            }
        }
        function gethelp2() {
            var eReturn = window.showModalDialog("../appsdrag/draghelp2.aspx", "", "dialogHeight:250px; dialogWidth:680px; resizable=yes");
            if (eReturn) {
            }
        }
        function gethelp1() {
            var eReturn = window.showModalDialog("../appsdrag/draghelp1.aspx", "", "dialogHeight:250px; dialogWidth:680px; resizable=yes");
            if (eReturn) {
            }
        }
        function goback() {
            document.getElementById("lblsubmit").value = "goback";
            document.getElementById("form1").submit();
        }
        function checkpm_noalert() {
            var chk = document.getElementById("lbladdprof").value;
            if (chk == "yes") {
                document.getElementById("ddskillo").disabled = false;
                document.getElementById("txtqtyo").disabled = false;
                document.getElementById("txtfreqo").disabled = false;
                document.getElementById("ddeqstato").disabled = false;

                var skill = document.getElementById("lblskillid").value;
                var sklst = document.getElementById("ddskillo");
                var sknum = sklst.options[sklst.selectedIndex].text

                var skillqty = document.getElementById("txtqtyo").value;

                var freq = document.getElementById("lblfreqid").value;

                var frnum = document.getElementById("lblfreq").value;

                var stat = document.getElementById("lblstatusid").value;
                var stlst = document.getElementById("ddeqstato");
                var stnum = stlst.options[stlst.selectedIndex].text

                //var eq = document.getElementById("lbleqid").value;
                //var eqlst = document.getElementById("ddeq");
                //var eqnum = eqlst.options[eqlst.selectedIndex].text

                if (skill == "" && skill != "0") {
                    //alert("Skill Required")
                }
                else if (skillqty == "" || skillqty == "0") {
                    //alert("Skill Quantity Required")
                }
                else if (frnum == "" || frnum == 0) {
                    //alert("Frequency Required") && freq != "0"
                }
                else if (stat == "" || stat == 0) {
                    //alert("Status Required")//&& stat != "0"
                }
                //else if(eq==""&&eq!="0") {
                //alert("Equipment Required")
                //}
                else {
                    document.getElementById("lblsubmit").value = "reload";
                    document.getElementById("form1").submit();
                    //var hrefstr = "start=yes&eqid=" + eq + "&eqnum=" + eqnum + "&skill=" + skill + "&sknum=" + sknum + "&freq=" + freq + "&frnum=" + frnum + "&stat=" + stat + "&stnum=" + stnum;
                    //window.parent.handledragpm(hrefstr);
                }
            }
        }
        function checkfclose() {
            var fbox = document.getElementById("lblfclose").value;
            //alert(fbox)
            if (fbox != "") {
                var flst = fbox.split("~");
                for (var i = 0; i < flst.length; i++) {
                    var chk = flst[i];
                    if (chk != "") {
                        var chkarr = chk.split("^");
                        var chk1 = chkarr[0];
                        var chk2 = chkarr[1];
                        //alert(chk + "," + chk1 + "," + chk2)
                        fclose(chk1, chk2, "0");
                    }
                }
            }
        }

        function fclose(td, img, who) {
            //alert(td + "," + img + "," + who)
            var fbox = document.getElementById("lblfclose").value;
            //alert(fbox)
            var loc;
            if (who == "0") {
                loc = "plus.gif"
            }
            else {
                var str = document.getElementById(img).src
                var lst = str.lastIndexOf("/") + 1
                loc = str.substr(lst)

            }

            var newloc;
            //alert(str)
            if (loc == 'minusgray.gif') {

            }
            else if (loc == 'minus.gif') {
                document.getElementById(img).src = '../images/appbuttons/bgbuttons/plus.gif';
                try {
                    document.getElementById(td).className = "details";
                    //alert(td)
                }
                catch (err) {

                }
                //added for reopen
                newloc = img
                if (fbox != "") {
                    //alert("rem")
                    var remclose = td + "^" + newloc;
                    var remclose1 = "~" + td + "^" + newloc;
                    document.getElementById("lblfclose").value = fbox.replace(remclose1, "");
                    document.getElementById("lblfclose").value = fbox.replace(remclose, "");
                }
                //end reopen


            }
            else {
                document.getElementById(img).src = '../images/appbuttons/bgbuttons/minus.gif';
                try {
                    var dchki = td.indexOf("dfunc");
                    if (dchki == 0) {
                        document.getElementById(td).className = "view DragContainer4";
                    }
                    else {
                        document.getElementById(td).className = "view";
                    }
                }
                catch (err) {

                }
                //added for reopen
                //alert("add")
                newloc = img
                if (fbox == "") {
                    document.getElementById("lblfclose").value = td + "^" + newloc;
                }
                else {
                    document.getElementById("lblfclose").value = fbox + "~" + td + "^" + newloc;
                }
                //end added


            }
        }

        function gettaskedit(tasknum, fuid) {
            var chk = document.getElementById("lblusetot").value;
            var eReturn = window.showModalDialog("../appsdrag/taskeditdialog.aspx?tchk=" + chk + "&fuid=" + fuid + "&tasknum=" + tasknum, "", "dialogHeight:500px; dialogWidth:750px; resizable=yes");
            if (eReturn) {
            //alert(eReturn)
                document.getElementById("lblsubmit").value = "reload";
                document.getElementById("form1").submit();
            }
        }

        function getfunctasks(fuid, func) {
            var eReturn = window.showModalDialog("../appsdrag/dragdialog.aspx?typ=fu&fuid=" + fuid + "&func=" + func, "", "dialogHeight:670px; dialogWidth:600px; resizable=yes");
            if (eReturn) {

            }
        }
        function getcomptasks(comid, comp) {
            var eReturn = window.showModalDialog("../appsdrag/dragdialog.aspx?typ=co&comid=" + comid + "&comp=" + comp, "", "dialogHeight:670px; dialogWidth:600px; resizable=yes");
            if (eReturn) {

            }
        }

        var docflag = 0;
        function OpenFile(newstr, type) {
            docflag = 1;
            //handleapp();
            window.parent.handledoc("doc.aspx?file=" + newstr + "&type=" + type);
            //parent.main.location.href = "doc.aspx?file=" + newstr + "&type=" + type;

        }
        function CloseFile() {
            docflag = 1;
            //handleapp();
            window.parent.handledoc("OptHolder.aspx");
            //parent.main.location.href = "OptHolder.aspx";
        }

        function checktot() {
            var skill = document.getElementById("lblskillid").value;
            var qty = document.getElementById("txtqtyo").value;
            var freq = document.getElementById("txtfreqo").value;
            var stat = document.getElementById("ddeqstato").value;
            var cbtot = document.getElementById("cbtot");
            var usetot = document.getElementById("lblusetot").value;
            if (skill == "" || qty == "" || freq == "" || stat == "") {
                alert("No PM Skill, Qty, Frequency, or Status Selected")
                if (cbtot.checked == false) {
                    document.getElementById("cbtot").checked = true;
                }
                else {
                    document.getElementById("cbtot").checked = false;
                }
            }
            else {
                var cb = document.getElementById("cbtot")
                var chk = document.getElementById("lblusetot").value;
                if (chk == "0" && cb.checked == true) {
                    document.getElementById("lblsubmit").value = "maketot";
                    document.getElementById("form1").submit();
                }
                else if (chk == "1" && cb.checked == false) {
                    //put confirm here?
                    document.getElementById("lblsubmit").value = "untot";
                    document.getElementById("form1").submit();
                }
            }
        }
        function refreshit() {
            var taskids = document.getElementById("lbltaskids").value;
            var taskidsarr = taskids.split("~");
            var task;
            var lbl1 = document.getElementById("lbl1chk");
            var dchk = "";
            for (var i = 0; i < taskidsarr.length; i++) {
                var chk = taskidsarr[i];
                if (dchk == "") {
                    dchk = chk;
                    try {
                        task = document.getElementById(chk).value;
                        var chkarr = chk.split("-");
                        if (task != "") {
                            if (lbl1 == "") {
                                lbl1.value = chkarr[1] + ";" + chkarr[2] + ";" + task;
                            }
                            else {
                                lbl1.value += "~" + chkarr[1] + ";" + chkarr[2] + ";" + task;
                            }
                        }
                    }
                    catch (err) {
                        //user removed task box
                    }
                }
                else {
                    if (dchk != chk) {
                        try {
                            task = document.getElementById(chk).value;
                            var chkarr = chk.split("-");
                            if (task != "") {
                                if (lbl1 == "") {
                                    lbl1.value = chkarr[1] + ";" + chkarr[2] + ";" + task;
                                }
                                else {
                                    lbl1.value += "~" + chkarr[1] + ";" + chkarr[2] + ";" + task;
                                }
                            }
                        }
                        catch (err) {
                            //user removed task box
                        }
                    }
                    dchk = chk;
                }
            }
            if (lbl1.value != "") {
                lbl1.value = ""
                alert("Please Save Changes Below Before Refreshing")
            }
            else {
                var eqid = document.getElementById("lbleqid").value;
                var eqnum = document.getElementById("lbleqnum").value;
                var proc = document.getElementById("lblproc").value;
                var ustr = document.getElementById("lbluser").value;
                var cap = "../appsdrag/dragndropno.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&proc=" + proc + "&ustr=" + ustr;
                window.location = cap;
            }
        }
       
        function GetProcDiv() {
            //handleapp();
            eqid = document.getElementById("lbleqid").value;
            //var eqlst = document.getElementById("ddeq");
            var eqnum = document.getElementById("lbleqnum").value; //eqlst.options[eqlst.selectedIndex].text
            if (eqid != "") {
                var eReturn = window.showModalDialog("../appsopt/OptDialog.aspx?eqid=" + eqid + "&eqnum=" + eqnum, GetProcDivCallback, "dialogHeight:600px;dialogWidth:580px;resizable=yes;showclose:no");
                if (eReturn) {
                    GetProcDivCallback(eReturn);
                }
            }
        }

        function GetProcDivCallback(eReturn) {
            OpenFile(eReturn, "pdf");
            document.getElementById("lblsubmit").value = "pr";
            document.getElementById("form1").submit();
        }

        function retopt() {
            //alert(document.getElementById("lbl1").value)
            var ret = document.getElementById("lblret").value;
            if (ret != "") {
                window.parent.handledragreturn(ret);
            }
            else {
                checkfclose();
                //FreezeScreen("No PM Selected")
            }
            var skill = document.getElementById("lblskillid").value;
            var qty = document.getElementById("txtqtyo").value;
            var freq = document.getElementById("txtfreqo").value;
            if (freq == "0") {
                freq = document.getElementById("lblfreq").value;
                document.getElementById("txtfreqo").value = freq;
            }
            //alert(freq)
            var stat = document.getElementById("ddeqstato").value;
            //qty==""||
            if (skill == "" || freq == "" || stat == "") {
                document.getElementById("txttottime").disabled = true;
                document.getElementById("txtdowntime").disabled = true;
                document.getElementById("totlabel").className = "graylabel";
                document.getElementById("downlabel").className = "graylabel";
                document.getElementById("cbtot").disabled = true;
            }
            var usetot = document.getElementById("lblusetot").value;
            if (usetot == "1") {
                document.getElementById("cbtot").checked = true;
            }
            else {
                document.getElementById("cbtot").checked = false;
            }
        }
        function FreezeScreen(msg) {
            alert()
            scroll(0, 0);
            var outerPane = document.getElementById('FreezePane');
            var innerPane = document.getElementById('InnerFreezePane');
            if (outerPane) outerPane.className = 'FreezePaneOn6';
            if (innerPane) innerPane.innerHTML = msg;

            //var outerPane1 = document.getElementById('FreezePane1');
            //var innerPane1 = document.getElementById('InnerFreezePane1');
            //if (outerPane1) outerPane1.className = 'FreezePaneOn5';
            //if (innerPane1) innerPane1.innerHTML = msg;
        }

        function filldown(val) {
            var chk = document.getElementById("trsave").className;
            if (chk == "view") {
                var conf = confirm("Warning!\nSome New Tasks Might Not Have Been Saved\nDo You Wish To Coninue?")
                if (conf == true) {
                    document.getElementById("lblskillqty").value = val;
                    //disbelow();
                    document.getElementById("txttottime").disabled = true;
                    document.getElementById("txtdowntime").disabled = true;
                    document.getElementById("totlabel").className = "graylabel";
                    document.getElementById("downlabel").className = "graylabel";
                    document.getElementById("cbtot").disabled = true;
                    document.getElementById("tdcomps").innerHTML = "";
                    document.getElementById("trsave").className = "details";

                    //document.getElementById("lblmeterid").value = "";
                    //document.getElementById("lblmeterfreq").value = "";
                    //document.getElementById("lbldayfreq").value = "";
                    //document.getElementById("lblmeterunit").value = "";
                    //document.getElementById("lblmaxdays").value = "";
                    //document.getElementById("lblmeter").value = "";
                }
                else {
                    //document.getElementById("ddskillo").value = skill;
                    //document.getElementById("txtfreqo").value = freq;
                    //document.getElementById("ddeqstato").value = stat;
                    //alert(document.getElementById("ddskillo").valuez)
                    alert("Action Cancelled")
                }
            }
            else {
                document.getElementById("lblskillqty").value = val;

                document.getElementById("txttottime").disabled = true;
                document.getElementById("txtdowntime").disabled = true;
                document.getElementById("totlabel").className = "graylabel";
                document.getElementById("downlabel").className = "graylabel";
                document.getElementById("cbtot").disabled = true;
                document.getElementById("tdcomps").innerHTML = "";
                document.getElementById("trsave").className = "details";

                
            }
            
        }
        function filldownfreq(val) {
            var chk = document.getElementById("trsave").className;
            //alert(chk)
            if (chk == "view") {
                var conf = confirm("Warning!\nSome New Tasks Might Not Have Been Saved\nDo You Wish To Coninue?")
                if (conf == true) {
                    document.getElementById("lblfreq").value = val;
                    //disbelow();
                    document.getElementById("txttottime").disabled = true;
                    document.getElementById("txtdowntime").disabled = true;
                    document.getElementById("totlabel").className = "graylabel";
                    document.getElementById("downlabel").className = "graylabel";
                    document.getElementById("cbtot").disabled = true;
                    document.getElementById("tdcomps").innerHTML = "";
                    document.getElementById("trsave").className = "details";

                    document.getElementById("lblmeterid").value = "";
                    document.getElementById("lblmeterfreq").value = "";
                    document.getElementById("lbldayfreq").value = "";
                    document.getElementById("lblmeterunit").value = "";
                    document.getElementById("lblmaxdays").value = "";
                    document.getElementById("lblmeter").value = "";
                }
                else {
                    //document.getElementById("ddskillo").value = skill;
                    //document.getElementById("txtfreqo").value = freq;
                    //document.getElementById("ddeqstato").value = stat;
                    //alert(document.getElementById("ddskillo").value)
                    alert("Action Cancelled")
                }
            }
            else {
                document.getElementById("lblfreq").value = val;
                document.getElementById("txttottime").disabled = true;
                document.getElementById("txtdowntime").disabled = true;
                document.getElementById("totlabel").className = "graylabel";
                document.getElementById("downlabel").className = "graylabel";
                document.getElementById("cbtot").disabled = true;
                document.getElementById("tdcomps").innerHTML = "";
                document.getElementById("trsave").className = "details";

                document.getElementById("lblmeterid").value = "";
                document.getElementById("lblmeterfreq").value = "";
                document.getElementById("lbldayfreq").value = "";
                document.getElementById("lblmeterunit").value = "";
                document.getElementById("lblmaxdays").value = "";
                document.getElementById("lblmeter").value = "";
            }
            
        }
        function disbelow() {
            var chk = document.getElementById("trsave").className;
            var skill = document.getElementById("ddskillo").value;
            var qty = document.getElementById("txtqtyo").value;
            var freq = document.getElementById("txtfreqo").value;
            var stat = document.getElementById("ddeqstato").value;
            if (chk == "view") {
                var conf = confirm("Warning!\nSome New Tasks Might Not Have Been Saved\nDo You Wish To Coninue?")
                if (conf == true) {
                    document.getElementById("txttottime").disabled = true;
                    document.getElementById("txtdowntime").disabled = true;
                    document.getElementById("totlabel").className = "graylabel";
                    document.getElementById("downlabel").className = "graylabel";
                    document.getElementById("cbtot").disabled = true;
                    document.getElementById("tdcomps").innerHTML = "";
                    document.getElementById("trsave").className = "details";

                    document.getElementById("lblmeterid").value = "";
                    document.getElementById("lblmeterfreq").value = "";
                    document.getElementById("lbldayfreq").value = "";
                    document.getElementById("lblmeterunit").value = "";
                    document.getElementById("lblmaxdays").value = "";
                    document.getElementById("lblmeter").value = "";
                }
                else {
                    document.getElementById("ddskillo").value = skill;
                    document.getElementById("txtfreqo").value = freq;
                    document.getElementById("ddeqstato").value = stat;
                    //alert(document.getElementById("ddskillo").value)
                    alert("Action Cancelled")
                }

            }
            else {
                document.getElementById("txttottime").disabled = true;
                document.getElementById("txtdowntime").disabled = true;
                document.getElementById("totlabel").className = "graylabel";
                document.getElementById("downlabel").className = "graylabel";
                document.getElementById("cbtot").disabled = true;
                document.getElementById("tdcomps").innerHTML = "";
                document.getElementById("trsave").className = "details";
            }
        }
        function getfail(eqid, fuid, coid, sid, pmtskid, eqnum, func, comp, task) {
                var eReturn = window.showModalDialog("../apps/taskwodialog.aspx?eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&sid=" + sid + "&pmtskid=" + pmtskid + "&eqnum=" + eqnum + "&comp=" + comp + "&func=" + func + "&task=" + task, "", "dialogHeight:600px; dialogWidth:900px; resizable=yes");
                if (eReturn) {

                }
            }
        //-->    
    </script>
</head>
<body onload="retopt();">
    <form id="form1" method="post" runat="server">
    <div id="FreezePane" class="FreezePaneOff" align="center">
        <div id="InnerFreezePane" class="InnerFreezePane">
        </div>
    </div>
    <div style="z-index: 101; position: absolute; visibility: hidden" id="overDiv">
    </div>
    <table style="position: absolute; top: 2px; left: 2px" cellspacing="0" cellpadding="0"
        width="720">
        <tr>
            <td colspan="13">
                <table cellspacing="0" cellpadding="1">
                    <tr>
                    <td width="20"><img src="../images/appbuttons/minibuttons/addnew.gif" alt="" onclick="enableprofile();" onmouseover="return overlib('Add a New Profile', BELOW, LEFT)"
                                onmouseout="return nd()" /></td>
                        <td class="label" width="40" id="tdskill" runat="server">
                            Skill
                        </td>
                        <td width="140">
                            <asp:DropDownList ID="ddskillo" runat="server" Width="144px" CssClass="plainlabel">
                            </asp:DropDownList>
                        </td>
                        <td class="label" width="80" id="tdskillqty" runat="server">
                            Skill Qty
                        </td>
                        <td width="30">
                            <asp:TextBox ID="txtqtyo" runat="server" Width="30px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td class="label" width="80" id="tdfreq" runat="server">
                            Frequency
                        </td>
                        <td width="60">
                            <asp:TextBox ID="txtfreqo" runat="server" CssClass="plainlabel" Width="60px"></asp:TextBox>
                        </td>
                        <td width="20">
                            <img onclick="getmeter();" onmouseover="return overlib('View, Add, or Edit Meter Frequency', ABOVE, LEFT)"
                                onmouseout="return nd()" src="../images/appbuttons/minibuttons/meter1.gif" alt="" />
                        </td>
                        <td class="label" width="40" id="tdstat" runat="server">
                            Status
                        </td>
                        <td width="90">
                            <asp:DropDownList ID="ddeqstato" runat="server" Width="90px" CssClass="plainlabel">
                            </asp:DropDownList>
                        </td>
                        <td id="tdlooktot" width="20" runat="server">
                            <img alt="" onmouseover="return overlib('Select an Established Drag and Drop PM Cycle to Edit', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="getpm();" src="../images/appbuttons/minibuttons/magnifier.gif" />
                        </td>
                        <td id="tdtotpms" width="20" runat="server">
                            <img alt="" onmouseover="return overlib('View\Edit Original Total Values', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="getpms();" src="../images/appbuttons/minibuttons/pencil12.gif" />
                        </td>
                        <td width="20">
                            <img alt="" onmouseover="return overlib('Transfer PM Data to Drag-n-Drop Screen', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="checkpm();" src="../images/appbuttons/minibuttons/download.gif" />
                        </td>
                        <td width="20">
                            <img alt="" onclick="gethelp2();" src="../images/appbuttons/minibuttons/Q.gif" width="18"
                                height="18" />
                        </td>
                        <td width="20">
                            <img alt="" onclick="refreshit();" src="../images/appbuttons/minibuttons/refreshit.gif"
                                width="18" height="18" />
                        </td>
                        <td width="40" align="right">
                            <a id="hrefret" class="A1" onclick="goback();" href="#" runat="server">Return</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="13">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="thdrsinglft" width="26" align="left">
                            <img alt="" border="0" src="../images/appbuttons/minibuttons/dndbg.gif" />
                        </td>
                        <td id="tdloctop" class="thdrsingrt label" width="714" align="left">
                            Drag and Drop Task Screen
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="13">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="label" width="80" height="20">
                            Equipment
                        </td>
                        <td id="tdeq" class="bluelabellt" width="180" runat="server">
                        </td>
                        <td id="tdcbtot" class="label" width="85" runat="server">
                            <input id="cbtot" type="checkbox" name="cbtot" runat="server" />Use Total
                        </td>
                        <td width="20">
                            <img alt="" onclick="gethelp1();" src="../images/appbuttons/minibuttons/Q.gif" width="18"
                                height="18" />
                        </td>
                        <td id="totlabel" class="lillabel" width="120" runat="server">
                            Total Time (mins)
                        </td>
                        <td id="totbox" width="50" runat="server">
                            <asp:TextBox ID="txttottime" runat="server" Width="50px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td id="downlabel" class="lillabel" width="160" align="right" runat="server">
                            Total Down Time (mins)
                        </td>
                        <td width="50">
                            <asp:TextBox ID="txtdowntime" runat="server" Width="50px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Procedure
                        </td>
                        <td>
                            <asp:DropDownList ID="ddproc" runat="server" Width="170px" CssClass="plainlabel"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="label" colspan="2">
                            <img class="imgbutton" onmouseover="return overlib('Add Procedures to Optimize', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="GetProcDiv();" alt="" src="../images/appbuttons/minibuttons/upload.gif"
                                width="20" height="20" />
                            <img alt="" onclick="getdyk();" src="../images/appbuttons/minibuttons/Q.gif" height="18"
                                width="18" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="imgsavopt" onmouseover="return overlib('Save Current Tasks to Optimize', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="saveopttasks();" alt="../images/appbuttons/minibuttons/savedisk1.gif"
                                    src="../images/appbuttons/minibuttons/savedisk1.gif" runat="server" />
                        </td>
                        <td id="tdpm" class="bluelabellt" colspan="4" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td class="thdrsingg plainlabel" colspan="8">
                            Functions, Components &amp; Tasks
                        </td>
                    </tr>
                    <tr id="trsave" class="details">
                        <td colspan="8" align="center">
                            <table>
                                <tr>
                                    <td class="plainlabelred" align="center">
                                        You Have Added New Task Boxes That Need To Be Reviewed For Save
                                    </td>
                                    <td>
                                        <img id="Img1" onmouseover="return overlib('Save Current Tasks to Optimize', ABOVE, LEFT)"
                                            onmouseout="return nd()" onclick="saveopttasks();" alt="../images/appbuttons/minibuttons/savedisk1.gif"
                                            src="../images/appbuttons/minibuttons/savedisk1.gif" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td id="tdcomps" class="bluelabel" valign="top" colspan="8" runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lbl1" type="hidden" name="lbl1" runat="server" />
    <input id="lblmove" type="hidden" name="lblmove" runat="server" /><input id="lblremove"
        type="hidden" name="lblremove" runat="server" />
    <input id="lblnodes" type="hidden" name="lblnodes" runat="server" /><input id="lbleqnum"
        type="hidden" name="lbleqnum" runat="server" />
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server" /><input id="lblcid"
        type="hidden" name="lblcid" runat="server" />
    <input id="lbldocs" type="hidden" name="lbldocs" runat="server" />
    <input id="lblcreateobjs" type="hidden" name="lblcreateobjs" runat="server" />
    <input id="lblcreatetasks" type="hidden" name="lblcreatetasks" runat="server" />
    <input id="lblsave" type="hidden" name="lblsave" runat="server" />
    <input id="lblsavetasks" type="hidden" name="lblsavetasks" runat="server" />
    <input id="lbltasks" type="hidden" name="lbltasks" runat="server" />
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server" /><input id="lblcoid"
        type="hidden" name="lblcoid" runat="server" />
    <input id="lblsubmit" type="hidden" name="lblsubmit" runat="server" />
    <input id="xCoord" type="hidden" /><input id="yCoord" type="hidden" />
    <input id="lblskill" type="hidden" name="lblskill" runat="server" />
    <input id="lblskillid" type="hidden" name="lblskillid" runat="server" />
    <input id="lblfreq" type="hidden" name="lblfreq" runat="server" />
    <input id="lblfreqid" type="hidden" name="lblfreqid" runat="server" />
    <input id="lblstatus" type="hidden" name="lblstatus" runat="server" />
    <input id="lblstatusid" type="hidden" name="lblstatusid" runat="server" />
    <input id="lblstart" type="hidden" name="lblstart" runat="server" /><input id="lblcreatefobjs"
        type="hidden" name="lblcreatefobjs" runat="server" />
    <input id="lbltyp" type="hidden" name="lbltyp" runat="server" /><input id="lbldocpmid"
        type="hidden" name="lbldocpmid" runat="server" />
    <input id="lbldocpmstr" type="hidden" name="lbldocpmstr" runat="server" />
    <input id="lblsid" type="hidden" name="lblsid" runat="server" />
    <input id="lbldid" type="hidden" name="lbldid" runat="server" />
    <input id="lblclid" type="hidden" name="lblclid" runat="server" />
    <input id="lblchk" type="hidden" name="lblchk" runat="server" />
    <input id="lblret" type="hidden" name="lblret" runat="server" />
    <input id="lblusetot" type="hidden" name="lblusetot" runat="server" /><input id="lbloldtot"
        type="hidden" name="lbloldtot" runat="server" />
    <input id="lblskillqty" type="hidden" name="lblskillqty" runat="server" />
    <input id="lbloskillqty" type="hidden" name="lbloskillqty" runat="server" />
    <input id="lblodown" type="hidden" name="lblodown" runat="server" />
    <input id="lbloskill" type="hidden" name="lblskill" runat="server" />
    <input id="lbloskillid" type="hidden" name="lblskillid" runat="server" />
    <input id="lblofreq" type="hidden" name="lblfreq" runat="server" />
    <input id="lblofreqid" type="hidden" name="lblfreqid" runat="server" />
    <input id="lblostatus" type="hidden" name="lblstatus" runat="server" />
    <input id="lblostatusid" type="hidden" name="lblstatusid" runat="server" />
    <input id="lbllid" type="hidden" name="lbllid" runat="server" />
    <input id="lbltaskids" type="hidden" runat="server" /><input id="lblcap" type="hidden"
        name="lblcap" runat="server" />
    <input id="lblproc" type="hidden" runat="server" /><input id="lbl1chk" type="hidden"
        runat="server" />
    <input id="lblfclose" type="hidden" runat="server" />
    <input type="hidden" id="lblmeterid" runat="server" />
    <input type="hidden" id="lbldayfreq" runat="server" />
    <input type="hidden" id="lblmeterunit" runat="server" />
    <input type="hidden" id="lblmaxdays" runat="server" />
    <input type="hidden" id="lblmeterfreq" runat="server" />
    <input type="hidden" id="lblmeter" runat="server" />
    <input type="hidden" id="lbldeltaskid" runat="server" />
    <input type="hidden" id="lbldodel" runat="server" />
    <input type="hidden" id="lbldelfuid" runat="server" />
    <input type="hidden" id="lbldeltasknum" runat="server" />
    <input type="hidden" id="lblcopytaskid" runat="server" />
    <input type="hidden" id="lbldocopy" runat="server" />
    <input type="hidden" id="lblcopyfuid" runat="server" />
    <input type="hidden" id="lblcopytasknum" runat="server" />
    <input type="hidden" id="lbluser" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblghostoff" runat="server" />
    <input type="hidden" id="lblxstatus" runat="server" />
    <input type="hidden" id="lblflist" runat="server" />
    <input type="hidden" id="lblclist" runat="server" />
    <input type="hidden" id="lbltdpm" runat="server" />
    <input type="hidden" id="lbladdprof" runat="server" />
    <input type="hidden" id="lblnoedit" runat="server" />
    <input type="hidden" id="lblnewtot" runat="server" />
    <input type="hidden" id="lblnewdown" runat="server" />
    </form>
</body>
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../scripts/showModalDialog.js"></script>
</html>
