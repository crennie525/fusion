Imports System.Data.SqlClient
Imports System.Text

Public Class totlook
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim tots As New Utilities
    Dim eqid, typ As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents totdiv As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            typ = Request.QueryString("typ").ToString
            lbleqid.Value = eqid
            lbltyp.Value = typ
            tots.Open()
            GetTots(eqid, typ)
            tots.Dispose()
        End If
    End Sub
    Private Sub GetTots(ByVal eqid As String, ByVal typ As String)
        Dim skillid, skill, freqid, freq, rdid, rd, tot, pm, pmstr, sqty As String
        Dim ismeter, meterid, meter, meterunit, meterfreq, maxdays
        If typ = "1" Then
            sql = "select * from pmtottime where eqid = '" & eqid & "'"

            Dim sb As New System.Text.StringBuilder
            sb.Append("<table>")

            Dim noflg As Integer = 0

            dr = tots.GetRdrData(sql)
            While dr.Read
                noflg = 1
                skillid = dr.Item("skillid").ToString
                skill = dr.Item("skill").ToString
                freqid = dr.Item("freqid").ToString
                freq = dr.Item("freq").ToString
                rdid = dr.Item("rdid").ToString
                rd = dr.Item("rd").ToString
                sqty = dr.Item("skillqty").ToString

                ismeter = dr.Item("ismeter").ToString
                meterid = dr.Item("meterid").ToString
                meter = dr.Item("meter").ToString
                meterunit = dr.Item("meterunit").ToString
                meterfreq = dr.Item("meterfreq").ToString
                maxdays = dr.Item("maxdays").ToString

                pm = skill & "(" & sqty & ") / " & freq & " days / " & rd
                pmstr = skillid & "," & skill & "," & freqid & "," & freq & "," & rdid & "," & rd & "," & sqty & "," & ismeter _
                    + "," & meterid & "," & meter & "," & meterunit & "," & meterfreq & "," & maxdays
                If ismeter = "1" Then
                    pm += " - " & meter & " / " & meterunit & " / " & meterfreq
                End If
                sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getpm('" & pmstr & "');"">" & pm & "</a></td></tr>")

            End While
            dr.Close()
            If noflg = 0 Then
                sb.Append("<tr><td class=""redlabel"">No Records Found</td></tr>")
            End If
            sb.Append("</table>")
            totdiv.InnerHtml = sb.ToString
        End If
    End Sub
End Class
