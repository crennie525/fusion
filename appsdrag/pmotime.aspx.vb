﻿Imports System.Data.SqlClient
Imports System.Text

Public Class pmotime
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim pmot As New Utilities
    Dim eqid, istot, eqnum, eqdesc, sid, did, clid, lid As String
    Dim skillid, skillqty, rdid, meteridi, freq As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString '"973" '
            lbleq.Value = eqid
            pmot.Open()
            checktot(eqid)
            pmot.Dispose()

        End If
    End Sub
    Private Sub checktot(ByVal eqid As String)
        sql = "select usetot, eqnum, eqdesc, siteid, dept_id, cellid, locid from equipment where eqid = '" & eqid & "'"
        dr = pmot.GetRdrData(sql)
        While dr.Read
            istot = dr.Item("usetot").ToString
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            sid = dr.Item("siteid").ToString
            did = dr.Item("dept_id").ToString
            clid = dr.Item("cellid").ToString
            lid = dr.Item("locid").ToString
        End While
        dr.Close()
        lblsid.Value = sid
        lbldid.Value = did
        lblclid.Value = clid
        lbllid.Value = lid
        If istot = "" Then
            istot = "0"
        Else
            istot = "1"
        End If
        Dim sb As New StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""820"">")
        sb.Append("<tr><td class=""bigbold1"" colspan=""3"" align=""center"">PMO Time Overview</td></tr>")
        sb.Append("<tr><td class=""plainlabel"" colspan=""3"" align=""center"">" & Now & "</td></tr>")
        'sb.Append("<tr><td colspan=""3"" align=""center"">&nbsp;</td></tr>")
        sb.Append("<tr><td width=""140""></td><td width=""280""></td><td width=""400""></td></tr>")
        sb.Append("<tr><td class=""label"">Equipment#</td><td class=""plainlabel"" colspan=""2"">" & eqnum & "&nbsp;&nbsp;" & eqdesc & "</td></tr>")
        sb.Append("<tr><td class=""label"">Using Total Time</td><td class=""plainlabel"" colspan=""2"">" & istot & "</td></tr>")


        sql = "select count(*) from pmtasks where (taskstatus is null or len(taskstatus) = 0) and eqid = '" & eqid & "'"
        Dim twots As Integer = pmot.Scalar(sql)
        If twots = 0 Then
            sb.Append("<tr><td class=""label"">Tasks w/o Task Status</td><td class=""plainlabel"" colspan=""2"">" & twots & "</td></tr>")
        Else
            sb.Append("<tr><td class=""label""><a href=""#"" onclick=""gettsnotasks();"">Tasks w/o Task Status</a></td><td class=""plainlabel"" colspan=""2"">" & twots & "</td></tr>")
        End If


        sb.Append("<tr><td colspan=""3"" align=""center"">&nbsp;</td></tr>")

        sb.Append("<tr><td colspan=""3"" class=""label"">Original PM Profiles</td></tr>")

        sb.Append("<tr><td colspan=""3""><table cellspacing=""0"" cellpadding=""3"">")

        sb.Append("<tr><td width=""290""></td><td width=""40""></td><td width=""50""></td><td width=""60""></td><td width=""60""></td>")
        sb.Append("<td width=""80""></td><td width=""80""></td><td width=""80""></td><td width=""80""></td></tr>")


        sb.Append("<tr bgcolor=""#eeeded"" class=""label"">")
        sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"">PM</td>")
        sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"" align=""center"">Meter</td>")
        sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"" align=""center"">Tasks</td>")
        sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"" align=""center"">Total Time</td>")
        sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"" align=""center"">Total Down</td>")
        sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"" align=""center"">Frequency Per Year</td>")
        sb.Append("<td class=""blackcntr blacktop"" bgcolor=""#eeeded"" align=""center"">Total Time Year</td>")
        sb.Append("<td class=""blackright blacktop"" bgcolor=""#eeeded"" align=""center"">Total Down Year</td></tr>")

        sql = "usp_pmotimebreak '" & eqid & "','" & istot & "'"
        If istot = "" Then
            istot = "NO"
        Else
            istot = "YES"
        End If
        Dim pm, meterid, tasks, tt, td, fpy, tty, tdy, who As String
        Dim rflg As Integer = 0
        Dim ogtt As Integer = 0
        Dim ogtd As Integer = 0
        Dim rgtt As Long = 0
        Dim rgtd As Long = 0

        Dim hrs As Double = 0
        Dim ohrs As Double = "0"

        Dim dhrs As Double = 0
        Dim odhrs As Double = 0
        dr = pmot.GetRdrData(sql)
        While dr.Read
            pm = dr.Item("pm").ToString
            meterid = dr.Item("meterid").ToString
            tasks = dr.Item("tcnt").ToString
            tt = dr.Item("tasktot").ToString
            td = dr.Item("downtot").ToString
            fpy = dr.Item("freqyr").ToString
            tty = dr.Item("taskyr").ToString
            tdy = dr.Item("downyr").ToString
            who = dr.Item("who").ToString

            'skillid, skillqty, rdid, meterid, freq
            skillid = dr.Item("skillid").ToString
            skillqty = dr.Item("skillqty").ToString
            rdid = dr.Item("rdid").ToString
            freq = dr.Item("freq").ToString
            meteridi = dr.Item("meterid").ToString

            If meterid <> "" Then
                meterid = "YES"
            Else
                meterid = "NO"
            End If
            If who = "orig" Then

                sb.Append("<tr class=""plainlabel"">")
                sb.Append("<td class=""blackleft blacktop"">" & pm & "</td>")
                sb.Append("<td class=""blackleft blacktop"" align=""center"">" & meterid & "</td>")
                sb.Append("<td class=""blackleft blacktop"" align=""center"">" & tasks & "</td>")
                sb.Append("<td class=""blackleft blacktop"" align=""center"">" & tt & "</td>")
                sb.Append("<td class=""blackleft blacktop"" align=""center"">" & td & "</td>")
                sb.Append("<td class=""blackleft blacktop"" align=""center"">" & fpy & "</td>")
                sb.Append("<td class=""blackcntr blacktop"" align=""center"">" & tty & "</td>")
                sb.Append("<td class=""blackright blacktop"" align=""center"">" & tdy & "</td></tr>")
                ogtt += tty
                ogtd += tdy
            Else
                If rflg = 0 Then
                    rflg = 1
                    sb.Append("<tr><td class=""label"">Original Grand Total Time Per Year (Mins)</td><td class=""label"" colspan=""7"">" & ogtt & "</td></tr>")
                    sb.Append("<tr><td class=""label"">Original Grand Total Down Per Year (Mins)</td><td class=""label"" colspan=""7"">" & ogtd & "</td></tr>")

                    Dim dvar1 As Decimal
                    If ogtt <> 0 Then
                        dvar1 = ogtt / 60 'System.Math.Round()
                        ohrs = dvar1
                    Else
                        ohrs = 0
                    End If
                    If ogtd <> 0 Then
                        dvar1 = ogtd / 60 'System.Math.Round()
                        odhrs = dvar1
                    Else
                        odhrs = 0
                    End If
                    sb.Append("<tr><td class=""label"">Original Grand Total Time Per Year (Hours)</td><td class=""label"" colspan=""7"">" & ohrs & "</td></tr>")
                    sb.Append("<tr><td class=""label"">Original Grand Total Down Per Year (Hours)</td><td class=""label"" colspan=""7"">" & odhrs & "</td></tr>")

                    sb.Append("<tr><td colspan=""8"" align=""center"">&nbsp;</td></tr>")
                    sb.Append("<tr><td colspan=""8"" class=""label"">Revised PM Profiles</td></tr>")
                    sb.Append("<tr bgcolor=""#eeeded"" class=""label"">")
                    sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"">PM</td>")
                    sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"" align=""center"">Meter</td>")
                    sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"" align=""center"">Tasks</td>")
                    sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"" align=""center"">Total Time</td>")
                    sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"" align=""center"">Total Down</td>")
                    sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"" align=""center"">Frequency Per Year</td>")
                    sb.Append("<td class=""blackcntr blacktop"" bgcolor=""#eeeded"" align=""center"">Total Time Year</td>")
                    sb.Append("<td class=""blackright blacktop"" bgcolor=""#eeeded"" align=""center"">Total Down Year</td></tr>")
                End If
                sb.Append("<tr class=""plainlabel"">")
                sb.Append("<td class=""blackleft blacktop""><a href=""#"" onclick=""gettsnotasks1('" & skillid & "','" & skillqty & "','" & rdid & "','" & meteridi & "','" & freq & "')"">" & pm & "</a></td>")
                sb.Append("<td class=""blackleft blacktop"" align=""center"">" & meterid & "</td>")
                sb.Append("<td class=""blackleft blacktop"" align=""center"">" & tasks & "</td>")
                sb.Append("<td class=""blackleft blacktop"" align=""center"">" & tt & "</td>")
                sb.Append("<td class=""blackleft blacktop"" align=""center"">" & td & "</td>")
                sb.Append("<td class=""blackleft blacktop"" align=""center"">" & fpy & "</td>")
                sb.Append("<td class=""blackcntr blacktop"" align=""center"">" & tty & "</td>")
                sb.Append("<td class=""blackright blacktop"" align=""center"">" & tdy & "</td></tr>")
                Try
                    rgtt += tty
                    rgtd += tdy
                Catch ex As Exception

                End Try
                
            End If
            
        End While
        dr.Close()
        sb.Append("<tr><td class=""label"">Revised Grand Total Time Per Year (Mins)</td><td class=""label"" colspan=""7"">" & rgtt & "</td></tr>")
        sb.Append("<tr><td class=""label"">Revised Grand Total Down Per Year (Mins)</td><td class=""label"" colspan=""7"">" & rgtd & "</td></tr>")

        Dim dvar2 As Decimal
        hrs = rgtt
        If hrs <> 0 Then
            'hrs = System.Math.Round(hrs / 60)
            dvar2 = hrs / 60 'System.Math.Round()
            hrs = System.Math.Round(dvar2, 2) 'dvar2
        Else
            hrs = 0
        End If
        dhrs = rgtd
        If dhrs <> 0 Then
            'dhrs = System.Math.Round(dhrs / 60)
            dvar2 = dhrs / 60 'System.Math.Round()
            dhrs = System.Math.Round(dvar2, 2) 'dvar2
        Else
            dhrs = 0
        End If
        sb.Append("<tr><td class=""label"">Revised Grand Total Time Per Year (Hours)</td><td class=""label"" colspan=""7"">" & hrs & "</td></tr>")
        sb.Append("<tr><td class=""label"">Revised Grand Total Down Per Year (Hours)</td><td class=""label"" colspan=""7"">" & dhrs & "</td></tr>")

        sb.Append("<tr><td colspan=""8"" align=""center"">&nbsp;</td></tr>")

        Dim hrsper As Double = 0
        If ohrs <> 0 Then
            hrsper = System.Math.Round(((ohrs - hrs) / ohrs) * 100)
        End If
        Dim dhrsper As Double = 0
        If odhrs <> 0 Then
            dhrsper = System.Math.Round(((odhrs - dhrs) / odhrs) * 100)
        End If
        Dim hrsv As Double
        hrsv = ohrs - hrs
        Dim dhrsv As Double
        dhrsv = odhrs - dhrs
        sb.Append("<tr><td class=""label"">Hours Saved Per Year</td><td class=""label"" colspan=""7"">" & hrsv & "</td></tr>")
        sb.Append("<tr><td class=""label"">Percent</td><td class=""label"" colspan=""7"">" & hrsper & "%</td></tr>")

        sb.Append("<tr><td class=""label"">Down Hours Saved Per Year</td><td class=""label"" colspan=""7"">" & dhrsv & "</td></tr>")
        sb.Append("<tr><td class=""label"">Percent</td><td class=""label"" colspan=""7"">" & dhrsper & "%</td></tr>")

        sb.Append("</td></tr>")
        sb.Append("</table>")
        dvov.InnerHtml = sb.ToString


    End Sub
End Class