﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmotime.aspx.vb" Inherits="lucy_r12.pmotime" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PMO Time Review</title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript">
        function gettsnotasks() {
            var eq = document.getElementById("lbleq").value;
            fuid = "";
            if (eq != "") {
                var cid = "0";
                var sid = document.getElementById("lblsid").value;
                var did = document.getElementById("lbldid").value;
                var clid = document.getElementById("lblclid").value;
                var eqid = document.getElementById("lbleq").value;
                var chk = "";
                lid = document.getElementById("lbllid").value;
                var eReturn = window.showModalDialog("../reports/ttnodialog.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&who=ts&date=" + Date(), "", "dialogHeight:600px; dialogWidth:780px;resizable=yes");
                if (eReturn) {
                    window.location = "../appsdrag/pmotime.aspx?eqid=" + eq + "&date=" + Date();
                }
                else {
                    window.location = "../appsdrag/pmotime.aspx?eqid=" + eq + "&date=" + Date();
                }
            }
        }
        function gettsnotasks1(skillid, skillqty, rdid, meterid, freq) {
            var eq = document.getElementById("lbleq").value;
            fuid = "";
            if (eq != "") {
                var cid = "0";
                var sid = document.getElementById("lblsid").value;
                var did = document.getElementById("lbldid").value;
                var clid = document.getElementById("lblclid").value;
                var eqid = document.getElementById("lbleq").value;
                var chk = "";
                lid = document.getElementById("lbllid").value;
                //alert("../reports/ttnodialog.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&skillid=" + skillid + "&skillqty=" + skillqty + "&rdid=" + rdid + "&meterid=" + meterid + "&freq=" + freq + "&who=tot&date=" + Date())
                var eReturn = window.showModalDialog("../reports/ttnodialog.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&skillid=" + skillid + "&skillqty=" + skillqty + "&rdid=" + rdid + "&meterid=" + meterid + "&freq=" + freq + "&who=tot&date=" + Date(), "", "dialogHeight:600px; dialogWidth:780px;resizable=yes");
                if (eReturn) {
                    window.location = "../appsdrag/pmotime.aspx?eqid=" + eq + "&date=" + Date();
                }
                else {
                    window.location = "../appsdrag/pmotime.aspx?eqid=" + eq + "&date=" + Date();
                }
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="dvov" runat="server">
    
    </div>
    <input type="hidden" id="lbleq" runat="server" />
    <input id="lblsid" type="hidden" name="lblsid" runat="server" />
    <input id="lbldid" type="hidden" name="lbldid" runat="server"/>
    <input id="lblclid" type="hidden" name="lblclid" runat="server"/>
    <input id="lblchk" type="hidden" name="lblchk" runat="server"/>
    <input type="hidden" id="lbllid" runat="server" name="lbllid"/>
    </form>
</body>
</html>
