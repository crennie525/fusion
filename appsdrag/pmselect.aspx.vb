Imports System.Data.SqlClient
Public Class pmselect1
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim pms As New Utilities
    Dim dr As SqlDataReader
    Dim cid, sid, eqid, eqnum, lid, dept, clid, typ As String
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddeq As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hrefret As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents lblsave As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddproc As System.Web.UI.WebControls.DropDownList
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddskillo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtfreqo As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddeqstato As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            lid = Request.QueryString("lid").ToString
            lbllid.Value = lid
            dept = Request.QueryString("dept").ToString
            lbldept.Value = dept
            clid = Request.QueryString("clid").ToString
            lblclid.Value = clid
            typ = Request.QueryString("typ").ToString
            lbltyp.Value = typ
            pms.Open()
            PopEq(typ)
            Try
                ddeq.SelectedValue = eqid
            Catch ex As Exception

            End Try
            GetLists()
            PopProcedures(eqid)
            pms.Dispose()
        Else
            If Request.Form("lblpchk") = "pr" Then
                eqid = lbleqid.Value
                pms.Open()
                PopProcedures(eqid)
                lblpchk.Value = ""
                'CleanTasks()
                'GoToTasks()
                pms.Dispose()
            End If
        End If
        ddeq.Attributes.Add("onchange", "checksave('eq');")
        txtfreqo.Attributes.Add("onchange", "changepm('freq');")
        ddskillo.Attributes.Add("onchange", "changepm('skill');")
        ddeqstato.Attributes.Add("onchange", "changepm('stat');")

    End Sub
    Private Sub PopEq(ByVal typ As String)
        Dim did, filt, dt, val As String
        lid = lbllid.Value
        did = lbldept.Value
        clid = lblclid.Value
        Dim eqcnt As Integer
        If typ = "d" Then
            If lid = "" Then
                CheckTyp(did)
                sql = "select count(*) from equipment where dept_id = '" & did & "'"
                filt = " where dept_id = '" & did & "'"
            Else
                CheckTyp(did, lid)
                sql = "select count(*) from equipment where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            End If

        ElseIf typ = "c" Then
            If lid = "" Then
                CheckTyp(did)
                sql = "select count(*) from equipment where cellid = '" & clid & "'"
                filt = " where cellid = '" & clid & "'"
            Else
                CheckTyp(did, lid)
                sql = "select count(*) from equipment where cellid = '" & clid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & clid & "' and locid = '" & clid & "'"
            End If

        ElseIf typ = "l" Then
            CheckTyp(did, lid)
            If clid <> "" Then
                sql = "select count(*) from equipment where cellid = '" & clid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & clid & "' and locid = '" & lid & "'"
            ElseIf did <> "" Then
                sql = "select count(*) from equipment where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            Else
                sql = "select count(*) from equipment where locid = '" & lid & "'"
                filt = " where locid = '" & lid & "'"
            End If

        End If
        eqcnt = pms.Scalar(sql)
        If eqcnt > 0 Then
            dt = "equipment"
            val = "eqid, eqnum "

            dr = pms.GetList(dt, val, filt)
            ddeq.DataSource = dr
            ddeq.DataTextField = "eqnum"
            ddeq.DataValueField = "eqid"
            Try
                ddeq.DataBind()
            Catch ex As Exception

            End Try

            dr.Close()
            ddeq.Items.Insert(0, "Select Equipment")
            ddeq.Enabled = True
        Else
            Dim strMessage As String = " No Equipment Issued To This Department, Station/Cell, Location Combination"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ddeq.Items.Insert(0, "Select Equipment")
            ddeq.Enabled = False
        End If
        'GetLoc()
        'lbltaskcnt.Value = "2"
        'lblcleantasks.Value = "0"
        'CleanTasks()
    End Sub
    Private Sub CheckTyp(ByVal dept As String, Optional ByVal loc As String = "N")
        Dim typ As String
        Dim cnt As Integer
        If loc = "N" Then
            sql = "select count(locid) from equipment where dept_id = '" & dept & "' and locid <> 0 and locid is not null"
            cnt = pms.Scalar(sql)
            If cnt = 0 Then
                lbltyp.Value = "reg"
            Else
                lbltyp.Value = "dloc"
            End If
        Else
            sql = "select count(dept_id) from equipment where locid = '" & loc & "' and dept_id <> 0 and dept_id is not null"
            cnt = pms.Scalar(sql)
            If cnt = 0 Then
                lbltyp.Value = "loc"
            Else
                lbltyp.Value = "dloc"
            End If
        End If

    End Sub
    Private Sub GetLists()

        cid = "0" 
       

        sid = HttpContext.Current.Session("dfltps").ToString() 'lblsid.Value
        Dim scnt As Integer
        sql = "select count(*) from pmSiteSkills where siteid = '" & sid & "'"
        scnt = pms.Scalar(sql)
        If scnt <> 0 Then
            sql = "select skillid, skill " _
            + "from pmSiteSkills where compid = '" & cid & "' and siteid = '" & sid & "'"
        Else
            sql = "select skillid, skill " _
            + "from pmSkills where compid = '" & cid & "' order by compid"
        End If
        'orig
        dr = pms.GetRdrData(sql)
        ddskillo.DataSource = dr
        ddskillo.DataTextField = "skill"
        ddskillo.DataValueField = "skillid"
        Try
            ddskillo.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddskillo.Items.Insert(0, New ListItem("Select"))
        ddskillo.Items(0).Value = 0

        sql = "select statid, status " _
        + "from pmStatus where compid = '" & cid & "' or compid = '0' order by compid"
        'orig
        dr = pms.GetRdrData(sql)
        ddeqstato.DataSource = dr
        ddeqstato.DataTextField = "status"
        ddeqstato.DataValueField = "statid"
        Try
            ddeqstato.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddeqstato.Items.Insert(0, New ListItem("Select"))
        ddeqstato.Items(0).Value = 0
    End Sub
    Private Sub PopProcedures(ByVal eqid As String)
        Dim dt, val, filt As String
        Dim prcnt As Integer
        sql = "select count(*) from PMOptOldProcedures where eqid = '" & eqid & "'"
        prcnt = pms.Scalar(sql)
        If prcnt = 0 Then
            ddproc.Items.Insert(0, "Select Procedure")
            ddproc.Enabled = False
        Else
            dt = "PMOptOldProcedures"
            val = "eqppmid, filename"
            filt = " where eqid = '" & eqid & "'"
            dr = pms.GetList(dt, val, filt)
            ddproc.DataSource = dr
            ddproc.DataTextField = "filename"
            ddproc.DataValueField = "eqppmid"
            Try
                ddproc.DataBind()
            Catch ex As Exception

            End Try

            dr.Close()
            ddproc.Items.Insert(0, "Select Procedure")
            ddproc.Enabled = True
        End If
    End Sub
End Class
