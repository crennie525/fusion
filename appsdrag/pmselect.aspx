<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmselect.aspx.vb" Inherits="lucy_r12.pmselect1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>pmselect</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" type="text/javascript">
		function GetProcDiv() {
		//handleapp();
			eqid = document.getElementById("lbleqid").value;
			var eqlst = document.getElementById("ddeq");
			var eqnum = eqlst.options[eqlst.selectedIndex].text
			if (eqid!="") {
			var eReturn = window.showModalDialog("../appsopt/OptDialog.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:580px; resizable=yes");
			}
			if (eReturn) {
			document.getElementById("lblpchk").value = "pr";
			document.getElementById("form1").submit();
			}
		}
		function checksave(who) {
		//alert(who)
		document.getElementById("lbleqid").value = document.getElementById("ddeq").value
		checkpm();
		}
		function changepm(who) {
		if(who=="skill") {
		document.getElementById("lblskill").value = document.getElementById("ddskillo").value;
		//alert(document.getElementById("lblskill").value)
		}
		if(who=="freq") {
		document.getElementById("lblfreq").value = document.getElementById("txtfreqo").value;
		}
		if(who=="stat") {
		document.getElementById("lbleqstat").value = document.getElementById("ddeqstato").value;
		}
		}
		function checkpm() {
		var skill = document.getElementById("lblskill").value;
		var sklst = document.getElementById("ddskillo");
		var sknum = sklst.options[sklst.selectedIndex].text

		var freq = ""; // document.getElementById("lblfreq").value;

		var frnum = document.getElementById("txtfreq").value;
		
		var stat = document.getElementById("lbleqstat").value;
		var stlst = document.getElementById("ddeqstato");
		var stnum = stlst.options[stlst.selectedIndex].text
		
		var eq = document.getElementById("lbleqid").value;
		var eqlst = document.getElementById("ddeq");
		var eqnum = eqlst.options[eqlst.selectedIndex].text
		
		if(skill==""&&skill!="0") {
		alert("Skill Required")
		}
		else if(freq==""&&freq!="0") {
		alert("Frequency Required")
		}
		else if(stat==""&&stat!="0") {
		alert("Status Required")
		}
		else if(eq==""&&eq!="0") {
		alert("Equipment Required")
		}
		else {
		var hrefstr = "start=yes&eqid=" + eq + "&eqnum=" + eqnum + "&skill=" + skill + "&sknum=" + sknum + "&freq=" + freq + "&frnum=" + frnum + "&stat=" + stat + "&stnum=" + stnum;
		window.parent.handledragpm(hrefstr);
		}
		}
		</script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 2px; POSITION: absolute; TOP: 2px" width="740" cellpadding="0" cellspacing="0">
				<tr height="22">
					<td colspan="2">
						<table>
							<tr>
								<td class="label" width="80">Equipment</td>
								<td class="plainlabel" id="tdeq" runat="server" width="170"><asp:dropdownlist id="ddeq" runat="server" AutoPostBack="True" Width="170px" CssClass="plainlabel"></asp:dropdownlist></td>
								<td width="50"><IMG class="imgbutton" onmouseover="return overlib('Add a New Equipment Record', ABOVE, LEFT)"
										onclick="GetEqDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
										width="20"><IMG onmouseover="return overlib('Copy an Equipment Record', ABOVE, LEFT)" onclick="GetEqCopy();"
										onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/copybg.gif" width="20"></td>
								<td class="label" width="80">Procedure</td>
								<td width="170"><asp:dropdownlist id="ddproc" runat="server" CssClass="plainlabel" Width="170px" AutoPostBack="True"></asp:dropdownlist></td>
								<td class="label" width="40"><IMG class="imgbutton" onmouseover="return overlib('Add Procedures to Optimize', ABOVE, LEFT)"
										onclick="GetProcDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/upload.gif"
										width="20"></td>
								<td align="right"><a href="#" class="A1" id="hrefret" runat="server">Return</a></td>
							</tr>
							<tr>
								<td class="label">Skill</td>
								<td><asp:dropdownlist id="ddskillo" runat="server" Width="144px" CssClass="plainlabel"></asp:dropdownlist></td>
								<td></td>
								<td class="label">Frequency</td>
								<td><asp:textbox id="txtfreqo" runat="server" Width="80px" CssClass="plainlabel"></asp:textbox></td>
								<td class="label">Status</td>
								<td><asp:dropdownlist id="ddeqstato" runat="server" Width="90px" CssClass="plainlabel"></asp:dropdownlist></td>
								<td><img src="../images/appbuttons/minibuttons/download.gif" onmouseover="return overlib('Transfer PM Data to Drag-n-Drop Screen', ABOVE, LEFT)"
										onmouseout="return nd()" onclick="checkpm();"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/optbg.gif" border="0"></td>
					<td class="thdrsingrt label" align="left" width="714" id="tdloctop">Drag and Drop 
						Tasks</td>
				</tr>
			</table>
			<input type="hidden" id="lbleqid" runat="server"><input type="hidden" id="lblpchk" runat="server">
			<input type="hidden" id="lbllid" runat="server"> <input type="hidden" id="lbldept" runat="server">
			<input type="hidden" id="lblclid" runat="server"> <input type="hidden" id="lbltyp" runat="server">
			<input type="hidden" id="lblsave" runat="server"> <input type="hidden" id="lblfreq" runat="server">
			<input type="hidden" id="lblskill" runat="server"> <input type="hidden" id="lbleqstat" runat="server">
		</form>
	</body>
</HTML>
