﻿Public Class taskmovedialog
    Inherits System.Web.UI.Page
    Dim eqid, typ, curr, pmstr As String
    Dim pmtskid, tpmtskid, istpm As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try

        If Not IsPostBack Then
            curr = Request.QueryString("curr").ToString
            eqid = Request.QueryString("eqid").ToString
            pmtskid = Request.QueryString("pmtskid").ToString
            tpmtskid = Request.QueryString("tpmtskid").ToString
            istpm = Request.QueryString("istpm").ToString
            iftot.Attributes.Add("src", "totmove.aspx?eqid=" & eqid & "&curr=" & curr & " &pmtskid=" & pmtskid & "&tpmtskid=" & tpmtskid & "&istpm=" & istpm & "&date=" & Now)

        End If
    End Sub

End Class