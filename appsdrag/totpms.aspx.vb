Imports System.Data.SqlClient

Public Class totpms
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim tots As New Utilities
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsavechk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdmsg As System.Web.UI.HtmlControls.HtmlTableCell
    Dim eqid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgtots As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString '"317" '
            lbleqid.Value = eqid
            tots.Open()
            GetTots()
            tots.Dispose()

        End If
    End Sub
    Private Sub GetTots()
        eqid = lbleqid.Value
        Dim pcnt As Integer
        sql = "select count(*) from pmtottime where eqid = '" & eqid & "'"
        pcnt = tots.Scalar(sql)
        If pcnt = 0 Then
            tdmsg.InnerHtml = "No Records Found<br>Make sure that you have Transferred a Skill, Skill Qty, Frequency, and Status to the Drag and Drop Task Screen at least once<br>and that Use Total is Selected"
        Else
            sql = "select eqid, skillid, skill, skillqty, freqid, freq, rdid, rd, tottime, downtime, " _
                   + "pm = skill + '(' + cast(skillqty as varchar(10)) + ')/' + cast(freq as varchar(10)) + ' days/' + rd " _
                   + "from pmtottime where eqid = '" & eqid & "'"
            dr = tots.GetRdrData(sql)
            dgtots.DataSource = dr
            dgtots.DataBind()
            dr.Close()
        End If

       
    End Sub

    Private Sub dgtots_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtots.CancelCommand
        dgtots.EditItemIndex = -1
        tots.Open()
        GetTots()
        tots.Dispose()
    End Sub

    Private Sub dgtots_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtots.EditCommand
        dgtots.EditItemIndex = e.Item.ItemIndex
        tots.Open()
        GetTots()
        tots.Dispose()
    End Sub

    Private Sub dgtots_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtots.ItemDataBound
        If e.Item.ItemType = ListItemType.EditItem Then
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "rd").ToString
            If id.ToLower <> "down" Then
                Dim idbox As TextBox
                idbox = CType(e.Item.FindControl("txtdowntimee"), TextBox)
                idbox.Enabled = False
            End If
        End If
    End Sub

    Private Sub dgtots_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtots.UpdateCommand
        eqid = lbleqid.Value
        Dim skillid, freqid, rdid, skillqty As String
        skillqty = CType(e.Item.FindControl("lblsqty"), Label).Text
        skillid = CType(e.Item.FindControl("lblskillide"), Label).Text
        freqid = CType(e.Item.FindControl("lblfreqide"), Label).Text
        rdid = CType(e.Item.FindControl("lblrdide"), Label).Text
        Dim tot, down As String
        tot = CType(e.Item.FindControl("txttottimee"), TextBox).Text
        down = CType(e.Item.FindControl("txtdowntimee"), TextBox).Text

        Dim nchk As Long
        Try
            nchk = System.Convert.ToInt64(down)
        Catch ex As Exception
            Dim strMessage As String = "Total Down Time (mins) Must Be a Numeric Value!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        Dim ntchk As Long
        Try
            ntchk = System.Convert.ToInt64(tot)
        Catch ex As Exception
            Dim strMessage As String = "Total Time (mins) Must Be a Numeric Value!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        tots.Open()
        sql = "update pmtottime set tottime = '" & tot & "', downtime = '" & down & "' " _
        + "where skillid = '" & skillid & "' and skillqty = '" & skillqty & "' " _
        + "and freq = '" & freqid & "' and rdid = '" & rdid & "' and eqid = '" & eqid & "'"
        tots.Update(sql)
        dgtots.EditItemIndex = -1
        GetTots()
        tots.Dispose()
        lblsavechk.Value = "1"
    End Sub
End Class
