﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="totmove.aspx.vb" Inherits="lucy_r12.totmove" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript">
		    function getpm(pmstr) {
		        document.getElementById("lblpmstr").value = pmstr;
		        document.getElementById("lblsubmit").value = "get";
		        document.getElementById("form1").submit();
		    }
		    function checkit() {
		        var chk = document.getElementById("lblsubmit").value;
		        if (chk == "go") {
		            window.parent.handlepm();
		        }
		    }
		</script>
</head>
<body onload="checkit();">
    <form id="form1" runat="server">
    <table>
    <tr>
    <td class="bluelabel" width="100" height="26">Current Profile</td>
    <td class="plainlabel" id="tdcurr" runat="server" width="260"></td>
    </tr>
    <tr>
    <td class="label" colspan="2" height="26">Choose a New Profile</td>
    </tr>
    <tr>
    <td colspan="2"><div id="totdiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; OVERFLOW: auto; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid; HEIGHT: 300px; WIDTH: 360px;"
							runat="server"></div></td>
    </tr>
    <tr>
    <td colspan="2" class="plainlabelblue" align="center">Note: Updates Original Profile Values Only</td>
    </tr>
    </table>
    <input id="lbleqid" type="hidden" runat="server" /> <input id="lbltyp" type="hidden" runat="server" />
    <input type="hidden" id="lblpmstr" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lbloskillqty" runat="server" />
    <input type="hidden" id="lbloskillid" runat="server" />
    <input type="hidden" id="lbloskill" runat="server" />
    <input type="hidden" id="lblofreqid" runat="server" />
    <input type="hidden" id="lblofreq" runat="server" />
    <input type="hidden" id="lblostatusid" runat="server" />
    <input type="hidden" id="lblostatus" runat="server" />
    <input type="hidden" id="lblpmtskid" runat="server" />
    </form>
</body>
</html>
