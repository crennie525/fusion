Imports System.Text
Imports System.Data.SqlClient
Public Class comptasks
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim rp As New Utilities
    Dim comid, comp As String
    Protected WithEvents lblcomid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdfu As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents rb1 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rb2 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents tdcomps As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents DragContainer1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbl1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbl2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            lbltyp.Value = "orig"
            comid = Request.QueryString("comid").ToString
            lblcomid.Value = comid
            comp = Request.QueryString("comp").ToString
            tdfu.InnerHtml = comp
            rp.Open()
            LoadCompFields(comid)
            rp.Dispose()
        Else
            If Request.Form("lblsubmit") = "orig" Or Request.Form("lblsubmit") = "rev" Then
                rp.Open()
                lbltyp.Value = lblsubmit.Value
                comid = lblcomid.Value
                LoadCompFields(comid)
                rp.Dispose()
            End If
        End If

    End Sub
    Private Sub ReOrder(ByVal fuid As String)

        Dim task, taskid, tasknum As String
        Dim taskhold As Integer = 0
        Dim ttasks As String = lbl1.Value
        Dim ttasksarr() As String = ttasks.Split(";")
        If ttasksarr.Length > 1 Then
            Dim j As Integer
            Dim i As Integer = 0
            For j = 0 To ttasksarr.Length - 1
                i += 1
                task = ttasksarr(j)
                Dim tcomarr() As String = task.Split("-")
                taskid = tcomarr(0)
                tasknum = tcomarr(1)
                If tasknum <> i Then
                    taskhold = tasknum
                    Dim subtasks As String
                    sql = "select pmtskid from pmtasks where funcid = '" & fuid & "' and tasknum = '" & tasknum & "' and subtask <> 0"
                    dr = rp.GetRdrData(sql)
                    While dr.Read
                        taskid += "','" & dr.Item("pmtskid").ToString
                    End While
                    dr.Close()
                    sql = "update pmtasks set tasknum = '" & i & "' where funcid = '" & fuid & "' and pmtskid in ('" & taskid & "')"
                    rp.Update(sql)
                End If

            Next
        End If
    End Sub
    Private Sub LoadCompFields(ByVal comid As String)
        'fuid = ddfu.SelectedValue
        Dim task, tnum, skill, freq, rd, tid As String
        Dim sb As StringBuilder = New StringBuilder
        Dim jb As StringBuilder = New StringBuilder
        'jb.Append("CreateDragContainer(")
        jb.Append("function CreateDragContainerOpen() { CreateDragContainer(")
        Dim typ As String = lbltyp.Value
        If typ = "orig" Then
            sql = "select pmtskid, tasknum, comid, compnum, otaskdesc, origfreq, origskill, origrd from pmtasks where comid = '" & comid & "' and subtask = 0 order by tasknum"
        Else
            sql = "select pmtskid, tasknum, comid, compnum, taskdesc, freq, skill, rd from pmtasks where comid = '" & comid & "' and subtask = 0 order by tasknum"
        End If
        Dim comp As String
        Dim i As Integer
        dr = rp.GetRdrData(sql)
        While dr.Read
            tid = dr.Item("pmtskid").ToString
            If typ = "orig" Then
                task = dr.Item("otaskdesc").ToString
                freq = dr.Item("origfreq").ToString
                skill = dr.Item("origskill").ToString
                rd = dr.Item("origrd").ToString
            Else
                task = dr.Item("taskdesc").ToString
                freq = dr.Item("freq").ToString
                skill = dr.Item("skill").ToString
                rd = dr.Item("rd").ToString
            End If

            tnum = dr.Item("tasknum").ToString
            comp = dr.Item("compnum").ToString
            If comp = "" Then
                comp = "Not Provided"
            End If

            sb.Append("<div class=""DragBox1 tdborder"" id=""" & tid & "-" & tnum & """  dragClass=""DragDragBox1"" overClass=""OverDragBox1"">")
            sb.Append("<font class=""bluelabel"">[" & tnum & "]&nbsp;&nbsp;" & comp & " - ")
            sb.Append(skill & " / " & freq & " days / " & rd & "</font><br>" & task & "</div>")
            'lblmove.Value += j & ";"
            If lbl2.Value = "" Then
                lbl2.Value = tid & "-" & tnum
            Else
                lbl2.Value += ";"
                lbl2.Value += tid & "-" & tnum
            End If
        End While
        dr.Close()
        DragContainer1.InnerHtml = sb.ToString
    End Sub

End Class
