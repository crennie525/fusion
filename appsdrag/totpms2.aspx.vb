﻿Imports System.Data.SqlClient
Public Class totpms2
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim tots As New Utilities
    Dim eqid, sid, pmtskid, eqnum, pmtid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString '
            lbleqid.Value = eqid
            sid = Request.QueryString("sid").ToString '
            lblsid.Value = sid
            eqnum = Request.QueryString("eqnum").ToString '
            lbleqnum.Value = eqnum
            tots.Open()
            GetTots()
            tots.Dispose()

        End If
    End Sub
    Private Sub GetTots()
        eqid = lbleqid.Value
        Dim pcnt As Integer
        sql = "select count(*) from pmtottime where eqid = '" & eqid & "'"
        pcnt = tots.Scalar(sql)
        If pcnt = 0 Then
            tdmsg.InnerHtml = "No Records Found<br>Make sure that you have Transferred a Skill, Skill Qty, Frequency, and Status to the Drag and Drop Task Screen at least once<br>and that Use Total is Selected"
        Else
            sql = "select pmtid, eqid, skillid, skill, skillqty, freqid, freq, rdid, rd, tottime, downtime, " _
                + "ismeter, meterid, meter, meterunit, meterfreq, maxdays, " _
                   + "pm = skill + '(' + cast(skillqty as varchar(10)) + ')/' + cast(freq as varchar(10)) + ' days/' + rd, " _
                   + "tcnt = (select count(*) from pmtasks t where t.origskillid = pmtottime.skillid and t.origqty = pmtottime.skillqty and " _
                   + "t.origfreq = pmtottime.freq and isnull(t.meterido, 0) = isnull(pmtottime.meterid, 0) " _
                   + "and isnull(t.meterfreqo, 0) = isnull(pmtottime.meterfreq, 0) " _
                   + "and isnull(t.maxdayso, 0) = isnull(pmtottime.maxdays, 0) " _
                   + "and t.eqid = pmtottime.eqid and t.subtask = 0 and t.origrdid = pmtottime.rdid) " _
                   + "from pmtottime where eqid = '" & eqid & "' order by skill, skillqty, freq, rd"
            dr = tots.GetRdrData(sql)
            dgtots.DataSource = dr
            dgtots.DataBind()
            dr.Close()
        End If


    End Sub

    Private Sub dgtots_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtots.CancelCommand
        dgtots.EditItemIndex = -1
        tots.Open()
        GetTots()
        tots.Dispose()
    End Sub

    Private Sub dgtots_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtots.DeleteCommand
        eqid = lbleqid.Value
        Dim skillid, freqid, rdid, skillqty As String
        Try
            skillqty = CType(e.Item.FindControl("lblsqty"), Label).Text
            skillid = CType(e.Item.FindControl("lblskillide"), Label).Text
            freqid = CType(e.Item.FindControl("lblfreqide"), Label).Text
            rdid = CType(e.Item.FindControl("lblrdide"), Label).Text
        Catch ex As Exception
            skillqty = CType(e.Item.FindControl("lblsqtyi"), Label).Text
            skillid = CType(e.Item.FindControl("lblskillidi"), Label).Text
            freqid = CType(e.Item.FindControl("lblfreqidi"), Label).Text
            rdid = CType(e.Item.FindControl("lblrdidi"), Label).Text
        End Try
        Dim oismeter, ometerid, ometer, ometerunit, ometerfreq, omaxdays As String
        Try
            oismeter = CType(e.Item.FindControl("lblismetere"), Label).Text
            ometerid = CType(e.Item.FindControl("lblmeteride"), Label).Text
            ometer = CType(e.Item.FindControl("lblmetere"), Label).Text
            ometerunit = CType(e.Item.FindControl("lblmeterunite"), Label).Text
            ometerfreq = CType(e.Item.FindControl("lblmeterfreqe"), Label).Text
            omaxdays = CType(e.Item.FindControl("lblmaxdayse"), Label).Text
        Catch ex As Exception
            oismeter = CType(e.Item.FindControl("lblismeteri"), Label).Text
            ometerid = CType(e.Item.FindControl("lblmeteridi"), Label).Text
            ometer = CType(e.Item.FindControl("lblmeteri"), Label).Text
            ometerunit = CType(e.Item.FindControl("lblmeteruniti"), Label).Text
            ometerfreq = CType(e.Item.FindControl("lblmeterfreqi"), Label).Text
            omaxdays = CType(e.Item.FindControl("lblmaxdaysi"), Label).Text
        End Try
        tots.Open()
        'sql = "usp_delpmtask '" & fuid & "', '" & pmtskid & "', '" & tnum & "'"
        If ometerid <> "" Then
            'sql = "delete from pmtasks where origfreq = '" & freqid & "' and origskillid = '" & skillid & "' and origrdid = '" & rdid & "' " _
            '+ "and origqty = '" & skillqty & "' and eqid = '" & eqid & "' " _
            '+ "and meterido = '" & ometerid & "' and meterfreqo = '" & ometerfreq & "' and maxdayso = '" & omaxdays & "'"

            sql = "select funcid, pmtskid, tasknum from pmtasks where origfreq = '" & freqid & "' and origskillid = '" & skillid & "' and origrdid = '" & rdid & "' " _
            + "and origqty = '" & skillqty & "' and eqid = '" & eqid & "' " _
            + "and meterido = '" & ometerid & "' and meterfreqo = '" & ometerfreq & "' and maxdayso = '" & omaxdays & "'"
        Else
            'sql = "delete from pmtasks where origfreq = '" & freqid & "' and origskillid = '" & skillid & "' and origrdid = '" & rdid & "' " _
            '+ "and origqty = '" & skillqty & "' and eqid = '" & eqid & "' "

            sql = "select funcid, pmtskid, tasknum from pmtasks where origfreq = '" & freqid & "' and origskillid = '" & skillid & "' and origrdid = '" & rdid & "' " _
           + "and origqty = '" & skillqty & "' and eqid = '" & eqid & "' "
        End If
        'tots.Update(sql)
        Dim ds As New DataSet
        ds = tots.GetDSData(sql)
        Dim i As Integer
        Dim f, p, t As String
        Dim x As Integer = ds.Tables(0).Rows.Count
        For i = 0 To (x - 1)
            f = ds.Tables(0).Rows(i)("funcid").ToString
            p = ds.Tables(0).Rows(i)("pmtskid").ToString
            t = ds.Tables(0).Rows(i)("tasknum").ToString
            sql = "usp_delpmtask '" & f & "', '" & p & "', '" & t & "'"
            tots.Update(sql)
        Next

        If ometerid <> "" Then
            sql = "delete from pmtottime " _
            + "where skillid = '" & skillid & "' and skillqty = '" & skillqty & "' " _
            + "and freq = '" & freqid & "' and rdid = '" & rdid & "' and eqid = '" & eqid & "' " _
            + "and meterid = '" & ometerid & "' and meterfreq = '" & ometerfreq & "' and maxdays = '" & omaxdays & "'"
        Else
            sql = "delete from pmtottime " _
            + "where skillid = '" & skillid & "' and skillqty = '" & skillqty & "' " _
            + "and freq = '" & freqid & "' and rdid = '" & rdid & "' and eqid = '" & eqid & "'"
        End If
        tots.Update(sql)
        dgtots.EditItemIndex = -1
        GetTots()
        tots.Dispose()
        lblsavechk.Value = "1"

    End Sub

    Private Sub dgtots_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtots.EditCommand
        dgtots.EditItemIndex = e.Item.ItemIndex
        tots.Open()
        GetTots()
        tots.Dispose()
    End Sub

    Private Sub dgtots_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtots.ItemDataBound

        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            Dim deleteButton As ImageButton = CType(e.Item.FindControl("ibDel"), ImageButton)
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete this profile and all associated tasks?')"
        End If

        If e.Item.ItemType = ListItemType.EditItem Then
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "rd").ToString
            Dim rid As String = DataBinder.Eval(e.Item.DataItem, "rdid").ToString
            If id.ToLower <> "down" And id <> "En arret" And rid <> "2" Then
                Dim idbox As TextBox
                idbox = CType(e.Item.FindControl("txtdowntimee"), TextBox)
                idbox.Enabled = False
            End If
            Dim lblskill As Label = CType(e.Item.FindControl("lblskill"), Label)
            Dim skillid As String
            skillid = lblskill.ClientID.ToString
            pmtskid = "TT"
            sid = lblsid.Value
            Dim imgskill As HtmlImage = CType(e.Item.FindControl("imgskill"), HtmlImage)
            imgskill.Attributes.Add("onclick", "getskill('" & skillid & "','" & pmtskid & "','" & sid & "','skill');")

            Dim lblrd As Label = CType(e.Item.FindControl("lblrd"), Label)
            Dim rdid As String
            rdid = lblrd.ClientID.ToString
            Dim imgrd As HtmlImage = CType(e.Item.FindControl("imgrd"), HtmlImage)
            imgrd.Attributes.Add("onclick", "getrd('" & rdid & "','" & pmtskid & "','" & sid & "','rd');")

            'freqid, eqid, pmtskid, fuid, skillid, skillqty, rdid, skill, rd, meterid
            Dim lblfreq As TextBox = CType(e.Item.FindControl("txtfreq"), TextBox)
            Dim freqid As String
            freqid = lblfreq.ClientID.ToString
            Dim imgfreq As HtmlImage = CType(e.Item.FindControl("imgfreq"), HtmlImage)
            pmtskid = ""
            Dim fuid As String = ""
            Dim skillqty As String = DataBinder.Eval(e.Item.DataItem, "skillqty").ToString
            Dim skill As String = DataBinder.Eval(e.Item.DataItem, "skill").ToString
            Dim rd As String = DataBinder.Eval(e.Item.DataItem, "rd").ToString
            pmtid = DataBinder.Eval(e.Item.DataItem, "pmtid").ToString
            eqid = lbleqid.Value
            Dim meterid As String = DataBinder.Eval(e.Item.DataItem, "meterid").ToString
            'freqid, eqid, pmtskid, fuid, skillid, skillqty, rdid, meterid, rd, skill
            imgfreq.Attributes.Add("onclick", "getmeter('" & pmtid & "','" & freqid & "','" & eqid & "','" & pmtskid & "','" & fuid & "','" & skillid & "','" & skillqty & "','" & rdid & "','" & meterid & "','" & rd & "','" & skill & "');")
            'Dim tst As String = "'" & freqid & "','" & pmtskid & "','" & fuid & "','" & skillid & "','" & skillqty & "','" & rdid & "','" & skill & "','" & rd & "','" & meterid & "'"
            'Dim tst1 As String = tst
        End If
    End Sub

    Private Sub dgtots_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtots.UpdateCommand
        Dim taskflag As Integer = 0
        Dim meterflag As Integer = 0
        Dim qtyflag As Integer = 0

        eqid = lbleqid.Value
        Dim skillid, freqid, rdid, skillqty, skill, rd As String
        skillqty = CType(e.Item.FindControl("lblsqty"), Label).Text
        skillid = CType(e.Item.FindControl("lblskillide"), Label).Text
        freqid = CType(e.Item.FindControl("lblfreqide"), Label).Text
        rdid = CType(e.Item.FindControl("lblrdide"), Label).Text
        skill = CType(e.Item.FindControl("lblskill"), Label).Text
        rd = CType(e.Item.FindControl("lblrd"), Label).Text

        Dim skillret, skillidret, rdret, rdidret, freq As String
        skillidret = lblskillidret.Value
        rdidret = lblrdidret.Value
        skillret = lblskillret.Value
        rdret = lblrdret.Value
        freq = CType(e.Item.FindControl("txtfreq"), TextBox).Text

        Dim newskillid, newskill, newqty, newrdid, newrd, newfreq As String

        'skill
        If skillid <> skillidret And skillidret <> "" Then
            taskflag = 1
            newskillid = skillidret
            newskill = skillret
        End If

        'skill qty
        Dim nskillqty As String = CType(e.Item.FindControl("txtqty"), TextBox).Text
        If skillqty <> nskillqty Then
            Dim schk As Long
            Try
                schk = System.Convert.ToInt64(nskillqty)
            Catch ex As Exception
                Dim strMessage As String = "Skill Qty Must Be a Numeric Value!"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            newqty = nskillqty
            taskflag = 1
        End If

        Dim tot, down As String
        tot = CType(e.Item.FindControl("txttottimee"), TextBox).Text
        down = CType(e.Item.FindControl("txtdowntimee"), TextBox).Text

        'running down
        If rdid <> rdidret And rdidret <> "" Then
            taskflag = 1
            newrd = rdret
            newrdid = rdidret
            If newrd.ToLower <> "down" Then
                down = "0"
            End If
        End If

        Dim oismeter, ometerid, ometer, ometerunit, ometerfreq, omaxdays As String
        oismeter = CType(e.Item.FindControl("lblismetere"), Label).Text
        ometerid = CType(e.Item.FindControl("lblmeteride"), Label).Text
        ometer = CType(e.Item.FindControl("lblmetere"), Label).Text
        ometerunit = CType(e.Item.FindControl("lblmeterunite"), Label).Text
        ometerfreq = CType(e.Item.FindControl("lblmeterfreqe"), Label).Text
        omaxdays = CType(e.Item.FindControl("lblmaxdayse"), Label).Text

        Dim ismeter, meterid, meter, meterunit, meterfreq, maxdays As String
        ismeter = lblismeter.Value
        meterid = lblmeterid.Value
        meter = lblmeter.Value
        meterunit = lblmeterunit.Value
        meterfreq = lblmeterfreq.Value
        maxdays = lblmaxdays.Value

        Dim freqret As String = lblfreqret.Value

        'frequency
        If freqid <> freq And freqret = "" Then
            Dim fchk As Long
            Try
                fchk = System.Convert.ToInt64(freq)
            Catch ex As Exception
                Dim strMessage As String = "Frequency Must Be a Numeric Value!"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            newfreq = freq
            taskflag = 1
        ElseIf freqret <> "" Then
            newfreq = freqret
            meterflag = 1
            taskflag = 1
        End If

        'meter info
        If ometerid <> meterid Or meterid <> "" Then
            meterflag = 1
        End If

       

        Dim nchk As Long
        Try
            nchk = System.Convert.ToInt64(down)
        Catch ex As Exception
            Dim strMessage As String = "Total Down Time (mins) Must Be a Numeric Value!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        Dim ntchk As Long
        Try
            ntchk = System.Convert.ToInt64(tot)
        Catch ex As Exception
            Dim strMessage As String = "Total Time (mins) Must Be a Numeric Value!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        If taskflag = 1 And newskillid = "" Then
            newskillid = skillid
            newskill = skill
        End If
        If taskflag = 1 And newfreq = "" Then
            newfreq = freqid
        End If
        If taskflag = 1 And newrdid = "" Then
            newrdid = rdid
            newrd = rd
        End If
        If taskflag = 1 And newqty = "" Then
            newqty = skillqty
        End If

        Dim dupcnt As Integer = 0
        Dim duptot As Integer = 0
        Dim dupdown As Integer = 0

        tots.Open()

        If taskflag = 0 Then
            If meterid <> "" Then
                sql = "update pmtottime set tottime = '" & tot & "', downtime = '" & down & "' " _
           + "where skillid = '" & skillid & "' and skillqty = '" & skillqty & "' " _
           + "and freq = '" & freqid & "' and rdid = '" & rdid & "' and eqid = '" & eqid & "' " _
               + "and meterid = '" & meterid & "' and meterfreq = '" & meterfreq & "' and maxdays = '" & maxdays & "'"
            Else
                sql = "update pmtottime set tottime = '" & tot & "', downtime = '" & down & "' " _
           + "where skillid = '" & skillid & "' and skillqty = '" & skillqty & "' " _
           + "and freq = '" & freqid & "' and rdid = '" & rdid & "' and eqid = '" & eqid & "'"
            End If
            tots.Update(sql)
        Else
            'let the games begin
            'need to check if new would be a duplicate
            
            If meterid <> "" Then
                sql = "select count(*) from pmtottime " _
               + "where skillid = '" & newskillid & "' and skillqty = '" & newqty & "' " _
               + "and freq = '" & newfreq & "' and rdid = '" & newrdid & "' and eqid = '" & eqid & "' " _
               + "and meterid = '" & meterid & "' and meterfreq = '" & meterfreq & "' and maxdays = '" & maxdays & "'"
            Else
                sql = "select count(*) from pmtottime " _
               + "where skillid = '" & newskillid & "' and skillqty = '" & newqty & "' " _
               + "and freq = '" & newfreq & "' and rdid = '" & newrdid & "' and eqid = '" & eqid & "'"
            End If
            dupcnt = tots.Scalar(sql)
            If dupcnt > 0 Then
                If meterid <> "" Then
                    sql = "select sum(tottime) as 'tottime', sum(downtime) as 'downtime' from pmtottime " _
                   + "where skillid = '" & newskillid & "' and skillqty = '" & newqty & "' " _
                   + "and freq = '" & newfreq & "' and rdid = '" & newrdid & "' and eqid = '" & eqid & "' " _
                   + "and meterid = '" & meterid & "' and meterfreq = '" & meterfreq & "' and maxdays = '" & maxdays & "'"
                Else
                    sql = "select sum(tottime) as 'tottime', sum(downtime) as 'downtime' from pmtottime " _
                   + "where skillid = '" & newskillid & "' and skillqty = '" & newqty & "' " _
                   + "and freq = '" & newfreq & "' and rdid = '" & newrdid & "' and eqid = '" & eqid & "'"
                End If
                dr = tots.GetRdrData(sql)
                While dr.Read
                    duptot = dr.Item("tottime").ToString
                    dupdown = dr.Item("downtime").ToString
                End While
                dr.Close()
                tot = tot + duptot
                down = down + dupdown
            End If
            If ometerid <> "" Then
                sql = "delete from pmtottime " _
                + "where skillid = '" & skillid & "' and skillqty = '" & skillqty & "' " _
                + "and freq = '" & freqid & "' and rdid = '" & rdid & "' and eqid = '" & eqid & "' " _
                + "and meterid = '" & ometerid & "' and meterfreq = '" & ometerfreq & "' and maxdays = '" & omaxdays & "'"
            Else
                sql = "delete from pmtottime " _
                + "where skillid = '" & skillid & "' and skillqty = '" & skillqty & "' " _
                + "and freq = '" & freqid & "' and rdid = '" & rdid & "' and eqid = '" & eqid & "'"
            End If
            tots.Update(sql)
            If dupcnt = 0 Then
                If meterflag = 1 Then
                    sql = "insert into pmtottime (eqid, skillid, skill, freq, rdid, rd, tottime, skillqty, downtime, " _
                        + "ismeter, meterid, meter, meterunit, meterfreq, maxdays) " _
                       + "values ('" & eqid & "','" & newskillid & "','" & newskill & "','" & newfreq & "','" & newrdid & "','" & newrd & "', " _
                       + "'" & tot & "','" & newqty & "','" & down & "','1','" & meterid & "','" & meter & "','" & meterunit & "','" & meterfreq & "','" & maxdays & "')"
                ElseIf ometerid <> "" Then
                    sql = "insert into pmtottime (eqid, skillid, skill, freq, rdid, rd, tottime, skillqty, downtime, " _
                        + "ismeter, meterid, meter, meterunit, meterfreq, maxdays) " _
                       + "values ('" & eqid & "','" & newskillid & "','" & newskill & "','" & newfreq & "','" & newrdid & "','" & newrd & "', " _
                       + "'" & tot & "','" & newqty & "','" & down & "','1','" & ometer & "','" & ometerunit & "','" & ometerid & "','" & ometerfreq & "','" & omaxdays & "')"
                Else
                    sql = "insert into pmtottime (eqid, skillid, skill, freq, rdid, rd, tottime, skillqty, downtime) " _
                      + "values ('" & eqid & "','" & newskillid & "','" & newskill & "','" & newfreq & "','" & newrdid & "','" & newrd & "', " _
                      + "'" & tot & "','" & newqty & "','" & down & "')"
                End If
                tots.Update(sql)
            Else
                If meterid <> "" Then
                    sql = "update pmtottime set tottime = '" & tot & "', downtime = '" & down & "' " _
                   + "where skillid = '" & newskillid & "' and skillqty = '" & newqty & "' " _
                   + "and freq = '" & newfreq & "' and rdid = '" & newrdid & "' and eqid = '" & eqid & "' " _
                   + "and meterid = '" & meterid & "' and meterfreq = '" & meterfreq & "' and maxdays = '" & maxdays & "'"
                Else
                    sql = "update pmtottime set tottime = '" & tot & "', downtime = '" & down & "' " _
                   + "where skillid = '" & newskillid & "' and skillqty = '" & newqty & "' " _
                   + "and freq = '" & newfreq & "' and rdid = '" & newrdid & "' and eqid = '" & eqid & "'"
                End If
                tots.Update(sql)
            End If
            If meterflag = 1 Then
                sql = "update pmtasks set origfreq = '" & newfreq & "', meterido = '" & meterid & "', meterfreqo = '" & meterfreq & "', maxdayso = '" & maxdays & "', " _
                    + "origskillid = '" & newskillid & "', origskill = '" & newskill & "', " _
                    + "origqty = '" & newqty & "', origrd = '" & newrd & "', " _
                    + "origrdid = '" & newrdid & "' where origfreq = '" & freqid & "' and origskillid = '" & skillid & "' and origrdid = '" & rdid & "' " _
                    + "and origqty = '" & skillqty & "' and eqid = '" & eqid & "' " _
                    + "and meterido = '" & ometerid & "' and meterfreqo = '" & ometerfreq & "' and maxdayso = '" & omaxdays & "'"
            ElseIf ometerid <> "" Then
                sql = "update pmtasks set origfreq = '" & newfreq & "', " _
                    + "origskillid = '" & newskillid & "', origskill = '" & newskill & "', " _
                    + "origqty = '" & newqty & "', origrd = '" & newrd & "', " _
                    + "origrdid = '" & newrdid & "' where origfreq = '" & freqid & "' and origskillid = '" & skillid & "' and origrdid = '" & rdid & "' " _
                    + "and origqty = '" & skillqty & "' and eqid = '" & eqid & "' " _
                    + "and meterido = '" & ometerid & "' and meterfreqo = '" & ometerfreq & "' and maxdayso = '" & omaxdays & "'"
            Else
                sql = "update pmtasks set origfreq = '" & newfreq & "', " _
                    + "origskillid = '" & newskillid & "', origskill = '" & newskill & "', " _
                    + "origqty = '" & newqty & "', origrd = '" & newrd & "', " _
                    + "origrdid = '" & newrdid & "' where origfreq = '" & freqid & "' and origskillid = '" & skillid & "' and origrdid = '" & rdid & "' " _
                    + "and origqty = '" & skillqty & "' and eqid = '" & eqid & "' "
            End If
            tots.Update(sql)
        End If
        If dupcnt <> 0 Then
            'pm = skill + '(' + cast(skillqty as varchar(10)) + ')/' + cast(freq as varchar(10)) + ' days/' + rd
            Dim pmstr As String
            pmstr = newskill & "(" & newqty & ")/" & newfreq & " days/" & newrd
            Dim strMessage As String = "Record was merged with existing Profile: " & pmstr & "\nPlease Adjust Total and Down Times Accordingly"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

        dgtots.EditItemIndex = -1
        GetTots()
        tots.Dispose()
        lblsavechk.Value = "1"
    End Sub
End Class