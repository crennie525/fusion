<%@ Page Language="vb" AutoEventWireup="false" Codebehind="draghelp1.aspx.vb" Inherits="lucy_r12.draghelp1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Drag and Drop - Use Total Help</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="plainlabelblue">
						The Use Total Option Allows you to Enter a Total Time for Original Tasks within 
						the PM Cycle selected in the Drag and Drop Details section of the screen.
						<br>
						<br>
						A PM Cycle is defined as a unique combination of a Skill, Skill Quantity, 
						Frequency, and Running\Down Status.
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
