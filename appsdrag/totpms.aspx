<%@ Page Language="vb" AutoEventWireup="false" Codebehind="totpms.aspx.vb" Inherits="lucy_r12.totpms" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>totpms</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript">
		function checksave() {
		var chk = document.getElementById("lblsavechk").value;
		if(chk=="1") {
		window.parent.handlesave();
		}
		}
		</script>
	</HEAD>
	<body  onload="checksave();">
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="redlabel" align="center" id="tdmsg" runat="server"></td>
				</tr>
				<tr>
					<td width="400">
						<asp:datagrid id="dgtots" runat="server" CellSpacing="1" AutoGenerateColumns="False" AllowCustomPaging="True"
							AllowPaging="True" GridLines="None" CellPadding="0" ShowFooter="False">
							<FooterStyle BackColor="White"></FooterStyle>
							<EditItemStyle Height="15px"></EditItemStyle>
							<AlternatingItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="#E7F1FD"></AlternatingItemStyle>
							<ItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="ControlLightLight"></ItemStyle>
							<HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="PM">
									<HeaderStyle Width="200px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<%# DataBinder.Eval(Container, "DataItem.pm") %>
									</ItemTemplate>
									<EditItemTemplate>
										<%# DataBinder.Eval(Container, "DataItem.pm") %>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Total Time">
									<HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.tottime") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txttottimee" runat="server" Width="50px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.tottime") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Down Time">
									<HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label4" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.downtime") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtdowntimee" runat="server" Width="50px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.downtime") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn headerText="skillid" Visible="False">
									<HeaderStyle Width="20px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label1 runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.skillid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblskillide" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.skillid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn headerText="skillqty" Visible="False">
									<HeaderStyle Width="20px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label5" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.skillqty") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblsqty" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.skillqty") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn headerText="freqid" Visible="False">
									<HeaderStyle Width="20px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label3" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblfreqide" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn headerText="rdid" Visible="False">
									<HeaderStyle Width="20px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label6" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.rdid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblrdide" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.rdid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
								ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
						</asp:datagrid>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lbleqid" runat="server"> <input type="hidden" id="lblsavechk" runat="server">
		</form>
	</body>
</HTML>
