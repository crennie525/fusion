﻿Imports System.Data.SqlClient
Imports System.Text
Public Class totmove
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim tots As New Utilities
    Dim eqid, typ, curr, pmstr, istpm, tpmtskid As String
    Dim pmtskid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            curr = Request.QueryString("curr").ToString
            eqid = Request.QueryString("eqid").ToString
            pmtskid = Request.QueryString("pmtskid").ToString
            tpmtskid = Request.QueryString("tpmtskid").ToString
            istpm = Request.QueryString("istpm").ToString
            tdcurr.InnerHtml = curr
            lblpmtskid.Value = pmtskid
            tots.Open()
            GetTots(eqid)
            tots.Dispose()
        Else
            If Request.Form("lblsubmit") = "get" Then
                tots.Open()
                TransIt()
                tots.Dispose()
            End If
        End If
    End Sub
    Private Sub TransIt()
        pmstr = lblpmstr.Value
        pmtskid = lblpmtskid.Value
        Dim skillid, skill, freqid, freq, rdid, rd, tot, pm, sqty As String
        Dim ismeter, meterid, meter, meterunit, meterfreq, maxdays
        Dim pmarr() As String
        pmarr = pmstr.Split(",")
        'pmstr = skillid & "," & skill & "," & freqid & "," & freq & "," & rdid & "," & rd & "," & sqty & "," & ismeter _
        '+ "," & meterid & "," & meter & "," & meterunit & "," & meterfreq & "," & maxdays
        skill = pmarr(0)
        skillid = pmarr(1)
        freqid = pmarr(2)
        freq = pmarr(3)
        rdid = pmarr(4)
        rd = pmarr(5)
        sqty = pmarr(6)
        ismeter = pmarr(7)
        meterid = pmarr(8)
        meter = pmarr(9)
        meterunit = pmarr(10)
        meterfreq = pmarr(11)
        maxdays = pmarr(12)
        If meterid = "" Then
            
            sql = "update pmtasks set origskillid = '" & skill & "', origskill = '" & skillid & "', origqty = '" & sqty & "', " _
                + "origfreqid = '" & freqid & "', origfreq = '" & freq & "', " _
                + "origrdid = '" & rdid & "', origrd = '" & rd & "' where pmtskid = '" & pmtskid & "'"
            tots.Update(sql)
        Else

            sql = "update pmtasks set origskillid = '" & skill & "', origskill = '" & skillid & "', origqty = '" & sqty & "', " _
                + "origfreqid = '" & freqid & "', origfreq = '" & freq & "', " _
                + "origrdid = '" & rdid & "', origrd = '" & rd & "', " _
                + "usemetero = '1', meterido = '" & meterid & "', meterfreqo = '" & meterfreq & "', maxdayso = '" & maxdays & "' " _
                + "where pmtskid = '" & pmtskid & "'"""
            tots.Update(sql)
            If istpm = "yes" Then
                sql = "update tpmtasks set origskillid = '" & skill & "', origskill = '" & skillid & "', origqty = '" & sqty & "', " _
               + "origfreqid = '" & freqid & "', origfreq = '" & freq & "', " _
               + "origrdid = '" & rdid & "', origrd = '" & rd & "', " _
               + "where pmtskid = '" & tpmtskid & "'"""
                tots.Update(sql)
            End If
        End If

        tots.Update(sql)
        lblsubmit.Value = "go"

    End Sub
    Private Sub GetTots(ByVal eqid As String)
        Dim skillid, skill, freqid, freq, rdid, rd, tot, pm, pmstr, sqty As String
        Dim ismeter, meterid, meter, meterunit, meterfreq, maxdays
        'If typ = "1" Then
        sql = "select * from pmtottime where eqid = '" & eqid & "'"

        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")

        Dim noflg As Integer = 0

        dr = tots.GetRdrData(sql)
        While dr.Read
            noflg = 1
            skillid = dr.Item("skillid").ToString
            skill = dr.Item("skill").ToString
            freqid = dr.Item("freqid").ToString
            freq = dr.Item("freq").ToString
            rdid = dr.Item("rdid").ToString
            rd = dr.Item("rd").ToString
            sqty = dr.Item("skillqty").ToString

            ismeter = dr.Item("ismeter").ToString
            meterid = dr.Item("meterid").ToString
            meter = dr.Item("meter").ToString
            meterunit = dr.Item("meterunit").ToString
            meterfreq = dr.Item("meterfreq").ToString
            maxdays = dr.Item("maxdays").ToString

            pm = skill & "(" & sqty & ") / " & freq & " days / " & rd
            pmstr = skillid & "," & skill & "," & freqid & "," & freq & "," & rdid & "," & rd & "," & sqty & "," & ismeter _
                + "," & meterid & "," & meter & "," & meterunit & "," & meterfreq & "," & maxdays
            If ismeter = "1" Then
                pm += " - " & meter & " / " & meterunit & " / " & meterfreq
            End If
            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getpm('" & pmstr & "');"">" & pm & "</a></td></tr>")

        End While
        dr.Close()
        If noflg = 0 Then
            sb.Append("<tr><td class=""redlabel"">No Records Found</td></tr>")
        End If
        sb.Append("</table>")
        totdiv.InnerHtml = sb.ToString
        'End If
    End Sub

End Class