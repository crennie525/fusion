<%@ Page Language="vb" AutoEventWireup="false" Codebehind="draghelp2.aspx.vb" Inherits="lucy_r12.draghelp2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Drag and Drop Help</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
	</head>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="plainlabelblue">
						To Add Tasks in the Drag and Drop Feature you will first need to choose a PM 
						Cycle.
						<br>
						<br>
						A PM Cycle is defined as a unique combination of a Skill, Skill Quantity, 
						Frequency, and Running\Down Status.
						<br>
						<br>
						<img src="../images/appbuttons/minibuttons/download.gif">
						The ``Transfer PM Data to 
						Drag-n-Drop Screen`` button transfers your selected PM Cycle to the Drag and Drop Screen. 
						<br>
						<br>
						<img src="../images/appbuttons/minibuttons/magnifier.gif">
						The ``Select an Established Drag and Drop PM Cycle`` button allows you to select a PM Cycle
						that you have already created in the Drag and Drop Screen.
						<br>
						<br>
						<img src="../images/appbuttons/minibuttons/pencil12.gif">
						The ``View\Edit Original Total Values`` button allows you to edit Total Task and Down Times for
						PM Cycles that you have already created in the Drag and Drop Module.
						<br>
						<br>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
