<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="taskedit2.aspx.vb" Inherits="lucy_r12.taskedit2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>taskedit2</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="javascript" src="../scripts/pmtaskdivfuncedit_1016c.js"></script>
    <script language="JavaScript" type="text/javascript">
		<!--
        function checkdeltasktpm() {
            var chken = document.getElementById("lblenable").value;
            //if(chken!="1") {
            tid = document.getElementById("lbltaskid").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            //alert(tid + "," + tpmhold)
            if (tid != "" && tid != "0" && tpmhold != "0") {
                //var deltask = confirm("Are you sure you want to Delete this Task from\nthe TPM Module and Enable Editing?");
                var deltask = confirm("Removing this task from TPM will result in the loss of images and scheduling data.\nDo you want to continue?");
                if (deltask == true) {
                    document.getElementById("lblcompchk").value = "deltpm";
                    FreezeScreen('Your Data Is Being Processed...');
                    document.getElementById("form1").submit();
                }
                else if (deltask == false) {
                    return false;
                    alert("Action Cancelled")
                }
            }
            //}
        }
        function doEnable() {
            
                var tpmhold = document.getElementById("lbltpmhold").value;
                var ro = document.getElementById("lblro").value;
                var pg = document.getElementById("lblpg").innerHTML;
                document.getElementById("lblenable").value = "0";
                if (pg != "0" && ro != "1" && tpmhold != "1") {

                    document.getElementById("lbfaillist").disabled = false;
                    document.getElementById("lbfailmodes").disabled = false;
                    document.getElementById("lbCompFM").disabled = false;
                    document.getElementById("txtcQty").disabled = false;
                    document.getElementById("ibReuse").disabled = false;
                    document.getElementById("ibToTask").disabled = false;
                    document.getElementById("ibFromTask").disabled = false;
                    document.getElementById("txtdesc").disabled = false;
                    document.getElementById("ddtype").disabled = false;

                    document.getElementById("txtpfint").disabled = false;
                    document.getElementById("txtqty").disabled = false;
                    document.getElementById("txttr").disabled = false;
                    document.getElementById("txtrdt").disabled = false;
                    var sstr;
                    var ssti;
                    var typlst;
                    typlst = document.getElementById("ddtype");
                    sstr = typlst.options[typlst.selectedIndex].text
                    ssti = document.getElementById("ddtype").value;
                    if (sstr == "7") {
                        document.getElementById("ddpt").disabled = false;
                        try {
                            document.getElementById("ddpto").disabled = false;
                        }
                        catch (err) {

                        }
                    }

                    document.getElementById("cbloto").disabled = false;

                    document.getElementById("cbcs").disabled = false;

                    //document.getElementById("btnaddtsk").disabled=false;
                    //document.getElementById("btnaddsubtask").disabled=false;
                    try {
                        document.getElementById("btndeltask").disabled = false;
                    }
                    catch (err) {
                        //do nothing - button not used in optimizer
                    }
                    try {
                        document.getElementById("btntpm").disabled = false;
                    }
                    catch (err) {

                    }
                    document.getElementById("ibCancel").disabled = false;
                    //document.getElementById("btnsavetask").disabled=false;
                    if (oflag == "1") {
                        document.getElementById("lbofailmodes").disabled = false;
                        document.getElementById("txtodesc").disabled = false;
                        document.getElementById("ddtypeo").disabled = false;

                        document.getElementById("txtqtyo").disabled = false;
                        document.getElementById("txttro").disabled = false;
                        document.getElementById("txtordt").disabled = false;

                        document.getElementById("ddtaskstat").disabled = false;
                    }
                }
            else {
                if (pg == "0") {
                    alert("No Task Records Are Loaded Yet!")
                }
                else if (tpmhold == "1") {
                    alert("This has been designated as a TPM Task and Cannot be Edited!")
                }
            }
        }
        function editadj(typ) {
            var fuid = document.getElementById("lblfuid").value;
            var tid = document.getElementById("lbltaskid").value;
            var ro = document.getElementById("lblro").value;
            if (typ == "pre") {
                tid = 0;
            }
            var eReturn = window.showModalDialog("../appsman/PMSetAdjManDialog.aspx?fuid=" + fuid + "&typ=" + typ + "&tid=" + tid + "&ro=" + ro, "", "dialogHeight:520px; dialogWidth:930px; resizable=yes");
            if (eReturn) {
                if (eReturn != "log") {
                    document.getElementById("form1").submit();
                    //window.parent.setref();
                }
                else {
                    window.parent.setref();
                }
            }
            else {
                document.getElementById("form1").submit();
            }
        }
        function DisableButton(b) {
            document.getElementById("ibToTask").className = "details";
            document.getElementById("ibFromTask").className = "details";
            document.getElementById("ibfromo").className = "details";
            document.getElementById("ibtoo").className = "details";
            document.getElementById("ibReuse").className = "details";
            document.getElementById("todis").className = "view";
            document.getElementById("fromdis").className = "view";
            document.getElementById("todis1").className = "view";
            document.getElementById("fromdis1").className = "view";
            document.getElementById("fromreusedis").className = "view";
            document.getElementById("form1").submit();
        }
        function FreezeScreen(msg) {
            scroll(0, 0);
            var outerPane = document.getElementById('FreezePane');
            var innerPane = document.getElementById('InnerFreezePane');
            if (outerPane) outerPane.className = 'FreezePaneOn';
            if (innerPane) innerPane.innerHTML = msg;
        }
        function CheckChanges() {
            for (var i = 0; i < values.length; i++) {
                var elem = document.getElementById(ids[i]);
                if (elem)
                    if ((elem.type == 'checkbox' || elem.type == 'radio')
				  && values[i] != elem.checked)
                        var change = confirm("You have made changes to a Revised Task with a Task Status of Delete.\nAre you sure you want to save these changes?");
                    else if (!(elem.type == 'checkbox' || elem.type == 'radio') &&
				  elem.value != values[i])
                        var change = confirm("You have made changes to a Revised Task with a Task Status of Delete.\nAre you sure you want to save these changes?");

            }
            if (change == true) {

                document.getElementById("lblcompchk").value = "3";
                FreezeScreen('Your Data Is Being Processed...');
                document.getElementById("form1").submit();
            }
            else if (change == false) {
                //document.getElementById("lblsvchk").value = "0";
                return false;
            }
        }
        function valpgnums() {
            if (document.getElementById("lblenable").value != "1") {
                var desc = document.getElementById("txtdesc").innerHTML;
                var odesc = document.getElementById("txtodesc").innerHTML;
                if (desc.length > 500) {
                    alert("Maximum Lenth for Task Description is 500 Characters")
                }
                else if (odesc.length > 500) {
                    alert("Maximum Lenth for Task Description is 500 Characters")
                }
                else if (isNaN(document.getElementById("txtqtyo").value)) {
                    alert("Original Skill Qty is Not a Number")
                }
                else if (isNaN(document.getElementById("txtqty").value)) {
                    alert("Revised Skill Qty is Not a Number")
                }
                else if (isNaN(document.getElementById("txttro").value)) {
                    alert("Original Skill Time is Not a Number")
                }
                else if (isNaN(document.getElementById("txttr").value)) {
                    alert("Revised Skill Time is Not a Number")
                }
                else if (isNaN(document.getElementById("txtordt").value)) {
                    alert("Original Running/Down Time is Not a Number")
                }
                else if (isNaN(document.getElementById("txtrdt").value)) {
                    alert("Revised Running/Down Time is Not a Number")
                }
                else if (document.getElementById("ddtaskstat").value == 'Delete') {
                    CheckChanges();
                }
                else {
                    document.getElementById("lblcompchk").value = "3";
                    FreezeScreen('Your Data Is Being Processed...');
                    document.getElementById("form1").submit();
                }

            }
        }
        function GetGrid() {
            handleapp();
            document.getElementById("lblgrid").value = "yes";
            chk = document.getElementById("lblchk").value;
            cid = document.getElementById("lblcid").value;
            tid = document.getElementById("lbltaskid").value;
            funid = document.getElementById("lblfuid").value;
            comid = document.getElementById("lblco").value;
            clid = document.getElementById("lblclid").value;
            pmid = "none"//document.getElementById("lbldocpmid").value;
            if (pmid == "") pmid = "none";
            pmstr = document.getElementById("lbldocpmstr").Value
            if (pmstr == "") pmstr = "none";

            if (tid != "" || tid != "0") {
                window.open("../apps/GTasksFunc2.aspx?app=opt&tli=5a&funid=" + funid + "&comid=no&cid=" + cid + "&chk=" + chk + "&pmid=" + pmid + "&pmstr=" + pmstr + "&date=" + Date(), target = "_top");
            }
            else {
                alert("No Task Records Selected Yet!")
            }
        }

        function checkit() {
            var tchk = document.getElementById("lblusetot").value;
            if (tchk == "1") {
                document.getElementById("txttro").disabled = true;
                document.getElementById("txtqtyo").disabled = true;
                document.getElementById("txtordt").disabled = true;
            }

            populateArrays();
            var start = document.getElementById("lblstart").value;
            if (start == "yes1") {
                GetNavGrid();
            }
            document.getElementById("lblstart").value = "no";

            var grid = document.getElementById("lblgrid").value;
            if (grid == "yes") {
                window.location.reload(false);
                document.getElementById("form1").submit();
            }
            var log = document.getElementById("lbllog").value;
            if (log == "no") setref();
            else window.setTimeout("setref();", 1205000);
        }
        function setref() {
            window.parent.setref();
        }
        function GetNavGrid() {
            handleapp();
            cid = document.getElementById("lblcid").value;
            sid = document.getElementById("lblsid").value;
            did = document.getElementById("lbldid").value;
            clid = document.getElementById("lblclid").value;
            eqid = document.getElementById("lbleqid").value;
            chk = document.getElementById("lblchk").value;
            fuid = document.getElementById("lblfuid").value;
            tcnt = document.getElementById("lblcnt").value;
            if (tcnt != "0") {
                window.parent.handletasks("PMOptTasksGrid.aspx?start=yes&tl=5&tc=0&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:800px")
                //if (eReturn) {
                //	document.getElementById("lblsvchk").value = "7";
                //	document.getElementById("pgflag").value = eReturn;
                //	document.getElementById("form1").submit();
                //}
            }
        }
        function tasklookup() {
            var chken = document.getElementById("lblenable").value;
            if (chken != "1") {
                var str = document.getElementById("txtdesc").innerHTML;
                var eReturn = window.showModalDialog("../apps/TaskLookup.aspx?str=" + str, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes");
                //alert(eReturn)
                if (eReturn != "no" && eReturn != "~none~") {
                    document.getElementById("txtdesc").innerHTML = eReturn;
                }
                else if (eReturn != "~none~") {
                    document.getElementById("form1").submit();
                }
            }
        }
        function jumpto() {
            var typ = document.getElementById("ddtype").value;
            var pdm = document.getElementById("ddpt").value;
            var tid = document.getElementById("lbltaskid").value;
            var sid = document.getElementById("lblsid").value;
            var ro = document.getElementById("lblro").value;
            var eReturn = window.showModalDialog("../apps/commontasksdialog.aspx?typ=" + typ + "&pdm=" + pdm + "&tid=" + tid + "&sid=" + sid + "&ro=" + ro, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                if (eReturn == "task") {
                    document.getElementById("lblcompchk").value = "6";
                    //document.getElementById("lbltaskholdid").value = eReturn;
                    document.getElementById("form1").submit();
                }
            }
        }
        var popwin = "directories=0,height=500,width=800,location=0,menubar=0,resizable=0,status=0,toolbar=0,scrollbars=1";
        var poprep = "directories=0,height=600,width=900,location=0,menubar=0,resizable=0,status=0,toolbar=0,scrollbars=1";
        var poprepsm = "directories=0,height=390,width=300,location=0,menubar=0,resizable=0,status=0,toolbar=0,scrollbars=1";

        function getValueAnalysis() {
            tl = document.getElementById("lbltasklev").value;
            cid = document.getElementById("lblcid").value;
            sid = document.getElementById("lblsid").value;
            did = document.getElementById("lbldid").value;
            clid = document.getElementById("lblclid").value;
            eqid = document.getElementById("lbleqid").value;
            //alert(eqid)
            chk = document.getElementById("lblchk").value;
            fuid = document.getElementById("lblfuid").value;
            if (fuid != "") {
                var ht = screen.Height - 20;
                var wd = screen.Width - 20;
                var eReturn = window.showModalDialog("../reports/reportdialog.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight: 1000px; dialogWidth:2000px;resizable=yes");
                //var eReturn = window.showModalDialog("../reports/reportdialog.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:610px; dialogWidth:610px");
                if (eReturn) {
                    document.getElementById("lblsvchk").value = "7";
                    document.getElementById("pgflag").value = eReturn;
                    document.getElementById("form1").submit();
                }
            }
            else {
                alert("No Task Records Selected Yet!")
            }
        }
        function GetLubeDiv() {
            handleapp();
            var lock = document.getElementById("lbllock").value;
            if (lock != "1") {
                cid = document.getElementById("lblcid").value;
                ptid = document.getElementById("lbltaskid").value;
                tnum = document.getElementById("lblpg").innerHTML;
                var ro = document.getElementById("lblro").value;
                if (ptid != "") {
                    window.open("../inv/optTaskLubeList.aspx?ptid=" + ptid + "&cid=" + cid + "&tnum=" + tnum + "&ro=" + ro, "repWin", popwin);
                    document.getElementById("form1").submit();
                }
            }
        }
        function GetPartDiv() {
            handleapp();
            var lock = document.getElementById("lbllock").value;
            if (lock != "1") {
                cid = document.getElementById("lblcid").value;
                ptid = document.getElementById("lbltaskid").value;
                tnum = document.getElementById("lblpg").innerHTML;
                var ro = document.getElementById("lblro").value;
                if (ptid != "") {
                    window.open("../inv/optTaskPartList.aspx?ptid=" + ptid + "&cid=" + cid + "&tnum=" + tnum + "&ro=" + ro, "repWin", popwin);
                    document.getElementById("form1").submit();
                }
            }
        }
        function GetToolDiv() {
            var lock = document.getElementById("lbllock").value;
            if (lock != "1") {
                handleapp();
                cid = document.getElementById("lblcid").value;
                ptid = document.getElementById("lbltaskid").value;
                tnum = document.getElementById("lblpg").innerHTML;
                var ro = document.getElementById("lblro").value;
                if (ptid != "") {
                    window.open("../inv/optTaskToolList.aspx?ptid=" + ptid + "&cid=" + cid + "&tnum=" + tnum + "&ro=" + ro, "repWin", popwin);
                    document.getElementById("form1").submit();
                }
            }
        }
        function GetNoteDiv() {
            var lock = document.getElementById("lbllock").value;
            var ro = document.getElementById("lblro").value;
            if (lock != "1") {
                handleapp();
                ptid = document.getElementById("lbltaskid").value;
                if (ptid != "") {
                    window.open("../apps/TaskNotes.aspx?ptid=" + ptid + "&ro=" + ro, "repWin", popwin);
                    document.getElementById("form1").submit();
                }
            }
        }

        function GetRationale() {
            handleapp();
            tnum = document.getElementById("lblpg").innerHTML;
            tid = document.getElementById("lbltaskid").value;
            did = document.getElementById("lbldid").value;
            clid = document.getElementById("lblclid").value;
            eqid = document.getElementById("lbleqid").value;
            fuid = document.getElementById("lblfuid").value;
            coid = document.getElementById("lblco").value;
            sav = document.getElementById("lblsave").value;
            var ro = document.getElementById("lblro").value;
            if (tid == "") {
                alert("No Task Records Seleted!")
            }
            else if (sav == "no") {
                alert("Task Record Is Not Saved!")
            }
            else {
                var eReturn = window.showModalDialog("../appsopt/PMRationaleDialog.aspx?tnum=" + tnum + "&tid=" + tid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:730px; dialogWidth:750px; resizable=yes");
                if (eReturn) {
                    document.getElementById("form1").submit();
                }
            }
        }

        function getsgrid() {
            handleapp();
            tid = document.getElementById("lblpg").innerHTML; //document.getElementById("lbltaskid").value;
            fuid = document.getElementById("lblfuid").value;
            cid = document.getElementById("lblcid").value;
            sid = document.getElementById("lblsid").value;
            sav = document.getElementById("lblsave").value;
            eqid = document.getElementById("lbleqid").value;
            if (tid == "" || tid == "0") {
                alert("No Task Records Seleted!")
            }
            else if (sav == "no") {
                alert("Task Record Is Not Saved!")
            }
            else {
                var eReturn = window.showModalDialog("../apps/GSubDialog.aspx?sid=" + sid + "&cid=" + cid + "&tid=" + tid + "&fuid=" + fuid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:680px; dialogWidth:850px; resizable=yes");
                if (eReturn) {
                    document.getElementById("lblcompchk").value = "5"
                    document.getElementById("form1").submit();
                }
            }
        }

        function OpenFile(newstr, type) {
            handleapp();
            parent.parent.main.location.href = "doc.aspx?file=" + newstr + "&type=" + type;

        }
        function GetFuncDiv() {
            handleapp();
            cid = document.getElementById("lblcid").value;
            eqid = document.getElementById("lbleqid").value;
            var eReturn = window.showModalDialog("../equip/FuncDialog.aspx?cid=" + cid + "&eqid=" + eqid, "", "dialogHeight:250px; dialogWidth:600px; resizable=yes");
            if (eReturn) {
                document.getElementById("lblpchk").value = "func";
                document.getElementById("form1").submit();
            }
        }

        function GetCompDiv() {
            handleapp();
            var chken = document.getElementById("lblenable").value;
            fuid = document.getElementById("lblfuid").value;
            if (fuid.length != 0 || fuid != "" || fuid != "0") {
                cid = document.getElementById("lblcid").value;
                sid = document.getElementById("lblsid").value;
                eqid = document.getElementById("lbleqid").value;
                fuid = document.getElementById("lblfuid").value;
                ptid = document.getElementById("lbltaskid").value;
                tli = document.getElementById("lbltasklev").value;
                chk = document.getElementById("lblchk").value;
                var ro = document.getElementById("lblro").value;
                var eReturn = window.showModalDialog("../equip/CompDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&tli=" + tli + "&chk=" + chk + "&sid=" + sid + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:700px; dialogWidth:800px; resizable=yes");
                if (eReturn == "log") {
                    window.parent.handlelogout();
                }
                else if (eReturn) {
                    document.getElementById("lblcompchk").value = "1";
                }
                document.getElementById("form1").submit();
            }
        }
        function getss() {
            cid = document.getElementById("lblcid").value;
            sid = document.getElementById("lblsid").value;
            var ro = document.getElementById("lblro").value;
            var eReturn = window.showModalDialog("../admin/pmSiteSkillsDialog.aspx?sid=" + sid + "&cid=" + cid + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:520px; dialogWidth:470px; resizable=yes");
            if (eReturn) {
                document.getElementById("lblcompchk").value = "4";
                document.getElementById("form1").submit()
            }
        }
        function GetCompCopy() {
            handleapp();
            var chken = document.getElementById("lblenable").value;
            //if(chken!="1") {
            if (ptid != "") {
                fuid = document.getElementById("lblfuid").value;
                if (fuid.length != 0 || fuid != "" || fuid != "0") {
                    cid = document.getElementById("lblcid").value;
                    sid = document.getElementById("lblsid").value;
                    did = document.getElementById("lbldid").value;
                    clid = document.getElementById("lblclid").value;
                    eqid = document.getElementById("lbleqid").value;
                    ptid = document.getElementById("lbltaskid").value;
                    tli = document.getElementById("lbltasklev").value;
                    chk = document.getElementById("lblchk").value;
                    usr = document.getElementById("lblusername").value;
                    var ro = document.getElementById("lblro").value;
                    //alert("../equip/CompCopyMiniDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&tli=" + tli + "&chk=" + chk + "&sid=" + sid + "&date=" + Date())
                    //var eReturn = window.showModalDialog("../equip/CompCopyMiniDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&tli=" + tli + "&chk=" + chk + "&sid=" + sid + "&date=" + Date(), "", "dialogHeight:650px; dialogWidth:730px");
                    var eReturn = window.showModalDialog("../equip/CompCopyMiniDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&tli=" + tli + "&chk=" + chk + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&usr=" + usr + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:700px; dialogWidth:880px; resizable=yes");
                    if (eReturn == "log") {
                        window.parent.handlelogout();
                    }
                    else if (eReturn) {
                        document.getElementById("lblcompchk").value = "1";
                    }
                    document.getElementById("form1").submit()
                }
            }
            //}

        }
        function getPFDiv() {
            handleapp();
            var chken = document.getElementById("lblenable").value;
            if (chken != "1") {
                var eq = document.getElementById("lbleqid").value;
                var valu = document.getElementById("lbltaskid").value;
                var eReturn = window.showModalDialog("../equip/PFIntDialog.aspx?tid=" + valu + "&eqid=" + eq, "", "dialogHeight:560px; dialogWidth:669px; resizable=yes");
            }
            if (eReturn) {
                document.getElementById("txtpfint").value = eReturn;
                document.getElementById("form1").submit()
            }
        }
        function GetFailDiv() {
            var chken = document.getElementById("lblenable").value;
            if (chken != "1") {
                if (ptid != "") {
                    cid = document.getElementById("lblcid").value;
                    eqid = document.getElementById("lbleqid").value;
                    fuid = document.getElementById("lblfuid").value;
                    ptid = document.getElementById("lbltaskid").value;
                    coid = document.getElementById("lblco").value;
                    var ro = document.getElementById("lblro").value;
                    cvalu = document.getElementById("tdcompnum").innerHTML;
                    cind = document.getElementById("lblco").value; //document.getElementById("ddcomp").value;
                    if (cind != "0") {
                        var eReturn = window.showModalDialog("../equip/CompFailDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&cvalu=" + cvalu + "&ro=" + ro, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                    }
                    else {
                        alert("No Component Selected")
                    }
                }
            }
            if (eReturn) {
                document.getElementById("lblcompfailchk").value = "1";
                document.getElementById("form1").submit()
            }
        }
        function controlrow(id) {
            document.getElementById("lbltabid").value = id;
            vwflg = document.getElementById(id).className;
            closerows();
            if (id == "tbeq" && vwflg == "details") {
                document.getElementById(id).className = "view";
                document.getElementById('tbeq2').className = "view";
                document.getElementById('imgeq').src = "../images/appbuttons/optdown.gif";
            }
            else if (id == "tbdtls" && vwflg == "details") {
                document.getElementById(id).className = "view";
                document.getElementById("tbdtls2").className = "view";
                document.getElementById('imgdtls').src = "../images/appbuttons/optdown.gif";
            }
            else if (id == "tbreqs" && vwflg == "details") {
                document.getElementById(id).className = "view";
                document.getElementById('imgreqs').src = "../images/appbuttons/optdown.gif";
            }
        }

        function closerows() {
            document.getElementById('tbeq').className = "details";
            document.getElementById('tbeq2').className = "details";
            document.getElementById('imgeq').src = "../images/appbuttons/optup.gif";
            document.getElementById('tbdtls').className = "details";
            document.getElementById('tbdtls2').className = "details";
            document.getElementById('imgdtls').src = "../images/appbuttons/optup.gif";
            document.getElementById('tbreqs').className = "details";
            document.getElementById('imgreqs').src = "../images/appbuttons/optup.gif";
        }
        function CheckDel(val) {

            if (val == "Delete") {
                var decision = confirm("Are You Sure You Want to Mark this Task as Delete?")
                if (decision == true) {
                    //document.getElementById("submit('ddcomp', '');
                    FreezeScreen('Your Data Is Being Processed...');
                    document.getElementById("lbldel").value = "delr"
                    document.getElementById("form1").submit();
                }
                else {
                    alert("Action Cancelled")
                }

            }
        }
        function GoToPMLib() {
            handleapp();
            window.open("../equip/EQCopy.aspx?date=" + Date(), target = "_top");
        }
        function pmidtest() {
            alert(document.getElementById("lbldocpmid").value)
        }
        function getMeasDiv() {
            var chken = document.getElementById("lblenable").value;
            if (chken != "1") {
                var coid = document.getElementById("lblco").value;
                var valu = document.getElementById("lbltaskid").value;
                var ro = document.getElementById("lblro").value;
                var eReturn = window.showModalDialog("../utils/tmdialog.aspx?typ=pm&taskid=" + valu + "&coid=" + coid + "&ro=" + ro, "", "dialogHeight:650px; dialogWidth:550px; resizable=yes");
            }
            if (eReturn) {
                document.getElementById("form1").submit();
            }
        }
        function handleapp() {
            //var app = document.getElementById("appchk").value;
            //if(app=="switch") window.parent.rtop.handleapp(app);
        }
        function GetType(app, sval) {
            //alert(app + ", " + sval)
            var ghostoff = document.getElementById("lblghostoff").value;
            var xstatus = document.getElementById("lblxstatus").value;
            //alert(ghostoff + ", " + xstatus)
            var xflg = 0;
            if (ghostoff == "yes" && xstatus == "yes") {
                xflg = 1;
            }
            var typlst
            var sstr
            var ssti
            if (app == "d") {
                typlst = document.getElementById("ddtype");
                ptlst = document.getElementById("ddpt");
                sstr = typlst.options[typlst.selectedIndex].text;
                ssti = document.getElementById("ddtype").value;
                //sstr == "4 - Cond Monitoring"
                if (ssti == "7") {
                    document.getElementById("ddpt").disabled = false;
                }
                else {
                    ptlst.value = "0";
                    document.getElementById("ddpt").disabled = true;
                }
            }
            else if (app == "o") {
                if (xflg == 0) {
                    document.getElementById("ddtype").value = sval;
                }
                typlst = document.getElementById("ddtypeo");
                ptlst = document.getElementById("ddpt");
                ptolst = document.getElementById("ddpto");
                sstr = typlst.options[typlst.selectedIndex].text;
                ssti = document.getElementById("ddtypeo").value;
                if (ssti == "7") {
                    if (xflg == 0) {
                        document.getElementById("ddpt").disabled = false;
                    }
                    document.getElementById("ddpto").disabled = false;
                }
                else {
                    if (xflg == 0) {
                        ptlst.value = "0";
                        document.getElementById("ddpt").disabled = true;
                    }
                    ptolst.value = "0";
                    document.getElementById("ddpto").disabled = true;
                }
            }
        }
        function filldown(id, val) {
            var ghostoff = document.getElementById("lblghostoff").value;
            var xstatus = document.getElementById("lblxstatus").value;
            var xflg = 0;
            if (ghostoff == "yes" && xstatus == "yes") {
                xflg = 1;
            }
            if (id == 'o') {
                if (xflg == 0) {
                    document.getElementById('txttr').value = val;
                }
                var dt = document.getElementById("ddeqstato").options[document.getElementById("ddeqstato").options.selectedIndex].text;
                if (dt == "Down") {
                    document.getElementById("txtordt").value = val;
                    if (xflg == 0) {
                        document.getElementById("txtrdt").value = val;
                    }
                }
            }
            else {
                var dt = document.getElementById("ddeqstat").options[document.getElementById("ddeqstat").options.selectedIndex].text;
                if (dt == "Down") {
                    document.getElementById("txtrdt").value = val;
                }
            }
        }
        function zerodt(id, val) {

            if (id == 'o') {
                var dt = document.getElementById("form1").ddeqstato.options[document.getElementById("form1").ddeqstato.options.selectedIndex].text;
                if (dt != "Down") {
                    document.getElementById("txtordt").value = 0;
                    document.getElementById("txtrdt").value = "0";
                }
                else {
                    document.getElementById("txtordt").value = document.getElementById("txttro").value;
                    document.getElementById("txtrdt").value = document.getElementById("txttr").value;
                }
            }
            else {
                var dt = document.getElementById("form1").ddeqstat.options[document.getElementById("form1").ddeqstat.options.selectedIndex].text;
                if (dt != "Down") {
                    document.getElementById("txtrdt").value = "0";
                }
                else {
                    document.getElementById("txtrdt").value = document.getElementById("txttr").value;
                }
            }
        }
        function checkret() {
            var ret = document.getElementById("txtodesc").innerHTML;
            window.parent.handleret("1");
        }
		-->
    </script>
</head>
<body class="tbg" onload="page_maint('o');checkit();" onunload="checkret();">
    <div id="overDiv" style="z-index: 1000; position: absolute; visibility: hidden">
    </div>
    <form id="form1" method="post" runat="server">
    <div class="FreezePaneOff" id="FreezePane" align="center">
        <div class="InnerFreezePane" id="InnerFreezePane">
        </div>
    </div>
    <div style="z-index: 100; position: absolute; top: 4px; left: 4px">
        <table cellspacing="0" cellpadding="3" width="742">
            <tr>
                <td class="thdrsinglft" align="left" width="26">
                    <img src="../images/appbuttons/minibuttons/optbg.gif" border="0">
                </td>
                <td class="thdrsingrt label" onmouseover="return overlib('Against what is the PM intended to protect?', ABOVE, LEFT)"
                    onmouseout="return nd()" width="696">
                    Failure Modes, Causes And/Or Effects
                </td>
            </tr>
        </table>
        <table id="tbeq" cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td class="label" width="140" height="20">
                    Component Addressed:
                </td>
                <td class="bluelabellt" width="242" id="tdcompnum" runat="server">
                    Component #
                </td>
                <td class="label" width="48">
                </td>
                <td class="label" width="30">
                    Qty:
                </td>
                <td class="label" width="282">
                    <asp:TextBox ID="txtcQty" runat="server" Width="40px" CssClass="plainlabel"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table id="tbeq2" cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td width="365">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="20">
                            </td>
                            <td width="150">
                            </td>
                            <td width="20">
                            </td>
                            <td width="5">
                            </td>
                            <td width="150">
                            </td>
                            <td width="20">
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#edf0f3">
                                &nbsp;
                            </td>
                            <td class="label" align="center" bgcolor="#edf0f3">
                                Original Failure Modes
                            </td>
                            <td bgcolor="#edf0f3">
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class="bluelabel" align="center" bgcolor="#bad8fc">
                                Revised Failure Modes
                            </td>
                            <td bgcolor="#bad8fc">
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#edf0f3">
                                &nbsp;
                            </td>
                            <td align="center" bgcolor="#edf0f3">
                                <asp:ListBox ID="lbofailmodes" runat="server" Width="150px" CssClass="plainlabel"
                                    ForeColor="Blue" Height="50px" SelectionMode="Multiple"></asp:ListBox>
                            </td>
                            <td valign="middle" align="center" bgcolor="#edf0f3">
                                <img class="details" id="todis1" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                                    width="20">
                                <img class="details" id="fromdis1" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                    width="20">
                                <asp:ImageButton ID="ibfromo" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardbg.gif">
                                </asp:ImageButton><br>
                                <asp:ImageButton ID="ibtoo" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbgdrk.gif">
                                </asp:ImageButton>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td align="center" bgcolor="#bad8fc">
                                <asp:ListBox ID="lbCompFM" runat="server" Width="150px" CssClass="plainlabel" Height="50px"
                                    SelectionMode="Multiple"></asp:ListBox>
                            </td>
                            <td valign="middle" align="center" bgcolor="#bad8fc">
                                <img id="btnaddnewfail" onmouseover="return overlib('Add a New Failure Mode to the Component Selected Above')"
                                    onclick="GetFailDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/addblu.gif"
                                    width="20"><br>
                                <asp:ImageButton ID="ibReuse" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardblu.gif">
                                </asp:ImageButton><img class="details" id="fromreusedis" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                    width="20">
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="377">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="150">
                            </td>
                            <td width="20">
                            </td>
                            <td width="150">
                            </td>
                            <td width="47">
                            </td>
                            <td width="10">
                            </td>
                        </tr>
                        <tr>
                            <td class="redlabel" align="center" bgcolor="#bad8fc">
                                Not Addressed
                            </td>
                            <td bgcolor="#bad8fc">
                                &nbsp;
                            </td>
                            <td class="bluelabel" align="center" bgcolor="#bad8fc">
                                Selected
                            </td>
                            <td bgcolor="#bad8fc">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#bad8fc">
                                <asp:ListBox ID="lbfaillist" runat="server" Width="150px" CssClass="plainlabel" ForeColor="Red"
                                    Height="50px" SelectionMode="Multiple"></asp:ListBox>
                            </td>
                            <td valign="middle" align="center" bgcolor="#bad8fc">
                                <img class="details" id="todis" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                                    width="20">
                                <img class="details" id="fromdis" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                    width="20">
                                <asp:ImageButton ID="ibToTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardblu.gif">
                                </asp:ImageButton><br>
                                <asp:ImageButton ID="ibFromTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/backblu.gif">
                                </asp:ImageButton>
                            </td>
                            <td align="center" bgcolor="#bad8fc">
                                <asp:ListBox ID="lbfailmodes" runat="server" Width="150px" CssClass="plainlabel"
                                    ForeColor="Blue" Height="50px" SelectionMode="Multiple"></asp:ListBox>
                            </td>
                            <td bgcolor="#bad8fc">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td colspan="2">
                    <img height="2" src="../images/appbuttons/minibuttons/2PX.gif">
                </td>
            </tr>
            <tr>
                <td class="thdrsinglft" align="left" width="26">
                    <img src="../images/appbuttons/minibuttons/optbg.gif" border="0">
                </td>
                <td class="thdrsingrt label" onmouseover="return overlib('What are you going to do?')"
                    onmouseout="return nd()" width="684" colspan="3">
                    Task Activity Details
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="2" width="372">
                        <tr>
                            <td width="30">
                            </td>
                            <td width="140">
                            </td>
                            <td width="30">
                            </td>
                            <td width="140">
                            </td>
                            <td width="2">
                            </td>
                        </tr>
                        <tr>
                            <td class="label" bgcolor="#edf0f3" colspan="4">
                                Original Task Description
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#edf0f3" colspan="4">
                                <textarea id="txtodesc" onkeyup="document.getElementById('txtdesc').innerHTML=this.value;"
                                    style="width: 296px; font-family: Arial; height: 40px; font-size: 12px" name="txtdesc"
                                    cols="34" onchange="document.getElementById('txtdesc').innerHTML=this.value;"
                                    runat="server"></textarea>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" bgcolor="#edf0f3">
                                Type
                            </td>
                            <td bgcolor="#bad8fc">
                                <asp:ListBox ID="ddtypeo" runat="server" Width="140px" CssClass="plainlabel" DataValueField="ttid"
                                    DataTextField="tasktype" Rows="1"></asp:ListBox>
                            </td>
                            <td class="label" bgcolor="#edf0f3">
                                PdM
                            </td>
                            <td bgcolor="#edf0f3">
                                <asp:DropDownList ID="ddpto" runat="server" Width="140px" CssClass="plainlabel">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <img src="../images/appbuttons/minibuttons/2PX.gif">
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table cellspacing="0" cellpadding="2" width="370">
                        <tr>
                            <td width="30">
                            </td>
                            <td width="140">
                            </td>
                            <td width="30">
                            </td>
                            <td width="90">
                            </td>
                            <td width="50">
                            </td>
                        </tr>
                        <tr>
                            <td class="label" bgcolor="#bad8fc" colspan="5">
                                Revised Task Description
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#bad8fc" colspan="4">
                                <textarea class="plainlabel" id="txtdesc" style="width: 290px; font-family: Arial;
                                    height: 40px; font-size: 12px" name="txtdesc" cols="32" runat="server"></textarea>
                            </td>
                            <td bgcolor="#bad8fc">
                                <img id="btnlookup" onclick="tasklookup();" src="../images/appbuttons/minibuttons/lookupblu.gif"
                                    runat="server">
                                <img id="btnlookup2" onclick="jumpto();" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    runat="server">
                                <br>
                                <img onmouseover="return overlib('Add/Edit Measurements for this Task')" onclick="getMeasDiv();"
                                    onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/measblu.gif"
                                    width="27">
                            </td>
                        </tr>
                        <tr>
                            <td class="label" bgcolor="#bad8fc">
                                Type
                            </td>
                            <td bgcolor="#bad8fc">
                                <asp:ListBox ID="ddtype" runat="server" Width="140px" CssClass="plainlabel" DataValueField="ttid"
                                    DataTextField="tasktype" Rows="1"></asp:ListBox>
                            </td>
                            <td class="label" bgcolor="#bad8fc">
                                PdM
                            </td>
                            <td bgcolor="#bad8fc" colspan="2">
                                <asp:DropDownList ID="ddpt" runat="server" Width="140px" CssClass="plainlabel">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="2" width="742">
            <tr>
                <td>
                    <img height="2" src="../images/appbuttons/minibuttons/2PX.gif" width="2">
                </td>
            </tr>
            <tr>
                <td class="thdrsinglft" align="left" width="26">
                    <img src="../images/appbuttons/minibuttons/optbg.gif" border="0">
                </td>
                <td class="thdrsingrt label" onmouseover="return overlib('How are you going to do it?')"
                    onmouseout="return nd()" width="554" colspan="3">
                    Planning &amp; Scheduling Details
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td valign="top">
                    <table cellspacing="0" cellpadding="2" width="372">
                        <tr>
                            <td width="92">
                            </td>
                            <td width="78">
                            </td>
                            <td width="16">
                            </td>
                            <td width="30">
                            </td>
                            <td width="30">
                            </td>
                            <td width="60">
                            </td>
                            <td width="20">
                            </td>
                            <td width="20">
                            </td>
                            <td width="31">
                            </td>
                            <td width="2">
                            </td>
                        </tr>
                        <tr>
                            <td class="label" bgcolor="#edf0f3" colspan="2">
                                Original Skill
                            </td>
                            <td class="label" bgcolor="#edf0f3">
                                Qty
                            </td>
                            <td class="label" bgcolor="#edf0f3" colspan="6">
                                Min Ea
                            </td>
                            <td>
                                <img src="../images/appbuttons/minibuttons/2PX.gif">
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#edf0f3" colspan="2" id="tdoskill" runat="server" class="bluelabellt">
                            </td>
                            <td bgcolor="#edf0f3">
                                <asp:TextBox ID="txtqtyo" runat="server" Width="24PX" CssClass="plainlabel"></asp:TextBox>
                            </td>
                            <td bgcolor="#edf0f3" colspan="6">
                                <asp:TextBox ID="txttro" runat="server" Width="35px" CssClass="plainlabel"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="width: 88px" bgcolor="#edf0f3">
                                Frequency
                            </td>
                            <td bgcolor="#edf0f3" id="tdofreq" runat="server" class="bluelabellt">
                            </td>
                            <td class="label" bgcolor="#edf0f3" colspan="3">
                                Status
                            </td>
                            <td bgcolor="#edf0f3" colspan="2" id="tdostat" runat="server" class="bluelabellt">
                            </td>
                            <td class="label" bgcolor="#edf0f3">
                                DT
                            </td>
                            <td bgcolor="#edf0f3">
                                <asp:TextBox ID="txtordt" runat="server" Width="35px" CssClass="plainlabel"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabel">
                                Task Status
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="ddtaskstat" runat="server" CssClass="plainlabel" AutoPostBack="False">
                                    <asp:ListItem Value="Select">Select</asp:ListItem>
                                    <asp:ListItem Value="UnChanged">UnChanged</asp:ListItem>
                                    <asp:ListItem Value="Add">Add</asp:ListItem>
                                    <asp:ListItem Value="Revised">Revised</asp:ListItem>
                                    <asp:ListItem Value="Delete">Delete</asp:ListItem>
                                    <asp:ListItem Value="Detailed Steps">Detailed Steps</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td id="tdtpmhold" class="plainlabelred" colspan="5" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table cellspacing="0" cellpadding="2" width="370">
                        <tr>
                            <td width="60">
                            </td>
                            <td width="70">
                            </td>
                            <td width="20">
                            </td>
                            <td width="30">
                            </td>
                            <td width="30">
                            </td>
                            <td width="40">
                            </td>
                            <td width="30">
                            </td>
                            <td width="20">
                            </td>
                            <td width="20">
                            </td>
                            <td width="35">
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="width: 134px" bgcolor="#bad8fc" colspan="2">
                                Revised Skill
                            </td>
                            <td class="label" bgcolor="#bad8fc">
                                Qty
                            </td>
                            <td class="label" bgcolor="#bad8fc" colspan="3">
                                Min Ea
                            </td>
                            <td class="label" bgcolor="#bad8fc" colspan="3">
                                P-F Interval
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 134px" bgcolor="#bad8fc" colspan="2" id="tdskill" runat="server"
                                class="bluelabellt">
                            </td>
                            <td bgcolor="#bad8fc">
                                <asp:TextBox ID="txtqty" runat="server" Width="30px" CssClass="plainlabel"></asp:TextBox>
                            </td>
                            <td bgcolor="#bad8fc" colspan="3">
                                <asp:TextBox ID="txttr" runat="server" Width="35px" CssClass="plainlabel"></asp:TextBox>
                            </td>
                            <td bgcolor="#bad8fc">
                                <asp:TextBox ID="txtpfint" runat="server" Width="35px" CssClass="plainlabel"></asp:TextBox>
                            </td>
                            <td bgcolor="#bad8fc">
                                <img id="btnpfint" onmouseover="return overlib('Estimate Frequency using the PF Interval')"
                                    onclick="getPFDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/lilcalcblu.gif"
                                    width="20">
                            </td>
                            <td bgcolor="#bad8fc">
                            </td>
                        </tr>
                        <tr>
                            <td class="label" bgcolor="#bad8fc">
                                Frequency
                            </td>
                            <td style="width: 70px" bgcolor="#bad8fc" id="tdfreq" runat="server" class="bluelabellt">
                            </td>
                            <td class="label" bgcolor="#bad8fc" colspan="3">
                                Status
                            </td>
                            <td bgcolor="#bad8fc" colspan="2" id="tdstat" runat="server" class="bluelabellt">
                            </td>
                            <td class="label" style="width: 19px" bgcolor="#bad8fc">
                                DT
                            </td>
                            <td bgcolor="#bad8fc">
                                <asp:TextBox ID="txtrdt" runat="server" Width="35px" CssClass="plainlabel"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelsm" colspan="6">
                                <asp:CheckBox ID="cbloto" runat="server"></asp:CheckBox>LOTO&nbsp;<asp:CheckBox ID="cbcs"
                                    runat="server"></asp:CheckBox>CS
                            </td>
                            <td class="bluelabel" align="right" colspan="4">
                                <img id="Img3" onmouseover="return overlib('Add/Edit Parts for this Task')" onclick="GetPartDiv();"
                                    onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/parttrans.gif"
                                    width="23" runat="server"><img onmouseover="return overlib('Add/Edit Tools for this Task')"
                                        onclick="GetToolDiv();" onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/tooltrans.gif"
                                        width="23"><img onmouseover="return overlib('Add/Edit Lubricants for this Task')"
                                            onclick="GetLubeDiv();" onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/lubetrans.gif"
                                            width="23"><img onmouseover="return overlib('Add/Edit Notes for this Task')" onclick="GetNoteDiv();"
                                                onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/notessm.gif"
                                                width="25">
                                <img id="Img5" onmouseover="return overlib('Edit Pass/Fail Adjustment Levels - This Task', ABOVE, LEFT)"
                                    onclick="editadj('0');" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/screwd.gif"
                                    border="0" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="742" border="0">
            <tr id="Tr1" runat="server" name="Tr1">
                <td align="center" style="border-top: #7ba4e0 thin solid">
                    <asp:ImageButton ID="btnaddsubtask" runat="server" CssClass="details" ImageUrl="../images/appbuttons/minibuttons/subtask.gif">
                    </asp:ImageButton><img id="btnedittask" onclick="doEnable();" border="0" alt=""
                            src="../images/appbuttons/minibuttons/lilpentrans.gif" width="19" height="19"
                            runat="server"><asp:ImageButton ID="ibCancel" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif">
                    </asp:ImageButton>
                    <img id="IMG2" onclick="valpgnums();" height="20" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif"
                        width="20" runat="server"><img id="imgrat" onmouseover="return overlib('Add/Edit Rationale for Task Changes')"
                            onclick="GetRationale();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/rationale.gif"
                            width="20" runat="server"><img onmouseover="return overlib('View Reports')" onclick="getValueAnalysis();"
                                onmouseout="return nd()" height="24" alt="" src="../images/appbuttons/minibuttons/report.gif"
                                width="24"><img id="sgrid" onmouseover="return overlib('Add/Edit Sub Tasks')" onclick="getsgrid();"
                                    onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/sgrid1.gif"
                                    width="20">
                    <img id="Img4" onmouseover="return overlib('Edit Pass/Fail Adjustment Levels - All Tasks', ABOVE, LEFT)"
                        onclick="editadj('pre');" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/screwall.gif"
                        border="0" runat="server"><asp:ImageButton ID="btntpm" runat="server" ImageUrl="../images/appbuttons/minibuttons/compresstpm.gif">
                        </asp:ImageButton><img id="imgdeltpm" class="details" onmouseover="return overlib('Delete This Task From the TPM Module and Restore Editing', ABOVE, LEFT)"
                        onmouseout="return nd()" onclick="checkdeltasktpm();" border="0" alt="" src="../images/appbuttons/minibuttons/cantpm.gif"
                        runat="server">
                </td>
                <td style="border-top: #7ba4e0 thin solid" align="center" height="30">
                    <asp:Label ID="Label22" runat="server" CssClass="bluelabel" ForeColor="Blue" Font-Size="X-Small"
                        Font-Names="Arial" Font-Bold="True">Task# </asp:Label><asp:Label ID="lblpg" runat="server"
                            CssClass="bluelabel" ForeColor="Blue" Font-Size="X-Small" Font-Names="Arial"
                            Font-Bold="True"></asp:Label><asp:Label ID="Label23" runat="server" CssClass="bluelabel"
                                ForeColor="Blue" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"> of </asp:Label><asp:Label
                                    ID="lblcnt" runat="server" CssClass="bluelabel" ForeColor="Blue" Font-Size="X-Small"
                                    Font-Names="Arial" Font-Bold="True"></asp:Label>
                </td>
                <td style="border-top: #7ba4e0 thin solid" align="center">
                    <asp:Label ID="Label26" runat="server" CssClass="greenlabel" Font-Bold="True" Font-Names="Arial"
                        Font-Size="X-Small">Sub Tasks: </asp:Label><asp:Label ID="lblsubcount" runat="server"
                            CssClass="greenlabel" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"></asp:Label><asp:Label
                                ID="lblspg" runat="server" CssClass="details" ForeColor="Green" Font-Bold="True"
                                Font-Names="Arial" Font-Size="X-Small"></asp:Label><asp:Label ID="Label27" runat="server"
                                    CssClass="details" ForeColor="Green" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"> of </asp:Label><asp:Label
                                        ID="lblscnt" runat="server" CssClass="details" ForeColor="Green" Font-Bold="True"
                                        Font-Names="Arial" Font-Size="X-Small"></asp:Label>
                </td>
                <td style="border-top: #7ba4e0 thin solid">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    <input id="lblsvchk" type="hidden" runat="server" name="lblsvchk">
    <input id="lblcompchk" type="hidden" name="lblcompchk" runat="server">
    <input id="lblsb" type="hidden" name="lblsb" runat="server">
    <input id="lblfilt" type="hidden" name="lblfilt" runat="server">
    <input id="lblenable" type="hidden" name="lblenable" runat="server">
    <input id="lblpgholder" type="hidden" name="lblpgholder" runat="server">
    <input id="lblcompfailchk" type="hidden" name="lblcompfailchk" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblstart" type="hidden" name="lblpmtype" runat="server">
    <input id="lblt" type="hidden" name="lblt" runat="server">
    <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
    <input id="lblst" type="hidden" name="lblst" runat="server">
    <input id="lblco" type="hidden" name="lblco" runat="server">
    <input id="lblpar" type="hidden" name="lblpar" runat="server">
    <input id="lbltabid" type="hidden" name="lbltabid" runat="server">
    <input id="lblpmstr" type="hidden" name="lblpmstr" runat="server">
    <input id="pgflag" type="hidden" name="pgflag" runat="server">
    <input id="lblpmid" type="hidden" name="lblpmid" runat="server">
    <input id="lbldid" type="hidden" runat="server" name="lbldid">
    <input id="lblclid" type="hidden" runat="server" name="lblclid"><input id="lblsid"
        type="hidden" runat="server" name="lblsid">
    <input id="lbldel" type="hidden" runat="server" name="lbldel"><input id="lblsave"
        type="hidden" runat="server" name="lblsave">
    <input id="lblgrid" type="hidden" runat="server" name="lblgrid"><input id="lblcind"
        type="hidden" runat="server" name="lblcind">
    <input id="lblcurrsb" type="hidden" runat="server" name="lblcurrsb"><input id="lblcurrcs"
        type="hidden" runat="server" name="lblcurrcs">
    <input id="lbldocpmid" type="hidden" runat="server" name="lbldocpmid"><input id="lbldocpmstr"
        type="hidden" runat="server" name="lbldocpmstr">
    <input id="lblsesscnt" type="hidden" runat="server" name="lblsesscnt"><input id="appchk"
        type="hidden" name="appchk" runat="server">
    <input id="lbllog" type="hidden" name="lbllog" runat="server"><input id="lblfiltcnt"
        type="hidden" runat="server" name="lblfiltcnt">
    <input id="lblusername" type="hidden" runat="server" name="lblusername">
    <input id="lbllock" type="hidden" name="lbllock" runat="server">
    <input id="lbllockedby" type="hidden" name="lbllockedby" runat="server">
    <input type="hidden" id="lblhaspm" runat="server" name="lblhaspm">
    <input type="hidden" id="lblro" runat="server"><input type="hidden" id="lblusetot"
        runat="server">
    <input type="hidden" id="lblghostoff" runat="server" />
    <input type="hidden" id="lblusemeter" runat="server" />
    <input type="hidden" id="lblxstatus" runat="server" />
    <input id="lbltpmalert" type="hidden" name="lbltpmalert" runat="server"><input id="lbltpmhold"
        type="hidden" runat="server">
        <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
