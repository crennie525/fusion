

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class OVRollup_OP
    Inherits System.Web.UI.Page
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim ovr As New Utilities
    Dim sid, eqid, eqstr, typ As String
    Dim skillstr As String
    Dim ohour As String
    Dim rhour As String
    Dim rhourb As String
    Dim tmod As New transmod
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        

'Put user code to initialize the page here
        typ = Request.QueryString("typ").ToString
        If typ = "site" Then
            sid = Request.QueryString("sid").ToString
            PopEqVal(typ, sid)
        ElseIf typ = "eq" Then
            eqid = Request.QueryString("eqid").ToString
            PopEqVal(typ, eqid)
        ElseIf typ = "select" Then
            sid = Request.QueryString("sid").ToString
            eqstr = Request.QueryString("eqstr").ToString
            PopEqVal(typ, sid, eqstr)
        End If
    End Sub
    Private Sub PopEqVal(ByVal typ As String, ByVal sid As String, Optional ByVal eqstr As String = "")
        Dim scnt As Integer
        scnt = eqstr.Length
        Dim es, ee As String
        es = eqstr
        es = Mid(es, 1)
        es = Replace(es, "'", "''")
        ee = eqstr
        eqstr = es

        Dim sb As New StringBuilder
        sb.Append("<html><head>")
        sb.Append("<LINK href=""../styles/pmcssa1.css"" type=""text/css"" rel=""stylesheet"">")
        sb.Append("</head><body>")
        sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""700"">")
        Dim title As String
        If typ = "site" Then
            title = "Site Rollup"
        ElseIf (typ = "select") Then
            title = "Asset Select"
        End If
        sb.Append("<tr><td class=""bigbold1"" colspan=""3"" align=""center"">" & tmod.getxlbl("xlb362" , "OVRollup_OP.aspx.vb") & " " & title & "</td>")
        Dim ocraft_min, rcraft_min, lab_min, rcraft_min_before, lab_min_before As String
        Dim ocraft_hrs, rcraft_hrs, lab_sav, lab_per, rcraft_hrs_before, lab_sav_before, lab_per_before As String
        Dim ordt_min, rrdt_min, rdt_min, rrdt_min_before, rdt_min_before As String
        Dim ordt_hrs, rrdt_hrs, rdt_sav, rrdt_hrs_before, rdt_sav_before As String
        Dim omat, rmat, mat_sav, rmat_before, mat_sav_before As String
        ovr.Open()
        Select Case typ
            Case "eq"
                sql = "usp_optValAnlEq4_op '" & eqid & "'"
            Case "site"
                sql = "usp_optValAnlEq4PS '" & sid & "'"
            Case "select"
                sql = "usp_optValAnlEq4PSS '" & sid & "', '" & eqstr & "'"
        End Select

        Try
            dr = ovr.GetRdrData(sql)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1615" , "OVRollup_OP.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Response.Write("Not Enough Data To Create Overview")
        End Try

        While dr.Read
            ocraft_min = dr.Item("origttime_min_yr").ToString
            rcraft_min = dr.Item("ttime_min_yr").ToString
            lab_min = dr.Item("lab_min_yr").ToString 'tot min

            rcraft_min_before = dr.Item("ttime_min_yr_before").ToString
            lab_min_before = dr.Item("lab_min_yr_before").ToString 'tot min

            ocraft_hrs = dr.Item("origttime_hrs_yr").ToString
            rcraft_hrs = dr.Item("ttime_hrs_yr").ToString
            lab_sav = dr.Item("lab_sav").ToString 'hours saved

            rcraft_hrs_before = dr.Item("ttime_hrs_yr_before").ToString
            lab_sav_before = dr.Item("lab_sav_before").ToString 'hours saved

            ordt_min = dr.Item("origrdt_min_yr").ToString
            rrdt_min = dr.Item("rdt_min_yr").ToString
            rdt_min = dr.Item("rdt_min_yr").ToString 'tot min

            rrdt_min_before = dr.Item("rdt_min_yr_before").ToString
            rdt_min_before = dr.Item("rdt_min_yr_before").ToString 'tot min

            ordt_hrs = dr.Item("origrdt_hrs_yr").ToString
            rrdt_hrs = dr.Item("ttime_hrs_yr").ToString
            rdt_sav = dr.Item("rdt_sav").ToString 'hours saved

            rrdt_hrs_before = dr.Item("ttime_hrs_yr_before").ToString
            rdt_sav_before = dr.Item("rdt_sav_before").ToString 'hours saved

            omat = dr.Item("origmat_yr").ToString
            rmat = dr.Item("mat_yr").ToString
            mat_sav = dr.Item("mat_sav").ToString

            rmat_before = dr.Item("mat_yr_before").ToString
            mat_sav_before = dr.Item("mat_sav_before").ToString

            'lab_per = dr.Item("lab_per").ToString
        End While

        dr.Close()
        'Dim sb As New StringBuilder

        If ocraft_min = "" Or ocraft_min = "0" Then
            Dim strMessage As String =  tmod.getmsg("cdstr1616" , "OVRollup_OP.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Response.Write("Not Enough Data To Create Overview")
            Exit Sub
        End If

        Select Case typ
            Case "eq"
                sql = "select eqnum, eqdesc from equipment where eqid = '" & eqid & "'"
                dr = ovr.GetRdrData(sql)
                While dr.Read
                    sb.Append("<tr><td class=""bigbold"" width=""60px"">" & tmod.getlbl("cdlbl967" , "OVRollup_OP.aspx.vb") & "</td>")
                    sb.Append("<td class=""bigbold"" width=""120px"">" & dr.Item("eqnum").ToString & ":</td>")
                    sb.Append("<td class=""bigbold"" width=""440px"">" & dr.Item("eqdesc").ToString & "</td></tr>")
                    sb.Append("<tr><td colspan=""3""><hr size=""2""></td></tr>")
                    sb.Append("</table>")
                End While
                dr.Close()
            Case "site"
                sql = "select sitename, sitedesc from sites where siteid = '" & sid & "'"
                dr = ovr.GetRdrData(sql) 'width=""80px""
                While dr.Read
                    sb.Append("<tr><td class=""bigbold"" colspan=""3"" align=""center"">" & tmod.getxlbl("xlb363" , "OVRollup_OP.aspx.vb") & "&nbsp;&nbsp;" & dr.Item("sitename").ToString & "</td></tr>")
                    'sb.Append("<td class=""bigbold"" width=""140px"">" & dr.Item("sitename").ToString & ":</td>")
                    'sb.Append("<td class=""bigbold"" width=""400px"">" & dr.Item("sitedesc").ToString & "</td></tr>")
                    sb.Append("<tr><td colspan=""3""><hr size=""2""></td></tr>")
                    sb.Append("</table>")
                End While
                dr.Close()
            Case "select"
                sql = "select sitename, sitedesc from sites where siteid = '" & sid & "'"
                dr = ovr.GetRdrData(sql) 'width=""80px""
                While dr.Read
                    sb.Append("<tr><td class=""bigbold"" colspan=""3"" align=""center"">" & tmod.getxlbl("xlb364" , "OVRollup_OP.aspx.vb") & "&nbsp;&nbsp;" & dr.Item("sitename").ToString & "</td></tr>")
                End While
                dr.Close()
                sql = "select eqnum, eqdesc from equipment where eqid in (" & ee & ")"
                dr = ovr.GetRdrData(sql)
                While dr.Read
                    sb.Append("<tr><td class=""label"" width=""80px"">" & tmod.getlbl("cdlbl968" , "OVRollup_OP.aspx.vb") & "</td>")
                    sb.Append("<td class=""label"">" & dr.Item("eqnum").ToString & ":</td>")
                    sb.Append("<td class=""label"">" & dr.Item("eqdesc").ToString & "</td></tr>")
                    sb.Append("<tr><td colspan=""3""><hr size=""2""></td></tr>")
                    sb.Append("</table>")
                End While
                dr.Close()
        End Select


        'Separate Columns Here ******************

        sb.Append("<Table cellSpacing=""0"" cellPadding=""0"" border=""0"" width=""700"">")

        sb.Append("<tr><td width=""420""></td><td width=""280""></td></tr>")

        sb.Append("<tr><td valign=""top"">") 'First Column ****************
        'sb.Append("1")
        sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""420"" border=""0"">")
        sb.Append("<tr><td colspan=""3""><table   cellSpacing=""0"" cellPadding=""3"" width=""420px"">")
        sb.Append("<tr><td width=""140px""></td>")
        sb.Append("<td width=""50px""></td>")
        sb.Append("<td width=""50px""></td>")
        sb.Append("<td width=""80px""></td>")
        sb.Append("<td width=""100px""></td></tr>")
        sb.Append("<tr bgcolor=""#eeeded"" class=""label"">")
        sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"">PM</td>")
        sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl970" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl971" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blackcntr blacktop"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl972" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blackright blacktop"" bgcolor=""#eeeded"">Req'd Mins</td></tr>")

        sb.Append("<tr><td class=""label blackleft"">" & tmod.getlbl("cdlbl974" , "OVRollup_OP.aspx.vb") & "</td>")

        Select Case typ
            Case "eq"
                sql = "usp_optValAnlEq_op '" & eqid & "'"
            Case "site"
                sql = "usp_optValAnlEqPS '" & sid & "'"
            Case "select"
                sql = "usp_optValAnlEqPSS '" & sid & "', '" & eqstr & "'"
        End Select

        Try
            dr = ovr.GetRdrData(sql)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1617" , "OVRollup_OP.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Response.Write("Not Enough Data To Create Overview")
        End Try

        Dim o, t, n, ro, rta, rn, edope, edope2, edo As Integer
        While dr.Read
            o = dr.Item("otasks").ToString
            t = dr.Item("rtasks").ToString
            n = t - o

            ro = dr.Item("otasks").ToString
            rta = dr.Item("rtasks_before").ToString
            'rn = ro - rta
            rn = rta - ro

            edope = dr.Item("rtasks_before").ToString
            edope2 = dr.Item("rtasks").ToString
            'edo = edope - edope2
            edo = edope2 - edope

            sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("otasks").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackleft"">&nbsp;</td>")
            sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & dr.Item("odown time").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackright"" align=""center"">" & dr.Item("ocraft time").ToString & "</td></tr>")
            sb.Append("<tr><td class=""label blackleft"">" & tmod.getlbl("cdlbl975" , "OVRollup_OP.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("rtasks_before").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & rn & "</td>")
            sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & dr.Item("down time_before").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackright"" align=""center"">" & dr.Item("rcraft time_before").ToString & "</td></tr>")
            sb.Append("<tr><td class=""label blackleft"">" & tmod.getlbl("cdlbl976" , "OVRollup_OP.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("rtasks").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & edo & "</td>") 'was n
            sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & dr.Item("down time").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackright"" align=""center"">" & dr.Item("rcraft time").ToString & "</td></tr>")
        End While
        dr.Close()

        sb.Append("</table></td></tr>")

        'sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>")
        sb.Append("<tr><td colspan=""3""><table   cellSpacing=""0"" cellPadding=""3"" width=""420px"">")
        sb.Append("<tr><td width=""110px""></td>")
        sb.Append("<td width=""100px""></td>")
        sb.Append("<td width=""110px""></td>")
        sb.Append("<td width=""100px""></td>")
        sb.Append("<td width=""110px""></td>")
        sb.Append("<td width=""100px""></td></tr>")

        sb.Append("<tr><td class=""label""><tr class=""label"" bgcolor=""#eeeded"">")
        sb.Append("<td class=""blacktop blackleft"">" & tmod.getlbl("cdlbl977" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blacktop blackleft"">" & tmod.getlbl("cdlbl978" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blacktop blackleft"">" & tmod.getlbl("cdlbl979" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blacktop blackleft"">" & tmod.getlbl("cdlbl980" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blacktop blackcntr"">" & tmod.getlbl("cdlbl981" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blacktop blackright"">" & tmod.getlbl("cdlbl982" , "OVRollup_OP.aspx.vb") & "</td></tr>")
        Dim ot, rt, dt, rtb, dtb As Integer
        ot = 0
        rt = 0
        dt = 0
        rtb = 0
        dtb = 0
        Select Case typ
            Case "eq"
                sql = "usp_optValAnlEq2_op '" & eqid & "'"
            Case "site"
                sql = "usp_optValAnlEq2PS '" & sid & "'"
            Case "select"
                sql = "usp_optValAnlEq2PSS '" & sid & "', '" & eqstr & "'"
        End Select

        Try
            dr = ovr.GetRdrData(sql)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1618" , "OVRollup_OP.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Response.Write("Not Enough Data To Create Overview")
        End Try

        If dr.HasRows Then
            While dr.Read
                ot = ot + dr.Item("OT").ToString
                rt = rt + dr.Item("RT").ToString
                dt = dt + dr.Item("diff").ToString
                rtb = rtb + dr.Item("RTB").ToString
                dtb = dtb + dr.Item("diffB").ToString

                sb.Append("<td class=""plainlabel blackleft"" >" & dr.Item("OS").ToString & "</td>")
                sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("OT").ToString & "</td>")
                sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("RTB").ToString & "</td>")
                sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("diffB").ToString & "</td>")
                sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & dr.Item("RT").ToString & "</td>")
                sb.Append("<td class=""plainlabel blackright"" align=""center"">" & dr.Item("diff").ToString & "</td></tr>")


            End While
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr1619" , "OVRollup_OP.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Response.Write("Not Enough Data To Create Overview")
            Exit Sub
        End If


        dr.Close()

        sb.Append("<tr class=""label"">")
        sb.Append("<td class=""labelibs blackleft"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl983" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & ot & "</td>")
        sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & rtb & "</td>")
        sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & dtb & "</td>")
        sb.Append("<td class=""plainlabel blackcntr"" bgcolor=""#eeeded"" align=""center"">" & rt & "</td>")
        sb.Append("<td class=""plainlabel blackright"" bgcolor=""#eeeded"" align=""center"">" & dt & "</td></tr>")

        sb.Append("<tr bgcolor=""#eeeded""><td class=""label""><tr class=""label"">")
        sb.Append("<td class=""labelibs blackleft"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl984" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & ocraft_min & "</td>")
        sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & rcraft_min_before & "</td>")
        sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & ocraft_min - rcraft_min_before & "</td>")
        sb.Append("<td class=""plainlabel blackcntr"" bgcolor=""#eeeded"" align=""center"">" & rcraft_min & "</td>")
        sb.Append("<td class=""plainlabel blackright"" bgcolor=""#eeeded"" align=""center"">" & ocraft_min - rcraft_min & "</td></tr>")

        sb.Append("<tr bgcolor=""#eeeded""><td class=""label""><tr class=""label"">")
        sb.Append("<td class=""labelibs blackleft"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl985" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & ocraft_hrs & "</td>")
        sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & rcraft_hrs_before & "</td>")
        sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & lab_sav_before & "</td>")
        sb.Append("<td class=""plainlabel blackcntr"" bgcolor=""#eeeded"" align=""center"">" & rcraft_hrs & "</td>")
        sb.Append("<td class=""plainlabel blackright"" bgcolor=""#eeeded"" align=""center"">" & lab_sav & "</td></tr>")

        'Resource Rollup
        sb.Append("</table></td></tr>")
        'sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>")
        sb.Append("<tr><td colspan=""3""><table cellSpacing=""0"" cellPadding=""3"" width=""420px"">")
        sb.Append("<tr><td width=""110px""></td>")
        sb.Append("<td width=""100px""></td>")
        sb.Append("<td width=""110px""></td>")
        sb.Append("<td width=""100px""></td>")
        sb.Append("<td width=""110px""></td>")
        sb.Append("<td width=""100px""></td></tr>")

        sb.Append("<tr><td class=""label""><tr class=""label"" bgcolor=""#eeeded"">")
        sb.Append("<td class=""blackleft blacktop"">" & tmod.getlbl("cdlbl986" , "OVRollup_OP.aspx.vb") & "<font class=""subscript8"">(" & tmod.getxlbl("xlb365" , "OVRollup_OP.aspx.vb") & ")</font></td>")
        sb.Append("<td class=""blackleft blacktop"">" & tmod.getlbl("cdlbl987" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blackleft blacktop"">" & tmod.getlbl("cdlbl988" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blackleft blacktop"">" & tmod.getlbl("cdlbl989" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blackcntr blacktop"">" & tmod.getlbl("cdlbl990" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blackright blacktop"">" & tmod.getlbl("cdlbl991" , "OVRollup_OP.aspx.vb") & "</td></tr>")
        'Dim ot1, rt1, dt1 As Integer
        'ot = 0
        'rt = 0
        'dt = 0
        Dim rcnt As Integer = 0
        Dim rcntb As Integer = 0
        Dim ocnt As Integer = 0

        Select Case typ
            Case "eq"
                sql = "usp_optValAnlEq5_op '" & eqid & "'"
            Case "site"
                sql = "usp_optValAnlEq5PS '" & sid & "'"
            Case "select"
                sql = "usp_optValAnlEq5PSS '" & sid & "', '" & eqstr & "'"
        End Select


        Try
            dr = ovr.GetRdrData(sql)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1620" , "OVRollup_OP.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Response.Write("Not Enough Data To Create Overview")
        End Try
        Dim skillid As String
        Dim skillchk As Integer = 0
        If dr.HasRows Then
            While dr.Read
                rcnt += 1
                'ot = ot + dr.Item("OT").ToString
                'rt = rt + dr.Item("RT").ToString
                'dt = dt + dr.Item("diff").ToString
                skillstr += dr.Item("skill").ToString & ","
                ohour += dr.Item("ohr").ToString & ","
                rhour += dr.Item("rhr").ToString & ","
                rhourb += dr.Item("rhrb").ToString & ","
                If dr.Item("ohr").ToString <> "0" Then
                    ocnt += 1
                End If
                If dr.Item("rhr").ToString <> "0" Then
                    rcnt += 1
                End If
                If dr.Item("rhrb").ToString <> "0" Then
                    rcntb += 1
                End If
                If dr.Item("skill").ToString = "Operator" Then
                    skillchk = 1
                End If
                sb.Append("<tr><td class=""plainlabel blackleft"" >" & dr.Item("skill").ToString & "</td>")
                sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("ohr").ToString & "</td>")
                sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("rhrb").ToString & "</td>")
                sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("diffhb").ToString & "</td>")
                sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & dr.Item("rhr").ToString & "</td>")
                sb.Append("<td class=""plainlabel blackright"" align=""center"">" & dr.Item("diffh").ToString & "</td></tr>")


            End While
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr1621" , "OVRollup_OP.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If


        dr.Close()

        sb.Append("</table></td></tr>")
        'sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>")
        sb.Append("<tr><td colspan=""3""><table cellSpacing=""0"" cellPadding=""3"" width=""420px"">")
        sb.Append("<tr><td width=""110px""></td>")
        sb.Append("<td width=""90px""></td>")
        sb.Append("<td width=""110px""></td>")
        sb.Append("<td width=""110px""></td></tr>")

        sb.Append("<tr><td class=""label""><tr class=""label"">")
        sb.Append("<td class=""blacktop blackleft"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl992" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blacktop blackleft"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl993" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blacktop blackleft"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl994" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blacktop blackleft"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl995" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blacktop blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl996" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""blacktop blackright"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl997" , "OVRollup_OP.aspx.vb") & "</td></tr>")

        Select Case typ
            Case "eq"
                sql = "usp_optValAnlEq3_op '" & eqid & "'"
            Case "site"
                sql = "usp_optValAnlEq3PS '" & sid & "'"
            Case "select"
                sql = "usp_optValAnlEq3PSS '" & sid & "', '" & eqstr & "'"
        End Select


        Try
            dr = ovr.GetRdrData(sql)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1622" , "OVRollup_OP.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Response.Write("Not Enough Data To Create Overview")
        End Try

        While dr.Read

            sb.Append("<td class=""plainlabel blackleft"" >" & tmod.getlbl("cdlbl998" , "OVRollup_OP.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("oparts").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("partsb").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("partsdiffb").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & dr.Item("parts").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackright"" align=""center"">" & dr.Item("partsdiff").ToString & "</td></tr>")

            sb.Append("<td class=""plainlabel blackleft"">" & tmod.getlbl("cdlbl999" , "OVRollup_OP.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("otools").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("toolsb").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("toolsdiffb").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & dr.Item("tools").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackright"" align=""center"">" & dr.Item("toolsdiff").ToString & "</td></tr>")

            sb.Append("<td class=""plainlabel blackleft"">" & tmod.getlbl("cdlbl1000" , "OVRollup_OP.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("olubes").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("lubesb").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("lubesdiffb").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & dr.Item("lubes").ToString & "</td>")
            sb.Append("<td class=""plainlabel blackright"" align=""center"">" & dr.Item("lubesdiff").ToString & "</td></tr>")


        End While
        dr.Close()

        sb.Append("<tr bgcolor=""#eeeded""><td class=""label""><tr class=""label"">")
        sb.Append("<td class=""labelibs blackleft"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1001" , "OVRollup_OP.aspx.vb") & "</td>")
        sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">$" & omat & "</td>")
        sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">$" & rmat_before & "</td>")
        sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">$" & mat_sav_before & "</td>")
        sb.Append("<td class=""plainlabel blackcntr"" bgcolor=""#eeeded"" align=""center"">$" & rmat & "</td>")
        sb.Append("<td class=""plainlabel blackright"" bgcolor=""#eeeded"" align=""center"">$" & mat_sav & "</td></tr>")

        sb.Append("</table></td></tr>")
        ovr.Dispose()
        sb.Append("</Table>")



        'Second Column Here ************************************

        sb.Append("</td>") 'End First Column **************************
        sb.Append("<td valign=""top"" align=""center"">") 'Start Second Column **************************
        'sb.Append("1")
        sb.Append("<table width=""250"" cellSpacing=""0"" cellPadding=""3"">")
        sb.Append("<tr><td><table width=""250"" cellSpacing=""0"" cellPadding=""3"">")
        sb.Append("<tr><td colspan=""2"" class=""bluelabel blacktop blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1002" , "OVRollup_OP.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"" width=""90"">" & lab_sav_before & "</td>")
        sb.Append("<td class=""label blackright"" align=""center"" >" & tmod.getlbl("cdlbl1003" , "OVRollup_OP.aspx.vb") & "</td></tr>")

        Try
            lab_per_before = System.Math.Round(((ocraft_hrs - rcraft_hrs_before) / ocraft_hrs) * 100)
        Catch ex As Exception
            lab_per_before = "0%"
        End Try
        sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & lab_per_before & "%</td>")
        sb.Append("<td class=""label blackright""  align=""center"" >" & tmod.getlbl("cdlbl1004" , "OVRollup_OP.aspx.vb") & "</td></tr>")

        sb.Append("<tr><td colspan=""2"" class=""bluelabel blacktop blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1005" , "OVRollup_OP.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"" width=""90"">" & lab_sav & "</td>")
        sb.Append("<td class=""label blackright"" align=""center"" >" & tmod.getlbl("cdlbl1006" , "OVRollup_OP.aspx.vb") & "</td></tr>")

        Try
            lab_per = System.Math.Round(((ocraft_hrs - rcraft_hrs) / ocraft_hrs) * 100)
        Catch ex As Exception
            lab_per = "0%"
        End Try
        sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & lab_per & "%</td>")
        sb.Append("<td class=""label blackright""  align=""center"" >" & tmod.getlbl("cdlbl1007" , "OVRollup_OP.aspx.vb") & "</td></tr>")


        sb.Append("<tr><td colspan=""2"" class=""bluelabel blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1008" , "OVRollup_OP.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">$" & mat_sav_before & "</td>")
        sb.Append("<td class=""label blackright"" align=""center"">" & tmod.getlbl("cdlbl1009" , "OVRollup_OP.aspx.vb") & "</td></tr>")

        sb.Append("<tr><td colspan=""2"" class=""bluelabel blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1010" , "OVRollup_OP.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">$" & mat_sav & "</td>")
        sb.Append("<td class=""label blackright"" align=""center"">" & tmod.getlbl("cdlbl1011" , "OVRollup_OP.aspx.vb") & "</td></tr>")

        sb.Append("<tr><td colspan=""2"" class=""bluelabel blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1012" , "OVRollup_OP.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & ordt_hrs & "</td>")
        sb.Append("<td class=""label blackright"" align=""center"" >" & tmod.getlbl("cdlbl1013" , "OVRollup_OP.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & rdt_sav_before & "</td>")
        sb.Append("<td class=""label blackright"" align=""center"" >" & tmod.getlbl("cdlbl1014" , "OVRollup_OP.aspx.vb") & "</td></tr>")

        sb.Append("<tr><td colspan=""2"" class=""bluelabel blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1015" , "OVRollup_OP.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & ordt_hrs & "</td>")
        sb.Append("<td class=""label blackright"" align=""center"" >" & tmod.getlbl("cdlbl1016" , "OVRollup_OP.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & rdt_sav & "</td>")
        sb.Append("<td class=""label blackright"" align=""center"" >" & tmod.getlbl("cdlbl1017" , "OVRollup_OP.aspx.vb") & "</td></tr>")

        'sb.Append("<tr><td colspan=""2"" class=""bluelabel blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1018" , "OVRollup_OP.aspx.vb") & "</td></tr>")
        Dim iht As Integer
        Dim rht As Integer
        If ocnt > 4 Then
            iht = ((ocnt - 4) * 18) + 122
        Else
            iht = 122
        End If
        If rcnt > 4 Then
            rht = ((rcnt - 4) * 18) + 122
        Else
            rht = 122
        End If
        If ocnt > 0 And rcnt > 0 Then
            sb.Append("<tr><td colspan=""2""  class="""">") 'blackcntr
            sb.Append("<iframe runat=""server"" width=""280"" height=" & iht & " frameBorder=""no"" scrolling=""no"" src=""../appsman/PMPie3D.aspx?title=Original&skills=" + skillstr + "&hrs=" & ohour & """>")
            sb.Append("</td></tr>")

            'sb.Append("<tr><td colspan=""2"" class=""bluelabel blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1019" , "OVRollup_OP.aspx.vb") & "</td></tr>")

            sb.Append("<tr><td colspan=""2"" class="""">") 'blackcntr
            sb.Append("111111111<iframe runat=""server"" width=""280"" height=" & rht & " frameBorder=""no"" scrolling=""no"" src=""../appsman/PMPie3D.aspx?title=Optimized&skills=" + skillstr + "&hrs=" & rhour & """>")
            sb.Append("</td></tr>")
        End If



        sb.Append("</table></td></tr>")
        'If skillchk <> 0 Then



        'Else
        sb.Append("</table>")
        'End If

        sb.Append("</td>") 'End Second Column **************************
        sb.Append("</tr></table>") 'End Multi-Column Table **************************
        sb.Append("</body></html>")

        Response.Write(sb)

    End Sub
End Class
