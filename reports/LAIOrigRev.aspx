<%@ Page Language="vb" AutoEventWireup="false" Codebehind="LAIOrigRev.aspx.vb" Inherits="lucy_r12.LAIOrigRev" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>LAIOrigRev</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="label"><asp:Label id="lang3339" runat="server">Select Output</asp:Label></td>
					<td><asp:dropdownlist id="ddout" runat="server" AutoPostBack="True">
							<asp:ListItem Value="Select">Select</asp:ListItem>
							<asp:ListItem Value="Original" Selected="True">Original</asp:ListItem>
							<asp:ListItem Value="Revised">Revised</asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3340" runat="server">Select Equipment#</asp:Label></td>
					<td><asp:dropdownlist id="ddeq" runat="server" AutoPostBack="True"></asp:dropdownlist></td>
					<td><asp:button id="btnexp" runat="server" Text="Export"></asp:button></td>
				</tr>
				<tr>
					<td colSpan="3">
						<asp:DataGrid id="dgexport" runat="server" AutoGenerateColumns="False">
							<Columns>
								<asp:BoundColumn DataField="func" HeaderText="Function"></asp:BoundColumn>
								<asp:BoundColumn DataField="task" HeaderText="Task#"></asp:BoundColumn>
								<asp:BoundColumn DataField="taskstatus" HeaderText="Task Status"></asp:BoundColumn>
								<asp:BoundColumn DataField="freq" HeaderText="Frequency"></asp:BoundColumn>
								<asp:BoundColumn DataField="freqyr" HeaderText="Freq/Yr"></asp:BoundColumn>
								<asp:BoundColumn DataField="origttime" HeaderText="Orig Time"></asp:BoundColumn>
								<asp:BoundColumn DataField="origqty" HeaderText="Orig Qty"></asp:BoundColumn>
								<asp:BoundColumn DataField="yr_origttime" HeaderText="Orig Time Yr"></asp:BoundColumn>
								<asp:BoundColumn DataField="origrdt" HeaderText="Orig DT"></asp:BoundColumn>
								<asp:BoundColumn DataField="yr_origrdt" HeaderText="Orig DT Yr"></asp:BoundColumn>
								<asp:BoundColumn DataField="opcost" HeaderText="Orig Part Cost"></asp:BoundColumn>
								<asp:BoundColumn DataField="yr_opcost" HeaderText="Orig Part Cost Yr"></asp:BoundColumn>
								<asp:BoundColumn DataField="otcost" HeaderText="Orig Tool Cost"></asp:BoundColumn>
								<asp:BoundColumn DataField="yr_otcost" HeaderText="Orig Tool Cost Yr"></asp:BoundColumn>
								<asp:BoundColumn DataField="olcost" HeaderText="Orig Lube Cost"></asp:BoundColumn>
								<asp:BoundColumn DataField="yr_olcost" HeaderText="Orig Lube Cost Yr"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid></td>
				</tr>
				<tr>
					<td colSpan="3">
						<asp:DataGrid id="dgexportrev" runat="server" AutoGenerateColumns="False">
							<Columns>
								<asp:BoundColumn DataField="func" HeaderText="Function"></asp:BoundColumn>
								<asp:BoundColumn DataField="task" HeaderText="Task#"></asp:BoundColumn>
								<asp:BoundColumn DataField="taskstatus" HeaderText="Task Status"></asp:BoundColumn>
								<asp:BoundColumn DataField="freq" HeaderText="Frequency"></asp:BoundColumn>
								<asp:BoundColumn DataField="freqyr" HeaderText="Freq/Yr"></asp:BoundColumn>
								<asp:BoundColumn DataField="origttime" HeaderText="Rev Time"></asp:BoundColumn>
								<asp:BoundColumn DataField="origqty" HeaderText="Rev Qty"></asp:BoundColumn>
								<asp:BoundColumn DataField="yr_origttime" HeaderText="Rev Time Yr"></asp:BoundColumn>
								<asp:BoundColumn DataField="origrdt" HeaderText="Rev DT"></asp:BoundColumn>
								<asp:BoundColumn DataField="yr_origrdt" HeaderText="Rev DT Yr"></asp:BoundColumn>
								<asp:BoundColumn DataField="opcost" HeaderText="Rev Part Cost"></asp:BoundColumn>
								<asp:BoundColumn DataField="yr_opcost" HeaderText="Rev Part Cost Yr"></asp:BoundColumn>
								<asp:BoundColumn DataField="otcost" HeaderText="Rev Tool Cost"></asp:BoundColumn>
								<asp:BoundColumn DataField="yr_otcost" HeaderText="Rev Tool Cost Yr"></asp:BoundColumn>
								<asp:BoundColumn DataField="olcost" HeaderText="Rev Lube Cost"></asp:BoundColumn>
								<asp:BoundColumn DataField="yr_olcost" HeaderText="Rev Lube Cost Yr"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid></td>
				</tr>
			</table>
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
