﻿Public Class RBASloclookdialog
    Inherits System.Web.UI.Page
    Dim skillid, qty, freq, rdid, siteid, who, rteid, ptid, days, shift As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            siteid = Request.QueryString("sid").ToString
            skillid = Request.QueryString("skillid").ToString
            qty = Request.QueryString("qty").ToString
            freq = Request.QueryString("freq").ToString
            rdid = Request.QueryString("rdid").ToString
            rteid = Request.QueryString("rteid").ToString
            ptid = Request.QueryString("ptid").ToString
            days = Request.QueryString("days").ToString
            shift = Request.QueryString("shift").ToString
            iff.Attributes.Add("src", "RBASloclook.aspx?sid=" & siteid & "&ptid=" & ptid & "&skillid=" & skillid & "&qty=" & qty & "&freq=" & freq & "&rdid=" & rdid & "&rteid=" & rteid & "&days=" & days & "&shift=" & shift & "&date=" & Now)

        End If
    End Sub

End Class