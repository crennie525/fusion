﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class repmeq

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''tddepts control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tddepts As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdlocs1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdlocs1 As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''imgsw control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgsw As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''trdepts control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trdepts As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''tddept control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tddept As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdcell control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdcell As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''trlocs control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trlocs As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''tdloc3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdloc3 As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdmsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdmsg As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''lang3468 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang3468 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lang3469 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang3469 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lbps control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbps As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''btntouser control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btntouser As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''btnfromuser control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnfromuser As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''lbuserps control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbuserps As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''Img5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Img5 As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''Img6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Img6 As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''lblipg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblipg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Img7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Img7 As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''Img8 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Img8 As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''usa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents usa As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''lbleq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbleq As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbleqid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbleqid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbleqlist control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbleqlist As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbldid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbldid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbldept control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbldept As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblclid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblclid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblcell control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblcell As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbllid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbllid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblloc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblloc As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblrettyp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblrettyp As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsubmit As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''txtipg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtipg As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''txtipgcnt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtipgcnt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblret control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblret As Global.System.Web.UI.HtmlControls.HtmlInputHidden
End Class
