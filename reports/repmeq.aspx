﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="repmeq.aspx.vb" Inherits="lucy_r12.repmeq" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    <script type="text/javascript">
        function getminsrch() {
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "lup";

            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("lbldept").value = ret[1];
                document.getElementById("tddept").innerHTML = ret[1];
                document.getElementById("lblclid").value = ret[2];
                document.getElementById("lblcell").value = ret[3];
                document.getElementById("tdcell").innerHTML = ret[3];

                document.getElementById("trdepts").className = "view";
                document.getElementById("lblrettyp").value = "depts";

                if (ret[0] != "") {
                    document.getElementById("lblret").value = "ifirst";
                    document.getElementById("form1").submit();
                }

            }
        }

        function getlocs1() {
            var sid = document.getElementById("lblsid").value;
            var lid = document.getElementById("lbllid").value;
            var typ = "lu"
            //alert(lid)
            if (lid != "") {
                typ = "retloc"
            }
            var eqid = ""; // document.getElementById("lbleqid").value;
            var fuid = ""; // document.getElementById("lblfuid").value;
            var coid = ""; // document.getElementById("lblcoid").value;
            //alert(fuid)
            var wo = "";
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                //ret = lidi + "~" + lid + "~" + loc + "~" + eq + "~" + eqnum + "~" + fu + "~" + func + "~" + co + "~" + comp + "~" + lev;
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("lblloc").value = ret[1];
                document.getElementById("tdloc3").innerHTML = ret[2];

                document.getElementById("trlocs").className = "view";
                document.getElementById("lblrettyp").value = "locs";

                if (ret[0] != "") {
                    document.getElementById("lblret").value = "ifirst";
                    document.getElementById("form1").submit();
                }

            }
        }
        function clearall() {
            document.getElementById("lbldid").value = "";
            document.getElementById("lbldept").value = "";
            document.getElementById("tddept").innerHTML = "";
            document.getElementById("lblclid").value = "";
            document.getElementById("lblcell").value = "";
            document.getElementById("tdcell").innerHTML = "";

            document.getElementById("lbllid").value = "";
            document.getElementById("lblloc").value = "";
            document.getElementById("tdloc3").innerHTML = "";

            document.getElementById("trdepts").className = "details";
            document.getElementById("trlocs").className = "details";
            document.getElementById("lblrettyp").value = "";
        }
        function undolocs() {
            document.getElementById("lblret").value = "undoloc";
            document.getElementById("form1").submit();
        }
        function saveapps() {
            var ret = document.getElementById("lbleqlist").value;
            window.parent.handlereturn(ret);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="500">
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td id="tddepts" class="bluelabel" runat="server">
                                Use Departments
                            </td>
                            <td>
                                <img onclick="getminsrch();" alt="" src="../images/appbuttons/minibuttons/magnifier.gif" />
                            </td>
                            <td id="tdlocs1" class="bluelabel" runat="server">
                                Use Locations
                            </td>
                            <td>
                                <img onclick="getlocs1();" alt="" src="../images/appbuttons/minibuttons/magnifier.gif" />
                            </td>
                            <td>
                                <img alt="" id="imgsw" onmouseover="return overlib('Clear Filter and View All Records', LEFT)"
                                    onclick="undolocs();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/switch.gif"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr id="trdepts" class="details" runat="server">
                            <td colspan="5">
                                <table>
                                    <tr>
                                        <td class="label" width="110">
                                            Department
                                        </td>
                                        <td id="tddept" class="plainlabel" width="170" runat="server">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">
                                            Station\Cell
                                        </td>
                                        <td id="tdcell" class="plainlabel" runat="server">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trlocs" class="details" runat="server">
                            <td colspan="5">
                                <table>
                                    <tr>
                                        <td class="label" width="110">
                                            Location
                                        </td>
                                        <td id="tdloc3" class="plainlabel" width="170" runat="server">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="plainlabelblue" id="tdmsg" runat="server">
                </td>
            </tr>
            <tr>
                <td>
                    <table width="500">
                        <tr>
                            <td class="label" align="center">
                                <asp:Label ID="lang3468" runat="server">Available Equipment</asp:Label>
                            </td>
                            <td>
                            </td>
                            <td class="label" align="center">
                                <asp:Label ID="lang3469" runat="server">Selected Equipment</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" width="235">
                                <asp:ListBox ID="lbps" runat="server" Height="150px" SelectionMode="Multiple" Width="235px">
                                </asp:ListBox>
                            </td>
                            <td valign="middle" align="center" width="30">
                                <br />
                                <asp:ImageButton ID="btntouser" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                                </asp:ImageButton><asp:ImageButton ID="btnfromuser" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                                </asp:ImageButton>
                            </td>
                            <td align="center" width="235">
                                <asp:ListBox ID="lbuserps" runat="server" Height="150px" SelectionMode="Multiple"
                                    Width="235px"></asp:ListBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                                    border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="border-right: blue 1px solid" width="20">
                                            <img id="Img5" onclick="getfirst('i');" src="../images/appbuttons/minibuttons/lfirst.gif"
                                                runat="server">
                                        </td>
                                        <td style="border-right: blue 1px solid" width="20">
                                            <img id="Img6" onclick="getprev('i');" src="../images/appbuttons/minibuttons/lprev.gif"
                                                runat="server">
                                        </td>
                                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="100">
                                            <asp:Label ID="lblipg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                                        </td>
                                        <td style="border-right: blue 1px solid" width="20">
                                            <img id="Img7" onclick="getnext('i');" src="../images/appbuttons/minibuttons/lnext.gif"
                                                runat="server">
                                        </td>
                                        <td width="20">
                                            <img id="Img8" onclick="getlast('i');" src="../images/appbuttons/minibuttons/llast.gif"
                                                runat="server">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="plainlabelblue" colspan="3" align="center">
                                Please choose Equipment Records in the order that you would like them to appear
                                in your report output.<br />
                                <br />
                                Note that when a Department or Location is selected that only Equipment Records
                                in that Department or Location will appear in the Selected Equipment List Box.
                                <br /><br />
                                To view All Selected Equipment records click the Refresh icon at the top of the screen.
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="3">
                                <img id="usa" onclick="saveapps();" height="19" alt="" src="../images/appbuttons/bgbuttons/save.gif"
                                    width="69" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lbleqlist" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lbldept" runat="server" />
    <input type="hidden" id="lblclid" runat="server" />
    <input type="hidden" id="lblcell" runat="server" />
    <input type="hidden" id="lbllid" runat="server" />
    <input type="hidden" id="lblloc" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblrettyp" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input id="txtipg" type="hidden" runat="server" />
    <input id="txtipgcnt" type="hidden" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    </form>
</body>
</html>
