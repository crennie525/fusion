﻿Public Class CMMS_Output_to_Word3
    Inherits System.Web.UI.Page
    Dim wrd As New CMMSWRD3
    Dim eqid, mode, inout As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            mode = Request.QueryString("mode").ToString
            inout = Request.QueryString("inout").ToString
            wrd.CMMSToWord(eqid, mode, inout, Response)
        End If
    End Sub

End Class