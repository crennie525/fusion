

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AssignmentReportPics
    Inherits System.Web.UI.Page
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomi As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sql As String
    Dim tasks As New Utilities
    Dim dr As SqlDataReader
    Dim eqid, fuid, comid, typ As String
    Dim tmod As New transmod
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblexport As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdwi As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'ADD FOR SUNCOR
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        lblcomi.Value = comi
        If comi = "SUN" Then
            lblfslang.Value = "fre"
        End If
        'END ADD FOR SUNCOR
        'Put user code to initialize the page here
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString '317 "305" '
            typ = Request.QueryString("typ").ToString '"eq" '
            tasks.Open()
            If typ = "fu" Then
                fuid = Request.QueryString("fuid").ToString
                GetReport(typ, eqid, fuid)
            ElseIf typ = "co" Then
                fuid = Request.QueryString("fuid").ToString
                comid = Request.QueryString("coid").ToString
                GetReport(typ, eqid, fuid, comid)
            Else
                GetReport(typ, eqid)
            End If
            tasks.Dispose()

        End If
    End Sub
    Private Sub GetReport(ByVal typ As String, ByVal eqid As String, Optional ByVal fuid As String = "", Optional ByVal comid As String = "")
        Dim lang As String = lblfslang.Value
        Dim coi As String = lblcomi.Value
        Dim sb As New System.Text.StringBuilder
        Dim sbs As New System.Text.StringBuilder
        Dim sbt As New System.Text.StringBuilder
        'sb.Append("<Table cellSpacing=""1"" cellPadding=""1"" width=""960"">")
        'sb.Append("<tr><td class=""replabel12"" colspan=""12"" align=""center"">" & tmod.getlbl("cdlbl682" , "AssignmentReportPics.aspx.vb") & "</td></tr>")
        'sb.Append("<tr><td colspan=""12"">&nbsp;</td></tr>")
        'sb.Append("<tr><td width=""50""></td><td width=""50""></td><td width=""120""></td>")
        'sb.Append("<td width=""50""></td><td width=""120""></td><td width=""120""></td>")
        'sb.Append("<td width=""97""></td><td width=""97""></td><td width=""97""></td>")
        'sb.Append("<td width=""97""></td><td width=""97""></td><td width=""97""></td></tr>")

        Select Case typ
            Case "eq"
                If coi <> "CAS" And coi <> "AGR" And coi <> "LAU" And coi <> "GLA" Then
                    sql = "usp_GetAssignmentReportPics '" & eqid & "'"
                Else
                    If lang = "fre" Then
                        sql = "usp_GetAssignmentReportPicsf '" & eqid & "'"
                    Else
                        sql = "usp_GetAssignmentReportPics '" & eqid & "'"
                    End If
                End If

            Case "fu"
                sql = "usp_GetAssignmentReportPics '" & eqid & "', '" & fuid & "'"
            Case "co"
                sql = "usp_GetAssignmentReportPics '" & eqid & "', '" & fuid & "', '" & comid & "'"
        End Select

        Dim start As Integer = 0
        Dim start1 As Integer = 0
        Dim scnt As Integer
        Dim schk As Integer = 0
        Dim fflag As Integer = 0
        Dim taskhold As Integer = 0
        Dim theight As Integer
        Dim fchk As String = ""
        Dim func As String
        Dim tchk As String = ""
        Dim task As String
        Dim subtask As String
        Dim subt As String
        Dim oti, ti, rat, ts, freq, loto, cs, rd, lube, parts, tools, loc, locd As String
        Dim sid, did, clid, eq, eqdesc, epic, fpic, tpic, fspl, cpic As String
        Dim compnum, cqty, desig, taskdesc, tasktype, skill, ttime As String
        Dim cnum, cdesc, cspl, oem, model, serial, assetclass, createdby, createdate As String
        Dim espl, elocation, estatus As String
        Dim ds As New DataSet
        ds = tasks.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)

        Dim row As DataRow

        Dim rowcnt = dt.Rows.Count

        If rowcnt = 0 Then
            sb.Append("<Table cellSpacing=""1"" cellPadding=""1"" width=""960"">")
            sb.Append("<tr><td class=""replabel12"" colspan=""12"" align=""center"">" & tmod.getlbl("cdlbl683", "AssignmentReportPics.aspx.vb") & "</td></tr>")
            sb.Append("</table>")
            tdwi.InnerHtml = sb.ToString
            Exit Sub

        End If

        'Added for nissan firewall
        Dim ThisPage1 As String = Request.ServerVariables("HTTP_HOST")
        Dim ns1i As Integer
        Dim nsstr As String
        Dim ns2i As Integer
        'End Added

        For Each row In dt.Rows
            loc = row("location").ToString
            locd = row("description").ToString
            sid = row("sitename").ToString
            did = row("dept_line").ToString
            clid = row("cell_name").ToString
            eq = row("eqnum").ToString
            eqdesc = row("eqdesc").ToString
            func = row("func").ToString

            oem = row("oem").ToString
            model = row("model").ToString
            serial = row("serial").ToString
            assetclass = row("assetclass").ToString

            espl = row("espl").ToString
            elocation = row("elocation").ToString
            estatus = row("estatus").ToString

            createdby = row("createdby").ToString
            createdate = row("createdate").ToString

            Try
                fspl = row("fspl").ToString
                cspl = row("cspl").ToString
                cdesc = row("cdesc").ToString
                cnum = row("cnum").ToString
            Catch ex As Exception

            End Try


            subtask = row("subtask").ToString

            freq = row("freq").ToString
            If freq = "0" Then
                freq = "None"
            ElseIf freq = "Select - Select" Then
                freq = "None"
            Else
                freq = row("freq").ToString
            End If
            subt = row("subt").ToString
            loto = row("lotoid").ToString
            If loto = "0" Then
                loto = "N"
            Else
                loto = "Y"
            End If
            cs = row("conid").ToString
            If cs = "0" Then
                cs = "N"
            Else
                cs = "Y"
            End If
            rd = row("rdid").ToString
            If rd = "1" Then
                rd = "R"
            ElseIf rd = "2" Then
                rd = "D"
            ElseIf rd = "3" Then
                rd = "A"
            End If
            parts = row("parts").ToString
            If Len(parts) = 0 OrElse parts = "None" Then
                parts = Nothing
            Else
                parts = tmod.getxlbl("xlb265", "AssignmentReportPics.aspx.vb") & row("parts").ToString
            End If
            tools = row("tools").ToString
            If Len(tools) = 0 OrElse tools = "None" Then
                tools = Nothing
            Else
                tools = tmod.getxlbl("xlb266", "AssignmentReportPics.aspx.vb") & row("tools").ToString
            End If
            lube = row("lubes").ToString
            If Len(lube) = 0 OrElse lube = "None" Then
                lube = Nothing
            Else
                lube = tmod.getxlbl("xlb267", "AssignmentReportPics.aspx.vb") & row("lubes").ToString
            End If

            compnum = row("compnum").ToString
            cqty = row("cqty").ToString
            desig = row("desig").ToString
            taskdesc = row("task").ToString
            tasktype = row("tasktype").ToString
            skill = row("skill").ToString
            ttime = row("ttime").ToString

            epic = row("epics").ToString
            fpic = row("fpics").ToString
            cpic = row("cpics").ToString
            tpic = row("tpics").ToString

            'Added for nissan firewall
            If tpic <> "" Then
                ns1i = tpic.IndexOf("//")
                nsstr = Mid(tpic, ns1i + 3)
                ns2i = nsstr.IndexOf("/")
                nsstr = Mid(nsstr, 1, ns2i)
                If nsstr <> ThisPage1 Then
                    tpic = tpic.Replace(nsstr, ThisPage1)
                End If
            End If
            'End Added

            subt = row("subt").ToString
            scnt = row("scnt").ToString

            fuid = row("funcid").ToString

            task = row("tasknum").ToString

            If fchk = "" Then
                fchk = func
            Else
                If fchk <> func Then
                    fchk = func
                    fflag = 1
                End If
            End If
            If start1 = 0 Or fflag = 1 Then
                start1 = 1

                If fflag = 1 And start1 <> 0 Then
                    sb.Append("</Table>")
                End If
                fflag = 0



                sb.Append("<Table cellSpacing=""1"" cellPadding=""1"" width=""960"" style=""page-break-after:always;"">")

                sb.Append("<tr><td colspan=""12""><table border=""0"" width=""960"" cellpadding=""0"" cellspacing=""2"">")

                sb.Append("<tr><td class=""replabel12"" width=""140""></td>")
                sb.Append("<td class=""repplain12"" width=""250""></td>")
                sb.Append("<td class=""repplain12"" width=""10""></td>")
                sb.Append("<td class=""replabel12"" width=""100""></td>")
                sb.Append("<td class=""repplain12"" width=""180""></td>")
                sb.Append("<td class=""replabel12"" width=""100""></td>")
                sb.Append("<td class=""repplain12"" width=""180""></td></tr>")

                sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl684", "AssignmentReportPics.aspx.vb") & "</td>")
                sb.Append("<td class=""repplain12"" colspan=""2"">" & Now & "</td>")
                sb.Append("<td class=""replabel12"">" & tmod.getlbl("cdlbl685", "AssignmentReportPics.aspx.vb") & "</td>")
                sb.Append("<td class=""repplain12"">" & eq & "</td>")
                sb.Append("<td class=""replabel12"">" & tmod.getlbl("cdlbl686", "AssignmentReportPics.aspx.vb") & "</td>")
                sb.Append("<td class=""repplain12"">" & func & "</td></tr>")

                sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl687", "AssignmentReportPics.aspx.vb") & "</td>")
                sb.Append("<td class=""repplain12"" colspan=""2"">" & sid & "</td>")
                sb.Append("<td class=""replabel12"">" & tmod.getlbl("cdlbl688", "AssignmentReportPics.aspx.vb") & "</td>")
                sb.Append("<td class=""repplain12"">" & eqdesc & "</td>")
                sb.Append("<td class=""replabel12"">SPL:</td>")
                sb.Append("<td class=""repplain12"">" & fspl & "</td></tr>")


                sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl690", "AssignmentReportPics.aspx.vb") & "</td>")
                sb.Append("<td class=""repplain12"" colspan=""2"">" & did & "</td>")
                If epic <> "" Then
                    sb.Append("<td colspan=""2"" rowspan=""3"" class=""box"" align=""center"" valign=""center""><table>")
                    sb.Append("<tr><td ><img src=""" & epic & """></td></tr>")
                    sb.Append("</table></td>")
                Else
                    sb.Append("<td colspan=""2"" rowspan=""3"" class=""box"" align=""center"" valign=""center""><table>")
                    sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl691", "AssignmentReportPics.aspx.vb") & "</td></tr>")
                    sb.Append("</table></td>")
                End If

                If fpic <> "" Then
                    sb.Append("<td colspan=""2"" rowspan=""3""  class=""box"" align=""center"" valign=""center""><table>")
                    sb.Append("<tr><td><img src=""" & fpic & """></td></tr>")
                    sb.Append("</table></td></tr>")
                Else
                    sb.Append("<td colspan=""2"" rowspan=""3"" class=""box"" align=""center"" valign=""center""><table>")
                    sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl692", "AssignmentReportPics.aspx.vb") & "</td></tr>")
                    sb.Append("</table></td></tr>")
                End If


                sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl693", "AssignmentReportPics.aspx.vb") & "</td>")
                sb.Append("<td class=""repplain12"" colspan=""2"">" & clid & "</td></tr>")



                sb.Append("<tr><td colspan=""2"" class=""box"" height=""200""><table cellspacing=""0"">")
                'sb.Append("<tr><td height=""10"" class=""replabel12"" colspan=""2"">&nbsp;</td>")

                sb.Append("<tr><td class=""replabel12"" colspan=""2""><u>" & tmod.getxlbl("xlb268", "AssignmentReportPics.aspx.vb") & "</u></td></tr>")

                sb.Append("<tr><td class=""replabel12"">SPL</td>")
                sb.Append("<td class=""repplain12"">" & espl & "</td></tr>")

                sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl695", "AssignmentReportPics.aspx.vb") & "</td>")
                sb.Append("<td class=""repplain12"">" & elocation & "</td></tr>")

                sb.Append("<tr><td class=""replabel12"" width=""120"">OEM</td>")
                sb.Append("<td class=""repplain12"" width=""160"">" & oem & "</td></tr>")

                sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl697", "AssignmentReportPics.aspx.vb") & "</td>")
                sb.Append("<td class=""repplain12"">" & model & "</td></tr>")

                sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl698", "AssignmentReportPics.aspx.vb") & "</td>")
                sb.Append("<td class=""repplain12"">" & serial & "</td></tr>")

                sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl699", "AssignmentReportPics.aspx.vb") & "</td>")
                sb.Append("<td class=""repplain12"">" & estatus & "</td></tr>")

                sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl700", "AssignmentReportPics.aspx.vb") & "</td>")
                sb.Append("<td class=""repplain12"">" & assetclass & "</td></tr>")

                sb.Append("<tr><td height=""5"" class=""replabel12"" colspan=""2"">&nbsp;</td></tr>")

                sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl701", "AssignmentReportPics.aspx.vb") & "</td>")
                sb.Append("<td class=""repplain12"">" & createdby & "</td></tr>")

                sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl702", "AssignmentReportPics.aspx.vb") & "</td>")
                sb.Append("<td class=""repplain12"">" & createdate & "</td></tr>")

                'sb.Append("<tr><td height=""20"" class=""replabel12"" colspan=""2"">&nbsp;</td></tr>")
                'sb.Append("<tr><td height=""10"" class=""replabel12"" colspan=""2"">&nbsp;</td>")

                sb.Append("</table></td><td>&nbsp;</td></tr>")

                sb.Append("</table></td></tr>")

            End If


            If scnt = 0 Then
                sb.Append("<tr><td colspan=""12"">&nbsp;</td></tr>")
                sb.Append("<tr><td width=""70"" class=""replabel12"" valign=""bottom""><u>" & tmod.getxlbl("xlb269", "AssignmentReportPics.aspx.vb") & "</u></td>")
                sb.Append("<td width=""40"" class=""replabel12"" valign=""bottom""><u>" & tmod.getxlbl("xlb270", "AssignmentReportPics.aspx.vb") & "</u></td>")
                sb.Append("<td width=""410"" class=""replabel12"" valign=""bottom""><u>" & tmod.getxlbl("xlb271", "AssignmentReportPics.aspx.vb") & "</u></td>")
                sb.Append("<td width=""30"" class=""replabel12"" valign=""bottom""><u>Qty</u></td>")

                sb.Append("<td width=""140"" class=""replabel12"" valign=""bottom""><u>" & tmod.getxlbl("xlb272", "AssignmentReportPics.aspx.vb") & "</u></td>")
                sb.Append("<td width=""30"" class=""replabel12"" valign=""bottom""><u>R/D</u></td>")
                sb.Append("<td width=""160"" class=""replabel12"" valign=""bottom""><u>" & tmod.getxlbl("xlb273", "AssignmentReportPics.aspx.vb") & "</u></td>")
                sb.Append("<td width=""30"" class=""replabel12"" valign=""bottom""><u>MINS</u></td>")
                sb.Append("<td width=""30"" class=""replabel12"" valign=""bottom""><u>LOTO</u></td>")
                sb.Append("<td width=""20"" class=""replabel12"" valign=""bottom""><u>CS</u></td></tr>")

                schk = 0
                sb.Append("<tr><td class=""repplain12"" valign=""top"">" & freq & "</td>")
                sb.Append("<td class=""repplain12"" valign=""top"">" & task & "</td>")

                sb.Append("<td class=""repplain12"" valign=""top""><table cellspacing=""0"" cellpadding=""0"">")
                sb.Append("<tr><td class=""replabel12"" width=""110"">" & tmod.getlbl("cdlbl703", "AssignmentReportPics.aspx.vb") & "</td><td class=""repplain12"" width=""300"">" & cnum & "</td></tr>")
                sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl704", "AssignmentReportPics.aspx.vb") & "</td><td class=""repplain12"">" & cdesc & "</td></tr>")
                sb.Append("<tr><td class=""replabel12"">SPL</td><td class=""repplain12"">" & cspl & "</td></tr>")
                sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl706", "AssignmentReportPics.aspx.vb") & "</td><td class=""repplain12"">" & desig & "</td></tr>")
                sb.Append("</table></td>")

                sb.Append("<td class=""repplain12"" valign=""top"">" & cqty & "</td>")
                'need update
                'sb.Append("<td class=""repplain12"">" & cdesc & "</td>")
                'sb.Append("<td class=""repplain12"">" & cspl & "</td>")

                'sb.Append("<td class=""repplain12"">" & desig & "</td>")
                sb.Append("<td class=""repplain12"" valign=""top"">" & tasktype & "</td>")
                sb.Append("<td class=""repplain12"" valign=""top"">" & rd & "</td>")
                sb.Append("<td class=""repplain12"" valign=""top"">" & skill & "</td>")
                sb.Append("<td class=""repplain12"" valign=""top"">" & ttime & "</td>")
                sb.Append("<td class=""repplain12"" valign=""top"">" & loto & "</td>")
                sb.Append("<td class=""repplain12"" valign=""top"">" & cs & "</td></tr>") 'tried here

                sb.Append("<tr><td colspan=""12""><table border=""0"">")

                If tpic = "" Then
                    sb.Append("<tr><td class=""repplain12 box"" width=""700"" valign=""top"">")
                Else
                    sb.Append("<tr><td class=""repplain12 box"" width=""700"" height=""240"" valign=""top"">")
                End If




                sb.Append("<table>")
                sb.Append("<tr><td class=""replabel12"" colspan=""1""><u>" & tmod.getxlbl("xlb274", "AssignmentReportPics.aspx.vb") & "</u></td><tr>")
                sb.Append("<tr><td class=""repplain12"">" & taskdesc & "</td></tr>")

                If parts <> Nothing Then
                    sb.Append("<tr>")
                    sb.Append("<td class=""repplain12"">&nbsp;&nbsp;&nbsp;&nbsp;" & parts & "</td></tr>")
                End If
                If tools <> Nothing Then
                    sb.Append("<tr>")
                    sb.Append("<td class=""repplain12"">&nbsp;&nbsp;&nbsp;&nbsp;" & tools & "</td></tr>")
                End If
                If lube <> Nothing Then
                    sb.Append("<tr>")
                    sb.Append("<td class=""replain12"">&nbsp;&nbsp;&nbsp;&nbsp;" & lube & "</td></tr>")
                End If

                sb.Append("</table></td>")
                Dim test As String = tpic
                If coi <> "NISS" Then
                    If tpic <> "" Then
                        sb.Append("<td rowspan=""4"" width=""260"" align=""center"" valign=""center"" align=""center"" class=""box""><img src=""" & tpic & """></td></tr>")
                    Else
                        sb.Append("<td rowspan=""4"" width=""260"" align=""center"" valign=""center"" align=""center"" class=""replabel12 box"">" & tmod.getlbl("cdlbl707", "AssignmentReportPics.aspx.vb") & "</td></tr>")
                    End If
                Else
                    If cpic <> "" Then
                        sb.Append("<td rowspan=""4"" width=""260"" align=""center"" valign=""center"" align=""center"" class=""box""><img src=""" & cpic & """></td></tr>")
                    Else
                        sb.Append("<td rowspan=""4"" width=""260"" align=""center"" valign=""center"" align=""center"" class=""replabel12 box"">" & tmod.getlbl("cdlbl707", "AssignmentReportPics.aspx.vb") & "</td></tr>")
                    End If
                End If



                sb.Append("</table></td></tr>")
            Else

                Select Case typ
                    Case "eq1"
                        sql = "usp_GetAssignmentReportS1 '" & eqid & "', '" & task & "'"
                    Case "eq" 'was fu
                        sql = "usp_GetAssignmentReportS1 '" & eqid & "', '" & task & "', '" & fuid & "'"
                    Case "co"
                        sql = "usp_GetAssignmentReportS1 '" & eqid & "', '" & task & "', '" & fuid & "', '" & comid & "'"
                End Select
                dr = tasks.GetRdrData(sql)
                While dr.Read
                    subtask = dr.Item("subtask").ToString
                    task = dr.Item("tasknum").ToString
                    func = row("func").ToString
                    If freq = "0" Then
                        freq = "None"
                    Else
                        freq = dr.Item("freq").ToString
                    End If
                    subt = dr.Item("subt").ToString
                    loto = dr.Item("lotoid").ToString
                    If loto = "0" Then
                        loto = "N"
                    Else
                        loto = "Y"
                    End If
                    cs = dr.Item("conid").ToString
                    If cs = "0" Then
                        cs = "N"
                    Else
                        cs = "Y"
                    End If
                    rd = dr.Item("rdid").ToString
                    If rd = "1" Then
                        rd = "R"
                    ElseIf rd = "2" Then
                        rd = "D"
                    ElseIf rd = "3" Then
                        rd = "A"
                    End If
                    parts = dr.Item("parts").ToString
                    If Len(parts) = 0 OrElse parts = "None" Then
                        parts = Nothing
                    Else
                        parts = tmod.getxlbl("xlb275", "AssignmentReportPics.aspx.vb") & dr.Item("parts").ToString
                    End If
                    tools = dr.Item("tools").ToString
                    If Len(tools) = 0 OrElse tools = "None" Then
                        tools = Nothing
                    Else
                        tools = tmod.getxlbl("xlb276", "AssignmentReportPics.aspx.vb") & dr.Item("tools").ToString
                    End If
                    lube = dr.Item("lubes").ToString
                    If Len(lube) = 0 OrElse lube = "None" Then
                        lube = Nothing
                    Else
                        lube = tmod.getxlbl("xlb277", "AssignmentReportPics.aspx.vb") & dr.Item("lubes").ToString
                    End If

                    compnum = dr.Item("compnum").ToString
                    cqty = dr.Item("cqty").ToString
                    desig = dr.Item("desig").ToString
                    taskdesc = dr.Item("task").ToString
                    tasktype = dr.Item("tasktype").ToString
                    skill = dr.Item("skill").ToString
                    ttime = dr.Item("ttime").ToString

                    tpic = dr.Item("tpics").ToString

                    subt = dr.Item("subt").ToString

                    If subtask = 0 Then
                        sb.Append("<tr><td colspan=""12"">&nbsp;</td></tr>")
                        sb.Append("<tr><td width=""70"" class=""replabel12"" valign=""bottom""><u>" & tmod.getxlbl("xlb269", "AssignmentReportPics.aspx.vb") & "</u></td>")
                        sb.Append("<td width=""40"" class=""replabel12"" valign=""bottom""><u>" & tmod.getxlbl("xlb270", "AssignmentReportPics.aspx.vb") & "</u></td>")
                        sb.Append("<td width=""410"" class=""replabel12"" valign=""bottom""><u>" & tmod.getxlbl("xlb271", "AssignmentReportPics.aspx.vb") & "</u></td>")
                        sb.Append("<td width=""30"" class=""replabel12"" valign=""bottom""><u>Qty</u></td>")

                        sb.Append("<td width=""140"" class=""replabel12"" valign=""bottom""><u>" & tmod.getxlbl("xlb272", "AssignmentReportPics.aspx.vb") & "</u></td>")
                        sb.Append("<td width=""30"" class=""replabel12"" valign=""bottom""><u>R/D</u></td>")
                        sb.Append("<td width=""160"" class=""replabel12"" valign=""bottom""><u>" & tmod.getxlbl("xlb273", "AssignmentReportPics.aspx.vb") & "</u></td>")
                        sb.Append("<td width=""30"" class=""replabel12"" valign=""bottom""><u>MINS</u></td>")
                        sb.Append("<td width=""30"" class=""replabel12"" valign=""bottom""><u>LOTO</u></td>")
                        sb.Append("<td width=""20"" class=""replabel12"" valign=""bottom""><u>CS</u></td></tr>")

                        schk = 0
                        sb.Append("<tr><td class=""repplain12"" valign=""top"">" & freq & "</td>")
                        sb.Append("<td class=""repplain12"" valign=""top"">" & task & "</td>")

                        sb.Append("<td class=""repplain12"" valign=""top""><table cellspacing=""0"" cellpadding=""0"">")
                        sb.Append("<tr><td class=""replabel12"" width=""110"">" & tmod.getlbl("cdlbl703", "AssignmentReportPics.aspx.vb") & "</td><td class=""repplain12"" width=""300"">" & cnum & "</td></tr>")
                        sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl704", "AssignmentReportPics.aspx.vb") & "</td><td class=""repplain12"">" & cdesc & "</td></tr>")
                        sb.Append("<tr><td class=""replabel12"">SPL</td><td class=""repplain12"">" & cspl & "</td></tr>")
                        sb.Append("<tr><td class=""replabel12"">" & tmod.getlbl("cdlbl706", "AssignmentReportPics.aspx.vb") & "</td><td class=""repplain12"">" & desig & "</td></tr>")
                        sb.Append("</table></td>")

                        sb.Append("<td class=""repplain12"" valign=""top"">" & cqty & "</td>")
                        'need update
                        'sb.Append("<td class=""repplain12"">" & cdesc & "</td>")
                        'sb.Append("<td class=""repplain12"">" & cspl & "</td>")

                        'sb.Append("<td class=""repplain12"">" & desig & "</td>")
                        sb.Append("<td class=""repplain12"" valign=""top"">" & tasktype & "</td>")
                        sb.Append("<td class=""repplain12"" valign=""top"">" & rd & "</td>")
                        sb.Append("<td class=""repplain12"" valign=""top"">" & skill & "</td>")
                        sb.Append("<td class=""repplain12"" valign=""top"">" & ttime & "</td>")
                        sb.Append("<td class=""repplain12"" valign=""top"">" & loto & "</td>")
                        sb.Append("<td class=""repplain12"" valign=""top"">" & cs & "</td></tr>") 'tried here

                        sb.Append("<tr><td colspan=""12""><table border=""0"">")

                        If tpic = "" Then
                            sb.Append("<tr><td class=""repplain12 box"" width=""700"" valign=""top"">")
                        Else
                            sb.Append("<tr><td class=""repplain12 box"" width=""700"" height=""240"" valign=""top"">")
                        End If




                        sb.Append("<table>")
                        sb.Append("<tr><td class=""replabel12"" colspan=""1""><u>" & tmod.getxlbl("xlb274", "AssignmentReportPics.aspx.vb") & "</u></td><tr>")
                        sb.Append("<tr><td class=""repplain12"">" & taskdesc & "</td></tr>")

                        If parts <> Nothing Then
                            sb.Append("<tr>")
                            sb.Append("<td class=""repplain12"">&nbsp;&nbsp;&nbsp;&nbsp;" & parts & "</td></tr>")
                        End If
                        If tools <> Nothing Then
                            sb.Append("<tr>")
                            sb.Append("<td class=""repplain12"">&nbsp;&nbsp;&nbsp;&nbsp;" & tools & "</td></tr>")
                        End If
                        If lube <> Nothing Then
                            sb.Append("<tr>")
                            sb.Append("<td class=""replain12"">&nbsp;&nbsp;&nbsp;&nbsp;" & lube & "</td></tr>")
                        End If

                        sb.Append("</table></td>")
                        Dim test As String = tpic
                        If coi <> "NISS" Then
                            If tpic <> "" Then
                                sb.Append("<td rowspan=""4"" width=""260"" align=""center"" valign=""center"" align=""center"" class=""box""><img src=""" & tpic & """></td></tr>")
                            Else
                                sb.Append("<td rowspan=""4"" width=""260"" align=""center"" valign=""center"" align=""center"" class=""replabel12 box"">" & tmod.getlbl("cdlbl707", "AssignmentReportPics.aspx.vb") & "</td></tr>")
                            End If
                        Else
                            If cpic <> "" Then
                                sb.Append("<td rowspan=""4"" width=""260"" align=""center"" valign=""center"" align=""center"" class=""box""><img src=""" & cpic & """></td></tr>")
                            Else
                                sb.Append("<td rowspan=""4"" width=""260"" align=""center"" valign=""center"" align=""center"" class=""replabel12 box"">" & tmod.getlbl("cdlbl707", "AssignmentReportPics.aspx.vb") & "</td></tr>")
                            End If
                        End If



                        sb.Append("</table></td></tr>")
                    Else
                        schk += 1
                        If schk = 0 Then
                            sb.Append("<tr><td>&nbsp;</td></tr>")
                        End If

                        sb.Append("<tr><td colspan=""2""><table><tr><td class=""repplain12"">" & tmod.getxlbl("xlb284", "AssignmentReportPics.aspx.vb") & " [" & subtask & "]</td>")
                        sb.Append("<td class=""repplain12"">" & subt & "</td></tr></table></td></tr>")

                        'sb.Append("</table></td></tr>")
                        'sb.Append("</table></td></tr>")
                    End If
                    If parts <> Nothing Then
                        sb.Append("<tr>")
                        sb.Append("<td class=""repplain12"">&nbsp;&nbsp;&nbsp;&nbsp;" & parts & "</td></tr>")
                    End If
                    If tools <> Nothing Then
                        sb.Append("<tr>")
                        sb.Append("<td class=""repplain12"">&nbsp;&nbsp;&nbsp;&nbsp;" & tools & "</td></tr>")
                    End If
                    If lube <> Nothing Then
                        sb.Append("<tr>")
                        sb.Append("<td class=""repplain12"">&nbsp;&nbsp;&nbsp;&nbsp;" & lube & "</td></tr>")
                    End If
                    If schk = scnt Then
                        schk = 0
                        scnt = 0
                        'sb.Append("</table></td></tr>")

                        'If coi <> "CAS" Then
                        'If tpic <> "" Then
                        'sb.Append("<td rowspan=""4"" width=""260"" align=""center"" valign=""center"" align=""center"" class=""box""><img src=""" & tpic & """></td></tr>")
                        'Else
                        'sb.Append("<td rowspan=""4"" width=""260"" align=""center"" valign=""center"" align=""center"" class=""replabel12 box"">" & tmod.getlbl("cdlbl707", "AssignmentReportPics.aspx.vb") & "</td></tr>")
                        'End If
                        'Else
                        'If cpic <> "" Then
                        'sb.Append("<td rowspan=""4"" width=""260"" align=""center"" valign=""center"" align=""center"" class=""box""><img src=""" & cpic & """></td></tr>")
                        'Else
                        'sb.Append("<td rowspan=""4"" width=""260"" align=""center"" valign=""center"" align=""center"" class=""replabel12 box"">" & tmod.getlbl("cdlbl707", "AssignmentReportPics.aspx.vb") & "</td></tr>")
                        'End If

                        sb.Append("</table></td></tr>")
                        sb.Append("<table>")
                    End If

                    'sb.Append("</table></td></tr>")
                    'End If

                End While
                dr.Close()
            End If




        Next

        'sb.Append("</table></td></tr>")
        sb.Append("</Table>")

        tdwi.InnerHtml = sb.ToString
    End Sub
End Class
