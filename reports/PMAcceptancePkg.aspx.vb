

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMAcceptancePkg
    Inherits System.Web.UI.Page
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim tasks As New Utilities
    Dim dr As SqlDataReader
    Dim eqid, rep1, rep2, rep3, rev1, rev2, asn, pmall, pdmall, title, mode, sall, spdmall, eqlev As String
    Dim pic, subhdr As String
    Protected WithEvents tdwi2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwi3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwi4 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwi5 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwi6 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwi As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwi7 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdpmall As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdparts As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtools As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlubes As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdpdmall As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsall As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdspdmall As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqlev As System.Web.UI.HtmlControls.HtmlTableCell
    Dim cover, order As String
    Dim tmod As New transmod
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblexport As System.Web.UI.HtmlControls.HtmlTable

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            eqid = Request.QueryString("eq").ToString
            cover = Request.QueryString("cvr").ToString
            pic = Request.QueryString("pic").ToString
            title = Request.QueryString("title").ToString
            If Len(title) = 0 Then title = "dflt"
            subhdr = Request.QueryString("sub").ToString
            If Len(subhdr) = 0 Then subhdr = "dflt"
            order = Request.QueryString("order").ToString

            Dim orderarr As String() = Split(order, ";")
            Dim i As Integer
            tasks.Open()
            If cover = "yes" Then
                GetCover(eqid, title, subhdr, pic)
            End If
            Dim ie As Integer = orderarr.Length
            If orderarr.Length > 0 Then
                For i = 0 To orderarr.Length - 1
                    Select Case orderarr(i)
                        Case 1
                            GetOpt(eqid, "all")
                        Case 2
                            GetOpt(eqid, "ronly")
                        Case 3
                            GetOpt(eqid, "chngonly")
                        Case 4
                            GetRev(eqid, "all")
                        Case 5
                            GetRev(eqid, "chngonly")
                        Case 6
                            GetAsn(eqid)
                        Case 7
                            GetPMOut1(eqid, "1")
                        Case 8
                            GetPMOut1(eqid, "2")
                        Case 9
                            GetSOut(eqid, "1")
                        Case 10
                            GetSOut(eqid, "2")
                        Case 11
                            PopEqVal(eqid)
                    End Select
                Next
            End If
            tasks.Dispose()
        End If
    End Sub
    Private Sub GetCover(ByVal eqid As String, ByVal title As String, ByVal subhdr As String, ByVal pic As String)
        If title = "dflt" Then
            title = tmod.getxlbl("xlb398" , "PMAcceptancePkg.aspx.vb")
        End If
        If subhdr = "dflt" Then
            subhdr = ""
        End If
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""600"" style=""page-break-after:always;"">")
        sb.Append("<tr><td class=""label16"" colspan=""4"" align=""center"">" & title & "</td></tr>")

        sb.Append("<tr><td class=""plainlabel"" colspan=""4"" align=""center"">" & subhdr & "</td></tr>")

        sb.Append("<tr><td colspan=""4"" align=""center"" class=""plainlabel"">" & Now() & "</td></tr>")
        sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
        sb.Append("<tr><td width=""120"">&nbsp;</td><td width=""180""></td><td width=""100""></td><td width=""200""></td></tr>")
        sql = "usp_GetCover '" & eqid & "'"
        dr = tasks.GetRdrData(sql)

        While dr.Read
            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl1087" , "PMAcceptancePkg.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("sitename").ToString & "</td></tr>")

            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl1088" , "PMAcceptancePkg.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("dept_line").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""2"">" & dr.Item("dept_desc").ToString & "</td></tr>")

            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl1089" , "PMAcceptancePkg.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("cell_name").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""2"">" & dr.Item("cell_desc").ToString & "</td></tr>")

            sb.Append("<tr><td>&nbsp;</td></tr>")

            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl1090" , "PMAcceptancePkg.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("eqnum").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""2"">" & dr.Item("eqdesc").ToString & "</td></tr>")

            Dim loc As String = dr.Item("location").ToString
            If Len(loc) = 0 Then
                loc = "No Location Specified"
            End If
            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl1091" , "PMAcceptancePkg.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & loc & "</td></tr>")

            Dim assetclass As String = dr.Item("assetclass").ToString
            If assetclass = "Select" Then
                assetclass = "No Asset Class Assigned"
            End If
            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl1092" , "PMAcceptancePkg.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & assetclass & "</td></tr>")

            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl1093" , "PMAcceptancePkg.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("oem").ToString & "</td></tr>")
            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl1094" , "PMAcceptancePkg.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("model").ToString & "</td></tr>")
            Dim serial As String = dr.Item("serial").ToString
            If Len(serial) = 0 Then
                serial = "No Serial Number Assigned"
            End If
            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl1095" , "PMAcceptancePkg.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & serial & "</td></tr>")
            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl1096" , "PMAcceptancePkg.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("spl").ToString & "</td></tr>")

            sb.Append("<tr><td>&nbsp;</td></tr>")

            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl1097" , "PMAcceptancePkg.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("createdby").ToString & "</td></tr>")
            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl1098" , "PMAcceptancePkg.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("createdate").ToString & "</td></tr>")

            sb.Append("<tr><td>&nbsp;</td></tr>")

            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl1099" , "PMAcceptancePkg.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("modifiedby").ToString & "</td></tr>")
            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl1100" , "PMAcceptancePkg.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("modifieddate").ToString & "</td></tr>")

            sb.Append("<tr><td>&nbsp;</td></tr>")

            If pic = "yes" Then
                Dim img As String = dr.Item("picurl").ToString
                If Len(img) > 0 AndAlso img <> "None" Then
                    sb.Append("<tr><td colspan=""4"" align=""center""><img src=""" & img & """></td></tr>")
                End If
            End If

        End While
        dr.Close()
        sb.Append("</Table>")
        tdwi.InnerHtml = sb.ToString
    End Sub
    Private Sub GetOpt(ByVal eqid As String, ByVal rep As String)

        If rep = "all" Then
            sql = "usp_GetRationalAll '" & eqid & "'"
            title = tmod.getxlbl("xlb399" , "PMAcceptancePkg.aspx.vb")
        ElseIf rep = "ronly" Then
            sql = "usp_GetRationalwROnly '" & eqid & "'"
            title = tmod.getxlbl("xlb400" , "PMAcceptancePkg.aspx.vb")
        ElseIf rep = "chngonly" Then
            sql = "usp_GetRationalwChngOnly '" & eqid & "'"
            title = tmod.getxlbl("xlb401" , "PMAcceptancePkg.aspx.vb")
        End If

        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""600"" style=""page-break-after:always;"">")
        sb.Append("<tr><td class=""label16"" colspan=""4"" align=""center"">" & title & "</td></tr>")
        sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
        sb.Append("<tr><td width=""100""></td><td width=""165""></td><td width=""165""></td><td width=""170""></td></tr>")



        dr = tasks.GetRdrData(sql)
        Dim start As Integer = 0
        Dim fchk As String = ""
        Dim func As String
        Dim tchk As String = ""
        Dim task As String
        Dim oti, ti, rat, ts As String
        While dr.Read
            If start = 0 Then
                start = 1
                sb.Append("<tr><td class=""label14"" colspan=""4"">" & tmod.getxlbl("xlb379" , "PMAcceptancePkg.aspx.vb") & " " & dr.Item("eqnum").ToString & " - " & dr.Item("eqdesc").ToString & "</td></tr>")
                sb.Append("<tr><td class=""label14"" colspan=""2"" style=""border-bottom: solid 1px gray;"">Department: " & dr.Item("dept_line").ToString & "</td>")
                sb.Append("<td class=""label14"" colspan=""2"" style=""border-bottom: solid 1px gray;"">Station/Cell: " & dr.Item("cell_name").ToString & "</td></tr>")


            End If
            func = dr.Item("func").ToString
            If fchk <> func Then
                fchk = func
                tchk = ""
                sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""label14"" colspan=""4"">" & tmod.getxlbl("xlb380" , "PMAcceptancePkg.aspx.vb") & " " & dr.Item("func").ToString & "</td></tr>")

            End If
            task = dr.Item("tasknum").ToString
            ts = dr.Item("taskstatus").ToString
            If Len(ts) = 0 Then
                ts = "No Status Selected"
            End If
            If tchk <> task Then
                If tchk <> "" Then
                    sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
                End If
                tchk = task

                sb.Append("<tr><td class=""label14"" colspan=""2"">" & tmod.getxlbl("xlb381" , "PMAcceptancePkg.aspx.vb") & " " & task & "</td>")
                sb.Append("<td class=""label14"" colspan=""2"">" & tmod.getxlbl("xlb382" , "PMAcceptancePkg.aspx.vb") & " " & ts & "</td></tr>")
                sb.Append("<tr><td class=""label14"" colspan=""4"">" & tmod.getxlbl("xlb383" , "PMAcceptancePkg.aspx.vb") & " " & dr.Item("compnum").ToString & "</td></tr>")
                sb.Append("<tr><td class=""rtlabelhdr"">" & tmod.getlbl("cdlbl1101" , "PMAcceptancePkg.aspx.vb") & "</td>")
                sb.Append("<td class=""cntrlabelhdr"">" & tmod.getlbl("cdlbl1102" , "PMAcceptancePkg.aspx.vb") & "</td>")
                sb.Append("<td class=""cntrlabelhdr"">" & tmod.getlbl("cdlbl1103" , "PMAcceptancePkg.aspx.vb") & "</td>")
                sb.Append("<td class=""endlabelhdr"">" & tmod.getlbl("cdlbl1104" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
            End If
            oti = dr.Item("orig").ToString
            If Len(oti) = 0 Then oti = "None"
            ti = dr.Item("revised").ToString
            If Len(ti) = 0 Then ti = "None"
            rat = dr.Item("rationale").ToString
            If Len(rat) = 0 Then rat = "None Provided"
            sb.Append("<tr><td class=""rtlabelpln"" border=""1"">" & dr.Item("taskcol").ToString & "</td>")
            sb.Append("<td class=""cntrlabelpln"">" & oti & "</td>")
            sb.Append("<td class=""cntrlabelpln"">" & ti & "</td>")
            sb.Append("<td class=""endlabelpln"">" & rat & "</td></tr>")

        End While
        dr.Close()
        sb.Append("</Table>")
        If rep = "all" Then
            tdwi2.InnerHtml = sb.ToString
        ElseIf rep = "ronly" Then
            tdwi3.InnerHtml = sb.ToString
        ElseIf rep = "chngonly" Then
            tdwi4.InnerHtml = sb.ToString
        End If
    End Sub
    Private Sub GetRev(ByVal eqid As String, ByVal rep As String)
        If rep = "all" Then
            sql = "usp_GetBeforeAfterAll '" & eqid & "'"
            title = "PMO Review Report - All Records"
        ElseIf rep = "chngonly" Then
            sql = "usp_GetBeforeAfterChng '" & eqid & "'"
            title = "PMO Review Report - with Changes Only"
        End If

        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""600"" style=""page-break-after:always;"">")
        sb.Append("<tr><td class=""label16"" colspan=""4"" align=""center"">" & title & "</td></tr>")
        sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
        sb.Append("<tr><td width=""100""></td><td width=""200""></td><td width=""50""></td><td width=""250""></td></tr>")

        dr = tasks.GetRdrData(sql)
        Dim start As Integer = 0
        Dim fchk As String = ""
        Dim func As String
        Dim tchk As String = ""
        Dim task As String
        Dim oti, ti, rat, ts As String
        While dr.Read
            If start = 0 Then
                start = 1
                sb.Append("<tr><td class=""label14"" colspan=""4"">" & tmod.getxlbl("xlb384" , "PMAcceptancePkg.aspx.vb") & " " & dr.Item("eqnum").ToString & " - " & dr.Item("eqdesc").ToString & "</td></tr>")
                sb.Append("<tr><td class=""label14"" colspan=""2"" style=""border-bottom: solid 1px gray;"">Department: " & dr.Item("dept_line").ToString & "</td>")
                sb.Append("<td class=""label14"" colspan=""2"" style=""border-bottom: solid 1px gray;"">Station/Cell: " & dr.Item("cell_name").ToString & "</td></tr>")


            End If
            func = dr.Item("func").ToString
            If fchk <> func Then
                fchk = func
                tchk = ""
                sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""label14"" colspan=""4"">" & tmod.getxlbl("xlb385" , "PMAcceptancePkg.aspx.vb") & " " & dr.Item("func").ToString & "</td></tr>")

            End If
            task = dr.Item("tasknum").ToString
            ts = dr.Item("taskstatus").ToString
            If Len(ts) = 0 Then
                ts = "No Status Selected"
            End If
            If tchk <> task Then
                If tchk <> "" Then
                    sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
                End If
                tchk = task

                sb.Append("<tr><td class=""label14"" colspan=""2"">" & tmod.getxlbl("xlb386" , "PMAcceptancePkg.aspx.vb") & " " & task & "</td>")
                sb.Append("<td class=""label14"" colspan=""2"">" & tmod.getxlbl("xlb387" , "PMAcceptancePkg.aspx.vb") & " " & ts & "</td></tr>")
                sb.Append("<tr><td class=""label14"" colspan=""4"">" & tmod.getxlbl("xlb388" , "PMAcceptancePkg.aspx.vb") & " " & dr.Item("compnum").ToString & "</td></tr>")
                sb.Append("<tr><td class=""rtlabelhdr"">" & tmod.getlbl("cdlbl1105" , "PMAcceptancePkg.aspx.vb") & "</td>")
                sb.Append("<td class=""cntrlabelhdr"" colspan=""2"">" & tmod.getlbl("cdlbl1106" , "PMAcceptancePkg.aspx.vb") & "</td>")
                sb.Append("<td class=""endlabelhdr"">" & tmod.getlbl("cdlbl1107" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")

            End If
            oti = dr.Item("orig").ToString
            If Len(oti) = 0 Then oti = "None"
            ti = dr.Item("revised").ToString
            If Len(ti) = 0 Then ti = "None"

            If Len(rat) = 0 Then rat = "None Provided"
            sb.Append("<tr><td class=""rtlabelpln"" border=""1"">" & dr.Item("taskcol").ToString & "</td>")
            sb.Append("<td class=""cntrlabelpln"" colspan=""2"">" & oti & "</td>")
            sb.Append("<td class=""endlabelpln"">" & ti & "</td></tr>")

        End While
        dr.Close()
        sb.Append("</Table>")
        If rep = "all" Then
            tdwi5.InnerHtml = sb.ToString
        ElseIf rep = "chngonly" Then
            tdwi6.InnerHtml = sb.ToString
        End If
    End Sub
    Private Sub GetAsn(ByVal eqid As String)
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""960""  style=""page-break-after:always;"">")
        sb.Append("<tr><td class=""label16"" colspan=""12"" align=""center"">" & tmod.getlbl("cdlbl1108" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td colspan=""12"">&nbsp;</td></tr>")

        sql = "usp_GetAssignmentReport '" & eqid & "'"
        dr = tasks.GetRdrData(sql)
        Dim start As Integer = 0
        Dim fchk As String = ""
        Dim func As String
        Dim tchk As String = ""
        Dim task As String
        Dim oti, ti, rat, ts, freq, loto, cs, rd, lube As String
        While dr.Read
            If start = 0 Then
                start = 1
                sb.Append("<tr><td class=""label14"" colspan=""4"" >" & tmod.getxlbl("xlb389" , "PMAcceptancePkg.aspx.vb") & " " & dr.Item("sitename").ToString & "</td>")
                sb.Append("<td class=""label14"" colspan=""2"" >" & tmod.getxlbl("xlb390" , "PMAcceptancePkg.aspx.vb") & " " & dr.Item("dept_line").ToString & "</td>")
                sb.Append("<td class=""label14"" colspan=""6"">" & tmod.getxlbl("xlb391" , "PMAcceptancePkg.aspx.vb") & " " & dr.Item("cell_name").ToString & "</td></tr>")
                sb.Append("<tr><td class=""label14"" colspan=""12"">" & tmod.getxlbl("xlb392" , "PMAcceptancePkg.aspx.vb") & " " & dr.Item("eqnum").ToString & " - " & dr.Item("eqdesc").ToString & "</td></tr>")
                sb.Append("<tr><td class=""label14"" colspan=""4"" style=""border-bottom: solid 1px gray;"">OEM: " & dr.Item("oem").ToString & "</td>")
                sb.Append("<td class=""label14"" colspan=""2"" style=""border-bottom: solid 1px gray;"">" & tmod.getxlbl("xlb393" , "PMAcceptancePkg.aspx.vb") & " " & dr.Item("model").ToString & "</td>")
                sb.Append("<td class=""label14"" colspan=""6"" style=""border-bottom: solid 1px gray;"">" & tmod.getxlbl("xlb394" , "PMAcceptancePkg.aspx.vb") & " " & dr.Item("serial").ToString & "</td></tr>")
            End If
            func = dr.Item("func").ToString
            If fchk <> func Then
                fchk = func
                tchk = ""
                sb.Append("<tr><td colspan=""12"">&nbsp;</td></tr>")
                sb.Append("<tr><td width=""70"" class=""labelsmtl"">" & tmod.getlbl("cdlbl1109" , "PMAcceptancePkg.aspx.vb") & "</td><td width=""40"" class=""labelsmtl"">" & tmod.getlbl("cdlbl1110" , "PMAcceptancePkg.aspx.vb") & "</td><td width=""150"" class=""labelsmtl"">" & tmod.getlbl("cdlbl1111" , "PMAcceptancePkg.aspx.vb") & "</td>")
                sb.Append("<td width=""30"" class=""labelsmtl"">Qty</td><td width=""150"" class=""labelsmtl"">" & tmod.getlbl("cdlbl1113" , "PMAcceptancePkg.aspx.vb") & "</td><td width=""220"" class=""labelsmtl"">" & tmod.getlbl("cdlbl1114" , "PMAcceptancePkg.aspx.vb") & "</td>")
                sb.Append("<td width=""100"" class=""labelsmtl"">" & tmod.getlbl("cdlbl1115" , "PMAcceptancePkg.aspx.vb") & "</td><td width=""30"" class=""labelsmtl"">R/D</td><td width=""80"" class=""labelsmtl"">" & tmod.getlbl("cdlbl1117" , "PMAcceptancePkg.aspx.vb") & "</td>")
                sb.Append("<td width=""30"" class=""labelsmtl"">MINS</td><td width=""40"" class=""labelsmtl"">LOTO</td><td width=""20"" class=""labelsmtl"">CS</td></tr>")
                sb.Append("<tr><td colspan=""12""  class=""labelsmu"">" & tmod.getxlbl("xlb395" , "PMAcceptancePkg.aspx.vb") & " " & dr.Item("func").ToString & "</td></tr>")

            End If
            task = dr.Item("tasknum").ToString
            freq = dr.Item("freqid").ToString
            If freq = "0" Then
                freq = "None"
            Else
                freq = dr.Item("freq").ToString
            End If
            loto = dr.Item("lotoid").ToString
            If loto = "0" Then
                loto = "N"
            Else
                loto = "Y"
            End If
            cs = dr.Item("conid").ToString
            If cs = "0" Then
                cs = "N"
            Else
                cs = "Y"
            End If
            rd = dr.Item("rdid").ToString
            If rd = "1" Then
                rd = "R"
            ElseIf rd = "2" Then
                rd = "D"
            ElseIf rd = "3" Then
                rd = "A"
            Else
                cs = "M"
            End If
            lube = dr.Item("lubes").ToString
            If Len(lube) = 0 OrElse lube = "None" Then
                lube = Nothing
            Else
                lube = "LUBE:" & dr.Item("lubes").ToString
            End If
            sb.Append("<tr><td class=""plainlabelsm"">" & freq & "</td>")
            sb.Append("<td class=""plainlabelsm"">" & task & "</td>")
            sb.Append("<td class=""plainlabelsm"">" & dr.Item("compnum").ToString & "</td>")
            sb.Append("<td class=""plainlabelsm"">" & dr.Item("cqty").ToString & "</td>")
            sb.Append("<td class=""plainlabelsm"">" & dr.Item("desig").ToString & "</td>")
            sb.Append("<td class=""plainlabelsm"">" & dr.Item("task").ToString & "</td>")
            sb.Append("<td class=""plainlabelsm"">" & dr.Item("tasktype").ToString & "</td>")
            sb.Append("<td class=""plainlabelsm"">" & rd & "</td>")
            sb.Append("<td class=""plainlabelsm"">" & dr.Item("skill").ToString & "</td>")
            sb.Append("<td class=""plainlabelsm"">" & dr.Item("ttime").ToString & "</td>")
            sb.Append("<td class=""plainlabelsm"">" & loto & "</td>")
            sb.Append("<td class=""plainlabelsm"">" & cs & "</td></tr>")
            sb.Append("<tr><td colspan=""3""></td>")
            sb.Append("<td colspan=""2"" class=""plainlabelsm"">" & lube & "</td></tr>")

        End While
        dr.Close()
        sb.Append("</Table>")
        tdwi7.InnerHtml = sb.ToString

    End Sub
    Private Sub GetPMOut1(ByVal eqid As String, ByVal mode As String)
        Dim fid As String = ""
        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim funcchk As String = ""
        Dim func As String = ""
        Dim subtask As String = ""
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620""  style=""page-break-after:always;"">")
        sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""580""></td></tr>")
        If mode = "1" Then
            sql = "usp_getWITotalPGEMS '" & eqid & "'"
        Else
            sql = "usp_getWITotalPGEMSPdM '" & eqid & "'"
        End If
        Dim parts, tools, lubes As String
        parts = ""
        tools = ""
        lubes = ""
        dr = tasks.GetRdrData(sql)
        While dr.Read
            skill = dr.Item("skill").ToString & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString

            If skill <> skillchk Then
                If skillchk <> "" Then
                    sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl1121" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
                    Dim partarr As String() = Split(parts, ";")
                    Dim p As Integer
                    For p = 0 To partarr.Length - 1
                        If partarr(p) = "none" Then 'Len(partarr(p)) = 0 Or
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & tmod.getlbl("cdlbl1122" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
                        Else
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & partarr(p) & "</td></tr>")
                        End If
                    Next

                    sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl1123" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
                    Dim toolarr As String() = Split(tools, ";")
                    Dim t As Integer
                    For t = 0 To toolarr.Length - 1
                        If toolarr(t) = "none" Then 'Len(toolarr(t)) = 0 Or 
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & tmod.getlbl("cdlbl1124" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
                        Else
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & toolarr(t) & "</td></tr>")
                        End If
                    Next

                    sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl1125" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
                    Dim lubearr As String() = Split(lubes, ";")
                    Dim l As Integer
                    For l = 0 To lubearr.Length - 1
                        If lubearr(l) = "none" Then 'Len(lubearr(l)) = 0 Or 
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & tmod.getlbl("cdlbl1126" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
                        Else
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & lubearr(l) & "</td></tr>")
                        End If

                    Next
                    parts = ""
                    tools = ""
                    lubes = ""
                    sb.Append("<tr><td colspan=""3"" style=""border-bottom: 1px solid black;"">&nbsp;</td>")
                    sb.Append("<tr><td>&nbsp;</td></tr>")
                End If
                skillchk = skill
                start = 0
                flag = 0
                If mode = "1" Then
                    sb.Append("<tr><td class=""bigbold"" colspan=""3"">" _
                    + "" & dr.Item("freq").ToString & " / " & dr.Item("skill").ToString & " / " & dr.Item("rd").ToString & "</td></tr>")
                Else
                    sb.Append("<tr><td class=""bigbold"" colspan=""3"">" _
                    + "PdM - " & dr.Item("pretech").ToString & " - " & dr.Item("freq").ToString & " / " & dr.Item("skill").ToString & " / " & dr.Item("rd").ToString & "</td></tr>")
                End If

                sb.Append("<tr><td class=""label"" colspan=""3"">" & tmod.getxlbl("xlb396" , "PMAcceptancePkg.aspx.vb") & "  " & dr.Item("down").ToString & " Minutes</td></tr>")
                sb.Append("<tr><td class=""label"" colspan=""3"">" & dr.Item("eqnum").ToString & " - " & dr.Item("eqdesc").ToString & "</td></tr>")
                sb.Append("<tr><td>&nbsp;</td></tr>")
                sb.Append("<tr><TD class=""label"" colspan=""3"" bgcolor=""silver"">" & tmod.getxlbl("xlb397" , "PMAcceptancePkg.aspx.vb") & "</TD></tr>")
                sb.Append("<tr><td>&nbsp;</td></tr>")
            End If
            func = dr.Item("func").ToString
            If flag = 0 Then
                flag = 1
                funcchk = func
                sb.Append("<tr><td class=""labelu"" colspan=""3"">" & func & "</td></tr>")
            Else
                If func <> funcchk Then
                    funcchk = func
                    sb.Append("<tr><td colspan=""3"" style=""border-bottom: 1px solid black;"">&nbsp;</td>")
                    sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""labelu"" colspan=""3"">" & func & "</td></tr>")
                End If
            End If
            Dim task, lube, meas As String
            task = dr.Item("task").ToString
            If dr.Item("lubes").ToString <> "none" Or Len(dr.Item("lubes").ToString) = 0 Then
                lube = "; " & dr.Item("lubes").ToString
            Else
                lube = ""
            End If
            'If dr.Item("lube").ToString <> "none" Then
            'lube = "; " & dr.Item("lube").ToString
            'Else
            'lube = ""
            'End If
            If dr.Item("meas").ToString <> "none" Then
                meas = "; " & dr.Item("meas").ToString
            Else
                meas = ""
            End If
            subtask = dr.Item("subtask").ToString
            task = task & lube & meas

            If subtask = "0" Then
                sb.Append("<tr><td class=""plainlabel"">[" & dr.Item("tasknum").ToString & "]</td>")
                If Len(dr.Item("task").ToString) > 0 Then
                    sb.Append("<td colspan=""2"" class=""plainlabel"">" & task & "</td></tr>")
                Else
                    sb.Append("<td colspan=""2"" class=""plainlabel"">" & tmod.getlbl("cdlbl1127" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
                End If
                sb.Append("<tr><td></td>")
                sb.Append("<td colspan=""2"" class=""plainlabel"">OK(___) " & dr.Item("fm1").ToString & "</td></tr>")
            Else
                sb.Append("<tr><td></td><td class=""plainlabel"">[" & subtask & "]</td>")
                If Len(dr.Item("subt").ToString) > 0 Then
                    sb.Append("<td class=""plainlabel"">" & dr.Item("subt").ToString & "</td></tr>")
                Else
                    sb.Append("<td class=""plainlabel"">" & tmod.getlbl("cdlbl1128" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
                End If
                sb.Append("<tr><td></td><td></td>")
                sb.Append("<td class=""plainlabel"">OK(___)</td></tr>")
            End If


            If dr.Item("parts").ToString <> "none" Then 'Or Len(dr.Item("parts").ToString) = 0 Then
                If Len(parts) = 0 Then
                    parts += dr.Item("parts").ToString
                Else
                    parts += ";" & dr.Item("parts").ToString
                End If
            End If

            If dr.Item("tools").ToString <> "none" Then 'Or Len(dr.Item("tools").ToString) <> 0 Then
                If Len(tools) = 0 Then
                    tools += dr.Item("tools").ToString
                Else
                    tools += ";" & dr.Item("tools").ToString
                End If

            End If

            If dr.Item("lubes").ToString <> "none" Then 'Or Len(dr.Item("lubes").ToString) <> 0 Then
                If Len(lubes) = 0 Then
                    lubes += dr.Item("lubes").ToString
                Else
                    lubes += ";" & dr.Item("lubes").ToString
                End If

            End If

        End While
        dr.Close()

        sb.Append("<tr><td>&nbsp;</td></tr>")
        sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl1129" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
        Dim partarr1 As String() = Split(parts, ";")
        Dim p1 As Integer
        For p1 = 0 To partarr1.Length - 1
            If Len(partarr1(p1)) = 0 Or partarr1(p1) = "none" Then
                Dim test As String = partarr1(p1)
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & tmod.getlbl("cdlbl1130" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
            Else
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & partarr1(p1) & "</td></tr>")
            End If
        Next

        sb.Append("<tr><td>&nbsp;</td></tr>")
        sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl1131" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
        Dim toolarr1 As String() = Split(tools, ";")
        Dim t1 As Integer
        For t1 = 0 To toolarr1.Length - 1
            If Len(toolarr1(t1)) = 0 Or toolarr1(t1) = "none" Then
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & tmod.getlbl("cdlbl1132" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
            Else
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & toolarr1(t1) & "</td></tr>")
            End If
        Next

        sb.Append("<tr><td>&nbsp;</td></tr>")
        sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl1133" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
        Dim lubearr1 As String() = Split(lubes, ";")
        Dim l1 As Integer
        For l1 = 0 To lubearr1.Length - 1
            If Len(lubearr1(l1)) = 0 Or lubearr1(l1) = "none" Then
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & tmod.getlbl("cdlbl1134" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
            Else
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & lubearr1(l1) & "</td></tr>")
            End If

        Next

        sb.Append("</Table>")
        If mode = "1" Then
            tdpmall.InnerHtml = sb.ToString
        Else
            tdpdmall.InnerHtml = sb.ToString
        End If

    End Sub
    Private Sub GetSOut(ByVal eqid As String, ByVal mode As String)
        Dim fid As String = ""
        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim funcchk As String = ""
        Dim func As String = ""
        Dim subtask As String = ""
        Dim fu As String = ""
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"" style=""page-break-after:always;"">")
        sb.Append("<tr><td width=""20""><td width=""20""></td><td width=""580""></td></tr>")
        If mode = "1" Then
            sql = "usp_getWITotalPGEMS '" & eqid & "'"
        Else
            sql = "usp_getWITotalPGEMSPdM '" & eqid & "'"
        End If
        Dim parts, tools, lubes As String
        parts = ""
        tools = ""
        lubes = ""
        dr = tasks.GetRdrData(sql)
        While dr.Read
            skill = dr.Item("skill").ToString & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString

            If skill <> skillchk Then
                If skillchk <> "" Then
                    sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl1135" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
                    Dim partarr As String() = Split(parts, ";")
                    Dim p As Integer
                    For p = 0 To partarr.Length - 1
                        If partarr(p) = "none" Then 'Len(partarr(p)) = 0 Or
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & tmod.getlbl("cdlbl1136" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
                        Else
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & partarr(p) & "</td></tr>")
                        End If
                    Next

                    sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl1137" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
                    Dim toolarr As String() = Split(tools, ";")
                    Dim t As Integer
                    For t = 0 To toolarr.Length - 1
                        If toolarr(t) = "none" Then 'Len(toolarr(t)) = 0 Or 
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & tmod.getlbl("cdlbl1138" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
                        Else
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & toolarr(t) & "</td></tr>")
                        End If
                    Next

                    sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl1139" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
                    Dim lubearr As String() = Split(lubes, ";")
                    Dim l As Integer
                    For l = 0 To lubearr.Length - 1
                        If lubearr(l) = "none" Then 'Len(lubearr(l)) = 0 Or 
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & tmod.getlbl("cdlbl1140" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
                        Else
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & lubearr(l) & "</td></tr>")
                        End If

                    Next
                    parts = ""
                    tools = ""
                    lubes = ""
                    sb.Append("<tr><td colspan=""3"" style=""border-bottom: 1px solid black;"">&nbsp;</td>")
                    sb.Append("<tr><td>&nbsp;</td></tr>")
                End If
                skillchk = skill
                start = 0
                flag = 0
                If mode = "1" Then
                    sb.Append("<tr><td class=""bigbold"" colspan=""3"">" _
                    + "" & dr.Item("freq").ToString & " / " & dr.Item("skill").ToString & " / " & dr.Item("rd").ToString & "</td></tr>")
                Else
                    sb.Append("<tr><td class=""bigbold"" colspan=""3"">" _
                    + "PdM - " & dr.Item("pretech").ToString & " - " & dr.Item("freq").ToString & " / " & dr.Item("skill").ToString & " / " & dr.Item("rd").ToString & "</td></tr>")
                End If

                sb.Append("<tr><td class=""label"" colspan=""3"">Total DownTime:  " & dr.Item("down").ToString & " Minutes</td></tr>")
                sb.Append("<tr><td class=""label"" colspan=""3"">" & dr.Item("eqnum").ToString & " - " & dr.Item("eqdesc").ToString & "</td></tr>")
                sb.Append("<tr><td>&nbsp;</td></tr>")
                sb.Append("<tr><TD class=""label"" colspan=""3"" bgcolor=""silver"">Work Instructions</TD></tr>")
                sb.Append("<tr><td>&nbsp;</td></tr>")
            End If
            fu = dr.Item("funcid").ToString
            func = dr.Item("func").ToString
            If flag = 0 Then
                flag = 1
                funcchk = func
                sb.Append("<tr><td class=""labelu"" colspan=""3"">" & func & "</td></tr>")
            Else
                If func <> funcchk Then
                    funcchk = func
                    sb.Append("<tr><td colspan=""3"" style=""border-bottom: 1px solid black;"">&nbsp;</td>")
                    sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""labelu"" colspan=""3"">" & func & "</td></tr>")
                End If
            End If
            Dim task, lube, meas As String
            task = dr.Item("task").ToString
            If dr.Item("lubes").ToString <> "none" Or Len(dr.Item("lubes").ToString) = 0 Then
                lube = "; " & dr.Item("lubes").ToString
            Else
                lube = ""
            End If
            'If dr.Item("lube").ToString <> "none" Then
            'lube = "; " & dr.Item("lube").ToString
            'Else
            'lube = ""
            'End If
            If dr.Item("meas").ToString <> "none" Then
                meas = "; " & dr.Item("meas").ToString
            Else
                meas = ""
            End If

            subtask = dr.Item("subtask").ToString
            task = task & lube & meas

            If subtask = "0" Then
                sb.Append("<tr><td class=""plainlabel"">[" & dr.Item("tasknum").ToString & "]</td>")
                If Len(dr.Item("task").ToString) > 0 Then
                    sb.Append("<td colspan=""2"" class=""plainlabel"">" & task & "</td></tr>")
                Else
                    sb.Append("<td  colspan=""2"" class=""plainlabel"">" & tmod.getlbl("cdlbl1141" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
                End If
                sb.Append("<tr><td></td>")
                sb.Append("<td  colspan=""2"" class=""plainlabel"">OK(___) " & dr.Item("fm1").ToString & "</td></tr>")
            Else
                sb.Append("<tr><td></td><td class=""plainlabel"">[" & subtask & "]</td>")
                If Len(dr.Item("subt").ToString) > 0 Then
                    sb.Append("<td class=""plainlabel"">" & dr.Item("subt").ToString & "</td></tr>")
                Else
                    sb.Append("<td class=""plainlabel"">" & tmod.getlbl("cdlbl1142" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
                End If
                sb.Append("<tr><td></td><td></td>")
                sb.Append("<td class=""plainlabel"">OK(___)</td></tr>")
            End If

            If dr.Item("parts").ToString <> "none" Then 'Or Len(dr.Item("parts").ToString) = 0 Then
                If Len(parts) = 0 Then
                    parts += dr.Item("parts").ToString
                Else
                    parts += ";" & dr.Item("parts").ToString
                End If
            End If

            If dr.Item("tools").ToString <> "none" Then 'Or Len(dr.Item("tools").ToString) <> 0 Then
                If Len(tools) = 0 Then
                    tools += dr.Item("tools").ToString
                Else
                    tools += ";" & dr.Item("tools").ToString
                End If

            End If

            If dr.Item("lubes").ToString <> "none" Then 'Or Len(dr.Item("lubes").ToString) <> 0 Then
                If Len(lubes) = 0 Then
                    lubes += dr.Item("lubes").ToString
                Else
                    lubes += ";" & dr.Item("lubes").ToString
                End If

            End If

        End While
        dr.Close()

        'Pictures - Eq
        Dim eqcnt As Integer
        Dim tdcnt As Integer = 1
        sql = "select count(*) from pmpictures where eqid = '" & eqid & "' and funcid is null and comid is null"
        eqcnt = tasks.Scalar(sql)
        If eqcnt > 0 Then
            sql = "select pic_id, picurltm from pmpictures where eqid = '" & eqid & "' and funcid is null and comid is null"
            sb.Append("<tr><td>&nbsp;</td></tr>")
            sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl1143" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
            sb.Append("<tr><td colspan=""3""><table><tr>")
            dr = tasks.GetRdrData(sql)
            While dr.Read
                If eqcnt <> 0 Then
                    If tdcnt <= 2 Then
                        sb.Append("<td width=""50%""><img id=""" + dr.Item("pic_id").ToString + """ border=""0"" src=""" + dr.Item("picurltm").ToString + """></td>")
                        eqcnt = eqcnt - 1
                        tdcnt = tdcnt + 1
                    Else
                        sb.Append("</tr><tr>")
                        sb.Append("<td width=""50%""><img id=""" + dr.Item("pic_id").ToString + """ border=""0"" src=""" + dr.Item("picurltm").ToString + """></td>")
                        tdcnt = 2
                        eqcnt = eqcnt - 1
                    End If
                End If
            End While
            dr.Close()
            sb.Append("</tr></table></td></tr>")
        End If
        'Pictures - Fu
        Dim fucnt As Integer
        Dim tdcnt2 As Integer = 1
        sql = "select count(*) from pmpictures where eqid = '" & eqid & "' and funcid = '" & fu & "' and comid is null"
        eqcnt = tasks.Scalar(sql)
        If fucnt > 0 Then
            sql = "select pic_id, picurltm from pmpictures where eqid = '" & eqid & "' and funcid = '" & fu & "' and comid is null"
            sb.Append("<tr><td>&nbsp;</td></tr>")
            sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl1144" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
            sb.Append("<tr><td colspan=""3""><table><tr>")
            dr = tasks.GetRdrData(sql)
            While dr.Read
                If fucnt <> 0 Then
                    If tdcnt2 <= 2 Then
                        sb.Append("<td width=""50%""><img id=""" + dr.Item("pic_id").ToString + """ border=""0"" src=""" + dr.Item("picurltm").ToString + """></td>")
                        fucnt = fucnt - 1
                        tdcnt2 = tdcnt2 + 1
                    Else
                        sb.Append("</tr><tr>")
                        sb.Append("<td width=""50%""><img id=""" + dr.Item("pic_id").ToString + """ border=""0"" src=""" + dr.Item("picurltm").ToString + """></td>")
                        tdcnt2 = 2
                        fucnt = fucnt - 1
                    End If
                End If
            End While
            dr.Close()
            sb.Append("</tr></table></td></tr>")
        End If
        'Pictures - Co
        Dim cocnt As Integer
        Dim tdcnt3 As Integer = 1
        sql = "select count(*) from pmpictures where funcid = '" & fu & "' and comid is not null"
        eqcnt = tasks.Scalar(sql)
        If fucnt > 0 Then
            sql = "select pic_id, picurltm from pmpictures where funcid = '" & fu & "' and comid is not null"
            sb.Append("<tr><td>&nbsp;</td></tr>")
            sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl1145" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
            sb.Append("<tr><td colspan=""3""><table><tr>")
            dr = tasks.GetRdrData(sql)
            While dr.Read
                If cocnt <> 0 Then
                    If tdcnt2 <= 2 Then
                        sb.Append("<td width=""50%""><img id=""" + dr.Item("pic_id").ToString + """ border=""0"" src=""" + dr.Item("picurltm").ToString + """></td>")
                        cocnt = cocnt - 1
                        tdcnt3 = tdcnt3 + 1
                    Else
                        sb.Append("</tr><tr>")
                        sb.Append("<td width=""50%""><img id=""" + dr.Item("pic_id").ToString + """ border=""0"" src=""" + dr.Item("picurltm").ToString + """></td>")
                        tdcnt3 = 2
                        cocnt = cocnt - 1
                    End If
                End If
            End While
            dr.Close()
            sb.Append("</tr></table></td></tr>")
        End If

        sb.Append("<tr><td>&nbsp;</td></tr>")
        sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl1146" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
        Dim partarr1 As String() = Split(parts, ";")
        Dim p1 As Integer
        For p1 = 0 To partarr1.Length - 1
            If Len(partarr1(p1)) = 0 Or partarr1(p1) = "none" Then
                Dim test As String = partarr1(p1)
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & tmod.getlbl("cdlbl1147" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
            Else
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & partarr1(p1) & "</td></tr>")
            End If
        Next

        sb.Append("<tr><td>&nbsp;</td></tr>")
        sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl1148" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
        Dim toolarr1 As String() = Split(tools, ";")
        Dim t1 As Integer
        For t1 = 0 To toolarr1.Length - 1
            If Len(toolarr1(t1)) = 0 Or toolarr1(t1) = "none" Then
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & tmod.getlbl("cdlbl1149" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
            Else
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & toolarr1(t1) & "</td></tr>")
            End If
        Next

        sb.Append("<tr><td>&nbsp;</td></tr>")
        sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl1150" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
        Dim lubearr1 As String() = Split(lubes, ";")
        Dim l1 As Integer
        For l1 = 0 To lubearr1.Length - 1
            If Len(lubearr1(l1)) = 0 Or lubearr1(l1) = "none" Then
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & tmod.getlbl("cdlbl1151" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
            Else
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & lubearr1(l1) & "</td></tr>")
            End If

        Next

        sb.Append("</Table>")
        If mode = "1" Then
            tdsall.InnerHtml = sb.ToString
        Else
            tdspdmall.InnerHtml = sb.ToString
        End If

    End Sub
    
    Private Sub PopEqVal(ByVal eqid As String)

        Dim ocraft_min, rcraft_min, lab_min As String
        Dim ocraft_hrs, rcraft_hrs, lab_sav, lab_per As String
        Dim ordt_min, rrdt_min, rdt_min As String
        Dim ordt_hrs, rrdt_hrs, rdt_sav As String
        Dim omat, rmat, mat_sav As String

        sql = "usp_optValAnlEq4 '" & eqid & "'"
        dr = tasks.GetRdrData(sql)
        While dr.Read
            ocraft_min = dr.Item("origttime_min_yr").ToString
            rcraft_min = dr.Item("ttime_min_yr").ToString
            lab_min = dr.Item("lab_min_yr").ToString 'tot min

            ocraft_hrs = dr.Item("origttime_hrs_yr").ToString
            rcraft_hrs = dr.Item("ttime_hrs_yr").ToString
            lab_sav = dr.Item("lab_sav").ToString 'hours saved

            ordt_min = dr.Item("origrdt_min_yr").ToString
            rrdt_min = dr.Item("rdt_min_yr").ToString
            rdt_min = dr.Item("rdt_min_yr").ToString 'tot min

            ordt_hrs = dr.Item("origttime_hrs_yr").ToString
            rrdt_hrs = dr.Item("ttime_hrs_yr").ToString
            rdt_sav = dr.Item("rdt_sav").ToString 'hours saved

            omat = dr.Item("origmat_yr").ToString
            rmat = dr.Item("mat_yr").ToString
            mat_sav = dr.Item("mat_sav").ToString
        End While
        dr.Close()
        Dim eqnum, eqdesc As String
        sql = "select eqnum, eqdesc from equipment where eqid = '" & eqid & "'"
        dr = tasks.GetRdrData(sql)
        While dr.Read
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
        End While
        dr.Close()

        Dim otasks, odown, otime, rtasks, rdown, rtime As String
        sql = "usp_optValAnlEq '" & eqid & "'"
        dr = tasks.GetRdrData(sql)
        Dim o, t, n As Integer
        While dr.Read
            o = dr.Item("otasks").ToString
            t = dr.Item("rtasks").ToString
            n = t - o
            otasks = dr.Item("otasks").ToString
            odown = dr.Item("odown time").ToString
            otime = dr.Item("ocraft time").ToString
            rtasks = dr.Item("rtasks").ToString
            rdown = dr.Item("down time").ToString
            rtime = dr.Item("rcraft time").ToString
        End While
        dr.Close()

        Dim ot, rt, dt As Integer
        Dim sot(100) As String
        Dim sos(100) As String
        Dim srt(100) As String
        Dim sdiff(100) As String
        Dim arr As Integer = -1
        ot = 0
        rt = 0
        dt = 0
        sql = "usp_optValAnlEq2 '" & eqid & "'"
        dr = tasks.GetRdrData(sql)

        While dr.Read
            arr = arr + 1
            ot = ot + dr.Item("OT").ToString
            rt = rt + dr.Item("RT").ToString
            dt = dt + dr.Item("diff").ToString
            sot(arr) = dr.Item("OT").ToString
            sos(arr) = dr.Item("OS").ToString
            srt(arr) = dr.Item("RT").ToString
            sdiff(arr) = dr.Item("diff").ToString
        End While
        dr.Close()



        Dim oparts, parts, partsd, otools, tools, toolsd, olubes, lubes, lubesd As String
        sql = "usp_optValAnlEq3 '" & eqid & "'"
        dr = tasks.GetRdrData(sql)
        While dr.Read
            oparts = dr.Item("oparts").ToString
            parts = dr.Item("parts").ToString
            partsd = dr.Item("partsdiff").ToString
            otools = dr.Item("otools").ToString
            tools = dr.Item("tools").ToString
            toolsd = dr.Item("toolsdiff").ToString
            olubes = dr.Item("olubes").ToString
            lubes = dr.Item("lubes").ToString
            lubesd = dr.Item("lubesdiff").ToString
        End While
        dr.Close()

        Dim sb As New System.Text.StringBuilder

        sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""680"" style=""page-break-after:always;"">")
        sb.Append("<tr><td class=""label16"" colspan=""3"" align=""center"">" & tmod.getlbl("cdlbl1152" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<tr><td width=""430""></td><td width=""10"">&nbsp;</td><td width=""240""></td></tr>")

        sb.Append("<tr><td colspan=""3""><table><tr><td class=""label14"" width=""80px"">" & tmod.getlbl("cdlbl1153" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""label14"" width=""120px"">" & eqnum & "</td>")
        sb.Append("<td class=""label14"" width=""480px"">" & eqdesc & "</td></tr>")
        sb.Append("<tr><td colspan=""3""><hr size=""2""></td></tr></table></td></tr>")

        sb.Append("<tr><td>")

        sb.Append("<table class=""blackborder""  cellSpacing=""0"" cellPadding=""3"" width=""430px"">")
        sb.Append("<tr><td width=""120px""></td>")
        sb.Append("<td width=""70px""></td>")
        sb.Append("<td width=""70px""></td>")
        sb.Append("<td width=""80px""></td>")
        sb.Append("<td width=""90px""></td></tr>")

        sb.Append("<tr bgcolor=""#eeeded""><td class=""label""><tr class=""label"">")
        sb.Append("<td class=""blackborder"" bgcolor=""#eeeded"">PM</td>")
        sb.Append("<td class=""blackborder"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1155" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""blackborder"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1156" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""blackborder"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1157" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""blackborder"" bgcolor=""#eeeded"">Req'd Mins</td></tr>")

        sb.Append("<tr><td class=""label blackborder"">" & tmod.getlbl("cdlbl1159" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & otasks & "</td>")
        sb.Append("<td class=""plainlabel blackborder"">&nbsp;</td>")
        sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & odown & "</td>")
        sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & otime & "</td></tr>")
        sb.Append("<tr><td class=""label blackborder"">" & tmod.getlbl("cdlbl1160" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & rtasks & "</td>")
        sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & n & "</td>")
        sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & rdown & "</td>")
        sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & rtime & "</td></tr>")

        sb.Append("</table>") '</td></tr>
        sb.Append("<br>")

        'sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>")<tr><td>
        sb.Append("<table  class=""blackborder"" cellSpacing=""0"" cellPadding=""3"" width=""430px"">")
        sb.Append("<tr><td width=""120px""></td>")
        sb.Append("<td width=""100px""></td>")
        sb.Append("<td width=""110px""></td>")
        sb.Append("<td width=""100px""></td></tr>")

        sb.Append("<tr><td class=""label""><tr class=""label"" bgcolor=""#eeeded"">")
        sb.Append("<td class=""blackborder"">" & tmod.getlbl("cdlbl1162" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""blackborder"">" & tmod.getlbl("cdlbl1163" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""blackborder"">" & tmod.getlbl("cdlbl1164" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""blackborder"">" & tmod.getlbl("cdlbl1165" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")

        Try
            Dim i As Integer
            Dim tst As String
            For i = 0 To arr

                sb.Append("<td class=""plainlabel blackborder"" >" & sos(i) & "</td>")
                sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & sot(i) & "</td>")
                sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & srt(i) & "</td>")
                sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & sdiff(i) & "</td></tr>")
            Next
        Catch ex As Exception

        End Try

        sb.Append("<tr bgcolor=""#eeeded""><td class=""label""><tr class=""label"">")
        sb.Append("<td class=""label blackborder"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1166" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""label blackborder"" bgcolor=""#eeeded"" align=""center"">" & ot & "</td>")
        sb.Append("<td class=""label blackborder"" bgcolor=""#eeeded"" align=""center"">" & rt & "</td>")
        sb.Append("<td class=""label blackborder"" bgcolor=""#eeeded"" align=""center"">" & dt & "</td></tr>")

        sb.Append("<tr bgcolor=""#eeeded""><td class=""label""><tr class=""label"">")
        sb.Append("<td class=""label blackborder"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1167" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""label blackborder"" bgcolor=""#eeeded"" align=""center"">" & ocraft_min & "</td>")
        sb.Append("<td class=""label blackborder"" bgcolor=""#eeeded"" align=""center"">" & rcraft_min & "</td>")
        sb.Append("<td class=""label blackborder"" bgcolor=""#eeeded"" align=""center"">" & ocraft_min - rcraft_min & "</td></tr>")

        sb.Append("<tr bgcolor=""#eeeded""><td class=""label""><tr class=""label"">")
        sb.Append("<td class=""label blackborder"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1168" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""label blackborder"" bgcolor=""#eeeded"" align=""center"">" & ocraft_hrs & "</td>")
        sb.Append("<td class=""label blackborder"" bgcolor=""#eeeded"" align=""center"">" & rcraft_hrs & "</td>")
        sb.Append("<td class=""label blackborder"" bgcolor=""#eeeded"" align=""center"">" & lab_sav & "</td></tr>")

        sb.Append("</table>") '</td></tr>
        sb.Append("<br>")

        'sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>")<tr><td>
        sb.Append("<table  class=""blackborder"" cellSpacing=""0"" cellPadding=""3"" width=""430px"">")
        sb.Append("<tr><td width=""115px""></td>")
        sb.Append("<td width=""90px""></td>")
        sb.Append("<td width=""90px""></td>")
        sb.Append("<td width=""130px""></td></tr>")

        sb.Append("<tr><td class=""label""><tr class=""label"">")
        sb.Append("<td class=""blackborder"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1170" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""blackborder"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1171" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""blackborder"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1172" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""blackborder"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1173" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")

        sb.Append("<tr><td class=""plainlabel blackborder"" >" & tmod.getlbl("cdlbl1174" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & oparts & "</td>")
        sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & parts & "</td>")
        sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & partsd & "</td></tr>")

        sb.Append("<tr><td class=""plainlabel blackborder"">" & tmod.getlbl("cdlbl1175" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & otools & "</td>")
        sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & tools & "</td>")
        sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & toolsd & "</td></tr>")

        sb.Append("<tr><td class=""plainlabel blackborder"">" & tmod.getlbl("cdlbl1176" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & olubes & "</td>")
        sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & lubes & "</td>")
        sb.Append("<td class=""plainlabel blackborder"" align=""center"">" & lubesd & "</td></tr>")


        sb.Append("<tr bgcolor=""#eeeded"">") '<td class=""label""><tr class=""label"">
        sb.Append("<td class=""label blackborder"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1177" , "PMAcceptancePkg.aspx.vb") & "</td>")
        sb.Append("<td class=""label blackborder"" bgcolor=""#eeeded"" align=""center"">$" & omat & "</td>")
        sb.Append("<td class=""label blackborder"" bgcolor=""#eeeded"" align=""center"">$" & rmat & "</td>")
        sb.Append("<td class=""label blackborder"" bgcolor=""#eeeded"" align=""center"">$" & mat_sav & "</td></tr>")

        sb.Append("</table>") '</td></tr>
        sb.Append("</td><td></td>")

        'tdval.InnerHtml = sb.ToString

        'Dim sb1 As New System.Text.StringBuilder

        sb.Append("<td valign=""top""><table width=""240"" class=""blackborder"" cellSpacing=""0"" cellPadding=""3"">")
        sb.Append("<tr><td colspan=""2"" class=""bluelabel blackborder"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1178" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td class=""label14 blackborder"" align=""center"" width=""100"">" & lab_sav & "</td>")
        sb.Append("<td class=""label blackborder"" align=""center"" width=""140"">" & tmod.getlbl("cdlbl1179" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")

        Try
            lab_per = System.Math.Round(((ocraft_hrs - rcraft_hrs) / ocraft_hrs) * 100)
        Catch ex As Exception
            lab_per = "0%"
        End Try
        sb.Append("<tr><td class=""label14 blackborder"" align=""center"">" & lab_per & "%</td>")
        sb.Append("<td class=""label blackborder""  align=""center"" >" & tmod.getlbl("cdlbl1180" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")

        sb.Append("<tr><td colspan=""2"" class=""bluelabel blackborder"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1181" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td class=""label14 blackborder"" align=""center"">$" & mat_sav & "</td>")
        sb.Append("<td class=""label blackborder"" align=""center"">" & tmod.getlbl("cdlbl1182" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")

        sb.Append("<tr><td colspan=""2"" class=""bluelabel blackborder"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl1183" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td class=""label14 blackborder"" align=""center"">" & rdt_sav & "</td>")
        sb.Append("<td class=""label blackborder"" align=""center"" >" & tmod.getlbl("cdlbl1184" , "PMAcceptancePkg.aspx.vb") & "</td></tr>")



        sb.Append("</table></td></tr>")

        sb.Append("</Table>")
        tdeqlev.InnerHtml = sb.ToString

        'Response.Write(sb)

    End Sub
End Class

