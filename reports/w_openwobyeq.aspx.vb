﻿Imports System.Data.SqlClient
Imports System.Text
Public Class w_openwobyeq
    Inherits System.Web.UI.Page
    Dim wrep As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim sdate, loctype, locid, did, cellid, dept, sid, fdate As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString '"23" '
            loctype = Request.QueryString("loctype").ToString '"loc" '
            locid = Request.QueryString("locid").ToString '"13792" '
            did = Request.QueryString("did").ToString '"0" '
            cellid = Request.QueryString("cellid").ToString '"0" '
            dept = Request.QueryString("dept").ToString '"0" '
            wrep.Open()
            getreport()
            wrep.Dispose()
        End If
    End Sub
    Private Sub getreport()
        Dim sb As New StringBuilder
        'usp_gettotaldown3(@who varchar(10), @sid int, @sdate datetime, @did int, @clid int, @locid int, @dept varchar(50))
        sql = "usp_openwobyeq '" & loctype & "','" & sid & "','" & did & "','" & cellid & "','" & locid & "','" & dept & "'"
        Dim test As String = sql
        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_openwobyeq @who, @sid, @did, @clid, @locid, @dept"

        Dim param01 = New SqlParameter("@who", SqlDbType.VarChar)
        If loctype = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = loctype
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@sid", SqlDbType.VarChar)
        If sid = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = sid
        End If
        cmd.Parameters.Add(param02)
       
        Dim param04 = New SqlParameter("@did", SqlDbType.VarChar)
        If did = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = did
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@clid", SqlDbType.VarChar)
        If cellid = "" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = cellid
        End If
        cmd.Parameters.Add(param05)
        Dim param06 = New SqlParameter("@locid", SqlDbType.VarChar)
        If locid = "" Then
            param06.Value = System.DBNull.Value
        Else
            param06.Value = locid
        End If
        cmd.Parameters.Add(param06)
        Dim param07 = New SqlParameter("@dept", SqlDbType.VarChar)
        If dept = "" Then
            param07.Value = System.DBNull.Value
        Else
            param07.Value = dept
        End If
        cmd.Parameters.Add(param07)

        sb.Append("<table cellspacing=""0"" cellpadding=""4"" width=""700"">")

        'sb.Append("<tr>")
        'sb.Append("<td width=""162""></td>")
        'sb.Append("<td width=""162""></td>")
        'sb.Append("<td width=""162""></td>")
        'sb.Append("<td width=""162""></td>")
        'sb.Append("<td width=""52""></td>")
        'sb.Append("</tr>")

        sb.Append("<tr>")
        sb.Append("<td colspan=""5"" align=""center"" class=""replabeltop"">Open Work Order by Equipment Report</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""5"" align=""center"" class=""plainlabel"">" & Now & "</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""5"" align=""center"">&nbsp;</td>")
        sb.Append("</tr>")

        Dim hdflg As Integer = 1
        hdflg = 0
        Dim loc, desc, eq, eqdesc, tt, wo, wodesc As String

        Dim lochold As String = "0"

        dr = wrep.GetRdrDataHack(cmd)

        While dr.Read
            loc = dr.Item("Location").ToString
            desc = dr.Item("Location_Description").ToString
            eq = dr.Item("Equipment").ToString
            eqdesc = dr.Item("Description").ToString
            tt = dr.Item("Required").ToString

            wo = dr.Item("WO#").ToString
            wodesc = dr.Item("WO_Description").ToString

            If hdflg = 0 Then

                hdflg = 1

                If loctype = "dept" Or loctype = "cell" Then
                    sb.Append("<tr>")
                    sb.Append("<td align=""left"" class=""label"">Department:</td>")
                    sb.Append("<td align=""left"" class=""plainlabel"" colspan=""4"">" & loc & " - " & desc & "</td>")
                    sb.Append("</tr>")
                ElseIf loctype = "loc" Then
                    sb.Append("<tr>")
                    sb.Append("<td align=""left"" class=""label"">Location:</td>")
                    sb.Append("<td align=""left"" class=""plainlabel"" colspan=""4"">" & loc & " - " & desc & "</td>")
                    sb.Append("</tr>")
                ElseIf loctype = "eq23" Then
                    sb.Append("<tr>")
                    sb.Append("<td align=""left"" class=""label"">Department:</td>")
                    sb.Append("<td align=""left"" class=""plainlabel"" colspan=""4"">" & loc & "</td>")
                    sb.Append("</tr>")
                End If


                sb.Append("<tr>")
                sb.Append("<td colspan=""5"" align=""center"">&nbsp;</td>")
                sb.Append("</tr>")

                sb.Append("<tr>")
                sb.Append("<td width=""80"" class=""label""><u>WO#</u></td>")
                sb.Append("<td width=""230"" class=""label""><u>Description</u></td>")
                sb.Append("<td width=""125"" class=""label""><u>Equipment</u></td>")
                sb.Append("<td width=""185"" class=""label""><u>Equipment Description</u></td>")
                sb.Append("<td width=""80"" class=""label""><u>Labor Hours</u></td>")
                sb.Append("</tr>")


            End If

            sb.Append("<tr>")
            sb.Append("<td class=""plainlabel gbox"">" & wo & "</td>")
            sb.Append("<td class=""plainlabel gbox"">" & wodesc & "</td>")
            sb.Append("<td class=""plainlabel gbox"">" & eq & "</td>")
            sb.Append("<td class=""plainlabel gbox"">" & eqdesc & "</td>")
            sb.Append("<td class=""plainlabel gbox"" align=""center"">" & tt & "</td>")
            sb.Append("</tr>")

        End While
        dr.Close()

        If hdflg = 0 Then
            sb.Append("<tr>")
            sb.Append("<td colspan=""5"" align=""center"" class=""redlabel"">No Records Found</td>")
            sb.Append("</tr>")
        End If

        sb.Append("</table>")

        dvrep.InnerHtml = sb.ToString

    End Sub
End Class