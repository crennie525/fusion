﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EQSelectRBAS.aspx.vb" Inherits="lucy_r12.EQSelectRBAS" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report Based Route Equipment Selection Dialog</title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/EqSelectaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
         function DisableButton(b) {
             document.getElementById("btntocomp").className = "details";
             document.getElementById("btnfromcomp").className = "details";
             document.getElementById("todis").className = "view";
             document.getElementById("fromdis").className = "view";
             document.getElementById("form1").submit();
         }
         function handleexit() {
             var str = document.getElementById("lblrteid").value;
             //alert(str)
             if (str != "") {
                 window.parent.handleexit(str);
             }
             else {
                 alert("No Assets Selected!")
             }
         }
     </script>
</head>
<body>
    <form id="form1" runat="server">
    <table width="572">
				<tr>
					<td class="thdrsingrt label" colSpan="2"><asp:Label id="lang3326" runat="server">Select Equipment for Overview</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="2">
						<hr style="BORDER-RIGHT: #0000ff 1px solid; BORDER-TOP: #0000ff 1px solid; BORDER-LEFT: #0000ff 1px solid; BORDER-BOTTOM: #0000ff 1px solid">
					</td>
				</tr>
			</table>
			<table width="572">
				<tr>
					<td class="label" align="center"><asp:Label id="lang3327" runat="server">Available Equipment</asp:Label></td>
					<td></td>
					<td class="label" align="center"><asp:Label id="lang3328" runat="server">Selected Equipment</asp:Label></td>
				</tr>
				<tr>
					<td align="center" width="280"><asp:listbox id="lbfailmaster" runat="server" Width="270px" SelectionMode="Multiple" Height="150px"></asp:listbox></td>
					<td vAlign="middle" align="center" width="22">
						<img src="../images/appbuttons/minibuttons/forwardgraybg.gif" class="details" id="todis"
							width="20" height="20"> <img src="../images/appbuttons/minibuttons/backgraybg.gif" class="details" id="fromdis"
							width="20" height="20">
						<asp:imagebutton id="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton>
						<asp:imagebutton id="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton><IMG id="btnaddtosite" onmouseover="return overlib('Choose Failure Modes for this Plant Site ')"
							onclick="getss();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/plusminus.gif" width="20" height="20" class="details"></td>
					<td align="center" width="280"><asp:listbox id="lbfailmodes" runat="server" Width="270px" SelectionMode="Multiple" Height="150px"></asp:listbox></td>
				</tr>
				<tr>
					<td align="right" colSpan="3"><IMG onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/submit.gif" id="ibtnret"
							runat="server" width="69" height="19"></td>
				</tr>
				<tr>
					<td class="bluelabel" colSpan="3"><asp:Label id="lang3329" runat="server">List Box Options</asp:Label></td>
				</tr>
				<tr>
					<td class="label" colSpan="3"><asp:radiobuttonlist id="cbopts" runat="server" Width="400px" AutoPostBack="True" CssClass="labellt">
							<asp:ListItem Value="0" Selected="True" id="lims" runat="server">Multi-Select (use arrows to move single or multiple selections)</asp:ListItem>
							<asp:ListItem Value="1" id="lico" runat="server">Click-Once (move selected item by clicking that item)</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td class="note" colSpan="3"><asp:Label id="lang3330" runat="server">* Please note that the Multi-Select Mode must be used to remove an item from the Available Equipment list if only one item is present.</asp:Label></td>
				</tr>
			</table>
			
			<input type="hidden" id="lbleqstr" runat="server" />
            <input type="hidden" id="lblrteid" runat="server" />
		<input type="hidden" id="lblopt" runat="server" />
        <input type="hidden" id="lblsid" runat="server" />
            <input type="hidden" id="lblskillid" runat="server" />
            <input type="hidden" id ="lblqty" runat="server" />
            <input type="hidden" id="lblfreq" runat="server" />
            <input type="hidden" id="lblrdid" runat="server" />
            <input type="hidden" id="lblrte" runat="server" />
            <input type="hidden" id="lblrttyp" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblptid" runat="server" />
<input type="hidden" id="lbldays" runat="server" />
<input type="hidden" id="lblshift" runat="server" />

    </form>
</body>
</html>
