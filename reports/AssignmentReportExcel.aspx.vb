﻿
Imports System.Data.SqlClient
Public Class AssignmentReportExcel
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim sqa As New Utilities
    Dim sessid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sessid = Request.QueryString("eqid").ToString
            sqa.Open()
            ExportDG(sessid)
            sqa.Dispose()

        End If
    End Sub
    Private Sub ExportDG(ByVal sessid As String)
        BindExport(sessid)
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
    Private Sub BindExport(ByVal sessid As String)
        Dim osql As String
        sql = "usp_GetAssignmentReport_new2 '" & sessid & "'"
        osql = "usp_GetAssignmentReport_excel '" & sessid & "'"
        Dim ds As New DataSet
        ds = sqa.GetDSData(osql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
    End Sub
End Class