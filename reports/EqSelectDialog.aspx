<%@ Page Language="vb" AutoEventWireup="false" Codebehind="EqSelectDialog.aspx.vb" Inherits="lucy_r12.EqSelectDialog" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Equipment Select Dialog</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<script language="JavaScript" src="../scripts1/EqSelectDialogaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
         function handleentry() {
             var log = document.getElementById("lbllog").value;
             if (log == "no") {
                 window.returnValue = "log";
                 window.close();
             }
             else if (log == "noeqid") {
                 window.returnValue = "no";
                 window.close();
             }
             else {
                 var sid = document.getElementById("lblsid").value;
                 var who = document.getElementById("lblwho").value;
                 document.getElementById("iff").src = "EqSelect.aspx?sid=" + sid + "&who=" + who + "&date=" + Date();
                 //window.close();
             }
         }
         function handleexit(str) {
             //window.returnValue = "1";
             var sid = document.getElementById("lblsid").value;
             var coi = document.getElementById("lblcoi").value;
             var who = document.getElementById("lblwho").value;
             var rid = document.getElementById("lblrid").value;
             if (who == "rep" || who == "rrep") {
                 //window.open("pmrselectdialog.aspx?eqstr=" + str + "&date=" + Date());
                 if (who == "rep") {
                     window.showModalDialog("pmrselectdialog.aspx?who=" + who + "&eqstr=" + str + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                 }
                 else {
                     var eReturn = window.showModalDialog("pmrselectdialog.aspx?who=" + who + "&eqstr=" + str + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                     if (eReturn) {
                         //alert(eReturn)
                         //window.parent.handlepmrepret(eReturn);
                         var ret = eReturn + "," + rid;
                         window.returnValue = ret;
                         ;
                     }
                 }
             }
             else {
                 if (coi == "BRI") {
                     window.open("OVRollup_BRI.aspx?typ=select&sid=" + sid + "&eqstr=" + str + "&date=" + Date());
                 }
                 else {
                     window.open("OVRollup.aspx?typ=select&sid=" + sid + "&eqstr=" + str + "&date=" + Date());
                 }
             }
             
             
             //was valu4
             window.close();
         }
     </script>
	</HEAD>
	<body  class="tbg" onload="handleentry();" >
		<form id="form1" method="post" runat="server">
			<iframe id="iff" src="" width="100%" height="100%" frameBorder="no"></iframe><input id="lblsid" type="hidden" runat="server">
			<input type="hidden" id="lbllog" runat="server" NAME="lbllog">
		
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblcoi" runat="server" />
<input type="hidden" id="lblwho" runat="server" />
<input type="hidden" id="lblrid" runat="server" />
</form>
	</body>
</HTML>
