Imports System.Data.SqlClient
Imports System.Text
Public Class tpmall
    Inherits System.Web.UI.Page
    Dim sql, funcid, tasknum, pmid, eqid, freq, rd, typ, ts, wonum, pmhid, fcust, pmtskid, psite As String
    Dim username, userid As String
    Dim news As New Utilities
    Dim comi As New mmenu_utils_a
    Dim dr As SqlDataReader
    Dim dr1 As SqlDataReader
    Dim ap As New AppUtils
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomi As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents div1 As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here


        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        lblcomi.Value = comi
        If comi = "SUN" Then
            lblfslang.Value = "fre"
        End If
        'END ADD FOR SUNCOR
        If Not IsPostBack Then
            ts = "1" 'Request.QueryString("ts").ToString
            tasknum = "1"
            news.Open()
            If ts = "0" Then
                pmid = Request.QueryString("pmid").ToString
                pmhid = Request.QueryString("pmhid").ToString
                wonum = Request.QueryString("wonum").ToString
                GetSTasksCnt(pmid, tasknum)

            Else
                eqid = Request.QueryString("eqid").ToString '"46" '
                freq = "0" 'Request.QueryString("freq").ToString '"7" '
                rd = "0" 'Request.QueryString("rd").ToString '"Running" '
                typ = "0" 'Request.QueryString("typ").ToString '"4" '
                GetOTasksCnt(eqid)
            End If
            news.Dispose()
        End If
    End Sub
    Private Sub GetOTasksCnt(ByVal eqid As String)
        
        Dim tpmrevchar, tpmapprdate, tpmapprby As String
        sql = "select tpmrevchar, Convert(char(10),tpmapprdate,101) as tpmapprdate, tpmapprby from equipment where eqid = '" & eqid & "'"
        dr = news.GetRdrData(sql)
        While dr.Read
            tpmrevchar = dr.Item("tpmrevchar").ToString
            tpmapprdate = dr.Item("tpmapprdate").ToString
            tpmapprby = dr.Item("tpmapprby").ToString
        End While
        dr.Close()
        Dim tsk As String = ""
        Dim funcid, tasknum, pmtskid As String
        sql = "select distinct pmtskid, funcid, tasknum from pmtaskstpm where eqid = '" & eqid & "' " _
        + "and ptid = 0 and rd not in ('Select', '0') and isnull(taskstatus, 'OK') <> 'Delete' " _
        + "and subtask = 0 order by funcid, tasknum"

        'sql = "select count(*) from pmtaskstpm t where t.eqid = '" & eqid & "' " _
        '+ "and isnull(t.taskstatus, 'OK') <> 'Delete' " _
        '+ "and ((t.first is not null or t.second is not null or t.third is not null)) " _
        '+ "and (t.shift1 is not null or t.shift2 is not null or t.shift3 is not null) " _
        '+ "and ((t.freq = 1 or t.freq = 7) or (t.fcust = 1 or t.fcust is null))"

        'sql = "select distinct t.funcid, t.tasknum from pmtaskstpm t where t.eqid = '" & eqid & "' " _
        '+ "and isnull(t.taskstatus, 'OK') <> 'Delete' " _
        '+ "and ((t.first is not null or t.second is not null or t.third is not null)) " _
        '+ "and (t.shift1 is not null or t.shift2 is not null or t.shift3 is not null) " _
        '+ "and ((t.freq = 1 or t.freq = 7) or (t.fcust = 1 or t.fcust is null)) and subtask = 0"

        Dim dslev As DataSet
        dslev = news.GetDSData(sql)
        Dim dt As New DataTable
        dt = dslev.Tables(0)
        Dim i As Integer = 0
        Dim ii As Integer = dt.Rows.Count
        Dim row As DataRow
        For Each row In dt.Rows
            i += 1
            If i = 12 Then
                Dim tcnttst As String = "ok"
            End If
            pmtskid = row("pmtskid").ToString
            sql = "exec usp_delTaskParttpm '" & pmtskid & "'"
            news.Update(sql)
            sql = "exec usp_delTaskTooltpm '" & pmtskid & "'"
            news.Update(sql)
            sql = "exec usp_delTaskLubetpm '" & pmtskid & "'"
            news.Update(sql)

            funcid = row("funcid").ToString
            tasknum = row("tasknum").ToString
            If funcid <> "" Then
                tsk += GetOTasks(eqid, funcid, tasknum, i, tpmrevchar, tpmapprdate, tpmapprby, ii)
            End If

        Next
        If i <> 0 Then
            div1.InnerHtml = tsk
        Else
            div1.InnerHtml = "No Records Found"
        End If

    End Sub
    Private Function GetOTasks(ByVal eqid As String, ByVal funcid As String, ByVal tasknum As String, ByVal i As Integer, ByVal tpmrevchar As String, ByVal tpmapprdate As String, ByVal tpmapprby As String, ByVal ii As Integer) As String
        Dim comi As String = lblcomi.Value
        Dim lang As String = lblfslang.Value
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        Dim ThisPage1 As String = Request.ServerVariables("HTTP_HOST")
        Dim ns1i As Integer = nsimage.IndexOf("//")
        Dim nsstr As String = Mid(nsimage, ns1i + 3)
        Dim ns2i As Integer = nsstr.IndexOf("/")
        nsstr = Mid(nsstr, 1, ns2i)
        Dim hostflag As Integer = 0
        If nsstr <> ThisPage1 Then
            nsimage = nsimage.Replace(nsstr, ThisPage1)
            hostflag = 1
            'lblhostflag.Value = "1"
            'lblhost.Value = nsstr

        Else
            'lblhostflag.Value = "0"
        End If

        sql = "select subtask, taskdesc from pmtaskstpm where funcid = '" & funcid & "' and tasknum = '" & tasknum & "' " _
        + "and subtask <> 0 order by subtask"
        Dim dslev As DataSet
        dslev = news.GetDSData(sql)
        Dim dt As New DataTable
        dt = dslev.Tables(0)
        sql = "usp_getTPMTaskChart '" & eqid & "','" & funcid & "','" & tasknum & "'"
        'sql = "usp_getweekly '" & eqid & "','" & PageNumber & "','" & PageSize & "'"
        Dim skill, task, func, comp, lubes, parts, tools, meas, pmtskid, itask, shift, days, calday, subcnt As String
        Dim shift1, shift2, shift3, first, second, third, ttid, eqnum, ttime, eqdesc, cqty As String
        Dim fm1, fm2, fm3, fm4, fm5, ffm1 As String
        Dim fm1i, fm2i, fm3i, fm4i, fm5i As String
        Dim fm1o, fm2o, fm3o, fm4o, fm5o As String
        Dim m1, m2, m3, m4, m5 As String
        Dim ms1, ms2, ms3, ms4, ms5 As String
        'Dim lang As String = lblfslang.Value
        'lblbpage.Text = "Task " & tasknum & " of " & mtask
        Dim sbt As New StringBuilder
        sbt.Append("<table style=""page-break-after:always;"" border=""0"">")
        'tpmrevchar, tpmapprdate, tpmapprby
        sbt.Append("<tr>")
        sbt.Append("<td colspan=""2"">")
        sbt.Append("<table>")
        sbt.Append("<td class=""plainlabel"" width=""90"">Revision#</td>")
        sbt.Append("<td class=""plainlabel"" width=""180"">" & tpmrevchar & "</td>")
        sbt.Append("<td class=""plainlabel"" width=""90"">Revison Date:</td>")
        sbt.Append("<td class=""plainlabel"" width=""90"">" & tpmapprdate & "</td>")
        sbt.Append("<td class=""plainlabel"" width=""100"">Approved By:</td>")
        sbt.Append("<td class=""plainlabel"" width=""150"">" & tpmapprby & "</td>")
        sbt.Append("</table>")
        sbt.Append("</td>")
        sbt.Append("</tr>")
        Dim shold As Integer = 0
        dr = news.GetRdrData(sql)
        While dr.Read

            pmtskid = dr.Item("pmtskid").ToString
            ttid = dr.Item("ttid").ToString
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            funcid = dr.Item("funcid").ToString
            'lblfuncid.Value = funcid
            skill = dr.Item("freq").ToString
            task = dr.Item("task").ToString
            func = dr.Item("func").ToString
            comp = dr.Item("compnum").ToString
            cqty = dr.Item("cqty").ToString
            parts = dr.Item("parts").ToString
            tools = dr.Item("tools").ToString
            lubes = dr.Item("lubes").ToString
            meas = dr.Item("meas").ToString
            itask = dr.Item("tasknum").ToString
            'lbltasknum1.Value = itask
            ffm1 = dr.Item("ffm1").ToString
            fm1 = dr.Item("fm1").ToString

            fm2 = dr.Item("fm2s").ToString
            fm3 = dr.Item("fm3s").ToString
            fm4 = dr.Item("fm4s").ToString
            fm5 = dr.Item("fm5s").ToString

            fm1i = dr.Item("fm1id").ToString
            fm2i = dr.Item("fm2id").ToString
            fm3i = dr.Item("fm3id").ToString
            fm4i = dr.Item("fm4id").ToString
            fm5i = dr.Item("fm5id").ToString

            m1 = dr.Item("tmdid1").ToString
            m2 = dr.Item("tmdid2").ToString
            m3 = dr.Item("tmdid3").ToString
            m4 = dr.Item("tmdid4").ToString
            m5 = dr.Item("tmdid5").ToString

            ms1 = dr.Item("mstr1").ToString
            ms2 = dr.Item("mstr2").ToString
            ms3 = dr.Item("mstr3").ToString
            ms4 = dr.Item("mstr4").ToString
            ms5 = dr.Item("mstr5").ToString


            subcnt = dr.Item("subcnt").ToString

            shift1 = dr.Item("shift1").ToString
            shift2 = dr.Item("shift2").ToString
            shift3 = dr.Item("shift3").ToString

            first = dr.Item("first").ToString
            second = dr.Item("second").ToString
            third = dr.Item("third").ToString

            ttime = dr.Item("time").ToString

            Dim sbimg As String

            If shold = 0 Then
                shold = 1

                sbt.Append("<tr>")
                sbt.Append("<td width=""280""><IMG src=""../menu/mimages/custcomp.gif""></td>")
                sbt.Append("<td id=""tdtpm"" class=""bigbold14"" vAlign=""top"" width=""420"" rowspan=""2"" runat=""server"" align=""right"">OP - " & skill)
                If shift1 <> "" Then
                    sbt.Append("<br>" & shift1)
                    If first <> "" Then
                        sbt.Append(" - " & first)
                    End If
                End If
                If shift2 <> "" Then
                    sbt.Append("<br>" & shift2)
                    If second <> "" Then
                        sbt.Append(" - " & second)
                    End If
                End If
                If shift3 <> "" Then
                    sbt.Append("<br>" & shift3)
                    If third <> "" Then
                        sbt.Append(" - " & third)
                    End If
                End If
                sbt.Append("</td>")
                sbt.Append("</tr>")

                If cqty = "" Then
                    cqty = "1"
                End If

                sbt.Append("<tr>")
                'sbt.Append("<td><IMG src=""../images/appbuttons/minibuttons/20PX.gif""></td>")
                sbt.Append("<td id=""tdeq"" class=""bigbold14"" align=""right"" runat=""server"" colspan=""2"">" & eqdesc & " - " & eqnum & "</td>")
                sbt.Append("</tr>")
                sbt.Append("<tr>")
                sbt.Append("<td class=""bigbold14"">" & pmtskid & "-" & itask & "</td><td id=""tdeq"" class=""bigbold14"" align=""right"" runat=""server"" colspan=""1"">" & func & " > " & comp & "(" & cqty & ")</td>")
                sbt.Append("</tr>")
                sbt.Append("<tr>")
                sbt.Append("<td colSpan=""3"">")
                sbt.Append("<table width=""700"">")
                sbt.Append("<tr>")
                sbt.Append("<td id=""tdtask"" class=""bigbold14 blackborder"" height=""60"" vAlign=""top"" width=""700"" runat=""server"">")


                If ttid = "2" Then
                    sbimg = "../images/appbuttons/minibuttons/tpmclean.gif"
                ElseIf ttid = "3" Then
                    sbimg = "../images/appbuttons/minibuttons/tpmlube.gif"
                ElseIf ttid = "5" Or ttid = "6" Or ttid = "7" Then
                    sbimg = "../images/appbuttons/minibuttons/tpminsp.gif"
                Else
                    sbimg = "no"
                End If

                sbt.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""690"">")
            End If
            Dim fmval As String
            If (comi = "CAS" Or comi = "AGR" Or comi = "LAU" Or comi = "GLA") And (lang = "FRE" Or lang = "fre") Then
                fmval = ffm1
            Else
                fmval = fm1
            End If
            If Len(task) > 0 Then
                If sbimg <> "no" Then
                    sbt.Append("<tr><td class=""bigbold14""><img src=""" & sbimg & """ align=""left"" hspace=""5"" vspace=""4"">" & task & "<br>OK(___) " & fmval & "</td></tr>")
                Else
                    sbt.Append("<tr><td class=""bigbold14"">" & task & "<br>OK(___) " & fmval & "</td></tr>")
                End If

            Else
                sbt.Append("<tr><td class=""bigbold14"">" & tmod.getxlbl("xlb60", "PMALL.vb") & "</td></tr>")
            End If

            sbt.Append("</table></td>")

            sbt.Append("</tr>")
            sbt.Append("</table>")
            sbt.Append("</td>")
            sbt.Append("</tr>")
            sbt.Append("<tr>")
            sbt.Append("<td colSpan=""3"">")
            sbt.Append("<table width=""700"">")
            sbt.Append("<tr>")

            Dim tpmimg As String
            'sql = "select p.pic_id, p.tpm_image, p.tpm_image_thumb, p.tpm_image_med, p.tpm_image_order, " _
            '   + "p.tpm_image_title, f.func " _
            '   + "from tpmimages p " _
            '   + "left join functions f on f.func_id = p.funcid where p.funcid = '" & funcid & "' and p.tasknum = '" & tasknum & "' order by p.tpm_image_order"
            'dr1 = news.GetRdrData(sql)
            'While dr1.Read
            tpmimg = dr.Item("tpm_image_med").ToString
            If hostflag = 1 Then
                'img = img.Replace(nsstr, ThisPage1)
                'bimg = bimg.Replace(nsstr, ThisPage1)
                'timg = timg.Replace(nsstr, ThisPage1)
                tpmimg = tpmimg.Replace(nsstr, ThisPage1)
            End If
            'End While
            'dr1.Close()
            Dim imgtask As String
            If tpmimg <> "" Then
                imgtask = tpmimg
            Else
                imgtask = "../images/appimages/tpmimg1.gif"
            End If

            sbt.Append("<td class=""blackborder"" height=""350"" vAlign=""middle"" rowSpan=""3"" width=""350"" align=""center"">")
            sbt.Append("<IMG id=""imgtask"" src=""" & imgtask & """ width=""340"" height=""340"" runat=""server"">")
            sbt.Append("</td>")

            sbt.Append("<td rowSpan=""3"" width=""5"">&nbsp;</td>")

            'ppe
            'sbt.Append("<td id=""tdppe"" class=""blackborder"" vAlign=""top"" width=""345"" align=""left"" runat=""server"">")
            'Dim ippe As String = "../images/appbuttons/minibuttons/tpmppe.jpg"
            'sbt.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""340"">")
            'If lang = "spa" Then
            'sbt.Append("<tr><td class=""plainlabel14""><img src=""" & ippe & """ align=""left"" hspace=""5"" vspace=""4"" ></td><td class=""plainlabel14"" valign=""middle"">No requiere protección especial de seguridad</td></tr>")

            'Else
            'sbt.Append("<tr><td class=""plainlabel14""><img src=""" & ippe & """ align=""left"" hspace=""5"" vspace=""4"" ></td><td class=""plainlabel14"" valign=""middle"">No Personal Protection Equipment Information Provided</td></tr>")
            'End If
            'sbt.Append("</table>")
            'sbt.Append("</td>")
            'sbt.Append("</tr>")

            Dim itool As String = "../images/appbuttons/minibuttons/tpmtool.jpg"

            'tools'
            sbt.Append("<tr>")
            sbt.Append("<td id=""tdtools"" class=""blackborder"" vAlign=""top"" width=""345""  align=""left"" runat=""server"">")

            sbt.Append("<table border=""0"">")
            sbt.Append("<tr>")
            sbt.Append("<td rowspan=""3"" valign=""top""><img src=""" & itool & """ align=""left"" hspace=""5"" vspace=""4"" ></td>")

            If lubes <> "none" Then
                'sbt.Append("<tr>")
                sbt.Append("<td colspan=""1"" class=""plainlabel14"" valign=""top""><b>" & tmod.getxlbl("xlb61", "PMALL.vb") & "</b><br>")
                'sbt.Append("<tr><td class=""bigplainblue14"">")
                Dim lubearr As String() = Split(lubes, ";")
                Dim l As Integer
                For l = 0 To lubearr.Length - 1
                    If lubearr(l) = "none" Then 'Len(lubearr(l)) = 0 Or 
                        sbt.Append(tmod.getxlbl("xlb28", "PGEMSHTM2.vb") & "<br>")
                    Else
                        sbt.Append(lubearr(l) & "<br>")
                    End If

                Next
                sbt.Append("</td></tr>")
            Else
                sbt.Append("<td colspan=""1"" class=""plainlabel14""><b>" & tmod.getxlbl("xlb61", "PMALL.vb") & "</b><br>" & tmod.getxlbl("xlb22", "PGEMSHTM.vb") & "</td></tr>")

            End If
            If tools <> "none" Then

                sbt.Append("<tr>")
                'sbt.Append("<td></td>")
                sbt.Append("<td colspan=""1"" class=""plainlabel14"" valign=""top""><b>" & tmod.getxlbl("xlb62", "PMALL.vb") & "</b><br>")
                'sbt.Append("<td class=""bigplainblue14"">")
                Dim toolarr As String() = Split(tools, ";")
                Dim t As Integer
                For t = 0 To toolarr.Length - 1
                    If toolarr(t) = "none" Then 'Len(toolarr(t)) = 0 Or 
                        sbt.Append(tmod.getxlbl("xlb20", "PGEMSHTM.vb") & "<br>")
                    Else
                        sbt.Append(toolarr(t) & "<br>")
                    End If
                Next
                sbt.Append("</td></tr>")
                'If toolarr.Length - 1 = 0 Then
                'sbt.Append("<br>")
                'End If
            Else
                sbt.Append("<tr>")
                'sbt.Append("<td></td>")
                sbt.Append("<td colspan=""1"" class=""plainlabel14""><b>" & tmod.getxlbl("xlb62", "PMALL.vb") & "</b><br>" & tmod.getxlbl("xlb20", "PGEMSHTM.vb") & "</td></tr>")
            End If
            If parts <> "none" Then
                sbt.Append("<tr>")
                'sbt.Append("<td></td>")
                sbt.Append("<td colspan=""1"" class=""plainlabel14"" valign=""top""><b>" & tmod.getxlbl("xlb63", "PMALL.vb") & "</b><br>")
                'sbt.Append("<td colspan=""1"" class=""bigplain14""><b>Parts:</b></td><td class=""bigplainblue14"">" & parts)
                Dim partarr As String() = Split(parts, ";")
                Dim p As Integer
                For p = 0 To partarr.Length - 1
                    If partarr(p) = "none" Then 'Len(partarr(p)) = 0 Or 
                        sbt.Append(tmod.getxlbl("xlb32", "PGEMSHTM2.vb") & "<br>")
                    Else
                        sbt.Append(partarr(p) & "<br>")
                    End If
                Next

            Else
                sbt.Append("<tr>")
                'sbt.Append("<td></td>")
                sbt.Append("<td colspan=""1"" class=""plainlabel14""><b>" & tmod.getxlbl("xlb63", "PMALL.vb") & "</b><br>" & tmod.getxlbl("xlb32", "PGEMSHTM2.vb") & "</td></tr>")
            End If

            sbt.Append("</table>")

            sbt.Append("</td>")
            sbt.Append("</tr>")

            'time
            sbt.Append("<tr>")
            sbt.Append("<td id=""tdtime"" class=""blackborder"" vAlign=""top"" width=""345"" align=""left"" runat=""server"">")
            Dim isw As String = "../images/appbuttons/minibuttons/tpmsw.jpg"
            sbt.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""340"">")
            sbt.Append("<tr><td><img src=""" & isw & """ align=""left"" hspace=""5"" vspace=""4"" ></td><td class=""bigbold14"" valign=""middle"">" & ttime & " min.</td></tr>")
            sbt.Append("</table>")
            sbt.Append("</td>")

            sbt.Append("</tr>")
            sbt.Append("<tr>")
            sbt.Append("<td id=""tdtasksteps"" class=""blackborder"" height=""360"" vAlign=""top"" width=""350"" runat=""server"">")

            sbt.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""340"">")
            sbt.Append("<tr><td width=""20""></td><td width=""320""></td></tr>")
            If lang = "spa" Then
                sbt.Append("<tr><td class=""bigbold14 stasklft"">" & tmod.getxlbl("lang3360a", "reports2.aspx") & "</td><td class=""bigbold14 staskrt"">Detalles de inspección</td></tr>")
            Else
                sbt.Append("<tr><td class=""bigbold14 stasklft"">" & tmod.getxlbl("lang3360a", "reports2.aspx") & "</td><td class=""bigbold14 staskrt"">Inspection Details </td></tr>")
            End If


            'sql = "usp_getWITotalTPMST '" & eqid & "','" & funcid & "','" & itask & "'"
            Dim subtask, subtasknum As String
            'dr = news.GetRdrData(sql)
            'While dr.Read
            'Dim i As Integer
            Dim row As DataRow
            For Each row In dt.Rows
                subtasknum = row("subtask").ToString
                subtask = row("taskdesc").ToString
                sbt.Append("<tr><td class=""bigbold14 stasklfts"">" & subtasknum & "</td><td class=""plainlabel14"">" & subtask & "</td></tr>")
            Next
            'subtasknum = dr.Item("subtask").ToString
            'If subtasknum <> "0" Then
            'subtask = dr.Item("subt").ToString

            'End If
            'End While
            'dr.Close()

            sbt.Append("</table>")

            sbt.Append("</td>")
            sbt.Append("<td rowSpan=""3"" width=""5"">&nbsp;</td>")
            sbt.Append("<td id=""tdcomments"" class=""blackborder"" height=""360"" vAlign=""top"" width=""350"" runat=""server"">")

            sbt.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""340"">")
            sbt.Append("<tr><td width=""340""></td></tr>")
            sbt.Append("<tr><td class=""bigbold14 staskrt"">" & tmod.getxlbl("lang3360b", "reports2.aspx") & "</td></tr>")
            sbt.Append("</table>")
        End While
        dr.Close()






        sbt.Append("</td>")
        sbt.Append("</tr>")
        sbt.Append("</table>")
        sbt.Append("</td>")
        sbt.Append("</tr>")

        sbt.Append("<tr><td colspan=""2"" align=""right"" class=""plainlabel"">Page#&nbsp;&nbsp;" & i & "&nbsp;&nbsp;of&nbsp;&nbsp;" & ii & "</td></tr>")
        sbt.Append("</table>")

        Dim ret As String = sbt.ToString
        Return ret
    End Function
    Private Sub GetSTasksCnt(ByVal pmid As String, ByVal tasknum As String)
        Dim mtask As Integer
        Dim tsk As String = ""
        sql = "select count(*) from pmtracktpm where pmid = '" & pmid & "' and subtask = 0"
        mtask = news.Scalar(sql)
        Dim i As Integer
        For i = 1 To mtask
            tsk += GetSTasks(pmid, tasknum)
        Next
        div1.InnerHtml = tsk
    End Sub
    Private Function GetSTasks(ByVal pmid As String, ByVal tasknum As String) As String
        sql = "usp_getpmmantpmperf '" & pmid & "','" & tasknum & "','1'"
        Dim skill, task, func, comp, lubes, parts, tools, meas, pmtskid, itask, shift, days, calday, subcnt, pmtid, taskcomp As String
        Dim rdt, actt, rd, tt, hrs, mins, aps, hre, mine, ape, hrsdt, minsdt, apsdt, hredt, minedt, apedt, ttid, eqnum, tocomp As String
        Dim nextdate, ttime As String
        Dim fm1, fm2, fm3, fm4, fm5 As String
        Dim fm1i, fm2i, fm3i, fm4i, fm5i As String
        Dim fm1o, fm2o, fm3o, fm4o, fm5o As String
        Dim m1, m2, m3, m4, m5 As String
        Dim ms1, ms2, ms3, ms4, ms5 As String
        Dim lang As String = lblfslang.Value

        dr = news.GetRdrData(sql)
        While dr.Read
            tocomp = dr.Item("tocomp").ToString
            taskcomp = dr.Item("taskcomp").ToString
            fcust = dr.Item("fcust").ToString
            nextdate = dr.Item("nextdate").ToString

            rd = dr.Item("rd").ToString
            rdt = dr.Item("actrd").ToString
            tt = dr.Item("ttime").ToString
            actt = dr.Item("acttime").ToString

            subcnt = dr.Item("subcnt").ToString


            hrs = dr.Item("hrs").ToString
            mins = dr.Item("mins").ToString
            aps = dr.Item("aps").ToString
            hre = dr.Item("hre").ToString
            mine = dr.Item("mine").ToString
            ape = dr.Item("ape").ToString

            hrsdt = dr.Item("hrsdt").ToString
            minsdt = dr.Item("minsdt").ToString
            apsdt = dr.Item("apsdt").ToString
            hredt = dr.Item("hredt").ToString
            minedt = dr.Item("minedt").ToString
            apedt = dr.Item("apedt").ToString

            ttid = dr.Item("ttid").ToString
            eqnum = dr.Item("eqnum").ToString
            pmtskid = dr.Item("pmtskid").ToString
            pmtid = dr.Item("pmtid").ToString
            eqid = dr.Item("eqid").ToString
            funcid = dr.Item("funcid").ToString
            skill = dr.Item("pm").ToString
            task = dr.Item("task").ToString
            func = dr.Item("func").ToString
            comp = dr.Item("compnum").ToString
            parts = dr.Item("parts").ToString
            tools = dr.Item("tools").ToString
            lubes = dr.Item("lubes").ToString
            meas = dr.Item("meas").ToString
            itask = dr.Item("tasknum").ToString
            freq = dr.Item("freq").ToString
            days = dr.Item("days").ToString
            If days = "" Then
                days = "no"
            End If

            shift = dr.Item("shift").ToString

            If shift = "" Then
                shift = "No Shift"
            End If

            fm1 = dr.Item("fm1s").ToString
            fm2 = dr.Item("fm2s").ToString
            fm3 = dr.Item("fm3s").ToString
            fm4 = dr.Item("fm4s").ToString
            fm5 = dr.Item("fm5s").ToString

            fm1i = dr.Item("fm1id").ToString
            fm2i = dr.Item("fm2id").ToString
            fm3i = dr.Item("fm3id").ToString
            fm4i = dr.Item("fm4id").ToString
            fm5i = dr.Item("fm5id").ToString

            fm1o = dr.Item("fm1").ToString
            fm2o = dr.Item("fm2").ToString
            fm3o = dr.Item("fm3").ToString
            fm4o = dr.Item("fm4").ToString
            fm5o = dr.Item("fm5").ToString

            m1 = dr.Item("tmdid1").ToString
            m2 = dr.Item("tmdid2").ToString
            m3 = dr.Item("tmdid3").ToString
            m4 = dr.Item("tmdid4").ToString
            m5 = dr.Item("tmdid5").ToString

            ms1 = dr.Item("mstr1").ToString
            ms2 = dr.Item("mstr2").ToString
            ms3 = dr.Item("mstr3").ToString
            ms4 = dr.Item("mstr4").ToString
            ms5 = dr.Item("mstr5").ToString

            ttime = dr.Item("time").ToString
        End While
        dr.Close()
        Dim sbt As New StringBuilder
        sbt.Append("<table style=""page-break-after:always;"">")
        sbt.Append("<tr>")
        sbt.Append("<td width=""80""><IMG src=""../menu/mimages/lilpfizer.gif""></td>")
        sbt.Append("<td id=""tdtpm"" class=""bigbold14"" vAlign=""top"" width=""620"" runat=""server"">OP - " & skill)
        sbt.Append("</td>")
        sbt.Append("</tr>")
        sbt.Append("<tr>")
        sbt.Append("<td><IMG src=""../menu/mimages/lilglobal.gif""></td>")
        sbt.Append("<td id=""tdeq"" class=""bigbold14"" align=""right"" runat=""server"">Equipment: " & eqnum & "</td>")
        sbt.Append("</tr>")
        sbt.Append("<tr>")
        sbt.Append("<td colSpan=""2"">")
        sbt.Append("<table width=""700"">")
        sbt.Append("<tr>")
        sbt.Append("<td id=""tdtask"" class=""bigbold14 blackborder"" height=""60"" vAlign=""top"" width=""700"" runat=""server"">")

        Dim sbimg As String
        If ttid = "2" Then
            sbimg = "../images/appbuttons/minibuttons/tpmclean.gif"
        ElseIf ttid = "3" Then
            sbimg = "../images/appbuttons/minibuttons/tpmlube.gif"
        ElseIf ttid = "5" Then
            sbimg = "../images/appbuttons/minibuttons/tpminsp.gif"
        Else
            sbimg = "no"
        End If

        sbt.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""690"">")

        If Len(task) > 0 Then
            If sbimg <> "no" Then
                sbt.Append("<tr><td class=""bigbold14""><img src=""" & sbimg & """ align=""left"" hspace=""5"" vspace=""4"">" & task & "</td></tr>")
            Else
                sbt.Append("<tr><td class=""bigbold14"">" & task & "</td></tr>")
            End If

        Else
            sbt.Append("<tr><td class=""bigbold14"">No Task Description Provided</td></tr>")
        End If

        sbt.Append("</table></td>")

        sbt.Append("</tr>")
        sbt.Append("</table>")
        sbt.Append("</td>")
        sbt.Append("</tr>")
        sbt.Append("<tr>")
        sbt.Append("<td colSpan=""2"">")
        sbt.Append("<table width=""700"">")
        sbt.Append("<tr>")

        Dim tpmimg As String
        sql = "select top 1 p.pic_id, p.tpm_image, p.tpm_image_thumb, p.tpm_image_med, p.tpm_image_order, " _
             + "p.tpm_image_title, f.func " _
             + "from pmtracktpmimg p " _
             + "left join functions f on f.func_id = p.funcid where p.funcid = '" & funcid & "' and p.tasknum = '" & tasknum & "' order by p.tpm_image_order"
        dr = news.GetRdrData(sql)
        While dr.Read
            tpmimg = dr.Item("tpm_image_med").ToString

        End While
        dr.Close()
        Dim imgtask As String
        If tpmimg <> "" Then
            imgtask = tpmimg
        Else
            imgtask = "../images/appimages/tpmimg1.gif"
        End If

        sbt.Append("<td class=""blackborder"" height=""350"" vAlign=""middle"" rowSpan=""3"" width=""350"" align=""center"">")
        sbt.Append("<IMG id=""imgtask"" src="" & imgtask & "" width=""340"" height=""340"" runat=""server"">")
        sbt.Append("</td>")

        sbt.Append("<td rowSpan=""3"" width=""5"">&nbsp;</td>")

        'ppe
        'sbt.Append("<td id=""tdppe"" class=""blackborder"" vAlign=""top"" width=""345"" align=""left"" runat=""server"">")
        'Dim ippe As String = "../images/appbuttons/minibuttons/tpmppe.gif"
        'sbt.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""340"">")

        'If lang = "spa" Then
        'sbt.Append("<tr><td class=""plainlabel14""><img src=""" & ippe & """ align=""left"" hspace=""5"" vspace=""4"" ></td><td class=""plainlabel14"" valign=""middle"">No requiere protección especial de seguridad</td></tr>")

        'Else
        'sbt.Append("<tr><td class=""plainlabel14""><img src=""" & ippe & """ align=""left"" hspace=""5"" vspace=""4"" ></td><td class=""plainlabel14"" valign=""middle"">No Personal Protection Equipment Information Provided</td></tr>")

        'End If
        'sbt.Append("</table>")
        'sbt.Append("</td>")
        'sbt.Append("</tr>")

        Dim itool As String = "../images/appbuttons/minibuttons/tpmtool.gif"

        'tools'
        sbt.Append("<tr>")
        sbt.Append("<td id=""tdtools"" class=""blackborder"" vAlign=""top"" width=""345"" align=""left"" runat=""server"">")

        sbt.Append("<table border=""0"">")
        sbt.Append("<tr>")
        sbt.Append("<td rowspan=""3"" valign=""top""><img src=""" & itool & """ align=""left"" hspace=""5"" vspace=""4"" ></td>")

        If lubes <> "none" Then
            'sbt.Append("<tr>")
            sbt.Append("<td colspan=""1"" class=""plainlabel14"" valign=""top""><b>" & tmod.getxlbl("xlb61", "PMALL.vb") & "</b><br>")
            'sbt.Append("<tr><td class=""bigplainblue14"">")
            Dim lubearr As String() = Split(lubes, ";")
            Dim l As Integer
            For l = 0 To lubearr.Length - 1
                If lubearr(l) = "none" Then 'Len(lubearr(l)) = 0 Or 
                    sbt.Append(tmod.getxlbl("xlb28", "PGEMSHTM2.vb") & "<br>")
                Else
                    sbt.Append(lubearr(l) & "<br>")
                End If

            Next
            sbt.Append("</td></tr>")
        Else
            sbt.Append("<td colspan=""1"" class=""plainlabel14""><b>" & tmod.getxlbl("xlb61", "PMALL.vb") & "</b><br>" & tmod.getxlbl("xlb28", "PGEMSHTM2.vb") & "</td></tr>")

        End If
        If tools <> "none" Then

            sbt.Append("<tr>")
            'sbt.Append("<td></td>")
            sbt.Append("<td colspan=""1"" class=""plainlabel14"" valign=""top""><b>" & tmod.getxlbl("xlb62", "PMALL.vb") & "</b><br>")
            'sbt.Append("<td class=""bigplainblue14"">")
            Dim toolarr As String() = Split(tools, ";")
            Dim t As Integer
            For t = 0 To toolarr.Length - 1
                If toolarr(t) = "none" Then 'Len(toolarr(t)) = 0 Or 
                    sbt.Append(tmod.getxlbl("xlb22", "PGEMSHTM.vb") & "<br>")
                Else
                    sbt.Append(toolarr(t) & "<br>")
                End If
            Next
            sbt.Append("</td></tr>")
            'If toolarr.Length - 1 = 0 Then
            'sbt.Append("<br>")
            'End If
        Else
            sbt.Append("<tr>")
            'sbt.Append("<td></td>")
            sbt.Append("<td colspan=""1"" class=""plainlabel14""><b>" & tmod.getxlbl("xlb62", "PMALL.vb") & "</b><br>" & tmod.getxlbl("xlb22", "PGEMSHTM.vb") & "</td></tr>")
        End If
        If parts <> "none" Then
            sbt.Append("<tr>")
            'sbt.Append("<td></td>")
            sbt.Append("<td colspan=""1"" class=""plainlabel14"" valign=""top""><b>" & tmod.getxlbl("xlb63", "PMALL.vb") & "</b><br>")
            'sbt.Append("<td colspan=""1"" class=""bigplain14""><b>Parts:</b></td><td class=""bigplainblue14"">" & parts)
            Dim partarr As String() = Split(parts, ";")
            Dim p As Integer
            For p = 0 To partarr.Length - 1
                If partarr(p) = "none" Then 'Len(partarr(p)) = 0 Or 
                    sbt.Append(tmod.getxlbl("xlb32", "PGEMSHTM2.vb") & "<br>")
                Else
                    sbt.Append(partarr(p) & "<br>")
                End If
            Next

        Else
            sbt.Append("<tr>")
            'sbt.Append("<td></td>")
            sbt.Append("<td colspan=""1"" class=""plainlabel14""><b>" & tmod.getxlbl("xlb63", "PMALL.vb") & "</b><br>" & tmod.getxlbl("xlb32", "PGEMSHTM2.vb") & "</td></tr>")
        End If

        sbt.Append("</table>")

        sbt.Append("</td>")
        sbt.Append("</tr>")

        'time
        sbt.Append("<tr>")
        sbt.Append("<td id=""tdtime"" class=""blackborder"" vAlign=""top"" width=""345"" align=""left"" runat=""server"">")
        Dim isw As String = "../images/appbuttons/minibuttons/tpmsw.gif"
        sbt.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""340"">")
        sbt.Append("<tr><td><img src=""" & isw & """ align=""left"" hspace=""5"" vspace=""4"" ></td><td class=""bigbold14"" valign=""middle"">" & ttime & " min.</td></tr>")
        sbt.Append("</table>")
        sbt.Append("</td>")

        sbt.Append("</tr>")
        sbt.Append("<tr>")
        sbt.Append("<td id=""tdtasksteps"" class=""blackborder"" height=""430"" vAlign=""top"" width=""350"" runat=""server"">")

        sbt.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""340"">")
        sbt.Append("<tr><td width=""20""></td><td width=""320""></td></tr>")
        If lang = "spa" Then
            sbt.Append("<tr><td class=""bigbold14 stasklft"">" & tmod.getxlbl("lang3360a", "reports2.aspx") & "</td><td class=""bigbold14 staskrt"">Detalles de inspección</td></tr>")
        Else
            sbt.Append("<tr><td class=""bigbold14 stasklft"">" & tmod.getxlbl("lang3360a", "reports2.aspx") & "</td><td class=""bigbold14 staskrt"">Inspection Details </td></tr>")
        End If
        sql = "usp_getpmmantpmperfst '" & pmid & "','" & eqid & "','" & funcid & "','" & tasknum & "'"
        Dim subtask, subtasknum As String
        dr = news.GetRdrData(sql)
        While dr.Read
            subtasknum = dr.Item("subtask").ToString
            If subtasknum <> "0" Then
                subtask = dr.Item("subt").ToString
                sbt.Append("<tr><td class=""bigbold14 stasklfts"">" & subtasknum & "</td><td class=""plainlabel14"">" & subtask & "</td></tr>")
            End If
        End While
        dr.Close()
        sbt.Append("</table>")

        sbt.Append("</td>")
        sbt.Append("<td rowSpan=""3"" width=""5"">&nbsp;</td>")
        sbt.Append("<td id=""tdcomments"" class=""blackborder"" height=""430"" vAlign=""top"" width=""350"" runat=""server"">")

        sbt.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""340"">")
        sbt.Append("<tr><td width=""340""></td></tr>")
        sbt.Append("<tr><td class=""bigbold14 staskrt"">" & tmod.getxlbl("lang3360b", "reports2.aspx") & "</td></tr>")
        sbt.Append("</table>")

        sbt.Append("</td>")
        sbt.Append("</tr>")
        sbt.Append("</table>")
        sbt.Append("</td>")
        sbt.Append("</tr>")
        sbt.Append("</table>")
        Dim ret As String = sbt.ToString
        Return ret

    End Function

End Class

