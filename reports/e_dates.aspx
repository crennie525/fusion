﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="e_dates.aspx.vb" Inherits="lucy_r12.e_dates" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function getcal(fld) {
            var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
            if (eReturn) {
                if (fld == "s") {
                    document.getElementById("divfrom").innerHTML = eReturn;
                    document.getElementById("lblfrom").value = eReturn;
                }
                else {
                    document.getElementById("divto").innerHTML = eReturn;
                    document.getElementById("lblto").value = eReturn;
                }
                
            }
        }
        function checkdat() {
            var f = document.getElementById("lblfrom").value;
            var t = document.getElementById("lblto").value;
            if (f != "" && t != "") {
                //alert(f + "," + t)
                var ret = f + "," + t;
                window.returnValue = ret;
                window.close();
            }
        }
    </script>
    <style type="text/css">
    .readonly140
        {
            border: 1px solid #CBCCCE;
            background-color: #ECF0F3;
            font-family: Arial,MS Sans Serif;
            font-size: 12px;
            vertical-align: middle;
            display: table-cell;
            text-indent: 1px;
            height: 20px;
            width: 140px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <table>
    <tr>
    <td class="label" colspan="2">From Date</td>
    </tr>
    <tr>
    <td><div class="readonly140" id="divfrom" runat="server">
                                </div></td>
    <td><img onclick="getcal('s');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                            width="19" height="19" /></td>
    </tr>
    <tr>
    <td class="label" colspan="2">To Date</td>
    </tr>
    <tr>
    <td><div class="readonly140" id="divto" runat="server">
                                </div></td>
    <td><img onclick="getcal('c');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                            width="19" height="19" /></td>
    </tr>
    <tr>
    <td></td>
    <td><img src="../images/appbuttons/minibuttons/savedisk1.gif" alt="" onclick="checkdat();" /></td>
    </tr>
    </table>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblfrom" runat="server" />
    <input type="hidden" id="lblto" runat="server" />
    </form>
</body>
</html>
