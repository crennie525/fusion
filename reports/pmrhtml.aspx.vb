﻿Imports System.Data.SqlClient
Public Class pmrhtml
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim eqstr, skillid, qty, freq, rdid, mode As String
    Dim tmod As New transmod
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        If comi = "SUN" Then
            lblfslang.Value = "fre"
        End If
        'END ADD FOR SUNCOR
        'Put user code to initialize the page here
        If Not IsPostBack Then
            eqstr = Request.QueryString("eqstr").ToString
            skillid = Request.QueryString("skillid").ToString
            qty = Request.QueryString("qty").ToString
            freq = Request.QueryString("freq").ToString
            rdid = Request.QueryString("rdid").ToString
            pms.Open()
            PopWI(eqstr, skillid, qty, freq, rdid)
            'PopParts(eqid, mode)
            'PopTools(eqid, mode)
            'PopLubes(eqid, mode)
            pms.Dispose()

        End If
    End Sub
    Private Sub Sep_EQ(ByVal eqstr As String, ByVal skillid As String, ByVal qty As String, ByVal freq As String, ByVal rdid As String)
        Dim eqid As String
        eqstr = eqstr.Replace("(", "")
        eqstr = eqstr.Replace(")", "")
        Dim eqarr() As String = eqstr.Split(",")
        Dim i As Integer
        For i = 0 To eqarr.Length - 1
            eqid = eqarr(i).ToString
            eqid = "(" & eqid & ")"
            PopWI(eqid, skillid, qty, freq, rdid)
        Next
    End Sub
    Private Sub PopWI(ByVal eqstr As String, ByVal skillid As String, ByVal qty As String, ByVal freq As String, ByVal rdid As String)
        Dim fid As String = ""
        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim funcchk As String = ""
        Dim func As String = ""
        Dim subtask As String = ""
        Dim eqnum As String = ""
        Dim eqnumchk As Integer = 0
        Dim hdchk As Integer = 0



        Dim eqid As String
        eqstr = eqstr.Replace("(", "")
        eqstr = eqstr.Replace(")", "")
        Dim eqarr() As String = eqstr.Split(",")
        Dim i As Integer
        Dim tst As String = eqarr.Length - 1
        Dim sb As New System.Text.StringBuilder
        Dim chk As Integer


        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""580""></td></tr>")

        For i = 0 To eqarr.Length - 1
            hdchk = 0
            eqid = eqarr(i).ToString
            eqstr = "(" & eqid & ")"
            'eqnumchk = eqid
            'PopWI(eqid, skillid, qty, freq, rdid)
            sql = "select count(*) from pmtasks where eqid in (" & eqstr & ") and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "'"

            chk = pms.Scalar(sql)


            If chk > 0 Then
                sql = "usp_pmr_rep '" & eqstr & "','" & skillid & "','" & qty & "','" & freq & "','" & rdid & "'"
                Dim parts, tools, lubes As String
                parts = ""
                tools = ""
                lubes = ""
                dr = pms.GetRdrData(sql)
                Dim pretech, pretechchk As String



                While dr.Read
                    skill = dr.Item("skill").ToString & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString
                    pretech = dr.Item("ptid").ToString
                    Dim skchk, skchk2 As String
                    skchk = skill.ToLower
                    skchk2 = skillchk.ToLower
                    If skchk <> skchk2 Or eqnumchk <> eqid Then 'OrElse pretech <> pretechchk Then
                        eqnumchk = eqid
                        hdchk = 1
                        
                        skillchk = skill
                        start = 0
                        flag = 0
                        If pretech = "" Or pretech = "0" Then
                            sb.Append("<tr><td class=""bigbold"" colspan=""3"">" _
                            + "" & dr.Item("freq").ToString & " / " & dr.Item("skill").ToString & " / " & dr.Item("rd").ToString & "</td></tr>")
                        Else
                            pretechchk = pretech
                            sb.Append("<tr><td class=""bigbold"" colspan=""3"">" _
                            + "PdM - " & dr.Item("pretech").ToString & " - " & dr.Item("freq").ToString & " / " & dr.Item("skill").ToString & " / " & dr.Item("rd").ToString & "</td></tr>")
                        End If

                        sb.Append("<tr><td class=""label"" colspan=""3"">" & tmod.getxlbl("xlb307", "CustomCMMShtml.aspx.vb") & "  " & dr.Item("down").ToString & " Minutes</td></tr>")
                        'If eqnumchk <> eqid Then
                        'eqnumchk = eqid
                        'If eqnumchk <> eqid Then
                        'eqid = eqnumchk
                        'Else
                        'eqid = eqid
                        'End If
                        'End If
                        sb.Append("<tr><td class=""label"" colspan=""3"">" & dr.Item("eqnum").ToString & " - " & dr.Item("eqdesc").ToString & "</td></tr>")
                        sb.Append("<tr><td>&nbsp;</td></tr>")
                        sb.Append("<tr><TD class=""label"" colspan=""3"" bgcolor=""silver"">" & tmod.getxlbl("xlb308", "CustomCMMShtml.aspx.vb") & "</TD></tr>")
                        sb.Append("<tr><td>&nbsp;</td></tr>")
                    End If
                    func = dr.Item("func").ToString
                    If flag = 0 Then
                        flag = 1
                        funcchk = func
                        sb.Append("<tr><td class=""labelu"" colspan=""3"">" & func & "</td></tr>")
                    Else
                        If func <> funcchk Then
                            funcchk = func
                            sb.Append("<tr><td colspan=""3"" style=""border-bottom: 1px solid black;"">&nbsp;</td>")
                            sb.Append("<tr><td>&nbsp;</td></tr>")
                            sb.Append("<tr><td class=""labelu"" colspan=""3"">" & func & "</td></tr>")
                        End If
                    End If
                    Dim task, lube, meas As String
                    task = dr.Item("task").ToString
                    If dr.Item("lubes").ToString <> "none" Or Len(dr.Item("lubes").ToString) = 0 Then
                        lube = "" '"; " & dr.Item("lubes").ToString
                    Else
                        lube = ""
                    End If
                    'If dr.Item("lube").ToString <> "none" Then
                    'lube = "; " & dr.Item("lube").ToString
                    'Else
                    'lube = ""
                    'End If

                    meas = ""

                    subtask = dr.Item("subtask").ToString
                    task = task & lube & meas

                    If subtask = "0" Then
                        sb.Append("<tr><td class=""plainlabel"">[" & dr.Item("tasknum").ToString & "]</td>")
                        If Len(dr.Item("task").ToString) > 0 Then
                            sb.Append("<td colspan=""2"" class=""plainlabel"">" & task & "</td></tr>")
                        Else
                            sb.Append("<td colspan=""2"" class=""plainlabel"">" & tmod.getlbl("cdlbl738", "CustomCMMShtml.aspx.vb") & "</td></tr>")
                        End If
                        meas = dr.Item("meas").ToString
                        If meas <> "none" Then
                            sb.Append("<tr><td></td>")
                            sb.Append("<td colspan=""2"" class=""plainlabel"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & meas & "</td></tr>")
                        Else
                            meas = ""
                        End If
                        sb.Append("<tr><td></td>")
                        sb.Append("<td colspan=""2"" class=""plainlabel"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OK(___)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & dr.Item("fm1").ToString & "</td></tr>")
                    Else
                        sb.Append("<tr><td></td><td class=""plainlabel"">[" & subtask & "]</td>")
                        If Len(dr.Item("subt").ToString) > 0 Then
                            sb.Append("<td class=""plainlabel"">" & dr.Item("subt").ToString & "</td></tr>")
                        Else
                            sb.Append("<td class=""plainlabel"">" & tmod.getlbl("cdlbl739", "CustomCMMShtml.aspx.vb") & "</td></tr>")
                        End If
                        sb.Append("<tr><td></td><td></td>")
                        sb.Append("<td class=""plainlabel"">OK(___)</td></tr>")
                    End If


                    If dr.Item("parts").ToString <> "none" Then 'Or Len(dr.Item("parts").ToString) = 0 Then
                        If Len(parts) = 0 Then
                            parts += dr.Item("parts").ToString
                        Else
                            parts += ";" & dr.Item("parts").ToString
                        End If
                    End If

                    If dr.Item("tools").ToString <> "none" Then 'Or Len(dr.Item("tools").ToString) <> 0 Then
                        If Len(tools) = 0 Then
                            tools += dr.Item("tools").ToString
                        Else
                            tools += ";" & dr.Item("tools").ToString
                        End If

                    End If

                    If dr.Item("lubes").ToString <> "none" Then 'Or Len(dr.Item("lubes").ToString) <> 0 Then
                        If Len(lubes) = 0 Then
                            lubes += dr.Item("lubes").ToString
                        Else
                            lubes += ";" & dr.Item("lubes").ToString
                        End If

                    End If

                End While
                dr.Close()

                If hdchk = 1 Then

                    sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl740", "CustomCMMShtml.aspx.vb") & "</td></tr>")
                    Dim partarr1 As String() = Split(parts, ";")
                    Dim p1 As Integer
                    For p1 = 0 To partarr1.Length - 1
                        If Len(partarr1(p1)) = 0 Or partarr1(p1) = "none" Then
                            Dim test As String = partarr1(p1)
                            sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & tmod.getlbl("cdlbl741", "CustomCMMShtml.aspx.vb") & "</td></tr>")
                        Else
                            sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & partarr1(p1) & "</td></tr>")
                        End If
                    Next

                    sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl742", "CustomCMMShtml.aspx.vb") & "</td></tr>")
                    Dim toolarr1 As String() = Split(tools, ";")
                    Dim t1 As Integer
                    For t1 = 0 To toolarr1.Length - 1
                        If Len(toolarr1(t1)) = 0 Or toolarr1(t1) = "none" Then
                            sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & tmod.getlbl("cdlbl743", "CustomCMMShtml.aspx.vb") & "</td></tr>")
                        Else
                            sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & toolarr1(t1) & "</td></tr>")
                        End If
                    Next

                    sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl744", "CustomCMMShtml.aspx.vb") & "</td></tr>")
                    Dim lubearr1 As String() = Split(lubes, ";")
                    Dim l1 As Integer
                    For l1 = 0 To lubearr1.Length - 1
                        If Len(lubearr1(l1)) = 0 Or lubearr1(l1) = "none" Then
                            sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & tmod.getlbl("cdlbl745", "CustomCMMShtml.aspx.vb") & "</td></tr>")
                        Else
                            sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & lubearr1(l1) & "</td></tr>")
                        End If

                    Next
                End If
                
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">&nbsp;</td></tr>")
            End If



        Next

        sb.Append("</Table>")

        tdwi.InnerHtml = sb.ToString

    End Sub
    Private Sub PopParts(ByVal eqid As String, ByVal mode As String)
        Dim pflg As String = "0"
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""250""></td><td width=""20""></td><td width=""350""></td></tr>")
        If mode = "1" Then
            sql = "usp_getWIPartsTotalPdM '" & eqid & "', '0'"
        Else
            sql = "usp_getWIPartsTotalPdM '" & eqid & "', '1'"
        End If
        dr = pms.GetRdrData(sql)
        While dr.Read
            If Len(dr.Item("parts").ToString) > 0 Then
                pflg = "1"
                sb.Append("<tr><td class=""label"">" & dr.Item("func").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("tasknum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("parts").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        sb.Append("</Table>")
        If pflg = "1" Then
            'tdparts.InnerHtml = sb.ToString
        Else
            'tdparts.InnerHtml = tmod.getxlbl("xlb309", "CustomCMMShtml.aspx.vb")
        End If

    End Sub
    Private Sub PopTools(ByVal eqid As String, ByVal mode As String)
        Dim pflg As String = "0"
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""250""></td><td width=""20""></td><td width=""350""></td></tr>")
        If mode = "1" Then
            sql = "usp_getWIToolsTotalPdM '" & eqid & "', '0'"
        Else
            sql = "usp_getWIToolsTotalPdM '" & eqid & "', '1'"
        End If
        dr = pms.GetRdrData(sql)
        While dr.Read
            If Len(dr.Item("parts").ToString) > 0 Then
                pflg = "1"
                sb.Append("<tr><td class=""label"">" & dr.Item("func").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("tasknum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("parts").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        sb.Append("</Table>")
        If pflg = "1" Then
            'tdtools.InnerHtml = sb.ToString
        Else
            'tdtools.InnerHtml = tmod.getxlbl("xlb310", "CustomCMMShtml.aspx.vb")
        End If

    End Sub
    Private Sub PopLubes(ByVal eqid As String, ByVal mode As String)
        Dim pflg As String = "0"
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""250""></td><td width=""20""></td><td width=""350""></td></tr>")
        If mode = "1" Then
            sql = "usp_getWILubesTotalPdM '" & eqid & "', '0'"
        Else
            sql = "usp_getWILubesTotalPdM '" & eqid & "', '1'"
        End If
        dr = pms.GetRdrData(sql)
        While dr.Read
            If Len(dr.Item("parts").ToString) > 0 Then
                pflg = "1"
                sb.Append("<tr><td class=""label"">" & dr.Item("func").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("tasknum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("parts").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        sb.Append("</Table>")
        If pflg = "1" Then
            'tdlubes.InnerHtml = sb.ToString
        Else
            'tdlubes.InnerHtml = tmod.getxlbl("xlb311", "CustomCMMShtml.aspx.vb")
        End If

    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            'lang3309.Text = axlabs.GetASPXPage("CustomCMMShtml.aspx", "lang3309")
        Catch ex As Exception
        End Try
        Try
            ' lang3310.Text = axlabs.GetASPXPage("CustomCMMShtml.aspx", "lang3310")
        Catch ex As Exception
        End Try
        Try
            ' lang3311.Text = axlabs.GetASPXPage("CustomCMMShtml.aspx", "lang3311")
        Catch ex As Exception
        End Try

    End Sub
End Class