

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class inprog
    Inherits System.Web.UI.Page
    Protected WithEvents lang3338 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3337 As System.Web.UI.WebControls.Label

    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Protected WithEvents tdtop As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdinprog As System.Web.UI.HtmlControls.HtmlTableCell
    Dim eqid, cid, coi As String
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents Imagebutton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ddsites As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Dim tmod As New transmod
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 50
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not Me.IsPostBack Then
            cid = "0" 'HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            coi = Request.QueryString("coi")
            lblcoi.Value = coi
            pms.Open()
            GetSites()
            GetAll(PageNumber)
            pms.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                pms.Open()
                GetNext()
                pms.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                pms.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetAll(PageNumber)
                pms.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                pms.Open()
                GetPrev()
                pms.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                pms.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetAll(PageNumber)
                pms.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetAll(PageNumber)
        Catch ex As Exception
            pms.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr875", "eqcopy2.aspx.vb")

            pms.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetAll(PageNumber)
        Catch ex As Exception
            pms.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr876", "eqcopy2.aspx.vb")

            pms.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetSites()
        sql = "select siteid, sitename from sites where compid = '" & cid & "'"
        dr = pms.GetRdrData(sql)
        ddsites.DataSource = dr
        ddsites.DataValueField = "siteid"
        ddsites.DataTextField = "sitename"
        ddsites.DataBind()
        dr.Close()
        ddsites.Items.Insert(0, "All")
    End Sub
    Private Sub GetAll(ByVal PageNumber As Integer, Optional ByVal site As String = "all", Optional ByVal srch As String = "no")
        coi = lblcoi.Value
        Dim pmostat As String
        Dim sb As New StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""700"">")
        sb.Append("<tr><td class=""label"" colspan=""5"" align=""center""><h3>" & tmod.getxlbl("xlb337", "inprog.aspx.vb") & "</h3></td></tr>")
        sb.Append("<tr><td class=""plainlabelblue"" colspan=""5"" align=""center"">Please note that the Practice Site is excluded unless specifically selected</td></tr>")
        sb.Append("<tr><td class=""label"" colspan=""5"">&nbsp;</td></tr>")
        sb.Append("</table>")
        tdtop.InnerHtml = sb.ToString
        Dim sb1 As New StringBuilder
        sb1.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""700"">")
        'sb1.Append("<tr>")
        'sb1.Append("<td></td>")
        'If site = "all" Then
        'site = "0"
        'End If
        If ddsites.SelectedIndex = 0 Then
            site = "0"
        Else
            site = ddsites.SelectedValue.ToString
        End If
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        Dim csrch = srch

        If Len(srch) = 0 Then
            srch = ""
            csrch = "%"
        Else
            csrch = "%" & csrch & "%"
        End If
        Dim top As String = 0
        Dim sidflg As String = 0
        Dim rowflg As String = 0
        Dim sid As String
        cid = lblcid.Value

        Dim psite As String
        sql = "select psite from pmsysvars"
        psite = pms.strScalar(sql)
        If psite <> site Then
            If site = "0" Then
                sql = "SELECT count(e.eqid) FROM equipment e " _
                + "left join sites s on s.siteid = e.siteid " _
                + "left join pmsysusers u on u.username = e.createdby " _
                + "where s.siteid like '%' and s.siteid <> '" & psite & "' and " _
                + "(e.eqnum like '" & csrch & "' or e.eqdesc like '" & csrch & "' or e.spl like '" & csrch & "' " _
                + "or e.oem like '" & csrch & "' or e.model like '" & csrch & "' or e.serial like '" & csrch & "' " _
                + "or e.location like '" & csrch & "' or e.createdby = '" & csrch & "')"
            Else
                sql = "SELECT count(e.eqid) FROM equipment e " _
                + "left join sites s on s.siteid = e.siteid " _
                + "left join pmsysusers u on u.username = e.createdby " _
                + "where s.siteid = '" & site & "' and " _
                + "(e.eqnum like '" & csrch & "' or e.eqdesc like '" & csrch & "' or e.spl like '" & csrch & "' " _
                + "or e.oem like '" & csrch & "' or e.model like '" & csrch & "' or e.serial like '" & csrch & "' " _
                + "or e.location like '" & csrch & "' or e.createdby = '" & csrch & "')"
            End If
        Else
            If site = "0" Then
                sql = "SELECT count(e.eqid) FROM equipment e " _
                + "left join sites s on s.siteid = e.siteid " _
                + "left join pmsysusers u on u.username = e.createdby " _
                + "where s.siteid like '%' and s.siteid <> '" & psite & "' and " _
                + "(e.eqnum like '" & csrch & "' or e.eqdesc like '" & csrch & "' or e.spl like '" & csrch & "' " _
                + "or e.oem like '" & csrch & "' or e.model like '" & csrch & "' or e.serial like '" & csrch & "' " _
                + "or e.location like '" & csrch & "' or e.createdby = '" & csrch & "')"
            Else
                sql = "SELECT count(e.eqid) FROM equipment e " _
                + "left join sites s on s.siteid = e.siteid " _
                + "left join pmsysusers u on u.username = e.createdby " _
                + "where s.siteid = '" & site & "' and " _
                + "(e.eqnum like '" & csrch & "' or e.eqdesc like '" & csrch & "' or e.spl like '" & csrch & "' " _
                + "or e.oem like '" & csrch & "' or e.model like '" & csrch & "' or e.serial like '" & csrch & "' " _
                + "or e.location like '" & csrch & "' or e.createdby = '" & csrch & "')"
            End If
        End If
        Dim intPgCnt, intPgNav As Integer
        txtpg.Value = PageNumber
        intPgNav = pms.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav
        sql = "usp_EgInprog_pgd '" & site & "', '" & srch & "', '" & PageNumber & "', '" & PageSize & "'"
        'Try
        dr = pms.GetRdrData(sql)
        While dr.Read
            sid = dr.Item("siteid").ToString
            If sid <> sidflg Then
                sidflg = sid
                If top = "0" Then
                    top = "1"
                    sb1.Append("<tr><td colspan=""6""><hr size=""3""></td></tr>")
                Else
                    sb1.Append("<tr><td colspan=""6""><hr size=""1""></td></tr>")
                End If

                sb1.Append("<tr><td class=""bigbold"" colspan=""6"">" & dr.Item("sitename").ToString & "</td></tr>")
                sb1.Append("<tr class=""label"">")
                sb1.Append("<td style=""BORDER-BOTTOM: 1px solid"">" & tmod.getlbl("cdlbl828", "inprog.aspx.vb") & "</td>")
                sb1.Append("<td style=""BORDER-BOTTOM: 1px solid"">OEM</td>")
                sb1.Append("<td style=""BORDER-BOTTOM: 1px solid"">" & tmod.getlbl("cdlbl830", "inprog.aspx.vb") & "</td>")
                sb1.Append("<td style=""BORDER-BOTTOM: 1px solid"">" & tmod.getlbl("cdlbl831", "inprog.aspx.vb") & "</td>")
                sb1.Append("<td style=""BORDER-BOTTOM: 1px solid"">" & tmod.getlbl("cdlbl832", "inprog.aspx.vb") & "</td>")
                sb1.Append("<td style=""BORDER-BOTTOM: 1px solid"">" & tmod.getlbl("cdlbl833", "inprog.aspx.vb") & "</td>")
                sb1.Append("</tr>")
                rowflg = 0
            End If
            If rowflg = 0 Then
                rowflg = 1
                sb1.Append("<tr>")
            Else
                rowflg = 0
                sb1.Append("<tr bgcolor=""#E7F1FD"">")
            End If
            Dim ac As String = dr.Item("assetclass").ToString
            If ac = "Select" Then
                ac = "TBD"
            End If
            sb1.Append("<td class=""plainlabel"">" & ac & "</td>")
            If Len(dr.Item("oem").ToString) <> 0 Then
                sb1.Append("<td class=""plainlabel"">" & dr.Item("oem").ToString & "</td>")
            Else
                sb1.Append("<td class=""plainlabel"">" & tmod.getlbl("cdlbl834", "inprog.aspx.vb") & "</td>")
            End If
            sb1.Append("<td class=""plainlabel"">" & dr.Item("eqnum").ToString & "</td>")
            If dr.Item("owner").ToString <> "not provided" Then
                sb1.Append("<td class=""plainlabel""><A href=""mailto:" & dr.Item("owner").ToString & """>" & dr.Item("owner").ToString & "</A></td>")
            Else
                sb1.Append("<td class=""plainlabel"">" & dr.Item("owner").ToString & "</td>")
            End If
            If coi = "AMG" Then
                If dr.Item("pmostat").ToString = "0" Or dr.Item("pmostat").ToString = "" Then
                    sb1.Append("<td class=""plainlabel"">Not Started</td>")
                Else
                    If dr.Item("pmostat").ToString = "1" Then
                        pmostat = "Not Started"
                    ElseIf dr.Item("pmostat").ToString = "2" Then
                        pmostat = "In Progress"
                    ElseIf dr.Item("pmostat").ToString = "3" Then
                        pmostat = "Optimized"
                    End If
                    sb1.Append("<td class=""plainlabel"">" & pmostat & "</td>")
                End If
            Else
                If Len(dr.Item("status").ToString) = 0 Or dr.Item("status").ToString = "Select" Then
                    sb1.Append("<td class=""plainlabel"">" & tmod.getlbl("cdlbl835", "inprog.aspx.vb") & "</td>")
                Else
                    sb1.Append("<td class=""plainlabel"">" & dr.Item("status").ToString & "</td>")
                End If
            End If
            

            If Len(dr.Item("pix").ToString) <> 0 Then
                sb1.Append("<td align=""center""><img src=""../images/appbuttons/minibuttons/magnifier.gif"" onclick=""getpix('" & dr.Item("pix").ToString & "')""></td>")
            Else
                sb1.Append("<td align=""center""><img src=""../images/appbuttons/minibuttons/magnifierdis.gif""></td>")
            End If
            sb1.Append("</tr>")
        End While
        dr.Close()
        sb1.Append("</table>")
        tdinprog.InnerHtml = sb1.ToString
        'Catch ex As Exception

        'End Try

    End Sub

    Private Sub ddsites_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsites.SelectedIndexChanged
        pms.Open()
        GetAll("1")
        pms.Dispose()
    End Sub

    Private Sub Imagebutton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click
        pms.Open()
        GetAll("1")
        pms.Dispose()
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3337.Text = axlabs.GetASPXPage("inprog.aspx", "lang3337")
        Catch ex As Exception
        End Try
        Try
            lang3338.Text = axlabs.GetASPXPage("inprog.aspx", "lang3338")
        Catch ex As Exception
        End Try

    End Sub

End Class
