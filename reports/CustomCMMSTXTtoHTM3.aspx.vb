﻿Public Class CustomCMMSTXTtoHTM3
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim pgem As New PGEMSHTM3
    Dim eqid, mode, longstring As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblfslang.value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.value = dlang.AppDfltLang
        End Try
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        If comi = "SUN" Then
            lblfslang.Value = "fre"
        End If
        'END ADD FOR SUNCOR
        'Put user code to initialize the page here
        eqid = Request.QueryString("eqid").ToString
        mode = Request.QueryString("mode").ToString
        If mode = "1" Then
            longstring = pgem.WritePMHTM(eqid)
        Else
            longstring = pgem.WritePMHTM(eqid, "PdM")
        End If

        'Response.Write(longstring)
        tdinput.InnerHtml = longstring
    End Sub

End Class