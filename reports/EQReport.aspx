<%@ Page Language="vb" AutoEventWireup="false" Codebehind="EQReport.aspx.vb" Inherits="lucy_r12.EQReport" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>EQReport</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/EQReportaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
         var fidarray;
         var cidarray;
         var fidstr;
         var cidstr;
         function checkarr() {
             fidstr = document.getElementById("lblfid").value;
             cidstr = document.getElementById("lblcid").value;
             fidarray = fidstr.split("/");
             cidarray = cidstr.split("/");
         }
         function openfid(typ) {
             var eid = document.getElementById("lbleqid").value;
             var ieid = "i" + eid;
             var teid = "t" + eid;
             try {
                 document.getElementById(ieid).src = '../images/appbuttons/bgbuttons/minus.gif';
                 document.getElementById(teid).className = "view";
             }
             catch (err) {
             }
             if (typ == "fid") {
                 document.getElementById("a1fid").className = "labellink details";
                 document.getElementById("a2fid").className = "labellink";
             }
             if (typ == "cid") {
                 document.getElementById("a1cid").className = "labellink details";
                 document.getElementById("a2cid").className = "labellink";
                 document.getElementById("a1fid").className = "labellink details";
                 document.getElementById("a2fid").className = "labellink";
             }
             if (typ == "fid" || typ == "cid") {
                 for (var i = 0; i < fidarray.length - 1; i++) {
                     fid = fidarray[i];
                     img = "i" + fid;
                     tbl = "t" + fid;
                     try {
                         document.getElementById(img).src = '../images/appbuttons/bgbuttons/minus.gif';
                         document.getElementById(tbl).className = "view";
                     }
                     catch (err) {
                     }
                 }
             }
             if (typ == "cid") {
                 for (var i = 0; i < cidarray.length - 1; i++) {
                     cid = cidarray[i];
                     img = "i" + cid;
                     tbl = "t" + cid;
                     try {
                         document.getElementById(img).src = '../images/appbuttons/bgbuttons/minus.gif';
                         document.getElementById(tbl).className = "view";
                     }
                     catch (err) {
                     }
                 }
             }
         }
         function closefid(typ) {
             if (typ == "fid") {
                 document.getElementById("a2fid").className = "labellink details";
                 document.getElementById("a1fid").className = "labellink";
                 document.getElementById("a2cid").className = "labellink details";
                 document.getElementById("a1cid").className = "labellink";
             }
             if (typ == "cid") {
                 document.getElementById("a2cid").className = "labellink details";
                 document.getElementById("a1cid").className = "labellink";
             }
             if (typ == "fid") {
                 for (var i = 0; i < cidarray.length - 1; i++) {
                     cid = cidarray[i];
                     img = "i" + cid;
                     tbl = "t" + cid;
                     try {
                         document.getElementById(img).src = '../images/appbuttons/bgbuttons/plus.gif';
                         document.getElementById(tbl).className = "details";
                     }
                     catch (err) {
                     }
                 }
                 for (var i = 0; i < fidarray.length - 1; i++) {
                     fid = fidarray[i];
                     img = "i" + fid;
                     tbl = "t" + fid;
                     try {
                         document.getElementById(img).src = '../images/appbuttons/bgbuttons/plus.gif';
                         document.getElementById(tbl).className = "details";
                     }
                     catch (err) {
                     }
                 }
             }
             if (typ == "cid") {
                 for (var i = 0; i < cidarray.length - 1; i++) {
                     cid = cidarray[i];
                     img = "i" + cid;
                     tbl = "t" + cid;
                     try {
                         document.getElementById(img).src = '../images/appbuttons/bgbuttons/plus.gif';
                         document.getElementById(tbl).className = "details";
                     }
                     catch (err) {
                     }
                 }
             }
         }
         function fclose(td, img, stat) {
             var str = document.getElementById(img).src
             var lst = str.lastIndexOf("/") + 1
             var loc = str.substr(lst)
             //alert(td + "," + img + "," + loc)
             if (loc == 'minus.gif') {
                 if (stat != "no") {
                     try {
                         document.getElementById(img).src = '../images/appbuttons/bgbuttons/plus.gif';
                         document.getElementById(td).className = "details";
                     }
                     catch (err) {

                     }
                 }
             }
             else {
             //alert(stat)
                 if (stat != "no") {
                 //alert("stat = no")
                     //try {
                         document.getElementById(img).src = '../images/appbuttons/bgbuttons/minus.gif';
                         document.getElementById(td).className = "view";
                         //alert(img + "," + document.getElementById(img).src)
                     //}
                     //catch (err) {

                     //}
                 }
             }
         }
     </script>
	</HEAD>
	<body  onload="checkarr();">
		<form id="form1" method="post" runat="server">
			<table cellpadding="3" cellspacing="3" width="620" align="center">
				<tr>
					<td class="label" align="center" colSpan="2">
						<h2><asp:Label id="lang3318" runat="server">Equipment Hierarchy Report</asp:Label></h2>
					</td>
				</tr>
				<tr height="20">
					<td class="label" width="80"><asp:Label id="lang3319" runat="server">Plant Site:</asp:Label></td>
					<td class="plainlabel" id="tdsite" runat="server" width="540"></td>
				</tr>
				<tr height="20">
					<td class="label"><asp:Label id="lang3320" runat="server">Department:</asp:Label></td>
					<td class="plainlabel" id="tddept" runat="server"></td>
				</tr>
				<tr height="20">
					<td class="label"><asp:Label id="lang3321" runat="server">Station/Cell:</asp:Label></td>
					<td class="plainlabel" id="tdcell" runat="server"></td>
				</tr>
				<tr>
					<td colspan="2"><a id="a1fid" href="#" onclick="openfid('fid');" class="labellink"><asp:Label id="lang3322" runat="server">Expand All Functions</asp:Label></a> <a id="a2fid" href="#" onclick="closefid('fid');" class="labellink details"><asp:Label id="lang3323" runat="server">Collapse All Functions</asp:Label></a>&nbsp;&nbsp;&nbsp;<a id="a1cid" href="#" onclick="openfid('cid');" class="labellink"><asp:Label id="lang3324" runat="server">Expand All Components</asp:Label></a> <a id="a2cid" href="#" onclick="closefid('cid');" class="labellink details"><asp:Label id="lang3325" runat="server">Collapse All Components</asp:Label></a></td>
				</tr>
				<tr>
					<td id="tdarch" runat="server" colspan="2"></td>
				</tr>
			</table>
			<input type="hidden" id="lbldid" runat="server" NAME="lbldid"> <input type="hidden" id="lblclid" runat="server" NAME="lblclid">
			<input type="hidden" id="lblchk" runat="server" NAME="lblchk"> <input type="hidden" id="lbleqid" runat="server">
			<input type="hidden" id="lblsid" runat="server"> <input type="hidden" id="lblfid" runat="server">
			<input type="hidden" id="lblcid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
