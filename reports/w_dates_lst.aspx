﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="w_dates_lst.aspx.vb" Inherits="lucy_r12.w_dates_lst" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Last Report Date</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
    <!--
        function getminsrch() {
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=lu&site=" + sid + "&wo=", "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("tddept").innerHTML = ret[1];
                document.getElementById("lblclid").value = ret[2];
                document.getElementById("tdcell").innerHTML = ret[3];
                if (ret[2] != "" && ret[2] != "0") {
                    document.getElementById("lbltyp").value = "cell";
                }
                else {
                    document.getElementById("lbltyp").value = "dept";
                }

            }
        }
        function getlocs1() {

            var sid = document.getElementById("lblsid").value;

            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=lu&sid=" + sid + "&wo=&rlid=&eqid=&fuid=&coid=", "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("tdloc3").innerHTML = ret[2];
                document.getElementById("lbltyp").value = "loc";
            }
        }
        function clearall() {
            document.getElementById("lbldid").value = "";
            document.getElementById("tddept").innerHTML = "";
            document.getElementById("lblclid").value = "";
            document.getElementById("tdcell").innerHTML = "";
            document.getElementById("lbllid").value = "";
            document.getElementById("tdloc3").innerHTML = "";
            document.getElementById("lbldept").value = "";
            document.getElementById("tdedept").innerHTML = "";
            document.getElementById("lbltyp").value = "";
        }
        function getemd() {

            var sid = document.getElementById("lblsid").value;

            var eReturn = window.showModalDialog("w_eq23.aspx?typ=lu&sid=" + sid, "", "dialogHeight:320px; dialogWidth:320px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn
                document.getElementById("lbldept").value = ret;
                document.getElementById("tdedept").innerHTML = ret;
                document.getElementById("lbltyp").value = "eq23";
            }
        }
        function getrep() {
            var sid = document.getElementById("lblsid").value;
            var sdate = document.getElementById("txtfrom").value;
            var did = document.getElementById("lbldid").value;
            var clid = document.getElementById("lblclid").value;
            var locid = document.getElementById("lbllid").value;
            var dept = document.getElementById("lbldept").value;
            var who = document.getElementById("lbltyp").value;
            var crbe = document.getElementById("rbe");
            var crbp = document.getElementById("rbp");
            var rep;
            if (crbe.checked == true) {
                rep = "eq";
            }
            else if (crbp.checked == true) {
                rep = "prod";
            }
            if (who == "") {
                who = "date";
            }
            if (sdate != "") {
                window.open("w_3monthdown.aspx?rep=" + rep + "&loctype=" + who + "&sid=" + sid + "&sdate=" + sdate + "&did=" + did + "&cellid=" + clid + "&locid=" + locid + "&dept=" + dept + "&date=" + Date());
            }
            else {
                alert("Date Required")
            }
        }
    //-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <table width="380" style="position: absolute; left: 5px; top: 5px;">
        <tr>
            <td colspan="3" align="center" class="bigbold1" id="tdtitle" runat="server">
                Total Equipment/Production Downtime (3 Month) Report Options
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3" class="label">
                Choose Date (Required)
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center" class="plainlabel" style="border-bottom: 1px solid blue;">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3" class="plainlabelblue" align="center">
                Report will calculate Total Down Time for past 3 months based on the Date selected
                below
            </td>
        </tr>
        <tr>
            <td class="label" width="110">
                From Date
            </td>
            <td width="170">
                <asp:TextBox ID="txtfrom" runat="server" ReadOnly="true" BackColor="#ECF0F3" Width="170px"></asp:TextBox>
            </td>
            <td width="40">
                <img alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg" id="btnfrom"
                    runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center" class="plainlabel" style="border-bottom: 1px solid blue;">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3" class="label">
                Choose Department or Location (Optional)
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center" class="plainlabel" style="border-bottom: 1px solid blue;">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table cellpadding="1" cellspacing="1">
                    <tr>
                        <td id="tddepts" class="bluelabel" runat="server" width="110">
                            Department
                        </td>
                        <td width="22">
                            <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                        <td id="tdlocs1" class="bluelabel" runat="server" width="110">
                            Location
                        </td>
                        <td width="22">
                            <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                        <td width="46">
                        </td>
                    </tr>
                    <tr id="tremd" runat="server">
                        <td id="td1" class="bluelabel" runat="server">
                            EMD Departments
                        </td>
                        <td>
                            <img onclick="getemd();" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Department
                        </td>
                        <td colspan="4" id="tddept" class="plainlabel">
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Cell
                        </td>
                        <td colspan="4" id="tdcell" class="plainlabel">
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Location
                        </td>
                        <td colspan="4" id="tdloc3" class="plainlabel">
                        </td>
                    </tr>
                    <tr id="treddept" runat="server">
                        <td class="label">
                            EMD Department
                        </td>
                        <td colspan="4" id="tdedept" class="plainlabel">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center" class="plainlabel" style="border-bottom: 1px solid blue;">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3" class="label">
                Choose Report Type
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center" class="plainlabel" style="border-bottom: 1px solid blue;">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
        <td colspan="3" align="center" class="bluelabel"><input type="radio" checked id="rbe" name="rb" />Equipment&nbsp;&nbsp;&nbsp;<input type="radio" id="rbp" name="rb" />Production</td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center" class="plainlabel" style="border-bottom: 1px solid blue;">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>

        <tr>
            <td colspan="3" align="right" class="plainlabel">
                <a href="#" onclick="getrep();">Get Report</a>
            </td>
        </tr>
    </table>
    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtfrom"
        PopupButtonID="btnfrom">
    </cc1:CalendarExtender>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lblclid" runat="server" />
    <input type="hidden" id="lbltyp" runat="server" />
    <input type="hidden" id="lbllid" runat="server" />
    <input type="hidden" id="lbldept" runat="server" />
    </form>
</body>
</html>
