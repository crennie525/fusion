﻿Imports System.Data.SqlClient
Imports System.Text
Public Class mwchart
    Inherits System.Web.UI.Page
    Dim mm As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim eqid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            mm.Open()
            getrep(eqid)
            mm.Dispose()
        End If
    End Sub
    Private Sub getrep(eqid As String)
        Dim mth As String
        Dim yr As String
        Dim i1, i2, i3, i4 As String
        Dim m1, m2, m3, m4 As String
        Dim w1, w2, w3, w4 As Integer
        Dim mn1, mn2, mn3, mn4 As String
        Dim w1s, w2s, w3s, w4s As Integer
        Dim currdate As String = mm.CNOW
        Dim test As String = currdate
        sql = "select datepart(mm, '" & currdate & "') as 'mth', datepart(yy, '" & currdate & "') as 'yr'"
        dr = mm.GetRdrData(sql)
        While dr.Read
            mth = dr.Item("mth").ToString
            yr = dr.Item("yr").ToString
        End While
        dr.Close()
        sql = " usp_getmnthsinyr_1 '" & yr & "','" & mth & "'"
        Dim arr As String
        arr = mm.strScalar(sql)
        Dim arr1() As String = arr.Split(",")
        i1 = arr1(0).ToString
        i2 = arr1(1).ToString
        i3 = arr1(2).ToString
        i4 = arr1(3).ToString

        Dim iarr() As String = i1.Split("-")
        m1 = iarr(0).ToString
        w1 = iarr(3).ToString
        mn1 = getmonth(m1)
        w1s = iarr(1).ToString

        Dim iarr2() As String = i2.Split("-")
        m2 = iarr2(0).ToString
        w2 = iarr2(3).ToString
        mn2 = getmonth(m2)
        w2s = iarr2(1).ToString

        Dim iarr3() As String = i3.Split("-")
        m3 = iarr3(0).ToString
        w3 = iarr3(3).ToString
        mn3 = getmonth(m3)
        w3s = iarr3(1).ToString

        Dim iarr4() As String = i4.Split("-")
        m4 = iarr4(0).ToString
        w4 = iarr4(3).ToString
        mn4 = getmonth(m4)
        w4s = iarr4(1).ToString

        Dim eq, eqd As String
        sql = "select eqnum, eqdesc from equipment where eqid = '" & eqid & "'"
        dr = mm.GetRdrData(sql)
        While dr.Read
            eq = dr.Item("eqnum").ToString
            eqd = dr.Item("eqdesc").ToString
        End While
        dr.Close()

        Dim tcnt As Integer

        Dim sb As New StringBuilder
        sql = "select count(*) from pmtaskstpm t where t.eqid = '" & eqid & "' " _
        + "and isnull(t.taskstatus, 'OK') <> 'Delete' " _
        + "and ((t.freq <= 7) or (t.freq = 30)) and subtask = 0"

        tcnt = mm.Scalar(sql)
        If tcnt > 0 Then
            Dim pgcnt As Integer = mm.PageCountRev(tcnt, 15)

            Dim wd1, wd2, wd3, wd4 As Integer
            Dim cs1, cs2, cs3, cs4 As Integer
            Dim colcnt As Integer = 4

            If w1 = 4 Then
                cs1 = 4 '80
            ElseIf w1 = 5 Then
                cs1 = 5 '100
            ElseIf w1 = 6 Then
                cs1 = 6 '120
            End If

            If w2 = 4 Then
                cs2 = 4 '80
            ElseIf w2 = 5 Then
                cs2 = 5 '100
            ElseIf w2 = 6 Then
                cs2 = 6 '120
            End If

            If w3 = 4 Then
                cs3 = 4 '80
            ElseIf w3 = 5 Then
                cs3 = 5 '100
            ElseIf w3 = 6 Then
                cs3 = 6 '120
            End If

            If w4 = 4 Then
                cs4 = 4 '80
            ElseIf w4 = 5 Then
                cs4 = 5 '100
            ElseIf w4 = 6 Then
                cs4 = 6 '120
            End If

            colcnt += cs1 + cs2 + cs3 + cs4

            Dim freq As String
            Dim tasknum, taskdesc, func, compnum, fm1, pmtskid, cqty As String
            Dim taskcnt As Integer = 0
            Dim currpg As Integer = 0
            Dim currt As Integer = 0
            Dim freqi As Integer
            sql = "select t.pmtskid, t.tasknum, t.taskdesc, t.freq, f.func, c.compnum, isnull(t.cqty, 1) as 'cqty', t.fm1 from pmtaskstpm t " _
                + "left join functions f on f.func_id = t.funcid " _
                + "left join components c on c.comid = t.comid " _
                + "where t.eqid = '" & eqid & "' " _
            + "and isnull(t.taskstatus, 'OK') <> 'Delete' " _
            + "and ((t.freq <= 7) or (t.freq = 30)) and t.subtask = 0"

            dr = mm.GetRdrData(sql)
            While dr.Read
                taskcnt += 1

                pmtskid = dr.Item("pmtskid").ToString
                cqty = dr.Item("cqty").ToString

                tasknum = dr.Item("tasknum").ToString
                taskdesc = dr.Item("taskdesc").ToString
                freq = dr.Item("freq").ToString
                func = dr.Item("func").ToString
                compnum = dr.Item("compnum").ToString
                fm1 = dr.Item("fm1").ToString
                currt += 1
                If taskcnt = 1 Then
                    currpg += 1
                    If currpg < pgcnt Then
                        sb.Append("<table style=""page-break-after:always;"" border=""0"" width=""980"" cellspacing=""0"">")
                    Else
                        sb.Append("<table border=""0"" width=""980"" cellspacing=""0"">")
                    End If

                    sb.Append("<tr>")
                    sb.Append("<td class=""label"" align=""center"" colspan=""" & colcnt & """>" & eq & " - " & eqd & "</td></tr>")
                    'sb.Append("<tr>")
                    'sb.Append("<tr><td><table cellspacing=""0"" border=""0"">")
                    sb.Append("<tr><td width=""70""></td><td width=""400""></td><td width=""20""></td><td width=""20""></td>")
                    'sb.Append("<td width=""100""></td><td width=""100""></td><td width=""100""></td><td width=""100""></td>")
                    'sb.Append("<tr>")
                    If w1 = 4 Then
                        sb.Append("<td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td>") '</tr>
                    ElseIf w1 = 5 Then
                        sb.Append("<td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td>") '</tr>
                    ElseIf w1 = 6 Then
                        sb.Append("<td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td>") '</tr>
                    End If
                    If w2 = 4 Then
                        sb.Append("<td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td>") '</tr>
                    ElseIf w2 = 5 Then
                        sb.Append("<td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td>") '</tr>
                    ElseIf w2 = 6 Then
                        sb.Append("<td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td>") '</tr>
                    End If
                    If w3 = 4 Then
                        sb.Append("<td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td>") '</tr>
                    ElseIf w3 = 5 Then
                        sb.Append("<td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td>") '</tr>
                    ElseIf w3 = 6 Then
                        sb.Append("<td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td>") '</tr>
                    End If
                    If w4 = 4 Then
                        sb.Append("<td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td>") '</tr>
                    ElseIf w4 = 5 Then
                        sb.Append("<td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td>") '</tr>
                    ElseIf w4 = 6 Then
                        sb.Append("<td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""20""></td>") '</tr>
                    End If
                    sb.Append("</tr>")
                    sb.Append("<tr>")

                    sb.Append("<td class=""plainlabel box1ltop"" align=""center"">TASK#</td>")
                    sb.Append("<td class=""plainlabel box1ltop"" align=""center"">TASK TEXT</td>")
                    sb.Append("<td class=""plainlabel box1ltop"" align=""center"" colspan=""2"">FREQ</td>")
                    sb.Append("<td class=""plainlabel box1ltop"" align=""center"" colspan=""" & cs1 & """>" & mn1 & "</td>")
                    sb.Append("<td class=""plainlabel box1ltop"" align=""center"" colspan=""" & cs2 & """>" & mn2 & "</td>")
                    sb.Append("<td class=""plainlabel box1ltop"" align=""center"" colspan=""" & cs3 & """>" & mn3 & "</td>")
                    sb.Append("<td class=""plainlabel box1ltopend"" align=""center"" colspan=""" & cs4 & """>" & mn4 & "</td>")

                    sb.Append("</tr>")
                    sb.Append("<tr>")
                    sb.Append("<td class=""plainlabel box1ltop"" align=""center"">&nbsp;</td>")
                    sb.Append("<td class=""plainlabel box1ltop"" align=""center"">&nbsp;</td>")
                    sb.Append("<td class=""plainlabel box1ltop"" align=""center"">M</td>")
                    sb.Append("<td class=""plainlabel box1ltop"" align=""center"">W</td>")

                    If w1 = 4 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">w" & w1s & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w1s + 1 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w1s + 2 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w1s + 3 & "</td>") '</tr>
                    ElseIf w1 = 5 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">w" & w1s & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w1s + 1 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w1s + 2 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w1s + 3 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w1s + 4 & "</td>") '</tr>
                    ElseIf w1 = 6 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">w" & w1s & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w1s + 1 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w1s + 2 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w1s + 3 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w1s + 4 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w1s + 5 & "</td>") '</tr>
                    End If
                    If w2 = 4 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">w" & w2s & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w2s + 1 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w2s + 2 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w2s + 3 & "</td>") '</tr>
                    ElseIf w2 = 5 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">w" & w2s & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w2s + 1 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w2s + 2 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w2s + 3 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w2s + 4 & "</td>") '</tr>
                    ElseIf w2 = 6 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">w" & w2s & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w2s + 1 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w2s + 2 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w2s + 3 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w2s + 4 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w2s + 5 & "</td>") '</tr>
                    End If
                    If w3 = 4 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">w" & w3s & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w3s + 1 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w3s + 2 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w3s + 3 & "</td>") '</tr>
                    ElseIf w3 = 5 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">w" & w3s & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w3s + 1 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w3s + 2 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w3s + 3 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w3s + 4 & "</td>") '</tr>
                    ElseIf w3 = 6 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">w" & w3s & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w3s + 1 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w3s + 2 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w3s + 3 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w3s + 4 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w3s + 5 & "</td>") '</tr>
                    End If
                    If w4 = 4 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">w" & w4s & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w4s + 1 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w4s + 2 & "</td><td width=""20"" class=""plainlabel box1ltopend"">w" & w4s + 3 & "</td>") '</tr>
                    ElseIf w4 = 5 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">w" & w4s & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w4s + 1 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w4s + 2 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w4s + 3 & "</td><td width=""20"" class=""plainlabel box1ltopend"">w" & w4s + 4 & "</td>") '</tr>
                    ElseIf w4 = 6 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">w" & w4s & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w4s + 1 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w4s + 2 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w4s + 3 & "</td><td width=""20"" class=""plainlabel box1ltop"">w" & w4s + 4 & "</td><td width=""20"" class=""plainlabel box1ltopend"">w" & w4s + 5 & "</td>") '</tr>
                    End If


                    sb.Append("</tr>")
                End If

                sb.Append("<tr>")
                sb.Append("<td class=""plainlabel box1ltopbg"" align=""center"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel box1ltopbg"" align=""center"">" & func & " - " & compnum & "(" & cqty & ")</td>")
                sb.Append("<td class=""plainlabel box1ltopbg"" align=""center""></td>")
                sb.Append("<td class=""plainlabel box1ltopbg"" align=""center""></td>")

                Dim test1 As String = w4
                If w1 = 4 Then
                    sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                ElseIf w1 = 5 Then
                    sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                ElseIf w1 = 6 Then
                    sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                End If
                If w2 = 4 Then
                    sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                ElseIf w2 = 5 Then
                    sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                ElseIf w2 = 6 Then
                    sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                End If
                If w3 = 4 Then
                    sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                ElseIf w3 = 5 Then
                    sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                ElseIf w3 = 6 Then
                    sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                End If
                If w4 = 4 Then
                    sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltopend"">&nbsp;</td>") '</tr>
                ElseIf w4 = 5 Then
                    sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltopend"">&nbsp;</td>") '</tr>
                ElseIf w4 = 6 Then
                    sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltopend"">&nbsp;</td>") '</tr>
                End If
                sb.Append("</tr>")

                If taskcnt = 12 Or currt = tcnt Then
                    sb.Append("<tr>")
                    sb.Append("<td class=""plainlabel box1lnext"" align=""left"">" & pmtskid & "-" & tasknum & "</td>")
                    sb.Append("<td class=""plainlabel box1lnext"" align=""left"">" & taskdesc & "" & fm1 & "</td>") '& compnum & " - " 
                    Try
                        freqi = freq
                    Catch ex As Exception

                    End Try
                    If freqi <= 7 Then 'freq = "7"
                        sb.Append("<td class=""plainlabel box1lnext"" align=""center""></td>")
                        sb.Append("<td class=""plainlabel box1lnext"" align=""center"">x</td>")
                    Else
                        sb.Append("<td class=""plainlabel box1lnext"" align=""center"">x</td>")
                        sb.Append("<td class=""plainlabel box1lnext"" align=""center""></td>")
                    End If

                    Dim test2 As String = w4
                    If w1 = 4 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td>") '</tr>
                    ElseIf w1 = 5 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td>") '</tr>
                    ElseIf w1 = 6 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td>") '</tr>
                    End If
                    If w2 = 4 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td>") '</tr>
                    ElseIf w2 = 5 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td>") '</tr>
                    ElseIf w2 = 6 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td>") '</tr>
                    End If
                    If w3 = 4 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td>") '</tr>
                    ElseIf w3 = 5 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td>") '</tr>
                    ElseIf w3 = 6 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td>") '</tr>
                    End If
                    If w4 = 4 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnextend"">&nbsp;</td>") '</tr>
                    ElseIf w4 = 5 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnextend"">&nbsp;</td>") '</tr>
                    ElseIf w4 = 6 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnext"">&nbsp;</td><td width=""20"" class=""plainlabel box1lnextend"">&nbsp;</td>") '</tr>
                    End If
                    sb.Append("</tr>")
                    'sb.Append("</td></tr>")
                    sb.Append("</table>")
                    taskcnt = 0
                Else
                    sb.Append("<tr>")
                    sb.Append("<td class=""plainlabel box1ltop"" align=""left"">" & pmtskid & "-" & tasknum & "</td>")
                    sb.Append("<td class=""plainlabel box1ltop"" align=""left"">" & taskdesc & "" & fm1 & "</td>") '& compnum & " - " 
                    Try
                        freqi = freq
                    Catch ex As Exception

                    End Try
                    If freqi <= 7 Then 'freq = "7"
                        sb.Append("<td class=""plainlabel box1ltop"" align=""center""></td>")
                        sb.Append("<td class=""plainlabel box1ltop"" align=""center"">x</td>")
                    Else
                        sb.Append("<td class=""plainlabel box1ltop"" align=""center"">x</td>")
                        sb.Append("<td class=""plainlabel box1ltop"" align=""center""></td>")
                    End If

                    Dim test2 As String = w4
                    If w1 = 4 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                    ElseIf w1 = 5 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                    ElseIf w1 = 6 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                    End If
                    If w2 = 4 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                    ElseIf w2 = 5 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                    ElseIf w2 = 6 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                    End If
                    If w3 = 4 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                    ElseIf w3 = 5 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                    ElseIf w3 = 6 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td>") '</tr>
                    End If
                    If w4 = 4 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltopend"">&nbsp;</td>") '</tr>
                    ElseIf w4 = 5 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltopend"">&nbsp;</td>") '</tr>
                    ElseIf w4 = 6 Then
                        sb.Append("<td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltop"">&nbsp;</td><td width=""20"" class=""plainlabel box1ltopend"">&nbsp;</td>") '</tr>
                    End If
                    sb.Append("</tr>")
                End If

            End While
            dr.Close()

            'sb.Append("</td></tr>")
            sb.Append("</table>")

            dvrep.InnerHtml = sb.ToString
        Else
            sb.Append("<table><tr>")
            sb.Append("<td class=""plainlabel"">No Records Found</td>")
            sb.Append("</tr></table>")
            dvrep.InnerHtml = sb.ToString
        End If
        
    End Sub
    Private Function getmonth(ByVal mnth As String) As String
        Dim retm As String
        Select Case mnth
            Case 1
                retm = "JANUARY"
            Case 2
                retm = "FEBRUARY"
            Case 3
                retm = "MARCH"
            Case 4
                retm = "APRIL"
            Case 5
                retm = "MAY"
            Case 6
                retm = "JUNE"
            Case 7
                retm = "JULY"
            Case 8
                retm = "AUGUST"
            Case 9
                retm = "SEPTEMBER"
            Case 10
                retm = "OCTOBER"
            Case 11
                retm = "NOVEMBER"
            Case 12
                retm = "DECEMBER"

        End Select
        Return retm
    End Function
End Class