Imports System.Data.SqlClient
Public Class eqdetails
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim ds As DataSet
    Dim gtasks As New Utilities
    Protected WithEvents dgexport As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dlhd As System.Web.UI.WebControls.DataList
    Dim eqid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            BindHead()
            BindExport(eqid)
            cmpDataGridToExcel.DataGridToExcel(dgexport, dlhd, Response)
            gtasks.Dispose()
        End If
    End Sub
    Private Sub BindExport(ByVal eq As String)
        sql = "select distinct e.eqid,e.eqnum, f.func, c.compnum " _
        + "from equipment e " _
        + "left outer join functions f on f.eqid = e.eqid " _
        + "left outer join components c on c.func_id = f.func_id " _
        + "where e.eqid = '" & eq & "'"
        gtasks.Open()
        ds = gtasks.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        dgexport.DataSource = dv
        dgexport.DataBind()
        gtasks.Dispose()
    End Sub
    Private Sub BindHead()

    End Sub
End Class

