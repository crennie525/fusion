<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AssignmentReportTPM.aspx.vb" Inherits="lucy_r12.AssignmentReportTPM" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AssignmentReportTPM</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<LINK href="../styles/reports.css" type="text/css" rel="stylesheet">
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/AssignmentReportTPMaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<TABLE id="tblexport" width="1000" runat="server">
				<tr>
					<td id="tdwi" align="center" runat="server"></td>
				</tr>
			</TABLE>
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
