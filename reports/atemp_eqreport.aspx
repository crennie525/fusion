﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="atemp_eqreport.aspx.vb"
    Inherits="lucy_r12.atemp_eqreport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <link rel="stylesheet" type="text/css" href="../styles/reports.css">
</head>
<body>
    <form id="form1" runat="server">
    <table cellpadding="3" cellspacing="3" width="620" align="center">
        <tr>
            <td class="label" align="center" colspan="2">
                <h2>
                    <span id="lang3318">Equipment Hierarchy Report</span></h2>
            </td>
        </tr>
        <tr height="20">
            <td class="label" width="80">
                <span id="lang3319">Plant Site:</span>
            </td>
            <td id="tdsite" class="plainlabel" width="540">
                Practice Site
            </td>
        </tr>
        <tr height="20">
            <td class="label">
                <span id="lang3320">Department:</span>
            </td>
            <td id="tddept" class="plainlabel">
                Practice Dept 1
            </td>
        </tr>
        <tr height="20">
            <td class="label">
                <span id="lang3321">Station/Cell:</span>
            </td>
            <td id="tdcell" class="plainlabel">
                Practice Cell 1
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <a id="a1fid" href="#" onclick="openfid('fid');" class="labellink"><span id="lang3322">
                    Expand All Functions</span></a> <a id="a2fid" href="#" onclick="closefid('fid');"
                        class="labellink details"><span id="lang3323">Collapse All Functions</span></a>&nbsp;&nbsp;&nbsp;<a
                            id="a1cid" href="#" onclick="openfid('cid');" class="labellink"><span id="lang3324">Expand
                                All Components</span></a> <a id="a2cid" href="#" onclick="closefid('cid');" class="labellink details">
                                    <span id="lang3325">Collapse All Components</span></a>
            </td>
        </tr>
        <tr>
            <td id="tdarch" colspan="2">
                <table cellspacing="0" width="425" table border="0">
                    <tr>
                        <td width="15">
                        </td>
                        <td width="15">
                        </td>
                        <td width="15">
                        </td>
                        <td width="15">
                        </td>
                        <td width="365">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img id='i97' onclick="fclose('t97', 'i97');" src="../images/appbuttons/bgbuttons/minus.gif">
                        </td>
                        <td colspan="4" class="plainlabel">
                            <b>Equipment#</b>&nbsp;&nbsp;AHU Tester - Air Handling Unit Train 1 & 2
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <table class="view" width="415" cellspacing="0" id='t97' table border="0">
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td colspan="3" class="label">
                                        <u>Functions</u>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15">
                                    </td>
                                    <td width="15">
                                    </td>
                                    <td width="15">
                                    </td>
                                    <td width="15">
                                    </td>
                                    <td width="355">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <img id='in01106' onclick="fclose('t01106', 'in01106','no');" src="../images/appbuttons/bgbuttons/minusg1.gif">
                                    </td>
                                    <td colspan="3" class="plainlabel">
                                        Mixing Chamber
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <table class="details" width="405" cellspacing="0" id='t01106' border="0">
                                            <tr>
                                                <td colspan="3">
                                                    &nbsp;
                                                </td>
                                                <td colspan="2" class="label">
                                                    <u>Components</u>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="15">
                                                </td>
                                                <td width="15">
                                                </td>
                                                <td width="15">
                                                </td>
                                                <td width="15">
                                                </td>
                                                <td width="345">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <img id='in01106933' onclick="fclose('t01106933', 'in01106933','no');" src="../images/appbuttons/bgbuttons/minusg1.gif">
            </td>
            <td colspan="3" class="plainlabel">
                Pre filter chamber
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <table class="details" width="405" cellspacing="0" id='t01106933' border="0">
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                        <td colspan="2" class="label">
                            <u>Components</u>
                        </td>
                    </tr>
                    <tr>
                        <td width="15">
                        </td>
                        <td width="15">
                        </td>
                        <td width="15">
                        </td>
                        <td width="15">
                        </td>
                        <td width="345">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </td></tr></table>
    </td></tr><tr>
        <td>
            &nbsp;
        </td>
        <td>
            <img id='in01106933397' onclick="fclose('t01106933397', 'in01106933397','no');" src="../images/appbuttons/bgbuttons/minusg1.gif">
        </td>
        <td colspan="3" class="plainlabel">
            Post filter chamber
        </td>
    </tr>
    <tr>
        <td colspan="5">
            <table class="details" width="405" cellspacing="0" id='t01106933397' border="0">
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                    <td colspan="2" class="label">
                        <u>Components</u>
                    </td>
                </tr>
                <tr>
                    <td width="15">
                    </td>
                    <td width="15">
                    </td>
                    <td width="15">
                    </td>
                    <td width="15">
                    </td>
                    <td width="345">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </td></tr></table></td></tr><tr>
        <td>
            &nbsp;
        </td>
        <td>
            <img id='in01106933397533' onclick="fclose('t01106933397533', 'in01106933397533','no');"
                src="../images/appbuttons/bgbuttons/minusg1.gif">
        </td>
        <td colspan="3" class="plainlabel">
            Supply fan chamber
        </td>
    </tr>
    <tr>
        <td colspan="5">
            <table class="details" width="405" cellspacing="0" id='t01106933397533' border="0">
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                    <td colspan="2" class="label">
                        <u>Components</u>
                    </td>
                </tr>
                <tr>
                    <td width="15">
                    </td>
                    <td width="15">
                    </td>
                    <td width="15">
                    </td>
                    <td width="15">
                    </td>
                    <td width="345">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </td></tr></table></td></tr><tr>
        <td>
            &nbsp;
        </td>
        <td>
            <img id='in01106933397533393' onclick="fclose('t01106933397533393', 'in01106933397533393','no');"
                src="../images/appbuttons/bgbuttons/minusg1.gif">
        </td>
        <td colspan="3" class="plainlabel">
            Dehumidifier
        </td>
    </tr>
    <tr>
        <td colspan="5">
            <table class="details" width="405" cellspacing="0" id='t01106933397533393' border="0">
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                    <td colspan="2" class="label">
                        <u>Components</u>
                    </td>
                </tr>
                <tr>
                    <td width="15">
                    </td>
                    <td width="15">
                    </td>
                    <td width="15">
                    </td>
                    <td width="15">
                    </td>
                    <td width="345">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </td></tr></table></td></tr></table></td> </tr> </table>
    </form>
</body>
</html>
