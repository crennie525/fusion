﻿Imports System.Data.SqlClient
Public Class emd_reps
    Inherits System.Web.UI.Page
    Dim rep As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim sid, fdate, tdate, who, nodate As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            fdate = Request.QueryString("fdate").ToString
            tdate = Request.QueryString("tdate").ToString
            who = Request.QueryString("who").ToString
            rep.Open()
            getrep(sid, fdate, tdate, who)
            rep.Dispose()
        End If
    End Sub
    Private Sub getrep(ByVal sid As String, ByVal fdate As String, ByVal tdate As String, ByVal who As String)

        Dim title As String
        If who = "open_no" Then
            sql = "select Convert(char(10),'" & Now & "',101)"
            nodate = rep.strScalar(sql)
            sql = "usp_e_wobwtbsup '" & sid & "'"
            title = "Number of Open Jobs by Work Type and Supervisor"
        ElseIf who = "open_dt" Then
            sql = "usp_e_wobwtbsup_dtr '" & fdate & "','" & tdate & "','" & sid & "'"
            title = "Number of Open Jobs by Work Type and Supervisor"
        ElseIf who = "closed_dt" Then
            sql = "usp_e_wobwtbsup__cl_dtr '" & fdate & "','" & tdate & "','" & sid & "'"
            title = "Number of Closed Jobs by Work Type and Supervisor"
        End If
        Dim sb As New StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""720"">")
        sb.Append("<tr><td class=""replabeltop"" colspan=""19"" align=""center"">" & title & "</td>")
        If who = "open_dt" Or who = "closed_dt" Then
            sb.Append("<tr><td class=""replabelplain"" colspan=""19"" align=""center"">From " & fdate & " To " & tdate & "</td>")
        Else
            sb.Append("<tr><td class=""replabelplain"" colspan=""19"" align=""center"">" & nodate & "</td>")
        End If
        sb.Append("<tr><td colspan=""19"">&nbsp;</td></tr>")
        sb.Append("<tr><td width=""160""></td>")
        sb.Append("<td width=""32""></td><td width=""28""></td><td width=""32""></td><td width=""32""></td><td width=""32""></td>")
        sb.Append("<td width=""28""></td><td width=""28""></td><td width=""28""></td><td width=""32""></td><td width=""28""></td>")
        sb.Append("<td width=""28""></td><td width=""28""></td><td width=""28""></td><td width=""28""></td><td width=""28""></td>")
        sb.Append("<td width=""28""></td><td width=""28""></td><td width=""60""></td></tr>")

        sb.Append("<tr><td class=""tboxl1"">Supervisor</td>")
        sb.Append("<td class=""tboxl"">CH O</td><td class=""tboxl"">CM</td><td class=""tboxl"">CO DP</td><td class=""tboxl"">CO SA</td><td class=""tboxl"">CO CM</td>")
        sb.Append("<td class=""tboxl"">DP</td><td class=""tboxl"">EN</td><td class=""tboxl"">FR</td><td class=""tboxl"">MAXPM</td><td class=""tboxl"">PET</td>")
        sb.Append("<td class=""tboxl"">PM</td><td class=""tboxl"">PROJ</td><td class=""tboxl"">RCM</td><td class=""tboxl"">SA</td><td class=""tboxl"">TPM</td>")
        sb.Append("<td class=""tboxl"">TR</td><td class=""tboxl"">WR</td><td class=""tboxr"">Total</td></tr>")
        ''update @tbl2 set [Total] = ([CH O] + [CM] + [CO DP] + [CO SA] + [CO CM] + [DP]
        '+ [EN] + [FR] + [MAXPM] + [PET] + [PM] + [PROJ] + [RCM] + [SA] + [TPM] + [TR]
        '+ [WR])
        Dim cho As Integer = 0
        Dim cm As Integer = 0
        Dim codp As Integer = 0
        Dim cosa As Integer = 0
        Dim cocm As Integer = 0
        Dim dp As Integer = 0
        Dim en As Integer = 0
        Dim fr As Integer = 0
        Dim maxpm As Integer = 0
        Dim pet As Integer = 0
        Dim pm As Integer = 0
        Dim proj As Integer = 0
        Dim rcm As Integer = 0
        Dim sa As Integer = 0
        Dim tpm As Integer = 0
        Dim tr As Integer = 0
        Dim wr As Integer = 0
        Dim tot As Integer = 0

        dr = rep.GetRdrData(sql)
        While dr.Read
            Try
                cho += dr.Item("CH O").ToString
            Catch ex As Exception
                cho += 0
            End Try
            Try
                cm += dr.Item("CM").ToString
            Catch ex As Exception
                cm += 0
            End Try
            Try
                codp += dr.Item("CO DP").ToString
            Catch ex As Exception
                codp += 0
            End Try
            Try
                cosa += dr.Item("CO SA").ToString
            Catch ex As Exception
                cosa += 0
            End Try
            Try
                cocm += dr.Item("CO CM").ToString
            Catch ex As Exception
                cocm += 0
            End Try
            Try
                dp += dr.Item("DP").ToString
            Catch ex As Exception
                dp += 0
            End Try
            Try
                en += dr.Item("EN").ToString
            Catch ex As Exception
                en += 0
            End Try
            Try
                fr += dr.Item("FR").ToString
            Catch ex As Exception
                fr += 0
            End Try
            Try
                maxpm += dr.Item("MAXPM").ToString
            Catch ex As Exception
                maxpm += 0
            End Try
            Try
                pet += dr.Item("PET").ToString
            Catch ex As Exception
                pet += 0
            End Try
            Try
                pm += dr.Item("PM").ToString
            Catch ex As Exception
                pm += 0
            End Try
            Try
                proj = dr.Item("PROJ").ToString
            Catch ex As Exception
                proj = 0
            End Try
            Try
                rcm += dr.Item("RCM").ToString
            Catch ex As Exception
                rcm += 0
            End Try
            Try
                sa += dr.Item("SA").ToString
            Catch ex As Exception
                sa += 0
            End Try
            Try
                tpm += dr.Item("TPM").ToString
            Catch ex As Exception
                tpm += 0
            End Try
            Try
                tr += dr.Item("TR").ToString
            Catch ex As Exception
                tr += 0
            End Try
            Try
                wr += dr.Item("WR").ToString
            Catch ex As Exception
                wr += 0
            End Try
            Try
                tot += dr.Item("Total").ToString
            Catch ex As Exception
                tot += 0
            End Try
            sb.Append("<tr><td class=""rboxl1"">" & dr.Item("Supervisor").ToString & "</td>")
            sb.Append("<td class=""rboxl"">" & dr.Item("CH O").ToString & "</td><td class=""rboxl"">" & dr.Item("CM").ToString & "</td><td class=""rboxl"">" & dr.Item("CO DP").ToString & "</td><td class=""rboxl"">" & dr.Item("CO SA").ToString & "</td><td class=""rboxl"">" & dr.Item("CO CM").ToString & "</td>")
            sb.Append("<td class=""rboxl"">" & dr.Item("DP").ToString & "</td><td class=""rboxl"">" & dr.Item("EN").ToString & "</td><td class=""rboxl"">" & dr.Item("FR").ToString & "</td><td class=""rboxl"">" & dr.Item("MAXPM").ToString & "</td><td class=""rboxl"">" & dr.Item("PET").ToString & "</td>")
            sb.Append("<td class=""rboxl"">" & dr.Item("PM").ToString & "</td><td class=""rboxl"">" & dr.Item("PROJ").ToString & "</td><td class=""rboxl"">" & dr.Item("RCM").ToString & "</td><td class=""rboxl"">" & dr.Item("SA").ToString & "</td><td class=""rboxl"">" & dr.Item("TPM").ToString & "</td>")
            sb.Append("<td class=""rboxl"">" & dr.Item("TR").ToString & "</td><td class=""rboxl"">" & dr.Item("WR").ToString & "</td><td class=""rboxr"">" & dr.Item("Total").ToString & "</td></tr>")
        End While
        dr.Close()

        sb.Append("<tr><td class=""rboxl1""><b>Totals</b></td>")
        sb.Append("<td class=""rboxl""><b>" & cho & "</b></td><td class=""rboxl""><b>" & cm & "</b></td><td class=""rboxl""><b>" & codp & "</b></td><td class=""rboxl""><b>" & cosa & "</b></td><td class=""rboxl""><b>" & cocm & "</b></td>")
        sb.Append("<td class=""rboxl""><b>" & dp & "</b></td><td class=""rboxl""><b>" & en & "</b></td><td class=""rboxl""><b>" & fr & "</b></td><td class=""rboxl""><b>" & maxpm & "</b></td><td class=""rboxl""><b>" & pet & "</b></td>")
        sb.Append("<td class=""rboxl""><b>" & pm & "</b></td><td class=""rboxl""><b>" & proj & "</b></td><td class=""rboxl""><b>" & rcm & "</b></td><td class=""rboxl""><b>" & sa & "</b></td><td class=""rboxl""><b>" & tpm & "</b></td>")
        sb.Append("<td class=""rboxl""><b>" & tr & "</b></td><td class=""rboxl""><b>" & wr & "</b></td><td class=""rboxr""><b>" & tot & "</b></b></td></tr>")

        sb.Append("</table>")
        divrep.InnerHtml = sb.ToString

    End Sub
End Class