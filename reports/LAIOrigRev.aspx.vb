

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class LAIOrigRev
    Inherits System.Web.UI.Page
	Protected WithEvents lang3340 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3339 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim lai As New Utilities
    Dim sql As String
    Protected WithEvents btnexp As System.Web.UI.WebControls.Button
    Protected WithEvents dgexport As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgexportrev As System.Web.UI.WebControls.DataGrid
    Protected WithEvents ddout As System.Web.UI.WebControls.DropDownList
    Dim dr As SqlDataReader

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddeq As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            lai.Open()
            PopEq()
            lai.Dispose()
        End If
    End Sub
    Private Sub PopEq()
        Dim dt, val, filt As String
        filt = ""
        dt = "equipment"
        Val = "eqid, eqnum "
        sql = "select * from equipment order by eqnum"
        dr = lai.GetRdrData(sql) 'lai.GetList(dt, val, filt)
        ddeq.DataSource = dr
        ddeq.DataTextField = "eqnum"
        ddeq.DataValueField = "eqid"
        ddeq.DataBind()
        dr.Close()
        ddeq.Items.Insert(0, "Select Equipment")
    End Sub
    Private Sub BindExport(ByVal eq As String)
        Dim ds As New DataSet
        sql = "usp_optValLAIRevOrig '" & eq & "'"
        'lai.Open()
        ds = lai.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        dgexport.DataSource = dv
        dgexport.DataBind()
        'lai.Dispose()
    End Sub
    Private Sub BindExportRev(ByVal eq As String)
        Dim ds As New DataSet
        sql = "usp_optValLAIRevRev '" & eq & "'"
        'lai.Open()
        ds = lai.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        dgexportrev.DataSource = dv
        dgexportrev.DataBind()
        'lai.Dispose()
    End Sub

    Private Sub ddeq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddeq.SelectedIndexChanged
        Try
            Dim eq As String
            If ddout.SelectedIndex <> 0 Then
                If ddeq.SelectedIndex <> 0 Then
                    lai.Open()
                    If ddout.SelectedIndex = 1 Then
                        eq = ddeq.SelectedValue.ToString
                        dgexportrev.DataSource = Nothing
                        dgexportrev.DataBind()
                        dgexportrev.Dispose()
                        BindExport(eq)
                    Else
                        eq = ddeq.SelectedValue.ToString
                        dgexport.DataSource = Nothing
                        dgexport.DataBind()
                        dgexport.Dispose()
                        BindExportRev(eq)
                    End If
                    lai.Dispose()
                End If
            End If
        Catch ex As Exception
            Response.Redirect("LAIOrigRev.aspx")
        End Try
        
    End Sub

    Private Sub btnexp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnexp.Click
       
        Dim eq As String
        If ddout.SelectedIndex <> 0 Then
            If ddeq.SelectedIndex <> 0 Then
                lai.Open()
                If ddout.SelectedIndex = 1 Then
                    eq = ddeq.SelectedValue.ToString
                    dgexportrev.DataSource = Nothing
                    dgexportrev.DataBind()
                    dgexportrev.Dispose()
                    BindExport(eq)
                    cmpDataGridToExcel.DataGridToExcelNHD(dgexport, Response)
                Else
                    eq = ddeq.SelectedValue.ToString
                    dgexport.DataSource = Nothing
                    dgexport.DataBind()
                    dgexport.Dispose()
                    BindExportRev(eq)
                    cmpDataGridToExcel.DataGridToExcelNHD(dgexportrev, Response)
                End If
                lai.Dispose()
            End If
        End If
        

    End Sub

    Private Sub ddout_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddout.SelectedIndexChanged
        Try
            Dim eq As String
            If ddout.SelectedIndex <> 0 Then
                If ddeq.SelectedIndex <> 0 Then
                    lai.Open()
                    If ddout.SelectedIndex = 1 Then
                        eq = ddeq.SelectedValue.ToString
                        dgexportrev.DataSource = Nothing
                        dgexportrev.DataBind()
                        dgexportrev.Dispose()
                        BindExport(eq)
                    Else
                        eq = ddeq.SelectedValue.ToString
                        dgexport.DataSource = Nothing
                        dgexport.DataBind()
                        dgexport.Dispose()
                        BindExportRev(eq)
                    End If
                    lai.Dispose()
                End If
            End If
        Catch ex As Exception
            Response.Redirect("LAIOrigRev.aspx")
        End Try
        
    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang3339.Text = axlabs.GetASPXPage("LAIOrigRev.aspx","lang3339")
		Catch ex As Exception
		End Try
		Try
			lang3340.Text = axlabs.GetASPXPage("LAIOrigRev.aspx","lang3340")
		Catch ex As Exception
		End Try

	End Sub

End Class
