Imports System.Data.SqlClient
Public Class ttno
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim tmod As New transmod
    Dim eqid, fuid, cid, tl, sid, did, clid, chk, tasknum, who As String
    Dim skillid, skillqty, rdid, meterid, freq As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwi As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlbl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdass As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    'skillid, skillqty, rdid, meterid, freq
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillqty As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrdid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmeterid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        If comi = "SUN" Then
            lblfslang.Value = "fre"
        End If
        'END ADD FOR SUNCOR
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            fuid = Request.QueryString("fuid").ToString
            lblfuid.Value = fuid
            cid = Request.QueryString("cid").ToString 'HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            tl = Request.QueryString("tl").ToString
            lbltasklev.Value = tl
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            did = Request.QueryString("did").ToString
            lbldid.Value = did
            clid = Request.QueryString("clid").ToString
            lblclid.Value = clid
            chk = Request.QueryString("chk").ToString
            lblchk.Value = chk
            who = Request.QueryString("who").ToString
            lblwho.Value = who
            If who = "tot" Then
                'skillid, skillqty, rdid, meterid, freq
                skillid = Request.QueryString("skillid").ToString
                skillqty = Request.QueryString("skillqty").ToString
                rdid = Request.QueryString("rdid").ToString
                meterid = Request.QueryString("meterid").ToString
                freq = Request.QueryString("freq").ToString
                lblskillid.Value = skillid
                lblskillqty.Value = skillqty
                lblrdid.Value = rdid
                lblmeterid.Value = meterid
                lblfreq.Value = freq
            End If
            If who = "ts" Then
                tdpm.InnerHtml = "Tasks without a Task Type"
            ElseIf (who = "tt") Then
                tdpm.InnerHtml = "Tasks without a Task Status"
            Else
                tdpm.InnerHtml = "Selected Profile Tasks"
            End If
            'tasknum = Request.QueryString("tasknum").ToString
            'lbltasknum.Value = tasknum
            'tdpm.InnerHtml = tmod.getxlbl("xlb459", "ttno.aspx.vb")
            'tdlbl.InnerHtml = tmod.getxlbl("xlb459a", "ttno.aspx.vb")
            tdass.InnerHtml = tmod.getxlbl("xlb256", "AssignmentReport.aspx.vb")
            pms.Open()
            PopMD(eqid, fuid, who)
            pms.Dispose()
            'Else
            'If Request.Form("lbltasknum") <> "" Then
            'GetTask()
            'End If
        End If
    End Sub
    Function GetTask()
        Dim tasknum As String = lbltasknum.Value
        tl = lbltasklev.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        chk = lblchk.Value
        fuid = lblfuid.Value
        Response.Redirect("PMTaskDivFunc.aspx?start=yes&tl=5&chk=" & chk & "&cid=" & _
        cid + "&fuid=" & fuid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & _
        "&eqid=" & eqid & "&task=" & tasknum)
    End Function
    Private Sub PopMD(ByVal eqid As String, ByVal fuid As String, ByVal who As String)
        Dim fid As String = ""
        Dim eqnum, eqdesc As String
        Dim task, comp, freq, craft, rd, taskdesc, fm, coid, tskid As String
        Dim aid As String
        Dim aidi As Integer = 1
        'skillid, skillqty, rdid, meterid, freq
        skillid = lblskillid.Value
        skillqty = lblskillqty.Value
        rdid = lblrdid.Value
        meterid = lblmeterid.Value
        freq = lblfreq.Value
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"" border=""0"">")
        sb.Append("<tr><td width=""20""></td><td width=""220""></td><td width=""120""></td><td width=""120""></td><td width=""120""></td></tr>")
        If who = "ts" Then
            sql = "declare @tbl table(tasknum int, eqnum varchar(100), eqdesc varchar(200), func_id int, func varchar(200), comid int, compnum varchar(200), taskstatus varchar(100), orig varchar(100), rev varchar(100)) " _
                + "insert into @tbl " _
                + "select distinct p.tasknum, e.eqnum, e.eqdesc, f.func_id, f.func, c.comid, c.compnum, p.taskstatus, " _
                + "case when p.origtasktype is null then 'N' when len(p.origtasktype) = 0 then 'N' else p.origtasktype end, " _
                + "case when p.tasktype is null then 'N' when len(p.tasktype) = 0 then 'N' else p.tasktype end " _
                + "from pmtasks p " _
            + "left join equipment e on e.eqid = p.eqid " _
            + "left join functions f on f.func_id = p.funcid " _
            + "left join components c on c.comid = p.comid " _
                + "where ((p.tasktype is null or len(p.tasktype) = 0) or (p.origtasktype is null or len(p.origtasktype) = 0))  " _
                + "and p.taskstatus <> 'Delete' and taskstatus in ('Unchanged', 'Revised')" _
                + "and e.eqid = '" & eqid & "' and p.subtask = 0 " _
                + "order by e.eqnum, f.func, c.compnum " _
                + "insert into @tbl " _
                + "select distinct p.tasknum, e.eqnum, e.eqdesc, f.func_id, f.func, c.comid, c.compnum, p.taskstatus, " _
                + "case when p.origtasktype is null then 'N' when len(p.origtasktype) = 0 then 'N' else p.origtasktype end, " _
                 + "'N\A' " _
                + "from pmtasks p " _
            + "left join equipment e on e.eqid = p.eqid " _
            + "left join functions f on f.func_id = p.funcid " _
            + "left join components c on c.comid = p.comid " _
                + "where (p.origtasktype is null or len(p.origtasktype) = 0)  and p.taskstatus = 'Delete' " _
                + "and e.eqid = '" & eqid & "' and p.subtask = 0 " _
                + "order by e.eqnum, f.func, c.compnum " _
                + "insert into @tbl " _
                + "select distinct p.tasknum, e.eqnum, e.eqdesc, f.func_id, f.func, c.comid, c.compnum, p.taskstatus, " _
                 + "'N\A', " _
                + "case when p.origtasktype is null then 'N' when len(p.origtasktype) = 0 then 'N' else p.tasktype end " _
               + "from pmtasks p " _
            + "left join equipment e on e.eqid = p.eqid " _
            + "left join functions f on f.func_id = p.funcid " _
            + "left join components c on c.comid = p.comid " _
                + "where (p.tasktype is null or len(p.tasktype) = 0) and p.taskstatus = 'Add' " _
                + "and e.eqid = '" & eqid & "' and p.subtask = 0 " _
                + "order by e.eqnum, f.func, c.compnum " _
                + "select * from @tbl"
            'sql = "select distinct p.tasknum, e.eqnum, e.eqdesc, f.func_id, f.func, c.comid, c.compnum " _
            '+ "from pmtasks p " _
            '+ "left join equipment e on e.eqid = p.eqid " _
            '+ "left join functions f on f.func_id = p.funcid " _
            '+ "left join components c on c.comid = p.comid " _
            '+ "where (p.tasktype is null or len(p.tasktype) = 0)  and p.eqid = '" & eqid & "' " _
            '+ "and p.compnum <> 'Select' and len(p.compnum) > 0 and p.subtask = 0 " _
            '+ "order by e.eqnum, f.func, p.tasknum"
        ElseIf who = "tt" Then
            sql = "select distinct p.tasknum, e.eqnum, e.eqdesc, f.func_id, f.func, c.comid, c.compnum from components c " _
        + "left join functions f on f.func_id = c.func_id " _
        + "left join equipment e on e.eqid = f.eqid " _
        + "left join pmtasks p on e.eqid = e.eqid " _
        + "where p.taskstatus is null or len(p.taskstatus) = 0  " _
        + "and e.eqid = '" & eqid & "' " _
        + "order by e.eqnum, f.func, c.compnum"
            sql = "select distinct p.tasknum, e.eqnum, e.eqdesc, f.func_id, f.func, c.comid, c.compnum " _
            + "from pmtasks p " _
            + "left join equipment e on e.eqid = p.eqid " _
            + "left join functions f on f.func_id = p.funcid " _
            + "left join components c on c.comid = p.comid " _
            + "where (p.taskstatus is null or len(p.taskstatus) = 0)  and p.eqid = '" & eqid & "' " _
            + "and p.compnum <> 'Select' and len(p.compnum) > 0 and p.subtask = 0 " _
            + "order by e.eqnum, f.func, p.tasknum"
        Else
            'skillid, skillqty, rdid, meterid, freq
            If meterid = "" Then
                sql = "select distinct p.tasknum, e.eqnum, e.eqdesc, f.func_id, f.func, c.comid, c.compnum, p.skillid, p.qty, p.rdid, p.meterid, p.freq " _
                            + "from pmtasks p " _
                            + "left join equipment e on e.eqid = p.eqid " _
                            + "left join functions f on f.func_id = p.funcid " _
                            + "left join components c on c.comid = p.comid " _
                            + "where skillid = '" & skillid & "' and " _
                            + "p.eqid = '" & eqid & "' and p.qty = '" & skillqty & "' and p.rdid = '" & rdid & "' and p.freq = '" & freq & "' " _
                            + "and p.compnum <> 'Select' and len(p.compnum) > 0 and p.subtask = 0 " _
                            + "order by e.eqnum, f.func, p.tasknum"
            Else
                sql = "select distinct p.tasknum, e.eqnum, e.eqdesc, f.func_id, f.func, c.comid, c.compnum, p.skillid, p.qty, p.rdid, p.meterid, p.freq " _
                + "from pmtasks p " _
                + "left join equipment e on e.eqid = p.eqid " _
                + "left join functions f on f.func_id = p.funcid " _
                + "left join components c on c.comid = p.comid " _
                + "where skillid = '" & skillid & "' and " _
                + "p.eqid = '" & eqid & "' and p.qty = '" & skillqty & "' and p.rdid = '" & rdid & "' and p.meterid = '" & meterid & "' and p.freq = '" & freq & "' " _
                + "and p.compnum <> 'Select' and len(p.compnum) > 0 and p.subtask = 0 " _
                + "order by e.eqnum, f.func, p.tasknum"
            End If
            
        End If
        Dim ts, orig, rev As String
        dr = pms.GetRdrData(sql)
        While dr.Read
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            If who = "ts" Then
                ts = dr.Item("taskstatus").ToString
                orig = dr.Item("orig").ToString
                rev = dr.Item("rev").ToString
            End If
            If fid <> dr.Item("func_id").ToString Then
                If fid = "" Then
                    sb.Append("<tr><td class=""plainlabel"" colspan=""5""><b>" & tmod.getxlbl("xlb460", "ttno.aspx.vb") & "</b>&nbsp;&nbsp;&nbsp;" & dr.Item("func").ToString & "</td></tr>")
                Else
                    sb.Append("<tr><td class=""plainlabel"" colspan=""5""><br><b>" & tmod.getxlbl("xlb460", "ttno.aspx.vb") & "</b>&nbsp;&nbsp;&nbsp;" & dr.Item("func").ToString & "</td></tr>")
                End If
                If who = "ts" Then
                    sb.Append("<tr><td class=""plainlabel"" align=""center"" colspan=""2""><b><u>Task Number/Component</u></b></td><td class=""plainlabel"" align=""center""><b><u>Tasks Status</u></b></td><td class=""plainlabel""><b><u>Original Task Type</u></b></td><td class=""plainlabel"" align=""center""><b><u>Revised Task Type</u></b></td></tr>")
                End If
            End If
            fid = dr.Item("func_id").ToString
            coid = dr.Item("comid").ToString
            comp = dr.Item("compnum").ToString
            tskid = dr.Item("tasknum").ToString
            aidi += 1
            aid = "aid" & aidi
            If who = "tot" Then
                skillid = dr.Item("skillid").ToString
                skillqty = dr.Item("qty").ToString
                rdid = dr.Item("rdid").ToString
                meterid = dr.Item("meterid").ToString
                freq = dr.Item("freq").ToString
            End If
            ','" & skillid & "','" & skillqty & "'," & rdid & "','" & meterid & "','" & freq & "'
            If who = "ts" Then
                sb.Append("<tr><td class=""plainlabel"" colspan=""2""><A href=""#"" onclick=""gettask('" & fid & "','" & coid & "','" & comp & "','" & aid & "','" & tskid & "');"">" & tskid & "</A>" & "&nbsp;&nbsp;" & comp & "</td>")
                sb.Append("<td class=""plainlabel"" align=""center"">" & ts & "</td><td class=""plainlabel"" align=""center"">" & orig & "</td><td class=""plainlabel"" align=""center"">" & rev & "</td>")
            Else
                sb.Append("<tr><td>&nbsp;</td><td class=""plainlabel"" colspan=""4""><A href=""#"" onclick=""gettask('" & fid & "','" & coid & "','" & comp & "','" & aid & "','" & tskid & "');"">" & tskid & "</A>" & "&nbsp;&nbsp;" & comp & "</td></tr>")
            End If



        End While
        dr.Close()
        pms.Dispose()
        sb.Append("</Table>")
        tdwi.InnerHtml = sb.ToString
        tdeq.InnerHtml = eqnum & "&nbsp;&nbsp;&nbsp;&nbsp;" & eqdesc
    End Sub

End Class
