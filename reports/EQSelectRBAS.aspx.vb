﻿Imports System.Data.SqlClient
Public Class EQSelectRBAS
    Inherits System.Web.UI.Page
    Dim dr As SqlDataReader
    Dim sql As String
    Dim fail As New Utilities
    Dim rteid, eqstr, rte, skillid, qty, freq, rdid, eqid, typ, ptid, sid, days, shift, lang As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        getlabels()
        If Not IsPostBack Then
            rteid = Request.QueryString("rteid").ToString
            lblrteid.Value = rteid
            days = Request.QueryString("days").ToString
            lbldays.Value = days
            shift = Request.QueryString("shift").ToString
            lblshift.Value = shift
            lblopt.Value = "0"
            lbfailmodes.AutoPostBack = False
            lbfailmaster.AutoPostBack = False

            fail.Open()
            GetTaskInfo(rteid)
            PopFail(rteid)
            PopFailList(rteid)
            fail.Dispose()
        End If
    End Sub
    Private Sub getlabels()
        lang = lblfslang.Value
        If lang = "fre" Then
            lang3326.Text = "Sélectionner l'équipement pour une vue d'ensemble"
            lang3327.Text = "Équipement disponible"
            lang3328.Text = "Équipement sélectionné"
            lang3329.Text = "Options de liste"
            lims.Text = "Sélection multiple (utilisez les fèches pour la sélection simple ou multiple)"
            lico.Text = "Cliquez une fois (déplacez l'item en cliquant dessus)"
            lang3330.Text = "Veuillez noter que le mode de sélection multiple doit être utilisé pour retirer un item de la liste des Équipements Disponibles seulement si un item y est présent"

        End If
    End Sub
    Private Sub GetTaskInfo(ByVal rteid As String)
        sql = "select skillid, qty, freq, rdid, route, rttype, ptid, siteid, days, shift from pmroutes where rid = '" & rteid & "'"
        dr = fail.GetRdrData(sql)
        While dr.Read
            skillid = dr.Item("skillid").ToString
            qty = dr.Item("qty").ToString
            freq = dr.Item("freq").ToString
            rdid = dr.Item("rdid").ToString
            rte = dr.Item("route").ToString
            typ = dr.Item("rttype").ToString
            ptid = dr.Item("ptid").ToString
            sid = dr.Item("siteid").ToString
            days = dr.Item("days").ToString
            shift = dr.Item("shift").ToString
        End While
        dr.Close()
        'skillid = "26"
        'qty = "1"
        'freq = "30"
        'rdid = "1"
        'rte = "Sample"
        lblskillid.Value = skillid
        lblqty.Value = qty
        lblfreq.Value = freq
        lblrdid.Value = rdid
        lblrte.Value = rte
        lblrttyp.Value = typ
        lblptid.Value = ptid
        lblsid.Value = sid
        lbldays.Value = days
        lblshift.Value = shift
    End Sub
    Private Sub UpTasks(ByVal eqid As String)
        rteid = lblrteid.Value
        rte = lblrte.Value
        skillid = lblskillid.Value
        qty = lblqty.Value
        freq = lblfreq.Value
        rdid = lblrdid.Value
        typ = lblrttyp.Value
        ptid = lblptid.Value
        days = lbldays.Value
        shift = lblshift.Value
        If ptid = "" Then
            ptid = "0"
        End If
        If typ = "RBAS" Then
            If ptid = "" Or ptid = "0" Then
                sql = "update pmtasks set rteid = '" & rteid & "', rte = '" & rte & "', rteseq = null, rteno = 0 where eqid = '" & eqid & "' and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' and rteid is null or rteid = 0"
            Else
                sql = "update pmtasks set rteid = '" & rteid & "', rte = '" & rte & "', rteseq = null,  rteno = 0 where eqid = '" & eqid & "' and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' and ptid = '" & ptid & "' and rteid is null or rteid = 0"
            End If

        ElseIf typ = "RBAST" Then
            days = Replace(days, "-", ",", , , vbTextCompare)
            If shift = "" Then
                sql = "update pmtaskstpm set rteid = '" & rteid & "', rte = '" & rte & "', rteseq = null,  rteno = 0 where eqid = '" & eqid & "' " _
                + "and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' " _
                + "and rteid is null or rteid = 0  " _
              + "and isnull(taskstatus, '') <> 'Delete' and funcid in (select func_id from functions) " _
              + "and shift1 is null and shift2 is null and shift3 is null and first is null and second is null and third is null "
            ElseIf shift = "1st shift" Then
                sql = "update pmtaskstpm set rteid = '" & rteid & "', rte = '" & rte & "', rteseq = null,  rteno = 0 where eqid = '" & eqid & "' " _
                + "and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' " _
                + "and (rteid is null or rteid = 0) and isnull(taskstatus, '') <> 'Delete'" _
              + "and isnull(taskstatus, '') <> 'Delete' and funcid in (select func_id from functions) " _
              + "and shift1 is not null and first is not null  and first = '" & days & "'"
                'select * from pmtaskstpm where skillid = '2' and qty = '1' and freq = '1' and rdid = '1' and eqid = 22003 
                'and (rteid is null or rteid = 0) and subtask = 0 and isnull(tpmhold, '') <> '1' and isnull(taskstatus, '') <> 'Delete' and siteid = 22 
                'and funcid in (select func_id from functions) 
                'and shift1 is not null and first is not null and first = 'M,Tu,W,Th,F,Sa,Su'
            ElseIf shift = "2nd shift" Then
                sql = "update pmtaskstpm set rteid = '" & rteid & "', rte = '" & rte & "', rteseq = null,  rteno = 0 where eqid = '" & eqid & "' " _
                    + "and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' " _
                    + "and (rteid is null or rteid = 0) " _
                  + "and isnull(taskstatus, '') <> 'Delete' and funcid in (select func_id from functions) " _
                  + "and shift2 is not null and second is not null and second = '" & days & "'"
            ElseIf shift = "3rd shift" Then
                sql = "update pmtaskstpm set rteid = '" & rteid & "', rte = '" & rte & "', rteseq = null,  rteno = 0 where eqid = '" & eqid & "' " _
                        + "and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' " _
                        + "and (rteid is null or rteid = 0) and subtask = 0 " _
                      + "and isnull(taskstatus, '') <> 'Delete' and funcid in (select func_id from functions) " _
                      + "and shift3 is not null and third is not null and third = '" & days & "'"
            End If
            ' and isnull(tpmhold, '') <> '1'
            ' and isnull(tpmhold, '') <> '1'
            ' and isnull(tpmhold, '') <> '1'
            ' and isnull(tpmhold, '') <> '1'
        End If
        'sql = "update pmtasks set rteid = '" & rteid & "', rte = '" & rte & "', rteno = 0 where eqid = '" & eqid & "' and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "'"
        fail.Update(sql)
        If typ = "RBAS" Then
            sql = "usp_rbas_dflt_seq '" & rteid & "','" & skillid & "','" & qty & "','" & freq & "','" & rdid & "'"
            'fail.Update(sql)
        ElseIf typ = "RBAST" Then
            sql = "usp_rbas_dflt_seqT '" & rteid & "','" & skillid & "','" & qty & "','" & freq & "','" & rdid & "'"
            'fail.Update(sql)
        End If
    End Sub
    Private Sub UnTasks(ByVal eqid As String)
        rteid = lblrteid.Value
        rte = lblrte.Value
        skillid = lblskillid.Value
        qty = lblqty.Value
        freq = lblfreq.Value
        rdid = lblrdid.Value
        typ = lblrttyp.Value
        ptid = lblptid.Value
        days = lbldays.Value
        shift = lblshift.Value
        If ptid = "" Then
            ptid = "0"
        End If
        If typ = "RBAS" Then
            If ptid = "" Or ptid = "0" Then
                sql = "update pmtasks set rteid = NULL, rte = NULL, rteno = 0 where eqid = '" & eqid & "' and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' and rteid = '" & rteid & "'"
            Else
                sql = "update pmtasks set rteid = NULL, rte = NULL, rteno = 0 where eqid = '" & eqid & "' and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' and ptid = '" & ptid & "' and rteid = '" & rteid & "'"
            End If

        ElseIf typ = "RBAST" Then
            days = Replace(days, "-", ",", , , vbTextCompare)
            If shift = "" Then
                sql = "update pmtaskstpm set rteid = NULL, rte = NULL, rteno = 0 where eqid = '" & eqid & "' " _
            + "and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' " _
            + "and rteid = '" & rteid & "' " _
              + "and isnull(taskstatus, '') <> 'Delete' and funcid in (select func_id from functions) " _
              + "and shift1 is null and shift2 is null and shift3 is null and first is null and second is null and third is null "
            ElseIf shift = "1st shift" Then
                sql = "update pmtaskstpm set rteid = NULL, rte = NULL, rteno = 0 where eqid = '" & eqid & "' " _
            + "and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' " _
            + "and rteid = '" & rteid & "' " _
              + "and isnull(taskstatus, '') <> 'Delete' and funcid in (select func_id from functions) " _
              + "and shift1 is not null and first is not null and first = '" & days & "' "
            ElseIf shift = "2nd shift" Then
                sql = "update pmtaskstpm set rteid = NULL, rte = NULL, rteno = 0 where eqid = '" & eqid & "' " _
            + "and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' " _
            + "and rteid = '" & rteid & "' " _
              + "and isnull(taskstatus, '') <> 'Delete' and funcid in (select func_id from functions) " _
              + "and shift2 is not null and second is not null and second = '" & days & "'"
            ElseIf shift = "3rd shift" Then
                sql = "update pmtaskstpm set rteid = NULL, rte = NULL, rteno = 0 where eqid = '" & eqid & "' " _
                    + "and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' " _
                    + "and rteid = '" & rteid & "' " _
                      + "and isnull(taskstatus, '') <> 'Delete' and funcid in (select func_id from functions) " _
                      + "and shift3 is not null and third is not null and third = '" & days & "'"
            End If
            ' and isnull(tpmhold, '') <> '1'
            ' and isnull(tpmhold, '') <> '1'
            ' and isnull(tpmhold, '') <> '1'
            ' and isnull(tpmhold, '') <> '1'
        End If
        fail.Update(sql)
        If typ = "RBAS" Then
            sql = "usp_rbas_dflt_seq '" & rteid & "','" & skillid & "','" & qty & "','" & freq & "','" & rdid & "'"
            fail.Update(sql)
        ElseIf typ = "RBAST" Then
            sql = "usp_rbas_dflt_seqT '" & rteid & "','" & skillid & "','" & qty & "','" & freq & "','" & rdid & "'"
            fail.Update(sql)
        End If
    End Sub
    Private Sub PopFail(ByVal rteid As String)
        skillid = lblskillid.Value
        qty = lblqty.Value
        freq = lblfreq.Value
        rdid = lblrdid.Value
        typ = lblrttyp.Value
        days = lbldays.Value
        shift = lblshift.Value
        If typ = "RBAS" Then
            sql = "declare @level2 varchar(2000), @eqstr varchar(2000), @level1 varchar(2000) " _
                + "set @level1 = (select level1 from pmroutes where rid = '" & rteid & "') " _
           + "set @level2 = (select level2 from pmroutes where rid = '" & rteid & "') " _
           + "set @eqstr = (select isnull(eqstr, '') from pmroutes where rid = '" & rteid & "') " _
           + "if @level1 is not null and @level2 is null " _
           + "begin " _
           + "select p.eqid, p.location as eqnum from pmlocations p left join pmlocheir h on h.location = p.location where h.location in " _
           + "(select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level1, ','))) " _
           + "and p.eqid not in (SELECT val FROM dbo.f_split(@eqstr, ',')) " _
           + "and p.eqid in (select eqid from pmtasks where " _
           + "skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' and rteid is null  and isnull(tpmhold, '') <> '1') " _
           + "end " _
            + "else " _
            + "begin " _
            + "select p.eqid, p.location as eqnum from pmlocations p left join pmlocheir h on h.location = p.location where h.location in " _
           + "(select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level2, ','))) " _
           + "and p.eqid not in (SELECT val FROM dbo.f_split(@eqstr, ',')) " _
           + "and p.eqid in (select eqid from pmtasks where " _
           + "skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' and rteid is null and isnull(tpmhold, '') <> '1') " _
           + "end "
            '+ " or h.location in " _
            '+ "(select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level1, ','))) " _
            '
            'declare @level1 varchar(2000), @level2 varchar(2000), @eqstr varchar(2000) 
            'set @level1 = (select level1 from pmroutes where rid = '3036') 
            'set @level2 = (select level2 from pmroutes where rid = '3036') 
            'set @eqstr = (select isnull(eqstr, '') from pmroutes where rid = '3036') 
            'select p.eqid, p.location as eqnum 
            'from pmlocations p 
            'left join pmlocheir h on h.location = p.location
            '-left join equipment e on e.eqid = p.eqid 
            'where h.location in (select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level2, ','))  
            'or h.location in (select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level1, ',')))) 
            'and p.eqid not in (SELECT val FROM dbo.f_split(@eqstr, ',')) and p.siteid = 22 
            'and p.eqid in (select eqid from pmtaskstpm where skillid = '2' and qty = '1' and freq = '1' and rdid = '1' 
            'and rteid is null and subtask = 0 and isnull(taskstatus, '') <> 'Delete' 
            'and funcid in (select func_id from functions) and shift1 is null and shift2 is null 
            'and shift3 is null and first is null and second is null and third is null and siteid = 22)


        ElseIf typ = "RBAST" Then
            days = Replace(days, "-", ",", , , vbTextCompare)
            If shift = "" Then
                sql = "declare @level1 varchar(2000), @level2 varchar(2000), @eqstr varchar(2000) " _
              + "set @level1 = (select level1 from pmroutes where rid = '" & rteid & "') " _
          + "set @level2 = (select level2 from pmroutes where rid = '" & rteid & "') " _
              + "set @eqstr = (select isnull(eqstr, '') from pmroutes where rid = '" & rteid & "') " _
              + "if @level1 is not null and @level2 is null " _
           + "begin " _
              + "select p.eqid, p.location as eqnum from pmlocations p left join pmlocheir h on h.location = p.location where h.location in " _
              + "(select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level1, ','))) " _
              + "and p.eqid not in (SELECT val FROM dbo.f_split(@eqstr, ',')) " _
              + "and p.eqid in (select eqid from pmtaskstpm where " _
              + "skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' and rteid is null and subtask = 0 " _
              + "and isnull(taskstatus, '') <> 'Delete' and funcid in (select func_id from functions) " _
              + "and shift1 is null and shift2 is null and shift3 is null and first is null and second is null and third is null) " _
                + "end " _
            + "else " _
            + "begin " _
            + "select p.eqid, p.location as eqnum from pmlocations p left join pmlocheir h on h.location = p.location where h.location in " _
              + "(select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level2, ','))) " _
              + "and p.eqid not in (SELECT val FROM dbo.f_split(@eqstr, ',')) " _
              + "and p.eqid in (select eqid from pmtaskstpm where " _
              + "skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' and rteid is null and subtask = 0 " _
              + "and isnull(taskstatus, '') <> 'Delete' and funcid in (select func_id from functions) " _
              + "and shift1 is null and shift2 is null and shift3 is null and first is null and second is null and third is null) " _
              + "end"
                ' and isnull(tpmhold, '') <> '1'
                ' and isnull(tpmhold, '') <> '1'
                '+ " or h.location in " _
                '+ "(select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level1, ',')))) " _
            ElseIf shift = "1st shift" Then
                sql = "declare @level1 varchar(2000), @level2 varchar(2000), @eqstr varchar(2000) " _
              + "set @level1 = (select level1 from pmroutes where rid = '" & rteid & "') " _
          + "set @level2 = (select level2 from pmroutes where rid = '" & rteid & "') " _
              + "set @eqstr = (select isnull(eqstr, '') from pmroutes where rid = '" & rteid & "') " _
              + "if @level1 is not null and @level2 is null " _
           + "begin " _
              + "select p.eqid, p.location as eqnum from pmlocations p left join pmlocheir h on h.location = p.location where h.location in " _
              + "(select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level1, ','))) " _
             + "and p.eqid not in (SELECT val FROM dbo.f_split(@eqstr, ',')) " _
             + "and p.eqid in (select eqid from pmtaskstpm where " _
             + "skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' and rteid is null and subtask = 0  " _
             + "and isnull(taskstatus, '') <> 'Delete' and funcid in (select func_id from functions) " _
             + "and shift1 is not null and first is not null and first = '" & days & "') " _
                + "end " _
            + "else " _
            + "begin " _
+ "select p.eqid, p.location as eqnum from pmlocations p left join pmlocheir h on h.location = p.location where h.location in " _
              + "(select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level2, ','))) " _
             + "and p.eqid not in (SELECT val FROM dbo.f_split(@eqstr, ',')) " _
             + "and p.eqid in (select eqid from pmtaskstpm where " _
             + "skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' and rteid is null and subtask = 0  " _
             + "and isnull(taskstatus, '') <> 'Delete' and funcid in (select func_id from functions) " _
             + "and shift1 is not null and first is not null and first = '" & days & "') " _
             + "end"
                ' and isnull(tpmhold, '') <> '1'
                ' and isnull(tpmhold, '') <> '1'
                '+ " or h.location in " _
                '+ "(select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level1, ',')))) " _
            ElseIf shift = "2nd shift" Then
                sql = "declare @level1 varchar(2000), @level2 varchar(2000), @eqstr varchar(2000) " _
              + "set @level1 = (select level1 from pmroutes where rid = '" & rteid & "') " _
          + "set @level2 = (select level2 from pmroutes where rid = '" & rteid & "') " _
              + "set @eqstr = (select isnull(eqstr, '') from pmroutes where rid = '" & rteid & "') " _
              + "if @level1 is not null and @level2 is null " _
           + "begin " _
              + "select p.eqid, p.location as eqnum from pmlocations p left join pmlocheir h on h.location = p.location where h.location in " _
              + "(select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level1, ','))) " _
             + "and p.eqid not in (SELECT val FROM dbo.f_split(@eqstr, ',')) " _
             + "and p.eqid in (select eqid from pmtaskstpm where " _
             + "skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' and rteid is null and subtask = 0 " _
             + "and isnull(taskstatus, '') <> 'Delete' and funcid in (select func_id from functions) " _
             + "and shift2 is not null and second is not null and second = '" & days & "') " _
                                + "end " _
            + "else " _
            + "begin " _
                          + "select p.eqid, p.location as eqnum from pmlocations p left join pmlocheir h on h.location = p.location where h.location in " _
              + "(select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level2, ','))) " _
             + "and p.eqid not in (SELECT val FROM dbo.f_split(@eqstr, ',')) " _
             + "and p.eqid in (select eqid from pmtaskstpm where " _
             + "skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' and rteid is null and subtask = 0 " _
             + "and isnull(taskstatus, '') <> 'Delete' and funcid in (select func_id from functions) " _
             + "and shift2 is not null and second is not null and second = '" & days & "') " _
             + "end"
                ' and isnull(tpmhold, '') <> '1'
                ' and isnull(tpmhold, '') <> '1'
                '+ " or h.location in " _
                '+ "(select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level1, ',')))) " _
            ElseIf shift = "3rd shift" Then
                sql = "declare @level1 varchar(2000), @level2 varchar(2000), @eqstr varchar(2000) " _
              + "set @level1 = (select level1 from pmroutes where rid = '" & rteid & "') " _
          + "set @level2 = (select level2 from pmroutes where rid = '" & rteid & "') " _
              + "set @eqstr = (select isnull(eqstr, '') from pmroutes where rid = '" & rteid & "') " _
              + "if @level1 is not null and @level2 is null " _
           + "begin " _
              + "select p.eqid, p.location as eqnum from pmlocations p left join pmlocheir h on h.location = p.location where h.location in " _
              + "(select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level1, ','))) " _
                             + "and p.eqid not in (SELECT val FROM dbo.f_split(@eqstr, ',')) " _
                             + "and p.eqid in (select eqid from pmtaskstpm where " _
                             + "skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' and rteid is null and subtask = 0 " _
                             + "and isnull(taskstatus, '') <> 'Delete' and funcid in (select func_id from functions) " _
                             + "and shift3 is not null and third is not null and third = '" & days & "') " _
                              + "end " _
            + "else " _
            + "begin " _
                + "select p.eqid, p.location as eqnum from pmlocations p left join pmlocheir h on h.location = p.location where h.location in " _
              + "(select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level2, ','))) " _
                             + "and p.eqid not in (SELECT val FROM dbo.f_split(@eqstr, ',')) " _
                             + "and p.eqid in (select eqid from pmtaskstpm where " _
                             + "skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "' and rteid is null and subtask = 0 " _
                             + "and isnull(taskstatus, '') <> 'Delete' and funcid in (select func_id from functions) " _
                             + "and shift3 is not null and third is not null and third = '" & days & "') " _
                              + "end "
                ' and isnull(tpmhold, '') <> '1'
                ' and isnull(tpmhold, '') <> '1'
                '+ " or h.location in " _
                '+ "(select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level1, ',')))) " _
            End If

        End If
       
        dr = fail.GetRdrData(sql)
        'lbfailmodes.DataSource = dr
        lbfailmaster.DataSource = dr
        lbfailmaster.DataTextField = "eqnum"
        lbfailmaster.DataValueField = "eqid"
        lbfailmaster.DataBind()
        dr.Close()
    End Sub
    Private Sub PopFailList(ByVal rteid As String)
        sql = "declare @eqstr varchar(2000) " _
            + "set @eqstr = (select eqstr from pmroutes where rid = '" & rteid & "') " _
            + "select eqid, location as eqnum from pmlocations where eqid in (SELECT isnull(val, '') FROM dbo.f_split(@eqstr, ',')) " _
            + "and eqid <> 0"
        dr = fail.GetRdrData(sql)
        lbfailmodes.DataSource = dr
        lbfailmodes.DataValueField = "eqid"
        lbfailmodes.DataTextField = "eqnum"
        Try
            lbfailmodes.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        Try
            lbfailmodes.SelectedIndex = 0
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btntocomp_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToComp()
    End Sub
    Private Sub ToComp()
        Dim eqstr As String
        eqstr = lbleqstr.Value
        rteid = lblrteid.Value
        Dim Item As ListItem
        Dim f, fi As String
        fail.Open()
        'sql = "select isnull(eqstr, '') from pmroutes where rid = '" & rteid & "'"
        'eqstr = fail.strScalar(sql)
        If eqstr = "" Then
            sql = "select isnull(eqstr, '') from pmroutes where rid = '" & rteid & "'"
            eqstr = fail.strScalar(sql)
        End If
        lbleqstr.Value = eqstr
        For Each Item In lbfailmaster.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                If Len(lbleqstr.Value) = 0 Then
                    lbleqstr.Value = fi '"'" & fi & '
                    eqstr = "'" & fi & "'"
                    sql = "update pmroutes set eqstr = " & eqstr & " where rid = '" & rteid & "'"
                Else
                    lbleqstr.Value += "," & fi '& "'"
                    eqstr = lbleqstr.Value '"'" & fi & "'"
                    'eqstr = eqstr & " + ','+ '" & fi & "'"
                    sql = "update pmroutes set eqstr = '" & eqstr & "' where rid = '" & rteid & "'"
                End If

                fail.Update(sql)
                UpTasks(fi)

            End If
            
        Next
        'eqstr = lbleqstr.Value
        'sql = "update pmroutes set eqstr = '" & eqstr & "' where rteid = '" & rteid & "'"
        'fail.Update(sql)
        PopFailList(rteid)
        PopFail(rteid)
        fail.Dispose()
    End Sub

    Protected Sub btnfromcomp_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        FromComp()
    End Sub
    Private Sub FromComp()
        rteid = lblrteid.Value
        Dim Item As ListItem
        Dim f, fi As String
        fail.Open()
        Dim cnt As Integer = lbfailmodes.Items.Count
        If cnt = 1 Then
            Try
                lbfailmodes.SelectedIndex = 0
            Catch ex As Exception

            End Try

        End If
        lbleqstr.Value = ""
        For Each Item In lbfailmodes.Items
            If Not Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                If Len(lbleqstr.Value) = 0 Then
                    lbleqstr.Value = fi '"'" & fi & "'"
                    eqstr = fi '"'" & fi & "'"
                    sql = "update pmroutes set eqstr = '" & eqstr & "' where rid = '" & rteid & "'"
                Else
                    lbleqstr.Value += "," & fi '& "'"
                    eqstr = lbleqstr.Value 'eqstr & " + ','+ '" & fi & "'"
                    sql = "update pmroutes set eqstr = '" & eqstr & "' where rid = '" & rteid & "'"
                End If
                fail.Update(sql)
                'UnTasks(fi)
            Else
                fi = Item.Value.ToString
                UnTasks(fi)
            End If
            
        Next
        'eqstr = lbleqstr.Value
        'sql = "update pmroutes set eqstr = '" & eqstr & "' where rteid = '" & rteid & "'"
        'fail.Update(sql)
        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                UnTasks(fi)
            End If
        Next
        PopFailList(rteid)
        PopFail(rteid)
        fail.Dispose()
    End Sub
    Private Sub lbfailmaster_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbfailmaster.SelectedIndexChanged
        If lblopt.Value = "1" Then
            ToComp()
        End If
    End Sub

    Private Sub lbfailmodes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbfailmodes.SelectedIndexChanged
        If lblopt.Value = "1" Then
            FromComp()
        End If
    End Sub

    Private Sub cbopts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbopts.SelectedIndexChanged
        Dim opt As String = cbopts.SelectedValue.ToString
        lblopt.Value = opt
        If opt = "0" Then
            lbfailmodes.AutoPostBack = False
            lbfailmaster.AutoPostBack = False
        Else
            lbfailmodes.AutoPostBack = True
            lbfailmaster.AutoPostBack = True
        End If
    End Sub
End Class