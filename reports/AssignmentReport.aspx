<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AssignmentReport.aspx.vb" Inherits="lucy_r12.AssignmentReport" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AssignmentReport</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<LINK href="../styles/reports.css" type="text/css" rel="stylesheet">
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
        
		<script language="JavaScript" src="../scripts1/AssignmentReportaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
         function gettask(fid, coid, comp, tskid) {
             //document.getElementById("lbltasknum").value = task;
             //handleexit(task)
             //var fuid = document.getElementById("lblfuid").value
             //cid = document.getElementById("lblcid").value;
             //sid = document.getElementById("lblsid").value;
             //did = document.getElementById("lbldid").value;
             //clid = document.getElementById("lblclid").value;
             var eqid = document.getElementById("lbleqid").value;
             var usetot = document.getElementById("lblusetot").value;
             //chk = document.getElementById("lblchk").value;
             //if (cnt == "0") {
             var eReturn = window.showModalDialog("../complib/complibtasks2dialog.aspx?typ=tasks2&coid=" + coid + "&comp=" + comp + "&fuid=" + fid + " &tasknum=" + tskid + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:780px;resizable=yes");
             //}
             //else {
             //    var eReturn = window.showModalDialog("../complib/complibtasks2dialog.aspx?typ=tasksno&coid=" + coid + "&comp=" + comp + "&fuid=" + fid + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:780px;resizable=yes");
             //}
             if (eReturn) {
                 //reload report
                 //window.location = "AssignmentReport.aspx?typ=eq&fuid=" + fid + "&eqid=" + eqid + "&date=" + Date();
                 document.getElementById("lblsubmit").value = "ret";
                 document.getElementById("form1").submit();
             }
             else {
                 //reload report
                 //window.location = "AssignmentReport.aspx?typ=eq&fuid=" + fid + "&eqid=" + eqid + "&date=" + Date();
                 document.getElementById("lblsubmit").value = "ret";
                 document.getElementById("form1").submit();
             }
         }
         function handleexit(task) {
             window.returnValue = task;
             window.close();
         }
         function sstchur_SmartScroller_Scroll() {
             var x = document.getElementById("xCoord").value;
             var y = document.getElementById("yCoord").value;
             //alert(x + ", " + y)
             window.scrollTo(x, y);

         }
         function sstchur_SmartScroller_GetCoords() {

             var scrollX, scrollY;

             if (document.all) {
                 if (!document.documentElement.scrollLeft)
                     scrollX = document.body.scrollLeft;
                 else
                     scrollX = document.documentElement.scrollLeft;

                 if (!document.documentElement.scrollTop)
                     scrollY = document.body.scrollTop;
                 else
                     scrollY = document.documentElement.scrollTop;
             }
             else {
                 scrollX = window.pageXOffset;
                 scrollY = window.pageYOffset;
             }

             document.getElementById("xCoord").value = scrollX;
             document.getElementById("yCoord").value = scrollY;
             
             //alert(document.getElementById("xCoord").value + ", " + document.getElementById("xCoord").value)
         }
         window.onscroll = sstchur_SmartScroller_GetCoords;
         window.onkeypress = sstchur_SmartScroller_GetCoords;
         window.onclick = sstchur_SmartScroller_GetCoords;
		</script>
        <style type="text/css">
    .plainlabelsmr
{
	font-family:MS Sans Serif, arial, sans-serif, Verdana;
	font-size:10px;
	text-decoration:none;
	color:red;
}
    </style>
	</HEAD>
	<body onload="sstchur_SmartScroller_Scroll();">
		<form id="form1" method="post" runat="server">
			<TABLE id="scrollmenu" width="1000" runat="server">
				<tr>
					<td id="tdwi" align="center" runat="server"></td>
				</tr>
			</TABLE>
		
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lbleqid" runat="server" />
<input type="hidden" id="xCoord" runat="server" NAME="Hidden1"> <input type="hidden" id="yCoord" runat="server" NAME="Hidden2">
<input type="hidden" id="lblsubmit" runat="server" />
<input type="hidden" id="lblusetot" runat="server" />
</form>
	</body>
</HTML>
