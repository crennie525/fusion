<%@ Page Language="vb" AutoEventWireup="false" Codebehind="OVRollup.aspx.vb" Inherits="lucy_r12.OVRollup" %>

<script type="text/javascript" src="../jqplot/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="../jqplot/jqplot.pieRenderer.min.js"></script>
<link rel="stylesheet" type="text/css" href="../jqplot/jquery.jqplot.min.css" />
<script type="text/javascript">
    $(document).ready(function () {
        var reportTitle = $("#reportTitle").text();
        if (reportTitle == "PM Optimization Overview Site Rollup") {
           GetSitePieChartData();
        } else {
           GetEquipmentPieChartData();
        }
    });
</script>