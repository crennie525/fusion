﻿Imports System.Data.SqlClient
Public Class pmgselect
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim tots As New Utilities
    Dim sid, typ, days, shift, eqid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString '3494 '
            lbleqid.Value = eqid
            tots.Open()
            gettots(eqid)
        End If
    End Sub
    Private Sub gettots(ByVal eqid As String)

        sql = "usp_pmmgrrep '" & eqid & "'"
        dr = tots.GetRdrData(sql)
        dgtots.DataSource = dr
        dgtots.DataBind()
        dr.Close()
    End Sub
    Private Sub dgtots_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtots.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim skillid As String = DataBinder.Eval(e.Item.DataItem, "skillid").ToString
            Dim qty As String = DataBinder.Eval(e.Item.DataItem, "qty").ToString
            Dim freq As String = DataBinder.Eval(e.Item.DataItem, "freq").ToString
            Dim rid As String = DataBinder.Eval(e.Item.DataItem, "rdid").ToString
            Dim pm As String = DataBinder.Eval(e.Item.DataItem, "pm").ToString
            Dim tcnt As String = DataBinder.Eval(e.Item.DataItem, "tcnt").ToString
            Dim days As String = DataBinder.Eval(e.Item.DataItem, "days").ToString
            Dim shift As String = DataBinder.Eval(e.Item.DataItem, "shift").ToString
            Dim ptid As String
            Try
                ptid = DataBinder.Eval(e.Item.DataItem, "ptid").ToString
            Catch ex As Exception
                ptid = "0"
            End Try
            pm = Replace(pm, ",", "-", , , vbTextCompare)
            If days <> "" Then
                days = Replace(days, ",", "-", , , vbTextCompare)
            End If
            Dim lpm As LinkButton = CType(e.Item.FindControl("lbpm"), LinkButton)
            eqid = lbleqid.Value
            lpm.Attributes.Add("onclick", "getrep('" & eqid & "','" & ptid & "','" & skillid & "','" & qty & "','" & freq & "','" & rid & "','" & pm & "','" & tcnt & "','" & days & "','" & shift & "');")
        End If
    End Sub
End Class