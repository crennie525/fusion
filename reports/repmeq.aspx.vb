﻿Imports System.Data.SqlClient
Public Class repmeq
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim la As New Utilities
    Dim sid, did, clid, lid As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim intPgNav As Integer
    Dim tmod As New transmod
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = "12" 'Request.QueryString("sid").ToString
            lblsid.Value = sid
            la.Open()
            PageNumber = 1
            txtipg.Value = PageNumber
            PopEquipment(PageNumber)
            la.Dispose()
        Else
            If Request.Form("lblret") = "inext" Then
                la.Open()
                GetiNext()
                la.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "ilast" Then
                la.Open()
                PageNumber = txtipgcnt.Value
                txtipg.Value = PageNumber
                Dim dept, cell, lid As String
                dept = lbldid.Value
                cell = lblclid.Value
                If dept = "" Then
                    dept = "0"
                End If
                If cell = "" Then
                    cell = "0"
                End If
                lid = lbllid.Value
                If lid = "" Then
                    lid = "0"
                End If
                PopEquipment(PageNumber, dept, cell, lid)
                la.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "iprev" Then
                la.Open()
                GetiPrev()
                la.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "ifirst" Then
                la.Open()
                PageNumber = 1
                txtipg.Value = PageNumber
                Dim dept, cell, lid As String
                dept = lbldid.Value
                cell = lblclid.Value
                If dept = "" Then
                    dept = "0"
                End If
                If cell = "" Then
                    cell = "0"
                End If
                lid = lbllid.Value
                If lid = "" Then
                    lid = "0"
                End If
                PopEquipment(PageNumber, dept, cell, lid)
                PopLabEquipment(dept, cell, lid)
                la.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "undoloc" Then
                la.Open()
                PageNumber = 1
                txtipg.Value = PageNumber
                lbldid.Value = ""
                lbldept.Value = ""
                lblclid.Value = ""
                lblcell.Value = ""
                lbllid.Value = ""
                lblloc.Value = ""

                'need to clear locs here
                trdepts.Attributes.Add("class", "details")
                trlocs.Attributes.Add("class", "details")

                PopEquipment(PageNumber)
                PopLabEquipment()
                la.Dispose()
                lblret.Value = ""
            End If
            'need to repop locs here
            did = lbldid.Value
            lid = lbllid.Value
            If did <> "" Then
                trdepts.Attributes.Add("class", "view")
                trlocs.Attributes.Add("class", "details")
                tddept.InnerHtml = lbldept.Value
                tdcell.InnerHtml = lblcell.Value
            ElseIf lid <> "" Then
                trlocs.Attributes.Add("class", "view")
                trdepts.Attributes.Add("class", "details")
                tdloc3.InnerHtml = lblloc.Value
            Else
                trdepts.Attributes.Add("class", "details")
                trlocs.Attributes.Add("class", "details")
            End If

        End If
    End Sub
    Private Sub GetiNext()
        Try
            Dim pg As Integer = txtipg.Value
            PageNumber = pg + 1
            txtipg.Value = PageNumber
            Dim dept, cell, lid As String
            dept = lbldid.Value
            cell = lblclid.Value
            If dept = "" Then
                dept = "0"
            End If
            If cell = "" Then
                cell = "0"
            End If
            lid = lbllid.Value
            PopEquipment(PageNumber, dept, cell, lid)
        Catch ex As Exception
            la.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1695", "useradmin2.aspx.vb")

            la.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetiPrev()
        Try
            Dim pg As Integer = txtipg.Value
            PageNumber = pg - 1
            txtipg.Value = PageNumber
            Dim dept, cell, lid As String
            dept = lbldid.Value
            cell = lblclid.Value
            If dept = "" Then
                dept = "0"
            End If
            If cell = "" Then
                cell = "0"
            End If
            lid = lbllid.Value
            PopEquipment(PageNumber, dept, cell, lid)
        Catch ex As Exception
            la.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1696", "useradmin2.aspx.vb")

            la.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub PopEquipment(ByVal PageNumber As String, Optional ByVal dept As String = "0", Optional ByVal cell As String = "0", Optional ByVal lid As String = "0")
        Dim sid As String = lblsid.Value
        Dim filt, elval, eid As String
        filt = ""
        Dim elist As String = lbleqlist.Value
        If Len(elist) > 0 Then
            Dim s2 As Integer = elist.IndexOf("^")
            If s2 <> -1 Then
                Dim earr() As String = elist.Split("^")
                Dim i As Integer
                For i = 0 To earr.Length - 1
                    elval = earr(i)
                    Dim elarr() As String = elval.Split("~")
                    eid = elarr(4)
                    If filt = "" Then
                        filt = eid
                    Else
                        filt += "," & eid
                    End If
                Next
                filt = "(" & filt & ")"
            Else
                Dim earr() As String = elist.Split("~")
                eid = earr(4)
                If filt = "" Then
                    filt = eid
                Else
                    filt += "," & eid
                End If
                filt = "(" & filt & ")"
            End If
        End If
        If filt = "" Then
            filt = "(0)"
        End If
        If dept = "0" And cell = "0" And lid = "0" Then
            sql = "select count(*) from equipment where eqid not in " & filt
            Filter = "eqid not in " & filt
        ElseIf dept <> "0" And cell = "0" And lid = "0" Then
            sql = "select count(*) from equipment where dept_id = '" & dept & "' and eqid not in " & filt
            Filter = "dept_id = '" & dept & "' and eqid not in " & filt
        ElseIf dept <> "0" And cell <> "0" And lid = "0" Then
            sql = "select count(*) from equipment where dept_id = '" & dept & "' and cellid = '" & cell & "' and eqid not in " & filt
            Filter = "dept_id = '" & dept & "' and cellid = '" & cell & "' and eqid not in " & filt
        ElseIf dept <> "0" And cell <> "0" And lid <> "0" Then
            sql = "select count(*) from equipment where dept_id = '" & dept & "' and cellid = '" & cell & "' and locid = '" & lid & "' and eqid not in " & filt
            Filter = "dept_id = '" & dept & "' and cellid = '" & cell & "' and locid = '" & lid & "' and eqid not in " & filt
        ElseIf dept <> "0" And cell = "0" And lid <> "0" Then
            sql = "select count(*) from equipment where dept_id = '" & dept & "' and locid = '" & lid & "' and eqid not in " & filt
            Filter = "dept_id = '" & dept & "' and locid = '" & lid & "' and eqid not in " & filt
        ElseIf dept = "0" And cell = "0" And lid <> "0" Then
            Dim par As String = lblloc.Value
            sql = "select count(*) from equipment where eqnum in (select h.location from pmlocheir h where h.parent = '" & par & "') and eqid not in " & filt
            Filter = "eqnum in (select h.location from pmlocheir h where h.parent = '" & par & "') and eqid not in " & filt

        Else
            Dim par As String = lblloc.Value
            sql = "select count(*) from equipment where eqnum in (select h.location from pmlocheir h where h.parent = '" & par & "') and eqid not in " & filt
            Filter = "eqnum in (select h.location from pmlocheir h where h.parent = '" & par & "') and eqid not in " & filt
        End If

        txtipg.Value = PageNumber
        'intPgNav = adm.PageCount(intPgCnt, PageSize)

        intPgNav = la.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblipg.Text = "Page 0 of 0"
        Else
            lblipg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtipgcnt.Value = intPgNav

        Tables = "equipment"
        PK = "eqid"
        Sort = "eqnum desc"
        dr = la.GetPageHack(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        'dr = la.GetRdrData(sql)
        lbps.DataSource = dr
        lbps.DataTextField = "eqnum"
        lbps.DataValueField = "eqid"
        lbps.DataBind()
        dr.Close()
    End Sub
    Private Sub RemItems(ByVal ei As String)
        Dim filt, elval, eid, eqnum As String
        Dim dval, cval, lval As String
        filt = ""
        Dim elist As String = lbleqlist.Value
        Dim nlist As String = ""
        If Len(elist) > 0 Then
            Dim s2 As Integer = elist.IndexOf("^")
            If s2 <> -1 Then
                Dim earr() As String = elist.Split("^")
                Dim i As Integer
                For i = 0 To earr.Length - 1
                    elval = earr(i)
                    Dim elarr() As String = elval.Split("~")
                    eid = elarr(4)
                    If eid <> ei Then
                        If nlist = "" Then
                            nlist = elval
                        Else
                            nlist += "^" & elval
                        End If
                    End If
                Next
            Else
                nlist = ""
            End If
            elist = nlist
            lbleqlist.Value = elist
        End If
    End Sub
    Private Sub PopLabEquipment(Optional ByVal dept As String = "0", Optional ByVal cell As String = "0", Optional ByVal lid As String = "0")
        Dim filt, elval, eid, eqnum As String
        Dim dval, cval, lval As String
        filt = ""
        Dim elist As String = lbleqlist.Value
        lbuserps.Items.Clear()
        If Len(elist) > 0 Then
            Dim s2 As Integer = elist.IndexOf("^")
            If s2 <> -1 Then
                Dim earr() As String = elist.Split("^")
                Dim i As Integer
                For i = 0 To earr.Length - 1
                    elval = earr(i)
                    Dim elarr() As String = elval.Split("~")
                    eid = elarr(4)
                    eqnum = elarr(3)
                    lval = elarr(2)
                    cval = elarr(1)
                    dval = elarr(0)
                    If dept <> "0" Then
                        If dval = dept Then
                            If cell = "0" Then
                                Dim item As ListItem = New ListItem(eqnum, eid)
                                lbuserps.Items.Add(item)
                            Else
                                If cell = cval Then
                                    Dim item As ListItem = New ListItem(eqnum, eid)
                                    lbuserps.Items.Add(item)
                                End If
                            End If
                        End If
                    ElseIf lid <> 0 Then
                        If lid = lval Then
                            Dim item As ListItem = New ListItem(eqnum, eid)
                            lbuserps.Items.Add(item)
                        End If
                    Else
                        Dim item As ListItem = New ListItem(eqnum, eid)
                        lbuserps.Items.Add(item)
                    End If

                Next
            Else
                Dim earr() As String = elist.Split("~")
                eid = earr(4)
                eqnum = earr(3)
                lval = earr(2)
                cval = earr(1)
                dval = earr(0)
                If dept <> "0" Then
                    If dval = dept Then
                        If cell = "0" Then
                            Dim item As ListItem = New ListItem(eqnum, eid)
                            lbuserps.Items.Add(item)
                        Else
                            If cell = cval Then
                                Dim item As ListItem = New ListItem(eqnum, eid)
                                lbuserps.Items.Add(item)
                            End If
                        End If
                    End If
                ElseIf lid <> 0 Then
                    If lid = lval Then
                        Dim item As ListItem = New ListItem(eqnum, eid)
                        lbuserps.Items.Add(item)
                    End If
                Else
                    Dim item As ListItem = New ListItem(eqnum, eid)
                    lbuserps.Items.Add(item)
                End If
            End If
        Else
            lbuserps.Items.Clear()
        End If
    End Sub

    Protected Sub btntouser_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntouser.Click
        Dim Item As ListItem
        Dim ei, ed As String
        Dim dept, cell, lid As String
        Dim did, clid, lidi, cn As String
        dept = lbldid.Value
        cell = lblclid.Value
        Dim elist As String = lbleqlist.Value
        If dept = "" Then
            dept = "0"
        End If
        If cell = "" Then
            cell = "0"
        End If
        lid = lbllid.Value
        If lid = "" Then
            lid = "0"
        End If
        la.Open()
        For Each Item In lbps.Items
            If Item.Selected Then
                ei = Item.Value.ToString
                ed = Item.Text.ToString
                If dept = "0" And cell = "0" And lid = "0" Then
                    sql = "select isnull(dept_id, 0) as did, isnull(cellid, 0) as clid, isnull(locid, 0) as lidi from equipment where eqid = '" & ei & "'"
                    dr = la.GetRdrData(sql)
                    While dr.Read
                        did = dr.Item("did").ToString
                        clid = dr.Item("clid").ToString
                        lidi = dr.Item("lidi").ToString
                    End While
                    dr.Close()
                    If clid <> "0" Then
                        sql = "select cell_name from cells where cellid = '" & clid & "'"
                        cn = la.strScalar(sql)
                        If cn = "No Cells" Then
                            clid = "0"
                        End If
                    End If
                    If elist = "" Then
                        elist = did & "~" & clid & "~" & lidi & "~" & ed & "~" & ei
                    Else
                        elist += "^" & did & "~" & clid & "~" & lidi & "~" & ed & "~" & ei
                    End If
                Else
                    If elist = "" Then
                        elist = dept & "~" & cell & "~" & lid & "~" & ed & "~" & ei
                    Else
                        elist += "^" & dept & "~" & cell & "~" & lid & "~" & ed & "~" & ei
                    End If
                End If
            End If
        Next
        Dim tst As String = elist
        lbleqlist.Value = elist
        PageNumber = txtipgcnt.Value
        PopEquipment(PageNumber, dept, cell, lid)
        PopLabEquipment(dept, cell, lid)
        la.Dispose()
    End Sub

    Protected Sub btnfromuser_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromuser.Click
        Dim Item As ListItem
        Dim ei As String
        la.Open()
        For Each Item In lbuserps.Items
            If Item.Selected Then
                ei = Item.Value.ToString
                RemItems(ei)
            End If
        Next
        Dim dept, cell, lid As String
        dept = lbldid.Value
        cell = lblclid.Value
        If dept = "" Then
            dept = "0"
        End If
        If cell = "" Then
            cell = "0"
        End If
        lid = lbllid.Value
        If lid = "" Then
            lid = "0"
        End If
        PageNumber = txtipgcnt.Value
        PopEquipment(PageNumber, dept, cell, lid)
        PopLabEquipment(dept, cell, lid)
        la.Dispose()
    End Sub

End Class