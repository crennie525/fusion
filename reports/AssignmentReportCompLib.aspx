﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssignmentReportCompLib.aspx.vb" Inherits="lucy_r12.AssignmentReportCompLib" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Component Library Assignment Report</title>
    <link href="../styles/reports.css" type="text/css" rel="stylesheet" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/AssignmentReportaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>

    <style type="text/css">
    .plainlabelsmr
{
	font-family:MS Sans Serif, arial, sans-serif, Verdana;
	font-size:10px;
	text-decoration:none;
	color:red;
}
    </style>

</head>
<body>
    <form id="form1" runat="server">
    <table id="scrollmenu" width="1000" runat="server">
        <tr>
            <td id="tdwi" align="center" runat="server">
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="xCoord" runat="server" />
    <input type="hidden" id="yCoord" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblusetot" runat="server" />
    <input type="hidden" id="lblsiteid" runat="server" />
    </form>
</body>
</html>
