

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMRationaleAllTPM
    Inherits System.Web.UI.Page
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim tasks As New Utilities
    Dim dr As SqlDataReader
    Dim eqid, rep, title As String
    Dim tmod As New transmod
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblexport As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdwi As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            rep = Request.QueryString("rep").ToString
            tasks.Open()
            GetReport(eqid, rep)
            tasks.Dispose()

        End If
    End Sub
    Private Sub GetReport(ByVal eqid As String, ByVal rep As String)
        Dim saveasfile As String = "test"
        'HttpContext.Current.Response.ContentType = "application/vnd.ms-word"
        'HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & saveasfile & ".doc")
        'Dim tw As New System.IO.StringWriter
        'Dim hw As New System.Web.UI.HtmlTextWriter(tw)
        'HttpContext.Current.Response.Charset = ""
        If rep = "all" Then
            sql = "usp_GetRationalAllTPM '" & eqid & "'"
            title = tmod.getxlbl("xlb416" , "PMRationaleAllTPM.aspx.vb")
        ElseIf rep = "ronly" Then
            sql = "usp_GetRationalwROnlyTPM '" & eqid & "'"
            title = tmod.getxlbl("xlb417" , "PMRationaleAllTPM.aspx.vb")
        ElseIf rep = "chngonly" Then
            sql = "usp_GetRationalwChngOnlyTPM '" & eqid & "'"
            title = tmod.getxlbl("xlb418" , "PMRationaleAllTPM.aspx.vb")
        End If

        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""600"">") 'style=""page-break-after:always;""
        sb.Append("<tr><td class=""label16"" colspan=""4"" align=""center"">" & title & "</td></tr>")
        sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
        sb.Append("<tr><td width=""100""></td><td width=""165""></td><td width=""165""></td><td width=""170""></td></tr>")

        Dim ratchk As String = "0"

        dr = tasks.GetRdrData(sql)
        Dim start As Integer = 0
        Dim fchk As String = ""
        Dim func As String
        Dim tchk As String = ""
        Dim task As String
        Dim oti, ti, rat, ts As String
        While dr.Read
            ratchk = "1"
            If start = 0 Then
                start = 1
                sb.Append("<tr><td class=""label14"" colspan=""4"">" & tmod.getxlbl("xlb419" , "PMRationaleAllTPM.aspx.vb") & " " & dr.Item("eqnum").ToString & " - " & dr.Item("eqdesc").ToString & "</td></tr>")
                sb.Append("<tr><td class=""label14"" colspan=""2"" style=""border-bottom: solid 1px gray;"">" & tmod.getxlbl("xlb420" , "PMRationaleAllTPM.aspx.vb") & " " & dr.Item("dept_line").ToString & "</td>")
                sb.Append("<td class=""label14"" colspan=""2"" style=""border-bottom: solid 1px gray;"">" & tmod.getxlbl("xlb421" , "PMRationaleAllTPM.aspx.vb") & " " & dr.Item("cell_name").ToString & "</td></tr>")


            End If
            func = dr.Item("func").ToString
            If fchk <> func Then
                fchk = func
                tchk = ""
                sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""label14"" colspan=""4"">" & tmod.getxlbl("xlb422" , "PMRationaleAllTPM.aspx.vb") & " " & dr.Item("func").ToString & "</td></tr>")

            End If
            task = dr.Item("tasknum").ToString
            ts = dr.Item("taskstatus").ToString
            If Len(ts) = 0 Then
                ts = "No Status Selected"
            End If
            If tchk <> task Then
                If tchk <> "" Then
                    sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
                End If
                tchk = task

                sb.Append("<tr><td class=""label14"" colspan=""2"">" & tmod.getxlbl("xlb423" , "PMRationaleAllTPM.aspx.vb") & " " & task & "</td>")
                sb.Append("<td class=""label14"" colspan=""2"">" & tmod.getxlbl("xlb424" , "PMRationaleAllTPM.aspx.vb") & " " & ts & "</td></tr>")
                sb.Append("<tr><td class=""label14"" colspan=""4"">" & tmod.getxlbl("xlb425" , "PMRationaleAllTPM.aspx.vb") & " " & dr.Item("compnum").ToString & "</td></tr>")
                sb.Append("<tr><td class=""rtlabelhdr"">" & tmod.getlbl("cdlbl1191" , "PMRationaleAllTPM.aspx.vb") & "</td>")
                sb.Append("<td class=""cntrlabelhdr"">" & tmod.getlbl("cdlbl1192" , "PMRationaleAllTPM.aspx.vb") & "</td>")
                sb.Append("<td class=""cntrlabelhdr"">" & tmod.getlbl("cdlbl1193" , "PMRationaleAllTPM.aspx.vb") & "</td>")
                sb.Append("<td class=""endlabelhdr"">" & tmod.getlbl("cdlbl1194" , "PMRationaleAllTPM.aspx.vb") & "</td></tr>")
            End If
            oti = dr.Item("orig").ToString
            If Len(oti) = 0 Then oti = "None"
            ti = dr.Item("revised").ToString
            If Len(ti) = 0 Then ti = "None"
            rat = dr.Item("rationale").ToString
            If Len(rat) = 0 Then rat = "None Provided"
            sb.Append("<tr><td class=""rtlabelpln"" border=""1"">" & dr.Item("taskcol").ToString & "</td>")
            sb.Append("<td class=""cntrlabelpln"">" & oti & "</td>")
            sb.Append("<td class=""cntrlabelpln"">" & ti & "</td>")
            sb.Append("<td class=""endlabelpln"">" & rat & "</td></tr>")

        End While
        dr.Close()
        If ratchk = "0" Then
            sb.Append("<tr><td class=""redlabel"" colspan=""4"" align=""center"">" & tmod.getlbl("cdlbl1195" , "PMRationaleAllTPM.aspx.vb") & "</td></tr>")
        End If
        sb.Append("</Table>")
        'sb.Append("<table><tr><td>test</td></tr></table>")
        tdwi.InnerHtml = sb.ToString
        'cmpToWord.TableToWord(tblexport, Response)
    End Sub
End Class

