<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PMAcceptancePkg.aspx.vb" Inherits="lucy_r12.PMAcceptancePkg" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PMAcceptancePkg</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<LINK href="../styles/reports.css" type="text/css" rel="stylesheet">
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<TABLE id="tblexport" width="620" align="center" runat="server">
				<tr>
					<td id="tdwi" align="center" runat="server"></td>
				</tr>
				<tr>
					<td id="tdwi2" align="center" runat="server"></td>
				</tr>
				<tr>
					<td id="tdwi3" align="center" runat="server"></td>
				</tr>
				<tr>
					<td id="tdwi4" align="center" runat="server"></td>
				</tr>
				<tr>
					<td id="tdwi5" align="center" runat="server"></td>
				</tr>
				<tr>
					<td id="tdwi6" align="center" runat="server"></td>
				</tr>
				<tr>
					<td id="tdwi7" align="center" runat="server"></td>
				</tr>
				<tr>
					<td id="tdpmall" runat="server" align="center"></td>
				</tr>
				<tr>
					<td id="tdpdmall" runat="server" align="center"></td>
				</tr>
				<tr>
					<td id="tdsall" runat="server" align="center"></td>
				</tr>
				<tr>
					<td id="tdspdmall" runat="server" align="center"></td>
				</tr>
				<tr>
					<td id="tdeqlev" runat="server" align="center"></td>
				</tr>
			</TABLE>
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
