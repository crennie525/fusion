﻿Imports System.Data.SqlClient
Public Class AssignmentReport2
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim tasks As New Utilities
    Dim dr As SqlDataReader
    Dim eqid, fuid, comid, typ, usetot As String
    Dim tmod As New transmod
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try

        'ADD FOR SUNCOR
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        lblcomi.Value = comi
        If comi = "SUN" Then
            lblfslang.Value = "fre"
        End If
        'END ADD FOR SUNCOR

        'Put user code to initialize the page here
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            typ = Request.QueryString("typ").ToString
            tasks.Open()
            If typ = "fu" Then
                fuid = Request.QueryString("fuid").ToString
                GetReport(typ, eqid, fuid)
            ElseIf typ = "co" Then
                fuid = Request.QueryString("fuid").ToString
                comid = Request.QueryString("coid").ToString
                GetReport(typ, eqid, fuid, comid)
            Else
                GetReport(typ, eqid)
            End If
            tasks.Dispose()
        Else
            If Request.Form("lblsubmit") = "ret" Then
                tasks.Open()
                eqid = lbleqid.Value
                GetReport("eq", eqid)
                tasks.Dispose()
                lblsubmit.Value = ""
            End If
        End If
    End Sub
    Private Sub GetReport(ByVal typ As String, ByVal eqid As String, Optional ByVal fuid As String = "", Optional ByVal comid As String = "")
        'sql = "select isnull(usetot, 0) from equipment where eqid = '" & eqid & "'"
        'Try
        'usetot = tasks.strScalar(sql)
        'Catch ex As Exception
        'usetot = "0"
        'End Try
        Dim lang As String = lblfslang.Value
        Dim coi As String = lblcomi.Value
        lblusetot.Value = usetot
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""960"" border=""0"">")
        sb.Append("<tr><td class=""label16"" colspan=""9"" align=""center"">" & tmod.getlbl("cdlbl669", "AssignmentReport.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td colspan=""9"" align=""center"" class=""plainlabel"">" & Now & "</td></tr>")
        sb.Append("<tr><td colspan=""9"" align=""center"" class=""plainlabelsmr"">Tasks in RED indicate that this task has either been transferred to or created in the Operator Care Module</td></tr>")
        sb.Append("<tr><td colspan=""9"" align=""center"" class=""plainlabelsmb"">Tasks in BLUE indicate that this task has a Meter Based Frequency or is Meter Ready</td></tr>")
        sb.Append("<tr><td colspan=""9"" align=""center"" class=""plainlabel"">&nbsp;</td></tr>")
        'sb.Append("<tr><td width=""50""></td><td width=""50""></td><td width=""120""></td>")
        'sb.Append("<td width=""50""></td><td width=""120""></td><td width=""120""></td>")
        'sb.Append("<td width=""97""></td><td width=""97""></td><td width=""97""></td>")
        'sb.Append("<td width=""97""></td><td width=""97""></td><td width=""97""></td></tr>")
        Dim coid, istpm As String
        Select Case typ
            Case "eq"
                If coi <> "CAS" And coi <> "LAU" Then
                    sql = "usp_GetAssignmentReport_new2 '" & eqid & "'"
                Else
                    If lang = "fre" Then
                        sql = "usp_GetAssignmentReport_new2f '" & eqid & "'"
                    Else
                        sql = "usp_GetAssignmentReport_new2 '" & eqid & "'"
                    End If
                End If

            Case "fu"
                sql = "usp_GetAssignmentReport_new2 '" & eqid & "', '" & fuid & "'"
            Case "co"
                sql = "usp_GetAssignmentReport_new2 '" & eqid & "', '" & fuid & "', '" & comid & "'"
        End Select

        dr = tasks.GetRdrData(sql)
        Dim start As Integer = 0
        Dim fchk As String = ""
        Dim func As String
        Dim tchk As String = ""
        Dim task As String
        Dim subtask As String
        Dim subt As String
        Dim oti, ti, rat, ts, freq, loto, cs, rd, lube, parts, tools, loc, locd, meterid, mr As String
        While dr.Read
            If start = 0 Then
                loc = dr.Item("location").ToString
                locd = dr.Item("description").ToString
                start = 1
                sb.Append("<tr><td class=""label14"" colspan=""4"" >" & tmod.getxlbl("xlb251", "AssignmentReport.aspx.vb") & " " & dr.Item("sitename").ToString & "</td>")
                sb.Append("<td class=""label14"" colspan=""1"" >" & tmod.getxlbl("xlb252", "AssignmentReport.aspx.vb") & " " & dr.Item("dept_line").ToString & "</td>")
                sb.Append("<td class=""label14"" colspan=""4"">" & tmod.getxlbl("xlb253", "AssignmentReport.aspx.vb") & " " & dr.Item("cell_name").ToString & "</td></tr>")
                If loc <> "" Then
                    sb.Append("<tr><td class=""label14"" colspan=""4"" ></td>")
                    sb.Append("<td class=""label14"" colspan=""1"" >" & tmod.getxlbl("xlb254", "AssignmentReport.aspx.vb") & " " & loc & "</td>")
                    sb.Append("<td class=""label14"" colspan=""4"">" & tmod.getxlbl("xlb255", "AssignmentReport.aspx.vb") & " " & locd & "</td></tr>")
                End If
                sb.Append("<tr><td class=""label14"" colspan=""9"">" & tmod.getxlbl("xlb256", "AssignmentReport.aspx.vb") & " " & dr.Item("eqnum").ToString & " - " & dr.Item("eqdesc").ToString & "</td></tr>")
                sb.Append("<tr><td class=""label14"" colspan=""4"" style=""border-bottom: solid 1px gray;"">" & tmod.getxlbl("xlb257", "AssignmentReport.aspx.vb") & " " & dr.Item("oem").ToString & "</td>")
                sb.Append("<td class=""label14"" colspan=""1"" style=""border-bottom: solid 1px gray;"">" & tmod.getxlbl("xlb258", "AssignmentReport.aspx.vb") & " " & dr.Item("model").ToString & "</td>")
                sb.Append("<td class=""label14"" colspan=""4"" style=""border-bottom: solid 1px gray;"">" & tmod.getxlbl("xlb259", "AssignmentReport.aspx.vb") & " " & dr.Item("serial").ToString & "</td></tr>")
            End If
            func = dr.Item("func").ToString
            If fchk <> func Then
                fchk = func
                tchk = ""
                sb.Append("<tr><td colspan=""9"">&nbsp;</td></tr>")
                sb.Append("<tr><td width=""70"" class=""labelsmtl"" align=""left"">" & tmod.getlbl("cdlbl670", "AssignmentReport.aspx.vb") & "</td><td width=""40"" class=""labelsmtl"">" & tmod.getlbl("cdlbl671", "AssignmentReport.aspx.vb") & "</td><td width=""150"" class=""labelsmtl"" align=""left"">" & tmod.getlbl("cdlbl672", "AssignmentReport.aspx.vb") & "</td>")
                sb.Append("<td width=""30"" class=""labelsmtl"">Qty</td><td width=""350"" class=""labelsmtl"" align=""left"">" & tmod.getlbl("cdlbl675", "AssignmentReport.aspx.vb") & "</td>")
                sb.Append("<td width=""180"" class=""labelsmtl"" align=""left"">Task Type</td><td width=""30"" class=""labelsmtl"" align=""left"">R/D</td><td width=""80"" class=""labelsmtl"" align=""left"">" & tmod.getlbl("cdlbl678", "AssignmentReport.aspx.vb") & "</td>")
                sb.Append("<td width=""30"" class=""labelsmtl"" align=""left"">MINS</td></tr>")
                sb.Append("<tr><td colspan=""9"" align=""left"" class=""labelsmu"">" & tmod.getxlbl("xlb260", "AssignmentReport.aspx.vb") & " " & dr.Item("func").ToString & "</td></tr>")

            End If
            subtask = dr.Item("subtask").ToString
            task = dr.Item("tasknum").ToString
            coid = dr.Item("comid").ToString
            istpm = dr.Item("istpm").ToString
            freq = dr.Item("freq").ToString
            subt = dr.Item("subt").ToString
            meterid = dr.Item("meterid").ToString
            mr = dr.Item("mr").ToString
            If freq = "0" Or freq = "" Then
                freq = "None"
            Else
                freq = dr.Item("freq").ToString
            End If
            loto = dr.Item("lotoid").ToString
            If loto = "0" Then
                loto = "N"
            Else
                loto = "Y"
            End If
            cs = dr.Item("conid").ToString
            If cs = "0" Then
                cs = "N"
            Else
                cs = "Y"
            End If
            rd = dr.Item("rdid").ToString
            If rd = "1" Then
                rd = "R"
            ElseIf rd = "2" Then
                rd = "D"
            ElseIf rd = "3" Then
                rd = "A"
            Else
                rd = "M"
            End If
            parts = dr.Item("parts").ToString
            If Len(parts) = 0 OrElse parts = "None" Then
                parts = Nothing
            Else
                parts = tmod.getxlbl("xlb261", "AssignmentReport.aspx.vb") & dr.Item("parts").ToString
            End If
            tools = dr.Item("tools").ToString
            If Len(tools) = 0 OrElse tools = "None" Then
                tools = Nothing
            Else
                tools = tmod.getxlbl("xlb262", "AssignmentReport.aspx.vb") & dr.Item("tools").ToString
            End If
            lube = dr.Item("lubes").ToString
            If Len(lube) = 0 OrElse lube = "None" Then
                lube = Nothing
            Else
                lube = tmod.getxlbl("xlb263", "AssignmentReport.aspx.vb") & dr.Item("lubes").ToString
            End If
            If meterid <> "" Then
                Dim test As String = meterid
            End If
            If subtask = 0 And istpm <> "1" And meterid = "" And mr <> "1" Then
                sb.Append("<tr><td class=""plainlabelsm"" align=""left"">" & freq & "</td>")
                sb.Append("<td class=""plainlabelsm""><A href=""#"" onclick=""gettask('" & dr.Item("funcid").ToString & "','" & coid & "','" & dr.Item("compnum").ToString & "','" & task & "');"">" & task & "</A></td>")
                sb.Append("<td class=""plainlabelsm"" align=""left"">" & dr.Item("compnum").ToString & "</td>")
                sb.Append("<td class=""plainlabelsm"">" & dr.Item("cqty").ToString & "</td>")
                'sb.Append("<td class=""plainlabelsm"">" & dr.Item("desig").ToString & "</td>")
                sb.Append("<td class=""plainlabelsm"" align=""left"">" & dr.Item("task").ToString & "</td>")
                sb.Append("<td class=""plainlabelsm"" align=""left"">" & dr.Item("tasktype").ToString & "</td>")
                sb.Append("<td class=""plainlabelsm"" align=""left"">" & rd & "</td>")
                sb.Append("<td class=""plainlabelsm"" align=""left"">" & dr.Item("skill").ToString & "</td>")
                sb.Append("<td class=""plainlabelsm"" align=""left"">" & dr.Item("ttime").ToString & "</td>")
                'sb.Append("<td class=""plainlabelsm"">" & loto & "</td>")
                'sb.Append("<td class=""plainlabelsm"">" & cs & "</td>
                sb.Append("</tr>")
            ElseIf (subtask = 0 And istpm <> "1" And meterid <> "") Or mr = "1" Then
                sb.Append("<tr><td class=""plainlabelsmb"" align=""left"">" & freq & "</td>")
                sb.Append("<td class=""plainlabelsmb""><A href=""#"" onclick=""gettask('" & dr.Item("funcid").ToString & "','" & coid & "','" & dr.Item("compnum").ToString & "','" & task & "');"">" & task & "</A></td>")
                sb.Append("<td class=""plainlabelsmb"" align=""left"">" & dr.Item("compnum").ToString & "</td>")
                sb.Append("<td class=""plainlabelsmb"">" & dr.Item("cqty").ToString & "</td>")
                'sb.Append("<td class=""plainlabelsm"">" & dr.Item("desig").ToString & "</td>")
                sb.Append("<td class=""plainlabelsmb"" align=""left"">" & dr.Item("task").ToString & "</td>")
                sb.Append("<td class=""plainlabelsmb"" align=""left"">" & dr.Item("tasktype").ToString & "</td>")
                sb.Append("<td class=""plainlabelsmb"" align=""left"">" & rd & "</td>")
                sb.Append("<td class=""plainlabelsmb"" align=""left"">" & dr.Item("skill").ToString & "</td>")
                sb.Append("<td class=""plainlabelsmb"" align=""left"">" & dr.Item("ttime").ToString & "</td>")
                'sb.Append("<td class=""plainlabelsm"">" & loto & "</td>")
                'sb.Append("<td class=""plainlabelsm"">" & cs & "</td>
                sb.Append("</tr>")
            ElseIf subtask = 0 And istpm = "1" Then
                sb.Append("<tr><td class=""plainlabelsmr"" align=""left"">" & freq & "</td>")
                sb.Append("<td class=""plainlabelsmr"">" & task & "</td>")
                sb.Append("<td class=""plainlabelsmr"" align=""left"">" & dr.Item("compnum").ToString & "</td>")
                sb.Append("<td class=""plainlabelsmr"">" & dr.Item("cqty").ToString & "</td>")
                'sb.Append("<td class=""plainlabelred"">" & dr.Item("desig").ToString & "</td>")
                sb.Append("<td class=""plainlabelsmr"" align=""left"">" & dr.Item("task").ToString & "</td>")
                sb.Append("<td class=""plainlabelsmr"" align=""left"">" & dr.Item("tasktype").ToString & "</td>")
                sb.Append("<td class=""plainlabelsmr"" align=""left"">" & rd & "</td>")
                sb.Append("<td class=""plainlabelsmr"" align=""left"">" & dr.Item("skill").ToString & "</td>")
                sb.Append("<td class=""plainlabelsmr"" align=""left"">" & dr.Item("ttime").ToString & "</td>")
                'sb.Append("<td class=""plainlabelred"">" & loto & "</td>")
                'sb.Append("<td class=""plainlabelred"">" & cs & "</td></tr>")
                sb.Append("</tr>")
            Else
                If istpm <> "1" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td class=""plainlabelsm"">" & tmod.getxlbl("xlb264", "AssignmentReport.aspx.vb") & " [" & subtask & "]</td>")
                    sb.Append("<td colspan=""6"" class=""plainlabelsm"" align=""left"">" & dr.Item("subt").ToString & "</td>")
                Else
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td class=""plainlabelsmr"">" & tmod.getxlbl("xlb264", "AssignmentReport.aspx.vb") & " [" & subtask & "]</td>")
                    sb.Append("<td colspan=""6"" class=""plainlabelsmr"" align=""left"">" & dr.Item("subt").ToString & "</td>")
                End If

            End If
            If parts <> Nothing Then
                If istpm <> "1" Then
                    sb.Append("<tr><td colspan=""2""></td>")
                    sb.Append("<td colspan=""7"" class=""plainlabelsm"" align=""left"">" & parts & "</td></tr>")
                Else
                    sb.Append("<tr><td colspan=""2""></td>")
                    sb.Append("<td colspan=""7"" class=""plainlabelsmr"" align=""left"">" & parts & "</td></tr>")
                End If

            End If
            If tools <> Nothing Then
                If istpm <> "1" Then
                    sb.Append("<tr><td colspan=""2""></td>")
                    sb.Append("<td colspan=""7"" class=""plainlabelsm"" align=""left"">" & tools & "</td></tr>")
                Else
                    sb.Append("<tr><td colspan=""2""></td>")
                    sb.Append("<td colspan=""7"" class=""plainlabelsmr"" align=""left"">" & tools & "</td></tr>")
                End If

            End If
            If lube <> Nothing Then
                If istpm <> "1" Then
                    sb.Append("<tr><td colspan=""2""></td>")
                    sb.Append("<td colspan=""7"" class=""plainlabelsm"" align=""left"">" & lube & "</td></tr>")
                Else
                    sb.Append("<tr><td colspan=""2""></td>")
                    sb.Append("<td colspan=""7"" class=""plainlabelsmr"" align=""left"">" & lube & "</td></tr>")
                End If

            End If


            'oti = dr.Item("otaskitem").ToString
            'If Len(oti) = 0 Then oti = "None"
            'ti = dr.Item("taskitem").ToString
            'If Len(ti) = 0 Then ti = "None"
            'rat = dr.Item("rationale").ToString
            'If Len(rat) = 0 Then rat = "None Provided"
            'sb.Append("<tr><td class=""rtlabelpln"" border=""1"">" & dr.Item("taskcol").ToString & "</td>")
            'sb.Append("<td class=""cntrlabelpln"">" & oti & "</td>")
            'sb.Append("<td class=""cntrlabelpln"">" & ti & "</td>")
            'sb.Append("<td class=""endlabelpln"">" & rat & "</td></tr>")

        End While
        dr.Close()
        sb.Append("</Table>")
        tdwi.InnerHtml = sb.ToString
        'cmpToWord.TableToWord(tblexport, Response)
    End Sub
End Class