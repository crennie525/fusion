

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class assetclass
    Inherits System.Web.UI.Page
	Protected WithEvents lang3306 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3305 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim eqid, cid, sid, rep As String
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddsites As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents Imagebutton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tdtop As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdinprog As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not Me.IsPostBack Then
            cid = "0" 'HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            'Try
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            rep = "1"
            pms.Open()
            'GetSites()
            'ddsites.SelectedValue = sid
            GetAll(sid)
            pms.Dispose()
            'Catch ex As Exception
            'sid = HttpContext.Current.Session("dfltps").ToString()
            'lblsid.Value = sid
            'pms.Open()
            'GetSites()
            'GetAll()
            'pms.Dispose()
            'End Try
        End If
    End Sub
    Private Sub GetSites()
        sql = "select siteid, sitename from sites where compid = '" & cid & "'"
        dr = pms.GetRdrData(sql)
        ddsites.DataSource = dr
        ddsites.DataValueField = "siteid"
        ddsites.DataTextField = "sitename"
        ddsites.DataBind()
        dr.Close()
        ddsites.Items.Insert(0, "All")
    End Sub
    Private Sub GetAll(Optional ByVal site As String = "all", Optional ByVal srch As String = "no")
        Dim sb As New StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""700"">")
        sb.Append("<tr><td class=""label"" colspan=""5"" align=""center""><h3>Site Asset Class Report</h3></td></tr>")
        'sb.Append("<tr><td class=""label"" colspan=""5"">&nbsp;</td></tr>")
        sb.Append("</table>")
        tdtop.InnerHtml = sb.ToString
        Dim sb1 As New StringBuilder
        sb1.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""700"">")
        sb1.Append("<tr>")
        sb1.Append("<td width=""80""></td>")
        sb1.Append("<td width=""270""></td>")
        sb1.Append("<td width=""80""></td>")
        sb1.Append("<td width=""270""></td>")
        sb1.Append("</tr>")
        'sb1.Append("<tr>")
        'sb1.Append("<td></td>")
        'If site = "all" Then
        'site = "0"
        'End If
        If rep <> "1" Then
            If ddsites.SelectedIndex = 0 Then
                site = "0"
            Else
                site = ddsites.SelectedValue.ToString
            End If
        End If

        Dim dept, cell, loc, locd As String

        'srch = txtsrch.Text
        'srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        'If Len(srch) = 0 Then
        srch = ""
        'End If
        Dim top As String = 0
        Dim sidflg As String = 0
        Dim rowflg As String = 0
        Dim ac As String
        Dim assetflg As String = 0
        Dim sid As String
        cid = lblcid.Value
        sql = "usp_assetclass '" & site & "', '" & srch & "'"
        'Try

        Dim asschk As String = "0"

        dr = pms.GetRdrData(sql)
        While dr.Read
            asschk = "1"
            sid = dr.Item("siteid").ToString
            ac = dr.Item("assetclass").ToString
            dept = dr.Item("dept_line").ToString
            cell = dr.Item("cell_name").ToString
            loc = dr.Item("llocation").ToString
            locd = dr.Item("ldescription").ToString
            If sid <> sidflg Then
                sidflg = sid
                'If top = "0" Then
                'top = "1"
                'sb1.Append("<tr><td colspan=""6""><hr size=""3""></td></tr>")
                'Else
                'sb1.Append("<tr><td colspan=""6""><hr size=""1""></td></tr>")
                'End If
                sb1.Append("<tr><td class=""bigbold"" colspan=""1"">" & tmod.getlbl("cdlbl650" , "assetclass.aspx.vb") & "</td>")
                sb1.Append("<td class=""bigbold"" colspan=""5"">" & dr.Item("sitename").ToString & "</td></tr>")
                'sb1.Append("<tr><td colspan=""6""><hr size=""1""></td></tr>")
                sb1.Append("<tr><td colspan=""6"">&nbsp;</td></tr>")
            End If
            If ac <> assetflg Then
                assetflg = ac
                sb1.Append("<tr class=""label"">")
                sb1.Append("<td style=""BORDER-BOTTOM: 1px solid"">" & tmod.getlbl("cdlbl651" , "assetclass.aspx.vb") & "</td>")
                sb1.Append("<td class=""plainlabel"" style=""BORDER-BOTTOM: 1px solid"" colspan=""5"">" & ac & "</td>")
                sb1.Append("</tr>")
                'sb1.Append("<tr><td colspan=""6"">&nbsp;</td></tr>")
                'sb1.Append("<td style=""BORDER-BOTTOM: 1px solid"">OEM</td>")
                'sb1.Append("<td style=""BORDER-BOTTOM: 1px solid"">" & tmod.getlbl("cdlbl653" , "assetclass.aspx.vb") & "</td>")
                'sb1.Append("<td style=""BORDER-BOTTOM: 1px solid"">" & tmod.getlbl("cdlbl654" , "assetclass.aspx.vb") & "</td>")
                'sb1.Append("<td style=""BORDER-BOTTOM: 1px solid"">" & tmod.getlbl("cdlbl655" , "assetclass.aspx.vb") & "</td>")
                'sb1.Append("<td style=""BORDER-BOTTOM: 1px solid"">" & tmod.getlbl("cdlbl656" , "assetclass.aspx.vb") & "</td>")
                rowflg = 0
            End If
            'If rowflg = 0 Then
            'rowflg = 1
            'sb1.Append("<tr>")
            'Else
            'rowflg = 0
            'sb1.Append("<tr bgcolor=""#E7F1FD"">")
            'End If
            sb1.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl657" , "assetclass.aspx.vb") & "</td>")
            sb1.Append("<td class=""plainlabel"">" & dr.Item("eqnum").ToString & "</td>")
            sb1.Append("<td class=""label"">" & tmod.getlbl("cdlbl658" , "assetclass.aspx.vb") & "</td>")
            sb1.Append("<td class=""plainlabel"">" & dr.Item("eqdesc").ToString & "</td></tr>")
            sb1.Append("<tr><td class=""label"">SPL</td>")
            sb1.Append("<td class=""plainlabel"" colspan=""5"">" & dr.Item("eqspl").ToString & "</td></tr>")

            If dr.Item("loc").ToString <> "Not Provided" Then
                sb1.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl660" , "assetclass.aspx.vb") & "</td>")
                sb1.Append("<td class=""plainlabel"">" & dr.Item("loc").ToString & "</td></tr>")
            End If

            sb1.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl661" , "assetclass.aspx.vb") & "</td>")
            sb1.Append("<td class=""plainlabel"">" & dept & "</td>")
            sb1.Append("<td class=""label"">" & tmod.getlbl("cdlbl662" , "assetclass.aspx.vb") & "</td>")
            sb1.Append("<td class=""plainlabel"">" & cell & "</td></tr>")

            If loc <> "" Then
                sb1.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl663" , "assetclass.aspx.vb") & "</td>")
                sb1.Append("<td class=""plainlabel"">" & loc & "</td>")
                sb1.Append("<td class=""label"">" & tmod.getlbl("cdlbl664" , "assetclass.aspx.vb") & "</td>")
                sb1.Append("<td class=""plainlabel"">" & locd & "</td></tr>")
            End If

            sb1.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl665" , "assetclass.aspx.vb") & "</td>")
            sb1.Append("<td class=""plainlabel"">" & dr.Item("owner").ToString & "</td>")
            sb1.Append("<td class=""label"">" & tmod.getlbl("cdlbl666" , "assetclass.aspx.vb") & "</td>")
            sb1.Append("<td class=""plainlabel""><A href=""mailto:" & dr.Item("email").ToString & """>" & dr.Item("email").ToString & "</A></td></tr>")
            sb1.Append("<tr><td colspan=""6"">&nbsp;</td></tr>")
            'sb1.Append("<td class=""plainlabel"">" & dr.Item("oem").ToString & "</td>")
            'sb1.Append("<td class=""plainlabel""><A href=""mailto:" & dr.Item("owner").ToString & """>" & dr.Item("owner").ToString & "</A></td>")
            'sb1.Append("<td class=""plainlabel"">" & tmod.getlbl("cdlbl667" , "assetclass.aspx.vb") & "</td>")
            'sb1.Append("<td align=""center""><img src=""../images/appimages/classiclookupnbg.gif"" onclick=""getpix('" & dr.Item("pix").ToString & "')""></td>")
            'sb1.Append("</tr>")
        End While
        dr.Close()
        If asschk = "0" Then
            sb.Append("<tr><td class=""redlabel"" colspan=""5"" align=""center"">" & tmod.getlbl("cdlbl668" , "assetclass.aspx.vb") & "</td></tr>")
        End If
        sb1.Append("</table>")
        tdinprog.InnerHtml = sb1.ToString
        'Catch ex As Exception

        'End Try

    End Sub

    Private Sub ddsites_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsites.SelectedIndexChanged
        pms.Open()
        GetAll()
        pms.Dispose()
    End Sub

    Private Sub Imagebutton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click
        pms.Open()
        GetAll()
        pms.Dispose()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3305.Text = axlabs.GetASPXPage("assetclass.aspx", "lang3305")
        Catch ex As Exception
        End Try
        Try
            lang3306.Text = axlabs.GetASPXPage("assetclass.aspx", "lang3306")
        Catch ex As Exception
        End Try

    End Sub

End Class


