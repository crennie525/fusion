﻿Imports System.Data.SqlClient
Public Class RBASTaskOrdtpm
    Inherits System.Web.UI.Page
    Dim dr As SqlDataReader
    Dim sql As String
    Dim fail As New Utilities
    Dim rteid, typ, skillid, qty, freq, rdid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            rteid = Request.QueryString("rteid").ToString
            lblrteid.Value = rteid '"3426" '
            lblrttype.Value = "RBAS"
            fail.Open()
            loadrtasks()
            fail.Dispose()
        Else
            If lblsubmit.Value = "save" Then
                fail.Open()
                savetasksnew()
                fail.Dispose()

            ElseIf lblsubmit.Value = "savex" Then
                fail.Open()
                'savetasksx()
                fail.Dispose()
                lblsubmit.Value = "go"
            End If
        End If
    End Sub
    Private Sub savetasksnew()
        Dim cbfin As String = lblcbdatfin.Value
        'use varchar(max) for stored procedure
        'and then f_split in multiple phases?
        'tell them to fuck off? ...keep trying
        Dim cnt As Integer

        sql = "usp_RBASTaskOrdChk '" & cbfin & "'"
        cnt = fail.Scalar(sql)

        If cnt = 0 Then
            sql = "usp_RBASTaskOrdtpm '" & cbfin & "'"
            fail.Update(sql)
            tddup.InnerHtml = ""
            loadrtasks()
            lblsubmit.Value = "go"
        Else
            sql = "usp_RBASTaskOrdtmp '" & cbfin & "'"
            fail.Update(sql)
            tddup.InnerHtml = "Duplicates Found. Please Check Task Order"
            'need new load sub for changes
            'dvtasks.InnerHtml = lblhtml.Value
            loadotasks()
        End If



    End Sub
    Private Sub loadotasks()
        rteid = lblrteid.Value
        typ = lblrttype.Value
        'sql = "select t.pmtskid, t.taskdesc from PMTasks t " _
        '+ "where t.rteid = '" & rteid & "' and t.rteno = 0"
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""1"" cellPadding=""2"" width=""960"" border=""0"">")
        sb.Append("<tr><td width=""30""></td><td width=""70""></td><td width=""100""></td><td width=""120""></td>")
        sb.Append("<td width=""180""></td><td width=""460""></td><td width=""0""></td></tr>")
        sb.Append("<tr><td class=""label"">Seq#</td>")
        sb.Append("<td class=""label"">Location</td>")
        sb.Append("<td class=""label"">Equipment</td>")
        sb.Append("<td class=""label"">Function</td>")
        sb.Append("<td class=""label"">Component</td>")
        sb.Append("<td class=""label"">Task Description</td>")
        sb.Append("<td></td>")
        sb.Append("</tr>")
        Dim cbx, cbi, cbo, eq, eqnum, eqdesc, func, comp, tasknum, taskdesc, ttime, pmtskid, loc, locdesc, rteno, seq, seqo
        Dim eqhold As String = ""
        Dim lochold As String = ""
        Dim cbc As String = ""
        Dim cb As Integer = 1
        If typ = "RBAS" Then 'getRBASTaskOrd
            sql = "getRBASTaskOrdtmptpm '" & rteid & "'"
        ElseIf typ = "RBAST" Then
            sql = "usp_getRBASTasks2T '" & rteid & "'"
        End If

        dr = fail.GetRdrData(sql)
        While dr.Read
            seq = dr.Item("seq").ToString
            loc = dr.Item("loc").ToString
            eqnum = dr.Item("eqnum").ToString
            func = dr.Item("func").ToString
            comp = dr.Item("compnum").ToString
            taskdesc = dr.Item("task").ToString
            pmtskid = dr.Item("pmtskid").ToString
            seqo = dr.Item("seqo").ToString

            cb += 1
            cbx = "cb" & cb
            cbi = "ci" & cb
            cbo = "co" & cb

            sb.Append("<tr><td><input type=""text"" style=""width:30px;margin:0px;border-style:solid; border-width:1px;"" id=""" & cbx & """ value=""" & seq & """></td>")
            sb.Append("<td class=""plainlabel"" style=""width:margin:0px;border-style:solid; border-width:1px;"">" & loc & "</td>")
            sb.Append("<td class=""plainlabel"" style=""width:margin:0px;border-style:solid; border-width:1px;"">" & eqnum & "</td>")
            sb.Append("<td class=""plainlabel"" style=""width:margin:0px;border-style:solid; border-width:1px;"">" & func & "</td>")
            sb.Append("<td class=""plainlabel"" style=""width:margin:0px;border-style:solid; border-width:1px;"">" & comp & "</td>")
            sb.Append("<td class=""plainlabel"" style=""width:margin:0px;border-style:solid; border-width:1px;"">" & taskdesc & "</td>")
            sb.Append("<td><input type=""text"" style=""width:0px;margin:0px;border-style:none; border-width:0px;"""" id=""" & cbi & """ value=""" & pmtskid & """></td>")
            sb.Append("<td><input type=""text"" style=""width:0px;margin:0px;border-style:none; border-width:0px;"""" id=""" & cbo & """ value=""" & seqo & """></td>")

            sb.Append("</tr>")

            'Note that cbi is now the task id
            'Task ID, New Seq, Old Seq

            If cbc = "" Then
                cbc = cbi & "," & cbx & "," & cbo
            Else
                cbc += "~" & cbi & "," & cbx & "," & cbo
            End If
        End While
        dr.Close()
        sb.Append("<tr><td colspan=""6"" align=""right""><img src=""../images/appbuttons/bgbuttons/submit.gif"" onclick=""savetasksnew();""> ")
        sb.Append("</Table>")
        lblcbdat.Value = cbc
        'lblhtml.Value = sb.ToString
        dvtasks.InnerHtml = sb.ToString
    End Sub

    Private Sub loadrtasks()
        rteid = lblrteid.Value
        typ = lblrttype.Value
        'sql = "select t.pmtskid, t.taskdesc from PMTasks t " _
        '+ "where t.rteid = '" & rteid & "' and t.rteno = 0"
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""1"" cellPadding=""2"" width=""960"" border=""0"">")
        sb.Append("<tr><td width=""30""></td><td width=""70""></td><td width=""100""></td><td width=""120""></td>")
        sb.Append("<td width=""180""></td><td width=""460""></td><td width=""0""></td></tr>")
        sb.Append("<tr><td class=""label"">Seq#</td>")
        sb.Append("<td class=""label"">Location</td>")
        sb.Append("<td class=""label"">Equipment</td>")
        sb.Append("<td class=""label"">Function</td>")
        sb.Append("<td class=""label"">Component</td>")
        sb.Append("<td class=""label"">Task Description</td>")
        sb.Append("<td></td>")
        sb.Append("</tr>")
        Dim cbx, cbi, cbo, eq, eqnum, eqdesc, func, comp, tasknum, taskdesc, ttime, pmtskid, loc, locdesc, rteno, seq, seqo
        Dim eqhold As String = ""
        Dim lochold As String = ""
        Dim cbc As String = ""
        Dim cb As Integer = 1
        If typ = "RBAS" Then 'getRBASTaskOrd
            sql = "getRBASTaskOrdtpm '" & rteid & "'"
        ElseIf typ = "RBAST" Then
            sql = "usp_getRBASTasks2T '" & rteid & "'"
        End If

        dr = fail.GetRdrData(sql)
        While dr.Read
            seq = dr.Item("seq").ToString
            loc = dr.Item("loc").ToString
            eqnum = dr.Item("eqnum").ToString
            func = dr.Item("func").ToString
            comp = dr.Item("compnum").ToString
            taskdesc = dr.Item("task").ToString
            pmtskid = dr.Item("pmtskid").ToString
            seqo = dr.Item("seqo").ToString

            cb += 1
            cbx = "cb" & cb
            cbi = "ci" & cb
            cbo = "co" & cb

            sb.Append("<tr><td><input type=""text"" style=""width:30px;margin:0px;border-style:solid; border-width:1px;"" id=""" & cbx & """ value=""" & seq & """></td>")
            sb.Append("<td class=""plainlabel"" style=""width:margin:0px;border-style:solid; border-width:1px;"">" & loc & "</td>")
            sb.Append("<td class=""plainlabel"" style=""width:margin:0px;border-style:solid; border-width:1px;"">" & eqnum & "</td>")
            sb.Append("<td class=""plainlabel"" style=""width:margin:0px;border-style:solid; border-width:1px;"">" & func & "</td>")
            sb.Append("<td class=""plainlabel"" style=""width:margin:0px;border-style:solid; border-width:1px;"">" & comp & "</td>")
            sb.Append("<td class=""plainlabel"" style=""width:margin:0px;border-style:solid; border-width:1px;"">" & taskdesc & "</td>")
            sb.Append("<td><input type=""text"" style=""width:0px;margin:0px;border-style:none; border-width:0px;"""" id=""" & cbi & """ value=""" & pmtskid & """></td>")
            sb.Append("<td><input type=""text"" style=""width:0px;margin:0px;border-style:none; border-width:0px;"""" id=""" & cbo & """ value=""" & seqo & """></td>")

            sb.Append("</tr>")

            'Note that cbi is now the task id
            'Task ID, New Seq, Old Seq

            If cbc = "" Then
                cbc = cbi & "," & cbx & "," & cbo
            Else
                cbc += "~" & cbi & "," & cbx & "," & cbo
            End If
        End While
        dr.Close()
        sb.Append("<tr><td colspan=""6"" align=""right""><img src=""../images/appbuttons/bgbuttons/submit.gif"" onclick=""savetasksnew();""> ")
        sb.Append("</Table>")
        lblcbdat.Value = cbc
        'lblhtml.Value = sb.ToString
        dvtasks.InnerHtml = sb.ToString
    End Sub
End Class