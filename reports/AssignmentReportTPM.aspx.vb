

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AssignmentReportTPM
    Inherits System.Web.UI.Page
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim tasks As New Utilities
    Dim dr As SqlDataReader
    Dim eqid, fuid, comid, typ As String
    Dim tmod As New transmod
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblexport As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdwi As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        'ADD FOR SUNCOR
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        If comi = "SUN" Then
            lblfslang.Value = "fre"
        End If
        'END ADD FOR SUNCOR
'Put user code to initialize the page here
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            typ = Request.QueryString("typ").ToString
            tasks.Open()
            If typ = "fu" Then
                fuid = Request.QueryString("fuid").ToString
                GetReport(typ, eqid, fuid)
            ElseIf typ = "co" Then
                fuid = Request.QueryString("fuid").ToString
                comid = Request.QueryString("coid").ToString
                GetReport(typ, eqid, fuid, comid)
            Else
                GetReport(typ, eqid)
            End If
            tasks.Dispose()

        End If
    End Sub
    Private Sub GetReport(ByVal typ As String, ByVal eqid As String, Optional ByVal fuid As String = "", Optional ByVal comid As String = "")
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""960"">")
        sb.Append("<tr><td class=""label16"" colspan=""12"" align=""center"">" & tmod.getlbl("cdlbl713" , "AssignmentReportTPM.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td colspan=""12"">&nbsp;</td></tr>")
        'sb.Append("<tr><td width=""50""></td><td width=""50""></td><td width=""120""></td>")
        'sb.Append("<td width=""50""></td><td width=""120""></td><td width=""120""></td>")
        'sb.Append("<td width=""97""></td><td width=""97""></td><td width=""97""></td>")
        'sb.Append("<td width=""97""></td><td width=""97""></td><td width=""97""></td></tr>")

        Select Case typ
            Case "eq"
                sql = "usp_GetAssignmentReportTPM '" & eqid & "'"
            Case "fu"
                sql = "usp_GetAssignmentReportTPM '" & eqid & "', '" & fuid & "'"
            Case "co"
                sql = "usp_GetAssignmentReportTPM '" & eqid & "', '" & fuid & "', '" & comid & "'"
        End Select

        dr = tasks.GetRdrData(sql)
        Dim start As Integer = 0
        Dim fchk As String = ""
        Dim func As String
        Dim tchk As String = ""
        Dim task As String
        Dim subtask As String
        Dim subt As String
        Dim oti, ti, rat, ts, freq, loto, cs, rd, lube, parts, tools, loc, locd As String
        Dim shift1, shift2, shift3, first, second, third As String
        While dr.Read
            If start = 0 Then
                loc = dr.Item("location").ToString
                locd = dr.Item("description").ToString
                start = 1
                sb.Append("<tr><td class=""label14"" colspan=""4"" >" & tmod.getxlbl("xlb285" , "AssignmentReportTPM.aspx.vb") & " " & dr.Item("sitename").ToString & "</td>")
                sb.Append("<td class=""label14"" colspan=""2"" >" & tmod.getxlbl("xlb286" , "AssignmentReportTPM.aspx.vb") & " " & dr.Item("dept_line").ToString & "</td>")
                sb.Append("<td class=""label14"" colspan=""6"">" & tmod.getxlbl("xlb287" , "AssignmentReportTPM.aspx.vb") & " " & dr.Item("cell_name").ToString & "</td></tr>")
                If loc <> "" Then
                    sb.Append("<tr><td class=""label14"" colspan=""4"" ></td>")
                    sb.Append("<td class=""label14"" colspan=""2"" >" & tmod.getxlbl("xlb288" , "AssignmentReportTPM.aspx.vb") & " " & loc & "</td>")
                    sb.Append("<td class=""label14"" colspan=""6"">Description: " & locd & "</td></tr>")
                End If
                sb.Append("<tr><td class=""label14"" colspan=""12"">" & tmod.getxlbl("xlb289" , "AssignmentReportTPM.aspx.vb") & " " & dr.Item("eqnum").ToString & " - " & dr.Item("eqdesc").ToString & "</td></tr>")
                sb.Append("<tr><td class=""label14"" colspan=""4"" style=""border-bottom: solid 1px gray;"">OEM: " & dr.Item("oem").ToString & "</td>")
                sb.Append("<td class=""label14"" colspan=""2"" style=""border-bottom: solid 1px gray;"">" & tmod.getxlbl("xlb290" , "AssignmentReportTPM.aspx.vb") & " " & dr.Item("model").ToString & "</td>")
                sb.Append("<td class=""label14"" colspan=""6"" style=""border-bottom: solid 1px gray;"">" & tmod.getxlbl("xlb291" , "AssignmentReportTPM.aspx.vb") & " " & dr.Item("serial").ToString & "</td></tr>")
            End If
            func = dr.Item("func").ToString
            If fchk <> func Then
                fchk = func
                tchk = ""
                sb.Append("<tr><td colspan=""12"">&nbsp;</td></tr>")
                sb.Append("<tr><td width=""70"" class=""labelsmtl"">" & tmod.getlbl("cdlbl714" , "AssignmentReportTPM.aspx.vb") & "</td><td width=""40"" class=""labelsmtl"">" & tmod.getlbl("cdlbl715" , "AssignmentReportTPM.aspx.vb") & "</td><td width=""150"" class=""labelsmtl"">" & tmod.getlbl("cdlbl716" , "AssignmentReportTPM.aspx.vb") & "</td>")
                sb.Append("<td width=""30"" class=""labelsmtl"">Qty</td><td width=""150"" class=""labelsmtl"">" & tmod.getlbl("cdlbl718" , "AssignmentReportTPM.aspx.vb") & "</td><td width=""220"" class=""labelsmtl"">" & tmod.getlbl("cdlbl719" , "AssignmentReportTPM.aspx.vb") & "</td>")
                sb.Append("<td width=""100"" class=""labelsmtl"">" & tmod.getlbl("cdlbl720" , "AssignmentReportTPM.aspx.vb") & "</td><td width=""30"" class=""labelsmtl"">R/D</td><td width=""80"" class=""labelsmtl"">" & tmod.getlbl("cdlbl722" , "AssignmentReportTPM.aspx.vb") & "</td>")
                sb.Append("<td width=""30"" class=""labelsmtl"">MINS</td><td width=""40"" class=""labelsmtl"">LOTO</td><td width=""20"" class=""labelsmtl"">CS</td></tr>")
                sb.Append("<tr><td colspan=""12""  class=""labelsmu"">" & tmod.getxlbl("xlb292" , "AssignmentReportTPM.aspx.vb") & " " & dr.Item("func").ToString & "</td></tr>")

            End If
            subtask = dr.Item("subtask").ToString
            task = dr.Item("tasknum").ToString
            freq = dr.Item("freq").ToString
            'shift1, shift2, shift3, first, second, third
            shift1 = dr.Item("shift1").ToString
            shift2 = dr.Item("shift2").ToString
            shift3 = dr.Item("shift3").ToString

            first = dr.Item("first").ToString
            second = dr.Item("second").ToString
            third = dr.Item("third").ToString

            subt = dr.Item("subt").ToString
            If freq = "0" Or freq = "" Then
                'freq = "None"
                freq = ""
                If shift1 <> "" Then
                    freq = shift1
                    If first <> "" Then
                        freq += "-" & first
                    End If
                End If
                If shift2 <> "" Then
                    If freq = "" Then
                        freq = shift2
                        If second <> "" Then
                            freq += "-" & second
                        End If
                    Else
                        freq += "<br />" & shift2
                        If second <> "" Then
                            freq += "-" & second
                        End If
                    End If  
                End If
                If shift3 <> "" Then
                    If freq = "" Then
                        freq = shift3
                        If third <> "" Then
                            freq += "-" & third
                        End If
                    Else
                        freq += "<br />" & shift3
                        If third <> "" Then
                            freq += "-" & third
                        End If
                    End If
                End If
            Else
                freq = dr.Item("freq").ToString
                If shift1 <> "" Then
                    freq += "<br />" & shift1
                    If first <> "" Then
                        freq += "-" & first
                    End If
                End If
                If shift2 <> "" Then
                    freq += "<br />" & shift2
                    If second <> "" Then
                        freq += "-" & second
                    End If
                End If
                If shift3 <> "" Then
                    freq += "<br />" & shift3
                    If third <> "" Then
                        freq += "-" & third
                    End If
                End If
            End If
            loto = dr.Item("lotoid").ToString
            If loto = "0" Then
                loto = "N"
            Else
                loto = "Y"
            End If
            cs = dr.Item("conid").ToString
            If cs = "0" Then
                cs = "N"
            Else
                cs = "Y"
            End If
            rd = dr.Item("rdid").ToString
            If rd = "1" Then
                rd = "R"
            ElseIf rd = "2" Then
                rd = "D"
            ElseIf rd = "3" Then
                rd = "A"
            Else
                cs = "M"
            End If
            parts = dr.Item("parts").ToString
            If Len(parts) = 0 OrElse parts = "None" Then
                parts = Nothing
            Else
                parts = tmod.getxlbl("xlb293", "AssignmentReportTPM.aspx.vb") & dr.Item("parts").ToString
            End If
            tools = dr.Item("tools").ToString
            If Len(tools) = 0 OrElse tools = "None" Then
                tools = Nothing
            Else
                tools = tmod.getxlbl("xlb294", "AssignmentReportTPM.aspx.vb") & dr.Item("tools").ToString
            End If
            lube = dr.Item("lubes").ToString
            If Len(lube) = 0 OrElse lube = "None" Then
                lube = Nothing
            Else
                lube = tmod.getxlbl("xlb295", "AssignmentReportTPM.aspx.vb") & dr.Item("lubes").ToString
            End If

            If subtask = 0 Then
                sb.Append("<tr><td class=""plainlabelsm"">" & freq & "</td>")
                sb.Append("<td class=""plainlabelsm"">" & task & "</td>")
                sb.Append("<td class=""plainlabelsm"">" & dr.Item("compnum").ToString & "</td>")
                sb.Append("<td class=""plainlabelsm"">" & dr.Item("cqty").ToString & "</td>")
                sb.Append("<td class=""plainlabelsm"">" & dr.Item("desig").ToString & "</td>")
                sb.Append("<td class=""plainlabelsm"">" & dr.Item("task").ToString & "</td>")
                sb.Append("<td class=""plainlabelsm"">" & dr.Item("tasktype").ToString & "</td>")
                sb.Append("<td class=""plainlabelsm"">" & rd & "</td>")
                sb.Append("<td class=""plainlabelsm"">" & tmod.getlbl("cdlbl726", "AssignmentReportTPM.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabelsm"">" & dr.Item("ttime").ToString & "</td>")
                sb.Append("<td class=""plainlabelsm"">" & loto & "</td>")
                sb.Append("<td class=""plainlabelsm"">" & cs & "</td></tr>")
            Else
                sb.Append("<tr><td></td><td></td>")
                sb.Append("<td class=""plainlabelsm"">" & tmod.getxlbl("xlb296", "AssignmentReportTPM.aspx.vb") & " [" & subtask & "]</td>")
                sb.Append("<td colspan=""10"" class=""plainlabelsm"">" & dr.Item("subt").ToString & "</td>")
            End If
            If parts <> Nothing Then
                sb.Append("<tr><td colspan=""2""></td>")
                sb.Append("<td colspan=""9"" class=""plainlabelsm"">" & parts & "</td></tr>")
            End If
            If tools <> Nothing Then
                sb.Append("<tr><td colspan=""2""></td>")
                sb.Append("<td colspan=""9"" class=""plainlabelsm"">" & tools & "</td></tr>")
            End If
            If lube <> Nothing Then
                sb.Append("<tr><td colspan=""2""></td>")
                sb.Append("<td colspan=""9"" class=""plainlabelsm"">" & lube & "</td></tr>")
            End If


            'oti = dr.Item("otaskitem").ToString
            'If Len(oti) = 0 Then oti = "None"
            'ti = dr.Item("taskitem").ToString
            'If Len(ti) = 0 Then ti = "None"
            'rat = dr.Item("rationale").ToString
            'If Len(rat) = 0 Then rat = "None Provided"
            'sb.Append("<tr><td class=""rtlabelpln"" border=""1"">" & dr.Item("taskcol").ToString & "</td>")
            'sb.Append("<td class=""cntrlabelpln"">" & oti & "</td>")
            'sb.Append("<td class=""cntrlabelpln"">" & ti & "</td>")
            'sb.Append("<td class=""endlabelpln"">" & rat & "</td></tr>")

        End While
        dr.Close()
        sb.Append("</Table>")
        tdwi.InnerHtml = sb.ToString
        'cmpToWord.TableToWord(tblexport, Response)
    End Sub
End Class
