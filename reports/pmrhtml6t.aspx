﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmrhtml6t.aspx.vb" Inherits="lucy_r12.pmrhtml6t" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TPM Route Output Report (Weekly)</title>
    <LINK href="../styles/reports.css" type="text/css" rel="stylesheet">
        <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
        <style type="text/css">
        .replabeltop2
{
font-family: Arial;
font-size: 18px;
color: Black;
font-weight:bold
}
.replabeltopplain
{
font-family: Arial;
font-size: 15px;
color: Black;
}
.replabel
{
font-family: Arial;
font-size: 13px;
color: Black;
font-weight:bold
}
.box1bg
{
border-left: solid 1px black;
border-bottom: solid 1px black;
background-color:Silver;
/*background-image:url(../reports/gbox.gif);
background-repeat:repeat;*/
}
.box1b
{
border-bottom: solid 1px black;
}
.box1a
{
    border-top: solid 1px black;
border-left: solid 1px black;
border-bottom: solid 1px black;
}
.box1
{
border-left: solid 1px black;
border-bottom: solid 1px black;
}
.box1r
{
border-left: solid 1px black;
border-bottom: solid 1px black;
border-right: solid 1px black;
}
.gbox
{
border-bottom: solid 1px black;
border-left: solid 1px black;
border-top: solid 1px black;
border-right: solid 1px black;
background-color:Silver;
}
.gbox1
{
border-left: solid 1px black;
border-bottom: solid 1px black;
background-color:Silver;
/*background-image:url(../reports/gbox.gif);
background-repeat:repeat;*/
}
.gbox1r
{
border-left: solid 1px black;
border-bottom: solid 1px black;
border-right: solid 1px black;
background-color:Silver;
/*background-image:url(../reports/gbox.gif);
background-repeat:repeat;*/
}
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divwi" runat="server">
    </div>
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblcomi" runat="server" />
    </form>
</body>
</html>
