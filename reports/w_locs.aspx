﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="w_locs.aspx.vb" Inherits="lucy_r12.w_locs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Location Options</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
    <!--
        function getminsrch() {
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=lu&site=" + sid + "&wo=", "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("tddept").innerHTML = ret[1];
                document.getElementById("lblclid").value = ret[2];
                document.getElementById("tdcell").innerHTML = ret[3];
                if (ret[2] != "" && ret[2] != "0") {
                    document.getElementById("lbltyp").value = "cell";
                }
                else {
                    document.getElementById("lbltyp").value = "dept";
                }

            }
        }
        function getlocs1() {

            var sid = document.getElementById("lblsid").value;

            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=lu&sid=" + sid + "&wo=&rlid=&eqid=&fuid=&coid=", "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("tdloc3").innerHTML = ret[2];
                document.getElementById("lbltyp").value = "loc";
            }
        }
        function clearall() {
            document.getElementById("lbldid").value = "";
            document.getElementById("tddept").innerHTML = "";
            document.getElementById("lblclid").value = "";
            document.getElementById("tdcell").innerHTML = "";
            document.getElementById("lbllid").value = "";
            document.getElementById("tdloc3").innerHTML = "";
            document.getElementById("lbldept").value = "";
            document.getElementById("tdedept").innerHTML = "";
            document.getElementById("lbltyp").value = "";
        }
        function getemd() {

            var sid = document.getElementById("lblsid").value;

            var eReturn = window.showModalDialog("w_eq23.aspx?typ=lu&sid=" + sid, "", "dialogHeight:320px; dialogWidth:320px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn
                document.getElementById("lbldept").value = ret;
                document.getElementById("tdedept").innerHTML = ret;
                document.getElementById("lbltyp").value = "eq23";
            }
        }
        function getrep() {
            var sid = document.getElementById("lblsid").value;
            var did = document.getElementById("lbldid").value;
            var clid = document.getElementById("lblclid").value;
            var locid = document.getElementById("lbllid").value;
            var dept = document.getElementById("lbldept").value;
            var who = document.getElementById("lbltyp").value;
            if (who != "") {
                window.open("w_openwobyeq.aspx?loctype=" + who + "&sid=" + sid + "&did=" + did + "&cellid=" + clid + "&locid=" + locid + "&dept=" + dept + "&date=" + Date());
            }
            else {
                alert("Location Required")
            }
        }
    //-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table width="380" style="position: absolute; left: 5px; top: 5px;">
        <tr>
            <td colspan="3" align="center" class="bigbold1" id="tdtitle" runat="server">
                Open Work Order by Equipment Report Options
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3" class="label">
                Choose Department or Location (Required)
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center" class="plainlabel" style="border-bottom: 1px solid blue;">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table cellpadding="1" cellspacing="1">
                    <tr>
                        <td id="tddepts" class="bluelabel" runat="server" width="110">
                            Department
                        </td>
                        <td width="22">
                            <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                        <td id="tdlocs1" class="bluelabel" runat="server" width="110">
                            Location
                        </td>
                        <td width="22">
                            <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                        <td width="46">
                        </td>
                    </tr>
                    <tr id="tremd" runat="server">
                        <td id="td1" class="bluelabel" runat="server">
                            EMD Departments
                        </td>
                        <td>
                            <img onclick="getemd();" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Department
                        </td>
                        <td colspan="4" id="tddept" class="plainlabel">
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Cell
                        </td>
                        <td colspan="4" id="tdcell" class="plainlabel">
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Location
                        </td>
                        <td colspan="4" id="tdloc3" class="plainlabel">
                        </td>
                    </tr>
                    <tr id="treddept" runat="server">
                        <td class="label">
                            EMD Department
                        </td>
                        <td colspan="4" id="tdedept" class="plainlabel">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center" class="plainlabel" style="border-bottom: 1px solid blue;">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="../images/appbuttons/minibuttons/2PX.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="right" class="plainlabel">
                <a href="#" onclick="getrep();">Get Report</a>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lblclid" runat="server" />
    <input type="hidden" id="lbltyp" runat="server" />
    <input type="hidden" id="lbllid" runat="server" />
    <input type="hidden" id="lbldept" runat="server" />
    </form>
</body>
</html>
