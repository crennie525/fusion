﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="mwchart.aspx.vb" Inherits="lucy_r12.mwchart" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Checklist</title>
    <link href="../styles/reports.css" type="text/css" rel="stylesheet" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .box1ltop
        {
            border-top: solid 1px black;
            border-left: solid 1px black;
        }
        .box1ltopend
        {
            border-top: solid 1px black;
            border-left: solid 1px black;
            border-right: solid 1px black;
        }
        .box1ltopbg
        {
            border-top: solid 1px black;
            border-left: solid 1px black;
            background-color: Silver;
        }
        .box1lnext
        {
            border-bottom: solid 1px black;
            border-left: solid 1px black;
        }
        .box1lnextend
        {
            border-bottom: solid 1px black;
            border-left: solid 1px black;
            border-right: solid 1px black;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="dvrep" runat="server">
    </div>
    </form>
</body>
</html>
