﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="w_dates.aspx.vb" Inherits="lucy_r12.w_dates" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report Dates</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
    <!--
        function checkdat() {
            var whi = document.getElementById("lblwhi").value;
            var sid = document.getElementById("lblsid").value;
            var eqid = document.getElementById("lbleqid").value;
            var fdate = document.getElementById("txtfrom").value;
            var tdate = document.getElementById("txtto").value;
            var who;
            var pm;
            if (whi != "eqhist") {
                var rall = document.getElementById("rball");
                var rcrit = document.getElementById("rbcrit");
                var rinc = document.getElementById("rbinc");
                var rexp = document.getElementById("rbexp");
                var rjust = document.getElementById("rbjust");
                if (rall.checked == true) {
                    who = "all";
                }
                else {
                    who = "crit";
                }
                if (rinc.checked == true) {
                    pm = "yes";
                }
                else if (rexp.checked == true) {
                    pm = "no";
                }
                else {
                    pm = "only";
                }
                window.open("totaldown.aspx?sid=" + sid + "&who=" + who + "&fdate=" + fdate + "&tdate=" + tdate + "&pm=" + pm + "&date=" + Date());
            }
            else {
                window.open("eqdownhist.aspx?sid=" + sid + "&eqid=" + eqid + "&fdate=" + fdate + "&tdate=" + tdate + "&date=" + Date());
            }
            
        }
        
    //-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <table width="310">
    <tr>
    <td colspan="3" align="center" class="bigbold1" id="tdtitle" runat="server">Total Equipment Downtime Report Options</td>
    </tr>
    <tr>
        <td colspan="3" align="center" class="plainlabel" style="border-bottom: 1px solid blue;"><img src="../images/appbuttons/minibuttons/2PX.gif" alt="" /></td>
        </tr>
        <tr>
        <td colspan="3"><img src="../images/appbuttons/minibuttons/2PX.gif" alt="" /></td>
        </tr>
        <tr>
        <td colspan="3" class="plainlabelblue" align="center">Dates are not required</td>
        </tr>
        <tr>
            <td class="label" width="110">
                From Date
            </td>
            <td width="170">
                <asp:TextBox ID="txtfrom" runat="server" ReadOnly="true" BackColor="#ECF0F3" Width="170px"></asp:TextBox>
            </td>
            <td width="40">
                <img alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg" id="btnfrom"
                    runat="server" />
            </td>
        </tr>
        <tr>
            <td class="label">
                To Date
            </td>
            <td>
                <asp:TextBox ID="txtto" runat="server" ReadOnly="true" BackColor="#ECF0F3" Width="170px"></asp:TextBox>
            </td>
            <td>
                <img alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg" id="btnto" runat="server" />
            </td>
        </tr>
        <tr>
        <td colspan="3" align="center" class="plainlabel" style="border-bottom: 1px solid blue;"><img src="../images/appbuttons/minibuttons/2PX.gif" alt="" /></td>
        </tr>
        <tr id="trall" runat="server">
        <td colspan="3" align="center" class="plainlabel"><input type="radio" id="rball" name="rb1" checked />All Records&nbsp;&nbsp;
        <input type="radio" id="rbcrit" name="rb1" />Critical Records</td>
        </tr>
        <tr id="trall1" runat="server">
        <td colspan="3" style="border-bottom: 1px solid blue;"><img src="../images/appbuttons/minibuttons/2PX.gif" alt="" /></td>
        </tr>
        
        <tr id="trpm" runat="server">
        <td colspan="3" align="center" class="plainlabel"><input type="radio" id="rbinc" name="rb2" checked />Include PM Records&nbsp;&nbsp;
        <input type="radio" id="rbexp" name="rb2" />Exclude PM Records<br />
        <input type="radio" id="rbjust" name="rb2" />Just PM Records</td>
        </tr>
        <tr id="trpm1" runat="server">
        <td colspan="3" style="border-bottom: 1px solid blue;"><img src="../images/appbuttons/minibuttons/2PX.gif" alt="" /></td>
        </tr>
        <tr>
        <td colspan="2"></td>
            <td>
                <img src="../images/appbuttons/minibuttons/savedisk1.gif" alt="" onclick="checkdat();" />
            </td>
        </tr>
    </table>
    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtfrom"
        PopupButtonID="btnfrom" >
    </cc1:CalendarExtender>
    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtto"
        PopupButtonID="btnto">
    </cc1:CalendarExtender>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblwhi" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    </form>
</body>
</html>
