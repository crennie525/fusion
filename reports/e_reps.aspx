﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="e_reps.aspx.vb" Inherits="lucy_r12.e_reps" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Jobs By Work Type and Supervisor</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <link rel="stylesheet" type="text/css" href="../styles/reports.css" />
    <style type="text/css">
        .replabeltop
        {
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 15px;
            color: Black;
            font-weight: bold;
        }
        .replabelplain
        {
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 13px;
            color: Black;
        }
        .gbox
        {
            border-bottom: solid 1px black;
            border-left: solid 1px black;
            border-top: solid 1px black;
            border-right: solid 1px black;
            background-color: Silver;
        }
        .tboxl1
        {
            border-left: solid 1px black;
            border-top: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            font-weight: bold;
            color: Black;
            text-align: left;
        }
        .tboxl
        {
            border-left: solid 1px black;
            border-top: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            font-weight: bold;
            color: Black;
            text-align: center;
        }
        .tboxr
        {
            border-left: solid 1px black;
            border-top: solid 1px black;
            border-bottom: solid 1px black;
            border-right: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            font-weight: bold;
            color: Black;
            text-align: center;
        }
        .rboxl1
        {
            border-left: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            color: Black;
            text-align: left;
        }
        .rboxl
        {
            border-left: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            color: Black;
            text-align: center;
        }
        .rboxr
        {
            border-left: solid 1px black;
            border-right: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            color: Black;
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divrep" runat="server">
    </div>
    </form>
</body>
</html>
