Imports System.Data.SqlClient
Imports System.Text
Public Class tpmweekly3
    Inherits System.Web.UI.Page
    Dim eqid As String
    Dim tpmw As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 500
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        If comi = "SUN" Then
            lblfslang.Value = "fre"
        End If
        'END ADD FOR SUNCOR
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            tpmw.Open()
            GetWeekCnt(eqid)
            tpmw.Dispose()
        End If
    End Sub
    Private Sub GetWeekCnt(ByVal eqid As String)
        Dim sb As New StringBuilder
        sql = "select count(*) from pmtaskstpm t where t.eqid = '" & eqid & "' " _
        + "and isnull(t.taskstatus, 'OK') <> 'Delete' " _
        + "and ((t.freq <= 7) or (t.fcust = 1))"
        '+ "and ((t.first is not null or t.second is not null or t.third is not null)) " _
        '+ "and (t.shift1 is not null or t.shift2 is not null or t.shift3 is not null) " _
        '+ "and ((t.freq = 1 or t.freq = 7) or (t.fcust = 1 or t.fcust is null))"
        Dim intPgCnt, intPgNav As Integer
        intPgCnt = tpmw.Scalar(sql)
        If intPgCnt > 0 Then
            Dim PageCnt As Integer
            Dim Remainder As Integer
            Remainder = (intPgCnt Mod 6)
            PageCnt = (intPgCnt - Remainder) / 6
            If Remainder > 0 Then
                PageCnt += 1
            End If
            PageSize = "6"
            Dim i As Integer
            For i = 1 To PageCnt
                GetWeek(eqid, i, PageSize, intPgNav)
            Next
            intPgNav = tpmw.PageCount1(intPgCnt, PageCnt)
            'Dim i As Integer
            'For i = 1 To intPgNav
            'GetWeek(eqid, i, PageSize, intPgNav)
            'Next
        Else
            sb.Append("<b>No TPM Task Records Found</b>")
            Response.Write(sb.ToString)

        End If


    End Sub
    Private Sub GetWeek(ByVal eqid As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal intPgNav As Integer)
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        Dim ThisPage1 As String = Request.ServerVariables("HTTP_HOST")
        Dim ns1i As Integer = nsimage.IndexOf("//")
        Dim nsstr As String = Mid(nsimage, ns1i + 3)
        Dim ns2i As Integer = nsstr.IndexOf("/")
        nsstr = Mid(nsstr, 1, ns2i)
        Dim hostflag As Integer = 0
        If nsstr <> ThisPage1 Then
            nsimage = nsimage.Replace(nsstr, ThisPage1)
            hostflag = 1
            'lblhostflag.Value = "1"
            'lblhost.Value = nsstr

        Else
            'lblhostflag.Value = "0"
        End If
        Dim cb1mon, cb1tue, cb1wed, cb1thu, cb1fri, cb1sat, cb1sun As String
        Dim cb2mon, cb2tue, cb2wed, cb2thu, cb2fri, cb2sat, cb2sun As String
        Dim cb3mon, cb3tue, cb3wed, cb3thu, cb3fri, cb3sat, cb3sun As String
        Dim eqnum, eqdesc, pic, taskdesc, picstr, funcid, cqty As String
        Dim func, comp, tasknum, fm1, ttid, oem, pmtskid As String
        Dim bclass As String
        Dim estart As Integer = 0
        Dim tcount As Integer = 0
        Dim tcnt As Integer
        Dim pcnt As Integer = 0
        Dim sb As New StringBuilder
        sb.Append("<table style=""page-break-after:always;"" border=""0"">")
        sb.Append("<tr>")
        sb.Append("<td>")

        sql = "usp_getweekly2 '" & eqid & "','" & PageNumber & "','" & PageSize & "'"

        dr = tpmw.GetRdrData(sql)
        While dr.Read
            tcount += 1
            'tcnt = dr.Item("tcnt").ToString
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqnum").ToString
            funcid = dr.Item("funcid").ToString
            pic = dr.Item("pic").ToString
            func = dr.Item("func").ToString
            comp = dr.Item("compnum").ToString
            cqty = dr.Item("cqty").ToString
            fm1 = dr.Item("fm1").ToString
            tasknum = dr.Item("tasknum").ToString
            pmtskid = dr.Item("pmtskid").ToString
            ttid = dr.Item("ttid").ToString
            oem = dr.Item("oem").ToString
            '"Dirty(___)"
            fm1 = fm1.Replace("(___)", "~")
            Dim fm1arr() As String = fm1.Split("~")
            Dim i As Integer
            Dim fm1str As String = "A: OK "
            Dim leter, frev As String
            For i = 0 To fm1arr.Length - 1
                Select Case i
                    Case 0
                        leter = "B: "
                    Case 1
                        leter = "C: "
                    Case 2
                        leter = "D: "
                    Case 3
                        leter = "E: "
                    Case 4
                        leter = "F: "
                End Select
                frev = fm1arr(i)
                'frev = Mid(frev, 5)
                If frev <> "" Then
                    fm1str += " / " & leter & frev
                End If

            Next
            Dim imgstr As String
            If ttid = 2 Then
                imgstr = "../images/appbuttons/minibuttons/tpmclean.gif"
            ElseIf ttid = 3 Then
                imgstr = "../images/appbuttons/minibuttons/tpmlube.gif"
            ElseIf ttid = 5 Or ttid = 6 Or ttid = 7 Then
                imgstr = "../images/appbuttons/minibuttons/tpminsp.gif"
            Else
                imgstr = "0"
            End If
            If pic <> "" Then
                If pic <> "0" Then
                    pcnt += 1
                End If
                'sb.Append("<td class=""box1bg label"" rowspan=""1"" valign=""top"">" & func & " - Task# " & tasknum & "</td>")
                If picstr = "" Then
                    picstr = pic & "~" & func & "~" & tasknum
                Else
                    picstr += "^" & pic & "~" & func & "~" & tasknum
                End If
            Else
                If picstr = "" Then
                    picstr = "0" & "~" & func & "~" & tasknum
                Else
                    picstr += "^" & "0" & "~" & func & "~" & tasknum
                End If
            End If
            taskdesc = dr.Item("taskdesc").ToString

            cb1mon = dr.Item("cb1mon").ToString
            cb1tue = dr.Item("cb1tue").ToString
            cb1wed = dr.Item("cb1wed").ToString
            cb1thu = dr.Item("cb1thu").ToString
            cb1fri = dr.Item("cb1fri").ToString
            cb1sat = dr.Item("cb1sat").ToString
            cb1sun = dr.Item("cb1sun").ToString

            cb2mon = dr.Item("cb2mon").ToString
            cb2tue = dr.Item("cb2tue").ToString
            cb2wed = dr.Item("cb2wed").ToString
            cb2thu = dr.Item("cb2thu").ToString
            cb2fri = dr.Item("cb2fri").ToString
            cb2sat = dr.Item("cb2sat").ToString
            cb2sun = dr.Item("cb2sun").ToString

            cb3mon = dr.Item("cb3mon").ToString
            cb3tue = dr.Item("cb3tue").ToString
            cb3wed = dr.Item("cb3wed").ToString
            cb3thu = dr.Item("cb3thu").ToString
            cb3fri = dr.Item("cb3fri").ToString
            cb3sat = dr.Item("cb3sat").ToString
            cb3sun = dr.Item("cb3sun").ToString

            If estart = 0 Then
                estart = 1
                sb.Append("<table border=""0"" width=""750"" cellspacing=""0"" cellpadding=""0"" >") 'main table start
                sb.Append("<tr>")
                sb.Append("<td colspan=""10"">")
                sb.Append("<table width=""750"">") 'hdr table start

                sb.Append("<tr>")
                sb.Append("<td width=""260""><IMG src=""../menu/mimages/custcomp.gif""></td>") '../menu/mimages/fusionsm.gif
                'sb.Append("<td align=""right"" class=""replabeltop2"">" & tmod.getxlbl("lang3417", "reports2.aspx") & "<br>" & tmod.getxlbl("lang3417a", "reports2.aspx") & "<br><font class=""plainlabel"">Page# " & PageNumber & "</font>")
                sb.Append("<td align=""right"" class=""replabeltop2"">Operator Care<br>Weekly Check Sheet<br><font class=""plainlabel"">Page# " & PageNumber & "</font>")
                sb.Append("</td>")
                sb.Append("</tr>")

                sb.Append("<tr><td colspan=""10"">&nbsp;</td></tr>")
                sb.Append("</table>") 'hdr table end

                sb.Append("<tr>")
                sb.Append("<td colspan=""10"">")
                sb.Append("<table border=""0"" width=""750"" cellspacing=""0"" cellpadding=""3"" >") 'eq id table start
                sb.Append("<tr>")
                sb.Append("<td class=""replabeltop"" width=""120"">Machine")
                sb.Append("</td>")
                sb.Append("<td class=""replabeltopplain"" width=""580"">" & eqnum)
                sb.Append("</td>")
                sb.Append("</tr>")
                sb.Append("<tr>")
                sb.Append("<td class=""replabeltop"">Manufacturer")
                sb.Append("</td>")
                sb.Append("<td class=""replabeltopplain"">" & oem)
                sb.Append("</td>")
                sb.Append("</tr>")
                sb.Append("</table>") 'eq id table end
                sb.Append("</td>")
                sb.Append("</tr>")

                'sb.Append("</td>")
                'sb.Append("</tr>")

                sb.Append("<tr height=""22"">")
                sb.Append("<td class=""box1b replabel"" width=""471"">" & tmod.getxlbl("xlb58", "PMALL.vb") & "</td>")
                sb.Append("<td class=""box1b replabel"" width=""67"">" & tmod.getxlbl("xlb272", "AssignmentReportPics.aspx.vb") & "</td>")
                sb.Append("<td class=""box1b replabel"" width=""38"">" & tmod.getxlbl("lang3417d", "reports2.aspx") & "&nbsp;</td>")
                sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">M</td>")
                sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">Tu</td>")
                sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">W</td>")
                sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">Th</td>")
                sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">F</td>")
                sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">Sa</td>")
                sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">Su</td>")
                sb.Append("</tr>")
            End If
            sb.Append("<tr height=""30"">")
            sb.Append("<td class=""box1bg label"" rowspan=""1"" valign=""top"">" & func & " - " & tmod.getxlbl("xlb381", "PMAcceptancePkg.aspx.vb") & " " & tasknum & "</td>")
            If imgstr <> "0" Then
                sb.Append("<td class=""box1 label"" rowspan=""3"" valign=""middle"" align=""center""><img src=""" & imgstr & """></td>")
            Else
                sb.Append("<td class=""box1"" rowspan=""3"" valign=""middle"" align=""center"">&nbsp;</td>")
            End If


            sb.Append("<td class=""box1 plainlabel"" align=""center"">1</td>")
            If cb1mon = "1" Then bclass = "box1" Else bclass = "gbox1"
            'If cb1mon = "1" Then
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            'Else
            '    sb.Append("<td class=""" & bclass & """ width=""20""><img src=""gbox.gif""></td>")
            'End If

            If cb1tue = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb1wed = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb1thu = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb1fri = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb1sat = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb1sun = "1" Then bclass = "box1r" Else bclass = "gbox1r"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            sb.Append("</tr>")
            sb.Append("<tr height=""30"">")
            sb.Append("<td class=""box1 plainlabel"" rowspan=""2"" valign=""top""><font class=""label"">" & comp & "(" & cqty & ") - </font>" & taskdesc & "<br>" & fm1str & "</td>")
            sb.Append("<td class=""box1 plainlabel"" align=""center"">2</td>")
            If cb2mon = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb2tue = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb2wed = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb2thu = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb2fri = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb2sat = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb2sun = "1" Then bclass = "box1r" Else bclass = "gbox1r"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            sb.Append("</tr>")
            sb.Append("<tr height=""30"">")
            sb.Append("<td class=""box1 plainlabel"" align=""center"">3</td>")
            If cb3mon = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb3tue = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb3wed = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb3thu = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb3fri = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb3sat = "1" Then bclass = "box1" Else bclass = "gbox1"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            If cb3sun = "1" Then bclass = "box1r" Else bclass = "gbox1r"
            sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</td>")
        sb.Append("</tr>")
        sb.Append("</table>")

        sb.Append("</td>")
        sb.Append("</tr>")
        sb.Append("</table>")
        'Response.Write(sb.ToString)
        'Start Pic Page
        If pcnt <> 0 Then
            Dim sb1 As New StringBuilder
            If intPgNav <> PageNumber Then
                sb.Append("<table style=""page-break-after:always;"" border=""0"">")
                sb.Append("<tr>")
                sb.Append("<td>")
            Else
                sb.Append("<table style=""page-break-after:always;"" border=""0"">")
                sb.Append("<tr>")
                sb.Append("<td>")
                'sb1.Append("<table border=""0"">")
                'sb1.Append("<tr>")
                'sb1.Append("<td>")
            End If


            sb.Append("<table border=""0"" width=""750"" cellspacing=""0"" cellpadding=""3"" >") 'main table start
            sb.Append("<tr>")
            sb.Append("<td colspan=""5"">")
            sb.Append("<table width=""750"">") 'hdr table start

            sb.Append("<tr>")
            sb.Append("<td width=""230""><IMG src=""../menu/mimages/custcomp.gif""></td>")
            sb.Append("<td align=""right"" class=""replabeltop2"">Operator Care<br>Weekly Check Sheet<br><font class=""plainlabel"">Page# " & PageNumber & "</font>")
            sb.Append("</td>")
            sb.Append("</tr>")
            sb.Append("</table>")

            sb.Append("<tr>")
            sb.Append("<td colspan=""5"">")
            sb.Append("<table border=""0"" width=""750"" cellspacing=""0"" cellpadding=""3"" >") 'eq id table start
            sb.Append("<tr>")
            sb.Append("<td class=""replabeltop"" width=""120"">Machine")
            sb.Append("</td>")
            sb.Append("<td class=""replabeltopplain"" width=""580"">" & eqnum)
            sb.Append("</td>")
            sb.Append("</tr>")
            sb.Append("<tr>")
            sb.Append("<td class=""replabeltop"">Manufacturer")
            sb.Append("</td>")
            sb.Append("<td class=""replabeltopplain"">" & oem)
            sb.Append("</td>")
            sb.Append("</tr>")
            sb.Append("</table>") 'eq id table end
            sb.Append("</td>")
            sb.Append("</tr>")

            sb.Append("<tr>")
            sb.Append("<td width=""20"">&nbsp;</td>")
            sb.Append("<td width=""320""></td>")
            sb.Append("<td width=""20"">&nbsp;</td>")
            sb.Append("<td width=""320""></td>")
            sb.Append("<td width=""20"">&nbsp;</td>")
            sb.Append("</tr>")

            '/images/appimages/tpmimg1.gif
            Dim imgstrarr() As String = picstr.Split("^")
            Dim ii As Integer
            Dim picloc, imgstr, p1 As String
            For ii = 0 To imgstrarr.Length - 1
                p1 = imgstrarr(ii)
                Dim p1arr() As String = p1.Split("~")
                picloc = p1arr(0)
                Try
                    func = p1arr(1)
                Catch ex As Exception

                End Try
                Try
                    tasknum = p1arr(2)
                Catch ex As Exception

                End Try
                If hostflag = 1 Then
                    'img = img.Replace(nsstr, ThisPage1)
                    'bimg = bimg.Replace(nsstr, ThisPage1)
                    'timg = timg.Replace(nsstr, ThisPage1)
                    picloc = picloc.Replace(nsstr, ThisPage1)
                End If
                If picloc = "0" Then
                    picloc = "../images/appimages/tpmimg1.gif"
                End If
                imgstr = "<img src=""" & picloc & """ alt = ""Image Not Found"">"

                'If ii = 0 Or ii = 2 Or ii = 4 Then
                'sb.Append("<tr>")
                'sb.Append("<td>&nbsp;</td>")
                'sb.Append("<td class=""label"" align=""center"">" & func & " - Task# " & tasknum & "</td>")
                'ElseIf ii = 1 Or ii = 3 Or ii = 5 Then
                '    sb.Append("<td>&nbsp;</td>")
                '    sb.Append("<td class=""label"" align=""center"">" & func & " - Task# " & tasknum & "</td>")
                '    sb.Append("<td>&nbsp;</td>")
                '    sb.Append("</tr>")
                'End If

                If ii = 0 Then


                    sb.Append("<tr>")
                    sb.Append("<td>&nbsp;</td>")
                    sb.Append("<td align=""center"" valign=""center"" class=""label"">" & func & " - " & tmod.getxlbl("xlb381", "PMAcceptancePkg.aspx.vb") & " " & tasknum & "<br>" & imgstr & "</td>")
                ElseIf ii = 1 Then


                    sb.Append("<td>&nbsp;</td>")
                    sb.Append("<td align=""center"" valign=""center"" class=""label"">" & func & " - " & tmod.getxlbl("xlb381", "PMAcceptancePkg.aspx.vb") & " " & tasknum & "<br>" & imgstr & "</td>")
                    sb.Append("<td>&nbsp;</td>")
                    sb.Append("</tr>")
                ElseIf ii = 2 Then


                    sb.Append("<tr>")
                    sb.Append("<td>&nbsp;</td>")
                    sb.Append("<td align=""center"" valign=""center"" class=""label"">" & func & " - " & tmod.getxlbl("xlb381", "PMAcceptancePkg.aspx.vb") & " " & tasknum & "<br>" & imgstr & "</td>")
                ElseIf ii = 3 Then


                    sb.Append("<td>&nbsp;</td>")
                    sb.Append("<td align=""center"" valign=""center"" class=""label"">" & func & " - " & tmod.getxlbl("xlb381", "PMAcceptancePkg.aspx.vb") & " " & tasknum & "<br>" & imgstr & "</td>")
                    sb.Append("<td>&nbsp;</td>")
                    sb.Append("</tr>")
                ElseIf ii = 4 Then


                    sb.Append("<tr>")
                    sb.Append("<td>&nbsp;</td>")
                    sb.Append("<td align=""center"" valign=""center"" class=""label"">" & func & " - " & tmod.getxlbl("xlb381", "PMAcceptancePkg.aspx.vb") & " " & tasknum & "<br>" & imgstr & "</td>")
                ElseIf ii = 5 Then


                    sb.Append("<td>&nbsp;</td>")
                    sb.Append("<td align=""center"" valign=""center"" class=""label"">" & func & " - " & tmod.getxlbl("xlb381", "PMAcceptancePkg.aspx.vb") & " " & tasknum & "<br>" & imgstr & "</td>")
                    sb.Append("<td>&nbsp;</td>")
                    sb.Append("</tr>")
                End If


            Next
            sb.Append("</td>")
            sb.Append("</tr>")
            sb.Append("</table>")

            sb.Append("</td>")
            sb.Append("</tr>")
            sb.Append("</table>")


            Response.Write(sb.ToString)
        Else
            Response.Write(sb.ToString)
        End If





    End Sub
End Class
