

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class TPMAlert
    Inherits System.Web.UI.Page
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim tpma As New Utilities
    Dim dr As SqlDataReader
    Dim eqid, fuid As String
    Protected WithEvents tdtpma As System.Web.UI.HtmlControls.HtmlTableCell
    Dim tmod As New transmod

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            fuid = Request.QueryString("fuid").ToString
            tpma.Open()
            GetTPA(fuid)
            tpma.Dispose()
        End If
    End Sub
    Private Sub GetTPA(ByVal fuid As String)
        Dim sb As New StringBuilder

        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td colspan=""3"" align=""center"" class=""bigbold"">" & tmod.getlbl("cdlbl1212" , "TPMAlert.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td>&nbsp;</td></tr>")
        sb.Append("<tr><td width=""50"" class=""label""><u>" & tmod.getxlbl("xlb440" , "TPMAlert.aspx.vb") & "</u></td>")
        sb.Append("<td width=""180"" class=""label""><u>" & tmod.getxlbl("xlb441" , "TPMAlert.aspx.vb") & "</u></td>")
        sb.Append("<td width=""390"" class=""label""><u>" & tmod.getxlbl("xlb442" , "TPMAlert.aspx.vb") & "</td></u></tr>")

        sql = "usp_checktpmfunc '" & fuid & "'"
        dr = tpma.GetRdrData(sql)
        While dr.Read
            sb.Append("<tr height=""24""><td><a href=""#"" onclick=""handleexit('" & dr.Item("comid").ToString & "','" & dr.Item("tasknum").ToString & "')"" class=""A1"">" & dr.Item("tasknum").ToString & "</td>")
            sb.Append("<td width=""180"" class=""plainlabel"">" & dr.Item("compnum").ToString & "</td>")
            sb.Append("<td width=""390"" class=""plainlabel"">" & dr.Item("taskdesc").ToString & "</tr>")
        End While
        dr.Close()
        sb.Append("</table>")

        'Response.Write(sb.ToString)
        tdtpma.InnerHtml = sb.ToString
    End Sub
End Class
