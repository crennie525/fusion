﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmrhtml7np.aspx.vb" Inherits="lucy_r12.pmrhtml7np" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PM Route Output Report</title>
    <link href="../styles/reports.css" type="text/css" rel="stylesheet" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
    .demtop  
    {
    height: 10px;
    border-top: solid 2px gray;
} 
.dembot  
    {
    height: 10px;
    border-bottom: solid 2px gray;
} 

    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divwi" runat="server">
    </div>
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblcomi" runat="server" />
    </form>
</body>
</html>