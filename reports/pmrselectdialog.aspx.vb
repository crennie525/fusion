﻿Public Class pmrselectdialog
    Inherits System.Web.UI.Page
    Dim eqstr, who As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            who = Request.QueryString("who").ToString
            lblwho.Value = who
            eqstr = Request.QueryString("eqstr").ToString '"(825, 827)" '
            eqstr = eqstr.Replace("'", "")
            eqstr = "(" & eqstr & ")"
            lbleqstr.Value = eqstr
            iff.Attributes.Add("src", "pmrselect.aspx?eqstr='" + eqstr + "&who=" + who)
        End If
    End Sub

End Class