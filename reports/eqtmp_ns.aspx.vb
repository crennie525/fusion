﻿Imports System.Data.SqlClient

Public Class eqtmp_ns
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim rep As New Utilities
    Dim dr As SqlDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sql = "select * from eq_temp"
        Dim lid, did, clid, eqid, eqnum, eqdesc As String
        Dim sb As New StringBuilder
        rep.Open()
        dr = rep.GetRdrData(sql)
        sb.Append("<table>")
        'dr = rep.GetRdrData(sql)
        'If dr.Read Then
        Dim mcnt As String
        While dr.Read
            lid = dr.Item("locid").ToString
            did = dr.Item("dept_id").ToString
            mcnt = dr.Item("mcnt").ToString
            clid = dr.Item("cellid").ToString
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            eqnum = Replace(eqnum, "/", "//", , , vbTextCompare)
            eqnum = Replace(eqnum, """", Chr(180) & Chr(180), , , vbTextCompare)
            eqnum = ModString(eqnum)
            eqdesc = dr.Item("eqdesc").ToString
            eqdesc = Replace(eqdesc, "/", "//", , , vbTextCompare)
            eqdesc = Replace(eqdesc, """", Chr(180) & Chr(180), , , vbTextCompare)
            eqdesc = ModString(eqdesc)
            sb.Append("<tr><td>")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getdeq('" & did & "','" & clid & "','" & eqid & "','" & eqnum & "','" & eqdesc & "','" & mcnt & "','" & lid & "')"">")
            sb.Append(eqnum & " - " & eqdesc)
            sb.Append("</td></tr>")
        End While
        'Else
        'sb.Append("<tr><td class=""plainlabelred"">" & tmod.getlbl("cdlbl1211" , "reports2.aspx.vb") & "</td></tr>")
        'End If
        dr.Close()
        rep.Dispose()
        sb.Append("</table>")
        diveq.InnerHtml = sb.ToString
    End Sub
    Private Function ModString(ByVal str As String)
        str = Replace(str, "'", Chr(180), , , vbTextCompare)
        str = Replace(str, "--", "-", , , vbTextCompare)
        str = Replace(str, ";", ":", , , vbTextCompare)
        Return str
    End Function
End Class