

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class clientproc
    Inherits System.Web.UI.Page
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim old As New Utilities
    Dim oldid, typ As String
    Dim tmod As New transmod
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        

'Put user code to initialize the page here
        If Not IsPostBack Then
            oldid = "1" 'Request.QueryString("oldid").ToString
            typ = "2" 'Request.QueryString("typ").ToString
            old.Open()
            PopOLD(oldid, typ)
            old.Dispose()
        End If
    End Sub
    Private Sub PopOLD(ByVal oldid As String, ByVal typ As String)
        Dim fid As String = ""
        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim subtask As String = ""
        Dim sb As New System.Text.StringBuilder

        sb.Append("<html><head>")
        sb.Append("<LINK href=""../styles/pmcssa1.css"" type=""text/css"" rel=""stylesheet"">")
        sb.Append("</head><body>")

        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""620""></td></tr>")
        sb.Append("<tr><td class=""bigbold"" align=""center"">" & tmod.getlbl("cdlbl727" , "clientproc.aspx.vb") & "</td></tr>")

        sb.Append("<tr><td><Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""120"">&nbsp;</td>")
        sb.Append("<td width=""500"">&nbsp;</td></tr>")

        sql = "select cp.pmtitle, " _
        + "cp.eqnum, cp.skill from client_procs cp " _
        + "where cp.oldid = '" & oldid & "'"

        Dim pmt, eq, sk As String

        dr = old.GetRdrData(sql)
        While dr.Read
            pmt = dr.Item("pmtitle").ToString
            eq = dr.Item("eqnum").ToString
            sk = dr.Item("skill").ToString
            sb.Append("<tr><td width=""120"" class=""label"">" & tmod.getlbl("cdlbl728" , "clientproc.aspx.vb") & "</td>")
            sb.Append("<td width=""500"" class=""plainlabel"">" & pmt & "</td></tr>")
            sb.Append("<tr><td width=""120"" class=""label"">" & tmod.getlbl("cdlbl729" , "clientproc.aspx.vb") & "</td>")
            sb.Append("<td width=""500"" class=""plainlabel"">" & eq & "</td></tr>")
            sb.Append("<tr><td width=""120"" class=""label"">" & tmod.getlbl("cdlbl730" , "clientproc.aspx.vb") & "</td>")
            sb.Append("<td width=""500"" class=""plainlabel"">" & sk & "</td></tr></table></td></tr>")
            sb.Append("<tr><td>&nbsp;</td></tr>")
        End While
        dr.Close()
        sb.Append("<tr><td>&nbsp;</td></tr>")
        sb.Append("<tr><td><Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")

        Dim tnum, td, fm, sk1, qt, st, lube, tool, part, bp, bm As String

        If typ = "2" Then
            sb.Append("<tr><td class=""label"" colspan=""3""><u>" & tmod.getxlbl("xlb297" , "clientproc.aspx.vb") & "</u></td></tr>")
            sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""580""></td></tr>")

            sql = "select cps.stepnum, cps.task, cps.skill, cps.qty, cps.time, cpp.parts, cpt.tools, cpl.lubes " _
                   + "from client_procs_steps cps " _
                   + "left join client_procs_lubes cpl on cps.stepid = cpl.stepid " _
                   + "left join client_procs_tools cpt on cps.stepid = cpt.stepid " _
                   + "left join client_procs_parts cpp on cps.stepid = cpp.stepid " _
                   + "where cps.oldid = '" & oldid & "' order by cps.stepnum"


            dr = old.GetRdrData(sql)
            While dr.Read
                tnum = dr.Item("stepnum").ToString
                td = dr.Item("task").ToString
                sk = dr.Item("skill").ToString
                qt = dr.Item("qty").ToString
                st = dr.Item("time").ToString
                lube = dr.Item("lubes").ToString
                tool = dr.Item("tools").ToString
                part = dr.Item("parts").ToString
                sb.Append("<tr><td class=""plainlabel"">[" & tnum & "]</td>")
                If Len(td) > 0 Then
                    sb.Append("<td colspan=""2"" class=""plainlabel"">" & td & "</td></tr>")
                Else
                    sb.Append("<td colspan=""2"" class=""plainlabel"">" & tmod.getlbl("cdlbl731" , "clientproc.aspx.vb") & "</td></tr>")
                End If
                If sk <> "" And sk <> "Select" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb298" , "clientproc.aspx.vb") & "&nbsp;&nbsp;</b>" & sk)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Qty:&nbsp;&nbsp;</b>" & qt)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Time:&nbsp;&nbsp;</b>" & st & "</td></tr>")
                End If
                If lube <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb299" , "clientproc.aspx.vb") & "&nbsp;&nbsp;</b>" & lube & "</td></tr>")
                End If
                If tool <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb300" , "clientproc.aspx.vb") & "&nbsp;&nbsp;</b>" & tool & "</td></tr>")
                End If
                If part <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb301" , "clientproc.aspx.vb") & "&nbsp;&nbsp;</b>" & part & "</td></tr>")
                End If
                sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>")
            End While
            dr.Close()
            sb.Append("</table></td></tr></table>")
            sb.Append("</body></html>")
            Response.Write(sb.ToString)
        Else
            sql = "select cpmb.matbulk from client_procs_mb cpmb " _
                  + "where cpmb.oldid = '" & oldid & "'"

            dr = old.GetRdrData(sql)
            While dr.Read
                sb.Append("<tr><td class=""label"" colspan=""3""><u>" & tmod.getxlbl("xlb302" , "clientproc.aspx.vb") & "</u></td></tr>")
                sb.Append("<tr><td width=""620""></td></tr>")
                bm = dr.Item("matbulk").ToString
                sb.Append("<td colspan=""3"" class=""plainlabel"">" & bm & "</td></tr>")
                sb.Append("</table></td></tr></table>")
            End While
            dr.Close()

            sb.Append("<tr><td class=""label"" colspan=""3""><u>" & tmod.getxlbl("xlb303" , "clientproc.aspx.vb") & "</u></td></tr>")
            sb.Append("<tr><td width=""620""></td></tr>")

            sql = "select cpld.bulkproc from client_procs_ld cpld " _
                   + "where cpld.oldid = '" & oldid & "'"

            dr = old.GetRdrData(sql)
            While dr.Read
                bp = dr.Item("bulkproc").ToString
                sb.Append("<td colspan=""3"" class=""plainlabel"">" & bp & "</td></tr>")
                sb.Append("</table></td></tr></table>")
            End While
            dr.Close()
            sb.Append("</body></html>")
            Response.Write(sb.ToString)
        End If

    End Sub
End Class
