

'********************************************************
'*
'********************************************************



Public Class PGEMSTXTtoHTM
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim pgem As New PGEMSHTM2
    Dim eqid, mode, longstring As String
    Protected WithEvents tdinput As System.Web.UI.HtmlControls.HtmlTableCell

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        eqid = Request.QueryString("eqid").ToString
        mode = Request.QueryString("mode").ToString
        If mode = "1" Then
            longstring = pgem.WritePMHTM(eqid)
        Else
            longstring = pgem.WritePMHTM(eqid, "PdM")
        End If

        'Response.Write(longstring)
        tdinput.InnerHtml = longstring
    End Sub

End Class
