﻿Imports System.Data.SqlClient
Imports System.Text
Public Class w_3monthdown
    Inherits System.Web.UI.Page
    Dim wrep As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim sdate, loctype, locid, did, cellid, dept, sid, fdate, rep As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString '"23" '
            sdate = Request.QueryString("sdate").ToString '"12/31/2011" '
            loctype = Request.QueryString("loctype").ToString '"loc" '
            locid = Request.QueryString("locid").ToString '"13792" '
            did = Request.QueryString("did").ToString '
            cellid = Request.QueryString("cellid").ToString '"0" '
            dept = Request.QueryString("dept").ToString '"0" '
            rep = Request.QueryString("rep").ToString '"0" '
            wrep.Open()
            getreport()
            wrep.Dispose()
        End If
    End Sub
    Private Sub getreport()
        Dim sb As New StringBuilder
        'usp_gettotaldown3(@who varchar(10), @sid int, @sdate datetime, @did int, @clid int, @locid int, @dept varchar(50))
        If rep = "eq" Then
            sql = "usp_gettotaldown3 '" & loctype & "','" & sid & "','" & sdate & "','" & did & "','" & cellid & "','" & locid & "','" & dept & "'"
        Else
            sql = "usp_gettotaldown3p '" & loctype & "','" & sid & "','" & sdate & "','" & did & "','" & cellid & "','" & locid & "','" & dept & "'"
        End If

        Dim test As String = sql
        Dim cmd As New SqlCommand
        If rep = "eq" Then
            cmd.CommandText = "exec usp_gettotaldown3 @who, @sid, @sdate, @did, @clid, @locid, @dept"
        Else
            cmd.CommandText = "exec usp_gettotaldown3 @who, @sid, @sdate, @did, @clid, @locid, @dept"
        End If


        Dim param01 = New SqlParameter("@who", SqlDbType.VarChar)
        If loctype = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = loctype
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@sid", SqlDbType.VarChar)
        If sid = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = sid
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@sdate", SqlDbType.VarChar)
        If sdate = "" Then
            param03.Value = System.DBNull.Value
        Else
            param03.Value = sdate
        End If
        cmd.Parameters.Add(param03)
        Dim param04 = New SqlParameter("@did", SqlDbType.VarChar)
        If did = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = did
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@clid", SqlDbType.VarChar)
        If cellid = "" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = cellid
        End If
        cmd.Parameters.Add(param05)
        Dim param06 = New SqlParameter("@locid", SqlDbType.VarChar)
        If locid = "" Then
            param06.Value = System.DBNull.Value
        Else
            param06.Value = locid
        End If
        cmd.Parameters.Add(param06)
        Dim param07 = New SqlParameter("@dept", SqlDbType.VarChar)
        If dept = "" Then
            param07.Value = System.DBNull.Value
        Else
            param07.Value = dept
        End If
        cmd.Parameters.Add(param07)



        sql = "select dateadd(mm,-3,cast('" & sdate & "' as datetime))"
        fdate = wrep.strScalar(sql)

        sb.Append("<table cellspacing=""0"" cellpadding=""3"" width=""700"">")

        'sb.Append("<tr>")
        'sb.Append("<td width=""162""></td>")
        'sb.Append("<td width=""162""></td>")
        'sb.Append("<td width=""162""></td>")
        'sb.Append("<td width=""162""></td>")
        'sb.Append("<td width=""52""></td>")
        'sb.Append("</tr>")

        If rep = "eq" Then
            sb.Append("<tr>")
            sb.Append("<td colspan=""5"" align=""center"" class=""replabeltop"">Equipment Total Down Report (3 Month)</td>")
            sb.Append("</tr>")
        Else
            sb.Append("<tr>")
            sb.Append("<td colspan=""5"" align=""center"" class=""replabeltop"">Production Total Down Report (3 Month)</td>")
            sb.Append("</tr>")
        End If

        sb.Append("<tr>")
        sb.Append("<td colspan=""5"" align=""center"" class=""plainlabel"">" & fdate & " to " & sdate & "</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""5"" align=""center"" class=""plainlabel""><i>P/E indicates Production Down (P), Equipment Down (E), Both (PE), or Neither Applies (N/A)</i></td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""5"" align=""center"" class=""plainlabel""><i>Red text indicates that the Equipment is currently Down</i></td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""5"" align=""center""><img src=""../images/appbuttons/minibuttons/2PX.gif""></td>")
        sb.Append("</tr>")

        Dim hdflg As Integer = 1
        Dim woflg As Integer = 0
        hdflg = 0
        Dim loc, desc, eq, eqdesc, tt, wonum, wodesc, repdate, wostat, esthrs, wocnt, dt As String
        Dim isdown As Integer = 0
        Dim isdownp As Decimal = 0
        Dim gbox As String = "plainlabel gbox"
        Dim nbox As String = "plainlabel"
        Dim gboxd As String = "plainlabelred gbox"
        Dim nboxd As String = "plainlabelred"
        Dim bclass, wclass As String

        dr = wrep.GetRdrDataHack(cmd)

        While dr.Read
            loc = dr.Item("Location").ToString
            desc = dr.Item("Location_Description").ToString
            eq = dr.Item("Equipment").ToString
            eqdesc = dr.Item("Description").ToString
            tt = dr.Item("Total_Down").ToString
            wonum = dr.Item("wonum").ToString
            wodesc = dr.Item("wodesc").ToString
            repdate = dr.Item("reportdate").ToString
            wostat = dr.Item("status").ToString
            esthrs = dr.Item("required").ToString
            isdown = dr.Item("isdown").ToString
            isdownp = dr.Item("isdownp").ToString
            wocnt = dr.Item("wocnt").ToString

            If isdown = "1" And isdownp = "1" Then
                dt = "PE"
            ElseIf isdown = "1" And isdownp = "0" Then
                dt = "E"
            ElseIf isdown = "0" And isdownp = "1" Then
                dt = "P"
            Else
                dt = "N/A"
            End If

            If wocnt = "0" And wonum = "0" Then
                If isdown = 1 Or isdownp = 1 Then
                    bclass = gboxd
                Else
                    bclass = gbox
                End If

            ElseIf wonum = "0" Then
                If isdown = 1 Or isdownp = 1 Then
                    bclass = nboxd
                Else
                    bclass = nbox
                End If
            End If

            If wonum <> "0" And (isdown = 1 Or isdownp = 1) Then
                wclass = gboxd

            ElseIf wonum <> "0" Then
                wclass = gbox
            End If

            If woflg = 1 And wonum = "0" Then
                woflg = 0
                sb.Append("</table></td></tr>")

                sb.Append("<tr>")
                sb.Append("<td colspan=""5"" align=""center""><img src=""../images/appbuttons/minibuttons/2PX.gif""></td>")
                sb.Append("</tr>")
            End If

            If hdflg = 0 And woflg = 0 Then

                hdflg = 1

                If loctype = "dept" Or loctype = "cell" Then
                    sb.Append("<tr>")
                    sb.Append("<td align=""left"" class=""label"">Department:</td>")
                    sb.Append("<td align=""left"" class=""plainlabel"" colspan=""4"">" & loc & " - " & desc & "</td>")
                    sb.Append("</tr>")
                ElseIf loctype = "loc" Then
                    sb.Append("<tr>")
                    sb.Append("<td align=""left"" class=""label"">Location:</td>")
                    sb.Append("<td align=""left"" class=""plainlabel"" colspan=""4"">" & loc & " - " & desc & "</td>")
                    sb.Append("</tr>")
                ElseIf loctype = "eq23" Then
                    sb.Append("<tr>")
                    sb.Append("<td align=""left"" class=""label"">EMD Department:</td>")
                    sb.Append("<td align=""left"" class=""plainlabel"" colspan=""4"">" & loc & "</td>")
                    sb.Append("</tr>")
                End If


                sb.Append("<tr>")
                sb.Append("<td colspan=""5"" align=""center""><img src=""../images/appbuttons/minibuttons/2PX.gif""></td>")
                sb.Append("</tr>")

                sb.Append("<tr>")
                sb.Append("<td width=""125"" class=""label""><u>Location</u></td>")
                sb.Append("<td width=""185"" class=""label""><u>Location Description</u></td>")
                sb.Append("<td width=""125"" class=""label""><u>Equipment</u></td>")
                sb.Append("<td width=""185"" class=""label""><u>Equipment Description</u></td>")
                sb.Append("<td width=""80"" class=""label""><u>Total Down</u></td>")
                sb.Append("</tr>")

            ElseIf wonum <> 0 And woflg = 0 Then

                woflg = 1

                sb.Append("<tr>")
                sb.Append("<td colspan=""5"" align=""center""><img src=""../images/appbuttons/minibuttons/2PX.gif""></td>")
                sb.Append("</tr>")

                sb.Append("<tr>")
                sb.Append("<td colspan=""5"" align=""right"">")
                sb.Append("<table width=""650"" border=""0"" cellspacing=""0"" cellpadding=""3""><tr>")
                sb.Append("<td width=""70px"" class=""label"" align=""left""><u>Wonum</u></td>")
                sb.Append("<td width=""350px"" class=""label"" align=""left""><u>Description</u></td>")
                sb.Append("<td width=""70px"" class=""label"" align=""left""><u>Report Date</u></td>")
                sb.Append("<td width=""60px"" class=""label"" align=""left""><u>Status</u></td>")
                sb.Append("<td width=""70px"" class=""label"" align=""center""><u>Est Hours</u></td>")
                sb.Append("<td width=""30px"" class=""label"" align=""center""><u>P/E</u></td>")
                sb.Append("</tr>")
                sb.Append("<tr>")
                sb.Append("<td colspan=""6"" align=""center""><img src=""../images/appbuttons/minibuttons/2PX.gif""></td>")
                sb.Append("</tr>")
                sb.Append("</tr>")



            End If

            If wonum = 0 Then
                sb.Append("<tr>")
                sb.Append("<td class=""" & bclass & """>" & loc & "</td>")
                sb.Append("<td class=""" & bclass & """>" & desc & "</td>")
                sb.Append("<td class=""" & bclass & """>" & eq & "</td>")
                sb.Append("<td class=""" & bclass & """>" & eqdesc & "</td>")
                sb.Append("<td class=""" & bclass & """ align=""center"">" & tt & "</td>")
                sb.Append("</tr>")
            Else
                sb.Append("<tr>")
                sb.Append("<td class=""" & wclass & """ align=""left"">" & wonum & "</td>")
                sb.Append("<td class=""" & wclass & """ align=""left"">" & wodesc & "</td>")
                sb.Append("<td class=""" & wclass & """ align=""left"">" & repdate & "</td>")
                sb.Append("<td class=""" & wclass & """ align=""left"">" & wostat & "</td>")
                sb.Append("<td class=""" & wclass & """ align=""center"">" & esthrs & "</td>")
                sb.Append("<td class=""" & wclass & """ align=""center"">" & dt & "</td>")
                sb.Append("</tr>")
            End If



        End While
        dr.Close()

        If hdflg = 0 Then
            sb.Append("<tr>")
            sb.Append("<td colspan=""5"" align=""center"" class=""redlabel"">No Records Found</td>")
            sb.Append("</tr>")
        End If

        sb.Append("</table>")

        dvrep.InnerHtml = sb.ToString

    End Sub
End Class