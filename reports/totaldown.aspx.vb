﻿Imports System.Data.SqlClient
Imports System.Text

Public Class totaldown
    Inherits System.Web.UI.Page
    Dim etd As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim coi, who, fdate, tdate, sid, titl, pm, stitl As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            who = Request.QueryString("who").ToString
            lblwho.Value = who
            pm = Request.QueryString("pm").ToString
            lblpm.Value = pm
            fdate = Request.QueryString("fdate").ToString
            tdate = Request.QueryString("tdate").ToString
            etd.Open()
            If tdate <> "" And fdate = "" Then
                getfdate()
            End If
            lblfdate.Value = fdate
            lbltdate.Value = tdate
            getrep()
            etd.Dispose()
        Else
            etd.Open()
            sid = lblsid.Value
            who = lblwho.Value
            pm = lblpm.Value
            fdate = lblfdate.Value
            tdate = lbltdate.Value
            etd.Open()
            getrep()
            etd.Dispose()
        End If
        

    End Sub
    Private Sub getfdate()
        sql = "select isnull(Convert(char(10),min(isnull(startdown, '01/01/1900')),101), '01/01/1900') from eqhist and startdown is not null"
        fdate = etd.strScalar(sql)
    End Sub
    Private Sub getrep()
        Dim sb As New StringBuilder
        sql = "usp_gettotaldown '" & who & "','" & sid & "','" & fdate & "','" & tdate & "','" & pm & "'"
        If who = "all" Then
            titl = " - All Records"
        Else
            titl = " - Critical Records"
        End If
        If pm = "yes" Then
            stitl = "(includes PM Records)"
        ElseIf pm = "no" Then
            stitl = "(does not include PM Records)"
        Else
            stitl = "(PM Records Only)"
        End If
        sb.Append("<table cellspacing=""0"" cellpadding=""4"" width=""700"">")
        sb.Append("<tr>")
        sb.Append("<td colspan=""6"" align=""center"" class=""replabeltop"">Equipment Total Down Report" & titl & "</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""6"" align=""center"" class=""replabeltops"">" & stitl & "</td>")
        sb.Append("</tr>")
        If fdate <> "" And fdate <> "01/01/1900" Then
            sb.Append("<tr>")
            sb.Append("<td colspan=""6"" align=""center"" class=""plainlabel"">From " & fdate & " to " & tdate & "</td>")
            sb.Append("</tr>")
        ElseIf fdate <> "" Then
            sb.Append("<tr>")
            sb.Append("<td colspan=""6"" align=""center"" class=""plainlabel"">To " & tdate & "</td>")
            sb.Append("</tr>")
        Else
            sb.Append("<tr>")
            sb.Append("<td colspan=""6"" align=""center"" class=""plainlabel"">" & Now & "</td>")
            sb.Append("</tr>")
        End If
        sb.Append("<tr>")
        sb.Append("<td colspan=""6"">&nbsp;</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td class=""tboxl1"" width=""120"">Location</td>")
        sb.Append("<td class=""tboxl1"" width=""160"">Location Description</td>")
        sb.Append("<td class=""tboxl1"" width=""120"">Equipment</td>")
        sb.Append("<td class=""tboxl1"" width=""160"">Equipment Description</td>")
        sb.Append("<td class=""tboxl1"" width=""60"">Status</td>")
        sb.Append("<td class=""tboxr"" width=""80"">Total Down</td>")
        sb.Append("</tr>")
        Dim eq, eqd, lo, lod, st, td As String
        Dim cl As String = "rboxl1"
        Dim cr As String = "rboxr"
        dr = etd.GetRdrData(sql)
        While dr.Read
            eq = dr.Item("Equipment").ToString
            eqd = dr.Item("Description").ToString
            lo = dr.Item("Location").ToString
            lod = dr.Item("Location_Description").ToString
            st = dr.Item("Status").ToString
            td = dr.Item("Total_Down").ToString
            If st = "Down" Then
                cl = "rboxl1r"
                cr = "rboxrr"
            Else
                cl = "rboxl1"
                cr = "rboxr"
            End If
            sb.Append("<tr>")
            sb.Append("<td class=""" & cl & """>" & lo & "</td>")
            sb.Append("<td class=""" & cl & """>" & lod & "</td>")
            sb.Append("<td class=""" & cl & """>" & eq & "</td>")
            sb.Append("<td class=""" & cl & """>" & eqd & "</td>")
            sb.Append("<td class=""" & cl & """>" & st & "</td>")
            sb.Append("<td class=""" & cr & """>" & td & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()


        sb.Append("</table>")
        divrep.InnerHtml = sb.ToString

    End Sub
End Class