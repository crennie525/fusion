<%@ Page Language="vb" AutoEventWireup="false" Codebehind="origrevtpm.aspx.vb" Inherits="lucy_r12.origrevtpm" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>origrevtpm</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/origrevtpmaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
			</table>
			<table>
				<tr>
					<td class="bluelabel" width="100"><asp:Label id="lang3345" runat="server">Current Asset:</asp:Label></td>
					<td class="plainlabelblue" id="tdeqnum" runat="server" width="250"></td>
					<td width="40"><img src="../images/appbuttons/minibuttons/magnifier.gif" onclick="geteq();"></td>
					<td class="bluelabel" width="100"><asp:Label id="lang3346" runat="server">Select Output</asp:Label></td>
					<td width="100"><asp:dropdownlist id="ddout" runat="server" AutoPostBack="True">
							<asp:ListItem Value="Select">Select</asp:ListItem>
							<asp:ListItem Value="Original" Selected="True">Original</asp:ListItem>
							<asp:ListItem Value="Revised">Revised</asp:ListItem>
						</asp:dropdownlist></td>
					<td width="200" align="right"><img src="../images/appbuttons/minibuttons/excelexp.gif" onclick="exportit();"></td>
				</tr>
			</table>
			<tr>
				<td>
					<asp:DataGrid id="dgexport" runat="server" AutoGenerateColumns="False">
						<ItemStyle Font-Size="X-Small" Font-Names="Arial"></ItemStyle>
						<HeaderStyle Font-Size="X-Small" Font-Names="Arial" CssClass="btmmenu plainlabel" BackColor="Blue"></HeaderStyle>
						<Columns>
							<asp:BoundColumn DataField="func" HeaderText="Function"></asp:BoundColumn>
							<asp:BoundColumn DataField="task" HeaderText="Task#"></asp:BoundColumn>
							<asp:BoundColumn DataField="taskstatus" HeaderText="Task Status"></asp:BoundColumn>
							<asp:BoundColumn DataField="freq" HeaderText="Frequency"></asp:BoundColumn>
							<asp:BoundColumn DataField="freqyr" HeaderText="Freq/Yr"></asp:BoundColumn>
							<asp:BoundColumn DataField="origttime" HeaderText="Orig Time"></asp:BoundColumn>
							<asp:BoundColumn DataField="origqty" HeaderText="Orig Qty"></asp:BoundColumn>
							<asp:BoundColumn DataField="yr_origttime" HeaderText="Orig Time Yr"></asp:BoundColumn>
							<asp:BoundColumn DataField="origrdt" HeaderText="Orig DT"></asp:BoundColumn>
							<asp:BoundColumn DataField="yr_origrdt" HeaderText="Orig DT Yr"></asp:BoundColumn>
							<asp:BoundColumn DataField="opcost" HeaderText="Orig Part Cost"></asp:BoundColumn>
							<asp:BoundColumn DataField="yr_opcost" HeaderText="Orig Part Cost Yr"></asp:BoundColumn>
							<asp:BoundColumn DataField="otcost" HeaderText="Orig Tool Cost"></asp:BoundColumn>
							<asp:BoundColumn DataField="yr_otcost" HeaderText="Orig Tool Cost Yr"></asp:BoundColumn>
							<asp:BoundColumn DataField="olcost" HeaderText="Orig Lube Cost"></asp:BoundColumn>
							<asp:BoundColumn DataField="yr_olcost" HeaderText="Orig Lube Cost Yr"></asp:BoundColumn>
						</Columns>
					</asp:DataGrid></td>
			</tr>
			<tr>
				<td>
					<asp:DataGrid id="dgexportrev" runat="server" AutoGenerateColumns="False">
						<ItemStyle Font-Size="X-Small" Font-Names="Arial"></ItemStyle>
						<HeaderStyle Font-Size="X-Small" Font-Names="Arial" BackColor="Blue"></HeaderStyle>
						<Columns>
							<asp:BoundColumn DataField="func" HeaderText="Function"></asp:BoundColumn>
							<asp:BoundColumn DataField="task" HeaderText="Task#"></asp:BoundColumn>
							<asp:BoundColumn DataField="taskstatus" HeaderText="Task Status"></asp:BoundColumn>
							<asp:BoundColumn DataField="freq" HeaderText="Frequency"></asp:BoundColumn>
							<asp:BoundColumn DataField="freqyr" HeaderText="Freq/Yr"></asp:BoundColumn>
							<asp:BoundColumn DataField="origttime" HeaderText="Rev Time"></asp:BoundColumn>
							<asp:BoundColumn DataField="origqty" HeaderText="Rev Qty"></asp:BoundColumn>
							<asp:BoundColumn DataField="yr_origttime" HeaderText="Rev Time Yr"></asp:BoundColumn>
							<asp:BoundColumn DataField="origrdt" HeaderText="Rev DT"></asp:BoundColumn>
							<asp:BoundColumn DataField="yr_origrdt" HeaderText="Rev DT Yr"></asp:BoundColumn>
							<asp:BoundColumn DataField="opcost" HeaderText="Rev Part Cost"></asp:BoundColumn>
							<asp:BoundColumn DataField="yr_opcost" HeaderText="Rev Part Cost Yr"></asp:BoundColumn>
							<asp:BoundColumn DataField="otcost" HeaderText="Rev Tool Cost"></asp:BoundColumn>
							<asp:BoundColumn DataField="yr_otcost" HeaderText="Rev Tool Cost Yr"></asp:BoundColumn>
							<asp:BoundColumn DataField="olcost" HeaderText="Rev Lube Cost"></asp:BoundColumn>
							<asp:BoundColumn DataField="yr_olcost" HeaderText="Rev Lube Cost Yr"></asp:BoundColumn>
						</Columns>
					</asp:DataGrid></td>
			</tr>
			</TABLE> <input type="hidden" id="lbleqid" runat="server" NAME="lbleqid"> <input type="hidden" id="lblsubmit" runat="server" NAME="lblsubmit">
			<input type="hidden" id="lblsid" runat="server" NAME="lblsid"> <input type="hidden" id="lbleqnum" runat="server" NAME="lbleqnum">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
