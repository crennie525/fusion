﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssignmentReport2.aspx.vb"
    Inherits="lucy_r12.AssignmentReport2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Assignment Report</title>
    <link href="../styles/reports.css" type="text/css" rel="stylesheet" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/AssignmentReportaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function gettask(fid, coid, comp, tskid) {
            //var url = "../complib/complibtasks2dialog.aspx?typ=tasks2&fuid=" + fid + "&tasknum=" + tskid + "&coid=" + coid + "&comp=" + comp;
            var eReturn = window.showModalDialog("../complib/complibtasks2dialog.aspx?typ=tasks2&coid=" + coid + "&comp=" + comp + "&fuid=" + fid + " &tasknum=" + tskid + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:780px;resizable=yes");
            //var eReturn = window.showModalDialog(url, getTaskCallBack, "dialogHeight:600px; dialogWidth:780px;resizable=yes");
                //Not sure why this is here without a reference and untested; receives an error when referenced
                //getTaskCallBack();
                //Replacing with original for now
            if (eReturn) {
                document.getElementById("lblsubmit").value = "ret";
                document.getElementById("form1").submit();
            }
            else {
                document.getElementById("lblsubmit").value = "ret";
                document.getElementById("form1").submit();
            }
        }

        //function getTaskCallBack() {
            //LoadReport();
        //}

        function handleexit(task) {
            window.returnValue = task;
            window.close();
        }
        function sstchur_SmartScroller_Scroll() {
            var x = document.getElementById("xCoord").value;
            var y = document.getElementById("yCoord").value;
            //alert(x + ", " + y)
            window.scrollTo(x, y);

        }
        function sstchur_SmartScroller_GetCoords() {

            var scrollX, scrollY;

            if (document.all) {
                if (!document.documentElement.scrollLeft)
                    scrollX = document.body.scrollLeft;
                else
                    scrollX = document.documentElement.scrollLeft;

                if (!document.documentElement.scrollTop)
                    scrollY = document.body.scrollTop;
                else
                    scrollY = document.documentElement.scrollTop;
            }
            else {
                scrollX = window.pageXOffset;
                scrollY = window.pageYOffset;
            }

            document.getElementById("xCoord").value = scrollX;
            document.getElementById("yCoord").value = scrollY;

            //alert(document.getElementById("xCoord").value + ", " + document.getElementById("xCoord").value)
        }
        window.onscroll = sstchur_SmartScroller_GetCoords;
        window.onkeypress = sstchur_SmartScroller_GetCoords;
        window.onclick = sstchur_SmartScroller_GetCoords;
    </script>
    <style type="text/css">
    .plainlabelsmr
{
	font-family:MS Sans Serif, arial, sans-serif, Verdana;
	font-size:10px;
	text-decoration:none;
	color:red;
}
.plainlabelsmb
{
	font-family:MS Sans Serif, arial, sans-serif, Verdana;
	font-size:10px;
	text-decoration:none;
	color:blue;
}
    </style>
</head>
<body onload="sstchur_SmartScroller_Scroll();">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" width="1000" runat="server">
        <tr>
            <td id="tdwi" align="center" runat="server">
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="xCoord" runat="server" />
    <input type="hidden" id="yCoord" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblusetot" runat="server" />
    <input type="hidden" id="lblcomi" runat="server" />
    </form>
</body>

</html>
