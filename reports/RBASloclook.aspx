﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RBASloclook.aspx.vb" Inherits="lucy_r12.RBASloclook" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function DisableButton(b) {
            document.getElementById("btntocomp").className = "details";
            document.getElementById("btnfromcomp").className = "details";
            document.getElementById("todis").className = "view";
            document.getElementById("fromdis").className = "view";
            document.getElementById("form1").submit();
        }
        function handleexit() {
            var str = document.getElementById("lblrteid").value + "," + document.getElementById("lbldays").value + "," + document.getElementById("lblshift").value;
            //if (str != "") {
                window.parent.handleexit(str);
            //}
            //else {
                //alert("No Assets Selected!")
            //}
        }
     </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="422">
				<tr>
					<td class="thdrsingrt label" colSpan="2"><asp:Label id="unlang3326" runat="server">Select Level 1</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="2">
						<hr style="BORDER-RIGHT: #0000ff 1px solid; BORDER-TOP: #0000ff 1px solid; BORDER-LEFT: #0000ff 1px solid; BORDER-BOTTOM: #0000ff 1px solid">
					</td>
				</tr>
			</table>
			<table width="422">
				<tr>
					<td class="label" align="center"><asp:Label id="unlang3327" runat="server">Available Levels</asp:Label></td>
					<td></td>
					<td class="label" align="center"><asp:Label id="unlang3328" runat="server">Selected Levels</asp:Label></td>
				</tr>
				<tr>
					<td align="center" width="200"><asp:listbox id="lbfailmaster" runat="server" Width="170px" SelectionMode="Multiple" Height="150px"></asp:listbox></td>
					<td vAlign="middle" align="center" width="22">
						<img src="../images/appbuttons/minibuttons/forwardgraybg.gif" class="details" id="todis"
							width="20" height="20"> <img src="../images/appbuttons/minibuttons/backgraybg.gif" class="details" id="fromdis"
							width="20" height="20">
						<asp:imagebutton id="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton>
						<asp:imagebutton id="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton><IMG id="btnaddtosite" onmouseover="return overlib('Choose Failure Modes for this Plant Site ')"
							onclick="getss();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/plusminus.gif" width="20" height="20" class="details"></td>
					<td align="center" width="200"><asp:listbox id="lbfailmodes" runat="server" Width="170px" SelectionMode="Multiple" Height="150px"></asp:listbox></td>
				</tr>
				<tr class="details">
					<td align="right" colSpan="3"><IMG onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/submit.gif" id="ibtnret"
							runat="server" width="69" height="19"></td>
				</tr>
				<tr>
					<td class="bluelabel" colSpan="3"><asp:Label id="lang3329" runat="server">List Box Options</asp:Label></td>
				</tr>
				<tr>
					<td class="label" colSpan="3"><asp:radiobuttonlist id="cbopts" runat="server" Width="400px" AutoPostBack="True" CssClass="labellt">
							<asp:ListItem Value="0" Selected="True" id="lims" runat="server">Multi-Select (use arrows to move single or multiple selections)</asp:ListItem>
							<asp:ListItem Value="1" id="lico" runat="server">Click-Once (move selected item by clicking that item)</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td class="note" colSpan="3"><asp:Label id="lang3330" runat="server">* Please note that the Multi-Select Mode must be used to remove an item from the Available Equipment list if only one item is present.</asp:Label></td>
				</tr>
                <tr>
                <td class="redlabel" colspan="3" id="tdno2" runat="server">No Level 2 Locations Available</td>
                </tr>
			</table>
            </div>
            <div id="divlev2" runat="server">
            <table width="422">
				<tr>
					<td class="thdrsingrt label" colSpan="2"><asp:Label id="lbll2" runat="server">Select Level 2</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="2">
						<hr style="BORDER-RIGHT: #0000ff 1px solid; BORDER-TOP: #0000ff 1px solid; BORDER-LEFT: #0000ff 1px solid; BORDER-BOTTOM: #0000ff 1px solid">
					</td>
				</tr>
			</table>
			<table width="422">
				<tr>
					<td class="label" align="center"><asp:Label id="lblal" runat="server">Available Levels</asp:Label></td>
					<td></td>
					<td class="label" align="center"><asp:Label id="lblsl" runat="server">Selected Levels</asp:Label></td>
				</tr>
				<tr>
					<td align="center" width="200"><asp:listbox id="lbfailmaster2" runat="server" Width="170px" SelectionMode="Multiple" Height="150px"></asp:listbox></td>
					<td vAlign="middle" align="center" width="22">
						<img src="../images/appbuttons/minibuttons/forwardgraybg.gif" class="details" id="Img1"
							width="20" height="20"> <img src="../images/appbuttons/minibuttons/backgraybg.gif" class="details" id="Img2"
							width="20" height="20">
						<asp:imagebutton id="btntocomp2" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton>
						<asp:imagebutton id="btnfromcomp2" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton><IMG id="IMG3" onmouseover="return overlib('Choose Failure Modes for this Plant Site ')"
							onclick="getss();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/plusminus.gif" width="20" height="20" class="details"></td>
					<td align="center" width="200"><asp:listbox id="lbfailmodes2" runat="server" Width="170px" SelectionMode="Multiple" Height="150px"></asp:listbox></td>
				</tr>
				<tr class="details">
					<td align="right" colSpan="3"><IMG onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/submit.gif" id="IMG4"
							runat="server" width="69" height="19"></td>
				</tr>
				<tr>
					<td class="bluelabel" colSpan="3"><asp:Label id="Label4" runat="server">List Box Options</asp:Label></td>
				</tr>
				<tr>
					<td class="label" colSpan="3"><asp:radiobuttonlist id="cbopts2" runat="server" Width="400px" AutoPostBack="True" CssClass="labellt">
							<asp:ListItem Value="0" Selected="True" id="lims2" runat="server">Multi-Select (use arrows to move single or multiple selections)</asp:ListItem>
							<asp:ListItem Value="1" id="lico2" runat="server">Click-Once (move selected item by clicking that item)</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td class="note" colSpan="3"><asp:Label id="Label5" runat="server">* Please note that the Multi-Select Mode must be used to remove an item from the Available Equipment list if only one item is present.</asp:Label></td>
				</tr>
			</table>
            </div>
            <div>
            <table width="422">
            <tr>
            <td align="right"><img onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/submit.gif" id="IMG5"
							runat="server" width="69" height="19" /></td>
            </tr>
            </table>
            </div>
            <input type="hidden" id="lblwho" runat="server" />
            <input type="hidden" id="lblsid" runat="server" />
            <input type="hidden" id="lblskillid" runat="server" />
            <input type="hidden" id ="lblqty" runat="server" />
            <input type="hidden" id="lblfreq" runat="server" />
            <input type="hidden" id="lblrdid" runat="server" />
            <input type="hidden" id="lblrteid" runat="server" />
            <input type="hidden" id="lbllocstr" runat="server" />
            <input type="hidden" id="lblopt" runat="server" />
            <input type="hidden" id="lbllocstr2" runat="server" />
            <input type="hidden" id="lblrttype" runat="server" />
            <input type="hidden" id="lblptid" runat="server" />
            <input type="hidden" id="lbldays" runat="server" />
            <input type="hidden" id="lblshift" runat="server" />
            <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
