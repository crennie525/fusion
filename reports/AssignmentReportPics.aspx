<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AssignmentReportPics.aspx.vb" Inherits="lucy_r12.AssignmentReportPics" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PM Assignment Report</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<LINK rel="stylesheet" type="text/css" href="../styles/reports.css">
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" src="../scripts1/AssignmentReportPicsaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<TABLE id="tblexport" width="1000" runat="server">
				<tr>
					<td id="tdwi" align="center" runat="server"></td>
				</tr>
			</TABLE>
		
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblcomi" runat="server" />
</form>
	</body>
</HTML>
