﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="firebag.aspx.vb" Inherits="lucy_r12.firebag" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Firebag Equipment Strategy Data Sheet</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <link rel="stylesheet" type="text/css" href="../styles/reports.css" />
    <style type="text/css">
        .bg1
        {
            background-color: #ECF0F3
        }
        .bg2
        {
            background-color: #CED4D9
        }
        .replabeltop
        {
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 15px;
            color: Black;
            font-weight: bold;
        }
        .replabelplain
        {
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 13px;
            color: Black;
        }
        .dbox
        {
            border-bottom: solid 1px black;
            border-left: solid 1px black;
            border-right: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 13px;
            font-weight: bold;
            color: Black;
            text-align: right;
        }
        .tboxl1
        {
            border-left: solid 1px black;
            border-top: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 13px;
            font-weight: bold;
            color: Black;
            text-align: left;
        }
        .tboxl
        {
            border-left: solid 1px black;
            border-top: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 13px;
            font-weight: bold;
            color: Black;
            text-align: center;
        }
        .tboxr
        {
            border-left: solid 1px black;
            border-top: solid 1px black;
            border-bottom: solid 1px black;
            border-right: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 13px;
            font-weight: bold;
            color: Black;
            text-align: center;
        }
        .tboxr1
        {
            border-left: solid 1px black;
            border-top: solid 1px black;
            border-bottom: solid 1px black;
            border-right: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 13px;
            font-weight: bold;
            color: Black;
            text-align: left;
        }
        .rboxl1
        {
            border-left: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            color: Black;
            text-align: left;
        }
        .rboxr1
        {
            border-left: solid 1px black;
            border-right: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            color: Black;
            text-align: left;
        }
        .rboxl
        {
            border-left: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            color: Black;
            text-align: center;
        }
        .rboxr
        {
            border-left: solid 1px black;
            border-right: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            color: Black;
            text-align: center;
        }
        .rboxlh
        {
            border-left: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            font-weight: bold;
            color: Black;
            text-align: center;
        }
        .rboxrh
        {
            border-left: solid 1px black;
            border-right: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            font-weight: bold;
            color: Black;
            text-align: center;
        }
        .rboxrh1
        {
            border-left: solid 1px black;
            border-right: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            font-weight: bold;
            color: Black;
            text-align: left;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divrep" runat="server">
    </div>
    </form>
</body>
</html>
