

'********************************************************
'*
'********************************************************



Public Class EqSelectDialog
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim mutils As New mmenu_utils_a
    Dim Login, sid, coi, who, rid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        coi = mutils.COMPI
        lblcoi.Value = coi
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            who = Request.QueryString("who").ToString
            lblwho.Value = who
            Try
                rid = Request.QueryString("rid").ToString
                lblrid.Value = rid
            Catch ex As Exception

            End Try
            
        End If
    End Sub

End Class
