﻿Imports System.Data.SqlClient
Imports System.IO
Public Class pmrhtml7tpm_new
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim eqstr, skillid, qty, freq, rdid, mode, rid, rsid, typ, meas As String
    Dim tmod As New transmod
    Dim ridarr As ArrayList = New ArrayList
    Dim sb As New System.Text.StringBuilder
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            lblfslang.Value = "fre"
        End Try
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        lblcomi.Value = comi
        If comi = "SUN" Then
            lblfslang.Value = "fre"
        End If
        If Not IsPostBack Then
            rid = Request.QueryString("rid").ToString '3302 '"5588" '"2132" "6342" '
            pms.Open()
            'getrsid(rid)
            PopWI(rid)
            pms.Dispose()
            divwi.InnerHtml = sb.ToString
        End If
    End Sub
    Private Sub getrsid(ByVal rid As String)
        sql = "select rsid from pmroute_stops where rid = '" & rid & "' order by stopsequence"
        dr = pms.GetRdrData(sql)
        While dr.Read
            ridarr.Add(dr.Item("rsid"))
        End While
        dr.Close()
        Dim i As Integer
        For i = 0 To ridarr.Count - 1
            rsid = ridarr(i)
            sql = "select eqid, skillid, skillqty, freq, rdid from pmroute_stops where rsid = '" & rsid & "'"
            Dim ds As New DataSet
            ds = pms.GetDSData(sql)
            Dim x As Integer = ds.Tables(0).Rows.Count
            Dim ii As Integer
            For ii = 0 To (x - 1)
                eqstr = ds.Tables(0).Rows(ii)("eqid").ToString
                skillid = ds.Tables(0).Rows(ii)("skillid").ToString
                qty = ds.Tables(0).Rows(ii)("skillqty").ToString
                freq = ds.Tables(0).Rows(ii)("freq").ToString
                rdid = ds.Tables(0).Rows(ii)("rdid").ToString
                'PopWI(eqstr, skillid, qty, freq, rdid)
            Next
        Next
        divwi.InnerHtml = sb.ToString
    End Sub
    Private Sub PopWI(ByVal rteid As String)
        Dim lang As String = lblfslang.Value
        Dim coi As String = lblcomi.Value
        sql = "select rttype from pmroutes where rid = '" & rteid & "'"
        typ = pms.strScalar(sql)
        Dim fid As String = ""
        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim funcchk As String = ""
        Dim funccompchk As String = ""
        Dim func As String = ""
        Dim comid As String = ""
        Dim comp As String = ""
        Dim compchk As String = ""
        Dim compdesc As String = ""
        Dim subtask As String = ""
        Dim eqnum As String = ""
        Dim eqnumchk As String = ""
        Dim eqdesc As String = ""
        Dim hdchk As Integer = 0
        Dim hdchk1 As Integer = 0
        Dim eqid As String
        Dim pic As String
        Dim epic As String
        Dim fpic As String
        Dim loc As String = ""
        Dim fmsav As String = ""
        Dim meassave As String = ""
        Dim partssave As String = ""
        Dim toolssave As String = ""
        Dim lubesave As String = ""
        Dim ealert As Integer = 0
        Dim falert As Integer = 0
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr
        'eqid = eqstr
        'eqstr = eqstr.Replace("(", "")
        'eqstr = eqstr.Replace(")", "")
        'Dim eqarr() As String = eqstr.Split(",")
        'Dim i As Integer
        'Dim tst As String = eqarr.Length - 1
        Dim tskcnt, tsktrk, pg, siteid As Integer
        tsktrk = 0
        pg = 1
        'subcnt = 0
        sql = "select count(*) from pmtaskstpm where rteid = '" & rid & "' and subtask = 0"
        tskcnt = pms.Scalar(sql)
        sql = "select siteid from pmroutes where rid = '" & rteid & "'"
        siteid = pms.strScalar(sql)
        Dim chk As Integer

        'For i = 0 To eqarr.Length - 1
        hdchk = 0
        'eqid = eqarr(i).ToString
        ' = "(" & eqid & ")"
        'sql = "select count(*) from pmtasks where eqid = '" & eqid & "' and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "'"
        'chk = pms.Scalar(sql)
        'If chk > 0 Then
        If typ = "RBAST" Then
            If lang = "fre" Then
                sql = "usp_pmr_rep5wpftpm '" & rteid & "'"
            Else
                sql = "usp_pmr_rep5wptpm '" & rteid & "'"
            End If

            'ElseIf typ = "RBAST" Then
            'sql = "usp_pmr_rep3T '" & rteid & "'"
        End If

        Dim parts, tools, lubes As String
        parts = ""
        tools = ""
        lubes = ""
        Dim sk, fr, rd, tt As String
        dr = pms.GetRdrData(sql)
        Dim pretech, pretechchk As String
        'style=""page-break-after:always;""

        Dim fflg As Integer = 0

        While dr.Read
            eqid = dr.Item("eqid").ToString
            sk = dr.Item("skill").ToString
            fr = dr.Item("freq").ToString
            rd = dr.Item("rd").ToString
            If lang = "fre" Then
                If rd = "Running" Then
                    rd = "Marche"
                ElseIf rd = "Down" Then
                    rd = "Arrêt"
                End If
            End If
            tt = dr.Item("est").ToString
            skill = dr.Item("skill").ToString & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString
            pretech = dr.Item("pretech").ToString
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            func = dr.Item("func").ToString
            comid = dr.Item("comid").ToString
            parts = dr.Item("parts").ToString
            tools = dr.Item("tools").ToString
            lubes = dr.Item("lubes").ToString
            meas = dr.Item("meas").ToString
            If fmsav = "" Then
                fmsav = dr.Item("fm1").ToString
            End If
            If meassave = "" Then
                meassave = dr.Item("meas").ToString
            End If
            If partssave = "" Then
                partssave = dr.Item("parts").ToString
            End If
            If toolssave = "" Then
                toolssave = dr.Item("tools").ToString
            End If
            If lubesave = "" Then
                lubesave = dr.Item("lubes").ToString
            End If

            If hdchk = 0 Then
                hdchk = 1
                eqnumchk = eqnum
                funcchk = func
                sb.Append("<Table cellSpacing=""1"" cellPadding=""2"" width=""780"" border=""0"">")
                sb.Append("<tr><td width=""40""></td><td width=""40""></td><td width=""600""></td></tr>")
                If coi = "CAS" Then
                    sb.Append("<tr><td colspan=""2""><img src=""../menu/mimages/cascades_logo.png""></td>")
                ElseIf coi = "JDI" Then
                    If siteid = "39" Then
                        sb.Append("<tr><td colspan=""2""><img src=""../menu/mimages/CavLogoJPG.jpg""></td>")
                    Else
                        sb.Append("<tr><td colspan=""2""><img src=""../menu/mimages/nav_logo.bmp""></td>")
                    End If
                Else
                    sb.Append("<tr><td colspan=""2""><img src=""../menu/mimages/custcomp.gif""></td>")
                End If
                sb.Append("<td width=""600"" class=""tdborder1"">")

                sb.Append("<table width=""600"">")
                sb.Append("<tr><td class=""plainlabel"" width=""100"">Route:</td><td class=""plainlabel"" width=""150"">" & dr.Item("route").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" width=""100"">Description:</td><td width=""350"" class=""plainlabel"">" & dr.Item("description").ToString & "</td></tr>")
                sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
                sb.Append("<tr><td colspan=""4"">")
                sb.Append("<table width=""600"">")
                sb.Append("<tr><td class=""plainlabel"" width=""50"" id=""tdskill"" runat=""server"">" & checklang("lab1") & "</td><td class=""plainlabel"" width=""130"">" & sk & "</td>")
                sb.Append("<td class=""plainlabel"" width=""60"">" & checklang("lab2") & "</td><td class=""plainlabel"" width=""80"">" & fr & "</td>")
                sb.Append("<td class=""plainlabel"" width=""50"">" & checklang("lab3") & "</td><td class=""plainlabel"" width=""60"">" & rd & "</td>")
                sb.Append("<td class=""plainlabel"" width=""80"">" & checklang("lab13") & "</td><td class=""plainlabel"" width=""90"">" & tt & "</td></tr>")
                If pretech <> "" Then
                    sb.Append("<tr><td class=""plainlabel"">PdM:</td><td class=""plainlabel"" colspan=""6"">" & pretech & "</td></tr>")
                End If
                sb.Append("</table></td></tr>")
                sb.Append("</table></td></tr>")
                'sb.Append("</table></td></tr>")
                If lang = "fre" Then
                    sb.Append("<tr><td colspan=""4"" class=""plainlabel"" align=""right""><br />Complété par&nbsp;____________________&nbsp;&nbsp;Date&nbsp;__________________<br /></td></tr>")
                Else
                    sb.Append("<tr><td colspan=""4"" class=""plainlabel"" align=""right""><br />Completed By&nbsp;____________________&nbsp;&nbsp;Date&nbsp;__________________<br /></td></tr>")
                End If
            Else
                If eqnumchk <> eqnum Then
                    eqnumchk = eqnum
                    funcchk = ""
                    skillchk = skill
                    start = 0
                    flag = 0
                    ealert = 0
                    falert = 0
                Else
                    ealert = 1
                End If
                If func <> funcchk Then
                    funcchk = func
                    falert = 0
                Else
                    falert = 1
                End If
            End If
            'checklang(
            Dim skchk, skchk2 As String
            skchk = skill.ToLower
            skchk2 = skillchk.ToLower


            'insert component stuff here
            comp = dr.Item("compnum").ToString
            compdesc = dr.Item("compdesc").ToString

            If comid <> compchk Then
                compchk = comid
                sb.Append("<tr><td colspan=""3"">")
                sb.Append("<table width=""780"" border=""0"">")
                'sb.Append("<tr><td colspan=""4""><hr></td></tr>")
                'sb.Append("<tr><td colspan=""3"" class=""label""><u>" & checklang("lab6") & "</u></td></tr>") '
                If ealert = 0 Then
                    sb.Append("<tr><td class=""plainlabel"" width=""200""><b>" & eqnum & "</b></td><td width=""330"" class=""plainlabel"">" & eqdesc & "</td>")
                Else
                    sb.Append("<tr><td class=""plainlabel"" width=""200"">&nbsp;</td><td width=""330"">&nbsp;</td>")
                End If
                If falert = 0 Then
                    sb.Append("<td class=""label"" width=""250"">" & dr.Item("func").ToString & "</td></tr>")
                Else
                    sb.Append("<td class=""label"" width=""250"">&nbsp;</td></tr>")
                End If

                'sb.Append("<td class=""plainlabel"" width=""250""></td></tr>") '<b>" & comp & "</b><br />" & compdesc & "
                'sb.Append("<tr><td class=""plainlabel"">Description:</td><td class=""label"">" & dr.Item("compdesc").ToString & "</td></tr>")
                sb.Append("</table></td></tr>")

                'sb.Append("<tr><td colspan=""3"" class=""label""><u>" & checklang("lab6") & "</u></td></tr>")
                'sb.Append("<tr><td colspan=""2"" class=""label"">" & checklang("lab7") & "</td><td class=""label"">" & comp & "</td></tr>")
                'sb.Append("<tr><td colspan=""2"" class=""label"">Description</td><td class=""label"">" & compdesc & "</td></tr>")

                'sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>")
            End If

            pic = dr.Item("pic").ToString
            If File.Exists(pic) Then
                pic = pic
            Else
                'pic = ""
            End If

            subtask = dr.Item("subtask").ToString

            Dim measbr, partsbr, toolsbr, lubesbr As String

            If subtask = "0" Then
                sb.Append("<tr><td colspan=""3"" align=""center"">")
                sb.Append("<table width=""780"" border=""0"" valign=""top"">")
                sb.Append("<tr><td class=""tdborder1"" valign=""top""><table width=""500"" border=""0"" cellspacing=""0"" cellpadding=""2"">")
                sb.Append("<tr><td class=""plainlabel"" colspan=""2"" valign=""top"" align=""left""><b>" & comp & "</b>&nbsp;&nbsp;" & compdesc & "<br />&nbsp;</td></tr>")

                sb.Append("<tr><td class=""plainlabel grayright2"" width=""290"" valign=""top"" align=""left""><b>" & checklang("lab8") & "</b></td>") '

                sb.Append("<td class=""plainlabel"" width=""210"" valign=""top"" align=""left""><b>" & checklang("lab10") & "</b></td></tr>") '


                'sb.Append("<td class=""plainlabel"" width=""60""><b>" & checklang("lab9") & "</b></td>") '


                sb.Append("<tr><td class=""plainlabel grayright2"" valign=""top"" align=""left"">" & dr.Item("task").ToString & " ")

                'parts/tools/lubes were here

                'If meas <> "" And meas <> "no measurements" Then
                'measbr = meas.Replace(";", "<br />")
                'sb.Append("<br>" & measbr & " ")
                'End If

                'sb.Append("</td>")

                'sb.Append("<td class=""plainlabel"" valign=""top"">" & dr.Item("ttime").ToString & "</td>")


            Else
                If subtask = "1" Then
                    'subcnt = 1
                    'sb.Append("<br><b>Sub Tasks</b>")

                    sb.Append("<br>[" & subtask & "]&nbsp;" & dr.Item("subt").ToString & "")

                    'sb.Append("<tr><td class=""bigbold1 grayright2"" colspan=""3"">Sub Tasks</td></tr>")

                    'sb.Append("<tr><td class=""plainlabel grayright2"" colspan=""3"">[" & subtask & "]&nbsp;" & dr.Item("subt").ToString & "</td></tr>")
                Else

                    sb.Append("<br>[" & subtask & "]&nbsp;" & dr.Item("subt").ToString & "")

                    'sb.Append("<tr><td class=""plainlabel grayright2"" colspan=""3"">[" & subtask & "]&nbsp;" & dr.Item("subt").ToString & "</td></tr>")
                End If

            End If

            'sb.Append("</td>")

            'sb.Append("<td class=""plainlabel"" valign=""top"">" & dr.Item("fm1").ToString & "</td></tr>")
            'X compare subcnt to subtask as int before inserting the below
            Dim subcnt As String = dr.Item("subcnt").ToString
            Dim subc As Integer = Convert.ToInt64(subcnt)
            Dim subtsk As Integer = Convert.ToInt64(subtask)

            If subc = subtsk Then
                sb.Append("<br><b>" & checklang("lab11") & "</b><br><br><br>")
                sb.Append("</td>")

                Dim fmbr As String = ""

                If dr.Item("fm1").ToString <> "" Then
                    fmbr = dr.Item("fm1").ToString
                    Dim fmbrtst As String = fmbr
                    fmbr = fmbr.Replace(")", ")<br />")
                    If lang = "fre" Then
                        fmbr = fmbr + "OK (___)<br /><br /><b>Mesures/Outils/Huiles:</b>"
                    Else
                        fmbr = fmbr + "OK (___)<br /><br /><b>Measurements/Materials:</b>"
                    End If
                Else
                    fmbr = fmsav
                    fmbr = fmbr.Replace(")", ")<br />")
                    If lang = "fre" Then
                        fmbr = fmbr + "OK (___)<br /><br /><b>Mesures/Outils/Huiles:</b>"
                    Else
                        fmbr = fmbr + "OK (___)<br /><br /><b>Measurements/Materials:</b>"
                    End If
                End If
                fmsav = ""
                Dim test As String = eqid & " - " & fmbr
                If fmbr = "" Then
                    'fmbr = "OK (   )<br />"
                    If lang = "fre" Then
                        fmbr = "OK (___)<br /><br /><b>Mesures/Outils/Huiles:</b>"
                    Else
                        fmbr = "OK (___)<br /><br /><b>Measurements/Materials:</b>"
                    End If
                End If

                sb.Append("<td class=""plainlabel"" valign=""top"" align=""left"">" & fmbr & "")
                fmbr = ""
                If meassave = "" Then
                    If meas <> "" And meas <> "no measurements" Then
                        measbr = meas.Replace(";", "<br />")
                        sb.Append("<br>" & measbr & " ")
                        'Else
                        'sb.Append("</td></tr>")
                    End If
                Else
                    If meassave <> "" And meassave <> "no measurements" Then
                        measbr = meassave.Replace(";", "<br />")
                        sb.Append("<br>" & measbr & " ")
                        'Else
                        'sb.Append("</td></tr>")
                    End If
                End If
                meassave = ""

                If partssave = "" Then
                    If parts <> "" And parts <> "no parts" Then
                        partsbr = parts.Replace(";", "<br />")
                        sb.Append("<br>" & partsbr & " ")
                    End If
                Else
                    If partssave <> "" And partssave <> "no parts" Then
                        partsbr = partssave.Replace(";", "<br />")
                        sb.Append("<br>" & partsbr & " ")
                    End If
                End If
                partssave = ""

                If toolssave = "" Then
                    If tools <> "" And tools <> "no tools" Then
                        toolsbr = tools.Replace(";", "<br />")
                        sb.Append("<br><br>" & toolsbr & " ")
                    End If
                Else
                    If toolssave <> "" And toolssave <> "no tools" Then
                        toolsbr = toolssave.Replace(";", "<br />")
                        sb.Append("<br><br>" & toolsbr & " ")
                    End If
                End If
                toolssave = ""

                If lubesave <> "" Then
                    If lubes <> "" And lubes <> "no lubes" Then
                        lubesbr = lubes.Replace(";", "<br />")
                        sb.Append("<br><br>" & lubesbr & " ")
                    End If
                Else
                    If lubesave <> "" And lubesave <> "no lubes" Then
                        lubesbr = lubesave.Replace(";", "<br />")
                        sb.Append("<br><br>" & lubesbr & " ")
                    End If
                End If

                'sb.Append("</td></tr>")

                'sb.Append("<tr><td colspan=""3"" class=""label"">" & checklang("lab11") & "</td></tr>")
                'sb.Append("<tr><td colspan=""3"" rowspan=""3"">&nbsp;</td></tr>")

                'sb.Append("<tr><td colspan=""3"" class=""label"">" & checklang("lab11") & "</td></tr>")
                'sb.Append("<tr><td colspan=""3"" rowspan=""3"">&nbsp;</td></tr>")


                sb.Append("</td></tr>")

                sb.Append("</table></td>")

                If pic = "" Then
                    sb.Append("<td align=""center"" rowspan=""8"">") ' class=""tdborder1""
                    sb.Append("<table width=""280"" class=""tdborder1"">")
                    If lang = "fre" Then
                        sb.Append("<tr><td align=""center"" valign=""center"" class=""plainlabel"">Aucune image</td></tr>") '" & checklang("lab12") & "
                    Else
                        sb.Append("<tr><td align=""center"" valign=""center"" class=""plainlabel"">no image</td></tr>") '" & checklang("lab12") & "
                    End If

                    sb.Append("</table></td></tr>")
                Else
                    sb.Append("<td align=""center"" rowspan=""5"">")
                    sb.Append("<table width=""280"" class=""tdborder1"">")
                    sb.Append("<tr><td align=""center"" valign=""center"" colspan=""2""><img src=""" & pic & """></td></tr>")
                    sb.Append("</table></td></tr>")
                End If

                sb.Append("</table></td></tr>")

                sb.Append("</table></td></tr>")
                'ealert = 0
                'falert = 0

                tsktrk += 1
                tskcnt -= 1
                If subcnt > 5 Then
                    tsktrk += 1
                End If

                If tsktrk = 4 And tskcnt > 0 Then
                    tsktrk = 0
                    pg += 1
                    sb.Append("<table width=""780""><tr><td class=""label"" align=""right""><h4>Page " & pg & "</td></tr></table>")
                End If
            End If

            'need to work on when to add pic




            'sb.Append("<tr><td colspan=""3""><hr></td></tr>")
        End While
        dr.Close()



        'checklang()

        sb.Append("</Table>")
        'End If
        'Next



    End Sub
    Private Function checklang(ByVal lab As String)
        Dim lang As String = lblfslang.Value
        Dim ret As String
        Dim lab1 As String
        'Acune image de tâche
        If lang = "fre" Then
            If lab = "lab1" Then
                lab1 = "Métier:"
            ElseIf lab = "lab2" Then
                lab1 = "Fréquence:"
            ElseIf lab = "lab3" Then
                lab1 = "Statut:"
            ElseIf lab = "lab4" Then
                lab1 = "Fonction:"

            ElseIf lab = "lab5" Then
                lab1 = "Équipement:"
            ElseIf lab = "lab6" Then
                lab1 = "Détail de la composante:"
            ElseIf lab = "lab7" Then
                lab1 = "Nom:"
            ElseIf lab = "lab8" Then
                lab1 = "Tâche:"
            ElseIf lab = "lab9" Then
                lab1 = "Temps estimé:"
            ElseIf lab = "lab10" Then
                lab1 = "Modes de défaillances:"
            ElseIf lab = "lab11" Then
                lab1 = "Commentaire:"
            ElseIf lab = "lab12" Then
                lab1 = "Acune image"
            ElseIf lab = "lab13" Then
                lab1 = "Temps total"
            End If
        Else
            If lab = "lab1" Then
                lab1 = "Skill:"
            ElseIf lab = "lab2" Then
                lab1 = "Frequency:"
            ElseIf lab = "lab3" Then
                lab1 = "Status:"
            ElseIf lab = "lab4" Then
                lab1 = "Function:"
            ElseIf lab = "lab5" Then
                lab1 = "Equipment:"
            ElseIf lab = "lab6" Then
                lab1 = "Component Details:"
            ElseIf lab = "lab7" Then
                lab1 = "Name:"
            ElseIf lab = "lab8" Then
                lab1 = "Task:"
            ElseIf lab = "lab9" Then
                lab1 = "Est Duration:"
            ElseIf lab = "lab10" Then
                lab1 = "Failure Modes:"
            ElseIf lab = "lab11" Then
                lab1 = "Comments:"
            ElseIf lab = "lab12" Then
                lab1 = "No Image Available"
            ElseIf lab = "lab13" Then
                lab1 = "Total Time"
            End If
        End If
        ret = lab1
        Return ret
    End Function
End Class