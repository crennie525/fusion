﻿Imports System.Data.SqlClient
Imports System.IO
Public Class pmrhtml3
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim eqstr, skillid, qty, freq, rdid, mode, rid, rsid As String
    Dim tmod As New transmod
    Dim ridarr As ArrayList = New ArrayList
    Dim sb As New System.Text.StringBuilder
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        If comi = "SUN" Then
            lblfslang.Value = "fre"
        End If
        If Not IsPostBack Then
            rid = Request.QueryString("rid").ToString '"1156" '
            pms.Open()
            getrsid(rid)
            pms.Dispose()
        End If
    End Sub
    Private Sub getrsid(ByVal rid As String)
        sql = "select rsid from pmroute_stops where rid = '" & rid & "' order by stopsequence"
        dr = pms.GetRdrData(sql)
        While dr.Read
            ridarr.Add(dr.Item("rsid"))
        End While
        dr.Close()
        Dim i As Integer
        For i = 0 To ridarr.Count - 1
            rsid = ridarr(i)
            sql = "select eqid, skillid, skillqty, freq, rdid from pmroute_stops where rsid = '" & rsid & "'"
            Dim ds As New DataSet
            ds = pms.GetDSData(sql)
            Dim x As Integer = ds.Tables(0).Rows.Count
            Dim ii As Integer
            For ii = 0 To (x - 1)
                eqstr = ds.Tables(0).Rows(ii)("eqid").ToString
                skillid = ds.Tables(0).Rows(ii)("skillid").ToString
                qty = ds.Tables(0).Rows(ii)("skillqty").ToString
                freq = ds.Tables(0).Rows(ii)("freq").ToString
                rdid = ds.Tables(0).Rows(ii)("rdid").ToString
                PopWI(eqstr, skillid, qty, freq, rdid)
            Next
        Next
        divwi.InnerHtml = sb.ToString
    End Sub
    Private Sub PopWI(ByVal eqstr As String, ByVal skillid As String, ByVal qty As String, ByVal freq As String, ByVal rdid As String)
        Dim fid As String = ""
        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim funcchk As String = ""
        Dim func As String = ""
        Dim subtask As String = ""
        Dim eqnum As String = ""
        Dim eqnumchk As Integer = 0
        Dim hdchk As Integer = 0
        Dim eqid As String
        Dim pic As String
        eqid = eqstr
        eqstr = eqstr.Replace("(", "")
        eqstr = eqstr.Replace(")", "")
        Dim eqarr() As String = eqstr.Split(",")
        Dim i As Integer
        Dim tst As String = eqarr.Length - 1

        Dim chk As Integer

        For i = 0 To eqarr.Length - 1
            hdchk = 0
            eqid = eqarr(i).ToString
            eqstr = "(" & eqid & ")"
            sql = "select count(*) from pmtasks where eqid = '" & eqid & "' and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "'"
            chk = pms.Scalar(sql)
            If chk > 0 Then
                sql = "usp_pmr_rep2 '" & eqstr & "','" & skillid & "','" & qty & "','" & freq & "','" & rdid & "'"
                Dim parts, tools, lubes As String
                parts = ""
                tools = ""
                lubes = ""
                dr = pms.GetRdrData(sql)
                Dim pretech, pretechchk As String
                While dr.Read
                    skill = dr.Item("skill").ToString & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString
                    pretech = dr.Item("ptid").ToString
                    Dim skchk, skchk2 As String
                    skchk = skill.ToLower
                    skchk2 = skillchk.ToLower
                    If skchk <> skchk2 Or eqnumchk <> eqid Then
                        eqnumchk = eqid
                        hdchk = 1
                        skillchk = skill
                        start = 0
                        flag = 0
                        sb.Append("<Table cellSpacing=""1"" cellPadding=""2"" width=""620"" border=""0"" style=""page-break-after:always;"">")
                        sb.Append("<tr><td width=""40""></td><td width=""40""></td><td width=""540""></td></tr>")
                        sb.Append("<tr><td colspan=""2""><img src=""../menu/mimages/cascades_logo.png""></td>")
                        sb.Append("<td width=""540"" class=""tdborder1"">")
                        sb.Append("<table width=""540"">")
                        sb.Append("<tr><td class=""plainlabel"" width=""130"">Asset Number:</td><td class=""plainlabel"">" & dr.Item("eqnum").ToString & "</td></tr>")
                        sb.Append("<tr><td class=""plainlabel"">Asset Description:</td><td class=""plainlabel"">" & dr.Item("eqdesc").ToString & "</td></tr>")
                        sb.Append("<tr><td colspan=""2""><hr></td></tr>")
                        sb.Append("<tr><td class=""plainlabel"">Tasks Executed By:</td><td class=""plainlabel"">" & dr.Item("skill").ToString & "</td></tr>")
                        sb.Append("<tr><td class=""plainlabel"">Tasks Executed Every:</td><td class=""plainlabel"">" & dr.Item("freq").ToString & "&nbsp;&nbsp;Days</td></tr>")
                        sb.Append("<tr><td class=""plainlabel"">Asset Must Be:</td><td class=""plainlabel"">" & dr.Item("rd").ToString & "</td></tr>")
                        sb.Append("</table></td></tr>")
                        

                    End If
                    func = dr.Item("func").ToString
                    If flag = 0 Then
                        flag = 1
                        funcchk = func
                        pic = dr.Item("pic").ToString

                        If pic = "" Then
                            pic = "../images/appimages/funcimg.gif"
                        Else
                            If File.Exists(pic) Then
                                pic = pic
                            Else
                                pic = "../images/appimages/funcimg.gif"
                            End If
                        End If
                        sb.Append("<tr><td colspan=""3"" class=""tdborder1"">")
                        sb.Append("<table >")
                        sb.Append("<tr><td class=""plainlabel"" width=""80"" valign=""top""><br />Function:</td><td class=""plainlabel"" width=""280"" valign=""top""><br />" & dr.Item("func").ToString & "</td>")
                        sb.Append("<td width=""300"" align=""center"" valign=""center""><img src=""" & pic & """></td></tr>")

                        sb.Append("</table></td></tr>")

                        sb.Append("<tr><td colspan=""3"" align=""center"">")
                        sb.Append("<table>")

                        sb.Append("<tr><td class=""plainlabel"" width=""380""><b>Task Description</b></td>")
                        sb.Append("<td class=""plainlabel"" width=""80""><b>Est Duration</b></td>")
                        sb.Append("<td class=""plainlabel"" width=""200""><b>Failure Modes</b></td></tr>")
                    Else
                        If func <> funcchk Then
                            funcchk = func
                            pic = dr.Item("pic").ToString
                            If pic = "" Then
                                pic = "../images/appimages/funcimg.gif"
                            Else
                                If File.Exists(pic) Then
                                    pic = pic
                                Else
                                    pic = "../images/appimages/funcimg.gif"
                                End If
                            End If
                            sb.Append("</table></td></tr>")

                            sb.Append("<tr><td colspan=""3"" class=""tdborder1"">")
                            sb.Append("<table >")
                            sb.Append("<tr><td class=""plainlabel"" width=""80"" valign=""top""><br />Function:</td><td class=""plainlabel"" width=""280"" valign=""top""><br />" & dr.Item("func").ToString & "</td>")
                            sb.Append("<td width=""300"" align=""center"" valign=""center""><img src=""" & pic & """></td></tr>")

                            sb.Append("</table></td></tr>")

                            sb.Append("<tr><td colspan=""3"" align=""center"">")
                            sb.Append("<table>")

                            sb.Append("<tr><td class=""plainlabel"" width=""380""><b>Task Description</b></td>")
                            sb.Append("<td class=""plainlabel"" width=""80""><b>Est Duration</b></td>")
                            sb.Append("<td class=""plainlabel"" width=""200""><b>Failure Modes</b></td></tr>")
                        End If
                    End If

                    sb.Append("<tr><td class=""plainlabel tdborder1"">" & dr.Item("task").ToString & "</td>")
                    sb.Append("<td class=""plainlabel tdborder1"">" & dr.Item("ttime").ToString & "</td>")
                    sb.Append("<td class=""plainlabel tdborder1"">" & dr.Item("fm1").ToString & "</td></tr>")


                End While
                dr.Close()

                sb.Append("</table></td></tr>")

                sb.Append("</Table>")
            End If
        Next


    End Sub
End Class