Imports System.Data.SqlClient
Public Class compnotasks
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim tmod As New transmod
    Dim eqid, fuid, cid, tl, sid, did, clid, chk As String
    Protected WithEvents tdpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwi As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdass As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcwt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'ADD FOR SUNCOR
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        If comi = "SUN" Then
            lblfslang.Value = "fre"
        End If
        'END ADD FOR SUNCOR
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            fuid = Request.QueryString("fuid").ToString
            lblfuid.Value = fuid
            cid = Request.QueryString("cid").ToString 'HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            tl = Request.QueryString("tl").ToString
            lbltasklev.Value = tl
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            did = Request.QueryString("did").ToString
            lbldid.Value = did
            clid = Request.QueryString("clid").ToString
            lblclid.Value = clid
            chk = Request.QueryString("chk").ToString
            lblchk.Value = chk
            tdass.InnerHtml = tmod.getxlbl("xlb256", "AssignmentReport.aspx.vb")
            tdcwt.InnerHtml = tmod.getxlbl("lang3308", "reports2.aspx1")
            tdpm.InnerHtml = tmod.getxlbl("lang3375", "reports2.aspx")
            pms.Open()
            PopMD(eqid, fuid)
            pms.Dispose()
            'Else
            'If Request.Form("lbltasknum") <> "" Then
            'GetTask()
            'End If
        End If
    End Sub
    Function GetTask()
        Dim tasknum As String = lbltasknum.Value
        tl = lbltasklev.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        chk = lblchk.Value
        fuid = lblfuid.Value
        Response.Redirect("PMTaskDivFunc.aspx?start=yes&tl=5&chk=" & chk & "&cid=" & _
        cid + "&fuid=" & fuid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & _
        "&eqid=" & eqid & "&task=" & tasknum)
    End Function
    Private Sub PopMD(ByVal eqid As String, ByVal fuid As String)
        Dim fid As String = ""
        Dim eqnum, eqdesc, rtf, rpn, det As String
        Dim task, comp, freq, craft, rd, taskdesc, fm, coid As String
        Dim aid As String
        Dim aidi As Integer = 1
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""20""></td><td width=""600""></td></tr>")
        sql = "select distinct e.eqnum, e.eqdesc, f.func_id, f.func, c.comid, c.compnum, f.routing, c.crouting, c.rtf, c.rpn, c.det from components c " _
        + "left join functions f on f.func_id = c.func_id " _
        + "left join equipment e on e.eqid = f.eqid " _
        + "where c.comid not in (select p.comid from pmtasks p where p.eqid = '" & eqid & "' and p.funcid = f.func_id) " _
        + "and e.eqid = '" & eqid & "' " _
        + "order by e.eqnum, f.routing, c.crouting" 'f.func, c.compnum"
        dr = pms.GetRdrData(sql)
        While dr.Read
            rtf = dr.Item("rtf").ToString
            rpn = dr.Item("rpn").ToString
            det = dr.Item("det").ToString
            If det = "1" Then
                det = "YES"
            Else
                det = "NO"
            End If
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            If fid <> dr.Item("func_id").ToString Then
                If fid = "" Then
                    sb.Append("<tr><td class=""plainlabel"" colspan=""2""><b>" & tmod.getxlbl("xlb460", "ttno.aspx.vb") & "</b>&nbsp;&nbsp;&nbsp;" & dr.Item("func").ToString & "</td></tr>")
                Else
                    sb.Append("<tr><td class=""plainlabel"" colspan=""2""><br><b>" & tmod.getxlbl("xlb460", "ttno.aspx.vb") & "</b>&nbsp;&nbsp;&nbsp;" & dr.Item("func").ToString & "</td></tr>")
                End If
            End If
            fid = dr.Item("func_id").ToString
            coid = dr.Item("comid").ToString
            comp = dr.Item("compnum").ToString
            aidi += 1
            aid = "aid" & aidi
            If rtf = "1" Then
                sb.Append("<tr><td class=""plainlabelred"" colspan=""2"">" & comp & "&nbsp;&nbsp;&nbsp;RTF&nbsp;&nbsp;&nbsp;<b>RPN:</b>&nbsp;" & rpn & "&nbsp;&nbsp;&nbsp;<b>Detectable:</b>&nbsp;" & det & "</td></tr>")
            Else
                sb.Append("<tr><td class=""plainlabel"" colspan=""2""><A href=""#"" onclick=""gettask('" & fid & "','" & coid & "','" & comp & "','" & aid & "');"">" & comp & "</A>&nbsp;&nbsp;&nbsp;<b>RPN:</b>&nbsp;" & rpn & "&nbsp;&nbsp;&nbsp;<b>Detectable:</b>&nbsp;" & det & "</td></tr>")
            End If



        End While
        dr.Close()
        pms.Dispose()
        sb.Append("</Table>")
        tdwi.InnerHtml = sb.ToString
        tdeq.InnerHtml = eqnum & "&nbsp;&nbsp;&nbsp;&nbsp;" & eqdesc
    End Sub

End Class
