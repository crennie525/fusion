﻿Imports System.Data.SqlClient
Imports System.Text
Public Class w_eq23
    Inherits System.Web.UI.Page
    Dim ctasks As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim sid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            ctasks.Open()
            GetList()
            ctasks.Dispose()
        End If
    End Sub
    Private Sub GetList()
        sql = "select distinct eq23 from equipment where siteid = '" & sid & "' and eq23 is not null order by eq23"
        Dim sb As New StringBuilder
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 300px;  HEIGHT: 300px"">")
        sb.Append("<table width=""280"" cellpadding=""1"">")
        Dim eq23 As String
        dr = ctasks.GetRdrData(sql)
        While dr.Read
            eq23 = dr.Item("eq23").ToString
            sb.Append("<tr><td class=""plainlabel""><a href=""#"" class=""A1"" onclick=""getthis('" & eq23 & "')"">" & eq23 & "</td></tr>" & vbCrLf)
        End While
        dr.Close()

        sb.Append("</table></div>")
        tdtasks.InnerHtml = sb.ToString
    End Sub
End Class