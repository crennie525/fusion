﻿Imports System.Data.SqlClient
Public Class pmrlist
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim tots As New Utilities
    Dim sid, typ As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            Try
                typ = Request.QueryString("typ").ToString

            Catch ex As Exception
                typ = "RBAS"
            End Try
            lbltyp.Value = typ
            tots.Open()
            getlist(sid, typ)
            tots.Dispose()
        End If
    End Sub
    Private Sub getlist(ByVal sid As String, ByVal typ As String)
        Dim sname, ttyp As String
        sql = "select sitename from sites where siteid = '" & sid & "'"
        sname = tots.strScalar(sql)

        If typ = "RBAS" Then
            ttyp = "PM Route Report List"
            sql = "select distinct r.rid, r.[route], r.[description], r.pm, " _
            + "tasks = (select count(*) from pmtasks p where p.rteid = r.rid and p.subtask = 0), " _
            + "ttime = (select sum(p.ttime) from pmtasks p where p.rteid = r.rid) " _
            + "from pmroutes r " _
            + "where r.siteid = '" & sid & "' and r.rttype = 'RBAS' and r.pm is not null order by r.route"
        Else
            ttyp = "TPM Route Report List"
            sql = "select distinct r.rid, r.[route], r.[description], r.pm, " _
            + "tasks = (select count(*) from pmtaskstpm p where p.rteid = r.rid and p.subtask = 0), " _
            + "ttime = (select sum(p.ttime) from pmtaskstpm p where p.rteid = r.rid) " _
            + "from pmroutes r " _
            + "where r.siteid = '" & sid & "' and r.rttype = 'RBAST' and r.pm is not null order by r.route"
        End If
        Dim sb As StringBuilder = New StringBuilder
        sb.Append("<table width=""700"">")
        sb.Append("<tr><td width=""120""></td><td width=""270""></td><td width=""130""></td><td width=""90""></td><td width=""90""></td></tr>")
        sb.Append("<tr><td class=""replabeltop"" colspan=""5"" align=""center"">" & ttyp & "</td></tr>")
        sb.Append("<tr><td class=""label"" colspan=""5"" align=""center"">" & sname & "</td></tr>")
        sb.Append("<tr><td colspan=""5"">&nbsp;</td></tr>")
        sb.Append("<tr><td class=""labelsmtl"">Route Name</td>")
        sb.Append("<td class=""labelsmtl"">Route Description</td>")
        If typ = "RBAS" Then
            sb.Append("<td class=""labelsmtl"">PM</td>")
        Else
            sb.Append("<td class=""labelsmtl"">TPM</td>")
        End If

        sb.Append("<td class=""labelsmtl"">Total Tasks</td>")
        sb.Append("<td class=""labelsmtl"">Total Time</td></tr>")

        Dim rid, rte, rted, pm, tsks, ttime As String
        dr = tots.GetRdrData(sql)
        While dr.Read
            rid = dr.Item("rid").ToString
            rte = dr.Item("route").ToString
            rted = dr.Item("description").ToString
            pm = dr.Item("pm").ToString
            tsks = dr.Item("tasks").ToString
            ttime = dr.Item("ttime").ToString
            If tsks <> "0" Then
                sb.Append("<tr><td class=""plainlabelsm""><a href=""#"" onclick=""handleprint('" & typ & "','" & rid & "');"">" & rte & "</a></td>")
                sb.Append("<td class=""plainlabelsm"">" & rted & "</td>")
                sb.Append("<td class=""plainlabelsm"">" & pm & "</td>")
                sb.Append("<td class=""plainlabelsm"">" & tsks & "</td>")
                sb.Append("<td class=""plainlabelsm"">" & ttime & "</td></tr>")
            End If
            
        End While
        dr.Close()
        sb.Append("</table>")
        dvlist.InnerHtml = sb.ToString
    End Sub
End Class