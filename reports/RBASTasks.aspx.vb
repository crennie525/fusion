﻿Imports System.Data.SqlClient
Public Class RBASTasks
    Inherits System.Web.UI.Page
    Dim dr As SqlDataReader
    Dim sql As String
    Dim fail As New Utilities
    Dim rteid, typ, skillid, qty, freq, rdid, lang As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try

        If Not IsPostBack Then
            rteid = Request.QueryString("rteid").ToString
            lblrteid.Value = rteid
            fail.Open()
            GetTaskInfo(rteid)
            loadtop(rteid)
            loadrtasks()
            fail.Dispose()
        Else
            If lblsubmit.Value = "save" Then
                fail.Open()
                savetasks()
                fail.Dispose()
                lblsubmit.Value = "go"
            ElseIf lblsubmit.Value = "savex" Then
                fail.Open()
                savetasksx()
                fail.Dispose()
                lblsubmit.Value = "go"
            End If
        End If
    End Sub
    Private Sub GetTaskInfo(ByVal rteid As String)
        sql = "select skillid, qty, freq, rdid, route, rttype from pmroutes where rid = '" & rteid & "'"
        dr = fail.GetRdrData(sql)
        While dr.Read
            skillid = dr.Item("skillid").ToString
            qty = dr.Item("qty").ToString
            freq = dr.Item("freq").ToString
            rdid = dr.Item("rdid").ToString
            typ = dr.Item("rttype").ToString
            'rte = dr.Item("route").ToString
        End While
        dr.Close()
        If skillid = "0" Then
            If typ = "RBAS" Then
                sql = "select distinct skillid, qty, freq, rdid from pmtasks where rteid = '" & rteid & "'"
            ElseIf typ = "RBAS2" Then
                sql = "select distinct skillid, qty, freq, rdid from pmtaskstpm where rteid = '" & rteid & "'"
            End If

            dr = fail.GetRdrData(sql)
            While dr.Read
                skillid = dr.Item("skillid").ToString
                qty = dr.Item("qty").ToString
                freq = dr.Item("freq").ToString
                rdid = dr.Item("rdid").ToString
                'rte = dr.Item("route").ToString
            End While
            dr.Close()
            sql = "update pmroutes set skillid = '" & skillid & "', qty = '" & qty & "', freq = '" & freq & "', rdid = '" & rdid & "' where rid = '" & rteid & "'"
            fail.Update(sql)
        End If
        'skillid = "26"
        'qty = "1"
        'freq = "30"
        'rdid = "1"
        'rte = "Sample"
        lblskillid.Value = skillid
        lblqty.Value = qty
        lblfreq.Value = freq
        lblrdid.Value = rdid
        lblrttype.Value = typ
        'lblrte.Value = rte
    End Sub
    Private Sub savetasks()
        skillid = lblskillid.Value
        qty = lblqty.Value
        freq = lblfreq.Value
        rdid = lblrdid.Value
        rteid = lblrteid.Value
        typ = lblrttype.Value
        Dim tskstr As String = lbltasks.Value
        Dim tskstredit As String = lbltasksedit.Value
        Dim taskstradd, taskstrrem As String
        'taskstradd = 0
        'taskstrrem = 0
        Dim taskent As String
        Dim tskfnd As Integer
        Dim tskedar() As String = tskstredit.Split(",")
        For i = 0 To tskedar.Length - 1
            tskfnd = 0
            taskent = tskedar(i)
            tskfnd = tskstr.IndexOf(taskent)
            If tskfnd = -1 Then
                If taskstrrem = "" Then
                    taskstrrem = taskent
                Else
                    taskstrrem += "," + taskent
                End If
            End If
        Next
        Dim tskedar2() As String = tskstr.Split(",")
        For i = 0 To tskedar2.Length - 1
            tskfnd = 0
            taskent = tskedar2(i)
            tskfnd = tskstredit.IndexOf(taskent)
            If tskfnd = -1 Then
                If taskstradd = "" Then
                    taskstradd = taskent
                Else
                    taskstradd += "," + taskent
                End If
            End If
        Next
        'Do rem and reorder process here
        If Len(taskstrrem) > 0 Then
            'send rteid, pmtskid str
            If typ = "RBAS" Then
                sql = "exec usp_rbas_taskrem '" & rteid & "','" & taskstrrem & "'"
                fail.Update(sql)
            Else
                sql = "exec usp_rbas_taskremtpm '" & rteid & "','" & taskstrrem & "'"
                fail.Update(sql)
            End If
        End If
        Dim pmtskid As String
        If typ = "RBAS" Then
            sql = "update pmtasks set rteno = 0 where rteid = '" & rteid & "'"
        ElseIf typ = "RBAST" Then
            sql = "update pmtaskstpm set rteno = 0 where rteid = '" & rteid & "'"
        End If

        fail.Update(sql)
        If tskstr <> "" Then
            Dim tskrar() As String = tskstr.Split(",")
            For i = 0 To tskrar.Length - 1
                pmtskid = tskrar(i)
                If typ = "RBAS" Then
                    sql = "update pmtasks set rteid = '" & rteid & "', rteno = 1 where pmtskid = '" & pmtskid & "'"
                ElseIf typ = "RBAST" Then
                    sql = "update pmtaskstpm set rteid = '" & rteid & "', rteno = 1 where pmtskid = '" & pmtskid & "'"
                End If

                fail.Update(sql)
            Next
        End If
        'Do add process below
        If typ = "RBAS" Then
            
            If Len(lbltasksedit.Value) > 0 Then
                If Len(taskstradd) > 0 Then
                    sql = "exec usp_rbas_taskadd '" & rteid & "','" & taskstradd & "'"
                    fail.Update(sql)
                End If
            Else
                'Should be a new entry so use default task order sys
                sql = "update pmtasks set rteid = NULL where rteid = '" & rteid & "' and rteno = 0"
                fail.Update(sql)
                sql = "usp_rbas_dflt_seq '" & rteid & "','" & skillid & "','" & qty & "','" & freq & "','" & rdid & "'"
                fail.Update(sql)
            End If
            
            
        ElseIf typ = "RBAST" Then
            
            If Len(lbltasksedit.Value) > 0 Then
                If Len(taskstradd) > 0 Then
                    sql = "exec usp_rbas_taskaddtpm '" & rteid & "','" & taskstradd & "'"
                    fail.Update(sql)
                End If
            Else
                'Should be a new entry so use default task order sys
                sql = "update pmtaskstpm set rteid = NULL where rteid = '" & rteid & "' and rteno = 0"
                fail.Update(sql)
                sql = "usp_rbas_dflt_seqT '" & rteid & "','" & skillid & "','" & qty & "','" & freq & "','" & rdid & "'"
                fail.Update(sql)
            End If
            
            
        End If

    End Sub
    Private Sub savetasksx()
        skillid = lblskillid.Value
        qty = lblqty.Value
        freq = lblfreq.Value
        rdid = lblrdid.Value
        rteid = lblrteid.Value
        typ = lblrttype.Value
        Dim tskstr As String = lbltasks.Value
        Dim pmtskid As String
        If typ = "RBAS" Then
            sql = "update pmtasks set rteno = 0 where rteid = '" & rteid & "'"
        ElseIf typ = "RBAST" Then
            sql = "update pmtaskstpm set rteno = 0 where rteid = '" & rteid & "'"
        End If

        fail.Update(sql)

        If typ = "RBAS" Then
            sql = "update pmtasks set rteid = NULL where rteid = '" & rteid & "' and rteno = 0"
            fail.Update(sql)
            sql = "usp_rbas_dflt_seq '" & rteid & "','" & skillid & "','" & qty & "','" & freq & "','" & rdid & "'"
            fail.Update(sql)
        ElseIf typ = "RBAST" Then
            sql = "update pmtaskstpm set rteid = NULL where rteid = '" & rteid & "' and rteno = 0"
            fail.Update(sql)
            sql = "usp_rbas_dflt_seqT '" & rteid & "','" & skillid & "','" & qty & "','" & freq & "','" & rdid & "'"
            fail.Update(sql)
        End If

    End Sub
    Private Sub loadtop(ByVal rteid As String)
        lang = lblfslang.Value

        lblsel.Value = 0
        typ = lblrttype.Value
        Dim ttime As Integer
        If typ = "RBAS" Then
            sql = "select sum(ttime) from pmtasks where rteid = '" & rteid & "' and rteno = 0"
        ElseIf typ = "RBAST" Then
            sql = "select sum(ttime) from pmtaskstpm where rteid = '" & rteid & "' and rteno = 0"
        End If

        Try
            ttime = fail.Scalar(sql)
        Catch ex As Exception
            ttime = 0
        End Try
        Dim pmtskid, taskstr As String
        taskstr = ""
        If typ = "RBAS" Then
            sql = "select pmtskid from pmtasks where rteid = '" & rteid & "' and rteno = 1"
        ElseIf typ = "RBAST" Then
            sql = "select pmtskid from pmtaskstpm where rteid = '" & rteid & "' and rteno = 1"
        End If

        dr = fail.GetRdrData(sql)
        While dr.Read
            pmtskid = dr.Item("pmtskid").ToString
            If taskstr = "" Then
                taskstr = pmtskid
            Else
                taskstr += "," & pmtskid
            End If
        End While
        dr.Close()
        lbltasks.Value = taskstr
        lbltasksedit.Value = taskstr
        Dim stcnt As Integer
        If typ = "RBAS" Then
            sql = "select count(*) from pmtasks where rteid = '" & rteid & "' and rteno = 1"
        ElseIf typ = "RBAST" Then
            sql = "select count(*) from pmtaskstpm where rteid = '" & rteid & "' and rteno = 1"
        End If

        Try
            stcnt = fail.Scalar(sql)
        Catch ex As Exception
            stcnt = 0
        End Try
        Dim sttime As Integer
        If typ = "RBAS" Then
            sql = "select sum(ttime) from pmtasks where rteid = '" & rteid & "' and rteno = 1"
        ElseIf typ = "RBAST" Then
            sql = "select sum(ttime) from pmtaskstpm where rteid = '" & rteid & "' and rteno = 1"
        End If

        Try
            sttime = fail.Scalar(sql)
        Catch ex As Exception
            sttime = 0
        End Try
        lblttime.Value = ttime
        Dim tottime As Integer
        If typ = "RBAS" Then
            sql = "select isnull(sum(isnull(ttime, 0)), 0) from pmtasks where rteid = '" & rteid & "' and rteno = 1"
        ElseIf typ = "RBAST" Then
            sql = "select isnull(sum(isnull(ttime, 0)), 0) from pmtaskstpm where rteid = '" & rteid & "' and rteno = 1"
        End If

        tottime = fail.Scalar(sql)
        lbltottime.Value = tottime
        Dim pm, route, routedesc As String '= "MECH(1)/30 DAYS/Running"
        sql = "select pm, route, description from pmroutes where rid = '" & rteid & "'"
        'pm = fail.strScalar(sql)
        dr = fail.GetRdrData(sql)
        While dr.Read
            pm = dr.Item("pm").ToString
            route = dr.Item("route").ToString
            routedesc = dr.Item("description").ToString
        End While
        dr.Close()

        Dim tottasks As Integer
        If typ = "RBAS" Then
            sql = "select count(*) from pmtasks where rteid = '" & rteid & "' and rteno = 0 and subtask = 0"
        ElseIf typ = "RBAST" Then
            sql = "select count(*) from pmtaskstpm where rteid = '" & rteid & "' and rteno = 0 and subtask = 0"
        End If

        tottasks = fail.Scalar(sql)
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td colspan=""7"">")
        sb.Append("<table><tr>")
        sb.Append("<td width=""20""><img src=""../images/appbuttons/minibuttons/routenum.gif""></td>")
        sb.Append("<td width=""180"" class=""plainlabel"">" & route & "</td>")
        sb.Append("<td width=""20""><img src=""../images/appbuttons/minibuttons/routeblnk.gif""></td>")
        sb.Append("<td width=""80"" class=""label"">Description</td>")
        sb.Append("<td width=""180"" class=""plainlabel"">" & routedesc & "</td>")
        sb.Append("</tr></table>")
        sb.Append("</td></tr>")
        sb.Append("<tr><td colspan=""7"" style=""border-bottom: 2px solid blue;"">&nbsp;</td></tr>")
        sb.Append("<tr><td width=""160"" class=""plainlabelblue"">" & pm & "</td>")
        If lang = "fre" Then
            sb.Append("<td width=""80"" class=""label"">Comptage total:</td>")
        Else
            sb.Append("<td width=""80"" class=""label"">Total Count:</td>")
        End If

        sb.Append("<td width=""40"" class=""plainlabel"">" & tottasks & "</td>")
        If lang = "fre" Then
            sb.Append("<td width=""120"" class=""label"">Temps total pour toutes les tâches:</td>")
        Else
            sb.Append("<td width=""120"" class=""label"">Total Time All Tasks:</td>")
        End If

        sb.Append("<td width=""40"" class=""plainlabel"">" & ttime & "</td>")
        If lang = "fre" Then
            sb.Append("<td width=""100"" class=""label"">Tâches sélectionnées:</td>")
        Else
            sb.Append("<td width=""100"" class=""label"">Selected Tasks:</td>")
        End If

        sb.Append("<td width=""40"" class=""plainlabel"" id=""tdsel"">" & stcnt & "</td>")
        sb.Append("</tr>")
        If lang = "fre" Then
            sb.Append("<tr><td colspan=""3""></td><td colspan=""3"" class=""label"">Temps cumulé pour les tâches sélectionnées</td>")
        Else
            sb.Append("<tr><td colspan=""3""></td><td colspan=""3"" class=""label"">Cumulative Time for Selected Tasks</td>")
        End If

        sb.Append("<td class=""plainlabel"" id=""tdtottime"">" & tottime & "</td></tr>")
        'sb.Append("<tr><td colspan=""7"" style=""border-bottom: 2px solid blue;"">&nbsp;</td></tr>")
        'sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>")
        sb.Append("</table>")
        dvtop.InnerHtml = sb.ToString
    End Sub
    Private Sub loadrtasks()
        rteid = lblrteid.Value
        typ = lblrttype.Value
        'sql = "select t.pmtskid, t.taskdesc from PMTasks t " _
        '+ "where t.rteid = '" & rteid & "' and t.rteno = 0"
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""20""><td width=""620""></td></tr>")
        Dim cbx, eq, eqnum, eqdesc, func, comp, tasknum, taskdesc, ttime, pmtskid, loc, locdesc, rteno
        Dim eqhold As String = ""
        Dim lochold As String = ""
        Dim cb As Integer = 1
        If typ = "RBAS" Then
            sql = "usp_getRBASTasks2 '" & rteid & "'"
        ElseIf typ = "RBAST" Then
            sql = "usp_getRBASTasks2T '" & rteid & "'"
        End If

        dr = fail.GetRdrData(sql)
        While dr.Read
            loc = dr.Item("loc").ToString
            locdesc = dr.Item("locdesc").ToString
            pmtskid = dr.Item("pmtskid").ToString
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            func = dr.Item("func").ToString
            'comp = dr.Item("comp").ToString
            tasknum = dr.Item("tasknum").ToString
            taskdesc = dr.Item("task").ToString
            ttime = dr.Item("ttime").ToString
            rteno = dr.Item("rteno").ToString
            eq = eqnum & " - " & func

            If loc <> lochold Then
                lochold = loc
                If lochold <> "" Then
                    sb.Append("<tr><td colspan=""2"">&nbsp;</td></tr>")
                    sb.Append("<tr><td></td><td class=""bluelabel"">" & loc & " - " & locdesc & "</td></tr>")
                    sb.Append("<tr><td colspan=""2"" style=""border-top: 2px solid blue;"">&nbsp;</td></tr>")
                Else
                    sb.Append("<tr><td></td><td class=""bluelabel"">" & loc & " - " & locdesc & "</td></tr>")
                    sb.Append("<tr><td colspan=""2"" style=""border-top: 2px solid blue;"">&nbsp;</td></tr>")
                End If


            End If
            If eq <> eqhold Then
                eqhold = eq
                sb.Append("<tr><td></td><td class=""label"">" & eq & "</td></tr>")
                cb += 1
                cbx = "cb" & cb
                sb.Append("<tr><td colspan=""2""><table><tr>")
                If rteno = "0" Then
                    sb.Append("<td width=""20""><input type=""checkbox"" id=""" & cbx & """ onclick=""getsel('" + cbx + "','" + ttime + "','" + pmtskid + "')""></td>")
                Else
                    sb.Append("<td width=""20""><input type=""checkbox"" id=""" & cbx & """ onclick=""getsel('" + cbx + "','" + ttime + "','" + pmtskid + "')"" checked=""yes""></td>")
                End If

                sb.Append("<td width=""20"" class=""plainlabel"">" & tasknum & "</td>")
                sb.Append("<td width=""540"" class=""plainlabel"">" & taskdesc & "</td>")
                sb.Append("<td width=""40"" class=""plainlabel"">" & ttime & "</td>")
                sb.Append("</tr></table></td></tr>")
            Else
                cb += 1
                cbx = "cb" & cb
                sb.Append("<tr><td colspan=""2""><table><tr>")
                If rteno = "0" Then
                    sb.Append("<td width=""20""><input type=""checkbox"" id=""" & cbx & """ onclick=""getsel('" + cbx + "','" + ttime + "','" + pmtskid + "')""></td>")
                Else
                    sb.Append("<td width=""20""><input type=""checkbox"" id=""" & cbx & """ onclick=""getsel('" + cbx + "','" + ttime + "','" + pmtskid + "')"" checked=""yes""></td>")
                End If
                sb.Append("<td width=""20"" class=""plainlabel"">" & tasknum & "</td>")
                sb.Append("<td width=""540"" class=""plainlabel"">" & taskdesc & "</td>")
                sb.Append("<td width=""40"" class=""plainlabel"">" & ttime & "</td>")
                sb.Append("</tr></table></td></tr>")
            End If

        End While
        dr.Close()
        sb.Append("<tr><td colspan=""2"" align=""right""><img src=""../images/appbuttons/bgbuttons/submit.gif"" onclick=""savetasks();""> ")
        sb.Append("</Table>")
        dvtasks.InnerHtml = sb.ToString
    End Sub
End Class