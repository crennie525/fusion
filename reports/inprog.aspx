<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="inprog.aspx.vb" Inherits="lucy_r12.inprog" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>inprog</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" src="../scripts1/inprogaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                var d = new Date();
                var curr_date = d.getDate();
                var curr_month = d.getMonth();
                curr_month++;
                var curr_year = d.getFullYear();
                var hr = d.getHours();
                var mn = d.getMinutes();
                var ss = d.getSeconds();
                //document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                var d = new Date();
                var curr_date = d.getDate();
                var curr_month = d.getMonth();
                curr_month++;
                var curr_year = d.getFullYear();
                var hr = d.getHours();
                var mn = d.getMinutes();
                var ss = d.getSeconds();
                //document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                var d = new Date();
                var curr_date = d.getDate();
                var curr_month = d.getMonth();
                curr_month++;
                var curr_year = d.getFullYear();
                var hr = d.getHours();
                var mn = d.getMinutes();
                var ss = d.getSeconds();
                //document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                var d = new Date();
                var curr_date = d.getDate();
                var curr_month = d.getMonth();
                curr_month++;
                var curr_year = d.getFullYear();
                var hr = d.getHours();
                var mn = d.getMinutes();
                var ss = d.getSeconds();
                //document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }

    </script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td id="tdtop" runat="server">
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" width="700">
                    <tr>
                        <td class="label" width="90">
                            <asp:Label ID="lang3337" runat="server">Site Location</asp:Label>
                        </td>
                        <td width="330">
                            <asp:DropDownList ID="ddsites" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="label" width="50">
                            <asp:Label ID="lang3338" runat="server">Search</asp:Label>
                        </td>
                        <td width="210">
                            <asp:TextBox ID="txtsrch" runat="server" Width="200px"></asp:TextBox>
                        </td>
                        <td width="20">
                            <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                            </asp:ImageButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td id="tdinprog" runat="server">
            </td>
        </tr>
        <tr>
            <td align="center">
                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                    border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input id="txtpg" type="hidden" name="txtpg" runat="server">
			<input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
            <input type="hidden" id="lblret" runat="server" />
            <input type="hidden" id="lblcoi" runat="server" />
    </form>
</body>
</html>
