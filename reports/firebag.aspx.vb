﻿Imports System.Data.SqlClient
Public Class firebag
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim fb As New Utilities
    Dim dr As SqlDataReader
    Dim eqid, fuid As String
    Dim eqnum, eqdesc, assetclass, oem, ecr As String
    Dim func, comp, fmodes As String
    Dim tasknum, taskdesc, skill, freq, fm1, rdid As String
    Dim sb As New StringBuilder

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString '"485" '
            fb.Open()
            getit()
            'buildit()
            loopit()
            fb.Dispose()
            divrep.InnerHtml = sb.ToString
        End If

    End Sub
    Private Sub buildit()
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""1020"" style=""page-break-after:always;table-layout: fixed;"">")
        sb.Append("<tr>")
        sb.Append("<td width=""22""></td>")
        sb.Append("<td width=""160""></td>")
        sb.Append("<td width=""140""></td>")
        sb.Append("<td width=""120""></td>")
        sb.Append("<td width=""100""></td>")
        sb.Append("<td width=""100""></td>")
        sb.Append("<td width=""100""></td>")
        sb.Append("<td width=""100""></td>")
        sb.Append("<td width=""80""></td>")
        sb.Append("<td width=""100""></td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""3"" class=""tboxl bg1"">Section 1</td>")
        sb.Append("<td colspan=""4"" class=""tboxl1 bg1"">Firebag Equipment Strategy Data Sheet</td>")
        sb.Append("<td colspan=""3"" class=""tboxr1 bg1"">Equip ID#&nbsp;&nbsp" & eqnum & "</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        If oem <> "" Then
            sb.Append("<td colspan=""10"" class=""dbox bg1"">" & eqdesc & " / " & oem & "</td>")
        Else
            sb.Append("<td colspan=""10"" class=""dbox bg1"">" & eqdesc & "</td>")
        End If

        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""3"" class=""rboxlh"">Equipment Identifier</td>")
        sb.Append("<td colspan=""1"" class=""rboxlh"">See Asset Tag Sheet</td>")
        sb.Append("<td colspan=""3"" class=""rboxlh"">Business Unit/Area</td>")
        sb.Append("<td colspan=""3"" class=""rboxrh"">Firebag</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""3"" class=""rboxl1"">Equipment Type</td>")
        If assetclass <> "" And assetclass.ToLower <> "select" Then
            sb.Append("<td colspan=""1"" class=""rboxl1"">" & assetclass & "</td>")
        Else
            sb.Append("<td colspan=""1"" class=""rboxl1"">&nbsp;</td>")
        End If
        sb.Append("<td colspan=""3"" class=""rboxl1"">Production impact</td>")
        sb.Append("<td colspan=""3"" class=""rboxr1"">&nbsp;</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""3"" class=""rboxl1"">Criticality Ranking</td>")
        If ecr <> "" Then
            sb.Append("<td colspan=""1"" class=""rboxl1"">" & ecr & "</td>")
        Else
            sb.Append("<td colspan=""1"" class=""rboxl1"">&nbsp;</td>")
        End If
        sb.Append("<td colspan=""3"" class=""rboxl1"">Availability Target agreed with BUCFT</td>")
        sb.Append("<td colspan=""3"" class=""rboxr1"">&nbsp;</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""3"" class=""rboxl1"">By-passable/spared</td>")
        sb.Append("<td colspan=""1"" class=""rboxl1"">&nbsp;</td>")
        sb.Append("<td colspan=""3"" class=""rboxl1"">Major Overhaul/Replacement Frequency</td>")
        sb.Append("<td colspan=""3"" class=""rboxr1"">&nbsp;</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""3"" class=""rboxl1"">Consequence of Equip. Outage - Prod Y or N</td>")
        sb.Append("<td colspan=""1"" class=""rboxl1"">&nbsp;</td>")
        sb.Append("<td colspan=""3"" class=""rboxl1"">Unit/Equip replacement cost</td>")
        sb.Append("<td colspan=""3"" class=""rboxr1"">&nbsp;</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""3"" class=""rboxl1"">Cost of Equipment Outage (rental rate)</td>")
        sb.Append("<td colspan=""1"" class=""rboxl1"">&nbsp;</td>")
        sb.Append("<td colspan=""3"" class=""rboxl1"">Cost of major failure</td>")
        sb.Append("<td colspan=""3"" class=""rboxr1"">&nbsp;</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""3"" class=""rboxl1"">Maintenance Response?</td>")
        sb.Append("<td colspan=""1"" class=""rboxl1"">&nbsp;</td>")
        sb.Append("<td colspan=""3"" class=""rboxl1"">Replace/repair time</td>")
        sb.Append("<td colspan=""3"" class=""rboxr1"">&nbsp;</td>")
        sb.Append("</tr>")
    End Sub
    Private Sub loopit()
        sql = "select func_id as 'fuid' from functions where eqid = '" & eqid & "' order by routing"
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds = fb.GetDSData(sql)
        dt = New DataTable
        dt = ds.Tables(0)
        Dim dti As Integer = dt.Rows.Count
        Dim row As DataRow
        Dim fcnt As Integer = 0
        For Each row In dt.Rows
            fcnt += 1
            buildit()
            sb.Append("<tr>")
            sb.Append("<td colspan=""3"" class=""rboxlh bg1"">Section 2</td>")
            sb.Append("<td colspan=""7"" class=""rboxrh1 bg1"">Significant Failure Modes and Maintenance Strategies for Risk Mitigation</td>")
            sb.Append("</tr>")
            sb.Append("<tr>")
            sb.Append("<td class=""rboxl bg2"">#</td>")
            sb.Append("<td class=""rboxl bg2"">Part</td>")
            sb.Append("<td class=""rboxl bg2"">Degradation Mechanism</td>")
            sb.Append("<td class=""rboxl bg2"">Effect Of Degradation</td>")
            sb.Append("<td class=""rboxl bg2"">Degrade. Rate</td>")
            sb.Append("<td class=""rboxl bg2"">Likelihood</td>")
            sb.Append("<td class=""rboxl bg2"">Consequence</td>")
            sb.Append("<td class=""rboxl bg2"">Risk Asses.</td>")
            sb.Append("<td class=""rboxl bg2"">Action</td>")
            sb.Append("<td class=""rboxr bg2"">Maintenance Strategy</td>")
            sb.Append("</tr>")
            fuid = row("fuid").ToString
            getfunc(fuid)

            sb.Append("<tr>")
            sb.Append("<td colspan=""3"" class=""rboxlh bg1"">Section 3</td>")
            sb.Append("<td colspan=""7"" class=""rboxrh1 bg1"">Maintenance Programs - Input Details</td>")
            sb.Append("</tr>")
            sb.Append("<tr>")
            sb.Append("<td class=""rboxl bg2"">#</td>")
            sb.Append("<td class=""rboxl bg2"" colspan=""2"">Action</td>")
            sb.Append("<td class=""rboxl bg2"">Procedure</td>")
            sb.Append("<td class=""rboxl bg2"">Frequency</td>")
            sb.Append("<td class=""rboxl bg2"" colspan=""2"">Comments</td>")
            sb.Append("<td class=""rboxl bg2"">Mit. Risk</td>")
            sb.Append("<td class=""rboxl bg2"">Unit S/D</td>")
            sb.Append("<td class=""rboxr bg2"">By Who</td>")
            sb.Append("</tr>")
            gettasks(fuid)
            sb.Append("</table>")
        Next
        'use usp_firebag_fu with fuid to get func, comp and fail modes
    End Sub
    Private Sub getfunc(ByVal fuid As String)
        Dim ccnt As Integer = 0
        sql = "usp_firebag_fu '" & fuid & "'"
        dr = fb.GetRdrData(sql)
        While dr.Read
            ccnt += 1
            func = dr.Item("func").ToString
            comp = dr.Item("compnum").ToString
            fmodes = dr.Item("failmodes").ToString
            sb.Append("<tr>")
            sb.Append("<td class=""rboxl"">" & ccnt & "</td>")
            sb.Append("<td class=""rboxl1"">" & func & "<br>" & comp & "</td>")
            sb.Append("<td class=""rboxl"">" & fmodes & "</td>")
            sb.Append("<td class=""rboxl"">&nbsp;</td>")
            sb.Append("<td class=""rboxl"">&nbsp;</td>")
            sb.Append("<td class=""rboxl"">&nbsp;</td>")
            sb.Append("<td class=""rboxl"">&nbsp;</td>")
            sb.Append("<td class=""rboxl"">&nbsp;</td>")
            sb.Append("<td class=""rboxl"">&nbsp;</td>")
            sb.Append("<td class=""rboxr"">&nbsp;</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        'Dim func, comp, fmodes As String
    End Sub
    Private Sub gettasks(ByVal fuid As String)
        sql = "select t.tasknum, t.taskdesc, t.skill, t.freq, t.fm1, t.rdid " _
            + "from pmtasks t " _
            + "left join functions f on f.func_id = t.funcid " _
            + "left join components c on c.comid = t.comid " _
            + "where t.taskstatus <> 'Delete' and t.funcid = '" & fuid & "' and t.subtask = 0 " _
            + "order by f.routing, c.crouting, t.tasknum"
        dr = fb.GetRdrData(sql)
        While dr.Read
            tasknum = dr.Item("tasknum").ToString
            taskdesc = dr.Item("taskdesc").ToString
            skill = dr.Item("skill").ToString
            freq = dr.Item("freq").ToString
            fm1 = dr.Item("fm1").ToString
            rdid = dr.Item("rdid").ToString
            If rdid = "1" Then
                rdid = "Y"
            ElseIf rdid = "2" Then
                rdid = "N"
            Else
                rdid = "N"
            End If

            sb.Append("<tr>")
            sb.Append("<td class=""rboxl"">" & tasknum & "</td>")
            sb.Append("<td class=""rboxl1"" colspan=""2"">" & taskdesc & "<br>" & fm1 & "</td>")
            sb.Append("<td class=""rboxl"">&nbsp;</td>")
            sb.Append("<td class=""rboxl"">" & freq & "</td>")
            sb.Append("<td class=""rboxl"" colspan=""2""&nbsp;></td>")
            sb.Append("<td class=""rboxl"">&nbsp;</td>")
            sb.Append("<td class=""rboxl"">" & rdid & "</td>")
            sb.Append("<td class=""rboxr"">" & skill & "</td>")
            sb.Append("</tr>")

        End While
        dr.Close()

    End Sub
    Private Sub getit()
        'Run Once
        sql = "select e.eqnum, e.eqdesc, e.assetclass, e.[oem], e.ecr from equipment e where eqid = '" & eqid & "'"
        dr = fb.GetRdrData(sql)
        While dr.Read
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            assetclass = dr.Item("assetclass").ToString
            oem = dr.Item("oem").ToString
            ecr = dr.Item("ecr").ToString
        End While
        dr.Close()
    End Sub
End Class