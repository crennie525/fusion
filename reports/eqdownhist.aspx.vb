﻿Imports System.Data.SqlClient
Imports System.Text
Public Class eqdownhist
    Inherits System.Web.UI.Page
    Dim etd As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim eqid, fdate, tdate As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            fdate = Request.QueryString("fdate").ToString
            tdate = Request.QueryString("tdate").ToString
            etd.Open()
            If tdate <> "" And fdate = "" Then
                getfdate()
            ElseIf fdate <> "" And tdate = "" Then
                gettdate()
            End If
            lblfdate.Value = fdate
            lbltdate.Value = tdate
            getrep()
            etd.Dispose()
        Else
            eqid = lbleqid.Value
            fdate = lblfdate.Value
            tdate = lbltdate.Value
            etd.Open()
            getrep()
            etd.Dispose()
        End If
    End Sub
    Private Sub getfdate()
        sql = "select isnull(Convert(char(10),min(isnull(startdown, '01/01/1900')),101), '01/01/1900') from eqhist where eqid = '" & eqid & "' and startdown is not null"
        fdate = etd.strScalar(sql)
    End Sub
    Private Sub gettdate()
        sql = "select Convert(char(10)," & Now & ", 101)"
        tdate = etd.strScalar(sql)
    End Sub
    Private Sub getrep()
        Dim lcnt As Integer = 0
        sql = "select count(*) from equipment where locid is not null and locid <> 0 and eqid = '" & eqid & "'"
        lcnt = etd.Scalar(sql)
        Dim eq, eqd As String
        sql = "select eqnum, eqdesc from equipment where eqid = '" & eqid & "'"
        dr = etd.GetRdrData(sql)
        While dr.Read
            eq = dr.Item("eqnum").ToString
            eqd = dr.Item("eqdesc").ToString
        End While
        dr.Close()
        Dim loc, locdesc, did, clid, cell, celldesc As String
        If lcnt > 0 Then
            sql = "select parent from pmlocheir where location = '" & eq & "'"
            dr = etd.GetRdrData(sql)
            While dr.Read
                loc = dr.Item("parent").ToString
            End While
            dr.Close()
            sql = "select description from pmlocations where location = '" & loc & "'"
            dr = etd.GetRdrData(sql)
            While dr.Read
                locdesc = dr.Item("description").ToString
            End While
            dr.Close()
        Else
            sql = "select d.dept_id, d.dept_line, d.dept_desc, c.cellid, c.cell_name, c.cell_desc " _
                + "from equipment e " _
                + "left join dept d on d.dept_id = e.dept_id " _
                + "left join cells c on c.cellid = e.cellid " _
                + "where e.eqid = '" & eqid & "'"
            dr = etd.GetRdrData(sql)
            While dr.Read
                loc = dr.Item("dept_line").ToString
                locdesc = dr.Item("dept_desc").ToString
                cell = dr.Item("cell_name").ToString
                celldesc = dr.Item("cell_desc").ToString
            End While
            dr.Close()
        End If
        Dim dtot, dtotnpm, dtotjpm As String
        sql = "declare @dtot decimal(10,2), @dtotnpm decimal(10,2), @dtotjpm decimal(10,2) " _
            + "set @dtot = (select isnull(sum(isnull(totaldown,0)),0) from eqhist where eqid = '" & eqid & "') " _
            + "set @dtotnpm = (select isnull(sum(isnull(totaldown,0)),0) from eqhist where ispm <> 1 and eqid = '" & eqid & "') " _
            + "set @dtotjpm = (select isnull(sum(isnull(totaldown,0)),0) from eqhist where ispm = 1 and eqid = '" & eqid & "') " _
            + "select @dtot as dtot, @dtotnpm as dtotnpm, @dtotjpm as dtotjpm"
        dr = etd.GetRdrData(sql)
        While dr.Read
            dtot = dr.Item("dtot").ToString
            dtotnpm = dr.Item("dtotnpm").ToString
            dtotjpm = dr.Item("dtotjpm").ToString
        End While
        dr.Close()

        Dim sb As New StringBuilder
        sb.Append("<table cellspacing=""0"" cellpadding=""4"" width=""700"">")
        sb.Append("<tr>")
        sb.Append("<td colspan=""6"" align=""center"" class=""replabeltop"">Equipment Downtime History Report</td>")
        sb.Append("</tr>")
        If fdate <> "" And fdate <> "01/01/1900" Then
            sb.Append("<tr>")
            sb.Append("<td colspan=""6"" align=""center"" class=""plainlabel"">From " & fdate & " to " & tdate & "</td>")
            sb.Append("</tr>")
        ElseIf fdate <> "" Then
            sb.Append("<tr>")
            sb.Append("<td colspan=""6"" align=""center"" class=""plainlabel"">To " & tdate & "</td>")
            sb.Append("</tr>")
        Else
            sb.Append("<tr>")
            sb.Append("<td colspan=""6"" align=""center"" class=""plainlabel"">" & Now & "</td>")
            sb.Append("</tr>")
        End If
        sb.Append("<tr>")
        sb.Append("<td colspan=""6"">&nbsp;</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td class=""label"">Equipment</td>")
        sb.Append("<td class=""plainlabel"" colspan=""5"">" & eq & "</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td class=""label"">Dscription</td>")
        sb.Append("<td class=""plainlabel"" colspan=""5"">" & eqd & "</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""6"">&nbsp;</td>")
        sb.Append("</tr>")
        If lcnt > 0 Then
            sb.Append("<tr>")
            sb.Append("<td class=""label"">Location</td>")
            sb.Append("<td class=""plainlabel"" colspan=""5"">" & loc & "</td>")
            sb.Append("</tr>")
            sb.Append("<tr>")
            sb.Append("<td class=""label"">Desription</td>")
            sb.Append("<td class=""plainlabel"" colspan=""5"">" & locdesc & "</td>")
            sb.Append("</tr>")
        Else
            sb.Append("<tr>")
            sb.Append("<td class=""label"">Department</td>")
            sb.Append("<td class=""plainlabel"" colspan=""5"">" & loc & " - " & locdesc & "</td>")
            sb.Append("</tr>")
            sb.Append("<tr>")
            sb.Append("<td class=""label"">Cell</td>")
            sb.Append("<td class=""plainlabel"" colspan=""5"">" & cell & " - " & celldesc & "</td>")
            sb.Append("</tr>")
        End If
        sb.Append("<tr>")
        sb.Append("<td colspan=""6"">&nbsp;</td>")
        sb.Append("</tr>")
        'down totals
        sb.Append("<tr>")
        sb.Append("<td colspan=""2"" class=""label"">Total Downtime for this Record:</td>")
        sb.Append("<td colspan=""4"" class=""label"">" & dtot & "</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""2"" class=""label"">Total Downtime for this Record (No PM):</td>")
        sb.Append("<td colspan=""4"" class=""label"">" & dtotnpm & "</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td colspan=""2"" class=""label"">Total Downtime for this Record (Just PM):</td>")
        sb.Append("<td colspan=""4"" class=""label"">" & dtotjpm & "</td>")
        sb.Append("</tr>")
        '***
        sb.Append("<tr>")
        sb.Append("<td colspan=""6"">&nbsp;</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td class=""tboxl1"" width=""100"">WO#</td>")
        sb.Append("<td class=""tboxl1"" width=""190"">Description</td>")
        sb.Append("<td class=""tboxl1"" width=""50"">is PM?</td>")
        sb.Append("<td class=""tboxl1"" width=""140"">Start Down</td>")
        sb.Append("<td class=""tboxl1"" width=""140"">Stop Down</td>")
        sb.Append("<td class=""tboxr"" width=""80"">Total Down</td>")
        sb.Append("</tr>")
        Dim wo, wod, ispm, dstart, dstop, tot

        sql = "select h.wonum, w.description, h.ispm, h.startdown, h.stopdown, isnull(h.totaldown,0) as totaldown " _
            + "from eqhist h " _
            + "left join workorder w on w.wonum = h.wonum " _
            + "where h.eqid = '" & eqid & "' order by h.startdown desc"
        dr = etd.GetRdrData(sql)
        While dr.Read
            wo = dr.Item("wonum").ToString
            wod = dr.Item("description").ToString
            ispm = dr.Item("ispm").ToString
            dstart = dr.Item("startdown").ToString
            dstop = dr.Item("stopdown").ToString
            tot = dr.Item("totaldown").ToString
            'dtot += tot
            If ispm = "1" Then
                ispm = "Yes"
            Else
                ispm = "No"
            End If
            sb.Append("<tr>")
            sb.Append("<td class=""rboxl1"" width=""100"">" & wo & "</td>")
            sb.Append("<td class=""rboxl1"" width=""190"">" & wod & "</td>")
            sb.Append("<td class=""rboxl1"" width=""50"">" & ispm & "</td>")
            sb.Append("<td class=""rboxl1"" width=""140"">" & dstart & "</td>")
            sb.Append("<td class=""rboxl1"" width=""140"">" & dstop & "'</td>")
            sb.Append("<td class=""rboxr"" width=""80"">" & tot & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        divrep.InnerHtml = sb.ToString
    End Sub
End Class