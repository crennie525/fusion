

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class jobplanhtml
    Inherits System.Web.UI.Page
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Protected WithEvents tdwi As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tblwx As System.Web.UI.HtmlControls.HtmlTable
    Dim jpid As String
    Dim tmod As New transmod
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            jpid = Request.QueryString("jpid").ToString
            pms.Open()
            PopJP(jpid)
            pms.Dispose()
        End If
    End Sub
    Private Sub PopJP(ByVal jpid As String)
        Dim fid As String = ""
        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim subtask As String = ""
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""620""></td></tr>")
        sb.Append("<tr><td class=""bigbold"" align=""center"">" & tmod.getlbl("cdlbl836", "jobplanhtml.aspx.vb") & "</td></tr>")

        sb.Append("<tr><td><Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""120"">&nbsp;</td>")
        sb.Append("<td width=""500"">&nbsp;</td></tr>")

        sql = "select jp.jpnum, jp.description, " _
        + "d.dept_line, d.dept_desc, c.cell_name, c.cell_desc, l.location, l.description as loc_desc, " _
        + "e.eqnum, e.eqdesc, n.ncnum, n.ncdesc, f.func, f.func_desc, co.compnum, co.compdesc " _
        + "from pmjobplans jp " _
        + "left join dept d on d.dept_id = jp.deptid " _
        + "left join cells c on c.cellid = jp.cellid " _
        + "left join pmlocations l on l.locid = jp.locid " _
        + "left join equipment e on e.eqid = jp.eqid " _
        + "left join noncritical n on n.ncid = jp.ncid " _
        + "left join functions f on f.func_id = jp.funcid " _
        + "left join components co on co.comid = jp.comid " _
        + "where jp.jpid = '" & jpid & "'"

        Dim jp, jpd, dept, deptd, cell, celld, loc, locd, eq, eqd, nc, ncd, fu, fud, co, cod, mea As String

        dr = pms.GetRdrData(sql)
        While dr.Read
            jp = dr.Item("jpnum").ToString
            jpd = dr.Item("description").ToString
            sb.Append("<tr><td width=""120"" class=""label"">" & tmod.getlbl("cdlbl837", "jobplanhtml.aspx.vb") & "</td>")
            sb.Append("<td width=""500"" class=""plainlabel"">" & jp & "</td></tr>")
            sb.Append("<tr><td width=""120"" class=""label"">" & tmod.getlbl("cdlbl838", "jobplanhtml.aspx.vb") & "</td>")
            sb.Append("<td width=""500"" class=""plainlabel"">" & jpd & "</td></tr></table></td></tr>")
            sb.Append("<tr><td>&nbsp;</td></tr>")
            sb.Append("<tr><td><Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
            sb.Append("<tr><td class=""label"" colspan=""3""><u>" & tmod.getxlbl("xlb338", "jobplanhtml.aspx.vb") & "</u></td></tr>")
            dept = dr.Item("dept_line").ToString
            deptd = dr.Item("dept_desc").ToString
            cell = dr.Item("cell_name").ToString
            celld = dr.Item("cell_desc").ToString
            loc = dr.Item("location").ToString
            locd = dr.Item("loc_desc").ToString
            sb.Append("<tr><td width=""120"" class=""label"">" & tmod.getlbl("cdlbl839", "jobplanhtml.aspx.vb") & "</td>")
            sb.Append("<td width=""150"" class=""plainlabel"">" & dept & "</td>")
            sb.Append("<td width=""350"" class=""plainlabel"">" & deptd & "</td><tr>")
            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl840", "jobplanhtml.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & cell & "</td>")
            sb.Append("<td class=""plainlabel"">" & celld & "</td></tr>")
            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl841", "jobplanhtml.aspx.vb") & "</td>")
            sb.Append("<td  class=""plainlabel"">" & loc & "</td>")
            sb.Append("<td class=""plainlabel"">" & locd & "</td></tr><table></td></tr>")
            sb.Append("<tr><td>&nbsp;</td></tr>")
            sb.Append("<tr><td><Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
            sb.Append("<tr><td class=""label"" colspan=""3""><u>" & tmod.getxlbl("xlb339", "jobplanhtml.aspx.vb") & "</u></td></tr>")
            eq = dr.Item("eqnum").ToString
            eqd = dr.Item("eqdesc").ToString
            nc = dr.Item("ncnum").ToString
            ncd = dr.Item("ncdesc").ToString
            fu = dr.Item("func").ToString
            fud = dr.Item("func_desc").ToString
            co = dr.Item("compnum").ToString
            cod = dr.Item("compdesc").ToString
            sb.Append("<tr><td width=""120"" class=""label"">" & tmod.getlbl("cdlbl842", "jobplanhtml.aspx.vb") & "</td>")
            sb.Append("<td width=""150"" class=""plainlabel"">" & eq & "</td>")
            sb.Append("<td width=""350"" class=""plainlabel"">" & eqd & "</td><tr>")
            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl843", "jobplanhtml.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & nc & "</td>")
            sb.Append("<td class=""plainlabel"">" & ncd & "</td></tr>")
            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl844", "jobplanhtml.aspx.vb") & "</td>")
            sb.Append("<td  class=""plainlabel"">" & fu & "</td>")
            sb.Append("<td  class=""plainlabel"">" & fud & "</td></tr>")
            sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl845", "jobplanhtml.aspx.vb") & "</td>")
            sb.Append("<td  class=""plainlabel"">" & co & "</td>")
            sb.Append("<td  class=""plainlabel"">" & cod & "</td></tr></table></td></tr>")
        End While
        dr.Close()
        sb.Append("<tr><td>&nbsp;</td></tr>")
        sb.Append("<tr><td><Table cellSpacing=""0"" cellPadding=""2"" border=""0"" width=""620"">")
        sb.Append("<tr><td class=""label"" colspan=""4""><u>" & tmod.getxlbl("xlb340", "jobplanhtml.aspx.vb") & "</u></td></tr>")
        sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""560""></td></tr>")

        sql = "select t.tasknum, t.subtask, t.taskdesc, t.failuremode, t.skill, t.qty, t.ttime, " _
        + "l.output as lout, tl.output as tout, p.output as pout, m.measurement " _
        + "from pmjobtasks t " _
        + "left join jplubesout l on t.pmtskid = l.pmtskid " _
        + "left join jptoolsout tl on t.pmtskid = tl.pmtskid " _
        + "left join jppartsout p on t.pmtskid = p.pmtskid " _
        + "left join pmTaskMeasDetails m on m.pmtskid = t.pmtskid and m.jpid = '" & jpid & "' " _
        + "where t.jpid = '" & jpid & "' order by t.tasknum"

        Dim tnum, td, fm, sk, qt, st, lube, tool, part, stnum As String

        dr = pms.GetRdrData(sql)
        While dr.Read
            tnum = dr.Item("tasknum").ToString
            stnum = dr.Item("subtask").ToString
            td = dr.Item("taskdesc").ToString
            sk = dr.Item("skill").ToString
            qt = dr.Item("qty").ToString
            st = dr.Item("ttime").ToString
            fm = dr.Item("failuremode").ToString
            lube = dr.Item("lout").ToString
            tool = dr.Item("tout").ToString
            part = dr.Item("pout").ToString
            mea = dr.Item("measurement").ToString
            If stnum = "0" Then
                sb.Append("<tr><td class=""plainlabel"">[" & tnum & "]</td>")
                If Len(td) > 0 Then
                    sb.Append("<td colspan=""3"" class=""plainlabel"">" & td & "</td></tr>")
                Else
                    sb.Append("<td colspan=""3"" class=""plainlabel"">" & tmod.getlbl("cdlbl846", "jobplanhtml.aspx.vb") & "</td></tr>")
                End If
                If sk <> "" And sk <> "Select" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>" & tmod.getxlbl("xlb341", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & sk)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Qty:&nbsp;&nbsp;</b>" & qt)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Time:&nbsp;&nbsp;</b>" & st & "</td></tr>")
                End If
                If fm <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>" & tmod.getxlbl("xlb342", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & fm & "</td></tr>")
                End If
                If mea <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>Measurements:&nbsp;&nbsp;</b>" & mea & "</td></tr>")
                End If
                If lube <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>" & tmod.getxlbl("xlb343", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & lube & "</td></tr>")
                End If
                If tool <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>" & tmod.getxlbl("xlb344", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & tool & "</td></tr>")
                End If
                If part <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>" & tmod.getxlbl("xlb345", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & part & "</td></tr>")
                End If
                sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
            Else
                sb.Append("<tr><td class=""plainlabel"">[" & stnum & "]</td>")
                If Len(td) > 0 Then
                    sb.Append("<td></td><td colspan=""2"" class=""plainlabel"">" & td & "</td></tr>")
                Else
                    sb.Append("<td></td><td colspan=""2"" class=""plainlabel"">" & tmod.getlbl("cdlbl847", "jobplanhtml.aspx.vb") & "</td></tr>")
                End If
                If sk <> "" And sk <> "Select" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb346", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & sk)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Qty:&nbsp;&nbsp;</b>" & qt)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Time:&nbsp;&nbsp;</b>" & st & "</td></tr>")
                End If
                If fm <> "" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb347", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & fm & "</td></tr>")
                End If
                If lube <> "" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb348", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & lube & "</td></tr>")
                End If
                If tool <> "" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb349", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & tool & "</td></tr>")
                End If
                If part <> "" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb350", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & part & "</td></tr>")
                End If
                sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
            End If

        End While
        dr.Close()
        sb.Append("</table></td></tr></table>")
        tdwi.InnerHtml = sb.ToString
        'cmpToWord.TableToWord(tblwx, Response)
        'HttpContext.Current.Response.AddHeader("content-disposition", "inline; filename=ExportWord.doc")
        'HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=ExportWord.doc")

    End Sub
End Class
