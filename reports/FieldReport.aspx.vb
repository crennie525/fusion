

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class FieldReport
    Inherits System.Web.UI.Page
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim tasks As New Utilities
    Dim dr As SqlDataReader
    Dim eqid, rep1, rep2, rep3, rev1, rev2, asn, pmall, pdmall, title, mode, sall, spdmall, eqlev As String
    Dim pic, subhdr As String
    Dim sid, did, clid
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdpmall As System.Web.UI.HtmlControls.HtmlTableCell
    Dim tmod As New transmod
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then

            'eqid = "36"
            mode = "1"
            eqid = Request.QueryString("eqid").ToString
            'lbleqid.Value = eqid
            sid = Request.QueryString("sid").ToString
            'lblsid.Value = sid
            did = Request.QueryString("did").ToString
            'lbldid.Value = did
            clid = Request.QueryString("clid").ToString
            'lblclid.Value = clid
            tasks.Open()
            GetHead(eqid, mode, sid, did, clid)

            tasks.Dispose()
        End If
    End Sub
    Private Sub GetHead(ByVal eqid As String, ByVal mode As String, ByVal sid As String, ByVal did As String, ByVal clid As String)
        sql = "usp_getHeadloc '" & sid & "', '" & did & "', '" & clid & "','" & eqid & "'"
        dr = tasks.GetRdrData(sql)
        Dim str, str1, loc, dept, cell As String
        str = ""
        str1 = ""
        While dr.Read
            dept = dr.Item("dept").ToString
            cell = dr.Item("cell").ToString
            If dept <> "" Then
                str = dr.Item("site").ToString
                str += " > " & dept
                If cell <> "" Then
                    str += " > " & cell
                End If
            End If
           
            loc = dr.Item("loc").ToString
            If loc <> "" Then
                str1 = dr.Item("site").ToString
                str1 += " > " & loc & " - " & dr.Item("locd").ToString
            End If
        End While
        dr.Close()
        GetPMOut1(eqid, mode, str, str1)
    End Sub
    Private Sub GetPMOut1(ByVal eqid As String, ByVal mode As String, ByVal str As String, ByVal str1 As String)
        Dim fid As String = ""
        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim funcchk As String = ""
        Dim func As String = ""
        Dim compchk As String = ""
        Dim comp As String = ""
        Dim tskchk As String = ""
        Dim tsk As String = ""
        Dim subtask As String = ""
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620""  style=""page-break-after:always;"">")
        sb.Append("<tr><td width=""120""></td><td width=""160""></td><td width=""180""></td><td width=""160""></td></tr>")
        Dim tskcnt As Integer
        sql = "select count(*) from pmtasks where len(otaskdesc) <> 0 and otaskdesc is not null and eqid = '" & eqid & "'"
        tskcnt = tasks.Scalar(sql)
        If tskcnt = 0 Then
            sb.Append("<tr><td class=""bluelabel"" colspan=""4"">" & tmod.getlbl("cdlbl774" , "FieldReport.aspx.vb") & "</td></tr></table>")
            Response.Write(sb.ToString)
        Else
            If mode = "1" Then
                sql = "usp_getFieldOutput '" & eqid & "'"
            Else
                sql = "usp_getWITotalPGEMSPdM '" & eqid & "'"
            End If
            Dim parts, tools, lubes As String
            parts = ""
            tools = ""
            lubes = ""
            dr = tasks.GetRdrData(sql)
            'If dr.Read Then
            While dr.Read
                func = dr.Item("func").ToString
                'If func <> funcchk Then
                'funcchk = func


                'Else
                comp = dr.Item("compnum").ToString
                tsk = dr.Item("tasknum").ToString
                If tsk <> tskchk AndAlso Len(comp) <> 0 Then
                    tskchk = tsk
                    If str <> "" Then
                        sb.Append("<tr><td class=""label"" colspan=""4"">" & str & " > " & dr.Item("eqnum").ToString & "</td></tr>")
                    End If
                    If str1 <> "" Then
                        If str <> "" Then
                            sb.Append("<tr><td class=""label"" colspan=""4"">" & str1 & "</td></tr>")
                        Else
                            sb.Append("<tr><td class=""label"" colspan=""4"">" & str1 & " > " & dr.Item("eqnum").ToString & "</td></tr>")
                        End If
                    End If
                    'sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""bluelabel"">" & tmod.getlbl("cdlbl775" , "FieldReport.aspx.vb") & "</td><td class=""label"" colspan=""3"">" & func & "</td></tr>")
                    'sb.Append("<tr><td>&nbsp;</td></tr>")
                    'End If

                    'If comp <> compchk Then
                    'compchk = comp

                    sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl776" , "FieldReport.aspx.vb") & "</td><td class=""plainlabel"">" & tsk & "</td></tr>")
                    sb.Append("<tr><td></td><td class=""label"" bgcolor=""silver"" align=""center"">" & tmod.getlbl("cdlbl777" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""label"" bgcolor=""silver"" align=""center"">" & tmod.getlbl("cdlbl778" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""label"" bgcolor=""silver"" align=""center"">" & tmod.getlbl("cdlbl779" , "FieldReport.aspx.vb") & "</td></tr>")

                    If Len(comp) = 0 Then comp = "&nbsp;"
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl780" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft"">" & comp & "</td>")
                    sb.Append("<td class=""graycntr"">&nbsp;</td>")
                    sb.Append("<td class=""grayright"">&nbsp;</td></tr>")

                    sb.Append("<tr height=""20""><td class=""label"">Qty</td>")
                    sb.Append("<td class=""plainlabel grayleft"">" & dr.Item("cqty").ToString & "</td>")
                    sb.Append("<td class=""graycntr"">&nbsp;</td>")
                    sb.Append("<td class=""grayright"">&nbsp;</td></tr>")

                    Dim spl As String = dr.Item("spl").ToString
                    If Len(spl) = 0 Then spl = "&nbsp;"
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl782" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft"">" & spl & "</td>")
                    sb.Append("<td class=""graycntr"">&nbsp;</td>")
                    sb.Append("<td class=""grayright"">&nbsp;</td></tr>")

                    Dim desig As String = dr.Item("desig").ToString
                    If Len(desig) = 0 Then desig = "&nbsp;"
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl783" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft"">" & desig & "</td>")
                    sb.Append("<td class=""graycntr"">&nbsp;</td>")
                    sb.Append("<td class=""grayright"">&nbsp;</td></tr>")

                    Dim ofm As String = dr.Item("ofm1").ToString
                    If Len(ofm) = 0 Then ofm = "&nbsp;"
                    sb.Append("<tr height=""40""><td class=""label"">" & tmod.getlbl("cdlbl784" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft"">" & ofm & "</td>")
                    sb.Append("<td class=""graycntr"">&nbsp;</td>")
                    sb.Append("<td class=""grayright"">&nbsp;</td></tr>")

                    Dim tfm As String = dr.Item("failout").ToString
                    If Len(tfm) = 0 Then tfm = "&nbsp;"
                    sb.Append("<tr height=""40""><td class=""label"">" & tmod.getlbl("cdlbl785" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft"">" & tfm & "</td>")
                    sb.Append("<td class=""graycntr"">&nbsp;</td>")
                    sb.Append("<td class=""grayright"">&nbsp;</td></tr>")

                    sb.Append("<tr><td>&nbsp;</td></tr>")

                    Dim otask As String = dr.Item("otaskdesc").ToString
                    If Len(otask) = 0 Then otask = "&nbsp;"
                    sb.Append("<tr height=""120"" valign=""top""><td class=""label"">" & tmod.getlbl("cdlbl786" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft graytop"" valign=""top"">" & otask & "</td>")
                    sb.Append("<td class=""graycntr graytop"">&nbsp;</td>")
                    sb.Append("<td class=""grayright graytop"">&nbsp;</td></tr>")

                    Dim tt As String = dr.Item("origtasktype").ToString
                    If Len(tt) = 0 Then tt = "&nbsp;"
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl787" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft"">" & tt & "</td>")
                    sb.Append("<td class=""graycntr"">&nbsp;</td>")
                    sb.Append("<td class=""grayright"">&nbsp;</td></tr>")

                    Dim pdm As String = dr.Item("origpretech").ToString
                    If Len(pdm) = 0 Then pdm = "none"
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl788" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft"">" & pdm & "</td>")
                    sb.Append("<td class=""graycntr"">&nbsp;</td>")
                    sb.Append("<td class=""grayright"">&nbsp;</td></tr>")

                    Dim meas As String = dr.Item("meas").ToString
                    If Len(meas) = 0 Then meas = "none"
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl789" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft"">" & meas & "</td>")
                    sb.Append("<td class=""graycntr"">&nbsp;</td>")
                    sb.Append("<td class=""grayright"">&nbsp;</td></tr>")

                    sb.Append("<tr><td>&nbsp;</td></tr>")

                    Dim ofreq As String = dr.Item("origfreq").ToString
                    If Len(ofreq) = 0 Then ofreq = "&nbsp;"
                    sb.Append("<tr height=""20"" valign=""top""><td class=""label"">" & tmod.getlbl("cdlbl790" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft graytop"" valign=""top"">" & ofreq & "</td>")
                    sb.Append("<td class=""graycntrt graytop"">&nbsp;</td>")
                    sb.Append("<td class=""grayrightt graytop"">&nbsp;</td></tr>")

                    Dim oskill As String = dr.Item("origskill").ToString
                    If Len(oskill) = 0 Then oskill = "&nbsp;"
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl791" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft"">" & oskill & "</td>")
                    sb.Append("<td class=""graycntr"">&nbsp;</td>")
                    sb.Append("<td class=""grayright"">&nbsp;</td></tr>")

                    Dim oqty As String = dr.Item("origqty").ToString
                    If Len(oqty) = 0 Then oqty = "&nbsp;"
                    sb.Append("<tr height=""20""><td class=""label"">Qty</td>")
                    sb.Append("<td class=""plainlabel grayleft"">" & oqty & "</td>")
                    sb.Append("<td class=""graycntr"">&nbsp;</td>")
                    sb.Append("<td class=""grayright"">&nbsp;</td></tr>")

                    Dim omin As String = dr.Item("origttime").ToString
                    If Len(omin) = 0 Then omin = "&nbsp;"
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl793" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft"">" & omin & "</td>")
                    sb.Append("<td class=""graycntr"">&nbsp;</td>")
                    sb.Append("<td class=""grayright"">&nbsp;</td></tr>")

                    Dim odown As String = dr.Item("origrd").ToString
                    If Len(odown) = 0 Then odown = "&nbsp;"
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl794" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft"">" & odown & "</td>")
                    sb.Append("<td class=""graycntr plainlabel"" align=""center"">" & tmod.getxlbl("xlb333" , "FieldReport.aspx.vb") & "&nbsp;&nbsp;&nbsp;" & tmod.getxlbl("xlb334" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""grayright"">&nbsp;</td></tr>")

                    Dim otime As String = dr.Item("origrdt").ToString
                    If Len(otime) = 0 Then otime = "&nbsp;"
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl795" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft"">" & otime & "</td>")
                    sb.Append("<td class=""graycntr"">&nbsp;</td>")
                    sb.Append("<td class=""grayright"">&nbsp;</td></tr>")

                    sb.Append("<tr><td>&nbsp;</td></tr>")

                    Dim olube As String = dr.Item("lubes").ToString
                    If Len(olube) = 0 Then olube = "none"
                    sb.Append("<tr height=""20"" valign=""top""><td class=""label"">" & tmod.getlbl("cdlbl796" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft graytop"" valign=""top"">" & olube & "</td>")
                    sb.Append("<td class=""graycntrt graytop"">&nbsp;</td>")
                    sb.Append("<td class=""grayrightt graytop"">&nbsp;</td></tr>")

                    Dim opart As String = dr.Item("parts").ToString
                    If Len(opart) = 0 Then opart = "none"
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl797" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft"">" & opart & "</td>")
                    sb.Append("<td class=""graycntr"">&nbsp;</td>")
                    sb.Append("<td class=""grayright"">&nbsp;</td></tr>")

                    Dim otool As String = dr.Item("tools").ToString
                    If Len(otool) = 0 Then otool = "&nbsp;"
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl798" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel grayleft"">" & otool & "</td>")
                    sb.Append("<td class=""graycntr"">&nbsp;</td>")
                    sb.Append("<td class=""grayright"">&nbsp;</td></tr>")

                    sb.Append("<tr><td>&nbsp;</td></tr>")

                    sb.Append("<tr height=""30"" valign=""top""><td class=""label"">" & tmod.getlbl("cdlbl799" , "FieldReport.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel graycntrt graytop"" colspan=""3"">&nbsp;</td></tr>")

                    sb.Append("<tr height=""30"" valign=""top""><td class=""label"">&nbsp;</td>")
                    sb.Append("<td class=""plainlabel graycntr"" colspan=""3"">&nbsp;</td></tr>")

                    sb.Append("<tr height=""30"" valign=""top""><td class=""label"">&nbsp;</td>")
                    sb.Append("<td class=""plainlabel graycntr"" colspan=""3"">&nbsp;</td></tr>")

                    sb.Append("<tr height=""30"" valign=""top""><td class=""label"">&nbsp;</td>")
                    sb.Append("<td class=""plainlabel graycntr"" colspan=""3"">&nbsp;</td></tr>")

                    sb.Append("<tr height=""30"" valign=""top""><td class=""label"">&nbsp;</td>")
                    sb.Append("<td class=""plainlabel graycntr"" colspan=""3"">&nbsp;</td></tr>")

                    sb.Append("<tr><td>&nbsp;</td></tr>")

                End If
                'End If
            End While
            'Else
            'sb.Append("<tr><td class=""bluelabel"" colspan=""4"">" & tmod.getlbl("cdlbl800" , "FieldReport.aspx.vb") & "</td></tr>")
            'End If

            sb.Append("</table>")
            'tdpmall.InnerHtml = sb.ToString
            Response.Write(sb.ToString)

        End If



    End Sub
End Class
