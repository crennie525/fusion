<%@ Page Language="vb" AutoEventWireup="false" Codebehind="compnotasks.aspx.vb" Inherits="lucy_r12.compnotasks" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>compnotasks</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<LINK href="../styles/reports.css" type="text/css" rel="stylesheet">
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="javascript" type="text/javascript">
		function gettask(fid, coid, comp, aid) {
		//document.getElementById("lbltasknum").value = task;
		    //handleexit(task)
		    var conf = confirm("Are you sure you want to add a task to this Component?")
		    if (conf == true) {
		        var fuid = document.getElementById("lblfuid").value
		        cid = document.getElementById("lblcid").value;
		        sid = document.getElementById("lblsid").value;
		        did = document.getElementById("lbldid").value;
		        clid = document.getElementById("lblclid").value;
		        eqid = document.getElementById("lbleqid").value;
		        chk = document.getElementById("lblchk").value;
		        var eReturn = window.showModalDialog("../complib/complibtasks2dialog.aspx?typ=tasks&coid=" + coid + "&comp=" + comp + "&fuid=" + fid + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:780px;resizable=yes");
		        if (eReturn) {
		            //reload report
		            window.location = "compnotasks.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date();
		        }
		        else {
		            //reload report
		            window.location = "compnotasks.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date();
		        }
		    }
		    else {
		        alert("Action Cancelled")
		    }
		}
		function handleexit(task) {
		window.returnValue = task;
		window.close();
		}
		</script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table width="816">
				<tr>
					<td class="bigbold" id="tdpm" width="816" runat="server" colspan="4" align="center"></td>
				</tr>
				<tr>
					<td width="93">&nbsp;</td>
					<td class="bigplain" id="tdass" runat="server">Asset:</td>
					<td class="bigplain" id="tdeq" runat="server"></td>
					<td width="93" align="right"></td>
				</tr>
				<tr>
					<td></td>
					<td colSpan="2">
						<hr SIZE="1">
					</td>
					<TD></TD>
				</tr>
			</table>
			<TABLE width="816">
				<TR>
					<td width="93">&nbsp;</td>
					<TD class="label" width="624" bgColor="silver" id="tdcwt" runat="server">Components Without Tasks</TD>
					<td width="93">&nbsp;</td>
				</TR>
				<tr>
					<td></td>
					<td id="tdwi" runat="server"></td>
					<td></td>
				</tr>
			</TABLE>
			<input type="hidden" id="lbltasklev" runat="server" NAME="lbltasklev"> <input type="hidden" id="lblsid" runat="server" NAME="lblsid">
			<input type="hidden" id="lbldid" runat="server" NAME="lbldid"> <input type="hidden" id="lblclid" runat="server" NAME="lblclid">
			<input type="hidden" id="lblchk" runat="server" NAME="lblchk"> <input type="hidden" id="lbltasknum" runat="server" NAME="lbltasknum">
			<input type="hidden" id="lblflag" runat="server" NAME="lblflag"><input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lblfuid" type="hidden" runat="server" NAME="lblfuid"> <input type="hidden" id="lblcid" runat="server" NAME="lblcid">
            <input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
