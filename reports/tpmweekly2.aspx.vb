

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class tpmweekly2
    Inherits System.Web.UI.Page
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim eqid As String
    Dim tpmw As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim tmod As New transmod
    Dim tst As Integer = 0
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            tpmw.Open()
            GetWeek(eqid)
            tpmw.Dispose()
        End If
    End Sub
    Private Sub GetWeek(ByVal eqid As String)
        Dim cb1mon, cb1tue, cb1wed, cb1thu, cb1fri, cb1sat, cb1sun As String
        Dim cb2mon, cb2tue, cb2wed, cb2thu, cb2fri, cb2sat, cb2sun As String
        Dim cb3mon, cb3tue, cb3wed, cb3thu, cb3fri, cb3sat, cb3sun As String
        'cb1mon as string, cb1tue as string, cb1wed as string, cb1thu as string, cb1fri as string, cb1sat as string, cb1sun as string, 
        'cb2mon as string, cb2tue as string, cb2wed as string, cb2thu as string, cb2fri as string, cb2sat as string, cb2sun as string, 
        'cb3mon as string, cb3tue as string, cb3wed as string, cb3thu as string, cb3fri as string, cb3sat as string, cb3sun as string
        Dim lubes, parts, tools, meas, fm1 As String
        Dim eqnum, eqdesc, funcid, func, comid, compnum, compdesc, spl, desig, freq, pics As String
        Dim pmtskid, tasknum, subtask, taskdesc As String
        Dim subcnt As Integer
        Dim subchk As Integer = 0
        Dim taskhold As Integer = 1
        Dim estart As Integer = 0
        Dim tstart As Integer = 0
        Dim pstart1 As Integer = 0
        Dim wkstr As String

        Dim freqhold, funchold, comhold As String
        freqhold = ""
        funchold = ""
        comhold = ""

        Dim sb As New StringBuilder
        sb.Append("<table style=""page-break-after:always;"" border=""0"">")
        sb.Append("<tr>")
        sb.Append("<td colspan=""2"">")

        Dim dt As DataTable = New DataTable("tbllist")
        Dim dtcol As DataColumn
        Dim dtrow As DataRow

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "task"
        dt.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "pics"
        dt.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "freq"
        dt.Columns.Add(dtcol)

        sql = "usp_getWITotalTPMEL_rep1 '" & eqid & "'"

        dr = tpmw.GetRdrData(sql)
        While dr.Read
            tstart = 1
            freq = dr.Item("freq").ToString

            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqnum").ToString
            funcid = dr.Item("funcid").ToString
            func = dr.Item("func").ToString
            comid = dr.Item("comid").ToString
            compnum = dr.Item("compnum").ToString
            compdesc = dr.Item("compdesc").ToString
            spl = dr.Item("spl").ToString
            desig = dr.Item("desig").ToString
            funcid = dr.Item("funcid").ToString
            func = dr.Item("func").ToString

            pmtskid = dr.Item("pmtskid").ToString
            tasknum = dr.Item("tasknum").ToString
            subtask = dr.Item("subtask").ToString

            pics = dr.Item("pics").ToString


            If subtask = "0" Then
                tst += 1
            End If


            If subtask = "0" Then
                If freq <> freqhold Then
                    freqhold = freq
                    taskhold = 1
                Else
                    taskhold += 1
                End If
                subcnt = dr.Item("subcnt").ToString
                If subcnt = 0 Then
                    subchk = 0
                Else
                    subchk = subcnt
                End If
                If pics <> "" Then
                    pstart1 = 1
                    Dim picarr() As String = pics.Split(",")
                    Dim i As Integer
                    For i = 0 To picarr.Length - 1
                        dtrow = dt.NewRow()
                        dtrow("task") = taskhold
                        dtrow("pics") = picarr(i)
                        dtrow("freq") = freq
                        dt.Rows.Add(dtrow)
                    Next


                End If
                cb1mon = dr.Item("cb1mon").ToString
                cb1tue = dr.Item("cb1tue").ToString
                cb1wed = dr.Item("cb1wed").ToString
                cb1thu = dr.Item("cb1thu").ToString
                cb1fri = dr.Item("cb1fri").ToString
                cb1sat = dr.Item("cb1sat").ToString
                cb1sun = dr.Item("cb1sun").ToString

                cb2mon = dr.Item("cb2mon").ToString
                cb2tue = dr.Item("cb2tue").ToString
                cb2wed = dr.Item("cb2wed").ToString
                cb2thu = dr.Item("cb2thu").ToString
                cb2fri = dr.Item("cb2fri").ToString
                cb2sat = dr.Item("cb2sat").ToString
                cb2sun = dr.Item("cb2sun").ToString

                cb3mon = dr.Item("cb3mon").ToString
                cb3tue = dr.Item("cb3tue").ToString
                cb3wed = dr.Item("cb3wed").ToString
                cb3thu = dr.Item("cb3thu").ToString
                cb3fri = dr.Item("cb3fri").ToString
                cb3sat = dr.Item("cb3sat").ToString
                cb3sun = dr.Item("cb3sun").ToString
            End If

            taskdesc = dr.Item("taskdesc").ToString

            lubes = dr.Item("lubes").ToString
            parts = dr.Item("parts").ToString
            tools = dr.Item("tools").ToString
            meas = dr.Item("meas").ToString
            fm1 = dr.Item("fm1").ToString

            ' Task Details

            If subtask = "0" Then

                sb.Append("<table border=""0"" width=""800"" cellspacing=""0"" cellpadding=""3"" >") 'main table start
                If estart = 0 Then
                    estart = 1

                    sb.Append("<tr>")
                    sb.Append("<td colspan=""2"">")
                    sb.Append("<table width=""800"" border=""0"">") 'hdr table start

                    sb.Append("<tr>")
                    sb.Append("<td align=""center"" class=""replabeltop"" colspan=""2"">" & tmod.getlbl("cdlbl1230", "tpmweekly2.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("</tr>")

                    sb.Append("<tr>")
                    sb.Append("<td class=""replabeltop"" width=""100"">" & tmod.getlbl("cdlbl1231", "tpmweekly2.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabeltopplain"" width=""700"">" & eqnum)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabeltop"">" & tmod.getlbl("cdlbl1232", "tpmweekly2.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabeltopplain"">" & eqdesc)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                    sb.Append("</table>") 'hdr table end
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If
                If taskhold = 1 Then
                    sb.Append("<tr>")
                    sb.Append("<td align=""left"" class=""replabel"" colspan=""2"">" & freq)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If
                sb.Append("<tr>")
                sb.Append("<td width=""610"" style=""border-top: solid 1px gray;"" valign=""top"">") 'Info Tab

                sb.Append("<table cellspacing=""3"" cellpadding=""3"" width=""610"" border=""0"">") 'Info Table Start

                sb.Append("<tr>")
                sb.Append("<td class=""replabel"" width=""100"" >" & tmod.getlbl("cdlbl1233", "tpmweekly2.aspx.vb"))
                sb.Append("</td>")
                sb.Append("<td class=""replabelplain"" width=""510"">")
                sb.Append(taskhold)
                sb.Append("</td>")
                sb.Append("</tr>")

                sb.Append("<tr>")
                sb.Append("<td class=""replabel"">" & tmod.getlbl("cdlbl1234", "tpmweekly2.aspx.vb"))
                sb.Append("</td>")
                sb.Append("<td class=""replabelplain"">")
                sb.Append(func)
                sb.Append("</td>")
                sb.Append("</tr>")

                sb.Append("<tr>")
                sb.Append("<td class=""replabel"">" & tmod.getlbl("cdlbl1235", "tpmweekly2.aspx.vb"))
                sb.Append("</td>")
                sb.Append("<td class=""replabelplain"">")
                sb.Append(compnum)
                sb.Append("</td>")
                sb.Append("</tr>")

                If compdesc <> "" Then
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">" & tmod.getxlbl("xlb451", "tpmweekly2.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(compdesc)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                If spl <> "" Then
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">SPL:")
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(spl)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                If desig <> "" Then
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">" & tmod.getlbl("cdlbl1236", "tpmweekly2.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(desig)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                'sb.Append("<tr>")
                'sb.Append("<td>HELLO Info")
                'sb.Append("</td>")
                'sb.Append("</tr>")

                sb.Append("</table>") 'End Info Table
                sb.Append("</td>") 'End Info Tab

                ' Week Days
                sb.Append("<td width=""190"" valign=""top"" rowspan=""1""  style=""border-top: solid 1px gray;"">")

                wkstr = GetWeekTable(cb1mon, cb1tue, cb1wed, cb1thu, cb1fri, cb1sat, cb1sun, cb2mon, cb2tue, _
                cb2wed, cb2thu, cb2fri, cb2sat, cb2sun, cb3mon, cb3tue, cb3wed, cb3thu, cb3fri, cb3sat, cb3sun)

                sb.Append(wkstr)

                sb.Append("</td>")
                ' End Week Days

                sb.Append("</tr>")

                sb.Append("</table>")
            End If


            ' Task Guts Row
            If subtask = "0" Then
                sb.Append("<tr>")
                sb.Append("<td colspan=""2"">")
                sb.Append("<table width=""800"">") 'Task Guts Table Start

                sb.Append("<tr>")
                sb.Append("<td class=""replabel"" colspan=""2"">" & tmod.getlbl("cdlbl1237", "tpmweekly2.aspx.vb"))
                sb.Append("</td>")
                sb.Append("</tr>")
                sb.Append("<tr>")
                sb.Append("<td class=""replabelplain"" colspan=""2"">")
                sb.Append(taskdesc)
                sb.Append("</td>")
                sb.Append("</tr>")

                sb.Append("<tr>")
                sb.Append("<td class=""replabel"" width=""120"">" & tmod.getlbl("cdlbl1238", "tpmweekly2.aspx.vb"))
                sb.Append("</td>")
                sb.Append("<td class=""replabelplain"" width=""490"">")
                sb.Append(fm1)
                sb.Append("</td>")
                sb.Append("</tr>")

                If lubes <> "" And meas <> "none" Then  'And lubes <> "None"
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">" & tmod.getlbl("cdlbl1239", "tpmweekly2.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(lubes)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                If tools <> "" And meas <> "none" Then  'And tools <> "None"
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">" & tmod.getlbl("cdlbl1240", "tpmweekly2.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(tools)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                If parts <> "" And meas <> "none" Then  'And parts <> "None"
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">" & tmod.getlbl("cdlbl1241", "tpmweekly2.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(parts)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                If meas <> "" And meas <> "none" Then  'And meas <> "None"
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">" & tmod.getlbl("cdlbl1242", "tpmweekly2.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(meas)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If


            Else
                ' Sub Task Details

                sb.Append("<tr>")
                sb.Append("<td class=""replabelplain"">&nbsp;&nbsp;<b>" & tmod.getxlbl("xlb452", "tpmweekly2.aspx.vb") & "</b> [" & subtask & "]:")
                sb.Append("</td>")
                sb.Append("<td class=""replabelplain"">")
                sb.Append(taskdesc)
                sb.Append("</td>")
                sb.Append("</tr>")

                If lubes <> "" And meas <> "none" Then  'And lubes <> "None"
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">&nbsp;&nbsp;" & tmod.getxlbl("xlb453", "tpmweekly2.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(lubes)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                If tools <> "" And meas <> "none" Then  'And tools <> "None"
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">&nbsp;&nbsp;" & tmod.getxlbl("xlb454", "tpmweekly2.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(tools)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                If parts <> "" And meas <> "none" Then   'And parts <> "None"
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">&nbsp;&nbsp;" & tmod.getxlbl("xlb455", "tpmweekly2.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(parts)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                If meas <> "" And meas <> "none" Then  '
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">&nbsp;&nbsp;" & tmod.getxlbl("xlb456", "tpmweekly2.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(meas)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If


            End If

            If subcnt = 0 Then  'subtask = "0" And
                sb.Append("<tr>")
                sb.Append("<td>&nbsp;")
                sb.Append("</td>")
                sb.Append("</tr>")

                'sb.Append("<tr>")
                'sb.Append("<td>HELLO Guts " & tst)
                'sb.Append("</td>")
                'sb.Append("</tr>")

                sb.Append("</table>") 'End Task Guts Table

                'main end was here
            Else
                subcnt = subcnt - 1
            End If

        End While
        dr.Close()

        sb.Append("<tr>")
        sb.Append("<td>&nbsp;")
        sb.Append("</td>")
        sb.Append("</tr>")

        'sb.Append("<tr>")
        'sb.Append("<td>HELLO Main")
        'sb.Append("</td>")
        'sb.Append("</tr>")

        sb.Append("</table>") 'Main Table End

        sb.Append("</td>")
        sb.Append("</tr>")

        'sb.Append("<tr>")
        'sb.Append("<td>HELLO Bot")
        'sb.Append("</td>")
        'sb.Append("</tr>")

        sb.Append("</table>") ' Big Table End

        'test
        'sb.Append("</td>")
        'sb.Append("</tr>")

        'sb.Append("<tr>")
        'sb.Append("<td>HELLO Bot1")
        'sb.Append("</td>")
        'sb.Append("</tr>")

        'sb.Append("</table>")

        If tstart = 1 Then
            If pstart1 <> 0 Then
                sb.Append("<table width=""800"" border=""0"">")
                sb.Append("<tr>")
                sb.Append("<td align=""center"" class=""replabeltop"">" & tmod.getlbl("cdlbl1243", "tpmweekly2.aspx.vb"))
                sb.Append("</td>")
                sb.Append("</tr>")
                sb.Append("<tr>")
                sb.Append("<td>&nbsp;")
                sb.Append("</td>")
                sb.Append("</tr>")

                Dim ds As New DataSet
                ds.Tables.Add(dt)
                Dim task, pic, pfreq As String
                Dim holdtask As String = "0"
                Dim pfreqhold As String = "0"
                Dim taskflag As Integer = 0
                Dim pstart As Integer = 0
                Dim drM As DataRow
                For Each drM In dt.Select()
                    task = drM("task").ToString
                    pfreq = drM("freq").ToString
                    If task <> holdtask Then
                        holdtask = task
                        pfreqhold = pfreq
                        taskflag = 0
                    Else
                        If pfreq <> pfreqhold Then
                            taskflag = 0
                        Else
                            taskflag = 1
                        End If

                    End If
                    pic = drM("pics").ToString

                    If taskflag = 0 Then
                        If pstart = 0 Then
                            pstart = 1
                        Else
                            sb.Append("</td>")
                            sb.Append("</tr>")
                        End If
                        sb.Append("<tr>")
                        sb.Append("<td align=""center"" class=""replabel"" width=""800"">" & pfreq)
                        sb.Append("</td>")
                        sb.Append("</tr>")
                        sb.Append("<tr>")
                        sb.Append("<td align=""center"" class=""replabel"" width=""800"">" & tmod.getxlbl("xlb457", "tpmweekly2.aspx.vb") & " " & task)
                        sb.Append("</td>")
                        sb.Append("</tr>")



                        sb.Append("<tr>")
                        sb.Append("<td align=""center"" width=""800"">")
                        sb.Append("<img src=""" & pic & """></td></tr>")
                    End If
                    'sb.Append("<img src=""" & pic & """></td></tr>")
                Next
                'sb.Append("</td>")
                'sb.Append("</tr>")

                sb.Append("</table>")
            End If
            Dim tst2 As String = tst
            Response.Write(sb.ToString)
        Else
            'sb.Append("<table width=""800"">")
            'sb.Append("<tr>")
            'sb.Append("<td class=""replabeltop"" align=""center"">")
            sb.Append("<b>" & tmod.getxlbl("xlb458", "tpmweekly2.aspx.vb") & "</b>")
            sb.Append("</td>")
            sb.Append("</tr>")
            sb.Append("</table>")
            Response.Write(sb.ToString)
        End If


    End Sub
    Private Function GetWeekTable(ByVal cb1mon As String, ByVal cb1tue As String, ByVal cb1wed As String, ByVal cb1thu As String, _
    ByVal cb1fri As String, ByVal cb1sat As String, ByVal cb1sun As String, ByVal cb2mon As String, ByVal cb2tue As String, _
    ByVal cb2wed As String, ByVal cb2thu As String, ByVal cb2fri As String, ByVal cb2sat As String, ByVal cb2sun As String, _
    ByVal cb3mon As String, ByVal cb3tue As String, ByVal cb3wed As String, ByVal cb3thu As String, ByVal cb3fri As String, _
    ByVal cb3sat As String, ByVal cb3sun As String) As String

        Dim sb As New StringBuilder
        Dim bclass As String
        Dim ret As String

        sb.Append("<table border=""0"">")
        sb.Append("<tr height=""20"">")
        sb.Append("<td>&nbsp;</td>")
        sb.Append("<td class=""replabel"" width=""20"" align=""center"">M</td>")
        sb.Append("<td class=""replabel"" width=""20"" align=""center"">Tu</td>")
        sb.Append("<td class=""replabel"" width=""20"" align=""center"">W</td>")
        sb.Append("<td class=""replabel"" width=""20"" align=""center"">Th</td>")
        sb.Append("<td class=""replabel"" width=""20"" align=""center"">F</td>")
        sb.Append("<td class=""replabel"" width=""20"" align=""center"">Sa</td>")
        sb.Append("<td class=""replabel"" width=""20"" align=""center"">Su</td>")
        sb.Append("</tr>")
        sb.Append("<tr height=""20"">")
        sb.Append("<td class=""replabel"" width=""20"" align=""center"">1st</td>")
        If cb1mon = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb1tue = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb1wed = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb1thu = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb1fri = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb1sat = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb1sun = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        sb.Append("</tr>")
        sb.Append("<tr height=""20"">")
        sb.Append("<td class=""replabel"" width=""20"" align=""center"">2nd</td>")
        If cb2mon = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb2tue = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb2wed = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb2thu = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb2fri = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb2sat = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb2sun = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        sb.Append("</tr>")
        sb.Append("<tr height=""20"">")
        sb.Append("<td class=""replabel"" width=""20"" align=""center"">3rd</td>")
        If cb3mon = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb3tue = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb3wed = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb3thu = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb3fri = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb3sat = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        If cb3sun = "1" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        sb.Append("</tr>")
        sb.Append("</table>")

        ret = sb.ToString
        Return ret
    End Function

End Class
