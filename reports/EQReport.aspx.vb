

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class EQReport
    Inherits System.Web.UI.Page
	Protected WithEvents lang3325 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3324 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3323 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3322 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3321 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3320 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3319 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3318 As System.Web.UI.WebControls.Label

    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim main As New Utilities
    Dim chk, Login As String
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim eqid, sid, did, clid, mode As String
    Protected WithEvents tdsite As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tddept As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblfid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdcell As System.Web.UI.HtmlControls.HtmlTableCell
    Dim tmod As New transmod
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdarch As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        If comi = "SUN" Then
            lblfslang.Value = "fre"
        End If
        'END ADD FOR SUNCOR
'Put user code to initialize the page here
        'Try
        'Login = HttpContext.Current.Session("Logged_IN").ToString()
        'Catch ex As Exception
        'Exit Sub
        'End Try
        If Not IsPostBack Then
            'If Request.QueryString("start").ToString = "no" Then
            'tdarch.InnerHtml = "Waiting for Location..."
            'Else
            'lbleqid.Value = "46"
            'lbldid.Value = "24" 'Request.QueryString("did").ToString
            'lblclid.Value = "11" 'Request.QueryString("clid").ToString
            mode = "1"
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            did = Request.QueryString("did").ToString
            lbldid.Value = did
            clid = Request.QueryString("clid").ToString
            lblclid.Value = clid
            main.Open()
            'GetArch()
            GetHead(eqid, mode, sid, did, clid)
            main.Dispose()
            'End If
            'Else
            'lbldid.Value = Request.QueryString("did").ToString
            'lblclid.Value = Request.QueryString("clid").ToString
            'lblchk.Value = Request.QueryString("chk").ToString
            'main.Open()
            'GetArch()
            'main.Dispose()
        End If
    End Sub
    Private Sub GetHead(ByVal eqid As String, ByVal mode As String, ByVal sid As String, ByVal did As String, ByVal clid As String)
        sql = "usp_getHead '" & sid & "', '" & did & "', '" & clid & "'"
        dr = main.GetRdrData(sql)
        Dim str As String
        While dr.Read
            tdsite.InnerHtml = dr.Item("site").ToString
            tddept.InnerHtml = dr.Item("dept").ToString
            tdcell.InnerHtml = dr.Item("cell").ToString
        End While
        dr.Close()
        GetArch()
    End Sub
    Private Function GetRandom(ByVal Min As Integer, ByVal Max As Integer) As Integer
        ' by making Generator static, we preserve the same instance ' 
        ' (i.e., do not create new instances with the same seed over and over) ' 
        ' between calls ' 
        Static Generator As System.Random = New System.Random()
        Return Generator.Next(Min, Max)
    End Function
    Private Sub GetArch()
        '
        Dim sb As StringBuilder = New StringBuilder
        Dim dept As String = lbldid.Value '"24"
        Dim cell As String = lblclid.Value '"11"
        Dim eqnum As String = "" '"eqcopytest"
        Dim eqid As String = lbleqid.Value
        Dim eqdesc As String
        sb.Append("<table cellspacing=""0"" width=""425"" table border=""0""><tr>")
        sb.Append("<td width=""15""></td>")
        sb.Append("<td width=""15""></td>")
        sb.Append("<td width=""15""></td>")
        sb.Append("<td width=""15""></td>")
        sb.Append("<td width=""365""></td></tr>" & vbCrLf)
        chk = lblchk.Value

        sql = "usp_getEQHierarchy '" & eqid & "'"

        Dim sid, did, clid As String
        Dim func, comp, fail, compd, failhold As String
        'h.Open()
        dr = main.GetRdrData(sql)
        Dim locby As String
        Dim lock As String = "0"
        Dim fid As String = "0"
        Dim cid As String = "0"
        Dim eid As String = "0"
        Dim cidhold As Integer = 0
        Dim cnt As Integer = 0
        Dim ccnt As Integer = 0
        Dim cmfid As String
        Dim cmf As String
        Dim fcnt As Integer = 0
        Dim fflag As String = "0"
        Dim cflag As String = "0"
        Dim ifid As String = "0"
        Dim icid As String = "0"
        lblfid.Value = ""
        lblcid.Value = ""
        While dr.Read
            If dr.Item("eqid").ToString <> eid Then
                eid = dr.Item("eqid").ToString
                eqnum = dr.Item("eqnum").ToString
                eqdesc = dr.Item("eqdesc").ToString
                sb.Append("<tr><td><img id='i" + eid + "' ")
                sb.Append("onclick=""fclose('t" + eid + "', 'i" + eid + "');""")
                sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>")
                sb.Append("<td colspan=""4"" class=""plainlabel""><b>" & tmod.getxlbl("xlb324" , "EQReport.aspx.vb") & "</b>&nbsp;&nbsp;" & eqnum & " - " & eqdesc)
                sb.Append("</td></tr>" & vbCrLf)

                sb.Append("<tr><td colspan=""5""><table class=""view"" width=""415"" cellspacing=""0"" id='t" + eid + "' table border=""0"">")
                sb.Append("<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan=""3"" class=""label""><u>" & tmod.getxlbl("xlb325" , "EQReport.aspx.vb") & "</u></td></tr>")
                sb.Append("<tr><td width=""15""></td><td width=""15""></td><td width=""15""></td><td width=""15""></td><td width=""355""></td></tr>")
            End If


            If dr.Item("func_id").ToString <> fid Then
                If fid <> 0 Then
                    sb.Append("</td></tr></table></td></tr>")
                End If
                fid = dr.Item("func_id").ToString
                
                'lblfid.Value += fid & "/"
                func = dr.Item("func").ToString
                ccnt = dr.Item("cnt").ToString
                'If fid = eid Then
                'ifid = fid & "999"
                'Else
                'ifid = fid
                'End If
                ifid = ifid & GetRandom(111, 1111)
                lblfid.Value += ifid & "/"
                If ccnt <> 0 Then
                    sb.Append("<tr><td>&nbsp;</td><td><img id='i" + ifid + "' ")
                    sb.Append("onclick=""fclose('t" + ifid + "', 'i" + ifid + "');""")
                    sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel"">" & func)
                    sb.Append("</td></tr>" & vbCrLf)
                Else
                    sb.Append("<tr><td>&nbsp;</td><td><img id='in" + ifid + "' ")
                    sb.Append("onclick=""fclose('t" + ifid + "', 'in" + ifid + "','no');""")
                    sb.Append(" src=""../images/appbuttons/bgbuttons/minusg1.gif""></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel"">" & func)
                    sb.Append("</td></tr>" & vbCrLf)
                End If
                'If ccnt <> 0 Then
                Dim tst As String = ifid
                Dim tst2 As String = ifid
                sb.Append("<tr><td colspan=""5""><table class=""details"" width=""405"" cellspacing=""0"" id='t" + ifid + "' border=""0"">")
                sb.Append("<tr><td colspan=""3"">&nbsp;</td><td colspan=""2"" class=""label""><u>" & tmod.getxlbl("xlb326", "EQReport.aspx.vb") & "</u></td></tr>")
                sb.Append("<tr><td width=""15""></td><td width=""15""></td><td width=""15""></td><td width=""15""></td><td width=""345""></td></tr>")
                'End If
            End If
            ' 

            If dr.Item("comid").ToString <> cid Then
                cid = dr.Item("comid").ToString

                'lblcid.Value += cid & "/"
                comp = dr.Item("compnum").ToString
                compd = dr.Item("compdesc").ToString
                'If cid = eid Or cid = fid Then
                'icid = cid & "888"
                'Else
                'icid = cid
                'End If
                icid = icid & GetRandom(111, 1111)
                lblcid.Value += icid & "/"
                If cid <> "0" Then
                    cidhold = dr.Item("cntfm").ToString
                    If cidhold <> 0 Then
                        sb.Append("<tr><td colspan=""5""><table  width=""405"" cellspacing=""0"" table border=""0"">" & vbCrLf)
                        sb.Append("<tr><td width=""15""></td><td width=""15""></td><td width=""15""></td><td width=""15""></td><td width=""345""></td></tr>")
                        sb.Append("<tr><td colspan=""2"">&nbsp;</td><td><img id='i" + icid + "' onclick=""fclose('t" + icid + "', 'i" + icid + "');"" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        sb.Append("<td class=""plainlabel"" colspan=""2"">" & comp)
                    Else
                        sb.Append("<tr><td colspan=""5""><table  width=""405"" cellspacing=""0"" table border=""0"">" & vbCrLf)
                        sb.Append("<tr><td width=""15""></td><td width=""15""></td><td width=""15""></td><td width=""15""></td><td width=""345""></td></tr>")
                        sb.Append("<tr><td colspan=""2"">&nbsp;</td><td><img id='in" + icid + "' onclick=""fclose('t" + icid + "', 'in" + icid + "','no');"" src=""../images/appbuttons/bgbuttons/minusg1.gif""></td>" & vbCrLf)
                        sb.Append("<td class=""plainlabel"" colspan=""2"">" & comp)
                    End If

                    If Len(compd) <> 0 Then
                        sb.Append(" - " & compd & "</td></tr></table></td></tr>" & vbCrLf)
                    Else
                        sb.Append("</td></tr></table></td></tr>" & vbCrLf)
                    End If
                    If dr.Item("compfailid").ToString <> cmfid Then
                        cmfid = dr.Item("compfailid").ToString
                        fail = dr.Item("failuremode").ToString
                        failhold = fail
                        If cmfid <> "0" Then
                            If cnt = 0 Then
                                cnt = cnt + 1
                                sb.Append("<tr><td colspan=""5""><table border=""0"" class=""details"" width=""395"" cellspacing=""0"" id=""t" + icid + """>" & vbCrLf)
                                sb.Append("<tr><td width=""15""></td><td width=""15""></td><td width=""15""></td><td width=""15""></td><td width=""335""></td></tr>")
                                sb.Append("<tr><td colspan=""4"">&nbsp;</td><td class=""label""><u>" & tmod.getxlbl("xlb327", "EQReport.aspx.vb") & "</u></td></tr>")
                                sb.Append("<tr><td colspan=""4"">&nbsp;</td><td class=""plainlabel"">" & fail & "</td></tr>" & vbCrLf)
                                If cnt = cidhold Then
                                    cnt = 0
                                    sb.Append("</table></td></tr>")
                                End If
                            End If
                        Else
                            'If cnt = cidhold Then
                            'cnt = 0
                            'sb.Append("</table></td></tr>")
                        End If
                        'End If
                        'Else
                        'sb.Append("</table></td></tr>")
                    End If
                Else
                    cid = "0"
                End If

            ElseIf dr.Item("comid").ToString = cid Then
                'If cid = eid Or cid = fid Then
                'icid = cid & "888"
                'Else
                'icid = cid
                'End If
                '***added if cid <> "0" 11/09/11
                If cid <> "0" Then
                    If cnt = cidhold Then
                        cnt = 0
                        sb.Append("</table></td></tr>")
                    Else
                        fail = dr.Item("failuremode").ToString
                        If fail <> failhold Then
                            If cid <> "0" Then
                                If cnt = 0 Then
                                    cnt = cnt + 1
                                    sb.Append("<tr><td colspan=""5""><table border=""0"" class=""details"" width=""395"" cellspacing=""0"" id=""t" + icid + """>" & vbCrLf)
                                    sb.Append("<tr><td width=""15""></td><td width=""15""></td><td width=""15""></td><td width=""15""></td><td width=""335""></td></tr>")
                                    sb.Append("<tr><td colspan=""4"">&nbsp;</td><td class=""label""><u>" & tmod.getxlbl("xlb328", "EQReport.aspx.vb") & "</u></td></tr>")
                                    sb.Append("<tr><td colspan=""4"">&nbsp;</td><td class=""plainlabel"">" & fail & "</td></tr>" & vbCrLf)
                                Else
                                    cnt = cnt + 1
                                    sb.Append("<tr><td colspan=""4"">&nbsp;</td><td class=""plainlabel"">" & fail & "</td></tr>" & vbCrLf)
                                End If
                                If cnt = cidhold Then
                                    cnt = 0
                                    sb.Append("</table></td></tr>")
                                End If
                            Else
                                cid = "0"
                                'cnt = 0
                                'sb.Append("</table></td></tr>")
                            End If

                        End If

                    End If
                End If
                
                'Else
                'sb.Append("</table></td></tr>")
            End If

        End While
        dr.Close()
        'h.Dispose()
        sb.Append("</td></tr></table></td></tr></table>")
        'Response.Write(sb.ToString)
        tdarch.InnerHtml = sb.ToString
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3318.Text = axlabs.GetASPXPage("EQReport.aspx", "lang3318")
        Catch ex As Exception
        End Try
        Try
            lang3319.Text = axlabs.GetASPXPage("EQReport.aspx", "lang3319")
        Catch ex As Exception
        End Try
        Try
            lang3320.Text = axlabs.GetASPXPage("EQReport.aspx", "lang3320")
        Catch ex As Exception
        End Try
        Try
            lang3321.Text = axlabs.GetASPXPage("EQReport.aspx", "lang3321")
        Catch ex As Exception
        End Try
        Try
            lang3322.Text = axlabs.GetASPXPage("EQReport.aspx", "lang3322")
        Catch ex As Exception
        End Try
        Try
            lang3323.Text = axlabs.GetASPXPage("EQReport.aspx", "lang3323")
        Catch ex As Exception
        End Try
        Try
            lang3324.Text = axlabs.GetASPXPage("EQReport.aspx", "lang3324")
        Catch ex As Exception
        End Try
        Try
            lang3325.Text = axlabs.GetASPXPage("EQReport.aspx", "lang3325")
        Catch ex As Exception
        End Try

    End Sub

End Class
