﻿Imports System.Data.SqlClient
Public Class RBASloclook
    Inherits System.Web.UI.Page
    Dim dr As SqlDataReader
    Dim sql As String
    Dim fail As New Utilities
    Dim skillid, qty, freq, rdid, siteid, who, rteid, typ, ptid, days, shift, lang As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        getlabels()
        If Not IsPostBack Then
            siteid = Request.QueryString("sid").ToString
            lblsid.Value = siteid
            'who = Request.QueryString("who").ToString
            'lblwho.Value = who
            skillid = Request.QueryString("skillid").ToString
            lblskillid.Value = skillid
            qty = Request.QueryString("qty").ToString
            lblqty.Value = qty
            freq = Request.QueryString("freq").ToString
            lblfreq.Value = freq
            rdid = Request.QueryString("rdid").ToString
            lblrdid.Value = rdid
            rteid = Request.QueryString("rteid").ToString
            lblrteid.Value = rteid
            ptid = Request.QueryString("ptid").ToString
            lblptid.Value = ptid
            days = Request.QueryString("days").ToString
            lbldays.Value = days
            shift = Request.QueryString("shift").ToString
            lblshift.Value = shift
            fail.Open()
            checkfail(ptid, skillid, qty, freq, rdid, siteid, days, shift)
            fail.Dispose()

            lblopt.Value = "0"
            lbfailmodes.AutoPostBack = False
            lbfailmaster.AutoPostBack = False
        End If
    End Sub
    Private Sub getlabels()
        lang = lblfslang.Value
        If lang = "fre" Then
            unlang3326.Text = "Sélection Niveau 1"
            unlang3327.Text = "Niveaux disponibles"
            unlang3328.Text = "Niveaux sélectionnés"
            'lang3329.Text = 
            'lang3330.Text = 
            lims.Text = "Sélection multiple (utilisez les fèches pour la sélection simple ou multiple)"
            lico.Text = "Cliquez une fois (déplacez l'item en cliquant dessus)"
            lbll2.Text = "Sélection Niveau 2"
            lblal.Text = "Niveaux disponibles"
            lblsl.Text = "Niveaux sélectionnés"
            lims2.Text = "Sélection multiple (utilisez les fèches pour la sélection simple ou multiple)"
            lico2.Text = "Cliquez une fois (déplacez l'item en cliquant dessus)"
            Label4.Text = "Options de liste"
            lang3329.Text = "Options de liste"

        End If
    End Sub
    Private Sub checkfail(ByVal ptid As String, ByVal skillid As String, ByVal qty As String, ByVal freq As String, ByVal rdid As String, ByVal siteid As String, ByVal days As String, ByVal shift As String)
        rteid = lblrteid.Value
        sql = "select rttype from pmroutes where rid = '" & rteid & "'"
        typ = fail.strScalar(sql)
        lblrttype.Value = typ
        ptid = lblptid.Value

        who = "0"
        Dim cnt As String
        Dim rd, skill, pm, pdm As String
        If rdid = "2" Then
            rd = "DOWN"
        Else
            rd = "RUNNING"
        End If
        sql = "select skill from pmskills where skillid = '" & skillid & "'"
        skill = fail.strScalar(sql)
        If ptid <> "0" Then
            sql = "select pretech from pmpretech where ptid = '" & ptid & "'"
            pdm = fail.strScalar(sql)
        Else
            pdm = ""
        End If

        If pdm = "" Then
            If typ = "RBAS" Then
                pm = skill & "(" & qty & ")/" & freq & "/" & rd
            Else
                If shift = "" And days = "" Then
                    pm = skill & "(" & qty & ")/" & freq & "/" & rd
                Else
                    pm = skill & "(" & qty & ")/" & freq & "/" & rd & "-" & shift & "-" & days
                End If

            End If

        Else
            If typ = "RBAS" Then
                pm = "PdM-" & pdm & "-" & skill & "(" & qty & ")/" & freq & "/" & rd
            End If

        End If

        days = Replace(days, "-", ",", , , vbTextCompare)
        sql = "update pmroutes set ptid = '" & ptid & "', skillid = '" & skillid & "', qty = '" & qty & "', freq = '" & freq & "', rdid = '" & rdid & "', pm = '" & pm & "', days = '" & days & "', shift = '" & shift & "' where rid = '" & rteid & "'"
        fail.Update(sql)
        If typ = "RBAS" Then
            sql = "usp_locmess '" & ptid & "','" & skillid & "','" & qty & "','" & freq & "','" & rdid & "','" & siteid & "','" & who & "','" & rteid & "'"
        ElseIf typ = "RBAST" Then
            sql = "usp_locmessT '" & skillid & "','" & qty & "','" & freq & "','" & rdid & "','" & siteid & "','" & who & "','" & rteid & "','" & days & "','" & shift & "'"
        End If

        cnt = fail.strScalar(sql)
        If cnt = "0" Then
            who = "2"
        Else
            who = "1"
        End If
        'who = "1"
        lblwho.Value = who
        PopFail(ptid, skillid, qty, freq, rdid, siteid, who, days, shift)
        PopFailList(rteid)
        If who = "1" Then
            divlev2.Visible = True
            tdno2.Visible = False
            PopFail2()
            PopFailList2()


        Else
            divlev2.Visible = False
            tdno2.Visible = True


        End If
    End Sub
    Private Sub PopFail(ByVal ptid As String, ByVal skillid As String, ByVal qty As String, ByVal freq As String, ByVal rdid As String, ByVal siteid As String, ByVal who As String, ByVal days As String, ByVal shift As String)
        rteid = lblrteid.Value
        typ = lblrttype.Value
        days = Replace(days, "-", ",", , , vbTextCompare)
        If typ = "RBAS" Then
            sql = "usp_locmess '" & ptid & "','" & skillid & "','" & qty & "','" & freq & "','" & rdid & "','" & siteid & "','" & who & "','" & rteid & "'"
        ElseIf typ = "RBAST" Then
            sql = "usp_locmessT '" & skillid & "','" & qty & "','" & freq & "','" & rdid & "','" & siteid & "','" & who & "','" & rteid & "','" & days & "','" & shift & "'"
        End If

        dr = fail.GetRdrData(sql)
        'lbfailmodes.DataSource = dr
        lbfailmaster.DataSource = dr
        If who = "1" Then
            lbfailmaster.DataTextField = "parent1"
            lbfailmaster.DataValueField = "parent1"
        Else
            lbfailmaster.DataTextField = "parent2"
            lbfailmaster.DataValueField = "parent2"
        End If

        lbfailmaster.DataBind()
        dr.Close()
    End Sub
   
    Private Sub PopFailList(ByVal rteid As String)
        Dim locstr As String
        who = lblwho.Value
        If who = "1" Then
            sql = "declare @level1 varchar(2000) " _
                + "set @level1 = (select level1 from pmroutes where rid = '" & rteid & "') " _
                + "select distinct location as level1 from pmlocations where location in (SELECT isnull(val, '') FROM dbo.f_split(@level1, ','))"
        Else
            sql = "declare @level2 varchar(2000) " _
                + "set @level2 = (select level2 from pmroutes where rid = '" & rteid & "') " _
                + "select distinct location as level2 from pmlocations where location in (SELECT isnull(val, '') FROM dbo.f_split(@level2, ','))"
        End If
        'locstr = fail.strScalar(sql)
        dr = fail.GetRdrData(sql)
        lbfailmodes.DataSource = dr
        If who = "1" Then
            lbfailmodes.DataValueField = "level1"
            lbfailmodes.DataTextField = "level1"
        Else
            lbfailmodes.DataValueField = "level2"
            lbfailmodes.DataTextField = "level2"
        End If
        Try
            lbfailmodes.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        Try
            lbfailmodes.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btntocomp_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToComp()
    End Sub
    Private Sub ToComp()
        Dim locstr, minstr As String
        who = lblwho.Value
        'lbllocstr.Value = ""
        siteid = lblsid.Value
        rteid = lblrteid.Value
        ptid = lblptid.Value
        days = lbldays.Value
        shift = lblshift.Value

        Dim Item As ListItem
        Dim f, fi As String
        fail.Open()
        sql = "select isnull(level1, '') from pmroutes where rid = '" & rteid & "'"
        locstr = fail.strScalar(sql)
        If locstr = "" Then
            sql = "select isnull(level2, '') from pmroutes where rid = '" & rteid & "'"
            locstr = fail.strScalar(sql)
        End If
        lbllocstr.Value = locstr
        For Each Item In lbfailmaster.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                If Len(lbllocstr.Value) = 0 Then
                    lbllocstr.Value = fi '"'" & fi & "'"
                    locstr = "'" & fi & "'"
                    minstr = "'" & fi & "'"
                    If who = 1 Then
                        sql = "update pmroutes set level1 = " & minstr & " where rid = '" & rteid & "'"
                    Else
                        sql = "update pmroutes set level2 = " & minstr & " where rid = '" & rteid & "'"
                    End If
                Else
                    lbllocstr.Value += "," & fi '& "'"
                    minstr = lbllocstr.Value '+= ",'" & fi & "'"
                    'locstr = lbllocstr.Value
                    ''N112PR1' + ',' + 'N11DRY1'
                    'minstr = "(" & locstr & " + ','+ '" & fi & "')"
                    If who = "1" Then
                        sql = "update pmroutes set level1 = '" & minstr & "' where rid = '" & rteid & "'"
                    Else
                        sql = "update pmroutes set level2 = '" & minstr & "' where rid = '" & rteid & "'"
                    End If
                End If

                fail.Update(sql)
            End If
        Next
        Dim tst As String = locstr
        If who = "1" Then
            'sql = "update pmroutes set level1 = NULL where rid = '" & rteid & "'"
            'fail.Update(sql)
            'sql = "update pmroutes set level1 = '" & locstr & "' where rid = '" & rteid & "'"
        Else
            'sql = "update pmroutes set level2 = NULL where rid = '" & rteid & "'"
            'fail.Update(sql)
            'sql = "update pmroutes set level2 = '" & locstr & "' where rid = '" & rteid & "'"
        End If
        'fail.Update(sql)
        skillid = lblskillid.Value
        qty = lblqty.Value
        freq = lblfreq.Value
        rdid = lblrdid.Value
        PopFailList(rteid)
        PopFail(ptid, skillid, qty, freq, rdid, siteid, who, days, shift)
        If who = "1" Then
            PopFail2()
            PopFailList2()
        End If
        fail.Dispose()
    End Sub

    Protected Sub btnfromcomp_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        FromComp()
    End Sub
    Private Sub FromComp()
        Dim locstr, minstr As String
        who = lblwho.Value
        siteid = lblsid.Value
        rteid = lblrteid.Value
        ptid = lblptid.Value
        days = lbldays.Value
        shift = lblshift.Value
        Dim Item As ListItem
        Dim f, fi As String
        fail.Open()
        If who = "1" Then
            sql = "update pmroutes set level1 = NULL where rid = '" & rteid & "'"
        Else
            sql = "update pmroutes set level2 = NULL where rid = '" & rteid & "'"
        End If
        fail.Update(sql)
        Dim cnt As Integer = lbfailmodes.Items.Count
        If cnt = 1 Then
            Try
                lbfailmodes.SelectedIndex = 0
            Catch ex As Exception

            End Try

        End If
        If Len(lbllocstr.Value) > 0 Then

        End If
        lbllocstr.Value = ""
        For Each Item In lbfailmodes.Items
            If Not Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                If Len(lbllocstr.Value) = 0 Then
                    lbllocstr.Value = fi '"'" & fi & "'"
                    locstr = "'" & fi & "'"
                    minstr = lbllocstr.Value 'minstr = "'" & fi & "'"
                    If who = 1 Then
                        sql = "update pmroutes set level1 = " & locstr & " where rid = '" & rteid & "'"
                    Else
                        sql = "update pmroutes set level2 = " & locstr & " where rid = '" & rteid & "'"
                    End If

                Else
                    lbllocstr.Value += "," & fi 'lbllocstr.Value += "','" & fi & "'"
                    locstr = "','" & fi & "'"
                    'minstr = locstr & " + ','+ '" & fi & "'"
                    'minstr = lbllocstr.Value
                    'If who = "1" Then
                    'sql = "update pmroutes set level1 = level1 + ',' + " & minstr & " where rid = '" & rteid & "'"
                    'Else
                    'sql = "update pmroutes set level2 = level2 + ',' + " & minstr & " where rid = '" & rteid & "'"
                    'End If

                    If who = "1" Then
                        sql = "update pmroutes set level1 =  level1 + '" & locstr & "' where rid = '" & rteid & "'"
                    Else
                        sql = "update pmroutes set level2 =  level2 + '" & locstr & "' where rid = '" & rteid & "'"
                    End If
                End If
                fail.Update(sql)
            End If
        Next
        'locstr = lbllocstr.Value
        If who = "1" Then
            'sql = "update pmroutes set level1 = NULL where rid = '" & rteid & "'"
            'fail.Update(sql)
            'sql = "update pmroutes set level1 = '" & locstr & "' where rid = '" & rteid & "'"
        Else
            'sql = "update pmroutes set level2 = NULL where rid = '" & rteid & "'"
            'fail.Update(sql)
            'sql = "update pmroutes set level2 = '" & locstr & "' where rid = '" & rteid & "'"
        End If
        'fail.Update(sql)
        skillid = lblskillid.Value
        qty = lblqty.Value
        freq = lblfreq.Value
        rdid = lblrdid.Value
        PopFailList(rteid)
        PopFail(ptid, skillid, qty, freq, rdid, siteid, who, days, shift)
        Dim eqstr As String
        sql = "select eqstr from pmroutes where rid = '" & rteid & "'"
        Try
            eqstr = fail.strScalar(sql)
        Catch ex As Exception

        End Try
        typ = lblrttype.Value
        If who = "1" Then
            CheckLevel2()
            PopFail2()
            PopFailList2()
            If eqstr <> "" Then
                If typ = "RBAS" Then
                    sql = "usp_checkRBASremeq '" & rteid & "'"

                ElseIf typ = "RBAST" Then
                    sql = "usp_checkRBASremeqT '" & rteid & "'"

                End If
                fail.Update(sql)
            End If
        End If
        fail.Dispose()
    End Sub
    Private Sub FromComp2()
        Dim locstr, minstr As String
        who = lblwho.Value
        siteid = lblsid.Value
        rteid = lblrteid.Value
        Dim Item As ListItem
        Dim f, fi As String
        fail.Open()
        sql = "update pmroutes set level2 = NULL where rid = '" & rteid & "'"
        fail.Update(sql)
        Dim cnt As Integer = lbfailmodes2.Items.Count
        If cnt = 1 Then
            Try
                lbfailmodes2.SelectedIndex = 0
            Catch ex As Exception

            End Try

        End If
        lbllocstr2.Value = ""
        For Each Item In lbfailmodes2.Items
            If Not Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                If Len(lbllocstr.Value) = 0 Then
                    lbllocstr.Value = fi '"'" & fi & "'"
                    locstr = "'" & fi & "'"
                    minstr = lbllocstr.Value 'minstr = "'" & fi & "'"
                    sql = "update pmroutes set level2 = " & locstr & " where rid = '" & rteid & "'"

                Else
                    lbllocstr.Value += "," & fi 'lbllocstr.Value += "','" & fi & "'"
                    locstr = "','" & fi & "'"

                    sql = "update pmroutes set level2 =  level2 + '" & locstr & "' where rid = '" & rteid & "'"
                End If
                fail.Update(sql)
            End If
        Next

        'sql = "update pmroutes set level2 = '" & locstr & "' where rid = '" & rteid & "'"
        'fail.Update(sql)
        PopFailList2()
        PopFail2()
        Dim eqstr As String
        sql = "select eqstr from pmroutes where rteid = '" & rteid & "'"
        Try
            eqstr = fail.strScalar(sql)
        Catch ex As Exception

        End Try
        If eqstr <> "" Then
            sql = "usp_checkRBASremeq '" & rteid & "'"
            fail.Update(sql)
        End If
        fail.Dispose()
    End Sub
    Private Sub CheckLevel2()
        rteid = lblrteid.Value
        sql = "usp_checklevel2 '" & rteid & "'"
        fail.Update(sql)
    End Sub
    Private Sub PopFail2()
        rteid = lblrteid.Value
        skillid = lblskillid.Value
        qty = lblqty.Value
        freq = lblfreq.Value
        rdid = lblrdid.Value
        ptid = lblptid.Value
        days = lbldays.Value
        shift = lblshift.Value
        who = "2"
        typ = lblrttype.Value
        If typ = "RBAS" Then
            sql = "usp_locmess '" & ptid & "','" & skillid & "','" & qty & "','" & freq & "','" & rdid & "','" & siteid & "','" & who & "','" & rteid & "'"
        ElseIf typ = "RBAST" Then
            sql = "usp_locmessT '" & skillid & "','" & qty & "','" & freq & "','" & rdid & "','" & siteid & "','" & who & "','" & rteid & "','" & days & "','" & shift & "'"
        End If


        'sql = "declare @level1 varchar(2000), @level2 varchar(2000) " _
        '+ "set @level1 = (select level1 from pmroutes where rid = '" & rteid & "') " _
        '+ "set @level2 = (select level2 from pmroutes where rid = '" & rteid & "') " _
        '+ "select location from pmlocheir where parent in (SELECT val FROM dbo.f_split(@level1, ',')) " _
        '+ " and location not in (select location from pmlocheir where location in (SELECT val FROM dbo.f_split(@level2, ',')))"
        dr = fail.GetRdrData(sql)
        lbfailmaster2.DataSource = dr
        lbfailmaster2.DataSource = dr
        lbfailmaster2.DataTextField = "parent2" '"location"
        lbfailmaster2.DataValueField = "parent2" '"location"
        lbfailmaster2.DataBind()
        dr.Close()
    End Sub
    Private Sub PopFailList2()
        rteid = lblrteid.Value
        sql = "declare @level2 varchar(2000) " _
                + "set @level2 = (select level2 from pmroutes where rid = '" & rteid & "') " _
                + "select distinct location from pmlocations where location in (SELECT isnull(val, '') FROM dbo.f_split(@level2, ','))"

        dr = fail.GetRdrData(sql)
        lbfailmodes2.DataSource = dr
        'lbfailmodes2.DataSource = dr
        lbfailmodes2.DataTextField = "location"
        lbfailmodes2.DataValueField = "location"
        Try  
            lbfailmodes2.DataBind()

        Catch ex As Exception

        End Try
        dr.Close()
    End Sub

    Protected Sub btntocomp2_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btntocomp2.Click
        ToComp2()
    End Sub
    Private Sub ToComp2()
        Dim locstr, minstr As String
        who = lblwho.Value
        'lbllocstr.Value = ""
        siteid = lblsid.Value
        rteid = lblrteid.Value
        days = lbldays.Value
        shift = lblshift.Value
        Dim Item As ListItem
        Dim f, fi As String
        fail.Open()
        sql = "select isnull(level2, '') from pmroutes where rid = '" & rteid & "'"
        locstr = fail.strScalar(sql)
        lbllocstr2.Value = locstr
        'sql = "update pmroutes set level2 = NULL where rid = '" & rteid & "'"
        'fail.Update(sql)
        For Each Item In lbfailmaster2.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                If Len(lbllocstr2.Value) = 0 Then
                    lbllocstr2.Value = fi '"'" & fi & "'"
                    locstr = fi '"'" & fi & "'"
                    minstr = fi '"'" & fi & "'"
                    sql = "update pmroutes set level2 = '" & minstr & "' where rid = '" & rteid & "'"
                Else
                    lbllocstr2.Value += "," & fi ' & "'"
                    locstr += "," & fi '"','" & fi & "'"
                    'minstr = "'" & fi & "'"
                    minstr = locstr '& " + ','+ '" & fi & "'"
                    'sql = "update pmroutes set level2 = level2 + ',' + " & minstr & " where rid = '" & rteid & "'"
                    sql = "update pmroutes set level2 = '" & minstr & "' where rid = '" & rteid & "'"
                End If
            fail.Update(sql)
            End If
        Next
        
        'sql = "update pmroutes set level2 = '" & locstr & "' where rid = '" & rteid & "'"
        'fail.Update(sql)
        PopFailList2()
        PopFail2()
        fail.Dispose()
    End Sub

    Protected Sub btnfromcomp2_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp2.Click
        FromComp2()
    End Sub
    
    Private Sub lbfailmaster_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbfailmaster.SelectedIndexChanged
        If lblopt.Value = "1" Then
            ToComp()
        End If
    End Sub

    Private Sub lbfailmodes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbfailmodes.SelectedIndexChanged
        If lblopt.Value = "1" Then
            FromComp()
        End If
    End Sub

    Private Sub cbopts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbopts.SelectedIndexChanged
        Dim opt As String = cbopts.SelectedValue.ToString
        lblopt.Value = opt
        If opt = "0" Then
            lbfailmodes.AutoPostBack = False
            lbfailmaster.AutoPostBack = False
        Else
            lbfailmodes.AutoPostBack = True
            lbfailmaster.AutoPostBack = True
        End If
    End Sub
End Class