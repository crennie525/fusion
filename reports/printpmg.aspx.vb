﻿Imports System.Data.SqlClient
Public Class printpmg
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim pmid, eqid, ptid, skillid, qty, freq, rid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eqid = Request.QueryString("eqid").ToString
        ptid = Request.QueryString("ptid").ToString
        skillid = Request.QueryString("skillid").ToString
        qty = Request.QueryString("qty").ToString
        freq = Request.QueryString("freq").ToString
        rid = Request.QueryString("rid").ToString
        pms.Open()
        PopReport(eqid, ptid, skillid, qty, freq, rid)
        pms.Dispose()
    End Sub
    Private Sub PopReport(ByVal eqid As String, ByVal ptid As String, ByVal skillid As String, ByVal qty As String, ByVal freq As String, ByVal rid As String)
        '[usp_getpmg](@eqid int, @ptid int, @skillid int, @qty int, @freq int, @rdid int
        Dim flag As Integer = 0
        Dim subcnt As Integer = 0
        Dim subcnth As Integer = 0
        Dim func, funcchk, task, tasknum, subtask, pm, eqnum As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
        sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""610""></td></tr>")
        sql = "usp_getpmg '" & eqid & "','" & ptid & "','" & skillid & "','" & qty & "','" & freq & "','" & rid & "'"
        dr = pms.GetRdrData(sql)
        Dim headhold As String = "0"
        While dr.Read
            If headhold = "0" Then
                headhold = "1"
                pm = dr.Item("pm").ToString
                eqnum = dr.Item("eqnum").ToString
                sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""3"">" & eqnum & "</td></tr>")
                sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""3"">" & pm & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">Total Estimated Time: " & dr.Item("ttime") & " Minutes</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">Total Down Time: " & dr.Item("dtime") & " Minutes</td></tr>")
                'sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">Scheduled Start Date: " & dr.Item("nextdate") & "</td></tr>")
                sb.Append("<tr><td class=""bigbold"" colspan=""3""><hr></td></tr>")
            End If

            func = dr.Item("func").ToString
            If funcchk <> func Then 'flag = 0 Then
                If flag = 0 Then
                    flag = 1
                Else
                    'sb.Append("<tr><td>&nbsp;</td></tr>")
                End If

                funcchk = func
                sb.Append("<tr height=""20""><td class=""label"" colspan=""3""><u>Function: " & func & "</u></td>")
                'sb.Append("<td class=""plainlabel"" colspan=""2"">" & func & "</td></tr>")
                'sb.Append("<tr><td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl63" , "PrintPM.aspx.vb") & "</td>")
                'sb.Append("<td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl64" , "PrintPM.aspx.vb") & "</td>")
                'sb.Append("<td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl65" , "PrintPM.aspx.vb") & "</td></tr>")
            End If
            task = dr.Item("task").ToString
            tasknum = dr.Item("tasknum").ToString
            subtask = dr.Item("subtask").ToString
            subcnt = dr.Item("subcnt").ToString


            If subtask = "0" Then
                'sb.Append("<tr height=""20""><td></td><td class=""label"">" & subcnt & "</td></tr>")
                sb.Append("<tr><td class=""plainlabel"">[" & tasknum & "]</td>")
                If Len(dr.Item("task").ToString) > 0 Then
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Task</b> :" & task & "</td></tr>")
                Else
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Task:</b> No Task Description Provided</td></tr>")
                End If
                'sb.Append("<tr><td>&nbsp;</td></tr>")
                sb.Append("<tr height=""20""><td></td>")
                sb.Append("<td colspan=""2"" class=""plainlabel"">Check: OK(___) " & dr.Item("fm1").ToString & "</td></tr>")
                If dr.Item("lube") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Lubes: " & dr.Item("lube").ToString & "</td></tr>")
                End If
                If dr.Item("tool") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Tools: " & dr.Item("tool").ToString & "</td></tr>")
                End If
                If dr.Item("part") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Parts: " & dr.Item("part").ToString & "</td></tr>")
                End If
                If dr.Item("meas") <> "none" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Measurements: " & dr.Item("meas").ToString & "</td></tr>")
                End If
                If subcnt = 0 Then
                    sb.Append("<tr><td>&nbsp;</td></tr>")
                Else
                    If subcnt <> 0 Then

                        sb.Append("<tr height=""20""><td></td><td class=""label"" colspan=""2""><u>Sub Tasks</u></td></tr>")
                    End If

                End If

            Else
                'sb.Append("<tr><td>" & subcnt & "</td></tr>")
                sb.Append("<tr height=""20""><td></td><td class=""plainlabel"">[" & subtask & "]</td>")
                If Len(dr.Item("subt").ToString) > 0 Then
                    sb.Append("<td class=""plainlabel""><b>Task</b>: " & dr.Item("subt").ToString & "</td></tr>")
                Else
                    sb.Append("<td class=""plainlabel""><b>Task</b>: No Sub Task Description Provided</td></tr>")
                End If
                sb.Append("<tr><td></td><td></td>")
                sb.Append("<td class=""plainlabel"">Check: OK(___)</td></tr>")
                sb.Append("<tr><td>&nbsp;</td></tr>")
            End If


        End While
        dr.Close()
        sb.Append("</table>")
        tdwi.InnerHtml = sb.ToString

    End Sub
End Class