﻿Imports System.Data.SqlClient
Imports System.Text
Public Class origpie
    Inherits System.Web.UI.Page
    Dim dr As SqlDataReader
    Dim sql As String
    Dim ovr As New Utilities
    Dim sid, eqid, eqstr, typ As String
    Dim skillstr As String
    Dim ohour As String
    Dim rhour As String
    Dim rhourb As String
    Dim tmod As New transmod
    Dim lang As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        If comi = "SUN" Then
            'lblfslang.Value = "fre"
        End If
        'END ADD FOR SUNCOR
        'Put user code to initialize the page here
        typ = Request.QueryString("typ").ToString
        If typ = "site" Then
            sid = Request.QueryString("sid").ToString
            PopEqVal(typ, sid)
        ElseIf typ = "eq" Then
            eqid = Request.QueryString("eqid").ToString
            sid = Request.QueryString("sid").ToString
            PopEqVal(typ, sid, eqid)
        ElseIf typ = "select" Then
            sid = Request.QueryString("sid").ToString
            '"'888','902','940','944','951','956','957','958','1436','1887','2765','2766','2781','2782','2783','2784','2785','2786','4275','4285','4286','4288','4294','4295','4307','4519','4525','4557','4561','4901','4923','4972','4973','4991','4995','4996','5080','5081','5082','5083','5084','5085','5086','5087','5088','5089','5090','5091','5993','6292','6366','6368','6431','6677','6678','6679','6680','6681','6682','6683','6684','6685','6686','6687','6688','6689','6690','6691','6692','6693','6694','6695','6696','6697','6698','6699','6703','6704','6705','6706','6707','6708','6709','6710','6711','6805','6811','6812','6813','6814','6816','6817','6818','6819','6820','6822','6823','6824','6825','6826','6828','6830','6831','6832','6833','6834','6851','6853','6854','6873','6936','6937','6938','6939','6940','6941','6942','6943','6944','6978','6979','7154','7175','7182','7185','7191','7294','7319','7320','7425','7428','7429','7511','7547','7581','7587','7597','7726','7727','7743','7744','7745','7746','7747','7748','7749','7891','7892','8182','8195','8202','8203','8204','8205','8206','8207','8214','8215','8246','8247','8248','8249','8250','8251','8278','8279','8280','8296','8297','8307','8316','8325','8326','8332','8338','8339','8340','8341','8342','8343','8348','8368','8369','8439','8440','8497','8498','8499','8510','8511','8514','8515','8662','8663','8664','8665','8666','8667','8668','8669','8670','8671','8779','8780','8781','8782','8783','8784','8785','8786','8787','8788','8789','8801','8920','8921','8922','8923','8924','8925','8926','8927','8928','8929','8930','8931','8932','8933','8934','8935','8936','8937','8938','8939','8940','8941','8942','8943','8944','8945','8947','8984','8985','9056','9057','9135','9181','9182','9183','9201','9203','9204','9213','9214','9215','9222','9253','9254','9256','9257','9258','9525','9536','9591','9613','9624','9628','9629','9633','9635','9636','9648','9654','9655','9681','9683','9775','9779','9780','9781','9785','9786','9788','9790','9791','9793','9794','9795','9796','9797','9799','9800','9801','9802','9803','9804','9805','9806','9807','9808','9809','9810','9811','9812','9813','9814','9815','9816','9817','9818','9819','9820','9821','9822','9823','9824','9825','9826','9827','9828','9829','9830','9832','9833','9834','9835','9836','9837','9838','9839','9840','9841','9842','9843','9844','9845','9846','9849','9850','9851','9852','9853','9854','9855','9856','9857','9858','9859','9860','9861','9862','9863','9865','9866','9867','9868','9869','9870','9871','9872','9873','9874','9875','9876','9877','9878','9879','9880','9881','9882','9883','9884','9885','9886','9887','9888','9889','9890','9891','9893','9894','9895','9896','9897','9898','9899','9900','9901','9902','9903','9904','9905','9906','9908','9909','9910','9911','9912','9913','9914','9915','9916','9917','9918','9919','9920','9921','9922','9923','9924','9926'"

            eqstr = Request.QueryString("eqstr").ToString()
            PopEqVal(typ, sid, eqstr)
        End If

    End Sub
    Private Sub PopEqVal(ByVal typ As String, ByVal sid As String, Optional ByVal eqstr As String = "")
        Dim scnt As Integer
        scnt = eqstr.Length
        Dim es, ee As String
        es = eqstr
        es = Mid(es, 1)
        es = Replace(es, "'", "''")
        ee = eqstr
        eqstr = es
        Dim istot As String
        Dim usetot As String = ""
        ovr.Open()


        Select Case typ
            Case "eq"
                sql = "select isnull(usetot, '0') from equipment where eqid = '" & eqid & "'"
                istot = ovr.strScalar(sql)
                sql = "usp_optValAnlEq2 '" & eqid & "'"
            Case "site"
                sql = "select count(*) from equipment where siteid = '" & sid & "' and usetot = 1"
                istot = ovr.strScalar(sql)
                If istot > 0 Then
                    istot = "1"
                End If
                sql = "usp_optValAnlEq2PS '" & sid & "'"
            Case "select"
                Dim eqstrin As String = eqstr.Replace("''", "'")
                sql = "select count(*) from equipment where siteid = '" & sid & "' and usetot = 1 and eqid in (" & eqstrin & ")"
                istot = ovr.strScalar(sql)
                If istot > 0 Then
                    istot = "1"
                End If
                sql = "usp_optValAnlEq2PSS '" & sid & "', '" & eqstr & "'"
        End Select


        Dim sb As New StringBuilder
        Dim ihttemp As Integer = 200
        Dim rhttemp As Integer = 300
        sb.Append("<html><head>")
        sb.Append("<LINK href=""../styles/pmcssa1.css"" type=""text/css"" rel=""stylesheet"">")
        sb.Append("</head><body>")

        'sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""700"">") ' style=""page-break-after:always;""
        'sb.Append("<tr><td><iframe width=""280"" height=" & ihttemp & " frameBorder=""no"" scrolling=""no"" src=""../appsman/PMPie3D.aspx?title=Original&skills=" & skillstr & "&hrs=" & ohour & """></td><td><iframe width=""280"" height=" & rhttemp & " frameBorder=""no"" scrolling=""no"" src=""../appsman/PMPie3D.aspx?title=Optimized&skills=" & skillstr & "&hrs=" & rhour & """></td></tr>")
        Dim title As String
        If typ = "site" Then
            title = "Site Rollup"
        ElseIf (typ = "select") Then
            title = "Asset Select"
        End If
        'sb.Append("<tr><td class=""bigbold1"" colspan=""3"" align=""center"">" & tmod.getxlbl("xlb357", "OVRollup.aspx.vb") & " " & title & "</td>")
        Dim ocraft_min, rcraft_min, lab_min, rcraft_min_before, lab_min_before As String
        Dim ocraft_hrs, rcraft_hrs, lab_sav, lab_per, rcraft_hrs_before, lab_sav_before, lab_per_before As String
        Dim ordt_min, rrdt_min, rdt_min, rrdt_min_before, rdt_min_before As String
        Dim ordt_hrs, rrdt_hrs, rdt_sav, rrdt_hrs_before, rdt_sav_before As String
        Dim omat, rmat, mat_sav, rmat_before, mat_sav_before As String


        Select Case typ
            Case "eq"

                sql = "usp_optValAnlEq4 '" & eqid & "'"
            Case "site"
                sql = "usp_optValAnlEq4PS '" & sid & "'"
            Case "select"
                sql = "usp_optValAnlEq4PSS '" & sid & "', '" & eqstr & "'"
        End Select

        Try
            dr = ovr.GetRdrData(sql)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr1601", "OVRollup.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Response.Write("Not Enough Data To Create Overview")
            Exit Sub
        End Try

        Dim taskyro, taskyr, downyro, downyr, taskyreff, downyreff As String

        While dr.Read
            ocraft_min = dr.Item("origttime_min_yr").ToString
            rcraft_min = dr.Item("ttime_min_yr").ToString
            lab_min = dr.Item("lab_min_yr").ToString 'tot min

            rcraft_min_before = dr.Item("ttime_min_yr_before").ToString
            lab_min_before = dr.Item("lab_min_yr_before").ToString 'tot min

            ocraft_hrs = dr.Item("origttime_hrs_yr").ToString
            rcraft_hrs = dr.Item("ttime_hrs_yr").ToString
            lab_sav = dr.Item("lab_sav").ToString 'hours saved

            rcraft_hrs_before = dr.Item("ttime_hrs_yr_before").ToString
            lab_sav_before = dr.Item("lab_sav_before").ToString 'hours saved

            ordt_min = dr.Item("origrdt_min_yr").ToString
            rrdt_min = dr.Item("rdt_min_yr").ToString
            rdt_min = dr.Item("rdt_min_yr").ToString 'tot min

            rrdt_min_before = dr.Item("rdt_min_yr_before").ToString
            rdt_min_before = dr.Item("rdt_min_yr_before").ToString 'tot min

            ordt_hrs = dr.Item("origrdt_hrs_yr").ToString
            rrdt_hrs = dr.Item("ttime_hrs_yr").ToString
            rdt_sav = dr.Item("rdt_sav").ToString 'hours saved

            rrdt_hrs_before = dr.Item("ttime_hrs_yr_before").ToString
            rdt_sav_before = dr.Item("rdt_sav_before").ToString 'hours saved

            omat = dr.Item("origmat_yr").ToString
            rmat = dr.Item("mat_yr").ToString
            mat_sav = dr.Item("mat_sav").ToString

            rmat_before = dr.Item("mat_yr_before").ToString
            mat_sav_before = dr.Item("mat_sav_before").ToString




            'lab_per = dr.Item("lab_per").ToString
        End While

        dr.Close()
        'Dim sb As New StringBuilder

        If ocraft_min = "" Or ocraft_min = "0" Then  'Or (rcraft_min_before = "")
            Dim strMessage As String = tmod.getxlbl("xlb361", "OVRollup.aspx.vb")
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Response.Write("Not Enough Data To Create Overview")
            Exit Sub
        End If

        Select Case typ
            Case "eq"
                sql = "select eqnum, eqdesc from equipment where eqid = '" & eqid & "'"
                dr = ovr.GetRdrData(sql)
                If istot = "1" Then
                    usetot = "(Using Total Time)"
                End If
                While dr.Read
                    'sb.Append("<tr><td class=""bigbold"" width=""60px"">" & tmod.getlbl("cdlbl849", "OVRollup.aspx.vb") & "</td>")
                    'sb.Append("<td class=""bigbold"" width=""120px"">" & dr.Item("eqnum").ToString & ":</td>")
                    'sb.Append("<td class=""bigbold"" width=""280px"">" & dr.Item("eqdesc").ToString & "</td><td class=""bigbold"" width=""140px"">" & usetot & "</td></tr>")
                    'sb.Append("<tr><td colspan=""4""><hr size=""2""></td></tr>")
                    'sb.Append("</table>")
                End While
                dr.Close()
            Case "site"
                sql = "select sitename, sitedesc from sites where siteid = '" & sid & "'"
                dr = ovr.GetRdrData(sql) 'width=""80px""
                While dr.Read
                    'sb.Append("<tr><td class=""bigbold"" colspan=""3"" align=""center"">" & tmod.getxlbl("xlb358", "OVRollup.aspx.vb") & "&nbsp;&nbsp;" & dr.Item("sitename").ToString & "</td></tr>")
                    'sb.Append("<td class=""bigbold"" width=""140px"">" & dr.Item("sitename").ToString & ":</td>")
                    'sb.Append("<td class=""bigbold"" width=""400px"">" & dr.Item("sitedesc").ToString & "</td></tr>")
                    'sb.Append("<tr><td colspan=""3""><hr size=""2""></td></tr>")
                    'sb.Append("</table>")
                End While
                dr.Close()
            Case "select"
                sql = "select sitename, sitedesc from sites where siteid = '" & sid & "'"
                dr = ovr.GetRdrData(sql) 'width=""80px""
                While dr.Read
                    'sb.Append("<tr><td class=""bigbold"" colspan=""3"" align=""center"">" & tmod.getxlbl("xlb359", "OVRollup.aspx.vb") & "&nbsp;&nbsp;" & dr.Item("sitename").ToString & "</td></tr>")
                End While
                dr.Close()
                sql = "select eqnum, eqdesc from equipment where eqid in (" & ee & ")"
                dr = ovr.GetRdrData(sql)


                While dr.Read
                    'sb.Append("<tr><td class=""label"" width=""80px"">" & tmod.getlbl("cdlbl850", "OVRollup.aspx.vb") & "</td>")
                    'sb.Append("<td class=""label"">" & dr.Item("eqnum").ToString & ":</td>")
                    'sb.Append("<td class=""label"">" & dr.Item("eqdesc").ToString & "</td><td class=""label"">" & usetot & "</td></tr>")
                    'sb.Append("<tr><td colspan=""4""><hr size=""2""></td></tr>")
                    'sb.Append("</table>")
                End While
                dr.Close()
        End Select

        'istot = "3"
        'Separate Columns Here ******************

        'sb.Append("<Table cellSpacing=""0"" cellPadding=""0"" border=""0"" width=""700"">")

        'sb.Append("<tr><td width=""420""></td><td width=""280""></td></tr>")

        'sb.Append("<tr><td valign=""top"">") 'First Column ****************
        'sb.Append("1")
        'sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""420"" border=""0"">")
        'sb.Append("<tr><td colspan=""3""><table   cellSpacing=""0"" cellPadding=""3"" width=""420px"">")
        'sb.Append("<tr><td width=""140px""></td>")
        'sb.Append("<td width=""50px""></td>")
        'sb.Append("<td width=""50px""></td>")
        'sb.Append("<td width=""80px""></td>")
        'sb.Append("<td width=""100px""></td></tr>")
        'sb.Append("<tr bgcolor=""#eeeded"" class=""label"">")
        'sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"">PM</td>")
        'sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl852", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blackleft blacktop"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl853", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blackcntr blacktop"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl854", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blackright blacktop"" bgcolor=""#eeeded"">Req'd Mins</td></tr>")

        'sb.Append("<tr><td class=""label blackleft"">" & tmod.getlbl("cdlbl856", "OVRollup.aspx.vb") & "</td>")

        Select Case typ
            Case "eq"
                sql = "usp_optValAnlEq '" & eqid & "'"
            Case "site"
                sql = "usp_optValAnlEqPS '" & sid & "'"
            Case "select"
                sql = "usp_optValAnlEqPSS '" & sid & "', '" & eqstr & "'"
        End Select

        Try
            dr = ovr.GetRdrData(sql)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr1602", "OVRollup.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Response.Write("Not Enough Data To Create Overview")
            Exit Sub
        End Try

        Dim o, t, n, ro, rta, rn, edope, edope2, edo As Integer
        While dr.Read
            o = dr.Item("otasks").ToString
            t = dr.Item("rtasks").ToString
            n = t - o

            ro = dr.Item("otasks").ToString
            rta = dr.Item("rtasks_before").ToString

            taskyro = dr.Item("taskyro").ToString
            taskyr = dr.Item("taskyr").ToString
            downyro = dr.Item("downyro").ToString
            downyr = dr.Item("downyr").ToString
            taskyreff = dr.Item("taskyreff").ToString
            downyreff = dr.Item("downyreff").ToString

            'rn = ro - rta
            'rn = rta - ro
            If ro <> 0 Then
                If rta <> 0 Then
                    rn = ro - rta
                    rn = rn * -1
                Else
                    rn = 0
                End If
            Else
                rn = 0
            End If

            edope = dr.Item("rtasks_before").ToString
            edope2 = dr.Item("rtasks").ToString
            'edo = edope - edope2
            edo = edope2 - edope

            'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("otasks").ToString & "</td>")
            'sb.Append("<td class=""plainlabel blackleft"">&nbsp;</td>")
            'sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & dr.Item("downyro").ToString & "</td>")
            'sb.Append("<td class=""plainlabel blackright"" align=""center"">" & dr.Item("taskyro").ToString & "</td></tr>")

            'sb.Append("<tr><td class=""label blackleft"">" & tmod.getlbl("cdlbl857", "OVRollup.aspx.vb") & "</td>")
            'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("rtasks_before").ToString & "</td>")
            'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & rn & "</td>")
            If istot = "1" Then 'dr.Item("down time_before").ToString = "" Or
                'sb.Append("<td class=""plainlabel blackcntr"" align=""center"">N\A</td>")
            Else
                'sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & dr.Item("downyreff").ToString & "</td>")
            End If
            If istot = "1" Then 'dr.Item("rcraft time_before").ToString = "" Or
                'sb.Append("<td class=""plainlabel blackright"" align=""center"">N\A</td>")
            Else
                'sb.Append("<td class=""plainlabel blackright"" align=""center"">" & dr.Item("taskyreff").ToString & "</td>")
            End If

            'sb.Append("<tr><td class=""label blackleft"">" & tmod.getlbl("cdlbl860", "OVRollup.aspx.vb") & "</td>")
            'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("rtasks").ToString & "</td>")
            'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & edo & "</td>") 'was n
            'sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & dr.Item("downyr").ToString & "</td>")
            'sb.Append("<td class=""plainlabel blackright"" align=""center"">" & dr.Item("taskyr").ToString & "</td></tr>")
        End While
        dr.Close()

        'sb.Append("</table></td></tr>")

        '*****************task types********************************
        If typ = "eq" Then
            'sb.Append("<tr><td colspan=""3""><table   cellSpacing=""0"" cellPadding=""3"" width=""420px"">")
            'sb.Append("<tr><td width=""180px""></td>")
            'sb.Append("<td width=""120px""></td>")
            'sb.Append("<td width=""120px""></td></tr>")

            'sb.Append("<tr><td class=""label""><tr class=""label"" bgcolor=""#eeeded"">")
            'sb.Append("<td class=""blacktop blackleft"">" & tmod.getlbl("cdlbl861", "OVRollup.aspx.vb") & "</td>")
            'sb.Append("<td class=""blacktop blackcntr"">" & tmod.getlbl("cdlbl862", "OVRollup.aspx.vb") & "</td>")
            'sb.Append("<td class=""blacktop blackright"">" & tmod.getlbl("cdlbl863", "OVRollup.aspx.vb") & "</td></tr>")

            Select Case typ
                Case "eq"
                    sql = "usp_optvaltt '" & eqid & "'"
                Case "site"
                    'sql = "usp_optValAnlEq2PS '" & sid & "'"
                Case "select"
                    'sql = "usp_optValAnlEq2PSS '" & sid & "', '" & eqstr & "'"
            End Select
            dr = ovr.GetRdrData(sql)
            Dim tt, ottc, rttc As String
            If dr.HasRows Then
                While dr.Read
                    tt = dr.Item("tasktype").ToString
                    ottc = dr.Item("orig").ToString
                    rttc = dr.Item("rev").ToString
                    'sb.Append("<td class=""plainlabel blackleft"" >" & tt & "</td>")
                    'sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & ottc & "</td>")
                    'sb.Append("<td class=""plainlabel blackright"" align=""center"">" & rttc & "</td></tr>")

                End While

            End If
            dr.Close()

            'sb.Append("</table></td></tr>")
        End If

        '*****************task types********************************

        'sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>")
        'sb.Append("<tr><td colspan=""3""><table   cellSpacing=""0"" cellPadding=""3"" width=""420px"">")
        'sb.Append("<tr><td width=""110px""></td>")
        'sb.Append("<td width=""100px""></td>")
        'sb.Append("<td width=""110px""></td>")
        'sb.Append("<td width=""100px""></td>")
        'sb.Append("<td width=""110px""></td>")
        'sb.Append("<td width=""100px""></td></tr>")

        'sb.Append("<tr><td class=""label""><tr class=""label"" bgcolor=""#eeeded"">")
        'sb.Append("<td class=""blacktop blackleft"">" & tmod.getlbl("cdlbl864", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blacktop blackleft"">" & tmod.getlbl("cdlbl865", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blacktop blackleft"">" & tmod.getlbl("cdlbl866", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blacktop blackleft"">" & tmod.getlbl("cdlbl867", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blacktop blackcntr"">" & tmod.getlbl("cdlbl868", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blacktop blackright"">" & tmod.getlbl("cdlbl869", "OVRollup.aspx.vb") & "</td></tr>")
        Dim ot, rt, dt, rtb, dtb As Integer
        ot = 0
        rt = 0
        dt = 0
        rtb = 0
        dtb = 0
        Select Case typ
            Case "eq"
                sql = "usp_optValAnlEq2 '" & eqid & "'"
            Case "site"
                sql = "usp_optValAnlEq2PS '" & sid & "'"
            Case "select"
                sql = "usp_optValAnlEq2PSS '" & sid & "', '" & eqstr & "'"
        End Select

        Try
            dr = ovr.GetRdrData(sql)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr1603", "OVRollup.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Response.Write("Not Enough Data To Create Overview")
            Exit Sub
        End Try

        If dr.HasRows Then
            While dr.Read
                ot = ot + dr.Item("OT").ToString
                rt = rt + dr.Item("RT").ToString
                dt = dt + dr.Item("diff").ToString
                If istot <> "1" Then
                    rtb = rtb + dr.Item("RTB").ToString
                    dtb = dtb + dr.Item("diffB").ToString
                Else
                    rtb = 0
                    dtb = 0
                End If


                'sb.Append("<td class=""plainlabel blackleft"" >" & dr.Item("OS").ToString & "</td>")
                'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("OT").ToString & "</td>")
                If istot <> "1" Then
                    'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("RTB").ToString & "</td>")
                    'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("diffB").ToString & "</td>")
                Else
                    'sb.Append("<td class=""plainlabel blackleft"" align=""center"">N\A</td>")
                    'sb.Append("<td class=""plainlabel blackleft"" align=""center"">N\A</td>")
                End If

                'sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & dr.Item("RT").ToString & "</td>")
                'sb.Append("<td class=""plainlabel blackright"" align=""center"">" & dr.Item("diff").ToString & "</td></tr>")


            End While
        Else
            Dim strMessage As String = tmod.getmsg("cdstr1604", "OVRollup.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Response.Write("Not Enough Data To Create Overview")
            Exit Sub
        End If


        dr.Close()

        'sb.Append("<tr class=""label"">")
        'sb.Append("<td class=""labelibs blackleft"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl870", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & ot & "</td>")
        If istot <> "1" Then
            'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & rtb & "</td>")
        Else
            'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">N\A</td>")
        End If
        If istot <> "1" Then
            'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & dtb & "</td>")
        Else
            'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">N\A</td>")
        End If

        'sb.Append("<td class=""plainlabel blackcntr"" bgcolor=""#eeeded"" align=""center"">" & rt & "</td>")
        'sb.Append("<td class=""plainlabel blackright"" bgcolor=""#eeeded"" align=""center"">" & dt & "</td></tr>")

        'this row
        'sb.Append("<tr bgcolor=""#eeeded""><td class=""label""><tr class=""label"">")
        'sb.Append("<td class=""labelibs blackleft"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl871", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & ocraft_min & "</td>")

        If rcraft_min_before <> "" And istot <> "1" Then
            'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & rcraft_min_before & "</td>")
        Else
            'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">N\A</td>")
        End If


        If rcraft_min_before <> "" And istot <> "1" Then
            'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & ocraft_min - rcraft_min_before & "</td>")
        Else
            'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">N\A</td>")
        End If



        'sb.Append("<td class=""plainlabel blackcntr"" bgcolor=""#eeeded"" align=""center"">" & rcraft_min & "</td>")
        'sb.Append("<td class=""plainlabel blackright"" bgcolor=""#eeeded"" align=""center"">" & ocraft_min - rcraft_min & "</td></tr>")

        'sb.Append("<tr bgcolor=""#eeeded""><td class=""label""><tr class=""label"">")
        'sb.Append("<td class=""labelibs blackleft"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl874", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & ocraft_hrs & "</td>")
        If rcraft_hrs_before <> "" And istot <> "1" Then
            'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & rcraft_hrs_before & "</td>")
        Else
            'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">N\A</td>")
        End If
        If lab_sav_before <> "" And istot <> "1" Then
            'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">" & lab_sav_before & "</td>")
        Else
            'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">N\A</td>")
        End If

        'sb.Append("<td class=""plainlabel blackcntr"" bgcolor=""#eeeded"" align=""center"">" & rcraft_hrs & "</td>")
        'sb.Append("<td class=""plainlabel blackright"" bgcolor=""#eeeded"" align=""center"">" & lab_sav & "</td></tr>")

        'Resource Rollup
        'sb.Append("</table></td></tr>")
        'sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>")
        'sb.Append("<tr><td colspan=""3""><table cellSpacing=""0"" cellPadding=""3"" width=""420px"">")
        'sb.Append("<tr><td width=""110px""></td>")
        'sb.Append("<td width=""100px""></td>")
        'sb.Append("<td width=""110px""></td>")
        'sb.Append("<td width=""100px""></td>")
        'sb.Append("<td width=""110px""></td>")
        'sb.Append("<td width=""100px""></td></tr>")

        'sb.Append("<tr><td class=""label""><tr class=""label"" bgcolor=""#eeeded"">")
        'sb.Append("<td class=""blackleft blacktop"">" & tmod.getlbl("cdlbl877", "OVRollup.aspx.vb") & "<font class=""subscript8"">(" & tmod.getxlbl("xlb360", "OVRollup.aspx.vb") & ")</font></td>")
        'sb.Append("<td class=""blackleft blacktop"">" & tmod.getlbl("cdlbl878", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blackleft blacktop"">" & tmod.getlbl("cdlbl879", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blackleft blacktop"">" & tmod.getlbl("cdlbl880", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blackcntr blacktop"">" & tmod.getlbl("cdlbl881", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blackright blacktop"">" & tmod.getlbl("cdlbl882", "OVRollup.aspx.vb") & "</td></tr>")
        'Dim ot1, rt1, dt1 As Integer
        'ot = 0
        'rt = 0
        'dt = 0
        Dim rcnt As Integer = 0
        Dim rcntb As Integer = 0
        Dim ocnt As Integer = 0

        Select Case typ
            Case "eq"
                sql = "usp_optValAnlEq5 '" & eqid & "'"
            Case "site"
                sql = "usp_optValAnlEq5PS '" & sid & "'"
            Case "select"
                sql = "usp_optValAnlEq5PSS '" & sid & "', '" & eqstr & "'"
        End Select

        Try
            dr = ovr.GetRdrData(sql)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr1605", "OVRollup.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Response.Write("Not Enough Data To Create Overview")
        End Try
        Dim skillid As Integer
        Dim skillchk As Integer = 0
        If dr.HasRows Then
            While dr.Read
                rcnt += 1
                'skillid = dr.Item("skillid").ToString
                'ot = ot + dr.Item("OT").ToString
                'rt = rt + dr.Item("RT").ToString
                'dt = dt + dr.Item("diff").ToString
                skillstr += dr.Item("skill").ToString & ","
                ohour += dr.Item("ohr").ToString & ","
                rhour += dr.Item("rhr").ToString & ","
                rhourb += dr.Item("rhrb").ToString & ","
                If dr.Item("ohr").ToString <> "0" Then
                    ocnt += 1
                End If
                If dr.Item("rhr").ToString <> "0" Then
                    rcnt += 1
                End If
                If dr.Item("rhrb").ToString <> "0" Then
                    rcntb += 1
                End If
                If dr.Item("skill").ToString = "Operator" Then
                    skillchk = 1
                End If
                'sb.Append("<tr><td class=""plainlabel blackleft"" >" & dr.Item("skill").ToString & "</td>")
                'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("ohr").ToString & "</td>")
                If istot <> "1" Then
                    'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("rhrb").ToString & "</td>")
                    'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("diffhb").ToString & "</td>")
                Else
                    'sb.Append("<td class=""plainlabel blackleft"" align=""center"">N\A</td>")
                    'sb.Append("<td class=""plainlabel blackleft"" align=""center"">N\A</td>")
                End If

                'sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & dr.Item("rhr").ToString & "</td>")
                'sb.Append("<td class=""plainlabel blackright"" align=""center"">" & dr.Item("diffh").ToString & "</td></tr>")


            End While
        Else
            Dim strMessage As String = tmod.getmsg("cdstr1606", "OVRollup.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If


        dr.Close()

        'sb.Append("</table></td></tr>")
        'sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>")
        'sb.Append("<tr><td colspan=""3""><table cellSpacing=""0"" cellPadding=""3"" width=""420px"">")
        'sb.Append("<tr><td width=""110px""></td>")
        'sb.Append("<td width=""90px""></td>")
        'sb.Append("<td width=""110px""></td>")
        'sb.Append("<td width=""110px""></td></tr>")

        'sb.Append("<tr><td class=""label""><tr class=""label"">")
        'sb.Append("<td class=""blacktop blackleft"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl883", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blacktop blackleft"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl884", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blacktop blackleft"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl885", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blacktop blackleft"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl886", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blacktop blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl887", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""blacktop blackright"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl888", "OVRollup.aspx.vb") & "</td></tr>")

        Select Case typ
            Case "eq"
                sql = "usp_optValAnlEq3 '" & eqid & "'"
            Case "site"
                sql = "usp_optValAnlEq3PS '" & sid & "'"
            Case "select"
                sql = "usp_optValAnlEq3PSS '" & sid & "', '" & eqstr & "'"
        End Select

        Try
            dr = ovr.GetRdrData(sql)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr1607", "OVRollup.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Response.Write("Not Enough Data To Create Overview")
        End Try


        While dr.Read

            'sb.Append("<td class=""plainlabel blackleft"" >" & tmod.getlbl("cdlbl889", "OVRollup.aspx.vb") & "</td>")
            'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("oparts").ToString & "</td>")
            If istot <> "1" Then
                'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("partsb").ToString & "</td>")
                'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("partsdiffb").ToString & "</td>")
            Else
                'sb.Append("<td class=""plainlabel blackleft"" align=""center"">N\A</td>")
                'sb.Append("<td class=""plainlabel blackleft"" align=""center"">N\A</td>")
            End If

            'sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & dr.Item("parts").ToString & "</td>")
            'sb.Append("<td class=""plainlabel blackright"" align=""center"">" & dr.Item("partsdiff").ToString & "</td></tr>")

            'sb.Append("<td class=""plainlabel blackleft"">" & tmod.getlbl("cdlbl890", "OVRollup.aspx.vb") & "</td>")
            'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("otools").ToString & "</td>")
            If istot <> "1" Then
                'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("toolsb").ToString & "</td>")
                'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("toolsdiffb").ToString & "</td>")
            Else
                'sb.Append("<td class=""plainlabel blackleft"" align=""center"">N\A</td>")
                'sb.Append("<td class=""plainlabel blackleft"" align=""center"">N\A</td>")
            End If

            'sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & dr.Item("tools").ToString & "</td>")
            'sb.Append("<td class=""plainlabel blackright"" align=""center"">" & dr.Item("toolsdiff").ToString & "</td></tr>")

            'sb.Append("<td class=""plainlabel blackleft"">" & tmod.getlbl("cdlbl891", "OVRollup.aspx.vb") & "</td>")
            'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("olubes").ToString & "</td>")
            If istot <> "1" Then
                'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("lubesb").ToString & "</td>")
                'sb.Append("<td class=""plainlabel blackleft"" align=""center"">" & dr.Item("lubesdiffb").ToString & "</td>")
            Else
                'sb.Append("<td class=""plainlabel blackleft"" align=""center"">N\A</td>")
                'sb.Append("<td class=""plainlabel blackleft"" align=""center"">N\A</td>")
            End If

            'sb.Append("<td class=""plainlabel blackcntr"" align=""center"">" & dr.Item("lubes").ToString & "</td>")
            'sb.Append("<td class=""plainlabel blackright"" align=""center"">" & dr.Item("lubesdiff").ToString & "</td></tr>")


        End While
        dr.Close()

        'sb.Append("<tr bgcolor=""#eeeded""><td class=""label""><tr class=""label"">")
        'sb.Append("<td class=""labelibs blackleft"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl892", "OVRollup.aspx.vb") & "</td>")
        'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">$" & omat & "</td>")
        If istot <> "1" Then
            'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">$" & rmat_before & "</td>")
            'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">$" & mat_sav_before & "</td>")
        Else
            'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">N\A</td>")
            'sb.Append("<td class=""plainlabel blackleft"" bgcolor=""#eeeded"" align=""center"">N\A</td>")
        End If

        'sb.Append("<td class=""plainlabel blackcntr"" bgcolor=""#eeeded"" align=""center"">$" & rmat & "</td>")
        'sb.Append("<td class=""plainlabel blackright"" bgcolor=""#eeeded"" align=""center"">$" & mat_sav & "</td></tr>")

        'sb.Append("</table></td></tr>")
        'ovr.Dispose()
        'sb.Append("</Table>")



        'Second Column Here ************************************

        'sb.Append("</td>") 'End First Column **************************
        'sb.Append("<td valign=""top"" align=""center"">") 'Start Second Column **************************
        'sb.Append("1")
        'sb.Append("<table width=""250"" cellSpacing=""0"" cellPadding=""3"">")
        'sb.Append("<tr><td><table width=""250"" cellSpacing=""0"" cellPadding=""3"">")
        'sb.Append("<tr><td colspan=""2"" class=""bluelabel blacktop blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl893", "OVRollup.aspx.vb") & "</td></tr>")
        If lab_sav_before <> "" And istot <> "1" Then
            'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"" width=""90"">" & lab_sav_before & "</td>")
        ElseIf istot = "1" Then
            'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"" width=""90"">N\A</td>")
        Else
            'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"" width=""90"">0</td>")
        End If

        'sb.Append("<td class=""label blackright"" align=""center"" >" & tmod.getlbl("cdlbl895", "OVRollup.aspx.vb") & "</td></tr>")

        Try
            If istot <> "1" Then
                lab_per_before = System.Math.Round(((ocraft_hrs - rcraft_hrs_before) / ocraft_hrs) * 100)
                'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & lab_per_before & "%</td>")
            Else
                lab_per_before = "N\A"
                'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & lab_per_before & "</td>")
            End If

        Catch ex As Exception
            lab_per_before = "N\A"
            'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & lab_per_before & "</td>")
        End Try

        'sb.Append("<td class=""label blackright""  align=""center"" >" & tmod.getlbl("cdlbl896", "OVRollup.aspx.vb") & "</td></tr>")

        'sb.Append("<tr><td colspan=""2"" class=""bluelabel blacktop blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl897", "OVRollup.aspx.vb") & "</td></tr>")
        'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"" width=""90"">" & lab_sav & "</td>")
        'sb.Append("<td class=""label blackright"" align=""center"" >" & tmod.getlbl("cdlbl898", "OVRollup.aspx.vb") & "</td></tr>")

        Try
            lab_per = System.Math.Round(((ocraft_hrs - rcraft_hrs) / ocraft_hrs) * 100)
        Catch ex As Exception
            lab_per = "0%"
        End Try
        'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & lab_per & "%</td>")
        'sb.Append("<td class=""label blackright""  align=""center"" >" & tmod.getlbl("cdlbl899", "OVRollup.aspx.vb") & "</td></tr>")


        'sb.Append("<tr><td colspan=""2"" class=""bluelabel blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl900", "OVRollup.aspx.vb") & "</td></tr>")
        If istot <> "1" Then
            'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">$" & mat_sav_before & "</td>")
        Else
            'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">N\A</td>")
        End If

        'sb.Append("<td class=""label blackright"" align=""center"">" & tmod.getlbl("cdlbl901", "OVRollup.aspx.vb") & "</td></tr>")

        'sb.Append("<tr><td colspan=""2"" class=""bluelabel blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl902", "OVRollup.aspx.vb") & "</td></tr>")
        'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">$" & mat_sav & "</td>")
        'sb.Append("<td class=""label blackright"" align=""center"">" & tmod.getlbl("cdlbl903", "OVRollup.aspx.vb") & "</td></tr>")

        'Try
        'lab_per_before = System.Math.Round(((ocraft_hrs - rcraft_hrs_before) / ocraft_hrs) * 100)
        'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & lab_per_before & "%</td>")
        'Catch ex As Exception
        'lab_per_before = "N\A"
        'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & lab_per_before & "</td>")
        'End Try

        'sb.Append("<tr><td colspan=""2"" class=""bluelabel blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl904", "OVRollup.aspx.vb") & "</td></tr>")
        'If istot <> "1" Then
        'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & ordt_hrs & "</td>")
        'Else
        'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">N\A</td>")
        'End If

        'sb.Append("<td class=""label blackright"" align=""center"" >" & tmod.getlbl("cdlbl905", "OVRollup.aspx.vb") & "</td></tr>")

        If rdt_sav_before <> "" And istot <> "1" Then
            'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & rdt_sav_before & "</td>")
        Else
            'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">N\A</td>")
        End If

        'sb.Append("<td class=""label blackright"" align=""center"" >" & tmod.getlbl("cdlbl907", "OVRollup.aspx.vb") & "</td></tr>")

        Try
            lab_per = System.Math.Round(((ordt_hrs - (ordt_hrs - rdt_sav_before)) / ordt_hrs) * 100)
        Catch ex As Exception
            lab_per = "0%"
        End Try

        If istot <> "1" Then
            'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & lab_per & "%</td>")
        Else
            'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">N\A</td>")
        End If

        'sb.Append("<td class=""label blackright"" align=""center"" >Reduction in Down Time</td></tr>")

        'sb.Append("<tr><td colspan=""2"" class=""bluelabel blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl908", "OVRollup.aspx.vb") & "</td></tr>")
        'lose this
        'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & ordt_hrs & "</td>")
        'sb.Append("<td class=""label blackright"" align=""center"" >" & tmod.getlbl("cdlbl909", "OVRollup.aspx.vb") & "</td></tr>")
        '
        'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & rdt_sav & "</td>")
        'sb.Append("<td class=""label blackright"" align=""center"" >" & tmod.getlbl("cdlbl910", "OVRollup.aspx.vb") & "</td></tr>")


        Try
            lab_per = System.Math.Round(((ordt_hrs - (ordt_hrs - rdt_sav)) / ordt_hrs) * 100)
        Catch ex As Exception
            lab_per = "0%"
        End Try

        'sb.Append("<tr><td class=""bigbold blackcntr"" align=""center"">" & lab_per & "%</td>")
        'sb.Append("<td class=""label blackright"" align=""center"" >Reduction in Down Time</td></tr>")

        'sb.Append("<tr><td colspan=""2"" class=""bluelabel blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl911" , "OVRollup.aspx.vb") & "</td></tr>")
        Dim iht As Integer
        Dim rht As Integer
        If ocnt > 4 Then
            iht = ((ocnt - 4) * 18) + 122
        Else
            iht = 122
        End If
        If rcnt > 4 Then
            rht = ((rcnt - 4) * 18) + 122
        Else
            rht = 122
        End If

        If typ = "eq" Or typ = "site" Or typ = "select" Then
            Dim skcnt As Integer = 0
            Select Case typ
                Case "eq"
                    sql = "usp_optValAnlEq5_op1 '" & eqid & "'"
                Case "site"
                    sql = "usp_optValAnlEq5PS_op1 '" & sid & "'"
                Case "select"
                    sql = "usp_optValAnlEq5PSS_op1 '" & sid & "', '" & eqstr & "'"
            End Select
            'sql = "usp_optValAnlEq5_op1 '" & eqid & "'"
            dr = ovr.GetRdrData(sql)
            Dim ohouri As Integer = 0
            Dim rhouri As Integer = 0
            While dr.Read
                skcnt += 1
                ohouri += dr.Item("ohr").ToString
                rhouri += dr.Item("rhr").ToString
            End While
            dr.Close()

            If skcnt > 0 Then
                skillstr += "OP Care,"
                ohour += ohouri & ","
                rhour += rhouri & ","

            End If
        End If

        ovr.Dispose()
        If ocnt > 0 And rcnt > 0 Then
            'iht = 150
            'rht = 150
            'sb.Append("<tr><td colspan=""2""  class="""">") 'blackcntr
            'sb.Append("<div>")
            'sb.Append("<iframe width=""300"" height=" & iht & " frameBorder=""no"" scrolling=""no"" src=""../appsman/PMPie3D.aspx?title=Original&skills=" & skillstr & "&hrs=" & ohour & """>")
            'sb.Append("</div>")
            'sb.Append("<br>HELLO<br>")
            'sb.Append("<iframe width=""300"" height=" & rht & " frameBorder=""no"" scrolling=""no"" src=""../appsman/PMPie3D.aspx?title=Optimized&skills=" & skillstr & "&hrs=" & rhour & """>")
            'sb.Append("<br>HELLO<br>")
            'sb.Append("</td></tr>")

            'sb.Append("<tr><td colspan=""2"" class=""bluelabel blackcntr"" bgcolor=""#eeeded"">" & tmod.getlbl("cdlbl912" , "OVRollup.aspx.vb") & "</td></tr>")

            'sb.Append("<tr><td colspan=""2"" class="""">HELLO") 'blackcntr
            'sb.Append("<iframe runat=""server"" width=""280"" height=" & rht & " frameBorder=""no"" scrolling=""no"" src=""../appsman/PMPie3D.aspx?title=Optimized&skills=" & skillstr & "&hrs=" & rhour & """>")
            'sb.Append("</td></tr>")
        End If



        'sb.Append("</table></td></tr>")
        'If skillchk <> 0 Then



        'Else
        'sb.Append("</table>")
        'End If

        'sb.Append("</td>") 'End Second Column **************************
        'sb.Append("</tr></table>") 'End Multi-Column Table **************************
        'sb.Append("</tr></table>")
        'sb.Append("<iframe width=""280"" height=" & rht & " frameBorder=""no"" scrolling=""no"" src=""../appsman/PMPie3D.aspx?title=Optimized&skills=" & skillstr & "&hrs=" & rhour & """>")
        'sb.Append("<table border=""1"">")



        'sb.Append("</table>")

        'sb.Append("<iframe width=""300"" height=" & rht & " frameBorder=""no"" scrolling=""no"" src=""../appsman/PMPie3D.aspx?title=Optimized&skills=" & skillstr & "&hrs=" & rhour & """>")
        sb.Append("<iframe width=""300"" height=" & iht & " frameBorder=""no"" scrolling=""no"" src=""../appsman/PMPie3D.aspx?title=Original&skills=" & skillstr & "&hrs=" & ohour & """>")
        sb.Append("</body></html>")

        Response.Write(sb)

    End Sub
End Class