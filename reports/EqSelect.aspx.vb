

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class EqSelect
    Inherits System.Web.UI.Page
	Protected WithEvents btnaddtosite As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang3330 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3329 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3328 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3327 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3326 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim fail As New Utilities
    Dim eqid, sid, eqstr, Login, who As String
    Protected WithEvents lbleqstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbfailmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents cbopts As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents ibtnret As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblopt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

      
	GetFSOVLIBS()
  GetFSLangs()



Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            who = Request.QueryString("who").ToString
            lblwho.Value = who
            If who <> "rrep" Then
                lblopt.Value = "0"
                lbfailmodes.AutoPostBack = False
                lbfailmaster.AutoPostBack = False
            Else
                cbopts.SelectedValue = "1"
                lblopt.Value = "1"
                lbfailmodes.AutoPostBack = True
                lbfailmaster.AutoPostBack = True
                cbopts.Enabled = False
            End If
            fail.Open()
            PopFail(sid)
            PopFailList(sid)
            fail.Dispose()
        End If
        'ibtnret.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/submithov.gif'")
        'ibtnret.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/submit.gif'")
        'btntocomp.Attributes.Add("onclick", "DisableButton(this);")
        'btnfromcomp.Attributes.Add("onclick", "DisableButton(this);")
    End Sub
    Private Sub PopFailList(ByVal sid As String)

        eqstr = lbleqstr.Value
        If Len(eqstr) = 0 Then eqstr = "''"
        sql = "select eqid, eqnum from equipment where siteid = '" & sid & "' and eqid in (" & eqstr & ") order by eqnum"
        dr = fail.GetRdrData(sql)
        lbfailmodes.DataSource = dr
        lbfailmodes.DataValueField = "eqid"
        lbfailmodes.DataTextField = "eqnum"
        Try
            lbfailmodes.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        Try
            lbfailmodes.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub

    Private Sub PopFail(ByVal sid As String)
        Dim chk As String = lblfailchk.Value
        eqstr = lbleqstr.Value
        If Len(eqstr) = 0 Then eqstr = "''"
        sql = "select eqid, eqnum from equipment where siteid = '" & sid & "' and eqid not in (" & eqstr & ") order by eqnum"
        dr = fail.GetRdrData(sql)
        lbfailmodes.DataSource = dr
        lbfailmaster.DataSource = dr
        lbfailmaster.DataTextField = "eqnum"
        lbfailmaster.DataValueField = "eqid"
        lbfailmaster.DataBind()
        dr.Close()
    End Sub

    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToComp()
    End Sub
    Private Sub ToComp()
        'fail.Dispose()
        who = lblwho.Value
        If who = "rrep" Then
            lbleqstr.Value = ""
        End If
        sid = lblsid.Value
        Dim Item As ListItem
        Dim f, fi As String
        fail.Open()
       
        For Each Item In lbfailmaster.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                If Len(lbleqstr.Value) = 0 Then
                    lbleqstr.Value = "'" & fi & "'"
                Else
                    lbleqstr.Value += ", '" & fi & "'"
                End If
            End If
        Next
        PopFailList(sid)
        PopFail(sid)
        fail.Dispose()
    End Sub


    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        FromComp()
    End Sub
    Private Sub FromComp()
        'fail.Dispose()
        sid = lblsid.Value
        Dim Item As ListItem
        Dim f, fi As String
        fail.Open()
        Dim cnt As Integer = lbfailmodes.Items.Count
        If cnt = 1 Then
            Try
                lbfailmodes.SelectedIndex = 0
            Catch ex As Exception

            End Try

        End If
        lbleqstr.Value = ""
        For Each Item In lbfailmodes.Items
            If Not Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                If Len(lbleqstr.Value) = 0 Then
                    lbleqstr.Value = "'" & fi & "'"
                Else
                    lbleqstr.Value += ", '" & fi & "'"
                End If
                'Dim remitem As ListItem = New ListItem(f, fi)
                'If lbfailmodes.Items.Contains(remitem) Then
                'lbfailmodes.Items.Remove(remitem)
                'End If
            End If
        Next
        'ReFill()
        PopFailList(sid)
        PopFail(sid)
        fail.Dispose()
    End Sub
    Private Sub ReFill()
        Dim Item As ListItem
        Dim f, fi As String
        lbleqstr.Value = ""
        For Each Item In lbfailmodes.Items
            'If Item.Selected Then
            f = Item.Text.ToString
            fi = Item.Value.ToString
            If Len(lbleqstr.Value) = 0 Then
                lbleqstr.Value = "'" & fi & "'"
            Else
                lbleqstr.Value += ", '" & fi & "'"
            End If
            'End If
        Next
    End Sub

  

    Private Sub lbfailmaster_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbfailmaster.SelectedIndexChanged
        If lblopt.Value = "1" Then
            ToComp()
        End If
    End Sub

    Private Sub lbfailmodes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbfailmodes.SelectedIndexChanged
        If lblopt.Value = "1" Then
            FromComp()
        End If
    End Sub

    Private Sub cbopts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbopts.SelectedIndexChanged
        Dim opt As String = cbopts.SelectedValue.ToString
        lblopt.Value = opt
        If opt = "0" Then
            lbfailmodes.AutoPostBack = False
            lbfailmaster.AutoPostBack = False
        Else
            lbfailmodes.AutoPostBack = True
            lbfailmaster.AutoPostBack = True
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3326.Text = axlabs.GetASPXPage("EqSelect.aspx", "lang3326")
        Catch ex As Exception
        End Try
        Try
            lang3327.Text = axlabs.GetASPXPage("EqSelect.aspx", "lang3327")
        Catch ex As Exception
        End Try
        Try
            lang3328.Text = axlabs.GetASPXPage("EqSelect.aspx", "lang3328")
        Catch ex As Exception
        End Try
        Try
            lang3329.Text = axlabs.GetASPXPage("EqSelect.aspx", "lang3329")
        Catch ex As Exception
        End Try
        Try
            lang3330.Text = axlabs.GetASPXPage("EqSelect.aspx", "lang3330")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                ibtnret.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                ibtnret.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                ibtnret.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                ibtnret.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "spa" Then
                ibtnret.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddtosite.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("EqSelect.aspx", "btnaddtosite") & "')")
            btnaddtosite.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
