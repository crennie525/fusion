﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="eqdownhist.aspx.vb" Inherits="lucy_r12.eqdownhist" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Equipment Down History</title>
<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <style type="text/css">
        .replabeltop
        {
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 16px;
            color: Black;
            font-weight: bold;
        }
        .replabeltops
        {
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 14px;
            color: Black;
            font-weight: bold;
        }
        .replabelplain
        {
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 13px;
            color: Black;
        }
        .gbox
        {
            border-bottom: solid 1px black;
            border-left: solid 1px black;
            border-top: solid 1px black;
            border-right: solid 1px black;
            background-color: Silver;
        }
        .tboxl1
        {
            border-left: solid 1px black;
            border-top: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            font-weight: bold;
            color: Black;
            text-align: left;
        }
        .tboxl
        {
            border-left: solid 1px black;
            border-top: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            font-weight: bold;
            color: Black;
            text-align: center;
        }
        .tboxr
        {
            border-left: solid 1px black;
            border-top: solid 1px black;
            border-bottom: solid 1px black;
            border-right: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            font-weight: bold;
            color: Black;
            text-align: center;
        }
        .rboxl1
        {
            border-left: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            color: Black;
            text-align: left;
        }
        .rboxl1r
        {
            border-left: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            color: Red;
            text-align: left;
        }
        .rboxl
        {
            border-left: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            color: Black;
            text-align: center;
        }
        .rboxr
        {
            border-left: solid 1px black;
            border-right: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            color: Black;
            text-align: center;
        }
        .rboxrr
        {
            border-left: solid 1px black;
            border-right: solid 1px black;
            border-bottom: solid 1px black;
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            color: Red;
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divrep" runat="server">
    </div>
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lblfdate" runat="server" />
    <input type="hidden" id="lbltdate" runat="server" />
    </form>
</body>
</html>