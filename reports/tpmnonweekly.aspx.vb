

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class tpmnonweekly
    Inherits System.Web.UI.Page
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim eqid As String
    Dim tpmw As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim tmod As New transmod
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            tpmw.Open()
            GetWeek(eqid)
            tpmw.Dispose()
        End If
    End Sub
    Private Sub GetWeek(ByVal eqid As String)
        Dim shift1, shift2, shift3 As String
        Dim lubes, parts, tools, meas, fm1 As String
        Dim eqnum, eqdesc, funcid, func, comid, compnum, compdesc, spl, desig, freq, pics As String
        Dim pmtskid, tasknum, subtask, taskdesc As String
        Dim subcnt As Integer
        Dim subchk As Integer = 0
        Dim taskhold As Integer = 1
        Dim estart As Integer = 0
        Dim tstart As Integer = 0
        Dim pstart1 As Integer = 0
        Dim wkstr As String

        Dim freqhold, funchold, comhold As String
        freqhold = ""
        funchold = ""
        comhold = ""

        Dim sb As New StringBuilder
        sb.Append("<table style=""page-break-after:always;"" border=""0"">")
        sb.Append("<tr>")
        sb.Append("<td colspan=""2"">")

        Dim dt As DataTable = New DataTable("tbllist")
        Dim dtcol As DataColumn
        Dim dtrow As DataRow

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "task"
        dt.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "pics"
        dt.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "freq"
        dt.Columns.Add(dtcol)

        sql = "usp_getWITotalTPMEL_rep2 '" & eqid & "'"

        dr = tpmw.GetRdrData(sql)
        While dr.Read
            tstart = 1
            freq = dr.Item("freq").ToString

            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            funcid = dr.Item("funcid").ToString
            func = dr.Item("func").ToString
            comid = dr.Item("comid").ToString
            compnum = dr.Item("compnum").ToString
            compdesc = dr.Item("compdesc").ToString
            spl = dr.Item("spl").ToString
            desig = dr.Item("desig").ToString
            funcid = dr.Item("funcid").ToString
            func = dr.Item("func").ToString

            pmtskid = dr.Item("pmtskid").ToString
            tasknum = dr.Item("tasknum").ToString
            subtask = dr.Item("subtask").ToString

            pics = dr.Item("pics").ToString




            If subtask = "0" Then
                If freq <> freqhold Then
                    freqhold = freq
                    taskhold = 1
                Else
                    taskhold += 1
                End If
                subcnt = dr.Item("subcnt").ToString
                If subcnt = 0 Then
                    subchk = 0
                Else
                    subchk = subcnt
                End If
                If pics <> "" Then
                    pstart1 = 1
                    Dim picarr() As String = pics.Split(",")
                    Dim i As Integer
                    For i = 0 To picarr.Length - 1
                        dtrow = dt.NewRow()
                        dtrow("task") = taskhold
                        dtrow("pics") = picarr(i)
                        dtrow("freq") = freq
                        dt.Rows.Add(dtrow)
                    Next


                End If
                shift1 = dr.Item("shift1").ToString
                shift2 = dr.Item("shift2").ToString
                shift3 = dr.Item("shift3").ToString
                
            End If

            taskdesc = dr.Item("taskdesc").ToString

            lubes = dr.Item("lubes").ToString
            parts = dr.Item("parts").ToString
            tools = dr.Item("tools").ToString
            meas = dr.Item("meas").ToString
            fm1 = dr.Item("fm1").ToString

            ' Task Details

            If subtask = "0" Then

                sb.Append("<table border=""0"" width=""800"" cellspacing=""0"" cellpadding=""3"" >") 'main table start
                If estart = 0 Then
                    estart = 1

                    sb.Append("<tr>")
                    sb.Append("<td colspan=""2"">")
                    sb.Append("<table width=""800"">") 'hdr table start

                    sb.Append("<tr>")
                    sb.Append("<td align=""center"" class=""replabeltop"" colspan=""2"">" & tmod.getlbl("cdlbl1213" , "tpmnonweekly.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("</tr>")

                    sb.Append("<tr>")
                    sb.Append("<td class=""replabeltop"" width=""100"">" & tmod.getlbl("cdlbl1214" , "tpmnonweekly.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabeltopplain"" width=""700"">" & eqnum)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabeltop"">" & tmod.getlbl("cdlbl1215" , "tpmnonweekly.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabeltopplain"">" & eqdesc)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                    sb.Append("</table>") 'hdr table end
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If
                If taskhold = 1 Then
                    sb.Append("<tr>")
                    sb.Append("<td align=""left"" class=""replabel"" colspan=""2"">" & freq)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If
                sb.Append("<tr>")
                sb.Append("<td width=""610"" style=""border-top: solid 1px gray;"" valign=""top"">") 'Info Tab

                sb.Append("<table cellspacing=""3"" cellpadding=""3"" width=""610"" border=""0"">") 'Info Table Start

                sb.Append("<tr>")
                sb.Append("<td class=""replabel"" width=""100"" >" & tmod.getlbl("cdlbl1216" , "tpmnonweekly.aspx.vb"))
                sb.Append("</td>")
                sb.Append("<td class=""replabelplain"" width=""510"">")
                sb.Append(taskhold)
                sb.Append("</td>")
                sb.Append("</tr>")

                sb.Append("<tr>")
                sb.Append("<td class=""replabel"">" & tmod.getlbl("cdlbl1217" , "tpmnonweekly.aspx.vb"))
                sb.Append("</td>")
                sb.Append("<td class=""replabelplain"">")
                sb.Append(func)
                sb.Append("</td>")
                sb.Append("</tr>")

                sb.Append("<tr>")
                sb.Append("<td class=""replabel"">" & tmod.getlbl("cdlbl1218" , "tpmnonweekly.aspx.vb"))
                sb.Append("</td>")
                sb.Append("<td class=""replabelplain"">")
                sb.Append(compnum)
                sb.Append("</td>")
                sb.Append("</tr>")

                If compdesc <> "" Then
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">" & tmod.getxlbl("xlb443" , "tpmnonweekly.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(compdesc)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                If spl <> "" Then
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">SPL:")
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(spl)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                If desig <> "" Then
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">" & tmod.getlbl("cdlbl1219" , "tpmnonweekly.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(desig)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                sb.Append("</table>") 'End Info Table
                sb.Append("</td>") 'End Info Tab

                ' Week Days
                sb.Append("<td width=""190"" valign=""top"" align=""right"" rowspan=""1""  style=""border-top: solid 1px gray;"">")

                wkstr = GetWeekTable(shift1, shift2, shift3)

                sb.Append(wkstr)

                sb.Append("</td>")
                ' End Week Days

                sb.Append("</tr>")
            End If


            ' Task Guts Row
            If subtask = "0" Then
                sb.Append("<tr>")
                sb.Append("<td colspan=""2"">")
                sb.Append("<table width=""800"">") 'Task Guts Table Start

                sb.Append("<tr>")
                sb.Append("<td class=""replabel"" colspan=""2"">" & tmod.getlbl("cdlbl1220" , "tpmnonweekly.aspx.vb"))
                sb.Append("</td>")
                sb.Append("</tr>")
                sb.Append("<tr>")
                sb.Append("<td class=""replabelplain"" colspan=""2"">")
                sb.Append(taskdesc)
                sb.Append("</td>")
                sb.Append("</tr>")

                sb.Append("<tr>")
                sb.Append("<td class=""replabel"" width=""120"">" & tmod.getlbl("cdlbl1221" , "tpmnonweekly.aspx.vb"))
                sb.Append("</td>")
                sb.Append("<td class=""replabelplain"" width=""490"">")
                sb.Append(fm1)
                sb.Append("</td>")
                sb.Append("</tr>")

                If lubes <> "" And meas <> "none" Then  'And lubes <> "None"
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">" & tmod.getlbl("cdlbl1222" , "tpmnonweekly.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(lubes)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                If tools <> "" And meas <> "none" Then  'And tools <> "None"
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">" & tmod.getlbl("cdlbl1223" , "tpmnonweekly.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(tools)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                If parts <> "" And meas <> "none" Then  'And parts <> "None"
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">" & tmod.getlbl("cdlbl1224" , "tpmnonweekly.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(parts)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                If meas <> "" And meas <> "none" Then  'And meas <> "None"
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">" & tmod.getlbl("cdlbl1225" , "tpmnonweekly.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(meas)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If


            Else
                ' Sub Task Details

                sb.Append("<tr>")
                sb.Append("<td class=""replabelplain"">&nbsp;&nbsp;<b>" & tmod.getxlbl("xlb444" , "tpmnonweekly.aspx.vb") & "</b> [" & subtask & "]:")
                sb.Append("</td>")
                sb.Append("<td class=""replabelplain"" align=""left"">")
                sb.Append(taskdesc)
                sb.Append("</td>")
                sb.Append("</tr>")

                If lubes <> "" And meas <> "none" Then  'And lubes <> "None"
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">&nbsp;&nbsp;" & tmod.getxlbl("xlb445" , "tpmnonweekly.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(lubes)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                If tools <> "" And meas <> "none" Then  'And tools <> "None"
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">&nbsp;&nbsp;" & tmod.getxlbl("xlb446" , "tpmnonweekly.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(tools)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                If parts <> "" And meas <> "none" Then   'And parts <> "None"
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">&nbsp;&nbsp;" & tmod.getxlbl("xlb447" , "tpmnonweekly.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(parts)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If

                If meas <> "" And meas <> "none" Then  '
                    sb.Append("<tr>")
                    sb.Append("<td class=""replabel"">&nbsp;&nbsp;" & tmod.getxlbl("xlb448" , "tpmnonweekly.aspx.vb"))
                    sb.Append("</td>")
                    sb.Append("<td class=""replabelplain"">")
                    sb.Append(meas)
                    sb.Append("</td>")
                    sb.Append("</tr>")
                End If


            End If

            If subcnt = 0 Then  'subtask = "0" And
                sb.Append("<tr>")
                sb.Append("<td>&nbsp;")
                sb.Append("</td>")
                sb.Append("</tr>")

                sb.Append("</table>") 'End Task Guts Table

                'main end was here
            Else
                subcnt = subcnt - 1

                
            End If

        End While
        dr.Close()

        'sb.Append("<tr>")
        'sb.Append("<td>&nbsp;")
        'sb.Append("</td>")
        'sb.Append("</tr>")

        'sb.Append("</table>") 'Main Table End

        sb.Append("</td>")
        sb.Append("</tr>")
        sb.Append("</table>") ' Big Table End

        If tstart = 1 Then
            If pstart1 <> 0 Then
                sb.Append("<table width=""800"" border=""0"">")
                sb.Append("<tr>")
                sb.Append("<td align=""center"" class=""replabeltop"">" & tmod.getlbl("cdlbl1226" , "tpmnonweekly.aspx.vb"))
                sb.Append("</td>")
                sb.Append("</tr>")
                sb.Append("<tr>")
                sb.Append("<td>&nbsp;")
                sb.Append("</td>")
                sb.Append("</tr>")

                Dim ds As New DataSet
                ds.Tables.Add(dt)
                Dim task, pic, pfreq As String
                Dim holdtask As String = "0"
                Dim taskflag As Integer = 0
                Dim pstart As Integer = 0
                Dim drM As DataRow
                For Each drM In dt.Select()
                    task = drM("task").ToString
                    pfreq = drM("freq").ToString
                    If task <> holdtask Then
                        holdtask = task
                        taskflag = 0
                    Else
                        taskflag = 1
                    End If
                    pic = drM("pics").ToString

                    If taskflag = 0 Then
                        If pstart = 0 Then
                            pstart = 1
                        Else
                            sb.Append("</td>")
                            sb.Append("</tr>")
                        End If
                        sb.Append("<tr>")
                        sb.Append("<td align=""center"" class=""replabel"" width=""800"">" & pfreq)
                        sb.Append("</td>")
                        sb.Append("</tr>")
                        sb.Append("<tr>")
                        sb.Append("<td align=""center"" class=""replabel"" width=""800"">" & tmod.getxlbl("xlb449" , "tpmnonweekly.aspx.vb") & " " & task)
                        sb.Append("</td>")
                        sb.Append("</tr>")
                        sb.Append("<tr>")
                        sb.Append("<td align=""center"" width=""800"">")
                    End If

                    sb.Append("<img src=""" & pic & """>")
                Next
                sb.Append("</td>")
                sb.Append("</tr>")

                sb.Append("</table>")
            End If
            
            Response.Write(sb.ToString)
        Else
            'sb.Append("<table width=""800"">")
            'sb.Append("<tr>")
            'sb.Append("<td class=""replabeltop"" align=""center"">")
            sb.Append("<b>" & tmod.getxlbl("xlb450" , "tpmnonweekly.aspx.vb") & "</b>")
            sb.Append("</td>")
            sb.Append("</tr>")
            sb.Append("</table>")
            Response.Write(sb.ToString)
        End If


    End Sub
    Private Function GetWeekTable(ByVal shift1 As String, ByVal shift2 As String, ByVal shift3 As String) As String

        Dim sb As New StringBuilder
        Dim bclass As String
        Dim ret As String

        sb.Append("<table border=""0"">")
        sb.Append("<tr height=""20"">")
        If shift1 <> "" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        sb.Append("<td class=""replabel"" width=""60"">1st Shift</td>")
        sb.Append("</tr>")
        sb.Append("<tr height=""20"">")
        If shift2 <> "" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        sb.Append("<td class=""replabel"" width=""60"">2nd Shift</td>")
        sb.Append("</tr>")
        sb.Append("<tr height=""20"">")
        If shift3 <> "" Then bclass = "gbox" Else bclass = "box"
        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
        sb.Append("<td class=""replabel"" width=""60"">3rd Shift</td>")
        sb.Append("</tr>")
        sb.Append("</table>")

        ret = sb.ToString
        Return ret
    End Function
End Class
