

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class CustomCMMShtmlTPMpics
    Inherits System.Web.UI.Page
	Protected WithEvents lang3317 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3316 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3315 As System.Web.UI.WebControls.Label

    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim eqid, mode As String
    Protected WithEvents tdwi As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdparts As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtools As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlubes As System.Web.UI.HtmlControls.HtmlTableCell
    Dim tmod As New transmod
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            mode = Request.QueryString("mode").ToString
            pms.Open()
            PopWI(eqid, mode)
            'PopParts(eqid, mode)
            'PopTools(eqid, mode)
            'PopLubes(eqid, mode)
            pms.Dispose()

        End If
    End Sub
    Private Sub PopWI(ByVal eqid As String, ByVal mode As String)
        Dim fid As String = ""
        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim funcchk As String = ""
        Dim func As String = ""
        Dim subtask As String = ""
        Dim pics As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""580""></td></tr>")
        If mode = "1" Then
            sql = "usp_getWITotalTPMpics '" & eqid & "'"
        Else
            sql = "usp_getWITotalPGEMSPdM '" & eqid & "'"
        End If
        Dim parts, tools, lubes As String
        parts = ""
        tools = ""
        lubes = ""
        dr = pms.GetRdrData(sql)
        Dim pretech, pretechchk As String
        While dr.Read
            skill = "Operator / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString
            'pretech = dr.Item("ptid").ToString
            Dim skchk, skchk2 As String
            skchk = skill.ToLower
            skchk2 = skillchk.ToLower
            If skchk <> skchk2 Then 'OrElse pretech <> pretechchk Then
                If skillchk <> "" Then
                    sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl760" , "CustomCMMShtmlTPMpics.aspx.vb") & "</td></tr>")
                    Dim partarr As String() = Split(parts, ";")
                    Dim p As Integer
                    For p = 0 To partarr.Length - 1
                        If partarr(p) = "none" Then 'Len(partarr(p)) = 0 Or
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & tmod.getlbl("cdlbl761" , "CustomCMMShtmlTPMpics.aspx.vb") & "</td></tr>")
                        Else
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & partarr(p) & "</td></tr>")
                        End If
                    Next

                    sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl762" , "CustomCMMShtmlTPMpics.aspx.vb") & "</td></tr>")
                    Dim toolarr As String() = Split(tools, ";")
                    Dim t As Integer
                    For t = 0 To toolarr.Length - 1
                        If toolarr(t) = "none" Then 'Len(toolarr(t)) = 0 Or 
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & tmod.getlbl("cdlbl763" , "CustomCMMShtmlTPMpics.aspx.vb") & "</td></tr>")
                        Else
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & toolarr(t) & "</td></tr>")
                        End If
                    Next

                    sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl764" , "CustomCMMShtmlTPMpics.aspx.vb") & "</td></tr>")
                    Dim lubearr As String() = Split(lubes, ";")
                    Dim l As Integer
                    For l = 0 To lubearr.Length - 1
                        If lubearr(l) = "none" Then 'Len(lubearr(l)) = 0 Or 
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & tmod.getlbl("cdlbl765" , "CustomCMMShtmlTPMpics.aspx.vb") & "</td></tr>")
                        Else
                            sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & lubearr(l) & "</td></tr>")
                        End If

                    Next
                    parts = ""
                    tools = ""
                    lubes = ""
                    sb.Append("<tr><td colspan=""3"" style=""border-bottom: 1px solid black;"">&nbsp;</td>")
                    sb.Append("<tr><td>&nbsp;</td></tr>")
                End If
                skillchk = skill
                start = 0
                flag = 0
                If mode = "1" Then
                    sb.Append("<tr><td class=""bigbold"" colspan=""3"">" _
                    + "" & dr.Item("freq").ToString & " / " & tmod.getxlbl("xlb318" , "CustomCMMShtmlTPMpics.aspx.vb") & " / " & dr.Item("rd").ToString & "</td></tr>")
                Else
                    pretechchk = pretech
                    sb.Append("<tr><td class=""bigbold"" colspan=""3"">" _
                    + "PdM - " & dr.Item("pretech").ToString & " - " & dr.Item("freq").ToString & " / " & dr.Item("skill").ToString & " / " & dr.Item("rd").ToString & "</td></tr>")
                End If

                sb.Append("<tr><td class=""label"" colspan=""3"">" & tmod.getxlbl("xlb319" , "CustomCMMShtmlTPMpics.aspx.vb") & "  " & dr.Item("down").ToString & " Minutes</td></tr>")
                sb.Append("<tr><td class=""label"" colspan=""3"">" & dr.Item("eqnum").ToString & " - " & dr.Item("eqdesc").ToString & "</td></tr>")
                sb.Append("<tr><td>&nbsp;</td></tr>")
                sb.Append("<tr><TD class=""label"" colspan=""3"" bgcolor=""silver"">" & tmod.getxlbl("xlb320" , "CustomCMMShtmlTPMpics.aspx.vb") & "</TD></tr>")
                sb.Append("<tr><td>&nbsp;</td></tr>")
            End If
            func = dr.Item("func").ToString
            If flag = 0 Then
                flag = 1
                funcchk = func
                sb.Append("<tr><td class=""labelu"" colspan=""3"">" & func & "</td></tr>")
            Else
                If func <> funcchk Then
                    funcchk = func
                    sb.Append("<tr><td colspan=""3"" style=""border-bottom: 1px solid black;"">&nbsp;</td>")
                    sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""labelu"" colspan=""3"">" & func & "</td></tr>")
                End If
            End If
            Dim task, lube, meas As String
            task = dr.Item("task").ToString
            If dr.Item("lubes").ToString <> "none" Or Len(dr.Item("lubes").ToString) = 0 Then
                lube = "; " & dr.Item("lubes").ToString
            Else
                lube = ""
            End If
            'If dr.Item("lube").ToString <> "none" Then
            'lube = "; " & dr.Item("lube").ToString
            'Else
            'lube = ""
            'End If
            If dr.Item("meas").ToString <> "none" Then
                meas = "; " & dr.Item("meas").ToString
            Else
                meas = ""
            End If
            subtask = dr.Item("subtask").ToString
            task = task & lube & meas

            If subtask = "0" Then
                sb.Append("<tr><td class=""plainlabel"">[" & dr.Item("tasknum").ToString & "]</td>")
                If Len(dr.Item("task").ToString) > 0 Then
                    sb.Append("<td colspan=""2"" class=""plainlabel"">" & task & "</td></tr>")
                Else
                    sb.Append("<td colspan=""2"" class=""plainlabel"">" & tmod.getlbl("cdlbl766" , "CustomCMMShtmlTPMpics.aspx.vb") & "</td></tr>")
                End If
                sb.Append("<tr><td></td>")
                sb.Append("<td colspan=""2"" class=""plainlabel"">OK(___) " & dr.Item("fm1").ToString & "</td></tr>")
            Else
                sb.Append("<tr><td></td><td class=""plainlabel"">[" & subtask & "]</td>")
                If Len(dr.Item("subt").ToString) > 0 Then
                    sb.Append("<td class=""plainlabel"">" & dr.Item("subt").ToString & "</td></tr>")
                Else
                    sb.Append("<td class=""plainlabel"">" & tmod.getlbl("cdlbl767" , "CustomCMMShtmlTPMpics.aspx.vb") & "</td></tr>")
                End If
                sb.Append("<tr><td></td><td></td>")
                sb.Append("<td class=""plainlabel"">OK(___)</td></tr>")
            End If

            pics = dr.Item("pics").ToString
            Dim pt, ptr, ptd As Integer
            ptr = 0
            ptd = 0
            If pics <> "" Then
                Dim picstr() As String = pics.Split(",")
                sb.Append("<tr><td colspan=""3"" align=""center""><table>")
                For pt = 0 To picstr.Length - 1
                    Dim ptst As String = picstr(pt)
                    If ptr = 0 And ptd = 0 Then
                        ptr = 1
                        ptd = 1
                        sb.Append("<tr><td width=""220"" align=""center""><img src=""" + picstr(pt) + """></td>")
                        sb.Append("<td width=""10""></td>")
                    ElseIf ptr = 1 And ptd = 1 Then
                        ptr = 0
                        ptd = 0
                        sb.Append("<td width=""220"" align=""center""><img src=""" + picstr(pt) + """></td></tr>")
                    End If

                Next
                sb.Append("</table></td></tr>")
            End If

            If dr.Item("parts").ToString <> "none" Then 'Or Len(dr.Item("parts").ToString) = 0 Then
                If Len(parts) = 0 Then
                    parts += dr.Item("parts").ToString
                Else
                    parts += ";" & dr.Item("parts").ToString
                End If
            End If

            If dr.Item("tools").ToString <> "none" Then 'Or Len(dr.Item("tools").ToString) <> 0 Then
                If Len(tools) = 0 Then
                    tools += dr.Item("tools").ToString
                Else
                    tools += ";" & dr.Item("tools").ToString
                End If

            End If

            If dr.Item("lubes").ToString <> "none" Then 'Or Len(dr.Item("lubes").ToString) <> 0 Then
                If Len(lubes) = 0 Then
                    lubes += dr.Item("lubes").ToString
                Else
                    lubes += ";" & dr.Item("lubes").ToString
                End If

            End If

        End While
        dr.Close()

        sb.Append("<tr><td>&nbsp;</td></tr>")
        sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl768" , "CustomCMMShtmlTPMpics.aspx.vb") & "</td></tr>")
        Dim partarr1 As String() = Split(parts, ";")
        Dim p1 As Integer
        For p1 = 0 To partarr1.Length - 1
            If Len(partarr1(p1)) = 0 Or partarr1(p1) = "none" Then
                Dim test As String = partarr1(p1)
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & tmod.getlbl("cdlbl769" , "CustomCMMShtmlTPMpics.aspx.vb") & "</td></tr>")
            Else
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & partarr1(p1) & "</td></tr>")
            End If
        Next

        sb.Append("<tr><td>&nbsp;</td></tr>")
        sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl770" , "CustomCMMShtmlTPMpics.aspx.vb") & "</td></tr>")
        Dim toolarr1 As String() = Split(tools, ";")
        Dim t1 As Integer
        For t1 = 0 To toolarr1.Length - 1
            If Len(toolarr1(t1)) = 0 Or toolarr1(t1) = "none" Then
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & tmod.getlbl("cdlbl771" , "CustomCMMShtmlTPMpics.aspx.vb") & "</td></tr>")
            Else
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & toolarr1(t1) & "</td></tr>")
            End If
        Next

        sb.Append("<tr><td>&nbsp;</td></tr>")
        sb.Append("<tr><td class=""label"" bgcolor=""silver""  colspan=""3"">" & tmod.getlbl("cdlbl772" , "CustomCMMShtmlTPMpics.aspx.vb") & "</td></tr>")
        Dim lubearr1 As String() = Split(lubes, ";")
        Dim l1 As Integer
        For l1 = 0 To lubearr1.Length - 1
            If Len(lubearr1(l1)) = 0 Or lubearr1(l1) = "none" Then
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & tmod.getlbl("cdlbl773" , "CustomCMMShtmlTPMpics.aspx.vb") & "</td></tr>")
            Else
                sb.Append("<tr><td class=""plainlabel""  colspan=""3"">" & lubearr1(l1) & "</td></tr>")
            End If

        Next

        sb.Append("</Table>")
        tdwi.InnerHtml = sb.ToString
    End Sub
    Private Sub PopParts(ByVal eqid As String, ByVal mode As String)
        Dim pflg As String = "0"
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""250""></td><td width=""20""></td><td width=""350""></td></tr>")
        If mode = "1" Then
            sql = "usp_getWIPartsTotalPdM '" & eqid & "', '0'"
        Else
            sql = "usp_getWIPartsTotalPdM '" & eqid & "', '1'"
        End If
        dr = pms.GetRdrData(sql)
        While dr.Read
            If Len(dr.Item("parts").ToString) > 0 Then
                pflg = "1"
                sb.Append("<tr><td class=""label"">" & dr.Item("func").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("tasknum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("parts").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        sb.Append("</Table>")
        If pflg = "1" Then
            tdparts.InnerHtml = sb.ToString
        Else
            tdparts.InnerHtml = tmod.getxlbl("xlb321" , "CustomCMMShtmlTPMpics.aspx.vb")
        End If

    End Sub
    Private Sub PopTools(ByVal eqid As String, ByVal mode As String)
        Dim pflg As String = "0"
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""250""></td><td width=""20""></td><td width=""350""></td></tr>")
        If mode = "1" Then
            sql = "usp_getWIToolsTotalPdM '" & eqid & "', '0'"
        Else
            sql = "usp_getWIToolsTotalPdM '" & eqid & "', '1'"
        End If
        dr = pms.GetRdrData(sql)
        While dr.Read
            If Len(dr.Item("parts").ToString) > 0 Then
                pflg = "1"
                sb.Append("<tr><td class=""label"">" & dr.Item("func").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("tasknum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("parts").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        sb.Append("</Table>")
        If pflg = "1" Then
            tdtools.InnerHtml = sb.ToString
        Else
            tdtools.InnerHtml = tmod.getxlbl("xlb322" , "CustomCMMShtmlTPMpics.aspx.vb")
        End If

    End Sub
    Private Sub PopLubes(ByVal eqid As String, ByVal mode As String)
        Dim pflg As String = "0"
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""250""></td><td width=""20""></td><td width=""350""></td></tr>")
        If mode = "1" Then
            sql = "usp_getWILubesTotalPdM '" & eqid & "', '0'"
        Else
            sql = "usp_getWILubesTotalPdM '" & eqid & "', '1'"
        End If
        dr = pms.GetRdrData(sql)
        While dr.Read
            If Len(dr.Item("parts").ToString) > 0 Then
                pflg = "1"
                sb.Append("<tr><td class=""label"">" & dr.Item("func").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("tasknum").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("parts").ToString & "</td></tr>")
            End If
        End While
        dr.Close()
        sb.Append("</Table>")
        If pflg = "1" Then
            tdlubes.InnerHtml = sb.ToString
        Else
            tdlubes.InnerHtml = tmod.getxlbl("xlb323" , "CustomCMMShtmlTPMpics.aspx.vb")
        End If

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3315.Text = axlabs.GetASPXPage("CustomCMMShtmlTPMpics.aspx", "lang3315")
        Catch ex As Exception
        End Try
        Try
            lang3316.Text = axlabs.GetASPXPage("CustomCMMShtmlTPMpics.aspx", "lang3316")
        Catch ex As Exception
        End Try
        Try
            lang3317.Text = axlabs.GetASPXPage("CustomCMMShtmlTPMpics.aspx", "lang3317")
        Catch ex As Exception
        End Try

    End Sub

End Class
