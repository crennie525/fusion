﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RBASTasks.aspx.vb" Inherits="lucy_r12.RBASTasks" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report Based Route Task Selection</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function getsel(cbx, ttime, pmtskid) {
            //alert(cbx)
            //alert(ttime)
            //alert(pmtskid)
            var tasks = document.getElementById("lbltasks").value;
            var tottime = document.getElementById("lbltottime").value;
            var totsel = document.getElementById("lbltotsel").value;
            var sel = document.getElementById("lblsel").value;
            var cb = document.getElementById(cbx);
            if (document.getElementById(cbx).checked == true) {
                sel = parseInt(sel) + 1
                document.getElementById("lblsel").value = sel;

                tottime = parseInt(tottime) + parseInt(ttime)
                document.getElementById("lbltottime").value = tottime;

                if (tasks == "") {
                    tasks = pmtskid;
                }
                else {
                    tasks = tasks + "," + pmtskid;
                }
                document.getElementById("lbltasks").value = tasks;
            }
            else {
                if (parseInt(sel) > 0) {
                    sel = parseInt(sel) - 1
                    document.getElementById("lblsel").value = sel;

                    tottime = parseInt(tottime) - parseInt(ttime)
                    document.getElementById("lbltottime").value = tottime;
                }
                if (tasks == "") {
                    tasks = pmtskid;
                }
                else {
                    tasks = tasks.replace(pmtskid, "");
                    tasks = tasks.replace(",,", ",");
                }
            }
            document.getElementById("tdsel").innerHTML = sel;
            document.getElementById("tdtottime").innerHTML = tottime;
            document.getElementById("lbltasks").value = tasks;
            //alert(tasks)
        }
        function savetasks() {
        //alert()
            document.getElementById("lblsubmit").value = "save";
            document.getElementById("form1").submit();
        }
        function checkret() {
            var ret = document.getElementById("lblsubmit").value;
            var rteid = document.getElementById("lblrteid").value;
            if (ret == "go") {
                window.parent.handleexit(rteid);
            }
        }
        function checkexit() {
            var ret = document.getElementById("lblsubmit").value;
            var rteid = document.getElementById("lblrteid").value;
            //alert(ret)
            if (ret != "go" && ret != "save") { 
                document.getElementById("lblsubmit").value = "save"; //was savex
                document.getElementById("form1").submit();
            }
        }
        // onunload="checkexit();
    </script>
</head>
<body onload="checkret();"">
    <form id="form1" runat="server">
    <div id="dvtop" runat="server">
    </div>
    <div id="dvtasks" runat="server">
    </div>
    <input type="hidden" id="lblrteid" runat="server" />
    <input type="hidden" id="lblsel" runat="server" />
    <input type="hidden" id="lbltasks" runat="server" />
    <input type="hidden" id="lbltotsel" runat="server" />
    <input type="hidden" id="lbltottime" runat="server" />
    <input type="hidden" id="lblttime" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" /> 
    <input type="hidden" id="lbltyp" runat="server" />
    <input type="hidden" id="lblskillid" runat="server" />
            <input type="hidden" id ="lblqty" runat="server" />
            <input type="hidden" id="lblfreq" runat="server" />
            <input type="hidden" id="lblrdid" runat="server" />
            <input type="hidden" id="lblrttype" runat="server" />
            <input type="hidden" id="lblhasadd" runat="server" />
            <input type="hidden" id="lblhasrem" runat="server" />
            <input type="hidden" id="lbltasksedit" runat="server" />
            <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
