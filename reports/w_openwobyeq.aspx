﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="w_openwobyeq.aspx.vb" Inherits="lucy_r12.w_openwobyeq" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Open Work Order by Equipment Report</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <style type="text/css">
        .replabeltop
        {
            font-family: Arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 16px;
            color: Black;
            font-weight: bold;
        }
        .gbox
        {
            border-bottom: solid 1px black;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="dvrep" runat="server">
    
    </div>
    </form>
</body>
</html>
