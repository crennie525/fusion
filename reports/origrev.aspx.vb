

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class origrev
    Inherits System.Web.UI.Page
	Protected WithEvents lang3344 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3343 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim lai As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim eqid, eqnum, sid As String
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddout As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tdeqnum As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents dgexport As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgexportrev As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            eqnum = Request.QueryString("eqnum").ToString
            tdeqnum.InnerHtml = eqnum
            lbleqnum.value = eqnum
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            lai.Open()
            BindExport(eqid)
            lai.Dispose()
        Else
            If Request.Form("lblsubmit") = "export" Then
                lblsubmit.value = ""
                lai.Open()
                exportgrid()
                lai.Dispose()
            ElseIf Request.Form("lblsubmit") = "neweq" Then
                lblsubmit.Value = ""
                lai.Open()
                GetNew()
                lai.Dispose()
            End If
        End If
    End Sub
    Private Sub GetNew()
        Try
            Dim eq As String = lbleqid.Value
            If ddout.SelectedIndex <> 0 Then
                If eq <> 0 And eq <> "" Then
                    If ddout.SelectedIndex = 1 Then
                        dgexportrev.DataSource = Nothing
                        dgexportrev.DataBind()
                        dgexportrev.Dispose()
                        BindExport(eq)
                        tdeqnum.InnerHtml = lbleqnum.Value
                    Else
                        dgexport.DataSource = Nothing
                        dgexport.DataBind()
                        dgexport.Dispose()
                        BindExportRev(eq)
                        tdeqnum.InnerHtml = lbleqnum.Value
                    End If
                End If
            End If
        Catch ex As Exception
            Response.Redirect("LAIOrigRev.aspx")
        End Try
    End Sub
    Private Sub BindExport(ByVal eq As String)
        Dim ds As New DataSet
        sql = "usp_optValLAIRevOrig '" & eq & "'"
        'lai.Open()
        ds = lai.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        dgexport.DataSource = dv
        dgexport.DataBind()
        tdeqnum.InnerHtml = lbleqnum.Value
        'lai.Dispose()
    End Sub
    Private Sub BindExportRev(ByVal eq As String)
        Dim ds As New DataSet
        sql = "usp_optValLAIRevRev '" & eq & "'"
        'lai.Open()
        ds = lai.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        dgexportrev.DataSource = dv
        dgexportrev.DataBind()
        tdeqnum.InnerHtml = lbleqnum.Value
        'lai.Dispose()
    End Sub

    Private Sub ddout_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddout.SelectedIndexChanged
        Try
            Dim eq As String = lbleqid.Value
            If ddout.SelectedIndex <> 0 Then
                If eq <> 0 And eq <> "" Then
                    lai.Open()
                    If ddout.SelectedIndex = 1 Then
                        dgexportrev.DataSource = Nothing
                        dgexportrev.DataBind()
                        dgexportrev.Dispose()
                        BindExport(eq)
                        tdeqnum.InnerHtml = lbleqnum.Value
                    Else
                        dgexport.DataSource = Nothing
                        dgexport.DataBind()
                        dgexport.Dispose()
                        BindExportRev(eq)
                        tdeqnum.InnerHtml = lbleqnum.Value
                    End If
                    lai.Dispose()
                End If
            End If
        Catch ex As Exception
            Response.Redirect("LAIOrigRev.aspx")
        End Try
    End Sub

    Private Sub exportgrid()
        Dim eq As String = lbleqid.Value
        If ddout.SelectedIndex <> 0 Then
            If eq <> 0 Then
                If ddout.SelectedIndex = 1 Then
                    dgexportrev.DataSource = Nothing
                    dgexportrev.DataBind()
                    dgexportrev.Dispose()
                    BindExport(eq)
                    cmpDataGridToExcel.DataGridToExcelNHD(dgexport, Response)
                    tdeqnum.InnerHtml = lbleqnum.Value
                Else
                    dgexport.DataSource = Nothing
                    dgexport.DataBind()
                    dgexport.Dispose()
                    BindExportRev(eq)
                    cmpDataGridToExcel.DataGridToExcelNHD(dgexportrev, Response)
                    tdeqnum.InnerHtml = lbleqnum.Value
                End If
            End If
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3343.Text = axlabs.GetASPXPage("origrev.aspx", "lang3343")
        Catch ex As Exception
        End Try
        Try
            lang3344.Text = axlabs.GetASPXPage("origrev.aspx", "lang3344")
        Catch ex As Exception
        End Try

    End Sub

End Class
