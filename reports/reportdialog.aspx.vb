

'********************************************************
'*
'********************************************************



Public Class reportdialog
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim eqid, fuid, cid, tl, sid, did, clid, chk, who As String
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifrep As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpgemstxt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpgemshref As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmod As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try
        If Not IsPostBack Then
            Try
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                fuid = Request.QueryString("fuid").ToString
                lblfuid.Value = fuid
                cid = Request.QueryString("cid").ToString 'HttpContext.Current.Session("comp").ToString
                lblcid.Value = cid
                tl = Request.QueryString("tl").ToString
                lbltasklev.Value = tl
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                did = Request.QueryString("did").ToString
                lbldid.Value = did
                clid = Request.QueryString("clid").ToString
                lblclid.Value = clid
                chk = Request.QueryString("chk").ToString
                lblchk.Value = chk
                Try
                    who = Request.QueryString("who").ToString
                Catch ex As Exception

                End Try
                ifrep.Attributes.Add("src", "../reports/reports2.aspx?tl=5&chk=" & chk & "&cid=" & cid & "&fuid=" & fuid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&eqid=" & eqid & "&who=" & who)
            Catch ex As Exception
                ifrep.Attributes.Add("src", "../reports/reports2.aspx")
            End Try

            'fuid = Request.QueryString("fuid").ToString
            'lblfuid.Value = fuid
            'cid = Request.QueryString("cid").ToString 'HttpContext.Current.Session("comp").ToString
            'lblcid.Value = cid


        End If
    End Sub

End Class
