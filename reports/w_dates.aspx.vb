﻿Public Class w_dates
    Inherits System.Web.UI.Page
    Dim sid, whi, eqid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            whi = Request.QueryString("whi").ToString
            lblwhi.Value = whi
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            If whi = "eqhist" Then
                tdtitle.InnerHtml = "Equipment Downtime History Options"
                trall.Attributes.Add("class", "details")
                trall1.Attributes.Add("class", "details")
                trpm.Attributes.Add("class", "details")
                trpm1.Attributes.Add("class", "details")
            Else
                tdtitle.InnerHtml = "Total Equipment Downtime Report Options"
                trall.Attributes.Add("class", "view")
                trall1.Attributes.Add("class", "view")
                trpm.Attributes.Add("class", "view")
                trpm1.Attributes.Add("class", "view")
            End If
        End If

    End Sub

End Class