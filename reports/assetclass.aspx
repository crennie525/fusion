<%@ Page Language="vb" AutoEventWireup="false" Codebehind="assetclass.aspx.vb" Inherits="lucy_r12.assetclass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>assetclass</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/assetclassaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td id="tdtop" runat="server"></td>
				</tr>
				<tr class="details">
					<td>
						<table cellPadding="0" width="700">
							<tr>
								<td class="label" width="90"><asp:Label id="lang3305" runat="server">Site Location</asp:Label></td>
								<td width="330"><asp:dropdownlist id="ddsites" runat="server" AutoPostBack="True"></asp:dropdownlist></td>
								<td class="label" width="50"><asp:Label id="lang3306" runat="server">Search</asp:Label></td>
								<td width="210"><asp:textbox id="txtsrch" runat="server" Width="200px"></asp:textbox></td>
								<td width="20"><asp:imagebutton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td id="tdinprog" runat="server"></td>
				</tr>
			</table>
			<input id="lblcid" type="hidden" runat="server"> <input id="lblsid" type="hidden" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
