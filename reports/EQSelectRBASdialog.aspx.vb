﻿Public Class EQSelectRBASdialog
    Inherits System.Web.UI.Page
    Dim rteid, days, shift As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            rteid = Request.QueryString("rteid").ToString
            shift = Request.QueryString("shift").ToString
            days = Request.QueryString("days").ToString
            iff.Attributes.Add("src", "EQSelectRBAS.aspx?rteid=" & rteid & "&days=" & days & "&shift=" & shift)
        End If
    End Sub

End Class