Imports System.Data.SqlClient
Public Class MissingData
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim tmod As New transmod
    Dim eqid, fuid, cid, tl, sid, did, clid, chk, usrname As String
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdass As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmd As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwi As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'ADD FOR SUNCOR
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        If comi = "SUN" Then
            lblfslang.Value = "fre"
        End If
        'END ADD FOR SUNCOR
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            fuid = Request.QueryString("fuid").ToString
            lblfuid.Value = fuid
            cid = Request.QueryString("cid").ToString 'HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            tl = Request.QueryString("tl").ToString
            lbltasklev.Value = tl
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            did = Request.QueryString("did").ToString
            lbldid.Value = did
            clid = Request.QueryString("clid").ToString
            lblclid.Value = clid
            chk = Request.QueryString("chk").ToString
            lblchk.Value = chk
            'usrname = HttpContext.Current.Session("username").ToString
            'lblusername.Value = usrname


            tdass.InnerHtml = tmod.getxlbl("xlb256", "AssignmentReport.aspx.vb")
            tdpm.InnerHtml = tmod.getxlbl("xlb366", "missingdata.aspx")
            tdmd.InnerHtml = tmod.getxlbl("lang3342", "missingdata.aspx1")
            pms.Open()
            PopMD(eqid, fuid)
            pms.Dispose()
            'Else
            'If Request.Form("lbltasknum") <> "" Then
            'GetTask()
            'End If
        End If
    End Sub
    Function GetTask()
        Dim tasknum As String = lbltasknum.Value
        tl = lbltasklev.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        chk = lblchk.Value
        fuid = lblfuid.Value
        Response.Redirect("PMTaskDivFunc.aspx?start=yes&tl=5&chk=" & chk & "&cid=" & _
        cid + "&fuid=" & fuid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & _
        "&eqid=" & eqid & "&task=" & tasknum)
    End Function
    Private Sub PopMD(ByVal eqid As String, ByVal fuid As String)
        Dim fid As String = ""
        Dim eqnum, eqdesc As String
        Dim task, comp, freq, craft, rd, taskdesc, fm, fidhold, stask As String
        fidhold = ""
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""40""></td><td width=""580""></td></tr>")
        sql = "usp_getMissingData1 '" & eqid & "', '" & fuid & "'"
        dr = pms.GetRdrData(sql)
        While dr.Read
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            If fid = "" Then
                fid = dr.Item("funcid").ToString
                fidhold = fid
                sb.Append("<tr><td class=""label"" colspan=""2"">" & dr.Item("func").ToString & "</td></tr>")
            Else
                fid = dr.Item("funcid").ToString
                If fid <> fidhold Then
                    fidhold = fid
                    fid = dr.Item("funcid").ToString
                    sb.Append("<tr><td class=""label"" colspan=""2""><br>" & dr.Item("func").ToString & "</td></tr>")
                End If
            End If

            

            task = dr.Item("tasknum").ToString
            stask = dr.Item("subtask").ToString
            comp = dr.Item("compnum").ToString
            freq = dr.Item("freq").ToString
            craft = dr.Item("skillid").ToString
            rd = dr.Item("rdid").ToString
            taskdesc = dr.Item("taskdesc").ToString
            fm = dr.Item("fm1").ToString
            'sb.Append("<tr><td class=""plainlabel"">[" & dr.Item("tasknum").ToString & "]</td>")
            sb.Append("<tr><td class=""plainlabel"">[<A href=""#"" onclick=""gettask('" & dr.Item("tasknum").ToString & "','" & fid & "');"">" & dr.Item("tasknum").ToString & "</A>]")
            sb.Append(" [" & dr.Item("subtask").ToString & "]</td>")
            Dim mstr As String = "Missing: "
            If Len(comp) = 0 Then
                mstr += " " & tmod.getxlbl("xlb351", "missingdata.aspx") & "; "
            End If
            If freq = "0" Then
                mstr += " " & tmod.getxlbl("xlb352", "missingdata.aspx") & "; "
            End If
            If craft = "0" Then
                mstr += " " & tmod.getxlbl("xlb353", "missingdata.aspx") & "; "
            End If
            If rd = "0" Then
                mstr += " " & tmod.getxlbl("xlb354", "missingdata.aspx") & "; "
            End If
            If Len(taskdesc) = 0 Then
                mstr += " " & tmod.getxlbl("xlb355", "missingdata.aspx") & "; "
            End If
            If Len(fm) = 0 Then
                mstr += " " & tmod.getxlbl("xlb356", "missingdata.aspx") & "; "
            End If
            sb.Append("<td class=""plainlabel"" colspan=""3"">" & mstr & "</td></tr>")

        End While
        dr.Close()
        pms.Dispose()
        sb.Append("</Table>")
        tdwi.InnerHtml = sb.ToString
        tdeq.InnerHtml = eqnum & "&nbsp;&nbsp;&nbsp;&nbsp;" & eqdesc
    End Sub
End Class
