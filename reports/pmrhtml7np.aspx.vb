﻿Imports System.Data.SqlClient
Imports System.IO
Public Class pmrhtml7np
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim eqstr, skillid, qty, freq, rdid, mode, rid, rsid, typ, meas As String
    Dim tmod As New transmod
    Dim ridarr As ArrayList = New ArrayList
    Dim sb As New System.Text.StringBuilder

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        lblcomi.Value = comi
        If comi = "SUN" Then
            lblfslang.Value = "fre"
        End If
        If Not IsPostBack Then
            rid = Request.QueryString("rid").ToString '"1014" '
            pms.Open()
            'getrsid(rid)
            If comi = "DEMO" Then
                PopWIDemo(rid)
            Else
                PopWI(rid)
            End If
            pms.Dispose()
            If sb.ToString <> "</table></td></tr></Table>" Then
                divwi.InnerHtml = sb.ToString
            Else
                divwi.InnerHtml = "No Records Found"
            End If
                    End If
    End Sub
    Private Sub getrsid(ByVal rid As String)
        sql = "select rsid from pmroute_stops where rid = '" & rid & "' order by stopsequence"
        dr = pms.GetRdrData(sql)
        While dr.Read
            ridarr.Add(dr.Item("rsid"))
        End While
        dr.Close()
        Dim i As Integer
        For i = 0 To ridarr.Count - 1
            rsid = ridarr(i)
            sql = "select eqid, skillid, skillqty, freq, rdid from pmroute_stops where rsid = '" & rsid & "'"
            Dim ds As New DataSet
            ds = pms.GetDSData(sql)
            Dim x As Integer = ds.Tables(0).Rows.Count
            Dim ii As Integer
            For ii = 0 To (x - 1)
                eqstr = ds.Tables(0).Rows(ii)("eqid").ToString
                skillid = ds.Tables(0).Rows(ii)("skillid").ToString
                qty = ds.Tables(0).Rows(ii)("skillqty").ToString
                freq = ds.Tables(0).Rows(ii)("freq").ToString
                rdid = ds.Tables(0).Rows(ii)("rdid").ToString
                'PopWI(eqstr, skillid, qty, freq, rdid)
            Next
        Next
        divwi.InnerHtml = sb.ToString
    End Sub
    Private Sub PopWI(ByVal rteid As String)
        Dim lang As String = lblfslang.Value
        Dim coi As String = lblcomi.Value
        sql = "select rttype from pmroutes where rid = '" & rteid & "'"
        typ = pms.strScalar(sql)
        Dim fid As String = ""
        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim funcchk As String = ""
        Dim func As String = ""
        Dim comp As String = ""
        Dim compchk As String = ""
        Dim compdesc As String = ""
        Dim subtask As String = ""
        Dim eqnum As String = ""
        Dim eqnumchk As String = ""
        Dim comid As String = ""
        Dim hdchk As Integer = 0
        Dim eqid As String
        Dim pic As String
        Dim epic As String
        Dim fpic As String
        Dim loc As String = ""
        'eqid = eqstr
        'eqstr = eqstr.Replace("(", "")
        'eqstr = eqstr.Replace(")", "")
        'Dim eqarr() As String = eqstr.Split(",")
        'Dim i As Integer
        'Dim tst As String = eqarr.Length - 1

        Dim chk As Integer

        'For i = 0 To eqarr.Length - 1
        hdchk = 0
        'eqid = eqarr(i).ToString
        ' = "(" & eqid & ")"
        'sql = "select count(*) from pmtasks where eqid = '" & eqid & "' and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "'"
        'chk = pms.Scalar(sql)
        'If chk > 0 Then
        If typ = "RBAS" Then
            If lang = "fre" Then
                sql = "usp_pmr_rep5wpf '" & rteid & "'"
            Else
                sql = "usp_pmr_rep5wp '" & rteid & "'"
            End If

        ElseIf typ = "RBAST" Then
            If lang = "fre" Then
                sql = "usp_pmr_rep3Tf '" & rteid & "'"
            Else
                sql = "usp_pmr_rep3T '" & rteid & "'"
            End If

        End If

        Dim parts, tools, lubes As String
        parts = ""
        tools = ""
        lubes = ""
        Dim sk, fr, rd, tt As String
        dr = pms.GetRdrData(sql)
        Dim pretech, pretechchk As String
        'style=""page-break-after:always;""


        While dr.Read
            sk = dr.Item("skill").ToString
            fr = dr.Item("freq").ToString
            rd = dr.Item("rd").ToString
            tt = dr.Item("est").ToString
            skill = dr.Item("skill").ToString & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString
            pretech = dr.Item("pretech").ToString
            eqnum = dr.Item("eqnum").ToString
            'func = dr.Item("func").ToString
            comid = dr.Item("comid").ToString
            parts = dr.Item("parts").ToString
            tools = dr.Item("tools").ToString
            lubes = dr.Item("lubes").ToString
            meas = dr.Item("meas").ToString
            If hdchk = 0 Then
                hdchk = 1
                sb.Append("<Table cellSpacing=""1"" cellPadding=""2"" width=""680"" border=""0"">")
                sb.Append("<tr><td width=""40""></td><td width=""40""></td><td width=""600""></td></tr>")
                sb.Append("<tr><td colspan=""2""><img src=""../menu/mimages/cascades_logo.png""></td>")
                sb.Append("<td width=""600"" class=""tdborder1"">")

                sb.Append("<table width=""600"">")
                sb.Append("<tr><td class=""plainlabel"" width=""100"">Route:</td><td class=""plainlabel"" width=""150"">" & dr.Item("route").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" width=""100"">Description:</td><td width=""350"" class=""plainlabel"">" & dr.Item("description").ToString & "</td></tr>")
                sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
                sb.Append("<tr><td colspan=""4"">")
                sb.Append("<table width=""600"">")
                sb.Append("<tr><td class=""plainlabel"" width=""50"">" & checklang("lab1") & "</td><td class=""plainlabel"" width=""130"">" & sk & "</td>")
                sb.Append("<td class=""plainlabel"" width=""60"">" & checklang("lab2") & "</td><td class=""plainlabel"" width=""80"">" & fr & "</td>")
                sb.Append("<td class=""plainlabel"" width=""50"">" & checklang("lab3") & "</td><td class=""plainlabel"" width=""60"">" & rd & "</td>")
                sb.Append("<td class=""plainlabel"" width=""80"">" & checklang("lab13") & "</td><td class=""plainlabel"" width=""90"">" & tt & "</td></tr>")
                If pretech <> "" Then
                    sb.Append("<tr><td class=""plainlabel"">PdM:</td><td class=""plainlabel"" colspan=""6"">" & pretech & "</td></tr>")
                End If
                sb.Append("</table></td></tr>")
                sb.Append("</table></td></tr>")
                'sb.Append("</table></td></tr>")
            End If

            Dim skchk, skchk2 As String
            skchk = skill.ToLower
            skchk2 = skillchk.ToLower
            If eqnumchk <> eqnum Then
                eqnumchk = eqnum
                'hdchk = 1
                skillchk = skill
                start = 0
                flag = 0

                epic = dr.Item("epic").ToString
                fpic = dr.Item("fpic").ToString
                If File.Exists(epic) Then
                    epic = epic
                Else
                    epic = ""
                End If
                If File.Exists(fpic) Then
                    fpic = fpic
                Else
                    'fpic = ""
                End If

                sb.Append("<tr><td colspan=""3""><hr></td></tr>")
                sb.Append("<tr><td colspan=""3"">")
                sb.Append("<table width=""680"" border=""0"">")
                sb.Append("<tr><td class=""label"" width=""120"">" & checklang("lab4") & "</td><td class=""bigbold1"" width=""220"">" & dr.Item("eqnum").ToString & "<C:\Team1\lucy_r12\lucy_r12\reports\RBASloclookdialog.aspx/td>")
                sb.Append("<td class=""label"" width=""120""></td><td class=""label"" width=""220""></td></tr>")
                sb.Append("<tr><td class=""label"">Description:</td><td class=""label"">" & dr.Item("eqdesc").ToString & "</td></tr>")
                '" & dr.Item("func").ToString & "
                '" & checklang("lab5") & "
                'If epic = "" Then
                'sb.Append("<tr><td colspan=""2"" align=""center"" class=""plainlabel tdborder1"">No Image Available</td>")
                'Else
                'sb.Append("<tr><td colspan=""2"" class=""tdborder1""><img src=""" & epic & """></td>")
                'End If
                'If fpic = "" Then
                'sb.Append("<td colspan=""2"" align=""center"" class=""plainlabel tdborder1"">No Image Available</td></tr>")
                'Else
                'sb.Append("<td colspan=""2"" class=""tdborder1""><img src=""" & fpic & """></td></tr>")
                'End If
                sb.Append("</table></td></tr>")
            ElseIf func <> funcchk Then
                funcchk = func
                epic = dr.Item("epic").ToString
                fpic = dr.Item("fpic").ToString
                If File.Exists(epic) Then
                    epic = epic
                Else
                    'epic = ""
                End If
                If File.Exists(fpic) Then
                    fpic = fpic
                Else
                    'fpic = ""
                End If

                sb.Append("<tr><td colspan=""3"">")
                sb.Append("<table width=""680"">")
                sb.Append("<tr><td colspan=""4""><hr></td></tr>")
                sb.Append("<tr><td class=""label"" width=""120"">" & checklang("lab4") & "</td><td class=""bigbold1"" width=""220"">" & dr.Item("eqnum").ToString & "</td>")
                sb.Append("<td class=""label"" width=""120""></td><td class=""label"" width=""220""></td></tr>")
                sb.Append("<tr><td class=""plainlabel"">Description:</td><td class=""label"">" & dr.Item("eqdesc").ToString & "</td></tr>")
                '" & dr.Item("func").ToString & "
                '" & checklang("lab5") & "
                'If epic = "" Then
                'ppend("<tr><td colspan=""2"" align=""center"" valign=""center"" class=""plainlabel tdborder1"">No Image Available</td>")
                'Else
                'sb.Append("<tr><td colspan=""2"" class=""tdborder1""><img src=""" & epic & """></td>")
                'End If
                'If fpic = "" Then
                'sb.Append("<td colspan=""2"" align=""center"" valign=""center"" class=""plainlabel tdborder1"">No Image Available</td></tr>")
                'Else
                'sb.Append("<td colspan=""2"" class=""tdborder1""><img src=""" & fpic & """></td></tr>")
                'End If
                sb.Append("</table></td></tr>")
            End If

            'insert component stuff here
            comp = dr.Item("compnum").ToString
            compdesc = dr.Item("compdesc").ToString

            If comid <> compchk Then
                compchk = comid
                sb.Append("<tr><td colspan=""3"">")
                sb.Append("<table width=""680"">")
                sb.Append("<tr><td colspan=""4""><hr></td></tr>")
                sb.Append("<tr><td colspan=""3"" class=""label""><u>" & checklang("lab6") & "</u></td></tr>")
                sb.Append("<tr><td class=""label"" width=""120"">" & checklang("lab7") & "</td><td class=""label"" width=""220"">" & comp & "</td>")
                sb.Append("<td class=""label"" width=""120"">" & checklang("lab5") & "</td><td class=""label"" width=""220"">" & dr.Item("func").ToString & "</td></tr>")
                sb.Append("<tr><td class=""plainlabel"">Description:</td><td class=""label"">" & dr.Item("compdesc").ToString & "</td></tr>")
                sb.Append("</table></td></tr>")

                'sb.Append("<tr><td colspan=""3"" class=""label""><u>" & checklang("lab6") & "</u></td></tr>")
                'sb.Append("<tr><td colspan=""2"" class=""label"">" & checklang("lab7") & "</td><td class=""label"">" & comp & "</td></tr>")
                'sb.Append("<tr><td colspan=""2"" class=""label"">Description</td><td class=""label"">" & compdesc & "</td></tr>")

                'sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>")
            End If

            pic = dr.Item("pic").ToString
            If File.Exists(pic) Then
                pic = pic
            Else
                pic = ""
            End If

            subtask = dr.Item("subtask").ToString

            If subtask = "0" Then
                sb.Append("<tr><td colspan=""3"" align=""center"">")
                sb.Append("<table width=""680"">")
                sb.Append("<tr><td class=""tdborder1""><table width=""680"">") 'was 460
                sb.Append("<tr><td class=""plainlabel"" width=""380""><b>" & checklang("lab8") & "</b></td>") 'was 260
                sb.Append("<td class=""plainlabel"" width=""100""><b>" & checklang("lab9") & "</b></td>")
                sb.Append("<td class=""plainlabel"" width=""200""><b>" & checklang("lab10") & "</b></td></tr>") 'was 100

                sb.Append("<tr><td class=""plainlabel"">" & dr.Item("task").ToString & " ")
                If parts <> "" And parts <> "no parts" Then
                    sb.Append("<br>" & parts & " ")
                End If
                If tools <> "" And tools <> "no tools" Then
                    sb.Append("<br>" & tools & " ")
                End If
                If lubes <> "" And lubes <> "no lubes" Then
                    sb.Append("<br>" & lubes & " ")
                End If
                If meas <> "" And meas <> "no measurements" Then
                    sb.Append("<br>" & meas & " ")
                End If
                sb.Append("</td>")

                sb.Append("<td class=""plainlabel"" valign=""top"">" & dr.Item("ttime").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" valign=""top"">" & dr.Item("fm1").ToString & "</td></tr>")

            Else
                If subtask = "1" Then
                    sb.Append("<tr><td class=""bigbold1"" colspan=""3"">Sub Tasks</td></tr>")

                    sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & dr.Item("subt").ToString & "</td></tr>")
                Else
                    sb.Append("<tr><td class=""plainlabel"" colspan=""3"">" & dr.Item("subt").ToString & "</td></tr>")
                End If

            End If
            'X compare subcnt to subtask as int before inserting the below
            Dim subcnt As String = dr.Item("subcnt").ToString
            Dim subc As Integer = Convert.ToInt64(subcnt)
            Dim subtsk As Integer = Convert.ToInt64(subtask)

            If subc = subtsk Then
                sb.Append("<tr><td colspan=""3"" class=""label"">" & checklang("lab11") & "</td></tr>")
                sb.Append("<tr><td colspan=""3"" rowspan=""3"">&nbsp;</td></tr>")

                sb.Append("</table></td>")

                'If pic = "" Then
                'sb.Append("<td align=""center"" rowspan=""8"" class=""tdborder1"">")
                'sb.Append("<table width=""280"">")
                'sb.Append("<tr><td align=""center"" valign=""center"" class=""plainlabel"">No Image Available</td></tr>")
                'sb.Append("</table></td></tr>")
                'Else
                'sb.Append("<td align=""center"" rowspan=""5"">")
                'sb.Append("<table width=""220"" class=""tdborder1"">")
                'sb.Append("<tr><td colspan=""2""><img src=""" & fpic & """></td></tr>")
                'sb.Append("</table></td></tr>")
                'End If

                sb.Append("</table></td></tr>")
            End If

            'need to work on when to add pic




            'sb.Append("<tr><td colspan=""3""><hr></td></tr>")
        End While
        dr.Close()

        sb.Append("</table></td></tr>")

        sb.Append("</Table>")
        'End If
        'Next


    End Sub

    Private Sub PopWIDemo(ByVal rteid As String)
        sql = "select rttype from pmroutes where rid = '" & rteid & "'"
        typ = pms.strScalar(sql)
        Dim fid As String = ""
        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim funcchk As String = ""
        Dim func As String = ""
        Dim comp As String = ""
        Dim compchk As String = ""
        Dim compdesc As String = ""
        Dim subtask As String = ""
        Dim eqnum As String = ""
        Dim eqnumchk As String = ""
        Dim comid As String = ""
        Dim hdchk As Integer = 0
        Dim eqid As String
        Dim pic As String
        Dim epic As String
        Dim fpic As String
        Dim cqty As String
        Dim loc As String = ""
        'eqid = eqstr
        'eqstr = eqstr.Replace("(", "")
        'eqstr = eqstr.Replace(")", "")
        'Dim eqarr() As String = eqstr.Split(",")
        'Dim i As Integer
        'Dim tst As String = eqarr.Length - 1

        Dim chk As Integer

        'For i = 0 To eqarr.Length - 1
        hdchk = 0
        'eqid = eqarr(i).ToString
        ' = "(" & eqid & ")"
        'sql = "select count(*) from pmtasks where eqid = '" & eqid & "' and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "'"
        'chk = pms.Scalar(sql)
        'If chk > 0 Then
        If typ = "RBAS" Then
            sql = "usp_pmr_rep5wp '" & rteid & "'"
        ElseIf typ = "RBAST" Then
            sql = "usp_pmr_rep3T '" & rteid & "'"
        End If

        Dim parts, tools, lubes As String
        parts = ""
        tools = ""
        lubes = ""
        Dim sk, fr, rd, tt As String
        dr = pms.GetRdrData(sql)
        Dim pretech, pretechchk As String
        'style=""page-break-after:always;""


        While dr.Read
            sk = dr.Item("skill").ToString
            fr = dr.Item("freq").ToString
            rd = dr.Item("rd").ToString
            tt = dr.Item("est").ToString
            skill = dr.Item("skill").ToString & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString
            pretech = dr.Item("pretech").ToString
            eqnum = dr.Item("eqnum").ToString
            func = dr.Item("func").ToString
            comid = dr.Item("comid").ToString
            comp = dr.Item("compnum").ToString
            compdesc = dr.Item("compdesc").ToString
            cqty = dr.Item("cqty").ToString
            parts = dr.Item("parts").ToString
            tools = dr.Item("tools").ToString
            lubes = dr.Item("lubes").ToString
            meas = dr.Item("meas").ToString
            If hdchk = 0 Then
                hdchk = 1
                sb.Append("<Table cellSpacing=""1"" cellPadding=""2"" width=""680"" border=""0"">")
                sb.Append("<tr><td width=""40""></td><td width=""40""></td><td width=""580""></td></tr>")
                sb.Append("<tr><td colspan=""2"" height=""80""><img src=""../menu/mimages/werth_logo.png""></td>")
                sb.Append("<td width=""580"" class=""tdborder1"">")

                sb.Append("<table width=""580"" cellSpacing=""1"" cellPadding=""2"">")
                sb.Append("<tr><td class=""label"" width=""60"">Route:</td><td class=""label"" width=""160"">" & dr.Item("route").ToString & "</td>")
                sb.Append("<td class=""label"" width=""80"">Description:</td><td width=""290"" class=""label"">" & dr.Item("description").ToString & "</td></tr>")
                sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
                sb.Append("<tr><td colspan=""4"">")
                sb.Append("<table width=""580"" cellSpacing=""1"" cellPadding=""2"">")
                sb.Append("<tr><td class=""plainlabel"" width=""50"">" & checklang("lab1") & "</td><td class=""plainlabel"" width=""80"">" & sk & "</td>")
                sb.Append("<td class=""plainlabel"" width=""50"">" & checklang("lab2") & "</td><td class=""plainlabel"" width=""80"">" & fr & "</td>")
                sb.Append("<td class=""plainlabel"" width=""50"">" & checklang("lab3") & "</td><td class=""plainlabel"" width=""60"">" & rd & "</td>")
                sb.Append("<td class=""plainlabel"" width=""60"">" & checklang("lab13") & "</td><td class=""plainlabel"" width=""50"">" & tt & "</td></tr>")
                If pretech <> "" Then
                    sb.Append("<tr><td class=""plainlabel"">PdM:</td><td class=""plainlabel"" colspan=""6"">" & pretech & "</td></tr>")
                End If
                sb.Append("</table></td></tr>")
                sb.Append("</table></td></tr>")
                'sb.Append("</table></td></tr>")
            End If

            Dim skchk, skchk2 As String
            skchk = skill.ToLower
            skchk2 = skillchk.ToLower
            If eqnumchk <> eqnum Then
                If eqnumchk <> "" Then
                    sb.Append("<tr><td colspan=""3"">")
                    sb.Append("<table width=""680"" border=""0"">")
                    sb.Append("<tr><td colspan=""4"" class=""label"">" & checklang("lab11") & "<br><br><br><br><br></td></tr>")
                    'sb.Append("<tr><td colspan=""4""><hr></td></tr>")
                    sb.Append("</table></td></tr>")
                End If
                eqnumchk = eqnum
                funcchk = ""
                compchk = ""
                'hdchk = 1
                skillchk = skill
                start = 0
                flag = 0

                epic = dr.Item("epic").ToString
                fpic = dr.Item("fpic").ToString
                If File.Exists(epic) Then
                    epic = epic
                Else
                    epic = ""
                End If
                If File.Exists(fpic) Then
                    fpic = fpic
                Else
                    'fpic = ""
                End If

                'sb.Append("<tr><td colspan=""3""><hr></td></tr>")
                sb.Append("<tr><td colspan=""3"">")
                sb.Append("<table width=""680"" border=""0"">")
                sb.Append("<tr><td colspan=""2"" class=""demtop""></td></tr>")
                sb.Append("<tr><td class=""label"" width=""120"">" & checklang("lab5") & "</td><td class=""bigbold1"" width=""560"">" & dr.Item("eqnum").ToString & "</td></tr>")
                'sb.Append("<td class=""label"" width=""120""></td><td class=""label"" width=""220""></td></tr>")
                sb.Append("<tr><td class=""label"">Description:</td><td class=""label"">" & dr.Item("eqdesc").ToString & "</td></tr>")
                sb.Append("<tr><td colspan=""2"" class=""dembot""></td></tr>")
                '" & dr.Item("func").ToString & "
                '" & checklang("lab5") & "
                'If epic = "" Then
                'sb.Append("<tr><td colspan=""2"" align=""center"" class=""plainlabel tdborder1"">No Image Available</td>")
                'Else
                'sb.Append("<tr><td colspan=""2"" class=""tdborder1""><img src=""" & epic & """></td>")
                'End If
                'If fpic = "" Then
                'sb.Append("<td colspan=""2"" align=""center"" class=""plainlabel tdborder1"">No Image Available</td></tr>")
                'Else
                'sb.Append("<td colspan=""2"" class=""tdborder1""><img src=""" & fpic & """></td></tr>")
                'End If
                sb.Append("</table></td></tr>")
            End If
            If func <> funcchk Then
                funcchk = func
                compchk = ""
                epic = dr.Item("epic").ToString
                fpic = dr.Item("fpic").ToString
                If File.Exists(epic) Then
                    epic = epic
                Else
                    'epic = ""
                End If
                If File.Exists(fpic) Then
                    fpic = fpic
                Else
                    'fpic = ""
                End If

                sb.Append("<tr><td colspan=""3"">")
                sb.Append("<table width=""680"">")
                'sb.Append("<tr><td colspan=""4""><hr></td></tr>")
                sb.Append("<tr><td class=""label"" width=""120"">" & checklang("lab4") & "</td><td class=""label"" width=""560"">" & dr.Item("func").ToString & "</td></tr>")
                'sb.Append("<tr><td class=""plainlabel"">Description:</td><td class=""label"">" & dr.Item("eqdesc").ToString & "</td></tr>")
                
                sb.Append("</table></td></tr>")
            End If

            'insert component stuff here
            comp = dr.Item("compnum").ToString
            compdesc = dr.Item("compdesc").ToString

            If comid <> compchk Then
                
                If compchk <> "" Then
                    sb.Append("<tr><td colspan=""3"">")
                    sb.Append("<table width=""680"" border=""0"">")
                    'sb.Append("<tr><td colspan=""4"" class=""label"">" & checklang("lab11") & "<br><br></td></tr>")
                    'sb.Append("<tr><td colspan=""4""><hr></td></tr>")
                    sb.Append("</table></td></tr>")
                End If
                compchk = comid
                'sb.Append("<tr><td colspan=""4""><hr></td></tr>")
                'sb.Append("<tr><td colspan=""3"" class=""label""><u>" & checklang("lab6") & "</u></td></tr>")
                'sb.Append("<tr><td class=""label"" width=""120"">" & checklang("lab7") & "</td><td class=""label"" width=""220"">" & comp & "</td>")
                'sb.Append("<td class=""label"" width=""120"">" & checklang("lab4") & "</td><td class=""label"" width=""220"">" & dr.Item("func").ToString & "</td></tr>")
                'sb.Append("<tr><td class=""plainlabel"">Description:</td><td class=""label"">" & dr.Item("compdesc").ToString & "</td></tr>")
                'sb.Append("</table></td></tr>")

            End If

            pic = dr.Item("pic").ToString
            If File.Exists(pic) Then
                pic = pic
            Else
                pic = ""
            End If

            subtask = dr.Item("subtask").ToString

            If subtask = "0" Then
                sb.Append("<tr><td colspan=""3"" align=""center"">")
                sb.Append("<table width=""680"">")
                sb.Append("<tr><td class=""tdborder1""><table width=""680"">") 'was 460
                sb.Append("<tr><td class=""plainlabel"" width=""580""><b>" & checklang("lab8") & "</b></td>") 'was 260
                sb.Append("<td class=""plainlabel"" width=""100""><b>" & checklang("lab9") & "</b></td></tr>")
                'sb.Append("<td class=""plainlabel"" width=""200""><b>" & checklang("lab10") & "</b></td></tr>") 'was 100

                sb.Append("<tr><td class=""plainlabel"">")
                sb.Append("<b>" & comp)
                If compdesc <> "" Then
                    sb.Append("," & compdesc)
                End If
                sb.Append("&nbsp;(" & cqty & "ea)</b>&nbsp;-&nbsp;" & dr.Item("task").ToString & "</td></tr>")
                sb.Append("<tr><td class=""plainlabel"">OK(___)&nbsp;" & dr.Item("fm1").ToString & " ")
                If parts <> "" And parts <> "no parts" Then
                    sb.Append("<br>" & parts & " ")
                End If
                If tools <> "" And tools <> "no tools" Then
                    sb.Append("<br>" & tools & " ")
                End If
                If lubes <> "" And lubes <> "no lubes" Then
                    sb.Append("<br>" & lubes & " ")
                End If
                If meas <> "" And meas <> "no measurements" Then
                    sb.Append("<br>" & meas & " ")
                End If
                sb.Append("</td>")

                sb.Append("<td class=""plainlabel"" valign=""top"">" & dr.Item("ttime").ToString & "</td></tr>")
                'sb.Append("<td class=""plainlabel"" valign=""top"">" & dr.Item("fm1").ToString & "</td></tr>")

            Else
                If subtask = "1" Then
                    sb.Append("<tr><td class=""bigbold1"" colspan=""3"">Sub Tasks</td></tr>")

                    sb.Append("<tr><td class=""plainlabel"" colspan=""3"">[1]&nbsp;" & dr.Item("subt").ToString & "</td></tr>")
                Else
                    sb.Append("<tr><td class=""plainlabel"" colspan=""3"">[" & subtask & "]&nbsp;]" & dr.Item("subt").ToString & "</td></tr>")
                End If

            End If
            'X compare subcnt to subtask as int before inserting the below
            Dim subcnt As String = dr.Item("subcnt").ToString
            Dim subc As Integer = Convert.ToInt64(subcnt)
            Dim subtsk As Integer = Convert.ToInt64(subtask)

            If subc = subtsk Then
                'sb.Append("<tr><td colspan=""3"" class=""label"">" & checklang("lab11") & "</td></tr>")
                'sb.Append("<tr><td colspan=""3"" rowspan=""3"">&nbsp;</td></tr>")

                sb.Append("</table></td>")

                'If pic = "" Then
                'sb.Append("<td align=""center"" rowspan=""8"" class=""tdborder1"">")
                'sb.Append("<table width=""280"">")
                'sb.Append("<tr><td align=""center"" valign=""center"" class=""plainlabel"">No Image Available</td></tr>")
                'sb.Append("</table></td></tr>")
                'Else
                'sb.Append("<td align=""center"" rowspan=""5"">")
                'sb.Append("<table width=""220"" class=""tdborder1"">")
                'sb.Append("<tr><td colspan=""2""><img src=""" & fpic & """></td></tr>")
                'sb.Append("</table></td></tr>")
                'End If

                sb.Append("</table></td></tr>")
            End If

            'need to work on when to add pic




            'sb.Append("<tr><td colspan=""3""><hr></td></tr>")
        End While
        dr.Close()

        sb.Append("<tr><td colspan=""3"">")
        sb.Append("<table width=""680"" border=""0"">")
        sb.Append("<tr><td colspan=""4"" class=""label"">" & checklang("lab11") & "<br><br><br><br><br></td></tr>")
        'sb.Append("<tr><td colspan=""4""><hr></td></tr>")
        sb.Append("</table></td></tr>")

        sb.Append("</table></td></tr>")

        sb.Append("</Table>")
        'End If
        'Next


    End Sub

    Private Function checklang(ByVal lab As String)
        Dim lang As String = lblfslang.Value
        Dim ret As String
        Dim lab1 As String
        'Acune image de tâche
        If lang = "fre" Then
            If lab = "lab1" Then
                lab1 = "Métier:"
            ElseIf lab = "lab2" Then
                lab1 = "Fréquence:"
            ElseIf lab = "lab3" Then
                lab1 = "Statut:"
            ElseIf lab = "lab4" Then
                lab1 = "Fonction:"

            ElseIf lab = "lab5" Then
                lab1 = "Équipement:"
            ElseIf lab = "lab6" Then
                lab1 = "Détail de la composante:"
            ElseIf lab = "lab7" Then
                lab1 = "Nom:"
            ElseIf lab = "lab8" Then
                lab1 = "Tâche:"
            ElseIf lab = "lab9" Then
                lab1 = "Temps estimé:"
            ElseIf lab = "lab10" Then
                lab1 = "Modes de défaillances:"
            ElseIf lab = "lab11" Then
                lab1 = "Commentaire:"
            ElseIf lab = "lab12" Then
                lab1 = "Acune image de tâche"
            ElseIf lab = "lab13" Then
                lab1 = "Temps total"
            End If
        Else
            If lab = "lab1" Then
                lab1 = "Skill:"
            ElseIf lab = "lab2" Then
                lab1 = "Frequency:"
            ElseIf lab = "lab3" Then
                lab1 = "Status:"
            ElseIf lab = "lab4" Then
                lab1 = "Function:"
            ElseIf lab = "lab5" Then
                lab1 = "Equipment:"
            ElseIf lab = "lab6" Then
                lab1 = "Component Details:"
            ElseIf lab = "lab7" Then
                lab1 = "Name:"
            ElseIf lab = "lab8" Then
                lab1 = "Task:"
            ElseIf lab = "lab9" Then
                lab1 = "Est Duration:"
            ElseIf lab = "lab10" Then
                lab1 = "Failure Modes:"
            ElseIf lab = "lab11" Then
                lab1 = "Comments:"
            ElseIf lab = "lab12" Then
                lab1 = "No Image Available"
            ElseIf lab = "lab13" Then
                lab1 = "Total Time"
            End If
        End If
        ret = lab1
        Return ret
    End Function
End Class