﻿Imports System.Data.SqlClient
Public Class pmrselect
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim tots As New Utilities
    Dim eqstr, who As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqstr = Request.QueryString("eqstr").ToString '"(825, 827)"
            who = Request.QueryString("who").ToString
            
            lbleqstr.Value = eqstr
            lblwho.Value = who
            tots.Open()
            gettots(eqstr)
            tots.Dispose()

        End If
    End Sub
    Private Sub gettots(ByVal eqstr As String)

        sql = "exec usp_pmr " & eqstr & "'"
        dr = tots.GetRdrData(sql)
        dgtots.DataSource = dr
        dgtots.DataBind()
        dr.Close()
    End Sub

    Private Sub dgtots_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtots.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim skillid As String = DataBinder.Eval(e.Item.DataItem, "skillid").ToString
            Dim qty As String = DataBinder.Eval(e.Item.DataItem, "qty").ToString
            Dim freq As String = DataBinder.Eval(e.Item.DataItem, "freq").ToString
            Dim rid As String = DataBinder.Eval(e.Item.DataItem, "rdid").ToString
            Dim pm As String = DataBinder.Eval(e.Item.DataItem, "pm").ToString
            Dim lpm As LinkButton = CType(e.Item.FindControl("lbpm"), LinkButton)
            lpm.Attributes.Add("onclick", "getrep('" & skillid & "','" & qty & "','" & freq & "','" & rid & "','" & pm & "');")
        End If
    End Sub
End Class