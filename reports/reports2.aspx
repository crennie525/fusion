<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="reports2.aspx.vb" Inherits="lucy_r12.reports2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PM Report Dialog</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/reportnav.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/reports2aspx_12.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script type="text/javascript" language="javascript">
    <!--
        function getinprog() {
            var coi = document.getElementById("lblcoi").value;
            //alert(coi)
            window.open("inprog.aspx?coi=" + coi + "&date=" + Date());
        }
        function getasclib() {
            var sid = document.getElementById("lblsid").value;
            if (sid != "") {
                //var eReturn = window.showModalDialog("AssignmentReportCompLib.aspx?sid=" + sid + "&typ=" + typ + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                window.open("AssignmentReportCompLib.aspx?siteid=" + sid + "&date=" + Date());
                //if(eReturn) {
                //window.parent.handleexit(eReturn);
                //}
            }
        }
        function getpmrepoutlist(typ) {
            var sid = document.getElementById("lblsid").value;
            if (sid != "") {
                //if (typ == "RBAS") {
                window.open("pmrlist.aspx?sid=" + sid + "&typ=" + typ + "&date=" + Date());
                //}
                //else {
                //typ = "RBASTM2"
                //window.open("../reports/pmrhtml6t.aspx?rid=" + rtid + "&typ=" + typ, "repWin", popwin);
                //}
            }
        }

        function getxout(typ) {
            window.open("../reports/toexcel.aspx?typ=" + typ + "&date=" + Date())
        }

        function getpmrepoutputr(typ) {
            var sid = document.getElementById("lblsid").value;
            if (sid != "") {
                var eReturn = window.showModalDialog("../appsrt/pmrrtelkupdialog.aspx?sid=" + sid + "&typ=" + typ + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:650px; resizable=yes");
                //if(eReturn) {
                //window.parent.handleexit(eReturn);
                //}
            }
        }
        function geteqrt() {
            var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
            var sid = document.getElementById("lblsid").value;
            if (sid != "") {
                window.open("../equip/eqrouting.aspx?sid=" + sid + "&date=" + Date(), "repWin", popwin);
            }
        }
        function getoptsumpss(who) {
            var sid = document.getElementById("lblsid").value;
            if (sid != "") {
                var eReturn = window.showModalDialog("EqSelectDialog.aspx?sid=" + sid + "&who=" + who + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                //if(eReturn) {
                //window.parent.handleexit(eReturn);
                //}
            }
        }
        function getpmrepoutput() {
            var eq = document.getElementById("lbleqid").value;
            if (eq != "") {
                window.open("pmrep2.aspx?eqid=" + eq + "&date=" + Date());
            }
        }
        function getpmrepoutput2() {
            var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
            var eq = document.getElementById("lbleqid").value;
            if (eq != "") {
                window.open("pmgselect.aspx?eqid=" + eq + "&date=" + Date(), "repWin", popwin);
            }
        }
        function getpgemswrd(id) {

            var eq = document.getElementById("lbleqid").value;
            if (eq != "") {
                //if (id == "1") {
                //window.open("CMMS_Output_to_Word.aspx?eqid=" + eq + "&mode=&inout=no&date=" + Date());
                //}
                //else if (id == "2") {
                //window.open("CMMS_Output_to_Word.aspx?eqid=" + eq + "&mode=&inout=yes&date=" + Date());
                //}
                //else if (id == "3") {
                //window.open("CMMS_Output_to_Word.aspx?eqid=" + eq + "&mode=pdm&inout=no&date=" + Date());
                //}
                //else if (id == "4") {
                //window.open("CMMS_Output_to_Word.aspx?eqid=" + eq + "&mode=pdm&inout=yes&date=" + Date());
                //}
                //else 
                if (id == "S") {
                    //alert("CMMS_Output_to_Word_SYM.aspx?eqid=" + eq + "&mode=pdm&inout=yes&date=" + Date())
                    window.open("CMMS_Output_to_Word_SYM.aspx?eqid=" + eq + "&mode=pdm&inout=yes&date=" + Date());
                }
            }
            //else alert("No Equipment Record Selected!");
        }

        function getoptsumps() {
            var sid = document.getElementById("lblsid").value;
            var coi = document.getElementById("lblcoi").value;
            if (sid != "") {
                if (coi == "BRI") {
                    window.open("OVRollup_BRI.aspx?typ=site&sid=" + sid + "&date=" + Date());
                }
                else {
                    window.open("OVRollup.aspx?typ=site&sid=" + sid + "&date=" + Date());
                }

                //was valu3
            }
        }

        function getoptsum() {
            var eq = document.getElementById("lbleqid").value;
            var coi = document.getElementById("lblcoi").value;
            var sid = document.getElementById("lblsid").value;
            if (eq != "") {
                if (coi == "BRI") {
                    window.open("OVRollup_BRI.aspx?typ=eq&eqid=" + eq + "&date=" + Date());
                }
                else {

                    window.open("OVRollup.aspx?typ=eq&eqid=" + eq + "&sid=" + sid + "&date=" + Date());
                }

            }
            //else alert("No Equipment Record Selected!");
        }
        function getoptsum_orig() {
            var eq = document.getElementById("lbleqid").value;
            var coi = document.getElementById("lblcoi").value;
            var sid = document.getElementById("lblsid").value;
            if (eq != "") {
                //if (coi == "BRI") {
                //window.open("OVRollup_BRI.aspx?typ=eq&eqid=" + eq + "&date=" + Date());
                //}
                //else {

                window.open("origpie.aspx?typ=eq&eqid=" + eq + "&sid=" + sid + "&date=" + Date());
                //}

            }
            //else alert("No Equipment Record Selected!");
        }
        function getoptsum_rev() {
            var eq = document.getElementById("lbleqid").value;
            var coi = document.getElementById("lblcoi").value;
            var sid = document.getElementById("lblsid").value;
            if (eq != "") {
                //if (coi == "BRI") {
                //window.open("OVRollup_BRI.aspx?typ=eq&eqid=" + eq + "&date=" + Date());
                //}
                //else {

                window.open("revpie.aspx?typ=eq&eqid=" + eq + "&sid=" + sid + "&date=" + Date());
                //}

            }
            //else alert("No Equipment Record Selected!");
        }
        function getspring() {
            var eqid = document.getElementById("lbleqid").value;
            window.open("CMMS_HTM_Spring.aspx?eqid=" + eqid + "&mode=1&date=" + Date());
        }
        function gettpmw3() {
            var eqid = document.getElementById("lbleqid").value;
            window.open("tpmweekly3.aspx?eqid=" + eqid + "&mode=1&date=" + Date());
        }
        function gettpmw4() {
            var eqid = document.getElementById("lbleqid").value;
            window.open("mwchart.aspx?eqid=" + eqid + "&mode=1&date=" + Date());
        }
        function getoptsumop(who) {
            var eq = document.getElementById("lbleqid").value;
            if (eq != "" && who == "e") {
                window.open("OVRollup_OP2.aspx?typ=eq&eqid=" + eq + "&date=" + Date());
            }
            else if (eq != "" && who == "s") {
                var sid = document.getElementById("lblsid").value;
                if (sid != "") {
                    window.open("OVRollup_OP2.aspx?typ=site&sid=" + sid + "&date=" + Date());
                }
                //window.open("OVRollup_OP2.aspx?typ=eq&eqid=" + eq + "&date=" + Date());
            }
            //else alert("No Equipment Record Selected!");
        }
        function gettsnotasks(who) {
            var eq = document.getElementById("lbleqid").value;
            fuid = document.getElementById("lblfuid").value;
            //&&fuid!=""
            if (eq != "") {
                tl = document.getElementById("lbltasklev").value;
                cid = document.getElementById("lblcid").value;
                sid = document.getElementById("lblsid").value;
                did = document.getElementById("lbldid").value;
                clid = document.getElementById("lblclid").value;
                eqid = document.getElementById("lbleqid").value;
                chk = document.getElementById("lblchk").value;
                lid = document.getElementById("lbllid").value;
                var eReturn = window.showModalDialog("ttnodialog.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&who=" + who + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:780px;resizable=yes");
                if (eReturn) {
                }
            }
        }
        function getasnmnt2() {
            var eq = document.getElementById("lbleqid").value;
            if (eq != "") {
                window.open("AssignmentReport2.aspx?typ=eq&eqid=" + eq + "&date=" + Date());
            }
            //else alert("No Equipment Record Selected!");
        }
        function getasnmnt2a() {
            var eq = document.getElementById("lbleqid").value;
            if (eq != "") {
                window.open("AssignmentReport3.aspx?typ=eq&eqid=" + eq + "&date=" + Date());
            }
            //else alert("No Equipment Record Selected!");
        }
        function getasnmnt3() {
            var eq = document.getElementById("lbleqid").value;
            if (eq != "") {
                window.open("AssignmentReportExcel.aspx?typ=eq&eqid=" + eq + "&date=" + Date());
            }
            //else alert("No Equipment Record Selected!");
        }
        function getasnmnt4() {
            var popwin = "directories=0,height=600,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
            var sid = document.getElementById("lblsid").value;
            if (sid != "") {
                window.open("../equip/eqmasterselect.aspx?sid=" + sid + "&date=" + Date(), "repWin", popwin);
            }
            //else alert("No Equipment Record Selected!");
        }
        function getusetot() {
            var usetot = document.getElementById("lblusetot").value;
            //alert(usetot)
            if (usetot == "1") {
                var eqid = document.getElementById("lbleqid").value;
                var sid = document.getElementById("lblsid").value;
                var eqnum = document.getElementById("lbleqnum").value;
                var eReturn = window.showModalDialog("../appsdrag/totpmsdialog.aspx?typ=1&eqid=" + eqid + "&sid=" + sid + "&eqnum=" + eqnum, "", "dialogHeight:610px; dialogWidth:860px; resizable=yes");
            }
        }

        function hclass() {
            var eqid = document.getElementById("lbleqid").value;
            var rcnt = document.getElementById("lblrepcnt").value;
            var fuid = document.getElementById("lblfuid").value;
            var i = 1;
            var h;
            if (eqid != "") {
                for (i = 1; i <= parseInt(rcnt); i++) {
                    if (i != 4 && i != 5) {
                        h = "a" + i;
                        if (h != "a3") {
                            document.getElementById(h).className = "bluelabelu";
                        }
                    }
                }
                document.getElementById("hrefstask1").className = "bluelabelu";
                document.getElementById("tdtpmw3").className = "bluelabelu";
                document.getElementById("tdtpmw4").className = "bluelabelu";

                document.getElementById("pgemstxt1").className = "bluelabelu";
                document.getElementById("pgemstxthtm").className = "bluelabelu";
                document.getElementById("pgemstxthtm3").className = "bluelabelu";
                document.getElementById("pgemshtm1").className = "bluelabelu";
                document.getElementById("pgemstxt2").className = "bluelabelu";
                document.getElementById("pgemstxthtm1").className = "bluelabelu";
                document.getElementById("pgemshtm12").className = "bluelabelu";
                //document.getElementById("pgemshtm13").className="bluelabelu";
                document.getElementById("pgemstxt1tpm").className = "bluelabelu";
                document.getElementById("pgemstxthtmtpm").className = "bluelabelu";
                document.getElementById("pgemshtm1tpm").className = "bluelabelu";
                document.getElementById("pgemshtm1tpmpic").className = "bluelabelu";
                document.getElementById("a3a").className = "bluelabelu";
                document.getElementById("a3excel").className = "bluelabelu";
                document.getElementById("a3anew").className = "bluelabelu";
                document.getElementById("a3anewa").className = "bluelabelu";

                document.getElementById("a11").className = "bluelabelu";
                document.getElementById("a11orig").className = "bluelabelu";
                document.getElementById("a11rev").className = "bluelabelu";
                //document.getElementById("a11a").className = "bluelabelu";
                //document.getElementById("a11b").className = "bluelabelu";
                document.getElementById("A313").className = "bluelabelu";

                document.getElementById("a12").className = "bluelabelu";
                document.getElementById("a13").className = "bluelabelu";
                document.getElementById("a14").className = "bluelabelu";
                document.getElementById("a15").className = "bluelabelu";
                document.getElementById("a16").className = "bluelabelu";
                document.getElementById("a17").className = "bluelabelu";
                document.getElementById("a18").className = "bluelabelu";
                document.getElementById("a19").className = "bluelabelu";
                document.getElementById("a3tpm").className = "bluelabelu";
                document.getElementById("a8tpm").className = "bluelabelu";
                document.getElementById("a9tpm").className = "bluelabelu";
                document.getElementById("a10tpm").className = "bluelabelu";
                document.getElementById("tdtpmw1").className = "bluelabelu";
                document.getElementById("tdtpmw2").className = "bluelabelu";

                document.getElementById("pgemstxtwrd").className = "bluelabelu";
                document.getElementById("pgemstxtwrd1").className = "bluelabelu";
                document.getElementById("pgemstxtwrd1sym").className = "bluelabelu";
                document.getElementById("pgemstxtwrd3").className = "bluelabelu";
                document.getElementById("pgemstxtwrd4").className = "bluelabelu";

                //document.getElementById("hrefstask").className="bluelabelu";
                //document.getElementById("hrefstask1").className="bluelabelu";
                document.getElementById("hrefstask2").className = "bluelabelu";
                document.getElementById("hrefstask3").className = "bluelabelu";
                document.getElementById("hrefstask4").className = "bluelabelu";

                //if(fuid!="") {
                document.getElementById("a1").className = "bluelabelu";
                document.getElementById("a1c").className = "bluelabelu";
                document.getElementById("a1e").className = "bluelabelu";
                document.getElementById("a1e1").className = "bluelabelu";

                document.getElementById("A20").className = "bluelabelu";
                document.getElementById("A21").className = "bluelabelu";
                document.getElementById("A22").className = "bluelabelu";
                document.getElementById("A23").className = "bluelabelu";
                document.getElementById("a24").className = "bluelabelu";
                document.getElementById("A27").className = "bluelabelu";
                document.getElementById("A25sun").className = "bluelabelu";
                document.getElementById("tddownhist").className = "bluelabelu";
                document.getElementById("A617").className = "bluelabelu";
                //document.getElementById("a1").className="bluelabelu";
                //}
                //else {
                //document.getElementById("a1").className="graylabelu";
                //}
                var usetot = document.getElementById("lblusetot").value;
                if (usetot == "1") {
                    document.getElementById("o10").className = "bluelabelu";
                }
                else {
                    document.getElementById("o10").className = "graylabelu";
                }
                var meascnt = document.getElementById("lblmeascnt").value;
                if (meascnt > 0) {
                    //document.getElementById("hrefmeas").className="bluelabelu";
                }
                else {
                    //document.getElementById("hrefmeas").className="graylabelu";
                }
            }
            else {
                for (i = 1; i <= parseInt(rcnt); i++) {
                    if (i != 4 && i != 5) {
                        h = "a" + i;
                        if (h != "a3") {
                            document.getElementById(h).className = "graylabelu";
                        }
                    }
                }
                document.getElementById("a1c").className = "graylabelu";
                document.getElementById("a1e").className = "graylabelu";
                document.getElementById("a1e1").className = "graylabelu";

                document.getElementById("pgemstxt1").className = "graylabelu";
                document.getElementById("pgemstxthtm").className = "graylabelu";
                document.getElementById("pgemstxthtm3").className = "graylabelu";
                document.getElementById("pgemshtm1").className = "graylabelu";
                document.getElementById("pgemstxt2").className = "graylabelu";
                document.getElementById("pgemstxthtm1").className = "graylabelu";
                document.getElementById("pgemshtm12").className = "graylabelu";
                //document.getElementById("pgemshtm13").className="graylabelu";
                document.getElementById("pgemstxt1tpm").className = "graylabelu";
                document.getElementById("pgemstxthtmtpm").className = "graylabelu";
                document.getElementById("pgemshtm1tpm").className = "graylabelu";
                document.getElementById("pgemshtm1tpmpic").className = "graylabelu";
                document.getElementById("a3a").className = "graylabelu";
                document.getElementById("a3anew").className = "graylabelu";

                document.getElementById("a3anewa").className = "graylabelu";

                document.getElementById("A313").className = "graylabelu";

                document.getElementById("a11").className = "graylabelu";
                document.getElementById("a11orig").className = "graylabelu";
                document.getElementById("a11rev").className = "graylabelu";
                //document.getElementById("a11a").className = "graylabelu";
                //document.getElementById("a11b").className = "graylabelu";
                document.getElementById("a3excel").className = "graylabelu";
                document.getElementById("a12").className = "graylabelu";
                document.getElementById("a13").className = "graylabelu";
                document.getElementById("a14").className = "graylabelu";
                document.getElementById("a15").className = "graylabelu";
                document.getElementById("a16").className = "graylabelu";
                document.getElementById("a17").className = "graylabelu";
                document.getElementById("a18").className = "graylabelu";
                document.getElementById("a19").className = "graylabelu";
                document.getElementById("o10").className = "graylabelu";
                //alert(document.getElementById("o10").className)
                document.getElementById("A27").className = "graylabelu";

                document.getElementById("a3tpm").className = "graylabelu";
                document.getElementById("a8tpm").className = "graylabelu";
                document.getElementById("a9tpm").className = "graylabelu";
                document.getElementById("a10tpm").className = "graylabelu";
                document.getElementById("tdtpmw1").className = "graylabelu";
                document.getElementById("tdtpmw2").className = "graylabelu";




                document.getElementById("pgemstxtwrd").className = "graylabelu";
                document.getElementById("pgemstxtwrd1").className = "graylabelu";
                document.getElementById("pgemstxtwrd1sym").className = "graylabelu";
                document.getElementById("pgemstxtwrd3").className = "graylabelu";
                document.getElementById("pgemstxtwrd4").className = "graylabelu";

                //document.getElementById("hrefmeas").className="graylabelu";
                //document.getElementById("hrefstask").className="graylabelu";
                //document.getElementById("hrefstask1").className="graylabelu";
                document.getElementById("hrefstask2").className = "graylabelu";
                document.getElementById("hrefstask3").className = "graylabelu";
                document.getElementById("hrefstask4").className = "graylabelu";

                document.getElementById("hrefstask1").className = "graylabelu";
                document.getElementById("tdtpmw3").className = "graylabelu";
                document.getElementById("tdtpmw4").className = "graylabelu";

                document.getElementById("A20").className = "graylabelu";
                document.getElementById("A21").className = "graylabelu";
                document.getElementById("A22").className = "graylabelu";
                document.getElementById("A23").className = "graylabelu";
                document.getElementById("a24").className = "graylabelu";
                document.getElementById("A25sun").className = "graylabelu";
                document.getElementById("tddownhist").className = "graylabelu";
                document.getElementById("A617").className = "graylabelu";
            }
        }
        function checkit() {
            eqnum = document.getElementById("lbleqnum").value;
            eqdesc = document.getElementById("lbleqdesc").value;
            if (eqnum != "") {
                document.getElementById("tdeq").innerHTML = "Current Equipment: " + eqnum + " - " + eqdesc;
            }
            //var redir = document.getElementById("lblredir").value
            //if(redir.length!=0) {
            //	window.parent.handleredir(redir);
            //}
            //window.setTimeout("setref();", 1205000);
            hclass();
            var eflag = document.getElementById("lbleflag").value;
            if (eflag == "1") {
                checke();
            }
        }
        function getpgemshtm3(id) {
            var eq = document.getElementById("lbleqid").value;
            if (eq != "") {
                if (id == "1") {
                    window.open("CustomCMMSTXTtoHTM3.aspx?eqid=" + eq + "&mode=1&date=" + Date());
                }
                else if (id == "2") {
                    window.open("CustomCMMSTXTtoHTM.aspx?eqid=" + eq + "&mode=2&date=" + Date());
                }
                else {
                    window.open("CustomCMMSTXTtoHTMTPM.aspx?eqid=" + eq + "&mode=1&date=" + Date());
                }
            }
            //else alert("No Equipment Record Selected!");
        }
        function getpgemswrd2(id) {
            var eq = document.getElementById("lbleqid").value;
            if (eq != "") {
                if (id == "1") {
                    window.open("CMMS_Output_to_Word3.aspx?eqid=" + eq + "&mode=&inout=no&date=" + Date());
                }
                else if (id == "2") {
                    window.open("CMMS_Output_to_Word3.aspx?eqid=" + eq + "&mode=&inout=yes&date=" + Date());
                }
                else if (id == "3") {
                    window.open("CMMS_Output_to_Word3.aspx?eqid=" + eq + "&mode=pdm&inout=no&date=" + Date());
                }
                else if (id == "4") {
                    window.open("CMMS_Output_to_Word3.aspx?eqid=" + eq + "&mode=pdm&inout=yes&date=" + Date());
                }
            }
            //else alert("No Equipment Record Selected!");
        }
        function getfire() {
            var eqid = document.getElementById("lbleqid").value;
            if (eqid != "") {
                window.open("firebag.aspx?eqid=" + eqid + "&date=" + Date());
            }
        }
        function getdown() {
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("w_dates.aspx?whi=tdown&eqid=&sid=" + sid, "", "dialogHeight:450px; dialogWidth:400px; resizable=yes");
        }
        function getdown3() {
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("w_dates_lst.aspx?sid=" + sid, "", "dialogHeight:450px; dialogWidth:450px; resizable=yes");
        }
        function getwoeq() {
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("w_locs.aspx?sid=" + sid, "", "dialogHeight:400px; dialogWidth:400px; resizable=yes");
        }
        function gethist() {
            var sid = document.getElementById("lblsid").value;
            var eqid = document.getElementById("lbleqid").value;
            var eReturn = window.showModalDialog("w_dates.aspx?whi=eqhist&sid=" + sid + "&eqid=" + eqid, "", "dialogHeight:400px; dialogWidth:400px; resizable=yes");
        }
        function getemd(who) {
            var sid = document.getElementById("lblsid").value;
            var fdate;
            var tdate;
            if (who == "open_no") {
                window.open("emd_reps.aspx?who=" + who + "&sid=" + sid + "&fdate=&tdate=&date=" + Date());
            }
            else {
                var eReturn = window.showModalDialog("e_dates.aspx?sid=" + sid, "", "dialogHeight:300px; dialogWidth:300px; resizable=yes");
                if (eReturn) {
                    var dta = eReturn.split(",");
                    fdate = dta[0];
                    tdate = dta[1];
                    document.getElementById("lblewho").value = who;
                    document.getElementById("lblfdate").value = fdate;
                    document.getElementById("lbltdate").value = tdate;
                    document.getElementById("lbleflag").value = "1";
                    //alert(fdate + "," + tdate)
                    //window.open("e_reps.aspx?who=" + who + "&sid=" + sid + "&fdate=" + fdate + "&tdate=" + tdate + "&date=" + Date());
                    document.getElementById("form1").submit();
                }
            }
        }
        function checke() {
            var sid = document.getElementById("lblsid").value;
            var who = document.getElementById("lblewho").value;
            var fdate = document.getElementById("lblfdate").value;
            var tdate = document.getElementById("lbltdate").value;
            if (who != "" && fdate != "" && tdate != "") {
                document.getElementById("lblewho").value = "";
                document.getElementById("lblfdate").value = "";
                document.getElementById("lbltdate").value = "";
                document.getElementById("lbleflag").value = "";
                window.open("emd_reps.aspx?who=" + who + "&sid=" + sid + "&fdate=" + fdate + "&tdate=" + tdate + "&date=" + Date());
            }
        }
        function getminsrch() {
            var did = document.getElementById("lbldid").value;
            //alert(did)
            //if (did != "") {
            //retminsrch();
            //}
            //else {
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "lup";

            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("lbldept").value = ret[1];
                document.getElementById("tddept").innerHTML = ret[1];
                document.getElementById("lblclid").value = ret[2];
                document.getElementById("lblcell").value = ret[3];
                document.getElementById("tdcell").innerHTML = ret[3];
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("lbleq").value = ret[5];
                document.getElementById("tdeq1").innerHTML = ret[5];
                document.getElementById("trdepts").className = "view";
                document.getElementById("lblrettyp").value = "depts";

                if (ret[4] != "") {
                    hclass();
                    document.getElementById("tdeq").innerHTML = "Current Equipment:  " + ret[5];
                    document.getElementById("lbleqnum").value = ret[5];
                }
                else {
                    if (ret[0] != "") {
                        document.getElementById("lblsubmit").value = "first";
                        document.getElementById("form1").submit();
                    }
                }

            }
            //}
        }

        function getlocs1() {
            var sid = document.getElementById("lblsid").value;
            var lid = document.getElementById("lbllid").value;
            var typ = "lu"
            //alert(lid)
            if (lid != "") {
                typ = "retloc"
            }
            var eqid = document.getElementById("lbleqid").value;
            var fuid = ""; // document.getElementById("lblfuid").value;
            var coid = ""; // document.getElementById("lblcoid").value;
            //alert(fuid)
            var wo = "";
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                //ret = lidi + "~" + lid + "~" + loc + "~" + eq + "~" + eqnum + "~" + fu + "~" + func + "~" + co + "~" + comp + "~" + lev;
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("lblloc").value = ret[1];
                document.getElementById("tdloc3").innerHTML = ret[2];

                document.getElementById("lbleq").value = ret[4];
                document.getElementById("tdeq3").innerHTML = ret[4];
                document.getElementById("lbleqid").value = ret[3];
                document.getElementById("trlocs").className = "view";
                document.getElementById("lblrettyp").value = "locs";

                if (ret[3] != "") {
                    hclass();
                    document.getElementById("tdeq").innerHTML = "Current Equipment:  " + ret[4];
                    document.getElementById("lbleqnum").value = ret[4];
                }
                else {
                    if (ret[0] != "") {
                        document.getElementById("lblsubmit").value = "first";
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        function clearall() {
            document.getElementById("lbldid").value = "";
            document.getElementById("lbldept").value = "";
            document.getElementById("tddept").innerHTML = "";
            document.getElementById("lblclid").value = "";
            document.getElementById("lblcell").value = "";
            document.getElementById("tdcell").innerHTML = "";
            document.getElementById("lbleqid").value = "";
            document.getElementById("lbleq").value = "";
            document.getElementById("tdeq1").innerHTML = "";

            document.getElementById("lbllid").value = "";
            document.getElementById("lblloc").value = "";
            document.getElementById("tdloc3").innerHTML = "";
            document.getElementById("tdeq3").innerHTML = "";

            document.getElementById("trdepts").className = "details";
            document.getElementById("trlocs").className = "details";
            document.getElementById("lblrettyp").value = "";
        }
        function getdeq(did, clid, eqid, eqnum, eqdesc, mcnt, lid, usetot) {
            //document.getElementById("lblwho").value = "menu";
            document.getElementById("lblmeascnt").value = mcnt;
            //alert(document.getElementById("lblwho").value)
            //alert(usetot)
            document.getElementById("lblusetot").value = usetot;
            document.getElementById("lbllid").value = lid;
            document.getElementById("lbldid").value = did;
            document.getElementById("lblclid").value = clid;
            document.getElementById("lbleqid").value = eqid;
            document.getElementById("lbleqnum").value = eqnum;
            document.getElementById("lbleqdesc").value = eqdesc;
            document.getElementById("tdeq").innerHTML = "Current Equipment: " + eqnum + " - " + eqdesc;
            document.getElementById("lblfuid").value = "";
            hclass();
            var ret = document.getElementById("lblrettyp").value;
            if (ret != "") {
                if (ret == "depts") {
                    document.getElementById("trdepts").className = "view";
                    document.getElementById("trlocs").className = "details";
                }
                else if (ret == "locs") {
                    document.getElementById("trdepts").className = "details";
                    document.getElementById("trlocs").className = "view";
                }
            }
        }
        var winbx = "directories=0,height=600,width=720,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
        function getpmot() {
            var eq = document.getElementById("lbleqid").value;
            window.open("../appsdrag/pmotime.aspx?eqid=" + eq, "winbx", "width=900, height=700, scrollbars=1");

        }
        /*
        <tr>
        <td class="details" height="20">
        <a id="a3" onclick="getasnmnt();" href="#">
        <asp:Label ID="lang3378" runat="server">PM Assignment Report</asp:Label></a>
        </td>
        </tr>
        */
        //-->
    </script>
</head>
<body class="tbg" onload="checkhref();checkit();">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 0px; left: 4px" border="0" cellspacing="0"
        cellpadding="0" width="1080">
        <tr>
            <td width="26">
            </td>
            <td width="244">
            </td>
            <td width="230">
            </td>
            <td width="5">
            </td>
            <td width="26">
            </td>
            <td width="214">
            </td>
            <td width="5">
            </td>
            <td width="26">
            </td>
            <td width="304">
            </td>
        </tr>
        <tr>
            <td colspan="9">
                <table id="tdtop">
                    <tr>
                        <td height="62" valign="top" width="230">
                            <img id="imglogo" runat="server" src="../menu/mimages/fusionsm.gif">
                        </td>
                        <td valign="top" width="760" align="right">
                            <img id="imghdr" runat="server" src="../images/appheaders/rep.gif">
                        </td>
                        <td valign="top" width="80" align="right">
                            <img id="btnreturn" onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td id="tdeq" class="plainlabelblue" height="30" colspan="9" runat="server">
                <asp:Label ID="lang3350" runat="server">No Equipment Record Selected</asp:Label>
            </td>
        </tr>
        <tr id="trhead" runat="server">
            <td class="thdrsinglft">
                <img src="../images/appbuttons/minibuttons/reporthd.gif">
            </td>
            <td class="thdrsingrt label" colspan="2" align="left">
                <asp:Label ID="lang3351" runat="server">Required Report Details</asp:Label>
            </td>
            <td>
                <img src="../images/appbuttons/minibuttons/4PX.gif">
            </td>
            <td class="thdrsinglft">
                <img src="../images/appbuttons/minibuttons/reporthd.gif">
            </td>
            <td class="thdrsingrt label">
                <asp:Label ID="lang3352" runat="server">PM Progress Reports</asp:Label>
            </td>
            <td>
                <img src="../images/appbuttons/minibuttons/4PX.gif">
            </td>
            <td class="thdrsinglft">
                <img src="../images/appbuttons/minibuttons/reporthd.gif">
            </td>
            <td class="thdrsingrt label">
                <asp:Label ID="lang3353" runat="server">PM Output Reports</asp:Label>
            </td>
        </tr>
        <tr id="trmain" runat="server">
            <td valign="top" rowspan="3" colspan="2">
                <table width="270">
                    <tr>
                        <td class="btmmenu plainlabel" colspan="2">
                            <asp:Label ID="lang3354" runat="server">Lookup Location</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td id="tddepts" class="bluelabel" runat="server">
                                        Use Departments
                                    </td>
                                    <td>
                                        <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                                    </td>
                                    <td id="tdlocs1" class="bluelabel" runat="server">
                                        Use Locations
                                    </td>
                                    <td>
                                        <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trdepts" class="details" runat="server">
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td class="label" width="110">
                                        Department
                                    </td>
                                    <td id="tddept" class="plainlabel" width="170" runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        Station\Cell
                                    </td>
                                    <td id="tdcell" class="plainlabel" runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        Equipment
                                    </td>
                                    <td id="tdeq1" class="plainlabel" runat="server">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trlocs" class="details" runat="server">
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td class="label" width="110">
                                        Location
                                    </td>
                                    <td id="tdloc3" class="plainlabel" width="170" runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        Equipment
                                    </td>
                                    <td id="tdeq3" class="plainlabel" runat="server">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
            </td>
        </tr>
        <tr>
            <td class="btmmenu plainlabel" colspan="2">
                <asp:Label ID="lang3358" runat="server">Search Equipment</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang3359" runat="server">Equipment#</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txteqnum" runat="server" CssClass="plainlabel wd160" Width="160px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang3360" runat="server">Description</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txteqdesc" runat="server" CssClass="plainlabel wd160" Width="160px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                SPL
            </td>
            <td>
                <asp:TextBox ID="txtspl" runat="server" CssClass="plainlabel wd160" Width="160px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                OEM
            </td>
            <td>
                <asp:TextBox ID="txtoem" runat="server" CssClass="plainlabel wd160" Width="160px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang3361" runat="server">Model</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtmodel" runat="server" CssClass="plainlabel wd160" Width="160px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang3362" runat="server">Serial#</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtserial" runat="server" CssClass="plainlabel wd160" Width="160px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang3363" runat="server">Asset Class</asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddac" runat="server" CssClass="plainlabel wd160">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <img onclick="srcheq();" src="../images/appbuttons/minibuttons/srchsm.gif">
                <img onclick="goback();" src="../images/appbuttons/minibuttons/refreshit.gif">
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2">
                <table cellspacing="1" cellpadding="1" width="270">
                    <tr id="Tr1" runat="server">
                        <td colspan="2">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="thdrsinglft" width="26">
                                        <img src="../images/appbuttons/minibuttons/reporthd.gif">
                                    </td>
                                    <td class="thdrsingrt label" width="244">
                                        <asp:Label ID="lang3364" runat="server">PM Forms</asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabelu" height="22">
                            <a onclick="getfuid();" href="#">
                                <asp:Label ID="lang3365" runat="server">Function ID List</asp:Label></a>
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabelu" height="22">
                            <a onclick="getcoid();" href="#">
                                <asp:Label ID="lang3366" runat="server">Component ID List</asp:Label></a>
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabelu" height="22">
                            <a onclick="getpmo1();" href="#">
                                <asp:Label ID="lang3367" runat="server">PMO Checklist</asp:Label></a>
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabelu" height="22">
                            <a onclick="getpmo2();" href="#">
                                <asp:Label ID="lang3368" runat="server">PMO Title Sheet</asp:Label></a>
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabelu" height="22">
                            <a onclick="getform();" href="#">
                                <asp:Label ID="lang3369" runat="server">PM Data Collection Form</asp:Label></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </td>
    <td valign="top" rowspan="3">
        <table width="270">
            <tr>
                <td class="btmmenu plainlabel">
                    <asp:Label ID="lang3370" runat="server">Choose From List</asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="border-bottom: black 1px solid; border-left: black 1px solid; width: 270px;
                        height: 257px; overflow: auto; border-top: black 1px solid; border-right: black 1px solid"
                        id="diveq" runat="server">
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                        border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" valign="middle" width="190" align="center">
                                <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                    runat="server">
                            </td>
                            <td width="20">
                                <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                    runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="2">
                    <table cellspacing="1" cellpadding="1" width="270">
                        <tr id="Tr5" runat="server">
                            <td colspan="2">
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="thdrsinglft" width="26">
                                            <img src="../images/appbuttons/minibuttons/reporthd.gif">
                                        </td>
                                        <td class="thdrsingrt label" width="244">
                                            <asp:Label ID="lang3371" runat="server">Misc</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="tdtpmw1" onclick="gettpmw1();" href="#">
                                                <asp:Label ID="lang3372" runat="server">OP Weekly Tasks</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="tdtpmw2" onclick="gettpmw2();" href="#">
                                                <asp:Label ID="lang3373" runat="server">OP Non-Weekly Tasks</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="tdtpmw3" onclick="gettpmw3();" href="#">OP Weekly Tasks (Display)</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="tdtpmw4" onclick="gettpmw4();" href="#">OP Monthly/Weekly Chart</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="hrefstask1" onclick="getstask1();" href="#">Operator Care Task Cards</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="r1" onclick="getoptsumpss('rep');" href="#">
                                                <asp:Label ID="Label13" runat="server">CMMS Output - Asset Select</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="A313" onclick="getpmrepoutput();" href="#">
                                                <asp:Label ID="Label14" runat="server">PM Report Output</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="A617" onclick="getpmrepoutput2();" href="#">
                                                <asp:Label ID="Label617" runat="server">PM Report Output 2</asp:Label></a>
                                        </td>
                                    </tr>
                                    <!--<tr id="trrbas1" runat="server">
                            <td class="bluelabelu" height="20" colspan="2">
                                <a id="A3" onclick="getpmrepoutputr('RBAS');" href="#">
                                    <asp:Label ID="Label15" runat="server">Report Based PM Route Output</asp:Label></a>
                            </td>
                        </tr>
                        <tr id="trrbas2" runat="server">
                            <td class="bluelabelu" height="20" colspan="2">
                                <a id="A29" onclick="getpmrepoutputr('RBASM');" href="#">
                                    <asp:Label ID="Label17" runat="server">Report Based PM Route Output - Mod</asp:Label></a>
                            </td>
                        </tr>
                        <tr id="trrbas22" runat="server">
                            <td class="bluelabelu" height="20" colspan="2">
                                <a id="A29T" onclick="getpmrepoutputr('RBASM2');" href="#">
                                    <asp:Label ID="Label22" runat="server">Report Based PM Route Output - Mod2</asp:Label></a>
                            </td>
                        </tr>-->
                                    <tr id="trcomplibx" runat="server">
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="A38" onclick="getpmrepoutputr('COMPx');" href="#">
                                                <asp:Label ID="Label15clx" runat="server">Component Library Excel Output</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr id="trcomplibwk" runat="server">
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="A40a" onclick="getxout('COMPwk');" href="#">
                                                <asp:Label ID="Label15clwk" runat="server">Component Library Weekly</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr id="trrbas3" runat="server">
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="A34" onclick="getpmrepoutputr('RBASM3');" href="#">
                                                <asp:Label ID="Label26" runat="server">Route Report with Pictures</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr id="trrbas33" runat="server">
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="A35" onclick="getpmrepoutputr('RBASM3NP');" href="#">
                                                <asp:Label ID="Label27" runat="server">Route Report without Pictures</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr id="trrbas35np" runat="server">
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="A29" onclick="getpmrepoutputr('RBASM3NP');" href="#">
                                                <asp:Label ID="Label35" runat="server">Route Report without Pictures (Demo)</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr id="treqrte" runat="server">
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="A37" onclick="geteqrt();" href="#">
                                                <asp:Label ID="Label15eqrt" runat="server">Equipment Routing (Demo)</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr id="trrbas34" runat="server">
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="A3" onclick="getpmrepoutputr('RBASM3x');" href="#">
                                                <asp:Label ID="Label15x" runat="server">Route Report Excel Output</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr id="trrbas34s" runat="server">
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="A30" onclick="getpmrepoutputr('RBASM3xa');" href="#">
                                                <asp:Label ID="Label15s" runat="server">Route Report Excel Output (Site)</asp:Label></a>
                                        </td>
                                    </tr>
                                    <!--<tr id="trrbast1" runat="server">
                            <td class="bluelabelu" height="20" colspan="2">
                                <a id="A28" onclick="getpmrepoutputr('RBAST');" href="#">
                                    <asp:Label ID="Label16" runat="server">Report Based PM Route Output (TPM)</asp:Label></a>
                            </td>
                        </tr>
                        <tr id="trrbast2" runat="server">
                            <td class="bluelabelu" height="20" colspan="2">
                                <a id="A30" onclick="getpmrepoutputr('RBASTM');" href="#">
                                    <asp:Label ID="Label18" runat="server">Report Based PM Route Output (TPM) - Mod</asp:Label></a>
                            </td>
                        </tr>-->
                                    <tr id="trrbast3" runat="server">
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="A31" onclick="getpmrepoutputr('RBASTM2');" href="#">
                                                <asp:Label ID="Label23" runat="server">TPM Route Report (English)</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr id="trrbast34" runat="server">
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="A28" onclick="getpmrepoutputr('RBASTM2x');" href="#">
                                                <asp:Label ID="Label15xt" runat="server">TPM Route Report Excel Output</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr id="trrbast34s" runat="server">
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="A36" onclick="getpmrepoutputr('RBASTM2xa');" href="#">
                                                <asp:Label ID="Label16s" runat="server">TPM Route Report Excel Output (site)</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr id="trrbast4" runat="server">
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="A32" onclick="getpmrepoutlist('RBAS');" href="#">
                                                <asp:Label ID="Label24" runat="server">PM Route Report List</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr id="trrbast5" runat="server">
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="A33" onclick="getpmrepoutlist('RBAST');" href="#">
                                                <asp:Label ID="Label25" runat="server">TPM Route Report List</asp:Label></a>
                                        </td>
                                    </tr>

                                    <tr id="trl0" runat="server">
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="AL0" onclick="getpmrepoutputr('L0');" href="#">
                                                <asp:Label ID="Label15l0" runat="server">LEVEL 0 - BACKLOG TASK AND TIME ANNUAL</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr id="trl1" runat="server">
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="AL1" onclick="getpmrepoutputr('L1');" href="#">
                                                <asp:Label ID="Label16l1" runat="server">LEVEL 1 - BACKLOG TASK AND TIME ANNUAL</asp:Label></a>
                                        </td>
                                    </tr>
                                    <tr id="trl2" runat="server">
                                        <td class="bluelabelu" height="20" colspan="2">
                                            <a id="AL2" onclick="getpmrepoutputr('L2');" href="#">
                                                <asp:Label ID="Label17l2" runat="server">Equipment - BACKLOG TASK AND TIME ANNUAL</asp:Label></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
    <td rowspan="3">
        <img src="../images/appbuttons/minibuttons/4PX.gif">
    </td>
    <td valign="top" rowspan="3" colspan="2">
        <table cellspacing="1" cellpadding="1" width="240">
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a1" onclick="getmissreport();" href="#">
                        <asp:Label ID="lang3374" runat="server">Asset Missing Data Report</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a1c" onclick="getcompnotasks();" href="#">
                        <asp:Label ID="lang3375" runat="server">Components without Tasks Report</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a2" onclick="getfailnotasks();" href="#">
                        <asp:Label ID="lang3376" runat="server">Failure Modes Not Addressed</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a1e" onclick="gettsnotasks('tt');" href="#">
                        <asp:Label ID="lang3377" runat="server">Tasks with No Task Status</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a1e1" onclick="gettsnotasks('ts');" href="#">
                        <asp:Label ID="Label6" runat="server">Tasks with No Task Type</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a3anew" onclick="getasnmnt2();" href="#">
                        <asp:Label ID="Label5" runat="server">Total PM Report</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a3anewa" onclick="getasnmnt2a();" href="#">
                        <asp:Label ID="Label8" runat="server">Total PM Report w/Optimization</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a3clib" onclick="getasclib();" href="#">
                        <asp:Label ID="Label19" runat="server">Component Library Assignment Report</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a3excel" onclick="getasnmnt3();" href="#">
                        <asp:Label ID="Label7" runat="server">PM Assignment Report (Excel)</asp:Label></a>
                </td>
            </tr>
            <tr id="tramgassn" runat="server">
                <td class="bluelabelu" height="20" id="tramrp1">
                    <a id="a39" onclick="getasnmnt4();" href="#">
                        <asp:Label ID="Label15amg" runat="server">Amgen Assignment Report (Excel)</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a3a" onclick="getasnmnta();" href="#">
                        <asp:Label ID="lang3379" runat="server">PM Assignment Report w\images</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a3tpm" onclick="getasnmnttpm();" href="#">
                        <asp:Label ID="lang3380" runat="server">OP Assignment Report</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a4" onclick="getinprog();" href="#">
                        <asp:Label ID="lang3381" runat="server">Site "In Progress" Report</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a5" onclick="getassetclass();" href="#">
                        <asp:Label ID="lang3382" runat="server">Site Asset Class Report</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a6" onclick="geteqd();" href="#">
                        <asp:Label ID="lang3383" runat="server">Equipment Details</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a7" onclick="geteqhier();" href="#">
                        <asp:Label ID="lang3384" runat="server">Equipment Hierarchy Report</asp:Label></a>
                </td>
            </tr>
            <tr id="Tr3" runat="server">
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="thdrsinglft" width="26">
                                <img src="../images/appbuttons/minibuttons/reporthd.gif">
                            </td>
                            <td class="thdrsingrt label" width="214">
                                <asp:Label ID="lang3385" runat="server">PM Documentation Reports</asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a8" onclick="getoptrat('all');" href="#">
                        <asp:Label ID="lang3386" runat="server">PMO Rationale Report - All Records</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a9" onclick="getoptrat('ronly');" href="#">
                        <asp:Label ID="lang3387" runat="server">PMO Rationale Report - Rationale Only</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a10" onclick="getoptrat('chngonly');" href="#">
                        <asp:Label ID="lang3388" runat="server">PMO Rationale Report - Changes Only</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a8tpm" onclick="getoptrattpm('all');" href="#">
                        <asp:Label ID="lang3389" runat="server">TPMO Rationale Report - All Records</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a9tpm" onclick="getoptrattpm('ronly');" href="#">
                        <asp:Label ID="lang3390" runat="server">TPMO Rationale Report - Rationale Only</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="a10tpm" onclick="getoptrattpm('chngonly');" href="#">
                        <asp:Label ID="lang3391" runat="server">TPMO Rationale Report - Changes Only</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <table cellspacing="1" cellpadding="1" width="240">
                        <tr id="Tr2" runat="server">
                            <td colspan="2">
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="thdrsinglft" width="26">
                                            <img src="../images/appbuttons/minibuttons/reporthd.gif">
                                        </td>
                                        <td class="thdrsingrt label" width="214">
                                            <asp:Label ID="lang3392" runat="server">PM Optimization Reports</asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabelu" height="20">
                                <a id="a11" onclick="getoptsum();" href="#">
                                    <asp:Label ID="lang3393" runat="server">PM Optimization Overview</asp:Label></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabelu" height="20">
                                <a id="a11orig" onclick="getoptsum_orig();" href="#">
                                    <asp:Label ID="Label20" runat="server">PMO Overview - Original Tasks Pie Chart</asp:Label></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabelu" height="20">
                                <a id="a11rev" onclick="getoptsum_rev();" href="#">
                                    <asp:Label ID="Label21" runat="server">PMO Overview - Revised Tasks Pie Chart</asp:Label></a>
                            </td>
                        </tr>
                        <tr class="details">
                            <td class="bluelabelu" height="20">
                                <a id="a11a" onclick="getoptsumop('e');" href="#">
                                    <asp:Label ID="lang3394" runat="server">PM Optimization Overview (No Op)</asp:Label></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabelu" height="20">
                                <a id="a12" onclick="getoptsumps();" href="#">
                                    <asp:Label ID="lang3395" runat="server">PM Optimization Overview - Site Rollup</asp:Label></a>
                            </td>
                        </tr>
                        <tr class="details">
                            <td class="bluelabelu" height="20">
                                <a id="a11b" onclick="getoptsumop('s');" href="#">
                                    <asp:Label ID="Label9" runat="server">PM Optimization Overview (No Op) - Site Rollup</asp:Label></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabelu" height="20">
                                <a id="a13" onclick="getoptsumpss('pmo');" href="#">
                                    <asp:Label ID="lang3396" runat="server">PM Optimization Overview - Asset Select</asp:Label></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabelu" height="20">
                                <a id="a24" onclick="getpmot();" href="#">
                                    <asp:Label ID="Label3" runat="server">PMO Time Overview (Asset)</asp:Label></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabelu" height="20">
                                <a id="a18" onclick="getoptrev();" href="#">
                                    <asp:Label ID="lang3397" runat="server">PM Optimization Raw Data (Asset)</asp:Label></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabelu" height="20">
                                <a id="a14" onclick="getfldrpt();" href="#">
                                    <asp:Label ID="lang3398" runat="server">PM Optimization Field Report</asp:Label></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabelu" height="20">
                                <a id="a15" onclick="getoptsumtpm();" href="#">
                                    <asp:Label ID="lang3399" runat="server">OP Optimization Overview</asp:Label></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabelu" height="20">
                                <a id="a16" onclick="getoptsumpstpm();" href="#">
                                    <asp:Label ID="lang3400" runat="server">OP Optimization Overview - Site Rollup</asp:Label></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabelu" height="20">
                                <a id="a19" onclick="getoptrevtpm();" href="#">
                                    <asp:Label ID="lang3401" runat="server">OP Optimization Raw Data (Asset)</asp:Label></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabelu" height="20">
                                <a id="a17" onclick="getfldrpttpm();" href="#">
                                    <asp:Label ID="lang3402" runat="server">OP Optimization Field Report</asp:Label></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabelu" height="20">
                                <a id="o10" onclick="getusetot();" href="#">
                                    <asp:Label ID="Label12" runat="server">PMO Total Time Dialog</asp:Label></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
    <td rowspan="3">
        <img src="../images/appbuttons/minibuttons/4PX.gif">
    </td>
    <td valign="top" rowspan="3" colspan="2">
        <table>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="pgemstxt1" title="Output to Text File - Requires Lower Internet Security Settings"
                        onclick="getpgemstxt('1');" href="#">
                        <asp:Label ID="lang3403" runat="server">CMMS PM Output (text)</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="pgemstxthtm" title="Outputs Text version to HTML format - Use if you cannot open Text Output Above"
                        onclick="getpgemshtm('1');" href="#">
                        <asp:Label ID="lang3404" runat="server">CMMS PM Output (text to html)</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="pgemstxthtm3" title="Outputs Text version to HTML format with No Check Boxes for Failure Modes"
                        onclick="getpgemshtm3('1');" href="#">
                        <asp:Label ID="Label4" runat="server">CMMS PM Output - No Check (text to html)</asp:Label></a>
                </td>
            </tr>
            <tr id="trnorm1" runat="server">
                <td class="bluelabelu" height="20">
                    <a id="pgemstxtwrd" title="Outputs Text version to Word in your Browser" onclick="getpgemswrd('1');"
                        href="#">
                        <asp:Label ID="lang3405" runat="server">CMMS PM Output (MS Word in Browser)</asp:Label></a>
                </td>
            </tr>
            <tr id="trnorm2" runat="server">
                <td class="bluelabelu" height="20">
                    <a id="pgemstxtwrd1" title="Outputs Text version to MS Word on your Desktop" onclick="getpgemswrd('2');"
                        href="#">
                        <asp:Label ID="lang3406" runat="server">CMMS PM Output (MS Word)</asp:Label></a>
                </td>
            </tr>
            <tr id="tr4" runat="server">
                <td class="bluelabelu" height="20">
                    <a id="pgemstxtwrd1sym" title="Outputs Text version to MS Word on your Desktop" onclick="getpgemswrd('S');"
                        href="#">
                        <asp:Label ID="Label11" runat="server">CMMS PM Output (MS Word)(All)</asp:Label></a>
                </td>
            </tr>
            <tr id="trgsk1" runat="server">
                <td class="bluelabelu" height="22">
                    <a id="A20" title="Outputs Text version to Word in your Browser" onclick="getpgemswrd2('1');"
                        href="#">CMMS Datastream PM Output (MS Word in Browser)</a>
                </td>
            </tr>
            <tr id="tdgsk2" runat="server">
                <td class="bluelabelu" height="22">
                    <a id="A21" title="Outputs Text version to MS Word on your Desktop" onclick="getpgemswrd2('2');"
                        href="#">CMMS Datastream PM Output (MS Word)</a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="A27" title="Print version with formatting of CMMS Output" onclick="getspring('1');"
                        href="#">
                        <asp:Label ID="Label10" runat="server">CMMS PM Output (html)(Materials on Top)</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="pgemshtm1" title="Print version with formatting of CMMS Output" onclick="getpgemshtml('1');"
                        href="#">
                        <asp:Label ID="lang3407" runat="server">CMMS PM Output (html)</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="pgemstxt2" title="PdM Output to Text File - Requires Lower Internet Security Settings"
                        onclick="getpgemstxt('2');" href="#">
                        <asp:Label ID="lang3408" runat="server">CMMS PM PdM Output (text)</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="pgemstxthtm1" title="Outputs PdM Text version to HTML format - Use if you cannot open Text Output Above"
                        onclick="getpgemshtm('2');" href="#">
                        <asp:Label ID="lang3409" runat="server">CMMS PM PdM Output (text to html)</asp:Label></a>
                </td>
            </tr>
            <tr id="trnorm3" runat="server">
                <td class="bluelabelu" height="20">
                    <a id="pgemstxtwrd3" title="Outputs Text version to Word in your Browser" onclick="getpgemswrd('3');"
                        href="#">
                        <asp:Label ID="lang3410" runat="server">CMMS PM PdM Output (MS Word in Browser)</asp:Label></a>
                </td>
            </tr>
            <tr id="trnorm4" runat="server">
                <td class="bluelabelu" height="20">
                    <a id="pgemstxtwrd4" title="Outputs Text version to MS Word on your Desktop" onclick="getpgemswrd('4');"
                        href="#">
                        <asp:Label ID="lang3411" runat="server">CMMS PM PdM Output (MS Word)</asp:Label></a>
                </td>
            </tr>
            <tr id="tdgsk3" runat="server">
                <td class="bluelabelu" height="20">
                    <a id="A22" title="Outputs Text version to Word in your Browser" onclick="getpgemswrd2('3');"
                        href="#">
                        <asp:Label ID="Label1" runat="server">CMMS Datastream PM PdM Output (MS Word in Browser)</asp:Label></a>
                </td>
            </tr>
            <tr id="tdgsk4" runat="server">
                <td class="bluelabelu" height="20">
                    <a id="A23" title="Outputs Text version to MS Word on your Desktop" onclick="getpgemswrd2('4');"
                        href="#">
                        <asp:Label ID="Label2" runat="server">CMMS Datastream PM PdM Output (MS Word)</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="pgemshtm12" title="Print version with formatting of CMMS PdM Output" onclick="getpgemshtml('2');"
                        href="#">
                        <asp:Label ID="lang3412" runat="server">CMMS PM PdM Output (html)</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="pgemstxt1tpm" letit="Output to Text File - Requires Lower Internet Security Settings"
                        onclick="getpgemstxt('3');" href="#">
                        <asp:Label ID="lang3413" runat="server">CMMS OP Output (text)</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="pgemstxthtmtpm" title="Outputs Text version to HTML format - Use if you cannot open Text Output Above"
                        onclick="getpgemshtm('3');" href="#">
                        <asp:Label ID="lang3414" runat="server">CMMS OP Output (text to html)</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="pgemshtm1tpm" title="Print version with formatting of CMMS Output" onclick="getpgemshtml('3');"
                        href="#">
                        <asp:Label ID="lang3415" runat="server">CMMS OP Output (html)</asp:Label></a>
                </td>
            </tr>
            <tr>
                <td class="bluelabelu" height="20">
                    <a id="pgemshtm1tpmpic" title="Print version with formatting of CMMS Output" target="_top"
                        onclick="getpgemshtmlpic('3');" href="#">
                        <asp:Label ID="lang3416" runat="server">CMMS OP Output (html) w\images</asp:Label></a>
                </td>
            </tr>
            <tr id="tdemd1" runat="server">
                <td class="bluelabelu" height="20">
                    <a id="A25eee" onclick="getemd('open_no');" href="#">Jobs By Work Type and Supervisor</a>
                </td>
            </tr>
            <tr id="tdemd2" runat="server">
                <td class="bluelabelu" height="20">
                    <a id="A26eee" onclick="getemd('open_dt');" href="#">Jobs By Work Type and Supervisor
                        (by Date)</a>
                </td>
            </tr>
            <tr id="tdemd3" runat="server">
                <td class="bluelabelu" height="20">
                    <a id="A27eee" onclick="getemd('closed_dt');" href="#">Closed Jobs By Work Type and
                        Supervisor (by Date)</a>
                </td>
            </tr>
            <tr id="tdwoeq" runat="server">
                <td class="bluelabelu" height="20">
                    <a id="A26" onclick="getwoeq();" href="#">Open Work Order by Equipment Report</a>
                </td>
            </tr>
            <tr id="tddwn1" runat="server">
                <td class="details" height="20">
                    <a id="A25" onclick="getdown();" href="#">Total Equipment Downtime (by Date)</a>
                </td>
            </tr>
            <tr id="tddwn3" runat="server">
                <td class="details" height="20">
                    <a id="A25_3" onclick="getdown3();" href="#">Total Equipment Downtime (3 Month)</a>
                </td>
            </tr>
            <tr>
                <td height="20" class="details">
                    <a id="tddownhist" onclick="gethist();" href="#">Equipment Downtime History</a>
                </td>
            </tr>
            <tr id="tdsun1" runat="server">
                <td class="bluelabelu" height="20">
                    <a id="A25sun" onclick="getfire();" href="#">Firebag Equipment Strategy</a>
                </td>
            </tr>
            <tr id="Tr6" runat="server" class="details">
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="thdrsinglft" width="26">
                                <img src="../images/appbuttons/minibuttons/reporthd.gif">
                            </td>
                            <td class="thdrsingrt label" width="244">
                                <asp:Label ID="lang3417" runat="server">Operator Care (Weekly)</asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="details">
                <td class="bluelabelu" height="20">
                    <a id="hrefstask2" onclick="getstask2();" href="#">
                        <asp:Label ID="lang3418" runat="server">Operator Care Weekly Tasks (All)</asp:Label></a>
                </td>
            </tr>
            <tr class="details">
                <td class="bluelabelu" height="20">
                    <a id="hrefstask3" onclick="getstask3();" href="#">
                        <asp:Label ID="lang3419" runat="server">Operator Care Weekly Tasks Week Planner (Print)</asp:Label></a>
                </td>
            </tr>
            <tr class="details">
                <td class="bluelabelu" height="20">
                    <a id="hrefstask4" onclick="getstask4();" href="#">
                        <asp:Label ID="lang3420" runat="server">Operator Care Weekly Tasks Week Planner (Screen)</asp:Label></a>
                </td>
            </tr>
        </table>
    </td>
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server"><input id="lblpgemstxt"
        type="hidden" name="lblpgemstxt" runat="server">
    <input id="lbleqnum" type="hidden" name="lbleqnum" runat="server"><input id="lblpgemshref"
        type="hidden" name="Hidden1" runat="server">
    <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lbldid" type="hidden" name="lbldid" runat="server">
    <input id="lblclid" type="hidden" name="lblclid" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server">
    <input id="lblmod" type="hidden" name="lblmod" runat="server">
    <input id="lblredir" type="hidden" name="lblredir" runat="server"><input id="lblsubmit"
        type="hidden" runat="server">
    <input id="txtpgcnt" type="hidden" runat="server"><input id="txtpg" type="hidden"
        runat="server">
    <input id="lbleqdesc" type="hidden" name="lbleqdesc" runat="server">
    <input id="lblspl" type="hidden" name="lblspl" runat="server">
    <input id="lblmodel" type="hidden" name="lblmodel" runat="server">
    <input id="lbloem" type="hidden" name="lbloem" runat="server">
    <input id="lblserial" type="hidden" name="lblserial" runat="server">
    <input id="lblac" type="hidden" name="lblac" runat="server">
    <input id="lbleqnumsrch" type="hidden" runat="server">
    <input id="lblrepcnt" type="hidden" runat="server">
    <input id="lblmeascnt" type="hidden" runat="server"><input type="hidden" id="lblwho"
        runat="server" name="lblwho">
    <input type="hidden" id="lbllid" runat="server" name="lbllid"><input type="hidden"
        id="lblwho2" runat="server" name="lblwho2">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblrettyp" runat="server" />
    <input type="hidden" id="lbldept" runat="server" />
    <input type="hidden" id="lblcell" runat="server" />
    <input type="hidden" id="lblloc" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lblusername" runat="server" />
    <input type="hidden" id="lblfdate" runat="server" />
    <input type="hidden" id="lbltdate" runat="server" />
    <input type="hidden" id="lblewho" runat="server" />
    <input type="hidden" id="lbleflag" runat="server" />
    <input type="hidden" id="lblcoi" runat="server" />
    <input type="hidden" id="lblusetot" runat="server" />
    </form>
</body>
</html>
