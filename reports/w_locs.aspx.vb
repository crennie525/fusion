﻿Public Class w_locs
    Inherits System.Web.UI.Page
    Dim sid, coi As String
    Dim mutils As New mmenu_utils_a
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            sid = Request.QueryString("sid").ToString '"23"
            lblsid.Value = sid
            coi = mutils.COMPI
            If coi <> "EMD" Then
                tremd.Attributes.Add("class", "details")
                treddept.Attributes.Add("class", "details")
            End If
        End If
    End Sub

End Class