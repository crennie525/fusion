<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ttno.aspx.vb" Inherits="lucy_r12.ttno"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ttno</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<LINK rel="stylesheet" type="text/css" href="../styles/reports.css">
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="javascript" type="text/javascript">
		    function gettask(fid, coid, comp, aid, tskid) {
		        //'skillid, skillqty, rdid, meterid, freq
		//document.getElementById("lbltasknum").value = task;
		//handleexit(task)
		var fuid = document.getElementById("lblfuid").value
		var cid = document.getElementById("lblcid").value;
		var sid = document.getElementById("lblsid").value;
		var did = document.getElementById("lbldid").value;
		var clid = document.getElementById("lblclid").value;
		var eqid = document.getElementById("lbleqid").value;
		var chk = document.getElementById("lblchk").value;
		var who = document.getElementById("lblwho").value;

		var skillid = document.getElementById("lblskillid").value;
		var skillqty = document.getElementById("lblskillqty").value;
		var rdid = document.getElementById("lblrdid").value;
		var meterid = document.getElementById("lblmeterid").value;
		var freq = document.getElementById("lblfreq").value;

		var eReturn = window.showModalDialog("../complib/complibtasks2dialog.aspx?typ=tasks2&coid=" + coid + "&comp=" + comp + "&fuid=" + fid + " &tasknum=" + tskid + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:780px;resizable=yes");
		if(eReturn) {
		//reload report
		    window.location = "ttno.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + "&who=" + who + "&skillid=" + skillid + "&skillqty=" + skillqty + "&rdid=" + rdid + "&meterid=" + meterid + "&freq=" + freq + "&date=" + Date();
		}
		else {
		//reload report
		    window.location = "ttno.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&who=" + who + "&skillid=" + skillid + "&skillqty=" + skillqty + "&rdid=" + rdid + "&meterid=" + meterid + "&freq=" + freq + "&date=" + Date();
		}
		}
		function handleexit(task) {
		window.returnValue = task;
		window.close();
		}
		</script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table width="816">
				<tr>
					<td id="tdpm" class="bigbold" width="816" colSpan="4" align="center" runat="server"></td>
				</tr>
				<tr>
					<td width="93">&nbsp;</td>
					<td class="bigplain" id="tdass" runat="server">Asset:</td>
					<td id="tdeq" class="bigplain" runat="server"></td>
					<td width="93" align="right"></td>
				</tr>
				<tr class="details">
					<td colSpan="4" align="center" class="plainlabelblue" height="30" valign="middle">Note 
						that the Task Edit Screen is Component Based, so only tasks for the Component 
						indicated next to the Task Number below will be selected.
					</td>
				</tr>
				<tr>
					<td></td>
					<td colSpan="2">
						<hr SIZE="1">
					</td>
					<TD></TD>
				</tr>
			</table>
			<TABLE width="816">
				<TR>
					<td width="93">&nbsp;</td>
					<TD class="label" bgColor="silver" width="624" id="tdlbl" runat="server"></TD>
					<td width="93">&nbsp;</td>
				</TR>
				<tr>
					<td></td>
					<td id="tdwi" align="center" runat="server"></td>
					<td></td>
				</tr>
			</TABLE>
			<input id="lbltasklev" type="hidden" name="lbltasklev" runat="server"> <input id="lblsid" type="hidden" name="lblsid" runat="server">
			<input id="lbldid" type="hidden" name="lbldid" runat="server"> <input id="lblclid" type="hidden" name="lblclid" runat="server">
			<input id="lblchk" type="hidden" name="lblchk" runat="server"> <input id="lbltasknum" type="hidden" name="lbltasknum" runat="server">
			<input id="lblflag" type="hidden" name="lblflag" runat="server"><input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lblfuid" type="hidden" runat="server" NAME="lblfuid"> <input type="hidden" id="lblcid" runat="server" NAME="lblcid">
            <input type="hidden" id="lblfslang" runat="server" />
            <input type="hidden" id="lblwho" runat="server" />
            
            <input type="hidden" id="lblskillid" runat="server" />
            <input type="hidden" id="lblskillqty" runat="server" />
            <input type="hidden" id="lblrdid" runat="server" />
            <input type="hidden" id="lblmeterid" runat="server" />
            <input type="hidden" id="lblfreq" runat="server" />
		</form>
	</body>
</HTML>
