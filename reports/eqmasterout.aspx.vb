﻿Imports System.Data.SqlClient
Public Class eqmasterout
    Inherits System.Web.UI.Page
    Dim eqstr As String
    Dim dr As SqlDataReader
    Dim sql As String
    Dim win As New Utilities

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eqstr = Request.QueryString("eqstr").ToString
        win.Open()
        ExportDG(eqstr)
        win.Dispose()
    End Sub
    Private Sub ExportDG(ByVal eqstr As String)
        BindExport(eqstr)
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
    Private Sub BindExport(ByVal eqstr As String)
        Dim osql As String
        osql = "usp_GetAssignmentReport_excel_mch1 '" & eqstr & "'"
        Dim ds As New DataSet
        ds = win.GetDSData(osql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
    End Sub
End Class