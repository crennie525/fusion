﻿Imports System.Data.SqlClient
Public Class pmrep2
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim eqid, mode As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString '"1481" '
            'mode = Request.QueryString("mode").ToString
            pms.Open()
            PopWI(eqid)
            pms.Dispose()

        End If
    End Sub
    
    Private Sub PopWI(ByVal eqid As String)
        Dim sb As New System.Text.StringBuilder
        'sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
        Dim funcchk As String = ""
        Dim func As String = ""
        Dim skill As String = ""
        Dim qty As String = ""
        Dim freq As String = ""
        Dim rd As String = ""
        Dim pm As String = ""
        Dim pmchk As String = ""
        Dim subt As String = ""
        Dim start As Integer = 0
        sql = "usp_getpmrep2 '" & eqid & "'"
        dr = pms.GetRdrData(sql)
        While dr.Read
            skill = dr.Item("skill").ToString
            qty = dr.Item("qty").ToString
            freq = dr.Item("freq").ToString
            rd = dr.Item("rd").ToString
            pm = skill & "(" & qty & ")/" & freq & "&nbsp;DAYS/" & rd

            func = dr.Item("func").ToString

            If pm <> pmchk Then
                pmchk = pm
                If start <> 0 Then

                    sb.Append("</td></tr></table>")
                    sb.Append("</table>")
                Else
                    start = 1
                End If
                sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""680"" style=""page-break-after:always;"">")
                sb.Append("<tr><td width=""160""></td><td width=""50""></td><td width=""520""></td></tr>")
                sb.Append("<tr><td class=""plainlabel box"" width=""160"" height=""20"" box><b>Equipment Number:</b></td>")
                sb.Append("<td class=""plainlabel box"" colspan=""2"" box>" & dr.Item("eqnum").ToString & "</td></tr>")
                sb.Append("<tr><td class=""plainlabel box"" height=""20"" box><b>Equipment Description:</b></td>")
                sb.Append("<td class=""plainlabel box"" colspan=""2"" box>" & dr.Item("eqdesc").ToString & "</td></tr>")
                sb.Append("<tr><td class=""plainlabel box"" height=""20"" box><b>PM Description:</b></td>")
                sb.Append("<td class=""plainlabel box"" colspan=""2"" box>" & pm & "</td></tr>")
                sb.Append("<tr><td class=""plainlabel box"" height=""20"" box><b>Date Completed:</b></td>")
                sb.Append("<td class=""plainlabel box"" colspan=""2"" box>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td></tr>")
                sb.Append("<tr><td class=""plainlabel box"" height=""20"" box><b>Hours (Planned):</b></td>")
                sb.Append("<td class=""plainlabel box"">" & dr.Item("est").ToString & "</td>")
                sb.Append("<td class=""plainlabel box"">Green <b>(G):</b> Item is in acceptable condition.  No repair necessary.</td></tr>")
                sb.Append("<tr><td class=""plainlabel box"" height=""20""><b>Hours (Actual):</b></td>")
                sb.Append("<td class=""plainlabel box"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel box"">Yellow <b>(Y):</b> Item is reaching the end of its life and should be replaced within one month, pending parts availability.</td></tr>")
                sb.Append("<tr><td class=""plainlabel box"" height=""20""><b>No. Men Needed:</b></td>")
                sb.Append("<td class=""plainlabel box"">" & dr.Item("qty").ToString & "</td>")
                sb.Append("<td class=""plainlabel box"">Red <b>(R):</b> Item is on the brink of failure.  Item to be replaced/fixed within 1 week, pending parts availability.</td></tr>")
                sb.Append("<tr><td colspan=""3""><table cellSpacing=""0"" cellPadding=""2"">")
                sb.Append("<tr><td class=""plainlabel box"" width=""30""><b>Task</b></td>")
                sb.Append("<td class=""plainlabel box"" width=""120""><b>Function</b></td>")
                sb.Append("<td class=""plainlabel box"" width=""120""><b>Component</b></td>")
                sb.Append("<td class=""plainlabel box"" width=""150""><b>Task Description</b></td>")
                sb.Append("<td class=""plainlabel box"" width=""80""><b>Location</b></td>")
                sb.Append("<td class=""plainlabel box"" width=""50""><b>Est Hrs</b></td>")
                sb.Append("<td class=""plainlabel box"" width=""50""><b>Act Hrs</b></td>")
                sb.Append("<td class=""plainlabel box"" width=""20""><b>R</b></td>")
                sb.Append("<td class=""plainlabel box"" width=""20""><b>Y</b></td>")
                sb.Append("<td class=""plainlabel box"" width=""20""><b>G</b></td></tr>")

            End If
            subt = dr.Item("subtask").ToString
            If subt = "0" Then
                sb.Append("<tr><td class=""plainlabel box"" width=""30"">" & dr.Item("tasknum").ToString & "</td>")
            Else
                sb.Append("<tr><td class=""plainlabel box"" width=""30"">" & dr.Item("tasknum").ToString & "." & subt & "</td>")
            End If
            sb.Append("<td class=""plainlabel box"" width=""120"">" & dr.Item("func").ToString & "</td>")
            sb.Append("<td class=""plainlabel box"" width=""120"">" & dr.Item("compnum").ToString & "</td>")
            sb.Append("<td class=""plainlabel box"" width=""150"">" & dr.Item("taskdesc").ToString & "<br>" & dr.Item("fm1").ToString & "</td>")
            sb.Append("<td class=""plainlabel box"" width=""80"">&nbsp;</td>")
            sb.Append("<td class=""plainlabel box"" width=""50"">" & dr.Item("ttime").ToString & "</td>")
            sb.Append("<td class=""plainlabel box"" width=""50"">&nbsp;</td>")
            sb.Append("<td class=""plainlabel box"" width=""20"">&nbsp;</td>")
            sb.Append("<td class=""plainlabel box"" width=""20"">&nbsp;</td>")
            sb.Append("<td class=""plainlabel box"" width=""20"">&nbsp;</td></tr>")


        End While
        dr.Close()

        'sb.Append("</Table>")
        If sb.ToString = "" Then
            sb.Append("No Records Found")
        End If
        rep.InnerHtml = sb.ToString
    End Sub

   
End Class