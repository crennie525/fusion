<%@ Page Language="vb" AutoEventWireup="false" Codebehind="NotAddressed.aspx.vb" Inherits="lucy_r12.NotAddressed" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>NotAddressed</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<asp:datalist id="dlhd" runat="server">
				<HeaderTemplate>
					<table>
				</HeaderTemplate>
				<ItemTemplate>
					<TR>
						<TD></TD>
					</TR>
				</ItemTemplate>
				<FooterTemplate>
					</table>
				</FooterTemplate>
			</asp:datalist>
			<asp:datagrid id="dgexport" runat="server" AutoGenerateColumns="False">
				<AlternatingItemStyle Font-Size="X-Small" Font-Names="Arial"></AlternatingItemStyle>
				<ItemStyle Font-Size="X-Small" Font-Names="Arial"></ItemStyle>
				<HeaderStyle Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="func" HeaderText="Function"></asp:BoundColumn>
					<asp:BoundColumn DataField="compnum" HeaderText="Component"></asp:BoundColumn>
					<asp:BoundColumn DataField="compdesc" HeaderText="Description"></asp:BoundColumn>
					<asp:BoundColumn DataField="failuremode" HeaderText="Failure Modes"></asp:BoundColumn>
				</Columns>
			</asp:datagrid>
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
