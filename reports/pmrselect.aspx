﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmrselect.aspx.vb" Inherits="lucy_r12.pmrselect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function getrep(skillid, qty, freq, rid, pm) {
            //alert(skillid)
            window.parent.handleexit(skillid, qty, freq, rid, pm);
            //window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table>
    <tr>
         <asp:DataGrid ID="dgtots" runat="server" CellSpacing="1" AutoGenerateColumns="False"
                        AllowCustomPaging="True" AllowPaging="True" GridLines="None" CellPadding="0"
                        ShowFooter="False" PageSize="50">
                        <FooterStyle BackColor="White"></FooterStyle>
                        <EditItemStyle Height="15px"></EditItemStyle>
                        <AlternatingItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="#E7F1FD">
                        </AlternatingItemStyle>
                        <ItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="ControlLightLight">
                        </ItemStyle>
                        <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                        <Columns>
                            
                            <asp:TemplateColumn HeaderText="PM" Visible="true">
                                <HeaderStyle Width="300px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbpm" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pm") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <%# DataBinder.Eval(Container, "DataItem.pm")%>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                           
                            <asp:TemplateColumn HeaderText="Total Time">
                                <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txttottimee" runat="server" Width="50px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Down Time">
                                <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.rdt") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtdowntimee" runat="server" Width="50px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.rdt") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn> 
                            
                            <asp:TemplateColumn HeaderText="skillid" Visible="False">
                                <HeaderStyle Width="20px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblskillidi" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.skillid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblskillide" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.skillid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="skillqty" Visible="False">
                                <HeaderStyle Width="20px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblsqtyi" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblsqty" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="freqid" Visible="False">
                                <HeaderStyle Width="20px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblfreqidi" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblfreqide" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="rdid" Visible="False">
                                <HeaderStyle Width="20px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblrdidi" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.rdid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblrdide" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.rdid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                           
                            
                            
                        </Columns>
                        <PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
                            ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
                    </asp:DataGrid>
    <td>
    
    </td>
    </tr>
    </table>
    <input type="hidden" id="lbleqstr" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    </form>
</body>
</html>
