﻿Imports System.Data.SqlClient
Public Class AssignmentReportCompLib
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim tasks As New Utilities
    Dim dr As SqlDataReader
    Dim eqid, fuid, comid, typ, usetot, siteid As String
    Dim tmod As New transmod
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            siteid = Request.QueryString("siteid").ToString
            lblsiteid.Value = siteid
            tasks.Open()
            GetReport(siteid)
            tasks.Dispose()
        End If
    End Sub
    Private Sub GetReport(ByVal siteid As String)
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""3"" width=""960"" border=""0"">")
        sb.Append("<tr><td class=""label16"" colspan=""9"" align=""center"">Site Component Library Assignment Report</td></tr>")
        sb.Append("<tr><td colspan=""9"" align=""center"" class=""plainlabel"">" & Now & "</td></tr>")
        'sb.Append("<tr><td colspan=""9"" align=""center"" class=""plainlabelsmr"">Tasks in RED indicate that this task has either been transferred to or created in the Operator Care Module</td></tr>")
        sb.Append("<tr><td colspan=""9"" align=""center"" class=""plainlabel"">&nbsp;</td></tr>")
        sql = "select sitename from sites where siteid = '" & siteid & "'"
        Dim sitename As String
        sitename = tasks.strScalar(sql)
        sb.Append("<tr><td class=""label14"" colspan=""9"" align=""center"">Plant Site:&nbsp;&nbsp;" & sitename & "</td>")
        sql = "usp_GetAssignmentReport_compliball '" & siteid & "'"
        dr = tasks.GetRdrData(sql)
        Dim start As Integer = 0
        Dim cochk As String = ""
        Dim comp As String
        Dim tchk As String = ""
        Dim task As String
        Dim subtask As String
        Dim subt As String
        Dim oti, ti, rat, ts, freq, loto, cs, rd, lube, parts, tools, loc, locd As String
        While dr.Read
            If start = 0 Then


            End If
            comp = dr.Item("compnum").ToString
            If cochk <> comp Then
                cochk = comp
                tchk = ""
                sb.Append("<tr><td colspan=""9"">&nbsp;</td></tr>")
                sb.Append("<tr><td colspan=""9"" align=""left"" class=""labelsmu"" >Component:&nbsp;&nbsp;" & dr.Item("compnum").ToString & "</td></tr>")
                sb.Append("<tr><td width=""70"" class=""labelsmtl"" align=""left"">" & tmod.getlbl("cdlbl670", "AssignmentReport.aspx.vb") & "</td>")
                sb.Append("<td width=""40"" class=""labelsmtl"">" & tmod.getlbl("cdlbl671", "AssignmentReport.aspx.vb") & "</td>")
                'sb.Append("<td width=""150"" class=""labelsmtl"" align=""left"">" & tmod.getlbl("cdlbl672", "AssignmentReport.aspx.vb") & "03</td>")
                sb.Append("<td width=""350"" class=""labelsmtl"" align=""left"">" & tmod.getlbl("cdlbl675", "AssignmentReport.aspx.vb") & "</td>")
                sb.Append("<td width=""180"" class=""labelsmtl"" align=""left"">Task Type</td><td width=""30"" class=""labelsmtl"" align=""left"">R/D</td><td width=""80"" class=""labelsmtl"" align=""left"">" & tmod.getlbl("cdlbl678", "AssignmentReport.aspx.vb") & "</td>")
                sb.Append("<td width=""30"" class=""labelsmtl"" align=""left"">MINS</td></tr>")


            End If

            subtask = dr.Item("subtask").ToString
            task = dr.Item("tasknum").ToString
            'coid = dr.Item("comid").ToString
            'istpm = dr.Item("istpm").ToString
            freq = dr.Item("freq").ToString
            subt = dr.Item("subt").ToString
            If freq = "0" Or freq = "" Then
                freq = "None"
            Else
                freq = dr.Item("freq").ToString
            End If
            loto = dr.Item("lotoid").ToString
            If loto = "0" Then
                loto = "N"
            Else
                loto = "Y"
            End If
            cs = dr.Item("conid").ToString
            If cs = "0" Then
                cs = "N"
            Else
                cs = "Y"
            End If
            rd = dr.Item("rdid").ToString
            If rd = "1" Then
                rd = "R"
            ElseIf rd = "2" Then
                rd = "D"
            ElseIf rd = "3" Then
                rd = "A"
            Else
                rd = "M"
            End If
            parts = dr.Item("parts").ToString
            If Len(parts) = 0 OrElse parts = "None" Then
                parts = Nothing
            Else
                parts = tmod.getxlbl("xlb261", "AssignmentReport.aspx.vb") & dr.Item("parts").ToString
            End If
            tools = dr.Item("tools").ToString
            If Len(tools) = 0 OrElse tools = "None" Then
                tools = Nothing
            Else
                tools = tmod.getxlbl("xlb262", "AssignmentReport.aspx.vb") & dr.Item("tools").ToString
            End If
            lube = dr.Item("lubes").ToString
            If Len(lube) = 0 OrElse lube = "None" Then
                lube = Nothing
            Else
                lube = tmod.getxlbl("xlb263", "AssignmentReport.aspx.vb") & dr.Item("lubes").ToString
            End If

            sb.Append("<tr><td class=""plainlabelsm"" align=""left"">" & freq & "</td>")
            sb.Append("<td class=""plainlabelsm"">" & task & "</A></td>")
            'sb.Append("<td class=""plainlabelsm"" align=""left"">" & dr.Item("compnum").ToString & "</td>")
            'sb.Append("<td class=""plainlabelsm"">" & dr.Item("cqty").ToString & "</td>")
            'sb.Append("<td class=""plainlabelsm"">" & dr.Item("desig").ToString & "</td>")
            sb.Append("<td class=""plainlabelsm"" align=""left"">" & dr.Item("task").ToString & "</td>")
            sb.Append("<td class=""plainlabelsm"" align=""left"">" & dr.Item("tasktype").ToString & "</td>")
            sb.Append("<td class=""plainlabelsm"" align=""left"">" & rd & "</td>")
            sb.Append("<td class=""plainlabelsm"" align=""left"">" & dr.Item("skill").ToString & "</td>")
            sb.Append("<td class=""plainlabelsm"" align=""left"">" & dr.Item("ttime").ToString & "</td>")
            'sb.Append("<td class=""plainlabelsm"">" & loto & "</td>")
            'sb.Append("<td class=""plainlabelsm"">" & cs & "</td>
            sb.Append("</tr>")

        End While

        dr.Close()
        sb.Append("</Table>")
        tdwi.InnerHtml = sb.ToString
    End Sub
End Class