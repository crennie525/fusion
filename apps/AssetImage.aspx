<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetImage.aspx.vb" Inherits="lucy_r12.AssetImage" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AssetImage</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/AssetImageaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table style="left: 5px; position: absolute; top: 0px" cellpadding="0">
        <tr>
            <td colspan="3" align="center">
                <a onclick="getbig();" href="#">
                    <img id="imgeq" height="216" src="../images/appimages/eqimg1.gif" width="216" border="0"
                        runat="server"></a>
            </td>
        </tr>
        <tr>
            <td class="bluelabel" id="tdcnt" align="center" runat="server" width="150">
            </td>
            <td align="center">
                <a onclick="getprev();" href="#">
                    <img id="pr" alt="" src="../images/appbuttons/minibuttons/prevarrowbg.gif" border="0"
                        runat="server" width="20" height="20"></a> <a onclick="getnext();" href="#">
                            <img id="ne" alt="" src="../images/appbuttons/minibuttons/nextarrowbg.gif" border="0"
                                runat="server" width="20" height="20"></a>
            </td>
            <td align="center">
                <a onclick="getport();" href="#">
                    <img id="po" alt="" src="../images/appbuttons/bgbuttons/picgrid.gif" border="0" runat="server"
                        width="20" height="20"></a>
            </td>
        </tr>
    </table>
    <input id="lblpic" type="hidden" runat="server"><input type="hidden" id="lbleqid"
        runat="server">
    <input type="hidden" id="lblcur" runat="server"><input type="hidden" id="lblcnt"
        runat="server">
    <input type="hidden" id="lblpareqid" runat="server"><input type="hidden" id="lblurl"
        runat="server">
    <input type="hidden" id="lblfuid" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
