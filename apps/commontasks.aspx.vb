

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class commontasks
    Inherits System.Web.UI.Page
	Protected WithEvents ovid8 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid7 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid6 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid5 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid4 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid3 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid2 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang212 As System.Web.UI.WebControls.Label

	Protected WithEvents lang211 As System.Web.UI.WebControls.Label

	Protected WithEvents lang210 As System.Web.UI.WebControls.Label

	Protected WithEvents lang209 As System.Web.UI.WebControls.Label

	Protected WithEvents lang208 As System.Web.UI.WebControls.Label

	Protected WithEvents lang207 As System.Web.UI.WebControls.Label

	Protected WithEvents lang206 As System.Web.UI.WebControls.Label

	Protected WithEvents lang205 As System.Web.UI.WebControls.Label

	Protected WithEvents lang204 As System.Web.UI.WebControls.Label

	Protected WithEvents lang203 As System.Web.UI.WebControls.Label

	Protected WithEvents lang202 As System.Web.UI.WebControls.Label

	Protected WithEvents lang201 As System.Web.UI.WebControls.Label

	Protected WithEvents lang200 As System.Web.UI.WebControls.Label

	Protected WithEvents lang199 As System.Web.UI.WebControls.Label

	Protected WithEvents lang198 As System.Web.UI.WebControls.Label

	Protected WithEvents lang197 As System.Web.UI.WebControls.Label

	Protected WithEvents lang196 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim ctasks As New Utilities
    Dim sql As String
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
    Dim cid, taskid, tasktype, pdm, filter, sid, ro As String
    Protected WithEvents lbltasktype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpdm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbc As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbt As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents txtmemo As System.Web.UI.WebControls.TextBox
    Protected WithEvents imgsavecommon As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblmode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtkey As System.Web.UI.WebControls.TextBox

    Protected WithEvents TextBox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcompnum As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcompdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcompdesig As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcompspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfunc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfuncspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbfailmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents tdpm As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents iflst As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblreturn As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblorigtasktype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblorigpdm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblctaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblscreen As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblexit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmemo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbtt As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents rbpt As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskholdid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibaddnew As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ir1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ir2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ir3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdtasks As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddtype As System.Web.UI.WebControls.ListBox
    Protected WithEvents ddpt As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            'ro = Request.QueryString("ro").ToString
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                ibaddnew.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                ibaddnew.Attributes.Add("onclick", "")
                imgsavecommon.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                imgsavecommon.Attributes.Add("onclick", "")

                ir1.Attributes.Add("src", "../images/appbuttons/minibuttons/return2dis.gif")
                ir1.Attributes.Add("onclick", "")
                ir2.Attributes.Add("src", "../images/appbuttons/minibuttons/return2dis.gif")
                ir2.Attributes.Add("onclick", "")
                ir3.Attributes.Add("src", "../images/appbuttons/minibuttons/return2dis.gif")
                ir3.Attributes.Add("onclick", "")
            End If
            sid = Request.QueryString("sid").ToString '"1C - Clean" '
            lblsid.Value = sid
            tasktype = Request.QueryString("typ").ToString '"1C - Clean" '
            lbltasktype.Value = tasktype
            lblorigtasktype.Value = tasktype
            pdm = Request.QueryString("pdm").ToString
            lblpdm.Value = pdm
            lblorigpdm.Value = pdm
            taskid = Request.QueryString("tid").ToString
            lbltaskid.Value = taskid
            ctasks.Open()
            lblcid.Value = "0"
            GetLists()
            If tasktype <> "" Then
                Try
                    ddtype.SelectedValue = lbltasktype.Value
                    Dim cond As String
                    sql = "select tasktype from pmtasktypes where ttid = '" & tasktype & "'"
                    cond = ctasks.strScalar(sql)
                    cond = Mid(cond, 1, 1)
                    If cond = "4" Then
                        Try
                            ddpt.SelectedValue = pdm
                        Catch ex As Exception

                        End Try
                    Else
                        ddpt.Enabled = False
                    End If
                Catch ex As Exception
                    'ddtype.Items.Insert(0, New ListItem(tasktype))
                    'ddtype.SelectedValue = tasktype
                End Try
            End If
            GetTasks()
            PopFail()
            ctasks.Dispose()
        Else
        If Request.Form("lblsubmit") = "refresh" Then
            lblsubmit.Value = ""
            Refreshit()
        ElseIf Request.Form("lblsubmit") = "addtask" Then
            lblsubmit.Value = ""
            lblscreen.Value = ""
            ctasks.Open()
            AddTask()
            ctasks.Dispose()
        ElseIf Request.Form("lblsubmit") = "savetask" Then
            lblsubmit.Value = ""
            lblscreen.Value = ""
            ctasks.Open()
            SaveTask()
            ctasks.Dispose()
        ElseIf Request.Form("lblsubmit") = "returntask" Then
            lblsubmit.Value = ""
            lblscreen.Value = ""
            ctasks.Open()
            ReturnTask()
            ctasks.Dispose()
        End If
        End If
    End Sub
    Private Sub ReturnTask()
        Dim typ As String = lbltyp.Value
        lbltyp.Value = ""
        If typ = "add" Then
            AddTask("yes")
        ElseIf typ = "save" Then
            SaveTask("yes")
        End If
        Dim tid As String = lbltaskid.Value
        tasktype = lbltasktype.Value
        pdm = lblpdm.Value
        
        Dim desc As String
        If typ <> "reg" Then
            desc = txtmemo.Text
        Else
            desc = lblmemo.Value
        End If
        desc = ctasks.ModString2(desc)
        Dim tasktypestr, pdmstr As String
        tasktypestr = ddtype.SelectedItem.ToString
        tasktypestr = ctasks.ModString2(tasktypestr)
        pdmstr = ddpt.SelectedItem.ToString
        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_updateFromCommon1 @tid, @desc"
        Dim param0 = New SqlParameter("@tid", SqlDbType.Int)
        param0.Value = tid
        cmd.Parameters.Add(param0)
        Dim param01 = New SqlParameter("@desc", SqlDbType.VarChar)
        param01.Value = desc
        cmd.Parameters.Add(param01)
        ctasks.UpdateHack(cmd)
        'sql = "update pmTasks set taskdesc = '" & desc & "' where pmtskid = '" & tid & "'"
        'Dim thid As Integer
        'sql = "insert into taskhold (taskdesc) values ('" & desc & "') select @@identity"
        'thid = ctasks.Scalar(sql)
        'lbltaskholdid.Value = thid
        If rbtt.Checked = True Then
            sql = "update pmTasks set tasktype = '" & tasktypestr & "', " _
            + "ttid = '" & tasktype & "' where pmtskid = '" & tid & "'"
            'sql = "update taskhold set tasktype = '" & tasktypestr & "', " _
            '+ "ttid = '" & tasktype & "' where taskholdid = '" & thid & "'"
            ctasks.Update(sql)
        End If
        If rbpt.Checked = True Then
            If ddpt.SelectedIndex <> 0 Then
                sql = "update pmTasks set pretech = '" & pdmstr & "', ptid = '" & pdm & "' " _
                + "where pmtskid = '" & tid & "'"
                'sql = "update taskhold set pretech = '" & pdmstr & "', ptid = '" & pdm & "' " _
                '+ "where taskholdid = '" & thid & "'"
                ctasks.Update(sql)
            End If
        End If
        sid = lblsid.Value
        sql = "usp_updateTaskDDIndexes '" & taskid & "', '" & sid & "'"
        ctasks.Update(sql)

        lblexit.Value = "yes"
    End Sub
    Private Sub SaveTask(Optional ByVal ret As String = "no")
        Dim tid As String = lblctaskid.Value
        lblctaskid.Value = ""
        Dim desc As String
        Dim cnt As Integer
        tasktype = lbltasktype.Value
        pdm = lblpdm.Value
        desc = txtmemo.Text
        desc = ctasks.ModString2(desc)
        sql = "select count(*) from commontasks where description = '" & desc & "' and ttid = '" & tasktype & "' and ptid = '" & pdm & "'"
        cnt = ctasks.Scalar(sql)
        If cnt = 0 Then
            sql = "update commontasks set description = '" & desc & "' where ctaskid = '" & tid & "'"
            ctasks.Update(sql)
            If ret = "no" Then
                Dim strMessage As String =  tmod.getmsg("cdstr199" , "commontasks.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If        
        Else
            If ret = "no" Then
                Dim strMessage As String =  tmod.getmsg("cdstr200" , "commontasks.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If
    End Sub
    Private Sub AddTask(Optional ByVal ret As String = "no")
        Dim desc As String
        Dim cnt As Integer
        tasktype = lbltasktype.Value
        pdm = lblpdm.Value
        
        desc = txtmemo.Text
        desc = ctasks.ModString2(desc)
        sql = "select count(*) from commontasks where description = '" & desc & "' and ttid = '" & tasktype & "' and ptid = '" & pdm & "'"
        cnt = ctasks.Scalar(sql)
        If cnt = 0 Then
            Dim tasktypestr, pdmstr As String
            tasktypestr = ddtype.SelectedItem.ToString
            pdmstr = ddpt.SelectedItem.ToString
            sql = "insert into commontasks (description, ttid, tasktype, ptid, pretech) values " _
            + "('" & desc & "','" & tasktype & "','" & tasktypestr & "','" & pdm & "','" & pdmstr & "')"
            ctasks.Update(sql)
            If ret = "no" Then
                Dim strMessage As String =  tmod.getmsg("cdstr201" , "commontasks.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

        Else
            If ret = "no" Then
                Dim strMessage As String =  tmod.getmsg("cdstr202" , "commontasks.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

        End If


    End Sub
    Private Sub Refreshit()
        txtcompnum.Text = ""
        txtcompdesc.Text = ""
        txtcompdesig.Text = ""
        txtcompspl.Text = ""
        txtfunc.Text = ""
        txtfuncspl.Text = ""
        txtkey.Text = ""
        rbt.Checked = False
        rbc.Checked = True
        tdpm.Attributes.Add("class", "details")
        tasktype = lblorigtasktype.Value
        lbltasktype.Value = lblorigtasktype.Value
        pdm = lblorigpdm.Value
        lblpdm.Value = lblorigpdm.Value
        ctasks.Open()
        If tasktype <> "" Then
            Try
                ddtype.SelectedValue = tasktype
                If Mid(tasktype, 1, 1) = "4" Then
                    Try
                        ddpt.SelectedValue = pdm
                    Catch ex As Exception

                    End Try
                Else
                    ddpt.Enabled = False
                End If
            Catch ex As Exception

            End Try
        End If
        GetTasks()
        PopFail()
        ctasks.Dispose()
    End Sub
    Private Sub PopFail()
        cid = lblcid.Value
        Dim dt, val, filt As String
        dt = "FailureModes"
        val = "failid, failuremode"
        filt = " where compid = '" & cid & "'"
        dr = ctasks.GetList(dt, val, filt)
        lbfailmaster.DataSource = dr
        lbfailmaster.DataTextField = "failuremode"
        lbfailmaster.DataValueField = "failid"
        Try
            lbfailmaster.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
    End Sub
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        'lbltasktype.Value = ddtype.SelectedValue
        ctasks.Open()
        GetTasks()
        ctasks.Dispose()
    End Sub
    Private Sub GetTasks()
        Dim locstr, desc, typ, locid, pd, tid As String
        Dim noflag As Integer = 0
        Dim img As String = "../images/appbuttons/minibuttons/magnifier.gif"
        Dim img1 As String = "../images/appbuttons/minibuttons/2PX.gif"
        Dim msg As String = "onmouseover = ""return overlib('" & tmod.getov("cov1" , "commontasks.aspx.vb") & "', ABOVE, LEFT)"" onmouseout = ""return nd()"""
        Dim msg1 As String = "onmouseover = ""return overlib('" & tmod.getov("cov2" , "commontasks.aspx.vb") & "', ABOVE, LEFT)"" onmouseout = ""return nd()"""
        Dim simg As String = "<img src=""" & img & """ onclick=""lookup();""" & msg & ">"
        Dim spimg As String = "<img src=""" & img & """ onclick=""jumpto('" & desc & "');""" & msg1 & ">" 'locstr

        Dim sb As New StringBuilder
        sb.Append("<table width=""660"" cellpadding=""0"">")
        sb.Append("<tr><td>")

        sb.Append("<tr><td colspan=""4"" valign=""top""><div style=""OVERFLOW: auto; WIDTH: 660px;  HEIGHT: 160px"">")
        sb.Append("<table width=""640"" cellpadding=""1"">")
        sb.Append("<tr><td class=""btmmenu plainlabel"" width=""300"">" & tmod.getlbl("cdlbl16" , "commontasks.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""20""><img src=" & img & "></td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""160"">" & tmod.getlbl("cdlbl17" , "commontasks.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""160"">PdM</td></tr>")

        tasktype = lbltasktype.Value
        pdm = lblpdm.Value

        If Mid(tasktype, 1, 1) <> "4" Then
            filter = "t.ttid = '" & tasktype & "' "
        Else
            filter = "t.ttid = '" & tasktype & "' and t.pretech = '" & pdm & "' "
        End If
        Dim keyword As String = txtkey.Text
        If keyword <> "" Then
            If rbc.Checked = True Then
                filter += "and t.description like '%" & keyword & "%' "
            Else
                filter += "and t.taskdesc like '%" & keyword & "%' "
            End If

        End If
        Dim compnum, compdesc, compdesig, compspl, func, funcspl, fail, compstr, comstr, tskstr, ftskstr As String
        If rbc.Checked = True Then
            lblmode.Value = "common"
            imgsavecommon.Visible = True
            tdpm.Attributes.Add("class", "details")
            sql = "select * from commontasks t where " & filter
        Else
            tdpm.Attributes.Add("class", "view")
            lblmode.Value = "lookup"
            imgsavecommon.Visible = False
            compnum = txtcompnum.Text
            compdesc = txtcompdesc.Text
            compdesig = txtcompdesig.Text
            compspl = txtcompspl.Text
            func = txtfunc.Text
            funcspl = txtfuncspl.Text
            If compnum <> "" Then
                filter += "and c.compnum like '%" & compnum & "%' "
            End If
            If compdesc <> "" Then
                filter += "and c.compdesc like '%" & compdesc & "%' "
            End If
            If compdesig <> "" Then
                filter += "and c.desig like '%" & compdesig & "%' "
            End If
            If compspl <> "" Then
                filter += "and c.spl like '%" & compspl & "%' "
            End If
            If func <> "" Then
                filter += "and f.func like '%" & func & "%' "
            End If
            If funcspl <> "" Then
                filter += "and f.spl like '%" & funcspl & "%' "
            End If
            Dim fi As String
            Dim Item As ListItem
            For Each Item In lbfailmaster.Items
                If Item.Selected Then
                    fi = Item.Value.ToString
                    If fail = "" Then
                        fail = "'" & fi & "'"
                    Else
                        fail += ",'" & fi & "'"
                    End If
                End If
            Next
            If fail <> "" Then
                If compnum <> "" Then
                    sql = "select distinct t.comid from pmtasks t left join components c on c.compnum = t.compnum " _
                    + "left join functions f on f.func_id = t.funcid where " & filter
                    dr = ctasks.GetRdrData(sql)
                    While dr.Read
                        If compstr = "" Then
                            compstr = "'" & dr.Item("comid").ToString & "'"
                        Else
                            compstr += ",'" & dr.Item("comid").ToString & "'"
                        End If
                    End While
                    dr.Close()
                    If compstr <> "" Then
                        sql = "select distinct comid from componentfailmodes where failid in (" & fail & ") and " _
                        + "comid in (" & compstr & ")"
                        dr = ctasks.GetRdrData(sql)
                        While dr.Read
                            If comstr = "" Then
                                comstr = "'" & dr.Item("comid").ToString & "'"
                            Else
                                comstr += ",'" & dr.Item("comid").ToString & "'"
                            End If
                        End While
                        dr.Close()
                        If comstr <> "" Then
                            filter += "and t.comid in (" & comstr & ") "
                            sql = "select distinct t.taskdesc, t.pmtskid, t.tasktype, t.pretech from pmtasks t left join components c on c.compnum = t.compnum " _
                            + "left join functions f on f.func_id = t.funcid where " & filter
                        Else
                            'Msg?
                            noflag = 1
                        End If
                    Else
                        'Msg?
                        noflag = 1
                    End If
                Else
                    sql = "select distinct t.pmtskid from pmtasks t left join components c on c.compnum = t.compnum " _
                     + "left join functions f on f.func_id = t.funcid where " & filter
                    dr = ctasks.GetRdrData(sql)
                    While dr.Read
                        If tskstr = "" Then
                            tskstr = "'" & dr.Item("pmtskid").ToString & "'"
                        Else
                            tskstr += ",'" & dr.Item("pmtskid").ToString & "'"
                        End If
                    End While
                    dr.Close()
                    If tskstr <> "" Then
                        sql = "select distinct taskid from pmtaskfailmodes where parentfailid in (" & fail & ") and " _
                        + "taskid in (" & tskstr & ")"
                        dr = ctasks.GetRdrData(sql)
                        While dr.Read
                            If ftskstr = "" Then
                                ftskstr = "'" & dr.Item("taskid").ToString & "'"
                            Else
                                ftskstr += ",'" & dr.Item("taskid").ToString & "'"
                            End If
                        End While
                        dr.Close()
                        If ftskstr <> "" Then
                            filter += "and t.pmtskid in (" & ftskstr & ") "
                            sql = "select distinct t.taskdesc, t.pmtskid, t.tasktype, t.pretech from pmtasks t left join components c on c.compnum = t.compnum " _
                            + "left join functions f on f.func_id = t.funcid where " & filter
                        Else
                            'Msg?
                            noflag = 1
                        End If
                    Else
                        'Msg?
                        noflag = 1
                    End If
                End If




            Else
                sql = "select distinct t.taskdesc, t.pmtskid, t.tasktype, t.pretech from pmtasks t left join components c on c.compnum = t.compnum " _
                + "left join functions f on f.func_id = t.funcid where " & filter
            End If
        End If
        Dim deschold As String
        If noflag = 0 Then
            If rbc.Checked = True Then
                filter += "and t.description is not null and len(description) <> 0 "
            Else
                filter += "and t.taskdesc is not null and len(taskdesc) <> 0 "
            End If
            dr = ctasks.GetRdrData(sql)
            While dr.Read
                If rbc.Checked = True Then
                    desc = dr.Item("description").ToString
                    tid = dr.Item("ctaskid").ToString
                Else
                    desc = dr.Item("taskdesc").ToString
                    tid = dr.Item("pmtskid").ToString
                End If
                desc = ctasks.ModString2(desc)
                typ = dr.Item("tasktype").ToString
                pd = dr.Item("pretech").ToString
                If desc <> "" Then
                    If deschold <> desc Then
                        deschold = desc
                        ro = lblro.Value
                        If ro = "1" Then
                            sb.Append("<tr><td class=""plainlabel grayborder"" valign=""top""><a href=""#"" class=""A1"">" & desc & "</td>")
                        Else
                            sb.Append("<tr><td class=""plainlabel grayborder"" valign=""top""><a href=""#"" class=""A1"" onclick=""getthis('" & desc & "')"">" & desc & "</td>")
                        End If

                        sb.Append("<td class=""plainlabel grayborder"" valign=""top""><img src=""" & img & """ onclick=""jumpto('" & tid & "','" & desc & "');""" & msg1 & "></td>")
                        sb.Append("<td class=""plainlabel grayborder"" valign=""top"">" & typ & "</td>")
                        If pd <> "" Then
                            sb.Append("<td class=""plainlabel grayborder"" valign=""top"">" & pd & "</td></tr>")
                        Else
                            sb.Append("<td class=""plainlabel grayborder"" valign=""top"">&nbsp;</td></tr>")
                        End If
                    End If
                End If
            End While

            dr.Close()

            Dim i As Integer
            For i = 0 To 5
                sb.Append("<tr><td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")
            Next
        Else
            Dim i As Integer
            For i = 0 To 10
                sb.Append("<tr><td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")
            Next
        End If

        sb.Append("</table></div></td></tr>")
        sb.Append("</table>")
        tdtasks.InnerHtml = sb.ToString

    End Sub
    Private Sub GetLists()
        cid = lblcid.Value

      


        sql = "select ptid, pretech " _
        + "from pmPreTech where compid = '" & cid & "' or compid = '0' order by compid"
        dr = ctasks.GetRdrData(sql)
        ddpt.DataSource = dr
        ddpt.DataTextField = "pretech"
        ddpt.DataValueField = "ptid"
        Try
            ddpt.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddpt.Items.Insert(0, New ListItem("None"))
        ddpt.Items(0).Value = 0

        sql = "select ttid, tasktype " _
      + "from pmTaskTypes where compid = '" & cid & "' or compid = '0' and tasktype <> 'Select' order by compid"
        dr = ctasks.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataTextField = "tasktype"
        ddtype.DataValueField = "ttid"
        Try
            ddtype.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddtype.Items.Insert(0, New ListItem("Select"))
        ddtype.Items(0).Value = 0


    End Sub


    Private Sub ddtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddtype.SelectedIndexChanged
        If ddtype.SelectedIndex <> 0 Then
            Dim tasktypestr As String
            tasktype = ddtype.SelectedValue
            lbltasktype.Value = tasktype
            tasktypestr = ddtype.SelectedItem.ToString
            If tasktype <> "" Then
                Try
                    ddtype.SelectedValue = tasktype
                    If Mid(tasktypestr, 1, 1) = "4" Then
                        ddpt.Enabled = True
                    Else
                        ddpt.Enabled = False
                        ctasks.Open()
                        GetTasks()
                        ctasks.Dispose()
                    End If
                Catch ex As Exception

                End Try
            End If
        End If
    End Sub

    Private Sub ddpt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddpt.SelectedIndexChanged
        If ddpt.SelectedIndex <> 0 Then
            ctasks.Open()
            GetTasks()
            ctasks.Dispose()
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang196.Text = axlabs.GetASPXPage("commontasks.aspx", "lang196")
        Catch ex As Exception
        End Try
        Try
            lang197.Text = axlabs.GetASPXPage("commontasks.aspx", "lang197")
        Catch ex As Exception
        End Try
        Try
            lang198.Text = axlabs.GetASPXPage("commontasks.aspx", "lang198")
        Catch ex As Exception
        End Try
        Try
            lang199.Text = axlabs.GetASPXPage("commontasks.aspx", "lang199")
        Catch ex As Exception
        End Try
        Try
            lang200.Text = axlabs.GetASPXPage("commontasks.aspx", "lang200")
        Catch ex As Exception
        End Try
        Try
            lang201.Text = axlabs.GetASPXPage("commontasks.aspx", "lang201")
        Catch ex As Exception
        End Try
        Try
            lang202.Text = axlabs.GetASPXPage("commontasks.aspx", "lang202")
        Catch ex As Exception
        End Try
        Try
            lang203.Text = axlabs.GetASPXPage("commontasks.aspx", "lang203")
        Catch ex As Exception
        End Try
        Try
            lang204.Text = axlabs.GetASPXPage("commontasks.aspx", "lang204")
        Catch ex As Exception
        End Try
        Try
            lang205.Text = axlabs.GetASPXPage("commontasks.aspx", "lang205")
        Catch ex As Exception
        End Try
        Try
            lang206.Text = axlabs.GetASPXPage("commontasks.aspx", "lang206")
        Catch ex As Exception
        End Try
        Try
            lang207.Text = axlabs.GetASPXPage("commontasks.aspx", "lang207")
        Catch ex As Exception
        End Try
        Try
            lang208.Text = axlabs.GetASPXPage("commontasks.aspx", "lang208")
        Catch ex As Exception
        End Try
        Try
            lang209.Text = axlabs.GetASPXPage("commontasks.aspx", "lang209")
        Catch ex As Exception
        End Try
        Try
            lang210.Text = axlabs.GetASPXPage("commontasks.aspx", "lang210")
        Catch ex As Exception
        End Try
        Try
            lang211.Text = axlabs.GetASPXPage("commontasks.aspx", "lang211")
        Catch ex As Exception
        End Try
        Try
            lang212.Text = axlabs.GetASPXPage("commontasks.aspx", "lang212")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            ibaddnew.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontasks.aspx", "ibaddnew") & "', ABOVE, LEFT)")
            ibaddnew.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsavecommon.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontasks.aspx", "imgsavecommon") & "', ABOVE, LEFT)")
            imgsavecommon.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ir1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontasks.aspx", "ir1") & "', ABOVE, LEFT)")
            ir1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ir2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontasks.aspx", "ir2") & "', ABOVE, LEFT)")
            ir2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ir3.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontasks.aspx", "ir3") & "', ABOVE, LEFT)")
            ir3.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontasks.aspx", "ovid2") & "', ABOVE, LEFT)")
            ovid2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid3.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontasks.aspx", "ovid3") & "', ABOVE, LEFT)")
            ovid3.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid4.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontasks.aspx", "ovid4") & "', ABOVE, LEFT)")
            ovid4.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid5.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontasks.aspx", "ovid5") & "', ABOVE, LEFT)")
            ovid5.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid6.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontasks.aspx", "ovid6") & "', ABOVE, LEFT)")
            ovid6.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid7.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontasks.aspx", "ovid7") & "', ABOVE, LEFT)")
            ovid7.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid8.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("commontasks.aspx", "ovid8") & "', ABOVE, LEFT)")
            ovid8.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
