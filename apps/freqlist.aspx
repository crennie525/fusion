<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="freqlist.aspx.vb" Inherits="lucy_r12.freqlist" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>freqlist</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/freqlistaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <div id="divval" style="border-bottom: black 1px solid; position: absolute; border-left: black 1px solid;
        width: 400px; height: 400px; overflow: auto; border-top: black 1px solid; top: 4px;
        border-right: black 1px solid; left: 4px" runat="server">
    </div>
    <input type="hidden" id="lblpmtskid" runat="server">
    <input type="hidden" id="lblfld" runat="server">
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lblvid" runat="server">
    <input type="hidden" id="lblsubmit" runat="server">
    <input type="hidden" id="lblval" runat="server">
    <input type="hidden" id="lblvind" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
