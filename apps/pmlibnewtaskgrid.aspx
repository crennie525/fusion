<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmlibnewtaskgrid.aspx.vb"
    Inherits="lucy_r12.pmlibnewtaskgrid" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Task Grid View</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="javascript" type="text/javascript" src="../scripts/gtask.js"></script>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmlibnewtaskgridaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table width="3200">
        <tr>
            <td>
                <asp:ImageButton ID="addtask" runat="server" ImageUrl="../images/appbuttons/bgbuttons/addtask.gif">
                </asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DataGrid ID="dgtasks" runat="server" CellSpacing="1" AutoGenerateColumns="False"
                    AllowSorting="True" GridLines="None">
                    <AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                                <img id="ibast" runat="server" src="../images/appbuttons/minibuttons/subtask.gif"
                                    onclick="getsgrid();" width="20" height="20">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="tasknum desc" HeaderText="Task#">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblta" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="lblt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'
                                    Width="40px">
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="subtask desc" HeaderText="Sub Tasks" Visible="False">
                            <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblsubt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblst" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <HeaderTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CommandName="hd" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                    ToolTip="Hide Description"></asp:ImageButton><br>
                                <asp:ImageButton ID="ImageButton2" runat="server" CommandName="sd" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                    ToolTip="Show Description"></asp:ImageButton>
                            </HeaderTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Description">
                            <HeaderStyle Width="270px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label3" Width="270px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtdesc" runat="server" CssClass="plainlabel" MaxLength="500" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>'
                                    Width="260px" TextMode="MultiLine" Height="70px">
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <HeaderTemplate>
                                <asp:ImageButton ID="ImageButton3" runat="server" CommandName="htt" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                    ToolTip="Hide Task Type"></asp:ImageButton><br>
                                <asp:ImageButton ID="ImageButton4" runat="server" CommandName="stt" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                    ToolTip="Show Task Type"></asp:ImageButton>
                            </HeaderTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="tasktype desc" HeaderText="Task Type">
                            <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasktype") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lbltasktype" runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.tasktype") %>'>
                                </asp:Label>
                                <img id="imgtasktype" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <HeaderTemplate>
                                <asp:ImageButton ID="Imagebutton5" runat="server" CommandName="hfr" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                    ToolTip="Hide Frequency"></asp:ImageButton><br>
                                <asp:ImageButton ID="Imagebutton6" runat="server" CommandName="sfr" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                    ToolTip="Show Frequency"></asp:ImageButton>
                            </HeaderTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="freq desc" HeaderText="Frequency">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblfreq" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'>
                                </asp:Label>
                                <img id="imgfreq" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <HeaderTemplate>
                                <asp:ImageButton ID="Imagebutton24" runat="server" CommandName="hcomp" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                    ToolTip="Hide Reliability Maintenance Type"></asp:ImageButton><br>
                                <asp:ImageButton ID="Imagebutton25" runat="server" CommandName="scomp" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                    ToolTip="Show Reliability Maintenance Type"></asp:ImageButton>
                            </HeaderTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="compnum desc" HeaderText="Component">
                            <HeaderStyle Width="90px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblcompnum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
                                </asp:Label>
                                <img id="imgcompnum" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="compnum desc" HeaderText="Failure Modes This Task Will Address">
                            <HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label21" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fm1") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblfm1" Width="230px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fm1") %>'>
                                </asp:Label>
                                <img id="imgfm" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <HeaderTemplate>
                                <asp:ImageButton ID="Imagebutton11" runat="server" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                    CommandName="hsk" ToolTip="Hide Skill Required"></asp:ImageButton><br>
                                <asp:ImageButton ID="Imagebutton12" runat="server" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                    CommandName="ssk" ToolTip="Show Skill Required"></asp:ImageButton>
                            </HeaderTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="skill desc" HeaderText="Skill Required">
                            <HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblskill" Width="230px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                </asp:Label>
                                <img id="imgskill" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Qty">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'
                                    ID="Label10" NAME="Label8">
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtqty" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Time">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'
                                    ID="Label12">
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txttr" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                                <asp:ImageButton ID="Imagebutton9" runat="server" CommandName="hpt" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                    ToolTip="Hide Predictive Technology"></asp:ImageButton><br>
                                <asp:ImageButton ID="Imagebutton10" runat="server" CommandName="spt" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                    ToolTip="Show Predictive Technology"></asp:ImageButton>
                            </HeaderTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="pretech desc" HeaderText="Predictive Technology">
                            <HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pretech") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblpretech" Width="230px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pretech") %>'>
                                </asp:Label>
                                <img id="imgpretech" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <HeaderTemplate>
                                <asp:ImageButton ID="Imagebutton13" runat="server" CommandName="hes" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                    ToolTip="Hide PM Equipment Status"></asp:ImageButton><br>
                                <asp:ImageButton ID="Imagebutton14" runat="server" CommandName="ses" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                    ToolTip="Show PM Equipment Status"></asp:ImageButton>
                            </HeaderTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="rd desc" HeaderText="PM Equipment Status">
                            <HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label75" runat="server" Font-Size="10pt" Font-Names="Arial" ForeColor="Black"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.rd") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblrd" Width="120px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rd") %>'>
                                </asp:Label>
                                <img id="imgrd" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <HeaderTemplate>
                                <asp:ImageButton ID="Imagebutton15" runat="server" CommandName="hlo" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                    ToolTip="Hide LOTO"></asp:ImageButton><br>
                                <asp:ImageButton ID="Imagebutton16" runat="server" CommandName="slo" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                    ToolTip="Show LOTO"></asp:ImageButton>
                            </HeaderTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="loto desc" HeaderText="LOTO">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label11" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.loto") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddloto" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("lotoid")) %>'>
                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="2">No</asp:ListItem>
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <HeaderTemplate>
                                <asp:ImageButton ID="Imagebutton17" runat="server" CommandName="hco" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                    ToolTip="Hide Confined Space"></asp:ImageButton><br>
                                <asp:ImageButton ID="Imagebutton18" runat="server" CommandName="sco" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                    ToolTip="Show Confined Space"></asp:ImageButton>
                            </HeaderTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="confined desc" HeaderText="Confined Spaced">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label19" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.confined") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddcs" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("conid")) %>'>
                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="2">No</asp:ListItem>
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Parts/Tools/Lubes">
                            <HeaderStyle CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton23" runat="server" ImageUrl="../images/appbuttons/minibuttons/parttrans.gif"
                                    ToolTip="Add Parts" CommandName="Part"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton23a" runat="server" ImageUrl="../images/appbuttons/minibuttons/tooltrans.gif"
                                    ToolTip="Add Tools" CommandName="Tool"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton23b" runat="server" ImageUrl="../images/appbuttons/minibuttons/lubetrans.gif"
                                    ToolTip="Add Lubricants" CommandName="Lube"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton23c" runat="server" ImageUrl="../images/appbuttons/minibuttons/notessm.gif"
                                    ToolTip="Add Notes to the Task" CommandName="Note"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="pmtskid">
                            <ItemTemplate>
                                <asp:Label ID="lbltid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblttid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Delete">
                            <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="ibDel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblfuid" runat="server">
    <input type="hidden" id="lblfilt" runat="server">
    <input type="hidden" id="lblfiltfilt" runat="server">
    <input type="hidden" id="lbltcnt" runat="server">
    <input type="hidden" id="lblsubval" runat="server">
    <input type="hidden" id="lblcurrsort" runat="server">
    <input type="hidden" id="lbltool" runat="server">
    <input type="hidden" id="lblpart" runat="server" name="Hidden1">
    <input type="hidden" id="lbllube" runat="server" name="Hidden2">
    <input type="hidden" id="lblnote" runat="server" name="Hidden2">
    <input type="hidden" id="lblpmtid" runat="server" name="Hidden2">
    <input type="hidden" id="lbltasknum" runat="server" name="Hidden2">
    <input type="hidden" id="lblsort" runat="server">
    <input type="hidden" id="lbloldtask" runat="server">
    <input type="hidden" id="lblcurrcomid" runat="server">
    <input type="hidden" id="lblcurrcomdesc" runat="server">
    <input type="hidden" id="xcoord" runat="server" name="Hidden1">
    <input type="hidden" id="ycoord" runat="server" name="Hidden2">
    <input type="hidden" id="lbleqid" runat="server" name="Hidden1">
    <input type="hidden" id="lblsid" runat="server" name="Hidden2">
    <input type="hidden" id="lbldid" runat="server" name="Hidden2">
    <input type="hidden" id="lblcid" runat="server" name="Hidden2">
    <input type="hidden" id="lblclid" runat="server" name="Hidden2">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
