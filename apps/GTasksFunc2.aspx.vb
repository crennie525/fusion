

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class GTasksFunc2
    Inherits System.Web.UI.Page
	Protected WithEvents lang240 As System.Web.UI.WebControls.Label

	Protected WithEvents lang239 As System.Web.UI.WebControls.Label

	Protected WithEvents lang238 As System.Web.UI.WebControls.Label

	Protected WithEvents lang237 As System.Web.UI.WebControls.Label

	Protected WithEvents lang236 As System.Web.UI.WebControls.Label

	Protected WithEvents lang235 As System.Web.UI.WebControls.Label

	Protected WithEvents lang234 As System.Web.UI.WebControls.Label

	Protected WithEvents lang233 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tli, cid, sid, did, clid, eqid, funid, comid, tid, se, cell, sql, fuid, typ, lid, ro As String
    Dim Filter, AddVal, SubVal, FiltFilt, login, app, ustr As String
    Dim ds As DataSet
    Dim dr As SqlDataReader
    Dim gtasks As New Utilities
    Dim bgtasks As New Utilities
    Dim bgtasks2 As New Utilities
    Dim dslev As DataSet
    Dim dshd As DataSet
    Dim appc As New AppUtils
    Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings.Item("custAppUrl")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lstfilter As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lstdef As System.Web.UI.WebControls.DropDownList
    Protected WithEvents addtask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnreturn As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblsort As System.Web.UI.WebControls.Label
    Protected WithEvents ErrorLabel As System.Web.UI.WebControls.Label
    Protected WithEvents dlhd As System.Web.UI.WebControls.DataList
    Protected WithEvents dgtasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpmtid As System.Web.UI.WebControls.Label
    Protected WithEvents xcoord As System.Web.UI.WebControls.TextBox
    Protected WithEvents ycoord As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnexport As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents hpBackward As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltli As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltool As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllube As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnote As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcomdesc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblitemindex As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldocpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldocpmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblncindx As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusemeter As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskdesc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblqty As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltime As System.Web.UI.HtmlControls.HtmlInputHidden
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            Dim redir As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
            redir = redir & "?logout=yes"
            Response.Redirect(redir)
        End Try

        Dim url As String = appc.Switch
        If url <> "ok" Then
            Response.Redirect(url)
        End If


        If Not IsPostBack Then
            ustr = Request.QueryString("ustr").ToString
            lbluser.value = ustr
            'ro = Request.QueryString("ro").ToString
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                dgtasks.Columns(0).Visible = False
                dgtasks.Columns(26).Visible = False
                addtask.ImageUrl = "../images/appbuttons/bgbuttons/addtaskdis.gif"
                addtask.Enabled = False

            End If
            Try
                If ro = "1" Then
                    'pgtitle.InnerHtml = "Task Grid View (Read Only)"
                Else
                    'pgtitle.InnerHtml = "Task Grid View"
                End If
            Catch ex As Exception

            End Try
            tli = Request.QueryString("tli").ToString
            cid = Request.QueryString("cid").ToString
            app = Request.QueryString("app").ToString
            If app = "opt" Then
                Dim pmid, pmstr As String
                pmid = Request.QueryString("pmid").ToString
                lbldocpmid.Value = pmid
                pmstr = Request.QueryString("pmstr").ToString
                lbldocpmstr.Value = pmstr
            End If
            lbltli.Value = tli
            lblcid.Value = cid
            lbladdchk.Value = "no"
            lblapp.Value = app
            If tli = "5s" Then
                funid = Request.QueryString("funid").ToString
                comid = Request.QueryString("comid").ToString
                tid = Request.QueryString("tid").ToString
                lblcoid.Value = comid
                If lblcoid.Value = "no" Then
                    lblcoid.Value = "0"
                End If
                lblfuid.Value = funid
                lbltaskid.Value = tid
                tid = Request.QueryString("tid").ToString
                Filter = "funcid = '" & funid & "' and tasknum = '" & tid & "'"
                lblfilt.Value = Filter
                SubVal = "(compid, funcid, tasknum, subtask) values ('" & cid & "', '" & funid & "', "
                lblsubval.Value = SubVal
                addtask.Visible = False
            ElseIf tli = "5a" Then
                funid = Request.QueryString("funid").ToString
                lblfuid.Value = funid
                comid = Request.QueryString("comid").ToString
                lblcoid.Value = comid
                If lblcoid.Value = "no" Then
                    lblcoid.Value = "0"
                End If
                Filter = "funcid = '" & funid & "' and subtask = 0"
                lblfilt.Value = Filter
                AddVal = "(compid, funcid, tasknum) values ('" & cid & "', '" & funid & "', "
                lbladdval.Value = AddVal
                SubVal = "(compid, funcid, tasknum, subtask) values ('" & cid & "', '" & funid & "', "
                lblsubval.Value = SubVal
            End If
            Try
                cell = Request.QueryString("chk").ToString
                lblchk.Value = cell
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                lid = Request.QueryString("lid").ToString
                lbllid.Value = lid
                If typ = "loc" Then
                    lblchk.Value = "loc"
                End If
            Catch ex As Exception

            End Try
            lbltool.Value = "no"
            lblpart.Value = "no"
            lbllube.Value = "no"
            lblnote.Value = "no"
            lblcurrsort.Value = "tasknum, subtask asc"
            lblsort.Text = "Task#/Sub Task# Ascending"
            gtasks.Open()
            BindFuncHead2()
            BindGrid("tasknum, subtask asc")

            gtasks.Dispose()

        End If
        'btnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'btnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Sub BindFuncHead2()
        tli = lbltli.Value
        cid = lblcid.Value
        cell = lblchk.Value
        comid = lblcoid.Value
        funid = lblfuid.Value
        sql = "select f.func_id, e.eqid, e.dept_id, e.cellid, e.siteid, d.dept_line, isnull(c.cell_name, 'N\A') as cell_name, " _
        + "e.eqnum, f.func " _
        + "from equipment e " _
        + "left join functions f on f.eqid = e.eqid " _
        + "left join dept d on d.dept_id = e.dept_id " _
        + "left join cells c on c.cellid = e.cellid " _
        + "where f.func_id = '" & funid & "'"
        dr = gtasks.GetRdrData(sql)
        dlhd.DataSource = dr
        dlhd.DataBind()
        dr.Close()
        sql = "select f.func_id, e.eqid, e.dept_id, e.cellid, e.siteid " _
        + "from equipment e left join functions f on f.eqid = e.eqid " _
        + "where f.func_id = '" & funid & "'"
        dr = gtasks.GetRdrData(sql)
        If dr.Read Then
            lblsid.Value = dr.Item("siteid").ToString
            lbldid.Value = dr.Item("dept_id").ToString
            lbleqid.Value = dr.Item("eqid").ToString
            lblfuid.Value = dr.Item("func_id").ToString
            lblclid.Value = dr.Item("cellid").ToString
        End If
        dr.Close()
    End Sub
    Private Sub BindFuncHead()
        tli = lbltli.Value
        cid = lblcid.Value
        cell = lblchk.Value
        comid = lblcoid.Value
        funid = lblfuid.Value
        Dim head As New clsHead
        If comid <> "no" Then
            If cell = "yes" Then
                dr = head.GetFuncHead(tli, cid, cell, comid, funid)
                dlhd.DataSource = dr
                dlhd.DataBind()
                dr.Close()
                dr = head.GetFuncHead(tli, cid, cell, comid, funid)
                If dr.Read Then
                    lblsid.Value = dr.Item("siteid").ToString
                    lbldid.Value = dr.Item("dept_id").ToString
                    lbleqid.Value = dr.Item("eqid").ToString
                    lblfuid.Value = dr.Item("func_id").ToString
                    lblclid.Value = dr.Item("cellid").ToString
                End If
                dr.Close()
            Else
                dr = head.GetFuncHead(tli, cid, cell, comid, funid)
                dlhd.DataSource = dr
                dlhd.DataBind()
                dr.Close()
                dr = head.GetFuncHead(tli, cid, cell, comid, funid)
                If dr.Read Then
                    lblsid.Value = dr.Item("siteid").ToString
                    Try
                        lbldid.Value = dr.Item("dept_id").ToString
                    Catch ex As Exception

                    End Try
                    lbleqid.Value = dr.Item("eqid").ToString
                    lblfuid.Value = dr.Item("func_id").ToString
                    lblclid.Value = "0"
                End If
                dr.Close()
            End If
        Else
            If cell = "yes" Then
                dr = head.GetFuncHead(tli, cid, cell, comid, funid)
                dlhd.DataSource = dr
                dlhd.DataBind()
                dr.Close()
                dr = head.GetFuncHead(tli, cid, cell, comid, funid)
                If dr.Read Then
                    lblsid.Value = dr.Item("siteid").ToString
                    lbldid.Value = dr.Item("dept_id").ToString
                    lbleqid.Value = dr.Item("eqid").ToString
                    lblclid.Value = dr.Item("cellid").ToString
                End If
                dr.Close()
            Else
                dr = head.GetFuncHead(tli, cid, cell, comid, funid)
                dlhd.DataSource = dr
                dlhd.DataBind()
                dr.Close()
                dr = head.GetFuncHead(tli, cid, cell, comid, funid)
                If dr.Read Then
                    lblsid.Value = dr.Item("siteid").ToString
                    lbleqid.Value = dr.Item("eqid").ToString
                End If
                dr.Close()
            End If
        End If
    End Sub
    Private Sub BindHead()
        tli = lbltli.Value
        cid = lblcid.Value
        cell = lblchk.Value
        comid = lblcoid.Value
        If comid <> "no" Then
            If cell = "yes" Then
                sql = "select " _
                + "c.compname, co.compnum, " _
                + "co.compdesc, f.func_id, f.func, f.func_desc, e.eqid, e.eqnum, e.eqdesc, cl.cellid, cl.cell_name, " _
                + "cl.cell_desc, d.dept_id, d.dept_line, d.dept_desc, s.siteid, s.sitename, s.sitedesc " _
                + "from company c, components co, functions f, equipment e, cells cl, dept d, sites s " _
                + "where s.compid = c.compid and f.func_id = co.func_id and f.eqid = e.eqid and " _
                + "e.cellid = cl.cellid and cl.Dept_ID = d.Dept_ID and " _
                + "d.siteid = s.siteid and co.comid = '" & comid & "'"
                dr = gtasks.GetRdrData(sql)
                dlhd.DataSource = dr
                dlhd.DataBind()
                dr.Close()
                dr = gtasks.GetRdrData(sql)
                If dr.Read Then
                    lblsid.Value = dr.Item("siteid").ToString
                    lbldid.Value = dr.Item("dept_id").ToString
                    lbleqid.Value = dr.Item("eqid").ToString
                    lblfuid.Value = dr.Item("func_id").ToString
                    lblclid.Value = dr.Item("cellid").ToString
                End If
                dr.Close()
            ElseIf cell = "no" Then
                sql = "select " _
                 + "c.compname, co.compnum, " _
                + "co.compdesc, f.func_id, f.func, f.func_desc, e.eqid, e.eqnum, e.eqdesc, " _
                + "d.dept_id, d.dept_line, d.dept_desc, s.siteid, s.sitename, s.sitedesc, " _
                + "'N/A' as cell_name, '' as cell_desc " _
                + "from company c, components co, functions f, equipment e, dept d, sites s " _
                + "where s.compid = c.compid and f.func_id = co.func_id and f.eqid = e.eqid and " _
                + "e.Dept_ID = d.Dept_ID and " _
                + "d.siteid = s.siteid and co.comid = '" & comid & "'"
                dr = gtasks.GetRdrData(sql)
                dlhd.DataSource = dr
                dlhd.DataBind()
                dr.Close()
                dr = gtasks.GetRdrData(sql)
                If dr.Read Then
                    lblsid.Value = dr.Item("siteid").ToString
                    lbldid.Value = dr.Item("dept_id").ToString
                    lbleqid.Value = dr.Item("eqid").ToString
                    lblfuid.Value = dr.Item("func_id").ToString
                End If
                dr.Close()
            Else
                sql = "select " _
                               + "c.compname, co.compnum, " _
                               + "co.compdesc, f.func_id, f.func, f.func_desc, e.eqid, e.eqnum, e.eqdesc, " _
                               + "'N/A' as dept_line, '' as dept_desc, s.siteid, s.sitename, s.sitedesc, " _
                               + "'N/A' as cell_name, '' as cell_desc " _
                               + "from company c, components co, functions f, equipment e, sites s " _
                               + "where s.compid = c.compid and f.func_id = co.func_id and f.eqid = e.eqid and " _
                               + "co.comid = '" & comid & "'"
                dr = gtasks.GetRdrData(sql)
                dlhd.DataSource = dr
                dlhd.DataBind()
                dr.Close()
                dr = gtasks.GetRdrData(sql)
                If dr.Read Then
                    lblsid.Value = dr.Item("siteid").ToString
                    lbleqid.Value = dr.Item("eqid").ToString
                    lblfuid.Value = dr.Item("func_id").ToString
                End If
                dr.Close()
            End If
        Else
            funid = lblfuid.Value
            If cell = "yes" Then
                sql = "select " _
                + "c.compname, " _
                + "f.func, f.func_desc, e.eqid, e.eqnum, e.eqdesc, cl.cellid, cl.cell_name, " _
                + "cl.cell_desc, d.dept_id, d.dept_line, d.dept_desc, s.siteid, s.sitename, s.sitedesc, " _
                + "'N/A' as compnum, '' as compdesc " _
                + "from company c, functions f, equipment e, cells cl, dept d, sites s " _
                + "where s.compid = c.compid and f.eqid = e.eqid and " _
                + "e.cellid = cl.cellid and cl.Dept_ID = d.Dept_ID and " _
                + "d.siteid = s.siteid and f.func_id = '" & funid & "'"
                dr = gtasks.GetRdrData(sql)
                dlhd.DataSource = dr
                dlhd.DataBind()
                dr.Close()
                dr = gtasks.GetRdrData(sql)
                If dr.Read Then
                    lblsid.Value = dr.Item("siteid").ToString
                    lbldid.Value = dr.Item("dept_id").ToString
                    lbleqid.Value = dr.Item("eqid").ToString
                    lblclid.Value = dr.Item("cellid").ToString
                End If
                dr.Close()
            ElseIf cell = "no" Then
                sql = "select " _
                + "c.compname, " _
               + "f.func, f.func_desc, e.eqid, e.eqnum, e.eqdesc, " _
               + "d.dept_id, d.dept_line, d.dept_desc, s.siteid, s.sitename, s.sitedesc, " _
               + "'N/A' as cell_name, '' as cell_desc, " _
               + "'N/A' as compnum, '' as compdesc " _
               + "from company c, functions f, equipment e, dept d, sites s " _
               + "where s.compid = c.compid and f.eqid = e.eqid and " _
               + "e.Dept_ID = d.Dept_ID and " _
               + "d.siteid = s.siteid and f.func_id = '" & funid & "'"
                dr = gtasks.GetRdrData(sql)
                dlhd.DataSource = dr
                dlhd.DataBind()
                dr.Close()
                dr = gtasks.GetRdrData(sql)
                If dr.Read Then
                    lblsid.Value = dr.Item("siteid").ToString
                    lbldid.Value = dr.Item("dept_id").ToString
                    lbleqid.Value = dr.Item("eqid").ToString
                End If
                dr.Close()
            Else
                sql = "select " _
                              + "c.compname, " _
                              + "f.func_id, f.func, f.func_desc, e.eqid, e.eqnum, e.eqdesc, " _
                              + "'N/A' as dept_line, '' as dept_desc, s.siteid, s.sitename, s.sitedesc, " _
                              + "'N/A' as cell_name, '' as cell_desc, " _
                              + "'N/A' as compnum, '' as compdesc " _
                              + "from company c, functions f, equipment e, sites s " _
                              + "where s.compid = c.compid and f.eqid = e.eqid and " _
                              + "f.func_id = '" & funid & "'"
                dr = gtasks.GetRdrData(sql)
                dlhd.DataSource = dr
                dlhd.DataBind()
                dr.Close()
                dr = gtasks.GetRdrData(sql)
                If dr.Read Then
                    lblsid.Value = dr.Item("siteid").ToString
                    lbleqid.Value = dr.Item("eqid").ToString
                End If
                dr.Close()
            End If
        End If
    End Sub

    Private Sub BindGrid(ByVal sortExp As String)
        Try
            'gtasks.Open()
        Catch ex As Exception

        End Try
        Filter = lblfilt.Value
        FiltFilt = lblfiltfilt.Value
        sql = "select count(*) from pmtasks where " & Filter & FiltFilt
        Try
            Dim tcnt As Integer = gtasks.Scalar(sql)
            lbltcnt.Value = tcnt
        Catch ex As Exception

        End Try
        funid = lblfuid.Value
        sql = "update pmtasks set lotoid = 0 where lotoid is null " _
        + "update pmtasks set conid = 0 where conid is null"
        gtasks.Update(sql)
        sql = "select * from pmtasks where " & Filter & FiltFilt & " order by " & sortExp
        ds = gtasks.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Try
            dgtasks.DataSource = dv
            dgtasks.DataBind()
        Catch ex As Exception

        End Try
        'gtasks.Dispose()
    End Sub

    Private Sub dgtasks_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgtasks.SortCommand
        gtasks.Open()
        BindGrid(e.SortExpression)
        gtasks.Dispose()
        Dim se As String = e.SortExpression
        lblcurrsort.Value = se
        Select Case se
            Case "tasknum desc"
                dgtasks.Columns(1).SortExpression = "tasknum asc"
                lblsort.Text = "Task# Descending"
            Case "tasknum asc"
                dgtasks.Columns(1).SortExpression = "tasknum desc"
                lblsort.Text = "Task# Ascending"
            Case "subtask desc"
                dgtasks.Columns(2).SortExpression = "subtask asc"
                lblsort.Text = "Sub Task# Descending"
            Case "subtask asc"
                dgtasks.Columns(2).SortExpression = "subtask desc"
                lblsort.Text = "Sub Task# Ascending"
            Case "tasktype desc"
                dgtasks.Columns(6).SortExpression = "tasktype asc"
                lblsort.Text = "Task Type Descending"
            Case "tasktype asc"
                dgtasks.Columns(6).SortExpression = "tasktype desc"
                lblsort.Text = "Task Type Ascending"
            Case "freq desc"
                dgtasks.Columns(8).SortExpression = "freq asc"
                lblsort.Text = "Frequency Descending"
            Case "freq asc"
                dgtasks.Columns(8).SortExpression = "freq desc"
                lblsort.Text = "Frequency Ascending"

            Case "comp desc"
                dgtasks.Columns(10).SortExpression = "comp asc"
                dgtasks.Columns(11).SortExpression = "comp asc"
                lblsort.Text = "Component Descending"
            Case "comp asc"
                dgtasks.Columns(10).SortExpression = "comp desc"
                dgtasks.Columns(11).SortExpression = "comp desc"
                lblsort.Text = "Component Ascending"

            Case "skill desc"
                dgtasks.Columns(13).SortExpression = "skill asc"
                lblsort.Text = " Skill Required Descending"
            Case "skill asc"
                dgtasks.Columns(13).SortExpression = "skill desc"
                lblsort.Text = "Skill Required Ascending"

            Case "pretech desc"
                dgtasks.Columns(17).SortExpression = "pretech asc"
                lblsort.Text = " Predictive Technology Descending"
            Case "pretech asc"
                dgtasks.Columns(17).SortExpression = "pretech desc"
                lblsort.Text = "Predictive Technology Ascending"
            Case "rd desc"
                dgtasks.Columns(19).SortExpression = "rd asc"
                lblsort.Text = "PM Equipment Status Descending"
            Case "rd asc"
                dgtasks.Columns(19).SortExpression = "rd desc"
                lblsort.Text = "PM Equipment Status Ascending"

            Case "loto desc"
                dgtasks.Columns(21).SortExpression = "loto asc"
                lblsort.Text = " LOTO Descending"
            Case "loto asc"
                dgtasks.Columns(21).SortExpression = "loto desc"
                lblsort.Text = "LOTO Ascending"
            Case "confined desc"
                dgtasks.Columns(23).SortExpression = "confined asc"
                lblsort.Text = " Confined Space Descending"
            Case "confined asc"
                dgtasks.Columns(23).SortExpression = "confined desc"
                lblsort.Text = "Confined Space Ascending"
        End Select

    End Sub
    Public Function PopulateTaskTypes() As DataSet
        cid = lblcid.Value
        sql = "select ttid, tasktype, taskindex from pmTaskTypes where compid = '" & cid & "' or taskindex = '0' order by taskindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Public Function PopulateStatus() As DataSet
        cid = lblcid.Value
        sql = "select statid, status, statusindex from pmStatus where compid = '" & cid & "' or statusindex = '0' order by statusindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer

        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function

    Private Sub dgtasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.EditCommand
        se = lblcurrsort.Value
        lbloldtask.Value = CType(e.Item.FindControl("lblta"), Label).Text
        dgtasks.EditItemIndex = e.Item.ItemIndex
        gtasks.Open()
        BindGrid(se)
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.CancelCommand
        dgtasks.EditItemIndex = -1
        se = lblcurrsort.Value
        gtasks.Open()
        BindGrid(se)
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.ItemCommand
        Dim vc As String = e.CommandName
        Select Case vc
            Case "hd"
                dgtasks.Columns(4).Visible = False
            Case "sd"
                dgtasks.Columns(4).Visible = True

            Case "htt"
                dgtasks.Columns(6).Visible = False
            Case "stt"
                dgtasks.Columns(6).Visible = True
            Case "hfr"
                dgtasks.Columns(8).Visible = False
            Case "sfr"
                dgtasks.Columns(8).Visible = True

            Case "hcomp"
                dgtasks.Columns(10).Visible = False
                dgtasks.Columns(11).Visible = False
            Case "scomp"
                dgtasks.Columns(10).Visible = True
                dgtasks.Columns(11).Visible = True

            Case "hsk"
                dgtasks.Columns(13).Visible = False
                dgtasks.Columns(14).Visible = False
                dgtasks.Columns(15).Visible = False
            Case "ssk"
                dgtasks.Columns(13).Visible = True
                dgtasks.Columns(14).Visible = True
                dgtasks.Columns(15).Visible = True

            Case "hpt"
                dgtasks.Columns(17).Visible = False
            Case "spt"
                dgtasks.Columns(17).Visible = True

            Case "hes"
                dgtasks.Columns(19).Visible = False
            Case "ses"
                dgtasks.Columns(19).Visible = True


            Case "hlo"
                dgtasks.Columns(21).Visible = False
            Case "slo"
                dgtasks.Columns(21).Visible = True
            Case "hco"
                dgtasks.Columns(23).Visible = False
            Case "sco"
                dgtasks.Columns(23).Visible = True
        End Select

        If e.CommandName = "AddSub" Then
            ErrorLabel.Text = ""
            Filter = lblfilt.Value
            SubVal = lblsubval.Value
            Dim ts, tn As String
            ts = CType(e.Item.FindControl("lblst"), Label).Text
            tn = CType(e.Item.FindControl("lblt"), TextBox).Text
            If ts <> "0" Then
                ErrorLabel.Text = "Can't add sub tasks to a sub task"
                Return
            Else
                gtasks.Open()
                Dim stcnt As Integer
                sql = "Select count(*) from pmTasks " _
                + "where " & Filter & " and taskNum = '" & tn & "' and subTask <> '0'"
                stcnt = gtasks.Scalar(sql)
                Dim newtst As String = stcnt + 1 '((stcnt * 0.1) + 0.1)
                Dim newsub As Integer
                sql = "insert into pmtasks " & SubVal & "'" & tn & "', '" & newtst & "') select @@identity as 'identity'"
                newsub = gtasks.Scalar(sql)
                'to do *** convert to common sub - same used in update task
                'gtasks.Open()
                Dim fre, tfre, ski, tski, sta, tsta, lot, tlot, con, tcon, tas, ttas, mai, tmai, pre, tpre, tskd As String
                fre = "" 'CType(e.Item.FindControl("txtfreq"), DropDownList).SelectedItem.Value
                tfre = CType(e.Item.FindControl("txtfreq"), TextBox).Text
                ski = CType(e.Item.FindControl("ddskill"), DropDownList).SelectedItem.Value
                tski = CType(e.Item.FindControl("ddskill"), DropDownList).SelectedItem.Text
                sta = CType(e.Item.FindControl("ddeqstat"), DropDownList).SelectedItem.Value
                tsta = CType(e.Item.FindControl("ddeqstat"), DropDownList).SelectedItem.Text
                lot = CType(e.Item.FindControl("ddloto"), DropDownList).SelectedIndex
                tlot = CType(e.Item.FindControl("ddloto"), DropDownList).SelectedItem.Text
                con = CType(e.Item.FindControl("ddcs"), DropDownList).SelectedIndex
                tcon = CType(e.Item.FindControl("ddcs"), DropDownList).SelectedItem.Text
                tas = CType(e.Item.FindControl("ddtype"), DropDownList).SelectedItem.Value
                ttas = CType(e.Item.FindControl("ddtype"), DropDownList).SelectedItem.Text
                pre = CType(e.Item.FindControl("ddpt"), DropDownList).SelectedItem.Value
                tpre = CType(e.Item.FindControl("ddpt"), DropDownList).SelectedItem.Text
                Dim txt, qty As String
                qty = CType(e.Item.FindControl("txtqty"), TextBox).Text
                If Len(qty) = 0 Then
                    qty = "0"
                End If
                txt = CType(e.Item.FindControl("txttr"), TextBox).Text
                tn = CType(e.Item.FindControl("lblt"), TextBox).Text
                ts = CType(e.Item.FindControl("lblst"), Label).Text
                Filter = lblfilt.Value
                Dim fm1, fm1s, fm2, fm2s, fm3, fm3s, fm4, fm4s, fm5, fm5s As String

                sql = "update pmTasks set " _
                + "qty = '" & qty & "', " _
                + "tTime = '" & txt & "', " _
                + "rdid = '" & sta & "', " _
                + "rd = '" & tsta & "', " _
                + "skillid = '" & ski & "', " _
                + "skill = '" & tski & "', " _
                + "ptid = '" & pre & "', " _
                + "pretech = '" & tpre & "', " _
                + "freqid = '" & fre & "', " _
                + "freq = '" & tfre & "', " _
                + "lotoid = '" & lot & "', " _
                + "loto = '" & tlot & "', " _
                + "conid = '" & con & "', " _
                + "confined = '" & tcon & "', " _
                + "tasktype = '" & ttas & "', " _
                + "ttid = '" & tas & "', " _
                + "revised = getDate() where " & Filter & " and pmtskid = '" & newsub & "'"
                gtasks.Update(sql)

                sql = "sp_updateTaskFMIndexes '" & newsub & "'"

                gtasks.Update(sql)
                '*** end to do section        

                se = lblcurrsort.Value
                dgtasks.EditItemIndex = -1
                BindGrid(se)
            End If
            dgtasks.EditItemIndex = -1
        End If

        If e.CommandName = "Tool" Then
            lbltool.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Text = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Text = CType(e.Item.FindControl("lbltid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If

        End If
        If e.CommandName = "Part" Then
            lblpart.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Text = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Text = CType(e.Item.FindControl("lbltid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        If e.CommandName = "Lube" Then
            lbllube.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Text = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Text = CType(e.Item.FindControl("lbltid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        If e.CommandName = "Note" Then
            lblnote.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Text = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Text = CType(e.Item.FindControl("lbltid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If

    End Sub

    Private Sub btnreturn_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnreturn.Click

        tli = lbltli.Value
        tli = Mid(tli, 1, 1)
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        eqid = lbleqid.Value
        funid = lblfuid.Value
        clid = lblclid.Value
        app = lblapp.Value
        lid = lbllid.Value
        typ = lbltyp.Value
        ustr = lbluser.value
        If lid = "" Then ' And typ = "" 
            typ = "reg"
        End If

        Dim pmid, pmstr As String
        pmid = lbldocpmid.Value
        pmstr = lbldocpmstr.Value
        If lblcoid.Value = "no" Then
            comid = "0"
        Else
            comid = lblcoid.Value
        End If
        cell = lblchk.Value
        If app = "dev" Then
            Response.Redirect("PMTasks.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & funid & "&comid=" & comid & "&chk=" & cell & "&typ=" & typ & "&lid=" & lid & "&ustr=" & ustr)
        ElseIf app = "opt" Then
            Response.Redirect("../appsopt/PM3OptMain.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & funid & "&comid=" & comid & "&chk=" & cell & "&pmid=" & pmid & "&pmstr=" & pmstr & "&app=opt" & "&typ=" & typ & "&lid=" & lid & "&ustr=" & ustr)
        Else
            'Response.Redirect("../appsopt/PM3OptMainnohdr.aspx?who=opt&jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + comid + "&chk=&pmid=&pmstr=&app=opt&typ=" + typ + "&lid=" + lid + "&ustr=" + ustr + "&eqnum=&dept=&cell=" + cell + "&loc=")
            Response.Redirect("../appspmo123tab/pmo123tabmain.aspx?who=grid&lvl=fu&start=yes&ret=yes&lid=" & lid & "&chk=yes&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&eqid=" & eqid & "&fuid=" & funid & "&usrname=" & ustr & "&eq=&dept=&cell=&loc=&coid=")
        End If


    End Sub
    Private Sub BindExport(ByVal sortExp As String)
        Filter = lblfilt.Value
        FiltFilt = lblfiltfilt.Value
        sql = "select * from pmtasks where " & Filter & FiltFilt & " order by " & sortExp
        gtasks.Open()
        ds = gtasks.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        'dgexport.DataSource = dv
        'dgexport.DataBind()
        gtasks.Dispose()
    End Sub

    Private Sub addtask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles addtask.Click
        se = lblcurrsort.Value
        Dim tskcnt As Integer
        Filter = lblfilt.Value
        Dim subfilt As String
        subfilt = " and subtask = '0'"
        AddVal = lbladdval.Value
        gtasks.Open()
        sql = "select count(*) from pmtasks where " & Filter & subfilt
        tskcnt = gtasks.Scalar(sql)
        tskcnt = tskcnt + 1
        tli = lbltli.Value
        tli = Mid(tli, 1, 1)
        tli = 5
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        clid = lblclid.Value
        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        sql = "usp_AddTask '" & tli & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & ustr & "'"
        gtasks.Scalar(sql)
        eqid = lbleqid.Value
        gtasks.UpMod(eqid)
        'sql = "insert into pmtasks " & AddVal & "'" & tskcnt & "')"
        'gtasks.Update(sql)
        BindGrid(se)
        'BindGrid(se)
        gtasks.Dispose()
        lbladdchk.Value = "yes"

    End Sub

    Private Sub lstfilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstfilter.SelectedIndexChanged
        Dim filtid As String
        filtid = lstfilter.SelectedValue
        cid = lblcid.Value
        gtasks.Open()
        Select Case filtid
            Case 1 'Task Types
                sql = "select ttid, tasktype " _
                + "from pmTaskTypes where compid = '" & cid & "'"
                dr = gtasks.GetRdrData(sql)
                lstdef.DataSource = dr
                lstdef.DataTextField = "tasktype"
                lstdef.DataValueField = "ttid"
                Try
                    lstdef.DataBind()
                Catch ex As Exception

                End Try

                dr.Close()
                lstdef.Items.Insert(0, "Select Task Type")
            Case 2 'Frequency
                sql = "select freqid, freq " _
                       + "from Frequencies where compid = '" & cid & "'"
                dr = gtasks.GetRdrData(sql)
                lstdef.DataSource = dr
                lstdef.DataTextField = "freq"
                lstdef.DataValueField = "freqid"
                Try
                    lstdef.DataBind()
                Catch ex As Exception

                End Try

                dr.Close()
                lstdef.Items.Insert(0, "Select Frequency")
            Case 3 ' RMT
                sql = "select rmtid, rmt " _
                        + "from RMT"
                dr = gtasks.GetRdrData(sql)
                lstdef.DataSource = dr
                lstdef.DataTextField = "rmt"
                lstdef.DataValueField = "rmtid"
                Try
                    lstdef.DataBind()
                Catch ex As Exception

                End Try

                dr.Close()
                lstdef.Items.Insert(0, "Select Maintenance Type")
            Case 4 'Eq Status
                sql = "select statid, status " _
                        + "from Status where compid = '" & cid & "'"
                dr = gtasks.GetRdrData(sql)
                lstdef.DataSource = dr
                lstdef.DataTextField = "status"
                lstdef.DataValueField = "statid"
                Try
                    lstdef.DataBind()
                Catch ex As Exception

                End Try

                dr.Close()
                lstdef.Items.Insert(0, "Select Status")
            Case 5 'Skill
                sql = "select skillid, skill " _
                         + "from Skills where compid = '" & cid & "'"
                dr = gtasks.GetRdrData(sql)
                lstdef.DataSource = dr
                lstdef.DataTextField = "skill"
                lstdef.DataValueField = "skillid"
                Try
                    lstdef.DataBind()
                Catch ex As Exception

                End Try

                dr.Close()
                lstdef.Items.Insert(0, "Select Skill")
            Case 6 'Skill
                lstdef.DataSource = ""
                Try
                    lstdef.DataBind()
                Catch ex As Exception

                End Try

                FiltFilt = ""
                lblfiltfilt.Value = FiltFilt
                se = lblcurrsort.Value
                gtasks.Open()
                BindGrid(se)
                gtasks.Dispose()
            Case Else
        End Select
        gtasks.Dispose()
    End Sub

    Private Sub lstdef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstdef.SelectedIndexChanged
        Dim filtid, defid As String
        filtid = lstfilter.SelectedValue
        se = lblcurrsort.Value
        Select Case filtid
            Case 1 'Task Types
                defid = lstdef.SelectedValue.ToString
                FiltFilt = " and ttid = '" & defid & "'"
                lblfiltfilt.Value = FiltFilt
                gtasks.Open()
                BindGrid(se)
                gtasks.Dispose()
            Case 2 'Frequency
                defid = lstdef.SelectedValue.ToString
                Dim defstr As String = lstdef.SelectedItem.ToString
                FiltFilt = " and freq = '" & defid & "'"
                lblfiltfilt.Value = FiltFilt
                gtasks.Open()
                BindGrid(se)
                gtasks.Dispose()
            Case 3 ' RMT
                defid = lstdef.SelectedValue.ToString
                FiltFilt = " and rmtid = '" & defid & "'"
                lblfiltfilt.Value = FiltFilt
                gtasks.Open()
                BindGrid(se)
                gtasks.Dispose()
            Case 4 'Eq Status
                defid = lstdef.SelectedValue.ToString
                FiltFilt = " and rdid = '" & defid & "'"
                lblfiltfilt.Value = FiltFilt
                gtasks.Open()
                BindGrid(se)
                gtasks.Dispose()
            Case 5 'Skill
                defid = lstdef.SelectedValue.ToString
                FiltFilt = " and skillid = '" & defid & "'"
                lblfiltfilt.Value = FiltFilt
                gtasks.Open()
                BindGrid(se)
                gtasks.Dispose()
            Case Else
        End Select
    End Sub

    Private Sub dgtasks_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.DeleteCommand
        Dim tnum, snum, pmtskid
        'ListItemType.SelectedItem()
        Try
            tnum = CType(e.Item.FindControl("lblta"), Label).Text
            'snum = CType(e.Item.FindControl("lblsubt"), Label).Text
            pmtskid = CType(e.Item.FindControl("lbltid"), Label).Text
        Catch ex As Exception
            tnum = CType(e.Item.FindControl("lblt"), TextBox).Text
            'snum = CType(e.Item.FindControl("lblst"), Label).Text
            pmtskid = CType(e.Item.FindControl("lblttid"), Label).Text
        End Try
        'If e.Item.ItemType = ListItemType.Item Then

        'ElseIf e.Item.ItemType = ListItemType.EditItem Then

        'End If
        Dim fuid, eqid As String

        fuid = lblfuid.Value
        sql = "usp_delpmtask '" & fuid & "', '" & pmtskid & "', '" & tnum & "'"
        gtasks.Open()
        gtasks.Update(sql)
        eqid = lbleqid.Value
        gtasks.UpMod(eqid)
        Try
            dgtasks.EditItemIndex = -1
        Catch ex As Exception

        End Try
        se = lblcurrsort.Value
        BindGrid(se)
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtasks.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim loto As String = DataBinder.Eval(e.Item.DataItem, "lotoid").ToString
            Dim cs As String = DataBinder.Eval(e.Item.DataItem, "conid").ToString
            Dim lblloto As Label = CType(e.Item.FindControl("lblloto"), Label)
            Dim lblcs As Label = CType(e.Item.FindControl("lblcs"), Label)
            Dim lotostr, csstr As String
            If loto = "1" Then
                lotostr = "YES"
            Else
                lotostr = "NO"
            End If
            If cs = "1" Then
                csstr = "YES"
            Else
                csstr = "NO"
            End If
            lblloto.Text = lotostr
            lblcs.Text = csstr
        End If

        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            Dim deleteButton As ImageButton = CType(e.Item.FindControl("ibDel"), ImageButton)
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete Task #" & _
            DataBinder.Eval(e.Item.DataItem, "tasknum") & " - Sub Task #" & _
            DataBinder.Eval(e.Item.DataItem, "subtask") & "?')"

            If e.Item.ItemType = ListItemType.EditItem Then
                Dim pmtskid As String = DataBinder.Eval(e.Item.DataItem, "pmtskid").ToString
                Dim sid As String = DataBinder.Eval(e.Item.DataItem, "siteid").ToString
                Dim comid As String = DataBinder.Eval(e.Item.DataItem, "comid").ToString
                Dim cid As String = DataBinder.Eval(e.Item.DataItem, "compid").ToString
                Dim eqid As String = DataBinder.Eval(e.Item.DataItem, "eqid").ToString
                Dim comp As String = DataBinder.Eval(e.Item.DataItem, "compnum").ToString
                Dim ttid As String = DataBinder.Eval(e.Item.DataItem, "ttid").ToString
                Dim taskdesc As String = DataBinder.Eval(e.Item.DataItem, "taskdesc").ToString
                Dim qty As String = DataBinder.Eval(e.Item.DataItem, "qty").ToString
                Dim ttime As String = DataBinder.Eval(e.Item.DataItem, "ttime").ToString

                lblcurrcomid.Value = comid
                lblcurrcomdesc.Value = comp
                Dim lblfreq As TextBox = CType(e.Item.FindControl("txtfreq"), TextBox)
                Dim freqid As String
                freqid = lblfreq.ClientID.ToString
                Dim imgfreq As HtmlImage = CType(e.Item.FindControl("imgfreq"), HtmlImage)
                'imgfreq.Attributes.Add("onclick", "getfreq('" & freqid & "','" & pmtskid & "','" & sid & "','freq');")
                Dim skillqty As String = DataBinder.Eval(e.Item.DataItem, "qty").ToString
                Dim skill As String = DataBinder.Eval(e.Item.DataItem, "skill").ToString
                Dim skillid1 As String = DataBinder.Eval(e.Item.DataItem, "skillid").ToString
                Dim rdid1 As String = DataBinder.Eval(e.Item.DataItem, "rdid").ToString
                Dim rd As String = DataBinder.Eval(e.Item.DataItem, "rd").ToString
                Dim meterid As String = DataBinder.Eval(e.Item.DataItem, "meterid").ToString
                'freqid, eqid, pmtskid, fuid, skillid, skillqty, rdid, skill, rd, meterid
                imgfreq.Attributes.Add("onclick", "getmeter('" & freqid & "','" & eqid & "','" & pmtskid & "','" & fuid & "','" & skillid1 & "','" & skillqty & "','" & rdid1 & "','" & skill & "','" & rd & "','" & meterid & "');")

                Dim funcid As String = DataBinder.Eval(e.Item.DataItem, "funcid").ToString
                Dim lblfm As Label = CType(e.Item.FindControl("lblfm1"), Label)
                Dim fmid As String = lblfm.ClientID.ToString

                lbltaskdesc.Value = taskdesc
                Dim txtdesce As TextBox = CType(e.Item.FindControl("txtdesc"), TextBox)
                txtdesce.Attributes.Add("onchange", "document.getElementById('lbltaskdesc').value=this.value;")

                lblqty.Value = qty
                Dim txtqtye As TextBox = CType(e.Item.FindControl("txtqty"), TextBox)
                txtqtye.Attributes.Add("onchange", "document.getElementById('lblqty').value=this.value;")

                lbltime.Value = ttime
                Dim txttre As TextBox = CType(e.Item.FindControl("txttr"), TextBox)
                txttre.Attributes.Add("onchange", "document.getElementById('lbltime').value=this.value;")

                Dim lblcompnum As Label = CType(e.Item.FindControl("lblcompnum"), Label)
                Dim compid As String
                compid = lblcompnum.ClientID.ToString
                Dim imgcompnum As HtmlImage = CType(e.Item.FindControl("imgcompnum"), HtmlImage)
                imgcompnum.Attributes.Add("onclick", "getcomp('" & compid & "','" & pmtskid & "','" & funcid & "','comp','" & fmid & "');")

                Dim imgfm As HtmlImage = CType(e.Item.FindControl("imgfm"), HtmlImage)
                imgfm.Attributes.Add("onclick", "getfm('" & fmid & "','" & cid & "','" & eqid & "','" & funcid & "','" & comid & "','" & comp & "','" & pmtskid & "');")

                Dim lblskill As Label = CType(e.Item.FindControl("lblskill"), Label)
                Dim skillid As String
                skillid = lblskill.ClientID.ToString
                Dim imgskill As HtmlImage = CType(e.Item.FindControl("imgskill"), HtmlImage)
                imgskill.Attributes.Add("onclick", "getskill('" & skillid & "','" & pmtskid & "','" & sid & "','skill');")

                Dim lblpretech As Label = CType(e.Item.FindControl("lblpretech"), Label)
                Dim ptid As String
                ptid = lblpretech.ClientID.ToString
                Dim imgpretech As HtmlImage = CType(e.Item.FindControl("imgpretech"), HtmlImage)
                If ttid = 7 Then
                    imgpretech.Attributes.Add("onclick", "getpretech('" & ptid & "','" & pmtskid & "','" & sid & "','pretech');")
                Else
                    imgpretech.Attributes.Add("onclick", "getpretech('" & ptid & "','" & pmtskid & "','" & sid & "','pretech');")
                    imgpretech.Attributes.Add("class", "details")
                End If

                Dim imgptid As String
                imgptid = imgpretech.ClientID.ToString

                Dim lbltasktype As Label = CType(e.Item.FindControl("lbltasktype"), Label)
                Dim tasktypeid As String
                tasktypeid = lbltasktype.ClientID.ToString
                Dim imgtasktype As HtmlImage = CType(e.Item.FindControl("imgtasktype"), HtmlImage)
                imgtasktype.Attributes.Add("onclick", "gettasktype('" & tasktypeid & "','" & pmtskid & "','" & sid & "','tasktype','" & imgptid & "','" & ptid & "');")

                Dim lblrd As Label = CType(e.Item.FindControl("lblrd"), Label)
                Dim rdid As String
                rdid = lblrd.ClientID.ToString
                Dim imgrd As HtmlImage = CType(e.Item.FindControl("imgrd"), HtmlImage)
                imgrd.Attributes.Add("onclick", "getrd('" & rdid & "','" & pmtskid & "','" & sid & "','rd');")

            End If
        End If

    End Sub

    Private Sub dgtasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.UpdateCommand
        gtasks.Open()
        Dim otn, tn, ts, fre, tfre, ski, tski, sta, tsta, lot, tlot, con, tcon, tas, ttas, mai, tmai, pre, tpre, tskd, fuid As String
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        sid = lblsid.Value
        otn = lbloldtask.Value




        Try
            lot = CType(e.Item.FindControl("ddloto"), DropDownList).SelectedIndex
        Catch ex As Exception
            lot = "0"
        End Try
        Try
            tlot = CType(e.Item.FindControl("ddloto"), DropDownList).SelectedItem.Text
        Catch ex As Exception
            tlot = "Select"
        End Try
        Try
            con = CType(e.Item.FindControl("ddcs"), DropDownList).SelectedIndex
        Catch ex As Exception
            con = "0"
        End Try
        Try
            tcon = CType(e.Item.FindControl("ddcs"), DropDownList).SelectedItem.Text
        Catch ex As Exception
            tcon = "Select"
        End Try

       

        Dim txt, qty As String
        qty = CType(e.Item.FindControl("txtqty"), TextBox).Text
        qty = lblqty.Value
        txt = CType(e.Item.FindControl("txttr"), TextBox).Text
        txt = lbltime.Value
        If qty = "" Then
            qty = "0"
        End If
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToInt64(qty)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr214" , "GTasksFunc2.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If txt = "" Then
            txt = "0"
        End If
        Dim txtchk As Long
        Try
            txtchk = System.Convert.ToDecimal(txt)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr215" , "GTasksFunc2.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        tskd = CType(e.Item.FindControl("txtdesc"), TextBox).Text
        tskd = lbltaskdesc.Value
        tskd = gtasks.ModString2(tskd)
        tn = CType(e.Item.FindControl("lblt"), TextBox).Text
        Dim tnchk As Long
        Try

            tnchk = System.Convert.ToInt64(tn)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr216" , "GTasksFunc2.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If tn = 0 Or Len(tn) = 0 Then
            tn = otn
        End If
        If tn < 1 Then
            tn = 1
        End If
        Try
            Dim tcnt As Integer = lbltcnt.Value
            If tn > tcnt Then
                tn = tcnt
            End If
        Catch ex As Exception

        End Try


        ts = CType(e.Item.FindControl("lblst"), Label).Text
        Filter = lblfilt.Value
        Dim ttid As String
        ttid = CType(e.Item.FindControl("lblttid"), Label).Text

        Dim freq As String
        freq = CType(e.Item.FindControl("txtfreq"), TextBox).Text
        Dim freqchk As Long
        Try
            freqchk = System.Convert.ToInt64(freq)
            sql = "update pmtasks set freq = '" & freqchk & "' where pmtskid = '" & ttid & "'"
            gtasks.Update(sql)
        Catch ex As Exception
            Dim strMessage As String = "Frequency Must Be A Numeric Value"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try


        Dim fm1, fm1s, fm2, fm2s, fm3, fm3s, fm4, fm4s, fm5, fm5s As String
        Dim ofm1, ofm2, ofm3, ofm4, ofm5 As String
        '+ "comid = '" & ci & "', " _
        '+ "compindex = '" & cin & "', " _
        '+ "compnum = '" & cn & "', " _

        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)

        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_updatepmtasksg2 @pmtskid, @des, @qty, @ttime, " _
        + "@lotoid, @conid, @filter, " _
        + "@pagenumber, @fuid, " _
        + "@ustr, @eqid, @sid"

        Dim param = New SqlParameter("@pmtskid", SqlDbType.Int)
        param.Value = ttid
        cmd.Parameters.Add(param)
        Dim param01 = New SqlParameter("@des", SqlDbType.VarChar)
        If tskd = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = tskd
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@qty", SqlDbType.Int)
        If qty = "" Then
            param02.Value = "1"
        Else
            param02.Value = qty
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@ttime", SqlDbType.Decimal)
        If txt = "" Then
            param03.Value = "0"
        Else
            param03.Value = txt
        End If
        cmd.Parameters.Add(param03)

        Dim param08 = New SqlParameter("@lotoid", SqlDbType.Int)
        If lot = "" Then
            param08.Value = "0"
        Else
            param08.Value = lot
        End If
        cmd.Parameters.Add(param08)
        Dim param09 = New SqlParameter("@conid", SqlDbType.Int)
        If con = "" Then
            param09.Value = "0"
        Else
            param09.Value = con
        End If
        cmd.Parameters.Add(param09)

        Dim param18 = New SqlParameter("@filter", SqlDbType.VarChar)
        If Filter = "" Then
            param18.Value = System.DBNull.Value
        Else
            param18.Value = Filter
        End If
        cmd.Parameters.Add(param18)

        Dim param19 = New SqlParameter("@pagenumber", SqlDbType.Int)
        param19.Value = otn
        cmd.Parameters.Add(param19)

        Dim param20 = New SqlParameter("@fuid", SqlDbType.Int)
        If fuid = "" Then
            param20.Value = "0"
        Else
            param20.Value = fuid
        End If
        cmd.Parameters.Add(param20)

        Dim param48 = New SqlParameter("@ustr", SqlDbType.VarChar)
        If ustr = "" Then
            param48.Value = System.DBNull.Value
        Else
            param48.Value = ustr
        End If
        cmd.Parameters.Add(param48)
        Dim param49 = New SqlParameter("@eqid", SqlDbType.Int)
        param49.Value = eqid
        cmd.Parameters.Add(param49)
        Dim param50 = New SqlParameter("@sid", SqlDbType.Int)
        param50.Value = sid
        cmd.Parameters.Add(param50)

        Dim tpm As Integer
        Try
            gtasks.UpdateHack(cmd)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr217" , "GTasksFunc2.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        lbltaskdesc.Value = ""
        lblqty.Value = ""
        lbltime.Value = ""
        ' Check for task reorder
        If otn <> tn Then
            fuid = lblfuid.Value
            sql = "usp_reorderPMTasks '" & ttid & "', '" & fuid & "', '" & tn & "', '" & otn & "'"
            gtasks.Update(sql)
          
           
        End If
        'sql = "usp_UpdateFM1 '" & ttid & "'"
        'gtasks.Update(sql)
        eqid = lbleqid.Value
        gtasks.UpMod(eqid)
        dgtasks.EditItemIndex = -1
        se = lblcurrsort.Value
        BindGrid(se)
        gtasks.Dispose()
        'Catch ex As Exception
        'Dim strMessage As String =  tmod.getmsg("cdstr218" , "GTasksFunc2.aspx.vb")
 
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'End Try

    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgtasks.Columns(0).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "0")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(1).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "1")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(3).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "3")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(4).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "4")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(5).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "5")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(6).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "6")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(7).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "7")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(8).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "8")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(9).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "9")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(10).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "10")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(11).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "11")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(12).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "12")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(13).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "13")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(15).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "15")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(16).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "16")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(17).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "17")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(18).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "18")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(19).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "19")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(22).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "22")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(23).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "23")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(24).HeaderText = dlabs.GetDGPage("GTasksFunc2.aspx", "dgtasks", "24")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang233.Text = axlabs.GetASPXPage("GTasksFunc2.aspx", "lang233")
        Catch ex As Exception
        End Try
        Try
            lang234.Text = axlabs.GetASPXPage("GTasksFunc2.aspx", "lang234")
        Catch ex As Exception
        End Try
        Try
            lang235.Text = axlabs.GetASPXPage("GTasksFunc2.aspx", "lang235")
        Catch ex As Exception
        End Try
        Try
            lang236.Text = axlabs.GetASPXPage("GTasksFunc2.aspx", "lang236")
        Catch ex As Exception
        End Try
        Try
            lang237.Text = axlabs.GetASPXPage("GTasksFunc2.aspx", "lang237")
        Catch ex As Exception
        End Try
        Try
            lang238.Text = axlabs.GetASPXPage("GTasksFunc2.aspx", "lang238")
        Catch ex As Exception
        End Try
        Try
            lang239.Text = axlabs.GetASPXPage("GTasksFunc2.aspx", "lang239")
        Catch ex As Exception
        End Try
        Try
            lang240.Text = axlabs.GetASPXPage("GTasksFunc2.aspx", "lang240")
        Catch ex As Exception
        End Try
        Try
            lblsort.Text = axlabs.GetASPXPage("GTasksFunc2.aspx", "lblsort")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                addtask.Attributes.Add("src", "../images2/eng/bgbuttons/addtask.gif")
            ElseIf lang = "fre" Then
                addtask.Attributes.Add("src", "../images2/fre/bgbuttons/addtask.gif")
            ElseIf lang = "ger" Then
                addtask.Attributes.Add("src", "../images2/ger/bgbuttons/addtask.gif")
            ElseIf lang = "ita" Then
                addtask.Attributes.Add("src", "../images2/ita/bgbuttons/addtask.gif")
            ElseIf lang = "spa" Then
                addtask.Attributes.Add("src", "../images2/spa/bgbuttons/addtask.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btnexport.Attributes.Add("src", "../images2/eng/bgbuttons/export.gif")
            ElseIf lang = "fre" Then
                btnexport.Attributes.Add("src", "../images2/fre/bgbuttons/export.gif")
            ElseIf lang = "ger" Then
                btnexport.Attributes.Add("src", "../images2/ger/bgbuttons/export.gif")
            ElseIf lang = "ita" Then
                btnexport.Attributes.Add("src", "../images2/ita/bgbuttons/export.gif")
            ElseIf lang = "spa" Then
                btnexport.Attributes.Add("src", "../images2/spa/bgbuttons/export.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
