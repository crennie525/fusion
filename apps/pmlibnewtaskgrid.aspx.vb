

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmlibnewtaskgrid
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Filter, AddVal, SubVal, FiltFilt, login, app, se, eqid, sid, tli, fuid, cid, clid, did As String
    Dim funid As String
    Dim sql As String
    Dim ds As DataSet
    Dim dr As SqlDataReader
    Dim gtasks As New Utilities
    Dim bgtasks As New Utilities
    Dim bgtasks2 As New Utilities
    Dim dslev As DataSet
    Dim dshd As DataSet
    Dim appc As New AppUtils
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltool As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllube As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnote As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcomdesc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xcoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ycoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents addtask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings.Item("custAppUrl")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgtasks As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetDGLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            funid = Request.QueryString("funid").ToString '"3580" '
            lblfuid.Value = funid
            Filter = "funcid = '" & funid & "' and subtask = 0"
            lblfilt.Value = Filter
            gtasks.Open()
            GetStuff(funid)
            lblcurrsort.Value = "tasknum, subtask asc"
            BindGrid("tasknum, subtask asc")
            gtasks.Dispose()

        End If
    End Sub
    Private Sub GetStuff(ByVal funid As String)
        sql = "select f.eqid, e.siteid, e.cellid, e.dept_id, e.compid from functions f " _
        + "left join equipment e on e.eqid = f.eqid " _
        + "where f.func_id = '" & funid & "'"
        dr = gtasks.GetRdrData(sql)
        While dr.Read
            lbleqid.Value = dr.Item("eqid").ToString
            lblsid.Value = dr.Item("siteid").ToString
            lbldid.Value = dr.Item("dept_id").ToString
            lblcid.Value = dr.Item("compid").ToString
            lblclid.Value = dr.Item("cellid").ToString
        End While
        dr.Close()

    End Sub
    Private Sub BindGrid(ByVal sortExp As String)
        Try
            'gtasks.Open()
        Catch ex As Exception

        End Try
        Filter = lblfilt.Value
        FiltFilt = lblfiltfilt.Value
        sql = "select count(*) from pmtasks where " & Filter & FiltFilt
        Try
            Dim tcnt As Integer = gtasks.Scalar(sql)
            lbltcnt.Value = tcnt
        Catch ex As Exception

        End Try
        funid = lblfuid.Value
        sql = "update pmtasks set lotoid = 0 where lotoid is null " _
        + "update pmtasks set conid = 0 where conid is null"
        gtasks.Update(sql)
        sql = "select * from pmtasks where " & Filter & FiltFilt & " order by " & sortExp
        ds = gtasks.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Try
            dgtasks.DataSource = dv
            dgtasks.DataBind()
        Catch ex As Exception

        End Try
        'gtasks.Dispose()
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer

        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function

    Private Sub dgtasks_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.ItemCommand
        Dim vc As String = e.CommandName
        Select Case vc
            Case "hd"
                dgtasks.Columns(4).Visible = False
            Case "sd"
                dgtasks.Columns(4).Visible = True

            Case "htt"
                dgtasks.Columns(6).Visible = False
            Case "stt"
                dgtasks.Columns(6).Visible = True
            Case "hfr"
                dgtasks.Columns(8).Visible = False
            Case "sfr"
                dgtasks.Columns(8).Visible = True

            Case "hcomp"
                dgtasks.Columns(10).Visible = False
                dgtasks.Columns(11).Visible = False
            Case "scomp"
                dgtasks.Columns(10).Visible = True
                dgtasks.Columns(11).Visible = True

            Case "hsk"
                dgtasks.Columns(13).Visible = False
                dgtasks.Columns(14).Visible = False
                dgtasks.Columns(15).Visible = False
            Case "ssk"
                dgtasks.Columns(13).Visible = True
                dgtasks.Columns(14).Visible = True
                dgtasks.Columns(15).Visible = True

            Case "hpt"
                dgtasks.Columns(17).Visible = False
            Case "spt"
                dgtasks.Columns(17).Visible = True

            Case "hes"
                dgtasks.Columns(19).Visible = False
            Case "ses"
                dgtasks.Columns(19).Visible = True


            Case "hlo"
                dgtasks.Columns(21).Visible = False
            Case "slo"
                dgtasks.Columns(21).Visible = True
            Case "hco"
                dgtasks.Columns(23).Visible = False
            Case "sco"
                dgtasks.Columns(23).Visible = True
        End Select


        If e.CommandName = "Tool" Then
            lbltool.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If

        End If
        If e.CommandName = "Part" Then
            lblpart.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        If e.CommandName = "Lube" Then
            lbllube.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        If e.CommandName = "Note" Then
            lblnote.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If

    End Sub

    Private Sub dgtasks_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgtasks.SortCommand
        gtasks.Open()
        BindGrid(e.SortExpression)
        gtasks.Dispose()
        Dim se As String = e.SortExpression
        lblcurrsort.Value = se
        Select Case se
            Case "tasknum desc"
                dgtasks.Columns(1).SortExpression = "tasknum asc"
                lblsort.Value = "Task# Descending"
            Case "tasknum asc"
                dgtasks.Columns(1).SortExpression = "tasknum desc"
                lblsort.Value = "Task# Ascending"
            Case "subtask desc"
                dgtasks.Columns(2).SortExpression = "subtask asc"
                lblsort.Value = "Sub Task# Descending"
            Case "subtask asc"
                dgtasks.Columns(2).SortExpression = "subtask desc"
                lblsort.Value = "Sub Task# Ascending"
            Case "tasktype desc"
                dgtasks.Columns(6).SortExpression = "tasktype asc"
                lblsort.Value = "Task Type Descending"
            Case "tasktype asc"
                dgtasks.Columns(6).SortExpression = "tasktype desc"
                lblsort.Value = "Task Type Ascending"
            Case "freq desc"
                dgtasks.Columns(8).SortExpression = "freq asc"
                lblsort.Value = "Frequency Descending"
            Case "freq asc"
                dgtasks.Columns(8).SortExpression = "freq desc"
                lblsort.Value = "Frequency Ascending"

            Case "comp desc"
                dgtasks.Columns(10).SortExpression = "comp asc"
                dgtasks.Columns(11).SortExpression = "comp asc"
                lblsort.Value = "Component Descending"
            Case "comp asc"
                dgtasks.Columns(10).SortExpression = "comp desc"
                dgtasks.Columns(11).SortExpression = "comp desc"
                lblsort.Value = "Component Ascending"

            Case "skill desc"
                dgtasks.Columns(13).SortExpression = "skill asc"
                lblsort.Value = " Skill Required Descending"
            Case "skill asc"
                dgtasks.Columns(13).SortExpression = "skill desc"
                lblsort.Value = "Skill Required Ascending"

            Case "pretech desc"
                dgtasks.Columns(17).SortExpression = "pretech asc"
                lblsort.Value = " Predictive Technology Descending"
            Case "pretech asc"
                dgtasks.Columns(17).SortExpression = "pretech desc"
                lblsort.Value = "Predictive Technology Ascending"
            Case "rd desc"
                dgtasks.Columns(19).SortExpression = "rd asc"
                lblsort.Value = "PM Equipment Status Descending"
            Case "rd asc"
                dgtasks.Columns(19).SortExpression = "rd desc"
                lblsort.Value = "PM Equipment Status Ascending"

            Case "loto desc"
                dgtasks.Columns(21).SortExpression = "loto asc"
                lblsort.Value = " LOTO Descending"
            Case "loto asc"
                dgtasks.Columns(21).SortExpression = "loto desc"
                lblsort.Value = "LOTO Ascending"
            Case "confined desc"
                dgtasks.Columns(23).SortExpression = "confined asc"
                lblsort.Value = " Confined Space Descending"
            Case "confined asc"
                dgtasks.Columns(23).SortExpression = "confined desc"
                lblsort.Value = "Confined Space Ascending"
        End Select
    End Sub

    Private Sub dgtasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.EditCommand
        se = lblcurrsort.Value
        lbloldtask.Value = CType(e.Item.FindControl("lblta"), Label).Text
        dgtasks.EditItemIndex = e.Item.ItemIndex
        gtasks.Open()
        BindGrid(se)
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtasks.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            Dim deleteButton As ImageButton = CType(e.Item.FindControl("ibDel"), ImageButton)
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete Task #" & _
            DataBinder.Eval(e.Item.DataItem, "tasknum") & " - Sub Task #" & _
            DataBinder.Eval(e.Item.DataItem, "subtask") & "?')"

            If e.Item.ItemType = ListItemType.EditItem Then
                Dim pmtskid As String = DataBinder.Eval(e.Item.DataItem, "pmtskid").ToString
                Dim sid As String = DataBinder.Eval(e.Item.DataItem, "siteid").ToString
                Dim comid As String = DataBinder.Eval(e.Item.DataItem, "comid").ToString
                Dim cid As String = DataBinder.Eval(e.Item.DataItem, "compid").ToString
                Dim eqid As String = DataBinder.Eval(e.Item.DataItem, "eqid").ToString
                Dim comp As String = DataBinder.Eval(e.Item.DataItem, "compnum").ToString
                Dim ttid As String = DataBinder.Eval(e.Item.DataItem, "ttid").ToString
                lblcurrcomid.Value = comid
                lblcurrcomdesc.Value = comp
                Dim lblfreq As Label = CType(e.Item.FindControl("lblfreq"), Label)
                Dim freqid As String
                freqid = lblfreq.ClientID.ToString
                Dim imgfreq As HtmlImage = CType(e.Item.FindControl("imgfreq"), HtmlImage)
                imgfreq.Attributes.Add("onclick", "getfreq('" & freqid & "','" & pmtskid & "','" & sid & "','freq');")

                Dim funcid As String = DataBinder.Eval(e.Item.DataItem, "funcid").ToString
                Dim lblfm As Label = CType(e.Item.FindControl("lblfm1"), Label)
                Dim fmid As String = lblfm.ClientID.ToString

                Dim lblcompnum As Label = CType(e.Item.FindControl("lblcompnum"), Label)
                Dim compid As String
                compid = lblcompnum.ClientID.ToString
                Dim imgcompnum As HtmlImage = CType(e.Item.FindControl("imgcompnum"), HtmlImage)
                imgcompnum.Attributes.Add("onclick", "getcomp('" & compid & "','" & pmtskid & "','" & funcid & "','comp','" & fmid & "');")

                Dim imgfm As HtmlImage = CType(e.Item.FindControl("imgfm"), HtmlImage)
                imgfm.Attributes.Add("onclick", "getfm('" & fmid & "','" & cid & "','" & eqid & "','" & funcid & "','" & comid & "','" & comp & "','" & pmtskid & "');")

                Dim lblskill As Label = CType(e.Item.FindControl("lblskill"), Label)
                Dim skillid As String
                skillid = lblskill.ClientID.ToString
                Dim imgskill As HtmlImage = CType(e.Item.FindControl("imgskill"), HtmlImage)
                imgskill.Attributes.Add("onclick", "getskill('" & skillid & "','" & pmtskid & "','" & sid & "','skill');")

                Dim lblpretech As Label = CType(e.Item.FindControl("lblpretech"), Label)
                Dim ptid As String
                ptid = lblpretech.ClientID.ToString
                Dim imgpretech As HtmlImage = CType(e.Item.FindControl("imgpretech"), HtmlImage)
                If ttid = 7 Then
                    imgpretech.Attributes.Add("onclick", "getpretech('" & ptid & "','" & pmtskid & "','" & sid & "','pretech');")
                Else
                    imgpretech.Attributes.Add("onclick", "getpretech('" & ptid & "','" & pmtskid & "','" & sid & "','pretech');")
                    imgpretech.Attributes.Add("class", "details")
                End If

                Dim imgptid As String
                imgptid = imgpretech.ClientID.ToString

                Dim lbltasktype As Label = CType(e.Item.FindControl("lbltasktype"), Label)
                Dim tasktypeid As String
                tasktypeid = lbltasktype.ClientID.ToString
                Dim imgtasktype As HtmlImage = CType(e.Item.FindControl("imgtasktype"), HtmlImage)
                imgtasktype.Attributes.Add("onclick", "gettasktype('" & tasktypeid & "','" & pmtskid & "','" & sid & "','tasktype','" & imgptid & "','" & ptid & "');")

                Dim lblrd As Label = CType(e.Item.FindControl("lblrd"), Label)
                Dim rdid As String
                rdid = lblrd.ClientID.ToString
                Dim imgrd As HtmlImage = CType(e.Item.FindControl("imgrd"), HtmlImage)
                imgrd.Attributes.Add("onclick", "getrd('" & rdid & "','" & pmtskid & "','" & sid & "','rd');")

            End If
        End If

    End Sub

    Private Sub dgtasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.CancelCommand
        dgtasks.EditItemIndex = -1
        se = lblcurrsort.Value
        gtasks.Open()
        BindGrid(se)
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.DeleteCommand
        Dim tnum, snum, pmtskid
        'ListItemType.SelectedItem()
        Try
            tnum = CType(e.Item.FindControl("lblta"), Label).Text
            'snum = CType(e.Item.FindControl("lblsubt"), Label).Text
            pmtskid = CType(e.Item.FindControl("lbltid"), Label).Text
        Catch ex As Exception
            tnum = CType(e.Item.FindControl("lblt"), TextBox).Text
            'snum = CType(e.Item.FindControl("lblst"), Label).Text
            pmtskid = CType(e.Item.FindControl("lblttid"), Label).Text
        End Try
        'If e.Item.ItemType = ListItemType.Item Then

        'ElseIf e.Item.ItemType = ListItemType.EditItem Then

        'End If
        Dim fuid, eqid As String

        fuid = lblfuid.Value
        sql = "usp_delpmtask '" & fuid & "', '" & pmtskid & "', '" & tnum & "'"
        gtasks.Open()
        gtasks.Update(sql)
        eqid = lbleqid.Value
        gtasks.UpMod(eqid)
        Try
            dgtasks.EditItemIndex = -1
        Catch ex As Exception

        End Try
        se = lblcurrsort.Value
        BindGrid(se)
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.UpdateCommand
        gtasks.Open()
        Dim otn, tn, ts, fre, tfre, ski, tski, sta, tsta, lot, tlot, con, tcon, tas, ttas, mai, tmai, pre, tpre, tskd, fuid As String
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        sid = lblsid.Value
        otn = lbloldtask.Value




        Try
            lot = CType(e.Item.FindControl("ddloto"), DropDownList).SelectedIndex
        Catch ex As Exception
            lot = "0"
        End Try
        Try
            tlot = CType(e.Item.FindControl("ddloto"), DropDownList).SelectedItem.Text
        Catch ex As Exception
            tlot = "Select"
        End Try
        Try
            con = CType(e.Item.FindControl("ddcs"), DropDownList).SelectedIndex
        Catch ex As Exception
            con = "0"
        End Try
        Try
            tcon = CType(e.Item.FindControl("ddcs"), DropDownList).SelectedItem.Text
        Catch ex As Exception
            tcon = "Select"
        End Try


        Dim txt, qty As String
        qty = CType(e.Item.FindControl("txtqty"), TextBox).Text
        txt = CType(e.Item.FindControl("txttr"), TextBox).Text
        If qty = "" Then
            qty = "0"
        End If
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToInt64(qty)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr226" , "pmlibnewtaskgrid.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If txt = "" Then
            txt = "0"
        End If
        Dim txtchk As Long
        Try
            txtchk = System.Convert.ToDecimal(txt)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr227" , "pmlibnewtaskgrid.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        tskd = CType(e.Item.FindControl("txtdesc"), TextBox).Text
        tskd = gtasks.ModString2(tskd)
        tn = CType(e.Item.FindControl("lblt"), TextBox).Text
        Dim tnchk As Long
        Try

            tnchk = System.Convert.ToInt64(tn)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr228" , "pmlibnewtaskgrid.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If tn = 0 Or Len(tn) = 0 Then
            tn = otn
        End If
        If tn < 1 Then
            tn = 1
        End If
        Try
            Dim tcnt As Integer = lbltcnt.Value
            If tn > tcnt Then
                tn = tcnt
            End If
        Catch ex As Exception

        End Try


        ts = CType(e.Item.FindControl("lblst"), Label).Text
        Filter = lblfilt.Value
        Dim ttid As String
        ttid = CType(e.Item.FindControl("lblttid"), Label).Text

        Dim fm1, fm1s, fm2, fm2s, fm3, fm3s, fm4, fm4s, fm5, fm5s As String
        Dim ofm1, ofm2, ofm3, ofm4, ofm5 As String
        '+ "comid = '" & ci & "', " _
        '+ "compindex = '" & cin & "', " _
        '+ "compnum = '" & cn & "', " _

        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)

        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_updatepmtasksg2 @pmtskid, @des, @qty, @ttime, " _
        + "@lotoid, @conid, @filter, " _
        + "@pagenumber, @fuid, " _
        + "@ustr, @eqid, @sid"

        Dim param = New SqlParameter("@pmtskid", SqlDbType.Int)
        param.Value = ttid
        cmd.Parameters.Add(param)
        Dim param01 = New SqlParameter("@des", SqlDbType.VarChar)
        If tskd = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = tskd
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@qty", SqlDbType.Int)
        If qty = "" Then
            param02.Value = "1"
        Else
            param02.Value = qty
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@ttime", SqlDbType.Decimal)
        If txt = "" Then
            param03.Value = "0"
        Else
            param03.Value = txt
        End If
        cmd.Parameters.Add(param03)

        Dim param08 = New SqlParameter("@lotoid", SqlDbType.Int)
        If lot = "" Then
            param08.Value = "0"
        Else
            param08.Value = lot
        End If
        cmd.Parameters.Add(param08)
        Dim param09 = New SqlParameter("@conid", SqlDbType.Int)
        If con = "" Then
            param09.Value = "0"
        Else
            param09.Value = con
        End If
        cmd.Parameters.Add(param09)

        Dim param18 = New SqlParameter("@filter", SqlDbType.VarChar)
        If Filter = "" Then
            param18.Value = System.DBNull.Value
        Else
            param18.Value = Filter
        End If
        cmd.Parameters.Add(param18)

        Dim param19 = New SqlParameter("@pagenumber", SqlDbType.Int)
        param19.Value = otn
        cmd.Parameters.Add(param19)

        Dim param20 = New SqlParameter("@fuid", SqlDbType.Int)
        If fuid = "" Then
            param20.Value = "0"
        Else
            param20.Value = fuid
        End If
        cmd.Parameters.Add(param20)

        Dim param48 = New SqlParameter("@ustr", SqlDbType.VarChar)
        If ustr = "" Then
            param48.Value = System.DBNull.Value
        Else
            param48.Value = ustr
        End If
        cmd.Parameters.Add(param48)
        Dim param49 = New SqlParameter("@eqid", SqlDbType.Int)
        param49.Value = eqid
        cmd.Parameters.Add(param49)
        Dim param50 = New SqlParameter("@sid", SqlDbType.Int)
        param50.Value = sid
        cmd.Parameters.Add(param50)

        Dim tpm As Integer
        Try
            gtasks.UpdateHack(cmd)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr229" , "pmlibnewtaskgrid.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try


        ' Check for task reorder
        If otn <> tn Then
            fuid = lblfuid.Value
            sql = "usp_reorderPMTasks '" & ttid & "', '" & fuid & "', '" & tn & "', '" & otn & "'"
            gtasks.Update(sql)
            
            
        End If
        'sql = "usp_UpdateFM1 '" & ttid & "'"
        'gtasks.Update(sql)
        eqid = lbleqid.Value
        gtasks.UpMod(eqid)
        dgtasks.EditItemIndex = -1
        se = lblcurrsort.Value
        BindGrid(se)
        gtasks.Dispose()
        'Catch ex As Exception
        'Dim strMessage As String =  tmod.getmsg("cdstr230" , "pmlibnewtaskgrid.aspx.vb")
 
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'End Try

    End Sub

    Private Sub addtask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles addtask.Click
        se = lblcurrsort.Value
        Dim tskcnt As Integer
        Filter = lblfilt.Value
        Dim subfilt As String
        subfilt = " and subtask = '0'"
        gtasks.Open()
        sql = "select count(*) from pmtasks where " & Filter & subfilt
        tskcnt = gtasks.Scalar(sql)
        tskcnt = tskcnt + 1

        tli = 5
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        clid = lblclid.Value
        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        sql = "usp_AddTask '" & tli & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & ustr & "'"
        gtasks.Scalar(sql)
        BindGrid(se)
        gtasks.Dispose()
    End Sub
	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgtasks.Columns(0).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","0")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(1).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","1")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(3).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","3")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(4).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","4")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(5).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","5")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(6).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","6")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(7).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","7")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(8).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","8")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(9).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","9")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(10).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","10")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(11).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","11")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(12).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","12")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(13).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","13")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(15).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","15")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(16).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","16")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(17).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","17")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(18).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","18")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(19).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","19")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(22).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","22")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(23).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","23")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(24).HeaderText = dlabs.GetDGPage("pmlibnewtaskgrid.aspx","dgtasks","24")
		Catch ex As Exception
		End Try

	End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                addtask.Attributes.Add("src", "../images2/eng/bgbuttons/addtask.gif")
            ElseIf lang = "fre" Then
                addtask.Attributes.Add("src", "../images2/fre/bgbuttons/addtask.gif")
            ElseIf lang = "ger" Then
                addtask.Attributes.Add("src", "../images2/ger/bgbuttons/addtask.gif")
            ElseIf lang = "ita" Then
                addtask.Attributes.Add("src", "../images2/ita/bgbuttons/addtask.gif")
            ElseIf lang = "spa" Then
                addtask.Attributes.Add("src", "../images2/spa/bgbuttons/addtask.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
