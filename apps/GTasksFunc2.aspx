<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GTasksFunc2.aspx.vb" Inherits="lucy_r12.GTasksFunc2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>GTasksFunc2</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="javascript" type="text/javascript" src="../scripts/gtask.js"></script>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/GTasksFunc2aspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
    <!--
        function getmeter(freqid, eqid, pmtskid, fuid, skillid, skillqty, rdid, skill, rd, meterid) {
            //alert(meterid)
            var eqnum = document.getElementById("lbleqnum").value;
            var func = document.getElementById("lblfunc").value;
            var varflg = 0;
            if (skillid == "" || skillid == "0") {
                varflg = 1;
            }
            if (rdid == "" || rdid == "0") {
                varflg = 1;
            }
            if (varflg == 0) {
                var eReturn = window.showModalDialog("../equip/usemeterdialog.aspx?typ=reg&eqid=" + eqid + "&pmtskid=" + pmtskid + "&fuid=" + fuid + "&skillid=" + skillid
                                   + "&skillqty=" + skillqty + "&rdid=" + rdid + "&func=" + func + "&skill=" + skill + "&rd=" + rd
                                   + "&meterid=" + meterid + "&rdt=" + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:900px;resizable=yes");
                if (eReturn) {
                    var ret = eReturn.split("~");
                    var del = ret[6];
                    if (del != "yes") {
                        document.getElementById(freqid).value = ret[2];
                        document.getElementById(freqid).disabled = true;
                        document.getElementById("lblfreq").value = ret[2];
                        document.getElementById("lblusemeter").value = "1";
                    }
                    else {
                        document.getElementById(freqid).disabled = false;
                        document.getElementById("lblusemeter").value = "";
                    }
                    //document.getElementById("lblcompchk").value = "2";
                    //document.getElementById("form1").submit();
                }
            }
        }
        //-->
    </script>
</head>
<body onload="scrolltop();start2();" onunload="stoptimer();">
    <form id="form1" method="post" runat="server">
    <table id="scrollmenu" style="z-index: 103; position: absolute; top: 8px; left: 4px"
        width="900" bgcolor="#2f77df">
        <tr>
            <td class="label">
                <asp:Label ID="lang233" runat="server">Filter</asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="lstfilter" runat="server" CssClass="plainlabel" AutoPostBack="True">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="1">Task Type</asp:ListItem>
                    <asp:ListItem Value="4">Equipment Status</asp:ListItem>
                    <asp:ListItem Value="5">Skill</asp:ListItem>
                    <asp:ListItem Value="6">No Filter</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="label">
                <asp:Label ID="lang234" runat="server">Filter Definition</asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="lstdef" runat="server" CssClass="plainlabel" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td>
                <asp:ImageButton ID="addtask" runat="server" ImageUrl="../images/appbuttons/bgbuttons/addtask.gif">
                </asp:ImageButton>
            </td>
            <td>
                <img class="details" id="btnexport" onclick="exportGrid('func');" height="19" src="../images/appbuttons/bgbuttons/export.gif"
                    width="69" runat="server">
            </td>
            <td>
                <asp:ImageButton ID="btnreturn" runat="server" ImageUrl="../images/appbuttons/bgbuttons/return.gif"
                    EnableViewState="False"></asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang235" runat="server">Current Sort</asp:Label>
            </td>
            <td class="labelwht" colspan="2">
                <asp:Label ID="lblsort" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"
                    ForeColor="White">Current Sort</asp:Label>
            </td>
            <td align="center" colspan="4">
                <asp:Label ID="ErrorLabel" runat="server" CssClass="labelwht" Font-Bold="True" Font-Names="Arial"
                    Font-Size="X-Small" ForeColor="White"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="100">
            </td>
            <td width="120">
            </td>
            <td width="120">
            </td>
            <td width="250">
            </td>
            <td width="90">
            </td>
            <td width="90">
            </td>
            <td width="90">
                <img class="details" id="hpBackward" alt="" src="../images/appbuttons/btnreturnnew.gif"
                    runat="server">
            </td>
        </tr>
    </table>
    <table style="z-index: 102; position: absolute; top: 64px; left: 4px" width="3200">
        <tbody>
            <tr>
                <td>
                    <asp:DataList ID="dlhd" runat="server">
                        <HeaderTemplate>
                            <table width="900">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr height="20">
                                <td class="label">
                                    <asp:Label ID="lang236" runat="server">Department</asp:Label>
                                </td>
                                <td>
                                    <asp:Label CssClass="plainlabel" ID="lbldept" runat="server" Font-Bold="False" Font-Names="Arial"
                                        Font-Size="X-Small" ForeColor="Black" Text='<%# DataBinder.Eval(Container, "DataItem.dept_line") %>'>
                                    </asp:Label>
                                </td>
                                <td class="label">
                                    <asp:Label ID="lang237" runat="server">Station/Cell</asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:Label CssClass="plainlabel" ID="lblcell" runat="server" Font-Bold="False" Font-Names="Arial"
                                        Font-Size="X-Small" ForeColor="Black" Text='<%# DataBinder.Eval(Container, "DataItem.cell_name") %>'>
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr height="20">
                                <td class="label">
                                    <asp:Label ID="lang238" runat="server">Equipment</asp:Label>
                                </td>
                                <td>
                                    <asp:Label CssClass="plainlabel" ID="lbleq" runat="server" Font-Bold="False" Font-Names="Arial"
                                        Font-Size="X-Small" ForeColor="Black" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
                                    </asp:Label>
                                </td>
                                <td class="label">
                                    <asp:Label ID="lang239" runat="server">Function</asp:Label>
                                </td>
                                <td>
                                    <asp:Label CssClass="plainlabel" ID="lblfunc" runat="server" Font-Bold="False" Font-Names="Arial"
                                        Font-Size="X-Small" ForeColor="Black" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td width="100">
                                </td>
                                <td width="200">
                                </td>
                                <td width="100">
                                </td>
                                <td width="200">
                                </td>
                                <td width="100">
                                </td>
                                <td width="200">
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:DataList>
                </td>
            </tr>
            <tr>
                <td class="plainlabelred" colspan="6">
                    <asp:Label ID="lang240" runat="server">Please note that look-up values are saved when selected and cannot be cancelled in Edit Mode.</asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DataGrid ID="dgtasks" runat="server" CellSpacing="1" AutoGenerateColumns="False"
                        AllowSorting="True" GridLines="None">
                        <AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
                        <ItemStyle CssClass="ptransrow"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                                    <img id="ibast" runat="server" src="../images/appbuttons/minibuttons/subtask.gif"
                                        onclick="getsgrid();" width="20" height="20">
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="tasknum desc" HeaderText="Task#">
                                <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblta" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="lblt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'
                                        Width="40px">
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="subtask desc" HeaderText="Sub Tasks" Visible="False">
                                <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblsubt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblst" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" CommandName="hd" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                        ToolTip="Hide Description"></asp:ImageButton><br>
                                    <asp:ImageButton ID="ImageButton2" runat="server" CommandName="sd" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                        ToolTip="Show Description"></asp:ImageButton>
                                </HeaderTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Description">
                                <HeaderStyle Width="270px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" Width="270px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtdesc" runat="server" CssClass="plainlabel" MaxLength="500" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>'
                                        Width="260px" TextMode="MultiLine" Height="70px">
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:ImageButton ID="ImageButton3" runat="server" CommandName="htt" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                        ToolTip="Hide Task Type"></asp:ImageButton><br>
                                    <asp:ImageButton ID="ImageButton4" runat="server" CommandName="stt" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                        ToolTip="Show Task Type"></asp:ImageButton>
                                </HeaderTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="tasktype desc" HeaderText="Task Type">
                                <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasktype") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lbltasktype" runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.tasktype") %>'>
                                    </asp:Label>
                                    <img id="imgtasktype" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:ImageButton ID="Imagebutton5" runat="server" CommandName="hfr" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                        ToolTip="Hide Frequency"></asp:ImageButton><br>
                                    <asp:ImageButton ID="Imagebutton6" runat="server" CommandName="sfr" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                        ToolTip="Show Frequency"></asp:ImageButton>
                                </HeaderTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="freq desc" HeaderText="Frequency">
                                <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblfreqi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtfreq" runat="server" CssClass="plainlabel" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'></asp:TextBox>
                                    <img id="imgfreq" alt="" runat="server" onmouseover="return overlib('View, Add, or Edit Meter Frequency for this Task', ABOVE, LEFT)"
                                        onmouseout="return nd()" src="../images/appbuttons/minibuttons/meter1.gif" />
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:ImageButton ID="Imagebutton24" runat="server" CommandName="hcomp" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                        ToolTip="Hide Reliability Maintenance Type"></asp:ImageButton><br>
                                    <asp:ImageButton ID="Imagebutton25" runat="server" CommandName="scomp" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                        ToolTip="Show Reliability Maintenance Type"></asp:ImageButton>
                                </HeaderTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="compnum desc" HeaderText="Component">
                                <HeaderStyle Width="90px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblcompnum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
                                    </asp:Label>
                                    <img id="imgcompnum" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="compnum desc" HeaderText="Failure Modes This Task Will Address">
                                <HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label21" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fm1") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblfm1" Width="230px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fm1") %>'>
                                    </asp:Label>
                                    <img id="imgfm" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:ImageButton ID="Imagebutton11" runat="server" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                        CommandName="hsk" ToolTip="Hide Skill Required"></asp:ImageButton><br>
                                    <asp:ImageButton ID="Imagebutton12" runat="server" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                        CommandName="ssk" ToolTip="Show Skill Required"></asp:ImageButton>
                                </HeaderTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="skill desc" HeaderText="Skill Required">
                                <HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblskill" Width="230px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                    </asp:Label>
                                    <img id="imgskill" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Qty">
                                <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'
                                        ID="Label10" NAME="Label8">
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtqty" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Time">
                                <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'
                                        ID="Label12">
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txttr" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <HeaderTemplate>
                                    <asp:ImageButton ID="Imagebutton9" runat="server" CommandName="hpt" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                        ToolTip="Hide Predictive Technology"></asp:ImageButton><br>
                                    <asp:ImageButton ID="Imagebutton10" runat="server" CommandName="spt" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                        ToolTip="Show Predictive Technology"></asp:ImageButton>
                                </HeaderTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="pretech desc" HeaderText="Predictive Technology">
                                <HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pretech") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblpretech" Width="230px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pretech") %>'>
                                    </asp:Label>
                                    <img id="imgpretech" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:ImageButton ID="Imagebutton13" runat="server" CommandName="hes" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                        ToolTip="Hide PM Equipment Status"></asp:ImageButton><br>
                                    <asp:ImageButton ID="Imagebutton14" runat="server" CommandName="ses" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                        ToolTip="Show PM Equipment Status"></asp:ImageButton>
                                </HeaderTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="rd desc" HeaderText="PM Equipment Status">
                                <HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label75" runat="server" Font-Size="10pt" Font-Names="Arial" ForeColor="Black"
                                        Text='<%# DataBinder.Eval(Container, "DataItem.rd") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblrd" Width="120px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rd") %>'>
                                    </asp:Label>
                                    <img id="imgrd" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:ImageButton ID="Imagebutton15" runat="server" CommandName="hlo" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                        ToolTip="Hide LOTO"></asp:ImageButton><br>
                                    <asp:ImageButton ID="Imagebutton16" runat="server" CommandName="slo" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                        ToolTip="Show LOTO"></asp:ImageButton>
                                </HeaderTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="loto desc" HeaderText="LOTO">
                                <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblloto" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.loto") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddloto" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("lotoid")) %>'>
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:ImageButton ID="Imagebutton17" runat="server" CommandName="hco" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
                                        ToolTip="Hide Confined Space"></asp:ImageButton><br>
                                    <asp:ImageButton ID="Imagebutton18" runat="server" CommandName="sco" ImageUrl="../images/appbuttons/minibuttons/show.gif"
                                        ToolTip="Show Confined Space"></asp:ImageButton>
                                </HeaderTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="confined desc" HeaderText="Confined Spaced">
                                <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblcs" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.confined") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddcs" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("conid")) %>'>
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Parts/Tools/Lubes">
                                <HeaderStyle CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton23" runat="server" ImageUrl="../images/appbuttons/minibuttons/parttrans.gif"
                                        ToolTip="Add Parts" CommandName="Part"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton23a" runat="server" ImageUrl="../images/appbuttons/minibuttons/tooltrans.gif"
                                        ToolTip="Add Tools" CommandName="Tool"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton23b" runat="server" ImageUrl="../images/appbuttons/minibuttons/lubetrans.gif"
                                        ToolTip="Add Lubricants" CommandName="Lube"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton23c" runat="server" ImageUrl="../images/appbuttons/minibuttons/notessm.gif"
                                        ToolTip="Add Notes to the Task" CommandName="Note"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="pmtskid">
                                <ItemTemplate>
                                    <asp:Label ID="lbltid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblttid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Delete">
                                <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibDel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="usemeter">
                                <ItemTemplate>
                                    <asp:Label ID="lblmidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.usemeter") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblmide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.usemeter") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblpmtid" runat="server" ForeColor="White"></asp:Label><asp:TextBox
                        ID="xcoord" runat="server" ForeColor="Black" BorderColor="White" BorderStyle="None"
                        Width="32px"></asp:TextBox><asp:TextBox ID="ycoord" runat="server" ForeColor="Black"
                            BorderColor="White" BorderStyle="None" Width="32px"></asp:TextBox>
                </td>
            </tr>
        </tbody>
    </table>
    <div style="position: absolute; background-color: blue; width: 5px; height: 5px;
        top: 900px">
    </div>
    <input id="lbltab" type="hidden" name="lbltab" runat="server">
    <input id="lbleqid" type="hidden" value="test" name="lbleqid" runat="server"><input
        id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lbldid" type="hidden" name="lbldid" runat="server"><input id="lblclid"
        type="hidden" name="lblclid" runat="server">
    <input id="lblret" type="hidden" name="lblret" runat="server"><input id="lblchk"
        type="hidden" name="lblchk" runat="server">
    <input id="lbldchk" type="hidden" name="lbldchk" runat="server"><input id="lblfuid"
        type="hidden" name="lblfuid" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
    <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server"><input id="lbladdchk"
        type="hidden" name="lbladdchk" runat="server"><input id="lblcid" type="hidden" name="lblcid"
            runat="server">
    <input id="lbltli" type="hidden" name="lbltli" runat="server"><input id="lblsubval"
        type="hidden" name="lblsubval" runat="server">
    <input id="lblfilt" type="hidden" name="lblfilt" runat="server"><input id="lblfiltfilt"
        type="hidden" name="lblfiltfilt" runat="server">
    <input id="lbladdval" type="hidden" name="lbladdval" runat="server"><input id="lblpart"
        type="hidden" name="lblpart" runat="server">
    <input id="lbltool" type="hidden" name="lbltool" runat="server"><input id="lbllube"
        type="hidden" name="lbllube" runat="server">
    <input id="lblnote" type="hidden" name="lblnote" runat="server"><input id="lblcurrsort"
        type="hidden" name="lblcurrsort" runat="server">
    <input id="lbloldtask" type="hidden" name="lbloldtask" runat="server"><input id="lblcurrcomid"
        type="hidden" runat="server" name="lblcurrcomid">
    <input id="lblcurrcomdesc" type="hidden" name="lblcurrcomdesc" runat="server"><input
        id="lblcurrtask" type="hidden" name="lblcurrtask" runat="server">
    <input id="lblcompfailchk" type="hidden" name="lblcompfailchk" runat="server"><input
        id="lblitemindex" type="hidden" name="lblitemindex" runat="server">
    <input id="spy" type="hidden" runat="server" name="spy"><input id="lblapp" type="hidden"
        runat="server" name="lblapp">
    <input id="lbldocpmid" type="hidden" runat="server" name="lbldocpmid"><input id="lbldocpmstr"
        type="hidden" runat="server" name="lbldocpmstr">
    <input id="lbltasknum" type="hidden" runat="server" name="lbltasknum">
    <input id="lblnewcomp" type="hidden" runat="server" name="lblnewcomp">
    <input id="lblncid" type="hidden" runat="server" name="lblncid">
    <input id="lblncindx" type="hidden" runat="server" name="lblncindx">
    <input id="lbltyp" type="hidden" runat="server" name="lbltyp">
    <input id="lbllid" type="hidden" runat="server" name="lbllid">
    <input type="hidden" id="lblro" runat="server" name="lblro">
    <input type="hidden" id="lbltcnt" runat="server" name="lbltcnt">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblfreq" runat="server" />
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblfunc" runat="server" />
    <input type="hidden" id="lblusemeter" runat="server" />
    <input type="hidden" id="lblusemeteri" runat="server" />
    <input type="hidden" id="lblusemetere" runat="server" />
    <input type="hidden" id="lbluser" runat="server" />
    <input type="hidden" id="lbltaskdesc" runat="server" />
    <input type="hidden" id="lblqty" runat="server" />
    <input type="hidden" id="lbltime" runat="server" />
    </form>
</body>
</html>
