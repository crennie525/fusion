Public Class appgetdialog
    Inherits System.Web.UI.Page
    Dim sid, wonum, typ As String
    Dim who, did, dept, clid, cell, eqid, eq, fuid, fu, coid, comp, lid, loc, jpid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents iftd As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            sid = Request.QueryString("site").ToString
            typ = Request.QueryString("typ").ToString
            If typ = "wo" Or typ = "wr" Or typ = "woret" Then
                wonum = Request.QueryString("wo").ToString
            ElseIf typ = "jp" Or typ = "jpret" Then
                jpid = Request.QueryString("jpid").ToString
            ElseIf typ = "wrret" Then
                wonum = Request.QueryString("wonum").ToString
            End If
            Try
                who = Request.QueryString("who").ToString
                did = Request.QueryString("did").ToString
                dept = Request.QueryString("dept").ToString
                dept = Replace(dept, "#", "%23")
                clid = Request.QueryString("clid").ToString
                cell = Request.QueryString("cell").ToString
                cell = Replace(cell, "#", "%23")
                eqid = Request.QueryString("eqid").ToString
                eq = Request.QueryString("eq").ToString
                eq = Replace(eq, "#", "%23")
                fuid = Request.QueryString("fuid").ToString
                fu = Request.QueryString("fu").ToString
                fu = Replace(fu, "#", "%23")
                coid = Request.QueryString("coid").ToString
                comp = Request.QueryString("comp").ToString
                comp = Replace(comp, "#", "%23")
                lid = Request.QueryString("lid").ToString
                loc = Request.QueryString("loc").ToString
                loc = Replace(loc, "#", "%23")
            Catch ex As Exception
                who = ""
                did = ""
                dept = ""
                clid = ""
                cell = ""
                eqid = ""
                eq = ""
                fuid = ""
                fu = ""
                coid = ""
                comp = ""
                lid = ""
                loc = ""
            End Try
            If typ = "jp" Or typ = "jpret" Then
                iftd.Attributes.Add("src", "appget.aspx?typ=" + typ + "&site=" + sid + "&jpid=" + jpid + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who + "&date=" + Now)
            ElseIf typ = "wrret" Then
                iftd.Attributes.Add("src", "appget.aspx?typ=" + typ + "&site=" + sid + "&wonum=" + wonum + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who + "&date=" + Now)
            Else
                iftd.Attributes.Add("src", "appget.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wonum + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who + "&date=" + Now)
            End If
        End If
    End Sub

End Class
