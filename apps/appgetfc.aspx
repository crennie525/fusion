<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="appgetfc.aspx.vb" Inherits="lucy_r12.appgetfc" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>appgetfc</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
    <!--
        function getecd(fi, fm, ok) {
            //alert(fi)
            var onechk = 0;
            var isone = document.getElementById("lblisone").value;
            var hasone = document.getElementById("lblhasone").value;
            var tot = document.getElementById("lbltotcnt").value;
            //alert(tot + ", " + hasone)
            if (isone == "NISS") {

                if (hasone == "1" || tot > 0) {
                    //need confirm
                    var dodel = confirm("One Failure Mode Allowed\nDo you want to Delete the current selection?")
                    if (dodel == true) {
                        document.getElementById("lblfids").value = "";
                        document.getElementById("lbltotcnt").value = "0";
                        document.getElementById("lblhasone").value = "0";
                        onechk = 1;
                    }
                    else {
                        alert("Action Cancelled")
                    }
                }
                else {
                    onechk = 1;
                }
            }
            else {
                onechk = 1;
            }
            if (onechk == "1") {


                var fmids = document.getElementById("lblfids").value;
                var fms = document.getElementById("lblfms").value;
                var fmidsarr = fmids.split(",");
                var fmidchk;
                var stpflg = 0;
                for (i = 0; i < fmidsarr.length; i++) {
                    fmidchk = fmidsarr[i];
                    if (fmidchk == fi) {
                        stpflg = 1;
                    }
                }
                if (stpflg != 1) {
                    if (tot < 5) {
                        tot = Number(tot) + 1;
                        document.getElementById("lbltotcnt").value = tot;
                        if (fmids == "") {
                            document.getElementById("tdecd1").innerHTML = '<a href="#" onclick="remit(\'' + fi + '\',\'' + fm + '\');">' + fm + '</a>';
                            document.getElementById("lblfids").value = fi;
                            document.getElementById("lblfms").value = fm;
                        }
                        else {
                            var curr = document.getElementById("tdecd1").innerHTML;
                            document.getElementById("tdecd1").innerHTML = curr + "<br>" + '<a href="#" onclick="remit(\'' + fi + '\',\'' + fm + '\');">' + fm + '</a>';
                            document.getElementById("lblfids").value = fmids + "," + fi;
                            document.getElementById("lblfms").value = fms + "," + fm;
                        }

                    }
                    else {
                        alert("Maximum Number of Failure Modes is 5")
                    }
                }
                else {
                    alert("This Failure Mode is Already in the List")
                }
                document.getElementById("trsave").className = "view";
            }

        }
        function remit(fi, fm) {
            var fmids = document.getElementById("lblfids").value;
            var remids = document.getElementById("lblremids").value;
            var fms = document.getElementById("lblfms").value;
            var fmidsarr = fmids.split(",");
            var fmsarr = fms.split(",");
            document.getElementById("tdecd1").innerHTML = "";
            document.getElementById("lblfids").value = "";
            document.getElementById("lblfms").value = "";
            var fmidchk;
            var newfmids;
            var newfms;
            var newfmspl;
            var curr;
            var icurr;
            var pcurr;
            var strtflg = 0;
            for (i = 0; i < fmidsarr.length; i++) {
                fmidchk = fmidsarr[i];
                if (fmidchk != fi) {
                    newfmids = fmidsarr[i]
                    newfms = '<a href="#" onclick="remit(\'' + fmidsarr[i] + '\',\'' + fmsarr[i] + '\');">' + fmsarr[i] + '</a>';
                    newfmspl = fmsarr[i]
                    if (strtflg == 0) {
                        strtflg = 1;
                        document.getElementById("tdecd1").innerHTML = newfms;
                        document.getElementById("lblfids").value = newfmids;
                        document.getElementById("lblfms").value = newfmspl;
                        curr = newfms;
                        icurr = newfmids;
                        pcurr = newfmspl;
                    }
                    else {
                        document.getElementById("tdecd1").innerHTML = curr + "<br>" + newfms;
                        document.getElementById("lblfids").value = icurr + "," + newfmids;
                        document.getElementById("lblfms").value = pcurr + "," + newfmspl;
                        curr = curr + "<br>" + newfms;
                        icurr = icurr + "," + newfmids;
                        pcurr = pcurr + "," + newfmspl;
                    }
                }
                else {
                    if (document.getElementById("lblremids").value == "") {
                        document.getElementById("lblremids").value = fmidchk;
                    }
                    else {
                        document.getElementById("lblremids").value = "," + fmidchk;
                    }
                }
            }
            document.getElementById("lblsubmit").value = "remit";
            document.getElementById("form1").submit();
        }

        function gettab(who) {
            if (who == "all") {
                //closeall();
                //document.getElementById("tdec").className = "view";
                //document.getElementById("tdtab").innerHTML = "All Failure Modes";
                document.getElementById("lblsubmit").value = "open";
                
            }
            else if (who == "site") {
                //if(chkarr[1]=="ok") {
                //closeall();
                //document.getElementById("tdec1").className = "view";
                //document.getElementById("tdtab").innerHTML = "Site Failure Modes";
                document.getElementById("lblsubmit").value = "site";
                //}
            }
            else if (who == "comp") {
                //if(chkarr[2]=="ok") {
                //closeall();
                //document.getElementById("tdec2").className = "view";
                //document.getElementById("tdtab").innerHTML = "Component Failure Modes";
                document.getElementById("lblsubmit").value = "co";
                //}
            }
            document.getElementById("form1").submit();
        }
        function closeall() {
            document.getElementById("tdec").className = "details";
            document.getElementById("tdec1").className = "details";
            document.getElementById("tdec2").className = "details";
        }
        function checkit() {
            var chk = document.getElementById("lblsubmit").value;
            if (chk == "return") {
                goback();
            }
        }
        function retit() {
            var typ = "wo"; //document.getElementById("lbltyp").value;
            if (typ == "wo") {
                document.getElementById("lblsubmit").value = "savewo";
                document.getElementById("form1").submit();
            }
        }
        function goback() {
            var fms = document.getElementById("lblfms").value
            //alert(fms)
            window.parent.handlereturn(fms);
        }
        //-->
    </script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td class="label" height="20" id="lbloca" runat="server">
                Occurrence Area
            </td>
            <td colspan="3">
                <asp:DropDownList ID="ddoca" runat="server" AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td id="tdtab" class="label" runat="server" colspan="4">
                Failure Modes
            </td>
        </tr>
        <tr>
            <td id="tdec" runat="server" colspan="4">
            </td>
            <td id="tdec1" class="details" runat="server" colspan="4">
            </td>
            <td id="tdec2" class="details" runat="server" colspan="4">
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <table>
                    <tr>
                        <td class="bluelabel" height="20" width="230">
                            <input type="radio" id="rball" name="rb1" onclick="gettab('all');" runat="server"><asp:Label ID="langafm" runat="server">All Failure Modes</asp:Label><br>
                            <input type="radio" id="rbsite" name="rb1" onclick="gettab('site');" runat="server"><asp:Label ID="langsfm" runat="server">Site Failure Modes</asp:Label><br>
                            <input type="radio" id="rbcomp" name="rb1" onclick="gettab('comp');" runat="server"><asp:Label ID="langcfm" runat="server">Component Failure Modes</asp:Label><br>
                        </td>
                        <td width="20" valign="top">
                            <img onclick="refit();" class="details" src="../images/appbuttons/minibuttons/refreshit.gif">
                        </td>
                    </tr>
                    <tr>
                        <td class="label" id="tdsfm" runat="server">
                            Selected Failure Modes
                        </td>
                    </tr>
                    <tr>
                        <td class="plainlabelred" id="tdcfmr" runat="server">
                            Click Failure Mode to Remove
                        </td>
                    </tr>
                    <tr>
                        <td id="tdecd1" class="plainlabel" runat="server">
                        </td>
                    </tr>
                    <tr id="trsave" class="view" runat="server">
                        <td colspan="3" align="right">
                            <img onclick="retit();" src="../images/appbuttons/minibuttons/savedisk1.gif">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lblwonum" runat="server">
    <input type="hidden" id="lblfailchk" runat="server">
    <input type="hidden" id="lblcoid" runat="server">
    <input type="hidden" id="lbltyp" runat="server">
    <input type="hidden" id="lblsubmit" runat="server">
    <input type="hidden" id="lblfids" runat="server">
    <input type="hidden" id="lblfms" runat="server">
    <input type="hidden" id="lblcfids" runat="server">
    <input type="hidden" id="lblcfms" runat="server">
    <input type="hidden" id="lbltotcnt" runat="server">
    <input type="hidden" id="lblfcnt" runat="server">
    <input type="hidden" id="lblcfcnt" runat="server">
    <input type="hidden" id="lblremids" runat="server">
    <input type="hidden" id="lblocarea" runat="server" />
    <input type="hidden" id="lbloaid" runat="server" />
    <input type="hidden" id="lblisecd" runat="server" />
    <input type="hidden" id="lblisone" runat="server" />
    <input type="hidden" id="lblhasone" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
