

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMTaskDivFunc
    Inherits System.Web.UI.Page
    Protected WithEvents ovid24 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid23 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid22 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid21 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid20 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid19 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid18 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid17 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid16 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents ovid15 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid14 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents ovid13 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents btnpfint As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang268 As System.Web.UI.WebControls.Label

    Protected WithEvents lang267 As System.Web.UI.WebControls.Label

    Protected WithEvents lang266 As System.Web.UI.WebControls.Label

    Protected WithEvents lang265 As System.Web.UI.WebControls.Label

    Protected WithEvents lang264 As System.Web.UI.WebControls.Label

    Protected WithEvents lang263 As System.Web.UI.WebControls.Label

    Protected WithEvents lang262 As System.Web.UI.WebControls.Label

    Protected WithEvents lang261 As System.Web.UI.WebControls.Label

    Protected WithEvents lang260 As System.Web.UI.WebControls.Label

    Protected WithEvents lang259 As System.Web.UI.WebControls.Label

    Protected WithEvents lang258 As System.Web.UI.WebControls.Label

    Protected WithEvents lang257 As System.Web.UI.WebControls.Label

    Protected WithEvents lang256 As System.Web.UI.WebControls.Label

    Protected WithEvents lang255 As System.Web.UI.WebControls.Label

    Protected WithEvents lang254 As System.Web.UI.WebControls.Label

    Protected WithEvents lang253 As System.Web.UI.WebControls.Label

    Protected WithEvents lang252 As System.Web.UI.WebControls.Label

    Protected WithEvents lang251 As System.Web.UI.WebControls.Label

    Protected WithEvents lang250 As System.Web.UI.WebControls.Label

    Protected WithEvents lang249 As System.Web.UI.WebControls.Label

    Protected WithEvents lang248 As System.Web.UI.WebControls.Label

    Protected WithEvents lang247 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tl, cid, sid, did, clid, eqid, fuid, coid, sql, val, name, field, ttid, tnum, typ, lid, appstr As String
    Dim co, fail, chk, tpmhold As String
    Dim tskcnt As Integer
    Dim tasks As New Utilities
    Dim tasksadd As New Utilities
    Dim taskssav As New Utilities
    Public dr As SqlDataReader
    Dim ds As DataSet
    Dim Tables As String = "pmtasks"
    Dim PK As String = "pmtskid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 1
    Dim Fields As String = "*, haspm = (select e.haspm from equipment e where e.eqid = pmtasks.eqid)"
    Dim Filter As String = ""
    Dim CntFilter As String = ""
    Dim TaskFilter As String = ""
    Dim Group As String = ""
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents btnNext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnaddtsk As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnsavetask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents msglbl As System.Web.UI.WebControls.Label
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtfreq As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblfail As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbfaillist As System.Web.UI.WebControls.ListBox
    Protected WithEvents ibToTask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibFromTask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents trlookup As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trh4 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Label21 As System.Web.UI.WebControls.Label
    Protected WithEvents btnRefreshPFI As System.Web.UI.WebControls.ImageButton
    Protected WithEvents pfdiv As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ipf As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents Label25 As System.Web.UI.WebControls.Label
    Protected WithEvents btndeltask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblsb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpgholder As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lblt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblst As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnaddsubtask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblsvchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnPrev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpfint As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblenable As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbcs As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnedittask As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ibReuse As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbCompFM As System.Web.UI.WebControls.ListBox
    Protected WithEvents txtcQty As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibCancel As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnStart As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnEnd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcompfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtrdt As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblcurrsb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdtasknav As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsubnav As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcnt As System.Web.UI.WebControls.Label
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents Label26 As System.Web.UI.WebControls.Label
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Sort As String = "tasknum, subtask asc"
    Dim strScript, login, username, ro, lang As String
    Dim haspm As Integer
    Protected WithEvents pgflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lot As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbloto As System.Web.UI.WebControls.CheckBox
    Protected WithEvents imgClock As System.Web.UI.WebControls.Image
    Protected WithEvents lblsubcount As System.Web.UI.WebControls.Label
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnsav As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ggrid As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents sgrid As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnaddcomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblhaspm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Img5 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iflst As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents btnlookup2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnlookup As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromreusedis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgcopycomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnaddnewfail As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblnoeq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltpmalert As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btntpm As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbltpmhold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdtpmhold As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgdeltpm As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbloldtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txttaskorder As System.Web.UI.WebControls.TextBox
    Protected WithEvents cbpush As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblnewkey As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewdesc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillqty As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmeterid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrdid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfunc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusemeter As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmfid As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lblfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfreqo As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents cbfixed As System.Web.UI.HtmlControls.HtmlInputCheckBox

    Protected WithEvents lblfixed As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lbltpmpmtskid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddpt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtqty As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttr As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddeqstat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcomp As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblptid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
            username = HttpContext.Current.Session("username").ToString()
            lblusername.Value = username
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If lbllog.Value <> "no" Then
            'Dim ftr As Footer
            'ftr = Page.FindControl("Footer1")
            'ftr.loc = Footer.eloc.divpg
            If lblsvchk.Value = "1" Then
                goNext()
            ElseIf lblsvchk.Value = "2" Then
                goPrev()
            ElseIf lblsvchk.Value = "3" Then
                'goSubNext()
            ElseIf lblsvchk.Value = "4" Then
                'goSubPrev()
            ElseIf lblsvchk.Value = "5" Then
                GoFirst()
            ElseIf lblsvchk.Value = "6" Then
                GoLast()
            ElseIf lblsvchk.Value = "7" Then
                GoNav()
            End If
            If Request.Form("lblcompchk") = "1" Then
                lblcompchk.Value = "0"
                tasks.Open()
                PopComp()
                'added in case components with tasks were copied on return
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                'end new
                coid = lblco.Value
                Try
                    ddcomp.SelectedValue = coid
                    SaveTaskComp()
                Catch ex As Exception

                End Try
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "2" Then
                lblcompchk.Value = "0"
                tasks.Open()
                PopComp()
                'added in case components deleted on return
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                'end new
                coid = lblco.Value
                Try
                    ddcomp.SelectedValue = coid
                Catch ex As Exception

                End Try
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "3" Then
                lblcompchk.Value = "0"
                tasksadd.Open()
                SaveTask2()
                tasksadd.Dispose()
            ElseIf Request.Form("lblcompchk") = "4" Then
                lblcompchk.Value = "0"
                tasks.Open()
                GetLists()
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "5" Then
                lblcompchk.Value = "0"
                tasks.Open()
                SubCount(PageNumber, Filter)
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "6" Then
                lblcompchk.Value = "0"
                tasks.Open()
                'LoadCommon()
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "uptpm" Then
                lblcompchk.Value = "0"
                tasks.Open()
                UpTPM()
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "deltpm" Then
                lblcompchk.Value = "0"
                tasks.Open()
                DelTPM()
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "pushcomp" Then
                lblcompchk.Value = "0"
                tasks.Open()
                PushComp()
                tasks.Dispose()
            End If
            If Request.Form("lblcompfailchk") = "1" Then
                lblcompfailchk.Value = "0"
                AddFail()
            End If
            Dim start As String = Request.QueryString("start").ToString
            If start = "no" Then
                lblpg.Text = "0"
                lblcnt.Text = "0"
                'lblspg.Text = "0"
                lblsubcount.Text = "0"
            End If
            If Not IsPostBack Then
                If start = "yes" Then
                    appstr = HttpContext.Current.Session("appstr").ToString()
                    CheckApps(appstr)
                    lblcnt.Text = "0"
                    lblsvchk.Value = "0"
                    lblenable.Value = "1"
                    tl = Request.QueryString("tl").ToString
                    lbltasklev.Value = tl
                    cid = Request.QueryString("cid").ToString
                    lblcid.Value = cid
                    sid = Request.QueryString("sid").ToString
                    lblsid.Value = sid
                    did = Request.QueryString("did").ToString
                    lbldid.Value = did
                    clid = Request.QueryString("clid").ToString
                    lblclid.Value = clid
                    eqid = Request.QueryString("eqid").ToString
                    lbleqid.Value = eqid
                    chk = Request.QueryString("chk").ToString
                    typ = Request.QueryString("typ").ToString
                    lbltyp.Value = typ
                    lid = Request.QueryString("lid").ToString
                    lbllid.Value = lid
                    lblchk.Value = chk
                    tasks.Open()
                    fuid = Request.QueryString("fuid").ToString
                    lblfuid.Value = fuid
                    val = fuid
                    field = "funcid"
                    name = "Function"
                    eqid = Request.QueryString("eqid").ToString
                    lbleqid.Value = eqid
                    GetLists()
                    lblsb.Value = "0"
                    Filter = field & " = " & val & " and subtask = 0"
                    CntFilter = field & " = " & val & " and subtask = 0"
                    lblfilt.Value = Filter
                    lblfiltcnt.Value = CntFilter
                    Dim tasknum As String
                    Try
                        tasknum = Request.QueryString("task").ToString
                        If tasknum = "" Then
                            tasknum = "0"
                        End If
                        'Filter = field & " = " & val & " and tasknum = " & tasknum
                        PageNumber = tasknum
                    Catch ex As Exception
                        tasknum = "0"
                    End Try


                    LoadPage(PageNumber, Filter)
                    btnaddtsk.Enabled = True
                    Dim lock, lockby As String
                    Dim user As String = lblusername.Value
                    'Read Only
                    Try
                        ro = HttpContext.Current.Session("ro").ToString
                    Catch ex As Exception
                        ro = "0"
                    End Try
                    lblro.Value = ro

                    'End Read Only
                    lock = CheckLock(eqid)
                    If lock = "1" Then
                        lockby = lbllockedby.Value
                        If lockby = user Then
                            lbllock.Value = "0"

                        Else
                            lblro.Value = "1" '***use instead?
                            ro = "1"
                            Dim strMessage As String = tmod.getmsg("cdstr231", "PMTaskDivFunc.aspx.vb") & " " & lockby & "."
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        End If
                    ElseIf lock = "0" Then
                        'LockRecord(user, eq)
                    End If
                    If ro = "1" Then
                        ibToTask.Visible = False
                        ibFromTask.Visible = False
                        ibReuse.Visible = False
                        todis.Attributes.Add("class", "view")
                        fromdis.Attributes.Add("class", "view")
                        fromreusedis.Attributes.Add("class", "view")
                        btnedittask.Attributes.Add("src", "../images/appbuttons/minibuttons/lilpendis.gif")
                        btnaddtsk.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                        btnaddtsk.Enabled = False
                        btndeltask.Attributes.Add("src", "../images/appbuttons/minibuttons/deldis.gif")
                        btndeltask.Enabled = False
                        ibCancel.Attributes.Add("src", "../images/appbuttons/minibuttons/candisk1dis.gif")
                        ibCancel.Enabled = False
                        btnsav.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                        btnsav.Attributes.Add("onclick", "")
                    End If
                    tasks.Dispose()

                Else
                    btnaddtsk.Enabled = False
                End If

                Dim locked As String = lbllock.Value
                If locked = "1" Then
                    'btnedittask.Attributes.Add("class", "details")
                    'btnaddtsk.Attributes.Add("class", "details")
                    'btndeltask.Attributes.Add("class", "details")
                    'ibCancel.Attributes.Add("class", "details")
                    'btnsav.Attributes.Add("class", "details")
                    'ggrid.Attributes.Add("class", "details")
                    'btnaddcomp.Attributes.Add("class", "details")
                End If
                'Disable all fields
                If ro <> "1" Then
                    txttaskorder.Enabled = False
                    ddcomp.Enabled = False
                    ''lbfaillist.Enabled = False
                    ''lbfailmodes.Enabled = False
                    ibToTask.Enabled = False
                    ibFromTask.Enabled = False
                    txtdesc.Enabled = False
                    ddtype.Enabled = False
                    txtfreq.Enabled = False

                    cbfixed.Disabled = True

                    txtpfint.Enabled = False
                    ddskill.Enabled = False
                    txtqty.Enabled = False
                    txttr.Enabled = False
                    txtrdt.Enabled = False
                    ddpt.Enabled = False
                    ddeqstat.Enabled = False
                    'cbloto.Enabled = False
                    'cbcs.Enabled = False
                    lblenable.Value = "1"
                    txtcQty.Enabled = False
                    'lbCompFM.Enabled = False
                    'buttons

                    btnaddsubtask.Enabled = False
                    btndeltask.Enabled = False
                    ibCancel.Enabled = False
                    btnsavetask.Enabled = False
                    btntpm.Enabled = False
                    'end disable
                End If

                btnsavetask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov9", "PMTaskDivFunc.aspx.vb") & "')")
                btnsavetask.Attributes.Add("onmouseout", "return nd()")
                btntpm.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov10", "PMTaskDivFunc.aspx.vb") & "')")
                btntpm.Attributes.Add("onmouseout", "return nd()")
                btndeltask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov11", "PMTaskDivFunc.aspx.vb") & "')")
                btndeltask.Attributes.Add("onmouseout", "return nd()")
                btnaddsubtask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov12", "PMTaskDivFunc.aspx.vb") & "')")
                btnaddsubtask.Attributes.Add("onmouseout", "return nd()")
                btnaddtsk.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov13", "PMTaskDivFunc.aspx.vb") & "')")
                btnaddtsk.Attributes.Add("onmouseout", "return nd()")
                btnaddtsk.Attributes.Add("onclick", "FreezeScreen('Your Data is Being Processed...');")
                btnedittask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov14", "PMTaskDivFunc.aspx.vb") & "')")
                btnedittask.Attributes.Add("onmouseout", "return nd()")
                ibToTask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov15", "PMTaskDivFunc.aspx.vb") & "')")
                ibToTask.Attributes.Add("onmouseout", "return nd()")
                'ibToTask.Attributes.Add("onclick", "DisableButton(this);")
                ibReuse.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov16", "PMTaskDivFunc.aspx.vb") & "')")
                ibReuse.Attributes.Add("onmouseout", "return nd()")
                'ibReuse.Attributes.Add("onclick", "DisableButton(this);")
                ibCancel.Attributes.Add("onmouseout", "return nd()")
                ibCancel.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov17", "PMTaskDivFunc.aspx.vb") & "')")
                ibFromTask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov18", "PMTaskDivFunc.aspx.vb") & "')")
                ibFromTask.Attributes.Add("onmouseout", "return nd()")
                'ibFromTask.Attributes.Add("onclick", "DisableButton(this);")
                btnPrev.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov19", "PMTaskDivFunc.aspx.vb") & "')")
                btnPrev.Attributes.Add("onmouseout", "return nd()")
                btnPrev.Attributes.Add("onclick", "confirmExit('tb', 'dev');")
                btnStart.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov20", "PMTaskDivFunc.aspx.vb") & "')")
                btnStart.Attributes.Add("onmouseout", "return nd()")
                btnStart.Attributes.Add("onclick", "confirmExit('ts', 'dev');")
                btnNext.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov21", "PMTaskDivFunc.aspx.vb") & "')")
                btnNext.Attributes.Add("onmouseout", "return nd()")
                btnNext.Attributes.Add("onclick", "confirmExit('tf', 'dev');")
                btnEnd.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov22", "PMTaskDivFunc.aspx.vb") & "')")
                btnEnd.Attributes.Add("onmouseout", "return nd()")
                btnEnd.Attributes.Add("onclick", "confirmExit('te', 'dev');")
                btnlookup.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov23", "PMTaskDivFunc.aspx.vb") & "')")
                btnlookup.Attributes.Add("onmouseout", "return nd()")
                btnlookup2.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov24", "PMTaskDivFunc.aspx.vb") & "')")
                btnlookup2.Attributes.Add("onmouseout", "return nd()")
                ddcomp.Attributes.Add("onchange", "chngchk();")
                ddtype.Attributes.Add("onchange", "GetType('d', this.value);")
                txttr.Attributes.Add("onkeyup", "filldown(this.value);")
                ddeqstat.Attributes.Add("onchange", "zerodt(this.value);")
                'tasks.Dispose()
            End If
        End If
    End Sub
    Private Sub PushComp()
        'usp_copyCompSing1coMSpush
        lang = lblfslang.Value
        Try
            cid = lblcid.Value
            sid = lblsid.Value
            Dim usr As String = lblusername.Value
            Dim oldcomp As String = lblco.Value
            Dim nc, nd, newkey As String
            nc = lblnewcomp.Value
            nd = lblnewdesc.Value
            newkey = lblnewkey.Value

            sql = "usp_copyCompSing1coMSpush '" & cid & "', '" & oldcomp & "', '" & sid & "', '" & nc & "', '" & nd & "', " _
                + "'" & usr & "','" & newkey & "','" & lang & "'"
            Dim ap As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
            Dim fid, cmid, ofid, ocmid As String
            Dim oloc As String = ap 'lbloloc.Value
            Dim cloc As String = ap
            Dim pid, t, tn, tm, ts, td, tns, tnd, tms, tmd, nt, ntn, ntm, ont, ontn, ontm As String
            Dim piccnt As Integer
            Dim ds As New DataSet
            ds = tasks.GetDSData(sql)
            Dim i As Integer
            Dim f As Integer = 0
            Dim c As Integer = 0
            Dim x As Integer = ds.Tables(0).Rows.Count
            For i = 0 To (x - 1)
                piccnt = ds.Tables(0).Rows(i)("piccnt").ToString
                If piccnt = 0 Then
                    cmid = ds.Tables(0).Rows(i)("comid").ToString
                    'lblnewcoid.Value = cmid
                Else
                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                    'fid = ds.Tables(0).Rows(i)("funcid").ToString
                    'ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                    cmid = ds.Tables(0).Rows(i)("comid").ToString
                    'lblnewcoid.Value = cmid
                    ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                    t = ds.Tables(0).Rows(i)("picurl").ToString
                    tn = ds.Tables(0).Rows(i)("picurltn").ToString
                    tm = ds.Tables(0).Rows(i)("picurltm").ToString
                    t = Replace(t, "..", "")
                    tn = Replace(tn, "..", "")
                    tm = Replace(tm, "..", "")
                    If cmid = "" Then
                        f = f + 1
                        nt = "b-eqImg" & fid & "i"
                        ntn = "btn-eqImg" & fid & "i"
                        ntm = "btm-eqImg" & fid & "i"

                        ont = "b-eqImg" & ofid & "i"
                        ontn = "btn-eqImg" & ofid & "i"
                        ontm = "btm-eqImg" & ofid & "i"
                    Else
                        If typ = "site" Then
                            c = c + 1
                            nt = "c-clImg" & cmid & "i"
                            ntn = "ctn-clImg" & cmid & "i"
                            ntm = "ctm-clImg" & cmid & "i"

                            ont = "c-clImg" & ocmid & "i"
                            ontn = "ctn-clImg" & ocmid & "i"
                            ontm = "ctm-clImg" & ocmid & "i"
                        Else
                            c = c + 1
                            nt = "c-coImg" & cmid & "i"
                            ntn = "ctn-coImg" & cmid & "i"
                            ntm = "ctm-coImg" & cmid & "i"

                            ont = "c-clImg" & ocmid & "i"
                            ontn = "ctn-clImg" & ocmid & "i"
                            ontm = "ctm-clImg" & ocmid & "i"

                        End If

                    End If

                    ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                    ts = Replace(ts, "\", "/")
                    td = Server.MapPath("\") & ap & t
                    td = Replace(td, "\", "/")


                    tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                    tn = Replace(tn, "\", "/")
                    tnd = Server.MapPath("\") & ap & tn
                    tnd = Replace(tnd, "\", "/")


                    tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                    tms = Replace(tms, "\", "/")
                    tmd = Server.MapPath("\") & ap & tm
                    tmd = Replace(tmd, "\", "/")

                    System.IO.File.Copy(ts, td, True)
                    System.IO.File.Copy(tns, tnd, True)
                    System.IO.File.Copy(tms, tmd, True)
                End If

            Next
            cbpush.Checked = False
            Dim strMessage As String = tmod.getmsg("cdstr401", "PMOptTasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Catch ex As Exception

        End Try

    End Sub
    Private Sub UpTPM()
        Dim usemeter As String = lblusemeter.Value
        If usemeter <> "1" Then
            eqid = lbleqid.Value
            cid = lblcid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            fuid = lblfuid.Value
            ttid = lbltaskid.Value
            Dim ttyp As String = ddtype.SelectedValue.ToString
            'If ttyp = "2" Or ttyp = "3" Or ttyp = "5" Then
            Dim tasknum As String = lblt.Value
            Dim user As String = HttpContext.Current.Session("username").ToString '"PM Administrator"
            'create procedure [dbo].[usp_copyTPMTask] (@cid int, 
            '@sid int, @did int, @clid int, @eqid int , @fuid int, @user varchar(50), @pmtskid int) as
            sql = "usp_copyTPMTask '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & user & "','" & ttid & "','" & tasknum & "'"
            tasks.Update(sql)
            'ddskill.SelectedValue = "2"
            'SaveTask2()
            'Else
            'Dim strMessage As String =  tmod.getmsg("cdstr232" , "PMTaskDivFunc.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'End If
        Else
            Dim strMessage As String = "Can`t Add Task with Meter Based Frequency to TPM"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If


    End Sub
    Private Sub DelTPM()
        fuid = lblfuid.Value
        tnum = lblt.Value
        ttid = lbltaskid.Value
        Dim tpmpmtskid As String = lbltpmpmtskid.Value
        Dim st As Integer = "0" 'lblsb.Value
        sql = "usp_delTPMTaskPM '" & fuid & "', '" & tnum & "', '" & st & "', '" & tpmpmtskid & "'"
        tasks.Update(sql)
        sql = "update pmtasks set tpmhold = '0' where funcid = '" & fuid & "' and tasknum = '" & tnum & "'" ' and pmtskid = '" & ttid & "'"
        tasks.Update(sql)
    End Sub
    Private Sub CheckApps(ByVal appstr As String)
        Dim apparr() As String = appstr.Split(",")
        Dim e As String = "0"
        Dim t As String = "0"
        'eq,dev,opt,inv
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                If apparr(i) = "eq" Then
                    e = "1"
                End If
                If apparr(i) = "tpd" Or apparr(i) = "tpo" Then
                    t = "1"
                End If
            Next
            If e <> "1" Then
                lblnoeq.Value = "1"
                btnaddcomp.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                btnaddcomp.Attributes.Add("onclick", "")
                btnaddnewfail.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                btnaddnewfail.Attributes.Add("onclick", "")
                imgcopycomp.Attributes.Add("onclick", "")
            End If
            If t <> "1" Then
                lblnoeq.Value = "1"
                btntpm.Attributes.Add("src", "../images/appbuttons/minibuttons/compresstpmdis.gif")
                btntpm.Enabled = False
                imgdeltpm.Attributes.Add("src", "../images/appbuttons/minibuttons/cantpmdis.gif")
                imgdeltpm.Attributes.Add("onclick", "")
            End If
        Else
            lblnoeq.Value = "0"
        End If

    End Sub
    Private Sub LoadCommon()

    End Sub
    Private Function CheckLock(ByVal eqid As String) As String
        Dim lock As String
        sql = "select locked, lockedby from equipment where eqid = '" & eqid & "'"
        Try
            dr = tasks.GetRdrData(sql)
            While dr.Read
                lock = dr.Item("locked").ToString
                lbllock.Value = dr.Item("locked").ToString
                lbllockedby.Value = dr.Item("lockedby").ToString
            End While
            dr.Close()
        Catch ex As Exception

        End Try
        Return lock
    End Function
    Private Sub GoNav()
        Dim pg As String = pgflag.Value
        If pg <> "0" Then
            Filter = lblfilt.Value
            lblsb.Value = "0"
            PageNumber = pg
            tasks.Open()
            LoadPage(PageNumber, Filter)
            tasks.Dispose()
            lblenable.Value = "1"
        End If
    End Sub
    Private Function SubCount(ByVal PageNumber As Integer, ByVal Filter As String) As Integer
        Dim scnt As Integer
        fuid = lblfuid.Value
        sql = "select count(*) from pmTasks where funcid = '" & fuid & "' and tasknum = " & PageNumber & " and subtask <> '0'"
        Try
            scnt = tasks.Scalar(sql)
        Catch ex As Exception
            scnt = tasksadd.Scalar(sql)
        End Try
        lblsubcount.Text = scnt
        Return scnt
    End Function
    Private Sub LoadPage(ByVal PageNumber As Integer, ByVal Filter As String)
        '**** Added for PM Manager
        eqid = lbleqid.Value
        'Try
        'lblhaspm.Value = tasks.HasPM(eqid)
        'Catch ex As Exception
        'lblhaspm.Value = tasksadd.HasPM(eqid)
        'End Try


        If lblsb.Value = "0" Then
            Dim scnt As Integer
            scnt = SubCount(PageNumber, Filter)
            lblsubcount.Text = scnt
            lblpgholder.Value = PageNumber
            cbloto.Checked = False
            cbcs.Checked = False
        End If
        Dim tskcnt, optcnt As Integer
        tskcnt = 0 'lblcnt.Text
        If tskcnt = 0 Then
            CntFilter = lblfiltcnt.Value
            sql = "select count(*) from pmtasks where " & CntFilter
            Try
                tskcnt = tasks.Scalar(sql)
            Catch ex As Exception
                tskcnt = tasksadd.Scalar(sql)
            End Try
        End If
        lblcnt.Text = tskcnt
        lblpg.Text = PageNumber

        If tskcnt = 0 Then
            lblpg.Text = "0"
            lblcnt.Text = tskcnt
            Dim strMessage As String = tmod.getmsg("cdstr233", "PMTaskDivFunc.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            'sql = "select tasknum from pmtasks where " & Filter & " order by tasknum"
            'Try
            'dr = tasks.GetRdrData(sql)
            'Catch ex As Exception
            'dr = tasksadd.GetRdrData(sql)
            'End Try

            'ddgoto.DataSource = dr
            'ddgoto.DataValueField = "tasknum"
            'ddgoto.DataTextField = "tasknum"
            'ddgoto.DataBind()
            'ddgoto.Items.Insert(0, New ListItem("GoTo"))
            'dr.Close()
            Tables = "pmtasks"
            PK = "pmtskid"
            fuid = lblfuid.Value
            Try
                dr = tasks.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            Catch ex As Exception
                dr = tasksadd.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            End Try
            Dim test As String
            test = lbltaskid.Value
            Dim freqid, rdid, qty, skillid, ismeter, fixed As String
            If dr.Read Then
                '***for meters
                fixed = dr.Item("fixed").ToString

                ismeter = dr.Item("usemeter").ToString
                lblusemeter.Value = dr.Item("usemeter").ToString
                lblmeterid.Value = dr.Item("meterid").ToString
                lblmfid.Value = dr.Item("mfid").ToString
                '***
                lbltpmpmtskid.Value = dr.Item("tpmpmtskid").ToString
                lbltpmhold.Value = dr.Item("tpmhold").ToString
                txtpfint.Text = dr.Item("pfInterval").ToString
                lblt.Value = dr.Item("tasknum").ToString
                lbloldtask.Value = dr.Item("tasknum").ToString
                txttaskorder.Text = dr.Item("tasknum").ToString
                ttid = dr.Item("pmtskid").ToString
                lbltaskid.Value = ttid
                lblst.Value = dr.Item("subtask").ToString
                Try
                    ddtype.SelectedValue = dr.Item("ttid").ToString
                Catch ex As Exception
                    ddtype.SelectedIndex = 0
                End Try
                freqid = dr.Item("freqid").ToString
                
                txtfreq.Text = dr.Item("freq").ToString

                lblfreq.Value = dr.Item("freq").ToString

                Try
                    ddpt.SelectedValue = dr.Item("ptid").ToString
                Catch ex As Exception
                    ddpt.SelectedIndex = 0
                End Try
                Try
                    ddskill.SelectedValue = dr.Item("skillid").ToString
                Catch ex As Exception
                    ddskill.SelectedIndex = 0
                End Try
                Try
                    ddeqstat.SelectedValue = dr.Item("rdid").ToString
                Catch ex As Exception
                    ddeqstat.SelectedIndex = 0
                End Try
                rdid = dr.Item("rdid").ToString
                '***for meters
                lblrdid.Value = rdid
                '***
                Dim desc As String = dr.Item("taskdesc").ToString
                desc = Replace(desc, "&#180;", "'")
                txtdesc.Text = desc

                txtqty.Text = dr.Item("qty").ToString
                txttr.Text = dr.Item("tTime").ToString
                txtrdt.Text = dr.Item("rdt").ToString
                qty = dr.Item("qty").ToString
                skillid = dr.Item("skillid").ToString

                '***for meters
                lblskillid.Value = skillid
                lblskillqty.Value = qty
                '***
                If dr.Item("lotoid").ToString = "1" Then
                    cbloto.Checked = True
                Else
                    cbloto.Checked = False
                End If
                If dr.Item("conid").ToString = "1" Then
                    cbcs.Checked = True
                Else
                    cbcs.Checked = False
                End If
                co = dr.Item("comid").ToString
                If Len(co) = 0 Or co = "" Then
                    co = "0"
                End If
                lblco.Value = co
                txtcQty.Text = dr.Item("cqty").ToString
                If txtcQty.Text = "" Then
                    txtcQty.Text = "1"
                End If
                lblhaspm.Value = dr.Item("haspm").ToString
            End If
            dr.Close()
            lblfixed.Value = fixed

            If fixed = "1" Then
                cbfixed.Checked = True
            Else
                cbfixed.Checked = False
            End If

            If lbltpmhold.Value = "1" Then
                tdtpmhold.InnerHtml = "(TPM Task)"
                lbltpmalert.Value = "no"
                imgdeltpm.Attributes.Add("class", "visible")
                btntpm.Visible = False
            Else
                If ismeter = "1" Then
                    lbltpmhold.Value = "0"
                    tdtpmhold.InnerHtml = ""
                    'lbltpmalert.Value = ""
                    imgdeltpm.Attributes.Add("class", "details")
                    btntpm.Visible = False
                Else
                    lbltpmhold.Value = "0"
                    tdtpmhold.InnerHtml = ""
                    'lbltpmalert.Value = ""
                    imgdeltpm.Attributes.Add("class", "details")
                    btntpm.Visible = True
                End If
            End If
            If co <> "0" Then
                lblco.Value = co
                Try
                    ddcomp.SelectedValue = co
                Catch ex As Exception

                End Try
                PopCompFailList(co)
                PopFailList(co)
                PopTaskFailModes(co)
                'UpdateFailStats(co)
                lblco.Value = co
                chk = "comp"
            Else
                Try
                    ddcomp.SelectedValue = co
                Catch ex As Exception

                End Try
                lbfaillist.Items.Clear()
                lbfailmodes.Items.Clear()
                lbCompFM.Items.Clear()
                lblco.Value = co
                chk = "comp"
            End If
            lblpar.Value = "comp"
            'UpDateRevs(ttid)
            'If lblsb.Value = "0" Then
            'lblpg.Text = PageNumber
            'Else
            'lblpg.Text = lblpgholder.Value
            'PageNumber = lblpgholder.Value
            'If Len(Filter) = 0 Then
            'Filter = " subtask = 0"
            'Else
            'Filter = Filter & " and subtask = 0"
            'End If
            'sql = "select count(*) from pmtasks where " & Filter
            'Try
            'tskcnt = tasks.Scalar(sql)
            'Catch ex As Exception
            'tskcnt = tasksadd.Scalar(sql)
            'End Try

            'tdtasknav.InnerHtml = "Page# " & PageNumber & " of " & tskcnt
            'End If
            lblcnt.Text = tskcnt
            'CheckPgNav(tskcnt, PageNumber)
            btnaddsubtask.Enabled = True
            GetMeterDetails(skillid, rdid, fuid, eqid)
        End If
    End Sub
    Private Sub GetMeterDetails(ByVal skillid As String, ByVal rdid As String, ByVal fuid As String, ByVal eqid As String)
        Dim rd, func, skill, eqnum, rdo, skillo, mfid, mfido As String
        sql = "declare @rd varchar(50), @func varchar(50), @skill varchar(50), @eqnum varchar(50) "
        sql += "declare @rdo varchar(50), @skillo varchar(50), @mfid int, @mfido int "
        sql += "set @rd = (select status from pmstatus where statid = '" & rdid & "') "
        sql += "set @func = (select func from functions where func_id = '" & fuid & "') "
        sql += "set @skill = (select skill from pmskills where skillid = '" & skillid & "') "
        sql += "set @eqnum = (select eqnum from equipment where eqid = '" & eqid & "') "
        sql += "select @rd as rd, @func as func, @skill as skill, @eqnum as eqnum"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try
        While dr.Read
            rd = dr.Item("rd").ToString
            func = dr.Item("func").ToString
            skill = dr.Item("skill").ToString
            eqnum = dr.Item("eqnum").ToString
        End While
        dr.Close()
        lblrd.Value = rd
        lblfunc.Value = func
        lblskill.Value = skill
        lbleqnum.Value = eqnum

    End Sub
    Private Function SubTaskCnt(ByVal PageNumber As Integer, ByVal Filter As String) As Integer
        Dim tcnt As Integer
        CntFilter = lblfiltcnt.Value
        sql = "select count(*) from pmTasks where " & CntFilter & " and tasknum <= " & PageNumber & " and subtask = '0'"
        tcnt = tasks.Scalar(sql)
        Dim scnt As Integer
        sql = "select count(*) from pmTasks where " & CntFilter & " and tasknum < " & PageNumber & " and subtask <> '0'"
        scnt = tasks.Scalar(sql)
        Dim wcnt = tcnt + scnt
        Return wcnt
    End Function

    Private Sub GoFirst()
        Filter = lblfilt.Value
        lblsb.Value = "0"
        PageNumber = 1
        'Filter = Filter & " and tasknum = " & PageNumber
        tasks.Open()
        LoadPage(PageNumber, Filter)
        tasks.Dispose()
        lblenable.Value = "1"
    End Sub
    Private Sub GoLast()
        Filter = lblfilt.Value
        lblsb.Value = "0"
        PageNumber = lblcnt.Text
        'Filter = Filter & " and tasknum = " & PageNumber
        tasks.Open()
        LoadPage(PageNumber, Filter)
        tasks.Dispose()
        lblenable.Value = "1"
    End Sub
    Private Sub goNext()
        Filter = lblfilt.Value
        If lblsb.Value = "0" Then
            PageNumber = lblpg.Text
        Else
            PageNumber = lblpgholder.Value
        End If

        lblsb.Value = "0"
        PageNumber = PageNumber + 1
        'Filter = Filter & " and tasknum = " & PageNumber
        If PageNumber <= lblcnt.Text Then
            tasks.Open()
            LoadPage(PageNumber, Filter)
            tasks.Dispose()
        Else

            Dim strMessage As String = tmod.getmsg("cdstr234", "PMTaskDivFunc.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        lblenable.Value = "1"
    End Sub
    Private Sub goPrev()
        Filter = lblfilt.Value
        'If lblsb.Value = "0" Or lblspg.Text = "1" Then
        'PageNumber = lblpg.Text
        'Else
        PageNumber = lblpgholder.Value
        'End If
        lblsb.Value = "0"
        PageNumber = lblpg.Text
        If PageNumber > 1 Then
            If PageNumber = 1 Then
                fuid = lblfuid.Value
                Dim tcnt As Integer
                sql = "select count(*) from pmtasks where funcid = '" & fuid & "' and tasknum = '0'"
                Dim task0 As New Utilities
                task0.Open()
                tcnt = task0.Scalar(sql)
                task0.Dispose()
                If tcnt = 1 Then
                    Try
                        PageNumber = PageNumber - 1
                        'Filter = Filter & " and tasknum = " & PageNumber
                        tasks.Open()
                        LoadPage(PageNumber, Filter)
                    Catch ex As Exception
                        Dim strMessage As String = tmod.getmsg("cdstr235", "PMTaskDivFunc.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        Exit Sub
                    End Try
                End If
            Else
                Try
                    PageNumber = PageNumber - 1
                    'Filter = Filter & " and tasknum = " & PageNumber
                    tasks.Open()
                    LoadPage(PageNumber, Filter)
                Catch ex As Exception
                    Dim strMessage As String = tmod.getmsg("cdstr236", "PMTaskDivFunc.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If

            'ElseIf lblspg.Text = "1" Then
            'PageNumber = PageNumber
            'Filter = Filter & " and tasknum = " & PageNumber
            'LoadPage(PageNumber, Filter)
        Else
            Dim strMessage As String = tmod.getmsg("cdstr237", "PMTaskDivFunc.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        lblenable.Value = "1"
        tasks.Dispose()
    End Sub

    Private Sub UpDateRevs(ByVal Filter As String)
        sql = "select created, revised from pmtasks where pmtskid = '" & Filter & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        Dim cr, rv As String
        While dr.Read
            cr = dr.Item("created").ToString
            rv = dr.Item("revised").ToString
        End While
        dr.Close()
        If Len(rv) = 0 Then
            rv = "NA"
        End If
        imgClock.Attributes.Add("onmouseover", "return overlib('Task Created: " & cr & "<br> Task Revised: " & rv & "')")
        imgClock.Attributes.Add("onmouseout", "return nd()")
    End Sub
    Private Sub CheckPgNav(ByVal tskcnt As Integer, ByVal PageNumber As Integer)
        If PageNumber = 1 Or tskcnt = 1 Then
            'btnPrev.Enabled = False
        Else
            'btnPrev.Enabled = True
        End If
        If PageNumber < tskcnt Then
            'btnNext.Enabled = True
        Else
            'btnNext.Enabled = False
        End If
    End Sub

    Private Sub GetLists()
        cid = lblcid.Value


        sql = "select ttid, tasktype " _
        + "from pmTaskTypes where compid = '" & cid & "' or compid = '0' and tasktype <> 'Select' order by compid"
        dr = tasks.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataBind()
        dr.Close()
        ddtype.Items.Insert(0, New ListItem("Select"))
        ddtype.Items(0).Value = 0

        sid = lblsid.Value
        Dim scnt As Integer
        sql = "select count(*) from pmSiteSkills where siteid = '" & sid & "'"
        scnt = tasks.Scalar(sql)
        If scnt <> 0 Then
            sql = "select skillid, skill " _
            + "from pmSiteSkills where compid = '" & cid & "' and siteid = '" & sid & "'"
        Else
            sql = "select skillid, skill " _
            + "from pmSkills where compid = '" & cid & "' order by compid"
        End If
        dr = tasks.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("Select"))
        ddskill.Items(0).Value = 0

        sql = "select ptid, pretech " _
        + "from pmPreTech where compid = '" & cid & "' or compid = '0' order by compid"
        dr = tasks.GetRdrData(sql)
        ddpt.DataSource = dr
        ddpt.DataTextField = "pretech"
        ddpt.DataValueField = "ptid"
        ddpt.DataBind()
        dr.Close()
        ddpt.Items.Insert(0, New ListItem("None"))
        ddpt.Items(0).Value = 0

        sql = "select statid, status " _
        + "from pmStatus where compid = '" & cid & "' or compid = '0' order by compid"
        dr = tasks.GetRdrData(sql)
        ddeqstat.DataSource = dr
        ddeqstat.DataTextField = "status"
        ddeqstat.DataValueField = "statid"
        ddeqstat.DataBind()
        dr.Close()
        ddeqstat.Items.Insert(0, New ListItem("Select"))
        ddeqstat.Items(0).Value = 0
        PopComp()
    End Sub

    Private Sub btnaddtsk_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnaddtsk.Click
        lblenable.Value = "0"
        tl = lbltasklev.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        tl = 5
        tasksadd.Open()

        sql = "usp_AddTask '" & tl & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & ustr & "'"
        tasksadd.Scalar(sql)
        Filter = lblfilt.Value

        PageNumber = lblcnt.Text
        PageNumber = PageNumber + 1
        lblcnt.Text = PageNumber
        field = "funcid"
        val = fuid
        Filter = field & " = " & val & " and subtask = 0" '& " and tasknum = " & PageNumber

        LoadPage(PageNumber, Filter)
        tasksadd.Dispose()
    End Sub

    Private Sub btnsavetask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsavetask.Click

    End Sub
    Private Sub SaveTask2()
        Dim usemeter As String = lblusemeter.Value
        Dim tn, otn As String
        tn = txttaskorder.Text
        Dim tnchk As Long
        Try

            tnchk = System.Convert.ToInt64(tn)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr238", "PMTaskDivFunc.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        otn = lblt.Value
        If tn = 0 Or Len(tn) = 0 Then
            tn = otn
        End If
        Dim lst As Integer = lblcnt.Text
        Try
            If tnchk > lst Then
                tn = lst
            End If
        Catch ex As Exception

        End Try
        If tn < 1 Then
            tn = 1
        End If
        ro = lblro.Value
        If ro <> "1" Then
            Dim pf, t, st, tid, typ, fre, des, rmt, pt, ski, qty, tr, eqs, lot, cs, ci, cn, cin As String
            Dim typstr, frestr, rmtstr, ptstr, skistr, eqsstr, cqty, rdt, fixed As String
            Dim fm As String
            Dim Item As ListItem
            Dim f, fi As String
            Dim ipar As Integer = 0
            For Each Item In lbfailmodes.Items
                f = Item.Text.ToString
                ipar = f.LastIndexOf("(")
                If ipar <> -1 Then
                    f = Mid(f, 1, ipar)
                End If
                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
            Next
            fm = tasks.ModString2(fm)
            t = lblt.Value
            st = lblsb.Value
            If ddtype.SelectedIndex = 0 Then
                typ = "0"
            Else
                typ = ddtype.SelectedValue
            End If
            typstr = ddtype.SelectedItem.ToString
            typstr = Replace(typstr, "'", Chr(180), , , vbTextCompare)

            fre = "" 'txtfreq.Text
            frestr = txtfreq.Text
            If frestr = "" Then
                frestr = lblfreq.Value
            End If

            des = txtdesc.Text
            des = tasks.ModString2(des)

            rdt = txtrdt.Text

            qty = txtqty.Text
            Dim qtychk As Long
            Try
                qtychk = System.Convert.ToInt32(qty)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr239", "PMTaskDivFunc.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            If Len(qty) = 0 Then
                qty = "1"
            End If

            tr = txttr.Text
            If Len(tr) = 0 Then
                tr = "0"
            End If

            If ddeqstat.SelectedIndex = 0 Then
                eqs = "0"
            Else
                eqs = ddeqstat.SelectedValue
            End If
            If eqs = "0" Then
                If usemeter = "1" Then
                    Dim strMessage1 As String = "Status Required When Using Meter Frequency"
                    Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                    Exit Sub
                End If
            End If
            eqsstr = ddeqstat.SelectedItem.ToString
            eqsstr = Replace(eqsstr, "'", Chr(180), , , vbTextCompare)
            If cbloto.Checked = True Then
                lot = "1"
            Else
                lot = "0"
            End If

            If cbcs.Checked = True Then
                cs = "1"
            Else
                cs = "0"
            End If

            tid = lbltaskid.Value
            Filter = lblfilt.Value
            If ddcomp.SelectedIndex = 0 Then
                ci = "0"
                cin = "0"
            Else
                ci = ddcomp.SelectedValue.ToString
            End If
            fuid = lblfuid.Value

            cqty = txtcQty.Text
            Dim cqtychk As Long
            Try
                cqtychk = System.Convert.ToInt32(cqty)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr240", "PMTaskDivFunc.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try

            pf = txtpfint.Text

            If cin Is Nothing Then
                cin = "0"
            End If

            cn = ddcomp.SelectedItem.ToString
            cn = tasks.ModString2(cn)

            tl = lbltasklev.Value
            cid = lblcid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            eqid = lbleqid.Value
            fuid = lblfuid.Value

            Dim ttid As String = lbltaskid.Value

            sid = lblsid.Value

            PageNumber = lblpg.Text
            fuid = lblfuid.Value

            Dim usr As String = HttpContext.Current.Session("username").ToString()
            Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
            eqid = lbleqid.Value
            Dim hpm As String = lblhaspm.Value

            If ddpt.SelectedIndex = 0 Then
                pt = "0"
            Else
                pt = ddpt.SelectedValue
            End If
            ptstr = ddpt.SelectedItem.ToString
            ptstr = Replace(ptstr, "'", Chr(180), , , vbTextCompare)
            If ddskill.SelectedIndex = 0 Then
                ski = "0"
            Else
                ski = ddskill.SelectedValue
            End If
            If ski = "0" Then
                If usemeter = "1" Then
                    Dim strMessage1 As String = "Skill Required When Using Meter Frequency"
                    Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                    Exit Sub
                End If
            End If
            skistr = ddskill.SelectedItem.ToString
            skistr = Replace(skistr, "'", Chr(180), , , vbTextCompare)
            fixed = lblfixed.Value
            If cbfixed.Checked = True Then
                fixed = "1"
            End If
            
            If Len(ttid) <> 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = "exec usp_updatepmtasks @pmtskid, @des, @qty, @ttime, @rdid, @rd, " _
                + "@fre, @freq, @lotoid, @conid, @ttid, @tasktype, @comid, " _
                + "@compnum, @fm1, @cqty, @pfinterval, @rdt, @filter, " _
                + "@pagenumber, @fuid, " _
                + "@ustr, @eqid, @sid, @hpm, @pt, @ptstr, @ski, @skistr, @fixed"

                Dim param = New SqlParameter("@pmtskid", SqlDbType.Int)
                param.Value = ttid
                cmd.Parameters.Add(param)
                Dim param01 = New SqlParameter("@des", SqlDbType.VarChar)
                If des = "" Then
                    param01.Value = System.DBNull.Value
                Else
                    param01.Value = des
                End If
                cmd.Parameters.Add(param01)
                Dim param02 = New SqlParameter("@qty", SqlDbType.Int)
                If qty = "" Then
                    param02.Value = "1"
                Else
                    param02.Value = qty
                End If
                cmd.Parameters.Add(param02)
                Dim param03 = New SqlParameter("@ttime", SqlDbType.Decimal)
                If tr = "" Then
                    param03.Value = "0"
                Else
                    param03.Value = tr
                End If
                cmd.Parameters.Add(param03)
                Dim param04 = New SqlParameter("@rdid", SqlDbType.Int)
                If eqs = "" Then
                    param04.Value = "0"
                Else
                    param04.Value = eqs
                End If
                cmd.Parameters.Add(param04)
                Dim param05 = New SqlParameter("@rd", SqlDbType.VarChar)
                If eqsstr = "" Then
                    param05.Value = System.DBNull.Value
                Else
                    param05.Value = eqsstr
                End If
                cmd.Parameters.Add(param05)
                Dim param06 = New SqlParameter("@fre", SqlDbType.Int)
                If fre = "" Then
                    param06.Value = "0"
                Else
                    param06.Value = fre
                End If
                cmd.Parameters.Add(param06)
                Dim param07 = New SqlParameter("@freq", SqlDbType.VarChar)
                If frestr = "" Or frestr = "Select" Then
                    param07.Value = System.DBNull.Value
                Else
                    param07.Value = frestr
                End If
                cmd.Parameters.Add(param07)
                Dim param08 = New SqlParameter("@lotoid", SqlDbType.Int)
                If lot = "" Then
                    param08.Value = "0"
                Else
                    param08.Value = lot
                End If
                cmd.Parameters.Add(param08)
                Dim param09 = New SqlParameter("@conid", SqlDbType.Int)
                If cs = "" Then
                    param09.Value = "0"
                Else
                    param09.Value = cs
                End If
                cmd.Parameters.Add(param09)
                Dim param10 = New SqlParameter("@ttid", SqlDbType.Int)
                If typ = "" Then
                    param10.Value = "0"
                Else
                    param10.Value = typ
                End If
                cmd.Parameters.Add(param10)
                Dim param11 = New SqlParameter("@tasktype", SqlDbType.VarChar)
                If typstr = "" Or typstr = "Select" Then
                    param11.Value = System.DBNull.Value
                Else
                    param11.Value = typstr
                End If
                cmd.Parameters.Add(param11)
                Dim param12 = New SqlParameter("@comid", SqlDbType.Int)
                If ci = "" Or ci = "Select" Then
                    param12.Value = "0"
                Else
                    param12.Value = ci
                End If
                cmd.Parameters.Add(param12)
                Dim param13 = New SqlParameter("@compnum", SqlDbType.VarChar)
                If cn = "" Or cn = "Select" Then
                    param13.Value = System.DBNull.Value
                Else
                    param13.Value = cn
                End If
                cmd.Parameters.Add(param13)
                Dim param14 = New SqlParameter("@fm1", SqlDbType.VarChar)
                If fm = "" Then
                    param14.Value = System.DBNull.Value
                Else
                    param14.Value = fm
                End If
                cmd.Parameters.Add(param14)
                Dim param15 = New SqlParameter("@cqty", SqlDbType.Int)
                If cqty = "" Then
                    param15.Value = "1"
                Else
                    param15.Value = cqty
                End If
                cmd.Parameters.Add(param15)
                Dim param16 = New SqlParameter("@pfinterval", SqlDbType.Int)
                If pf = "" Then
                    param16.Value = System.DBNull.Value
                Else
                    param16.Value = pf
                End If
                cmd.Parameters.Add(param16)
                Dim param17 = New SqlParameter("@rdt", SqlDbType.Decimal)
                If rdt = "" Then
                    param17.Value = "0"
                Else
                    param17.Value = rdt
                End If
                cmd.Parameters.Add(param17)
                Dim param18 = New SqlParameter("@filter", SqlDbType.VarChar)
                If Filter = "" Then
                    param18.Value = System.DBNull.Value
                Else
                    param18.Value = Filter
                End If
                cmd.Parameters.Add(param18)
                Dim param19 = New SqlParameter("@pagenumber", SqlDbType.Int)
                param19.Value = lblt.Value
                cmd.Parameters.Add(param19)
                Dim param20 = New SqlParameter("@fuid", SqlDbType.Int)
                If fuid = "" Then
                    param20.Value = "0"
                Else
                    param20.Value = fuid
                End If
                cmd.Parameters.Add(param20)

                Dim param48 = New SqlParameter("@ustr", SqlDbType.VarChar)
                If ustr = "" Then
                    param48.Value = System.DBNull.Value
                Else
                    param48.Value = ustr
                End If
                cmd.Parameters.Add(param48)
                Dim param49 = New SqlParameter("@eqid", SqlDbType.Int)
                param49.Value = eqid
                cmd.Parameters.Add(param49)
                Dim param50 = New SqlParameter("@sid", SqlDbType.Int)
                param50.Value = sid
                cmd.Parameters.Add(param50)

                Dim param89 = New SqlParameter("@hpm", SqlDbType.Int)
                If hpm = "" Then
                    param89.Value = "0"
                Else
                    param89.Value = hpm
                End If
                cmd.Parameters.Add(param89)

                '@pt, @ptstr, @ski, @skistr
                Dim param21 = New SqlParameter("@pt", SqlDbType.Int)
                If pt = "" Then
                    param21.Value = "0"
                Else
                    param21.Value = pt
                End If
                cmd.Parameters.Add(param21)
                Dim param22 = New SqlParameter("@ptstr", SqlDbType.VarChar)
                If ptstr = "" Or ptstr = "Select" Then
                    param22.Value = System.DBNull.Value
                Else
                    param22.Value = ptstr
                End If
                cmd.Parameters.Add(param22)

                Dim param23 = New SqlParameter("@ski", SqlDbType.Int)
                If ski = "" Then
                    param23.Value = "0"
                Else
                    param23.Value = ski
                End If
                cmd.Parameters.Add(param23)
                Dim param24 = New SqlParameter("@skistr", SqlDbType.VarChar)
                If skistr = "" Or ptstr = "Select" Then
                    param24.Value = System.DBNull.Value
                Else
                    param24.Value = skistr
                End If
                cmd.Parameters.Add(param24)

                Dim param30 = New SqlParameter("@fixed", SqlDbType.VarChar)
                If fixed = "" Then
                    param30.Value = System.DBNull.Value
                Else
                    param30.Value = fixed
                End If
                cmd.Parameters.Add(param30)

                Dim tpm As Integer
                Try
                    tpm = tasks.ScalarHack(cmd)
                Catch ex As Exception
                    tpm = tasksadd.ScalarHack(cmd)
                End Try

                If tpm = 1 Then
                    lbltpmalert.Value = "yes"
                Else
                    lbltpmalert.Value = "no"
                End If

                If otn <> tn Then
                    fuid = lblfuid.Value
                    sql = "usp_reorderPMTasks '" & ttid & "', '" & fuid & "', '" & tn & "', '" & otn & "'"
                    Try
                        tasks.Update(sql)


                    Catch ex As Exception
                        tasksadd.Update(sql)


                    End Try

                End If

                '*** New For Meters - Should add to procedure ***
                Dim mfid As String
                mfid = lblmfid.Value
                If mfid <> "" Then
                    sql = "update meterfreq set skillid = '" & ski & "', rdid = '" & eqs & "', skillqty = '" & qty & "' " _
                   + "where mfid = '" & mfid & "'"
                    Try
                        tasks.Update(sql)
                    Catch ex As Exception
                        tasksadd.Update(sql)
                    End Try
                End If

                '***end new ***

                Dim currcnt As String = lblcurrsb.Value
                Dim cscnt As String = lblcurrcs.Value
                If st = 0 Then
                    If otn <> tn Then
                        PageNumber = tn
                    Else
                        PageNumber = lblpg.Text
                    End If

                Else
                    PageNumber = currcnt + cscnt
                End If
                'question this
                field = "pmtasks.funcid"
                val = fuid
                Filter = field & " = " & val & " and pmtasks.subtask = 0"
                'end question
                LoadPage(PageNumber, Filter)
                lblenable.Value = "1"
            End If
        End If

    End Sub
    Private Sub SaveTask()
        Dim pf, t, st, tid, typ, fre, des, rmt, pt, ski, qty, tr, eqs, lot, cs, ci, cn, cin As String
        Dim typstr, frestr, rmtstr, ptstr, skistr, eqsstr, cqty, rdt, fixed As String
        Dim fm As String
        Dim Item As ListItem
        Dim f, fi As String
        For Each Item In lbfailmodes.Items
            f = Item.Text.ToString
            If Len(fm) = 0 Then
                fm = f & "(___)"
            Else
                fm += " " & f & "(___)"
            End If
        Next
        fm = tasks.ModString2(fm)
        t = lblt.Value
        st = lblsb.Value
        If ddtype.SelectedIndex = 0 Then
            typ = "0"
        Else
            typ = ddtype.SelectedValue
        End If
        typstr = ddtype.SelectedItem.ToString
        typstr = Replace(typstr, "'", Chr(180), , , vbTextCompare)

        fre = "0"
       
        frestr = txtfreq.Text
        frestr = Replace(frestr, "'", Chr(180), , , vbTextCompare)
        des = txtdesc.Text
        des = tasks.ModString2(des)

        rdt = txtrdt.Text

        If ddpt.SelectedIndex = 0 Then
            pt = "0"
        Else
            pt = ddpt.SelectedValue
        End If
        ptstr = ddpt.SelectedItem.ToString
        ptstr = Replace(ptstr, "'", Chr(180), , , vbTextCompare)
        If ddskill.SelectedIndex = 0 Then
            ski = "0"
        Else
            ski = ddskill.SelectedValue
        End If
        skistr = ddskill.SelectedItem.ToString
        skistr = Replace(skistr, "'", Chr(180), , , vbTextCompare)
        qty = txtqty.Text
        Dim qtychk As Long
        'Try
        'qtychk = System.Convert.ToInt32(qty)
        'Catch ex As Exception
        'Dim strMessage As String =  tmod.getmsg("cdstr241" , "PMTaskDivFunc.aspx.vb")

        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End Try
        If Len(qty) = 0 Then
            qty = "1"
        End If



        tr = txttr.Text
        If Len(tr) = 0 Then
            tr = "0"
        End If

        If ddeqstat.SelectedIndex = 0 Then
            eqs = "0"
        Else
            eqs = ddeqstat.SelectedValue
        End If
        eqsstr = ddeqstat.SelectedItem.ToString
        eqsstr = Replace(eqsstr, "'", Chr(180), , , vbTextCompare)
        If cbloto.Checked = True Then
            lot = "1"
        Else
            lot = "0"
        End If

        If cbcs.Checked = True Then
            cs = "1"
        Else
            cs = "0"
        End If

        tid = lbltaskid.Value
        Filter = lblfilt.Value
        If ddcomp.SelectedIndex = 0 Then
            ci = "0"
            cin = "0"
        Else
            ci = ddcomp.SelectedValue.ToString
            sql = "select compindex from components where func_id = '" & fuid & "' and comid = '" & ci & "'"
            Try
                dr = tasks.GetRdrData(sql)
            Catch ex As Exception
                dr = tasksadd.GetRdrData(sql)
            End Try

            If dr.Read Then
                cin = dr.Item("compindex").ToString
            End If
            dr.Close()
        End If
        fuid = lblfuid.Value

        cqty = txtcQty.Text
        Dim cqtychk As Long
        'Try
        'cqtychk = System.Convert.ToInt32(cqty)
        'Catch ex As Exception
        'Dim strMessage As String =  tmod.getmsg("cdstr242" , "PMTaskDivFunc.aspx.vb")

        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End Try

        pf = txtpfint.Text

        If cin Is Nothing Then
            cin = "0"
        End If
        cn = ddcomp.SelectedItem.ToString
        cn = tasks.ModString2(cn)

        tl = lbltasklev.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value

        Dim ttid As String = lbltaskid.Value

        sql = "update pmTasks set " _
        + "taskdesc = '" & des & "', " _
        + "qty = '" & qty & "', " _
        + "tTime = '" & tr & "', " _
        + "rdid = '" & eqs & "', " _
        + "rd = '" & eqsstr & "', " _
        + "skillid = '" & ski & "', " _
        + "skill = '" & skistr & "', " _
        + "ptid = '" & pt & "', " _
        + "pretech = '" & ptstr & "', " _
        + "freqid = '" & fre & "', " _
        + "freq = '" & frestr & "', " _
        + "lotoid = '" & lot & "', " _
        + "conid = '" & cs & "', " _
        + "ttid = '" & typ & "', " _
        + "tasktype = '" & typstr & "', " _
        + "comid = '" & ci & "', " _
        + "compindex = '" & cin & "', " _
        + "compnum = '" & cn & "', " _
        + "fm1 = '" & fm & "', " _
        + "cqty = '" & cqty & "', " _
        + "rdt = '" & rdt & "', " _
        + "revised = getDate() " _
        + "where " & Filter & " and pmtskid = '" & ttid & "'"
        'Try
        Try
            tasks.Update(sql)
        Catch ex As Exception
            tasksadd.Update(sql)
        End Try
        sid = lblsid.Value
        sql = "usp_updateTaskDDIndexes '" & ttid & "', '" & sid & "'"
        Try
            tasks.Update(sql)
        Catch ex As Exception
            tasksadd.Update(sql)
        End Try
        PageNumber = lblpg.Text
        fuid = lblfuid.Value
        sql = "update pmTasks set " _
                + "rdid = '" & eqs & "', " _
                + "rd = '" & eqsstr & "', " _
                + "skillid = '" & ski & "', " _
                + "skill = '" & skistr & "', " _
                + "ptid = '" & pt & "', " _
                + "pretech = '" & ptstr & "', " _
                + "freqid = '" & fre & "', " _
                + "freq = '" & frestr & "', " _
                + "lotoid = '" & lot & "', " _
                + "conid = '" & cs & "', " _
                + "ttid = '" & typ & "', " _
                + "tasktype = '" & typstr & "', " _
                + "comid = '" & ci & "', " _
                + "compindex = '" & cin & "', " _
                + "compnum = '" & cn & "', " _
                + "cqty = '" & cqty & "', " _
                + "pfinterval = '" & pf & "', " _
                + "revised = getDate() " _
                + "where " & Filter & " and tasknum = '" & PageNumber & "' " _
                + "and funcid = '" & fuid & "' and subtask <> '0'"
        'Try
        Try
            tasks.Update(sql)
        Catch ex As Exception
            tasksadd.Update(sql)
        End Try

        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        eqid = lbleqid.Value
        '**** Added for PM Manager
        Dim hpm As String = lblhaspm.Value
        If hpm = "1" Then
            sql = "update equipment set modifieddate = getDate(), hasmod = 1, modifiedby = '" & ustr & "' " _
        + "where eqid = '" & eqid & "'"
        Else
            sql = "update equipment set modifieddate = getDate(), modifiedby = '" & ustr & "' " _
        + "where eqid = '" & eqid & "'"
        End If

        Try
            tasks.Update(sql)
        Catch ex As Exception
            tasksadd.Update(sql)
        End Try
        eqid = lbleqid.Value
        Try
            tasks.UpMod(eqid)
        Catch ex As Exception
            tasksadd.UpMod(eqid)
        End Try

        Dim currcnt As String = lblcurrsb.Value
        Dim cscnt As String = lblcurrcs.Value
        If st = 0 Then
            PageNumber = lblpg.Text
        Else
            PageNumber = currcnt + cscnt
        End If
        'question this
        field = "funcid"
        val = fuid
        Filter = field & " = " & val '& " and tasknum = " & PageNumber
        'end question
        LoadPage(PageNumber, Filter)
        lblenable.Value = "1"
        'lblsave.Value = "yes"
        'Catch ex As Exception
        'Dim strMessage As String =  tmod.getmsg("cdstr243" , "PMTaskDivFunc.aspx.vb")

        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'End Try
    End Sub

    Private Sub SaveTaskComp()
        ro = lblro.Value
        If ro <> "1" Then
            Dim comp As String = ddcomp.SelectedValue.ToString
            Dim compd As String = ddcomp.SelectedItem.ToString
            If ddcomp.SelectedIndex <> 0 Then
                lblco.Value = comp
                chk = comp
                ttid = lbltaskid.Value
                fuid = lblfuid.Value
                Try
                    tasks.Open()
                    sql = "sp_insertComp '" & comp & "', '" & compd & "', '" & ttid & "', '" & fuid & "'"
                    tasks.Update(sql)
                    PopCompFailList(comp)
                    PopFailList(comp)
                    PopTaskFailModes(comp)
                    UpdateFailStats(comp)
                    eqid = lbleqid.Value
                    tasks.UpMod(eqid)
                Catch ex As Exception

                End Try
                Try
                    tasks.Dispose()
                Catch ex As Exception

                End Try
            Else
                Try
                    ddcomp.SelectedValue = lblco.Value
                Catch ex As Exception

                End Try
            End If
        End If

    End Sub
    Private Sub ddcomp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcomp.SelectedIndexChanged
        SaveTaskComp()
    End Sub

    Private Sub CheckComp(ByVal comp As String)
        ttid = lblt.Value
    End Sub

    Private Sub PopComp()
        fuid = lblfuid.Value
        sql = "select comid, compnum + ' - ' + compdesc as compnum " _
        + "from components where func_id = '" & fuid & "' order by crouting"

        sql = "select comid, compnum + ' - ' + isnull(compdesc, '') as compnum " _
         + "from components where func_id = '" & fuid & "' order by crouting"

        dr = tasks.GetRdrData(sql)
        ddcomp.DataSource = dr
        ddcomp.DataTextField = "compnum"
        ddcomp.DataValueField = "comid"
        ddcomp.DataBind()
        dr.Close()
        ddcomp.Items.Insert(0, New ListItem("Select"))
        ddcomp.Items(0).Value = 0
    End Sub
    Private Sub PopFailList(ByVal comp As String)
        ttid = lbltaskid.Value
        'sql = "select count(*) " _
        '+ "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
        '+ "select failid from pmtaskfailmodes where comid = '" & comp & "' " _
        '+ "and parentfailid is not null)" ' and taskid = '" & ttid & "')"
        'Dim ercnt As Integer
        'ercnt = tasks.Scalar(sql)
        'If ercnt = 0 Then
        sql = "select compfailid, failuremode " _
        + "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
        + "select failid from pmtaskfailmodes where comid = '" & comp & "')" ' and taskid = '" & ttid & "')"
        'Else
        'sql = "select compfailid, failuremode " _
        '+ "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
        '+ "select failid from pmtaskfailmodes where comid = '" & comp & "' " _
        '+ "and parentfailid is null)" ' and taskid = '" & ttid & "')"
        'End If
        sql = "usp_getcfall_tskna '" & comp & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try
        lbfaillist.DataSource = dr
        lbfaillist.DataTextField = "failuremode"
        lbfaillist.DataValueField = "compfailid"
        lbfaillist.DataBind()
        dr.Close()
    End Sub

    Private Sub PopCompFailList(ByVal comp As String)
        ttid = lbltaskid.Value
        sql = "select compfailid, failuremode " _
         + "from componentfailmodes where comid = '" & comp & "'"
        sql = "usp_getcfall_co '" & comp & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        lbCompFM.DataSource = dr
        lbCompFM.DataTextField = "failuremode"
        lbCompFM.DataValueField = "compfailid"
        lbCompFM.DataBind()
        dr.Close()
    End Sub
    Private Sub PopTaskFailModes(ByVal comp As String)
        ttid = lbltaskid.Value
        'sql = "select * from pmtaskfailmodes where taskid = '" & ttid & "'"
        sql = "select * from pmtaskfailmodes where comid = '" & comp & "' and taskid = '" & ttid & "'"
        sql = "usp_getcfall_tsk '" & comp & "','" & ttid & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try
        lbfailmodes.DataSource = dr
        lbfailmodes.DataTextField = "failuremode"
        lbfailmodes.DataValueField = "failid"
        lbfailmodes.DataBind()
        dr.Close()
    End Sub
    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        tasks.Open()
        fuid = lblfuid.Value
        sql = "select comid, compnum " _
        + "from components where func_id = '" & fuid & "'"
        dr = tasks.GetRdrData(sql)
        ddcomp.DataSource = dr
        ddcomp.DataTextField = "compnum"
        ddcomp.DataValueField = "comid"
        ddcomp.DataBind()
        dr.Close()
        ddcomp.Items.Insert(0, New ListItem("Select"))
        ddcomp.Items(0).Value = 0
        tasks.Dispose()
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        tasks.Open()
        fuid = lblfuid.Value
        sql = "select comid, compnum " _
        + "from components where func_id = '" & fuid & "'"
        dr = tasks.GetRdrData(sql)
        ddcomp.DataSource = dr
        ddcomp.DataTextField = "compnum"
        ddcomp.DataValueField = "comid"
        ddcomp.DataBind()
        dr.Close()
        ddcomp.Items.Insert(0, New ListItem("Select"))
        ddcomp.Items(0).Value = 0
        tasks.Dispose()
        co = lblco.Value
        If Len(co) <> 0 Then
            ddcomp.SelectedValue = co
        End If
    End Sub


    Private Sub GetItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Dim fcnt, fcnt2 As Integer
        If oaid <> "0" Then
            sql = "select count(*) from pmTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and oaid = '" & oaid & "'"
        Else
            sql = "select count(*) from pmTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and (oaid is null or oaid = '0')"
        End If

        fcnt = tasks.Scalar(sql)
        sql = "select count(*) from pmTaskFailModes where taskid = '" & ttid & "'"
        fcnt2 = tasks.Scalar(sql)

        If fcnt2 >= 5 Then
            Dim strMessage As String = tmod.getmsg("cdstr408", "PMOptTasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf fcnt > 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr409", "PMOptTasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Try
                sql = "sp_addTaskFailureMode " & ttid & ", " & fi & ", '" & f & "', '" & comp & "', 'dev','" & oaid & "'"
                tasks.Update(sql)
                Dim Item As ListItem
                Dim fm As String
                Dim ipar As Integer = 0
                For Each Item In lbfailmodes.Items
                    f = Item.Text.ToString
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                    If Len(fm) = 0 Then
                        fm = f & "(___)"
                    Else
                        fm += " " & f & "(___)"
                    End If
                Next
                fm = tasks.ModString2(fm)
                sql = "update pmtasks set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
                tasks.Update(sql)

            Catch ex As Exception

            End Try
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
        End If
    End Sub
    Private Sub RemItems(ByVal f As String, ByVal fi As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Try
            sql = "sp_delTaskFailureMode '" & ttid & "', '" & fi & "','" & oaid & "'"
            tasks.Update(sql)
            Dim fm As String
            Dim Item As ListItem
            Dim ipar As Integer = 0
            For Each Item In lbfailmodes.Items
                f = Item.Text.ToString
                ipar = f.LastIndexOf("(")
                If ipar <> -1 Then
                    f = Mid(f, 1, ipar)
                End If
                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
            Next
            fm = tasks.ModString2(fm)
            sql = "update pmtasks set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
            tasks.Update(sql)
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub UpdateFailStats(ByVal comp As String)
        Dim fmcnt, fmcnta As Integer
        sql = "select count(*) from ComponentFailModes where comid = '" & comp & "'"
        Try
            fmcnt = tasks.Scalar(sql)
        Catch ex As Exception
            fmcnt = tasksadd.Scalar(sql)
        End Try
        sql = "select count(distinct failid) from pmTaskFailModes where comid = '" & comp & "'"
        Try
            fmcnta = tasks.Scalar(sql)
        Catch ex As Exception
            fmcnta = tasksadd.Scalar(sql)
        End Try

        fmcnta = fmcnt - fmcnta
    End Sub
    Private Sub ibToTask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibToTask.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        ttid = lbltaskid.Value
        Dim comp As String = ddcomp.SelectedValue.ToString
        tasks.Open()
        Dim tst As String
        For Each Item In lbfaillist.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetItems(f, fi, comp, oa)
            End If
        Next

        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            UpdateFM()
            'LoadPage(PageNumber, Filter) 'test
            'SaveTask()
            'lblenable.Value = "0"
            lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            'PopComp()
            Filter = lblfilt.Value

            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub
    Private Sub UpdateFM()
        Dim ttid As String = lbltaskid.Value
        sql = "usp_UpdateFM1 '" & ttid & "'"
        tasks.Update(sql)
    End Sub
    Private Sub ibReuse_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibReuse.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0

        ttid = lbltaskid.Value
        Dim comp As String = ddcomp.SelectedValue.ToString
        tasks.Open()

        For Each Item In lbCompFM.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            lbltaskid.Value = ttid
            'SaveTask()
            'lblenable.Value = "0"
            PageNumber = lblpg.Text
            Filter = lblfilt.Value
            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try

        tasks.Dispose()
    End Sub
    Private Sub ibFromTask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibFromTask.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        ttid = lbltaskid.Value
        Dim comp As String = ddcomp.SelectedValue.ToString
        tasks.Open()

        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                RemItems(f, fi, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            UpdateFM()
            'LoadPage(PageNumber, Filter) 'test
            'SaveTask()
            'lblenable.Value = "0"
            lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            'PopComp()
            Filter = lblfilt.Value

            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub

    Private Sub AddFail()
        Dim comp As String = lblco.Value '= colblcoid.Value
        tasks.Open()
        PopCompFailList(comp)
        PopFailList(comp)
        PopTaskFailModes(comp)
        tasks.Dispose()
    End Sub
    Private Sub btnAddFail_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim comp As String = lblco.Value
        tasks.Open()
        PopFailList(comp)
        tasks.Dispose()
    End Sub

    Private Sub btndeltask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btndeltask.Click
        PageNumber = lblpg.Text
        Filter = lblfilt.Value
        fuid = lblfuid.Value
        tnum = lblt.Value
        Dim st As Integer = lblsb.Value
        Dim scnt As String = lblsubcount.Text
        Dim pmtskid As String = lbltaskid.Value
        'sql = "sp_delPMTask '" & fuid & "', '" & tnum & "', '" & st & "'"
        sql = "usp_delpmtask '" & fuid & "', '" & pmtskid & "', '" & tnum & "'"
        tasks.Open()
        tasks.Update(sql)
        tskcnt = lblcnt.Text
        tskcnt = tskcnt - 1
        lblcnt.Text = tskcnt
        If PageNumber <= tskcnt Then
            PageNumber = PageNumber
        Else
            PageNumber = tskcnt
        End If
        If PageNumber = 0 Then
            PageNumber = 1
        End If
        eqid = lbleqid.Value
        tasks.UpMod(eqid)

        LoadPage(PageNumber, Filter)
        tasks.Dispose()
    End Sub


    Private Sub btnaddsubtask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnaddsubtask.Click
        lblenable.Value = "0"
        Dim tid, typ, fre, rmt, pt, ski, qty, tr, eqs, lot, cs, ci, cn, cin As String
        typ = ddtype.SelectedValue
        Try
            fre = txtfreq.Text
        Catch ex As Exception
            fre = lblfreq.Value
        End Try
        If fre = "" Then
            fre = lblfreq.Value
        End If
        pt = ddpt.SelectedValue
        ski = ddskill.SelectedValue
        qty = txtqty.Text
        If Len(qty) = 0 Then
            qty = "1"
        End If
        tr = txttr.Text
        Dim trchk As Long
        Try
            trchk = System.Convert.ToInt32(tr)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr246", "PMTaskDivFunc.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        eqs = ddeqstat.SelectedValue
        If cbloto.Checked = True Then
            lot = "1"
        Else
            lot = "0"
        End If
        If cbcs.Checked = True Then
            cs = "1"
        Else
            cs = "0"
        End If
        Filter = lblfilt.Value
        Dim ts As String
        Dim tn As Integer
        ts = lblst.Value
        tn = lblt.Value
        fuid = lblfuid.Value
        Dim comp As String = ddcomp.SelectedValue.ToString
        If ts = "0" Then
            tasks.Open()
            Dim stcnt As Integer
            sql = "Select count(*) from pmTasks " _
            + "where " & Filter & " and taskNum = '" & tn & "' and subTask <> '0'"
            stcnt = tasks.Scalar(sql)
            Dim newtst As String = stcnt + 1 '((stcnt * 0.1) + 0.1)

            sql = "usp_addSubTask '" & tn & "', '" & newtst & "', '" & fuid & "', '" & comp & "', " _
            + "'" & qty & "', '" & tr & "', '" & eqs & "', '" & ski & "', '" & pt & "', '" & fre & "', " _
            + "'" & lot & "', '" & cs & "', '" & typ & "'"
            tasks.Update(sql)

            Dim scnt, cscnt As Integer
            cscnt = lblsb.Value
            Filter = lblfilt.Value
            PageNumber = lblpg.Text
            'tasks.Open()
            scnt = SubCount(PageNumber, Filter)
            If scnt > 0 Then
                cscnt = cscnt + 1
                If cscnt <= scnt Then
                    lblsb.Value = cscnt
                    PageNumber = PageNumber + cscnt
                    LoadPage(PageNumber, Filter)
                    'lblspg.Text = cscnt
                    lblsubcount.Text = scnt
                Else
                    tasks.Dispose()
                    Dim strMessage As String = tmod.getmsg("cdstr247", "PMTaskDivFunc.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If
        Else

        End If
    End Sub


    Private Sub ibCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibCancel.Click
        tasks.Open()
        GetLists()
        Filter = lblfilt.Value
        LoadPage(PageNumber, Filter)
    End Sub


    Private Sub btntpm_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntpm.Click
        tasks.Open()
        UpTPM()
        Filter = lblfilt.Value
        PageNumber = lblpg.Text
        LoadPage(PageNumber, Filter)
        tasks.Dispose()
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label25.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "Label25")
        Catch ex As Exception
        End Try
        Try
            Label26.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "Label26")
        Catch ex As Exception
        End Try
        Try
            lang247.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang247")
        Catch ex As Exception
        End Try
        Try
            lang248.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang248")
        Catch ex As Exception
        End Try
        Try
            lang249.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang249")
        Catch ex As Exception
        End Try
        Try
            lang250.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang250")
        Catch ex As Exception
        End Try
        Try
            lang251.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang251")
        Catch ex As Exception
        End Try
        Try
            lang252.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang252")
        Catch ex As Exception
        End Try
        Try
            lang253.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang253")
        Catch ex As Exception
        End Try
        Try
            lang254.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang254")
        Catch ex As Exception
        End Try
        Try
            lang255.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang255")
        Catch ex As Exception
        End Try
        Try
            lang256.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang256")
        Catch ex As Exception
        End Try
        Try
            lang257.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang257")
        Catch ex As Exception
        End Try
        Try
            lang258.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang258")
        Catch ex As Exception
        End Try
        Try
            lang259.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang259")
        Catch ex As Exception
        End Try
        Try
            lang260.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang260")
        Catch ex As Exception
        End Try
        Try
            lang261.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang261")
        Catch ex As Exception
        End Try
        Try
            lang262.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang262")
        Catch ex As Exception
        End Try
        Try
            lang263.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang263")
        Catch ex As Exception
        End Try
        Try
            lang264.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang264")
        Catch ex As Exception
        End Try
        Try
            lang265.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang265")
        Catch ex As Exception
        End Try
        Try
            lang266.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang266")
        Catch ex As Exception
        End Try
        Try
            lang267.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang267")
        Catch ex As Exception
        End Try
        Try
            lang268.Text = axlabs.GetASPXPage("PMTaskDivFunc.aspx", "lang268")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddcomp.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "btnaddcomp") & "')")
            btnaddcomp.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            btnaddnewfail.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "btnaddnewfail") & "')")
            btnaddnewfail.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            btnpfint.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "btnpfint") & "')")
            btnpfint.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ggrid.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "ggrid") & "')")
            ggrid.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            img1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "img1") & "')")
            img1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img4.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "Img4") & "', ABOVE, LEFT)")
            Img4.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img5.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "Img5") & "', ABOVE, LEFT)")
            Img5.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgcopycomp.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "imgcopycomp") & "')")
            imgcopycomp.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgdeltpm.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "imgdeltpm") & "', ABOVE, LEFT)")
            imgdeltpm.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid13.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "ovid13") & "', ABOVE, LEFT)")
            ovid13.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid14.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "ovid14") & "')")
            ovid14.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid15.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "ovid15") & "')")
            ovid15.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid16.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "ovid16") & "')")
            ovid16.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid17.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "ovid17") & "')")
            ovid17.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid18.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "ovid18") & "')")
            ovid18.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid19.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "ovid19") & "')")
            ovid19.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid20.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "ovid20") & "')")
            ovid20.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid21.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "ovid21") & "')")
            ovid21.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid22.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "ovid22") & "')")
            ovid22.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid23.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "ovid23") & "')")
            ovid23.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid24.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "ovid24") & "')")
            ovid24.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            sgrid.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMTaskDivFunc.aspx", "sgrid") & "')")
            sgrid.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
