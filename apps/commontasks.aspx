<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="commontasks.aspx.vb" Inherits="lucy_r12.commontasks" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>commontasks</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="javascript" type="text/javascript" src="../scripts/pmtaskdivfunc_1016f.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/commontasksaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td class="label" width="90">
                <asp:Label ID="lang196" runat="server">Task Type</asp:Label>
            </td>
            <td width="170">
                <asp:ListBox ID="ddtype" runat="server" Width="160px" CssClass="plainlabel" Rows="1"
                    DataValueField="ttid" DataTextField="tasktype" AutoPostBack="True"></asp:ListBox>
            </td>
            <td class="plainlabelblue" width="400">
                <input id="rbtt" type="checkbox" checked runat="server"><asp:Label ID="lang197" runat="server">Return Task Type with Task</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang198" runat="server">PdM Tech</asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddpt" runat="server" Width="160px" CssClass="plainlabel" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td class="plainlabelblue">
                <input id="rbpt" type="checkbox" checked runat="server"><asp:Label ID="lang199" runat="server">Return PdM with Task</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang200" runat="server">Keyword/Phrase</asp:Label>
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtkey" runat="server" Width="300"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="plainlabelblue" colspan="2">
                <input id="rbc" onclick="hidepm();" type="radio" checked name="rb" runat="server"><asp:Label
                    ID="lang201" runat="server">Search Common Tasks</asp:Label><input id="rbt" onclick="showpm();"
                        type="radio" name="rb" runat="server"><asp:Label ID="lang202" runat="server">Search PM Tasks</asp:Label>
            </td>
            <td align="right">
                <img onclick="refreshscreen();" src="../images/appbuttons/minibuttons/refreshit.gif"
                    onmouseover="return overlib('Reset Screen', ABOVE, LEFT)" onmouseout="return nd()">&nbsp;&nbsp;<asp:ImageButton
                        ID="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                    </asp:ImageButton>&nbsp;&nbsp;
            </td>
        </tr>
        <tr class="details" id="tdpm" runat="server">
            <td colspan="3">
                <table cellspacing="0">
                    <tr height="22">
                        <td colspan="4" class="btmmenu plainlabel">
                            <asp:Label ID="lang203" runat="server">PM Task Lookup Options</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="180">
                            <asp:Label ID="lang204" runat="server">Component Name</asp:Label>
                        </td>
                        <td width="180">
                            <asp:TextBox ID="txtcompnum" runat="server"></asp:TextBox>
                        </td>
                        <td width="20">
                            <img onmouseover="return overlib('Lookup Component Name', ABOVE, LEFT)" onclick="filtlook('comp','txtcompnum','compnum');"
                                onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                        <td class="label" valign="top" align="center" width="280" rowspan="6">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="label" align="center">
                                        <asp:Label ID="lang205" runat="server">Failure Modes</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" align="center">
                                        <asp:ListBox ID="lbfailmaster" runat="server" Width="170px" SelectionMode="Multiple"
                                            Height="140px"></asp:ListBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="lang206" runat="server">Component Description</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtcompdesc" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <img onmouseover="return overlib('Lookup Component Description', ABOVE, LEFT)" onclick="filtlook('comp','txtcompdesc','compdesc');"
                                onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="lang207" runat="server">Component Designation</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtcompdesig" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <img onmouseover="return overlib('Lookup Component Designation', ABOVE, LEFT)" onclick="filtlook('comp','txtcompdesig','desig');"
                                onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="lang208" runat="server">Component Special Identifier</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtcompspl" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <img onmouseover="return overlib('Lookup Component Special Identifier', ABOVE, LEFT)"
                                onclick="filtlook('comp','txtcompspl','spl');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="lang209" runat="server">Function Name</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtfunc" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <img onmouseover="return overlib('Lookup Function Name', ABOVE, LEFT)" onclick="filtlook('func','txtfunc','func');"
                                onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="lang210" runat="server">Function Special Identifier</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtfuncspl" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <img onmouseover="return overlib('Lookup Function Special Identifier', ABOVE, LEFT)"
                                onclick="filtlook('func','txtfuncspl','spl');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td id="tdtasks" colspan="3" runat="server">
            </td>
        </tr>
    </table>
    <div class="details" id="ptndiv" style="border-right: black 1px solid; border-top: black 1px solid;
        z-index: 999; border-left: black 1px solid; width: 500px; border-bottom: black 1px solid;
        height: 220px">
        <table cellspacing="0" cellpadding="0" width="500" bgcolor="white">
            <tr bgcolor="blue" height="20">
                <td class="labelwht">
                    <asp:Label ID="lang211" runat="server">Add/Edit Description</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closeptn();" height="18" alt="" src="../images/close.gif" width="18"><br>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="txtmemo" runat="server" Width="500px" CssClass="plainlabel" Height="200px"
                        TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <img onmouseover="return overlib('Add To Common Tasks', ABOVE, LEFT)" onclick="addtask();"
                        onmouseout="return nd()" src="../images/appbuttons/minibuttons/addnew.gif" id="ibaddnew"
                        runat="server">
                    <img id="imgsavecommon" onmouseover="return overlib('Save Changes to This Common Task', ABOVE, LEFT)"
                        onclick="savetask();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/savedisk1.gif"
                        runat="server">
                    <img id="ir1" runat="server" onmouseover="return overlib('Add Task and Return This Task to my PM Task Screen', ABOVE, LEFT)"
                        onclick="returntask('add');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/return2grn.gif">
                    <img id="ir2" runat="server" onmouseover="return overlib('Save Changes and Return This Task to my PM Task Screen', ABOVE, LEFT)"
                        onclick="returntask('save');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/return2.gif">
                    <img id="ir3" runat="server" onmouseover="return overlib('Return This Task to my PM Task Screen', ABOVE, LEFT)"
                        onclick="returntask('return');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/return2red.gif">
                </td>
            </tr>
        </table>
    </div>
    <div class="details" id="lstdiv" style="border-right: black 1px solid; border-top: black 1px solid;
        z-index: 999; border-left: black 1px solid; width: 330px; border-bottom: black 1px solid;
        height: 220px">
        <table cellspacing="0" cellpadding="0" width="330">
            <tr bgcolor="blue" height="20">
                <td class="labelwht">
                    <asp:Label ID="lang212" runat="server">List Dialog</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closelst();" height="18" alt="" src="../images/close.gif" width="18"><br>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <iframe id="iflst" style="width: 330px; height: 220px" runat="server"></iframe>
                </td>
            </tr>
        </table>
    </div>
    <iframe class="details" id="ifptn" style="width: 500px; height: 246px" src="#"></iframe>
    <input id="lblcid" type="hidden" runat="server">
    <input id="lbltasktype" type="hidden" runat="server">
    <input id="lblpdm" type="hidden" runat="server">
    <input id="lbltaskid" type="hidden" runat="server">
    <input id="lblmode" type="hidden" runat="server">
    <input id="lblreturn" type="hidden" runat="server">
    <input id="lblorigtasktype" type="hidden" runat="server"><input id="lblorigpdm" type="hidden"
        runat="server">
    <input id="lblsubmit" type="hidden" runat="server">
    <input type="hidden" id="lblctaskid" runat="server">
    <input type="hidden" id="lblscreen" runat="server"><input type="hidden" id="lblexit"
        runat="server">
    <input type="hidden" id="lbltyp" runat="server"><input type="hidden" id="lblmemo"
        runat="server">
    <input type="hidden" id="lblsid" runat="server"><input type="hidden" id="lbltaskholdid"
        runat="server">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
