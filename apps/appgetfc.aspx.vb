Imports System.Data.SqlClient
Imports System.Text
Public Class appgetfc
    Inherits System.Web.UI.Page
    Dim ec As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sql As String
    Dim sid, coid, typ, fi, fm, chk, wonum, isecd, isone As String
    Dim mu As New mmenu_utils_a
    Dim comi As New mmenu_utils_a
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfids As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfms As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcfids As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcfms As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltotcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcfcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rball As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbsite As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbcomp As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents lblremids As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddoca As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lbloaid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblocarea As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisecd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisone As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhasone As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdtab As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trsave As System.Web.UI.HtmlControls.HtmlTableRow

    Protected WithEvents lbloca As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcfmr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsfm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents langafm As Global.System.Web.UI.WebControls.Label
    Protected WithEvents langsfm As Global.System.Web.UI.WebControls.Label
    Protected WithEvents langcfm As Global.System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        Dim coi As String = comi.COMPI
        Dim lang As String = lblfslang.Value
        poplang(lang)

        If Not IsPostBack Then
            isecd = mu.ECD
            isone = mu.COMPI
            lblisecd.Value = isecd
            lblisone.Value = isone
            If isecd = "1" Then
                rball.Disabled = True
                rball.Disabled = True

            End If


            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            coid = Request.QueryString("coid").ToString
            lblcoid.Value = coid
            wonum = Request.QueryString("wo").ToString
            lblwonum.Value = wonum
            ec.Open()
            chk = CheckFail(sid)
            popoca()
            If coid <> "" Then
                typ = "co"
                tdec.Attributes.Add("class", "details")
                tdec1.Attributes.Add("class", "details")
                tdec2.Attributes.Add("class", "view")
                ddoca.Enabled = True
                rbcomp.Checked = True
            Else
                typ = "fm"
                rbcomp.Disabled = True
                If chk = "site" Then
                    tdec.Attributes.Add("class", "details")
                    tdec1.Attributes.Add("class", "view")
                    tdec2.Attributes.Add("class", "details")
                    rbsite.Checked = True
                    ddoca.Enabled = False
                Else
                    tdec.Attributes.Add("class", "view")
                    tdec1.Attributes.Add("class", "details")
                    tdec2.Attributes.Add("class", "details")
                    rball.Checked = True
                    ddoca.Enabled = False
                End If

            End If
            lbltyp.Value = typ
            lbltotcnt.Value = "0"
            lblfcnt.Value = "0"
            lblcfcnt.Value = "0"

            If typ = "fm" Then
                If chk = "site" Then
                    PopFail(sid, coid, "fm", "site")
                Else
                    rbsite.Disabled = True
                    PopFail(sid, coid, "fm", "open")
                End If
            ElseIf typ = "co" Then
                PopFail(sid, coid, "co", chk)
            End If
            PopCurrent(wonum)
            ec.Dispose()
        Else
            If Request.Form("lblsubmit") = "savewo" Then
                lblsubmit.Value = ""
                ec.Open()
                savewo()
                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "remit" Then
                lblsubmit.Value = ""
                Dim chk As String = lblfailchk.Value
                ec.Open()
                Dim remit As Integer
                remit = checkrem()
                sid = lblsid.Value
                coid = lblcoid.Value
                'PopFail(sid, coid, "fm", "open")
                If chk = "site" Then
                    PopFail(sid, coid, "fm", "site")
                Else
                    rbsite.Disabled = True
                End If
                If chk = "all" Then
                    PopFail(sid, coid, "fm", "open")
                End If
                If chk = "co" Then
                    PopFail(sid, coid, "co", "chk")
                End If
                chk = CheckFail(sid)
                'PopFail(sid, coid, "co", chk)
                PopCurrent(wonum)
                ec.Dispose()
                trsave.Attributes.Add("class", "view")
            ElseIf Request.Form("lblsubmit") = "co" Then
                lblsubmit.Value = ""
                ec.Open()
                sid = lblsid.Value
                coid = lblcoid.Value
                PopFail(sid, coid, "co", chk)
                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "open" Then
                lblsubmit.Value = ""
                ec.Open()
                sid = lblsid.Value
                coid = lblcoid.Value
                PopFail(sid, coid, "fm", "open")
                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "site" Then
                lblsubmit.Value = ""
                ec.Open()
                sid = lblsid.Value
                coid = lblcoid.Value
                PopFail(sid, coid, "fm", "site")
                ec.Dispose()
            End If

        End If

    End Sub
    Private Sub poplang(ByVal lang As String)
        If lang = "fre" Then
            lbloca.InnerHtml = "Zone d'évènement"
            tdtab.InnerHtml = "Modes de défaillance"
            tdcfmr.InnerHtml = "Cliquez sur le mode de défaillance pour le retirer"
            tdsfm.InnerHtml = "Modes de défaillance sélectionnés"
            langafm.Text = "Tous les modes de défaillance"
            langsfm.Text = "Modes de défaillance du composant"
            langcfm.Text = "Modes de défaillance sélectionnés"

        End If
    End Sub
    Private Sub popoca()
        sql = "select * from ocareas order by ocarea"
        dr = ec.GetRdrData(sql)
        ddoca.DataSource = dr
        ddoca.DataTextField = "ocarea"
        ddoca.DataValueField = "oaid"
        ddoca.DataBind()
        dr.Close()
        isecd = lblisecd.Value
        If isecd <> "1" Then
            ddoca.Items.Insert(0, "ALL")
        Else
            ddoca.Items.Insert(0, "NONE")
            'ddoca.SelectedIndex = 0
            'wonum = lblwonum.Value
            'PopCurrent(wonum)
        End If

    End Sub
    Private Function checkrem() As Integer
        wonum = lblwonum.Value
        Dim fids As String = lblremids.Value
        Dim fidslen = Len(fids)
        If fidslen > 0 Then
            Dim fiarr() As String = fids.Split(",")
            Dim i As Integer
            For i = 0 To fiarr.Length - 1
                fi = fiarr(i).ToString
                Dim fiarr2() As String = fi.Split("-")
                fi = fiarr2(0).ToString
                sql = "delete from wofailmodes1 where wonum = '" & wonum & "' and failid = '" & fi & "'"
                ec.Update(sql)
                sql = "delete from wofailout where wonum = '" & wonum & "'"
                ec.Update(sql)
            Next
        End If
        lblremids.Value = ""
        Return fidslen
    End Function
    Private Sub savewo()
        Dim remit As Integer
        Dim oaid, ocarea As String
        'remit = checkrem()
        wonum = lblwonum.Value
        coid = lblcoid.Value
        oaid = lbloaid.Value
        ocarea = lblocarea.Value

        isone = lblisone.Value

        Dim fids As String = lblfids.Value
        Dim fms As String = lblfms.Value
        Dim fiarr() As String = fids.Split(",")
        Dim fmarr() As String = fms.Split(",")


        Dim e1id, e2id, e3id As String
        e1id = ""
        e2id = ""
        e3id = ""

        Dim fcnt, fcnt2 As Integer
        Dim fmstr As String
        Dim i As Integer
        Dim ipar As Integer = 0
       
        For i = 0 To fiarr.Length - 1
            fi = fiarr(i).ToString
            fm = fmarr(i).ToString
            ipar = fm.LastIndexOf("(")
            If ipar <> -1 Then
                fm = Mid(fm, 1, ipar)
            End If
            Dim fiarr2() As String = fi.Split("-")
            fi = fiarr2(0).ToString
            Try
                oaid = fiarr2(1)
            Catch ex As Exception

            End Try
            If fm <> "" And fi <> "0" And fi <> "" Then
                If isone = "NISS" Then
                    sql = "delete from wofailmodes1 where wonum = '" & wonum & "'"
                    ec.Update(sql)
                End If
                sql = "select count(*) from wofailmodes1 where wonum = '" & wonum & "' and failid = '" & fi & "'"
                fcnt = ec.Scalar(sql)
                If fcnt > 0 Then

                    'Dim strMessage As String = tmod.getmsg("cdstr565", "wofail.aspx.vb")
                    'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    'Exit Sub
                Else
                    Try
                        If coid <> "" Then


                            typ = "hascomp"
                            sql = "usp_addWoFailureMode " & wonum & ", " & fi & ", '" & fm & "', '" & coid & "','" & typ & "','" & oaid & "','" & ocarea & "'"
                        Else
                            sql = "insert into wofailmodes1 (wonum, failid, failuremode) values " _
                            + "('" & wonum & "','" & fi & "','" & fm & "')"
                        End If
                        ec.Update(sql)

                    Catch ex As Exception

                    End Try
                End If
            End If

        Next
        If remit = 0 Then
            sql = "delete from wofailout where wonum = '" & wonum & "'"
            ec.Update(sql)
        End If
        Dim ii As Integer
        For ii = 0 To fiarr.Length - 1
            fi = fiarr(ii).ToString
            fm = fmarr(ii).ToString
            fm = Replace(fm, "'", Chr(180), , , vbTextCompare)
            If Len(fmstr) = 0 Then
                fmstr = fm & "(___)"
            Else
                fmstr += " " & fm & "(___)"
            End If
        Next
        'If fcnt <> 1 Then
        'sql = "update wofailout set failout = '" & fm & "' where wonum = '" & wonum & "'"
        'Else
        sql = "insert into wofailout (wonum, failout) values " _
        + "('" & wonum & "', '" & fm & "')"
        'End If
        ec.Update(sql)
        fms = lblfms.Value
        lblsubmit.Value = "return"

    End Sub
    Private Function CheckFail(ByVal sid As String) As String
        Dim chk As String = lblfailchk.Value
        Dim scnt As Integer
        If chk = "" Then
            sql = "select count(*) from pmSiteFM where siteid = '" & sid & "'"
            scnt = ec.Scalar(sql)
            If scnt = 0 Then
                lblfailchk.Value = "open"
                chk = "open"
            Else
                lblfailchk.Value = "site"
                chk = "site"
            End If
        End If
        Return chk
    End Function
    Private Sub PopCurrent(ByVal wonum As String)
        sql = "select failid, failuremode from wofailmodes1 where wonum = '" & wonum & "' order by failuremode"
        dr = ec.GetRdrData(sql)
        Dim sb As New StringBuilder
        Dim sflg As Integer = 0
        Dim totcnt As Integer = 0
        While dr.Read
            totcnt += 1
            fi = dr.Item("failid").ToString
            fm = dr.Item("failuremode").ToString
            If sflg = 0 Then
                sflg = 1
                sb.Append("<a href=""#"" onclick=""remit('" & fi & "','" & fm & "');"">" & fm & "</a>")
                lblfids.Value = fi
                lblfms.Value = fm
            Else
                sb.Append("<br><a href=""#"" onclick=""remit('" & fi & "','" & fm & "');"">" & fm & "</a>")
                lblfids.Value += "," & fi
                lblfms.Value += "," & fm
            End If
        End While
        dr.Close()
        Dim fms As String = lblfms.Value
        tdecd1.InnerHtml = sb.ToString
        isone = lblisone.Value
        If isone = "NISS" And totcnt > 0 Then
            lblhasone.Value = "1"
        Else
            lblhasone.Value = "0"
        End If
    End Sub
    Private Sub PopFail(ByVal sid As String, ByVal coid As String, ByVal typ As String, ByVal chk As String, Optional ByVal oaid As String = "0")
        Dim lang As String = lblfslang.Value
        Dim coi As String = comi.COMPI
        'TEMP
        'If lang = "fre" And coi = "LAU" Then
        'lang = "eng"
        'End If
        'lang = "eng"
        If typ = "co" Then
            If oaid = "0" Then

                sql = "usp_getcfall_co '" & coid & "','" & lang & "'"
            Else
                sql = "select compfailid, failuremode " _
           + "from componentfailmodes where comid = '" & coid & "' and oaid = '" & oaid & "' order by failuremode"


            End If

        Else
            If chk = "open" Then
                If lang = "fre" Then
                    sql = "select failid, fmeng from failuremodes order by failuremode"
                Else
                    sql = "select failid, failuremode from failuremodes order by failuremode"
                End If

            ElseIf chk = "site" Then
                sql = "select failid, failuremode from pmSiteFM where siteid = '" & sid & "' order by failuremode"
            Else
                Exit Sub
            End If
        End If
        dr = ec.GetRdrData(sql)
        Dim sb As New StringBuilder
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 250px;  HEIGHT: 280px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
        sb.Append("<table width=""230"" cellpadding=""1"">")
        sb.Append("<tr>")
        sb.Append("<td width=""230""></td>")
        sb.Append("</tr>")
        While dr.Read
            If typ = "co" Then 'oaid = "0" And 
                fi = dr.Item("compfailid").ToString
            Else
                fi = dr.Item("failid").ToString
            End If

            fm = dr.Item("failuremode").ToString
            sb.Append("<tr>")
            If typ = "fm" Then
                sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getecd('" & fi & "','" & fm & "','yes');"">" & fm & "</td>")
            Else
                sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getecd('" & fi & "','" & fm & "','yes');"">" & fm & "</td>")
            End If
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table>")

        tdec.Attributes.Add("class", "details")
        tdec1.Attributes.Add("class", "details")
        tdec2.Attributes.Add("class", "details")

        If typ = "fm" And chk = "open" Then
            tdec.InnerHtml = sb.ToString
            tdec.Attributes.Add("class", "view")
        ElseIf typ = "fm" And chk = "site" Then
            tdec1.InnerHtml = sb.ToString
            tdec1.Attributes.Add("class", "view")
        Else
            tdec2.InnerHtml = sb.ToString
            tdec2.Attributes.Add("class", "view")
        End If
    End Sub

    Private Sub ddoca_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddoca.SelectedIndexChanged
        Dim oaid, ocarea As String
        lbloaid.Value = ""
        lblocarea.Value = ""
        If ddoca.SelectedIndex <> 0 And ddoca.SelectedIndex <> -1 Then
            oaid = ddoca.SelectedValue.ToString
            ocarea = ddoca.SelectedItem.ToString
            lbloaid.Value = oaid
            lblocarea.Value = ocarea
            ec.Open()
            sid = lblsid.Value
            coid = lblcoid.Value
            wonum = lblwonum.Value
            chk = CheckFail(sid)
            PopFail(sid, coid, "co", chk, oaid)
            PopCurrent(wonum)
            ec.Dispose()

            tdec.Attributes.Add("class", "view")
            tdec1.Attributes.Add("class", "details")
            tdec2.Attributes.Add("class", "view")
            ddoca.Enabled = True
            rbcomp.Checked = True
        End If

    End Sub
End Class
