<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMTaskDivFuncGrid.aspx.vb"
    Inherits="lucy_r12.PMTaskDivFuncGrid" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMTaskDivFuncGrid</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMTaskDivFuncGridaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        <!--
        function addtask() {
            document.getElementById("lblsubmit").value = "addnew";
            document.getElementById("form1").submit();
        }
        function addtask2() {
            document.getElementById("lblsubmit").value = "addnewcomp";
            document.getElementById("form1").submit();
        }

//-->
    </script>
</head>
<body class="tbg" onload="checkcnt();">
    <form id="form1" method="post" runat="server">
    <div style="z-index: 100; position: absolute; height: 380px; overflow: auto; top: 0px;
        left: 0px">
        <table width="742" cellspacing="0">
            <tbody>
                <tr>
                    <td class="thdrsinglft" align="left" width="26">
                        <img src="../images/appbuttons/minibuttons/3gearsh.gif" border="0">
                    </td>
                    <td class="thdrsingrt label" width="674">
                        <asp:Label ID="lang269" runat="server">Current Tasks</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" id="tdaddtask" runat="server">
                        <table>
                            <tr>
                                <td width="20">
                                    <img src="../images/appbuttons/minibuttons/addnew.gif" border="0" onclick="addtask();">
                                </td>
                                <td class="bluelabel" width="80">
                                    <asp:Label ID="lang1042" runat="server">Add Task</asp:Label>
                                </td>
                                <td width="20">
                                    <img id="imgaddcomp" class="details" onclick="addtask2();" border="0" src="../images/appbuttons/minibuttons/addnew.gif"
                                        runat="server">
                                </td>
                                <td class="bluelabel" width="382">
                                    <asp:Label ID="lbladdcomp" runat="server" Visible="False">Add Task to Component</asp:Label>
                                </td>
                                <td id="tdsort" class="plainlabelblue" width="200" align="right" runat="server">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Repeater ID="rptrtasks" runat="server">
                            <HeaderTemplate>
                                <table cellspacing="1" width="680">
                                    <tr class="tbg" height="26">
                                        <td class="thdrsingg plainlabel" width="80">
                                            <asp:LinkButton OnClick="SortTasks" ID="lang270" runat="server">Task#</asp:LinkButton>
                                        </td>
                                        <td class="thdrsingg plainlabel" width="180">
                                            <asp:LinkButton OnClick="SortCompTasks" ID="lang271" runat="server">Component Addressed</asp:LinkButton>
                                        </td>
                                        <td class="thdrsingg plainlabel" width="320">
                                            <asp:Label ID="lang272" runat="server">Task Description</asp:Label>
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="tbg" height="20">
                                    <td class="plainlabel">
                                        &nbsp;
                                        <asp:LinkButton ID="lbltn" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"tasknum")%>'
                                            runat="server">
                                        </asp:LinkButton>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"taskdesc")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lbleqiditem" Text='<%# DataBinder.Eval(Container.DataItem,"pmtskid")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="transrowblue" height="20">
                                    <td class="plainlabel transrowblue">
                                        &nbsp;
                                        <asp:LinkButton ID="lbltnalt" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"tasknum")%>'
                                            runat="server">
                                        </asp:LinkButton>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        <asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        <asp:Label ID="Label3" Text='<%# DataBinder.Eval(Container.DataItem,"taskdesc")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lbleqidalt" Text='<%# DataBinder.Eval(Container.DataItem,"pmtskid")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <input id="lblsid" type="hidden" name="lblsid" runat="server"><input id="lbltaskid"
        type="hidden" name="lbltaskid" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
    <input id="lbldid" type="hidden" name="lbldid" runat="server">
    <input id="lblclid" type="hidden" name="lblclid" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblfilt" type="hidden" name="lblfilt" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server">
    <input id="taskcnt" type="hidden" runat="server"><input id="appchk" type="hidden"
        name="appchk" runat="server">
    <input id="lblcoid" type="hidden" runat="server"><input type="hidden" id="lbltyp"
        runat="server" name="lbltyp">
    <input type="hidden" id="lbllid" runat="server" name="lbllid">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server"><input type="hidden" id="lblcurrsort"
        runat="server" name="lblcurrsort">
    <input type="hidden" id="lblcompnum" runat="server">
    </form>
</body>
</html>
