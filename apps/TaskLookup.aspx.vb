

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class TaskLookup
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, Login, ro As String
    Dim Tasks As New Utilities
    Protected WithEvents tdlookup As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try

        If Not IsPostBack Then
            Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                If ro = "1" Then
                    'pgtitle.InnerHtml = "Task Lookup (Read Only)"
                Else
                    'pgtitle.InnerHtml = "Task Lookup"
                End If
            Catch ex As Exception

            End Try
            Dim sb As New System.Text.StringBuilder
            Dim str As String = Request.QueryString("str").ToString
            sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""100%"">")
            sql = "select distinct taskdesc from pmtasks where taskdesc like '%" & str & "%'"
            Tasks.Open()
            dr = Tasks.GetRdrData(sql)
            While dr.Read
                sb.Append("<tr><td class=""suggest""><a href=""#"" class=""suggest"" onclick=""handleexit1(this.innerHTML);"">" & dr("taskdesc").ToString() & "</a></td></tr>")
            End While
            dr.Close()
            Tasks.Dispose()
            sb.Append("</Table></div>")
            tdlookup.InnerHtml = sb.ToString
        End If
        
    End Sub

End Class
