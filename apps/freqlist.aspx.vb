

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class freqlist
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dval As New Utilities
    Dim dr As SqlDataReader
    Dim vid, vind, val, pmtskid, fld, sid As String
    Protected WithEvents divval As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblpmtskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfld As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblvid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblvind As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            pmtskid = Request.QueryString("pmtskid").ToString
            fld = Request.QueryString("fld").ToString
            sid = Request.QueryString("sid").ToString
            lblpmtskid.Value = pmtskid
            lblfld.Value = fld
            lblsid.Value = sid
            dval.Open()
            If fld = "freq" Then
                GetFreq()
            ElseIf fld = "comp" Then
                GetComp()
            ElseIf fld = "skill" Then
                GetSkill()
            ElseIf fld = "pretech" Then
                GetPreTech()
            ElseIf fld = "tasktype" Then
                GetTaskType()
            ElseIf fld = "rd" Then
                GetRD()
            End If
            dval.Dispose()
        Else
            If Request.Form("lblsubmit") = "saveit" Then
                lblsubmit.Value = ""
                fld = lblfld.Value
                dval.Open()
                Select Case fld
                    Case "freq"
                        SaveFreq()
                    Case "comp"
                        SaveComp()
                    Case "skill"
                        SaveSkill()
                    Case "pretech"
                        SavePreTech()
                    Case "tasktype"
                        SaveTaskType()
                    Case "rd"
                        SaveRD()
                End Select
                dval.Dispose()
            End If
        End If
    End Sub
    Private Sub GetFreq()
        Dim sb As New StringBuilder
        sb.Append("<table>")
        pmtskid = lblpmtskid.Value
        sid = lblsid.Value

        sql = "select freqid, freq, freqindex from pmFrequencies where freqid <> 0 and freq <> 'Select' order by freqindex"
        dr = dval.GetRdrData(sql)
        While dr.Read
            vid = dr.Item("freqid").ToString
            vind = dr.Item("freqindex").ToString
            val = dr.Item("freq").ToString
            sb.Append("<tr><td class=""plainlabel"">")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getval('" & vid & "','" & val & "','" & vind & "')"">")
            sb.Append(val & "</a>")
            sb.Append("</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        divval.InnerHtml = sb.ToString
    End Sub
    Private Sub SaveFreq()
        pmtskid = lblpmtskid.Value
        sid = lblsid.Value
        vid = lblvid.Value
        val = lblval.Value
        vind = lblvind.Value
        sql = "update pmtasks set freq = '" & val & "', freqid = '" & vid & "', freqindex = '" & vind & "' " _
        + "where pmtskid = '" & pmtskid & "'"
        dval.Update(sql)
        lblsubmit.Value = "go"

    End Sub
    Private Sub GetComp()
        sid = lblsid.Value
        Dim sb As New StringBuilder
        sql = "select comid, compnum from components where func_id = '" & sid & "' and compnum <> 'Select' order by crouting"
        dr = dval.GetRdrData(sql)
        sb.Append("<table>")
        While dr.Read
            vid = dr.Item("comid").ToString
            val = dr.Item("compnum").ToString
            vind = "0"
            sb.Append("<tr><td class=""plainlabel"">")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getval('" & vid & "','" & val & "','" & vind & "')"">")
            sb.Append(val & "</a>")
            sb.Append("</td></tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        divval.InnerHtml = sb.ToString
    End Sub
    Private Sub SaveComp()
        pmtskid = lblpmtskid.Value
        sid = lblsid.Value
        vid = lblvid.Value
        val = lblval.Value
        vind = lblvind.Value
        sql = "sp_insertComp '" & vid & "', '" & val & "', '" & pmtskid & "', '" & sid & "'"
        'sql = "update pmtasks set compnum = '" & val & "', comid = '" & vid & "', compindex = '" & vind & "' " _
        '+ "where pmtskid = '" & pmtskid & "'"
        dval.Update(sql)
        lblsubmit.Value = "cgo"

    End Sub
    Private Sub GetSkill()
        Dim cid As String = "0"
        sid = lblsid.Value
        Dim sb As New StringBuilder
        Dim scnt, scnt2 As Integer
        sql = "select count(*) from pmSiteSkills where (compid = '" & cid & "' and siteid = '" & sid & "') " 'or skillindex = '0'"

        scnt = dval.Scalar(sql)
       

        If scnt > 1 Then
            sql = "select count(*) from pmsiteskills where skill = 'Select' and compid = '" & cid & "'" ' and siteid = '" & sid & "'"

            scnt2 = dval.Scalar(sql)
          
            sql = "select skillid, skill, skillindex from pmSiteSkills where (compid = '" & cid & "' and siteid = '" & sid & "')" '  order by skillindex" 'or skillindex = '0'


            dr = dval.GetRdrData(sql)
            sb.Append("<table>")
            While dr.Read
                vid = dr.Item("skillid").ToString
                val = dr.Item("skill").ToString
                vind = "0"
                sb.Append("<tr><td class=""plainlabel"">")
                sb.Append("<a class=""A1"" href=""#"" onclick=""getval('" & vid & "','" & val & "','" & vind & "')"">")
                sb.Append(val & "</a>")
                sb.Append("</td></tr>")

            End While
            dr.Close()
            sb.Append("</table>")
            divval.InnerHtml = sb.ToString
        Else
            sql = "select skillid, skill, skillindex from pmSkills where skill <> 'Select' and compid = '" & cid & "' " 'or skillindex = '0'" ' order by skillindex"
            dr = dval.GetRdrData(sql)
            sb.Append("<table>")
            While dr.Read
                vid = dr.Item("skillid").ToString
                val = dr.Item("skill").ToString
                vind = "0"
                sb.Append("<tr><td class=""plainlabel"">")
                sb.Append("<a class=""A1"" href=""#"" onclick=""getval('" & vid & "','" & val & "','" & vind & "')"">")
                sb.Append(val & "</a>")
                sb.Append("</td></tr>")

            End While
            dr.Close()
            sb.Append("</table>")
            divval.InnerHtml = sb.ToString
        End If
    End Sub
    Private Sub SaveSkill()
        pmtskid = lblpmtskid.Value
        If pmtskid <> "TT" Then
            sid = lblsid.Value
            vid = lblvid.Value
            val = lblval.Value
            vind = lblvind.Value
            sql = "update pmtasks set skill = '" & val & "', skillid = '" & vid & "', skillindex = '" & vind & "' " _
            + "where pmtskid = '" & pmtskid & "'"
            dval.Update(sql)
            lblsubmit.Value = "go"
        Else
            lblsubmit.Value = "cgo"
        End If
       
    End Sub
    Private Sub GetPreTech()
        Dim cid As String = "0"
        Dim sb As New StringBuilder
        sql = "select ptid, pretech, ptindex from pmPreTech where (compid = '" & cid & "' or ptindex = '0') and pretech <> 'Select'" ' order by ptindex"
        dr = dval.GetRdrData(sql)
        sb.Append("<table>")
        While dr.Read
            vid = dr.Item("ptid").ToString
            val = dr.Item("pretech").ToString
            vind = "0"
            sb.Append("<tr><td class=""plainlabel"">")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getval('" & vid & "','" & val & "','" & vind & "')"">")
            sb.Append(val & "</a>")
            sb.Append("</td></tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        divval.InnerHtml = sb.ToString
    End Sub
    Private Sub SavePreTech()
        pmtskid = lblpmtskid.Value
        sid = lblsid.Value
        vid = lblvid.Value
        val = lblval.Value
        vind = lblvind.Value
        sql = "update pmtasks set pretech = '" & val & "', ptid = '" & vid & "', ptindex = '" & vind & "' " _
        + "where pmtskid = '" & pmtskid & "'"
        dval.Update(sql)
        lblsubmit.Value = "go"
    End Sub
    Private Sub GetTaskType()
        Dim cid As String = "0"
        Dim sb As New StringBuilder
        sql = "select ttid, tasktype, taskindex from pmTaskTypes where compid = '" & cid & "' or taskindex = '0' and tasktype <> 'Select'" ' order by taskindex"
        dr = dval.GetRdrData(sql)
        sb.Append("<table>")
        While dr.Read
            vid = dr.Item("ttid").ToString
            val = dr.Item("tasktype").ToString
            vind = "0"
            sb.Append("<tr><td class=""plainlabel"">")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getval('" & vid & "','" & val & "','" & vind & "')"">")
            sb.Append(val & "</a>")
            sb.Append("</td></tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        divval.InnerHtml = sb.ToString
    End Sub
    Private Sub SaveTaskType()
        pmtskid = lblpmtskid.Value
        sid = lblsid.Value
        vid = lblvid.Value
        val = lblval.Value
        vind = lblvind.Value
        sql = "update pmtasks set tasktype = '" & val & "', ttid = '" & vid & "', taskindex = '" & vind & "' " _
        + "where pmtskid = '" & pmtskid & "'"
        dval.Update(sql)
        If vid <> "7" Then
            sql = "update pmtasks set pretech = '" & System.DBNull.Value & "', ptid = '" & System.DBNull.Value & "', ptindex = '" & System.DBNull.Value & "' " _
            + "where pmtskid = '" & pmtskid & "'"
            dval.Update(sql)
        End If
        lblsubmit.Value = "cgo"
    End Sub
    Private Sub GetRD()
        Dim cid As String = "0"
        Dim sb As New StringBuilder
        sql = "select statid, status, statusindex from pmStatus where compid = '" & cid & "' or statusindex = '0' and status <> 'Select'" ' order by statusindex"
        dr = dval.GetRdrData(sql)
        sb.Append("<table>")
        While dr.Read
            vid = dr.Item("statid").ToString
            val = dr.Item("status").ToString
            vind = "0"
            sb.Append("<tr><td class=""plainlabel"">")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getval('" & vid & "','" & val & "','" & vind & "')"">")
            sb.Append(val & "</a>")
            sb.Append("</td></tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        divval.InnerHtml = sb.ToString
    End Sub
    Private Sub SaveRD()
        pmtskid = lblpmtskid.Value
        If pmtskid <> "TT" Then
            sid = lblsid.Value
            vid = lblvid.Value
            val = lblval.Value
            vind = lblvind.Value
            sql = "update pmtasks set rd = '" & val & "', rdid = '" & vid & "', rdindex = '" & vind & "' " _
            + "where pmtskid = '" & pmtskid & "'"
            dval.Update(sql)
            lblsubmit.Value = "go"
        Else
            lblsubmit.Value = "cgo"
        End If
       
    End Sub
End Class
