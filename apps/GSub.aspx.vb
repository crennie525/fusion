

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class GSub
    Inherits System.Web.UI.Page
    Protected WithEvents lang228 As System.Web.UI.WebControls.Label

    Protected WithEvents lang227 As System.Web.UI.WebControls.Label

    Protected WithEvents lang226 As System.Web.UI.WebControls.Label

    Protected WithEvents lang225 As System.Web.UI.WebControls.Label

    Protected WithEvents lang224 As System.Web.UI.WebControls.Label

    Protected WithEvents lang223 As System.Web.UI.WebControls.Label

    Protected WithEvents lang222 As System.Web.UI.WebControls.Label

    Protected WithEvents lang221 As System.Web.UI.WebControls.Label

    Protected WithEvents lang220 As System.Web.UI.WebControls.Label

    Protected WithEvents lang219 As System.Web.UI.WebControls.Label

    Protected WithEvents lang218 As System.Web.UI.WebControls.Label

    Protected WithEvents lang217 As System.Web.UI.WebControls.Label

    Protected WithEvents lang216 As System.Web.UI.WebControls.Label

    Protected WithEvents lang215 As System.Web.UI.WebControls.Label

    Protected WithEvents lang214 As System.Web.UI.WebControls.Label

    Protected WithEvents lang213 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sid, cid, fuid, tid, sql, username, eqid, ro As String
    Dim Filter, SubVal As String
    Dim ds As DataSet
    Dim dslev As DataSet
    Dim dr As SqlDataReader
    Dim gtasks As New Utilities
    Dim Login As String
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dgtasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdtnum As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tddesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdskill As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdqty As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmin As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdrd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdrdt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Datagrid1 As System.Web.UI.WebControls.DataGrid
    Protected WithEvents addtask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbloldtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltool As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllube As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnote As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dlhd As System.Web.UI.WebControls.DataList
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskstatus As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasktype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillqty As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrd As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load



        GetDGLangs()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
            username = HttpContext.Current.Session("username").ToString()
            lblusername.Value = username
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                lblro.Value = ro
                If ro = "1" Then
                    dgtasks.Columns(0).Visible = False
                    dgtasks.Columns(10).Visible = False
                    addtask.ImageUrl = "../images/appbuttons/bgbuttons/addtaskdis.gif"
                    addtask.Enabled = False
                End If

                tid = Request.QueryString("tid").ToString
                lbltid.Value = tid
                If Len(tid) <> 0 AndAlso tid <> "" AndAlso tid <> "0" Then
                    cid = Request.QueryString("cid").ToString
                    lblcid.Value = cid
                    fuid = Request.QueryString("fuid").ToString
                    lblfuid.Value = fuid
                    sid = Request.QueryString("sid").ToString
                    lblsid.Value = sid
                    eqid = Request.QueryString("eqid").ToString
                    lbleqid.Value = eqid
                    Filter = "funcid = '" & fuid & "' and tasknum = '" & tid & "'"
                    lblfilt.Value = Filter
                    SubVal = "(compid, funcid, tasknum, subtask) values ('" & cid & "', '" & fuid & "', "
                    lblsubval.Value = SubVal
                    gtasks.Open()
                    BindTaskHead()
                    BindHead()
                    BindGrid()
                    Dim lock, lockby As String
                    Dim user As String = lblusername.Value
                    lock = CheckLock(eqid)
                    If lock = "1" Then
                        lockby = lbllockedby.Value
                        If lockby = user Then
                            lbllock.Value = "0"
                        Else
                            Dim strMessage As String = tmod.getmsg("cdstr203", "GSub.aspx.vb") & " " & lockby & "."
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            addtask.Attributes.Add("class", "details")
                        End If
                    ElseIf lock = "0" Then
                        'LockRecord(user, eq)
                    End If
                    gtasks.Dispose()
                    lbltool.Value = "no"
                    lblpart.Value = "no"
                    lbllube.Value = "no"
                    lblnote.Value = "no"
                Else
                    Dim strMessage As String = tmod.getmsg("cdstr204", "GSub.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "noeqid"
                End If

            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr205", "GSub.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "noeqid"
            End Try

        End If

        'btnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'btnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Function CheckLock(ByVal eqid As String) As String
        Dim lock As String
        sql = "select locked, lockedby from equipment where eqid = '" & eqid & "'"
        Try
            dr = gtasks.GetRdrData(sql)
            While dr.Read
                lock = dr.Item("locked").ToString
                lbllock.Value = dr.Item("locked").ToString
                lbllockedby.Value = dr.Item("lockedby").ToString
            End While
            dr.Close()
        Catch ex As Exception

        End Try
        Return lock
    End Function
    Private Sub BindHead()
        sql = "usp_getTaskHead '" & fuid & "', '" & tid & "'"
        dr = gtasks.GetRdrData(sql)
        dlhd.DataSource = dr
        dlhd.DataBind()
        dr.Close()
    End Sub
    Private Sub BindTaskHead()
        sql = "select * from pmtasks where funcid = '" & fuid & "' and tasknum = '" & tid & "' and subtask = 0"
        dr = gtasks.GetRdrData(sql)
        While dr.Read
            tdtnum.InnerHtml = dr.Item("tasknum").ToString
            tddesc.InnerHtml = dr.Item("taskdesc").ToString
            tdskill.InnerHtml = dr.Item("skill").ToString
            tdqty.InnerHtml = dr.Item("qty").ToString
            tdmin.InnerHtml = dr.Item("ttime").ToString
            tdrd.InnerHtml = dr.Item("rd").ToString
            tdrdt.InnerHtml = dr.Item("rdt").ToString
            lbltasktype.Value = dr.Item("tasktype").ToString
            lbltaskstatus.Value = dr.Item("taskstatus").ToString
        End While
        dr.Close()
    End Sub
    Private Sub BindGrid()
        fuid = lblfuid.Value
        tid = tdtnum.InnerHtml
        sql = "select * from pmtasks where funcid = '" & fuid & "' and tasknum = '" & tid & "' " _
        + "and subtask <> 0 order by subtask"
        ds = gtasks.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Try
            dgtasks.DataSource = dv
            dgtasks.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Public Function PopulateFail(ByVal comid As String) As DataSet
        cid = lblcid.Value
        sql = "select failid, failuremode from componentfailmodes where comid = '" & comid & "' or failindex = '0' order by failuremode"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Public Function PopulatePreTech() As DataSet
        cid = lblcid.Value
        sql = "select ptid, pretech, ptindex from pmPreTech where compid = '" & cid & "' or ptindex = '0' order by ptindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Public Function PopulateComp() As DataSet
        fuid = lblfuid.Value
        sql = "select * from components where func_id = '" & fuid & "' or compindex = '0' order by compindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Public Function PopulateTaskTypes() As DataSet
        cid = lblcid.Value
        sql = "select ttid, tasktype, taskindex from pmTaskTypes where compid = '" & cid & "' or taskindex = '0' order by taskindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function

    Public Function PopulateStatus() As DataSet
        cid = lblcid.Value
        sql = "select statid, status, statusindex from pmStatus where compid = '" & cid & "' or statusindex = '0' order by statusindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    
    Public Function PopulateSkills() As DataSet
        cid = lblcid.Value
        sid = lblsid.Value
        sql = "select skillid, skill, skillindex from pmSiteSkills where (compid = '" & cid & "' and siteid = '" & sid & "') or skillindex = '0'" 'order by skillindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function
    Function PopulateCompFM(ByVal comp As String)
        If comp <> "0" Then
            sql = "select failid, failuremode " _
                     + "from componentfailmodes where comid = '" & comp & "'"
            dslev = gtasks.GetDSData(sql)
            Return dslev
        End If

    End Function
    Function PopulateFL(ByVal comp As String)
        If comp <> "0" Then
            sql = "select failid, failuremode " _
                    + "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
                    + "select failid from pmtaskfailmodes where comid = '" & comp & "')"
            dslev = gtasks.GetDSData(sql)
            Return dslev
        End If

    End Function
    Function PopulateTaskFM(ByVal comp As String, ByVal ttid As String)
        If comp <> "0" Then
            sql = "select * from pmtaskfailmodes where taskid = '" & ttid & "'"
            dslev = gtasks.GetDSData(sql)
            Return dslev
        End If
    End Function

    Private Sub addtask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles addtask.Click

        fuid = lblfuid.Value
        tid = tdtnum.InnerHtml 'lbltid.Value '
        gtasks.Open()
        Dim stcnt As Integer
        sql = "Select count(*) from pmTasks " _
        + "where funcid = '" & fuid & "' and tasknum = '" & tid & "' and subtask <> 0"
        stcnt = gtasks.Scalar(sql)
        Dim newtst As String = stcnt + 1
        Dim dcnt As Integer
        sql = "Select count(*) from pmTasks " _
        + "where funcid = '" & fuid & "' and tasknum = '" & tid & "' and subtask = '" & newtst & "'"
        dcnt = gtasks.Scalar(sql)
        If dcnt = 0 Then
            sql = "insert into pmtasks (compid, siteid, deptid, cellid, eqid, funcid, tasknum, subtask, " _
        + "skillid, skillindex, skill, qty, freqid, freqindex, freq, rdid, rd, rdindex, taskstatus, ptid, " _
        + "ptindex, pretech, tasktype, comid, compnum, rteid, rteseq) select top 1 " _
        + "compid, siteid, deptid, cellid, eqid, funcid, '" & tid & "', '" & newtst & "', " _
        + "skillid, skillindex, skill, qty, freqid, freqindex, freq, rdid, rd, rdindex, taskstatus, " _
        + "ptid, ptindex, pretech, tasktype, comid, compnum, rteid, rteseq from pmtasks " _
        + "where funcid = '" & fuid & "' and tasknum = '" & tid & "' and subtask = 0"
            gtasks.Update(sql)
        End If
        
        eqid = lbleqid.Value
        gtasks.UpMod(eqid)
        BindGrid()
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.EditCommand
        Dim lock As String = lbllock.Value
        If lock <> "1" Then
            addtask.Enabled = False
            gtasks.Open()
            lbloldtask.Value = CType(e.Item.FindControl("lblsubt"), Label).Text
            dgtasks.EditItemIndex = e.Item.ItemIndex
            BindGrid()
            gtasks.Dispose()
        End If


    End Sub

    Private Sub dgtasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.CancelCommand
        addtask.Enabled = True
        gtasks.Open()
        dgtasks.EditItemIndex = -1
        BindGrid()
        gtasks.Dispose()
    End Sub
    Private Sub dgtasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtasks.ItemDataBound
        If e.Item.ItemType = ListItemType.EditItem Then
            'Try
            Dim qty As String = DataBinder.Eval(e.Item.DataItem, "qty").ToString
            Dim rdid As String = DataBinder.Eval(e.Item.DataItem, "rdid").ToString
            Dim tqty As TextBox = CType(e.Item.FindControl("txtqty"), TextBox)
            Dim trdt As TextBox = CType(e.Item.FindControl("txtrdt"), TextBox)
            If qty = "" Or qty = "1" Then
                tqty.Enabled = False
                qty = "1"
            End If
            lblskillqty.Value = qty
            If rdid <> "2" Then
                trdt.Enabled = False
                lblrd.Value = "no"
            Else
                lblrd.Value = "yes"
            End If
            'Catch ex As Exception

            'End Try

        End If
    End Sub
    Private Sub dgtasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.UpdateCommand
        addtask.Enabled = True
        Dim tn, otn, txt, qty, dt, desc, st, tid As String
        'qty = CType(e.Item.FindControl("txtqty"), TextBox).Text
        txt = CType(e.Item.FindControl("txttr"), TextBox).Text
        dt = CType(e.Item.FindControl("txtdt"), TextBox).Text
        desc = CType(e.Item.FindControl("txtdesc"), TextBox).Text
        desc = gtasks.ModString2(desc)
        'If Len(qty) = 0 Then
        'qty = "1"
        'End If
        If Len(desc) > 500 Then
            Dim strMessage As String = tmod.getmsg("cdstr206", "GSub.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If

        st = CType(e.Item.FindControl("lblst"), TextBox).Text
        Dim stnchk As Long
        Try
            stnchk = System.Convert.ToInt64(st)
        Catch ex As Exception
            Dim strMessage As String = "Sub Task Number Must be a Non Decimal Numeric Value"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        tid = CType(e.Item.FindControl("lblttid"), Label).Text
        Dim qt As String = lblskillqty.Value
        Dim qtychk, qtchk As Long
        'If qt <> "qty" Then
        'Try
        'qtychk = System.Convert.ToInt64(qty)
        'Try
        'qtchk = System.Convert.ToInt64(qt)
        'If qtychk > qtchk Then
        'Dim strMessage As String = "Skill Qty is greater than specified in the top level Task"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'Else
        'qty = qtychk
        'End If
        'Catch ex As Exception
        'qty = "1"
        'End Try
        'Catch ex As Exception
        'Dim strMessage As String = tmod.getmsg("cdstr207", "GSub.aspx.vb")
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        ' Try
        'Else
        'qty = "1"
        'End If

        Dim txtchk As Long
        Try
            txtchk = System.Convert.ToDecimal(txt)
        Catch ex As Exception
            'Dim strMessage As String = tmod.getmsg("cdstr208", "GSub.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
        End Try
        txt = "0"

        Dim rd As String = lblrd.Value
        Dim dtchk As Long
        If rd = "yes" Then
            Try
                dtchk = System.Convert.ToDecimal(dt)
            Catch ex As Exception
                'Dim strMessage As String = tmod.getmsg("cdstr209", "GSub.aspx.vb")

                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                'Exit Sub
            End Try
        Else
            dt = "0"
        End If
        dt = "0"

        Dim tasktype, taskstatus As String
        tasktype = lbltasktype.Value
        taskstatus = lbltaskstatus.Value

        sql = "update pmtasks set " _
        + "ttime = '" & txt & "', " _
        + "rdt = '" & dt & "', " _
        + "taskdesc = '" & desc & "', " _
        + "tasktype = '" & tasktype & "', " _
        + "taskstatus = '" & taskstatus & "' " _
        + "where pmtskid = '" & tid & "'"
        gtasks.Open()
        gtasks.Update(sql)
        tn = tdtnum.InnerHtml
        otn = lbloldtask.Value
        If otn <> st Then
            fuid = lblfuid.Value
            sql = "usp_reorderPMSubTasks2 '" & tid & "', '" & fuid & "', '" & tn & "', '" & st & "', '" & otn & "'"
            gtasks.Update(sql)
        End If
        eqid = lbleqid.Value
        gtasks.UpMod(eqid)
        dgtasks.EditItemIndex = -1
        BindGrid()
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.DeleteCommand
        Dim tnum, snum
        'ListItemType.SelectedItem()
        Try
            tnum = CType(e.Item.FindControl("lblta"), Label).Text
            snum = CType(e.Item.FindControl("lblsubt"), Label).Text
        Catch ex As Exception
            tnum = CType(e.Item.FindControl("lblt"), TextBox).Text
            snum = CType(e.Item.FindControl("lblst"), TextBox).Text
        End Try
        'If e.Item.ItemType = ListItemType.Item Then

        'ElseIf e.Item.ItemType = ListItemType.EditItem Then

        'End If

        fuid = lblfuid.Value
        'sql = "sp_delPMTask '" & fuid & "', '" & tnum & "', '" & snum & "'"
        sql = "usp_delPMSubTask '" & fuid & "', '" & tnum & "', '" & snum & "'"
        gtasks.Open()
        gtasks.Update(sql)
        eqid = lbleqid.Value
        gtasks.UpMod(eqid)
        Try
            dgtasks.EditItemIndex = -1
        Catch ex As Exception

        End Try
        BindGrid()
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.ItemCommand
        If e.CommandName = "Tool" Then
            lbltool.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If

        End If
        If e.CommandName = "Part" Then
            lblpart.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        If e.CommandName = "Lube" Then
            lbllube.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        If e.CommandName = "Note" Then
            lblnote.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
    End Sub






    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgtasks.Columns(0).HeaderText = dlabs.GetDGPage("GSub.aspx", "dgtasks", "0")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(2).HeaderText = dlabs.GetDGPage("GSub.aspx", "dgtasks", "2")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(3).HeaderText = dlabs.GetDGPage("GSub.aspx", "dgtasks", "3")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(5).HeaderText = dlabs.GetDGPage("GSub.aspx", "dgtasks", "5")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(6).HeaderText = dlabs.GetDGPage("GSub.aspx", "dgtasks", "6")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(7).HeaderText = dlabs.GetDGPage("GSub.aspx", "dgtasks", "7")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(8).HeaderText = dlabs.GetDGPage("GSub.aspx", "dgtasks", "8")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang213.Text = axlabs.GetASPXPage("GSub.aspx", "lang213")
        Catch ex As Exception
        End Try
        Try
            lang214.Text = axlabs.GetASPXPage("GSub.aspx", "lang214")
        Catch ex As Exception
        End Try
        Try
            lang215.Text = axlabs.GetASPXPage("GSub.aspx", "lang215")
        Catch ex As Exception
        End Try
        Try
            lang216.Text = axlabs.GetASPXPage("GSub.aspx", "lang216")
        Catch ex As Exception
        End Try
        Try
            lang217.Text = axlabs.GetASPXPage("GSub.aspx", "lang217")
        Catch ex As Exception
        End Try
        Try
            lang218.Text = axlabs.GetASPXPage("GSub.aspx", "lang218")
        Catch ex As Exception
        End Try
        Try
            lang219.Text = axlabs.GetASPXPage("GSub.aspx", "lang219")
        Catch ex As Exception
        End Try
        Try
            lang220.Text = axlabs.GetASPXPage("GSub.aspx", "lang220")
        Catch ex As Exception
        End Try
        Try
            lang221.Text = axlabs.GetASPXPage("GSub.aspx", "lang221")
        Catch ex As Exception
        End Try
        Try
            lang222.Text = axlabs.GetASPXPage("GSub.aspx", "lang222")
        Catch ex As Exception
        End Try
        Try
            lang223.Text = axlabs.GetASPXPage("GSub.aspx", "lang223")
        Catch ex As Exception
        End Try
        Try
            lang224.Text = axlabs.GetASPXPage("GSub.aspx", "lang224")
        Catch ex As Exception
        End Try
        Try
            lang225.Text = axlabs.GetASPXPage("GSub.aspx", "lang225")
        Catch ex As Exception
        End Try
        Try
            lang226.Text = axlabs.GetASPXPage("GSub.aspx", "lang226")
        Catch ex As Exception
        End Try
        Try
            lang227.Text = axlabs.GetASPXPage("GSub.aspx", "lang227")
        Catch ex As Exception
        End Try
        Try
            lang228.Text = axlabs.GetASPXPage("GSub.aspx", "lang228")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                addtask.Attributes.Add("src", "../images2/eng/bgbuttons/addtask.gif")
            ElseIf lang = "fre" Then
                addtask.Attributes.Add("src", "../images2/fre/bgbuttons/addtask.gif")
            ElseIf lang = "ger" Then
                addtask.Attributes.Add("src", "../images2/ger/bgbuttons/addtask.gif")
            ElseIf lang = "ita" Then
                addtask.Attributes.Add("src", "../images2/ita/bgbuttons/addtask.gif")
            ElseIf lang = "spa" Then
                addtask.Attributes.Add("src", "../images2/spa/bgbuttons/addtask.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

   
End Class
