

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class PMTasks
    Inherits System.Web.UI.Page
	Protected WithEvents lang276 As System.Web.UI.WebControls.Label

	Protected WithEvents lang275 As System.Web.UI.WebControls.Label

	Protected WithEvents lang274 As System.Web.UI.WebControls.Label

	Protected WithEvents lang273 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Login, cid, jump, ret As String
    Dim tasklev, sid, did, clid, eqid, fuid, coid, chk, tli, typ, lid, start, ro, task As String
    Dim main As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Protected WithEvents iftaskdet As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents taskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdarch As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblgetarch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifarch As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifimg As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdqs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblretqs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents retqs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpicmode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents geteq As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)

        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            Response.Redirect(url)
        End If
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("PMTasks.aspx?jump=no")
        Catch ex As Exception

        End Try
        Try
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                'pgtitle.InnerHtml = "PM Developer (Read Only)"
            Else
                'pgtitle.InnerHtml = "PM Developer"
            End If
        Catch ex As Exception

        End Try
        sid = Request.Form("lblps")
        If Not IsPostBack Then
            lblpicmode.Value = "asset"
            Try
                start = Request.QueryString("start").ToString
                If start = "yes" Then
                    Dim qs As String = Request.QueryString.ToString
                    retqs.Value = qs
                    Dim lvl As String = Request.QueryString("lvl").ToString
                    If lvl = "eq" Then
                        tdqs.InnerHtml = "<a href='../equip/eqmain2.aspx?" & qs & "' class='A1'>Return to Equipment Screen</a>"
                    ElseIf lvl = "fu" Then
                        tdqs.InnerHtml = "<a href='../equip/eqmain2.aspx?" & qs & "' class='A1'>Return to Function Screen</a>"
                    ElseIf lvl = "co" Then
                        tdqs.InnerHtml = "<a href='../equip/eqmain2.aspx?" & qs & "' class='A1'>Return to Component Screen</a>"
                    End If

                End If
            Catch ex As Exception

            End Try
            'tdarch.InnerHtml = "Waiting for location..."
            Dim cid As String = HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            taskcnt.Value = "0"
            jump = Request.QueryString("jump").ToString
            If jump = "yes" Then
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                lid = Request.QueryString("lid").ToString
                lbllid.Value = lid
                tli = Request.QueryString("tli").ToString
                lbltasklev.Value = "5" 'tli
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                did = Request.QueryString("did").ToString
                lbldid.Value = did
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                clid = Request.QueryString("clid").ToString
                lblclid.Value = clid
                Try
                    fuid = Request.QueryString("funid").ToString
                    lblfuid.Value = fuid
                    coid = Request.QueryString("comid").ToString
                    lblcoid.Value = coid
                Catch ex As Exception

                End Try
                Try
                    task = Request.QueryString("task").ToString
                Catch ex As Exception

                End Try
                chk = Request.QueryString("chk").ToString
                lblchk.Value = chk
                lblgetarch.Value = "yes"
                geteq.Attributes.Add("src", "pmget.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task)
            End If
        Else
            If Page.Request("lblgetarch") = "eq" Then
                lblgetarch.Value = "yes"
                cid = lblcid.Value
                tli = "4"
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = lblcoid.Value
                chk = lblchk.Value
                typ = lbltyp.Value
                lid = lbllid.Value
                geteq.Attributes.Add("src", "pmget.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" + lid + "&typ=" + typ)
            ElseIf Page.Request("lblgetarch") = "fu" Then
                lblgetarch.Value = "yes"
                cid = lblcid.Value
                tli = "5"
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = "" 'lblcoid.Value
                chk = lblchk.Value
                typ = lbltyp.Value
                lid = lbllid.Value
                geteq.Attributes.Add("src", "pmget.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" + lid + "&typ=" + typ)
            ElseIf Page.Request("lblgetarch") = "co" Then
                lblgetarch.Value = "yes"
                cid = lblcid.Value
                tli = "5"
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = lblcoid.Value
                chk = lblchk.Value
                typ = lbltyp.Value
                lid = lbllid.Value
                geteq.Attributes.Add("src", "pmget.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" + lid + "&typ=" + typ)
            End If
            If Request.Form("lblsubmit") = "optit" Then
                lblsubmit.Value = ""
                main.Open()
                ArchiveEq()
                OptIt()
                main.Dispose()
                cid = lblcid.Value
                tli = "4"
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = lblcoid.Value
                chk = lblchk.Value
                typ = lbltyp.Value
                lid = lbllid.Value
                Response.Redirect("../appsopt/PM3OptMain.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" + lid + "&typ=" + typ + "&chk=" + chk)
            ElseIf Request.Form("lblsubmit") = "archit" Then
                lblsubmit.Value = ""
                main.Open()
                ArchiveEq()

                main.Dispose()
                cid = lblcid.Value
                tli = "4"
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = lblcoid.Value
                chk = lblchk.Value
                typ = lbltyp.Value
                lid = lbllid.Value
                geteq.Attributes.Add("src", "pmget.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" + lid + "&typ=" + typ)
            End If
        End If

    End Sub
    Private Sub ArchiveEq()
        cid = lblcid.Value
        lid = lbllid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        sid = lblsid.Value
        eqid = lbleqid.Value
        Dim newid As Integer
        Dim user As String = HttpContext.Current.Session("username").ToString '"PM Administrator"
        Session("username") = user
        Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)
        'usp_copyEqArch] (@cid int, @site int, @dept int, @cell int,
        '@eqid int, @eqnum varchar(50), @user varchar(50), @lid int = null, @tpm varchar(10))
        sql = "usp_copyEqArch '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & ustr & "', '" & lid & "', 'N'"
        newid = main.Scalar(sql)
    End Sub
    Private Sub OptIt()
        Dim user As String = HttpContext.Current.Session("username").ToString '"PM Administrator"
        Session("username") = user
        Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)
        eqid = lbleqid.Value
        sql = "usp_copyTasksToOpt '" & eqid & "', '" & ustr & "'"
        main.Update(sql)
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang273.Text = axlabs.GetASPXPage("PMTasks.aspx", "lang273")
        Catch ex As Exception
        End Try
        Try
            lang274.Text = axlabs.GetASPXPage("PMTasks.aspx", "lang274")
        Catch ex As Exception
        End Try
        Try
            lang275.Text = axlabs.GetASPXPage("PMTasks.aspx", "lang275")
        Catch ex As Exception
        End Try
        Try
            lang276.Text = axlabs.GetASPXPage("PMTasks.aspx", "lang276")
        Catch ex As Exception
        End Try

    End Sub

End Class
