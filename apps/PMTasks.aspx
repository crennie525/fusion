<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMTasks.aspx.vb" Inherits="lucy_r12.PMTasks" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title id="pgtitle">PMTasks</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/taskgrid.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMTasksaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
     <!--
        function handletasks(href, rev) {
            var curr = document.getElementById("lblpicmode").value;
            //alert(href)
            document.getElementById("iftaskdet").src = href;

            if (rev == "dept") {
                checkarch()
                if (curr == "asset") {
                    document.getElementById("ifimg").src = "../equip/eqimages.aspx?typ=no&eqid=0&fuid=0&coid=0";
                }
                else {
                    document.getElementById("ifimg").src = "../pmpics/taskimagepm.aspx?eqid=0&funcid=0&tasknum=0";
                }
            }
            else if (rev == "cell") {
                if (curr == "asset") {
                    document.getElementById("ifimg").src = "../equip/eqimages.aspx?typ=no&eqid=0&fuid=0&coid=0";
                }
                else {
                    document.getElementById("ifimg").src = "../pmpics/taskimagepm.aspx?eqid=0&funcid=0&tasknum=0";
                }
            }

        }
         //-->
    </script>
</head>
<body class="tbg" onload="checkarch();">
    <form id="form1" method="post" runat="server">
    <table style="z-index: 1; position: absolute; top: 83px; left: 7px" cellspacing="0"
        cellpadding="2" width="1020">
        <tr>
            <td id="tdqs" runat="server" colspan="2">
            </td>
        </tr>
        <tr>
            <td class="thdrsinglft" align="left" width="26">
                <img src="../images/appbuttons/minibuttons/3gearsh.gif" border="0">
            </td>
            <td class="thdrsingrt label" width="738">
                <asp:Label ID="lang273" runat="server">Add/Edit PM Tasks</asp:Label>
            </td>
            <td width="3">
                &nbsp;
            </td>
            <td class="thdrsinglft" id="tdnavtop" width="22">
                <img src="../images/appbuttons/minibuttons/eqarch.gif" border="0">
            </td>
            <td class="thdrsingrt label" width="208">
                <asp:Label ID="lang274" runat="server">Asset Hierarchy</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <iframe id="geteq" style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                                border-top-style: none; border-left-style: none" src="pmget.aspx?jump=no" frameborder="no"
                                width="760" scrolling="no" height="132" runat="server" allowtransparency></iframe>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <iframe id="iftaskdet" style="border-bottom-style: none; padding-bottom: 0px; border-right-style: none;
                                background-color: transparent; margin: 0px; padding-left: 0px; padding-right: 0px;
                                border-top-style: none; border-left-style: none; padding-top: 0px" src="PMTaskDivFuncGrid.aspx?start=no"
                                frameborder="no" width="760" scrolling="no" height="440" runat="server" allowtransparency>
                            </iframe>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
            <td valign="top" colspan="2" rowspan="2">
                <table cellspacing="0" cellpadding="1">
                    <tr>
                        <td colspan="2">
                            <iframe id="ifarch" style="border-bottom-style: none; padding-bottom: 0px; border-right-style: none;
                                background-color: transparent; margin: 0px; padding-left: 0px; padding-right: 0px;
                                border-top-style: none; border-left-style: none; padding-top: 0px" src="DevArch.aspx?start=no"
                                frameborder="no" width="260" scrolling="no" height="170" runat="server" allowtransparency>
                            </iframe>
                        </td>
                    </tr>
                    <tr>
                        <td class="thdrsinglft" width="22">
                            <img src="../images/appbuttons/minibuttons/eqarch.gif" border="0">
                        </td>
                        <td class="thdrsingrt" width="227">
                            <a href="#" onclick="getpics('asset');" class="A1black" id="hassets">
                                <asp:Label ID="lang275" runat="server">Asset Images</asp:Label></a>&nbsp;&nbsp;&nbsp;&nbsp;<a
                                    href="#" onclick="getpics('task');" class="A1blue" id="htasks"><asp:Label ID="lang276"
                                        runat="server">Task Images</asp:Label></a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <iframe id="ifimg" style="border-bottom-style: none; padding-bottom: 0px; border-right-style: none;
                                background-color: transparent; margin: 0px; padding-left: 0px; padding-right: 0px;
                                border-top-style: none; border-left-style: none; padding-top: 0px" src="../equip/eqimages.aspx?typ=no&amp;eqid=0&amp;fuid=0&amp;coid=0"
                                frameborder="no" width="260" scrolling="no" height="296" runat="server" allowtransparency>
                            </iframe>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lbltab" type="hidden" name="lbltab" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lbldid" type="hidden" name="lbldid" runat="server"><input id="lblclid"
        type="hidden" name="lblclid" runat="server">
    <input id="lblret" type="hidden" name="lblret" runat="server"><input id="lblchk"
        type="hidden" name="lblchk" runat="server">
    <input id="lbldchk" type="hidden" name="lbldchk" runat="server"><input id="lblfuid"
        type="hidden" name="lblfuid" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lbltaskid" type="hidden" runat="server">
    <input id="lbltasklev" type="hidden" runat="server">
    <input id="lblcid" type="hidden" runat="server">
    <input id="tasknum" type="hidden" name="tasknum" runat="server"><input id="taskcnt"
        type="hidden" name="taskcnt" runat="server">
    <input id="lblgetarch" type="hidden" runat="server">
    <input type="hidden" id="lbltyp" runat="server">
    <input type="hidden" id="lbllid" runat="server">
    <input type="hidden" id="retqs" runat="server">
    <input type="hidden" id="lblsubmit" runat="server">
    <input type="hidden" id="lblpicmode" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
    <uc1:mmenu1 ID="Mmenu11" runat="server"></uc1:mmenu1>
</body>
</html>
