Imports System.Data.SqlClient
Imports System.Text
Public Class appget
    Inherits System.Web.UI.Page
    Dim ec As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sql As String
    Dim sid, did, dept, cid, cell, eqid, eq, fuid, fu, comid, comp, lid, loc, ncid, nc As String
    Dim filt, dt, val, Filter, wonum, typ, who, jpid, fpc, eqdesc As String
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdecd4 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trcomp As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdecd5 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtab2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdec1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec1a As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec4 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbltabs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdtab As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trmisc As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents location As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdec5 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd6 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdloc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec6 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblnccnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdmsg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcell As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfpc As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdecd1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trsave As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblecd1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblecd2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblecd3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqdesc As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            sid = Request.QueryString("site").ToString
            typ = Request.QueryString("typ").ToString
            If typ = "wo" Or typ = "wr" Or typ = "woret" Then
                wonum = Request.QueryString("wo").ToString
            ElseIf typ = "jp" Or typ = "jpret" Then
                jpid = Request.QueryString("jpid").ToString
            ElseIf typ = "wrret" Then
                wonum = Request.QueryString("wonum").ToString
            End If

            lblsid.Value = sid
            lblwonum.Value = wonum
            lbltyp.Value = typ
            lbljpid.Value = jpid
            If typ = "lup" Then
                tdmsg.InnerHtml = "Equipment# Only Required for Return"
            End If
            If typ = "lul" Then
                tdmsg.InnerHtml = "Department and\or Cell Only Required for Return"
            End If
            If typ = "wrret" Or typ = "wr" Then
                tdmsg.InnerHtml = "Equipment# Only Required for Return"
            End If
            ec.Open()
            getdepts(sid)
            If typ = "ret" Or typ = "wrret" Or typ = "jpret" Or typ = "woret" Then
                who = Request.QueryString("who").ToString
                did = Request.QueryString("did").ToString
                Dim retdid As String = did
                dept = Request.QueryString("dept").ToString
                cid = Request.QueryString("clid").ToString
                Dim retcid As String = cid
                cell = Request.QueryString("cell").ToString
                eqid = Request.QueryString("eqid").ToString
                Dim reteqid As String = eqid
                eq = Request.QueryString("eq").ToString
                fuid = Request.QueryString("fuid").ToString
                fu = Request.QueryString("fu").ToString
                comid = Request.QueryString("coid").ToString
                comp = Request.QueryString("comp").ToString
                lid = Request.QueryString("lid").ToString
                loc = Request.QueryString("loc").ToString

                If eqid <> "" And eq = "" Then
                    eq = picupeq(eqid)
                End If
                If fuid <> "" And fu = "" Then
                    fu = picupfu(fuid)
                End If

                lblsid.Value = sid
                lbldid.Value = did
                lbldept.Value = dept

                lblcid.Value = cid
                lblcell.Value = cell

                lbleqid.Value = eqid
                lbleq.Value = eq

                lblfuid.Value = fuid
                lblfu.Value = fu

                lblcomid.Value = comid
                lblcomp.Value = comp
                lbllid.Value = lid
                lblloc.Value = loc

                lblwho.Value = who

                If who = "checkcell" Then
                    lblsubmit.Value = ""
                    checkdept(retdid)
                    dept = lbldept.Value
                    If dept = "" And retdid <> "" Then
                        dept = getdept(retdid)
                        lbldid.Value = retdid
                        'lblcid.Value = retcid
                    End If
                    tdecd1.InnerHtml = dept

                ElseIf who = "checkdept" Then
                    lblsubmit.Value = ""
                    checkdept(retdid)
                ElseIf who = "checkeq" Then
                    lblsubmit.Value = ""
                    checkdept(retdid)
                    If cid <> "" Then
                        checkcell(retcid)
                    End If
                    dept = lbldept.Value
                    If dept = "" And retdid <> "" Then
                        Dim cret As String
                        cret = getcell(retdid, retcid)
                        Dim cretarr() As String = cret.Split(",")
                        dept = cretarr(0)
                        cell = cretarr(1)
                        lbldid.Value = retdid
                        lblcid.Value = retcid
                    End If
                    tdecd1.InnerHtml = dept
                    tdecd2.InnerHtml = cell
                    'tdecd3.InnerHtml = eq
                    'checkeq()
                ElseIf who = "checkfu" Then
                    lblsubmit.Value = ""
                    checkdept(retdid)
                    If cid <> "" Then
                        checkcell(retcid)
                    End If
                    checkeq(reteqid)
                    dept = lbldept.Value
                    If dept = "" And retdid <> "" Then
                        Dim eret As String
                        eret = geteq(retdid, retcid, reteqid)
                        Dim cretarr() As String = eret.Split(",")
                        dept = cretarr(0)
                        cell = cretarr(1)
                        eq = cretarr(2)
                        eqdesc = cretarr(3)
                        lbldid.Value = retdid
                        lblcid.Value = retcid
                        lbleqid.Value = reteqid
                    End If
                    tdecd1.InnerHtml = dept
                    tdecd2.InnerHtml = cell
                    tdecd3.InnerHtml = eq
                    picupeq2(reteqid)
                    'tdecd4.InnerHtml = fu
                    'checkfu()
                ElseIf who = "deptret" Then
                    lblsubmit.Value = ""
                    'checkdept(retdid)
                    dept = lbldept.Value
                    If dept = "" And retdid <> "" Then
                        dept = getdept(retdid)
                        lbldid.Value = retdid
                        'lblcid.Value = retcid
                    End If
                    tdecd1.InnerHtml = dept
                    


                End If
                If comid <> "" Then
                    comid = lblcomid.Value 'Request.QueryString("coid").ToString
                    comp = lblcomp.Value 'Request.QueryString("comp").ToString
                    Dim test As String = comp
                    tdecd5.InnerHtml = comp
                End If
            End If
            ec.Dispose()
        Else
            If Request.Form("lblsubmit") = "checkcell" Then
                closecells()
                lblsubmit.Value = ""
                ec.Open()
                cid = lblcid.Value
                checkcell(cid)
                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "checkdept" Then
                closedepts()
                lblsubmit.Value = ""
                ec.Open()
                did = lbldid.Value
                checkdept(did)
                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "checkeq" Then
                closeeq()
                lblsubmit.Value = ""
                ec.Open()
                eqid = lbleqid.Value
                checkeq(eqid)
                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "checkfu" Then
                closefu()
                lblsubmit.Value = ""
                ec.Open()
                checkfu()
                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "savewo" Then
                lblsubmit.Value = ""
                ec.Open()
                savewo()
                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "savejp" Then
                lblsubmit.Value = ""
                ec.Open()
                savejp()
                ec.Dispose()
            End If
            typ = lbltyp.Value
            If typ = "lup" Then
                tdmsg.InnerHtml = "Equipment# Only Required for Return"
            End If
            If typ = "lul" Then
                tdmsg.InnerHtml = "Department and\or Cell Only Required for Return"
            End If
            If typ = "wrret" Then
                tdmsg.InnerHtml = "Equipment# Only Required for Return"
            End If
        End If
    End Sub
    Private Sub picupeq2(ByVal eqid As String)
        sql = "select eqnum, eqdesc, fpc from equipment where eqid = '" & eqid & "'"
        dr = ec.GetRdrData(sql)
        While dr.Read
            eq = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            fpc = dr.Item("fpc").ToString
        End While
        dr.Close()
        lbleqdesc.Value = eqdesc
        lblfpc.Value = fpc
    End Sub
    Private Function picupeq(ByVal eqid As String) As String
        Dim ret As String
        sql = "select eqnum from equipment where eqid = '" & eqid & "'"
        Try
            ret = ec.strScalar(sql)
        Catch ex As Exception
            ret = ""
        End Try
        Return ret
    End Function
    Private Function picupfu(ByVal fuid As String) As String
        Dim ret As String
        sql = "select func from functions where func_id = '" & fuid & "'"
        Try
            ret = ec.strScalar(sql)
        Catch ex As Exception
            ret = ""
        End Try
        Return ret
    End Function
    Private Sub closedepts()
        lblcid.Value = ""
        lblcell.Value = ""
        tdecd2.InnerHtml = ""
        lbleqid.Value = ""
        lbleq.Value = ""
        tdecd3.InnerHtml = ""
        lblfuid.Value = ""
        lblfu.Value = ""
        tdecd4.InnerHtml = ""
        lblcomid.Value = ""
        lblcomp.Value = ""
        tdecd5.InnerHtml = ""
        lblncid.Value = ""
        lblnc.Value = ""
        tdecd6.InnerHtml = ""
        lbllid.Value = ""
        lblloc.Value = ""
        tdloc.InnerHtml = ""
        lblfpc.Value = ""
    End Sub
    Private Sub closecells()

        lbleqid.Value = ""
        lbleq.Value = ""
        tdecd3.InnerHtml = ""
        lblfuid.Value = ""
        lblfu.Value = ""
        tdecd4.InnerHtml = ""
        lblcomid.Value = ""
        lblcomp.Value = ""
        tdecd5.InnerHtml = ""
        lblncid.Value = ""
        lblnc.Value = ""
        tdecd6.InnerHtml = ""
        lbllid.Value = ""
        lblloc.Value = ""
        tdloc.InnerHtml = ""
        lblfpc.Value = ""
    End Sub
    Private Sub closeeq()


        lblfuid.Value = ""
        lblfu.Value = ""
        tdecd4.InnerHtml = ""
        lblcomid.Value = ""
        lblcomp.Value = ""
        tdecd5.InnerHtml = ""


    End Sub
    Private Sub closefu()



        lblcomid.Value = ""
        lblcomp.Value = ""
        tdecd5.InnerHtml = ""

    End Sub

    Private Function geteq(ByVal did As String, ByVal cid As String, ByVal eqid As String) As String
        Dim ret As String
        Dim eqdesc As String
        sql = "select d.dept_line, c.cell_name, e.eqnum, e.eqdesc, e.fpc from dept d " _
            + "left join cells c on c.dept_id = d.dept_id " _
            + "left join equipment e on e.dept_id = d.dept_id " _
            + "where d.dept_id = '" & did & "' and e.eqid = '" & eqid & "'"
        dr = ec.GetRdrData(sql)
        While dr.Read
            dept = dr.Item("dept_line").ToString
            cell = dr.Item("cell_name").ToString
            eq = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
        End While
        dr.Close()
        ret = dept & "," & cell & "," & eq & "," & eqdesc
        Return ret
    End Function
    Private Function getfu(ByVal fuid As String)
        Dim ret As String
        sql = "select func from functions where func_id = '" & fuid & "'"
        ret = ec.strScalar(sql)
        Return ret

    End Function
    Private Function getcell(ByVal did As String, ByVal cid As String) As String
        Dim ret As String
        sql = "select d.dept_line, c.cell_name from dept d " _
            + "left join cells c on c.dept_id = d.dept_id where d.dept_id = '" & did & "'"
        dr = ec.GetRdrData(sql)
        While dr.Read
            dept = dr.Item("dept_line").ToString
            cell = dr.Item("cell_name").ToString
        End While
        dr.Close()
        ret = dept & "," & cell
        Return ret
    End Function
    Private Function getdept(ByVal did As String) As String
        Dim ret As String
        sql = "select dept_line from dept where dept_id = '" & did & "'"
        ret = ec.strScalar(sql)
        Return ret
    End Function
    Private Sub savejp()
        jpid = lbljpid.Value
        sid = lblsid.Value
        did = lbldid.Value
        dept = lbldept.Value
        cid = lblcid.Value
        cell = lblcell.Value
        eqid = lbleqid.Value
        eq = lbleq.Value
        fuid = lblfuid.Value
        fu = lblfu.Value
        comid = lblcomid.Value
        comp = lblcomp.Value
        ncid = lblnc.Value
        nc = lblnc.Value
        lid = lbllid.Value
        loc = lblloc.Value

        Dim cmd As New SqlCommand
        cmd.CommandText = "update pmjobplans set siteid = @sid, deptid = @did, cellid = @cid, " _
        + "eqid = @eqid, funcid = @fuid, comid = @comid, ncid = @ncid, " _
        + "locid = @lid, ld = 'D' where jpid = @jpid"
        Dim param = New SqlParameter("@jpid", SqlDbType.VarChar)
        param.Value = jpid
        cmd.Parameters.Add(param)
        Dim param01 = New SqlParameter("@sid", SqlDbType.VarChar)
        If sid = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = sid
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@did", SqlDbType.VarChar)
        If did = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = did
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@cid", SqlDbType.VarChar)
        If cid = "0" Then
            param03.Value = System.DBNull.Value
        Else
            param03.Value = cid
        End If
        cmd.Parameters.Add(param03)
        Dim param04 = New SqlParameter("@eqid", SqlDbType.VarChar)
        If eqid = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = eqid
        End If
        cmd.Parameters.Add(param04)

        Dim param06 = New SqlParameter("@fuid", SqlDbType.VarChar)
        If fuid = "" Then
            param06.Value = System.DBNull.Value
        Else
            param06.Value = fuid
        End If
        cmd.Parameters.Add(param06)
        Dim param07 = New SqlParameter("@comid", SqlDbType.VarChar)
        If comid = "" Then
            param07.Value = System.DBNull.Value
        Else
            param07.Value = comid
        End If
        cmd.Parameters.Add(param07)
        Dim param08 = New SqlParameter("@ncid", SqlDbType.VarChar)
        If ncid = "0" Then
            param08.Value = System.DBNull.Value
        Else
            param08.Value = ncid
        End If
        cmd.Parameters.Add(param08)

        Dim param10 = New SqlParameter("@lid", SqlDbType.VarChar)
        If lid = "" Then
            param10.Value = System.DBNull.Value
        Else
            param10.Value = lid
        End If
        cmd.Parameters.Add(param10)

        ec.UpdateHack(cmd)
        lblsubmit.Value = "return"
    End Sub
    Private Sub savewo()
        wonum = lblwonum.Value
        sid = lblsid.Value
        did = lbldid.Value
        dept = lbldept.Value
        cid = lblcid.Value
        cell = lblcell.Value
        eqid = lbleqid.Value
        eq = lbleq.Value
        fuid = lblfuid.Value
        fu = lblfu.Value
        comid = lblcomid.Value
        comp = lblcomp.Value
        ncid = lblncid.Value
        nc = lblnc.Value
        lid = lbllid.Value
        loc = lblloc.Value

        Dim cmd As New SqlCommand
        cmd.CommandText = "update workorder set siteid = @sid, deptid = @did, cellid = @cid, " _
        + "eqid = @eqid, eqnum = @eq, funcid = @fuid, comid = @comid, ncid = @ncid, ncnum = @nc, " _
        + "locid = @lid, location = @loc, ld = 'D' where wonum = @wonum"
        Dim param = New SqlParameter("@wonum", SqlDbType.VarChar)
        param.Value = wonum
        cmd.Parameters.Add(param)
        Dim param01 = New SqlParameter("@sid", SqlDbType.VarChar)
        If sid = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = sid
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@did", SqlDbType.VarChar)
        If did = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = did
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@cid", SqlDbType.VarChar)
        If cid = "0" Or cid = "" Then
            param03.Value = System.DBNull.Value
        Else
            param03.Value = cid
        End If
        cmd.Parameters.Add(param03)
        Dim param04 = New SqlParameter("@eqid", SqlDbType.VarChar)
        If eqid = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = eqid
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@eq", SqlDbType.VarChar)
        If eq = "" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = eq
        End If
        cmd.Parameters.Add(param05)
        Dim param06 = New SqlParameter("@fuid", SqlDbType.VarChar)
        If fuid = "" Then
            param06.Value = System.DBNull.Value
        Else
            param06.Value = fuid
        End If
        cmd.Parameters.Add(param06)
        Dim param07 = New SqlParameter("@comid", SqlDbType.VarChar)
        If comid = "" Then
            param07.Value = System.DBNull.Value
        Else
            param07.Value = comid
        End If
        cmd.Parameters.Add(param07)
        Dim param08 = New SqlParameter("@ncid", SqlDbType.VarChar)
        If ncid = "0" Then
            param08.Value = System.DBNull.Value
        Else
            param08.Value = ncid
        End If
        cmd.Parameters.Add(param08)
        Dim param09 = New SqlParameter("@nc", SqlDbType.VarChar)
        If nc = "" Then
            param09.Value = System.DBNull.Value
        Else
            param09.Value = nc
        End If
        cmd.Parameters.Add(param09)
        Dim param10 = New SqlParameter("@lid", SqlDbType.VarChar)
        If lid = "" Then
            param10.Value = System.DBNull.Value
        Else
            param10.Value = lid
        End If
        cmd.Parameters.Add(param10)
        Dim param11 = New SqlParameter("@loc", SqlDbType.VarChar)
        If loc = "" Then
            param11.Value = System.DBNull.Value
        Else
            param11.Value = loc
        End If
        cmd.Parameters.Add(param11)
        ec.UpdateHack(cmd)
        lblsubmit.Value = "return"
    End Sub
    Private Sub checkfu()
        fuid = lblfuid.Value
        fu = lblfu.Value
        PopComp(fuid)
        tdecd4.InnerHtml = fu
        lbltabs.Value = "ok,ok,ok,ok,ok,ok"

        trsave.Attributes.Add("class", "view")
    End Sub
    Private Sub checkeq(ByVal eqid As String)
        'eqid = lbleqid.Value
        Dim fucnt As Integer
        eq = lbleq.Value
        sql = "select count(*) from Functions where eqid = '" & eqid & "'"
        fucnt = ec.Scalar(sql)
        If fucnt > 0 Then
            PopFunc(eqid)
            tdecd3.InnerHtml = eq
            lbltabs.Value = "ok,ok,ok,ok,no,ok"
            tdtab.InnerHtml = "Functions"
            trsave.Attributes.Add("class", "view")
        Else
            Dim strMessage As String = tmod.getmsg("cdstr604", "woman.aspx.vb")
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            tdtab.InnerHtml = "Equipment"
            tdecd3.InnerHtml = eq
            lbltabs.Value = "ok,ok,ok,no,no,ok"
            tdtab.InnerHtml = "Equipment"
            trsave.Attributes.Add("class", "view")
            tdec1a.Attributes.Add("class", "details")
        End If
        
    End Sub
    Private Sub GetLoc()
        lid = lbllid.Value
        If lid <> "" And lid <> "0" Then
            sql = "select location, description from pmlocations where locid = '" & lid & "'"
            dr = ec.GetRdrData(sql)
            While dr.Read
                loc = dr.Item("location").ToString
            End While
            dr.Close()
            tdloc.InnerHtml = loc
            lblloc.Value = loc
        End If

    End Sub
    Private Sub checkcell(ByVal cis As String)
        cid = lblcid.Value
        cell = lblcell.Value
        PopEq(cid, "c")
        PopNC(cid, "c")
        tdecd2.InnerHtml = cell
        lbltabs.Value = "ok,ok,ok,no,no,ok"
        tdtab.InnerHtml = "Equipment"
        trsave.Attributes.Add("class", "view")
        tdec1a.Attributes.Add("class", "details")
    End Sub
    Private Sub checkdept(ByVal did As String)
        'did = lbldid.Value
        dept = lbldept.Value
        Dim tl As Integer
        'and cell_name = 'No Cells'"
        sql = "select count(*) from Cells where dept_id = '" & did & "' and cell_name <> 'No Cells'"
        tl = ec.Scalar(sql)
        Dim enc As Integer
        sql = "select count(*) from equipment where dept_id = '" & did & "' and (cellid = 0 or cellid is null)"
        enc = ec.Scalar(sql)
        If tl <> 0 Then
            PopCells(did)
            'lbltabs.Value = "ok,ok,no,no,no,no"
            'tdtab.InnerHtml = "Stations\Cells"
            'Else
            If enc <> 0 Then
                PopEq(did, "d", 1)
            End If
        Else
            PopEq(did, "d")
            PopNC(did, "d")
        End If

        If tl <> 0 Then
            tdtab.InnerHtml = "Stations\Cells"
            tdec1.Attributes.Add("class", "view")
            tdec2.Attributes.Add("class", "details")
            If enc <> 0 Then
                tdtab2.Attributes.Add("class", "label")
                tdec1a.Attributes.Add("class", "view")
            End If
        Else
            tdtab.InnerHtml = "Equipment"
            tdec1.Attributes.Add("class", "details")
            tdec2.Attributes.Add("class", "view")

        End If
        lbltabs.Value = "ok,ok,ok,no,no,ok"

        tdecd1.InnerHtml = dept
        trsave.Attributes.Add("class", "view")
    End Sub
    Private Sub PopNC(ByVal var As String, ByVal typ As String)
        lid = lbllid.Value
        did = lbldid.Value
        cid = lblcid.Value
        Dim eqcnt As Integer
        If typ = "d" Then
            If lid = "" Then
                sql = "select count(*) from noncritical where dept_id = '" & did & "'"
                filt = " where dept_id = '" & did & "'"
            Else
                sql = "select count(*) from noncritical where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            End If

        ElseIf typ = "c" Then
            If lid = "" Then
                sql = "select count(*) from noncritical where cellid = '" & cid & "'"
                filt = " where cellid = '" & cid & "'"
            Else
                sql = "select count(*) from noncritical where cellid = '" & cid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & cid & "' and locid = '" & cid & "'"
            End If

        ElseIf typ = "l" Then
            If cid <> "" Then
                sql = "select count(*) from noncritical where cellid = '" & cid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & cid & "' and locid = '" & lid & "'"
            ElseIf did <> "" Then
                sql = "select count(*) from noncritical where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            Else
                sql = "select count(*) from noncritical where locid = '" & lid & "'"
                filt = " where locid = '" & lid & "'"
            End If

        End If
        eqcnt = ec.Scalar(sql)
        lblnccnt.Value = eqcnt
        If eqcnt > 0 Then
            sql = "select ncid, ncnum, locid from noncritical " & filt
            'dt = "noncritical"
            'val = "ncid, ncnum "

            'dr = ec.GetList(dt, val, filt)
            dr = ec.GetRdrData(sql)
            Dim ncp, ncidp As String
            Dim sb As New StringBuilder
            sb.Append("<div style=""OVERFLOW: auto; WIDTH: 300px;  HEIGHT: 280px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
            sb.Append("<table width=""230"" cellpadding=""1"">")
            sb.Append("<tr>")
            'sb.Append("<td width=""80""></td>")
            sb.Append("<td width=""230""></td>")
            sb.Append("</tr>")
            While dr.Read
                ncidp = dr.Item("ncid").ToString
                ncp = dr.Item("ncnum").ToString
                'fcnt = dr.Item("fcnt").ToString
                lid = dr.Item("locid").ToString
                lbllid.Value = lid
                sb.Append("<tr>")
                'If fcnt > 0 Then
                sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getecd6('" & ncidp & "','" & ncp & "','yes');"">" & ncp & "</td>")
                'Else
                '    sb.Append("<td class=""plainlabel""><a href=""#"" class=""A1R"" onclick=""getecd3('" & eqid & "','" & eq & "','no');"">" & eq & "</td>")
                'End If

                'sb.Append("<td class=""plainlabel"">" & ecd & "</td>")
                sb.Append("</tr>")
            End While
            dr.Close()
            sb.Append("</table>")
            tdec5.InnerHtml = sb.ToString
            tdec.Attributes.Add("class", "details")
            tdec1.Attributes.Add("class", "details")
            tdec2.Attributes.Add("class", "view")
            tdec3.Attributes.Add("class", "details")
            tdec4.Attributes.Add("class", "details")
            tdec5.Attributes.Add("class", "details")

        End If
        GetLoc()
    End Sub

    Private Sub PopComp(ByVal fuid As String)
        'fuid = lblfuid.Value
        sql = "select count(*) " _
          + "from components where func_id = '" & fuid & "'"
        Dim compcnt As Integer = ec.Scalar(sql)
        If compcnt > 0 Then
            sql = "select comid, compnum + ' - ' + isnull(compdesc, '') as compnum " _
          + "from components where func_id = '" & fuid & "' order by crouting"
            dr = ec.GetRdrData(sql)
            Dim compp, comidp As String
            Dim sb As New StringBuilder
            sb.Append("<div style=""OVERFLOW: auto; WIDTH: 300px;  HEIGHT: 280px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
            sb.Append("<table width=""230"" cellpadding=""1"">")
            sb.Append("<tr>")
            'sb.Append("<td width=""80""></td>")
            sb.Append("<td width=""230""></td>")
            sb.Append("</tr>")
            While dr.Read
                comidp = dr.Item("comid").ToString
                compp = dr.Item("compnum").ToString
                sb.Append("<tr>")
                sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getecd5('" & comidp & "','" & compp & "');"">" & compp & "</td>")
                'sb.Append("<td class=""plainlabel"">" & ecd & "</td>")
                sb.Append("</tr>")
            End While
            dr.Close()
            sb.Append("</table>")
            tdec4.InnerHtml = sb.ToString
            tdec.Attributes.Add("class", "details")
            tdec1.Attributes.Add("class", "details")
            tdec2.Attributes.Add("class", "details")
            tdec3.Attributes.Add("class", "details")
            tdec4.Attributes.Add("class", "view")
            tdec5.Attributes.Add("class", "details")

            tdtab.InnerHtml = "Components"
        End If
        
    End Sub
    Private Sub PopFunc(ByVal eqid As String)
        Dim ccnt As Integer
        Dim fucnt As Integer
        Dim fuidp, fup As String
        sql = "select count(*) from Functions where eqid = '" & eqid & "'"
        fucnt = ec.Scalar(sql)
        If fucnt = 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr604", "woman.aspx.vb")
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            tdtab.InnerHtml = "Equipment"
        Else
            sql = "select func_id, func, ccnt = (select count(*) from components c where c.func_id = functions.func_id) " _
            + "from functions where eqid = '" & eqid & "' order by routing"
            'dt = "Functions"
            'val = "func_id, func"
            'filt = " where eqid = '" & eqid & "'"
            'dr = ec.GetList(dt, val, filt)
            dr = ec.GetRdrData(sql)
            Dim sb As New StringBuilder
            sb.Append("<div style=""OVERFLOW: auto; WIDTH: 300px;  HEIGHT: 280px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
            sb.Append("<table width=""230"" cellpadding=""1"">")
            sb.Append("<tr>")
            'sb.Append("<td width=""80""></td>")
            sb.Append("<td width=""230""></td>")
            sb.Append("</tr>")
            While dr.Read
                fuidp = dr.Item("func_id").ToString
                fup = dr.Item("func").ToString
                ccnt = dr.Item("ccnt").ToString
                sb.Append("<tr>")
                If ccnt > 0 Then
                    sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getecd4('" & fuidp & "','" & fup & "','yes');"">" & fup & "</td>")
                Else
                    sb.Append("<td class=""plainlabel""><a href=""#"" class=""A1R"" onclick=""getecd4('" & fuidp & "','" & fup & "','no');"">" & fup & "</td>")
                End If

                'sb.Append("<td class=""plainlabel"">" & ecd & "</td>")
                sb.Append("</tr>")
            End While
            dr.Close()
            sb.Append("</table>")
            tdec3.InnerHtml = sb.ToString
            tdec.Attributes.Add("class", "details")
            tdec1.Attributes.Add("class", "details")
            tdec1a.Attributes.Add("class", "details")
            tdtab2.Attributes.Add("class", "details")
            tdec2.Attributes.Add("class", "details")
            tdec3.Attributes.Add("class", "view")
            tdec4.Attributes.Add("class", "details")
            tdec5.Attributes.Add("class", "details")
        End If
    End Sub
    Private Sub PopCells(ByVal dept As String)
        Dim ecnt As Integer
        Dim cellp, cidp As String
        sql = "select cellid , cell_name, ecnt = (select count(*) from equipment e where e.cellid = cells.cellid) " _
        + "from cells where dept_id = " & dept & " order by cell_name"
        'filt = " where dept_id = " & dept
        'dt = "Cells"
        'val = "cellid , cell_name "
        'dr = ec.GetList(dt, val, filt)
        dr = ec.GetRdrData(sql)
        Dim sb As New StringBuilder
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 300px;  HEIGHT: 280px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
        sb.Append("<table width=""230"" cellpadding=""1"">")
        sb.Append("<tr>")
        'sb.Append("<td width=""80""></td>")
        sb.Append("<td width=""230""></td>")
        sb.Append("</tr>")
        While dr.Read
            cidp = dr.Item("cellid").ToString
            cellp = dr.Item("cell_name").ToString
            ecnt = dr.Item("ecnt").ToString
            sb.Append("<tr>")
            If ecnt > 0 Then
                sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getecd2('" & cidp & "','" & cellp & "','yes');"">" & cellp & "</td>")
            Else
                sb.Append("<td class=""plainlabelred""><a href=""#"" class=""A1R"" onclick=""getecd2('" & cidp & "','" & cellp & "','no');"">" & cellp & "</td>")
            End If

            '
            sb.Append("</tr>")
        End While
        dr.Close()

        sb.Append("</table>")
        tdec1.InnerHtml = sb.ToString
        tdec.Attributes.Add("class", "details")
        tdec1.Attributes.Add("class", "view")
        tdec2.Attributes.Add("class", "details")
        tdec3.Attributes.Add("class", "details")
        tdec4.Attributes.Add("class", "details")
        tdec5.Attributes.Add("class", "details")
    End Sub
    Private Sub PopEq(ByVal var As String, ByVal typ As String, Optional ByVal enc As Integer = 0)
        Dim fcnt As Integer
        Dim eqcnt As Integer
        If typ = "d" And enc = 0 Then
            sql = "select count(*) from equipment where dept_id = '" & var & "'"
            filt = " where dept_id = '" & var & "'"
        ElseIf typ = "d" And enc = 1 Then
            'dept_id = '" & did & "' and (cellid = 0 or cellid is null)"
            sql = "select count(*) from equipment where dept_id = '" & var & "' and (cellid = 0 or cellid is null)"
            filt = " where dept_id = '" & var & "' and (cellid = 0 or cellid is null)"
        ElseIf typ = "c" Then
            did = lbldid.Value
            sql = "select count(*) from equipment where cellid = '" & var & "'"
            filt = " where cellid = '" & var & "'"
        End If
        eqcnt = ec.Scalar(sql)
        Dim eqidp, eqp As String
        If eqcnt > 0 Then
            sql = "select eqid, eqnum, eqdesc, locid, fpc, fcnt = (select count(*) from functions f where f.eqid = equipment.eqid) " _
            + "from equipment " & filt & " order by eqnum"
            'dt = "equipment"
            'val = "eqid, eqnum "
            'dr = ec.GetList(dt, val, filt)
            dr = ec.GetRdrData(sql)
            Dim sb As New StringBuilder
            sb.Append("<div style=""OVERFLOW: auto; WIDTH: 300px;  HEIGHT: 280px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
            sb.Append("<table width=""230"" cellpadding=""1"">")
            sb.Append("<tr>")
            'sb.Append("<td width=""80""></td>")
            sb.Append("<td width=""230""></td>")
            sb.Append("</tr>")
            Dim rettyp As String = lbltyp.Value
            While dr.Read
                fpc = dr.Item("fpc").ToString
                eqidp = dr.Item("eqid").ToString
                eqp = dr.Item("eqnum").ToString
                eqdesc = dr.Item("eqdesc").ToString
                fcnt = dr.Item("fcnt").ToString
                lid = dr.Item("locid").ToString
                lbllid.Value = lid
                sb.Append("<tr>")
                If fcnt > 0 Then
                    If rettyp = "lup" Or rettyp = "wrret" Or rettyp = "wr" Then
                        sb.Append("<td class=""plainlabel""><a href=""#"" class=""A1R"" onclick=""getecd3('" & eqidp & "','" & eqp & "','" & fpc & "','" & eqdesc & "','no');"">" & eqp & "&nbsp;-&nbsp;" & eqdesc & "</td>")
                    Else
                        sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getecd3('" & eqidp & "','" & eqp & "','" & fpc & "','" & eqdesc & "','yes');"">" & eqp & "&nbsp;-&nbsp;" & eqdesc & "</td>")
                    End If
                Else
                    sb.Append("<td class=""plainlabel""><a href=""#"" class=""A1R"" onclick=""getecd3('" & eqidp & "','" & eqp & "','" & fpc & "','" & eqdesc & "','no');"">" & eqp & "&nbsp;-&nbsp;" & eqdesc & "</td>")
                End If

                'sb.Append("<td class=""plainlabel"">" & ecd & "</td>")
                sb.Append("</tr>")
            End While
            dr.Close()
            sb.Append("</table>")

            tdec.Attributes.Add("class", "details")
            tdec1.Attributes.Add("class", "details")
            If enc = 0 Then
                tdec2.InnerHtml = sb.ToString
                tdec2.Attributes.Add("class", "view")
            Else
                tdec2.InnerHtml = sb.ToString
                tdec1a.InnerHtml = sb.ToString
                tdec1a.Attributes.Add("class", "view")
            End If

            tdec3.Attributes.Add("class", "details")
            tdec4.Attributes.Add("class", "details")
            tdec5.Attributes.Add("class", "details")
        Else
            Dim strMessage As String = tmod.getmsg("cdstr978", "maxsearch.aspx.vb")
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        GetLoc()
    End Sub
    Private Sub getdepts(ByVal sid As String)
        Dim ccnt, ecnt As Integer
        sql = "select dept_id, dept_line, ccnt = (select count(*) from cells c where c.dept_id = dept.dept_id), " _
        + "ecnt = (select count(*) from equipment e where e.dept_id = dept.dept_id) from dept where siteid = '" & sid & "' order by dept_line"
        'filt = " where siteid = '" & sid & "'"
        'dt = "Dept"
        'val = "dept_id , dept_line "
        'dr = ec.GetList(dt, val, filt)
        dr = ec.GetRdrData(sql)
        Dim deptp, didp As String
        Dim sb As New StringBuilder
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 300px;  HEIGHT: 280px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
        sb.Append("<table width=""230"" cellpadding=""1"">")
        sb.Append("<tr>")
        'sb.Append("<td width=""80""></td>")
        sb.Append("<td width=""230""></td>")
        sb.Append("</tr>")
        While dr.Read
            didp = dr.Item("dept_id").ToString
            deptp = dr.Item("dept_line").ToString
            ccnt = dr.Item("ccnt").ToString
            ecnt = dr.Item("ecnt").ToString
            sb.Append("<tr>")
            If ccnt = 0 And ecnt = 0 Then
                sb.Append("<td class=""plainlabel""><a href=""#"" class=""A1R""  onclick=""getecd1('" & didp & "','" & deptp & "','no');"">" & deptp & "</td>")
            Else
                sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getecd1('" & didp & "','" & deptp & "','yes');"">" & deptp & "</td>")
            End If

            'sb.Append("<td class=""plainlabel"">" & ecd & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        lbltabs.Value = "ok,no,no,no,no,no"
        'tdec1.InnerHtml = ""
        'tdec2.InnerHtml = ""
        'tdec3.InnerHtml = ""
        'tdec4.InnerHtml = ""
        sb.Append("</table>")
        tdec.InnerHtml = sb.ToString
        tdec.Attributes.Add("class", "view")
        tdec1.Attributes.Add("class", "details")
        tdec2.Attributes.Add("class", "details")
        tdec3.Attributes.Add("class", "details")
        tdec4.Attributes.Add("class", "details")
        tdtab.InnerHtml = "Departments"
    End Sub
End Class
