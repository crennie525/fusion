<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="gtaskfail.aspx.vb" Inherits="lucy_r12.gtaskfail" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>gtaskfail</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/gtaskfailaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table class="view" id="tbeq" cellspacing="0" cellpadding="1" width="610">
        <tr>
            <td class="bluelabel" width="140" height="20">
                <asp:Label ID="lang229" runat="server">Component Addressed:</asp:Label>
            </td>
            <td width="464" id="tdcomp" runat="server" class="plainlabel">
            </td>
        </tr>
        <tr height="30">
            <td colspan="2">
                <table height="30" cellspacing="0" cellpadding="0" width="604">
                    <tr>
                        <td class="label" align="center" width="180">
                            <asp:Label ID="lang230" runat="server">Component Failure Modes</asp:Label>
                        </td>
                        <td width="22">
                        </td>
                        <td class="redlabel" align="center" width="200">
                            <asp:Label ID="lang231" runat="server">Not Addressed</asp:Label>
                        </td>
                        <td width="22">
                        </td>
                        <td class="bluelabel" align="center" width="180">
                            <asp:Label ID="lang232" runat="server">Selected</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:ListBox ID="lbCompFM" runat="server" CssClass="plainlabel" Width="150px" Height="60px"
                                SelectionMode="Multiple"></asp:ListBox>
                        </td>
                        <td>
                            <img id="btnaddnewfail" onmouseover="return overlib('Add a New Failure Mode to the Component Selected Above')"
                                onclick="GetFailDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
                                width="20" runat="server"><br>
                            <asp:ImageButton ID="ibReuse" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                            </asp:ImageButton><img class="details" id="fromreusedis" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                                width="20" runat="server">
                        </td>
                        <td align="center">
                            <asp:ListBox ID="lbfaillist" runat="server" CssClass="plainlabel" Width="150px" Height="60px"
                                SelectionMode="Multiple" ForeColor="Red"></asp:ListBox>
                        </td>
                        <td>
                            <img class="details" id="todis" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                                width="20" runat="server">
                            <img class="details" id="fromdis" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                width="20" runat="server">
                            <asp:ImageButton ID="ibToTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                            </asp:ImageButton><br>
                            <asp:ImageButton ID="ibFromTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                            </asp:ImageButton>
                        </td>
                        <td align="center">
                            <asp:ListBox ID="lbfailmodes" runat="server" CssClass="plainlabel" Width="150px"
                                Height="60px" SelectionMode="Multiple" ForeColor="Blue"></asp:ListBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <img src="../images/appbuttons/minibuttons/return2.gif" onmouseover="return overlib('Return', ABOVE, LEFT)"
                    onclick="handlereturn();" onmouseout="return nd()">
            </td>
        </tr>
    </table>
    <input type="hidden" id="lbltaskid" runat="server">
    <input type="hidden" id="lblcoid" runat="server">
    <input type="hidden" id="lbleqid" runat="server">
    <input type="hidden" id="lblfuid" runat="server">
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lblcid" runat="server">
    <input type="hidden" id="lblfm1" runat="server">
    <input type="hidden" id="lblcompfailchk" runat="server">
    <input type="hidden" id="lblcomp" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
