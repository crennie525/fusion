<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GSub.aspx.vb" Inherits="lucy_r12.GSub" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>GSub</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/GSubaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkinv();">
    <form id="form1" method="post" runat="server">
    <table cellspacing="1">
        <tr>
            <td class="thdrsing label" colspan="7">
                <asp:Label ID="lang213" runat="server">Sub Task View - Details</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <asp:DataList ID="dlhd" runat="server">
                    <HeaderTemplate>
                        <table width="700">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr height="20">
                            <td class="label">
                                <asp:Label ID="lang214" runat="server">Company</asp:Label>
                            </td>
                            <td>
                                <asp:Label CssClass="plainlabel" ID="lblcomp" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.compname") %>'>
                                </asp:Label>
                            </td>
                            <td class="label">
                                <asp:Label ID="lang215" runat="server">Plant Site</asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:Label CssClass="plainlabel" ID="lblplant" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.sitename") %>'>
                                </asp:Label>
                            </td>
                        </tr>
                        <tr height="20">
                            <td class="label">
                                <asp:Label ID="lang216" runat="server">Department</asp:Label>
                            </td>
                            <td>
                                <asp:Label CssClass="plainlabel" ID="lbldept" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dept_line") %>'>
                                </asp:Label>
                            </td>
                            <td class="label">
                                <asp:Label ID="lang217" runat="server">Station/Cell</asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:Label CssClass="plainlabel" ID="lblcell" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cell_name") %>'>
                                </asp:Label>
                            </td>
                        </tr>
                        <tr height="20">
                            <td class="label">
                                <asp:Label ID="lang218" runat="server">Equipment</asp:Label>
                            </td>
                            <td>
                                <asp:Label CssClass="plainlabel" ID="lbleq" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
                                </asp:Label>
                            </td>
                            <td class="label">
                                <asp:Label ID="lang219" runat="server">Function</asp:Label>
                            </td>
                            <td>
                                <asp:Label CssClass="plainlabel" ID="lblfunc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
                                </asp:Label>
                            </td>
                        </tr>
                        <tr height="20">
                            <td class="label">
                                <asp:Label ID="lang220" runat="server">Component</asp:Label>
                            </td>
                            <td>
                                <asp:Label CssClass="plainlabel" ID="lblcompon" runat="server" ForeColor="Black"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td width="100">
                            </td>
                            <td width="200">
                            </td>
                            <td width="100">
                            </td>
                            <td width="200">
                            </td>
                            <td width="100">
                            </td>
                            <td width="200">
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
    </table>
    <table cellspacing="1">
        <tr>
            <td class="thdrsing label" colspan="7">
                <asp:Label ID="lang221" runat="server">Current Task</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="btmmenu plainlabel" width="60">
                <asp:Label ID="lang222" runat="server">Task#</asp:Label>
            </td>
            <td class="btmmenu plainlabel" width="420">
                <asp:Label ID="lang223" runat="server">Top Level Task Description</asp:Label>
            </td>
            <td class="btmmenu plainlabel" width="100">
                <asp:Label ID="lang224" runat="server">Skill Required</asp:Label>
            </td>
            <td class="btmmenu plainlabel" width="60">
                Qty
            </td>
            <td class="btmmenu plainlabel" width="60">
                <asp:Label ID="lang225" runat="server">Min Ea</asp:Label>
            </td>
            <td class="btmmenu plainlabel" width="60">
                <asp:Label ID="lang226" runat="server">EQ Status</asp:Label>
            </td>
            <td class="btmmenu plainlabel" width="70">
                <asp:Label ID="lang227" runat="server">Down Time</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="plainlabel" id="tdtnum" runat="server">
            </td>
            <td class="plainlabel" id="tddesc" runat="server">
            </td>
            <td class="plainlabel" id="tdskill" runat="server">
            </td>
            <td class="plainlabel" id="tdqty" runat="server">
            </td>
            <td class="plainlabel" id="tdmin" runat="server">
            </td>
            <td class="plainlabel" id="tdrd" runat="server">
            </td>
            <td class="plainlabel" id="tdrdt" runat="server">
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="thdrsing label" colspan="7">
                <asp:Label ID="lang228" runat="server">Sub Tasks</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:ImageButton ID="addtask" runat="server" ImageUrl="../images/appbuttons/bgbuttons/addtask.gif">
                </asp:ImageButton>&nbsp;<img id="btnreturn" onclick="handlereturn();" height="19"
                    src="../images/appbuttons/bgbuttons/return.gif" width="69" runat="server">
            </td>
        </tr>
        <tr>
            <td>
                <asp:DataGrid ID="dgtasks" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    CellSpacing="1" GridLines="None">
                    <AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="Task#">
                            <HeaderStyle CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblta" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="lblt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'
                                    Width="40px">
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Sub Task#">
                            <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblsubt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="lblst" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>'
                                    Width="40px">
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Sub Task Description">
                            <HeaderStyle Width="280px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>'
                                    Width="270px">
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtdesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>'
                                    Height="70px" TextMode="MultiLine" Width="260px" MaxLength="1000">
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" SortExpression="skill desc" HeaderText="Skill Required">
                            <HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblskill" Width="230px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                </asp:Label>
                                <img id="imgskill" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Skill Qty" Visible="False">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'
                                    ID="Label10" NAME="Label8">
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtqty" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="Skill Min Ea">
                            <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'
                                    ID="Label12">
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txttr" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="Down Time">
                            <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rdt") %>'
                                    ID="Label2">
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtdt" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.rdt") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Parts/Tools/Lubes" Visible="False">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton23" runat="server" ImageUrl="../images/appbuttons/minibuttons/parttrans.gif"
                                    ToolTip="Add Parts" CommandName="Part"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton23a" runat="server" ImageUrl="../images/appbuttons/minibuttons/tooltrans.gif"
                                    ToolTip="Add Tools" CommandName="Tool"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton23b" runat="server" ImageUrl="../images/appbuttons/minibuttons/lubetrans.gif"
                                    ToolTip="Add Lubricants" CommandName="Lube"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton23c" runat="server" ImageUrl="../images/appbuttons/minibuttons/notessm.gif"
                                    ToolTip="Add Notes to the Task" CommandName="Note"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="pmtskid">
                            <ItemTemplate>
                                <asp:Label ID="lbltida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblttid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Delete">
                            <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="ibDel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" runat="server">
    <input id="lbltid" type="hidden" runat="server">
    <input id="lblfuid" type="hidden" runat="server">
    <input id="lblfilt" type="hidden" runat="server">
    <input id="lblsubval" type="hidden" runat="server"><input id="lblsid" type="hidden"
        runat="server">
    <input id="lbloldtask" type="hidden" runat="server"><input id="lblpart" type="hidden"
        name="lblpart" runat="server">
    <input id="lbltool" type="hidden" name="lbltool" runat="server"><input id="lbllube"
        type="hidden" name="lbllube" runat="server">
    <input id="lblnote" type="hidden" name="lblnote" runat="server"><input id="lbltasknum"
        type="hidden" runat="server">
    <input id="lblpmtid" type="hidden" runat="server"><input id="lbllog" type="hidden"
        runat="server">
    <input id="lbllock" type="hidden" name="lbllock" runat="server">
    <input id="lbllockedby" type="hidden" name="lbllockedby" runat="server">
    <input id="lblusername" type="hidden" runat="server"><input id="lbleqid" type="hidden"
        runat="server">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lbltasktype" runat="server">
    <input type="hidden" id="lbltaskstatus" runat="server">
    <input type="hidden" id="lblfreqid" runat="server">
    <input type="hidden" id="lblfreq" runat="server">
    <input type="hidden" id="lblrdid" runat="server">
    <input type="hidden" id="lblrd" runat="server">
    <input type="hidden" id="lblptid" runat="server">
    <input type="hidden" id="lblpretech" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblskillqty" runat="server" />
    </form>
</body>
</html>
