

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class gtaskfail
    Inherits System.Web.UI.Page


    Protected WithEvents lang232 As System.Web.UI.WebControls.Label

    Protected WithEvents lang231 As System.Web.UI.WebControls.Label

    Protected WithEvents lang230 As System.Web.UI.WebControls.Label

    Protected WithEvents lang229 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tasks As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim ttid, coid, eqid, fuid, sid, cid, comp As String
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbCompFM As System.Web.UI.WebControls.ListBox
    Protected WithEvents ibReuse As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfaillist As System.Web.UI.WebControls.ListBox
    Protected WithEvents ibToTask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibFromTask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents btnaddnewfail As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromreusedis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then

            cid = Request.QueryString("cid").ToString
            lblcid.Value = cid
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            fuid = Request.QueryString("fuid").ToString
            lblfuid.Value = fuid
            coid = Request.QueryString("comid").ToString
            lblcoid.Value = coid
            comp = Request.QueryString("comp").ToString
            lblcomp.Value = comp
            tdcomp.InnerHtml = comp
            ttid = Request.QueryString("pmtskid").ToString
            lbltaskid.Value = ttid

            tasks.Open()
            PopFailList(coid)
            PopTaskFailModes(coid)
            PopCompFailList(coid)
            GetFM1(ttid)
            tasks.Dispose()

        Else
            If Request.Form("lblcompfailchk") = "1" Then
                lblcompfailchk.Value = ""
                tasks.Open()
                AddFail()
                tasks.Dispose()
            End If
        End If
        ibToTask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov6", "gtaskfail.aspx.vb") & "')")
        ibToTask.Attributes.Add("onmouseout", "return nd()")
        'ibToTask.Attributes.Add("onclick", "DisableButton(this);")
        ibReuse.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov7", "gtaskfail.aspx.vb") & "')")
        ibReuse.Attributes.Add("onmouseout", "return nd()")
        'ibReuse.Attributes.Add("onclick", "DisableButton(this);")
        ibFromTask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov8", "gtaskfail.aspx.vb") & "')")
        ibFromTask.Attributes.Add("onmouseout", "return nd()")
        'ibFromTask.Attributes.Add("onclick", "DisableButton(this);")

    End Sub
    Private Sub AddFail()
        Dim comp As String = lblcoid.Value

        PopCompFailList(comp)
        PopFailList(comp)
        PopTaskFailModes(comp)

    End Sub
    Private Sub PopFailList(ByVal comp As String)
        ttid = lbltaskid.Value
        sql = "select compfailid, failuremode " _
        + "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
        + "select failid from pmtaskfailmodes where comid = '" & comp & "')"
        sql = "usp_getcfall_tskna '" & comp & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            'dr = tasksadd.GetRdrData(sql)
        End Try
        lbfaillist.DataSource = dr
        lbfaillist.DataTextField = "failuremode"
        lbfaillist.DataValueField = "compfailid"
        lbfaillist.DataBind()
        dr.Close()
    End Sub

    Private Sub PopCompFailList(ByVal comp As String)
        ttid = lbltaskid.Value
        sql = "select compfailid, failuremode " _
         + "from componentfailmodes where comid = '" & comp & "'"
        sql = "usp_getcfall_co '" & comp & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            'dr = tasksadd.GetRdrData(sql)
        End Try

        lbCompFM.DataSource = dr
        lbCompFM.DataTextField = "failuremode"
        lbCompFM.DataValueField = "compfailid"
        lbCompFM.DataBind()
        dr.Close()
    End Sub
    Private Sub GetFM1(ByVal pmtskid As String)
        sql = "select fm1 from pmtasks where pmtskid = '" & pmtskid & "'"
        dr = tasks.GetRdrData(sql)
        While dr.Read
            lblfm1.Value = dr.Item("fm1").ToString
        End While
        dr.Close()
    End Sub
    Private Sub PopTaskFailModes(ByVal comp As String)
        ttid = lbltaskid.Value
        sql = "select * from pmtaskfailmodes where comid = '" & comp & "' and taskid = '" & ttid & "'"
        sql = "usp_getcfall_tsk '" & comp & "','" & ttid & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            'dr = tasksadd.GetRdrData(sql)
        End Try
        lbfailmodes.DataSource = dr
        lbfailmodes.DataTextField = "failuremode"
        lbfailmodes.DataValueField = "failid"
        lbfailmodes.DataBind()
        dr.Close()


    End Sub

    Private Sub ibReuse_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibReuse.Click
        Dim Item As ListItem
        Dim f, fi, fm, oa As String
        Dim ipar As Integer = 0
        Dim comp As String = lblcoid.Value
        tasks.Open()
        'SaveTask()
        'lblenable.Value = "0"
        For Each Item In lbCompFM.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If

                End If
                GetItems(f, fi, comp, oa)
            End If
        Next
        Try
            fm = tasks.ModString2(fm)
            lblfm1.Value = fm
            sql = "update pmtasks set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
            tasks.Update(sql)
            eqid = lbleqid.Value
            tasks.UpMod(eqid)

            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            lbltaskid.Value = ttid
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
        Catch ex As Exception

        End Try
        tasks.Dispose()
    End Sub
    Private Sub GetItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Dim fcnt, fcnt2 As Integer
        If oaid <> "0" Then
            sql = "select count(*) from pmTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and oaid = '" & oaid & "'"
        Else
            sql = "select count(*) from pmTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and  (oaid is null or oaid = '0')"
        End If
        fcnt = tasks.Scalar(sql)
        sql = "select count(*) from pmTaskFailModes where taskid = '" & ttid & "'"
        fcnt2 = tasks.Scalar(sql)
        If fcnt2 >= 5 Then
            Dim strMessage As String = tmod.getmsg("cdstr212", "gtaskfail.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf fcnt > 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr213", "gtaskfail.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Try
                sql = "sp_addTaskFailureMode " & ttid & ", " & fi & ", '" & f & "', '" & comp & "', 'dev','" & oaid & "'"
                tasks.Update(sql)
                Dim Item As ListItem
                Dim fm As String
                Dim ipar As Integer = 0
                For Each Item In lbfailmodes.Items
                    f = Item.Text.ToString
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If

                    If Len(fm) = 0 Then
                        fm = f & "(___)"
                    Else
                        fm += " " & f & "(___)"
                    End If
                Next

            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Sub RemItems(ByVal f As String, ByVal fi As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Try
            sql = "sp_delTaskFailureMode '" & ttid & "', '" & fi & "','" & oaid & "'"
            tasks.Update(sql)
            Dim fm As String
            Dim ipar As Integer = 0
            Dim Item As ListItem
            For Each Item In lbfailmodes.Items
                f = Item.Text.ToString
                ipar = f.LastIndexOf("(")
                If ipar <> -1 Then
                    f = Mid(f, 1, ipar)
                End If

                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
            Next

        Catch ex As Exception

        End Try
    End Sub
    Private Sub UpdateFailStats(ByVal comp As String)
        Dim fmcnt, fmcnta As Integer
        sql = "select count(*) from ComponentFailModes where comid = '" & comp & "'"
        Try
            fmcnt = tasks.Scalar(sql)
        Catch ex As Exception
            'fmcnt = tasksadd.Scalar(sql)
        End Try
        sql = "select count(distinct failid) from pmTaskFailModes where comid = '" & comp & "'"
        Try
            fmcnta = tasks.Scalar(sql)
        Catch ex As Exception
            'fmcnta = tasksadd.Scalar(sql)
        End Try

        fmcnta = fmcnt - fmcnta
    End Sub

    Private Sub ibToTask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibToTask.Click
        Dim Item As ListItem
        Dim f, fi, fm, oa As String
        Dim ipar As Integer = 0
        Dim comp As String = lblcoid.Value
        tasks.Open()
        For Each Item In lbfaillist.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If

                End If
                GetItems(f, fi, comp, oa)
            End If
        Next
        Try
            fm = tasks.ModString2(fm)
            lblfm1.Value = fm
            sql = "update pmtasks set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
            tasks.Update(sql)
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            UpdateFM()
            lbltaskid.Value = ttid
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub
    Private Sub UpdateFM()
        Dim ttid As String = lbltaskid.Value
        sql = "usp_UpdateFM1 '" & ttid & "'"
        tasks.Update(sql)
    End Sub

    Private Sub ibFromTask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibFromTask.Click
        Dim Item As ListItem
        Dim f, fi, fm, oa As String
        Dim ipar As Integer = 0
        Dim comp As String = lblcoid.Value
        tasks.Open()
        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If

                End If
                RemItems(f, fi, oa)
            Else
                f = Item.Text.ToString
                fi = Item.Value.ToString
                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
            End If
        Next
        Try
            fm = tasks.ModString2(fm)
            lblfm1.Value = fm
            sql = "update pmtasks set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
            tasks.Update(sql)
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            UpdateFM()
            lbltaskid.Value = ttid
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang229.Text = axlabs.GetASPXPage("gtaskfail.aspx", "lang229")
        Catch ex As Exception
        End Try
        Try
            lang230.Text = axlabs.GetASPXPage("gtaskfail.aspx", "lang230")
        Catch ex As Exception
        End Try
        Try
            lang231.Text = axlabs.GetASPXPage("gtaskfail.aspx", "lang231")
        Catch ex As Exception
        End Try
        Try
            lang232.Text = axlabs.GetASPXPage("gtaskfail.aspx", "lang232")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddnewfail.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("gtaskfail.aspx", "btnaddnewfail") & "')")
            btnaddnewfail.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try


    End Sub

End Class
