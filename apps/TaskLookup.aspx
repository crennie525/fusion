<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TaskLookup.aspx.vb" Inherits="lucy_r12.TaskLookup" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title id="pgtitle" runat="server">TaskLookup</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/TaskLookupaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="handleentry();" onunload="handleexit();">
    <form id="form1" method="post" runat="server">
    <table width="100%">
        <tr>
            <td id="tdlookup" runat="server">
            </td>
        </tr>
    </table>
    <input type="hidden" id="lbllog" runat="server">
    <input type="hidden" id="lblstr" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
