﻿Public Class taskwodialog
    Inherits System.Web.UI.Page
    Dim sql, cid, ro, eqid, fuid, coid, sid, eqnum, func, comp, pmtskid, pmfid, wonum, task As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try

        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            fuid = Request.QueryString("fuid").ToString
            coid = Request.QueryString("coid").ToString

            eqnum = Request.QueryString("eqnum").ToString
            func = Request.QueryString("func").ToString
            comp = Request.QueryString("comp").ToString

            sid = Request.QueryString("sid").ToString
            pmtskid = Request.QueryString("pmtskid").ToString
            task = Request.QueryString("task").ToString
            iffmh.Attributes.Add("src", "taskwo.aspx?eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&sid=" + sid + "&pmtskid=" + pmtskid + "&eqnum=" + eqnum + "&comp=" + comp + "&func=" + func + "&task=" + task)
        End If
    End Sub

End Class