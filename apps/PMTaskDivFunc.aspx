<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMTaskDivFunc.aspx.vb"
    Inherits="lucy_r12.PMTaskDivFunc" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMTaskDivFunc</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="javascript" type="text/javascript" src="../scripts/pmtaskdivfunc_1016f.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMTaskDivFuncaspx_2.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
    <!--
        function checkdeltasktpm() {
            var chken = document.getElementById("lblenable").value;
            //if(chken!="1") {
            tid = document.getElementById("lbltaskid").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            //alert(tid + "," + tpmhold)
            if (tid != "" && tid != "0" && tpmhold != "0") {
                //var deltask = confirm("Are you sure you want to Delete this Task from\nthe TPM Module and Enable Editing?");
                var deltask = confirm("Removing this task from TPM will result in the loss of images and scheduling data.\nDo you want to continue?");
                if (deltask == true) {
                    document.getElementById("lblcompchk").value = "deltpm";
                    FreezeScreen('Your Data Is Being Processed...');
                    document.getElementById("form1").submit();
                }
                else if (deltask == false) {
                    return false;
                    alert("Action Cancelled")
                }
            }
            //}
        }
        function tasklookup() {

            var chken = document.getElementById("lblenable").value;
            var ro = document.getElementById("lblro").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            if ((chken != "1" || ro == "1") && tpmhold != "1") {
                ////window.parent.setref();
                //var str = document.getElementById("txtdesc").innerHTML;
                var str = document.getElementById("txtdesc").value;
                //alert(str)
                var eReturn = window.showModalDialog("../apps/TaskLookup.aspx?str=" + str, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes");
                //alert(eReturn)
                if (eReturn != "no" && eReturn != "~none~") {
                    //document.getElementById("txtdesc").innerHTML = eReturn;
                    document.getElementById("txtdesc").value = eReturn;
                }
                else if (eReturn != "~none~") {
                    document.getElementById("form1").submit();
                }
            }
        }
        function GetType(app, sval) {
            //alert(app + ", " + sval)
            var typlst
            var sstr
            var ssti
            if (app == "d") {
                typlst = document.getElementById("ddtype");
                ptlst = document.getElementById("ddpt");
                sstr = typlst.options[typlst.selectedIndex].text;
                ssti = document.getElementById("ddtype").value;
                if (ssti == "7") {
                    document.getElementById("form1").ddpt.disabled = false;
                }
                else {
                    ptlst.value = "0";
                    document.getElementById("form1").ddpt.disabled = true;
                }
            }
            
        }
        function filldown(val) {
            var dt = document.getElementById("ddeqstat").options[document.getElementById("ddeqstat").options.selectedIndex].text;
            dt = document.getElementById("ddeqstat").value
            if (dt == "2") {
                //if(document.getElementById("txtrdt").value!="") {
                document.getElementById("txtrdt").value = val;
                //}
            }
        }
        function zerodt(val) {
            var dt = document.getElementById("ddeqstat").options[document.getElementById("ddeqstat").options.selectedIndex].text;
            dt = document.getElementById("ddeqstat").value
            //alert(dt)
            if (dt != "2") {
                document.getElementById("txtrdt").value = "0";
            }
        }
        function checkit() {
            var tnum = document.getElementById("lblpg").innerHTML;
            window.parent.handletnum(tnum);

            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                window.parent.doref();
            }
            else {
                window.parent.setref();
            }
            var app = document.getElementById("appchk").value;
            if (app == "switch") window.parent.handleapp(app);

            var chk = document.getElementById("lblpar").value;

            if (chk == "task") {
                valu = document.getElementById("lbltaskid").value;
                var num = document.getElementById("lblt").innerHTML;
                //alert(num)
                window.parent.handletask(chk, valu, num);
            }
            else if (chk == "comp") {
                //alert(document.getElementById("lblco").value)
                cvalu = document.getElementById("lblco").value;
                tvalu = document.getElementById("lbltaskid").value;
                tnum = document.getElementById("lblt").value;
                //alert(tnum)
                window.parent.handleco(chk, cvalu, tvalu, tnum);
            }
            try {
                cnt = document.getElementById("taskcnt").value;
                if (cnt != "0") {
                    window.parent.handlecnt(cnt);
                }
            }
            catch (err) {
            }
            var tpm = document.getElementById("lbltpmalert").value;
            if (tpm == "yes") {
                var decision = confirm("This Task Meets Your Requirements for TPM\Do You Want to Transfer this Task to TPM?")
                if (decision == true) {

                    FreezeScreen('Your Data Is Being Processed...');
                    document.getElementById("lblcompchk").value = uptpm;
                    document.getElementById("form1").submit();
                }
                else {
                    //document.getElementById("ddcomp").value = goback;
                    document.getElementById("lbltpmalert").value = "no";
                    alert("Action Cancelled")
                }
            }
            page_maint('d');


        }
        function getmeter() {
            var enable = document.getElementById("lblenable").value;
            var eqid = document.getElementById("lbleqid").value;
            var eqnum = document.getElementById("lbleqnum").value;
            var pmtskid = document.getElementById("lbltaskid").value;
            var fuid = document.getElementById("lblfuid").value;
            var skillid = document.getElementById("lblskillid").value;
            var skillqty = document.getElementById("lblskillqty").value;
            var rdid = document.getElementById("lblrdid").value;
            var func = document.getElementById("lblfunc").value;
            var skill = document.getElementById("lblskill").value;
            var rd = document.getElementById("lblrd").value;
            var meterid = document.getElementById("lblmeterid").value;
            var varflg = 0;
            if (enable == "0" && pmtskid != "") {
                if (skillid == "" || skillid == "0") {
                    skillid = document.getElementById("ddskill").value;
                    if (skillid == "" || skillid == "0") {
                        alert("Skill Required")
                        varflg = 1;
                    }
                    else {
                        var sklist = document.getElementById("ddskill");
                        var skstr = sklist.options[sklist.selectedIndex].text;
                        skill = skstr;
                    }
                }
                else if (skillqty == "") {
                    skillqty = document.getElementById("txtqty").value;
                    if (skillqty == "") {
                        alert("Skill Qty Required")
                        varflg = 1;
                    }
                }
                else if (rdid == "" || rdid == "0") {
                    rdid = document.getElementById("ddeqstat").value;
                    if (rdid == "" || rdid == "0") {
                        alert("Running or Down Value Required")
                        varflg = 1;
                    }
                    else {
                        var rdlist = document.getElementById("ddeqstat");
                        var rdstr = sklist.options[rdlist.selectedIndex].text;
                        rd = rdstr;
                    }
                }
                if (varflg == 0) {
                    var eReturn = window.showModalDialog("../equip/usemeterdialog.aspx?typ=reg&eqid=" + eqid + "&pmtskid=" + pmtskid + "&fuid=" + fuid + "&skillid=" + skillid
                                   + "&skillqty=" + skillqty + "&rdid=" + rdid + "&func=" + func + "&skill=" + skill + "&rd=" + rd
                                   + "&meterid=" + meterid + "&rdt=" + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:900px;resizable=yes");
                    if (eReturn) {
                        document.getElementById("lblcompchk").value = "2";
                        document.getElementById("form1").submit();
                    }
                }
            }
        }

        function getfail2() {
            //eqid, fuid, coid, sid, pmtskid, eqnum, func, comp, task
            var eqid = document.getElementById("lbleqid").value;
            var fuid = document.getElementById("lblfuid").value;
            var coid = document.getElementById("lblco").value;
            var sid = document.getElementById("lblsid").value;
            var task = document.getElementById("lbltaskid").value;
            var eqnum = "";
            var func = "";
            var comp = "";
            var pmtskid = document.getElementById("lblpg").innerHTML;

            var eReturn = window.showModalDialog("../apps/taskwodialog.aspx?eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&sid=" + sid + "&pmtskid=" + pmtskid + "&eqnum=" + eqnum + "&comp=" + comp + "&func=" + func + "&task=" + task, "", "dialogHeight:600px; dialogWidth:900px; resizable=yes");
            if (eReturn) {

            }
        }

        function doEnable() {
            var tpmhold = document.getElementById("lbltpmhold").value;
            var ro = document.getElementById("lblro").value;
            var pg = document.getElementById("lblpg").innerHTML;
            //alert(pg + "," + ro + "," + tpmhold)
            if (pg != "0" && ro != "1" && tpmhold != "1") {
                //alert(document.getElementById("lblenable").value)
                document.getElementById("lblenable").value = "0";

                document.getElementById("ddcomp").disabled = false;
                document.getElementById("lbfaillist").disabled = false;
                document.getElementById("lbfailmodes").disabled = false;
                document.getElementById("lbCompFM").disabled = false;
                document.getElementById("txtcQty").disabled = false;
                document.getElementById("ibReuse").disabled = false;
                document.getElementById("ibToTask").disabled = false;
                document.getElementById("ibFromTask").disabled = false;
                document.getElementById("txtdesc").disabled = false;
                document.getElementById("ddtype").disabled = false;

                var ismeter = document.getElementById("lblusemeter").value;
                if (ismeter != "1") {
                    document.getElementById("txtfreq").disabled = false;
                    document.getElementById("cbfixed").disabled = false;
                }

                document.getElementById("txtpfint").disabled = false;
                document.getElementById("txtqty").disabled = false;
                document.getElementById("txttr").disabled = false;
                document.getElementById("txtrdt").disabled = false;
                //document.getElementById("ddpt.disabled=false;
                var sstr;
                var ssti;
                var typlst;
                typlst = document.getElementById("ddtype");
                sstr = typlst.options[typlst.selectedIndex].text
                ssti = document.getElementById("ddtype").value;
                if (sstr == "7") {
                    document.getElementById("ddpt").disabled = false;
                    try {
                        document.getElementById("ddpto").disabled = false;
                    }
                    catch (err) {

                    }
                }
                document.getElementById("ddeqstat").disabled = false;
                document.getElementById("ddskill").disabled = false;
                document.getElementById("cbloto").disabled = false;

                document.getElementById("cbcs").disabled = false;

                document.getElementById("btnaddtsk").disabled = false;
                document.getElementById("btnaddsubtask").disabled = false;
                document.getElementById("btntpm").disabled = false;
                document.getElementById("txttaskorder").disabled = false;
                try {
                    document.getElementById("btndeltask").disabled = false;
                }
                catch (err) {
                    //do nothing - button not used in optimizer
                }

                document.getElementById("ibCancel").disabled = false;
                //document.getElementById("btnsavetask").disabled=false;

            }
            else {
                if (pg == "0") {
                    alert("No Task Records Are Loaded Yet!")
                }
                else if (tpmhold == "1") {
                    alert("This has been designated as a TPM Task and Cannot be Edited!")
                }
            }
        }
        function valpgnums() {
        
            if (document.getElementById("lblenable").value != "1") {
                var tpmhold = document.getElementById("lbltpmhold").value;
                if (tpmhold != "1") {
                    //alert(document.getElementById("txtqty").value)
                    //alert(document.getElementById("txtcQty").value)
                    //alert(document.getElementById("txtfreq").value)
                    //alert(document.getElementById("txttr").value)
                    //alert(document.getElementById("txtrdt").value)
                    //alert(document.getElementById("txtpfint").value)
                    var desc = document.getElementById("txtdesc").value;
                    if (isNaN(document.getElementById("txtqty").value)) {
                        alert("Skill Qty is Not a Number")
                    }
                    else if (desc.length > 500) {
                        alert("Maximum Charater Length for Task Description is 500 Characters")
                    }
                    else if (isNaN(document.getElementById("txtcQty").value)) {
                        alert("Component Qty is Not a Number")
                    }
                    else if (isNaN(document.getElementById("txtqty").value)) {
                        alert("Skill Qty is Not a Number")
                    }
                    else if (isNaN(document.getElementById("txtfreq").value)) {
                        alert("Frequency is Not a Number")
                    }
                    else if (isNaN(document.getElementById("txttr").value)) {
                        alert("Skill Time is Not a Number")
                    }
                    else if (isNaN(document.getElementById("txtrdt").value)) {
                        alert("Running/Down Time is Not a Number")
                    }
                    else {
                        if (isNaN(document.getElementById("txtpfint").value)) {
                            document.getElementById("txtpfint").value = "0";
                        }
                        document.getElementById("lblcompchk").value = "3";
                        FreezeScreen('Your Data Is Being Processed...');
                        document.getElementById("form1").submit();
                    }

                }
            }
        }
//-->
    </script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <div class="FreezePaneOff" id="FreezePane" align="center">
        <div class="InnerFreezePane" id="InnerFreezePane">
        </div>
    </div>
    <div id="overDiv" style="z-index: 1000; position: absolute; visibility: hidden">
    </div>
    <div style="position: absolute; top: 0px; left: 0px">
        <table cellspacing="0" cellpadding="0" width="760">
            <tr>
                <td class="thdrsinglft" align="left" width="26">
                    <img src="../images/appbuttons/minibuttons/3gearsh.gif" border="0">
                </td>
                <td class="thdrsingrt label" onmouseover="return overlib('Against what is the PM intended to protect?', ABOVE, LEFT)"
                    onmouseout="return nd()" width="734">
                    <asp:Label ID="lang247" runat="server">Failure Modes, Causes And/Or Effects</asp:Label>
                </td>
            </tr>
        </table>
        <table class="view" id="tbeq" cellspacing="0" cellpadding="1" width="760">
            <tr>
                <td width="2">
                    &nbsp;
                </td>
                <td class="label" width="140">
                    <asp:Label ID="lang248" runat="server">Component Addressed:</asp:Label>
                </td>
                <td width="240">
                    <asp:DropDownList ID="ddcomp" runat="server" CssClass="plainlabel" Width="240px">
                    </asp:DropDownList>
                </td>
                <td width="40">
                    <img id="btnaddcomp" onmouseover="return overlib('Add/Edit Components for Selected Function', LEFT)"
                        onclick="GetCompDiv();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/addmod.gif"
                        runat="server"><img id="imgcopycomp" onmouseover="return overlib('Copy Components for Selected Function', LEFT)"
                            onclick="GetCompCopy();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                            runat="server">
                </td>
                <td class="label" width="30">
                    <asp:Label ID="Label25" runat="server" Height="20px">Qty:</asp:Label>
                </td>
                <td width="40">
                    <asp:TextBox ID="txtcQty" runat="server" CssClass="plainlabel" Width="40px"></asp:TextBox>
                </td>
                <td width="210" class="plainlabel">
                    <input id="cbpush" onclick="checkpush();" type="checkbox" runat="server" name="cbpush"><asp:Label
                        ID="lang249" runat="server">Copy to Component Library (Site Only)</asp:Label>
                </td>
            </tr>
            <tr height="30">
                <td colspan="7">
                    <table height="30" cellspacing="0" cellpadding="0" width="650">
                        <tr>
                            <td width="48">
                                &nbsp;
                            </td>
                            <td class="label" align="center" width="180">
                                <asp:Label ID="lang250" runat="server">Component Failure Modes</asp:Label>
                            </td>
                            <td width="22">
                            </td>
                            <td class="redlabel" align="center" width="200">
                                <asp:Label ID="lang251" runat="server">Not Addressed</asp:Label>
                            </td>
                            <td width="22">
                            </td>
                            <td class="bluelabel" align="center" width="180">
                                <asp:Label ID="lang252" runat="server">Selected</asp:Label>
                            </td>
                            <td width="48">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td align="center">
                                <asp:ListBox ID="lbCompFM" runat="server" CssClass="plainlabel" Width="150px" Height="60px"
                                    SelectionMode="Multiple"></asp:ListBox>
                            </td>
                            <td>
                                <img id="btnaddnewfail" onmouseover="return overlib('Add a New Failure Mode to the Component Selected Above')"
                                    onclick="GetFailDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
                                    width="20" runat="server"><br>
                                <asp:ImageButton ID="ibReuse" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                                </asp:ImageButton><img class="details" id="fromreusedis" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                                    width="20" runat="server">
                            </td>
                            <td align="center">
                                <asp:ListBox ID="lbfaillist" runat="server" CssClass="plainlabel" Width="150px" Height="60px"
                                    SelectionMode="Multiple" ForeColor="Red"></asp:ListBox>
                            </td>
                            <td>
                                <img class="details" id="todis" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                                    width="20" runat="server">
                                <img class="details" id="fromdis" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                    width="20" runat="server">
                                <asp:ImageButton ID="ibToTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                                </asp:ImageButton><br>
                                <asp:ImageButton ID="ibFromTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                                </asp:ImageButton>
                            </td>
                            <td align="center">
                                <asp:ListBox ID="lbfailmodes" runat="server" CssClass="plainlabel" Width="150px"
                                    Height="60px" SelectionMode="Multiple" ForeColor="Blue"></asp:ListBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" width="760">
            <tr>
                <td class="thdrsinglft" align="left" width="26">
                    <img src="../images/appbuttons/minibuttons/3gearsh.gif" border="0">
                </td>
                <td class="thdrsingrt label" onmouseover="return overlib('What are you going to do?')"
                    onmouseout="return nd()" width="734">
                    <asp:Label ID="lang253" runat="server">Task Activity Details</asp:Label>
                </td>
            </tr>
        </table>
        <table class="view" id="tbdtls" cellspacing="0" cellpadding="1" width="710">
            <tr>
                <td width="125">
                </td>
                <td width="310">
                </td>
                <td width="25">
                </td>
                <td width="100">
                </td>
                <td width="60">
                </td>
                <td width="70">
                </td>
                <td width="20">
                </td>
            </tr>
            <tr>
                <td class="label" valign="top" rowspan="3">
                    <asp:Label ID="lang254" runat="server">Task Description</asp:Label>
                </td>
                <td valign="top" rowspan="3">
                    <asp:TextBox ID="txtdesc" Width="300px" TextMode="MultiLine" MaxLength="1000" Rows="5"
                        runat="server" CssClass="plainlabel"></asp:TextBox>
                </td>
                <td width="25">
                    <img id="btnlookup" onclick="tasklookup();" src="../images/appbuttons/minibuttons/lookupgbg.gif"
                        runat="server">
                </td>
                <td class="label" width="100">
                    <asp:Label ID="lang255" runat="server">Task Type</asp:Label>
                </td>
                <td colspan="3">
                    <asp:DropDownList ID="ddtype" runat="server" CssClass="plainlabel" Width="160px"
                        Rows="1" DataTextField="tasktype" DataValueField="ttid">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <img id="btnlookup2" onclick="jumpto();" src="../images/appbuttons/minibuttons/magnifier.gif"
                        runat="server">
                </td>
                <td class="label">
                    <asp:Label ID="lang256" runat="server">PdM Tech</asp:Label>
                </td>
                <td colspan="3">
                    <asp:DropDownList ID="ddpt" runat="server" CssClass="plainlabel" Width="160px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <img onmouseover="return overlib('Generate Work Order for This Task')" onmouseout="return nd()"
                        onclick="getfail2();" alt="" src="../images/appbuttons/minibuttons/addmod.gif">
                </td>
                <td class="label" align="right" colspan="3">
                    <asp:Label ID="lang257" runat="server">Measurements Required?</asp:Label>
                </td>
                <td>
                    <img onmouseover="return overlib('Add/Edit Measurements for this Task')" onclick="getMeasDiv();"
                        onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/measure.gif"
                        width="27">
                </td>
            </tr>
        </table>
        <table cellspacing="0" width="760">
            <tr>
                <td class="thdrsinglft" align="left" width="26">
                    <img src="../images/appbuttons/minibuttons/3gearsh.gif" border="0">
                </td>
                <td class="thdrsingrt label" onmouseover="return overlib('How are you going to do it?')"
                    onmouseout="return nd()" width="734" colspan="3">
                    <asp:Label ID="lang258" runat="server">Planning &amp; Scheduling Details</asp:Label>
                </td>
            </tr>
        </table>
        <table class="view" id="tbreqs" cellspacing="0" cellpadding="0" width="700">
            <tr>
                <td colspan="3">
                    <table width="700">
                        <tr>
                            <td class="label" width="110">
                                <asp:Label ID="lang259" runat="server">Skill Required</asp:Label>
                            </td>
                            <td width="180">
                                <asp:DropDownList ID="ddskill" runat="server" Width="160px" CssClass="plainlabel">
                                </asp:DropDownList>
                                <img onmouseover="return overlib('Choose Skills for this Plant Site ')" onclick="getss();"
                                    onmouseout="return nd()" height="20" src="../images/appbuttons/minibuttons/plusminus.gif"
                                    width="20">
                            </td>
                            <td class="label" width="15">
                                Qty
                            </td>
                            <td width="20">
                                <asp:TextBox ID="txtqty" runat="server" CssClass="plainlabel" Width="30px"></asp:TextBox>
                            </td>
                            <td class="label" width="40">
                                <asp:Label ID="lang260" runat="server">Min Ea</asp:Label>
                            </td>
                            <td width="45">
                                <asp:TextBox ID="txttr" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                            </td>
                            <td class="label" width="80">
                                <asp:Label ID="lang261" runat="server">P-F Interval</asp:Label>
                            </td>
                            <td width="50">
                                <asp:TextBox ID="txtpfint" runat="server" CssClass="plainlabel" Width="40px"></asp:TextBox>
                            </td>
                            <td class="label" width="30">
                                <asp:Label ID="lang262" runat="server">Days</asp:Label>
                            </td>
                            <td width="20">
                                <img id="btnpfint" onmouseover="return overlib('Estimate Frequency using the PF Interval')"
                                    onclick="getPFDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/lilcalc.gif"
                                    width="20">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="700">
            <tr>
                <td colspan="10">
                    <table width="700">
                        <tr>
                            <td class="label" width="110">
                                <asp:Label ID="lang263" runat="server">Equipment Status</asp:Label>
                            </td>
                            <td width="95">
                                <asp:DropDownList ID="ddeqstat" runat="server" CssClass="plainlabel" Width="90px">
                                </asp:DropDownList>
                            </td>
                            <td class="label" width="40">
                                <asp:Label ID="lang264" runat="server">DTime</asp:Label>
                            </td>
                            <td width="40">
                                <asp:TextBox ID="txtrdt" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                            </td>
                            <td class="label" width="120">
                                <asp:CheckBox ID="cbloto" runat="server"></asp:CheckBox>LOTO&nbsp;&nbsp;<asp:CheckBox
                                    ID="cbcs" runat="server"></asp:CheckBox>CS
                            </td>
                            <td class="label" width="60">
                                <asp:Label ID="lang265" runat="server">Frequency</asp:Label>
                            </td>
                            <td width="60">
                                <asp:TextBox ID="txtfreq" runat="server" CssClass="plainlabel" Width="60px"></asp:TextBox>
                            </td>
                            <td width="20">
                                <input id="cbfixed" runat="server" type="checkbox" onmouseover="return overlib('Check if Frequency is Fixed (Calendar Frequency Only)')"
                                    onmouseout="return nd()" />
                            </td>
                            <td width="80">
                                <img alt="" onclick="getmeter();" onmouseover="return overlib('View, Add, or Edit Meter Frequency for this Task', ABOVE, LEFT)"
                                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/meter1.gif" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="plainlabelred" id="tdtpmhold" runat="server">
                </td>
                <td align="right" colspan="7">
                    <img onmouseover="return overlib('Choose Equipment Spare Parts for this Task')" onclick="GetSpareDiv();"
                        onmouseout="return nd()" src="../images/appbuttons/minibuttons/spareparts.gif"><img
                            id="img1" onmouseover="return overlib('Add/Edit Parts for this Task')" onclick="GetPartDiv();"
                            onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/parttrans.gif"
                            width="23" runat="server">
                    <img onmouseover="return overlib('Add/Edit Tools for this Task')" onclick="GetToolDiv();"
                        onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/tooltrans.gif"
                        width="23">
                    <img onmouseover="return overlib('Add/Edit Lubricants for this Task')" onclick="GetLubeDiv();"
                        onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/lubetrans.gif"
                        width="23">&nbsp;<img onmouseover="return overlib('Add/Edit Notes for this Task')"
                            onclick="GetNoteDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/notessm.gif"
                            width="25"><img id="Img5" onmouseover="return overlib('Edit Pass/Fail Adjustment Levels - This Task', ABOVE, LEFT)"
                                onclick="editadj('0');" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/screwd.gif"
                                border="0" runat="server">
                    <img class="details" onmouseover="return overlib('View or Edit Images for this Task')"
                        onclick="getti();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/pic.gif"
                        width="20">
                    &nbsp;<asp:Image ID="imgClock" runat="server" ImageUrl="../images/appbuttons/minibuttons/clock.gif"
                        Visible="False"></asp:Image>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="700">
            <tr class="view" id="trh4" runat="server">
                <td style="border-top: #7ba4e0 thin solid" align="center" width="20">
                    <img id="btnStart" height="20" src="../images/appbuttons/minibuttons/tostartbg.gif"
                        width="20" runat="server">
                </td>
                <td style="border-top: #7ba4e0 thin solid" align="center" width="20">
                    <img id="btnPrev" height="20" src="../images/appbuttons/minibuttons/prevarrowbg.gif"
                        width="20" runat="server">
                </td>
                <td class="bluelabel" style="border-top: #7ba4e0 thin solid" align="center" width="110">
                    <asp:Label ID="lang266" runat="server">Task#</asp:Label><asp:Label ID="lblpg" runat="server"
                        CssClass="bluelabel" ForeColor="Blue" Font-Size="X-Small" Font-Names="Arial"
                        Font-Bold="True"></asp:Label>&nbsp;of&nbsp;<asp:Label ID="lblcnt" runat="server"
                            CssClass="bluelabel" ForeColor="Blue" Font-Size="X-Small" Font-Names="Arial"
                            Font-Bold="True"></asp:Label>
                </td>
                <td style="border-top: #7ba4e0 thin solid" align="center" width="20">
                    <img id="btnNext" height="20" src="../images/appbuttons/minibuttons/nextarrowbg.gif"
                        width="20" runat="server">
                </td>
                <td style="border-top: #7ba4e0 thin solid" align="center" width="30">
                    <img id="btnEnd" height="20" src="../images/appbuttons/minibuttons/tolastbg.gif"
                        width="20" runat="server">
                </td>
                <td class="label" style="border-top: #7ba4e0 thin solid" align="center" width="70">
                    <asp:Label ID="lang267" runat="server">Task Order</asp:Label>
                </td>
                <td class="label" style="border-top: #7ba4e0 thin solid" width="40">
                    <asp:TextBox ID="txttaskorder" runat="server" Width="35px" CssClass="plainlabel"></asp:TextBox>
                </td>
                <td class="greenlabel" style="border-top: #7ba4e0 thin solid" align="left" width="150">
                    <asp:Label ID="Label26" runat="server" CssClass="greenlabel" Font-Size="X-Small"
                        Font-Names="Arial" Font-Bold="True"># of Sub Tasks: </asp:Label><asp:Label ID="lblsubcount"
                            runat="server" CssClass="greenlabel" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"></asp:Label>
                </td>
                <td style="border-top: #7ba4e0 thin solid" align="right" width="260">
                    <img onclick="gridret();" height="20" alt="" src="../images/appbuttons/bgbuttons/navgrid.gif"
                        width="20"><img id="btnedittask" onclick="doEnable();" height="19" alt="" src="../images/appbuttons/minibuttons/lilpentrans.gif"
                            width="19" border="0" runat="server"><asp:ImageButton ID="btnaddtsk" runat="server"
                                ImageUrl="../images/appbuttons/minibuttons/addnewbg1.gif" BorderStyle="None">
                    </asp:ImageButton>
                    <asp:ImageButton ID="btntpm" runat="server" ImageUrl="../images/appbuttons/minibuttons/compresstpm.gif">
                    </asp:ImageButton>
                    <img id="imgdeltpm" onmouseover="return overlib('Delete This Task From the TPM Module and Restore Editing', ABOVE, LEFT)"
                        onclick="checkdeltasktpm();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/cantpm.gif"
                        border="0" runat="server" class="details">
                    <asp:ImageButton ID="btnaddsubtask" runat="server" CssClass="details" ImageUrl="../images/appbuttons/minibuttons/subtask.gif"
                        BorderStyle="None"></asp:ImageButton><asp:ImageButton ID="btndeltask" runat="server"
                            ImageUrl="../images/appbuttons/minibuttons/del.gif"></asp:ImageButton><asp:ImageButton
                                ID="ibCancel" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif">
                            </asp:ImageButton><asp:ImageButton ID="btnsavetask" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                Visible="False"></asp:ImageButton><img id="btnsav" onclick="valpgnums();" height="20"
                                    alt="" src="../images/appbuttons/minibuttons/savedisk1.gif" width="20" runat="server"><img
                                        onmouseover="return overlib('View Reports')" onclick="getValueAnalysis();" onmouseout="return nd()"
                                        alt="" src="../images/appbuttons/minibuttons/report.gif"><img class="details" onmouseover="return overlib('Go to the Centralized PM Library', LEFT, WIDTH, 270)"
                                            onclick="GoToPMLib();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/gotolibsm.gif"
                                            width="23"><img id="ggrid" onmouseover="return overlib('Review Changes in Grid View')"
                                                onclick="GetGrid();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/grid1.gif"
                                                width="20" runat="server"><img id="sgrid" onmouseover="return overlib('Add/Edit Sub Tasks')"
                                                    onclick="getsgrid();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/sgrid1.gif"
                                                    width="20" runat="server"><img id="Img4" onmouseover="return overlib('Edit Pass/Fail Adjustment Levels - All Tasks', ABOVE, LEFT)"
                                                        onclick="editadj('pre');" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/screwall.gif"
                                                        border="0" runat="server">
                </td>
            </tr>
            <tr>
                <td align="center" colspan="10">
                    <asp:Label ID="msglbl" runat="server" CssClass="redlabel" Width="360px" ForeColor="Red"
                        Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div class="details" id="lstdiv" style="z-index: 999; border-bottom: black 1px solid;
        border-left: black 1px solid; width: 630px; height: 620px; border-top: black 1px solid;
        border-right: black 1px solid">
        <table cellspacing="0" cellpadding="0" width="630" bgcolor="white">
            <tr bgcolor="blue" height="20">
                <td class="labelwht">
                    <asp:Label ID="lang268" runat="server">List Dialog</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closelst();" height="18" alt="" src="../images/close.gif" width="18"><br>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <iframe id="iflst" style="width: 630px; height: 620px" runat="server"></iframe>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblsid" type="hidden" name="lblsid" runat="server"><input id="lbltaskid"
        type="hidden" name="lbltaskid" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server"><input id="lbltasklev"
        type="hidden" name="lbltasklev" runat="server">
    <input id="lbldid" type="hidden" name="lbldid" runat="server"><input id="lblclid"
        type="hidden" name="lblclid" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server"><input id="lblfuid"
        type="hidden" name="lblfuid" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server"><input id="lblfilt"
        type="hidden" name="lblfilt" runat="server">
    <input id="lblptid" type="hidden" name="lblptid" runat="server"><input id="lblpar"
        type="hidden" name="lblpar" runat="server">
    <input id="lblchk" type="hidden" runat="server"><input id="lblco" type="hidden" runat="server">
    <input id="lblfail" type="hidden" name="lblfail" runat="server"><input id="lblsb"
        type="hidden" runat="server">
    <input id="lblpgholder" type="hidden" runat="server"><input id="lblt" type="hidden"
        runat="server">
    <input id="lblst" type="hidden" runat="server"><input id="lblsvchk" type="hidden"
        runat="server">
    <input id="lblenable" type="hidden" runat="server"><input id="lblcompchk" type="hidden"
        runat="server">
    <input id="lblcompfailchk" type="hidden" runat="server"><input id="lblstart" type="hidden"
        runat="server">
    <input id="lblcurrsb" type="hidden" runat="server"><input id="lblcurrcs" type="hidden"
        runat="server">
    <input id="appchk" type="hidden" name="appchk" runat="server"><input id="lbllog"
        type="hidden" runat="server">
    <input id="pgflag" type="hidden" runat="server"><input id="lblfiltcnt" type="hidden"
        runat="server">
    <input id="lot" type="hidden" runat="server"><input id="cs" type="hidden" runat="server">
    <input id="lblusername" type="hidden" runat="server">
    <input id="lbllock" type="hidden" name="lbllock" runat="server">
    <input id="lbllockedby" type="hidden" name="lbllockedby" runat="server">
    <input id="lblhaspm" type="hidden" runat="server">
    <input id="lbltyp" type="hidden" name="lbltyp" runat="server">
    <input id="lbllid" type="hidden" name="lbllocid" runat="server">
    <input id="lblro" type="hidden" runat="server"><input type="hidden" id="lblnoeq"
        runat="server">
    <input type="hidden" id="lbltpmalert" runat="server"><input type="hidden" id="lbltpmhold"
        runat="server">
    <input type="hidden" id="lbloldtask" runat="server"><input id="lblnewkey" type="hidden"
        runat="server" name="lblnewkey">
    <input type="hidden" id="lblnewcomp" runat="server" name="lblnewcomp">
    <input type="hidden" id="lblnewdesc" runat="server" name="lblnewdesc">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblskillid" runat="server" />
    <input type="hidden" id="lblskill" runat="server" />
    <input type="hidden" id="lblskillqty" runat="server" />
    <input type="hidden" id="lblmeterid" runat="server" />
    <input type="hidden" id="lblrd" runat="server" />
    <input type="hidden" id="lblrdid" runat="server" />
    <input type="hidden" id="lblfunc" runat="server" />
    <input type="hidden" id="lblusemeter" runat="server" />
    <input type="hidden" id="lblmfid" runat="server" />
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblfreq" runat="server" />
    <input type="hidden" id="lblfreqo" runat="server" />
    <input type="hidden" id="lblfixed" runat="server" />
    <input type="hidden" id="lbltpmpmtskid" runat="server" />
    </form>
</body>
</html>
