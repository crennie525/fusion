<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TaskNotes.aspx.vb" Inherits="lucy_r12.TaskNotes" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>TaskNotes</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/TaskNotesaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True">Task#</asp:Label>
            </td>
            <td>
                <asp:Label ID="lbltsk" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label2" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True">Sub-Task#</asp:Label>
            </td>
            <td>
                <asp:Label ID="lblsub" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True"></asp:Label>
            </td>
            <td>
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif">
                </asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <asp:TextBox ID="txtnote" runat="server" CssClass="plainlabel" TextMode="MultiLine"
                    Height="168px" Width="384px"></asp:TextBox>
            </td>
        </tr>
    </table>
    <input id="lblptid" type="hidden" runat="server" name="lblptid"><input type="hidden"
        id="lbllog" runat="server" name="lbllog">
    <input type="hidden" id="lblsflg" runat="server" name="lblsflg">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
