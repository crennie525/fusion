﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="taskwo.aspx.vb" Inherits="lucy_r12.taskwo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts/dragitem.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMFailManaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
     <!--
        function getrt() {
            var chk = true //document.getElementById("cbcwo").checked;
            var wochk = document.getElementById("lblwo").value;
            if (chk == true) {
                if (wochk == "") {
                    var prob = document.getElementById("txtprob").value;
                    var corr = document.getElementById("txtcorr").value;
                    if (prob != "" && corr != "") {
                        //document.getElementById("lblsubmit").value="gencorr";
                        //document.getElementById("form1").submit();

                        document.getElementById("rtdiv").style.position = "absolute";
                        document.getElementById("rtdiv").style.top = "10px";
                        document.getElementById("rtdiv").style.left = "120px";
                        document.getElementById("rtdiv").className = "view";
                    }
                    else {
                        alert("Problem Code or Corrective Action Not Entered")
                    }
                }
                else {
                    alert("A Work Order Already Exists for this Occurrence")
                }
            }

        }
        function printwo() {
            var pm //= document.getElementById("lblpmid").value;
            var wo = document.getElementById("lblwo").value;
            if (wo == "") {
                alert("No Work Order Assigned for this Failure Mode")
            }
            else {
            alert("../appswo/woprint.aspx?typ=wo&pm=&wo=" + wo)
                window.open("../appswo/woprint.aspx?typ=wo&pm=" + pm + "&wo=" + wo, "repWin", popwin);
            }
        }
        function closert() {
            document.getElementById("lbldragid").value = "";
            document.getElementById("lblrowid").value = "";
            document.getElementById("rtdiv").className = "details";
            //document.getElementById("rtdiv1").className="details";
        }
        function getcal(fld) {
            var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
            if (eReturn) {
                var fldret = fld; //"txt" +
                document.getElementById(fldret).value = eReturn;
                document.getElementById("lblstart").value = eReturn;
            }
        }
        function getsuper(typ) {
            var skill //= document.getElementById("lblskillid").value;
            var ro = document.getElementById("lblro").value;
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    var retarr = eReturn.split(",")
                    if (typ == "sup") {
                        document.getElementById("lblsupid").value = retarr[0];
                        document.getElementById("txtsup").value = retarr[1];
                        document.getElementById("lblsup").value = retarr[1];
                    }
                    else {
                        document.getElementById("lblleadid").value = retarr[0];
                        document.getElementById("txtlead").value = retarr[1];
                        document.getElementById("lblsupid").value = retarr[2];
                        document.getElementById("txtsup").value = retarr[3];
                        document.getElementById("lbllead").value = retarr[1];
                        document.getElementById("lblsup").value = retarr[3];
                    }
                }
            }
        }
         //-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td width="100">
                </td>
                <td width="20">
                </td>
                <td width="80">
                </td>
                <td width="20">
                </td>
                <td width="130">
                </td>
                <td width="5">
                </td>
                <td width="50">
                </td>
                <td width="50">
                </td>
                <td width="250">
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True">Task#</asp:Label>
                </td>
                <td>
                </td>
                <td>
                    <asp:Label ID="lbltsk" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True"></asp:Label>
                </td>
                <td>
                </td>
                <td align="right" colspan="5">
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True">Equipment:</asp:Label>
                </td>
                <td>
                </td>
                <td colspan="6">
                    <asp:Label ID="lbleq" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True">Funtion:</asp:Label>
                </td>
                <td>
                </td>
                <td colspan="6">
                    <asp:Label ID="lblfu" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True">Component:</asp:Label>
                </td>
                <td>
                </td>
                <td colspan="6">
                    <asp:Label ID="lblco" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True">Work Order:</asp:Label>
                </td>
                <td>
                    <img id="iwoadd" onmouseover="return overlib('Create a Corrective or Emergency Maintenance Work Order')"
                        onclick="getrt();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/addwhite.gif"
                        runat="server">
                </td>
                <td>
                    <asp:Label ID="lblwonum" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True"></asp:Label>
                </td>
                <td>
                    <img id="imgi2" onmouseover="return overlib('Print This Work Order', ABOVE, LEFT)"
                        onclick="printwo();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/woprint.gif"
                        runat="server">
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label6" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True">Work Order Status:</asp:Label>
                </td>
                <td>
                </td>
                <td colspan="6">
                    <asp:Label ID="lblst" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="btmmenu plainlabel" colspan="5">
                    <asp:Label ID="lang605" runat="server">Problem Encountered</asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td class="btmmenu plainlabel" colspan="3">
                    <asp:Label ID="lang606" runat="server">Corrective Action</asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <asp:TextBox ID="txtprob" runat="server" CssClass="plainlabel" TextMode="MultiLine"
                        Height="120px" Width="360px"></asp:TextBox>
                </td>
                <td>
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtcorr" runat="server" CssClass="plainlabel" TextMode="MultiLine"
                        Height="120px" Width="352px"></asp:TextBox>
                </td>
            </tr>
            <tr class="details">
                <td colspan="7">
                    <table>
                        <tr>
                            <td class="bluelabel">
                                <asp:Label ID="lang607" runat="server">Generate Corrective Action Work Order?</asp:Label>
                            </td>
                            <td>
                                <input id="cbcwo" onclick="getrt();" type="checkbox" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="9" align="right">
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif">
                                </asp:ImageButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div class="details" id="rtdiv" style="z-index: 999; border-bottom: black 1px solid;
            border-left: black 1px solid; width: 370px; height: 180px; border-top: black 1px solid;
            border-right: black 1px solid">
            <table cellspacing="0" cellpadding="0" width="370" bgcolor="white">
                <tr bgcolor="blue" height="20">
                    <td class="whitelabel">
                        <asp:Label ID="lang608" runat="server">Work Order Details</asp:Label>
                    </td>
                    <td align="right">
                        <img onclick="closert();" height="18" alt="" src="../images/close.gif" width="18"><br>
                    </td>
                </tr>
                <tr class="details" id="trrt" height="180">
                    <td class="bluelabelb" valign="middle" align="center" colspan="2">
                        <asp:Label ID="lang609" runat="server">Moving Window...</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr height="20">
                                <td>
                                </td>
                                <td>
                                </td>
                                <td class="bluelabel">
                                    <asp:Label ID="lang610" runat="server">Skill Required</asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:DropDownList ID="ddskill" runat="server" Width="160px" CssClass="plainlabel">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr height="20">
                                <td>
                                    <input id="cbsupe" type="checkbox" name="cbsupe" runat="server">
                                </td>
                                <td>
                                    <img onmouseover="return overlib('Check to send email alert to Supervisor')" onmouseout="return nd()"
                                        src="../images/appbuttons/minibuttons/emailcb.gif">
                                </td>
                                <td class="bluelabel">
                                    <asp:Label ID="lang611" runat="server">Supervisor</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtsup" runat="server" CssClass="plainlabel" Width="130px" ReadOnly="True"></asp:TextBox>
                                </td>
                                <td>
                                    <img onclick="getsuper('sup');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                        border="0">
                                </td>
                            </tr>
                            <tr height="20">
                                <td>
                                    <input id="cbleade" type="checkbox" name="cbleade" runat="server">
                                </td>
                                <td>
                                    <img onmouseover="return overlib('Check to send email alert to Lead Craft')" onmouseout="return nd()"
                                        src="../images/appbuttons/minibuttons/emailcb.gif">
                                </td>
                                <td class="bluelabel">
                                    <asp:Label ID="lang612" runat="server">Lead Craft</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtlead" runat="server" CssClass="plainlabel" Width="130px" ReadOnly="True"></asp:TextBox>
                                </td>
                                <td>
                                    <img onclick="getsuper('lead');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                        border="0">
                                </td>
                            </tr>
                            <tr height="20">
                                <td>
                                </td>
                                <td>
                                </td>
                                <td class="bluelabel">
                                    <asp:Label ID="lang613" runat="server">Work Type</asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:DropDownList ID="ddwt" runat="server" CssClass="plainlabel">
                                        <asp:ListItem Value="CM" Selected="True">Corrective Maintenance</asp:ListItem>
                                        <asp:ListItem Value="EM">Emergency Maintenance</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr height="20">
                                <td>
                                </td>
                                <td>
                                </td>
                                <td class="bluelabel">
                                    <asp:Label ID="lang614" runat="server">Target Start</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtstart" runat="server" CssClass="plainlabel" Width="130px" ReadOnly="True"></asp:TextBox>
                                </td>
                                <td>
                                    <img onclick="getcal('txtstart');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                        width="19">
                                </td>
                            </tr>
                            <tr height="30">
                                <td align="right" colspan="5">
                                    <asp:ImageButton ID="btnwo" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif">
                                    </asp:ImageButton>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblwo" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lblfuid" runat="server" />
    <input type="hidden" id="lblcoid" runat="server" />
    <input type="hidden" id="lblro" runat="server" />
    <input type="hidden" id="lblcid" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblsup" runat="server" />
    <input type="hidden" id="lbllead" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblsave" runat="server" />
    <input type="hidden" id="lblpmtskid" runat="server" />
    <input type="hidden" id="lblpmfid" runat="server" />
    <input type="hidden" id="lblcomp" runat="server" />
    <input type="hidden" id="lblfunc" runat="server" />
    <input type="hidden" id="lbltask" runat="server" />
    <input type="hidden" id="lblstat" runat="server" />
    <input type="hidden" id="lblleadid" runat="server" />
    <input type="hidden" id="lblsupid" runat="server" />
    <input type="hidden" id="lblstart" runat="server" />
    <input type="hidden" id="lblcoi" runat="server" />
    </form>
</body>
</html>
