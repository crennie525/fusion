Public Class appgetfcdialog
    Inherits System.Web.UI.Page
    Dim sid, wonum, typ, coid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents iftd As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            wonum = Request.QueryString("wo").ToString
            coid = Request.QueryString("coid").ToString
            typ = "wo" 'Request.QueryString("typ").ToString
            iftd.Attributes.Add("src", "appgetfc.aspx?typ=" & typ & "&sid=" & sid & "&wo=" & wonum & "&coid=" & coid & "&date=" & Now)

        End If
    End Sub

End Class
