﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdminLogin.aspx.vb" Inherits="lucy_r12.AdminLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>Fusion Login Page</title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="scripts1/NewLoginaspx_1.js"></script>
    <script language="JavaScript" type="text/javascript" src="scripts2/jsfslangs.js"></script>
</head>
<body background="images/appimages/testbg1.gif" onload="lpload();">
    <noscript style="text-align: center; color: red">
        <asp:Label ID="lang3470" runat="server">Your browser does not support JavaScript! Reliability Fusion Requires that JavaScript is Enabled.</asp:Label></noscript>
    <form id="form1" method="post" runat="server">
    <div style="position: static; width: 1267px; height: 768px">
        <table width="900">
            <tr>
                <td colspan="4" height="30">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="160">
                </td>
                <td width="165">
                </td>
                <td width="365">
                    <asp:Label ID="lblch" runat="server" ForeColor="White">no</asp:Label>
                </td>
                <td width="160">
                </td>
            </tr>
            <tr>
                <td colspan="4" class="details" id="wb" align="center">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="left" colspan="2" height="36">
                    <asp:Image ID="Image2" runat="server" ImageUrl="images/appimages/welcome.jpg"></asp:Image>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="center" colspan="2" height="36">
                    <img id="Image3" runat="server" src="images/appimages/fusionmain1.gif">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td class="label" height="30">
                    &nbsp;
                </td>
                <td>
                </td>
            </tr>
            <tr class="details">
                <td>
                </td>
                <td align="center" class="bluelabel" colspan="2">
                    <table>
                        <tr>
                            <td class="bluelabel">
                                Choose Language&nbsp;
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlang" runat="server" AutoPostBack="True">
                                    <asp:ListItem Value="eng" Selected="True">English</asp:ListItem>
                                    <asp:ListItem Value="fre">French</asp:ListItem>
                                    <asp:ListItem Value="ger">German</asp:ListItem>
                                    <asp:ListItem Value="ita">Italian</asp:ListItem>
                                    <asp:ListItem Value="spa">Spanish</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td class="label" height="10">
                    &nbsp;
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="center" class="bluelabel" colspan="2">
                    <asp:Label ID="lang3471" runat="server">Please Log In...</asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="2">
                    &nbsp;
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="center" colspan="2">
                    <table cellspacing="0" cellpadding="0" width="530" id="tbllogin" border="0">
                        <tr>
                            <td width="80">
                            </td>
                            <td width="126">
                            </td>
                            <td width="126">
                            </td>
                            <td width="100">
                            </td>
                            <td width="44">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="lblname" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">User Name</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtuid" runat="server" Font-Size="X-Small" Font-Names="Arial" Width="120px"></asp:TextBox>
                            </td>
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="lblpass" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Password</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtpass" runat="server" Font-Size="X-Small" Font-Names="Arial" Width="120px"
                                    TextMode="Password"></asp:TextBox>
                            </td>
                            <td align="center">
                                <asp:ImageButton ID="btnlogin" runat="server" ImageUrl="images/appbuttons/bgbuttons/blogin.gif">
                                </asp:ImageButton>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                    <table id="tblupdate" class="details" cellspacing="0" cellpadding="0" width="420">
                        <tr>
                            <td class="redlabel" align="center">
                                <asp:Label ID="lang3472" runat="server">The Fusion Site is being updated and will be back on-line shortly.</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="redlabel" align="center">
                                <asp:Label ID="lang3473" runat="server">We apologize for the inconvenience.</asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
    <input id="pmapp" type="hidden" runat="server" /><input id="logbad" type="hidden"
        runat="server" />
    <input type="hidden" id="lblupdate" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblutcoffset" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
