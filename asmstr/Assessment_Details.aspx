<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Assessment_Details.aspx.vb" Inherits="lucy_r12.Assessment_Details" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Assessment_Details</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/Assessment_Detailsaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="gettop();">
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="bigblue"><asp:Label id="lang1630" runat="server">Reliability Best-Practices Benchmark</asp:Label></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<table>
							<tr>
								<td class="bluelabel"><asp:Label id="lang1631" runat="server">Current Category:</asp:Label></td>
								<td class="plainlabel" id="tdcurr" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><a href="Assessment_Master.aspx?atyp=u" class="AT"><asp:Label id="lang1632" runat="server">Return to Assessment Master</asp:Label></a></td>
				</tr>
				<tr>
					<td><asp:datagrid id="dgdepts" runat="server" CellPadding="0" GridLines="None" AllowCustomPaging="False"
							AutoGenerateColumns="False" CellSpacing="1">
							<FooterStyle BackColor="White"></FooterStyle>
							<EditItemStyle Height="15px"></EditItemStyle>
							<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
							<ItemStyle CssClass="plainlabel" BackColor="ControlLightLight"></ItemStyle>
							<HeaderStyle Height="20px" Width="50px" CssClass="btmmenu label_b"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="50px" CssClass="btmmenu label"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Check#">
									<HeaderStyle Width="50px" CssClass="btmmenu label"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label4 runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.bench_num") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id=txtname runat="server" Width="50px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.bench_num") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Check Point">
									<HeaderStyle Width="400px" CssClass="btmmenu label"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Width="300px" Text='<%# DataBinder.Eval(Container, "DataItem.bench_question") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="txtphone" runat="server" Width="400px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.bench_question") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Score">
									<HeaderStyle Width="50px" CssClass="btmmenu label"></HeaderStyle>
									<ItemTemplate>
										&nbsp;
										<asp:Label id="Label3" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.qscore") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										&nbsp;
										<asp:dropdownlist id="ddscore" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("qscore")) %>'>
											<asp:ListItem Value="0">0</asp:ListItem>
											<asp:ListItem Value="1">1</asp:ListItem>
											<asp:ListItem Value="2">2</asp:ListItem>
											<asp:ListItem Value="3">3</asp:ListItem>
											<asp:ListItem Value="4">4</asp:ListItem>
											<asp:ListItem Value="5">5</asp:ListItem>
										</asp:dropdownlist>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Comments">
									<HeaderStyle Width="300px" CssClass="btmmenu label"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label6" runat="server" Width="300px" Text='<%# DataBinder.Eval(Container, "DataItem.qcomments") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtqcomment" cssclass="plainlabel" runat="server" Width="300px" TextMode=MultiLine Height="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.qcomments") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<HeaderStyle Width="20px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label1 runat="server" Width="0px" Text='<%# DataBinder.Eval(Container, "DataItem.mid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblmid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.mid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<HeaderStyle Width="20px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label5" runat="server" Width="20px" Text='<%# DataBinder.Eval(Container, "DataItem.qid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblqid" runat="server" Width="20px" Text='<%# DataBinder.Eval(Container, "DataItem.qid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
								ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
						</asp:datagrid></td>
				</tr>
			</table>
			<input id="lbluid" type="hidden" name="lbluid" runat="server"> <input id="lblbenchid" type="hidden" name="lblbenchid" runat="server">
			<input type="hidden" id="lblcat" runat="server"> <input type="hidden" id="lbleditchk" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
