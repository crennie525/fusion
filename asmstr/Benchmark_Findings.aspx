<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Benchmark_Findings.aspx.vb" Inherits="lucy_r12.Benchmark_Findings" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Benchmark_Findings</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/Benchmark_Findingsaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="gettop();"  class="tbg">
		<form id="form1" method="post" runat="server">
			<table>
				<tr id="trreg">
					<td>
						<table style="BORDER-BOTTOM: black 1px solid">
							<tr>
								<td class="thdr label" id="tdpm" runat="server" width="130" height="22" onclick="getmain();"><asp:Label id="lang1637" runat="server">Data Master</asp:Label></td>
								<td class="thdrhov label" id="tdtpm" runat="server" width="130"><asp:Label id="lang1638" runat="server">Benchmark Findings</asp:Label></td>
								<td class="thdr label" id="tdp" runat="server" width="130" onclick="getsel();"><asp:Label id="lang1639" runat="server">Prime Selection</asp:Label></td>
								<td class="thdr label" id="tdwo" runat="server" width="130" onclick="getprime();"><asp:Label id="lang1640" runat="server">X-Y Matrix</asp:Label></td>
								<td width="520" id="tdx" runat="server">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td id="tdbench" runat="server"></td>
				</tr>
			</table>
			<input type="hidden" id="lbluid" runat="server"><input type="hidden" id="lblatyp" runat="server" NAME="lblatyp">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
