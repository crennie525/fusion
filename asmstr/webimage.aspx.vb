

'********************************************************
'*
'********************************************************



Imports System
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.Collections
Imports System.Math
Imports System.Drawing.Graphics
Public Class webimage
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim textimg As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        textimg = Request.QueryString("textimg").ToString
        DrawLabel(textimg)
    End Sub
    Private Sub DrawLabel(ByVal textimg As String)
        Dim objGraphics As Graphics
        Dim objBitmap As Bitmap
        Dim objwidth As Integer = 220
        If objwidth > 220 Then
            objwidth = 220
        End If
        objBitmap = New Bitmap(objwidth, 20)
        'Dim textimg As String = "test text for rotate"
        objGraphics = Graphics.FromImage(objBitmap)
        Dim isize As Integer = 10

        Dim sfont As New Font("Arial", isize + 1)
        Dim ssize As New SizeF
        ssize = objGraphics.MeasureString(textimg, sfont)
        Dim swid As Integer = ssize.Width
        Dim sheight As Integer = ssize.Height
        Dim rows As Integer
        Dim remainder As Decimal
        remainder = (ssize.Width - swid)
        Dim sadd As Integer
        Select Case isize
            Case 10
                sadd = 3
            Case 12
                sadd = 6
            Case 14
                sadd = 9
            Case 16
                sadd = 12
            Case 18
                sadd = 15
        End Select
        If remainder > 0 Then
            swid += sadd
        Else
            swid += (sadd / 2)
        End If
        Dim maxswid As Integer = 220

        If swid > maxswid Then
            rows = swid \ maxswid
            remainder = (swid Mod maxswid)
            If remainder > 0 Then
                rows += 1
            End If
            sheight = sheight * rows
            swid = maxswid
            'Dim strMessage As String =  tmod.getmsg("cdstr631" , "webimage.aspx.vb")
 
            'imgu.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        '" & (isize * rows) + 20 & "
        'lbldiv.Attributes.Add("style", "OVERFLOW: auto; WIDTH: 280px; HEIGHT: " & (sheight + 20) & "px;")
        objBitmap = New Bitmap(swid, sheight)
        Dim backColor As Color = objBitmap.GetPixel(1, 1)

        objBitmap.MakeTransparent(backColor)
        'objBitmap.MakeTransparent(objBitmap.GetPixel(0, objBitmap.Height - 1))
        objGraphics = Graphics.FromImage(objBitmap)
        objGraphics.FillRectangle(New SolidBrush(System.Drawing.Color.White), 0, 0, swid, sheight)
        Dim rect As New RectangleF
        rect.Width = swid
        rect.Height = sheight

        Dim sf As New Drawing.StringFormat
        Dim icolor As String = "blue" 'ddcolor.SelectedValue
        Dim icolor1 As Color
        Select Case icolor
            Case "red"
                icolor1 = System.Drawing.Color.Red
            Case "green"
                icolor1 = System.Drawing.Color.Green
            Case "orange"
                icolor1 = System.Drawing.Color.Orange
            Case "blue"
                icolor1 = System.Drawing.Color.Blue
            Case "yellow"
                icolor1 = System.Drawing.Color.Yellow
            Case "white"
                icolor1 = System.Drawing.Color.White
        End Select
        Dim blueBrush As Brush = New SolidBrush(icolor1)
        objGraphics.DrawString(textimg, New Font("Arial", isize, FontStyle.Bold), blueBrush, rect)
        'objGraphics.DrawString(textimg, New Font("arial", 12, FontStyle.Bold), blueBrush, 0, 0)

        'Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        'Dim strto As String = appstr + "/newsimages/"
        'Dim lab As Integer = lbllabelcnt.Value
        'picid = lblpicid.Value
        'Dim medstr As String = "imglbl" & picid & "-" & lab & ".gif"
        'If File.Exists(Server.MapPath("\") & strto & medstr) Then
        'File.Delete1(Server.MapPath("\") & strto & medstr)
        'End If
        'lab += 1
        'lbllabelcnt.Value = lab
        'medstr = "imglbl" & picid & "-" & lab & ".gif"
        'Dim ssend As String = medstr
        'lbllabelimg.Value = ssend
        'objBitmap.Save(Server.MapPath("\") & strto & medstr)
        'imgtext.Attributes.Add("src", "../newsimages/" & medstr)
        Response.ContentType = "image/gif"
        objBitmap.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipXY)
        objBitmap.Save(Response.OutputStream, ImageFormat.Gif)


        objBitmap.Dispose()
        objGraphics.Dispose()
        objBitmap = Nothing
        objGraphics = Nothing

    End Sub
End Class
