

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class Assessment_Details
    Inherits System.Web.UI.Page
	Protected WithEvents lang1632 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1631 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1630 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim ass As New Utilities
    Dim uid, benchid, cat As String
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dgdepts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents tdcurr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleditchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbenchid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then

            uid = Request.QueryString("uid").ToString
            lbluid.value = uid
            benchid = Request.QueryString("benchid").ToString
            lblbenchid.Value = benchid
            cat = Request.QueryString("cat").ToString
            tdcurr.InnerHtml = cat

            ass.Open()
            GetAssDetails()
            ass.Dispose()

        End If
    End Sub
    Private Sub GetAssDetails()
        uid = lbluid.Value
        benchid = lblbenchid.Value
        sql = "usp_checkuserassess '" & uid & "','" & benchid & "'"
        dr = ass.GetRdrData(sql)
        dgdepts.DataSource = dr
        dgdepts.DataBind()


    End Sub

    Private Sub dgdepts_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.EditCommand
        lbleditchk.Value = "1"

        dgdepts.EditItemIndex = e.Item.ItemIndex
        ass.Open()
        GetAssDetails()
        ass.Dispose()
    End Sub

    Private Sub dgdepts_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.CancelCommand
        lbleditchk.Value = "1"

        dgdepts.EditItemIndex = -1
        ass.Open()
        GetAssDetails()
        ass.Dispose()
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function
    Private Sub dgdepts_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.UpdateCommand
        lbleditchk.Value = "1"

        Dim mid, qid, score, comment As String
        uid = lbluid.Value
        qid = CType(e.Item.FindControl("lblqid"), Label).Text
        mid = CType(e.Item.FindControl("lblmid"), Label).Text
        score = CType(e.Item.FindControl("ddscore"), DropDownList).SelectedValue
        If score = "" Then
            score = "0"
        End If
        comment = CType(e.Item.FindControl("txtqcomment"), TextBox).Text
        Dim schk As Long
        Try
            schk = System.Convert.ToInt64(score)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr626" , "Assessment_Details.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        comment = Replace(comment, "'", Chr(180), , , vbTextCompare)

        If Len(comment) > 1000 Then
            Dim strMessage As String =  tmod.getmsg("cdstr627" , "Assessment_Details.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If

        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_updatebench @mid, @qid, @score, @comment, @uid"
        Dim param As SqlParameter = New SqlParameter("@mid", SqlDbType.Int)
        param.Value = mid
        cmd.Parameters.Add(param)

        Dim param1 As SqlParameter = New SqlParameter("@qid", SqlDbType.Int)
        param1.Value = qid
        cmd.Parameters.Add(param1)

        Dim param01 As SqlParameter = New SqlParameter("@score", SqlDbType.VarChar)
        If score = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = score
        End If
        cmd.Parameters.Add(param01)

        Dim param02 As SqlParameter = New SqlParameter("@comment", SqlDbType.VarChar)
        If comment = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = comment
        End If
        cmd.Parameters.Add(param02)

        Dim param03 As SqlParameter = New SqlParameter("@uid", SqlDbType.VarChar)
        If uid = "" Then
            param03.Value = System.DBNull.Value
        Else
            param03.Value = uid
        End If
        cmd.Parameters.Add(param03)

        ass.Open()
        ass.UpdateHack(cmd)
        dgdepts.EditItemIndex = -1
        GetAssDetails()
        ass.Dispose()
    End Sub
	

	

	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgdepts.Columns(0).HeaderText = dlabs.GetDGPage("Assessment_Details.aspx","dgdepts","0")
		Catch ex As Exception
		End Try
		Try
			dgdepts.Columns(1).HeaderText = dlabs.GetDGPage("Assessment_Details.aspx","dgdepts","1")
		Catch ex As Exception
		End Try
		Try
			dgdepts.Columns(2).HeaderText = dlabs.GetDGPage("Assessment_Details.aspx","dgdepts","2")
		Catch ex As Exception
		End Try
		Try
			dgdepts.Columns(3).HeaderText = dlabs.GetDGPage("Assessment_Details.aspx","dgdepts","3")
		Catch ex As Exception
		End Try
		Try
			dgdepts.Columns(4).HeaderText = dlabs.GetDGPage("Assessment_Details.aspx","dgdepts","4")
		Catch ex As Exception
		End Try

	End Sub

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang1630.Text = axlabs.GetASPXPage("Assessment_Details.aspx","lang1630")
		Catch ex As Exception
		End Try
		Try
			lang1631.Text = axlabs.GetASPXPage("Assessment_Details.aspx","lang1631")
		Catch ex As Exception
		End Try
		Try
			lang1632.Text = axlabs.GetASPXPage("Assessment_Details.aspx","lang1632")
		Catch ex As Exception
		End Try

	End Sub

End Class
