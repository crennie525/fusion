

'********************************************************
'*
'********************************************************



Public Class Assessment_Main2
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim atyp, uid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifas As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            atyp = Request.QueryString("atyp").ToString
            Response.Redirect("Assessment_Main2.aspx?atyp=" & atyp)
        Catch ex As Exception

        End Try
        If Not IsPostBack Then
            atyp = Request.QueryString("atyp").ToString
            uid = HttpContext.Current.Session("dfltps").ToString
            ifas.Attributes.Add("src", "Assessment_Master.aspx?atyp=" & atyp & "&uid=" & uid) 'http://www.laiproducts.com/Benchmark/asmstr/
        End If
    End Sub

End Class
