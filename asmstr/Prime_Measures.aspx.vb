

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Reflection


Public Class Prime_Measures
    Inherits System.Web.UI.Page
	Protected WithEvents lang1649 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1648 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1647 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1646 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1645 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1644 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1643 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1642 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1641 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim ds As DataSet
    Dim ass As New Utilities
    Protected WithEvents dlprime As System.Web.UI.WebControls.DataList
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblatyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim uid, atyp As String
    Protected WithEvents tdp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbleditchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdx As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            atyp = Request.QueryString("atyp").ToString
            lblatyp.Value = atyp
            If atyp <> "a" Then
                tdp.Attributes.Add("class", "details")
                tdx.Attributes.Add("width", "650px")
            End If
            uid = Request.QueryString("uid").ToString
            lbluid.Value = uid
            ass.Open()
            GetPrime()
            ass.Dispose()
        End If


    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function
    Private Sub GetPrime()
        uid = lbluid.Value
        sql = "select count(*) from bench_prime where uid = '" & uid & "'"
        Dim cnt As Integer = ass.Scalar(sql)
        lblcnt.Value = cnt
        sql = "select p.*, c.bench_title from bench_prime_xy p left join bench_cats c on c.bench_id = p.bench_id " _
        + "where p.uid = '" & uid & "' order by p.bench_order"
        ds = ass.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        dlprime.DataSource = dv
        dlprime.DataBind()

    End Sub

    Private Sub dlprime_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlprime.ItemDataBound
        Dim icnt As Integer = 0
        Dim cnt As Integer = lblcnt.Value
        If e.Item.ItemType = ListItemType.Header Then
            Dim i1 As HtmlImage = CType(e.Item.FindControl("img1"), HtmlImage)
            Dim i2 As HtmlImage = CType(e.Item.FindControl("img2"), HtmlImage)
            Dim i3 As HtmlImage = CType(e.Item.FindControl("img3"), HtmlImage)
            Dim i4 As HtmlImage = CType(e.Item.FindControl("img4"), HtmlImage)
            Dim i5 As HtmlImage = CType(e.Item.FindControl("img5"), HtmlImage)
            Dim i6 As HtmlImage = CType(e.Item.FindControl("img6"), HtmlImage)
            Dim i7 As HtmlImage = CType(e.Item.FindControl("img7"), HtmlImage)
            Dim i8 As HtmlImage = CType(e.Item.FindControl("img8"), HtmlImage)
            Dim i9 As HtmlImage = CType(e.Item.FindControl("img9"), HtmlImage)
            Dim i10 As HtmlImage = CType(e.Item.FindControl("img10"), HtmlImage)

            Dim i11 As HtmlImage = CType(e.Item.FindControl("img11"), HtmlImage)
            Dim i12 As HtmlImage = CType(e.Item.FindControl("img12"), HtmlImage)
            Dim i13 As HtmlImage = CType(e.Item.FindControl("img13"), HtmlImage)
            Dim i14 As HtmlImage = CType(e.Item.FindControl("img14"), HtmlImage)
            Dim i15 As HtmlImage = CType(e.Item.FindControl("img15"), HtmlImage)

            uid = lbluid.Value
            sql = "select * from bench_prime where uid = '" & uid & "' order by prime_order"
            ds = ass.GetDSData(sql)
            Dim dt As New DataTable
            dt = ds.Tables(0)
            Dim drM As DataRow
            Dim img, ord As String



            For Each drM In dt.Select()
                ord = drM("prime_order").ToString
                img = drM("prime_measure").ToString
                icnt += 1
                Select Case ord
                    Case "15"
                        i15.Attributes.Add("src", "webimage.aspx?textimg=" & img)
                    Case "14"
                        i14.Attributes.Add("src", "webimage.aspx?textimg=" & img)
                    Case "13"
                        i13.Attributes.Add("src", "webimage.aspx?textimg=" & img)
                    Case "12"
                        i12.Attributes.Add("src", "webimage.aspx?textimg=" & img)
                    Case "11"
                        i11.Attributes.Add("src", "webimage.aspx?textimg=" & img)
                    Case "10"
                        i10.Attributes.Add("src", "webimage.aspx?textimg=" & img)
                    Case "9"
                        i9.Attributes.Add("src", "webimage.aspx?textimg=" & img)
                    Case "8"
                        i8.Attributes.Add("src", "webimage.aspx?textimg=" & img)
                    Case "7"
                        i7.Attributes.Add("src", "webimage.aspx?textimg=" & img)
                    Case "6"
                        i6.Attributes.Add("src", "webimage.aspx?textimg=" & img)
                    Case "5"
                        i5.Attributes.Add("src", "webimage.aspx?textimg=" & img)
                    Case "4"
                        i4.Attributes.Add("src", "webimage.aspx?textimg=" & img)
                    Case "3"
                        i3.Attributes.Add("src", "webimage.aspx?textimg=" & img)
                    Case "2"
                        i2.Attributes.Add("src", "webimage.aspx?textimg=" & img)
                    Case "1"
                        i1.Attributes.Add("src", "webimage.aspx?textimg=" & img)
                End Select

            Next
            Dim td As HtmlTableCell
            If cnt < 15 Then
                td = CType(e.Item.FindControl("tt15"), HtmlTableCell)
                td.Attributes.Add("class", "details")
                td = CType(e.Item.FindControl("ti15"), HtmlTableCell)
                td.Attributes.Add("class", "details")
                td = CType(e.Item.FindControl("tw15"), HtmlTableCell)
                td.Attributes.Add("class", "details")
            End If
            If cnt < 14 Then
                td = CType(e.Item.FindControl("tt14"), HtmlTableCell)
                td.Attributes.Add("class", "details")
                td = CType(e.Item.FindControl("ti14"), HtmlTableCell)
                td.Attributes.Add("class", "details")
                td = CType(e.Item.FindControl("tw14"), HtmlTableCell)
                td.Attributes.Add("class", "details")
            End If
            If cnt < 13 Then
                td = CType(e.Item.FindControl("tt13"), HtmlTableCell)
                td.Attributes.Add("class", "details")
                td = CType(e.Item.FindControl("ti13"), HtmlTableCell)
                td.Attributes.Add("class", "details")
                td = CType(e.Item.FindControl("tw13"), HtmlTableCell)
                td.Attributes.Add("class", "details")
            End If
            If cnt < 12 Then
                td = CType(e.Item.FindControl("tt12"), HtmlTableCell)
                td.Attributes.Add("class", "details")
                td = CType(e.Item.FindControl("ti12"), HtmlTableCell)
                td.Attributes.Add("class", "details")
                td = CType(e.Item.FindControl("tw12"), HtmlTableCell)
                td.Attributes.Add("class", "details")
            End If
            If cnt < 11 Then
                td = CType(e.Item.FindControl("tt11"), HtmlTableCell)
                td.Attributes.Add("class", "details")
                td = CType(e.Item.FindControl("ti11"), HtmlTableCell)
                td.Attributes.Add("class", "details")
                td = CType(e.Item.FindControl("tw11"), HtmlTableCell)
                td.Attributes.Add("class", "details")
            End If

        End If
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim td As HtmlTableCell
            If cnt < 15 Then
                td = CType(e.Item.FindControl("tr15"), HtmlTableCell)
                td.Attributes.Add("class", "details")
            End If
            If cnt < 14 Then
                td = CType(e.Item.FindControl("tr14"), HtmlTableCell)
                td.Attributes.Add("class", "details")
            End If
            If cnt < 13 Then
                td = CType(e.Item.FindControl("tr13"), HtmlTableCell)
                td.Attributes.Add("class", "details")
            End If
            If cnt < 12 Then
                td = CType(e.Item.FindControl("tr12"), HtmlTableCell)
                td.Attributes.Add("class", "details")
            End If
            If cnt < 11 Then
                td = CType(e.Item.FindControl("tr11"), HtmlTableCell)
                td.Attributes.Add("class", "details")
            End If
        End If
        If e.Item.ItemType = ListItemType.EditItem Then
            Dim td As HtmlTableCell
            If cnt < 15 Then
                td = CType(e.Item.FindControl("te15"), HtmlTableCell)
                td.Attributes.Add("class", "details")
            End If
            If cnt < 14 Then
                td = CType(e.Item.FindControl("te14"), HtmlTableCell)
                td.Attributes.Add("class", "details")
            End If
            If cnt < 13 Then
                td = CType(e.Item.FindControl("te13"), HtmlTableCell)
                td.Attributes.Add("class", "details")
            End If
            If cnt < 12 Then
                td = CType(e.Item.FindControl("te12"), HtmlTableCell)
                td.Attributes.Add("class", "details")
            End If
            If cnt < 11 Then
                td = CType(e.Item.FindControl("te11"), HtmlTableCell)
                td.Attributes.Add("class", "details")
            End If
        End If

    End Sub

    Private Sub dlprime_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlprime.EditCommand
        lbleditchk.Value = "1"
        ass.Open()
        dlprime.EditItemIndex = e.Item.ItemIndex
        GetPrime()
        ass.Dispose()
    End Sub

    Private Sub dlprime_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlprime.CancelCommand
        lbleditchk.Value = "1"
        ass.Open()
        dlprime.EditItemIndex = -1
        GetPrime()
        ass.Dispose()
    End Sub

    Private Sub dlprime_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlprime.UpdateCommand
        lbleditchk.Value = "1"
        ass.Open()
        Dim d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, xyid As String
        xyid = CType(e.Item.FindControl("lblxyid"), Label).Text
        d1 = CType(e.Item.FindControl("dd1"), DropDownList).SelectedValue
        d2 = CType(e.Item.FindControl("dd2"), DropDownList).SelectedValue
        d3 = CType(e.Item.FindControl("dd3"), DropDownList).SelectedValue
        d4 = CType(e.Item.FindControl("dd4"), DropDownList).SelectedValue
        d5 = CType(e.Item.FindControl("dd5"), DropDownList).SelectedValue
        d6 = CType(e.Item.FindControl("dd6"), DropDownList).SelectedValue
        d7 = CType(e.Item.FindControl("dd7"), DropDownList).SelectedValue
        d8 = CType(e.Item.FindControl("dd8"), DropDownList).SelectedValue
        d9 = CType(e.Item.FindControl("dd9"), DropDownList).SelectedValue
        d10 = CType(e.Item.FindControl("dd10"), DropDownList).SelectedValue

        d11 = CType(e.Item.FindControl("dd11"), DropDownList).SelectedValue
        d12 = CType(e.Item.FindControl("dd12"), DropDownList).SelectedValue
        d13 = CType(e.Item.FindControl("dd13"), DropDownList).SelectedValue
        d14 = CType(e.Item.FindControl("dd14"), DropDownList).SelectedValue
        d15 = CType(e.Item.FindControl("dd15"), DropDownList).SelectedValue

        sql = "usp_updateprime '" & xyid & "','" & d1 & "','" & d2 & "','" & d3 & "','" & d4 & "','" & d5 & "','" & d6 & "','" & d7 & "','" & d8 & "','" & d9 & "','" & d10 & "','" & d11 & "','" & d12 & "','" & d13 & "','" & d14 & "','" & d15 & "'"
        ass.Update(sql)

        dlprime.EditItemIndex = -1
        GetPrime()
        ass.Dispose()

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1641.Text = axlabs.GetASPXPage("Prime_Measures.aspx", "lang1641")
        Catch ex As Exception
        End Try
        Try
            lang1642.Text = axlabs.GetASPXPage("Prime_Measures.aspx", "lang1642")
        Catch ex As Exception
        End Try
        Try
            lang1643.Text = axlabs.GetASPXPage("Prime_Measures.aspx", "lang1643")
        Catch ex As Exception
        End Try
        Try
            lang1644.Text = axlabs.GetASPXPage("Prime_Measures.aspx", "lang1644")
        Catch ex As Exception
        End Try
        Try
            lang1645.Text = axlabs.GetASPXPage("Prime_Measures.aspx", "lang1645")
        Catch ex As Exception
        End Try
        Try
            lang1646.Text = axlabs.GetASPXPage("Prime_Measures.aspx", "lang1646")
        Catch ex As Exception
        End Try
        Try
            lang1647.Text = axlabs.GetASPXPage("Prime_Measures.aspx", "lang1647")
        Catch ex As Exception
        End Try
        Try
            lang1648.Text = axlabs.GetASPXPage("Prime_Measures.aspx", "lang1648")
        Catch ex As Exception
        End Try
        Try
            lang1649.Text = axlabs.GetASPXPage("Prime_Measures.aspx", "lang1649")
        Catch ex As Exception
        End Try

    End Sub

End Class
