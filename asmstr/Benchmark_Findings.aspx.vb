

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text


Public Class Benchmark_Findings
    Inherits System.Web.UI.Page
	Protected WithEvents lang1640 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1639 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1638 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1637 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim ass As New Utilities
    Dim uid, atyp As String
    Protected WithEvents tdpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblatyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdx As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdbench As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            atyp = Request.QueryString("atyp").ToString
            lblatyp.Value = atyp
            If atyp <> "a" Then
                tdp.Attributes.Add("class", "details")
                tdx.Attributes.Add("width", "650px")
            End If
            uid = HttpContext.Current.Session("dfltps").ToString '"PM Admin" '
            lbluid.Value = uid
            ass.Open()
            GetFindings()
            ass.Dispose()

        End If
    End Sub
    Private Sub GetFindings()
        uid = lbluid.Value
        Dim sb As New StringBuilder

        sb.Append("<table cellspacing=""3"" cellpadding=""3"" width=""870"" border=""0"">" & vbCrLf)
        sb.Append("<tr><td class=""bigblue"" colspan=""7"">" & tmod.getlbl("cdlbl347" , "Benchmark_Findings.aspx.vb") & "</td></tr>" & vbCrLf)

        sb.Append("<tr><td width=""30"" class=""label"" bgcolor=""yellow"">#</td>" & vbCrLf)
        sb.Append("<td width=""340"" class=""label"" bgcolor=""yellow"">" & tmod.getlbl("cdlbl349" , "Benchmark_Findings.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td width=""100"" class=""label"" bgcolor=""yellow"">" & tmod.getlbl("cdlbl350" , "Benchmark_Findings.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td width=""100"" class=""label"" bgcolor=""yellow"">" & tmod.getlbl("cdlbl351" , "Benchmark_Findings.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td width=""100"" class=""label"" bgcolor=""yellow"">" & tmod.getlbl("cdlbl352" , "Benchmark_Findings.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td width=""100"" class=""label"" bgcolor=""yellow"">" & tmod.getlbl("cdlbl353" , "Benchmark_Findings.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td width=""100"" class=""label"" bgcolor=""yellow"">" & tmod.getlbl("cdlbl354" , "Benchmark_Findings.aspx.vb") & "</td><tr>" & vbCrLf)


        sql = "select c.bench_id, c.bench_title, c.success_avg, b.bench_order, b.customer_score, b.weight, " _
        + "b.dscore, b.opportunity, c.bench_id from bench_findings b " _
        + "left join bench_cats c on c.bench_order = b.bench_order " _
        + "where b.uid = '" & uid & "' order by b.bench_order"

        dr = ass.GetRdrData(sql)
        While dr.Read
            sb.Append("<tr>")
            sb.Append("<td class=""ATL"">" & dr.Item("bench_id").ToString & "</td>")
            sb.Append("<td class=""ATL"">" & dr.Item("bench_title").ToString & "</td>")
            sb.Append("<td class=""label"" align=""center"">" & dr.Item("success_avg").ToString & "</td>")
            sb.Append("<td class=""label"" align=""center"">" & dr.Item("customer_score").ToString & "</td>")
            sb.Append("<td class=""label"" align=""center"">" & dr.Item("weight").ToString & "</td>")
            sb.Append("<td class=""label"" align=""center"">" & dr.Item("dscore").ToString & "</td>")
            sb.Append("<td class=""label"" align=""center"">" & dr.Item("opportunity").ToString & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()

        sb.Append("</table>")
        tdbench.InnerHtml = sb.ToString

    End Sub

	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang1637.Text = axlabs.GetASPXPage("Benchmark_Findings.aspx","lang1637")
		Catch ex As Exception
		End Try
		Try
			lang1638.Text = axlabs.GetASPXPage("Benchmark_Findings.aspx","lang1638")
		Catch ex As Exception
		End Try
		Try
			lang1639.Text = axlabs.GetASPXPage("Benchmark_Findings.aspx","lang1639")
		Catch ex As Exception
		End Try
		Try
			lang1640.Text = axlabs.GetASPXPage("Benchmark_Findings.aspx","lang1640")
		Catch ex As Exception
		End Try

	End Sub

End Class
