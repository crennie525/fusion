<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Prime_Measures.aspx.vb" Inherits="lucy_r12.Prime_Measures" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Prime_Measures</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/Prime_Measuresaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="gettop();" class="tbg">
		<form id="form1" method="post" runat="server">
			<table>
				<TBODY>
					<tr id="trreg">
						<td>
							<table style="BORDER-BOTTOM: black 1px solid">
								<tr>
									<td class="thdr label" id="tdpm" runat="server" width="130" height="22" onclick="getmain();"><asp:Label id="lang1641" runat="server">Data Master</asp:Label></td>
									<td class="thdr label" id="tdtpm" runat="server" width="130" onclick="getfind();"><asp:Label id="lang1642" runat="server">Benchmark Findings</asp:Label></td>
									<td class="thdr label" id="tdp" runat="server" width="130" onclick="getsel();"><asp:Label id="lang1643" runat="server">Prime Selection</asp:Label></td>
									<td class="thdrhov label" id="tdwo" runat="server" width="130"><asp:Label id="lang1644" runat="server">X-Y Matrix</asp:Label></td>
									<td width="520" id="tdx" runat="server">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><asp:datalist id="dlprime" runat="server">
								<HeaderTemplate>
									<table cellspacing="1">
										<tr>
											<td width="60"></td>
											<td width="35"></td>
											<td width="200"></td>
											<td width="50"></td>
											<td width="40" id="tt15" runat="server"></td>
											<td width="40" id="tt14" runat="server"></td>
											<td width="40" id="tt13" runat="server"></td>
											<td width="40" id="tt12" runat="server"></td>
											<td width="40" id="tt11" runat="server"></td>
											<td width="40"></td>
											<td width="40"></td>
											<td width="40"></td>
											<td width="40"></td>
											<td width="40"></td>
											<td width="40"></td>
											<td width="40"></td>
											<td width="40"></td>
											<td width="40"></td>
											<td width="40"></td>
											<td width="100"></td>
										</tr>
										<tr>
											<td bgcolor="white" colspan="4" align="center" valign="center" class="bigblue"><asp:Label id="lang1645" runat="server">Value Exercise</asp:Label></td>
											<td bgcolor="white" id="ti15" runat="server" valign="bottom" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"><img src="" id="img15" runat="server"></td>
											<td bgcolor="white" id="ti14" runat="server" valign="bottom" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"><img src="" id="img14" runat="server"></td>
											<td bgcolor="white" id="ti13" runat="server" valign="bottom" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"><img src="" id="img13" runat="server"></td>
											<td bgcolor="white" id="ti12" runat="server" valign="bottom" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"><img src="" id="img12" runat="server"></td>
											<td bgcolor="white" id="ti11" runat="server" valign="bottom" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"><img src="" id="img11" runat="server"></td>
											<td bgcolor="white" valign="bottom" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"><img src="" id="img10" runat="server"></td>
											<td bgcolor="white" valign="bottom" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"><img src="" id="img9" runat="server"></td>
											<td bgcolor="white" valign="bottom" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"><img src="" id="img8" runat="server"></td>
											<td bgcolor="white" valign="bottom" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"><img src="" id="img7" runat="server"></td>
											<td bgcolor="white" valign="bottom" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"><img src="" id="img6" runat="server"></td>
											<td bgcolor="white" valign="bottom" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"><img src="" id="img5" runat="server"></td>
											<td bgcolor="white" valign="bottom" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"><img src="" id="img4" runat="server"></td>
											<td bgcolor="white" valign="bottom" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"><img src="" id="img3" runat="server"></td>
											<td bgcolor="white" valign="bottom" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"><img src="" id="img2" runat="server"></td>
											<td bgcolor="white" valign="bottom" style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"><img src="" id="img1" runat="server"></td>
											<td></td>
										</tr>
										<tr>
											<td class="thdrsing label"><asp:Label id="lang1646" runat="server">Edit</asp:Label></td>
											<td class="thdrsing label">#</td>
											<td class="thdrsing label"><asp:Label id="lang1647" runat="server">Element</asp:Label></td>
											<td class="thdrsing label"><asp:Label id="lang1648" runat="server">Weight</asp:Label></td>
											<td id="tw15" runat="server" align="center" class="label" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid">15</td>
											<td id="tw14" runat="server" align="center" class="label" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid">14</td>
											<td id="tw13" runat="server" align="center" class="label" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid">13</td>
											<td id="tw12" runat="server" align="center" class="label" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid">12</td>
											<td id="tw11" runat="server" align="center" class="label" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid">11</td>
											<td align="center" class="label" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid">10</td>
											<td align="center" class="label" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid">9</td>
											<td align="center" class="label" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid">8</td>
											<td align="center" class="label" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid">7</td>
											<td align="center" class="label" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid">6</td>
											<td align="center" class="label" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid">5</td>
											<td align="center" align="center" class="label" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid">4</td>
											<td align="center" class="label" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid">3</td>
											<td align="center" class="label" style="BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid">2</td>
											<td align="center" class="label" style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid">1</td>
											<td class="thdrsing label"><asp:Label id="lang1649" runat="server">Value Score</asp:Label></td>
										</tr>
								</HeaderTemplate>
								<ItemTemplate>
									<tr>
										<td><asp:ImageButton id="Imagebutton1" runat="server" ToolTip="Edit Record" CommandName="Edit" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"></asp:ImageButton></td>
										<td class="plainlabelblue"><%# DataBinder.Eval(Container, "DataItem.bench_id") %></td>
										<td class="plainlabelblue" colspan="2"><%# DataBinder.Eval(Container, "DataItem.bench_title") %></td>
										<td id="tr15" runat="server" class="plainlabel" align="center"><%# DataBinder.Eval(Container, "DataItem.w15") %></td>
										<td id="tr14" runat="server" class="plainlabel" align="center"><%# DataBinder.Eval(Container, "DataItem.w14") %></td>
										<td id="tr13" runat="server" class="plainlabel" align="center"><%# DataBinder.Eval(Container, "DataItem.w13") %></td>
										<td id="tr12" runat="server" class="plainlabel" align="center"><%# DataBinder.Eval(Container, "DataItem.w12") %></td>
										<td id="tr11" runat="server" class="plainlabel" align="center"><%# DataBinder.Eval(Container, "DataItem.w11") %></td>
										<td class="plainlabel" align="center"><%# DataBinder.Eval(Container, "DataItem.w10") %></td>
										<td class="plainlabel" align="center"><%# DataBinder.Eval(Container, "DataItem.w9") %></td>
										<td class="plainlabel" align="center"><%# DataBinder.Eval(Container, "DataItem.w8") %></td>
										<td class="plainlabel" align="center"><%# DataBinder.Eval(Container, "DataItem.w7") %></td>
										<td class="plainlabel" align="center"><%# DataBinder.Eval(Container, "DataItem.w6") %></td>
										<td class="plainlabel" align="center"><%# DataBinder.Eval(Container, "DataItem.w5") %></td>
										<td class="plainlabel" align="center"><%# DataBinder.Eval(Container, "DataItem.w4") %></td>
										<td class="plainlabel" align="center"><%# DataBinder.Eval(Container, "DataItem.w3") %></td>
										<td class="plainlabel" align="center"><%# DataBinder.Eval(Container, "DataItem.w2") %></td>
										<td class="plainlabel" align="center"><%# DataBinder.Eval(Container, "DataItem.w1") %></td>
										<td class="plainlabel" align="center"><%# DataBinder.Eval(Container, "DataItem.wscore") %></td>
									</tr>
								</ItemTemplate>
								<EditItemTemplate>
									<tr>
										<td><asp:imagebutton id="Imagebutton2" runat="server" ToolTip="Save Changes" CommandName="Update" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:imagebutton>
											<asp:imagebutton id="Imagebutton3" runat="server" ToolTip="Cancel Changes" CommandName="Cancel" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"></asp:imagebutton></td>
										<td class="plainlabelblue"><%# DataBinder.Eval(Container, "DataItem.bench_id") %>
										<td class="plainlabelblue" colspan="2"><%# DataBinder.Eval(Container, "DataItem.bench_title") %>
										<td id="te15" runat="server" class="plainlabel" bgcolor="#E7F1FD"><asp:dropdownlist id="dd15" CssClass="plainlabel" runat="server" Width="35px" SelectedIndex='<%# GetSelIndex(Container.DataItem("w15")) %>'>
												<asp:ListItem Value="0">0</asp:ListItem>
												<asp:ListItem Value="1">1</asp:ListItem>
												<asp:ListItem Value="2">2</asp:ListItem>
												<asp:ListItem Value="3">3</asp:ListItem>
												<asp:ListItem Value="4">4</asp:ListItem>
												<asp:ListItem Value="5">5</asp:ListItem>
											</asp:dropdownlist></td>
										<td id="te14" runat="server" class="plainlabel" bgcolor="#E7F1FD"><asp:dropdownlist id="dd14" CssClass="plainlabel" runat="server" Width="35px" SelectedIndex='<%# GetSelIndex(Container.DataItem("w14")) %>'>
												<asp:ListItem Value="0">0</asp:ListItem>
												<asp:ListItem Value="1">1</asp:ListItem>
												<asp:ListItem Value="2">2</asp:ListItem>
												<asp:ListItem Value="3">3</asp:ListItem>
												<asp:ListItem Value="4">4</asp:ListItem>
												<asp:ListItem Value="5">5</asp:ListItem>
											</asp:dropdownlist></td>
										<td id="te13" runat="server" class="plainlabel" bgcolor="#E7F1FD"><asp:dropdownlist id="dd13" CssClass="plainlabel" runat="server" Width="35px" SelectedIndex='<%# GetSelIndex(Container.DataItem("w13")) %>'>
												<asp:ListItem Value="0">0</asp:ListItem>
												<asp:ListItem Value="1">1</asp:ListItem>
												<asp:ListItem Value="2">2</asp:ListItem>
												<asp:ListItem Value="3">3</asp:ListItem>
												<asp:ListItem Value="4">4</asp:ListItem>
												<asp:ListItem Value="5">5</asp:ListItem>
											</asp:dropdownlist></td>
										<td id="te12" runat="server" class="plainlabel" bgcolor="#E7F1FD"><asp:dropdownlist id="dd12" CssClass="plainlabel" runat="server" Width="35px" SelectedIndex='<%# GetSelIndex(Container.DataItem("w12")) %>'>
												<asp:ListItem Value="0">0</asp:ListItem>
												<asp:ListItem Value="1">1</asp:ListItem>
												<asp:ListItem Value="2">2</asp:ListItem>
												<asp:ListItem Value="3">3</asp:ListItem>
												<asp:ListItem Value="4">4</asp:ListItem>
												<asp:ListItem Value="5">5</asp:ListItem>
											</asp:dropdownlist></td>
										<td id="te11" runat="server" class="plainlabel" bgcolor="#E7F1FD"><asp:dropdownlist id="dd11" CssClass="plainlabel" runat="server" Width="35px" SelectedIndex='<%# GetSelIndex(Container.DataItem("w11")) %>'>
												<asp:ListItem Value="0">0</asp:ListItem>
												<asp:ListItem Value="1">1</asp:ListItem>
												<asp:ListItem Value="2">2</asp:ListItem>
												<asp:ListItem Value="3">3</asp:ListItem>
												<asp:ListItem Value="4">4</asp:ListItem>
												<asp:ListItem Value="5">5</asp:ListItem>
											</asp:dropdownlist></td>
										<td class="plainlabel" bgcolor="#E7F1FD"><asp:dropdownlist id="dd10" CssClass="plainlabel" runat="server" Width="35px" SelectedIndex='<%# GetSelIndex(Container.DataItem("w10")) %>'>
												<asp:ListItem Value="0">0</asp:ListItem>
												<asp:ListItem Value="1">1</asp:ListItem>
												<asp:ListItem Value="2">2</asp:ListItem>
												<asp:ListItem Value="3">3</asp:ListItem>
												<asp:ListItem Value="4">4</asp:ListItem>
												<asp:ListItem Value="5">5</asp:ListItem>
											</asp:dropdownlist></td>
										<td class="plainlabel" bgcolor="#E7F1FD"><asp:dropdownlist id="dd9" CssClass="plainlabel" runat="server" Width="35px" SelectedIndex='<%# GetSelIndex(Container.DataItem("w9")) %>'>
												<asp:ListItem Value="0">0</asp:ListItem>
												<asp:ListItem Value="1">1</asp:ListItem>
												<asp:ListItem Value="2">2</asp:ListItem>
												<asp:ListItem Value="3">3</asp:ListItem>
												<asp:ListItem Value="4">4</asp:ListItem>
												<asp:ListItem Value="5">5</asp:ListItem>
											</asp:dropdownlist></td>
										<td class="plainlabel" bgcolor="#E7F1FD"><asp:dropdownlist id="dd8" CssClass="plainlabel" runat="server" Width="35px" SelectedIndex='<%# GetSelIndex(Container.DataItem("w8")) %>'>
												<asp:ListItem Value="0">0</asp:ListItem>
												<asp:ListItem Value="1">1</asp:ListItem>
												<asp:ListItem Value="2">2</asp:ListItem>
												<asp:ListItem Value="3">3</asp:ListItem>
												<asp:ListItem Value="4">4</asp:ListItem>
												<asp:ListItem Value="5">5</asp:ListItem>
											</asp:dropdownlist></td>
										<td class="plainlabel" bgcolor="#E7F1FD"><asp:dropdownlist id="dd7" CssClass="plainlabel" runat="server" Width="35px" SelectedIndex='<%# GetSelIndex(Container.DataItem("w7")) %>'>
												<asp:ListItem Value="0">0</asp:ListItem>
												<asp:ListItem Value="1">1</asp:ListItem>
												<asp:ListItem Value="2">2</asp:ListItem>
												<asp:ListItem Value="3">3</asp:ListItem>
												<asp:ListItem Value="4">4</asp:ListItem>
												<asp:ListItem Value="5">5</asp:ListItem>
											</asp:dropdownlist></td>
										<td class="plainlabel" bgcolor="#E7F1FD"><asp:dropdownlist id="dd6" CssClass="plainlabel" runat="server" Width="35px" SelectedIndex='<%# GetSelIndex(Container.DataItem("w6")) %>'>
												<asp:ListItem Value="0">0</asp:ListItem>
												<asp:ListItem Value="1">1</asp:ListItem>
												<asp:ListItem Value="2">2</asp:ListItem>
												<asp:ListItem Value="3">3</asp:ListItem>
												<asp:ListItem Value="4">4</asp:ListItem>
												<asp:ListItem Value="5">5</asp:ListItem>
											</asp:dropdownlist></td>
										<td class="plainlabel" bgcolor="#E7F1FD"><asp:dropdownlist id="dd5" CssClass="plainlabel" runat="server" Width="35px" SelectedIndex='<%# GetSelIndex(Container.DataItem("w5")) %>'>
												<asp:ListItem Value="0">0</asp:ListItem>
												<asp:ListItem Value="1">1</asp:ListItem>
												<asp:ListItem Value="2">2</asp:ListItem>
												<asp:ListItem Value="3">3</asp:ListItem>
												<asp:ListItem Value="4">4</asp:ListItem>
												<asp:ListItem Value="5">5</asp:ListItem>
											</asp:dropdownlist></td>
										<td class="plainlabel" bgcolor="#E7F1FD"><asp:dropdownlist id="dd4" CssClass="plainlabel" runat="server" Width="35px" SelectedIndex='<%# GetSelIndex(Container.DataItem("w4")) %>'>
												<asp:ListItem Value="0">0</asp:ListItem>
												<asp:ListItem Value="1">1</asp:ListItem>
												<asp:ListItem Value="2">2</asp:ListItem>
												<asp:ListItem Value="3">3</asp:ListItem>
												<asp:ListItem Value="4">4</asp:ListItem>
												<asp:ListItem Value="5">5</asp:ListItem>
											</asp:dropdownlist></td>
										<td class="plainlabel" bgcolor="#E7F1FD"><asp:dropdownlist id="dd3" CssClass="plainlabel" runat="server" Width="35px" SelectedIndex='<%# GetSelIndex(Container.DataItem("w3")) %>'>
												<asp:ListItem Value="0">0</asp:ListItem>
												<asp:ListItem Value="1">1</asp:ListItem>
												<asp:ListItem Value="2">2</asp:ListItem>
												<asp:ListItem Value="3">3</asp:ListItem>
												<asp:ListItem Value="4">4</asp:ListItem>
												<asp:ListItem Value="5">5</asp:ListItem>
											</asp:dropdownlist></td>
										<td class="plainlabel" bgcolor="#E7F1FD"><asp:dropdownlist id="dd2" CssClass="plainlabel" runat="server" Width="35px" SelectedIndex='<%# GetSelIndex(Container.DataItem("w2")) %>'>
												<asp:ListItem Value="0">0</asp:ListItem>
												<asp:ListItem Value="1">1</asp:ListItem>
												<asp:ListItem Value="2">2</asp:ListItem>
												<asp:ListItem Value="3">3</asp:ListItem>
												<asp:ListItem Value="4">4</asp:ListItem>
												<asp:ListItem Value="5">5</asp:ListItem>
											</asp:dropdownlist></td>
										<td class="plainlabel" bgcolor="#E7F1FD"><asp:dropdownlist id="dd1" CssClass="plainlabel" runat="server" Width="35px" SelectedIndex='<%# GetSelIndex(Container.DataItem("w1")) %>'>
												<asp:ListItem Value="0">0</asp:ListItem>
												<asp:ListItem Value="1">1</asp:ListItem>
												<asp:ListItem Value="2">2</asp:ListItem>
												<asp:ListItem Value="3">3</asp:ListItem>
												<asp:ListItem Value="4">4</asp:ListItem>
												<asp:ListItem Value="5">5</asp:ListItem>
											</asp:dropdownlist></td>
										<td></td>
										<td class="details"><asp:Label id="lblxyid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.xyid") %>'>
											</asp:Label></td>
									</tr>
								</EditItemTemplate>
								<FooterTemplate>
			</table>
			</FooterTemplate> </asp:datalist></TD></TR></TBODY></TABLE><input id="lbluid" type="hidden" runat="server"><input type="hidden" id="lblatyp" runat="server" NAME="lblatyp">
			<input type="hidden" id="lbleditchk" runat="server" NAME="lbleditchk"> <input type="hidden" id="lblcnt" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
