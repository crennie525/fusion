

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class Assessment_Admin
    Inherits System.Web.UI.Page
	Protected WithEvents lang1629 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1628 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1627 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1626 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1625 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1624 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim ass As New Utilities
    Dim uid, benchid, cat As String
    Protected WithEvents txtnew As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddnew As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tdcurr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleditchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgdepts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbenchid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then

            uid = Request.QueryString("uid").ToString
            lbluid.Value = uid
            benchid = Request.QueryString("benchid").ToString
            lblbenchid.Value = benchid
            cat = Request.QueryString("cat").ToString
            tdcurr.InnerHtml = cat
            ass.Open()
            GetAssDetails()
            ass.Dispose()

        Else
            If Request.Form("lblsubmit") = "go" Then
                ass.Open()
                AddAss()
                GetAssDetails()
                ass.Dispose()
                lblsubmit.Value = ""

            End If
        End If
    End Sub
    Private Sub AddAss()
        uid = lbluid.Value
        benchid = lblbenchid.Value
        Dim q, w As String
        q = txtnew.Text
        q = Replace(q, "'", Chr(180), , , vbTextCompare)
        If Len(q) > 1000 Then
            Dim strMessage As String =  tmod.getmsg("cdstr624" , "Assessment_Admin.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        w = ddnew.SelectedValue
        sql = "usp_adduseradmin '" & uid & "','" & benchid & "','" & q & "','" & w & "'"
        ass.Update(sql)
    End Sub
    Private Sub GetAssDetails()
        uid = lbluid.Value
        benchid = lblbenchid.Value
        sql = "select count(*) from bench_marks where uid = '" & uid & "' and bench_id = '" & benchid & "' and addon = 1"
        Dim cnt As Integer = ass.Scalar(sql)
        lblcnt.Value = cnt
        'If cnt = 0 Then
        dgdepts.Columns(8).Visible = False
        'End If
        sql = "usp_checkuseradmin '" & uid & "','" & benchid & "'"
        dr = ass.GetRdrData(sql)
        dgdepts.DataSource = dr
        dgdepts.DataBind()
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function

    Private Sub dgdepts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdepts.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                         e.Item.ItemType <> ListItemType.Footer Then

            If e.Item.ItemType = ListItemType.EditItem Then
                Dim qbox As TextBox = CType(e.Item.FindControl("txtq"), TextBox)
                Dim aflg As String = DataBinder.Eval(e.Item.DataItem, "addon").ToString
                If aflg = "0" Then
                    qbox.ReadOnly = True

                End If
            End If

            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim ibfm1 As ImageButton
                Dim aflg As String = DataBinder.Eval(e.Item.DataItem, "addon").ToString
                ibfm1 = CType(e.Item.FindControl("imgdel"), ImageButton)

                If aflg = "0" Then
                    ibfm1.Attributes.Add("class", "details")
                Else
                    ibfm1.Attributes("onclick") = "javascript:return " & _
                                       "confirm('Are you sure you want to delete this Prime Measure?')"
                End If


            End If
        End If
    End Sub

    Private Sub dgdepts_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.EditCommand
        lbleditchk.Value = "1"
        dgdepts.EditItemIndex = e.Item.ItemIndex
        ass.Open()
        GetAssDetails()
        ass.Dispose()
    End Sub

    Private Sub dgdepts_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.CancelCommand
        lbleditchk.Value = "1"
        dgdepts.EditItemIndex = -1
        ass.Open()
        GetAssDetails()
        ass.Dispose()
    End Sub

    Private Sub dgdepts_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.UpdateCommand
        lbleditchk.Value = "1"
        Dim qbox As TextBox = CType(e.Item.FindControl("txtq"), TextBox)
        Dim aflg As String = CType(e.Item.FindControl("lbladdon"), Label).Text
        Dim newq As String = ""
        newq = qbox.Text
        newq = Replace(newq, "'", Chr(180), , , vbTextCompare)
        If Len(newq) > 1000 Then
            Dim strMessage As String =  tmod.getmsg("cdstr625" , "Assessment_Admin.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim wbox As DropDownList = CType(e.Item.FindControl("ddscore"), DropDownList)
        Dim neww As String = ""
        neww = wbox.SelectedValue
        uid = lbluid.Value
        Dim qid As String = ""
        qid = CType(e.Item.FindControl("lblqid"), Label).Text
        Dim bwid As String = ""
        bwid = CType(e.Item.FindControl("lblbwid"), Label).Text
        Dim bid As String = ""
        bid = CType(e.Item.FindControl("lblbid"), Label).Text
        sql = "usp_updateuseradmin '" & qid & "','" & newq & "','" & neww & "','" & uid & "','" & aflg & "','" & bwid & "','" & bid & "'"
        ass.Open()
        ass.Update(sql)
        dgdepts.EditItemIndex = -1
        GetAssDetails()
        ass.Dispose()
    End Sub

    Private Sub dgdepts_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.DeleteCommand

    End Sub
	

	

	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgdepts.Columns(0).HeaderText = dlabs.GetDGPage("Assessment_Admin.aspx","dgdepts","0")
		Catch ex As Exception
		End Try
		Try
			dgdepts.Columns(1).HeaderText = dlabs.GetDGPage("Assessment_Admin.aspx","dgdepts","1")
		Catch ex As Exception
		End Try
		Try
			dgdepts.Columns(2).HeaderText = dlabs.GetDGPage("Assessment_Admin.aspx","dgdepts","2")
		Catch ex As Exception
		End Try
		Try
			dgdepts.Columns(3).HeaderText = dlabs.GetDGPage("Assessment_Admin.aspx","dgdepts","3")
		Catch ex As Exception
		End Try
		Try
			dgdepts.Columns(8).HeaderText = dlabs.GetDGPage("Assessment_Admin.aspx","dgdepts","8")
		Catch ex As Exception
		End Try

	End Sub

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang1624.Text = axlabs.GetASPXPage("Assessment_Admin.aspx","lang1624")
		Catch ex As Exception
		End Try
		Try
			lang1625.Text = axlabs.GetASPXPage("Assessment_Admin.aspx","lang1625")
		Catch ex As Exception
		End Try
		Try
			lang1626.Text = axlabs.GetASPXPage("Assessment_Admin.aspx","lang1626")
		Catch ex As Exception
		End Try
		Try
			lang1627.Text = axlabs.GetASPXPage("Assessment_Admin.aspx","lang1627")
		Catch ex As Exception
		End Try
		Try
			lang1628.Text = axlabs.GetASPXPage("Assessment_Admin.aspx","lang1628")
		Catch ex As Exception
		End Try
		Try
			lang1629.Text = axlabs.GetASPXPage("Assessment_Admin.aspx","lang1629")
		Catch ex As Exception
		End Try

	End Sub

End Class
