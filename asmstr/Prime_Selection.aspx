<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Prime_Selection.aspx.vb" Inherits="lucy_r12.Prime_Selection" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Prime_Selection</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/Prime_Selectionaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="gettop();"  class="tbg">
		<form id="form1" method="post" runat="server">
			<table>
				<tr id="trreg">
					<td>
						<table style="BORDER-BOTTOM: black 1px solid">
							<tr>
								<td class="thdr label" id="tdpm" onclick="getmain();" width="130" height="22" runat="server"><asp:Label id="lang1650" runat="server">Data Master</asp:Label></td>
								<td class="thdr label" id="tdtpm" onclick="getfind();" width="130" runat="server"><asp:Label id="lang1651" runat="server">Benchmark Findings</asp:Label></td>
								<td class="thdrhov label" id="tdp" width="130" runat="server"><asp:Label id="lang1652" runat="server">Prime Selection</asp:Label></td>
								<td class="thdr label" id="tdwo" onclick="getprime();" width="130" runat="server"><asp:Label id="lang1653" runat="server">X-Y Matrix</asp:Label></td>
								<td id="tdx" width="520" runat="server">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="bigblue"><asp:Label id="lang1654" runat="server">Reliability Prime Measures Selection</asp:Label></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td class="plainlabelred"><asp:Label id="lang1655" runat="server">Please Note that reordering Prime Measures will not transfer existing weight values assigned in the X-Y Matrix</asp:Label></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td class="plainlabelblue"><asp:Label id="lang1656" runat="server">Fusion Supports up to 5 extra Prime Measures</asp:Label></td>
				</tr>
				<tr id="trnew" runat="server">
					<td>
						<table>
							<tr>
								<td class="label" width="510" height="20"><asp:Label id="lang1657" runat="server">New Prime Measure</asp:Label></td>
								<td class="label" align="center" width="50"><asp:Label id="lang1658" runat="server">Add</asp:Label></td>
							</tr>
							<tr>
								<td><asp:textbox id="txtnew" runat="server" Width="500px"></asp:textbox></td>
								<td vAlign="middle" align="center"><IMG onclick="checknew();" alt="" src="addnewbg1.gif">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:datagrid id="dgdepts" runat="server" CellSpacing="1" AutoGenerateColumns="False" AllowCustomPaging="False"
							GridLines="None" CellPadding="0">
							<FooterStyle BackColor="White"></FooterStyle>
							<EditItemStyle Height="15px"></EditItemStyle>
							<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
							<ItemStyle CssClass="plainlabel" BackColor="ControlLightLight"></ItemStyle>
							<HeaderStyle Height="20px" Width="50px" CssClass="btmmenu label_b"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="50px" CssClass="btmmenu label"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Order">
									<HeaderStyle Width="50px" CssClass="btmmenu label"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblold runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.prime_order") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id=txtorder runat="server" Width="50px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.prime_order") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Prime Measure">
									<HeaderStyle Width="500px" CssClass="btmmenu label"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Width="500px" Text='<%# DataBinder.Eval(Container, "DataItem.prime_measure") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txtq" runat="server" Width="500px" Text='<%# DataBinder.Eval(Container, "DataItem.prime_measure") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<HeaderStyle Width="20px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblpid runat="server" Width="0px" Text='<%# DataBinder.Eval(Container, "DataItem.prime_id") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblbid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.prime_id") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<HeaderStyle Width="20px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label6" runat="server" Width="20px" Text='<%# DataBinder.Eval(Container, "DataItem.addon") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lbladdon" runat="server" Width="20px" Text='<%# DataBinder.Eval(Container, "DataItem.addon") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Remove">
									<HeaderStyle Width="64px" CssClass="btmmenu label"></HeaderStyle>
									<ItemTemplate>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:ImageButton id="imgdel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
								ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
						</asp:datagrid></td>
				</tr>
			</table>
			<input id="lbluid" type="hidden" name="lbluid" runat="server"> <input id="lblbenchid" type="hidden" name="lblbenchid" runat="server">
			<input id="lblsubmit" type="hidden" name="lblsubmit" runat="server"> <input id="lblorder" type="hidden" runat="server">
			<input id="lblcnt" type="hidden" runat="server"><input type="hidden" id="lbleditchk" runat="server" NAME="lbleditchk">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
