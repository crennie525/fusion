

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text

Public Class Assessment_Master
    Inherits System.Web.UI.Page
	Protected WithEvents lang1636 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1635 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1634 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1633 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents tdass As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbenchid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim ass As New Utilities
    Protected WithEvents lblatyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim uid, atyp As String
    Protected WithEvents tdpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdx As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdp As System.Web.UI.HtmlControls.HtmlTableCell

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            atyp = Request.QueryString("atyp").ToString
            lblatyp.Value = atyp
            If atyp <> "a" Then
                tdp.Attributes.Add("class", "details")
                tdx.Attributes.Add("width", "650px")
            End If
            uid = HttpContext.Current.Session("dfltps").ToString '"PM Admin"
            lbluid.Value = uid
            ass.Open()
            CheckAssess()
            GetAssMaster()
            ass.Dispose()

        End If
    End Sub
    Private Sub CheckAssess()
        uid = lbluid.Value
        sql = "usp_checkuserstart '" & uid & "'"
        Dim ucnt As Integer
        ucnt = ass.Scalar(sql)
        If ucnt = 0 Then
            ucnt = ucnt

        End If
    End Sub
    Private Sub GetAssMaster()
        Dim sb As New StringBuilder
        'sb.Append("<html><head>")
        'sb.Append("<LINK href=""../styles/pmcss1.css"" type=""text/css"" rel=""stylesheet"" > ")
        'sb.Append("</head><body>")
        uid = lbluid.Value

        sb.Append("<table cellspacing=""3"" cellpadding=""3"" width=""1040"" border=""0"">" & vbCrLf)
        sb.Append("<tr><td class=""bigblue"" colspan=""3"">" & tmod.getlbl("cdlbl337" , "Assessment_Master.aspx.vb") & "</td></tr>" & vbCrLf)
        'sb.Append("<tr><td class=""plainlabel12"" colspan=""7"">&nbsp;</td></tr>" & vbCrLf)

        sb.Append("<tr><td width=""15""></td>" & vbCrLf)
        sb.Append("<td width=""15""></td>" & vbCrLf)
        sb.Append("<td width=""740""></td>" & vbCrLf)

        sb.Append("<td width=""52""></td>" & vbCrLf)
        sb.Append("<td width=""52""></td>" & vbCrLf)
        sb.Append("<td width=""52""></td>" & vbCrLf)
        sb.Append("<td width=""52""></td>" & vbCrLf)
        sb.Append("<td width=""52""></td></tr>" & vbCrLf)

        atyp = lblatyp.Value
        If atyp = "a" Then
            sb.Append("<tr><td class=""plainlabelblue"" colspan=""3"">" & tmod.getlbl("cdlbl338" , "Assessment_Master.aspx.vb") & "</td></tr>" & vbCrLf)
        Else
            sb.Append("<tr><td class=""plainlabelblue"" colspan=""3"">" & tmod.getlbl("cdlbl339" , "Assessment_Master.aspx.vb") & "</td></tr>" & vbCrLf)
        End If


        sb.Append("<tr><td class=""label"" colspan=""3"" bgcolor=""yellow"">" & tmod.getlbl("cdlbl340" , "Assessment_Master.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""label"" align=""center"" bgcolor=""yellow"">" & tmod.getlbl("cdlbl341" , "Assessment_Master.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""label"" align=""center"" bgcolor=""yellow"" >" & tmod.getlbl("cdlbl342" , "Assessment_Master.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""label"" align=""center"" bgcolor=""yellow"">" & tmod.getlbl("cdlbl343" , "Assessment_Master.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""label"" align=""center"" bgcolor=""yellow"">" & tmod.getlbl("cdlbl344" , "Assessment_Master.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""label"" align=""center"" bgcolor=""yellow"">%</td></tr>" & vbCrLf)
        Dim num, cat, cathold As String
        Dim bnum, bq, wt As String
        Dim score, best, current, percent, wper, wa, pertot, wscore As String
        best = "0"
        current = "0"
        Dim cnt As Integer = 0
        cathold = "0"
        sql = "select q.*, c.bench_title, c.weight_avg, m.qscore, m.qcurrent, m.qpercent, m.qscore_avg, m.pertot, m.wscore_avg from bench_questions q " _
        + "left join bench_cats c on c.bench_id = q.bench_id " _
        + "left join bench_marks m on m.qid = q.qid and m.uid = '" & uid & "' " _
        + "order by c.bench_order, q.bench_num"

        sql = "select c.bench_title, q.bench_id, q.bench_num, q.bench_question, m.qscore, m.qcurrent, m.qpercent, m.qscore_avg, m.pertot, m.wscore_avg, " _
        + " w.weight, w.best, w.weight_avg " _
        + "from bench_questions q " _
        + "left join bench_cats c on c.bench_id = q.bench_id " _
        + "left join bench_marks m on m.qid = q.qid and m.uid = '" & uid & "' " _
        + "left join bench_weights w on w.qid = q.qid and w.uid = '" & uid & "' " _
        + "where q.uid = '" & uid & "' or q.uid is null " _
        + "order by c.bench_order, q.bench_num"
        dr = ass.GetRdrData(sql)
        While dr.Read
            num = dr.Item("bench_id").ToString
            cat = dr.Item("bench_title").ToString
            percent = dr.Item("qscore_avg").ToString

            If cat <> cathold Then
                cnt += 1
                If cnt > 1 Then
                    sb.Append("<tr><td></td>" & vbCrLf)
                    sb.Append("<td colspan=""2"" class=""label"" align=""right"">" & tmod.getlbl("cdlbl346" , "Assessment_Master.aspx.vb") & "</td>" & vbCrLf)
                    sb.Append("<td class=""label"" align=""center"" bgcolor=""#E2E1DF"">" & wa & "</td>" & vbCrLf)
                    sb.Append("<td class=""label"" align=""center"" bgcolor=""#E2E1DF"">" & wscore & "</td>" & vbCrLf)
                    sb.Append("<td class=""label"" align=""center"" bgcolor=""#E2E1DF"">&nbsp;</td>" & vbCrLf)
                    sb.Append("<td class=""label"" align=""center"" bgcolor=""#E2E1DF"">&nbsp;</td>" & vbCrLf)
                    sb.Append("<td class=""label"" align=""center"" bgcolor=""#E2E1DF"">" & pertot & "%</td></tr>" & vbCrLf)
                    sb.Append("<tr><td>&nbsp;</td></tr>" & vbCrLf)

                End If
                cathold = cat
                wa = dr.Item("weight_avg").ToString

                sb.Append("<tr><td class=""bluelabel"" colspan=""1"" align=""right"">" & num & "</td>" & vbCrLf)
                sb.Append("<td colspan=""2""><a href=""#"" class=""AT"" onclick=""getas('" & num & "','" & uid & "','" & cat & "','" & wa & "');"">" & cat & "</a></td>" & vbCrLf)
                sb.Append("<td>&nbsp;</td>" & vbCrLf)
                sb.Append("<td>&nbsp;</td>" & vbCrLf)
                sb.Append("<td>&nbsp;</td>" & vbCrLf)
                sb.Append("<td>&nbsp;</td>" & vbCrLf)
                sb.Append("<td class=""label"" align=""center"" bgcolor=""yellow"">" & percent & "%</td></tr>" & vbCrLf)

            End If
            wa = dr.Item("weight_avg").ToString
            wscore = dr.Item("wscore_avg").ToString
            pertot = dr.Item("pertot").ToString
            wa = dr.Item("weight_avg").ToString
            bnum = dr.Item("bench_num").ToString
            bq = dr.Item("bench_question").ToString
            wt = dr.Item("weight").ToString
            best = dr.Item("best").ToString
            score = dr.Item("qscore").ToString
            current = dr.Item("qcurrent").ToString
            wper = dr.Item("qpercent").ToString
            sb.Append("<tr><td></td><td class=""plainlabel"" colspan=""1"" align=""right"" valign=""top"">" & bnum & "</td>" & vbCrLf)
            sb.Append("<td class=""plainlabel"" colspan=""1"">" & bq & "</td>" & vbCrLf)
            sb.Append("<td class=""label"" colspan=""1"" align=""center"" valign=""top"" bgcolor=""#F7A5F6"">" & wt & "</td>" & vbCrLf)
            sb.Append("<td class=""orangelabel"" colspan=""1"" align=""center"" valign=""top"" >" & score & "</td>" & vbCrLf)
            sb.Append("<td class=""orangelabel"" colspan=""1"" align=""center"" valign=""top"" bgcolor=""#E2E1DF"">" & best & "</td>" & vbCrLf)
            sb.Append("<td class=""orangelabel"" colspan=""1"" align=""center"" valign=""top"" bgcolor=""#E2E1DF"">" & current & "</td>" & vbCrLf)
            sb.Append("<td class=""orangelabel"" colspan=""1"" align=""center"" valign=""top"" bgcolor=""#E2E1DF"">" & wper & "%</td></tr>" & vbCrLf)
        End While
        dr.Close()
        sb.Append("</table>" & vbCrLf)
        'sb.Append("</body></html>")
        'Response.Write(sb.ToString)
        tdass.InnerHtml = sb.ToString

    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang1633.Text = axlabs.GetASPXPage("Assessment_Master.aspx","lang1633")
		Catch ex As Exception
		End Try
		Try
			lang1634.Text = axlabs.GetASPXPage("Assessment_Master.aspx","lang1634")
		Catch ex As Exception
		End Try
		Try
			lang1635.Text = axlabs.GetASPXPage("Assessment_Master.aspx","lang1635")
		Catch ex As Exception
		End Try
		Try
			lang1636.Text = axlabs.GetASPXPage("Assessment_Master.aspx","lang1636")
		Catch ex As Exception
		End Try

	End Sub

End Class
