

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class Prime_Selection
    Inherits System.Web.UI.Page
	Protected WithEvents lang1658 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1657 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1656 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1655 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1654 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1653 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1652 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1651 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1650 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim ass As New Utilities
    Dim uid, benchid, cat As String
    Protected WithEvents lblcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdx As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trnew As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lbleditchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblorder As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtnew As System.Web.UI.WebControls.TextBox
    Protected WithEvents dgdepts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbenchid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            uid = Request.QueryString("uid").ToString
            lbluid.Value = uid
            ass.Open()
            GetPrime()
            ass.Dispose()
        Else
            If Request.Form("lblsubmit") = "go" Then
                ass.Open()
                AddPrime()
                GetPrime()
                ass.Dispose()
                lblsubmit.Value = ""

            End If
        End If
    End Sub
    Private Sub AddPrime()
        uid = lbluid.Value
        Dim q As String
        q = txtnew.Text
        q = Replace(q, "'", Chr(180), , , vbTextCompare)
        If Len(q) > 500 Then
            Dim strMessage As String =  tmod.getmsg("cdstr628" , "Prime_Selection.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        sql = "usp_addnewprime '" & uid & "','" & q & "'"
        ass.Update(sql)
    End Sub
    Private Sub GetPrime()
        uid = lbluid.Value
        sql = "select count(*) from bench_prime where uid = '" & uid & "'"
        Dim cnt As Integer = ass.Scalar(sql)
        lblcnt.Value = cnt
        'If cnt < 11 Then
        dgdepts.Columns(5).Visible = False
        'End If
        If cnt > 15 Then
            trnew.Attributes.Add("class", "details")
        End If
        sql = "select * from bench_prime where uid = '" & uid & "' order by prime_order"
        dr = ass.GetRdrData(sql)
        dgdepts.DataSource = dr
        dgdepts.DataBind()
    End Sub

    Private Sub dgdepts_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.EditCommand
        lbleditchk.Value = "1"
        lblorder.Value = CType(e.Item.FindControl("lblold"), Label).Text
        dgdepts.EditItemIndex = e.Item.ItemIndex
        ass.Open()
        GetPrime()
        ass.Dispose()
    End Sub

    Private Sub dgdepts_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.CancelCommand
        lbleditchk.Value = "1"
        dgdepts.EditItemIndex = -1
        ass.Open()
        GetPrime()
        ass.Dispose()
    End Sub

    Private Sub dgdepts_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.UpdateCommand
        lbleditchk.Value = "1"
        uid = lbluid.Value
        Dim onum As String = lblorder.Value
        Dim nnum As String = CType(e.Item.FindControl("txtorder"), TextBox).Text
        Dim ocnt As Integer = lblcnt.Value
        Dim rtechk As Long
        Try
            rtechk = System.Convert.ToInt32(nnum)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr629" , "Prime_Selection.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        ass.Open()
        If onum <> nnum Then
            If rtechk > ocnt Then
                nnum = ocnt
            End If
            sql = "usp_rerouteprime '" & uid & "','" & nnum & "','" & onum & "'"
            ass.Update(sql)
        End If
        Dim qbox As TextBox = CType(e.Item.FindControl("txtq"), TextBox)
        Dim q As String
        q = qbox.Text
        q = Replace(q, "'", Chr(180), , , vbTextCompare)
        If Len(q) > 500 Then
            Dim strMessage As String =  tmod.getmsg("cdstr630" , "Prime_Selection.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim pid As String = CType(e.Item.FindControl("lblbid"), Label).Text
        sql = "update bench_prime set prime_measure = '" & q & "' where prime_id = '" & pid & "'"
        ass.Update(sql)

        dgdepts.EditItemIndex = -1

        GetPrime()
        ass.Dispose()
    End Sub

    Private Sub dgdepts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdepts.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                        e.Item.ItemType <> ListItemType.Footer Then

            If e.Item.ItemType = ListItemType.EditItem Then
                Dim qbox As TextBox = CType(e.Item.FindControl("txtq"), TextBox)
                Dim aflg As String = DataBinder.Eval(e.Item.DataItem, "addon").ToString
                If aflg = "0" Then
                    qbox.ReadOnly = True
                End If
            End If
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim ibfm1 As ImageButton
                Dim aflg As String = DataBinder.Eval(e.Item.DataItem, "addon").ToString
                ibfm1 = CType(e.Item.FindControl("imgdel"), ImageButton)
                Dim ocnt As Integer = lblcnt.Value
                
                If aflg = "0" Then
                    ibfm1.Attributes.Add("class", "details")
                Else
                    ibfm1.Attributes("onclick") = "javascript:return " & _
                                       "confirm('Are you sure you want to delete this Prime Measure?')"
                End If


            End If
        End If
    End Sub

    Private Sub dgdepts_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.DeleteCommand
        Dim pid As String = CType(e.Item.FindControl("lblpid"), Label).Text

    End Sub
	

	

	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgdepts.Columns(0).HeaderText = dlabs.GetDGPage("Prime_Selection.aspx","dgdepts","0")
		Catch ex As Exception
		End Try
		Try
			dgdepts.Columns(1).HeaderText = dlabs.GetDGPage("Prime_Selection.aspx","dgdepts","1")
		Catch ex As Exception
		End Try
		Try
			dgdepts.Columns(2).HeaderText = dlabs.GetDGPage("Prime_Selection.aspx","dgdepts","2")
		Catch ex As Exception
		End Try
		Try
			dgdepts.Columns(5).HeaderText = dlabs.GetDGPage("Prime_Selection.aspx","dgdepts","5")
		Catch ex As Exception
		End Try

	End Sub

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang1650.Text = axlabs.GetASPXPage("Prime_Selection.aspx","lang1650")
		Catch ex As Exception
		End Try
		Try
			lang1651.Text = axlabs.GetASPXPage("Prime_Selection.aspx","lang1651")
		Catch ex As Exception
		End Try
		Try
			lang1652.Text = axlabs.GetASPXPage("Prime_Selection.aspx","lang1652")
		Catch ex As Exception
		End Try
		Try
			lang1653.Text = axlabs.GetASPXPage("Prime_Selection.aspx","lang1653")
		Catch ex As Exception
		End Try
		Try
			lang1654.Text = axlabs.GetASPXPage("Prime_Selection.aspx","lang1654")
		Catch ex As Exception
		End Try
		Try
			lang1655.Text = axlabs.GetASPXPage("Prime_Selection.aspx","lang1655")
		Catch ex As Exception
		End Try
		Try
			lang1656.Text = axlabs.GetASPXPage("Prime_Selection.aspx","lang1656")
		Catch ex As Exception
		End Try
		Try
			lang1657.Text = axlabs.GetASPXPage("Prime_Selection.aspx","lang1657")
		Catch ex As Exception
		End Try
		Try
			lang1658.Text = axlabs.GetASPXPage("Prime_Selection.aspx","lang1658")
		Catch ex As Exception
		End Try

	End Sub

End Class
