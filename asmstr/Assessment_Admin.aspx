<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Assessment_Admin.aspx.vb" Inherits="lucy_r12.Assessment_Admin" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Assessment_Admin</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/Assessment_Adminaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="gettop();" class="tbg">
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="bigblue"><asp:Label id="lang1624" runat="server">Reliability Best-Practices Benchmark Administration</asp:Label></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<table>
							<tr>
								<td class="bluelabel"><asp:Label id="lang1625" runat="server">Current Category:</asp:Label></td>
								<td class="plainlabel" id="tdcurr" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><A class="AT" href="Assessment_Master.aspx?atyp=a"><asp:Label id="lang1626" runat="server">Return to Assessment Master</asp:Label></A></td>
				</tr>
				<tr>
				</tr>
			</table>
			<table>
				<tr>
					<td class="label" width="510" height="20"><asp:Label id="lang1627" runat="server">New Check Point</asp:Label></td>
					<td class="label" align="center" width="80"><asp:Label id="lang1628" runat="server">New Weight</asp:Label></td>
					<td class="label" width="50" align="center"><asp:Label id="lang1629" runat="server">Add</asp:Label></td>
				</tr>
				<tr>
					<td>
						<asp:TextBox id="txtnew" runat="server" Height="40px" Width="500px" TextMode="MultiLine"></asp:TextBox>
					</td>
					<td align="center" valign="middle">
						<asp:DropDownList id="ddnew" runat="server">
							<asp:ListItem Value="0">0</asp:ListItem>
							<asp:ListItem Value="1">1</asp:ListItem>
							<asp:ListItem Value="2">2</asp:ListItem>
							<asp:ListItem Value="3">3</asp:ListItem>
							<asp:ListItem Value="4">4</asp:ListItem>
							<asp:ListItem Value="5">5</asp:ListItem>
						</asp:DropDownList>
					</td>
					<td align="center" valign="middle"><IMG alt="" src="addnewbg1.gif" onclick="checknew();">
					</td>
				</tr>
			</table>
			<td>
			</td>
			</TR>
			<tr>
				<td><asp:datagrid id="dgdepts" runat="server" CellPadding="0" GridLines="None" AllowCustomPaging="False"
						AutoGenerateColumns="False" CellSpacing="1">
						<FooterStyle BackColor="White"></FooterStyle>
						<EditItemStyle Height="15px"></EditItemStyle>
						<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
						<ItemStyle CssClass="plainlabel" BackColor="ControlLightLight"></ItemStyle>
						<HeaderStyle Height="20px" Width="50px" CssClass="btmmenu label_b"></HeaderStyle>
						<Columns>
							<asp:TemplateColumn HeaderText="Edit">
								<HeaderStyle Width="50px" CssClass="btmmenu label"></HeaderStyle>
								<ItemTemplate>
									<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
										CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
										CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
									<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
										CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Check#">
								<HeaderStyle Width="50px" CssClass="btmmenu label"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id=Label4 runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.bench_num") %>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:Label id=txtname runat="server" Width="50px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.bench_num") %>'>
									</asp:Label>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Check Point">
								<HeaderStyle Width="500px" CssClass="btmmenu label"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id="Label2" runat="server" Width="500px" Text='<%# DataBinder.Eval(Container, "DataItem.bench_question") %>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:textbox id="txtq" runat="server" Width="500px" TextMode="MultiLine" Text='<%# DataBinder.Eval(Container, "DataItem.bench_question") %>'>
									</asp:textbox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Weight">
								<HeaderStyle Width="50px" CssClass="btmmenu label"></HeaderStyle>
								<ItemTemplate>
									&nbsp;
									<asp:Label id="Label3" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.weight") %>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									&nbsp;
									<asp:dropdownlist id="ddscore" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("weight")) %>'>
										<asp:ListItem Value="0">0</asp:ListItem>
										<asp:ListItem Value="1">1</asp:ListItem>
										<asp:ListItem Value="2">2</asp:ListItem>
										<asp:ListItem Value="3">3</asp:ListItem>
										<asp:ListItem Value="4">4</asp:ListItem>
										<asp:ListItem Value="5">5</asp:ListItem>
									</asp:dropdownlist>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn Visible="False">
								<HeaderStyle Width="20px"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id=Label1 runat="server" Width="0px" Text='<%# DataBinder.Eval(Container, "DataItem.bench_id") %>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:Label id="lblbid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.bench_id") %>'>
									</asp:Label>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn Visible="False">
								<HeaderStyle Width="20px"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id="Label5" runat="server" Width="20px" Text='<%# DataBinder.Eval(Container, "DataItem.qid") %>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:Label id="lblqid" runat="server" Width="20px" Text='<%# DataBinder.Eval(Container, "DataItem.qid") %>'>
									</asp:Label>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn Visible="False">
								<HeaderStyle Width="20px"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id="Label6" runat="server" Width="20px" Text='<%# DataBinder.Eval(Container, "DataItem.addon") %>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:Label id="lbladdon" runat="server" Width="20px" Text='<%# DataBinder.Eval(Container, "DataItem.addon") %>'>
									</asp:Label>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn Visible="False">
								<HeaderStyle Width="20px"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id="Label8" runat="server" Width="20px" Text='<%# DataBinder.Eval(Container, "DataItem.bwid") %>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:Label id="lblbwid" runat="server" Width="20px" Text='<%# DataBinder.Eval(Container, "DataItem.bwid") %>'>
									</asp:Label>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Remove">
								<HeaderStyle Width="64px" CssClass="btmmenu label"></HeaderStyle>
								<ItemTemplate>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<asp:ImageButton id="imgdel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton>
								</ItemTemplate>
							</asp:TemplateColumn>
						</Columns>
						<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
							ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
					</asp:datagrid></td>
			</tr>
			</TABLE> <input id="lbluid" type="hidden" name="lbluid" runat="server"> <input id="lblbenchid" type="hidden" name="lblbenchid" runat="server">
			<input type="hidden" id="lblsubmit" runat="server"> <input type="hidden" id="lblcnt" runat="server"><input type="hidden" id="lbleditchk" runat="server" NAME="lbleditchk">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
