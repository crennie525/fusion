﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class pmdivmax

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''lang591 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang591 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pmpdm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pmpdm As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''pmtitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pmtitle As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''pmlast control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pmlast As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''lang594 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang594 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lang595 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang595 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lang596 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang596 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txts control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txts As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lang597 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang597 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cbs control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbs As Global.System.Web.UI.HtmlControls.HtmlInputCheckBox

    '''<summary>
    '''tdskill control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdskill As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Img5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Img5 As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''lang599 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang599 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtn As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Img6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Img6 As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''tdcnt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdcnt As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''ddpt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddpt As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lang601 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang601 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtwonum control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtwonum As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''imgwoshed control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgwoshed As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''txtruns control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtruns As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lang598 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang598 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtsup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtsup As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''txtfreq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtfreq As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddunit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddunit As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lang600 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang600 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtlead control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtlead As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Label3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Label2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''divlg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divlg As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lbljp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbljp As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''tdjp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdjp As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''imgplans control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgplans As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''imgaddplan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgaddplan As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''imgeditplan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgeditplan As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''txteq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txteq As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''imgdel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgdel As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''imgsav control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgsav As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''Img1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Img1 As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''Img2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Img2 As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''imgmeas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgmeas As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''btnedittask control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnedittask As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''Img3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Img3 As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''imgi2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgi2 As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''Img4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Img4 As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''imgcomp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgcomp As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''lang604 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang604 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''tdtasks control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdtasks As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ifsave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ifsave As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblpmid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblpmid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsubmit As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblpmhid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblpmhid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbleqid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbleqid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblnext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblnext As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblschk control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblschk As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsup As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbllead control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbllead As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblskillid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblskillid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblacnt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblacnt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''txtsearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtsearch As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblwo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblwo As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbllog control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbllog As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblmalert control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblmalert As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblalert control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblalert As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbldocs control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbldocs As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblondate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblondate As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblro As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblpiccnt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblpiccnt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblusetdt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblusetdt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblusetotal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblusetotal As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblttime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblttime As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbldtime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbldtime As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblacttime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblacttime As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblactdtime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblactdtime As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblhalert control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblhalert As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbldalert control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbldalert As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblfslang control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblfslang As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblworuns control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblworuns As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblruns control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblruns As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblstart control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblstart As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblleadid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblleadid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsupid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsupid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblissched control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblissched As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblusesched control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblusesched As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblhidejts control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblhidejts As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblpmnum control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblpmnum As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbljpid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbljpid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbljpnum control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbljpnum As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblpmdesc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblpmdesc As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblskill control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblskill As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblptid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblptid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblpretech control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblpretech As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbleqnum control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbleqnum As Global.System.Web.UI.HtmlControls.HtmlInputHidden
End Class
