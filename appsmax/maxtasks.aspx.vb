﻿Imports System.Data.SqlClient

Public Class maxtasks
    Inherits System.Web.UI.Page
    Dim tsks As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim eqid As Integer
    Dim who As String
    Dim pmid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            pmid = Request.QueryString("pmid").ToString
            who = Request.QueryString("who").ToString
            tsks.Open()
            If who = "dat" Then
                cleardat(eqid)
            ElseIf who = "pm" Then
                delpm(eqid)
            ElseIf who = "pmwo" Then
                canallwo(eqid)
                delpm(eqid)
            ElseIf who = "pms" Then
                delpmid(pmid)
            ElseIf who = "pmidwo" Then
                cansingwo(eqid, pmid)
                delpmid(pmid)
            End If
            tsks.Dispose()
            lblret.Value = "go"
        End If
    End Sub
    Private Sub delpmid(ByVal pmid As String)
        sql = "delete from pmmax where pmid = '" & pmid & "'"
        tsks.Update(sql)
    End Sub

    Private Sub cansingwo(ByVal eqid As String, ByVal pmid As String)
        Dim wonum, stat, typ As String
        sql = "select wonum from pmmax where pmid = '" & pmid & "'"
        wonum = tsks.strScalar(sql)
        sql = "select status, worktype from workorder where wonum = '" & wonum & "' and eqid = '" & eqid & "'"
        dr = tsks.GetRdrData(sql)
        While dr.Read
            stat = dr.Item("status").ToString
            typ = dr.Item("worktype").ToString
        End While
        dr.Close()
        If typ = "MAXPM" And (stat <> "COMP" And stat <> "CLOSE" And stat <> "CANCEL") Then
            sql = "update workorder set status = 'CANCEL' where wonum = '" & wonum & "' and eqid = '" & eqid & "'"
            tsks.Update(sql)
        End If
    End Sub
    Private Sub canallwo(ByVal eqid As String)
        Dim wonum, stat, typ As String
        sql = "select wonum from pmmax where eqid = '" & eqid & "'"
        Dim ds As New DataSet
        ds = tsks.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        Dim row As DataRow
        For Each row In dt.Rows
            wonum = row("wonum").ToString
            sql = "select status, worktype from workorder where wonum = '" & wonum & "' and eqid = '" & eqid & "'"
            dr = tsks.GetRdrData(sql)
            While dr.Read
                stat = dr.Item("status").ToString
                typ = dr.Item("worktype").ToString
            End While
            dr.Close()
            If typ = "MAXPM" And (stat <> "COMP" And stat <> "CLOSE" And stat <> "CANCEL") Then
                sql = "update workorder set status = 'CANCEL' where wonum = '" & wonum & "' and eqid = '" & eqid & "'"
                tsks.Update(sql)
            End If
        Next
    End Sub
    Private Sub cleardat(ByVal eqid As String)
        sql = "update pmmax set nextdate = null where eqid = '" & eqid & "'"
        tsks.Update(sql)
    End Sub
    Private Sub delpm(ByVal eqid As String)
        sql = "delete from pmmax where eqid = '" & eqid & "'"
        tsks.Update(sql)
    End Sub
End Class