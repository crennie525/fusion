﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmjpmanager.aspx.vb" Inherits="lucy_r12.pmjpmanager" %>

<%@ Register Src="../menu/mmenu1.ascx" TagName="mmenu1" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .FreezePaneOn6
        {
            position: absolute;
            top: 52px;
            left: 0px;
            visibility: visible;
            display: block;
            width: 630px;
            height: 480px;
            background-color: #eeeded;
            z-index: 999;
            filter: alpha(opacity=35);
            -moz-opacity: 0.35;
            padding-top: 3%;
        }
        
    </style>
    <script language="javascript" type="text/javascript">
        function handlepmdel() {
            document.getElementById("lblpmid").value = "";
            gettab("eq")
        }
        function handleswjplan(jpid) {
            setref();
            var href = document.getElementById("lblhref").value;

            var wo = document.getElementById("lblwo").value
            var ro = document.getElementById("lblro").value;
            href = href.replace(/&/g, "~")
            //var harr = href.split("~");
            //var remwo = harr[1] + "~";
            //href = href.replace(remwo, "")
            //alert(href)
            //alert(wo)
            //alert("from wolabmain wojpedit.aspx?jpid=" + jpid + "&wonum=" + wo + "&href=" + href)
            window.location = "maxjpeditmain.aspx?jpid=" + jpid + "&wonum=" + wo + "&href=" + href + "&ro=" + ro;
        }
        function handlejp() {
            setref();
            var won = document.getElementById("lblwo").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            //alert("../appsrt/jobplanmain2.aspx?start=wo&jid=0&wo=" + won + "&sid=" + sid + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr)
            window.location = "../appsrt/jobplanmain2.aspx?start=wo&jid=0&wo=" + won + "&sid=" + sid + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        var timer = window.setTimeout("doref();", 1205000);
        function doref() {
            //setref
            //document.getElementById("form1").submit();
        }
        function setref() {
            window.clearTimeout(timer);
            timer = window.setTimeout("doref();", 1205000);
        }
        /*function handlelogout() {
        document.getElementById("form1").submit();
        }
        function checkit() {
        window.setTimeout("setref();", 1205000);
        }
        function setref() {
        document.getElementById("form1").submit(); 
        }*/

        function checkup(eqid) {
            window.parent.setref();
            document.getElementById("lbleqid").value = eqid;
            document.getElementById("subdiv").className = "view";
            document.getElementById("subdiv").style.position = "absolute";
            document.getElementById("subdiv").style.top = "120px";
            document.getElementById("subdiv").style.left = "120px";
            document.getElementById("ifrev").src = "PMUpDate.aspx?eqid=" + eqid;
        }

        function closerev() {
            document.getElementById("subdiv").className = "details";
        }
        function handleexit(id) {
            if (id == "no") {
                closerev();
            }
            else {
                closerev();
                var eid = document.getElementById("lbleqid").value;
                var sid = document.getElementById("lblsid").value;
                document.getElementById("geteq").src = "pmgetpmmax.aspx?jump=yes&typ=no&eqid=" + eid + "&pmid=0&sid=" + sid;
            }
        }
        function handleproj(val, pmid, typ) {
            setref();
            var ro = document.getElementById("lblro").value;
            var sid = document.getElementById("lblsid").value;
            var ro = "0"; //document.getElementById("lblro").value;
            //var eqid = "0";
            var won = document.getElementById("lblwo").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            //alert("PMScheduling2.aspx?jump=yes&typ=pm&eqid=" + val + "&pmid=" + pmid + "&ro=" + ro + "&sid=" + sid + "&eqid=" + eqid + "&wo=" + won + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr)
            window.location = "PMScheduling2.aspx?jump=yes&typ=pm&eqid=" + val + "&pmid=" + pmid + "&ro=" + ro + "&sid=" + sid + "&wo=" + won + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
            //window.location = "PMScheduling2.aspx?jump=yes&typ=pm&eqid=" + val + "&pmid=" + pmid + "&date=" + Date() + "&ro=" + ro;
        }
        function handlejump(id, eid, piccnt, sid) {
            document.getElementById("lbljump").value = "eq";
            document.getElementById("lblpmid").value = id;
            document.getElementById("lbleqid").value = eid;
            checkarch();
            document.getElementById("geteq").src = "pmgetpmmax.aspx?jump=yes&typ=no&eqid=" + eid + "&pmid=" + id + "&piccnt=" + piccnt + "&sid=" + sid;
        }
        function updatepmid(pmid, pmhid, eqid, wo, jpid) {
            //alert(wo)
            document.getElementById("lblpmid").value = pmid;
            document.getElementById("lblpmhid").value = pmhid;
            document.getElementById("lbleqid").value = eqid;
            document.getElementById("lblwo").value = wo;
            document.getElementById("lbljpid").value = jpid;
            var sid = document.getElementById("lblsid").value;
            document.getElementById("lblhref").value = "pmjpmanager.aspx?jump=yes&typ=no&eqid=" + eqid + "&pmid=" + pmid + "&sid=" + sid
            //alert(document.getElementById("lblhref").value)
        }
        function geteq(eid) {
            document.getElementById("lbljump").value = "eq";
            checkarch();
            var sid = document.getElementById("lblsid").value;
            document.getElementById("geteq").src = "pmgetpmmax.aspx?jump=yes&typ=no&eqid=" + eid + "&pmid=0&fu=0&co=0&sid=" + sid;
        }
        function getfu(fid, eid) {
            document.getElementById("lbljump").value = "eq";
            checkarch();
            var sid = document.getElementById("lblsid").value;
            document.getElementById("geteq").src = "pmgetpmmax.aspx?jump=yes&typ=fu&eqid=" + eid + "&pmid=0&fu=" + fid + "&co=0&sid=" + sid;
        }
        function getco(cid, fid, eid) {
            document.getElementById("lbljump").value = "eq";
            checkarch();
            var sid = document.getElementById("lblsid").value;
            document.getElementById("geteq").src = "pmgetpmmax.aspx?jump=yes&typ=co&eqid=" + eid + "&pmid=0&fu=" + fid + "&co=" + cid + "&sid=" + sid;
        }

        function checkarch() {
            var jump = document.getElementById("lbljump").value;
            if (jump == "eq") {
                gettab(jump);
            }
            if (document.getElementById("lblgetarch").value == "yes") {
                document.getElementById("lblgetarch").value = "no"
                chk = document.getElementById("lblchk").value;
                did = document.getElementById("lbldid").value;
                clid = document.getElementById("lblclid").value;
                getarch(chk, did, clid)
            }
        }

        function getarch(chk, did, clid) {
            document.getElementById("lblchk").value = chk;
            document.getElementById("lbldid").value = did;
            document.getElementById("lblclid").value = clid;
            document.getElementById("ifarch").src = "pmarchmax.aspx?start=yes&chk=" + chk + "&did=" + did + "&clid=" + clid;
            if (document.getElementById("lbleqid").value != "") {
                document.getElementById("ifimg").src = "../equip/eqimg.aspx?eqid=" + document.getElementById("lbleqid").value + "&fuid=0";
            }
        }
        function getimage(eqid) {
            document.getElementById("ifimg").src = "../equip/eqimg.aspx?eqid=" + eqid + "&fuid=0";
        }

        function handletasks(href, rev) {
            document.getElementById("iftaskdet").src = href;
            if (rev == "dept") {
                checkarch()
                document.getElementById("ifimg").src = "../equip/eqimg.aspx?eqid=0&fuid=0";
            }
            else if (rev == "cell") {
                document.getElementById("ifimg").src = "../equip/eqimg.aspx?eqid=0&fuid=0";
            }
        }
        function handlecnt(cnt) {
            try {
                document.getElementById("taskcnt").value = cnt;
            }
            catch (err) {
            }
        }
        function handleapp(app) {
            if (app == "switch") document.getElementById("form1").submit();
        }
        function getjot() {
            var stat = ""; // document.getElementById("lblstat").value;
            var wo = document.getElementById("lblwo").value;
            var ro = document.getElementById("lblro").value;
            var wt = "MAXPM"; // document.getElementById("lblwt").value;
            var jpid = document.getElementById("lbljpid").value;
            var sid = document.getElementById("lblsid").value;
            var wonum = document.getElementById("lblwo").value;
            //alert(wonum)
            if (wonum != "") {
                //if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
                //window.parent.setref();
                var eReturn = window.showModalDialog("../appswo/wojpreportingdialog.aspx?stat=" + stat + "&jpid=" + jpid + "&wo=" + wo + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:650px; dialogWidth:850px; resizable=yes");
                if (eReturn) {
                    //document.getElementById("form1").submit()
                }
                //}
            }
        }
        function gettab(name) {

            if (name == "pm") {
                closeall(name);
                document.getElementById("tdpm").className = "thdrhov label";
                document.getElementById("pm").className = 'tdborder view';
                var sid = document.getElementById("lblsid").value;
                document.getElementById("ifmain").src = "pmmainmax.aspx?sid=" + sid + "&date=" + Date();
            }
            else if (name == "eq") {
                closeall(name);
                document.getElementById("tdeq").className = "thdrhov label";
                document.getElementById("eq").className = 'tdborder view';
                //var pmid = document.getElementById("lblpmid").value;
                //var eqid = document.getElementById("lbleqid").value;
                //geteq.Attributes.Add("src", "pmmainmax.aspx?jump=yes&typ=no&eqid=" & eqid & "&pmid=" & pmid)
                document.getElementById("lbljump").value = "eq";
                var id = document.getElementById("lblpmid").value;
                var eid = document.getElementById("lbleqid").value;
                var piccnt = "";
                //checkarch();
                var sid = document.getElementById("lblsid").value;
                document.getElementById("geteq").src = "pmgetpmmax.aspx?jump=yes&typ=no&eqid=" + eid + "&pmid=" + id + "&piccnt=" + piccnt + "&sid=" + sid;
            }
            else if (name == "fm") {

                /*
                var pmid = document.getElementById("lblpmid").value;
                var wo = document.getElementById("lblwo").value;
                if (pmid == "") pmid = "0";
                document.getElementById("ifmain").src = "pmfmmain.aspx?pmid=" + pmid + "&wo=" + wo + "&date=" + Date();
                */
                var pmid = document.getElementById("lblpmid").value;
                var wo = document.getElementById("lblwo").value;
                //alert(pmid + ", " + wo)
                if (pmid != "" && wo != "") {
                    closeall(name);
                    document.getElementById("tdfm").className = "thdrhov label";
                    document.getElementById("pm").className = 'tdborder view';
                    getjot();
                }
                else {
                    if (pmid == "") {
                        alert("No PM Record Selected")
                    }
                    else {
                        alert("No Work Order Record Selected")
                    }

                }


            }
            else if (name == "act") {
                document.getElementById("tdact").className = "thdrhov label";
                //document.getElementById("pm").className = 'tdborder view';
                var pmid = document.getElementById("lblpmid").value;
                var pmhid = document.getElementById("lblpmhid").value;
                var wo = document.getElementById("lblwo").value;
                var sid = document.getElementById("lblsid").value;
                if (pmid == "") pmid = "0";


                //document.getElementById("ifmain").src = "pmactmain.aspx?sid=" + sid + "&pmid=" + pmid + "&pmhid=" + pmhid + "&wo=" + wo + "&date=" + Date();
                var eReturn = window.showModalDialog("pmactdialog.aspx?sid=" + sid + "&pmid=" + pmid + "&pmhid=" + pmhid + "&wo=" + wo + "&date=" + Date(), "", "dialogHeight: 700px; dialogWidth:1040px;resizable=yes");

                if (eReturn) {

                }
            }
            else if (name == "cost") {
                document.getElementById("tdcost").className = "thdrhov label";
                document.getElementById("pm").className = 'tdborder view';
                var pmid = document.getElementById("lblpmid").value;
                var pmhid = document.getElementById("lblpmhid").value;
                var wo = document.getElementById("lblwo").value;
                if (pmid == "") pmid = "0";
                document.getElementById("ifmain").src = "pmcosts.aspx?pmid=" + pmid + "&pmhid=" + pmhid + "&wo=" + wo + "&date=" + Date();

            }
            else if (name == "mtr") {
                document.getElementById("tdmtr").className = "thdrhov label";
                document.getElementById("pm").className = 'tdborder view';
                var pmid = document.getElementById("lblpmid").value;
                var pmhid = document.getElementById("lblpmhid").value;
                var wo = document.getElementById("lblwo").value;
                var uid = document.getElementById("lbluid").value;
                var eqid = document.getElementById("lbleqid").value;
                //alert(wo)
                if (pmid == "") pmid = "0";
                document.getElementById("ifmain").src = "pmmeters.aspx?pmid=" + pmid + "&wo=" + wo + "&eqid=" + eqid + "&uid=" + uid + "&date=" + Date();

            }

        }
        function closeall(name) {

            if (name != "act") {
                document.getElementById("tdeq").className = "thdr label";
                document.getElementById("tdpm").className = "thdr label";
                document.getElementById("tdfm").className = "thdr label";
                /*
                document.getElementById("tdact").className = "thdr label";
                document.getElementById("tdcost").className = "thdr label";
                document.getElementById("tdmtr").className = "thdr label";
                */
                document.getElementById("pm").className = 'details';
                document.getElementById("eq").className = 'details';

            }

        }
        function checkarch() {
            var jump = document.getElementById("lbljump").value;
            if (jump == "eq") {
                gettab(jump);
            }
            if (document.getElementById("lblgetarch").value == "yes") {
                document.getElementById("lblgetarch").value = "no"
                chk = document.getElementById("lblchk").value;
                did = document.getElementById("lbldid").value;
                clid = document.getElementById("lblclid").value;
                getarch(chk, did, clid)
            }
        }

        function getarch(chk, did, clid) {
            document.getElementById("lblchk").value = chk;
            document.getElementById("lbldid").value = did;
            document.getElementById("lblclid").value = clid;
            document.getElementById("ifarch").src = "pmarchmax.aspx?start=yes&chk=" + chk + "&did=" + did + "&clid=" + clid;
            if (document.getElementById("lbleqid").value != "") {
                document.getElementById("ifimg").src = "../equip/eqimg.aspx?eqid=" + document.getElementById("lbleqid").value + "&fuid=0";
            }
        }
    </script>
</head>
<body onload="checkarch();">
    <form id="form1" runat="server">
    <div>
        <table style="z-index: 1; position: absolute; top: 83px; left: 7px" cellspacing="0"
            cellpadding="2" width="1043">
            <tr>
                <td width="760">
                    <table cellspacing="1" cellpadding="1">
                        <tr>
                            <td class="thdrhov label" id="tdpm" onclick="gettab('pm');" width="170" runat="server"
                                height="20">
                                <asp:Label ID="lang663" runat="server">Current PM Records</asp:Label>
                            </td>
                            <td class="thdr label" id="tdeq" onclick="gettab('eq');" width="140" runat="server">
                                <asp:Label ID="lang664" runat="server">PM Manager</asp:Label>
                            </td>
                            <td class="thdr label" id="tdfm" onclick="gettab('fm');" width="140" runat="server">
                                <asp:Label ID="lang665a" runat="server">Reporting</asp:Label>
                            </td>
                            <td class="details" id="tdact" onclick="gettab('act');" width="140" runat="server">
                                <asp:Label ID="lang666" runat="server">PM Actuals</asp:Label>
                            </td>
                            <td class="details" id="tdmtr" onclick="gettab('mtr');" width="140" runat="server">
                                Meter Readings
                            </td>
                            <td class="details" id="tdcost" onclick="gettab('cost');" width="140" runat="server">
                                <asp:Label ID="lang667" runat="server">PM Costs</asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="3">
                    &nbsp;
                </td>
                <td class="thdrsinglft" id="e1" width="26">
                    <img src="../images/appbuttons/minibuttons/eqarch.gif" border="0">
                </td>
                <td class="thdrsingrt label" id="e2" align="left" width="230">
                    <asp:Label ID="lang668" runat="server">Asset Hierarchy</asp:Label>
                </td>
            </tr>
            <tr>
                <td class="details" id="eq" runat="server" valign="top">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <iframe id="geteq" style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                                    margin: 0px; border-top-style: none; border-left-style: none" src="pmhold.htm"
                                    frameborder="no" width="790" scrolling="no" height="52" allowtransparency runat="server">
                                </iframe>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <iframe id="iftaskdet" style="border-bottom-style: none; padding-bottom: 0px; border-right-style: none;
                                    background-color: transparent; margin: 0px; padding-left: 0px; padding-right: 0px;
                                    border-top-style: none; border-left-style: none; padding-top: 0px" src="pmhold.htm"
                                    frameborder="no" width="790" scrolling="no" height="452" allowtransparency runat="server">
                                </iframe>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="tdborder" id="pm" valign="top" runat="server">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <iframe id="ifmain" style="background-color: transparent" src="pmhold.htm" frameborder="no"
                                    width="780" scrolling="no" height="500" allowtransparency runat="server"></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
                <td valign="top" colspan="2" rowspan="2">
                    <table cellspacing="0">
                        <tr>
                            <td colspan="2">
                                <iframe id="ifarch" style="border-bottom-style: none; padding-bottom: 0px; border-right-style: none;
                                    background-color: transparent; margin: 0px; padding-left: 0px; padding-right: 0px;
                                    border-top-style: none; border-left-style: none; padding-top: 0px" src="pmarchmax.aspx?start=no"
                                    frameborder="no" width="260" scrolling="yes" height="200" allowtransparency runat="server">
                                    ></iframe>
                            </td>
                        </tr>
                        <tr height="22">
                            <td class="thdrsinglft" width="26">
                                <img src="../images/appbuttons/minibuttons/eqarch.gif" border="0">
                            </td>
                            <td class="thdrsingrt label" width="204">
                                <asp:Label ID="lang669" runat="server">Asset Images</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <iframe id="ifimg" style="border-bottom-style: none; padding-bottom: 0px; border-right-style: none;
                                    background-color: transparent; margin: 0px; padding-left: 0px; padding-right: 0px;
                                    border-top-style: none; border-left-style: none; padding-top: 0px" src="../equip/eqimg.aspx?eqid=0&amp;fuid=0"
                                    frameborder="no" width="250" scrolling="no" height="272" allowtransparency runat="server">
                                </iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <input id="lbltab" type="hidden" name="lbltab" runat="server" />
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server" />
    <input id="lblsid" type="hidden" name="lblsid" runat="server" />
    <input id="lbldid" type="hidden" name="lbldid" runat="server" /><input id="lblclid"
        type="hidden" name="lblclid" runat="server" />
    <input id="lblret" type="hidden" name="lblret" runat="server" /><input id="lblchk"
        type="hidden" name="lblchk" runat="server" />
    <input id="lbldchk" type="hidden" name="lbldchk" runat="server" /><input id="lblfuid"
        type="hidden" name="lblfuid" runat="server" />
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server" />
    <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server" />
    <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server" />
    <input id="lblcid" type="hidden" name="lblcid" runat="server" />
    <input id="tasknum" type="hidden" name="tasknum" runat="server" /><input id="taskcnt"
        type="hidden" name="taskcnt" runat="server" />
    <input id="lblgetarch" type="hidden" name="lblgetarch" runat="server" />
    <input id="lblpmid" type="hidden" runat="server" />
    <input id="lbljump" type="hidden" runat="server" />
    <input id="lblwo" type="hidden" runat="server" />
    <input id="lblpmhid" type="hidden" runat="server" /><input id="lblro" type="hidden"
        runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblusername" runat="server" />
    <input type="hidden" id="lbluid" runat="server" />
    <input type="hidden" id="lblislabor" runat="server" />
    <input type="hidden" id="lblissuper" runat="server" />
    <input type="hidden" id="lblisplanner" runat="server" />
    <input type="hidden" id="lblLogged_In" runat="server" />
    <input type="hidden" id="lblms" runat="server" />
    <input type="hidden" id="lblappstr" runat="server" />
    <input type="hidden" id="lblhref" runat="server" />
    <input type="hidden" id="lbljpid" runat="server" />
    </form>
    <uc1:mmenu1 ID="mmenu11" runat="server" />
</body>
</html>
