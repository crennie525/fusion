﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class pmjpmanager

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''tdpm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdpm As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''lang663 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang663 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''tdeq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdeq As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''lang664 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang664 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''tdfm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdfm As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''lang665a control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang665a As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''tdact control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdact As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''lang666 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang666 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''tdmtr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdmtr As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdcost control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdcost As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''lang667 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang667 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lang668 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang668 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''eq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents eq As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''geteq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents geteq As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''iftaskdet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents iftaskdet As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''pm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pm As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''ifmain control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ifmain As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ifarch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ifarch As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lang669 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang669 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ifimg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ifimg As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lbltab control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbltab As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbleqid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbleqid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbldid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbldid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblclid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblclid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblret control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblret As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblchk control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblchk As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbldchk control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbldchk As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblfuid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblfuid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblcoid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblcoid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbltaskid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbltaskid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbltasklev control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbltasklev As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblcid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblcid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''tasknum control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tasknum As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''taskcnt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents taskcnt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblgetarch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblgetarch As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblpmid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblpmid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbljump control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbljump As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblwo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblwo As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblpmhid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblpmhid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblro As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblfslang control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblfslang As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblusername control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblusername As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbluid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbluid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblislabor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblislabor As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblissuper control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblissuper As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblisplanner control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblisplanner As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblLogged_In control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLogged_In As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblms control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblms As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblappstr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblappstr As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblhref control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblhref As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbljpid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbljpid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''mmenu11 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mmenu11 As Global.lucy_r12.mmenu1
End Class
