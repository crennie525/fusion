﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="maxnew.aspx.vb" Inherits="lucy_r12.maxnew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcsse.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="javascript" type="text/javascript">
        function checkret() {
            var ret = document.getElementById("lblret").value;
            if (ret == "go") {
                var pmid = document.getElementById("lblpmid").value;
                window.parent.handlereturn(pmid);
            }
        }
        function checknew() {
            var p = document.getElementById("txtnew").value;
            var d = document.getElementById("txtdesc").value;
            if (p == "") {
                alert("PM Number Required")
            }
            else if (d == "") {
                alert("Description Required")
            }
            else {
                document.getElementById("lblsubmit").value = "add";
                document.getElementById("form1").submit();
            }
        }
    </script>
</head>
<body onload="checkret();">
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td class="plainlabel">
                    New PM Number
                </td>
                <td>
                    <asp:TextBox ID="txtnew" runat="server" CssClass="plainlabel" Width="150"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="plainlabel">
                    Description
                </td>
                <td>
                    <asp:TextBox ID="txtdesc" runat="server" CssClass="plainlabel" Width="300"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <img alt="" src="../images/appbuttons/minibuttons/addmod.gif" onclick="checknew();"
                        onmouseover="return overlib('Add New PM Record and Exit')" onmouseout="return nd()" />
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblpmid" runat="server" />
    </form>
</body>
</html>
