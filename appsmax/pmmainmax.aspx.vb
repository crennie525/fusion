﻿Imports System.Data.SqlClient
Public Class pmmainmax
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim sql As String
    Dim man As New Utilities
    Dim sid, cid, srch, jump, eqid, fuid, coid, typ, ro, Login, tab, islabor, issched, userid, isplanner, cadm As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim cnt As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            tdrte.InnerHtml = ""
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            sid = Request.QueryString("sid").ToString 'HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            lblro.Value = ro
            man.Open()
            txtpg.Value = PageNumber
            'LoadSuper()
            LoadSkill()
            GetPdM()
            'GetDept()
            GetLists()
            'PopEq()

            Try
                jump = "no" 'Request.QueryString("jump").ToString
                If jump = "yes" Then
                    typ = Request.QueryString("typ").ToString
                    lbltyp.Value = typ
                    If typ = "eq" Then
                        eqid = Request.QueryString("eqid").ToString
                        lbleqid.Value = eqid
                    ElseIf typ = "fu" Then
                        fuid = Request.QueryString("fuid").ToString
                        lblfuid.Value = fuid
                    ElseIf typ = "co" Then
                        coid = Request.QueryString("coid").ToString
                        lblcoid.Value = coid
                    Else
                        lbltyp.Value = "no"
                    End If
                Else
                    lbltyp.Value = "no"
                End If
            Catch ex As Exception
                lbltyp.Value = "no"
            End Try
            GetPg(PageNumber)
            man.Dispose()
            'ddcells.Enabled = False
            lblprint.Value = "no"
        Else
            If Request.Form("lblprint") = "go" Then
                man.Open()
                GetPg(PageNumber, "yes")
                man.Dispose()
            End If
            If Request.Form("lblret") = "next" Then
                man.Open()
                GetNext()
                man.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                man.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetPg(PageNumber)
                man.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                man.Open()
                GetPrev()
                man.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                man.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetPg(PageNumber)
                man.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetLists()
        'get default
        Dim def As String
        sql = "select wostatus from wostatus where compid = '" & cid & "' and isdefault = 1"
        dr = man.GetRdrData(sql)
        While dr.Read
            def = dr.Item("wostatus").ToString
        End While
        dr.Close()
        sql = "select wostatus from wostatus where compid = '" & cid & "' and active = 1"
        dr = man.GetRdrData(sql)
        ddstatus.DataSource = dr
        ddstatus.DataTextField = "wostatus"
        ddstatus.DataValueField = "wostatus"
        ddstatus.DataBind()
        dr.Close()
        'If def <> "" Then
        'ddstatus.SelectedValue = def
        'Else
        ddstatus.Items.Insert(0, New ListItem("Select"))
        ddstatus.Items(0).Value = 0
        'End If

    End Sub
   
    Private Sub GetPdM()
        sid = lblsid.Value 'HttpContext.Current.Session("dfltps").ToString
        sql = "select distinct pm.ptid, pm.pretech from equipment e " _
        + "right join pm pm on pm.eqid = e.eqid " _
        + "where e.siteid = '" + sid + "' and ptid <> 0"
        dr = man.GetRdrData(sql)
        ddpdmlist.DataSource = dr
        ddpdmlist.DataValueField = "ptid"
        ddpdmlist.DataTextField = "pretech"
        ddpdmlist.DataBind()
        dr.Close()
        'ddskill.Items.Insert(0, "Select Skill")
        ddpdmlist.Items.Insert(0, "All Skills")
        Try
            ddpdmlist.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    
    Private Sub LoadSkill()
        sql = "select distinct p.skillid, p.skill from pm p left join equipment e on e.eqid = p.eqid"
        dr = man.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataValueField = "skillid"
        ddskill.DataTextField = "skill"
        ddskill.DataBind()
        dr.Close()
        'ddskill.Items.Insert(0, "Select Skill")
        ddskill.Items.Insert(0, "All Skills")
        Try
            ddskill.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Function fixsrch(ByVal srch As String)
        srch = man.ModString2(srch)
        Return srch
    End Function
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        lblsrch.Value = "yes"
        PageNumber = 1
        man.Open()
        GetPg(PageNumber)
        man.Dispose()
    End Sub
    Private Sub GetPg(ByVal PageNumber As Integer, Optional ByVal pall As String = "no")
        Dim filt, s, e, Filter, FilterCNT, srch As String

        sid = lblsid.Value
        cid = "0"

        issched = lblissched.Value
        userid = lbluserid.Value
        isplanner = lblisplanner.Value
        islabor = lblislabor.Value
        cadm = lblcadm.Value

        Dim sup, nex, las As String
        Dim piccnt As Integer

        Dim sflag As Integer = 0
        typ = lbltyp.Value
        'If typ = "no" Then
        If lblsrch.Value = "yes" Then
            Filter = "e.siteid = ''" & sid & "'' and pm.nextdate is not null "
            FilterCNT = "e.siteid = '" & sid & "' and pm.nextdate is not null "
        Else
            Filter = "e.siteid = ''" & sid & "'' and pm.nextdate is not null "
            FilterCNT = "e.siteid = '" & sid & "' and pm.nextdate is not null "
        End If

        If txtsrchwo.Text <> "" Then
            srch = txtsrchwo.Text
            srch = fixsrch(srch)
            Dim woi As Long
            Try
                woi = System.Convert.ToInt32(srch)
            Catch ex As Exception
                Dim strMessage As String = "Work Order Number Must Be A Numeric Value"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            Filter += "and w.wonum = ''" & srch & "'' "
            FilterCNT += "and w.wonum = '" & srch & "' "
        End If

        If ddstatus.SelectedIndex <> 0 And ddstatus.SelectedIndex <> -1 Then
            Filter += "and w.status = ''" & ddstatus.SelectedValue.ToString & "'' "
            FilterCNT += "and w.status = '" & ddstatus.SelectedValue.ToString & "' "
        Else
            Filter += "and w.status not in (''COMP'',''CLOSE'') "
            FilterCNT += "and w.status not in ('COMP','CLOSE') "
        End If

        If lblsup.Value <> "" Then
            srch = lblsup.Value 'txtsup.Text
            srch = fixsrch(srch)
            Filter += "and pm.superid = ''" & srch & "'' "
            FilterCNT += "and pm.superid = '" & srch & "' "
        End If
        If lbllead.Value <> "" Then
            srch = lbllead.Value 'txtlead.Text
            srch = fixsrch(srch)
            Filter += "and pm.leadid = ''" & srch & "'' "
            FilterCNT += "and pm.leadid = '" & srch & "' "
        End If

        If lblrtid.Value <> "" Then
            srch = lblrtid.Value
            srch = fixsrch(srch)
            Filter += "and pm.rid = ''" & lblrtid.Value & "'' "
            FilterCNT += "and pm.rid = '" & lblrtid.Value & "' "
        End If


        'And dddepts.SelectedIndex <> 0
        If lbldept.Value <> "" Then
            Filter += "and e.dept_id = ''" & lbldid.Value & "'' "
            FilterCNT += "and e.dept_id = '" & lbldid.Value & "' "
        End If
        If lblclid.Value <> "" Then
            Filter += "and e.cellid = ''" & lblclid.Value & "'' "
            FilterCNT += "and e.cellid = '" & lblclid.Value & "' "
        End If
        Dim par As String = lbllocstr.Value
        If lbllid.Value <> "" And par <> "" Then
            'e.eqnum in (select h.location from pmlocheir h where h.parent = 'CRTST')
            'Filter += "and e.locid = ''" & lbllid.Value & "'' "
            'FilterCNT += "and e.locid = '" & lbllid.Value & "' "
            Filter += "and e.eqnum in (select h.location from pmlocheir h where h.parent = ''" & par & "'')"
            FilterCNT += "and e.eqnum in (select h.location from pmlocheir h where h.parent = '" & par & "')"
            lblloc.Value = lbllocstr.Value
        End If

        If lbleqid.Value <> "" Then
            Filter += "and pm.eqid = ''" & lbleqid.Value & "'' "
            FilterCNT += "and pm.eqid = '" & lbleqid.Value & "' "
        End If
        If lblfuid.Value <> "" Then
            Filter += "and pt.funcid = ''" & lblfuid.Value & "'' "
            FilterCNT += "and pt.funcid = '" & lblfuid.Value & "' "
        End If
        If lblcomid.Value <> "" Then
            Filter += "and pt.comid = ''" & lblcomid.Value & "'' "
            FilterCNT += "and pt.comid = '" & lblcomid.Value & "' "
        End If
        If ddskill.SelectedIndex <> 0 And ddskill.SelectedIndex <> -1 Then
            Filter += "and pm.skillid = ''" & ddskill.SelectedValue.ToString & "'' "
            FilterCNT += "and pm.skillid = '" & ddskill.SelectedValue.ToString & "' "
        End If

        If ddpdmlist.SelectedIndex <> 0 And ddpdmlist.SelectedIndex <> -1 Then
            FilterCNT += "and pm.ptid = '" & ddpdmlist.SelectedValue.ToString & "'"
            Filter += "and pm.ptid = ''" & ddpdmlist.SelectedValue.ToString & "''"
        End If

        Dim rtid As String
        'was txtrte.Text
        If lblrtid.Value <> "" Then
            rtid = lblrtid.Value
            FilterCNT += "and pm.pm1 = '" & rtid & "'"
            Filter += "and pm.pm1 = ''" & rtid & "''"
            imgrteprint.Attributes.Add("class", "")
            imgrtewprint.Attributes.Add("class", "")

            imgregpm.Attributes.Add("class", "details")
            imgwopm.Attributes.Add("class", "details")
        Else
            imgrteprint.Attributes.Add("class", "details")
            imgrtewprint.Attributes.Add("class", "details")
            If lblsrch.Value = "yes" Then
                imgregpm.Attributes.Add("class", "")
                imgwopm.Attributes.Add("class", "")
            Else
                imgregpm.Attributes.Add("class", "details")
                imgwopm.Attributes.Add("class", "details")
            End If
        End If
        'Else
        If typ = "eq" Then
            eqid = lbleqid.Value
            FilterCNT += "and pm.eqid = '" & eqid & "'"
            Filter += "and pm.eqid = ''" & eqid & "''"
        ElseIf typ = "fu" Then
            fuid = lblfuid.Value
            'FilterCNT += "and pt.funcid = '" & fuid & "'"
            'Filter += "and pt.funcid = ''" & fuid & "''"
        ElseIf typ = "co" Then
            coid = lblcoid.Value
            'FilterCNT += "and pt.comid = '" & coid & "'"
            'Filter += "and pt.comid = ''" & coid & "''"
        End If
        'End If
        If cadm <> "1" Then
            If islabor = "1" And isplanner <> "1" Then
                If Filter <> "" Then
                    Filter += " and (w.eqid in (select eqid from pmlaborlocs where laborid = ''" & userid & "'') or " _
                        + "w.eqid in (select eqid from workorder where leadcraftid = ''" & userid & "''))"
                    FilterCNT += " and (w.eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "') or " _
                        + "w.eqid in (select eqid from workorder where leadcraftid = '" & userid & "'))"
                Else
                    Filter += " (w.eqid in (select eqid from pmlaborlocs where laborid = ''" & userid & "'') or " _
                       + "w.eqid in (select eqid from workorder where leadcraftid = ''" & userid & "''))"
                    FilterCNT += " (w.eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "') or " _
                        + "w.eqid in (select eqid from workorder where leadcraftid = '" & userid & "'))"
                End If

            End If
        End If

        s = lbls.Value 'txts.Text 'fixsrch(txts.Text)
        e = lble.Value 'txte.Text 'fixsrch(txte.Text)
        If Len(s) <> 0 Then
            If Len(e) <> 0 Then
                FilterCNT += " and pm.nextdate between '" & s & "' and '" & e & "'"
                Filter += " and pm.nextdate between ''" & s & "'' and ''" & e & "''"
            Else
                FilterCNT += " and pm.nextdate >= '" & s & "'"
                Filter += " and pm.nextdate >= ''" & s & "''"
            End If
        End If

        sql = "select count(distinct pm.pmid) from pmmax pm " _
        + "left join equipment e on e.eqid = pm.eqid " _
        + "left join workorder w on w.pmid = pm.pmid " _
        + "left join pmroutes pr on pr.rid = pm.rid " _
            + " where " + FilterCNT

        Dim intPgCnt, intPgNav As Integer
        intPgCnt = man.Scalar(sql)
        'intPgCnt = 1

        If intPgCnt > 0 Then
            txtpg.Value = PageNumber
            intPgNav = man.PageCountRev(intPgCnt, PageSize)
            sql = "usp_getAllPMMAXPg2 '" & sid & "', '" & PageNumber & "', '" & PageSize & "', '" & Filter & "'"
            Dim ds As New DataSet
            ds = man.GetDSData(sql)
            Dim dt As New DataTable
            dt = ds.Tables(0)
            rptrtasks.DataSource = dt
            rptrtasks.DataBind()
            Dim i, eint As Integer
            eint = 15
            lblfiltwo.Value = " where " & FilterCNT
            lblfilt.Value = " where " & FilterCNT
            filt = " where " & FilterCNT
            'GetDocs(filt)
            Dim row As DataRow
            Dim pmidstr, pmid As String
            For Each row In dt.Rows
                pmid = row("pmid").ToString
                If pmidstr = "" Then
                    pmidstr = pmid
                Else
                    pmidstr += "," & pmid
                End If
            Next
            Dim pmidarr() As String = pmidstr.Split(",")
            Dim sbc As Integer
            Dim doc, docs, fn, ty As String
            For i = 0 To pmidarr.Length - 1
                pmid = pmidarr(i).ToString
                'sql = "select count(*) from pmManAttach where pmid = '" & pmid & "'"
                'sbc = man.Scalar(sql)
                'If sbc > 0 Then ' should be using has rows instead of count
                sql = "select * from pmManAttach where pmid = '" & pmid & "'"
                ds = man.GetDSData(sql)
                dt = ds.Tables(0)
                'dr = man.GetRdrData(sql)
                'While dr.Read
                For Each row In dt.Rows
                    fn = row("filename").ToString 'dr.Item("filename").ToString
                    ty = row("doctype").ToString 'dr.Item("doctype").ToString
                    doc = ty & "~" & fn
                    If docs = "" Then
                        docs = doc
                    Else
                        docs += ";" & doc
                    End If
                Next

                'End While
                'dr.Close()
                lbldocs.Value = docs
                'End If
            Next
            If pall = "yes" Then
                lblprint.Value = "yes"
            End If
        Else
            lblfiltwo.Value = ""
            lblfilt.Value = ""
            lblprint.Value = "no"
            imgregpm.Attributes.Add("class", "details")
            imgwopm.Attributes.Add("class", "details")
            imgrteprint.Attributes.Add("class", "details")
            imgrtewprint.Attributes.Add("class", "details")
            Dim strMessage As String = tmod.getmsg("cdstr339", "PMMainMan.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        Dim who As String = lblwho.Value
        If who = "depts" Then
            trdepts.Attributes.Add("class", "view")
            'tddept.InnerHtml = lbldept.Value
            'tdcell.InnerHtml = lblcell.Value
            'tdeq.InnerHtml = lbleq.Value
            'tdfu.InnerHtml = lblfu.Value
            'tdco.InnerHtml = lblcomp.Value
        Else
            trdepts.Attributes.Add("class", "details")
        End If
        If who = "locs" Then
            trlocs3.Attributes.Add("class", "view")
            'tdloc3.InnerHtml = lblloc.Value
            'tdeq3.InnerHtml = lbleq.Value
            'tdfu3.InnerHtml = lblfu.Value
            'tdco3.InnerHtml = lblcomp.Value
        Else
            trlocs3.Attributes.Add("class", "details")
        End If
    End Sub
    Private Sub GetDocs(ByVal filt As String)
        sql = "select distinct pm.pmid from pm pm left join equipment e on e.eqid = pm.eqid " _
        + "left join workorder w on w.pmid = pm.pmid " _
        + "left join pmroutes pr on pr.rid = pm.rid " _
        + "left join pmtrack pt on pt.pmid = pm.pmid " & filt
        Dim pmidstr, pmid As String
        Dim sbc As Integer
        Dim doc, docs, fn, ty As String
        dr = man.GetRdrData(sql)
        While dr.Read
            If pmidstr = "" Then
                pmidstr = dr.Item("pmid").ToString
            Else
                pmidstr += "," & dr.Item("pmid").ToString
            End If
        End While
        dr.Close()
        Dim pmidarr() As String = pmidstr.Split(",")
        Dim i As Integer
        For i = 0 To pmidarr.Length - 1
            pmid = pmidarr(i).ToString
            sql = "select count(*) from pmManAttach where pmid = '" & pmid & "'"
            sbc = man.Scalar(sql)
            If sbc > 0 Then ' should be using has rows instead of count
                sql = "select * from pmManAttach where pmid = '" & pmid & "'"
                dr = man.GetRdrData(sql)
                While dr.Read
                    fn = dr.Item("filename").ToString
                    ty = dr.Item("doctype").ToString
                    doc = ty & "~" & fn
                    If docs = "" Then
                        docs = doc
                    Else
                        docs += ";" & doc
                    End If
                End While
                dr.Close()
                lbldocs.Value = docs
            End If
        Next
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetPg(PageNumber)
        Catch ex As Exception
            man.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr340", "PMMainMan.aspx.vb")

            man.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetPg(PageNumber)
        Catch ex As Exception
            man.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr341", "PMMainMan.aspx.vb")

            man.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub rptrtasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtasks.ItemDataBound
        If e.Item.ItemType = ListItemType.Header Then
            Dim tdwo As HtmlTableCell = CType(e.Item.FindControl("tdwo"), HtmlTableCell)
            'Dim tblywr As HtmlTable = CType(e.Item.FindControl("tblywr"), HtmlTable)
            tab = lbltab.Value
            If tab = "yes" Then
                tdwo.Attributes.Add("class", "details")
                'tblywr.Attributes.Add("width", "670")
            End If
        End If


        If e.Item.ItemType = ListItemType.Item Then

            Dim tdwoi As HtmlTableCell = CType(e.Item.FindControl("tdwoi"), HtmlTableCell)
            tab = lbltab.Value
            If tab = "yes" Then
                tdwoi.Attributes.Add("class", "details")
            End If


            Dim img As HtmlImage = CType(e.Item.FindControl("imgi"), HtmlImage)
            Dim imgi As HtmlImage = CType(e.Item.FindControl("imgi2"), HtmlImage)
            Dim imgi3 As HtmlImage = CType(e.Item.FindControl("imgi3"), HtmlImage)
            Dim link As LinkButton = CType(e.Item.FindControl("lbltn"), LinkButton)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "pmid").ToString
            Dim eid As String = DataBinder.Eval(e.Item.DataItem, "eqid").ToString
            Dim wo As String = DataBinder.Eval(e.Item.DataItem, "wonum").ToString
            Dim fccnt As String = DataBinder.Eval(e.Item.DataItem, "piccnt").ToString
            img.Attributes("onclick") = "print('" & id & "');"
            imgi.Attributes("onclick") = "printwo('" & id & "','" & wo & "');"
            If fccnt <> "0" And fccnt <> "" Then
                imgi3.Attributes("onclick") = "printwopics('" & id & "','" & wo & "');"
            Else
                imgi3.Attributes.Add("class", "details")
            End If
            sid = lblsid.Value
            link.Attributes("onclick") = "jump('" & id & "', '" & eid & "','" & fccnt & "','" & sid & "');"
            Dim simg As HtmlImage = CType(e.Item.FindControl("iwi"), HtmlImage)
            Dim newdate As String = DataBinder.Eval(e.Item.DataItem, "nextdate").ToString
            Try
                newdate = CType(newdate, DateTime)
                If newdate < Now Then
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/rwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov72", "PMMainMan.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                Else
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/gwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov73", "PMMainMan.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                End If
            Catch ex As Exception

            End Try

        ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim tdwoe As HtmlTableCell = CType(e.Item.FindControl("tdwoe"), HtmlTableCell)
            tab = lbltab.Value
            If tab = "yes" Then
                tdwoe.Attributes.Add("class", "details")
            End If

            Dim img As HtmlImage = CType(e.Item.FindControl("imga"), HtmlImage)
            Dim imga As HtmlImage = CType(e.Item.FindControl("imga2"), HtmlImage)
            Dim imga3 As HtmlImage = CType(e.Item.FindControl("imga3"), HtmlImage)
            Dim link As LinkButton = CType(e.Item.FindControl("lbltnalt"), LinkButton)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "pmid").ToString
            Dim eid As String = DataBinder.Eval(e.Item.DataItem, "eqid").ToString
            Dim wo As String = DataBinder.Eval(e.Item.DataItem, "wonum").ToString
            Dim fccnt1 As String = DataBinder.Eval(e.Item.DataItem, "piccnt").ToString
            img.Attributes("onclick") = "print('" & id & "');"
            imga.Attributes("onclick") = "printwo('" & id & "','" & wo & "');"
            sid = lblsid.Value
            link.Attributes("onclick") = "jump('" & id & "', '" & eid & "','" & fccnt1 & "','" & sid & "');"
            If fccnt1 <> "0" And fccnt1 <> "" Then
                imga3.Attributes("onclick") = "printwopics('" & id & "','" & wo & "');"
            Else
                imga3.Attributes.Add("class", "details")
            End If
            Dim simg As HtmlImage = CType(e.Item.FindControl("iwa"), HtmlImage)
            Dim newdate As String = DataBinder.Eval(e.Item.DataItem, "nextdate").ToString
            Try
                newdate = CType(newdate, DateTime)
                If newdate < Now Then
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/rwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov74", "PMMainMan.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                Else
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/gwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov75", "PMMainMan.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                End If
            Catch ex As Exception

            End Try

        End If

        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang642 As Label
                lang642 = CType(e.Item.FindControl("lang642"), Label)
                lang642.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang642")
            Catch ex As Exception
            End Try
            Try
                Dim lang643 As Label
                lang643 = CType(e.Item.FindControl("lang643"), Label)
                lang643.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang643")
            Catch ex As Exception
            End Try
            Try
                Dim lang644 As Label
                lang644 = CType(e.Item.FindControl("lang644"), Label)
                lang644.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang644")
            Catch ex As Exception
            End Try
            Try
                Dim lang645 As Label
                lang645 = CType(e.Item.FindControl("lang645"), Label)
                lang645.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang645")
            Catch ex As Exception
            End Try
            Try
                Dim lang646 As Label
                lang646 = CType(e.Item.FindControl("lang646"), Label)
                lang646.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang646")
            Catch ex As Exception
            End Try
            Try
                Dim lang647 As Label
                lang647 = CType(e.Item.FindControl("lang647"), Label)
                lang647.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang647")
            Catch ex As Exception
            End Try
            Try
                Dim lang648 As Label
                lang648 = CType(e.Item.FindControl("lang648"), Label)
                lang648.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang648")
            Catch ex As Exception
            End Try
            Try
                Dim lang649 As Label
                lang649 = CType(e.Item.FindControl("lang649"), Label)
                lang649.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang649")
            Catch ex As Exception
            End Try
            Try
                Dim lang650 As Label
                lang650 = CType(e.Item.FindControl("lang650"), Label)
                lang650.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang650")
            Catch ex As Exception
            End Try
            Try
                Dim lang651 As Label
                lang651 = CType(e.Item.FindControl("lang651"), Label)
                lang651.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang651")
            Catch ex As Exception
            End Try
            Try
                Dim lang652 As Label
                lang652 = CType(e.Item.FindControl("lang652"), Label)
                lang652.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang652")
            Catch ex As Exception
            End Try
            Try
                Dim lang653 As Label
                lang653 = CType(e.Item.FindControl("lang653"), Label)
                lang653.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang653")
            Catch ex As Exception
            End Try
            Try
                Dim lang654 As Label
                lang654 = CType(e.Item.FindControl("lang654"), Label)
                lang654.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang654")
            Catch ex As Exception
            End Try
            Try
                Dim lang655 As Label
                lang655 = CType(e.Item.FindControl("lang655"), Label)
                lang655.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang655")
            Catch ex As Exception
            End Try
            Try
                Dim lang656 As Label
                lang656 = CType(e.Item.FindControl("lang656"), Label)
                lang656.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang656")
            Catch ex As Exception
            End Try
            Try
                Dim lang657 As Label
                lang657 = CType(e.Item.FindControl("lang657"), Label)
                lang657.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang657")
            Catch ex As Exception
            End Try
            Try
                Dim lang658 As Label
                lang658 = CType(e.Item.FindControl("lang658"), Label)
                lang658.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang658")
            Catch ex As Exception
            End Try
            Try
                Dim lang659 As Label
                lang659 = CType(e.Item.FindControl("lang659"), Label)
                lang659.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang659")
            Catch ex As Exception
            End Try
            Try
                Dim lang660 As Label
                lang660 = CType(e.Item.FindControl("lang660"), Label)
                lang660.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang660")
            Catch ex As Exception
            End Try
            Try
                Dim lang661 As Label
                lang661 = CType(e.Item.FindControl("lang661"), Label)
                lang661.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang661")
            Catch ex As Exception
            End Try
            Try
                Dim lang662 As Label
                lang662 = CType(e.Item.FindControl("lang662"), Label)
                lang662.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang662")
            Catch ex As Exception
            End Try

        End If
    End Sub
    Private Sub ibfilt_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        man.Open()
        GetPg(PageNumber)
        man.Dispose()
    End Sub
    Public Function CellCheck(ByVal filt As String) As String
        Dim cells As String
        Dim cellcnt As Integer
        sql = "select count(*) from Cells where Dept_ID = '" & filt & "'"
        cellcnt = man.Scalar(sql)
        Dim nocellcnt As Integer
        If cellcnt = 0 Then
            cells = "no"
        ElseIf cellcnt = 1 Then
            sql = "select count(*) from Cells where Dept_ID = '" & filt & "' and cell_name = 'No Cells'"
            nocellcnt = man.Scalar(sql)
            If nocellcnt = 1 Then
                cells = "no"
            Else
                cells = "yes"
            End If
        Else
            cells = "yes"
        End If
        Return cells
    End Function
   
    
    Private Sub GetLoc(Optional ByVal lid As String = "0")
        If lid <> 0 Then
            lid = lbllid.Value
        End If

        If lid <> "" And lid <> "0" Then
            sql = "select location, description from pmlocations where locid = '" & lid & "'"
            dr = man.GetRdrData(sql)
            While dr.Read
                lblloc.Value = dr.Item("location").ToString
            End While
            dr.Close()
        End If

    End Sub
   

End Class