﻿Imports System.Data.SqlClient
Public Class pmgridmax
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim ds As DataSet
    Dim tasksg As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim start, tl, cid, sid, did, clid, eqid, chk, fuid, Val, field, name, Filter, coid, pmid, jump, typ, Login, piccnt As String
    Dim co_only As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        'Dim app As New AppUtils
        'Dim url As String = app.Switch
        'If url <> "ok" Then
        'appchk.Value = "switch"
        'End If
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            'lbllog.Value = "no"
            'Exit Sub
        End Try

        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            jump = Request.QueryString("jump").ToString
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            taskcnt.Value = "0"
            If start = "yes" Then
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                If jump = "yes" Then
                    pmid = Request.QueryString("pmid").ToString
                    lblpmid.Value = pmid
                    typ = Request.QueryString("typ").ToString
                    lbltyp.Value = typ
                    Try
                        piccnt = Request.QueryString("piccnt").ToString
                        lblpiccnt.Value = piccnt
                    Catch ex As Exception

                    End Try
                    If typ = "no" Then
                        Response.Redirect("pmdivmax.aspx?pmid=" & pmid & "&eqid=" & eqid & "&piccnt=" & piccnt & "&sid=" + sid)
                    Else
                        fuid = Request.QueryString("fu").ToString
                        lblfuid.Value = fuid
                        coid = Request.QueryString("co").ToString
                        lblcoid.Value = coid
                        tasksg.Open()
                        LoadPage(eqid, typ)
                        tasksg.Dispose()
                    End If

                Else
                    tasksg.Open()
                    LoadPage(eqid)
                    tasksg.Dispose()
                End If
            Else
                eqid = "na"
                tasksg.Open()
                LoadPage(eqid)
                tasksg.Dispose()
            End If
        End If

    End Sub
    Private Sub LoadPage(ByVal eqid As String, Optional ByVal typ As String = "no")
        Dim cnt As Integer
        Dim sqlcnt As String
        If eqid = "na" Then
            cnt = 0
            sql = "select distinct pm.pmid, " _
            + "pmd = (" _
            + "case pm.ptid " _
            + "when 0 then 'None' " _
            + "else pm.pretech " _
            + "End " _
            + "), " _
            + "pm = pm.description, " _
            + "pm.lastcompdate, pm.nextdate, '0' as 'flag' " _
            + "from pmmax pm " _
            + "where pm.eqid = 0 order by pm.nextdate"
            'join pmtasks t on t.pmid = pm.pmid " _
            sqlcnt = "select count(*) from pm where eqid is not null order by nextdate"
        Else
            cnt = 1
            If typ = "no" Then
                sql = "select distinct pm.pmid, " _
                + "pmd = (" _
                + "case pm.ptid " _
                + "when 0 then 'None' " _
                + "else pm.pretech " _
                + "End " _
                + "), " _
                + "pm = pm.description, " _
                + "Convert(char(10), pm.lastcompdate,101) as 'lastdate', Convert(char(10), pm.nextdate,101) as 'nextdate', '0' as 'flag' " _
                + "from pmmax pm " _
                + "where pm.eqid = '" & eqid & "' order by nextdate"
            ElseIf typ = "fu" Then
                fuid = lblfuid.Value
                sql = "select distinct pm.pmid, " _
                + "pmd = (case pm.ptid when 0 then 'None' else pm.pretech End ), " _
                + "pm = pm.description, " _
                + "lastdate = Convert(char(10), pm.lastcompdate,101), " _
                + "nextdate = Convert(char(10), pm.nextdate,101), " _
                + "flag = 0 " _
                + "from pmmax pm where pm.eqid = '" & eqid & "' " _
                + "order by flag desc, nextdate asc"
            ElseIf typ = "co" Then
                coid = lblcoid.Value
                sql = "select distinct pm.pmid, " _
                + "pmd = (case pm.ptid when 0 then 'None' else pm.pretech End ), " _
                + "pm = pm.description, " _
                + "lastdate = Convert(char(10), pm.lastcompdate,101), " _
                + "nextdate = Convert(char(10), pm.nextdate,101), " _
                + "flag = 0 " _
                + "from pmmax pm where pm.eqid = '" & eqid & "' " _
                + "order by flag desc, nextdate asc"
            End If

            sqlcnt = "select count(*) from pmmax where eqid = '" & eqid & "'"
        End If
        If cnt <> 0 Then
            cnt = tasksg.Scalar(sqlcnt)
            taskcnt.Value = cnt
        Else
            taskcnt.Value = "0"
        End If
        ds = tasksg.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        rptrtasks.DataSource = dt
        Dim i, eint As Integer
        eint = 15
        For i = cnt To eint
            dt.Rows.InsertAt(dt.NewRow(), i)
        Next
        rptrtasks.DataBind()
    End Sub

    Private Sub rptrtasks_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrtasks.ItemCommand
        If e.CommandName = "Select" Then
            Dim pmid, pmstr, pdm, dlast, dnext As String
            Try
                pmid = CType(e.Item.FindControl("lblpmiditem"), Label).Text
                pmstr = CType(e.Item.FindControl("lbltn"), LinkButton).Text
                pdm = CType(e.Item.FindControl("lblpdm"), Label).Text
                dlast = CType(e.Item.FindControl("lbllast"), Label).Text
                dnext = CType(e.Item.FindControl("lblnext"), Label).Text
            Catch ex As Exception
                pmid = CType(e.Item.FindControl("lblpmidalt"), Label).Text
                pmstr = CType(e.Item.FindControl("lbltnalt"), LinkButton).Text
                pdm = CType(e.Item.FindControl("lblpdmalt"), Label).Text
                dlast = CType(e.Item.FindControl("lbllastalt"), Label).Text
                dnext = CType(e.Item.FindControl("lblnextalt"), Label).Text
            End Try
            tl = lbltasklev.Value
            'cid = lblcid.Value
            'sid = lblsid.Value
            'did = lbldid.Value
            'clid = lblclid.Value
            eqid = lbleqid.Value
            'chk = lblchk.Value
            'fuid = lblfuid.Value
            'coid = lblcoid.Value
            piccnt = lblpiccnt.Value
            pmstr = pmstr.Replace("#", "%23")
            sid = lblsid.Value
            Dim tst As String = "pmdivmax.aspx?pmid=" & pmid & "&eqid=" & eqid & "&pmstr=" & pmstr & "&pdm=" & pdm & "&last=" & dlast & "&next=" & dnext & "&piccnt=" & piccnt & "&sid=" & sid
            Response.Redirect("pmdivmax.aspx?pmid=" & pmid & "&eqid=" & eqid & "&pmstr=" & pmstr & "&pdm=" & pdm & "&last=" & dlast & "&next=" & dnext & "&piccnt=" & piccnt & "&sid=" & sid)

        End If
    End Sub

    Private Sub rptrtasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtasks.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Then
            Dim link As LinkButton = CType(e.Item.FindControl("lbltn"), LinkButton)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "flag").ToString
            If id <> "0" Then
                link.Attributes.Add("class", "redlink")
                'link.Attributes("CssClass") = "redlink"
                'link.Attributes("ForeColor") = "Red"
            End If
            Dim simg As HtmlImage = CType(e.Item.FindControl("iwi"), HtmlImage)
            Dim newdate As String = DataBinder.Eval(e.Item.DataItem, "nextdate").ToString
            If newdate <> "" Then
                newdate = CType(newdate, DateTime)
                If newdate < Now Then
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/rwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov67", "PMGridMan.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                Else
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/gwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov68", "PMGridMan.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                End If
            Else
                simg.Attributes.Add("src", "../images/appbuttons/minibuttons/2PX.gif")
            End If

        ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim link As LinkButton = CType(e.Item.FindControl("lbltnalt"), LinkButton)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "flag").ToString
            If id <> "0" Then
                link.Attributes.Add("class", "redlink")
                'link.Attributes("CssClass") = "linklabelred"
            End If
            Dim simg As HtmlImage = CType(e.Item.FindControl("iwa"), HtmlImage)
            Dim newdate As String = DataBinder.Eval(e.Item.DataItem, "nextdate").ToString
            If newdate <> "" Then
                newdate = CType(newdate, DateTime)
                If newdate < Now Then
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/rwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov69", "PMGridMan.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                Else
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/gwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov70", "PMGridMan.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                End If
            Else
                simg.Attributes.Add("src", "../images/appbuttons/minibuttons/2PX.gif")
            End If
        End If

        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang639 As Label
                lang639 = CType(e.Item.FindControl("lang639"), Label)
                lang639.Text = axlabs.GetASPXPage("PMGridMan.aspx", "lang639")
            Catch ex As Exception
            End Try
            Try
                Dim lang640 As Label
                lang640 = CType(e.Item.FindControl("lang640"), Label)
                lang640.Text = axlabs.GetASPXPage("PMGridMan.aspx", "lang640")
            Catch ex As Exception
            End Try
            Try
                Dim lang641 As Label
                lang641 = CType(e.Item.FindControl("lang641"), Label)
                lang641.Text = axlabs.GetASPXPage("PMGridMan.aspx", "lang641")
            Catch ex As Exception
            End Try

        End If

    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            'lang639.Text = axlabs.GetASPXPage("PMGridMan.aspx", "lang639")
        Catch ex As Exception
        End Try
        Try
            'lang640.Text = axlabs.GetASPXPage("PMGridMan.aspx", "lang640")
        Catch ex As Exception
        End Try
        Try
            'lang641.Text = axlabs.GetASPXPage("PMGridMan.aspx", "lang641")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            'iwa.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMGridMan.aspx", "iwa") & "', ABOVE, LEFT)")
            'iwa.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'iwi.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMGridMan.aspx", "iwi") & "', ABOVE, LEFT)")
            'iwi.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub
End Class