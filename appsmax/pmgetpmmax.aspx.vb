﻿Imports System.Data.SqlClient
Public Class pmgetpmmax
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim tasks As New Utilities
    Dim sql, cid, sid, eq, eqid, fu, fuid, did, clid, jump, co, coid, typ, piccnt As String
    Dim Filter, filt, dt, df, tl, val, tli, ro, userid, islabor, isplanner, cadm, appstr As String
    Dim pmid, type, login As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            'lbllog.Value = "no"
            'Exit Sub
        End Try
        'If lbllog.Value <> "no" Then

        If Not IsPostBack Then
            Try
                userid = HttpContext.Current.Session("userid").ToString()
                islabor = HttpContext.Current.Session("islabor").ToString()
                appstr = HttpContext.Current.Session("appstr").ToString()
                isplanner = HttpContext.Current.Session("isplanner").ToString()
                cadm = HttpContext.Current.Session("cadm").ToString()
                lbluserid.Value = userid
                lblislabor.Value = islabor
                lblisplanner.Value = isplanner
                lblcadm.Value = cadm
                lblappstr.Value = appstr
                CheckApps(appstr)
            Catch ex As Exception

            End Try
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                'iadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                'iadd.Attributes.Add("onclick", "")
            End If
            lblgototasks.Value = "2"
            lblcleantasks.Value = "2"
            lbltaskcnt.Value = "0"
            cid = "0" 'HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            sid = Request.QueryString("sid").ToString 'HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            tasks.Open()
            'PopEq(sid)

            jump = Request.QueryString("jump").ToString

            lbljump.Value = jump
            If jump = "yes" Then
                Try
                    piccnt = Request.QueryString("piccnt").ToString
                    lblpiccnt.Value = piccnt
                Catch ex As Exception

                End Try

                eq = Request.QueryString("eqid").ToString
                lbleqid.Value = eq
                If eq <> "" Then
                    lbltaskcnt.Value = "2"
                    Try
                        'catch is temp
                        typ = Request.QueryString("typ").ToString
                        lbltyp.Value = typ
                        If typ <> "no" Then
                            fuid = Request.QueryString("fu").ToString
                            lblfuid.Value = fuid
                            coid = Request.QueryString("co").ToString
                            lblcoid.Value = coid
                        End If
                    Catch ex As Exception

                    End Try
                    pmid = Request.QueryString("pmid").ToString
                    lblpmid.Value = pmid
                    Dim cndate As Date = tasks.CNOW
                    Dim qtr, ndqtr As Integer
                    Try
                        'sql = "select datepart(qq, cast('" & cndate & "' as datetime))"
                        'qtr = tasks.Scalar(sql)
                        'sql = "select 'qtr' = (case when nextdate is not null then datepart(qq, nextdate) else 0 end) from pm where pmid = '" & pmid & "'"
                        'ndqtr = tasks.Scalar(sql)
                        'If ndqtr = 0 Or ndqtr <> qtr Then
                        'jts.Attributes.Add("class", "details")
                        'End If
                    Catch ex As Exception

                    End Try

                    Try
                        'ddeq.SelectedValue = eq
                        PopEqDesc(eq)
                        CheckTasks(eq)
                    Catch ex As Exception
                        lbltaskcnt.Value = "0"
                    End Try
                    lblcleantasks.Value = "0"
                    CleanTasks()
                End If
            Else
                'jts.Attributes.Add("class", "details")
                lbltaskcnt.Value = "0"
            End If

            tasks.Dispose()
        Else
            If Request.Form("lblsubmit") = "new" Then
                tasks.Open()
                eqid = lbleqid.Value
                If eqid <> "" Then
                    CheckEq(eqid)
                End If
                tasks.Dispose()
            ElseIf Request.Form("lblsubmit") = "checkeq" Then
                tasks.Open()
                eqid = lbleqid.Value
                Dim tst As String = lbleq.Value
                If eqid <> "" Then
                    CheckEq2()
                End If
                tasks.Dispose()
            End If

            tdeq.InnerHtml = lbleq.Value
            appstr = lblappstr.Value
            CheckApps(appstr)
        End If
        'End If
    End Sub
    Private Sub CheckApps(ByVal appstr As String)
        Dim apparr() As String = appstr.Split(",")
        Dim o As Integer = 1
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                Select Case apparr(i)
                    Case "sch"
                        o = 0
                End Select
            Next
        Else
            o = 0
        End If
        If o <> 0 Then
            'jts.Attributes.Add("class", "details")
        End If
    End Sub

    Private Sub PopEqDesc(ByVal eqid As String)
        'ddeq.SelectedValue = eqid
        Dim eqnum As String
        dt = "equipment"
        df = "eqdesc"
        filt = " where eqid = '" & eqid & "'"
        sql = "select * from equipment where eqid = '" & eqid & "'"
        dr = tasks.GetRdrData(sql)
        If dr.Read Then
            tddesc.InnerHtml = dr.Item("eqdesc").ToString
            eqnum = dr.Item("eqnum").ToString
            tdeq.InnerHtml = eqnum
            lbleq.Value = eqnum
            'tdloc.InnerHtml = dr.Item("location").ToString
            'tdspl.InnerHtml = dr.Item("spl").ToString
            lblmod.Value = dr.Item("hasmod").ToString
        End If
        dr.Close()
        If lblmod.Value = "0" Then
            'iw.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov65", "PMGetPMMan.aspx.vb") & "', ABOVE, LEFT)")
            'iw.Attributes.Add("onmouseout", "return nd()")
            'iw.Attributes.Add("onclick", "checkup();")
            'iw.Attributes.Add("src", "../images/appbuttons/minibuttons/gwarning.gif")
        ElseIf lblmod.Value = "1" Then
            'iw.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov66", "PMGetPMMan.aspx.vb") & "', ABOVE, LEFT)")
            'iw.Attributes.Add("onmouseout", "return nd()")
            'iw.Attributes.Add("onclick", "checkup();")
            'iw.Attributes.Add("src", "../images/appbuttons/minibuttons/rwarning.gif")
        End If
    End Sub




    Private Sub CheckEq2()
        eq = lbleqid.Value
        'tasks.Open()
        PopEqDesc(eq)
        CheckTasks(eq)
        lblcleantasks.Value = "0"
        CleanTasks()
        'tasks.Dispose()
        lbltaskcnt.Value = "0"
    End Sub

    Private Sub CleanTasks()
        Dim tskchk As String = lblcleantasks.Value
        If tskchk = "0" Then
            lblcleantasks.Value = "1"
            lblgototasks.Value = "1"
        End If
    End Sub
    Private Sub GetEqDesc(ByVal eq As String)
        dt = "equipment"
        df = "eqdesc, pmtyp"
        filt = " where eqid = '" & eq & "'"
        dr = tasks.GetDesc(dt, df, filt)
        If dr.Read Then
            'lbleqdesc.Text = dr.Item("eqdesc").ToString
        End If
        dr.Close()
    End Sub

    Private Sub CheckTasks(ByVal eq As String)
        sql = "select count(*) from pmmax where eqid = '" & eq & "'"
        Dim cnt As Integer
        cnt = tasks.Scalar(sql)
        isplanner = lblisplanner.Value
        islabor = lblislabor.Value
        cadm = lblcadm.Value
        If cnt = 0 Then
            lbltaskcnt.Value = "0"
            lblgototasks.Value = "0"
            'iadd.Attributes.Add("class", "view")
            'iw.Attributes.Add("class", "details")
            'Dim strMessage As String = tmod.getmsg("cdstr336", "PMGetPMMan.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'CheckEq(eq)

            If islabor = "1" Then
                If cadm = "0" And isplanner = "0" Then
                    Dim strMessage As String = "No Records Found for Current User"

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    'iadd.Attributes.Add("class", "view")
                    'iw.Attributes.Add("class", "details")
                    Dim strMessage As String = tmod.getmsg("cdstr374", "PMGetPMMan.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If

            Else

            End If
        Else
            If lbljump.Value = "yes" Then
                lbltaskcnt.Value = "1"
                If lblpmid.Value = "0" Then
                    lbltaskcnt.Value = "0"
                End If
                If lbltyp.Value <> "no" Then
                    lbltaskcnt.Value = "2"
                End If
            Else
                lbltaskcnt.Value = "0"
            End If
            lblgototasks.Value = "1"
            'iadd.Attributes.Add("class", "details")
            'iw.Attributes.Add("class", "view")
        End If
    End Sub
    Private Sub CheckEq(ByVal eq As String)
        sql = "select count(*) from pmmax where eqid = '" & eq & "'"
        Dim errcnt As Integer
        Dim pmcnt As Integer
        errcnt = tasks.Scalar(sql)
        lblerr.Value = errcnt
        If errcnt <> 0 Then
            sql = "select count(*) from pmtasks where eqid = '" & eq & "'"
            errcnt = tasks.Scalar(sql)
            lblerr.Value = errcnt
            sql = "select count(*) from pm where eqid = '" & eq & "'"
            pmcnt = tasks.Scalar(sql)
            If pmcnt = 0 Then
                If errcnt <> 0 Then
                    sql = "usp_makenewpm '" & eq & "'"
                    errcnt = tasks.Scalar(sql)
                    If errcnt <> 0 Then
                        lblgototasks.Value = "1"
                        'iadd.Attributes.Add("class", "details")
                        'iw.Attributes.Add("class", "view")
                    Else
                        lblgototasks.Value = "0"
                        'iadd.Attributes.Add("class", "view")
                        'iw.Attributes.Add("class", "details")
                    End If
                Else
                    lblgototasks.Value = "0"
                    'iadd.Attributes.Add("class", "view")
                    'iw.Attributes.Add("class", "details")
                End If
            Else
                'Dim strMessage As String = tmod.getmsg("cdstr337", "PMGetPMMan.aspx.vb")

                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                CheckEq2()
            End If
        Else
            lblgototasks.Value = "0"
            'iadd.Attributes.Add("class", "view")
            'iw.Attributes.Add("class", "details")
        End If
    End Sub











    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang638.Text = axlabs.GetASPXPage("PMGetPMMan.aspx", "lang638")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            'iadd.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMGetPMMan.aspx", "iadd") & "', ABOVE, RIGHT)")
            'iadd.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'iw.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMGetPMMan.aspx", "iw") & "', ABOVE, LEFT)")
            'iw.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'ovid75.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMGetPMMan.aspx", "ovid75") & "', ABOVE, LEFT)")
            'ovid75.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'ovid76.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMGetPMMan.aspx", "ovid76") & "', ABOVE, LEFT)")
            'ovid76.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'ovid77.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMGetPMMan.aspx", "ovid77") & "', ABOVE, LEFT)")
            'ovid77.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class