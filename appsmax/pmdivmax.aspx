﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmdivmax.aspx.vb" Inherits="lucy_r12.pmdivmax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcsse.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <!--<script language="JavaScript" type="text/javascript" src="../scripts1/PMDivManaspx.js"></script>-->
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function handlemaxtasks(ret) {
            if (ret == "ok") {
                redo();
            }
        }
        function delpm() {
            var pmid = document.getElementById("lblpmid").value;
            var eqid = document.getElementById("lbleqid").value;
            if (pmid != "") {
                var decision = confirm("Are You Sure You Want to Delete this PM Record?")
                if (decision == true) {
                    var decision2 = confirm("Cancel any Open PM Work Order associated with this PM Record?")
                    if (decision2 == true) {
                        document.getElementById("ifsave").src = "maxtasks.aspx?who=pmidwo&eqid=" + eqid + "&pmid=" + pmid;
                        //var timer = window.setTimeout("redo();", 3000);
                    }
                    else {
                        document.getElementById("ifsave").src = "maxtasks.aspx?who=pms&eqid=" + eqid + "&pmid=" + pmid;
                        //var timer = window.setTimeout("redo();", 3000);
                    }
                }
                else {
                    alert("Action Cancelled")
                }

            }
            
        }
        function redo() {
            window.parent.handlepmdel();
        }
        var popwin = "directories=0,height=500,width=800,location=0,menubar=0,resizable=0,status=0,toolbar=0,scrollbars=1";
        function getsk() {
            wo = document.getElementById("lblwo").value;
            sid = document.getElementById("lblsid").value;
            pmid = document.getElementById("lblpmid").value;
            //var stat = document.getElementById("lblstat").value;
            //var wt = document.getElementById("lblwt").value;
            //if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
            //alert(wo)
                var eReturn = window.showModalDialog("../labor/labskillsdialog.aspx?who=pmmax&sid=" + sid + "&wo=" + wo + "&pmid=" + pmid, "", "dialogHeight:500px; dialogWidth:360px; resizable=yes");
                if (eReturn) {
                    if (eReturn != "") {
                        var ret = eReturn.split("~")

                        //document.getElementById(who).value = "yes";
                        document.getElementById("tdskill").innerHTML = ret[1];
                        document.getElementById("lblskill").value = ret[1];
                        document.getElementById("lblskillid").value = ret[0];

                    }
                }
            //}
        }
        function getjplans() {
            var wo = document.getElementById("lblwo").value;
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../appsrt/JobPlanDialog.aspx?jump=yes&typ=wo&wo=" + wo + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:750px; resizable=yes");
            if (eReturn) {

                var retarr = eReturn.split(",")
                document.getElementById("lbljpid").value = retarr[0];
                document.getElementById("lbljpnum").value = retarr[1];
                document.getElementById("tdjp").innerHTML = retarr[1];
                if (retarr[0] != "") {
                    //document.getElementById("lblsubmit").value="addplan";
                    //document.getElementById("form1").submit();
                }
            }

        }
        function gotojp() {
            //var stat = document.getElementById("lblstat").value;
            //if (stat != "COMP" && stat != "CAN") {
                //var dis = document.getElementById("lbldis").value;
                //if (dis == "0" || dis == "") {
                    var jp = document.getElementById("lbljpid").value;
                    if (jp == "") {
                        jp = "0"
                    }
                    window.parent.parent.handlejp(jp);
                //}
            //}
        }
        function getwjplan() {
            var jpid = document.getElementById("lbljpid").value;
            if (jpid != "") {
                window.parent.handleswjplan(jpid);
            }
        }
        function printwo() {
            var pm = document.getElementById("lblpmid").value;
            var wo = document.getElementById("lblwo").value;
            if (wo == "") {
                alert("No Work Order Assigned for this PM")
            }
            else {
                //window.parent.setref();
                getdocs();
                window.open("../appswo/woprint.aspx?typ=PMMAX&pm=" + pm + "&wo=" + wo, "repWin", popwin);
            }
        }
        function printpm() {
            //window.parent.setref();
            getdocs();
            var pmid = document.getElementById("lblpmid").value;
            var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
            window.open("printpmmax.aspx?pmid=" + pmid, "repWin", popwin);
        }

        function getdocs() {
            var cbs = document.getElementById("lbldocs").value;
            var cbsarr = cbs.split(";")
            for (var i = 0; i < cbsarr.length; i++) {
                var cbstr = cbsarr[i];
                //alert(cbstr)
                if (cbstr != "") {
                    var cbstrarr = cbstr.split("~")
                    var fnstr = cbstrarr[1]
                    var docstr = cbstrarr[0]
                    OpenFile(fnstr, docstr);
                }
            }
        }
        function OpenFile(fn, doc) {
            //window.parent.setref();
            window.open("../appsopt/doc.aspx?file=" + fn + "&type=" + doc)
        }
        function checkcomp() {
            var wo = document.getElementById("lblwo").value;
            var next = document.getElementById("lblnext").value;
            if (next != "") {
                if (wo != "") {
                    document.getElementById("lblsubmit").value = "checkcomp";
                    document.getElementById("form1").submit();
                }
                else {
                    alert("No PM Work Order Created")
                }
            }
            else {
                alert("No Next Date Selected for this PM")
            }
        }
        function compalert(malert) {
            var marr = malert.split(",");
            var mmsg;
            var hasflg = 0;
            mmsg = "This PM has ";
            for (i = 0; i < marr.length; i++) {
                if (marr[i] == "m") {
                    if (mmsg == "This PM has ") {
                        mmsg = mmsg + "Measurements";
                    }
                    else {
                        mmsg = mmsg + ", Measurements";
                    }
                }
                else if (marr[i] == "h") {
                    if (mmsg == "This PM has ") {
                        mmsg = mmsg + "Task Times";
                    }
                    else {
                        mmsg = mmsg + ", Task Times";
                    }
                }
                else if (marr[i] == "l") {
                    if (mmsg == "This PM has ") {
                        mmsg = mmsg + "Labor Times";
                    }
                    else {
                        mmsg = mmsg + ", Labor Times";
                    }
                }
                else if (marr[i] == "d") {
                    if (mmsg == "This PM has ") {
                        mmsg = mmsg + "Down Times";
                    }
                    else {
                        mmsg = mmsg + ", Down Times";
                    }
                }
            }
            mmsg += " that have not been recorded\nDo you wish to Continue?";
            var conf = confirm(mmsg)
            if (conf == true) {
                window.parent.setref();
                document.getElementById("lblmalert").value = "ok";
                document.getElementById("lblsubmit").value = "checkcomp"
                document.getElementById("form1").submit();
            }
            else {
                document.getElementById("lblmalert").value = "";
                alert("Action Cancelled")
            }
        }
        function checkit() {

            var pmid = document.getElementById("lblpmid").value;
            var pmhid = document.getElementById("lblpmhid").value;
            var eqid = document.getElementById("lbleqid").value;
            var wo = document.getElementById("txtwonum").innerHTML;
            var jpid = document.getElementById("lbljpid").value;

            if (pmid != "") {
                window.parent.updatepmid(pmid, pmhid, eqid, wo, jpid);
            }

            var malert = document.getElementById("lblmalert").value;
            if (malert != "") {
                compalert(malert);
            }

            var alertstr = document.getElementById("lblalert").value;
            document.getElementById("lblalert").value = "";
            if (alertstr == "1" || alertstr == "2") {
                adjalert(alert);
            }
            else if (alertstr == "3") {
                document.getElementById("lblsubmit").value = "comp";
                document.getElementById("form1").submit();
            }
            else if (alertstr == "4") {
                alert("Your New PM Next Date is in the past\n Please Adjust if you believe this is incorrect")
                document.getElementById("lblsubmit").value = "comp";
                document.getElementById("form1").submit();
            }
            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                window.parent.doref();
            }
            else {
                window.parent.setref();
            }
        }
        function getleadsched() {
            var wo = document.getElementById("lblwo").value;
            var sid = document.getElementById("lblsid").value;
            var pmid = document.getElementById("lblpmid").value;
            var typ;
            if (wo != "") {
                typ = "wo";
            }
            else {
                typ = "pm";
            }
            //alert(wo)
            var issched = document.getElementById("lblissched").value;
            var eReturn = window.showModalDialog("../appswo/womlabordialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&issched=" + issched + "&pmid=" + pmid, "", "dialogHeight:520px; dialogWidth:820px; resizable=yes");
            if (eReturn) {
                //alert(eReturn)
                document.getElementById("lblsubmit").value = "refresh1";
                document.getElementById("form1").submit();
            }
        }
        function getsuper(typ) {
            var sched = document.getElementById("lblusesched").value;
            var issched = document.getElementById("lblissched").value;
            var skill = document.getElementById("lblskillid").value;
            var ro = document.getElementById("lblro").value;
            wo = document.getElementById("lblwo").value;
            //alert(wo)
            sid = document.getElementById("lblsid").value;
            pmid = document.getElementById("lblpmid").value;
            //alert(typ + "," + sched + "," + issched)
            //&& sched == "yes" && issched == "1"
            if (typ == "lead") {
                getleadsched();
            }
            else {
                var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&sid=" + sid + "&wo=" + wo + "&pmid=" + pmid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
                if (eReturn) {
                    if (eReturn != "log") {
                        if (eReturn != "") {
                            var retarr = eReturn.split(",")
                            if (typ == "sup") {
                                document.getElementById("lblsupid").value = retarr[0];
                                document.getElementById("lblsup").value = retarr[1];
                                document.getElementById("txtsup").innerHTML = retarr[1];

                            }
                            else {
                                document.getElementById("lblleadid").value = retarr[0];
                                document.getElementById("lbllead").value = retarr[1];
                                document.getElementById("txtlead").innerHTML = retarr[1];

                            }
                        }
                    }
                    else {
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        function remdat() {
            document.getElementById("txtn").innerHTML = "";
            document.getElementById("lblnext").value = "";
        }
        function getcal(fld) {
            var chk = document.getElementById("lblacnt").value;
            if (chk != "0") {
                var conf = confirm("Dates for this PM have been Adjusted in the PM Scheduler\nAre you sure you want to continue?")
                if (conf == true) {
                    window.parent.setref();
                    var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:300px; dialogWidth:300px; resizable=yes");
                    if (eReturn) {
                        var fldret = "txt" + fld;
                        document.getElementById(fldret).innerHTML = eReturn;
                        document.getElementById("lblschk").value = "1";
                        if (fld == "n") {
                            document.getElementById("lblnext").value = eReturn;
                            if (fld == "s") {
                                document.getElementById("lblstart").value = eReturn;
                                checknext();
                            }
                        }
                    }
                }
                else {
                    alert("Action Cancelled")
                }
            }
            else {
                window.parent.setref();
                var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:300px; dialogWidth:300px; resizable=yes");
                if (eReturn) {
                    //alert(eReturn)
                    var fldret = "txt" + fld;
                    document.getElementById(fldret).innerHTML = eReturn;
                    document.getElementById("lblschk").value = "1";
                    if (fld == "n") {
                        document.getElementById("lblnext").value = eReturn;

                    }
                    if (fld == "s") {
                        document.getElementById("lblstart").value = eReturn;
                        checknext();
                    }
                }
            }
        }
        function savetask() {
            var nxt = document.getElementById("lblnext").value;
            var freq = document.getElementById("txtfreq").value;
            var unit = document.getElementById("ddunit").value;
            if(nxt != "" && (freq == "" || unit == "Select")) {
            alert("Both a Frequency and Unit must be selected if a Next Date is specified")
            }
            else {
            document.getElementById("lblsubmit").value = "save";
            document.getElementById("form1").submit();
            }
            
        }
        function checknext() {
            var cbs = document.getElementById("cbs");
            if (cbs.checked == true) {
                var nxt = document.getElementById("lblnext").value;
                if (nxt == "") {
                    var nnxt = document.getElementById("lblstart").value;
                    document.getElementById("txtn").innerHTML = nnxt;
                    document.getElementById("lblnext").value = nnxt;
                }
            }
        }
        function gridret() {
            var eqid = document.getElementById("lbleqid").value;
            var sid = document.getElementById("lblsid").value;
            window.parent.handletasks("pmgridmax.aspx?start=yes&jump=no&eqid=" + eqid + "&sid=" + sid, "ok")
        }
        function checkdel(who) {
            var conf
            if (who == "lead") {
                conf = confirm("Are you sure you want to Delete this Lead Craft?")
            }
            else if (who == "sup") {
                conf = confirm("Are you sure you want to Delete this Supervisor?")
            }
            if (conf == true) {
                if (who == "lead") {
                    document.getElementById("lblleadid").value = ""
                    document.getElementById("lbllead").value = ""
                    document.getElementById("txtlead").innerHTML = ""
                    document.getElementById("lblsubmit").value = "save"
                    document.getElementById("form1").submit();
                }
                else if (who == "sup") {
                    document.getElementById("lblsupid").value = ""
                    document.getElementById("lblsup").value = ""
                    document.getElementById("txtsup").innerHTML = ""
                    document.getElementById("lblsubmit").value = "save"
                    document.getElementById("form1").submit();
                }
            }
            else {
                alert("Action Cancelled")
            }
        }
    </script>
</head>
<body onload="checkit();">
    <form id="form1" runat="server">
    <div>
        <table style="position: absolute; top: 0px; left: 0px" cellspacing="0" width="740"
            border="0">
            <tr class="details">
                <td class="thdrsinglft" width="26" height="24">
                    <img border="0" src="../images/appbuttons/minibuttons/pmgridhdr.gif">
                </td>
                <td class="thdrsingrt label" width="740" align="left">
                    <asp:Label ID="lang591" runat="server">PM Details</asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table cellspacing="2" width="740">
                        <tr class="tbg">
                            <td class="btmnew plainlabel" width="180">
                                PM#
                            </td>
                            <td class="btmnew plainlabel" width="350">
                                Description
                            </td>
                            <td class="btmnew plainlabel" width="120">
                                Last Date
                            </td>
                        </tr>
                        <tr>
                            <td id="pmpdm" class="plainlabel" runat="server">
                            </td>
                            <td id="pmtitle" class="plainlabel" runat="server">
                            </td>
                            <td id="pmlast" class="plainlabel" runat="server"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table cellspacing="0" width="740" border="0">
                        <tr>
                            <td width="100">
                            </td>
                            <td width="100">
                            </td>
                             <td width="20">
                            </td>
                            <td width="20">
                            </td>
                            <td width="20">
                            </td>
                            <td width="20">
                            </td>
                            <td width="20">
                            </td>
                            <td width="120">
                            </td>
                            <td width="5">
                            </td>
                            <td width="10">
                            </td>
                            <td width="10">
                            </td>
                            <td width="40">
                            </td>
                            <td width="80">
                            </td>
                            <td width="55">
                            </td>
                            <td width="40">
                            </td>
                        </tr>
                        <tr>
                            <td class="btmnew plainlabel" colspan="8">
                                <asp:Label ID="lang594" runat="server">Scheduling Information</asp:Label>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class="btmnew plainlabel" colspan="6">
                                <asp:Label ID="lang595" runat="server">Responsibility</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabel">
                                <asp:Label ID="lang596" runat="server">Start Date</asp:Label>
                            </td>
                            <td>
                                <div class="readonly100" id="txts" runat="server"></div>
                            </td>
                            <td></td>
                            <td>
                                <img onclick="getcal('s');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                    width="19" height="19">
                            </td>
                            <td class="plainlabelblue" colspan="4">
                                <asp:Label ID="lang597" runat="server">Use Start?</asp:Label><input id="cbs" onclick="checknext();"
                                    type="checkbox" runat="server" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            
                            <td class="bluelabel" colspan="3">
                                Skill
                            </td>
                            <td colspan="2">
                                <div class="readonly170" id="tdskill" runat="server"></div>
                            </td>
                            <td>
                                <img id="Img5" onclick="getsk();" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                runat="server">
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabel">
                                <asp:Label ID="lang599" runat="server">Next Date</asp:Label>
                            </td>
                            <td>
                                <div class="readonly100" id="txtn" runat="server"></div>
                            </td>
                            <td>
                            <img id="Img6" onclick="remdat();" border="0" alt="" src="../images/appbuttons/minibuttons/refreshit.gif"
                                runat="server" onmouseover="return overlib('Clear Next Date - Requires Save', ABOVE, LEFT)"
                                onmouseout="return nd()" />
                            </td>
                            <td>
                                <img onclick="getcal('n');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                    width="19" height="19" />
                            </td>
                            <td id="tdcnt" class="plainlabelblue" runat="server" colspan="4">
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            
                            <td class="bluelabel"  colspan="3">
                                PdM
                            </td>
                            <td colspan="2">
                                 <asp:dropdownlist id="ddpt" runat="server" CssClass="plainlabel" Width="174px" Height="19px"></asp:dropdownlist>
                            </td>
                            <td>
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabel">
                                <asp:Label ID="lang601" runat="server">Work Order#</asp:Label>
                            </td>
                            <td>
                                <div class="readonly100" id="txtwonum" runat="server"></div>
                            </td>
                            <td>
                                <img id="imgwoshed" onmouseover="return overlib('Add or Edit Schedule Days for this Work Order', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="getwosched('wo');" alt="" src="../images/appbuttons/minibuttons/wosched.gif"
                                    runat="server" class="details" />
                            </td>
                            <td></td>
                            <td class="plainlabelblue" colspan="3">
                                #Runs
                            </td>
                            <td>
                                <div class="readonly50" id="txtruns" runat="server"></div>
                            </td>
                            <td>
                            </td>
                           
                            <td class="bluelabel" colspan="3"><asp:Label ID="lang598" runat="server">Supervisor</asp:Label></td>
                        <td colspan="2"><div class="readonly170" id="txtsup" runat="server"></div></td>
                        <td><img onclick="getsuper('sup');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif" />
                        <img onclick="checkdel('sup');" alt="" src="../images/appbuttons/minibuttons/del.gif"
                                height="12px" width="12px" class="details" /></td>
                        </tr>
                        <tr>
                        <td class="bluelabel">Frequency</td>
                        <td><asp:TextBox ID="txtfreq" runat="server" CssClass="plainlabel" Width="100px"></asp:TextBox></td>
                        <td></td>
                        <td></td>
                        <td colspan="3" class="bluelabel">Unit</td>
                            
                            
                        <td><asp:DropDownList ID="ddunit" runat="server">
                            <asp:ListItem>Select</asp:ListItem>
                            <asp:ListItem>DAYS</asp:ListItem>
                            <asp:ListItem>WEEKS</asp:ListItem>
                            <asp:ListItem>MONTHS</asp:ListItem>
                            <asp:ListItem>YEARS</asp:ListItem>
                            </asp:DropDownList></td>
                        <td></td>    
                        
                        <td class="bluelabel"  colspan="3"><asp:Label ID="lang600" runat="server">Lead Craft</asp:Label></td>
                        <td colspan="2"><div class="readonly170" id="txtlead" runat="server"></div></td>
                        
                        <td><img onclick="getsuper('lead');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                        <img onclick="checkdel('lead');" alt="" src="../images/appbuttons/minibuttons/del.gif"
                                height="12px" width="12px" class="details" />
                                </td>
                        
                        
                        </tr>
                        <tr>
                        <td class="btmnew plainlabel" colspan="8">
                                <asp:Label ID="Label3" runat="server">Equipment\Job Plan Information</asp:Label>
                            </td>
                            
                        <td></td>
                        <td class="bluelabel" valign="top" colspan="3" rowspan="2"><asp:Label ID="Label2" runat="server">Labor Group</asp:Label></td>
                        
                        <td colspan="2" rowspan="2"><div class="readonly17045" id="divlg" runat="server">
                                </div></td>
                        
                        <td></td>
                        </tr>
                        <tr>
                        <td class="bluelabel">
                                <asp:Label ID="lbljp" runat="server">Job Plan</asp:Label>
                            </td>
                            <td colspan="4">
                            <div id="tdjp" runat="server" style="border: 1px solid #CBCCCE; height: 19px; width: 170px; background-color: #EAEDF0; font-family: Arial,MS Sans Serif; font-size: 12px; vertical-align: middle; display: table-cell; text-indent: 3px;} "></div>
                            </td>
                        <td>
                                <img id="imgplans" onmouseover="return overlib('View Existing Job Plans', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="getjplans();" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    runat="server" />
                            </td>
                            <td>
                                <img id="imgaddplan" onmouseover="return overlib('Create a New Job Plan', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="gotojp();" alt="" src="../images/appbuttons/minibuttons/addmod.gif"
                                    runat="server" />
                            </td>
                            <td>
                                <img id="imgeditplan" onmouseover="return overlib('Edit Selected Job Plan (Applies to Work Order Only)', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="getwjplan();" alt="" src="../images/appbuttons/minibuttons/pencil.gif"
                                    runat="server">
                            </td>
                            
                        <td></td>
                        <td></td>
                        
                        
                        
                        </tr>
                        <tr>
                        <td class="bluelabel">Equipment#</td>
                        <td colspan="4"><div class="readonly170" id="txteq" runat="server"></div></td>
                        
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        </tr>
                        <tr>
                        <td><img id="imgdel" onmouseover="return overlib('Delete this PM Record', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="delpm();" alt="" src="../images/appbuttons/minibuttons/del.gif"
                                    width="20" height="20" runat="server" /></td>
                            <td colspan="13" align="right">
                                <img id="imgsav" onmouseover="return overlib('Save Changes Above', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="savetask();" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                    width="20" height="20" runat="server" />
                                    
                                <img onmouseover="return overlib('Return to Navigation Grid', ABOVE, LEFT)" onmouseout="return nd()"
                                    onclick="gridret();" alt="" src="../images/appbuttons/bgbuttons/navgrid.gif"
                                    width="20" height="20" />
                                <img id="Img1" onmouseover="return overlib('View PM History', ABOVE, LEFT)" onmouseout="return nd()"
                                    onclick="gethist();" border="0" alt="" src="../images/appbuttons/minibuttons/medclock.gif"
                                    runat="server">
                                <img id="Img2" onmouseover="return overlib('View Failure Mode History', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="getfmhist();" border="0" alt="" src="../images/appbuttons/minibuttons/fmhist.gif"
                                    runat="server" class="details" /><img id="imgmeas" onmouseover="return overlib('View Measurements for this PM')"
                                        onmouseout="return nd()" onclick="getMeasDiv();" alt="" src="../images/appbuttons/minibuttons/measure.gif"
                                        width="27" height="20" runat="server" class="details" />
                                <img id="btnedittask" onmouseover="return overlib('View or Edit Pass/Fail Adjustment Levels - View Current Pass/Fail Counts', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="editadj();" border="0" alt="" src="../images/appbuttons/minibuttons/screwd.gif"
                                    runat="server" class="details" />
                                <img id="Img3" class="details" onmouseover="return overlib('Adjust Email Lead Time', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="editlead();" border="0" alt="" src="../images/appbuttons/minibuttons/emailadmin.gif"
                                    runat="server"><img onmouseover="return overlib('Add Attachments', ABOVE, LEFT)"
                                        onmouseout="return nd()" onclick="getattach();" src="../images/appbuttons/minibuttons/attach.gif">
                                <img onmouseover="return overlib('Print Current PM', ABOVE, LEFT)" onmouseout="return nd()"
                                    onclick="printpm();" src="../images/appbuttons/minibuttons/printex.gif">&nbsp;<img
                                        id="imgi2" onmouseover="return overlib('Print This PM Work Order', ABOVE, LEFT)"
                                        onmouseout="return nd()" onclick="printwo();" src="../images/appbuttons/minibuttons/woprint.gif"
                                        runat="server">
                                <img id="Img4" onmouseover="return overlib('Print This PM Work Order w\images', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="printwopic();" src="../images/appbuttons/minibuttons/printpic.gif"
                                    runat="server" class="details" />
                                <img id="imgcomp" onmouseover="return overlib('Complete this PM', ABOVE, LEFT)" onmouseout="return nd()"
                                    onclick="checkcomp();" src="../images/appbuttons/minibuttons/comp.gif" runat="server">
                            </td>
                            
                        
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="btmnew plainlabel" width="740" align="left" colspan="2">
                    <asp:Label ID="lang604" runat="server">PM Tasks</asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="width: 740px; height: 138px; overflow: auto; border-right: 1px solid black"
                        id="tdtasks" runat="server">
                    </div>
                </td>
            </tr>
        </table>
        <iframe id="ifsave" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
        <input id="lblpmid" type="hidden" runat="server" />
        <input id="lblsubmit" type="hidden" runat="server" />
        <input id="lblpmhid" type="hidden" runat="server" /><input id="lbleqid" type="hidden"
            runat="server" />
        <input id="lblnext" type="hidden" runat="server" /><input id="lblschk" type="hidden"
            runat="server" />
        <input id="lblsup" type="hidden" runat="server" />
        <input id="lbllead" type="hidden" runat="server" />
        <input id="lblskillid" type="hidden" runat="server" />
        <input id="lblacnt" type="hidden" runat="server" />
        <input id="txtsearch" type="hidden" runat="server" />
        <input id="lblwo" type="hidden" runat="server" />
        <input id="lbllog" type="hidden" runat="server" />
        <input id="lblmalert" type="hidden" runat="server" />
        <input id="lblalert" type="hidden" runat="server" />
        <input id="lbldocs" type="hidden" runat="server" />
        <input id="lblondate" type="hidden" runat="server" /><input id="lblro" type="hidden"
            runat="server" />
        <input id="lblpiccnt" type="hidden" runat="server" />
        <input id="lblusetdt" type="hidden" runat="server" />
        <input id="lblusetotal" type="hidden" runat="server" />
        <input id="lblttime" type="hidden" runat="server" />
        <input id="lbldtime" type="hidden" runat="server" />
        <input id="lblacttime" type="hidden" runat="server" />
        <input id="lblactdtime" type="hidden" runat="server" /><input type="hidden" id="lblhalert"
            runat="server" />
        <input type="hidden" id="lbldalert" runat="server" />
        <input type="hidden" id="lblsid" runat="server" />
        <input type="hidden" id="lblfslang" runat="server" />
        <input type="hidden" id="lblworuns" runat="server" />
        <input type="hidden" id="lblruns" runat="server" />
        <input type="hidden" id="lblstart" runat="server" />
        <input type="hidden" id="lblleadid" runat="server" />
        <input type="hidden" id="lblsupid" runat="server" />
        <input type="hidden" id="lblissched" runat="server" />
        <input id="lblusesched" type="hidden" runat="server" />
        <input id="lblhidejts" type="hidden" runat="server" />
        <input type="hidden" id="lblpmnum" runat="server" />
        <input type="hidden" id="lbljpid" runat="server" />
        <input type="hidden" id="lbljpnum" runat="server" />
        <input type="hidden" id="lblpmdesc" runat="server" />
       
       <input type="hidden" id="lblskill" runat="server" />
<input type="hidden" id="lblptid" runat="server" />
<input type="hidden" id="lblpretech" runat="server" />
<input type="hidden" id="lbleqnum" runat="server" />

    </div>
    </form>
</body>
</html>
