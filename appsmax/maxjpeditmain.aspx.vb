﻿Public Class maxjpeditmain
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim wonum, jid, href, sid, cid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Dim Login As String
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        If Not IsPostBack Then
            wonum = Request.QueryString("wonum").ToString
            lblwo.Value = wonum
            jid = Request.QueryString("jpid").ToString
            lbljpid.Value = jid
            href = Request.QueryString("href").ToString
            'href = Replace(href, "~", "&")
            '//var harr = href.split("~");
            '//var remwo = harr[1] + "~";
            '//href = href.replace(remwo, "")
            Dim harr() As String = href.Split("~")
            Dim remwo As String = "~" & harr(1) '& "~"
            href = href.Replace(remwo, "~wo=" & wonum)
            'href = href.Replace("~wo=", "~wo=" & wonum)
            lblhref.Value = href
            '"wojpedit.aspx?jpid=" + jpid + "&wonum=" + wo + "&href=" + href + "&ro=" + ro;
            ifinv.Attributes.Add("src", "maxjpedit.aspx?jpid=" + jid + "&wonum=" + wonum + "&href=" + href)
        End If
    End Sub

End Class