﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="maxjpedit.aspx.vb" Inherits="lucy_r12.maxjpedit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script language="JavaScript" src="../scripts1/wojpeditaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
        <script language="javascript" type="text/javascript">
            function getskill(freqid, pmtskid, sid, fld, wo) {
                var eReturn = window.showModalDialog("../appsrt/jpfreqlistdialog.aspx?pmtskid=" + pmtskid + "&fld=" + fld + "&sid=" + sid + "&wo=" + wo, "", "dialogHeight:460px; dialogWidth:460px; resizable=yes");
                if (eReturn) {
                    //alert(freqid)
                    var ret = eReturn.split(",");
                    document.getElementById(freqid).innerHTML = ret[1];
                    document.getElementById("lblskillidret").value = ret[0];
                }

            }
            function handlereturn() {
                //var href = document.getElementById("lblhref").value;
                window.parent.handlereturn();
            }
            function checkit() {
                var retchk = document.getElementById("lblsubmit").value;
                if (retchk == "del") {
                    handlereturn();
                }
                else {
                    document.getElementById("lblfailret").Value = "0";
                    //alert(document.getElementById("lbldboxid").value)
                    try {
                        var dbid = document.getElementById("lbldboxid").value;
                        if (dbid != "") {
                            var desc = document.getElementById("lbltaskdesc").value;
                            document.getElementById(dbid).value = desc;
                            //document.getElementById("lbldboxid").value = "";
                            //document.getElementById("lbltaskdesc").value = "";
                        }
                        var qbid = document.getElementById("lblqboxid").value;
                        if (qbid != "") {
                            var qdesc = document.getElementById("lblgridqty").value;
                            document.getElementById(qbid).value = qdesc;
                            //document.getElementById("lblqboxid").value = "";
                            //document.getElementById("lblgridqty").value = "";
                        }
                        var mbid = document.getElementById("lblmboxid").value;
                        if (mbid != "") {
                            var mdesc = document.getElementById("lblgridmins").value;
                            document.getElementById(mbid).value = mdesc;
                            //document.getElementById("lblmboxid").value = "";
                            //document.getElementById("lblgridmins").value = "";
                        }
                        var sbid = document.getElementById("lblsboxid").value;
                        if (sbid != "") {
                            var sdesc = document.getElementById("lblgridskillid").value;
                            document.getElementById(sbid).value = sdesc;
                            //document.getElementById("lblsboxid").value = "";
                            //document.getElementById("lblgridskillid").value = "";
                        }
                        //alert(document.getElementById("lblret").value)
                    }
                    catch (err) {

                    }

                    window.setTimeout("setref();", 1205000);
                    var to = document.getElementById("lbltool").value;
                    var pa = document.getElementById("lblpart").value;
                    var lu = document.getElementById("lbllube").value;
                    var no = document.getElementById("lblnote").value;
                    var me = document.getElementById("lblmeas").value;
                    if (to == "no") {

                    }
                    else {
                        document.getElementById("lbltool").value = "no";
                        GetToolDiv();
                    }
                    if (pa == "no") {

                    }
                    else {
                        document.getElementById("lblpart").value = "no";
                        //alert(pa)
                        GetPartDiv();
                    }
                    if (lu == "no") {

                    }
                    else {
                        document.getElementById("lbllube").value = "no";
                        GetLubeDiv();
                    }
                    if (me == "no") {

                    }
                    else {
                        document.getElementById("lblmeas").value = "no";
                        GetMeasDiv();
                    }

                }
            }
            function delpm() {
                var conf = confirm("Are You Sure You Want to Delete This Job Plan?")
                if (conf == true) {
                    document.getElementById("lblsubmit").value = "delpm";
                    document.getElementById("form1").submit();
                }
                else {
                    alert("Action Cancelled")
                }

            }
        </script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" runat="server">
    <div>
    <table id="scrollmenu" style="Z-INDEX: 1; POSITION: absolute; TOP: 4px; LEFT: 4px">
            <tr>
            <td align="right"><img src="../images/appbuttons/minibuttons/del.gif" alt="" onclick="delpm();" /></td>
            </tr>
				<tr>
					<td class="thdrsing label" style="HEIGHT: 24px"><asp:Label id="lang1468" runat="server">Work Order Job Plan Tasks</asp:Label></td>
				</tr>
				<tr>
					<td align="right"><IMG onclick="addtask();" id="bgbaddtask" runat="server" src="../images/appbuttons/bgbuttons/addtask.gif">&nbsp;<IMG id="btnreturn" onclick="handlereturn();" height="19" src="../images/appbuttons/bgbuttons/return.gif"
							width="69" runat="server"></td>
				</tr>
				<tr>
					<td><asp:datagrid id="dgtasks" runat="server" BorderWidth="0" CellSpacing="1" CellPadding="3" AutoGenerateColumns="False"
							AllowSorting="True">
							<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
							<ItemStyle CssClass="ptransrow"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="80px" Height="24px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:imagebutton id="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:imagebutton>
										<img id="ibast" runat="server" src="../images/appbuttons/minibuttons/subtask.gif" onclick="getsgrid();"
											width="20" height="20">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Task#">
									<HeaderStyle CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblta runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id=lblt runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>' width="20px">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Task Description">
									<HeaderStyle Width="280px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label3 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>' Width="270px">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id=txtdesc runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>' Height="70px" TextMode="MultiLine" width="260px" MaxLength="1000">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Failure Mode">
									<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.failuremode") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<TABLE>
											<TR>
												<TD class="bluelabel" align="center" width="160"><U>
														<asp:Label id="lang1469" runat="server">Component Failure Modes</asp:Label></U></TD>
												<TD width="22"></TD>
												<TD class="bluelabel" align="center" width="160"><U>
														<asp:Label id="lang1470" runat="server">Selected Failure Modes</asp:Label></U></TD>
											</TR>
											<TR>
												<TD align="center">
													<asp:listbox id=lbCompFM runat="server" CssClass="plainlabel" Width="150px" Height="70px" SelectionMode="Multiple" DataSource='<%# PopulateCompFM(Container.DataItem("comid"),Container.DataItem("wojtid")) %>' DataTextField="failuremode" DataValueField="failid">
													</asp:listbox></TD>
												<TD>
													<IMG onclick="getfm();" alt="" src="../images/appbuttons/minibuttons/magnifier.gif" border="0"><br>
													<asp:imagebutton id="ibToTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"
														CommandName="ToTask2"></asp:imagebutton><BR>
													<asp:imagebutton id="ibFromTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"
														CommandName="RemTask"></asp:imagebutton></TD>
												<TD align="center">
													<asp:listbox id=lbfaillist runat="server" CssClass="plainlabel" Width="150px" Height="70px" SelectionMode="Multiple" DataSource='<%# PopulateTaskFM(Container.DataItem("comid"),Container.DataItem("wojtid")) %>' DataTextField="failuremode" DataValueField="failid">
													</asp:listbox></TD>
											</TR>
										</TABLE>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Skill Required">
                                <HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblskill" Width="230px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                    </asp:Label>
                                    <img id="imgskill" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                </EditItemTemplate>
                            </asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Skill Qty">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>' ID="Label10" NAME="Label8">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txtqty" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Skill Min Ea">
									<HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>' ID="Label12" >
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txttr" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Down Time" Visible="False">
									<HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rdt") %>' ID="Label2" >
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txtdt" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.rdt") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Parts/Tools/Lubes/Meas">
									<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:imagebutton id="Imagebutton23" runat="server" ImageUrl="../images/appbuttons/minibuttons/parttrans.gif"
											ToolTip="Add Parts" CommandName="Part"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton23a" runat="server" ImageUrl="../images/appbuttons/minibuttons/tooltrans.gif"
											ToolTip="Add Tools" CommandName="Tool"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton23b" runat="server" ImageUrl="../images/appbuttons/minibuttons/lubetrans.gif"
											ToolTip="Add Lubricants" CommandName="Lube"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton23c" runat="server" ImageUrl="../images/appbuttons/minibuttons/measure.gif"
											ToolTip="Add Measurements" CommandName="Meas"></asp:imagebutton>
									</ItemTemplate>
                                     <EditItemTemplate>
                                </EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="pmtskid">
									<ItemTemplate>
										<asp:Label id="lbltida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wojtid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblttid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wojtid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Delete">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:ImageButton id="ibDel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="pmtskid">
									<ItemTemplate>
										<asp:Label id="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblpmtskid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
			<input id="lblwo" type="hidden" runat="server"> <input id="lbljpid" type="hidden" runat="server">
			<input id="lblcoid" type="hidden" runat="server"> <input id="lblsid" type="hidden" runat="server">
			<input id="lblcid" type="hidden" runat="server"> <input id="lblfailchk" type="hidden" runat="server">
			<input id="lblitemindex" type="hidden" runat="server"> <input id="lblcurrtask" type="hidden" runat="server">
			<input id="lbleditmode" type="hidden" runat="server"> <input id="lbloldtasknum" type="hidden" runat="server">
			<input id="lbltool" type="hidden" runat="server"> <input id="lblpmtid" type="hidden" runat="server">
			<input id="lbltasknum" type="hidden" runat="server"> <input id="lblpart" type="hidden" runat="server">
			<input id="lbllube" type="hidden" runat="server"> <input id="lblmeas" type="hidden" runat="server">
			<input id="lblnote" type="hidden" runat="server"><input id="lblsubmit" type="hidden" runat="server">
			<input id="xCoord" type="hidden" name="xCoord" runat="server"> <input id="yCoord" type="hidden" name="yCoord" runat="server">
			<input id="lblhref" type="hidden" runat="server"> <input id="lblsskills" type="hidden" runat="server">
			<input id="lblro" type="hidden" runat="server"> <input id="lbltaskdesc" type="hidden" name="lbltaskdesc" runat="server">
			<input id="lblgridskill" type="hidden" name="lblgridskill" runat="server"> <input id="lblgridskillid" type="hidden" name="lblgridskillid" runat="server">
			<input id="lblgridqty" type="hidden" name="lblgridqty" runat="server"> <input id="lblgridmins" type="hidden" name="lblgridmins" runat="server">
			<input id="lbldboxid" type="hidden" name="lbldboxid" runat="server"> <input id="lblsboxid" type="hidden" name="lblsboxid" runat="server">
			<input id="lblqboxid" type="hidden" name="lblqboxid" runat="server"> <input id="lblmboxid" type="hidden" name="lblmdboxid" runat="server">
			<input id="lblskillall" type="hidden" name="lblskillall" runat="server"> <input id="lblskillallid" type="hidden" name="lblskillallid" runat="server">
			<input id="lblfailret" type="hidden" name="lblfailret" runat="server"> <input type="hidden" id="lblfslang" runat="server" />
            <input type="hidden" id="lblskillidret" runat="server" />
    </div>
    </form>
</body>
</html>
