﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmgridmax.aspx.vb" Inherits="lucy_r12.pmgridmax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function checkcnt() {
            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                window.parent.doref();
            }
            else {
                window.parent.setref();
            }

            var app = document.getElementById("appchk").value;
            if (app == "switch") window.parent.handleapp(app);

            var cnt = document.getElementById("taskcnt").value;
            if (cnt != "0") {
                window.parent.handlecnt(cnt);
            }

        }
        function setref() {
            //document.getElementById("form1").submit();setref
            //window.location="/laipm3/NewLogin.aspx"
            //window.parent.handlelogout();
            window.parent.setref();
            window.setTimeout("setref();", 1205000);
        }
    </script>
</head>
<body class="tbg" onload="checkcnt();">
    <form id="form1" runat="server">
    <div>
        <table style="position: absolute; top: 0px; left: 0px" width="660" cellspacing="0">
            <tr>
                <td class="thdrsinglft" width="26" height="22">
                    <img src="../images/appbuttons/minibuttons/pmgridhdr.gif" border="0">
                </td>
                <td class="thdrsingrt label" align="left" width="634">
                    <asp:Label ID="lang639" runat="server">Current Records</asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <img src="../images/appbuttons/minibuttons/2PX.gif">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="width: 680px; height: 392px; overflow: auto">
                        <asp:Repeater ID="rptrtasks" runat="server">
                            <HeaderTemplate>
                                <table width="660" cellspacing="2">
                                    <tr class="tbg">
                                        <td class="thdrsingg plainlabel" width="150" height="26">
                                            PdM
                                        </td>
                                        <td class="thdrsingg plainlabel" width="320">
                                            PM
                                        </td>
                                        <td class="details" width="100">
                                            <asp:Label ID="lang640" runat="server">Last Date</asp:Label>
                                        </td>
                                        <td class="thdrsingg plainlabel" width="90">
                                            <asp:Label ID="lang641" runat="server">Next Date</asp:Label>
                                        </td>
                                        <td class="thdrsingg plainlabel" width="30">
                                            <img src="../images/appbuttons/minibuttons/gwarningnbg.gif">
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="tbg">
                                    <td class="plainlabel" height="20">
                                        &nbsp;
                                        <asp:Label ID="lblpdm" Text='<%# DataBinder.Eval(Container.DataItem,"pmd")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:LinkButton ID="lbltn" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"pm")%>'
                                            runat="server">
                                        </asp:LinkButton>
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lbllast" Text='<%# DataBinder.Eval(Container.DataItem,"lastdate")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label ID="lblnext" Text='<%# DataBinder.Eval(Container.DataItem,"nextdate")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td align="center">
                                        <img id="iwi" onmouseover="return overlib('Status Check Icon', ABOVE, LEFT)" onmouseout="return nd()"
                                            src="../images/appbuttons/minibuttons/gwarning.gif" runat="server">
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lblpmiditem" Text='<%# DataBinder.Eval(Container.DataItem,"pmid")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="transrowblue">
                                    <td class="plainlabel transrowblue" height="20"
                                    >
                                        &nbsp;
                                        <asp:Label ID="lblpdmalt" Text='<%# DataBinder.Eval(Container.DataItem,"pmd")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        <asp:LinkButton ID="lbltnalt" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"pm")%>'
                                            runat="server">
                                        </asp:LinkButton>
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lbllastalt" Text='<%# DataBinder.Eval(Container.DataItem,"lastdate")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        <asp:Label ID="lblnextalt" Text='<%# DataBinder.Eval(Container.DataItem,"nextdate")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td align="center" class="transrowblue">
                                        <img id="iwa" onmouseover="return overlib('Status Check Icon', ABOVE, LEFT)" onmouseout="return nd()"
                                            src="../images/appbuttons/minibuttons/gwarning.gif" runat="server">
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lblpmidalt" Text='<%# DataBinder.Eval(Container.DataItem,"pmid")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </td>
            </tr>
        </table>
        <input id="lblsid" type="hidden" name="lblsid" runat="server" /><input id="lbltaskid"
            type="hidden" name="lbltaskid" runat="server" />
        <input id="lblcid" type="hidden" name="lblcid" runat="server" />
        <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server" />
        <input id="lbldid" type="hidden" name="lbldid" runat="server" />
        <input id="lblclid" type="hidden" name="lblclid" runat="server" />
        <input id="lbleqid" type="hidden" name="lbleqid" runat="server" />
        <input id="lblfuid" type="hidden" name="lblfuid" runat="server" />
        <input id="lblfilt" type="hidden" name="lblfilt" runat="server" />
        <input id="lblchk" type="hidden" name="lblchk" runat="server" />
        <input id="taskcnt" type="hidden" runat="server" name="taskcnt"><input id="appchk"
            type="hidden" name="appchk" runat="server" />
        <input id="lblcoid" type="hidden" runat="server" name="lblcoid"><input type="hidden"
            id="lblpmid" runat="server" />
        <input type="hidden" id="lbltyp" runat="server" />
        <input type="hidden" id="lbllog" runat="server" />
        <input type="hidden" id="lblpiccnt" runat="server" />
        <input type="hidden" id="lblfslang" runat="server" />
    </div>
    </form>
</body>
</html>
