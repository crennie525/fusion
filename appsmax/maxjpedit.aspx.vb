﻿Imports System.Data.SqlClient
Public Class maxjpedit
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim ds As DataSet
    Dim dslev As DataSet
    Dim dr As SqlDataReader
    Dim gtasks As New Utilities
    Dim sql As String
    Dim wonum, jid, sid, coid, cid, href, ro, rostr As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        GetDGLangs()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Dim Login As String
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        If Not IsPostBack Then
            wonum = Request.QueryString("wonum").ToString
            lblwo.Value = wonum
            jid = Request.QueryString("jpid").ToString
            lbljpid.Value = jid
            href = Request.QueryString("href").ToString
            'href = Replace(href, "~", "&")
            lblhref.Value = href

            lbleditmode.Value = "0"
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            cid = HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            gtasks.Open()
            CheckStuff()
            BindGrid(jid, wonum)
            gtasks.Dispose()
        Else
            If Request.Form("lblsubmit") = "addtask" Then
                lblsubmit.Value = ""
                gtasks.Open()
                AddTask()
                gtasks.Dispose()
            ElseIf Request.Form("lblsubmit") = "upfail" Then
                lblsubmit.Value = ""
                dgtasks.EditItemIndex = lblitemindex.Value
                gtasks.Open()
                jid = lbljpid.Value
                wonum = lblwo.Value
                BindGrid(jid, wonum)
                gtasks.Dispose()
            ElseIf Request.Form("lblsubmit") = "delpm" Then
                lblsubmit.Value = ""
                gtasks.Open()
                delpm()
                gtasks.Dispose()
                lblsubmit.Value = "del"
            End If
        End If
    End Sub
    Private Sub delpm()
        Dim wo As String = lblwo.Value
        sql = "delete from wojobplans where wonum = '" & wo & "';delete from wojplubes where wonum = '" & wo & "';" _
                    + "delete from wojobtasks where wonum = '" & wo & "';delete from wojpfailmodes where wonum = '" & wo & "';" _
                    + "delete from wojpparts where wonum = '" & wo & "';delete from wojptools where wonum = '" & wo & "';" _
                    + "delete from wojppartsout where wonum = '" & wo & "';delete from wojptoolsout where wonum = '" & wo & "';" _
                    + "delete from wojplubesout where wonum = '" & wo & "';delete from wojptools where wonum = '" & wo & "';" _
                    + "delete from wojptaskmeasdetails where wonum = '" & wo & "';delete from wojptaskmeasoutput where wonum = '" & wo & "';" _
                    + "update workorder set estjplabhrs = 0, estjpmatcost = 0, estjplabcost = 0, estjptoolcost = 0, estjplubecost = 0, " _
                    + "jpid = null, jpref = null where wonum = '" & wo & "'"
        gtasks.Update(sql)

    End Sub
    Private Sub CheckStuff()
        sid = lblsid.Value
        Dim scnt As Integer
        sql = "select count(*) from pmsiteskills where siteid = '" & sid & "'"
        scnt = gtasks.Scalar(sql)
        If scnt > 0 Then
            lblsskills.Value = "yes"
        Else
            lblsskills.Value = "no"
        End If

    End Sub

    Private Sub BindGrid(Optional ByVal jid As String = "0", Optional ByVal wonum As String = "0")
        If jid <> "0" Then
            sql = "select t.*, isnull(p.comid, '0') as comid from wojobtasks t " _
            + "left join wojobplans p on p.jpid = t.jpid and p.wonum = '" & wonum & "' " _
            + "where t.jpid = '" & jid & "' and t.wonum = '" & wonum & "' and subtask = 0 order by t.tasknum"
        Else
            sql = "select t.* from wojobtasks t where t.jpid = '0'"
        End If

        ds = gtasks.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        'Try
        dgtasks.DataSource = dv
        dgtasks.DataBind()
        'Catch ex As Exception

        'End Try
    End Sub
    Public Function PopFail() As DataSet
        Dim chk As String = lblfailchk.Value
        Dim scnt As Integer
        sid = lblsid.Value
        coid = lblcoid.Value
        If chk = "" Then
            sql = "select count(*) from pmSiteFM where siteid = '" & sid & "'"
            scnt = gtasks.Scalar(sql)
            If scnt = 0 Then
                lblfailchk.Value = "open"
                chk = "open"
            Else
                lblfailchk.Value = "site"
                chk = "site"
            End If
        End If
        If chk = "open" Then
            sql = "select failid, failuremode from failuremodes order by failuremode"
        ElseIf chk = "site" Then
            sql = "select failid, failuremode from pmSiteFM where siteid = '" & sid & "' order by failuremode"
        Else
            Exit Function
        End If
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Public Function PopulateSkills() As DataSet
        cid = lblcid.Value
        sid = lblsid.Value
        Dim sskills As String = lblsskills.Value
        'If sskills = "yes" Then
        'sql = "select skillid, skill, skillindex from pmSiteSkills where (compid = '" & cid & "' and siteid = '" & sid & "') or skillindex = '0' order by skillindex"
        'Else
        sql = "select skillid, skill, skillindex from pmSkills order by skillindex"
        'End If

        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function

    Private Sub AddTask()
        jid = lbljpid.Value
        wonum = lblwo.Value
        If jid <> 0 Then
            'gtasks.Open()
            Dim stcnt As Integer
            sql = "Select count(*) from woJobTasks " _
            + "where jpid = '" & jid & "' and wonum = '" & wonum & "' and subtask = 0"
            stcnt = gtasks.Scalar(sql)
            stcnt += 1
            Dim pmtskid As Integer
            sql = "insert into pmjobtasks (jpid, tasknum) values ('" & jid & "', '" & stcnt & "') select @@identity"
            gtasks.Update(sql)
            sql = "insert into wojobtasks (jpid, wonum, tasknum) values ('" & jid & "', '" & wonum & "','" & stcnt & "') "
            gtasks.Update(sql)
            BindGrid(jid, wonum)
            'gtasks.Dispose()
        Else
            Dim strMessage As String = tmod.getmsg("cdstr574", "wojpedit.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub

    Private Sub dgtasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.CancelCommand
        jid = lbljpid.Value
        wonum = lblwo.Value
        lbleditmode.Value = "0"
        dgtasks.EditItemIndex = -1
        gtasks.Open()
        BindGrid(jid, wonum)
        gtasks.Dispose()

        lbldboxid.Value = ""
        lbltaskdesc.Value = ""
        lblqboxid.Value = ""
        lblgridqty.Value = ""
        lblmboxid.Value = ""
        lblgridmins.Value = ""
        lblsboxid.Value = ""
        lblgridskillid.Value = ""
    End Sub

    Private Sub dgtasks_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.DeleteCommand
        Dim tid, jid, tnum As String
        Try
            tid = CType(e.Item.FindControl("lblttid"), Label).Text
            tnum = CType(e.Item.FindControl("lblt"), TextBox).Text
        Catch ex As Exception
            tid = CType(e.Item.FindControl("lbltida"), Label).Text
            tnum = CType(e.Item.FindControl("lblta"), Label).Text
        End Try
        jid = lbljpid.Value
        wonum = lblwo.Value

        sql = "usp_delwjptask '" & jid & "', '" & tnum & "', '" & wonum & "'"
        gtasks.Open()
        gtasks.Update(sql)
        sql = "usp_deljptask '" & jid & "', '" & tnum & "', '" & wonum & "'"
        gtasks.Update(sql)
        'gtasks.UpMod(eqid)
        Try
            dgtasks.EditItemIndex = -1
        Catch ex As Exception

        End Try
        wonum = lblwo.Value
        BindGrid(jid, wonum)
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.EditCommand
        jid = lbljpid.Value
        wonum = lblwo.Value
        lbleditmode.Value = "1"
        lbloldtasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text
        dgtasks.EditItemIndex = e.Item.ItemIndex
        gtasks.Open()
        BindGrid(jid, wonum)
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.ItemCommand
        If e.CommandName = "Tool" Then
            lbltool.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblfailret.Value = "1"
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If

        End If
        If e.CommandName = "Part" Then
            lblpart.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblfailret.Value = "1"
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        If e.CommandName = "Lube" Then
            lbllube.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblfailret.Value = "1"
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        If e.CommandName = "Meas" Then
            lblmeas.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblfailret.Value = "1"
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        If e.CommandName = "Note" Then
            lblnote.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblfailret.Value = "1"
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        Dim ttid As String
        Dim pmtid As String
        wonum = lblwo.Value
        jid = lbljpid.Value
        If e.CommandName = "ToTask2" Then
            pmtid = CType(e.Item.FindControl("lblpmtskid"), Label).Text
            ttid = CType(e.Item.FindControl("lblttid"), Label).Text
            Dim Item As ListItem
            Dim f, fi, oa As String
            Dim ipar As Integer = 0
            Dim comp As String = lblcoid.Value
            gtasks.Open()
            For Each Item In CType(e.Item.FindControl("lbCompFM"), ListBox).Items
                If Item.Selected Then
                    'f = Item.Text.ToString
                    'fi = Item.Value.ToString
                    ttid = CType(e.Item.FindControl("lblttid"), Label).Text
                    'GetItems(f, fi, comp, ttid)
                    f = Item.Text.ToString
                    fi = Item.Value.ToString
                    Dim fiarr() As String = fi.Split("-")
                    fi = fiarr(0)
                    oa = fiarr(1)
                    If oa <> "0" Then
                        ipar = f.LastIndexOf("(")
                        If ipar <> -1 Then
                            f = Mid(f, 1, ipar)
                        End If
                    End If
                    GetItems(f, fi, comp, ttid, pmtid, oa)
                End If
            Next
            sql = "usp_UpdateJPFM1w '" & pmtid & "','" & wonum & "'"
            gtasks.Update(sql)

            sql = "usp_UpdateJPFM1 '" & ttid & "','" & wonum & "'"
            gtasks.Update(sql)

            dgtasks.Dispose()
            BindGrid(jid, wonum)
            dgtasks.EditItemIndex = e.Item.ItemIndex

            gtasks.Dispose()
        End If

        If e.CommandName = "RemTask" Then
            pmtid = CType(e.Item.FindControl("lblpmtskid"), Label).Text
            ttid = CType(e.Item.FindControl("lblttid"), Label).Text
            Dim Item As ListItem
            Dim f, fi, oa As String
            Dim ipar As Integer = 0
            Dim comp As String = lblcoid.Value
            gtasks.Open()
            Dim rf, rfd As String
            For Each Item In CType(e.Item.FindControl("lbfailmodes"), ListBox).Items
                If Item.Selected Then
                    'f = Item.Text.ToString
                    'fi = Item.Value.ToString
                    ttid = CType(e.Item.FindControl("lblttid"), Label).Text
                    'RemItems(f, fi, ttid)
                    f = Item.Text.ToString
                    fi = Item.Value.ToString
                    Dim fiarr() As String = fi.Split("-")
                    fi = fiarr(0)
                    oa = fiarr(1)
                    If oa <> "0" Then
                        ipar = f.LastIndexOf("(")
                        If ipar <> -1 Then
                            f = Mid(f, 1, ipar)
                        End If
                    End If
                    RemItems(f, fi, ttid, oa)
                    If Len(rf) = 0 Then
                        rf = f
                    Else
                        rf += "," & f
                    End If
                End If
            Next

            sql = "usp_UpdateJPFM1w '" & pmtid & "','" & wonum & "'"
            gtasks.Update(sql)

            sql = "usp_UpdateJPFM1 '" & ttid & "','" & wonum & "'"
            gtasks.Update(sql)

            dgtasks.Dispose()
            BindGrid(jid, wonum)
            dgtasks.EditItemIndex = e.Item.ItemIndex
            gtasks.Dispose()
        End If
        If e.CommandName = "AddSub" Then

        End If
    End Sub

    Private Sub dgtasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtasks.ItemDataBound
        If e.Item.ItemType = ListItemType.EditItem Then
            lblitemindex.Value = e.Item.ItemIndex
            lblcurrtask.Value = CType(e.Item.FindControl("lblttid"), Label).Text.ToString

            Dim dbox As TextBox = CType(e.Item.FindControl("txtdesc"), TextBox)
            Dim dboxid As String = dbox.ClientID
            lbldboxid.Value = dboxid


            Dim qbox As TextBox = CType(e.Item.FindControl("txtqty"), TextBox)
            Dim qboxid As String = qbox.ClientID
            lblqboxid.Value = qboxid


            Dim mbox As TextBox = CType(e.Item.FindControl("txttr"), TextBox)
            Dim mboxid As String = mbox.ClientID
            lblmboxid.Value = mboxid

            'Dim sbox As DropDownList = CType(e.Item.FindControl("ddskill"), DropDownList)
            'Dim sboxid As String = sbox.ClientID
            'lblsboxid.Value = sboxid
            Dim pmtskid As String = DataBinder.Eval(e.Item.DataItem, "pmtskid").ToString
            wonum = lblwo.Value
            Dim lblskill As Label = CType(e.Item.FindControl("lblskill"), Label)
            Dim skillid As String
            skillid = lblskill.ClientID.ToString
            Dim imgskill As HtmlImage = CType(e.Item.FindControl("imgskill"), HtmlImage)
            imgskill.Attributes.Add("onclick", "getskill('" & skillid & "','" & pmtskid & "','" & sid & "','skill','" & wonum & "');")

            Dim fchk = lblfailret.Value

            If lblfailret.Value <> "1" Then
                lbltaskdesc.Value = DataBinder.Eval(e.Item.DataItem, "taskdesc").ToString
                lblgridqty.Value = DataBinder.Eval(e.Item.DataItem, "qty").ToString
                lblgridmins.Value = DataBinder.Eval(e.Item.DataItem, "ttime").ToString
                lblgridskillid.Value = DataBinder.Eval(e.Item.DataItem, "skillid").ToString
            End If
        End If
    End Sub

    Private Sub dgtasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.UpdateCommand
        Dim tn, tid, desc, ski, tski, qty, tr, dt, otn, jid, fi, fs, jpid As String
        'addtask.Enabled = True
        qty = CType(e.Item.FindControl("txtqty"), TextBox).Text
        tr = CType(e.Item.FindControl("txttr"), TextBox).Text
        dt = CType(e.Item.FindControl("txtdt"), TextBox).Text
        desc = CType(e.Item.FindControl("txtdesc"), TextBox).Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", " ", , , vbTextCompare)
        tid = CType(e.Item.FindControl("lblttid"), Label).Text
        tn = CType(e.Item.FindControl("lblt"), TextBox).Text
        wonum = lblwo.Value
        jpid = lbljpid.Value
        Try
            ski = CType(e.Item.FindControl("ddskill"), DropDownList).SelectedItem.Value
            tski = CType(e.Item.FindControl("ddskill"), DropDownList).SelectedItem.Text
        Catch ex As Exception
            ski = "0"
            tski = "Select"
        End Try
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToInt64(qty)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr575", "wojpedit.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        Dim txtchk As Long
        Try
            txtchk = System.Convert.ToDecimal(tr)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr576", "wojpedit.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        lbleditmode.Value = "0"
        sql = "update wojobtasks set qty = '" & qty & "', " _
        + "ttime = '" & tr & "', " _
        + "skillid = '" & ski & "', " _
        + "skill = '" & tski & "', " _
        + "taskdesc = '" & desc & "', " _
        + "rate = (select rate from pmskills where skillid = '" & ski & "') " _
        + "where wojtid = '" & tid & "'; " _
        + "update wojobtasks set labcost = ((qty * (ttime / 60)) * rate) " _
        + "where wojtid = '" & tid & "'"
        gtasks.Open()
        gtasks.Update(sql)
        sql = "update pmjobtasks set qty = '" & qty & "', " _
            + "ttime = '" & tr & "', " _
            + "taskdesc = '" & desc & "' " _
            + "where pmtskid = '" & tid & "'; " _
            + "update pmjobtasks set labcost = ((qty * (ttime / 60)) * rate) " _
            + "where pmtskid = '" & tid & "'; " _
            + "update pmjobtasks set skillindex = ( " _
            + "isnull((select skillindex from pmskills s where " _
            + "pmjobtasks.skillid = s.skillid), 0)) where jpid = '" & jid & "'"
        gtasks.Update(sql)
        sid = lblsid.Value
        Dim sskills As String = lblsskills.Value
        If sskills = "yes" Then
            sql = "update wojobtasks  set skillindex = ( " _
            + "isnull((select skillindex from pmsiteskills s where " _
            + "wojobtasks.skillid = s.skillid and s.siteid = '" & sid & "'), 0)) " _
            + "where wojobtasks.wojtid = '" & tid & "'"
        Else
            sql = "update wojobtasks  set skillindex = ( " _
            + "isnull((select skillindex from pmskills s where " _
            + "wojobtasks.skillid = s.skillid), 0)) " _
            + "where wojobtasks.wojtid = '" & tid & "'"
        End If

        gtasks.Update(sql)
        If sskills = "yes" Then
            sql = "update pmjobtasks  set skillindex = ( " _
                  + "isnull((select skillindex from pmsiteskills s where " _
                  + "pmjobtasks.skillid = s.skillid and pmjobtasks.siteid = s.siteid), 0)) " _
                  + "where pmjobtasks.pmtskid = '" & tid & "'"
        Else
            sql = "update pmjobtasks  set skillindex = ( " _
      + "isnull((select skillindex from pmskills s where " _
      + "pmjobtasks.skillid = s.skillid), 0)) " _
      + "where pmjobtasks.pmtskid = '" & tid & "'"
        End If

        gtasks.Update(sql)
        Try
            sql = "update workorder set estjplabhrs = (select sum(ttime / 60) from wojobtasks where " _
            + "wonum = '" & wonum & "' and jpid = '" & jpid & "'), " _
            + "estjplabcost = (select sum(labcost) from wojobtasks where wonum = '" & wonum & "' and jpid = '" & jpid & "') " _
            + "where wonum = '" & wonum & "'"

            gtasks.Update(sql)
        Catch ex As Exception

        End Try

        '' and wojobtasks.siteid = s.siteid
        otn = lbloldtasknum.Value
        jid = lbljpid.Value
        If otn <> tn Then
            sql = "usp_reorderwjptasks '" & tid & "', '" & jid & "', '" & tn & "', '" & otn & "', '" & wonum & "'"
            gtasks.Update(sql)
            sql = "usp_reorderjptasks '" & tid & "', '" & jid & "', '" & tn & "', '" & otn & "', '" & wonum & "'"
            gtasks.Update(sql)
        End If
        dgtasks.EditItemIndex = -1
        BindGrid(jid, wonum)
        gtasks.Dispose()
    End Sub
    Private Sub RemItems(ByVal f As String, ByVal fi As String, ByVal ttid As String, ByVal oaid As String)
        Dim jp As String = lbljpid.Value
        wonum = lblwo.Value
        Try
            sql = "delete from wojpfailmodes where jpid = '" & jp & "' and failid = '" & fi & "' and wojtid = '" & ttid & "' and wonum = '" & wonum & "'"
            gtasks.Update(sql)
            sql = "delete from jpfailmodes where jpid = '" & jp & "' and failid = '" & fi & "' and pmtskid = '" & ttid & "' and oaid = '" & oaid & "'"
            gtasks.Update(sql)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GetItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal ttid As String, ByVal pmtskid As String, ByVal oaid As String)
        Dim typ As String
        Dim jp As String = lbljpid.Value
        wonum = lblwo.Value
        Dim fcnt, fcnt2 As Integer

        If oaid <> "0" Then
            sql = "select count(*) from jpfailmodes where pmtskid = '" & ttid & "' and failid = '" & fi & "' and oaid = '" & oaid & "'"
            fcnt = gtasks.Scalar(sql)
            sql = "select count(*) from jpfailmodes where pmtskid = '" & ttid & "'"
            fcnt2 = gtasks.Scalar(sql)
        Else
            sql = "select count(*) from jpfailmodes where pmtskid = '" & ttid & "' and failid = '" & fi & "'"
            fcnt = gtasks.Scalar(sql)
            sql = "select count(*) from jpfailmodes where pmtskid = '" & ttid & "'"
            fcnt2 = gtasks.Scalar(sql)
        End If


        If comp <> "" Then
            typ = "hascomp"
        Else
            typ = "na"
        End If
        If fcnt2 >= 5 Then
            Dim strMessage As String = tmod.getmsg("cdstr472", "JobPlan.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf fcnt > 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr473", "JobPlan.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            'Try
            sql = "usp_addJPFailureMode " & jp & ", " & fi & ", '" & f & "', '" & comp & "', '" & ttid & "', '" & wonum & "', '" & typ & "','" & oaid & "'"
            gtasks.Update(sql)
            'eqid = lbleqid.Value
            'End Try
        End If
    End Sub
    Function PopulateCompFM(ByVal comp As String, ByVal ttid As String)
        If comp <> "0" And comp <> "" Then
            sql = "select failid, failuremode " _
            + "from componentfailmodes where comid = '" & comp & "' and " _
            + "failid not in (select failid from wojpfailmodes where wojtid = '" & ttid & "')"
            sql = "usp_getcfall_tsknajp '" & comp & "','" & ttid & "'"
            dslev = gtasks.GetDSData(sql)
            Return dslev
        End If

    End Function
    Function PopulateTaskFM(ByVal comp As String, ByVal ttid As String)
        'If comp <> "0" Then
        sql = "select * from wojpfailmodes where wojtid = '" & ttid & "'"
        sql = "usp_getcfall_tskjp '" & comp & "','" & ttid & "'"
        dslev = gtasks.GetDSData(sql)
        Return dslev
        'End If
    End Function




    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgtasks.Columns(0).HeaderText = dlabs.GetDGPage("wojpedit.aspx", "dgtasks", "0")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(1).HeaderText = dlabs.GetDGPage("wojpedit.aspx", "dgtasks", "1")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(2).HeaderText = dlabs.GetDGPage("wojpedit.aspx", "dgtasks", "2")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(3).HeaderText = dlabs.GetDGPage("wojpedit.aspx", "dgtasks", "3")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(4).HeaderText = dlabs.GetDGPage("wojpedit.aspx", "dgtasks", "4")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(5).HeaderText = dlabs.GetDGPage("wojpedit.aspx", "dgtasks", "5")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(6).HeaderText = dlabs.GetDGPage("wojpedit.aspx", "dgtasks", "6")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(8).HeaderText = dlabs.GetDGPage("wojpedit.aspx", "dgtasks", "8")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(10).HeaderText = dlabs.GetDGPage("wojpedit.aspx", "dgtasks", "10")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1468.Text = axlabs.GetASPXPage("wojpedit.aspx", "lang1468")
        Catch ex As Exception
        End Try
        Try
            'lang1469.Text = axlabs.GetASPXPage("wojpedit.aspx", "lang1469")
        Catch ex As Exception
        End Try
        Try
            'lang1470.Text = axlabs.GetASPXPage("wojpedit.aspx", "lang1470")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                bgbaddtask.Attributes.Add("src", "../images2/eng/bgbuttons/addtask.gif")
            ElseIf lang = "fre" Then
                bgbaddtask.Attributes.Add("src", "../images2/fre/bgbuttons/addtask.gif")
            ElseIf lang = "ger" Then
                bgbaddtask.Attributes.Add("src", "../images2/ger/bgbuttons/addtask.gif")
            ElseIf lang = "ita" Then
                bgbaddtask.Attributes.Add("src", "../images2/ita/bgbuttons/addtask.gif")
            ElseIf lang = "spa" Then
                bgbaddtask.Attributes.Add("src", "../images2/spa/bgbuttons/addtask.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub
End Class