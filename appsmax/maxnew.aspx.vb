﻿Imports System.Data.SqlClient
Public Class maxnew
    Inherits System.Web.UI.Page
    Dim max As New Utilities
    Dim sql As String
    Dim eqid, eqnum, pmnum, desc As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            eqnum = Request.QueryString("eqnum").ToString
            lbleqid.Value = eqid
            lbleqnum.Value = eqnum

        Else
            If Request.Form("lblsubmit") = "add" Then
                max.Open()
                CheckPM()
                max.Dispose()
                lblsubmit.Value = ""
            End If

        End If
    End Sub
    Private Sub CheckPM()
        eqid = lbleqid.Value
        eqnum = lbleqnum.Value
        pmnum = txtnew.Text
        desc = txtdesc.Text
        pmnum = max.ModString1(pmnum)
        desc = max.ModString1(desc)
        Dim pcnt As Integer = 0
        Dim npmid As Integer
        sql = "select count(*) from pmmax where pmnum = '" & pmnum & "'"
        pcnt = max.Scalar(sql)
        If pcnt = 0 Then
            If Len(pmnum) > 25 Then
                Dim strMessage As String = "PM Number Limited to 25 Characters"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            If Len(desc) > 50 Then
                Dim strMessage As String = "Description Limited to 50 Characters"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            sql = "insert into pmmax (eqid, eqnum, pmnum, description) " _
                + "values('" & eqid & "','" & eqnum & "','" & pmnum & "','" & desc & "') select @@identity"
            npmid = max.Scalar(sql)
            lblpmid.Value = npmid
            lblret.Value = "go"
        Else
            Dim strMessage As String = "This PM NUmber is Aleady in Use"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
       
    End Sub
End Class