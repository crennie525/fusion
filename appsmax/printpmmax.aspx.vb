﻿Imports System.Data.SqlClient
Imports System.Text
Public Class printpmmax
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim pmi As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim pmid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pmid = Request.QueryString("pmid").ToString
        pmi.Open()
        Dim flag As Integer = 0
        Dim subcnt As Integer = 0
        Dim subcnth As Integer = 0
        Dim pccnt As Integer = 0
        Dim func, funcchk, task, tasknum, subtask, pm, pics As String
        Dim pmnum, pmdesc, nextd, jpid, jpnum, wo As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
        sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""610""></td></tr>")
        sql = "select pmnum, description, pmjp1, jpnum, nextdate from pmmax where pmid = '" & pmid & "'"
        dr = pmi.GetRdrData(sql)
        While dr.Read
            pmnum = dr.Item("pmnum").ToString
            pmdesc = dr.Item("description").ToString
            nextd = dr.Item("nextdate").ToString
            jpid = dr.Item("pmjp1").ToString
            jpnum = dr.Item("jpnum").ToString
            'wo = dr.Item("pmnum").ToString
        End While
        dr.Close()
        
        sb.Append("<tr height=""20""><td class=""bigbold"" colspan=""3"">PM:          " & pmnum & "</td></tr>")
        sb.Append("<tr height=""20""><td class=""bigbold"" colspan=""3"">Description: " & pmdesc & "</td></tr>")
        sb.Append("<tr><td class=""bigbold"" colspan=""3"">&nbsp;</td></tr>")
        sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">Scheduled Start Date: " & nextd & "</td></tr>")
        sb.Append("<tr><td class=""bigbold"" colspan=""3""><hr></td></tr>")
        sb.Append("</table>")

        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
        sb.Append("<tr><td width=""650""></td></tr>")

        Sql = "select jp.jpnum, jp.description, " _
       + "d.dept_line, d.dept_desc, c.cell_name, c.cell_desc, l.location, l.description as loc_desc, " _
       + "e.eqnum, e.eqdesc, n.ncnum, n.ncdesc, f.func, f.func_desc, co.compnum, co.compdesc " _
       + "from pmjobplans jp " _
       + "left join dept d on d.dept_id = jp.deptid " _
       + "left join cells c on c.cellid = jp.cellid " _
       + "left join pmlocations l on l.locid = jp.locid " _
       + "left join equipment e on e.eqid = jp.eqid " _
       + "left join noncritical n on n.ncid = jp.ncid " _
       + "left join functions f on f.func_id = jp.funcid " _
       + "left join components co on co.comid = jp.comid " _
       + "where jp.jpid = '" & jpid & "'"
        Dim jp, jpd, dept, deptd, cell, celld, loc, locd, eq, eqd, nc, ncd, fu, fud, co, cod, mea As String
        dr = pmi.GetRdrData(Sql)
        While dr.Read
            jpd = dr.Item("description").ToString

            sb.Append("<tr><td class=""label"">Job Plan # " & jpnum & "</td></tr>")
            sb.Append("<tr><td class=""label"">Description: " & jpd & "</td></tr>")
            sb.Append("<tr><td>&nbsp;</td></tr>")

        End While
        dr.Close()

        sb.Append("<tr><td><Table cellSpacing=""0"" cellPadding=""2"" border=""0"" width=""620"">")
        sb.Append("<tr><td class=""label"" colspan=""4""><u>" & tmod.getxlbl("xlb340", "jobplanhtml.aspx.vb") & "</u></td></tr>")
        sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""560""></td></tr>")


        Sql = "select t.tasknum, t.subtask, t.taskdesc, t.failuremode, t.skill, t.qty, t.ttime, " _
        + "l.output as lout, tl.output as tout, p.output as pout, m.measurement " _
        + "from pmjobtasks t " _
        + "left join jplubesout l on t.pmtskid = l.pmtskid " _
        + "left join jptoolsout tl on t.pmtskid = tl.pmtskid " _
        + "left join jppartsout p on t.pmtskid = p.pmtskid " _
        + "left join pmTaskMeasDetails m on m.pmtskid = t.pmtskid and m.jpid = '" & jpid & "' " _
        + "where t.jpid = '" & jpid & "' order by t.tasknum"

        Dim tnum, td, fm, sk, qt, st, lube, tool, part, stnum As String

        dr = pmi.GetRdrData(Sql)
        While dr.Read
            tnum = dr.Item("tasknum").ToString
            stnum = dr.Item("subtask").ToString
            td = dr.Item("taskdesc").ToString
            sk = dr.Item("skill").ToString
            qt = dr.Item("qty").ToString
            st = dr.Item("ttime").ToString
            fm = dr.Item("failuremode").ToString
            lube = dr.Item("lout").ToString
            tool = dr.Item("tout").ToString
            part = dr.Item("pout").ToString
            mea = "" 'dr.Item("measurement").ToString
            If stnum = "0" Then
                sb.Append("<tr><td class=""plainlabel"">[" & tnum & "]</td>")
                If Len(td) > 0 Then
                    sb.Append("<td colspan=""3"" class=""plainlabel"">" & td & "</td></tr>")
                Else
                    sb.Append("<td colspan=""3"" class=""plainlabel"">" & tmod.getlbl("cdlbl846", "jobplanhtml.aspx.vb") & "</td></tr>")
                End If
                If sk <> "" And sk <> "Select" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>" & tmod.getxlbl("xlb341", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & sk)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Qty:&nbsp;&nbsp;</b>" & qt)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Time:&nbsp;&nbsp;</b>" & st & "</td></tr>")
                End If
                If fm <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>" & tmod.getxlbl("xlb342", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & fm & "</td></tr>")
                End If
                If mea <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>Measurements:&nbsp;&nbsp;</b>" & mea & "</td></tr>")
                End If
                If lube <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>" & tmod.getxlbl("xlb343", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & lube & "</td></tr>")
                End If
                If tool <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>" & tmod.getxlbl("xlb344", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & tool & "</td></tr>")
                End If
                If part <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>" & tmod.getxlbl("xlb345", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & part & "</td></tr>")
                End If
                sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
            Else
                sb.Append("<tr><td class=""plainlabel"">[" & stnum & "]</td>")
                If Len(td) > 0 Then
                    sb.Append("<td></td><td colspan=""2"" class=""plainlabel"">" & td & "</td></tr>")
                Else
                    sb.Append("<td></td><td colspan=""2"" class=""plainlabel"">" & tmod.getlbl("cdlbl847", "jobplanhtml.aspx.vb") & "</td></tr>")
                End If
                If sk <> "" And sk <> "Select" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb346", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & sk)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Qty:&nbsp;&nbsp;</b>" & qt)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Time:&nbsp;&nbsp;</b>" & st & "</td></tr>")
                End If
                If fm <> "" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb347", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & fm & "</td></tr>")
                End If
                If lube <> "" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb348", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & lube & "</td></tr>")
                End If
                If tool <> "" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb349", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & tool & "</td></tr>")
                End If
                If part <> "" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb350", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & part & "</td></tr>")
                End If
                sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
            End If

        End While
        dr.Close()
        sb.Append("</table></td></tr></table>")
        tdwi.InnerHtml = sb.ToString

        pmi.Dispose()
    End Sub

End Class