﻿Public Class pmprintallmax
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim pgall As New PMALLMAX
    Dim filter, longstring As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Response.Clear()
            filter = Request.QueryString("filt").ToString
            longstring = pgall.GetReport(filter)
            Response.Write(longstring)
        End If
    End Sub

End Class