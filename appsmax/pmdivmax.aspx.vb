﻿Imports System.Data.SqlClient
Imports System.Text
Public Class pmdivmax
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim mu As New mmenu_utils_a
    Dim sql As String
    Dim ds As DataSet
    Dim pmid, pmhid, pmstr, pdm, dlast, dnext, dstart, sup, lead, eqid, usestart, ro, Login, piccnt, pdt, ttt, sid, sched As String
    Dim issched As Integer
    Dim pmi As New Utilities
    Dim ap As New AppUtils
    Dim dr As SqlDataReader
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            'lbllog.Value = "no"
        End Try
        If Not IsPostBack Then
            sched = mu.Sched
            lblusesched.Value = sched
            issched = mu.Is_Sched
            lblissched.Value = issched
            If issched = 1 Then
                Label2.Visible = True
                divlg.Attributes.Add("class", "readonly17045")
            Else
                Label2.Visible = True
                divlg.Attributes.Add("class", "readonly17045")
            End If
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            Try
                piccnt = Request.QueryString("piccnt").ToString
                lblpiccnt.Value = piccnt
            Catch ex As Exception
                piccnt = "0"
                lblpiccnt.Value = "0"
            End Try
            If piccnt <> "0" And piccnt <> "" Then
                'Img4.Attributes.Add("class", "view")
                Img4.Visible = True
            Else
                'Img4.Attributes.Add("class", "details")
                Img4.Visible = False
            End If
            lblro.Value = ro
            If ro = "1" Then
                imgsav.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                imgsav.Attributes.Add("onclick", "")
                imgcomp.Attributes.Add("onclick", "")
            End If
            pmid = Request.QueryString("pmid").ToString
            'pmstr = Request.QueryString("pmstr").ToString
            eqid = Request.QueryString("eqid").ToString
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            'pdm = Request.QueryString("pdm").ToString
            'dlast = Request.QueryString("last").ToString
            'dnext = Request.QueryString("next").ToString
            lblpmid.Value = pmid
            '
            'pmlast.InnerHtml = dlast
            'lblnext.Value = dnext
            'pmnext.InnerHtml = dnext
            lbleqid.Value = eqid
            pmi.Open()
            pdt = ap.PDTEntry
            If pdt = "lvdtt" Then
                lblusetdt.Value = "no"

            Else
                lblusetdt.Value = "yes"

            End If
            ttt = ap.TTTEntry
            If ttt = "lvtpt" Then
                lblusetotal.Value = "no"

            Else
                lblusetotal.Value = "yes"

            End If
            getpdm()
            GetDocs()
            GetDates(pmid)
            GetTasks(pmid)
            'CheckMeas(pmid)
            pmi.Dispose()

        Else
            If Request.Form("lblsubmit") = "save" Then
                lblsubmit.Value = ""
                pmid = lblpmid.Value
                pmi.Open()
                SavePM()
                GetTasks(pmid)
                GetDates(pmid)
                GetTasks(pmid)
                pmi.Dispose()
            ElseIf Request.Form("lblsubmit") = "refresh" Then
                lblsubmit.Value = ""
                pmid = lblpmid.Value
                pmi.Open()
                GetDocs()
                GetDates(pmid)
                GetTasks(pmid)
                'CheckMeas(pmid)
                pmi.Dispose()
            ElseIf Request.Form("lblsubmit") = "refresh1" Then
                lblsubmit.Value = ""
                pmid = lblpmid.Value
                pmi.Open()
                SavePM()
                GetDocs()
                GetDates(pmid)
                GetTasks(pmid)
                'CheckMeas(pmid)
                pmi.Dispose()
            ElseIf Request.Form("lblsubmit") = "comp" Then
                lblsubmit.Value = ""
                pmid = lblpmid.Value
                pmi.Open()
                GetDates(pmid)
                GetTasks(pmid)
                pmi.Dispose()
            ElseIf Request.Form("lblsubmit") = "s" Then

            ElseIf Request.Form("lblsubmit") = "n" Then

            ElseIf Request.Form("lblsubmit") = "checkcomp" Then
                lblsubmit.Value = ""
                pmi.Open()
                CheckPMComp()
                pmi.Dispose()
            ElseIf Request.Form("lblsubmit") = "updocs" Then
                lblsubmit.Value = ""
                pmi.Open()
                GetDocs()
                pmi.Dispose()
            ElseIf Request.Form("lblsubmit") = "compwo" Then
                lblsubmit.Value = ""
                pmi.Open()
                Dim won As String = txtwonum.InnerHtml
                pmid = lblpmid.Value
                CheckActuals(won)
                CompWO()
                GetDates(pmid)
                pmi.Dispose()
            End If
        End If
    End Sub
    Private Sub getpdm()
        Dim cid As String = 0
        sql = "select ptid, pretech " _
       + "from pmPreTech where compid = '" & cid & "' or compid = '0' order by compid"
        dr = pmi.GetRdrData(sql)
        ddpt.DataSource = dr
        ddpt.DataTextField = "pretech"
        ddpt.DataValueField = "ptid"
        ddpt.DataBind()
        dr.Close()
        ddpt.Items.Insert(0, New ListItem("None"))
        ddpt.Items(0).Value = 0
    End Sub
    Private Sub LoadMLabor(ByVal wonum As String)
        
        pmid = lblpmid.Value
        sql = "select userid, username, pmid, sum(assigned) as assigned " _
       + "from pmassignmax where pmid = '" & pmid & "' and (islead <> '1' or islead is null) " _
       + "group by userid, username, pmid"
        Dim sb As New StringBuilder
        sb.Append("<table>")
        Dim user, hrs As String
        dr = pmi.GetRdrData(sql)
        While dr.Read
            user = dr.Item("username").ToString
            hrs = dr.Item("assigned").ToString
            sb.Append("<tr><td class=""plainlabel"">" & user & "</td>")
            sb.Append("<td class=""plainlabel"">" & hrs & "</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        divlg.InnerHtml = sb.ToString
    End Sub
    Private Sub GetDocs()
        pmid = lblpmid.Value
        Dim sbc As Integer
        Dim doc, docs, fn, ty As String
        sql = "select count(*) from pmmaxattach where pmid = '" & pmid & "'"
        sbc = pmi.Scalar(sql)
        If sbc > 0 Then ' should be using has rows instead of count
            sql = "select * from pmmaxattach where pmid = '" & pmid & "'"
            dr = pmi.GetRdrData(sql)
            While dr.Read
                fn = dr.Item("filename").ToString
                ty = dr.Item("doctype").ToString
                doc = ty & "~" & fn
                If docs = "" Then
                    docs = doc
                Else
                    docs += ";" & doc
                End If
            End While
            dr.Close()
            lbldocs.Value = docs
        End If


    End Sub
    Private Sub CheckPMComp()
        Dim chkalert As Integer = 0
        Dim mchk As String = lblmalert.Value
        lblmalert.Value = ""
        'Check Labor Entry
        Dim esthrs, acthrs As Decimal
        Dim esthrsj As Decimal
        Dim esthrsw As Decimal
        Dim won As String
        sql = "select isnull(actlabhrs, 0) as acthrs from workorder where wonum = '" & won & "'"
        dr = pmi.GetRdrData(sql)
        While dr.Read
            acthrs = dr.Item("acthrs").ToString
        End While
        dr.Close()
        If acthrs = 0 Then
            If mchk <> "ok" Then
                chkalert += 1
                If lblmalert.Value = "" Then
                    lblmalert.Value = "l"
                Else
                    lblmalert.Value += ",l"
                End If
            End If
        End If
        won = txtwonum.InnerHtml
        If chkalert = 0 Then
            CheckActuals(won)
            CompWO()
        Else
            Exit Sub
        End If
    End Sub
    Private Sub CheckActuals(ByVal wonum As String)
        Dim icost As String = ap.InvEntry
    End Sub
    Private Sub CompWO()
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        pdt = lblusetdt.Value
        ttt = lblusetotal.Value
        Dim wonum As String = txtwonum.InnerHtml
        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        Dim cndate As Date = pmi.CNOW
        issched = lblissched.Value

        If pmhid = "" And pmid <> "" Then
            Try
                sql = "select max(pmhid) from pmhistmax where pmid = '" & pmid & "'"
                pmhid = pmi.strScalar(sql)
                lblpmhid.Value = pmhid
            Catch ex As Exception
                sql = "insert into pmhistmax (pmid) values ('" & pmid & "')"
                pmi.Update(sql)
                Try
                    sql = "select max(pmhid) from pmhistmax where pmid = '" & pmid & "'"
                    pmhid = pmi.strScalar(sql)
                    lblpmhid.Value = pmhid
                Catch ex1 As Exception

                End Try
            End Try

        End If
        Dim pmnum As String = lblpmnum.Value
        If pmid <> "" And pmhid <> "" Then
            '@pmid int, @pmhid int, @pmnum varchar(50), @wonum int, @cdate datetime, @uid varchar(50) = null, @issched int = null
            sql = "usp_comppmmax '" & pmid & "', '" & pmhid & "', '" & pmnum & "', '" & wonum & "','" & cndate & "','" &  ustr & "','" & issched & "'"
            'pmi.Update(sql)
            'ttt & "',"'" & pdt & "','" &
            Dim newdate As String '= sql
            Try
                newdate = pmi.strScalar(sql)
                newdate = CType(newdate, DateTime)
            Catch ex As Exception
                'this is temp for nissan
                Try
                    Dim sql1 As String
                    sql1 = "insert into err_tmp (edate, sql) values (getdate(), '" & sql & "')"
                    pmi.Update(sql1)
                    Dim strMessage As String = "Problem Completing PM - Please Contact Your System Administrator"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Catch ex1 As Exception

                End Try
            End Try

            If newdate < Now Then
                lblalert.Value = "4"
            Else
                lblalert.Value = "3"
            End If


            Dim won As String = txtwonum.InnerHtml
            sql = "usp_upreserved '" & won & "'"
            pmi.Update(sql)
        Else
            Dim strMessage As String = "Problem Completing PM - Please Contact Your System Administrator\nNote: Missing PM ID"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Private Sub GetDates(ByVal pmid As String)
        Dim usestart, pmcnt, se, le, runs, wonum, jpid, jpnum As String

        sql = "select distinct Convert(char(10),p.firstdate,101) as 'startdate', " _
        + "Convert(char(10),p.nextdate,101) as 'nextdate', Convert(char(10),p.laststartdate,101) as 'lastdate', p.superid, p.supervisor, p.leadid, p.lead, " _
        + "pmhid = (select max(p1.pmhid) from pmhistmax p1 where p1.pmid = p.pmid), p.usetargetdate, p.pmcounter, pm = description, " _
        + "pmd = pmnum, p.wonum, p.pmjp1, p.jpnum, p.eqnum, p.skillid, p.skill, p.ptid, p.pretech, p.frequency, p.frequnit " _
        + "from pmmax p left join pmhistmax h on h.pmid = p.pmid where p.pmid = '" & pmid & "'"
        dr = pmi.GetRdrData(sql)
        While dr.Read
            txtfreq.Text = dr.Item("frequency").ToString
            Try
                ddunit.SelectedValue = dr.Item("frequnit").ToString
            Catch ex As Exception

            End Try
            txteq.InnerHtml = dr.Item("eqnum").ToString
            lbleqnum.Value = dr.Item("eqnum").ToString
            txts.InnerHtml = dr.Item("startdate").ToString
            lblstart.Value = dr.Item("startdate").ToString
            txtn.InnerHtml = dr.Item("nextdate").ToString
            lblnext.Value = dr.Item("nextdate").ToString
            lblondate.Value = dr.Item("nextdate").ToString
            

            lblskillid.Value = dr.Item("skillid").ToString
            lblskill.Value = dr.Item("skill").ToString
            lblptid.Value = dr.Item("ptid").ToString
            lblpretech.Value = dr.Item("pretech").ToString

            tdskill.InnerHtml = dr.Item("skill").ToString
            'tdpre.innerhtml = dr.Item("pretech").ToString
            Try
                ddpt.SelectedValue = dr.Item("ptid").ToString
            Catch ex As Exception

            End Try
            txtsup.InnerHtml = dr.Item("supervisor").ToString
            txtlead.InnerHtml = dr.Item("lead").ToString
            lblsup.Value = dr.Item("supervisor").ToString
            lbllead.Value = dr.Item("lead").ToString
            lblsupid.Value = dr.Item("superid").ToString
            lblleadid.Value = dr.Item("leadid").ToString
            lbljpid.Value = dr.Item("pmjp1").ToString
            lbljpnum.Value = dr.Item("jpnum").ToString
            tdjp.InnerHtml = dr.Item("jpnum").ToString
            pmhid = dr.Item("pmhid").ToString
            'lblpmid.Value = dr.Item("pmid").ToString
            lblpmhid.Value = dr.Item("pmhid").ToString
            usestart = dr.Item("usetargetdate").ToString
            pmcnt = dr.Item("pmcounter").ToString
            pmlast.InnerHtml = dr.Item("lastdate").ToString
            pmtitle.InnerHtml = dr.Item("pm").ToString
            lblpmdesc.Value = dr.Item("pm").ToString
            pmpdm.InnerHtml = dr.Item("pmd").ToString
            lblpmnum.Value = dr.Item("pmd").ToString
            'lblskillid.Value = dr.Item("skillid").ToString
            wonum = dr.Item("wonum").ToString
            txtwonum.InnerHtml = dr.Item("wonum").ToString
            lblwo.Value = dr.Item("wonum").ToString
            'runs = dr.Item("multidates").ToString
            'txtruns.Text = dr.Item("multidates").ToString
            'lblruns.Value = runs
            'txtcharge.Text = dr.Item("chargenum").ToString
            'se = dr.Item("supalert").ToString
            'le = dr.Item("leadalert").ToString
            'txtleadtime.Text = dr.Item("elead").ToString

            'lblttime.Value = dr.Item("ttime").ToString
            'lbldtime.Value = dr.Item("dtime").ToString

            'lblacttime.Value = dr.Item("acttime").ToString
            'lblactdtime.Value = dr.Item("actdtime").ToString
        End While
        dr.Close()
        If pmhid = "" And pmid <> "" Then
            Try
                sql = "select max(pmhid) from pmhistmax where pmid = '" & pmid & "'"
                pmhid = pmi.strScalar(sql)
                lblpmhid.Value = pmhid
            Catch ex As Exception
                sql = "insert into pmhistmax (pmid) values ('" & pmid & "')"
                pmi.Update(sql)
                Try
                    sql = "select max(pmhid) from pmhistmax where pmid = '" & pmid & "'"
                    pmhid = pmi.strScalar(sql)
                    lblpmhid.Value = pmhid
                Catch ex1 As Exception

                End Try
            End Try

        End If
       
        If usestart = "1" Then
            cbs.Checked = True
        Else
            cbs.Checked = False
        End If
        Dim newdate As String = lblnext.Value
        tdcnt.InnerHtml = ""
        If newdate <> "" Then
            newdate = CType(newdate, DateTime)
            If newdate < Now Then
                tdcnt.InnerHtml = "<img src=""../images/appbuttons/minibuttons/rwarning.gif"" onmouseover=""return overlib('" & tmod.getov("cov63", "PMDivMan.aspx.vb") & "', ABOVE, LEFT)"" onmouseout=""return nd()"">"
            End If
        End If
        tdcnt.InnerHtml += "Current PM Cnt: " & pmcnt
       
        LoadMLabor(wonum)
    End Sub
    Private Sub SavePM()
        Dim supi, leadi, ch, se, le, wo, runs, ptid, pretech, eqid, eqnum, jpid, jpnum As String
        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        Dim issched As String = lblissched.Value
        eqid = lbleqid.Value
        eqnum = lbleqnum.Value
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        dstart = lblstart.Value 'txts.Text
        dnext = lblnext.Value 'txtn.Text
        sup = lblsup.Value 'txtsup.Text
        lead = lbllead.Value 'txtlead.Text
        supi = lblsupid.Value
        wo = txtwonum.InnerHtml
        jpid = lbljpid.Value
        jpnum = lbljpnum.Value
        'If supi = "" Then supi = "0"
        leadi = lblleadid.Value
        ptid = ddpt.SelectedValue.ToString
        pretech = ddpt.SelectedItem.ToString
        If ddpt.SelectedIndex = 0 Or ddpt.SelectedIndex = -1 Then
            ptid = ""
            pretech = ""
        End If
        'If leadi = "" Then leadi = "0"
        If cbs.Checked = True Then
            usestart = "1"
        Else
            usestart = "0"
        End If
        'If cbsupe.Checked = True Then
        'se = "1"
        'End If
        'If cbleade.Checked = True Then
        'le = "1"
        'End If
        'ch = txtcharge.Text
        'ch = pmi.ModString2(ch)
        Dim onext As String = lblondate.Value
        runs = txtruns.InnerHtml
        'Dim runsi As Integer
        'Try
        'runsi = CType(runs, Integer)
        'Catch ex As Exception
        'runs = "1"
        'runsi = 1
        'Dim strMessage As String = "Number of Runs Must Be A Numeric Value."
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End Try
        'If runsi = 0 Then
        'runsi = 1
        'runs = "1"
        'End If
        'Dim cruns As String = lblworuns.Value
        'Dim crunsi As Integer
        'Try
        'crunsi = CType(cruns, Integer)
        'Catch ex As Exception
        'cruns = "0"
        'crunsi = 0
        'End Try
        'Dim oruns As String = lblruns.Value
        'Dim orunsi As Integer
        'Try
        'orunsi = CType(oruns, Integer)
        'Catch ex As Exception
        'oruns = "1"
        'orunsi = 1
        'End Try
        'If runsi <> orunsi Then
        'crunsi = crunsi + 1
        'If crunsi <= runsi Then
        'If wo <> "" Then
        'sql = "update workorder set multidates = '" & runs & "' where wonum = '" & wo & "'; " _
        '+ "update pm set multidates = '" & runs & "' where pmid = '" & pmid & "'"
        'pmi.Update(sql)
        'If orunsi = 1 Then
        'sql = "insert into womultidates (wonum, wocnt) values ('" & wo & "','1')"
        'pmi.Update(sql)
        'End If
        'Else
        'sql = "update pm set multidates = '" & runs & "' where pmid = '" & pmid & "'"
        'pmi.Update(sql)
        'End If
        'Else
        'Dim strMessage As String = "New Run Value would less than Current Work Order Run Count\nCould not update this value."
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'End If

        'End If

        Dim freq, unit As String
        freq = txtfreq.Text
        'verify freq
        Dim lfreq As Long
        If freq <> "" Then
            Try
                lfreq = System.Convert.ToInt32(freq)
            Catch ex As Exception
                Dim strMessage As String = "Frequency Must Be a Non-Decimal Numeric Value"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        End If

        unit = ddunit.SelectedValue.ToString
        If unit = "Select" Then
            unit = ""
        End If

        If dnext <> "" And (freq = "" Or unit = "Select") Then
            Dim strMessage As String = "Both a Frequency and Unit must be selected if a Next Date is specified"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If

        Dim cmd1 As New SqlCommand("update pmmax set frequency = @freq, frequnit = @unit where pmid = @pmid")
        Dim param00 = New SqlParameter("@pmid", SqlDbType.Int)
        param00.Value = pmid
        cmd1.Parameters.Add(param00)
        Dim param001 = New SqlParameter("@freq", SqlDbType.VarChar)
        If Len(freq) > 0 Then
            param001.Value = freq
        Else
            param001.Value = System.DBNull.Value
        End If
        cmd1.Parameters.Add(param001)
        Dim param002 = New SqlParameter("@unit", SqlDbType.VarChar)
        If Len(unit) > 0 Then
            param002.Value = unit
        Else
            param002.Value = System.DBNull.Value
        End If
        cmd1.Parameters.Add(param002)

        pmi.UpdateHack(cmd1)

        sql = "usp_uppm3max '" & pmid & ", '" & dstart & "', '" & dnext & "', '" & sup & "', '" & usestart & "', '" & supi & "', '" & pmhid & "', '" & wo & "','" & ustr & "','" & onext & "','" & issched & "', '" & ptid & "', '" & pretech & "', '" & eqid & "', '" & eqnum & "', '" & jpid & "','" & jpnum & "'"
        Dim stest As String = sql
        'Dim cmd As New SqlCommand("exec usp_uppm3 @pmid, @start, @next, @sup, @lead, @use, @supi, @leadi, @pmhid, @ch, @supa, @leada, @elead, @wo, @uid, @onext, @runs")
        Dim cmd As New SqlCommand("exec usp_uppm3max @pmid, @start, @next, @sup, @use, @supi, @pmhid, @wo, @uid, @onext, @issched, @ptid, @pretech, @eqid, @eqnum, @jpid, @jpnum, @leadi, @lead")
        Dim param0 = New SqlParameter("@pmid", SqlDbType.Int)
        param0.Value = pmid
        cmd.Parameters.Add(param0)
        Dim param02 = New SqlParameter("@pmhid", SqlDbType.Int)
        param02.Value = pmhid
        cmd.Parameters.Add(param02)
        Dim param01 = New SqlParameter("@use", SqlDbType.Int)
        param01.Value = usestart
        cmd.Parameters.Add(param01)
        Dim param1 = New SqlParameter("@start", SqlDbType.DateTime)
        If Len(dstart) > 0 Then
            param1.Value = dstart
        Else
            param1.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param1)
        Dim param2 = New SqlParameter("@next", SqlDbType.DateTime)
        If Len(dnext) > 0 Then
            param2.Value = dnext
        Else
            param2.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param2)
        Dim param3 = New SqlParameter("@sup", SqlDbType.VarChar)
        If Len(sup) > 0 Then
            param3.Value = sup
        Else
            param3.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param3)
        Dim param4 = New SqlParameter("@lead", SqlDbType.VarChar)
        If Len(lead) > 0 Then
            param4.Value = lead
        Else
            param4.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param4)
        Dim param5 = New SqlParameter("@supi", SqlDbType.Int)
        If Len(supi) > 0 Then
            param5.Value = supi
        Else
            param5.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param5)


        Dim param14 = New SqlParameter("@leadi", SqlDbType.Int)
        If lblleadid.Value <> "" Then
            param14.Value = lblleadid.Value
        Else
            param14.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param14)

        Dim param11 = New SqlParameter("@wo", SqlDbType.Int)
        If wo <> "" Then
            param11.Value = wo
        Else
            param11.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param11)
        Dim param12 = New SqlParameter("@uid", SqlDbType.VarChar)
        If ustr <> "" Then
            param12.Value = ustr
        Else
            param12.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param12)
        Dim param13 = New SqlParameter("@onext", SqlDbType.VarChar)
        'If onext <> "" Then
        param13.Value = onext
        ' Else
        'param13.Value = System.DBNull.Value
        'End If
        cmd.Parameters.Add(param13)
        'Dim param14 = New SqlParameter("@runs", SqlDbType.VarChar)
        'If runs <> "" Then
        'param14.Value = runs
        'Else
        'param14.Value = System.DBNull.Value
        'End If
        'cmd.Parameters.Add(param14)

        Dim param15 = New SqlParameter("@issched", SqlDbType.VarChar)
        If issched <> "" Then
            param15.Value = issched
        Else
            param15.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param15)

        Dim param6 = New SqlParameter("@ptid", SqlDbType.Int)
        If Len(ptid) > 0 Then
            param6.Value = ptid
        Else
            param6.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param6)
        Dim param7 = New SqlParameter("@pretech", SqlDbType.VarChar)
        If Len(pretech) > 0 Then
            param7.Value = pretech
        Else
            param7.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param7)
        Dim param8 = New SqlParameter("@eqid", SqlDbType.Int)
        If Len(eqid) > 0 Then
            param8.Value = eqid
        Else
            param8.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param8)
        Dim param9 = New SqlParameter("@eqnum", SqlDbType.VarChar)
        If Len(eqnum) > 0 Then
            param9.Value = eqnum
        Else
            param9.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param9)

        Dim param10 = New SqlParameter("@jpid", SqlDbType.VarChar)
        If Len(jpid) > 0 Then
            param10.Value = jpid
        Else
            param10.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param10)
        Dim param16 = New SqlParameter("@jpnum", SqlDbType.VarChar)
        If Len(jpnum) > 0 Then
            param16.Value = jpnum
        Else
            param16.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param16)

        pmi.UpdateHack(cmd)



        'dr = pmi.GetRdrDataHack(cmd)
        'While dr.Read
        'lblpmid.Value = dr.Item("pmid").ToString
        'lblpmid.Value = dr.Item("pmhid").ToString
        'End While
        'dr.Close()
    End Sub
    Private Sub GetTasks(ByVal pmid As String)

        Dim flag As Integer = 0
        Dim subcnt As Integer = 0
        Dim subcnth As Integer = 0
        Dim pccnt As Integer = 0
        Dim func, funcchk, task, tasknum, subtask, pm, pics As String
        Dim pmnum, pmdesc, nextd, jpid, jpnum, wo As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
        sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""610""></td></tr>")

        pmnum = lblpmnum.Value
        pmdesc = lblpmdesc.Value
        nextd = lblnext.Value
        jpid = lbljpid.Value
        jpnum = lbljpnum.Value
        wo = lblwo.Value
        sb.Append("<tr height=""20""><td class=""bigbold"" colspan=""3"">PM:          " & pmnum & "</td></tr>")
        sb.Append("<tr height=""20""><td class=""bigbold"" colspan=""3"">Description: " & pmdesc & "</td></tr>")
        sb.Append("<tr><td class=""bigbold"" colspan=""3"">&nbsp;</td></tr>")
        sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">Scheduled Start Date: " & nextd & "</td></tr>")
        sb.Append("<tr><td class=""bigbold"" colspan=""3""><hr></td></tr>")
        sb.Append("</table>")

        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
        sb.Append("<tr><td width=""650""></td></tr>")

        sql = "select jp.jpnum, jp.description, " _
       + "d.dept_line, d.dept_desc, c.cell_name, c.cell_desc, l.location, l.description as loc_desc, " _
       + "e.eqnum, e.eqdesc, n.ncnum, n.ncdesc, f.func, f.func_desc, co.compnum, co.compdesc " _
       + "from pmjobplans jp " _
       + "left join dept d on d.dept_id = jp.deptid " _
       + "left join cells c on c.cellid = jp.cellid " _
       + "left join pmlocations l on l.locid = jp.locid " _
       + "left join equipment e on e.eqid = jp.eqid " _
       + "left join noncritical n on n.ncid = jp.ncid " _
       + "left join functions f on f.func_id = jp.funcid " _
       + "left join components co on co.comid = jp.comid " _
       + "where jp.jpid = '" & jpid & "'"
        Dim jp, jpd, dept, deptd, cell, celld, loc, locd, eq, eqd, nc, ncd, fu, fud, co, cod, mea As String
        dr = pmi.GetRdrData(sql)
        While dr.Read
            jpd = dr.Item("description").ToString

            sb.Append("<tr><td class=""label"">Job Plan # " & jpnum & "</td></tr>")
            sb.Append("<tr><td class=""label"">Description: " & jpd & "</td></tr>")
            sb.Append("<tr><td>&nbsp;</td></tr>")
            
        End While
        dr.Close()

        sb.Append("<tr><td><Table cellSpacing=""0"" cellPadding=""2"" border=""0"" width=""620"">")
        sb.Append("<tr><td class=""label"" colspan=""4""><u>" & tmod.getxlbl("xlb340", "jobplanhtml.aspx.vb") & "</u></td></tr>")
        sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""560""></td></tr>")
       

        sql = "select t.tasknum, t.subtask, t.taskdesc, t.failuremode, t.skill, t.qty, t.ttime, " _
        + "l.output as lout, tl.output as tout, p.output as pout, m.measurement " _
        + "from pmjobtasks t " _
        + "left join jplubesout l on t.pmtskid = l.pmtskid " _
        + "left join jptoolsout tl on t.pmtskid = tl.pmtskid " _
        + "left join jppartsout p on t.pmtskid = p.pmtskid " _
        + "left join pmTaskMeasDetails m on m.pmtskid = t.pmtskid and m.jpid = '" & jpid & "' " _
        + "where t.jpid = '" & jpid & "' order by t.tasknum"

        Dim tnum, td, fm, sk, qt, st, lube, tool, part, stnum As String

        dr = pmi.GetRdrData(sql)
        While dr.Read
            tnum = dr.Item("tasknum").ToString
            stnum = dr.Item("subtask").ToString
            td = dr.Item("taskdesc").ToString
            sk = dr.Item("skill").ToString
            qt = dr.Item("qty").ToString
            st = dr.Item("ttime").ToString
            fm = dr.Item("failuremode").ToString
            lube = dr.Item("lout").ToString
            tool = dr.Item("tout").ToString
            part = dr.Item("pout").ToString
            mea = "" 'dr.Item("measurement").ToString
            If stnum = "0" Then
                sb.Append("<tr><td class=""plainlabel"">[" & tnum & "]</td>")
                If Len(td) > 0 Then
                    sb.Append("<td colspan=""3"" class=""plainlabel"">" & td & "</td></tr>")
                Else
                    sb.Append("<td colspan=""3"" class=""plainlabel"">" & tmod.getlbl("cdlbl846", "jobplanhtml.aspx.vb") & "</td></tr>")
                End If
                If sk <> "" And sk <> "Select" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>" & tmod.getxlbl("xlb341", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & sk)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Qty:&nbsp;&nbsp;</b>" & qt)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Time:&nbsp;&nbsp;</b>" & st & "</td></tr>")
                End If
                If fm <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>" & tmod.getxlbl("xlb342", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & fm & "</td></tr>")
                End If
                If mea <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>Measurements:&nbsp;&nbsp;</b>" & mea & "</td></tr>")
                End If
                If lube <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>" & tmod.getxlbl("xlb343", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & lube & "</td></tr>")
                End If
                If tool <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>" & tmod.getxlbl("xlb344", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & tool & "</td></tr>")
                End If
                If part <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""3"" class=""plainlabel""><b>" & tmod.getxlbl("xlb345", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & part & "</td></tr>")
                End If
                sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
            Else
                sb.Append("<tr><td class=""plainlabel"">[" & stnum & "]</td>")
                If Len(td) > 0 Then
                    sb.Append("<td></td><td colspan=""2"" class=""plainlabel"">" & td & "</td></tr>")
                Else
                    sb.Append("<td></td><td colspan=""2"" class=""plainlabel"">" & tmod.getlbl("cdlbl847", "jobplanhtml.aspx.vb") & "</td></tr>")
                End If
                If sk <> "" And sk <> "Select" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb346", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & sk)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Qty:&nbsp;&nbsp;</b>" & qt)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Time:&nbsp;&nbsp;</b>" & st & "</td></tr>")
                End If
                If fm <> "" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb347", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & fm & "</td></tr>")
                End If
                If lube <> "" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb348", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & lube & "</td></tr>")
                End If
                If tool <> "" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb349", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & tool & "</td></tr>")
                End If
                If part <> "" Then
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb350", "jobplanhtml.aspx.vb") & "&nbsp;&nbsp;</b>" & part & "</td></tr>")
                End If
                sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
            End If

        End While
        dr.Close()
        sb.Append("</table></td></tr></table>")
        tdtasks.InnerHtml = sb.ToString
    End Sub




    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang591.Text = axlabs.GetASPXPage("PMDivMan.aspx", "lang591")
        Catch ex As Exception
        End Try
        Try
            'lang592.Text = axlabs.GetASPXPage("PMDivMan.aspx", "lang592")
        Catch ex As Exception
        End Try
        Try
            'lang593.Text = axlabs.GetASPXPage("PMDivMan.aspx","lang593")
        Catch ex As Exception
        End Try
        Try
            lang594.Text = axlabs.GetASPXPage("PMDivMan.aspx", "lang594")
        Catch ex As Exception
        End Try
        Try
            lang595.Text = axlabs.GetASPXPage("PMDivMan.aspx", "lang595")
        Catch ex As Exception
        End Try
        Try
            lang596.Text = axlabs.GetASPXPage("PMDivMan.aspx", "lang596")
        Catch ex As Exception
        End Try
        Try
            lang597.Text = axlabs.GetASPXPage("PMDivMan.aspx", "lang597")
        Catch ex As Exception
        End Try
        Try
            lang598.Text = axlabs.GetASPXPage("PMDivMan.aspx", "lang598")
        Catch ex As Exception
        End Try
        Try
            lang599.Text = axlabs.GetASPXPage("PMDivMan.aspx", "lang599")
        Catch ex As Exception
        End Try
        Try
            lang600.Text = axlabs.GetASPXPage("PMDivMan.aspx", "lang600")
        Catch ex As Exception
        End Try
        Try
            lang601.Text = axlabs.GetASPXPage("PMDivMan.aspx", "lang601")
        Catch ex As Exception
        End Try
        Try
            'lang602.Text = axlabs.GetASPXPage("PMDivMan.aspx", "lang602")
        Catch ex As Exception
        End Try
        Try
            'lang603.Text = axlabs.GetASPXPage("PMDivMan.aspx", "lang603")
        Catch ex As Exception
        End Try
        Try
            lang604.Text = axlabs.GetASPXPage("PMDivMan.aspx", "lang604")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnedittask.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMan.aspx", "btnedittask") & "', ABOVE, LEFT)")
            btnedittask.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMan.aspx", "Img1") & "', ABOVE, LEFT)")
            Img1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMan.aspx", "Img2") & "', ABOVE, LEFT)")
            Img2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img3.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMan.aspx", "Img3") & "', ABOVE, LEFT)")
            Img3.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img4.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMan.aspx", "Img4") & "', ABOVE, LEFT)")
            Img4.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgcomp.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMan.aspx", "imgcomp") & "', ABOVE, LEFT)")
            imgcomp.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgi2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMan.aspx", "imgi2") & "', ABOVE, LEFT)")
            imgi2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgmeas.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMan.aspx", "imgmeas") & "')")
            imgmeas.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsav.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMan.aspx", "imgsav") & "', ABOVE, LEFT)")
            imgsav.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgwoshed.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMan.aspx", "imgwoshed") & "', ABOVE, LEFT)")
            imgwoshed.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'ovid68.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMan.aspx", "ovid68") & "')")
            'ovid68.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'ovid69.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMan.aspx", "ovid69") & "')")
            'ovid69.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'ovid70.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMan.aspx", "ovid70") & "', ABOVE, LEFT)")
            ''ovid70.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'ovid71.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMan.aspx", "ovid71") & "', ABOVE, LEFT)")
            'ovid71.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'ovid72.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMDivMan.aspx", "ovid72") & "', ABOVE, LEFT)")
            'ovid72.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub
End Class