﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmgetpmmax.aspx.vb" Inherits="lucy_r12.pmgetpmmax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcsse.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <!--<script language="JavaScript" type="text/javascript" src="../scripts1/PMGetPMManaspx.js"></script>-->
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        //ifsave
        function handlemaxtasks(ret) {
            if (ret == "ok") {
                redo();
            }
        }
        function remdat() {
            var eqid = document.getElementById("lbleqid").value;
            var sid = document.getElementById("lblsid").value;
            if (eqid != "") {
                var decision = confirm("Are You Sure You Want to Clear all Next Dates for all PM Records associated with this Equipment Record?")
                if (decision == true) {
                    document.getElementById("ifsave").src = "maxtasks.aspx?who=dat&pmid=&eqid=" + eqid;
                    //var timer = window.setTimeout("redo();", 3000);
                }
                else {
                    alert("Action Cancelled")
                }
            }
        }
        function redo() {
            document.getElementById("lblsubmit").value = "checkeq";
            document.getElementById("form1").submit();
        }
        function delall() {
            var eqid = document.getElementById("lbleqid").value;
            var sid = document.getElementById("lblsid").value;
            if (eqid != "") {
                var decision = confirm("Are You Sure You Want to Delete all PM Records associated with this Equipment Record?")
                if (decision == true) {
                    var decision2 = confirm("Cancel all PM Work Orders associated with this Equipment Record?")
                    if (decision2 == true) {
                        document.getElementById("ifsave").src = "maxtasks.aspx?who=pmwo&pmid=&eqid=" + eqid;
                        //var timer = window.setTimeout("redo();", 3000);
                    }
                    else {
                        document.getElementById("ifsave").src = "maxtasks.aspx?who=pm&pmid=&eqid=" + eqid;
                        //var timer = window.setTimeout("redo();", 3000);
                    } 
                }
                else {
                    alert("Action Cancelled")
                }
                
            }
        }
        function makenew() {
            var eqid = document.getElementById("lbleqid").value;
            if (eqid != "") {
                var sid = document.getElementById("lblsid").value;
                var eqnum = document.getElementById("lbleq").value;
                var eReturn = window.showModalDialog("maxnewdialog.aspx?eqid=" + eqid + "&eqnum=" + eqnum, "", "dialogHeight:400px; dialogWidth:500px; resizable=yes");
                if (eReturn) {
                    var pmid = eReturn;
                    window.parent.handletasks("pmgridmax.aspx?start=yes&jump=yes&typ=no&eqid=" + eqid + "&pmid=" + pmid + "&piccnt=0&sid=" + sid, "ok")
                }
            }
            else {
                alert("No Location or Equipment Record Selected")
            }
        }
        function gridret() {
            handleapp();
            tl = document.getElementById("lbltasklev").value;
            cid = document.getElementById("lblcid").value;
            sid = document.getElementById("lblsid").value;
            did = document.getElementById("lbldid").value;
            //alert(did)
            clid = document.getElementById("lblclid").value;
            eqid = document.getElementById("lbleqid").value;
            chk = document.getElementById("lblchk").value;
            //alert(chk)
            fuid = document.getElementById("lblfuid").value;
            cnt = document.getElementById("lblcnt").innerHTML;
            coid = document.getElementById("lblcoid").value;
            typ = document.getElementById("lbltyp").value;
            lid = document.getElementById("lbllid").value;
            //alert(cnt)
            if (cnt != "0") {
                window.parent.handletasks("PMTaskDivFuncGrid.aspx?start=yes&tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&comid=" + coid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&lid=" + lid + "&typ=" + typ)
            }
            else {
                alert("No Tasks Entered Yet!")
            }
        }
        function checkup() {
            var islabor = document.getElementById("lblislabor").value;
            if (islabor != "1") {
                var eqid = document.getElementById("lbleqid").value;
                window.parent.checkup(eqid);
            }
        }
        function checkpg() {
            //alert(document.getElementById("lbleq").value)
            document.getElementById("tdeq").innerHTML = document.getElementById("lbleq").value
            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                window.parent.doref();
            }
            else {
                window.parent.setref();
            }

            var gototasks = document.getElementById("lblgototasks").value;
            //alert(gototasks)
            var sid = document.getElementById("lblsid").value;
            var app = document.getElementById("appchk").value;

            if (app == "switch") window.parent.handleapp(app);

            var chk = document.getElementById("lblpar").value;

            if (chk == "eq") {
                valu = document.getElementById("lbleqid").value;
                window.parent.handleeq(chk, valu);
            }
            var clean = document.getElementById("lblcleantasks").value;

            if (clean == "1") {
                document.getElementById("lblcleantasks").value = "2";
                window.parent.handletasks("PMGridMan.aspx?start=no&jump=no&sid=" + sid)
            }
            //alert(gototasks)

            if (gototasks == "1") {

                document.getElementById("lblgototasks").value = "0";
                document.getElementById("lblcleantasks").value = "0";
                eqid = document.getElementById("lbleqid").value;
                pmid = document.getElementById("lblpmid").value;
                tcnt = document.getElementById("lbltaskcnt").value;
                var mod = document.getElementById("lblgototasks").value;
                var fu = document.getElementById("lblfuid").value;
                var co = document.getElementById("lblcoid").value;
                var typ = document.getElementById("lbltyp").value;
                var piccnt = document.getElementById("lblpiccnt").value;

                if (mod == "1") {

                }
                //alert(tcnt)
                if (tcnt == "1") {
                    window.parent.handletasks("pmgridmax.aspx?start=yes&jump=yes&typ=no&eqid=" + eqid + "&pmid=" + pmid + "&piccnt=" + piccnt + "&sid=" + sid, "ok")
                    //window.parent.handletasks("PMGridMan.aspx?comid=" + coid + "&start=yes&eqid=" + eqid, "ok")
                }
                else if (tcnt == "0") {
                    //alert("PMGridMan.aspx?start=yes&eqid=" + eqid)
                    window.parent.handletasks("pmgridmax.aspx?start=yes&jump=no&typ=no&eqid=" + eqid + "&pmid=" + pmid + "&piccnt=" + piccnt + "&sid=" + sid, "ok")
                }
                else if (tcnt == "2") {
                    window.parent.handletasks("pmgridmax.aspx?start=yes&jump=yes&typ=" + typ + "&eqid=" + eqid + "&pmid=" + pmid + "&fu=" + fu + "&co=" + co + "&piccnt=" + piccnt + "&sid=" + sid, "ok")
                }
                else if (tcnt == "3") {
                    window.parent.handletasks("PMTaskDivFuncGrid.aspx?start=no", "cell")
                }
                else if (tcnt == "4") {
                    window.parent.handletasks("PMTaskDivFuncGrid.aspx?start=no", "eq")
                }
                else if (tcnt == "5") {
                    window.parent.handletasks("PMTaskDivFuncGrid.aspx?start=no", "fu")
                }
                window.parent.getimage(eqid);
            }
            else if (gototasks == "0") {
                //var err = document.getElementById("lblerr").value;
                //if(err!="0") {
                //alert("This Equipment Record is InComplete")
                //}
                //else {
                alert("No PM Record Set Has Been Created For this Equipment Record")
                //document.getElementById("ddeq").options.selectedIndex = 0;
                //document.getElementById("tdspl").innerHTML = "";
                //document.getElementById("tdloc").innerHTML = "";
                document.getElementById("lbleqid").value = "0";
                document.getElementById("tdeq").innerHTML = "";
                document.getElementById("lbleq").value = "";
                window.parent.handletasks("pmgridmax.aspx?start=no&jump=no&sid=" + sid)
            }

            //}
            //var arch = document.getElementById("lblgetarch").value;
            //if(arch=="yes") {
            //chk = document.getElementById("lblchk").value;
            //did = document.getElementById("lbldept").value;
            //clid = document.getElementById("lblclid").value;
            //window.parent.getarch(chk, did, clid);
            //}

            //window.setTimeout("checkit2();", 100);
        }
        function getminsrch() {

            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "lup";
            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                clearall();
                //alert(eReturn)
                var ret = eReturn.split("~");
                var eqid = ret[4];
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("tdeq").innerHTML = ret[5];
                document.getElementById("lbleq").value = ret[5];
                document.getElementById("lblsubmit").value = "checkeq";
                document.getElementById("form1").submit();
            }
        }
        function getlocs1() {
            var sid = document.getElementById("lblsid").value;
            var lid = document.getElementById("lbllid").value;
            var typ = "lup"
            //alert(lid)
            if (lid != "") {
                typ = "retloc"
            }
            var eqid = document.getElementById("lbleqid").value;
            var fuid = document.getElementById("lblfuid").value;
            var coid = document.getElementById("lblcoid").value;
            //alert(fuid)
            var wo = "";
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                clearall();
                //alert(eReturn)
                var ret = eReturn.split("~");
                var eqid = ret[3];
                document.getElementById("lbleqid").value = ret[3];
                document.getElementById("tdeq").innerHTML = ret[2];
                document.getElementById("lbleq").value = ret[2];
                document.getElementById("lblsubmit").value = "checkeq";
                document.getElementById("form1").submit();
                //was checkeq
            }
        }
        function clearall() {
            document.getElementById("lbleqid").value = "";
            document.getElementById("tdeq").innerHTML = "";
            document.getElementById("lbleq").value = "";
        }
        function proj() {
            //alert()
            var eqid = document.getElementById("lbleqid").value;
            if (eqid == "") {
                eqid = "0";
            }
            var pmid = document.getElementById("lblpmid").value;
            if (pmid == "") {
                pmid = "0";
            }
            var jump = "jump";
            alert(eqid + ", " + pmid)
            window.parent.handleproj(eqid, pmid, jump);
        }
        function setref() {
            window.parent.setref();
        }
    </script>
</head>
<body class="tbg" onload="checkpg();">
    <form id="form1" runat="server">
    <div style="position: absolute; top: 0px; left: 4px">
        <table cellspacing="0" cellpadding="0" width="740">
            <tr>
                <td width="92">
                </td>
                <td width="180">
                </td>
                <td width="20">
                </td>
                <td width="20">
                </td>
                <td width="20">
                </td>
                <td width="20">
                </td>
                <td width="20">
                </td>
                <td width="378">
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    <table>
                        <tr>
                            <td id="tddepts" class="bluelabel" runat="server">
                                Use Departments
                            </td>
                            <td>
                                <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                            <td id="tdlocs1" class="bluelabel" runat="server">
                                Use Locations
                            </td>
                            <td>
                                <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="eqdiv" height="20" runat="server">
                <td class="label">
                    <asp:Label ID="lang638" runat="server">Equipment#</asp:Label>
                </td>
                <td class="plainlabel" id="tdeq" runat="server">
                </td>
                <td>
                    <img alt="" src="../images/appbuttons/minibuttons/addmod.gif" onclick="makenew();"
                        onmouseover="return overlib('Create a New PM Record for this Equipment')" onmouseout="return nd()" />
                </td>
                <td>
                    <img id="Img6" onclick="remdat();" border="0" alt="" src="../images/appbuttons/minibuttons/refreshit.gif"
                        runat="server" onmouseover="return overlib('Clear Next Dates for all PM Records associated with this Equipment Record', ABOVE, LEFT)"
                                onmouseout="return nd()" />
                </td>
                <td>
                    <img id="Img1" onclick="delall();" border="0" alt="" src="../images/appbuttons/minibuttons/del.gif"
                        runat="server" onmouseover="return overlib('Delete all PM Records associated with this Equipment Record', ABOVE, LEFT)"
                                onmouseout="return nd()" /> 
                </td>
                <td class="label" id="tddesc" runat="server" colspan="3">
                </td>
            </tr>
        </table>
    </div>
     <iframe id="ifsave" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
    <input id="lblcid" type="hidden" runat="server" />
    <input id="lblsid" type="hidden" runat="server" />
    <input id="lblchk" type="hidden" runat="server" /><input id="lbleqid" type="hidden"
        runat="server" />
    <input id="lblpmid" type="hidden" runat="server" /><input id="lblcleantasks" type="hidden"
        name="lblcleantasks" runat="server" />
    <input id="lblgototasks" type="hidden" runat="server" /><input id="lbltaskcnt" type="hidden"
        name="lbltaskcnt" runat="server" />
    <input id="appchk" type="hidden" runat="server" /><input id="lbllog" type="hidden"
        name="lbllog" runat="server" />
    <input id="lblgetarch" type="hidden" runat="server" />
    <input id="lblpar" type="hidden" runat="server" />
    <input id="lblerr" type="hidden" runat="server" />
    <input id="lblmod" type="hidden" runat="server" />
    <input id="lbljump" type="hidden" runat="server" /><input type="hidden" id="lblcurr"
        runat="server" />
    <input type="hidden" id="lblfuid" runat="server" />
    <input type="hidden" id="lblcoid" runat="server" />
    <input type="hidden" id="lbltyp" runat="server" /><input type="hidden" id="lblsubmit"
        runat="server" />
    <input type="hidden" id="lblro" runat="server" /><input type="hidden" id="lblpiccnt"
        runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lbllid" runat="server" />
    <input type="hidden" id="lbluserid" runat="server" />
    <input type="hidden" id="lblislabor" runat="server" />
    <input type="hidden" id="lblisplanner" runat="server" />
    <input type="hidden" id="lblcadm" runat="server" />
    <input type="hidden" id="lblappstr" runat="server" />
    </form>
</body>
</html>
