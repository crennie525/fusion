﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class MasterPage1

    '''<summary>
    '''HeadPlaceHolder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HeadPlaceHolder As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''rightLogo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rightLogo As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''irItem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents irItem As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''PlaceHolder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PlaceHolder As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''TrackerPlaceHolder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TrackerPlaceHolder As Global.System.Web.UI.WebControls.ContentPlaceHolder
End Class
