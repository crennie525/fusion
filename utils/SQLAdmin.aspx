<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SQLAdmin.aspx.vb" Inherits="lucy_r12.SQLAdmin" %>
<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>SQLAdmin</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/SQLAdminaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table style="Z-INDEX: 1; POSITION: absolute; TOP: 81px; LEFT: 740px" cellSpacing="0" cellPadding="2"
				width="350">
				<tr height="20">
					<td class="thdrsinglft" width="22"><img src="../images/appbuttons/minibuttons/db.gif" border="0"></td>
					<td class="thdrsingrt label" width="328" bgColor="#1d11ef"><asp:Label id="lang3711" runat="server">Database Tables\Custom Views</asp:Label></td>
				</tr>
				<tr height="20">
					<td colspan="2"><iframe id="ifarch" style="BORDER-BOTTOM-STYLE: none; PADDING-BOTTOM: 0px; BORDER-RIGHT-STYLE: none; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; BORDER-TOP-STYLE: none; BORDER-LEFT-STYLE: none; PADDING-TOP: 0px"
							src="tables.aspx" frameBorder="no" width="350" scrolling="yes" height="500" runat="server"></iframe>
					</td>
				</tr>
			</table>
			<table style="Z-INDEX: 1; POSITION: absolute; TOP: 81px; LEFT: 7px" cellSpacing="0" cellPadding="2"
				width="722">
				<tr height="20">
					<td class="thdrsinglft" width="22"><img src="../images/appbuttons/minibuttons/db.gif" border="0"></td>
					<td class="thdrsingrt label" width="700" bgColor="#1d11ef" colSpan="3"><asp:Label id="lang3712" runat="server">SQL Analyzer</asp:Label></td>
				</tr>
				<tr height="54">
					<td vAlign="top" colSpan="4"><iframe id="ifq" style="BORDER-BOTTOM-STYLE: none; PADDING-BOTTOM: 0px; BORDER-RIGHT-STYLE: none; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; BORDER-TOP-STYLE: none; BORDER-LEFT-STYLE: none; PADDING-TOP: 0px"
							src="query.aspx?table=" frameBorder="no" width="720" scrolling="yes" height="500" runat="server">
						</iframe>
					</td>
				</tr>
			</table>
			<input id="lbltable" type="hidden" runat="server"> <input type="hidden" id="lblsubmit" runat="server">
			<input type="hidden" id="lbllog" runat="server"> <input type="hidden" id="lblsess" runat="server">
			<uc1:mmenu1 id="Mmenu11" runat="server"></uc1:mmenu1>
			<input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
