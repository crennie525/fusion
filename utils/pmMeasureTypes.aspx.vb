

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmMeasureTypes
    Inherits System.Web.UI.Page
	Protected WithEvents lang3676 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3675 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim mt As New Utilities
    Dim sql As String
    Protected WithEvents ibtnadd As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdnotypes As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trmsg As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trtypes As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents ibtnret As System.Web.UI.HtmlControls.HtmlImage
    Dim cid As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rptrtypes As System.Web.UI.WebControls.Repeater
    Protected WithEvents txtnewmeas As System.Web.UI.WebControls.TextBox
    Protected WithEvents Imagebutton1 As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            cid = "0" 'HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            mt.Open()
            LoadTypes(cid)
            mt.Dispose()

        End If
        'ibtnret.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ibtnret.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Sub LoadTypes(ByVal cid As String)
        sql = "select * from pmMeasureTypes where compid = '" & cid & "'"
        dr = mt.GetRdrData(sql)
        If dr.HasRows Then
            rptrtypes.DataSource = dr
            rptrtypes.DataBind()
            tdnotypes.InnerHtml = ""
            trmsg.Attributes.Add("class", "details")
            trtypes.Attributes.Add("class", "view")
        Else
            tdnotypes.InnerHtml = "No Types Loaded Yet"
            trmsg.Attributes.Add("class", "view")
            trtypes.Attributes.Add("class", "details")
        End If

        dr.Close()
    End Sub

    Private Sub ibtnadd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnadd.Click
        Dim meas As String = txtnewmeas.Text
        cid = lblcid.Value
        If Len(meas) > 0 Then
            mt.Open()
            sql = "select type from pmMeasureTypes where type = '" & meas & "'"
            Dim mcnt As Integer
            mcnt = mt.Scalar(sql)
            If mcnt = 0 Then
                sql = "insert into pmMeasureTypes (compid, type) values('" & cid & "', '" & meas & "')"
                mt.Update(sql)
                LoadTypes(cid)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1748" , "pmMeasureTypes.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            mt.Dispose()
        End If
    End Sub

    Private Sub rptrtypes_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtypes.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                         e.Item.ItemType <> ListItemType.Footer Then
            Dim deleteButton As ImageButton
            If e.Item.ItemType = ListItemType.Item Then
                deleteButton = CType(e.Item.FindControl("ibtndel1"), ImageButton)
            ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
                deleteButton = CType(e.Item.FindControl("ibtndel2"), ImageButton)
            End If
            'We can now add the onclick event handler
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete this Measure Type?')"

        End If
    
If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
		Try
                Dim lang3675 As Label
			lang3675 = CType(e.Item.FindControl("lang3675"), Label)
			lang3675.Text = axlabs.GetASPXPage("pmMeasureTypes.aspx","lang3675")
		Catch ex As Exception
		End Try
		Try
                Dim lang3676 As Label
			lang3676 = CType(e.Item.FindControl("lang3676"), Label)
			lang3676.Text = axlabs.GetASPXPage("pmMeasureTypes.aspx","lang3676")
		Catch ex As Exception
		End Try

End If

 End Sub

    Private Sub rptrtypes_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrtypes.ItemCommand
        If e.CommandName = "Delete" Then
            Dim mtid As String
            If e.Item.ItemType = ListItemType.Item Then
                mtid = CType(e.Item.FindControl("lbltid"), Label).Text
            ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
                mtid = CType(e.Item.FindControl("lbltidalt"), Label).Text
            End If
            sql = "delete from pmMeasureTypes where typeid = '" & mtid & "'"
            mt.Open()
            mt.Update(sql)
            cid = lblcid.Value
            LoadTypes(cid)
            mt.Dispose()
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3675.Text = axlabs.GetASPXPage("pmMeasureTypes.aspx", "lang3675")
        Catch ex As Exception
        End Try
        Try
            lang3676.Text = axlabs.GetASPXPage("pmMeasureTypes.aspx", "lang3676")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                ibtnret.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtnret.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtnret.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtnret.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtnret.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
