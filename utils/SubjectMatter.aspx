<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SubjectMatter.aspx.vb" Inherits="lucy_r12.SubjectMatter" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>SubjectMatter</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/SubjectMatteraspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  class="tbg" onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table width="320" cellSpacing="0" cellPadding="0">
				<tr>
					<td align="right" colSpan="4"><IMG onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif" id="ibtnreturn"
							runat="server" width="69" height="19"></td>
				</tr>
				<tr>
					<td colSpan="4"><asp:label id="lblpre" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"
							Width="328px" ForeColor="Red"></asp:label></td>
				</tr>
				<tr>
					<td colSpan="4"><asp:datagrid id="dgsm" runat="server" AutoGenerateColumns="False" AllowCustomPaging="True" AllowPaging="True"
							GridLines="None" CellPadding="0" ShowFooter="True" CellSpacing="1">
							<FooterStyle BackColor="transparent"></FooterStyle>
							<AlternatingItemStyle BackColor="#E7F1FD"></AlternatingItemStyle>
							<ItemStyle Font-Size="X-Small" Font-Names="Arial" BackColor="transparent"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<FooterTemplate>
										<asp:ImageButton id="ImageButton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
											CommandName="Add"></asp:ImageButton>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<ItemTemplate>
										<asp:Label id=Label23 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.smid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id=lblsmid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.smid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Subject Matter">
									<HeaderStyle Width="220px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										&nbsp;
										<asp:Label id=Label24 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subj") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id=txtnewsm runat="server" Width="230px" MaxLength="250" Text='<%# DataBinder.Eval(Container, "DataItem.subj") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										&nbsp;
										<asp:TextBox id=txtsme runat="server" Width="230px" MaxLength="250" Text='<%# DataBinder.Eval(Container, "DataItem.subj") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="30px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<IMG alt="" src="../images/appbuttons/minibuttons/del.gif" width="16" height="16">
									</HeaderTemplate>
									<ItemTemplate>
										&nbsp;
										<asp:ImageButton id="Imagebutton25" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
											CommandName="Delete"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False"></PagerStyle>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td colSpan="2" style="HEIGHT: 17px"><asp:label id="lblpgac" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"
							ForeColor="Blue"></asp:label></td>
					<td align="left" style="WIDTH: 79px; HEIGHT: 17px"><asp:imagebutton id="prePrev" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bprev.gif"></asp:imagebutton></td>
					<td align="right" style="HEIGHT: 17px"><asp:imagebutton id="preNext" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bnext.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td width="90"></td>
					<td width="110"></td>
					<td width="60"></td>
					<td width="60"></td>
				</tr>
			</table>
			<input type="hidden" id="lblcid" runat="server" NAME="lblcid"> <input type="hidden" id="lblpg" runat="server">
			<input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
