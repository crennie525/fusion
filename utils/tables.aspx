<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tables.aspx.vb" Inherits="lucy_r12.tables" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>tables</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/tablesaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checkout();">
		<form id="form1" method="post" runat="server">
			<table cellpadding="0" cellspacing="0" style="LEFT: 0px; POSITION: absolute; TOP: 0px">
				<tr>
					<td>
						<table>
							<tr height="22">
								<td class="thdrhov label" id="tdt" onclick="getview('tdt');" width="70"><asp:Label id="lang3717" runat="server">Tables</asp:Label></td>
								<td class="thdr label" id="tdv" runat="server" onclick="getview('tdv');" width="70"><asp:Label id="lang3718" runat="server">Views</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td id="tdarch" runat="server" colspan="2"></td>
				</tr>
			</table>
			<input type="hidden" id="lblcurr" runat="server"> <input type="hidden" id="lblsubmit" runat="server">
			<input type="hidden" id="lbllog" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
