

'********************************************************
'*
'********************************************************



Public Class PMApprovalMain
    Inherits System.Web.UI.Page
	Protected WithEvents lang3672 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3671 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Login, ustr As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifappr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifarch As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Dim pid, jump As String
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("PMApprovalMain.aspx")
        Catch ex As Exception

        End Try
        If Not IsPostBack Then
            ustr = Request.QueryString("usrname").ToString
            lbluser.Value = ustr
            Dim eqid As String
            Try
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
            Catch ex As Exception
                lbleqid.Value = 0
                lbleqid.Value = 0
            End Try
            ifappr.Attributes.Add("src", "PMApproval.aspx?eqid=" & eqid)
            ifarch.Attributes.Add("src", "PMApprovalEq.aspx?ustr=" + ustr)
        End If
    End Sub

	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3671.Text = axlabs.GetASPXPage("PMApprovalMain.aspx", "lang3671")
        Catch ex As Exception
        End Try
        Try
            lang3672.Text = axlabs.GetASPXPage("PMApprovalMain.aspx", "lang3672")
        Catch ex As Exception
        End Try

    End Sub

End Class
