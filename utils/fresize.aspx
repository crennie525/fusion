﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="fresize.aspx.vb" Inherits="lucy_r12.fresize" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<script type="text/javascript">
    function GetWidth() {
        var x = 0;
        if (self.innerHeight) {
            x = self.innerWidth;
        }
        else if (document.documentElement && document.documentElement.clientHeight) {
            x = document.documentElement.clientWidth;
        }
        else if (document.body) {
            x = document.body.clientWidth;
        }
        return x;
    }

    function GetHeight() {
        var y = 0;
        if (self.innerHeight) {
            y = self.innerHeight;
        }
        else if (document.documentElement && document.documentElement.clientHeight) {
            y = document.documentElement.clientHeight;
        }
        else if (document.body) {
            y = document.body.clientHeight;
        }
        return y;
    }
    function upsize(who) {
        //document.getElementById("tdw").innerHTML = GetWidth();
        //document.getElementById("tdh").innerHTML = GetHeight();
        document.getElementById("iftd").height = GetHeight();
        window.scrollTo(0, top);
    }
    //onresize="upsize();"
    </script>
</head>
<body onload="upsize();">
    <form id="form1" runat="server">
    <div>
       <script type="text/javascript">
           document.body.onresize = function () {
               upsize('2');
           }
           self.onresize = function () {
               upsize('3');
           }
           document.documentElement.onresize = function () {
               upsize('4');
           }
           </script>
        <iframe id="iftd" src="../genhold.htm" width="100%" height="100%" frameborder="1"
            runat="server"></iframe>
        <table>
            <tr>
                <td id="tdw">
                </td>
            </tr>
            <tr>
                <td id="tdh">
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
