

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class pmMeasure
    Inherits System.Web.UI.Page
	Protected WithEvents lang3674 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3673 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim mt As New Utilities
    Dim sql As String
    Protected WithEvents lbltypeid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents bgbreturn As System.Web.UI.HtmlControls.HtmlImage
    Dim type As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtnewmeas As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnadd As System.Web.UI.WebControls.ImageButton
    Protected WithEvents rptrtypes As System.Web.UI.WebControls.Repeater
    Protected WithEvents trmsg As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdnotypes As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trtypes As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            type = Request.QueryString("type").ToString
            lbltypeid.Value = type
            mt.Open()
            LoadMeas(type)
            mt.Dispose()

        End If
    End Sub
    Private Sub LoadMeas(ByVal type As String)
        sql = "select * from pmMeasurements where typeid = '" & type & "'"
        dr = mt.GetRdrData(sql)
        If dr.HasRows Then
            rptrtypes.DataSource = dr
            rptrtypes.DataBind()
            tdnotypes.InnerHtml = ""
            trmsg.Attributes.Add("class", "details")
            trtypes.Attributes.Add("class", "view")
        Else
            tdnotypes.InnerHtml = "No Measurement Loaded Yet"
            trmsg.Attributes.Add("class", "view")
            trtypes.Attributes.Add("class", "details")
        End If

        dr.Close()
    End Sub

    Private Sub ibtnadd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnadd.Click
        Dim meas As String = txtnewmeas.Text
        type = lbltypeid.Value
        If Len(meas) > 0 Then
            mt.Open()
            sql = "select count(*) from pmMeasurements where measurement = '" & meas & "' and typeid = '" & type & "'"
            Dim mcnt As Integer
            mcnt = mt.Scalar(sql)
            If mcnt = 0 Then
                sql = "insert into pmMeasurements (typeid, measurement) values('" & type & "', '" & meas & "')"
                mt.Update(sql)
                LoadMeas(type)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1747" , "pmMeasure.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            mt.Dispose()
        End If
    End Sub

    Private Sub rptrtypes_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtypes.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                         e.Item.ItemType <> ListItemType.Footer Then
            Dim deleteButton As ImageButton
            If e.Item.ItemType = ListItemType.Item Then
                deleteButton = CType(e.Item.FindControl("ibtndel1"), ImageButton)
            ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
                deleteButton = CType(e.Item.FindControl("ibtndel2"), ImageButton)
            End If
            'We can now add the onclick event handler
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete this Measurement?')"

        End If
    
If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
		Try
                Dim lang3673 As Label
			lang3673 = CType(e.Item.FindControl("lang3673"), Label)
			lang3673.Text = axlabs.GetASPXPage("pmMeasure.aspx","lang3673")
		Catch ex As Exception
		End Try
		Try
                Dim lang3674 As Label
			lang3674 = CType(e.Item.FindControl("lang3674"), Label)
			lang3674.Text = axlabs.GetASPXPage("pmMeasure.aspx","lang3674")
		Catch ex As Exception
		End Try

End If

 End Sub

    Private Sub rptrtypes_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrtypes.ItemCommand
        If e.CommandName = "Delete" Then
            Dim mtid, meas As String
            If e.Item.ItemType = ListItemType.Item Then
                mtid = CType(e.Item.FindControl("lbltid"), Label).Text
                meas = CType(e.Item.FindControl("lblmeas"), Label).Text
            ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
                mtid = CType(e.Item.FindControl("lbltidalt"), Label).Text
                meas = CType(e.Item.FindControl("lblmeasalt"), Label).Text
            End If
            sql = "delete from pmMeasurements where measurement = '" & meas & "' and typeid = '" & mtid & "'"
            mt.Open()
            mt.Update(sql)
            type = lbltypeid.Value
            LoadMeas(type)
            mt.Dispose()
        End If
    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang3673.Text = axlabs.GetASPXPage("pmMeasure.aspx","lang3673")
		Catch ex As Exception
		End Try
		Try
			lang3674.Text = axlabs.GetASPXPage("pmMeasure.aspx","lang3674")
		Catch ex As Exception
		End Try

	End Sub

	

	

	Private Sub GetBGBLangs()
		Dim lang as String = lblfslang.value
		Try
			If lang = "eng" Then
			bgbreturn.Attributes.Add("src" , "../images2/eng/bgbuttons/return.gif")
			ElseIf lang = "fre" Then
			bgbreturn.Attributes.Add("src" , "../images2/fre/bgbuttons/return.gif")
			ElseIf lang = "ger" Then
			bgbreturn.Attributes.Add("src" , "../images2/ger/bgbuttons/return.gif")
			ElseIf lang = "ita" Then
			bgbreturn.Attributes.Add("src" , "../images2/ita/bgbuttons/return.gif")
			ElseIf lang = "spa" Then
			bgbreturn.Attributes.Add("src" , "../images2/spa/bgbuttons/return.gif")
			End If
		Catch ex As Exception
		End Try
		Try
			If lang = "eng" Then
			ibtnadd.Attributes.Add("src" , "../images2/eng/bgbuttons/badd.gif")
			ElseIf lang = "fre" Then
			ibtnadd.Attributes.Add("src" , "../images2/fre/bgbuttons/badd.gif")
			ElseIf lang = "ger" Then
			ibtnadd.Attributes.Add("src" , "../images2/ger/bgbuttons/badd.gif")
			ElseIf lang = "ita" Then
			ibtnadd.Attributes.Add("src" , "../images2/ita/bgbuttons/badd.gif")
			ElseIf lang = "spa" Then
			ibtnadd.Attributes.Add("src" , "../images2/spa/bgbuttons/badd.gif")
			End If
		Catch ex As Exception
		End Try

	End Sub

End Class


