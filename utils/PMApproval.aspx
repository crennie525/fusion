<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PMApproval.aspx.vb" Inherits="lucy_r12.PMApproval" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PMApproval</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<LINK href="../styles/reports.css" type="text/css" rel="stylesheet">
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/PMApprovalaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="load_save();" >
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 0px; POSITION: absolute; TOP: 3px" cellSpacing="1" cellPadding="0"
				width="720">
				<tr>
					<td width="140"></td>
					<td width="80"></td>
					<td width="120"></td>
					<td width="80"></td>
					<td width="120"></td>
					<td width="110"></td>
					<td width="70"></td>
				</tr>
				<tr>
					<td class="bluelabel"><asp:Label id="lang3665" runat="server">Selected Mode:</asp:Label></td>
					<td>
						<asp:DropDownList id="ddmode" runat="server" AutoPostBack="True">
							<asp:ListItem Value="0">Select Mode</asp:ListItem>
							<asp:ListItem Value="1">Current</asp:ListItem>
							<asp:ListItem Value="2">Archive</asp:ListItem>
						</asp:DropDownList></td>
					<td class="label"><asp:Label id="lang3666" runat="server">Current Revision</asp:Label></td>
					<td class="plainlabel" id="tdapprrev" runat="server"></td>
					<td class="label"><asp:Label id="lang3667" runat="server">Archive Revision</asp:Label></td>
					<td class="plainlabel" id="tdarch" runat="server"></td>
				</tr>
				<tr height="20">
					<td class="bluelabel"><asp:Label id="lang3668" runat="server">Current Equipment:</asp:Label></td>
					<td class="label" id="tdeq" colSpan="5" runat="server"></td>
					<td align="right"><IMG class="details" id="ibtnsave" onclick="saveit();" src="../images/appbuttons/bgbuttons/save.gif"
							runat="server"></td>
				</tr>
				<tr>
					<td class="bluelabel"><asp:Label id="lang3669" runat="server">Approval Phase Status:</asp:Label></td>
					<td class="label" id="tda1" runat="server"></td>
					<td class="bluelabel"><asp:Label id="lang3670" runat="server">PM Library Status:</asp:Label></td>
					<td class="label" id="tda2" runat="server" colspan="2"></td>
					<td align="right"><IMG id="imgrem" src="../images/appbuttons/bgbuttons/removeg.gif" runat="server"></td>
					<td align="right"><IMG id="imgappr" src="../images/appbuttons/bgbuttons/apprg.gif" runat="server"></td>
				</tr>
				<tr>
					<td id="tdplan" colSpan="7" runat="server"></td>
				</tr>
			</table>
			<input id="lblprojid" type="hidden" name="lblprojid" runat="server"> <input id="lbleqid" type="hidden" runat="server">
			<input id="lbleqnum" type="hidden" runat="server"> <input id="lblsave" type="hidden" runat="server">
			<input id="lblorig" type="hidden" runat="server"> <input id="lblappreqid" type="hidden" runat="server">
			<input id="lblchng" type="hidden" runat="server"><input id="lblexpand" type="hidden" runat="server">
			<input type="hidden" id="lblapprgrp" runat="server"><input type="hidden" id="lbladdrem" runat="server">
			<input type="hidden" id="lbla2" runat="server"><input type="hidden" id="lblgrplev" runat="server">
			<input type="hidden" id="lblrev" runat="Server"><input type="hidden" id="lblorev" runat="Server">
			<input type="hidden" id="lbltable" runat="server"><input id="lbllog" type="hidden" runat="server" NAME="lbllog">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
