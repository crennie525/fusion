

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class SQLAdmin
    Inherits System.Web.UI.Page
	Protected WithEvents lang3712 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3711 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sqa As New Utilities
    Protected WithEvents ifarch As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbltable As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents DataGrid1 As System.Web.UI.WebControls.DataGrid
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents ifq As System.Web.UI.HtmlControls.HtmlGenericControl
    Dim sql, Login, sessid As String
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsess As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
            sessid = HttpContext.Current.Session.SessionID
            lblsess.value = sessid
        Catch ex As Exception
            Dim redir As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
            redir = redir & "?logout=yes"
            Response.Redirect(redir)
        End Try
        If Not IsPostBack Then
            
        Else
            If Request.Form("lblsubmit") = "gettable" Then
                GetTable()
            End If
        End If
    End Sub
    Private Sub GetTable()
        sqa.Open()
        sql = "select * from " & TextBox1.Text
        TextBox1.Text = "select * from " & TextBox1.Text
        Try
            dr = sqa.GetRdrData(sql)
            DataGrid1.DataSource = dr
            DataGrid1.DataBind()
            dr.Close()
        Catch ex As Exception
            'Dim clsLogError As New LogError(Server.GetLastError, Session, Request)
            'Dim gerr As String = clsLogError.GetError
            Label1.Text = ex.Message
        End Try

        sqa.Dispose()
    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang3711.Text = axlabs.GetASPXPage("SQLAdmin.aspx","lang3711")
		Catch ex As Exception
		End Try
		Try
			lang3712.Text = axlabs.GetASPXPage("SQLAdmin.aspx","lang3712")
		Catch ex As Exception
		End Try

	End Sub

End Class
