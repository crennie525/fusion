

'********************************************************
'*
'********************************************************



Public Class tmdialog
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim taskid, comp, Login, typ, wonum, jpid, ro, rev, wojtid As String
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents iftypes As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try

        If Not IsPostBack Then
            Try
                typ = Request.QueryString("typ").ToString
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                If typ = "pm" Or typ = "tpm" Or typ = "rev" Or typ = "trev" Or typ = "co" Then
                    If typ = "rev" Then
                        rev = Request.QueryString("rev").ToString
                    End If
                    taskid = Request.QueryString("taskid").ToString
                    If Len(taskid) <> 0 AndAlso taskid <> "" AndAlso taskid <> "0" Then
                        comp = Request.QueryString("coid").ToString
                        iftypes.Attributes.Add("src", "pmTaskMeasurements.aspx?typ=" & typ & "&taskid=" & taskid & "&coid=" & comp & "&ro=" & ro & "&rev=" & rev)
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr1776", "tmdialog.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        lbllog.Value = "nodeptid"
                    End If
                ElseIf typ = "wo" Then
                    wonum = Request.QueryString("wo").ToString
                    iftypes.Attributes.Add("src", "pmTaskMeasurements.aspx?typ=wo&wo=" & wonum & "&ro=" & ro)
                ElseIf typ = "jp" Or typ = "wjp" Then
                    Try
                        wojtid = Request.QueryString("wojtid").ToString
                    Catch ex As Exception

                    End Try
                    jpid = Request.QueryString("jpid").ToString
                    wonum = Request.QueryString("wo").ToString
                    taskid = Request.QueryString("taskid").ToString
                    iftypes.Attributes.Add("src", "pmTaskMeasurements.aspx?typ=" & typ & "&jpid=" & jpid & "&taskid=" & taskid & "&wo=" & wonum & "&ro=" & ro & "&wojtid=" & wojtid)
                End If

            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr1777", "tmdialog.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "nodeptid"
            End Try

        End If

    End Sub

End Class
