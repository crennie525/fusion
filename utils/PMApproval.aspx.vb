

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class PMApproval
    Inherits System.Web.UI.Page
	Protected WithEvents lang3670 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3669 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3668 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3667 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3666 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3665 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim pp As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents tdplan As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblprojid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsave As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblorig As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappreqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchng As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sql As String
    Protected WithEvents lblexpand As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibtnsave As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tda1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tda2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgrem As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgappr As System.Web.UI.HtmlControls.HtmlImage
    Dim eqid, apprgrp, Login As String
    Protected WithEvents lblapprgrp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbla2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgrplev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddmode As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tdapprrev As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbladdrem As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblorev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltable As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim rev As Integer
    Protected WithEvents tdarch As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim tbl As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Dim pid, jump As String
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try

        If Not IsPostBack Then
            ddmode.Enabled = False
            Try
                eqid = Request.QueryString("eqid").ToString
                apprgrp = HttpContext.Current.Session("apprgrp").ToString
                lbleqid.Value = eqid
                lblapprgrp.Value = apprgrp
                If eqid <> "0" And eqid <> "" Then
                    pp.Open()
                    rev = pp.Rev()
                    tdapprrev.InnerHtml = rev
                    tdarch.InnerHtml = "N/A"
                    lbltable.Value = "pmApprovalEq"
                    lblrev.Value = rev
                    CheckAppr(apprgrp)
                    CheckEq(eqid)
                    CheckRev()
                    'ddmode.Enabled = True
                    pp.Dispose()
                Else
                    tdeq.InnerHtml = "None Selected"
                End If
            Catch ex As Exception
                tdeq.InnerHtml = "None Selected"
            End Try

            'If eqid <> 0 Then

            'Else
            'tdeq.InnerHtml = "None Selected"
            'End If
        Else
            If Request.Form("lblchng") = "ADIS" Then
                lblchng.Value = ""
                pp.Open()
                NATask("1")
                pp.Dispose()
            ElseIf Request.Form("lblchng") = "AEN" Then
                lblchng.Value = ""
                pp.Open()
                NATask("0")
                pp.Dispose()
            ElseIf Request.Form("lblchng") = "ASAV" Then
                lblchng.Value = ""
                pp.Open()
                SaveTasks()
                pp.Dispose()
            End If
        End If
        'ibtnsave.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
        'ibtnsave.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")
    End Sub
    Private Sub ddmode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddmode.SelectedIndexChanged
        eqid = lbleqid.Value
        If ddmode.SelectedIndex <> 0 And eqid <> "" Then
            If ddmode.SelectedValue = "1" Then
                pp.Open()
                rev = pp.Rev()
                tdapprrev.InnerHtml = rev
                lblrev.Value = rev
                tdarch.InnerHtml = "N/A"
                lbltable.Value = "pmApprovalEq"
                CheckEq(eqid)
                LoadProj(eqid)
                pp.Dispose()
            Else
                pp.Open()
                rev = pp.Rev()
                tdapprrev.InnerHtml = rev
                lblrev.Value = rev
                tdarch.InnerHtml = lblorev.Value
                lbltable.Value = "pmApprovalEqArch"
                GetOStat(eqid)
                LoadProj(eqid)
                pp.Dispose()
            End If
        End If
    End Sub
    Private Sub GetOStat(ByVal eqid As String)
        rev = lblorev.Value
        Dim a1, a2 As String
        sql = "select apprstg1, apprstg2 from pmApprArchEq where eqid = '" & eqid & "' and rev = '" & rev & "'"
        dr = pp.GetRdrData(sql)
        While dr.Read
            a1 = dr.Item("apprstg1").ToString
            a2 = dr.Item("apprstg2").ToString
        End While
        dr.Close()
        If a1 = "1" Then
            tda1.InnerHtml = "Completed"
        Else
            tda1.InnerHtml = "Incomplete"
        End If
        If a2 = "1" Then
            tda2.InnerHtml = "Approved"
            lbla2.Value = "1"
        Else
            tda2.InnerHtml = "Not Approved"
            lbla2.Value = "0"
        End If
    End Sub
    Private Sub CheckRev()
        Dim revchk As Integer
        eqid = lbleqid.Value
        sql = "select max(rev) from pmApprovalEqArch where eqid = '" & eqid & "'"
        Try
            revchk = pp.Scalar(sql)
            lblorev.Value = revchk
            ddmode.SelectedIndex = 1
            ddmode.Enabled = True
        Catch ex As Exception
            ddmode.SelectedIndex = 1
            ddmode.Enabled = False
        End Try


    End Sub
    Private Sub NATask(ByVal dir As String)
        Dim nastr As String = lblappreqid.Value
        Dim naarr As String() = Split(nastr, "/")
        Dim typ, aid, id As String
        typ = naarr(0).ToString
        aid = naarr(1).ToString
        id = naarr(2).ToString
        Dim pmaid, pmacid, pmtid As String
        eqid = lbleqid.Value
        Dim eqchk As Integer
        sql = "usp_saveEqApprNA '" & typ & "', '" & dir & "', '" & aid & "', '" & id & "', '" & eqid & "'"
        'If typ = "na" Or typ = "pma" Then
        'pmaid
        'sql = "update pmApprovalEq set apprna = '" & dir & "' where eqid = '" & eqid & "' " _
        '+ "and pmaid = '" & id & "'"
        'ElseIf typ = "nac" Or typ = "pmac" Then
        'pmacid
        'sql = "update pmApprovalEq set pmacidna = '" & dir & "' where eqid = '" & eqid & "' " _
        '+ "and pmacid = '" & id & "'"
        ' ElseIf typ = "nat" Or typ = "pmt" Then
        'pmtid
        'sql = "update pmApprovalEq set pmtidna = '" & dir & "' where eqid = '" & eqid & "' " _
        '+ "and pmtid = '" & id & "'"
        'End If
        pp.Update(sql)
        eqchk = pp.Scalar(sql)
        CheckStat(eqchk, "0")
        'If eqchk = 1 Then
        'tda1.InnerHtml = "Completed"
        'Else
        'tda1.InnerHtml = "Incomplete"
        'End If
        LoadProj(eqid)
    End Sub
    Private Sub SaveTasks()
        Dim appr, chng, pma, pmac, pmt As String
        Dim eqchk As Integer
        eqid = lbleqid.Value
        Dim savstr As String = lblorig.Value 'lblsave.Value change test
        Dim savarr As String() = Split(savstr, "/")
        Dim i As Integer
        For i = 0 To savarr.Length - 1
            Dim arrstr As String = savarr(i)
            If Len(arrstr) <> 0 Then
                Dim arrarr() As String = Split(arrstr, "-")
                chng = arrarr(1)
                If chng = "1" Then
                    appr = arrarr(0)
                    pma = arrarr(2)
                    pmac = arrarr(3)
                    pmt = arrarr(4)
                    'sql = "update pmApprovalEq set pmaidappr = '" & pma & "', pmacidappr = '" & pmac & "', " _
                    '+ "pmtidappr = '" & pmt & "' where appreqid = '" & appr & "'"
                    sql = "usp_saveEqAppr '" & pma & "', '" & pmac & "', '" & pmt & "', '" & appr & "', '" & eqid & "'"
                    eqchk = pp.Scalar(sql)
                End If
            End If
        Next
        CheckStat(eqchk, "0")
        'If eqchk = 1 Then
        'tda1.InnerHtml = "Completed"
        'Else
        'tda1.InnerHtml = "Incomplete"
        'End If
        LoadProj(eqid)
    End Sub
    Private Sub CheckAppr(ByVal apprgrp As String)
        Dim addrem As Integer
        Dim grplev As String
        If apprgrp <> "no" Then
            sql = "select addremid, grplevel from pmApprovalGrps where apprgrp = '" & apprgrp & "'"
            dr = pp.GetRdrData(sql)
            While dr.Read
                addrem = dr.Item("addremid").ToString
                grplev = dr.Item("grplevel").ToString
            End While
            dr.Close()
            lbladdrem.Value = addrem
            lblgrplev.Value = grplev
        End If

        
    End Sub
    Private Sub CheckEq(ByVal eqid As String)
        rev = lblrev.Value
        Dim a1, a2 As String
        sql = "select apprstg1, apprstg2 from equipment where eqid = '" & eqid & "'"
        dr = pp.GetRdrData(sql)
        While dr.Read
            a1 = dr.Item("apprstg1").ToString
            a2 = dr.Item("apprstg2").ToString
        End While
        dr.Close()

        CheckStat(a1, a2)

        sql = "select count(*) from pmApprovalEq where eqid = '" & eqid & "'"
        Dim eqcnt As Integer
        eqcnt = pp.Scalar(sql)
        If eqcnt > 0 Then
            LoadProj(eqid)
        Else
            sql = "usp_AddApprEq '" & eqid & "', '" & rev & "'"
            pp.Update(sql)
            LoadProj(eqid)
        End If
    End Sub
    Private Sub CheckStat(ByVal a1 As String, ByVal a2 As String)
        If a1 = "1" Then
            tda1.InnerHtml = "Completed"
        Else
            tda1.InnerHtml = "Incomplete"
        End If
        If a2 = "1" Then
            tda2.InnerHtml = "Approved"
            lbla2.Value = "1"
        Else
            tda2.InnerHtml = "Not Approved"
            lbla2.Value = "0"
        End If

        Dim addrem As String = lbladdrem.Value
        If addrem = "2" Then
            If a2 = "1" And a2 = "1" Then
                imgrem.Attributes.Add("src", "../images/appbuttons/bgbuttons/remove.gif")
                'imgrem.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/removey.gif'")
                'imgrem.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/remove.gif'")
                imgrem.Attributes.Add("onclick", "remlib();")
                imgappr.Attributes.Add("src", "../images/appbuttons/bgbuttons/apprg.gif")
            ElseIf a2 = "0" And a1 = "1" Then
                imgappr.Attributes.Add("src", "../images/appbuttons/bgbuttons/appr.gif")
                'imgappr.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/appry.gif'")
                'imgappr.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/appr.gif'")
                imgappr.Attributes.Add("onclick", "addlib();")
                imgrem.Attributes.Add("src", "../images/appbuttons/bgbuttons/removeg.gif")
            End If
        End If
    End Sub
    Private Sub LoadProj(ByVal eqid As Integer)
        Dim sb As New StringBuilder
        Dim proj As String = lblprojid.Value
        tbl = lbltable.Value
        Dim archchk As String = ddmode.SelectedValue

        If tbl = "pmApprovalEq" Then
            rev = lblrev.Value
        Else
            rev = lblorev.Value
        End If
        sb.Append("<table cellspacing=""0"" width=""710"" border=""0""><tr><td width=""10px"">&nbsp;</td>" & vbCrLf)
        sb.Append("<td class=""btmmenu plainlabel"" width=""390px"">" & tmod.getlbl("cdlbl1269" , "PMApproval.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""btmmenu plainlabel"" width=""110px"">" & tmod.getlbl("cdlbl1270" , "PMApproval.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""btmmenu plainlabel"" width=""130px"">" & tmod.getlbl("cdlbl1271" , "PMApproval.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""btmmenu plainlabel"" width=""70px"">" & tmod.getlbl("cdlbl1272" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
        sql = "usp_GetApprEq '" & eqid & "', '" & tbl & "', '" & rev & "'"
        'sql = "select a.pmaid, a.listitem, a.apprgrp, b.pmacid, b.checkorder, b.checkitem, " _
        '+ "cntb = (select count(*) from pmApprovalChk b where b.pmaid = a.pmaid), " _
        '+ "cntt = (select count(*) from pmApprovalTasks c where c.pmacid = b.pmacid), " _
        '+ "c.pmtid, c.taskorder, c.apprtask, d.grpname from pmApproval a left outer join pmApprovalChk b " _
        '+ "on b.pmaid = a.pmaid left outer join pmApprovalTasks c on c.pmacid = b.pmacid " _
        '+ "left outer join pmApprovalGrps d on d.apprgrp = a.apprgrp order by a.listorder, b.checkorder, c.taskorder "

        dr = pp.GetRdrData(sql)

        Dim rowflag As Integer = 0
        Dim rowflag1 As Integer = 0
        Dim rowflag2 As Integer = 0
        Dim bg As String

        Dim gi As String = "0"
        Dim bid As String = "0"
        Dim bidchk As String = "0"
        Dim tid As String = "0"

        Dim start As String = "0"
        Dim start1 As String = "0"
        Dim bimg As String
        lblsave.Value = ""
        Dim bcnt As Integer = 0
        Dim tcnt As Integer = 0
        Dim cntb, cntt As Integer
        Dim apprid, appr, apprna, pmaidappr, pmacidappr, pmtidappr, aptot As Integer
        Dim pmacna, pmtna, subappr As Integer
        Dim arrcnt As Integer = -1
        Dim savestr As String
        Dim grplev As String = lblgrplev.Value
        Dim a2 As String = lbla2.Value
        Dim tgrplev As String = ""
        Dim dis As String = ""
        While dr.Read
            lbleqnum.Value = dr.Item("eqnum").ToString
            appr = dr.Item("approved").ToString
            subappr = dr.Item("subappr").ToString
            '*** save array ***
            arrcnt = arrcnt + 1
            apprid = dr.Item("appreqid").ToString
            savestr += apprid & "-"
            'lblsave.Value += apprid & "-chng-0" & "/"
            savestr += "0" & "-"
            apprna = dr.Item("apprna").ToString
            'lblsave.Value += apprid & "-na-" & apprna & "/"
            pmaidappr = dr.Item("pmaidappr").ToString
            'lblsave.Value += apprid & "-pma-" & pmaidappr & "/"
            savestr += pmaidappr & "-"
            pmacna = dr.Item("pmacidna").ToString
            'lblsave.Value += apprid & "-nac-" & pmacna & "/"
            pmacidappr = dr.Item("pmacidappr").ToString
            'lblsave.Value += apprid & "-pmac-" & pmacidappr & "/"
            savestr += pmacidappr & "-"
            pmtna = dr.Item("pmtidna").ToString
            'lblsave.Value += apprid & "-nat-" & pmtna & "/"
            pmtidappr = dr.Item("pmtidappr").ToString
            'lblsave.Value += apprid & "-pmt-" & pmtidappr & "/"
            savestr += pmtidappr.ToString
            lblsave.Value += savestr & "/"
            savestr = ""
            '*** end save array ***
            tgrplev = dr.Item("grplev").ToString
            If grplev < tgrplev OrElse tgrplev = "0" OrElse a2 = "1" OrElse archchk = "2" Then
                dis = "disabled"
            Else
                dis = ""
            End If
            bid = dr.Item("pmacid").ToString
            If dr.Item("pmaid").ToString <> gi Then
                If rowflag = 0 Then
                    bg = "white"
                    rowflag = 1
                Else
                    bg = "#e7f1fd"
                    rowflag = "0"
                End If
                gi = dr.Item("pmaid").ToString
                cntb = dr.Item("cntb").ToString
                cntt = dr.Item("cntt").ToString
                aptot = dr.Item("apprtot").ToString
                If apprna = "1" Then
                    sb.Append("<tr bgcolor=""" & bg & """><td><img id='1i" + gi + "' src=""../images/appbuttons/bgbuttons/minus.gif"" ></td>" & vbCrLf)
                Else
                    If cntb <> 0 Then
                        sb.Append("<tr bgcolor=""" & bg & """><td><img id='1i" + gi + "' src=""../images/appbuttons/bgbuttons/plus.gif"" " & vbCrLf)
                        sb.Append("onclick=""fclose('1t" + gi + "', '1i" + gi + "');""></td>" & vbCrLf)
                    Else
                        sb.Append("<tr bgcolor=""" & bg & """><td><img id='1i" + gi + "' src=""../images/appbuttons/bgbuttons/minus.gif"" ></td>" & vbCrLf)
                    End If
                End If

                sb.Append("<td class=""plainlabel"">" & dr.Item("listitem").ToString & "</td>" & vbCrLf)
                sb.Append("<td class=""plainlabel"">" & dr.Item("grpname").ToString & "</td>" & vbCrLf)

                If apprna = "1" Then
                    sb.Append("<td class=""plainlabel""><input type='checkbox' id='na" + gi + "' onclick=""checkbox('na" + gi + "','pma" + gi + "','" + gi + "','na','" & apprid & "');"" CHECKED disabled>N/A" & vbCrLf)
                    sb.Append("<input type='checkbox' id='pma" + gi + "' onclick=""checkbox('pma" + gi + "','na" + gi + "','" + gi + "','pma','" & apprid & "','dis');"" " & dis & ">Enable</td>" & vbCrLf)
                    sb.Append("<td class=""plainlabel"" id='tdpma" + gi + "'>N/A</td></tr>" & vbCrLf)
                Else
                    If cntb = 0 Then
                        'Treat Phase as a task
                        If pmaidappr = 1 Then
                            sb.Append("<td class=""plainlabel""><input type='checkbox' id='na" + gi + "' onclick=""checkbox('na" + gi + "','pma" + gi + "','" + gi + "','na','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                            sb.Append("<input type='checkbox' id='pma" + gi + "' onclick=""checkbox('pma" + gi + "','na" + gi + "','" + gi + "','pma','" & apprid & "','" & arrcnt & "');"" CHECKED " & dis & ">Completed</td>" & vbCrLf)
                            sb.Append("<td class=""plainlabel"" id='tdpma" + gi + "'>" & tmod.getlbl("cdlbl1274" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td class=""plainlabel""><input type='checkbox' id='na" + gi + "' onclick=""checkbox('na" + gi + "','pma" + gi + "','" + gi + "','na','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                            sb.Append("<input type='checkbox' id='pma" + gi + "' onclick=""checkbox('pma" + gi + "','na" + gi + "','" + gi + "','pma','" & apprid & "','" & arrcnt & "');"" " & dis & ">Completed</td>" & vbCrLf)
                            sb.Append("<td class=""plainlabel"" id='tdpma" + gi + "'>" & tmod.getlbl("cdlbl1275" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                        End If

                    Else
                        'Treat Phase as a Phase and let save proc calculate if complete or incomplete
                        If pmaidappr = 1 Then
                            sb.Append("<td class=""plainlabel""><input type='checkbox' id='na" + gi + "' onclick=""checkbox('na" + gi + "','pma" + gi + "','" + gi + "','na','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                            sb.Append("<input type='checkbox' id='pma" + gi + "' onclick=""checkbox('pma" + gi + "','na" + gi + "','" + gi + "','pma','" & apprid & "');"" CHECKED disabled>Completed</td>" & vbCrLf)
                            sb.Append("<td class=""plainlabel"" id='tdpma" + gi + "'>" & tmod.getlbl("cdlbl1276" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td class=""plainlabel""><input type='checkbox' id='na" + gi + "' onclick=""checkbox('na" + gi + "','pma" + gi + "','" + gi + "','na','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                            sb.Append("<input type='checkbox' id='pma" + gi + "' onclick=""checkbox('pma" + gi + "','na" + gi + "','" + gi + "','pma','" & apprid & "' );"" disabled>Completed</td>" & vbCrLf)
                            sb.Append("<td class=""plainlabel"" id='tdpma" + gi + "'>" & tmod.getlbl("cdlbl1277" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                        End If
                    End If
                End If


                If bid <> "" AndAlso bid <> "0" Then
                    bcnt = bcnt + 1
                    cntb = dr.Item("cntb").ToString
                    cntt = dr.Item("cntt").ToString
                    tid = dr.Item("pmtid").ToString
                    bidchk = dr.Item("pmacid").ToString
                    If tid <> "" Then
                        bimg = "../images/appbuttons/bgbuttons/plus.gif"
                    Else
                        bimg = "../images/appbuttons/bgbuttons/minus.gif"
                    End If
                    sb.Append("<tr class=""details"" id='1t" + gi + "'><td>&nbsp;</td><td colspan=""4"" align=""right"">" & vbCrLf)
                    sb.Append("<table border=""0"" width=""690"" cellspacing=""0"">")

                    sb.Append("<tr><td width=""10""></td>" & vbCrLf)
                    sb.Append("<td class=""tblmenuyrt plainlabel"" width=""40"">" & tmod.getlbl("cdlbl1278" , "PMApproval.aspx.vb") & "</td>" & vbCrLf)
                    sb.Append("<td class=""tblmenuyrt plainlabel"" width=""430"">" & tmod.getlbl("cdlbl1279" , "PMApproval.aspx.vb") & "</td>" & vbCrLf)
                    sb.Append("<td class=""tblmenuyrt plainlabel"" width=""130px"">" & tmod.getlbl("cdlbl1280" , "PMApproval.aspx.vb") & "</td>" & vbCrLf)
                    sb.Append("<td class=""tblmenuyrt plainlabel"" width=""70px"">" & tmod.getlbl("cdlbl1281" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)

                    If pmacna = "1" Then
                        sb.Append("<tr><td><img id='2i" + bid + "' src=""" & bimg & """ " & vbCrLf)
                        sb.Append("></td>" & vbCrLf)
                    Else
                        If cntt <> 0 Then
                            sb.Append("<tr><td><img id='2i" + bid + "' src=""" & bimg & """ " & vbCrLf)
                            sb.Append("onclick=""fclose('2t" + bid + "', '2i" + bid + "');""></td>" & vbCrLf)
                        Else
                            sb.Append("<tr><td><img id='2i" + bid + "' src=""" & bimg & """ " & vbCrLf)
                            sb.Append("></td>" & vbCrLf)
                        End If
                    End If

                    sb.Append("<td class=""plainlabel"" align=""center"">" & dr.Item("checkorder").ToString & "</td>" & vbCrLf)
                    sb.Append("<td class=""plainlabel"">" & dr.Item("checkitem").ToString & "</td>" & vbCrLf)

                    If pmacna = "1" Then
                        sb.Append("<td class=""plainlabel""><input type='checkbox' id='nac" + bid + "' onclick=""checkbox('nac" + bid + "','pmac" + bid + "','" + bid + "','nac','" & apprid & "');"" CHECKED disabled>N/A" & vbCrLf)
                        sb.Append("<input type='checkbox' id='pmac" + bid + "' onclick=""checkbox('pmac" + bid + "','nac" + bid + "','" + bid + "','pmac','" & apprid & "','dis');"" " & dis & ">Enable</td>" & vbCrLf)
                        sb.Append("<td class=""plainlabel"" id='tdpmac" + bid + "'>N/A</td></tr>" & vbCrLf)
                    Else
                        If cntt = 0 Then
                            'Treat High Level Task as a task
                            If pmacidappr = 1 Then
                                sb.Append("<td class=""plainlabel""><input type='checkbox' id='nac" + bid + "' onclick=""checkbox('nac" + bid + "','pmac" + bid + "','" + bid + "','nac','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                                sb.Append("<input type='checkbox' id='pmac" + bid + "' onclick=""checkbox('pmac" + bid + "','nac" + bid + "','" + bid + "','pmac','" & apprid & "','" & arrcnt & "');"" CHECKED " & dis & ">Completed</td>" & vbCrLf)
                                sb.Append("<td class=""plainlabel"" id='tdpmac" + bid + "'>" & tmod.getlbl("cdlbl1283" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                            Else
                                sb.Append("<td class=""plainlabel""><input type='checkbox' id='nac" + bid + "' onclick=""checkbox('nac" + bid + "','pmac" + bid + "','" + bid + "','nac','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                                sb.Append("<input type='checkbox' id='pmac" + bid + "' onclick=""checkbox('pmac" + bid + "','nac" + bid + "','" + bid + "','pmac','" & apprid & "','" & arrcnt & "');"" " & dis & ">Completed</td>" & vbCrLf)
                                sb.Append("<td class=""plainlabel"" id='tdpmac" + bid + "'>" & tmod.getlbl("cdlbl1284" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                            End If

                        Else
                            'Treat High Level Task like a Phase until sub tasks are completed
                            If subappr = 1 Then
                                If pmacidappr = 1 Then
                                    sb.Append("<td class=""plainlabel""><input type='checkbox' id='nac" + bid + "' onclick=""checkbox('nac" + bid + "','pmac" + bid + "','" + bid + "','nac','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                                    sb.Append("<input type='checkbox' id='pmac" + bid + "' onclick=""checkbox('pmac" + bid + "','nac" + bid + "','" + bid + "','pmac','" & apprid & "','" & arrcnt & "');"" CHECKED " & dis & ">Completed</td>" & vbCrLf)
                                    sb.Append("<td class=""plainlabel"" id='tdpmac" + bid + "'>" & tmod.getlbl("cdlbl1285" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                                Else
                                    sb.Append("<td class=""plainlabel""><input type='checkbox' id='nac" + bid + "' onclick=""checkbox('nac" + bid + "','pmac" + bid + "','" + bid + "','nac','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                                    sb.Append("<input type='checkbox' id='pmac" + bid + "' onclick=""checkbox('pmac" + bid + "','nac" + bid + "','" + bid + "','pmac','" & apprid & "','" & arrcnt & "');"" " & dis & ">Completed</td>" & vbCrLf)
                                    sb.Append("<td class=""plainlabel"" id='tdpmac" + bid + "'>" & tmod.getlbl("cdlbl1286" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                                End If
                            Else
                                sb.Append("<td class=""plainlabel""><input type='checkbox' id='nac" + bid + "' onclick=""checkbox('nac" + bid + "','pmac" + bid + "','" + bid + "','nac','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                                sb.Append("<input type='checkbox' id='pmac" + bid + "' onclick=""checkbox('pmac" + bid + "','nac" + bid + "','" + bid + "','pmac','" & apprid & "');"" disabled>Completed</td>" & vbCrLf)
                                sb.Append("<td class=""plainlabel"" id='tdpmac" + bid + "'>" & tmod.getlbl("cdlbl1287" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                            End If


                        End If
                    End If

                    If cntb = bcnt Then
                        bcnt = 0
                        sb.Append("</td></tr></table>")
                    End If
                End If
                If tid <> "" AndAlso tid <> "0" Then
                    tcnt = tcnt + 1
                    cntt = dr.Item("cntt").ToString

                    sb.Append("<tr class=""details"" id='2t" + bid + "' align=""right""><td colspan=""5"">" & vbCrLf)
                    sb.Append("<table border=""0"" width=""670"" cellspacing=""0"">")

                    sb.Append("<tr><td class=""tblmenugrt plainlabel"" width=""40"">" & tmod.getlbl("cdlbl1288" , "PMApproval.aspx.vb") & "</td>" & vbCrLf)
                    sb.Append("<td class=""tblmenugrt plainlabel"" width=""420"">" & tmod.getlbl("cdlbl1289" , "PMApproval.aspx.vb") & "</td>" & vbCrLf)
                    sb.Append("<td class=""tblmenugrt plainlabel"" width=""135px"">" & tmod.getlbl("cdlbl1290" , "PMApproval.aspx.vb") & "</td>" & vbCrLf)
                    sb.Append("<td class=""tblmenugrt plainlabel"" width=""75px"">" & tmod.getlbl("cdlbl1291" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)

                    sb.Append("<tr><td class=""plainlabel"" align=""center"" valign=""top"">" & dr.Item("taskorder").ToString & "</td>" & vbCrLf)
                    sb.Append("<td class=""plainlabel"" >" & dr.Item("apprtask").ToString & "</td>" & vbCrLf)

                    If pmtna = "1" Then
                        sb.Append("<td class=""plainlabel""><input type='checkbox' id='nat" + tid + "' onclick=""checkbox('nat" + tid + "','pmt" + tid + "','" + tid + "','nat','" & apprid & "');"" CHECKED disabled>N/A" & vbCrLf)
                        sb.Append("<input type='checkbox' id='pmt" + tid + "' onclick=""checkbox('pmt" + tid + "','nat" + tid + "','" + tid + "','pmt','" & apprid & "','dis');"" " & dis & ">Enable</td>" & vbCrLf)
                        sb.Append("<td class=""plainlabel"" id='tdpmt" + tid + "'>N/A</td></tr>" & vbCrLf)
                    Else
                        If pmtidappr = 1 Then
                            sb.Append("<td class=""plainlabel""><input type='checkbox' id='nat" + tid + "' onclick=""checkbox('nat" + tid + "','pmt" + tid + "','" + tid + "','nat','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                            sb.Append("<input type='checkbox' id='pmt" + tid + "' onclick=""checkbox('pmt" + tid + "','nat" + tid + "','" + tid + "','pmt','" & apprid & "','" & arrcnt & "');"" CHECKED " & dis & ">Completed</td>" & vbCrLf)
                            sb.Append("<td class=""plainlabel"" id='tdpmt" + tid + "'>" & tmod.getlbl("cdlbl1293" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td class=""plainlabel""><input type='checkbox' id='nat" + tid + "' onclick=""checkbox('nat" + tid + "','pmt" + tid + "','" + tid + "','nat','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                            sb.Append("<input type='checkbox' id='pmt" + tid + "' onclick=""checkbox('pmt" + tid + "','nat" + tid + "','" + tid + "','pmt','" & apprid & "','" & arrcnt & "');"" " & dis & ">Completed</td>" & vbCrLf)
                            sb.Append("<td class=""plainlabel"" id='tdpmt" + tid + "'>" & tmod.getlbl("cdlbl1294" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                        End If

                    End If

                    If cntt = tcnt Then
                        tcnt = 0
                        sb.Append("</td></tr></table>")
                    End If

                End If

            Else
                tid = dr.Item("pmtid").ToString
                'pmtidappr = dr.Item("pmacidappr").ToString
                If bid <> bidchk Then
                    bcnt = bcnt + 1
                    cntb = dr.Item("cntb").ToString
                    cntt = dr.Item("cntt").ToString
                    bidchk = dr.Item("pmacid").ToString
                    If rowflag1 = 0 Then
                        bg = "#FCEED4"
                        rowflag1 = 1
                    Else
                        bg = "white"
                        rowflag1 = "0"
                    End If
                    If tid <> "" Then
                        bimg = "../images/appbuttons/bgbuttons/plus.gif"
                    Else
                        bimg = "../images/appbuttons/bgbuttons/minus.gif"
                    End If
                    If pmacna = "1" Then
                        sb.Append("<tr><td><img id='2i" + bid + "' src=""" & bimg & """ " & vbCrLf)
                        sb.Append("></td>" & vbCrLf)
                    Else
                        If cntt <> 0 Then
                            sb.Append("<tr><td><img id='2i" + bid + "' src=""" & bimg & """ " & vbCrLf)
                            sb.Append("onclick=""fclose('2t" + bid + "', '2i" + bid + "');""></td>" & vbCrLf)
                        Else
                            sb.Append("<tr><td><img id='2i" + bid + "' src=""" & bimg & """ " & vbCrLf)
                            sb.Append("></td>" & vbCrLf)
                        End If
                    End If

                    sb.Append("<td class=""plainlabel"" align=""center"">" & dr.Item("checkorder").ToString & "</td>" & vbCrLf)
                    sb.Append("<td class=""plainlabel"">" & dr.Item("checkitem").ToString & "</td>" & vbCrLf)

                    If pmacna = "1" Then
                        sb.Append("<td class=""plainlabel""><input type='checkbox' id='nac" + bid + "' onclick=""checkbox('nac" + bid + "','pmac" + bid + "','" + bid + "','nac','" & apprid & "');"" CHECKED disabled>N/A" & vbCrLf)
                        sb.Append("<input type='checkbox' id='pmac" + bid + "' onclick=""checkbox('pmac" + bid + "','nac" + bid + "','" + bid + "','pmac','" & apprid & "','dis');"" " & dis & ">Enable</td>" & vbCrLf)
                        sb.Append("<td class=""plainlabel"" id='tdpmac" + bid + "'>N/A</td></tr>" & vbCrLf)
                    Else
                        If cntt = 0 Then
                            'Treat High Level Task as a task
                            If pmacidappr = 1 Then
                                sb.Append("<td class=""plainlabel""><input type='checkbox' id='nac" + bid + "' onclick=""checkbox('nac" + bid + "','pmac" + bid + "','" + bid + "','nac','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                                sb.Append("<input type='checkbox' id='pmac" + bid + "' onclick=""checkbox('pmac" + bid + "','nac" + bid + "','" + bid + "','pmac','" & apprid & "','" & arrcnt & "');"" CHECKED " & dis & ">Completed</td>" & vbCrLf)
                                sb.Append("<td class=""plainlabel"" id='tdpmac" + bid + "'>" & tmod.getlbl("cdlbl1296" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                            Else
                                sb.Append("<td class=""plainlabel""><input type='checkbox' id='nac" + bid + "' onclick=""checkbox('nac" + bid + "','pmac" + bid + "','" + bid + "','nac','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                                sb.Append("<input type='checkbox' id='pmac" + bid + "' onclick=""checkbox('pmac" + bid + "','nac" + bid + "','" + bid + "','pmac','" & apprid & "','" & arrcnt & "');"" " & dis & ">Completed</td>" & vbCrLf)
                                sb.Append("<td class=""plainlabel"" id='tdpmac" + bid + "'>" & tmod.getlbl("cdlbl1297" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                            End If

                        Else
                            'Treat High Level Task like a Phase until sub tasks are completed
                            If subappr = 1 Then
                                If pmacidappr = 1 Then
                                    sb.Append("<td class=""plainlabel""><input type='checkbox' id='nac" + bid + "' onclick=""checkbox('nac" + bid + "','pmac" + bid + "','" + bid + "','nac','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                                    sb.Append("<input type='checkbox' id='pmac" + bid + "' onclick=""checkbox('pmac" + bid + "','nac" + bid + "','" + bid + "','pmac','" & apprid & "','" & arrcnt & "');"" CHECKED " & dis & ">Completed</td>" & vbCrLf)
                                    sb.Append("<td class=""plainlabel"" id='tdpmac" + bid + "'>" & tmod.getlbl("cdlbl1298" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                                Else
                                    sb.Append("<td class=""plainlabel""><input type='checkbox' id='nac" + bid + "' onclick=""checkbox('nac" + bid + "','pmac" + bid + "','" + bid + "','nac','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                                    sb.Append("<input type='checkbox' id='pmac" + bid + "' onclick=""checkbox('pmac" + bid + "','nac" + bid + "','" + bid + "','pmac','" & apprid & "','" & arrcnt & "');"" " & dis & ">Completed</td>" & vbCrLf)
                                    sb.Append("<td class=""plainlabel"" id='tdpmac" + bid + "'>" & tmod.getlbl("cdlbl1299" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                                End If
                            Else
                                sb.Append("<td class=""plainlabel""><input type='checkbox' id='nac" + bid + "' onclick=""checkbox('nac" + bid + "','pmac" + bid + "','" + bid + "','nac','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                                sb.Append("<input type='checkbox' id='pmac" + bid + "' onclick=""checkbox('pmac" + bid + "','nac" + bid + "','" + bid + "','pmac','" & apprid & "');"" disabled>Completed</td>" & vbCrLf)
                                sb.Append("<td class=""plainlabel"" id='tdpmac" + bid + "'>" & tmod.getlbl("cdlbl1300" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                            End If


                        End If
                    End If


                    If cntb = bcnt Then
                        bcnt = 0
                        sb.Append("</td></tr></table>")
                    End If

                    If tid <> "" Then
                        tcnt = tcnt + 1
                        cntt = dr.Item("cntt").ToString
                        'pmtidappr = dr.Item("pmacidappr").ToString

                        sb.Append("<tr class=""details"" id='1t" + gi + "' align=""right""><td colspan=""5"">" & vbCrLf)
                        sb.Append("<table border=""0"" width=""670"" cellspacing=""0"">")

                        sb.Append("<tr><td class=""tblmenugrt plainlabel"" width=""40"">" & tmod.getlbl("cdlbl1301" , "PMApproval.aspx.vb") & "</td>" & vbCrLf)
                        sb.Append("<td class=""tblmenugrt plainlabel"" width=""420"">" & tmod.getlbl("cdlbl1302" , "PMApproval.aspx.vb") & "</td>" & vbCrLf)
                        sb.Append("<td class=""tblmenugrt plainlabel"" width=""135px"">" & tmod.getlbl("cdlbl1303" , "PMApproval.aspx.vb") & "</td>" & vbCrLf)
                        sb.Append("<td class=""tblmenugrt plainlabel"" width=""75px"">" & tmod.getlbl("cdlbl1304" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)

                        sb.Append("<tr><td class=""plainlabel"" align=""center"" valign=""top"">" & dr.Item("taskorder").ToString & "</td>" & vbCrLf)
                        sb.Append("<td class=""plainlabel"" >" & dr.Item("apprtask").ToString & "</td>" & vbCrLf)

                        If pmtna = "1" Then
                            sb.Append("<td class=""plainlabel""><input type='checkbox' id='nat" + tid + "' onclick=""checkbox('nat" + tid + "','pmt" + tid + "','" + tid + "','nat','" & apprid & "');"" CHECKED disabled>N/A" & vbCrLf)
                            sb.Append("<input type='checkbox' id='pmt" + tid + "' onclick=""checkbox('pmt" + tid + "','nat" + tid + "','" + tid + "','pmt','" & apprid & "','dis');"" " & dis & ">Enable</td>" & vbCrLf)
                            sb.Append("<td class=""plainlabel"" id='tdpmt" + tid + "'>N/A</td></tr>" & vbCrLf)
                        Else
                            If pmtidappr = 1 Then
                                sb.Append("<td class=""plainlabel""><input type='checkbox' id='nat" + tid + "' onclick=""checkbox('nat" + tid + "','pmt" + tid + "','" + tid + "','nat','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                                sb.Append("<input type='checkbox' id='pmt" + tid + "' onclick=""checkbox('pmt" + tid + "','nat" + tid + "','" + tid + "','pmt','" & apprid & "');"" CHECKED " & dis & ">Completed</td>" & vbCrLf)
                                sb.Append("<td class=""plainlabel"" id='tdpmt" + tid + "'>" & tmod.getlbl("cdlbl1306" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                            Else
                                sb.Append("<td class=""plainlabel""><input type='checkbox' id='nat" + tid + "' onclick=""checkbox('nat" + tid + "','pmt" + tid + "','" + tid + "','nat','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                                sb.Append("<input type='checkbox' id='pmt" + tid + "' onclick=""checkbox('pmt" + tid + "','nat" + tid + "','" + tid + "','pmt','" & apprid & "','" & arrcnt & "');"" " & dis & ">Completed</td>" & vbCrLf)
                                sb.Append("<td class=""plainlabel"" id='tdpmt" + tid + "'>" & tmod.getlbl("cdlbl1307" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                            End If

                        End If

                        If cntt = tcnt Then
                            tcnt = 0
                            sb.Append("</td></tr></table>")
                        End If

                    End If

                Else
                    If rowflag2 = 0 Then
                        bg = "#c2ffc1"
                        rowflag2 = 1
                    Else
                        bg = "white"
                        rowflag2 = "0"
                    End If

                    tcnt = tcnt + 1
                    cntt = dr.Item("cntt").ToString

                    sb.Append("<tr bgcolor=""" & bg & """><td class=""plainlabel"" width=""40"" align=""center"" valign=""top"">" & dr.Item("taskorder").ToString & "</td>" & vbCrLf)
                    sb.Append("<td class=""plainlabel"" width=""430"">" & dr.Item("apprtask").ToString & "</td>" & vbCrLf)

                    If pmtna = "1" Then
                        sb.Append("<td class=""plainlabel""><input type='checkbox' id='nat" + tid + "' onclick=""checkbox('nat" + tid + "','pmt" + tid + "','" + tid + "','nat','" & apprid & "');"" CHECKED disabled>N/A" & vbCrLf)
                        sb.Append("<input type='checkbox' id='pmt" + tid + "' onclick=""checkbox('pmt" + tid + "','nat" + tid + "','" + tid + "','pmt','" & apprid & "','dis');"" " & dis & ">Enable</td>" & vbCrLf)
                        sb.Append("<td class=""plainlabel"" id='tdpmt" + tid + "'>N/A</td></tr>" & vbCrLf)
                    Else
                        If pmtidappr = 1 Then
                            sb.Append("<td class=""plainlabel""><input type='checkbox' id='nat" + tid + "' onclick=""checkbox('nat" + tid + "','pmt" + tid + "','" + tid + "','nat','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                            sb.Append("<input type='checkbox' id='pmt" + tid + "' onclick=""checkbox('pmt" + tid + "','nat" + tid + "','" + tid + "','pmt','" & apprid & "');"" CHECKED " & dis & ">Completed</td>" & vbCrLf)
                            sb.Append("<td class=""plainlabel"" id='tdpmt" + tid + "'>" & tmod.getlbl("cdlbl1309" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td class=""plainlabel""><input type='checkbox' id='nat" + tid + "' onclick=""checkbox('nat" + tid + "','pmt" + tid + "','" + tid + "','nat','" & apprid & "');"" " & dis & ">N/A" & vbCrLf)
                            sb.Append("<input type='checkbox' id='pmt" + tid + "' onclick=""checkbox('pmt" + tid + "','nat" + tid + "','" + tid + "','pmt','" & apprid & "','" & arrcnt & "');"" " & dis & ">Completed</td>" & vbCrLf)
                            sb.Append("<td class=""plainlabel"" id='tdpmt" + tid + "'>" & tmod.getlbl("cdlbl1310" , "PMApproval.aspx.vb") & "</td></tr>" & vbCrLf)
                        End If

                    End If

                    If cntt = tcnt Then
                        tcnt = 0
                        sb.Append("</td></tr><tr><td><img src='../images/2PX.gif'></td></tr></table>")
                    End If
                End If

            End If


        End While
        dr.Close()
        sb.Append("</tr></table>")
        tdplan.InnerHtml = sb.ToString
        tdeq.InnerHtml = lbleqnum.Value
        'Catch ex As Exception

        'End Try
    End Sub


	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3665.Text = axlabs.GetASPXPage("PMApproval.aspx", "lang3665")
        Catch ex As Exception
        End Try
        Try
            lang3666.Text = axlabs.GetASPXPage("PMApproval.aspx", "lang3666")
        Catch ex As Exception
        End Try
        Try
            lang3667.Text = axlabs.GetASPXPage("PMApproval.aspx", "lang3667")
        Catch ex As Exception
        End Try
        Try
            lang3668.Text = axlabs.GetASPXPage("PMApproval.aspx", "lang3668")
        Catch ex As Exception
        End Try
        Try
            lang3669.Text = axlabs.GetASPXPage("PMApproval.aspx", "lang3669")
        Catch ex As Exception
        End Try
        Try
            lang3670.Text = axlabs.GetASPXPage("PMApproval.aspx", "lang3670")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                ibtnsave.Attributes.Add("src", "../images2/eng/bgbuttons/save.gif")
            ElseIf lang = "fre" Then
                ibtnsave.Attributes.Add("src", "../images2/fre/bgbuttons/save.gif")
            ElseIf lang = "ger" Then
                ibtnsave.Attributes.Add("src", "../images2/ger/bgbuttons/save.gif")
            ElseIf lang = "ita" Then
                ibtnsave.Attributes.Add("src", "../images2/ita/bgbuttons/save.gif")
            ElseIf lang = "spa" Then
                ibtnsave.Attributes.Add("src", "../images2/spa/bgbuttons/save.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                imgappr.Attributes.Add("src", "../images2/eng/bgbuttons/apprg.gif")
            ElseIf lang = "fre" Then
                imgappr.Attributes.Add("src", "../images2/fre/bgbuttons/apprg.gif")
            ElseIf lang = "ger" Then
                imgappr.Attributes.Add("src", "../images2/ger/bgbuttons/apprg.gif")
            ElseIf lang = "ita" Then
                imgappr.Attributes.Add("src", "../images2/ita/bgbuttons/apprg.gif")
            ElseIf lang = "spa" Then
                imgappr.Attributes.Add("src", "../images2/spa/bgbuttons/apprg.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                imgrem.Attributes.Add("src", "../images2/eng/bgbuttons/removeg.gif")
            ElseIf lang = "fre" Then
                imgrem.Attributes.Add("src", "../images2/fre/bgbuttons/removeg.gif")
            ElseIf lang = "ger" Then
                imgrem.Attributes.Add("src", "../images2/ger/bgbuttons/removeg.gif")
            ElseIf lang = "ita" Then
                imgrem.Attributes.Add("src", "../images2/ita/bgbuttons/removeg.gif")
            ElseIf lang = "spa" Then
                imgrem.Attributes.Add("src", "../images2/spa/bgbuttons/removeg.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
