<%@ Page Language="vb" AutoEventWireup="false" Codebehind="query.aspx.vb" Inherits="lucy_r12.query" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>query</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/dragitem.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/queryaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
         function getsc() {
             var q = document.getElementById("TextBox1").value;
             var dg = document.getElementById("lbldg").value;
             var ty = document.getElementById("lbltype").value;
             if (ty == "select") {
                 if (q == "") {
                     alert("SQL Statement Required")
                 }
                 else {
                     if (dg == "") {
                         alert("No Results to Save")
                     }
                     else {
                         document.getElementById("scdiv").style.position = "absolute";
                         document.getElementById("scdiv").style.top = "10px";
                         document.getElementById("scdiv").style.left = "120px";
                         document.getElementById("scdiv").className = "view";
                         //document.getElementById("scdiv").style.visibility = "visible";
                         var cq = document.getElementById("lblqname").value;
                         if (cq != "") {
                             document.getElementById("TextBox3").value = cq;
                         }

                     }
                 }
             }
             else {
                 alert("Simple Select Query Required")
             }

         }
         function savesc() {
             var chk = document.getElementById("TextBox3").value;

             if (chk != "") {
                 closesc();
                 document.getElementById("lblsubmit").value = "savq";
                 document.getElementById("form1").submit();
             }
             else {
                 alert("Query Name Required");
             }
         }
         function excel() {

             var q = document.getElementById("TextBox1").value;
             var dg = document.getElementById("lbldg").value;
             var ty = document.getElementById("lbltype").value;
             //alert(ty)
             if (ty == "select" || ty == "") {
                 if (q == "") {
                     alert("SQL Statement Required")
                 }
                 else {
                     if (dg == "") {
                         alert("No Results to Export")
                     }
                     else {
                         //document.getElementById("lblsubmit").value="exc";
                         //document.getElementById("form1").submit();
                         //window.open("excelout.aspx?str=" + q);
                         var sessid = document.getElementById("lblsessid").value;
                         window.open("excelout.aspx?sessid=" + sessid)
                     }
                 }
             }
             else {
                 alert("Simple Select Query Required for Output to Excel")
             }
         }
     </script>
	</HEAD>
	<body  onload="checkout();">
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 0px; POSITION: absolute; TOP: 4px" cellSpacing="1" cellPadding="1"
				width="670">
				<tr>
					<td class="plainlabelblue" align="center" colSpan="9"><asp:Label id="lang3700" runat="server">Click on a Table or Custom View on the right for instant results</asp:Label></td>
				</tr>
				<tr height="20">
					<td class="plainlabelred" id="tdqt" runat="server" colSpan="9"></td>
				</tr>
				<tr>
					<td class="bluelabel" width="80"><asp:Label id="lang3701" runat="server">Current Query</asp:Label></td>
					<td class="plainlabelblue" id="tdnewq" width="320" runat="server"><asp:Label id="lang3702" runat="server">New</asp:Label></td>
					<td align="right" width="150">
						
						
						
						
						</td>
							<td width="20"><IMG id="imgadmqt" class="details" runat="server" onmouseover="return overlib('Administrator Access to Adjust Query Timeout', ABOVE, LEFT)"
							onclick="qtime();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/clock.gif"></td>
							<td width="20"><IMG onmouseover="return overlib('Export Results to Excel', ABOVE, LEFT)" onclick="excel();"
							onmouseout="return nd()" src="../images/appbuttons/minibuttons/excelexp.gif"></td>
							<td width="20"><IMG onmouseover="return overlib('Save Query for Future Use', ABOVE, LEFT)" onclick="getsc();"
							onmouseout="return nd()" src="../images/appbuttons/minibuttons/savedisk1.gif"></td>
							<td width="20"><IMG onmouseover="return overlib('Look-up Saved Queries', ABOVE, LEFT)" onclick="srchq();"
							onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
							<td width="20"><IMG onmouseover="return overlib('Clear Query Screen', ABOVE, LEFT)" onclick="redo();"
							onmouseout="return nd()" src="../images/appbuttons/minibuttons/req.gif"></td>
							<td width="20"> <IMG onmouseover="return overlib('Submit Query', ABOVE, LEFT)" onclick="getq();" onmouseout="return nd()"
							src="../images/appbuttons/minibuttons/goq.gif"></td>
				</tr>
				<tr>
					<td class="label" colSpan="9"><asp:textbox id="TextBox1" runat="server" TextMode="MultiLine" Width="680px" Height="140px"></asp:textbox></td>
				</tr>
				<tr>
					<td class="plainlabelred" id="tdcntlabel" align="center" colSpan="9" runat="server"></td>
				</tr>
				<tr>
					<td colSpan="9">
						<div style="OVERFLOW: auto; WIDTH: 670px; HEIGHT: 260px"><asp:datagrid id="DataGrid1" runat="server">
								<AlternatingItemStyle Font-Size="X-Small" Font-Names="Arial"></AlternatingItemStyle>
								<ItemStyle Font-Size="X-Small" Font-Names="Arial"></ItemStyle>
								<HeaderStyle Font-Size="X-Small" Font-Names="Arial" BackColor="Aqua"></HeaderStyle>
							</asp:datagrid></div>
					</td>
				</tr>
			</table>
			<div class="details" id="scdiv" style="background-color: white; BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 999; BORDER-LEFT: black 1px solid; WIDTH: 260px; BORDER-BOTTOM: black 1px solid; HEIGHT: 120px;" >
				<table cellSpacing="0" cellPadding="0" width="260" bgColor="white">
					<tr bgColor="blue" height="20">
						<td class="whitelabel12"><asp:Label id="lang3703" runat="server">Save Query</asp:Label></td>
						<td align="right"><IMG onclick="closesc();" height="18" alt="" src="../images/appbuttons/minibuttons/close.gif"
								width="18"><br>
						</td>
					</tr>
					
					<tr>
						<td colSpan="2">
							<table bgcolor="white">
								<tr>
									<td width="85"></td>
									<td width="95"></td>
									<td width="80"></td>
								</tr>
								<tr>
									<td class="bluelabel"><asp:Label id="lang3705" runat="server">Query Name</asp:Label></td>
									<td colspan="2">
                                        <asp:TextBox ID="TextBox3" runat="server" Width="170"></asp:TextBox></td>
								</tr>
								<tr>
									<td class="plainlabelblue" align="center" colSpan="2"><input id="rbev" type="radio" CHECKED name="rbch" runat="server"><asp:Label id="lang3706" runat="server">For Everybody</asp:Label><input id="rbme" type="radio" name="rbch" runat="server"><asp:Label id="lang3707" runat="server">Just Me</asp:Label></td>
									<td align="right"><IMG onclick="savesc();" src="../images/appbuttons/minibuttons/savedisk1.gif"></td>
								</tr>

							</table>
						</td>
					</tr>
				</table>
			</div>
			<input id="lbltable" type="hidden" name="lbltable" runat="server"> <input id="lblsubmit" type="hidden" name="lblsubmit" runat="server">
			<input id="lbldg" type="hidden" runat="server"><input id="lbltype" type="hidden" runat="server">
			<input id="lblsessid" type="hidden" runat="server"> <input id="lblusername" type="hidden" runat="server">
			<input id="lbllog" type="hidden" runat="server"> <input id="lblquery" type="hidden" runat="server">
			<input id="lblqid" type="hidden" runat="server"> <input id="lblqname" type="hidden" runat="server">
			<input id="lblrettyp" type="hidden" runat="server"> <input id="lbldupchk" type="hidden" runat="server">
			<input id="lblnextint" type="hidden" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
