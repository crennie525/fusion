

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Timers
Public Class query
    Inherits System.Web.UI.Page
	Protected WithEvents ovid305 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid304 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid303 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid302 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid301 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang3707 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3706 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3705 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3704 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3703 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3702 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3701 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3700 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim SQLTimer As New System.Timers.Timer
    Dim nextint As Integer
    Dim xflg As Integer = 0
    Dim testcmd As SqlCommand
    Dim sqa As New Utilities
    Dim sql, table As String
    Dim sessid, Login, username, userid As String
    Protected WithEvents lbltable As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsessid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Textbox3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents rbev As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbme As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents lbldragid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrowid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblquery As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdnewq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblqname As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrettyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldupchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnextint As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdcntlabel As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdqt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgadmqt As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents DataGrid1 As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
            username = HttpContext.Current.Session("username").ToString()
            userid = HttpContext.Current.Session("uid").ToString()
            lblusername.Value = username
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            sqa.Open()
            GetQueryTime()
            If userid = "pmadmin1" Then
                imgadmqt.Attributes.Add("class", "view")
            Else
                imgadmqt.Attributes.Add("class", "details")
            End If
            'Try
            'sessid = HttpContext.Current.Session.SessionID
            'lblsessid.Value = sessid
            'Catch ex As Exception

            'End Try
            'Try
            table = Request.QueryString("table").ToString
            If table <> "" Then
                lbltable.Value = table

                GetTable()


            End If
            'Catch ex As Exception

            'End Try
            sqa.Dispose()
        Else
            If Request.Form("lblsubmit") = "getq" Then
                lblsubmit.Value = ""
                sqa.Open()
                CheckType()
                GetResults()
                sqa.Dispose()
         
            ElseIf Request.Form("lblsubmit") = "savq" Then
                sqa.Open()
                SaveQuery()
                sqa.Dispose()
            ElseIf Request.Form("lblsubmit") = "newq" Then
                sqa.Open()
                GetNew()
                sqa.Dispose()
            ElseIf Request.Form("lblsubmit") = "newqt" Then
                sqa.Open()
                GetQueryTime()
                sqa.Dispose()
            End If
        End If
        'TextBox3.Attributes.Add("onKeyPress", "return keychk(this,event)")
    End Sub
    Private Sub GetQueryTime()
        sql = "select sqltime from pmsysvars"
        Dim qt As String
        Try
            qt = sqa.strScalar(sql)
        Catch ex As Exception
            qt = "30000"
        End Try

        lblnextint.Value = qt
        Dim qtchk As Long
        Try
            qtchk = System.Convert.ToInt32(qt)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1755" , "query.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        qtchk = qtchk / 1000
        tdqt.InnerHtml = "Current Query Timeout is " & qtchk & " seconds"
    End Sub
    Private Function ModString(ByVal str As String)
        str = Replace(str, "'", Chr(180), , , vbTextCompare)
        str = Replace(str, "--", "-", , , vbTextCompare)
        str = Replace(str, ";", ":", , , vbTextCompare)
        str = Replace(str, ",", " ", , , vbTextCompare)
        Return str
    End Function
    Private Sub SaveQuery()
        username = lblusername.Value
        Dim osql As String = lblquery.Value
        Dim qname As String = Textbox3.Text
        qname = ModString(qname)
        Dim oqname As String = lblqname.Value
        Dim qid As String = lblqid.Value
        If qid = "" Then
            qid = "0"
        End If
        If oqname <> "" And qname <> oqname Then
            qname = oqname
        End If
        Dim qcnt As Integer
        Dim qstat As String
        If rbme.Checked = True Then
            qstat = "0"
            sql = "select count(*) from ns_squery where queryname = '" & qname & "' and qstat = 0"
        Else
            qstat = "1"
            sql = "select count(*) from ns_squery where queryname = '" & qname & "' and qstat = 1"
        End If

        qcnt = sqa.Scalar(sql)
        If qcnt = 0 Then
            Dim newqid As Integer
            Dim cmd As New SqlCommand
            cmd.CommandText = "exec ns_newq @user, @name, @query, @qid, @qstat"
            Dim param01 = New SqlParameter("@user", SqlDbType.VarChar)
            If username = "" Then
                param01.Value = System.DBNull.Value
            Else
                param01.Value = username
            End If
            cmd.Parameters.Add(param01)
            Dim param02 = New SqlParameter("@name", SqlDbType.VarChar)
            If qname = "" Then
                param02.Value = System.DBNull.Value
            Else
                param02.Value = qname
            End If
            cmd.Parameters.Add(param02)
            Dim param03 = New SqlParameter("@query", SqlDbType.NText)
            If osql = "" Then
                param03.Value = System.DBNull.Value
            Else
                param03.Value = osql
            End If
            cmd.Parameters.Add(param03)
            Dim param04 = New SqlParameter("@qid", SqlDbType.VarChar)
            If qid = "" Then
                param04.Value = System.DBNull.Value
            Else
                param04.Value = qid
            End If
            cmd.Parameters.Add(param04)
            Dim param05 = New SqlParameter("@qstat", SqlDbType.VarChar)
            If qstat = "" Then
                param05.Value = System.DBNull.Value
            Else
                param05.Value = qstat
            End If
            cmd.Parameters.Add(param05)
            newqid = sqa.ScalarHack(cmd)
            tdnewq.InnerHtml = qname
            lblqname.Value = qname
            lblqid.Value = qid
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr1756" , "query.aspx.vb")
 
            sqa.CreateMessageAlert(Me, strMessage, "strKey1")
            lbldupchk.Value = "yes"
            Exit Sub
        End If



    End Sub


    Private Sub CheckType()
        Dim s As String = TextBox1.Text
        s = s.ToLower
        Dim dcnt As Integer
        dcnt = s.IndexOf("drop")
        dcnt += s.IndexOf("delete")
        dcnt += s.IndexOf("truncate")
        dcnt += s.IndexOf("create")
        dcnt += s.IndexOf("alter")
        dcnt += s.IndexOf("exec")
        dcnt += s.IndexOf("execute")
        dcnt += s.IndexOf("insert")
        dcnt += s.IndexOf("update")
        'dcnt += s.IndexOf("alter")
        If dcnt <> -9 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1757" , "query.aspx.vb")
 
            sqa.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        Else
            'dcnt = s.IndexOf("while")
            'If dcnt <> -1 Then
            'Dim strMessage As String =  tmod.getmsg("cdstr1758" , "query.aspx.vb")
 
            'sqa.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
            'Else
            dcnt = s.IndexOf("select")
            If dcnt <> -1 Then
                lbltype.Value = "select"
                Exit Sub
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1759" , "query.aspx.vb")
 
                sqa.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            'End If
        End If

    End Sub
    Private Sub BindExport()
        Dim ds As New DataSet
        sql = TextBox1.Text
        ds = sqa.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        DataGrid1.DataSource = dv
        DataGrid1.DataBind()
    End Sub
    Private Sub StartSQLTimer(ByVal nextint As Integer)
        SQLTimer.Interval = nextint
        SQLTimer.Enabled = True
        AddHandler SQLTimer.Elapsed, New ElapsedEventHandler(AddressOf Me.SQLTimer_Elapsed)
    End Sub
    Private Sub SQLTimer_Elapsed(ByVal sender As Object, ByVal e As ElapsedEventArgs)
        testcmd.Cancel()
        xflg = 1
        'Dim strMessage As String =  tmod.getmsg("cdstr1760" , "query.aspx.vb")
 
        'sqa.CreateMessageAlert(Me, strMessage, "strKey1")
    End Sub
    Private Sub GetResults()
        Dim typ As String = lbltype.Value
        Dim ds As New DataSet
        Dim nstr As String
        Dim ustr As String
        Dim cnt, ocnt As Integer
        Dim s As String
        Dim wcnt, scnt, ucnt As Integer
        If typ = "select" Then
            Try
                sql = TextBox1.Text
                Dim osql As String
                osql = sql
                lblquery.Value = osql
                TextBox1.Text = osql
                Dim newsess As Integer
                Dim cmd As New SqlCommand
                cmd.CommandText = "exec ns_upsess @sessid, @query"
                Dim param01 = New SqlParameter("@sessid", SqlDbType.VarChar)
                If sessid = "" Then
                    param01.Value = System.DBNull.Value
                Else
                    param01.Value = sessid
                End If
                cmd.Parameters.Add(param01)
                Dim param02 = New SqlParameter("@query", SqlDbType.NText)
                If osql = "" Then
                    param02.Value = System.DBNull.Value
                Else
                    param02.Value = osql
                End If
                cmd.Parameters.Add(param02)
                newsess = sqa.ScalarHack(cmd)
                lblsessid.Value = newsess
                'Timeout Here
                nextint = lblnextint.Value
                Try
                    StartSQLTimer(nextint)
                Catch ex As Exception
                    Dim strMessage As String =  tmod.getmsg("cdstr1761" , "query.aspx.vb")
 
                    sqa.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try

                Dim sd As String = Now
                'Timeout End
                testcmd = New SqlCommand(osql)
                ds = sqa.GetDSDataHack(testcmd)
                'ds = sqa.GetDSData(osql)
                Dim dv As DataView
                dv = ds.Tables(0).DefaultView
                cnt = ds.Tables(0).Rows.Count
                If cnt <> 0 Then
                    lbldg.Value = cnt
                    Dim ed As String = Now
                    tdcntlabel.InnerHtml = cnt & " Records Found " ' & sd & " , " & ed & " , " & xflg
                Else
                    lbldg.Value = ""
                    tdcntlabel.InnerHtml = "No Records Found"
                End If
                DataGrid1.DataSource = dv
                DataGrid1.DataBind()
            Catch ex As Exception
                lbldg.Value = ""
                If xflg = 1 Then
                    tdcntlabel.InnerHtml = "Query Cancelled - Time Limit Exceeded - Please revise query or contact your System Administrator to increase Query Timeout"
                Else
                    tdcntlabel.InnerHtml = ex.Message
                End If

            End Try
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr1762" , "query.aspx.vb")
 
            sqa.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
    End Sub
    Private Sub GetNew()
        Dim sqlout As String
        Dim qid As String = lblqid.Value
        Dim typ As String = lblrettyp.Value
        Dim qname As String = lblqname.Value
        tdnewq.InnerHtml = qname
        If typ = "all" Then
            lblqid.Value = ""
            lblrettyp.Value = ""
            lblqname.Value = ""
        End If
        sql = "select query from ns_squery where qid = '" & qid & "'"
        sqlout = sqa.strScalar(sql)
        TextBox1.Text = sqlout
        lblquery.Value = sqlout
        Dim osql As String
        osql = sqlout
        Dim newsess As Integer
        Dim cmd As New SqlCommand
        cmd.CommandText = "exec ns_upsess @sessid, @query"
        Dim param01 = New SqlParameter("@sessid", SqlDbType.VarChar)
        If sessid = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = sessid
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@query", SqlDbType.NText)
        If osql = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = osql
        End If
        cmd.Parameters.Add(param02)
        newsess = sqa.ScalarHack(cmd)
        lblsessid.Value = newsess
        Dim ds As New DataSet
        Dim cnt As Integer
        Try
            ds = sqa.GetDSData(sqlout)
            Dim dv As DataView
            dv = ds.Tables(0).DefaultView
            cnt = ds.Tables(0).Rows.Count
            If cnt <> 0 Then
                lbldg.Value = cnt
                tdcntlabel.InnerHtml = cnt & " Records Found"
            Else
                lbldg.Value = ""
                tdcntlabel.InnerHtml = "No Records Found"
            End If
            DataGrid1.DataSource = dv
            DataGrid1.DataBind()
        Catch ex As Exception
            lbldg.Value = ""
            tdcntlabel.InnerHtml = ex.Message
        End Try
    End Sub
    Private Sub GetTable()
        Dim cnt As Integer
        Dim tblstr As String
        Dim col As String
        Dim sqlout As String
        sql = "select column_name from INFORMATION_SCHEMA.Columns where table_name = '" & lbltable.Value & "' " _
        + "order by ordinal_position"
        dr = sqa.GetRdrData(sql)
        While dr.Read
            col = dr.Item("column_name").ToString
            If tblstr = "" Then
                tblstr = col.ToLower
            Else
                tblstr += ", " & col.ToLower
            End If
        End While
        dr.Close()
        sql = "select count(*) from " & lbltable.Value
        cnt = sqa.Scalar(sql)
        If cnt <> 0 Then

            If cnt < 600 Then
                sql = "select " & tblstr & " from " & lbltable.Value
                lblquery.Value = sql
                TextBox1.Text = "select " & tblstr & " from " & lbltable.Value
                tdcntlabel.InnerHtml = cnt & " Records Found"
            Else
                sql = "select " & tblstr & " from " & lbltable.Value
                lblquery.Value = sql
                sql = "select top 600 " & tblstr & " from " & lbltable.Value
                TextBox1.Text = "select top 600 " & tblstr & " from " & lbltable.Value
                tdcntlabel.InnerHtml = cnt & " Records Found - Output Limited to Top 600 Records"
            End If

            sessid = lblsessid.Value
            Dim sqcnt As Integer
            Dim osql As String
            osql = sql
            Dim newsess As Integer
            Dim cmd As New SqlCommand
            cmd.CommandText = "exec ns_upsess @sessid, @query"
            Dim param01 = New SqlParameter("@sessid", SqlDbType.VarChar)
            If sessid = "" Then
                param01.Value = System.DBNull.Value
            Else
                param01.Value = sessid
            End If
            cmd.Parameters.Add(param01)
            Dim param02 = New SqlParameter("@query", SqlDbType.NText)
            If osql = "" Then
                param02.Value = System.DBNull.Value
            Else
                param02.Value = lblquery.Value 'osql
            End If
            cmd.Parameters.Add(param02)
            newsess = sqa.ScalarHack(cmd)
            lblsessid.Value = newsess
            Try
                dr = sqa.GetRdrData(osql)
                DataGrid1.DataSource = dr
                DataGrid1.DataBind()
                dr.Close()
                lbldg.Value = cnt
                lbltype.Value = "select"
            Catch ex As Exception
                lbldg.Value = ""
                tdcntlabel.InnerHtml = ex.Message
            End Try
        Else
            lbldg.Value = ""
            tdcntlabel.InnerHtml = "No Records Found"
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3700.Text = axlabs.GetASPXPage("query.aspx", "lang3700")
        Catch ex As Exception
        End Try
        Try
            lang3701.Text = axlabs.GetASPXPage("query.aspx", "lang3701")
        Catch ex As Exception
        End Try
        Try
            lang3702.Text = axlabs.GetASPXPage("query.aspx", "lang3702")
        Catch ex As Exception
        End Try
        Try
            lang3703.Text = axlabs.GetASPXPage("query.aspx", "lang3703")
        Catch ex As Exception
        End Try
        Try
            lang3704.Text = axlabs.GetASPXPage("query.aspx", "lang3704")
        Catch ex As Exception
        End Try
        Try
            lang3705.Text = axlabs.GetASPXPage("query.aspx", "lang3705")
        Catch ex As Exception
        End Try
        Try
            lang3706.Text = axlabs.GetASPXPage("query.aspx", "lang3706")
        Catch ex As Exception
        End Try
        Try
            lang3707.Text = axlabs.GetASPXPage("query.aspx", "lang3707")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgadmqt.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("query.aspx", "imgadmqt") & "', ABOVE, LEFT)")
            imgadmqt.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid301.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("query.aspx", "ovid301") & "', ABOVE, LEFT)")
            ovid301.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid302.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("query.aspx", "ovid302") & "', ABOVE, LEFT)")
            ovid302.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid303.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("query.aspx", "ovid303") & "', ABOVE, LEFT)")
            ovid303.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid304.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("query.aspx", "ovid304") & "', ABOVE, LEFT)")
            ovid304.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid305.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("query.aspx", "ovid305") & "', ABOVE, LEFT)")
            ovid305.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
