<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmresources.aspx.vb" Inherits="lucy_r12.pmresources" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PM Resources - Subject Matter Experts</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/Resourcesaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="scrolltop();checkit();" >
		<form id="form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="3" width="900" style="Z-INDEX: 1; POSITION: absolute; TOP: 2px; LEFT: 2px">
				<tr height="22">
					<td class="thdrsing label" colSpan="3"><asp:Label id="lang3708" runat="server">PM Resources - Subject Matter Experts</asp:Label></td>
				</tr>
				<tr class="details">
					<td></td>
					<td align="right"><asp:imagebutton id="ibtnreturn" runat="server" ImageUrl="../images/appbuttons/bgbuttons/return.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td colSpan="2">
						<table>
							<tr>
								<td class="label"><asp:Label id="lang3709" runat="server">Search by Name or Subject Matter</asp:Label></td>
								<td><asp:textbox id="txtsearch" runat="server"></asp:textbox></td>
								<td><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"
										CssClass="imgbutton"></asp:imagebutton></td>
								<td><IMG onmouseover="return overlib('Refresh Page')" onclick="refresh();" onmouseout="return nd()"
										src="../images/appbuttons/minibuttons/refreshit.gif">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td class="label"><asp:Label id="lang3710" runat="server">Filter by Subject Matter</asp:Label></td>
								<td><asp:dropdownlist id="ddsm" runat="server" AutoPostBack="True" Width="250px"></asp:dropdownlist></td>
								<td><IMG class="imgbutton" onmouseover="return overlib('Add a New Subject Matter')" onclick="getSMDiv();"
										onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/addmod.gif"></td>
							</tr>
						</table>
					</td>
				</tr>
				<TR>
					<TD colSpan="2"><asp:datagrid id="dgrec" runat="server" AutoGenerateColumns="False" AllowCustomPaging="True" AllowPaging="True"
							GridLines="None" CellPadding="0" ShowFooter="True" CellSpacing="1" BackColor="transparent">
							<FooterStyle BackColor="transparent"></FooterStyle>
							<EditItemStyle Height="15px"></EditItemStyle>
							<AlternatingItemStyle cssclass="ptransrowblue"></AlternatingItemStyle>
							<ItemStyle cssclass="ptransrow"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Font-Bold="True" Height="22px"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<FooterTemplate>
										<asp:ImageButton id="ImageButton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
											CommandName="Add"></asp:ImageButton>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Expert">
									<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblname runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id=txtnewname runat="server" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>' Width="160px">
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:TextBox id=txtname runat="server" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>' Width="160px">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="10px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<asp:ImageButton id="Imagebutton4" runat="server" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
											ToolTip="Hide Task Type" CommandName="hloc"></asp:ImageButton><BR>
										<asp:ImageButton id="Imagebutton5" runat="server" ImageUrl="../images/appbuttons/minibuttons/show.gif"
											ToolTip="Show Task Type" CommandName="sloc"></asp:ImageButton>
									</HeaderTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Location" Visible="False">
									<HeaderStyle Width="220px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id="txtloc1" runat="server" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>' Width="210px">
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtloc" runat="server" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>' Width="210px">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Subject Matter">
									<HeaderStyle Width="220px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label4 runat="server" Width="240px" Text='<%# DataBinder.Eval(Container, "DataItem.subj") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:dropdownlist Width="240px" id="ddsubj1" runat="server" DataSource="<%# PopulateSM %>" DataTextField="subj" DataValueField="smid" >
										</asp:dropdownlist>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:dropdownlist Width="240px" id="ddsubj" runat="server" DataSource="<%# PopulateSM %>" DataTextField="subj" DataValueField="smid" SelectedIndex='<%# GetSelIndex(Container.DataItem("smindex")) %>' >
										</asp:dropdownlist>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Phone#">
									<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.phone") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id="txtnewphone" runat="server" Width="110px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.phone") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtphone" runat="server" Width="110px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.phone") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Email Address">
									<HeaderStyle Width="220px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<a href='mailto:<%# DataBinder.Eval(Container.DataItem,"email")%>' >
											<asp:label ID="lblemail" Text='<%# DataBinder.Eval(Container.DataItem,"email")%>' Runat = server>
											</asp:label></a>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id="txtnewemail" runat="server" Width="240px" MaxLength="200" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtemail" runat="server" Width="240px" MaxLength="200" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<HeaderStyle Width="220px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblid runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.rid") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id=txtnewllabel runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.rid") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id=lblide runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.rid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="30px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<IMG alt="" src="../images/appbuttons/minibuttons/del.gif" width="16" height="16">
									</HeaderTemplate>
									<ItemTemplate>
										&nbsp;
										<asp:ImageButton id="Imagebutton25" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
											CommandName="Delete"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
								ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
			</table>
			<input id="lblcid" type="hidden" runat="server"> <input id="xCoord" type="hidden" runat="server">
			<input id="yCoord" type="hidden" runat="server"><input id="lblpchk" type="hidden" runat="server">
			<input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
