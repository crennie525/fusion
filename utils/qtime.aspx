<%@ Page Language="vb" AutoEventWireup="false" Codebehind="qtime.aspx.vb" Inherits="lucy_r12.qtime" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>qtime</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/qtimeaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checkout();">
		<form id="form1" method="post" runat="server">
			<table>
				<tr height="22">
					<td align="center" class="plainlabelred" colspan="2" id="tdqt" runat="server"></td>
				</tr>
				<tr>
					<td class="redlabel"><asp:Label id="lang3697" runat="server">Query Timeout (Seconds)</asp:Label></td>
					<td>
						<asp:TextBox id="txtqt" runat="server" Width="48px"></asp:TextBox></td>
				</tr>
				<tr>
					<td class="bluelabel"><asp:Label id="lang3698" runat="server">Administrator User ID</asp:Label></td>
					<td>
						<asp:textbox id="txtuid" runat="server" Width="120px" Font-Size="X-Small" Font-Names="Arial"></asp:textbox></td>
				</tr>
				<tr>
					<td class="bluelabel"><asp:Label id="lang3699" runat="server">Administrator Password</asp:Label></td>
					<td>
						<asp:textbox id="txtpass" runat="server" Width="120px" Font-Size="X-Small" Font-Names="Arial"
							TextMode="Password"></asp:textbox></td>
				</tr>
				<tr>
					<td align="right" colspan="2"><input type="button" value="Submit" onclick="checkit();"></td>
				</tr>
			</table>
			<input type="hidden" id="lblold" runat="server"> <input type="hidden" id="lblsubmit" runat="server">
			<input type="hidden" id="lblms" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
