<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PMApprovalEq.aspx.vb" Inherits="lucy_r12.PMApprovalEq" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PMApprovalEq</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/PMApprovalEqaspx1.js"></script>

     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  class="tbg">
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 0px" cellSpacing="0" cellPadding="0">
				<tr>
					<td id="tdarch" runat="server"></td>
				</tr>
			</table>
			<input id="lbllog" type="hidden" runat="server" NAME="lbllog">
		<input type="hidden" id="lbluser" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
