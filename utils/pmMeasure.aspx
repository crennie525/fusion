<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmMeasure.aspx.vb" Inherits="lucy_r12.pmMeasure" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>pmMeasure</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/pmMeasureaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" >
		<form id="form1" method="post" runat="server">
			<table>
				<TBODY>
					<tr>
						<td class="bluelabel"><asp:Label id="lang3673" runat="server">Add Measurement</asp:Label></td>
						<td><asp:textbox id="txtnewmeas" runat="server" MaxLength="50"></asp:textbox></td>
						<td><asp:imagebutton id="ibtnadd" runat="server" ImageUrl="../images/appbuttons/bgbuttons/badd.gif"></asp:imagebutton></td>
						<td><IMG onclick="handleexit();" id="bgbreturn" runat="server" alt="" src="../images/appbuttons/bgbuttons/return.gif"
								width="69" height="19"></td>
					</tr>
					<tr id="trmsg" runat="server">
						<td class="redlabel" id="tdnotypes" align="center" colSpan="4" runat="server"></td>
					</tr>
					<tr>
						<td colspan="4" align="center" class="label"><asp:Label id="lang3674" runat="server">Measurement</asp:Label></td>
					</tr>
					<tr id="trtypes" runat="server">
						<td align="center" colSpan="4">
							<div style="BORDER-BOTTOM: 2px groove; BORDER-LEFT: 2px groove; BACKGROUND-COLOR: white; WIDTH: 200px; HEIGHT: 150px; OVERFLOW: auto; BORDER-TOP: 2px groove; BORDER-RIGHT: 2px groove"><asp:repeater id="rptrtypes" runat="server">
									<HeaderTemplate>
										<table cellspacing="0">
									</HeaderTemplate>
									<ItemTemplate>
										<tr class="tbg">
											<td class="plainlabel" width="150"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"measurement")%>' Runat = server ID="lbltasknum">
												</asp:Label></td>
											<td width="20"><asp:ImageButton id="ibtndel1" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
													CommandName="Delete"></asp:ImageButton></td>
											<td class="details"><asp:Label id="lbltid"  Text='<%# DataBinder.Eval(Container.DataItem,"typeid")%>' Runat = server>
												</asp:Label></td>
											<td class="details"><asp:Label id="lblmeas"  Text='<%# DataBinder.Eval(Container.DataItem,"measid")%>' Runat = server>
												</asp:Label></td>
										</tr>
									</ItemTemplate>
									<AlternatingItemTemplate>
										<tr bgcolor="#E7F1FD">
											<td class="plainlabel"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"measurement")%>' Runat = server ID="Label1">
												</asp:Label></td>
											<td><asp:ImageButton id="ibtndel2" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
													CommandName="Delete"></asp:ImageButton></td>
											<td class="details"><asp:Label id="lbltidalt"  Text='<%# DataBinder.Eval(Container.DataItem,"typeid")%>' Runat = server>
												</asp:Label></td>
											<td class="details"><asp:Label id="lblmeasalt"  Text='<%# DataBinder.Eval(Container.DataItem,"measid")%>' Runat = server>
												</asp:Label></td>
										</tr>
									</AlternatingItemTemplate>
									<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></DIV></TD></TR></TBODY></TABLE><input id="lbltypeid" type="hidden" runat="server" NAME="lbltypeid">
			<input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
