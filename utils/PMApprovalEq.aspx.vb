

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class PMApprovalEq
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, Login, ustr As String
    Dim dr As SqlDataReader
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim nmm As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdarch As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here

        If Not IsPostBack Then
            ustr = Request.QueryString("ustr").ToString
            lbluser.Value = ustr
            nmm.Open()
            GetArch()
            nmm.Close()
        End If
      
    End Sub
    Private Sub GetArch()
        Dim siteid As String = HttpContext.Current.Session("dfltps").ToString()
        Dim comp As String = HttpContext.Current.Session("comp").ToString()
        'Dim siteid As String = "3"
        Dim sb As StringBuilder = New StringBuilder
        Dim eqnum As String = "eqcopytest"
        Dim eqdesc As String = ""
        Dim eqid As String = "129"
        sb.Append("<table cellspacing=""0"" border=""0""><tr>")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""155""></tr>" & vbCrLf)
        'sql = "select distinct e.siteid, e.dept_id, e.cellid, e.eqid, e.eqnum, f.func_id, " _
        '+ "f.func, c.comid, c.compnum, cnt = (select count(c.compnum) from components c " _
        '+ "where c.func_id = f.func_id) from equipment e join functions f on " _
        '+ "f.eqid = e.eqid join components c on c.func_id = f.func_id " _
        '+ " where siteid = '" & siteid & "'"

        'sql = "select distinct e.siteid, e.dept_id, d.dept_line, " _
        '+ "isnull(cl.cell_name, '') as 'cellname', e.cellid, e.eqid, e.eqnum, " _
        '+ "isnull(f.func_id, 0) as func_id, " _
        '+ "f.func, isnull(c.comid, 0) as comid, c.compnum, cnt = (select count(c.compnum) " _
        '+ "from components c where c.func_id = f.func_id) " _
        '+ "from equipment e left outer join functions f on f.eqid = e.eqid " _
        '+ "left outer join components c on c.func_id = f.func_id  " _
        '+ "right join dept d on d.dept_id = e.dept_id right join cells cl on cl.cellid = e.cellid " _
        '+ "where e.siteid = '" & siteid & "'"

        sql = "select distinct e.siteid, e.dept_id, " _
        + "e.cellid, e.eqid, e.eqnum, isnull(eqdesc, 'No Description') as eqdesc, " _
        + "e.locked, e.lockedby, " _
        + "isnull(f.func_id, 0) as func_id, " _
        + "f.func, isnull(c.comid, 0) as comid, c.compnum, cnt = (select count(c.compnum) " _
        + "from components c where c.func_id = f.func_id) " _
        + "from equipment e left outer join functions f on f.eqid = e.eqid " _
        + "left outer join components c on c.func_id = f.func_id  " _
        + "where e.siteid = '" & siteid & "' and e.compid = '" & comp & "'"

        'h.Open()
        dr = nmm.GetRdrData(sql)
        Dim locby As String
        Dim lock As String = "0"
        Dim fid As String = "0"
        Dim cid As String = "0"
        Dim eid As String = "0"
        Dim cidhold As Integer = 0
        Dim sid, did, clid, chk As String
        Dim cnt As Integer = 0
        While dr.Read
            If dr.Item("eqid") <> eid Then
                If eid <> 0 Then
                    sb.Append("</table></td></tr>")
                End If
                eid = dr.Item("eqid").ToString
                sid = dr.Item("siteid").ToString
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                If clid <> "" Then
                    chk = "yes"
                Else
                    chk = "no"
                End If
                eqnum = dr.Item("eqnum").ToString
                eqdesc = dr.Item("eqdesc").ToString
                lock = dr.Item("locked").ToString
                locby = dr.Item("lockedby").ToString
                sb.Append("<tr><td><img id='i" + eid + "' ")
                sb.Append("onclick=""fclose('t" + eid + "', 'i" + eid + "');""")
                sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>")
                sb.Append("<td colspan=""2"" class=""plainlabel""><a href=""#""")
                sb.Append("onclick=""gotoeq('" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "')""")
                sb.Append("class=""linklabel"" >" & eqnum & "</a> - " & eqdesc)
                If lock = "0" OrElse Len(lock) = 0 Then
                    sb.Append("</td></tr>" & vbCrLf)
                Else
                    sb.Append("&nbsp;<img src='../images/appbuttons/minibuttons/lillock.gif' ")
                    sb.Append("onmouseover=""return overlib('" & tmod.getov("cov363" , "PMApprovalEq.aspx.vb") & ": " &  locby & "')"" ")
                    sb.Append("onmouseout=""return nd()""></td></tr>" & vbCrLf)
                End If

                'onmouseover=""return overlib('" & dr.Item("dept_line").ToString & "', BELOW, LEFT)"" onmouseout=""return nd()""
                sb.Append("<tr><td></td><td colspan=""2""><table class=""details"" cellspacing=""0"" id='t" + eid + "' border=""0"">")
                sb.Append("<tr><td width==""15""></td><td width==""155""></td></tr>")
            End If


            If dr.Item("func_id").ToString <> fid Then
                If dr.Item("func_id").ToString <> "0" Then
                    eid = dr.Item("eqid").ToString
                    fid = dr.Item("func_id").ToString
                    sid = dr.Item("siteid").ToString
                    did = dr.Item("dept_id").ToString
                    clid = dr.Item("cellid").ToString
                    cidhold = dr.Item("cnt").ToString
                    sb.Append("<tr>" & vbCrLf & "<td colspan=""2""><table cellspacing=""0"" border=""0"">" & vbCrLf)
                    sb.Append("<tr><td><img id='i" + fid + "' onclick=""fclose('t" + fid + "', 'i" + fid + "');"" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                    sb.Append("<td class=""plainlabel"">" & dr.Item("func").ToString & "</td></tr></table></td></tr>" & vbCrLf)
                    If dr.Item("comid").ToString <> cid Then
                        If dr.Item("comid").ToString <> "0" Then
                            cid = dr.Item("comid").ToString
                            eid = dr.Item("eqid").ToString
                            fid = dr.Item("func_id").ToString
                            sid = dr.Item("siteid").ToString
                            did = dr.Item("dept_id").ToString
                            clid = dr.Item("cellid").ToString
                            If cnt = 0 Then
                                cnt = cnt + 1
                                sb.Append("<tr><td width=""15""></td>" & vbCrLf & "<td width=""155""><table border=""0"" class=""details"" cellspacing=""0"" id=""t" + fid + """>" & vbCrLf)
                                sb.Append("<tr><td class=""plainlabel"">" & dr.Item("compnum").ToString & "</td></tr>" & vbCrLf)
                                If cnt = cidhold Then
                                    cnt = 0
                                    sb.Append("</table></td></tr>")
                                End If
                            End If
                        Else
                            'cnt = 0
                            'sb.Append("</table></td></tr>")
                        End If

                    End If
                Else
                    fid = "0"
                End If

            ElseIf dr.Item("comid").ToString <> cid Then
                If fid <> "0" Then
                    cid = dr.Item("comid").ToString
                    If cnt = 0 Then
                        cnt = cnt + 1
                        sb.Append("<tr><td></td><td></td>" & vbCrLf & "<td><table cellspacing=""0"" id=""t" + fid + """>" & vbCrLf)
                        sb.Append("<tr><td class=""plainlabel"">" & dr.Item("compnum").ToString & "</td></tr>" & vbCrLf)
                    Else
                        cnt = cnt + 1
                        sb.Append("<tr><td class=""plainlabel"">" & dr.Item("compnum").ToString & "</td></tr>" & vbCrLf)
                    End If
                    If cnt = cidhold Then
                        cnt = 0
                        sb.Append("</table></td></tr>")
                    End If
                    'Else
                    'cid = "0"
                    'cnt = 0
                    'sb.Append("</table></td></tr>")
                End If

            End If

        End While
        dr.Close()
        'h.Dispose()
        sb.Append("</td></tr></table></td></tr></table>")
        'Response.Write(sb.ToString)
        tdarch.InnerHtml = sb.ToString
    End Sub

End Class
