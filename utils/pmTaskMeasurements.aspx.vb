

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class pmTaskMeasurements
    Inherits System.Web.UI.Page
    Protected WithEvents lang3696 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3695 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3694 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3693 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3692 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3691 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3690 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3689 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3688 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3687 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3686 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3685 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3684 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3683 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3682 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3681 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3680 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3679 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3678 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3677 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tm As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim taskid, eqid, func, comp, cid, type, login, typ, wonum, jpid, ro, rev, wojtid As String
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdco As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents t1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents t2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents t3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdspec3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdstype3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddtypes As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tdnomeas As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents rettype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrettype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddmeas As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lbltypeid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmeasid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rblchar As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents lblchr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblout As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdout As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents iaddtype As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iaddmeas As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwojtid As System.Web.UI.HtmlControls.HtmlInputHidden

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdstype As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trspec As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdspec As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdspec2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdstype2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents troutput As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents rptrmeas As System.Web.UI.WebControls.Repeater

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load



        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            If lbllog.Value <> "no" Then
                'Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                lblro.Value = ro
                If ro = "1" Then
                    iaddtype.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                    iaddmeas.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                    iaddtype.Attributes.Add("onclick", "")
                    iaddmeas.Attributes.Add("onclick", "")
                    btnadd.Attributes.Add("src", "../images/appbuttons/bgbuttons/badddis.gif")
                    btnadd.Attributes.Add("onclick", "")
                Else
                    'btnadd.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
                    'btnadd.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
                End If
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                If typ = "pm" Or typ = "tpm" Or typ = "rev" Or typ = "trev" Or typ = "co" Then
                    If typ = "rev" Then
                        rev = Request.QueryString("rev").ToString
                        lblrev.Value = rev
                    End If
                    taskid = Request.QueryString("taskid").ToString
                    lbltaskid.Value = taskid
                    If Len(taskid) <> 0 AndAlso taskid <> "" AndAlso taskid <> "0" Then
                        comp = Request.QueryString("coid").ToString
                        lblcoid.Value = comp
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr1749", "pmTaskMeasurements.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        lbllog.Value = "nodeptid"
                    End If
                ElseIf typ = "wo" Then
                    wonum = Request.QueryString("wo").ToString
                    lblwonum.Value = wonum
                ElseIf typ = "jp" Or typ = "wjp" Then
                    Try
                        lblwojtid.Value = Request.QueryString("wojtid").ToString
                    Catch ex As Exception

                    End Try
                    taskid = Request.QueryString("taskid").ToString
                    lbltaskid.Value = taskid
                    jpid = Request.QueryString("jpid").ToString
                    lbljpid.Value = jpid
                    wonum = Request.QueryString("wo").ToString
                    lblwonum.Value = wonum
                End If
                cid = HttpContext.Current.Session("comp").ToString
                lblcid.Value = cid
                tm.Open()
                LoadTypes(cid)
                LoadHeader(comp)
                LoadMeas(taskid)
                tm.Dispose()
                ClearInput()
                rblchar.Enabled = False
                ddmeas.Enabled = False
                lbltypeid.Value = "0"
                lblmeasid.Value = "0"
                lblchr.Value = "range"
                'Catch ex As Exception
                'Dim strMessage As String =  tmod.getmsg("cdstr1750" , "pmTaskMeasurements.aspx.vb")

                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                'lbllog.Value = "nodeptid"
                'End Try
            End If
        Else
            If lbllog.Value <> "no" Then
                If Request.Form("lblrettype") = "type" Then
                    cid = lblcid.Value
                    tm.Open()
                    LoadTypes(cid)
                    tm.Dispose()
                ElseIf Request.Form("lblrettype") = "meas" Then
                    type = lbltypeid.Value
                    tm.Open()
                    LoadDDMeas(type)
                    tm.Dispose()
                ElseIf Request.Form("lblrettype") = "add" Then
                    taskid = lbltaskid.Value
                    tm.Open()
                    AddMeas(taskid)
                    tm.Dispose()
                End If
                lblrettype.Value = ""
            End If
        End If

        'btnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'btnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub

    Private Sub ClearInput()
        t1.Text = ""
        t1.Enabled = False
        tdstype.InnerHtml = ""
        tdspec.Attributes.Add("class", "disablelabel")
        t2.Text = ""
        t2.Enabled = False
        tdstype2.InnerHtml = ""
        tdspec2.Attributes.Add("class", "disablelabel")
        t3.Text = ""
        t3.Enabled = False
        tdstype3.InnerHtml = ""
        tdspec3.Attributes.Add("class", "disablelabel")
    End Sub

    Private Sub LoadTypes(ByVal cid As String)
        ClearInput()
        rblchar.Enabled = False
        ddmeas.Enabled = False
        lbltypeid.Value = "0"
        lblmeasid.Value = "0"
        sql = "select * from pmMeasureTypes where compid = '" & cid & "' order by type"
        dr = tm.GetRdrData(sql)
        ddtypes.DataSource = dr
        ddtypes.DataValueField = "typeid"
        ddtypes.DataTextField = "type"
        ddtypes.DataBind()
        dr.Close()
        ddtypes.Items.Insert(0, "Select Type")
    End Sub

    Private Sub LoadHeader(ByVal comid As String)
        typ = lbltyp.Value
        If typ = "pm" Or typ = "tpm" Then
            sql = "select c.compnum, f.func, e.eqnum from components c " _
            + "left join functions f on f.func_id = c.func_id " _
            + "left join equipment e on e.eqid = f.eqid " _
            + "where c.comid = '" & comid & "'"
            dr = tm.GetRdrData(sql)

            While dr.Read
                tdeq.InnerHtml = dr.Item("eqnum").ToString
                tdfu.InnerHtml = dr.Item("func").ToString
                tdco.InnerHtml = dr.Item("compnum").ToString

            End While
        ElseIf typ = "rev" Then
            rev = lblrev.Value
            sql = "select c.compnum, f.func, e.eqnum from componentsarch c " _
                        + "left join functionsarch f on f.func_id = c.func_id and f.rev = c.rev " _
                        + "left join equipmentarch e on e.eqid = f.eqid and e.rev = f.rev " _
                        + "where c.comid = '" & comid & "' and c.rev = '" & rev & "'"
            dr = tm.GetRdrData(sql)

            While dr.Read
                tdeq.InnerHtml = dr.Item("eqnum").ToString
                tdfu.InnerHtml = dr.Item("func").ToString
                tdco.InnerHtml = dr.Item("compnum").ToString

            End While
        ElseIf typ = "trev" Then
            rev = lblrev.Value
            sql = "select c.compnum, f.func, e.eqnum from componentstpmarch c " _
                        + "left join functionstpmarch f on f.func_id = c.func_id and f.rev = c.rev " _
                        + "left join equipmenttpmarch e on e.eqid = f.eqid and e.rev = f.rev " _
                        + "where c.comid = '" & comid & "' and c.rev = '" & rev & "'"
            dr = tm.GetRdrData(sql)

            While dr.Read
                tdeq.InnerHtml = dr.Item("eqnum").ToString
                tdfu.InnerHtml = dr.Item("func").ToString
                tdco.InnerHtml = dr.Item("compnum").ToString

            End While
        ElseIf typ = "wo" Then
            wonum = lblwonum.Value
            sql = "select c.compnum, f.func, e.eqnum from workorder w " _
            + "left join components c on c.comid = w.comid " _
            + "left join functions f on f.func_id = w.funcid " _
            + "left join equipment e on e.eqid = w.eqid " _
            + "where w.wonum = '" & wonum & "'"
            dr = tm.GetRdrData(sql)

            While dr.Read
                tdeq.InnerHtml = dr.Item("eqnum").ToString
                tdfu.InnerHtml = dr.Item("func").ToString
                tdco.InnerHtml = dr.Item("compnum").ToString

            End While
        ElseIf typ = "co" Then
            sql = "select compnum from complib " _
            + "where comid = '" & comid & "'"
            dr = tm.GetRdrData(sql)

            While dr.Read
                tdeq.InnerHtml = "N\A"
                tdfu.InnerHtml = "N\A"
                tdco.InnerHtml = dr.Item("compnum").ToString

            End While
        End If



        dr.Close()
    End Sub

    Private Sub LoadMeas(ByVal taskid As String)
        typ = lbltyp.Value
        rev = lblrev.Value
        wonum = lblwonum.Value
        jpid = lbljpid.Value
        If typ = "pm" Then
            sql = "select * from pmTaskMeasDetails where pmtskid = '" & taskid & "' and jpid is null"
        ElseIf typ = "co" Then
            sql = "select * from compTaskMeasDetails where pmtskid = '" & taskid & "' and jpid is null"
        ElseIf typ = "tpm" Then
            sql = "select * from pmTaskMeasDetailstpm where pmtskid = '" & taskid & "' and jpid is null"
        ElseIf typ = "rev" Then
            sql = "select * from pmTaskMeasDetailsarch where pmtskid = '" & taskid & "' and jpid is null and rev = '" & rev & "'"
        ElseIf typ = "trev" Then
            sql = "select * from pmTaskMeasDetailstpmarch where pmtskid = '" & taskid & "' and jpid is null and rev = '" & rev & "'"
        ElseIf typ = "wo" Then
            sql = "select *, womid as tmdid from womeasdetails where wonum = '" & wonum & "'"
        ElseIf typ = "jp" Then
            sql = "select * from pmTaskMeasDetails where pmtskid = '" & taskid & "' and jpid = '" & jpid & "'"
        ElseIf typ = "wjp" Then
            sql = "select * from wojpTaskMeasDetails where wojtid = '" & taskid & "' and jpid = '" & jpid & "' and wonum = '" & wonum & "'"
        End If

        dr = tm.GetRdrData(sql)
        If dr.HasRows Then
            rptrmeas.DataSource = dr
            rptrmeas.DataBind()
            tdnomeas.InnerHtml = ""
        Else
            rptrmeas.DataSource = Nothing
            rptrmeas.DataBind()
            tdnomeas.InnerHtml = "No Measurements Loaded for this Task"
        End If
        dr.Close()
        If typ = "pm" Then
            sql = "select measurement from pmTaskMeasOutput where pmtskid = '" & taskid & "' and jpid is null"
        ElseIf typ = "co" Then
            sql = "select measurement from compTaskMeasOutput where pmtskid = '" & taskid & "' and jpid is null"
        ElseIf typ = "tpm" Then
            sql = "select measurement from pmTaskMeasOutputtpm where pmtskid = '" & taskid & "' and jpid is null"
        ElseIf typ = "rev" Then
            sql = "select measurement from pmTaskMeasOutputarch where pmtskid = '" & taskid & "' and jpid is null and rev = '" & rev & "'"
        ElseIf typ = "trev" Then
            sql = "select measurement from pmTaskMeasOutputtpmarch where pmtskid = '" & taskid & "' and jpid is null and rev = '" & rev & "'"
        ElseIf typ = "wo" Then
            sql = "select measurement from womeasoutput where wonum = '" & wonum & "'"
        ElseIf typ = "jp" Then
            sql = "select measurement from pmTaskMeasOutput where pmtskid = '" & taskid & "' and jpid = '" & jpid & "'"
        ElseIf typ = "wjp" Then
            sql = "select measurement from wojpTaskMeasOutput where wojtid = '" & taskid & "' and jpid = '" & jpid & "'"
        End If

        dr = tm.GetRdrData(sql)
        If dr.HasRows Then
            While dr.Read
                tdout.InnerHtml = dr.Item("measurement").ToString
            End While
        Else
            If typ = "pm" Then
                tdout.InnerHtml = "No Report Output Loaded for this Task"
            ElseIf typ = "wo" Then
                tdout.InnerHtml = "No Report Output Loaded for this Work Order"
            End If

        End If
        dr.Close()
    End Sub
    Private Sub LoadDDMeas(ByVal type As String)
        ClearInput()
        rblchar.Enabled = False
        ddmeas.Enabled = True
        lblmeasid.Value = "0"
        sql = "select * from pmMeasurements where typeid = '" & type & "' order by measurement"
        dr = tm.GetRdrData(sql)
        ddmeas.DataSource = dr
        ddmeas.DataValueField = "measid"
        ddmeas.DataTextField = "measurement"
        ddmeas.DataBind()
        dr.Close()
        ddmeas.Items.Insert(0, "Select Measurement")
    End Sub

    Private Sub ddtypes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddtypes.SelectedIndexChanged
        If ddtypes.SelectedIndex <> 0 Then
            Dim type As String = ddtypes.SelectedValue.ToString
            lbltypeid.Value = type
            tm.Open()
            LoadDDMeas(type)
            tm.Dispose()
        End If
    End Sub

    Private Sub ddmeas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddmeas.SelectedIndexChanged
        If ddmeas.SelectedIndex <> 0 Then
            Dim measi As String = ddmeas.SelectedValue.ToString
            lblmeasid.Value = measi
            Dim meas As String = ddmeas.SelectedItem.ToString
            rblchar.Enabled = True
            SetInput(meas)
        End If
    End Sub

    Private Sub SetInput(ByVal meas As String)
        Dim chr As String = rblchar.SelectedValue.ToString
        Select Case chr
            Case "spec"
                lblchr.Value = "spec"
                t1.Text = ""
                t1.Enabled = False
                tdstype.InnerHtml = ""
                tdspec.Attributes.Add("class", "disablelabel")
                t2.Text = ""
                t2.Enabled = False
                tdstype2.InnerHtml = ""
                tdspec2.Attributes.Add("class", "disablelabel")
                t3.Text = ""
                t3.Enabled = True
                tdstype3.InnerHtml = meas
                tdspec3.Attributes.Add("class", "bluelabellt")
            Case "range"
                lblchr.Value = "range"
                t1.Text = ""
                t1.Enabled = True
                tdstype.InnerHtml = meas
                tdspec.Attributes.Add("class", "bluelabellt")
                t2.Text = ""
                t2.Enabled = True
                tdstype2.InnerHtml = meas
                tdspec2.Attributes.Add("class", "bluelabellt")
                t3.Text = ""
                t3.Enabled = True
                tdstype3.InnerHtml = ""
                tdspec3.Attributes.Add("class", "bluelabellt")
            Case "max"
                lblchr.Value = "max"
                t1.Text = ""
                t1.Enabled = True
                tdstype.InnerHtml = meas
                tdspec.Attributes.Add("class", "bluelabellt")
                t2.Text = ""
                t2.Enabled = False
                tdstype2.InnerHtml = ""
                tdspec2.Attributes.Add("class", "disablelabel")
                t3.Text = ""
                t3.Enabled = True
                tdstype3.InnerHtml = ""
                tdspec3.Attributes.Add("class", "bluelabellt")
            Case "rec"
                lblchr.Value = "rec"
                ClearInput()
                Dim strMessage As String = tmod.getmsg("cdstr1751", "pmTaskMeasurements.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Select

    End Sub

    Private Sub rblchar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblchar.SelectedIndexChanged
        Dim measi As String = ddmeas.SelectedValue.ToString
        lblmeasid.Value = measi
        Dim meas As String = ddmeas.SelectedItem.ToString
        SetInput(meas)
    End Sub

    Private Sub rptrmeas_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrmeas.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                         e.Item.ItemType <> ListItemType.Footer Then
            Dim deleteButton As ImageButton
            ro = lblro.Value
            If e.Item.ItemType = ListItemType.Item Then
                deleteButton = CType(e.Item.FindControl("ibtndel1"), ImageButton)
                If ro = "1" Then
                    deleteButton.ImageUrl = "../images/appbuttons/minibuttons/deldis.gif"
                    deleteButton.Enabled = False
                Else

                End If

            ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
                deleteButton = CType(e.Item.FindControl("ibtndel2"), ImageButton)
                If ro = "1" Then
                    deleteButton.ImageUrl = "../images/appbuttons/minibuttons/deldis.gif"
                    deleteButton.Enabled = False
                Else

                End If

            End If
            'We can now add the onclick event handler
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete this Measurement?')"

        End If

        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang3677 As Label
                lang3677 = CType(e.Item.FindControl("lang3677"), Label)
                lang3677.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3677")
            Catch ex As Exception
            End Try
            Try
                Dim lang3678 As Label
                lang3678 = CType(e.Item.FindControl("lang3678"), Label)
                lang3678.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3678")
            Catch ex As Exception
            End Try
            Try
                Dim lang3679 As Label
                lang3679 = CType(e.Item.FindControl("lang3679"), Label)
                lang3679.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3679")
            Catch ex As Exception
            End Try
            Try
                Dim lang3680 As Label
                lang3680 = CType(e.Item.FindControl("lang3680"), Label)
                lang3680.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3680")
            Catch ex As Exception
            End Try
            Try
                Dim lang3681 As Label
                lang3681 = CType(e.Item.FindControl("lang3681"), Label)
                lang3681.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3681")
            Catch ex As Exception
            End Try
            Try
                Dim lang3682 As Label
                lang3682 = CType(e.Item.FindControl("lang3682"), Label)
                lang3682.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3682")
            Catch ex As Exception
            End Try
            Try
                Dim lang3683 As Label
                lang3683 = CType(e.Item.FindControl("lang3683"), Label)
                lang3683.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3683")
            Catch ex As Exception
            End Try
            Try
                Dim lang3684 As Label
                lang3684 = CType(e.Item.FindControl("lang3684"), Label)
                lang3684.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3684")
            Catch ex As Exception
            End Try
            Try
                Dim lang3685 As Label
                lang3685 = CType(e.Item.FindControl("lang3685"), Label)
                lang3685.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3685")
            Catch ex As Exception
            End Try
            Try
                Dim lang3686 As Label
                lang3686 = CType(e.Item.FindControl("lang3686"), Label)
                lang3686.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3686")
            Catch ex As Exception
            End Try
            Try
                Dim lang3687 As Label
                lang3687 = CType(e.Item.FindControl("lang3687"), Label)
                lang3687.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3687")
            Catch ex As Exception
            End Try
            Try
                Dim lang3688 As Label
                lang3688 = CType(e.Item.FindControl("lang3688"), Label)
                lang3688.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3688")
            Catch ex As Exception
            End Try
            Try
                Dim lang3689 As Label
                lang3689 = CType(e.Item.FindControl("lang3689"), Label)
                lang3689.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3689")
            Catch ex As Exception
            End Try
            Try
                Dim lang3690 As Label
                lang3690 = CType(e.Item.FindControl("lang3690"), Label)
                lang3690.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3690")
            Catch ex As Exception
            End Try
            Try
                Dim lang3691 As Label
                lang3691 = CType(e.Item.FindControl("lang3691"), Label)
                lang3691.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3691")
            Catch ex As Exception
            End Try
            Try
                Dim lang3692 As Label
                lang3692 = CType(e.Item.FindControl("lang3692"), Label)
                lang3692.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3692")
            Catch ex As Exception
            End Try
            Try
                Dim lang3693 As Label
                lang3693 = CType(e.Item.FindControl("lang3693"), Label)
                lang3693.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3693")
            Catch ex As Exception
            End Try
            Try
                Dim lang3694 As Label
                lang3694 = CType(e.Item.FindControl("lang3694"), Label)
                lang3694.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3694")
            Catch ex As Exception
            End Try
            Try
                Dim lang3695 As Label
                lang3695 = CType(e.Item.FindControl("lang3695"), Label)
                lang3695.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3695")
            Catch ex As Exception
            End Try
            Try
                Dim lang3696 As Label
                lang3696 = CType(e.Item.FindControl("lang3696"), Label)
                lang3696.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3696")
            Catch ex As Exception
            End Try

        End If

    End Sub

    Private Sub rptrmeas_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrmeas.ItemCommand
        If e.CommandName = "Delete" Then
            Dim tmdid As String
            taskid = lbltaskid.Value
            wonum = lblwonum.Value
            jpid = lbljpid.Value
            If e.Item.ItemType = ListItemType.Item Then
                tmdid = CType(e.Item.FindControl("lbltmdid1"), Label).Text
            ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
                tmdid = CType(e.Item.FindControl("lbltmdid2"), Label).Text
            End If
            typ = lbltyp.Value
            If typ = "pm" Then
                sql = "usp_delTaskMeas '" & tmdid & "', '" & taskid & "','pm'"
            ElseIf typ = "co" Then
                sql = "usp_delTaskMeas '" & tmdid & "', '" & taskid & "','co'"
            ElseIf typ = "tpm" Then
                sql = "usp_delTaskMeas '" & tmdid & "', '" & taskid & "','tpm'"
            ElseIf typ = "wo" Then
                sql = "usp_delTaskMeas '" & tmdid & "', '" & wonum & "','" & typ & "'"
            ElseIf typ = "jp" Or typ = "wjp" Then
                sql = "usp_delTaskMeas '" & tmdid & "', '" & wonum & "','" & typ & "','" & jpid & "','" & wonum & "'"
            End If

            tm.Open()
            tm.Update(sql)
            type = lbltypeid.Value
            LoadMeas(taskid)
            tm.Dispose()
        End If
    End Sub
    Private Sub AddMeas(ByVal taskid As String)
        Dim chr As String = rblchar.SelectedValue.ToString
        Dim meas As String = ddmeas.SelectedItem.ToString
        Dim type As String = ddtypes.SelectedItem.ToString
        Dim hi, lo, spec, max, rec, out As String
        jpid = lbljpid.Value
        wonum = lblwonum.Value
        Select Case chr
            Case "range"
                hi = t1.Text
                lo = t2.Text
                spec = t3.Text '"NA"
                rec = "Yes"
                out = "(___)Hi:" & hi & meas & "-Lo:" & lo & meas & "-Spec:" & spec & meas & ";"
            Case "spec"
                hi = "NA"
                lo = "NA"
                spec = t3.Text
                rec = "Yes"
                out = "(___)Spec:" & spec & " " & meas & ";"
            Case "max"
                hi = t1.Text
                lo = "NA"
                spec = t3.Text '"NA"
                rec = "Yes"
                out = "(___)Max:" & hi & " " & meas & ";"
            Case "rec"
                hi = "NA"
                lo = "NA"
                spec = "NA"
                rec = "Yes"
                out = "(___)" & meas & ";"
        End Select
        typ = lbltyp.Value

        If typ = "pm" Then
            sql = "usp_addTaskMeas '" & taskid & "', '" & type & "', '" & meas & "', '" & hi & "', '" & lo & "', " _
       + "'" & spec & "', '" & rec & "', '" & out & "','pm'"
        ElseIf typ = "co" Then
            sql = "usp_addTaskMeas '" & taskid & "', '" & type & "', '" & meas & "', '" & hi & "', '" & lo & "', " _
       + "'" & spec & "', '" & rec & "', '" & out & "','co'"
        ElseIf typ = "tpm" Then
            sql = "usp_addTaskMeas '" & taskid & "', '" & type & "', '" & meas & "', '" & hi & "', '" & lo & "', " _
       + "'" & spec & "', '" & rec & "', '" & out & "','tpm'"
        ElseIf typ = "wo" Then
            wonum = lblwonum.Value
            sql = "usp_addTaskMeas '" & wonum & "', '" & type & "', '" & meas & "', '" & hi & "', '" & lo & "', " _
       + "'" & spec & "', '" & rec & "', '" & out & "','wo'"
        ElseIf typ = "jp" Then
            sql = "usp_addTaskMeas '" & taskid & "', '" & type & "', '" & meas & "', '" & hi & "', '" & lo & "', " _
       + "'" & spec & "', '" & rec & "', '" & out & "','jp','" & jpid & "','" & wonum & "'"
        ElseIf typ = "jp" Then
            sql = "usp_addTaskMeas '" & taskid & "', '" & type & "', '" & meas & "', '" & hi & "', '" & lo & "', " _
       + "'" & spec & "', '" & rec & "', '" & out & "','wjp','" & jpid & "','" & wonum & "'"
        End If
        'tm.Open()
        tm.Update(sql)
        tm.UpModTask(taskid)
        type = lbltypeid.Value
        LoadMeas(taskid)
        'tm.Dispose()
        lbltypeid.Value = "0"
        lblmeasid.Value = "0"
        Try
            ddtypes.SelectedIndex = 0
        Catch ex As Exception

        End Try

        ddmeas.Enabled = False
        rblchar.Enabled = False
        ClearInput()
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3677.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3677")
        Catch ex As Exception
        End Try
        Try
            lang3678.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3678")
        Catch ex As Exception
        End Try
        Try
            lang3679.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3679")
        Catch ex As Exception
        End Try
        Try
            lang3680.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3680")
        Catch ex As Exception
        End Try
        Try
            lang3681.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3681")
        Catch ex As Exception
        End Try
        Try
            lang3682.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3682")
        Catch ex As Exception
        End Try
        Try
            lang3683.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3683")
        Catch ex As Exception
        End Try
        Try
            lang3684.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3684")
        Catch ex As Exception
        End Try
        Try
            lang3685.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3685")
        Catch ex As Exception
        End Try
        Try
            lang3686.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3686")
        Catch ex As Exception
        End Try
        Try
            lang3687.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3687")
        Catch ex As Exception
        End Try
        Try
            lang3688.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3688")
        Catch ex As Exception
        End Try
        Try
            lang3689.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3689")
        Catch ex As Exception
        End Try
        Try
            lang3690.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3690")
        Catch ex As Exception
        End Try
        Try
            lang3691.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3691")
        Catch ex As Exception
        End Try
        Try
            lang3692.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3692")
        Catch ex As Exception
        End Try
        Try
            lang3693.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3693")
        Catch ex As Exception
        End Try
        Try
            lang3694.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3694")
        Catch ex As Exception
        End Try
        Try
            lang3695.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3695")
        Catch ex As Exception
        End Try
        Try
            lang3696.Text = axlabs.GetASPXPage("pmTaskMeasurements.aspx", "lang3696")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                btnadd.Attributes.Add("src", "../images2/eng/bgbuttons/badd.gif")
            ElseIf lang = "fre" Then
                btnadd.Attributes.Add("src", "../images2/fre/bgbuttons/badd.gif")
            ElseIf lang = "ger" Then
                btnadd.Attributes.Add("src", "../images2/ger/bgbuttons/badd.gif")
            ElseIf lang = "ita" Then
                btnadd.Attributes.Add("src", "../images2/ita/bgbuttons/badd.gif")
            ElseIf lang = "spa" Then
                btnadd.Attributes.Add("src", "../images2/spa/bgbuttons/badd.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
