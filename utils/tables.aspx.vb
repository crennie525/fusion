

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class tables
    Inherits System.Web.UI.Page
	Protected WithEvents lang3718 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3717 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tab As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents tdv As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcurr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sql, curr As String
    Dim sessid, Login, username, userid As String
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdarch As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            lblcurr.Value = "tdt"
            tab.Open()
            GetAll("tdt")
            tab.Dispose()
        Else
            If Request.Form("lblsubmit") = "reset" Then
                curr = lblcurr.Value
                tab.Open()
                GetAll(curr)
                tab.Dispose()
            End If
        End If
    End Sub
    Private Sub GetAll(ByVal tdtyp As String)
        Dim sb As StringBuilder = New StringBuilder
        sb.Append("<table cellspacing=""0"" border=""0""><tr>")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""155""></tr>" & vbCrLf)
        If tdtyp = "tdt" Then
            'sql = "SELECT distinct table_name, column_name, DATA_TYPE, " _
            '+ "isnull(cast(CHARACTER_MAXIMUM_LENGTH as varchar(20)), 'N/A') as length " _
            '+ " FROM INFORMATION_SCHEMA.Columns " _
            '+ "WHERE table_schema='dbo' and table_name not like 'v_%' and table_name not like 'ns_%' and table_name not like 'client_%' " _
            '+ "and table_name not like 'dt%' and table_name not in (select outtbl from ns_outtbl)"
            sql = "select * from ns_stables"
        Else
            'sql = "SELECT distinct table_name, column_name, DATA_TYPE, " _
            '+ "isnull(cast(CHARACTER_MAXIMUM_LENGTH as varchar(20)), 'N/A') as length " _
            '+ " FROM INFORMATION_SCHEMA.Columns " _
            '+ "WHERE table_schema='dbo' and table_name like 'v_%'"
            sql = "select * from ns_sviews"
        End If

        Dim htable As String = "na"
        Dim table, col, typ As String
        dr = tab.GetRdrData(sql)
        While dr.Read
            table = dr.Item("table_name").ToString
            typ = dr.Item("length").ToString
            If htable <> table Then
                If htable <> "na" Then
                    sb.Append("</table></td></tr>")
                End If
                htable = dr.Item("table_name").ToString
                sb.Append("<tr><td><img id='i" + htable + "' ")
                sb.Append("onclick=""fclose('t" + htable + "', 'i" + htable + "');""")
                sb.Append(" src=""../images/appbuttons/minibuttons/plus.gif""></td>")
                sb.Append("<td class=""plainlabel"" colspan=""2""><a href=""#"" onclick=""gettab('" & table & "')"" class=""A1"">" & dr.Item("table_name").ToString & "</a></td></tr>")
                sb.Append("<tr class=""details"" id=""t" + htable + """><td></td><td colspan=""2""><table>")
            Else
                table = dr.Item("column_name").ToString
                sb.Append("<tr><td class=""plainlabel"" colspan=""2"">" & dr.Item("column_name").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">&nbsp;&nbsp;" & dr.Item("DATA_TYPE").ToString)
                If typ = "N/A" Then
                    sb.Append("</td></tr>")
                Else
                    sb.Append("(" & typ & ")</td></tr>")
                End If

            End If


        End While
        dr.Close()
        sb.Append("</table>")
        tdarch.InnerHtml = sb.ToString
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3717.Text = axlabs.GetASPXPage("tables.aspx", "lang3717")
        Catch ex As Exception
        End Try
        Try
            lang3718.Text = axlabs.GetASPXPage("tables.aspx", "lang3718")
        Catch ex As Exception
        End Try

    End Sub

End Class
