

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class SubjectMatter
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim Sort As String
    Dim dr As SqlDataReader
    Protected WithEvents lblpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibtnreturn As System.Web.UI.HtmlControls.HtmlImage
    Dim appset As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpre As System.Web.UI.WebControls.Label
    Protected WithEvents dgsm As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpgac As System.Web.UI.WebControls.Label
    Protected WithEvents prePrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents preNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            cid = Request.QueryString("cid")
            lblcid.Value = Request.QueryString("cid")
            cid = lblcid.Value
            lblpg.Value = PageNumber
            PopSM(PageNumber)

        End If
        'prePrev.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yprev.gif'")
        'prePrev.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bprev.gif'")
        'preNext.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ynext.gif'")
        'preNext.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bnext.gif'")
    End Sub
    ' Predictive
    Private Sub PopSM(ByVal PageNumber As Integer)
        'Try
        Try
            appset.Open()
        Catch ex As Exception

        End Try
        cid = lblcid.Value
        sql = "select count(*) " _
        + "from pmAssetClass where compid = '" & cid & "'"
        dgsm.VirtualItemCount = appset.Scalar(sql)
        If dgsm.VirtualItemCount = 0 Then
            lblpre.Text = "No Subject Matter Records Found"
            dgsm.Visible = True
            prePrev.Visible = False
            preNext.Visible = False

            Filter = "compid = " & cid
            Tables = "pmSubjectMatter"
            PK = "smid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgsm.DataSource = dr
            dgsm.DataBind()
            dr.Close()
            appset.Dispose()
        Else
            Filter = "compid = " & cid
            Tables = "pmSubjectMatter"
            PK = "smid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgsm.DataSource = dr
            dgsm.DataBind()
            dr.Close()
            appset.Dispose()
            lblpgac.Text = "Page " & dgsm.CurrentPageIndex + 1 & " of " & dgsm.PageCount
            lblpre.Text = ""
            prePrev.Visible = True
            preNext.Visible = True

        End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub prePrev_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles prePrev.Click
        If (dgsm.CurrentPageIndex > 0) Then
            dgsm.CurrentPageIndex = dgsm.CurrentPageIndex - 1
            lblpg.Value = dgsm.CurrentPageIndex - 1
            PopSM(dgsm.CurrentPageIndex - 1)
        End If
        checkPrePgCnt()
    End Sub
    Private Sub checkPrePgCnt()
        If (dgsm.CurrentPageIndex) = 0 Or dgsm.PageCount = 1 Then
            prePrev.Enabled = False
        Else
            prePrev.Enabled = True
        End If
        If dgsm.PageCount > 1 And (dgsm.CurrentPageIndex + 1 < dgsm.PageCount) Then
            preNext.Enabled = True
        Else
            preNext.Enabled = False
        End If
    End Sub
    Private Sub preNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles preNext.Click
        If ((dgsm.CurrentPageIndex) < (dgsm.PageCount - 1)) Then
            dgsm.CurrentPageIndex = dgsm.CurrentPageIndex + 1
            lblpg.Value = dgsm.CurrentPageIndex + 1
            PopSM(dgsm.CurrentPageIndex + 1)
            checkPrePgCnt()
        End If
        checkPrePgCnt()
    End Sub

    Private Sub dgsm_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgsm.EditCommand
        PageNumber = dgsm.CurrentPageIndex + 1
        dgsm.EditItemIndex = e.Item.ItemIndex
        PageNumber = lblpg.Value
        PopSM(PageNumber)
    End Sub

    Private Sub dgsm_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgsm.CancelCommand
        dgsm.EditItemIndex = -1
        PageNumber = lblpg.Value
        PopSM(PageNumber)
    End Sub

    Private Sub dgsm_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgsm.DeleteCommand
        appset.Open()
        Dim id As String
        id = CType(e.Item.Cells(1).Controls(1), Label).Text
        Dim cid As String = lblcid.Value
        sql = "usp_delSubjectMatter '" & id & "', '" & cid & "'"
        appset.Update(sql)
        dgsm.EditItemIndex = -1
        sql = "select Count(*) from pmAssetClass " _
                + "where compid = '" & cid & "'"
        PageNumber = appset.PageCount(sql, PageSize)
        'appset.Dispose()
        dgsm.EditItemIndex = -1
        If dgsm.CurrentPageIndex > PageNumber Then
            dgsm.CurrentPageIndex = PageNumber - 1
        End If
        If dgsm.CurrentPageIndex < PageNumber - 2 Then
            PageNumber = dgsm.CurrentPageIndex + 1
        End If
        PageNumber = lblpg.Value
        PopSM(PageNumber)
    End Sub

    Private Sub dgsm_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgsm.UpdateCommand
        Dim id, desc As String
        id = CType(e.Item.FindControl("lblsmid"), Label).Text
        desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        If Len(desc) > 0 Then
            If Len(desc) > 50 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1774" , "SubjectMatter.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Else
                appset.Open()
                sql = "update pmSubjectMatter set subj = " _
                + "'" & desc & "' where smid = '" & id & "'"
                appset.Update(sql)
                dgsm.EditItemIndex = -1
                PageNumber = lblpg.Value
                PopSM(PageNumber)
                appset.Dispose()
            End If
        End If    
    End Sub

    Private Sub dgsm_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgsm.ItemCommand
        Dim lname As String
        Dim lnamei As TextBox
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewsm"), TextBox)
            lname = lnamei.Text
            lname = Replace(lname, "'", Chr(180), , , vbTextCompare)
            If lnamei.Text <> "" Then
                If lnamei.Text <> "" Then
                    If Len(lnamei.Text) > 50 Then
                        Dim strMessage As String =  tmod.getmsg("cdstr1775" , "SubjectMatter.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Else
                        appset.Open()
                        Dim stat As String
                        cid = lblcid.Value
                        stat = lnamei.Text
                        sql = "usp_addSubjectMatter '" & lname & "', " & cid
                        appset.Update(sql)
                        Dim statcnt As Integer = dgsm.VirtualItemCount + 1
                        lnamei.Text = ""
                        sql = "select Count(*) from pmSubjectMatter " _
                                        + "where compid = '" & cid & "'"
                        PageNumber = appset.PageCount(sql, PageSize)
                        PopSM(PageNumber)
                        appset.Dispose()
                    End If

                End If
            End If
        End If
    End Sub
	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgsm.Columns(0).HeaderText = dlabs.GetDGPage("SubjectMatter.aspx","dgsm","0")
		Catch ex As Exception
		End Try
		Try
			dgsm.Columns(2).HeaderText = dlabs.GetDGPage("SubjectMatter.aspx","dgsm","2")
		Catch ex As Exception
		End Try

	End Sub
    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                ibtnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub
End Class
