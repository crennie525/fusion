

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text

Public Class squery
    Inherits System.Web.UI.Page
	Protected WithEvents lang3716 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3715 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3714 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3713 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim rep As New Utilities
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Sort As String
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents trmq As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents divmq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents traq As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents divaq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsrch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdall As System.Web.UI.HtmlControls.HtmlTableCell
    Dim username As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            username = Request.QueryString("username").ToString
            lblusername.Value = username
            lblcurr.Value = "tdme"
            txtpg.Value = "1"
            rep.Open()
            GetUser(username)
            rep.Dispose()
        Else
            If Request.Form("lblsubmit").ToString = "srchq" Then
                lblsubmit.Value = ""
                rep.Open()
                txtpg.Value = "1"
                Dim curr As String = lblcurr.Value
                If curr = "tdall" Then
                    GetAll()
                Else
                    username = lblusername.Value
                    GetUser(username)
                End If
                rep.Dispose()
            ElseIf Request.Form("lblsubmit").ToString = "reset" Then
                lblsubmit.Value = ""
                rep.Open()
                txtpg.Value = "1"
                Dim curr As String = lblcurr.Value
                If curr = "tdall" Then
                    GetAll()
                Else
                    username = lblusername.Value
                    GetUser(username)
                End If
                rep.Dispose()
            ElseIf Request.Form("lblsubmit") = "next" Then
                rep.Open()
                GetNext()
                rep.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "last" Then
                rep.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                Dim curr As String = lblcurr.Value
                If curr = "tdall" Then
                    GetAll()
                Else
                    username = lblusername.Value
                    GetUser(username)
                End If
                rep.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "prev" Then
                rep.Open()
                GetPrev()
                rep.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "first" Then
                rep.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                Dim curr As String = lblcurr.Value
                If curr = "tdall" Then
                    GetAll()
                Else
                    username = lblusername.Value
                    GetUser(username)
                End If
                rep.Dispose()
                lblsubmit.Value = ""
            End If
        End If

    End Sub
    Private Function ModString(ByVal str As String)
        str = Replace(str, "'", Chr(180), , , vbTextCompare)
        str = Replace(str, "--", "-", , , vbTextCompare)
        str = Replace(str, ";", ":", , , vbTextCompare)
        Return str
    End Function
    Private Sub GetUser(ByVal username As String)
        Dim sb As New StringBuilder
        Dim srch As String
        srch = lblsrch.Value
        srch = ModString(srch)
        Filter = "username = ''" & username & "'' "
        FilterCnt = "username = '" & username & "' "
        If srch <> "" Then
            Filter += "and queryname like ''%" & srch & "%'' or query like ''%" & srch & "'' "
            FilterCnt += "and queryname like '%" & srch & "%' or query like '%" & srch & "' "
        End If
        sql = "select Count(*) from ns_squery where " & FilterCnt
        Dim dc As Integer = rep.PageCount(sql, PageSize) 'rep.Scalar(sql)
        If dc = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & dc
        End If
        txtpgcnt.Value = dc
        Tables = "ns_squery"
        PK = "qid"
        PageSize = "200"
        Fields = "qid, username, queryname, qdate"
        dr = rep.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        sb.Append("<table>")
        Dim qid, queryname, qdate As String
        While dr.Read
            qid = dr.Item("qid").ToString
            username = dr.Item("username").ToString
            queryname = dr.Item("queryname").ToString
            qdate = dr.Item("qdate").ToString
            sb.Append("<tr><td>")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getq('" & qid & "','" & queryname & "','me')"">")
            sb.Append(queryname)
            sb.Append("</td>")
            sb.Append("<td class=""plainlabel"">" & username)
            sb.Append("</td>")
            sb.Append("<td class=""plainlabel"">" & qdate)
            sb.Append("</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        divmq.InnerHtml = sb.ToString
    End Sub
    Private Sub GetAll()
        Dim sb As New StringBuilder
        Dim srch As String
        srch = lblsrch.Value
        Filter = "qstat = 1 "
        FilterCnt = "qstat = 1 "
        If srch <> "" Then
            Filter += "and username like ''%" & srch & "%'' or queryname like ''%" & srch & "%'' or query like ''%" & srch & "''"
            FilterCnt += "and username like '%" & srch & "%' or queryname like '%" & srch & "%' or query like '%" & srch & "'"
        End If
        sql = "select Count(*) from ns_squery where " & FilterCnt
        Dim dc As Integer = rep.PageCount(sql, PageSize) 'rep.Scalar(sql)
        If dc = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & dc
        End If
        txtpgcnt.Value = dc
        Tables = "ns_squery"
        PK = "qid"
        PageSize = "200"
        Fields = "qid, username, queryname, qdate"
        dr = rep.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        sb.Append("<table>")
        Dim qid, queryname, qdate As String
        While dr.Read
            qid = dr.Item("qid").ToString
            username = dr.Item("username").ToString
            queryname = dr.Item("queryname").ToString
            qdate = dr.Item("qdate").ToString
            sb.Append("<tr><td>")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getq('" & qid & "','" & queryname & "','all')"">")
            sb.Append(queryname)
            sb.Append("</td>")
            sb.Append("<td class=""plainlabel"">" & username)
            sb.Append("</td>")
            sb.Append("<td class=""plainlabel"">" & qdate)
            sb.Append("</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        divaq.InnerHtml = sb.ToString
    End Sub

    Private Sub GetNext()
        Dim curr As String = lblcurr.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            If curr = "tdall" Then
                GetAll()
            Else
                username = lblusername.Value
                GetUser(username)
            End If

        Catch ex As Exception
            rep.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1772" , "squery.aspx.vb")
 
            rep.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Dim curr As String = lblcurr.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            If curr = "tdall" Then
                GetAll()
            Else
                username = lblusername.Value
                GetUser(username)
            End If
        Catch ex As Exception
            rep.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1773" , "squery.aspx.vb")
 
            rep.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3713.Text = axlabs.GetASPXPage("squery.aspx", "lang3713")
        Catch ex As Exception
        End Try
        Try
            lang3714.Text = axlabs.GetASPXPage("squery.aspx", "lang3714")
        Catch ex As Exception
        End Try
        Try
            lang3715.Text = axlabs.GetASPXPage("squery.aspx", "lang3715")
        Catch ex As Exception
        End Try
        Try
            lang3716.Text = axlabs.GetASPXPage("squery.aspx", "lang3716")
        Catch ex As Exception
        End Try

    End Sub

End Class
