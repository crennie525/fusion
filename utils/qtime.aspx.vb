

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class qtime
    Inherits System.Web.UI.Page
	Protected WithEvents lang3699 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3698 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3697 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim qt As New Utilities
    Dim sql As String
    Protected WithEvents tdqt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblms As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim old As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtuid As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpass As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtqt As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            old = Request.QueryString("old").ToString
            Dim qtchk As Long
            Try
                qtchk = System.Convert.ToInt32(old)
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr1752" , "qtime.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            qtchk = qtchk / 1000
            txtqt.Text = qtchk
            lblold.Value = qtchk
        Else
            If Request.Form("lblsubmit") = "go" Then
                qt.Open()
                Login()
                qt.Dispose()
            End If
        End If
    End Sub
    Private Sub Login()
        Dim u, p, s, pbd As String
        u = txtuid.Text
        u = Replace(u, "'", Chr(180), , , vbTextCompare)
        p = txtpass.Text
        pbd = txtpass.Text
        p = qt.Encrypt(p)
        If u = "pmadmin1" Then
            Dim bdlog As String
            bdlog = qt.BDLOGIN
            If pbd = bdlog Then
                ChangeQT()
            End If
        End If
    End Sub
    Private Sub ChangeQT()
        Dim nqt As String = txtqt.Text
        Dim qtchk As Long
        Try
            qtchk = System.Convert.ToInt32(nqt)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1753" , "qtime.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim newqt As Integer
        newqt = qtchk * 1000
        sql = "update pmsysvars set sqltime = '" & newqt & "'"
        Try
            qt.Update(sql)
            lblsubmit.Value = "ok"
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1754" , "qtime.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try


    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3697.Text = axlabs.GetASPXPage("qtime.aspx", "lang3697")
        Catch ex As Exception
        End Try
        Try
            lang3698.Text = axlabs.GetASPXPage("qtime.aspx", "lang3698")
        Catch ex As Exception
        End Try
        Try
            lang3699.Text = axlabs.GetASPXPage("qtime.aspx", "lang3699")
        Catch ex As Exception
        End Try

    End Sub

End Class
