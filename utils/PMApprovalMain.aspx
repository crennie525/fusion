<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PMApprovalMain.aspx.vb" Inherits="lucy_r12.PMApprovalMain" %>
<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PMApprovalMain</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/PMApprovalMainaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  class="tbg">
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 7px; POSITION: absolute; TOP: 76px; z-index: 1;" cellSpacing="0" cellPadding="2"
				width="970">
				<tr>
				<tr>
					<td width="730" class="thdrsing label"><asp:Label id="lang3671" runat="server">PM Assurance Approval Tracking</asp:Label></td>
					<td></td>
					<td width="240" class="thdrsing label"><asp:Label id="lang3672" runat="server">Plant Site Assets</asp:Label></td>
				</tr>
				<tr>
					<td vAlign="top">
						<table cellSpacing="0" cellPadding="0">
							<tr>
								<td colSpan="7"><iframe id="ifappr" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-TOP-STYLE: none; PADDING-TOP: 0px; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none"
										frameBorder="no" width="730" scrolling="auto" height="490" runat="server"></iframe>
								</td>
							</tr>
						</table>
					</td>
					<td></td>
					<td vAlign="top">
						<table>
							<tr>
								<td><iframe id="ifarch" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-TOP-STYLE: none; PADDING-TOP: 0px; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none"
										src="" frameBorder="no" width="240" scrolling="yes" height="490" runat="server"></iframe>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lbleqid" runat="server">
			<input type="hidden" id="lbluser" runat="server" />
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
		<uc1:mmenu1 id="Mmenu11" runat="server"></uc1:mmenu1>
	</body>
</HTML>
