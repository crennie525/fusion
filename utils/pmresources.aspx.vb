

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmresources
    Inherits System.Web.UI.Page
    Protected WithEvents ovid307 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid306 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang3710 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3709 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3708 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim dslev As New DataSet
    Dim ds As New DataSet
    Protected WithEvents dgrec As System.Web.UI.WebControls.DataGrid
    Dim rec As New Utilities
    Dim recnt As Integer
    Dim Filter As String
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtsearch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddsm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibtnreturn As System.Web.UI.WebControls.ImageButton
    Dim cid, login As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()


        GetDGLangs()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()

        'Put user code to initialize the page here
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        If Not IsPostBack Then
            Try
                cid = "0"
                lblcid.Value = cid
            Catch ex As Exception
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End Try
            Try
                login = HttpContext.Current.Session("Logged_IN").ToString()
            Catch ex As Exception
                Dim redir As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
                redir = redir & "?logout=yes"
                Response.Redirect(redir)
            End Try
            rec.Open()
            PopRec()
            PopSM()
            rec.Dispose()
        Else
            If Request.Form("lblpchk").ToString = "sm" Then
                lblpchk.Value = ""
                rec.Open()
                PopRec()
                PopSM()
                rec.Dispose()
            ElseIf Request.Form("lblpchk").ToString = "rf" Then
                lblpchk.Value = ""
                txtsearch.Text = ""
                Try
                    ddsm.SelectedIndex = 0
                Catch ex As Exception

                End Try

                rec.Open()
                PopRec()
                PopSM()
                rec.Dispose()
            End If
        End If
        'ibtnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ibtnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Sub PopSM()
        cid = lblcid.Value
        sql = "select * from pmSubjectMatter where compid = '" & cid & "' order by smindex"
        dr = rec.GetRdrData(sql)
        ddsm.DataSource = dr
        ddsm.DataValueField = "smid"
        ddsm.DataTextField = "subj"
        ddsm.DataBind()
        dr.Close()
        ddsm.Items.Insert(0, "Select Subject Matter")
    End Sub
    Private Sub PopRec(Optional ByVal Filter As String = "none")
        cid = lblcid.Value
        If Filter <> "none" Then
            sql = "select count(*) from pmResources where compid = '" & cid & "' " & Filter
            recnt = rec.Scalar(sql)
            If recnt > 0 Then
                sql = "select * from pmResources where compid = '" & cid & "' " & Filter
            Else
                sql = "select * from pmResources where compid = '" & cid & "'"
                Dim strMessage As String = tmod.getmsg("cdstr1763", "pmresources.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Else
            sql = "select * from pmResources where compid = '" & cid & "'"
        End If
        ds = rec.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Try
            dgrec.DataSource = dv
            dgrec.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Public Function PopulateSM() As DataSet
        cid = lblcid.Value
        sql = "select smid, smindex, subj from pmSubjectMatter where smid = 0 or compid = '" & cid & "' order by smindex"
        dslev = rec.GetDSData(sql)
        Return dslev
    End Function
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function
    Private Sub dgrec_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrec.EditCommand
        dgrec.EditItemIndex = e.Item.ItemIndex
        rec.Open()
        PopRec()
        rec.Dispose()
    End Sub
    Private Sub dgrec_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrec.CancelCommand
        dgrec.EditItemIndex = -1
        rec.Open()
        PopRec()
        rec.Dispose()
    End Sub

    Private Sub dgrec_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrec.ItemCommand

        Dim lname, lloc, lsubj, lsmid, lsmind, lphone, lemail As String
        Dim lnamei, lloci, lsubji, lphonei, lemaili As TextBox
        Dim lsubjd As DropDownList
        cid = lblcid.Value
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewname"), TextBox)
            lname = lnamei.Text
            lname = Replace(lname, "'", Chr(180), , , vbTextCompare)
            If Len(lname) > 50 Then
                Dim strMessage As String = tmod.getmsg("cdstr1764", "pmresources.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            lloci = CType(e.Item.FindControl("txtloc1"), TextBox)
            lloc = lloci.Text
            lloc = Replace(lloc, "'", Chr(180), , , vbTextCompare)
            If Len(lloc) > 50 Then
                Dim strMessage As String = tmod.getmsg("cdstr1765", "pmresources.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            lsmid = CType(e.Item.FindControl("ddsubj1"), DropDownList).SelectedItem.Value
            lsmind = CType(e.Item.FindControl("ddsubj1"), DropDownList).SelectedIndex.ToString
            lsubj = CType(e.Item.FindControl("ddsubj1"), DropDownList).SelectedItem.Text
            lsubj = Replace(lsubj, "'", Chr(180), , , vbTextCompare)
            lsubjd = CType(e.Item.FindControl("ddsubj1"), DropDownList)
            'lsubji = CType(e.Item.FindControl("txtnewsubj"), TextBox)
            'lsubj = lsubji.Text
            lphonei = CType(e.Item.FindControl("txtnewphone"), TextBox)
            lphone = lphonei.Text
            lemaili = CType(e.Item.FindControl("txtnewemail"), TextBox)
            lemail = lemaili.Text
            lemail = Replace(lemail, "'", "''")
            If lnamei.Text <> "" Then
                If Len(lname) > 50 Then
                    Dim strMessage As String = tmod.getmsg("cdstr1766", "pmresources.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    rec.Open()
                    sql = "insert into pmResources (name, location, compid, smid, smindex, subj, phone, email) " _
                    + "values ('" & lname & "', '" & lloc & "', '" & cid & "', '" & lsmid & "', '" & lsmind & "', '" & lsubj & "', '" & lphone & "', '" & lemail & "')"
                    rec.Update(sql)
                    lnamei.Text = ""
                    'lsubji.Text = ""
                    Try
                        lsubjd.SelectedIndex = 0
                    Catch ex As Exception

                    End Try

                    lphonei.Text = ""
                    lemaili.Text = ""
                    PopRec()
                    rec.Dispose()
                End If
            Else
                Dim strMessage As String = tmod.getmsg("cdstr1767", "pmresources.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If
        If e.CommandName = "hloc" Then
            dgrec.Columns(3).Visible = False
        End If
        If e.CommandName = "sloc" Then
            dgrec.Columns(3).Visible = True
        End If

    End Sub

    Private Sub dgrec_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrec.UpdateCommand

        Dim lname, lloc, lsubj, lsmid, lsmind, lphone, lemail, rid As String
        Dim lnamei, lloci, lsubji, lphonei, lemaili As TextBox
        rid = CType(e.Item.FindControl("lblide"), Label).Text
        lnamei = CType(e.Item.FindControl("txtname"), TextBox)
        lname = lnamei.Text
        lname = Replace(lname, "'", Chr(180), , , vbTextCompare)
        If Len(lname) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr1768", "pmresources.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        lloci = CType(e.Item.FindControl("txtloc"), TextBox)
        lloc = lloci.Text
        lloc = Replace(lloc, "'", Chr(180), , , vbTextCompare)
        If Len(lloc) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr1769", "pmresources.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim lsm As DropDownList = CType(e.Item.FindControl("ddsubj"), DropDownList)
        If lsm.SelectedIndex <> 0 Then
            lsmid = CType(e.Item.FindControl("ddsubj"), DropDownList).SelectedItem.Value
            lsmind = CType(e.Item.FindControl("ddsubj"), DropDownList).SelectedIndex.ToString
            lsubj = CType(e.Item.FindControl("ddsubj"), DropDownList).SelectedItem.Text
            lsubj = Replace(lsubj, "'", Chr(180), , , vbTextCompare)
        Else
            lsmid = "0"
            lsmind = "0"
            lsubj = "Select"
        End If


        'lsubji = CType(e.Item.FindControl("txtsubj"), TextBox)
        'lsubj = lsubji.Text
        lphonei = CType(e.Item.FindControl("txtphone"), TextBox)
        lphone = lphonei.Text
        lemaili = CType(e.Item.FindControl("txtemail"), TextBox)
        lemail = lemaili.Text
        lemail = Replace(lemail, "'", "''")
        sql = "update pmResources set name = " _
        + "'" & lname & "', location = '" & lloc & "', smindex = '" & lsmind & "', smid = '" & lsmid & "', subj = '" & lsubj & "', " _
        + "phone = '" & lphone & "', email = '" & lemail & "' where rid = '" & rid & "'"
        rec.Open()
        If Len(lname) > 0 Then
            Try
                rec.Update(sql)
                dgrec.EditItemIndex = -1
                PopRec()
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr1770", "pmresources.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try
        Else
            Dim strMessage As String = tmod.getmsg("cdstr1771", "pmresources.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If


        rec.Dispose()
    End Sub

    Private Sub dgrec_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrec.DeleteCommand
        Dim rid As String
        Try
            rid = CType(e.Item.FindControl("lblid"), Label).Text
        Catch ex As Exception
            rid = CType(e.Item.FindControl("lblide"), Label).Text
        End Try
        sql = "delete from pmResources where rid = '" & rid & "'"
        rec.Open()
        rec.Update(sql)
        PopRec()
        rec.Dispose()
    End Sub

    Private Sub ibtnreturn_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnreturn.Click
        Response.Redirect("../mainmenu/NewMainMenu2.aspx")
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        If Len(txtsearch.Text) > 0 Then
            Filter = " and (name like '%" & txtsearch.Text & "%' or subj like '%" & txtsearch.Text & "%')"
            txtsearch.Text = ""
            rec.Open()
            PopRec(Filter)
            rec.Dispose()
        End If
    End Sub

    Private Sub ddsm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsm.SelectedIndexChanged
        Dim smid As String
        If ddsm.SelectedIndex <> 0 Then
            smid = ddsm.SelectedValue.ToString
            Filter = " and smid = '" & smid & "'"
            Try
                ddsm.SelectedIndex = 0
            Catch ex As Exception

            End Try

            rec.Open()
            PopRec(Filter)
            rec.Dispose()
        End If
    End Sub




    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgrec.Columns(0).HeaderText = dlabs.GetDGPage("pmresources.aspx", "dgrec", "0")
        Catch ex As Exception
        End Try
        Try
            dgrec.Columns(1).HeaderText = dlabs.GetDGPage("pmresources.aspx", "dgrec", "1")
        Catch ex As Exception
        End Try
        Try
            dgrec.Columns(2).HeaderText = dlabs.GetDGPage("pmresources.aspx", "dgrec", "2")
        Catch ex As Exception
        End Try
        Try
            dgrec.Columns(4).HeaderText = dlabs.GetDGPage("pmresources.aspx", "dgrec", "4")
        Catch ex As Exception
        End Try
        Try
            dgrec.Columns(5).HeaderText = dlabs.GetDGPage("pmresources.aspx", "dgrec", "5")
        Catch ex As Exception
        End Try
        Try
            dgrec.Columns(6).HeaderText = dlabs.GetDGPage("pmresources.aspx", "dgrec", "6")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3708.Text = axlabs.GetASPXPage("pmresources.aspx", "lang3708")
        Catch ex As Exception
        End Try
        Try
            lang3709.Text = axlabs.GetASPXPage("pmresources.aspx", "lang3709")
        Catch ex As Exception
        End Try
        Try
            lang3710.Text = axlabs.GetASPXPage("pmresources.aspx", "lang3710")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                ibtnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            ovid306.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmresources.aspx", "ovid306") & "')")
            ovid306.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid307.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmresources.aspx", "ovid307") & "')")
            ovid307.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
