<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmMeasureTypes.aspx.vb" Inherits="lucy_r12.pmMeasureTypes" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>pmMeasureTypes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/pmMeasureTypesaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" >
		<form id="form1" method="post" runat="server">
			<table>
				<TBODY>
					<tr>
						<td class="bluelabel"><asp:Label id="lang3675" runat="server">Add Type</asp:Label></td>
						<td><asp:textbox id="txtnewmeas" runat="server" MaxLength="50"></asp:textbox></td>
						<td><asp:imagebutton id="ibtnadd" runat="server" ImageUrl="../images/appbuttons/minibuttons/addnewbg1.gif"></asp:imagebutton></td>
						<td><IMG onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif" id="ibtnret"
								runat="server" width="69" height="19"></td>
					</tr>
					<tr id="trmsg" runat="server">
						<td class="redlabel" id="tdnotypes" align="center" colSpan="4" runat="server"></td>
					</tr>
					<tr>
						<td colspan="4" align="center" class="label"><asp:Label id="lang3676" runat="server">Measure Types</asp:Label></td>
					</tr>
					<tr id="trtypes" runat="server">
						<td align="center" colSpan="4">
							<div style="BORDER-RIGHT: 2px groove; BORDER-TOP: 2px groove; OVERFLOW: auto; BORDER-LEFT: 2px groove; WIDTH: 200px; BORDER-BOTTOM: 2px groove; HEIGHT: 150px; BACKGROUND-COLOR: white"><asp:repeater id="rptrtypes" runat="server">
									<HeaderTemplate>
										<table cellspacing="0">
									</HeaderTemplate>
									<ItemTemplate>
										<tr class="tbg">
											<td class="plainlabel" width="150"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"type")%>' Runat = server ID="lbltasknum">
												</asp:Label></td>
											<td width="20"><asp:ImageButton id="ibtndel1" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
													CommandName="Delete"></asp:ImageButton></td>
											<td class="details"><asp:Label id="lbltid"  Text='<%# DataBinder.Eval(Container.DataItem,"typeid")%>' Runat = server>
												</asp:Label></td>
										</tr>
									</ItemTemplate>
									<AlternatingItemTemplate>
										<tr bgcolor="#E7F1FD">
											<td class="plainlabel"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"type")%>' Runat = server ID="Label1">
												</asp:Label></td>
											<td><asp:ImageButton id="ibtndel2" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
													CommandName="Delete"></asp:ImageButton></td>
											<td class="details"><asp:Label id="lbltidalt"  Text='<%# DataBinder.Eval(Container.DataItem,"typeid")%>' Runat = server>
												</asp:Label></td>
										</tr>
									</AlternatingItemTemplate>
									<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></DIV></TD></TR></TBODY></TABLE><input id="lblcid" type="hidden" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
