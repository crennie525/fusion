<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmTaskMeasurements.aspx.vb" Inherits="lucy_r12.pmTaskMeasurements" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>pmTaskMeasurements</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/pmTaskMeasurementsaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="startlog();" >
		<form id="form1" method="post" runat="server">
			<table>
				<TBODY>
					<tr>
						<td width="80"></td>
						<td width="200"></td>
						<td width="120"></td>
						<td width="50"></td>
					</tr>
					<tr>
						<td class="thdrsingrt label" colSpan="4"><asp:Label id="lang3677" runat="server">PM Task Measurements Dialog</asp:Label></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang3678" runat="server">Equipment#</asp:Label></td>
						<td class="plainlabel" id="tdeq" runat="server"></td>
						<td></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang3679" runat="server">Function</asp:Label></td>
						<td class="plainlabel" id="tdfu" runat="server"></td>
						<td></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang3680" runat="server">Component</asp:Label></td>
						<td class="plainlabel" id="tdco" runat="server"></td>
						<td></td>
					</tr>
					<tr>
						<td colSpan="3">
							<hr color="black" SIZE="1">
						</td>
					</tr>
					<tr>
						<td vAlign="top" colSpan="2">
							<div class="bluelabel" style="Z-INDEX: 100; POSITION: absolute; BACKGROUND-COLOR: white; LEFT: 20px"><asp:Label id="lang3681" runat="server">Measure Type</asp:Label></div>
							<div style="BORDER-BOTTOM: 2px groove; POSITION: relative; BORDER-LEFT: 2px groove; HEIGHT: 110px; BORDER-TOP: 2px groove; TOP: 10px; BORDER-RIGHT: 2px groove">
								<table>
									<tr>
										<td colSpan="3">&nbsp;</td>
									</tr>
									<tr>
										<td class="label"><asp:Label id="lang3682" runat="server">Measure Type</asp:Label></td>
										<td><asp:dropdownlist id="ddtypes" runat="server" AutoPostBack="True" Width="150px"></asp:dropdownlist></td>
										<td><IMG onclick="getAddType();" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
												width="20" id="iaddtype" runat="server"></td>
									</tr>
									<tr>
										<td class="label"><asp:Label id="lang3683" runat="server">Measurement</asp:Label></td>
										<td><asp:dropdownlist id="ddmeas" runat="server" AutoPostBack="True" Width="150px"></asp:dropdownlist></td>
										<td><IMG onclick="getAddMeas();" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
												width="20" id="iaddmeas" runat="server"></td>
									</tr>
								</table>
							</div>
						</td>
						<td vAlign="top">
							<div class="bluelabel" style="Z-INDEX: 100; POSITION: absolute; BACKGROUND-COLOR: white; LEFT: 310px"><asp:Label id="lang3684" runat="server">Characteristics</asp:Label></div>
							<div style="BORDER-BOTTOM: 2px groove; POSITION: relative; BORDER-LEFT: 2px groove; HEIGHT: 110px; BORDER-TOP: 2px groove; TOP: 10px; BORDER-RIGHT: 2px groove">
								<table>
									<tr>
										<td vAlign="middle" width="120" height="90"><asp:radiobuttonlist id="rblchar" runat="server" AutoPostBack="True" CssClass="label" RepeatLayout="Flow">
												<asp:ListItem Value="spec">Specific</asp:ListItem>
												<asp:ListItem Value="range" Selected="True">Range</asp:ListItem>
												<asp:ListItem Value="max">Not to Exceed</asp:ListItem>
												<asp:ListItem Value="rec">Record Only</asp:ListItem>
											</asp:radiobuttonlist></td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td colSpan="3">&nbsp;</td>
					</tr>
					<tr id="trspec" runat="server">
						<td align="center" colSpan="3">
							<table>
								<tr>
									<td class="disablelabel" id="tdspec" runat="server"><asp:Label id="lang3685" runat="server">Hi Limit</asp:Label></td>
									<td><asp:textbox id="t1" runat="server" MaxLength="50"></asp:textbox></td>
									<td class="plainlabel" id="tdstype" runat="server"></td>
								</tr>
								<tr>
									<td class="disablelabel" id="tdspec2" runat="server"><asp:Label id="lang3686" runat="server">Lo Limit</asp:Label></td>
									<td><asp:textbox id="t2" runat="server" MaxLength="50"></asp:textbox></td>
									<td class="plainlabel" id="tdstype2" runat="server"></td>
								</tr>
								<tr>
									<td class="disablelabel" id="tdspec3" runat="server"><asp:Label id="lang3687" runat="server">Specific</asp:Label></td>
									<td><asp:textbox id="t3" runat="server" MaxLength="50"></asp:textbox></td>
									<td class="plainlabel" id="tdstype3" runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="labelibl" colSpan="3" align="center"><asp:Label id="lang3688" runat="server">Use Hi Limit for Not to Exceed option</asp:Label><br><asp:Label id="lang3689" runat="server">Use Specific as Specification for Range and Not to Exceed options</asp:Label></td>
					</tr>
					<tr>
						<td align="right" colSpan="3"><IMG id="btnadd" onclick="addToTask();" height="19" alt="" src="../images/appbuttons/bgbuttons/badd.gif"
								width="69" runat="server">&nbsp;<IMG id="btnreturn" onclick="handleexit();" height="19" alt="" src="../images/appbuttons/bgbuttons/return.gif"
								width="69" runat="server">
						</td>
					</tr>
					<tr id="troutput" runat="server">
						<td align="left" colSpan="4"><asp:repeater id="rptrmeas" runat="server">
								<HeaderTemplate>
									<table cellspacing="0">
										<tr>
											<td class="btmmenu plainlabel" width="80"><asp:Label id="lang3690" runat="server">Type</asp:Label></td>
											<td class="btmmenu plainlabel" width="90"><asp:Label id="lang3691" runat="server">Measurement</asp:Label></td>
											<td class="btmmenu plainlabel" width="60"><asp:Label id="lang3692" runat="server">Hi Limit</asp:Label></td>
											<td class="btmmenu plainlabel" width="60"><asp:Label id="lang3693" runat="server">Lo Limit</asp:Label></td>
											<td class="btmmenu plainlabel" width="60"><asp:Label id="lang3694" runat="server">Specific</asp:Label></td>
											<td class="btmmenu plainlabel" width="80"><asp:Label id="lang3695" runat="server">Record Only</asp:Label></td>
											<td align="center" width="20" class="btmmenu plainlabel"><img src="../images/appbuttons/minibuttons/del.gif" width="16" height="16"></td>
										</tr>
								</HeaderTemplate>
								<ItemTemplate>
									<tr class="tbg">
										<td class="plainlabel"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"type")%>' Runat = server ID="lbltasknum">
											</asp:Label></td>
										<td class="plainlabel"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"measure")%>' Runat = server ID="Label1">
											</asp:Label></td>
										<td class="plainlabel"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"hi")%>' Runat = server ID="Label2">
											</asp:Label></td>
										<td class="plainlabel"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"lo")%>' Runat = server ID="Label3">
											</asp:Label></td>
										<td class="plainlabel"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"spec")%>' Runat = server ID="Label4">
											</asp:Label></td>
										<td class="plainlabel"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"record")%>' Runat = server ID="Label5">
											</asp:Label></td>
										<td width="20"><asp:ImageButton id="ibtndel1" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
												CommandName="Delete"></asp:ImageButton></td>
										<td class="details"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"tmdid")%>' Runat = server ID="lbltmdid1">
											</asp:Label></td>
									</tr>
								</ItemTemplate>
								<AlternatingItemTemplate>
									<tr bgcolor="#E7F1FD">
										<td class="plainlabel"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"type")%>' Runat = server ID="Label6">
											</asp:Label></td>
										<td class="plainlabel"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"measure")%>' Runat = server ID="Label7">
											</asp:Label></td>
										<td class="plainlabel"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"hi")%>' Runat = server ID="Label8">
											</asp:Label></td>
										<td class="plainlabel"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"lo")%>' Runat = server ID="Label9">
											</asp:Label></td>
										<td class="plainlabel"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"spec")%>' Runat = server ID="Label10">
											</asp:Label></td>
										<td class="plainlabel"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"record")%>' Runat = server ID="Label11">
											</asp:Label></td>
										<td width="20"><asp:ImageButton id="ibtndel2" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
												CommandName="Delete"></asp:ImageButton></td>
										<td class="details"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"tmdid")%>' Runat = server ID="lbltmdid2">
											</asp:Label></td>
									</tr>
								</AlternatingItemTemplate>
								<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></TD></TR>
			<tr>
				<td class="redlabel" id="tdnomeas" align="center" colSpan="3" runat="server"></td>
			</tr>
			<tr>
				<td class="bluelabel"><asp:Label id="lang3696" runat="server">Output:</asp:Label></td>
				<td class="plainlabel" id="tdout" colSpan="2" runat="server"></td>
			</tr>
			</TBODY></TABLE><input id="lbltaskid" type="hidden" runat="server"> <input id="lblcid" type="hidden" runat="server">
			<input id="lblrettype" type="hidden" runat="server"><input id="lbltypeid" type="hidden" runat="server">
			<input id="lblmeasid" type="hidden" runat="server"><input id="lblchr" type="hidden" runat="server">
			<input id="lblout" type="hidden" runat="server"><input id="lbllog" type="hidden" name="lbllog" runat="server">
			<input id="lbltyp" type="hidden" runat="server"> <input type="hidden" id="lblwonum" runat="server">
			<input type="hidden" id="lbljpid" runat="server"> <input type="hidden" id="lblro" runat="server">
			<input type="hidden" id="lblrev" runat="server"> <input type="hidden" id="lblcoid" runat="server">
		<input type="hidden" id="lblwojtid" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
