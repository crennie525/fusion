<%@ Page Language="vb" AutoEventWireup="false" Codebehind="squery.aspx.vb" Inherits="lucy_r12.squery" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>squery</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/reportnav.js"></script>
		<script language="JavaScript" src="../scripts1/squeryaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checktab();">
		<form id="form1" method="post" runat="server">
			<table cellspacing="0">
				<tr>
					<td class="thdrsing label"><asp:Label id="lang3713" runat="server">Saved Queries</asp:Label></td>
				</tr>
				<tr>
					<td>
						<table>
							<tr>
								<td class="label"><asp:Label id="lang3714" runat="server">Search</asp:Label></td>
								<td>
									<asp:TextBox id="txtsrch" runat="server" Width="200px"></asp:TextBox></td>
								<td><img src="../images/appbuttons/minibuttons/srchsm.gif" onclick="srchq();"></td>
								<td><img src="../images/appbuttons/minibuttons/refreshit.gif" onclick="resetq();"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table>
							<tr height="22">
								<td class="thdrhov label" id="tdme" onclick="gettab('tdme');" width="90"><asp:Label id="lang3715" runat="server">My Queries</asp:Label></td>
								<td class="thdr label" id="tdall" runat="server" onclick="gettab('tdall');" width="90"><asp:Label id="lang3716" runat="server">All Queries</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="trmq" runat="server">
					<td>
						<div id="divmq" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; OVERFLOW: auto; BORDER-LEFT: black 1px solid; WIDTH: 320px; BORDER-BOTTOM: black 1px solid; HEIGHT: 260px"
							runat="server"></div>
					</td>
				</tr>
				<tr id="traq" runat="server" class="details">
					<td>
						<div id="divaq" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; OVERFLOW: auto; BORDER-LEFT: black 1px solid; WIDTH: 320px; BORDER-BOTTOM: black 1px solid; HEIGHT: 260px"
							runat="server"></div>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="190"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lblsubmit" type="hidden" runat="server" NAME="lblsubmit"> <input id="txtpgcnt" type="hidden" runat="server" NAME="txtpgcnt"><input id="txtpg" type="hidden" runat="server" NAME="txtpg">
			<input type="hidden" id="lblusername" runat="server"> <input type="hidden" id="lblcurr" runat="server">
			<input type="hidden" id="lblsrch" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
