﻿Imports System.Data.SqlClient
Public Class eqverif2
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim eqv As New Utilities
    Dim mu As New mmenu_utils_a
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim eqnum, sid, wonum As String
    Dim eqid, eqret, eqdesc, fpc, location, parid, dept, did, cell, clid, who, wpage, usr As String
    Dim errflag As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqnum = Request.QueryString("eqnum").ToString
            Dim earr() As String = eqnum.Split("~~")
            Dim ear As String = earr(0)
            Dim etest As String = ear
            sid = Request.QueryString("sid").ToString
            sql = "usp_eqauto '" & ear & "', '" & sid & "'"
            eqv.Open()
            dr = eqv.GetRdrData(sql)
            While dr.Read
                who = dr.Item("who").ToString
                If who = "D" Or who = "L" Then
                    eqid = dr.Item("eqid").ToString
                    eqret = dr.Item("eqnum").ToString
                    eqdesc = dr.Item("eqdesc").ToString
                    fpc = dr.Item("fpc").ToString
                    If who = "D" Then
                        did = dr.Item("dept_id").ToString
                        dept = dr.Item("dept_line").ToString
                        cell = dr.Item("cell_name").ToString
                        clid = dr.Item("cellid").ToString
                    ElseIf who = "L" Then
                        location = dr.Item("location").ToString
                        parid = dr.Item("parid").ToString
                    End If
                End If

            End While
            dr.Close()
            lblwho.Value = who
            If who = "L" Or who = "D" Then
                lbleqid.Value = eqid
                lbleqnum.Value = eqret
                lbleqdesc.Value = eqdesc
                lblfpc.Value = fpc
                lbllocation.Value = location
                lblparid.Value = parid
                lbldid.Value = did
                lbldept.Value = dept
                lblcell.Value = cell
                lblclid.Value = clid
                'savewo()
            Else
                lbleqnum.Value = eqnum
            End If
        End If
        eqv.Dispose()
    End Sub

End Class