﻿Imports System.Data.SqlClient
Public Class pmo123tabeqmain
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim main As New Utilities
    Dim dr As SqlDataReader
    Dim sid, who, did, clid, typ, lid, ustr As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim eoem As New mmenu_utils_a
        Dim coi As String = eoem.COMPI
        lblcoi.Value = coi
        If coi <> "INN" And coi <> "LAU" Then
            tdRPN.Attributes.Add("class", "details")
        End If
        If Not IsPostBack Then
            If coi <> "INN" Then
                tdRPN.Attributes.Add("class", "details")
            Else
                tdRPN.Attributes.Add("class", "thdr plainlabel")
            End If
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            AutoCompleteExtender1.CompletionSetCount = sid
            txteqauto.Attributes.Add("onkeydown", "checkent();")

            who = Request.QueryString("who").ToString
            lblwho.Value = who
            ustr = Request.QueryString("ustr").ToString
            lbluser.Value = ustr
            If who = "eqready" Then
                tdarch.Attributes.Add("class", "details")
                'lbldchk.Value = Request.QueryString("dchk").ToString
                lbldid.Value = Request.QueryString("did").ToString
                did = Request.QueryString("did").ToString
                lbldept.Value = Request.QueryString("dept").ToString
                lblclid.Value = Request.QueryString("clid").ToString
                clid = Request.QueryString("clid").ToString
                'lblchk.Value = Request.QueryString("chk").ToString
                lblcell.Value = Request.QueryString("cell").ToString
                lbleqid.Value = Request.QueryString("eqid").ToString 'elim
                lbleq.Value = Request.QueryString("eq").ToString 'elim
                'lblfuid.Value = Request.QueryString("fuid").ToString 'elim
                'lblcoid.Value = Request.QueryString("coid").ToString 'elim
                'lblncid.Value = Request.QueryString("ncid").ToString 'elim
                lbllid.Value = Request.QueryString("lid").ToString
                lblloc.Value = Request.QueryString("loc").ToString
                lblrettyp.Value = Request.QueryString("rettyp").ToString
                lblgototasks.Value = Request.QueryString("gototasks").ToString
                lbllvl.Value = "eq"
                If did <> "" Then
                    lbldchk.Value = "yes"
                End If
                If clid <> "" Then
                    lblchk.Value = "yes"
                Else
                    lblchk.Value = "no"
                End If
                'trpick.Attributes.Add("class", "details")
                'trreturn.Attributes.Add("class", "view")
            ElseIf who = "eqmain" Then
                'lbldchk.Value = Request.QueryString("dchk").ToString
                lbldid.Value = Request.QueryString("did").ToString
                did = Request.QueryString("did").ToString
                lbldept.Value = Request.QueryString("dept").ToString
                lblclid.Value = Request.QueryString("clid").ToString
                clid = Request.QueryString("clid").ToString
                'lblchk.Value = Request.QueryString("chk").ToString
                lblcell.Value = Request.QueryString("cell").ToString
                lbleqid.Value = Request.QueryString("eqid").ToString 'elim
                lbleq.Value = Request.QueryString("eq").ToString 'elim
                'lblfuid.Value = Request.QueryString("fuid").ToString 'elim
                'lblcoid.Value = Request.QueryString("coid").ToString 'elim
                'lblncid.Value = Request.QueryString("ncid").ToString 'elim
                lbllid.Value = Request.QueryString("lid").ToString
                lblloc.Value = Request.QueryString("loc").ToString
                lblrettyp.Value = Request.QueryString("rettyp").ToString
                lblgototasks.Value = Request.QueryString("gototasks").ToString
                lbllvl.Value = "eq"
                If did <> "" Then
                    lbldchk.Value = "yes"
                End If
                If clid <> "" Then
                    lblchk.Value = "yes"
                Else
                    lblchk.Value = "no"
                End If
                'trpick.Attributes.Add("class", "details")
                'trreturn.Attributes.Add("class", "view")
            ElseIf who = "locready" Then
                'lbldchk.Value = Request.QueryString("dchk").ToString
                lbldid.Value = Request.QueryString("did").ToString
                did = Request.QueryString("did").ToString
                lbldept.Value = Request.QueryString("dept").ToString
                lblclid.Value = Request.QueryString("clid").ToString
                clid = Request.QueryString("clid").ToString
                'lblchk.Value = Request.QueryString("chk").ToString
                lblcell.Value = Request.QueryString("cell").ToString
                'lbleqid.Value = Request.QueryString("eqid").ToString 'elim
                'lblfuid.Value = Request.QueryString("fuid").ToString 'elim
                'lblcoid.Value = Request.QueryString("coid").ToString 'elim
                'lblncid.Value = Request.QueryString("ncid").ToString 'elim
                lbllid.Value = Request.QueryString("lid").ToString
                lblloc.Value = Request.QueryString("loc").ToString
                lblrettyp.Value = Request.QueryString("rettyp").ToString
                lblgototasks.Value = Request.QueryString("gototasks").ToString
                If did <> "" Then
                    lbldchk.Value = "yes"
                End If
                If clid <> "" Then
                    lblchk.Value = "yes"
                Else
                    lblchk.Value = "no"
                End If
                'trpick.Attributes.Add("class", "details")
                'trreturn.Attributes.Add("class", "view")
            ElseIf who = "tab" Then
                'trpick.Attributes.Add("class", "view")
                'trreturn.Attributes.Add("class", "details")
                'tdreturn.Attributes.Add("class", "details")
            ElseIf who = "tabret" Then
                'trpick.Attributes.Add("class", "view")
                'trreturn.Attributes.Add("class", "details")
                'tdreturn.Attributes.Add("class", "details")

                lbldid.Value = Request.QueryString("did").ToString
                did = Request.QueryString("did").ToString
                lbldept.Value = Request.QueryString("dept").ToString
                lblclid.Value = Request.QueryString("clid").ToString
                clid = Request.QueryString("clid").ToString
                lblcell.Value = Request.QueryString("cell").ToString
                lbllid.Value = Request.QueryString("lid").ToString
                lid = Request.QueryString("lid").ToString
                lblloc.Value = Request.QueryString("loc").ToString
                Dim eqid, fuid, coid As String
                lbleqid.Value = Request.QueryString("eqid").ToString
                eqid = Request.QueryString("eqid").ToString
                lblfuid.Value = Request.QueryString("fuid").ToString
                fuid = Request.QueryString("fuid").ToString
                lblcoid.Value = Request.QueryString("coid").ToString
                coid = Request.QueryString("coid").ToString
                'lblrettyp.Value = Request.QueryString("rettyp").ToString
                lblgototasks.Value = "1"
                If did <> "" Then
                    lbldchk.Value = "yes"
                    lbltyp.Value = "reg"
                Else
                    lbldchk.Value = "no"
                End If
                If clid <> "" And lid = "" Then
                    lblchk.Value = "yes"
                Else
                    lblchk.Value = "no"
                    If lid <> "" Then
                        lbltyp.Value = "loc"
                    End If

                End If
                If coid <> "" Then
                    lbllvl.Value = "co"
                ElseIf fuid <> "" Then
                    lbllvl.Value = "fu"
                ElseIf eqid <> "" Then
                    lbllvl.Value = "eq"
                End If
                If lid <> "" Then
                    typ = "loc"
                End If
                main.Open()
                If typ <> "loc" Then
                    GetStuff(did, clid)
                Else
                    GetLocStuff(lid)
                End If
                main.Dispose()
            Else
                tdarch.Attributes.Add("class", "details")
                'trpick.Attributes.Add("class", "view")
                'trreturn.Attributes.Add("class", "details")
            End If


        End If

    End Sub
    Private Sub GetLocStuff(ByVal lid As String)
        sql = "select location from pmlocations where locid = '" & lid & "'"
        Dim loc As String
        dr = main.GetRdrData(sql)
        While dr.Read
            loc = dr.Item("location").ToString
        End While
        dr.Close()
        trlocs.Attributes.Add("class", "view")
        tdloc2.InnerHtml = loc
    End Sub
    Private Sub GetStuff(ByVal did As String, ByVal clid As String)
        If clid = "" Or clid = "0" Then
            sql = "select d.dept_line, 'na' as 'cell_name' from dept d " _
             + "where d.dept_id = '" & did & "'"
        Else
            sql = "select d.dept_line, c.cell_name from dept d left join cells c on c.dept_id = d.dept_id " _
           + "where d.dept_id = '" & did & "' and c.cellid = '" & clid & "'"
        End If
        dr = main.GetRdrData(sql)
        Dim dept, cell As String
        While dr.Read
            dept = dr.Item("dept_line").ToString
            cell = dr.Item("cell_name").ToString
        End While
        dr.Close()
        If dept <> "" Then
            trdepts.Attributes.Add("class", "view")
            tddept.InnerHtml = dept
            If cell <> "na" Then
                tdcell.InnerHtml = cell
            End If
        End If

    End Sub
End Class