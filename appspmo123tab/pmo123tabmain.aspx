﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123tabmain.aspx.vb"
    Inherits="lucy_r12.pmo123tabmain" %>

<%@ Register Src="../menu/mmenu1.ascx" TagName="mmenu1" TagPrefix="uc1" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../scripts/showModalDialog.js"></script>
    <script  type="text/javascript">
        function setref() {

        }
        function doref() {

        }
        function handlearch(did, clid, lid, sid, typ, eqid, fuid, coid, eqnum, chld) {
            //alert(eqid + " from arch")
            document.getElementById("lblchld").value = chld;
            document.getElementById("lbldid").value = did;
            document.getElementById("lblclid").value = clid;
            document.getElementById("lbllid").value = lid;
            document.getElementById("lblsid").value = sid;
            document.getElementById("lbltyp").value = typ;
            document.getElementById("lbleqid").value = eqid;
            document.getElementById("lblfuid").value = fuid;
            document.getElementById("lblcoid").value = coid;
            document.getElementById("lbleqnum").value = eqnum;

            document.getElementById("lbllocret").value = "no";
        }
        function handlearchret(did, clid, lid, sid, typ, eqid, fuid, coid, eqnum, dept, cell, loc) {
            //alert(eqid + " from tab arch")
            document.getElementById("lbldid").value = did;
            document.getElementById("lblclid").value = clid;
            document.getElementById("lbllid").value = lid;
            document.getElementById("lblsid").value = sid;
            document.getElementById("lbltyp").value = typ;
            document.getElementById("lbleqid").value = eqid;
            document.getElementById("lblfuid").value = fuid;
            document.getElementById("lblcoid").value = coid;
            document.getElementById("lbleqnum").value = eqnum;

            document.getElementById("lbldept").value = dept;
            document.getElementById("lblcell").value = cell;
            document.getElementById("lblloc").value = loc;

            document.getElementById("lbllocret").value = "yes";
        }
        function checktab() {
            var chk = document.getElementById("lbltabcheck").value;
            if (chk == "yes") {
                document.getElementById("lbltabcheck").value = "";
                gettab("pmo");
            }
        }
        function gettab(who) {
            var chld = document.getElementById("lblchld").value;
            //alert(chld)
            var did = document.getElementById("lbldid").value;
            var clid = document.getElementById("lblclid").value;
            var lid = document.getElementById("lbllid").value;
            var sid = document.getElementById("lblsid").value;
            var typ = document.getElementById("lbltyp").value;
            var eqid = document.getElementById("lbleqid").value;
            var fuid = document.getElementById("lblfuid").value;
            var coid = document.getElementById("lblcoid").value;
            var eqnum = document.getElementById("lbleqnum").value;

            var dept = document.getElementById("lbldept").value;
            var cell = document.getElementById("lblcell").value;
            var loc = document.getElementById("lblloc").value;
            var ustr = document.getElementById("lbluser").value;
            //alert(fuid)
            //alert(cell)
            var ret = document.getElementById("lbllocret").value;
            var ret2 = document.getElementById("lbltab").value;
            
            //alert(ret)
            if (who == "eq") {
                //alert(ret)
                document.getElementById("lbltab").value = "";
                closeall();
                if (ret == "yes" || ret2 == "eqtab") {  
                //alert("pmo123tabeqmain.aspx?sid=" + sid + "&who=tabret&did=" + did + "&dept=" + dept + "&clid=" + clid + " &cell=" + cell + "&eqid=" + eqid + "&eq=" + eqnum + "&fuid=" + fuid + "&coid=" + coid + "&ncid=&lid=" + lid + "&loc=" + loc + "&ustr=" + ustr)                
                    document.getElementById("ifeq").src = "pmo123tabeqmain.aspx?sid=" + sid + "&who=tabret&did=" + did + "&dept=" + dept + "&clid=" + clid + " &cell=" + cell + "&eqid=" + eqid + "&eq=" + eqnum + "&fuid=" + fuid + "&coid=" + coid + "&ncid=&lid=" + lid + "&loc=" + loc + "&ustr=" + ustr;
                }
                document.getElementById("eqtab").className = "thdrhov plainlabel";
                document.getElementById("treq").className = "view";


            }
            else if (who == "ecr") {
                if (chld == "" || chld == "0") {
                    closeall();
                    if (eqid != "") {
                        document.getElementById("lbltab").value = "eqtab";
                        document.getElementById("ecrtab").className = "thdrhov plainlabel";
                        document.getElementById("ifecr").src = "../appspmo123/pmo123ecr.aspx?eqid=" + eqid;
                    }
                    else {
                        document.getElementById("lbltab").value = "";
                        document.getElementById("ifecr").src = "../sbhold.htm";
                    }
                    document.getElementById("trecr").className = "view";
                }
                else {
                    document.getElementById("lbltab").value = "";
                    alert("Child Record is Locked")
                }
            }
            else if (who == "doc") {
                if (chld == "" || chld == "0") {
                    var ustr = document.getElementById("lbluser").value;
                    closeall();
                    if (eqid != "") {
                        document.getElementById("doctab").className = "thdrhov plainlabel";
                        document.getElementById("ifdoc").src = "../appsopt/PM3OptMainnohdr.aspx?who=drag&jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&chk=&pmid=&pmstr=&app=opt&typ=" + typ + "&lid=" + lid + "&ustr=" + ustr;
                    }
                    else {
                        document.getElementById("ifdoc").src = "../sbhold.htm";
                    }
                    document.getElementById("trdoc").className = "view";
                    document.getElementById("lbltab").value = "";
                }
                else {
                    alert("Child Record is Locked")
                }
            }
            else if (who == "rep") {
                //window.location.href = "../pmoreports/pmoreports.aspx?eqid=" + eqid;
                if (eqid != "") {
                    getValueAnalysis();
                }
            }
            else if (who == "pmo") {
                var ustr = document.getElementById("lbluser").value;
                if (chld == "" || chld == "0") {
                    closeall();
                    if (eqid != "") {
                        document.getElementById("lbltab").value = "pmotab";
                        document.getElementById("pmotab").className = "thdrhov plainlabel";
                        document.getElementById("ifpmo").src = "../appsopt/PM3OptMainnohdr.aspx?who=tab&jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&chk=&pmid=&pmstr=&app=opt&typ=" + typ + "&lid=" + lid + "&ustr=" + ustr + "&eqnum=" + eqnum + "&dept=" + dept + "&cell=" + cell + "&loc=" + loc;
                    }
                    else {
                        document.getElementById("lbltab").value = "";
                        document.getElementById("ifpmo").src = "../sbhold.htm";
                    }
                    document.getElementById("trpmo").className = "view";
                }
                else {
                    alert("Child Record is Locked");
                }
            }
            else if (who == "tpm") {
                if (chld == "" || chld == "0") {
                    closeall();
                    if (eqid != "") {
                        document.getElementById("tpmtab").className = "thdrhov plainlabel";
                        document.getElementById("iftpm").src = "../appsopttpm/tpmopttasksmainnohdr.aspx?who=opt&jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&chk=&pmid=&pmstr=&app=opt&typ=" + typ + "&lid=" + lid + "&eqnum=" + eqnum + "&dept=" + dept + "&cell=" + cell + "&loc=" + loc;
                    }
                    else {
                        document.getElementById("iftpm").src = "../sbhold.htm";
                    }
                    document.getElementById("trtpm").className = "view";
                    document.getElementById("lbltab").value = "";
                }
                else {
                    alert("Child Record is Locked")
                }

            }
            //"../appsopttpm/tpmopttasksmain.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" + lid + "&typ=" + typ + "&chk=" + chk
            //"../appsopt/PM3OptMain.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & funid & "&comid=" & comid & "&chk=" & cell & "&pmid=" & pmid & "&pmstr=" & pmstr & "&app=opt&typ=" & typ & "&lid=" & lid
        }
        function closeall() {
            document.getElementById("treq").className = "details";
            document.getElementById("trecr").className = "details";
            document.getElementById("trdoc").className = "details";
            document.getElementById("trpmo").className = "details";
            document.getElementById("trtpm").className = "details";
            //document.getElementById("trrep").className = "details";

            document.getElementById("eqtab").className = "thdr plainlabel";
            document.getElementById("ecrtab").className = "thdr plainlabel";
            document.getElementById("doctab").className = "thdr plainlabel";
            document.getElementById("pmotab").className = "thdr plainlabel";
            document.getElementById("tpmtab").className = "thdr plainlabel";

        }
        function getValueAnalysis() {
            sid = document.getElementById("lblsid").value;
            did = document.getElementById("lbldid").value;
            clid = document.getElementById("lblclid").value;
            eqid = document.getElementById("lbleqid").value;
            chk = ""; // document.getElementById("lblchk").value;
            fuid = document.getElementById("lblfuid").value;
            if (eqid != "") {
                window.parent.setref();
                var ht = "2000"; //screen.Height - 20;
                var wd = "1000"; //screen.Width - 20;

                var eReturn = window.showModalDialog("../reports/reportdialog.aspx?who=pmo&tl=5&chk=" + chk + "&cid=0&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:1000px; dialogWidth:2000px;resizable=yes");
                //var eReturn = window.showModalDialog("../reports/reportdialog.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:610px; dialogWidth:610px");
                if (eReturn) {
                    if (eReturn != "ret") {
                        var chk = document.getElementById("lbltab").value;
                        //alert(chk)
                        if (chk == "pmotab") {
                            gettab("pmo");
                        }
                        //document.getElementById("lblsvchk").value = "7";
                        //document.getElementById("pgflag").value = eReturn;
                        //alert(eReturn)
                        //document.getElementById("form1").submit();
                    }
                }
            }
            else {
                alert("No Equipment Record Selected Yet!")
            }
        }
        function handlelocation(href) {
            //alert(href)
            window.location = href;
        }
        function pageScroll() {

            scrolldelay = setTimeout('pageScrolldo()', 200);
        }
        function pageScrolldo() {
            //alert()
            window.scrollTo(0, top);
            //
        } 
    </script>
</head>
<body onload="checktab();">
    <form id="form1" runat="server">
    <div style="position: absolute; top: 68px; left: 4px; z-index: 2">
        <table width="1400">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="thdrhov plainlabel" id="eqtab" onclick="gettab('eq');" style="width: 110px" height="18">
                                <asp:Label ID="lang3" runat="server">Load Equipment</asp:Label>
                            </td>
                            <td class="thdr plainlabel" id="ecrtab" onclick="gettab('ecr');" style="width: 110px">
                                Determine Criticality
                            </td>
                            <td class="thdr plainlabel" id="doctab" onclick="gettab('doc');" style="width: 110px">
                                Load Existing PM
                            </td>
                            <td class="thdr plainlabel" id="pmotab" onclick="gettab('pmo');" style="width: 110px">
                                Optimization
                            </td>
                            <td class="thdr plainlabel" id="tpmtab" onclick="gettab('tpm');" style="width: 110px">
                                TPM Activities
                            </td>
                            <td class="thdr plainlabel" id="reptab" onclick="gettab('rep');" width="160">
                                Produce and Share Results
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="treq">
                <td>
                    <iframe id="ifeq" style="background-color: transparent; width: 1400px; height: 570px"
                        src="../sbhold.htm" frameborder="0" runat="server" scrolling="no" allowtransparenc>
                    </iframe>
                </td>
            </tr>
            <tr id="trecr" class="details">
                <td>
                    <iframe id="ifecr" style="background-color: transparent; width: 1060px; height: 620px"
                        src="../sbhold.htm" frameborder="0" runat="server" ></iframe>
                </td>
            </tr>
            <tr id="trdoc" class="details">
                <td>
                    <iframe id="ifdoc" style="background-color: transparent; width: 1400px; height: 620px"
                        src="../sbhold.htm" frameborder="0" runat="server" ></iframe>
                </td>
            </tr>
            <tr id="trpmo" class="details">
                <td>
                    <iframe id="ifpmo" style="background-color: transparent; width: 1400px; height: 620px"
                        src="../sbhold.htm" frameborder="0" runat="server" ></iframe>
                </td>
            </tr>
            <tr id="trtpm" class="details">
                <td>
                    <iframe id="iftpm" style="background-color: transparent; width: 1400px; height: 620px"
                        src="../sbhold.htm" frameborder="0" runat="server" ></iframe>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lblclid" runat="server" />
    <input type="hidden" id="lbllid" runat="server" />
    <input type="hidden" id="lbltyp" runat="server" />
    <input type="hidden" id="lblfuid" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lblcoid" runat="server" />
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblchk" runat="server" />
    <input type="hidden" id="lbluser" runat="server" />
    <input type="hidden" id="lbldept" runat="server" />
    <input type="hidden" id="lblcell" runat="server" />
    <input type="hidden" id="lblloc" runat="server" />
    <input type="hidden" id="lbllocret" runat="server" />
    <input id="lbldchk" type="hidden" name="lbldchk" runat="server" />
    <input type="hidden" id="lbltabcheck" runat="server" />
    <input type="hidden" id="lblchld" runat="server" />
    <input type="hidden" id="lbltab" runat="server" />
    <uc1:mmenu1 ID="mmenu11" runat="server" />
    </form>
</body>
</html>
