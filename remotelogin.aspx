﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="remotelogin.aspx.vb" Inherits="lucy_r12.remotelogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
        function lpload() {
            var ret = document.getElementById("lblret").value;
            var retstring = document.getElementById("lblretstring").value;
            if (ret == "") {
                if (document.all) {
                    showFocus(); checkqs(); CheckBad();
                }
                else {
                    showFocus(); checkqs(); CheckBad();
                }
                GetClientsUTCOffset();
            }
            else {
                if (ret == "nosite") {
                    alert("You don't have access to this Plant Site in Fusion")
                    window.parent.handleret("no");
                }
                else if (ret == "noapp") {
                    alert("You don't have access to the Work Order Manager - Full Screen Module in Fusion")
                    window.parent.handleret("no");
                }
                else if (ret == "nosec") {
                    alert("You have never logged into Fusion before\nPlease Log Into Fusion at the Main Site\nReview the User License Agreement\nand make sure you have access to this site\nand that you have access to the\nWork Order Manager - Full Screen Module")
                    window.parent.handleret("no");
                }
                else {
                    window.parent.handleret(retstring);
                    //retstring = "appswo/" + retstring;
                    //alert(retstring)
                    //window.open(retstring)
                }
            }
        }
        function checkqs() {
            //disabled
        }
        function CheckBad() {

        }
        var chk;
        function showFocus() {
            var up = document.getElementById("lblupdate").value;
            if (up == "yes") {
                document.getElementById("tbllogin").className = "details";
                document.getElementById("tblupdate").className = "view";
            }
            else {
                document.getElementById("tbllogin").className = "view";
                document.getElementById("tblupdate").className = "details";
                document.getElementById("txtuid").focus();
            }
        }
        function GetClientsUTCOffset() {
            var tzo = (new Date().getTimezoneOffset() / 60) * (-1);
            document.getElementById("lblutcoffset").value = tzo;
        }
    </script>
    <style type="text/css">
        .redlabel
        {
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            color: red;
            font-weight: bold;
        }
        .details
        {
            display: none;
            visibility: hidden;
        }
        .view
        {
            display: block;
            visibility: visible;
        }
        .bluelabel
        {
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: Blue;
            font-weight: bold;
        }
    </style>
</head>
<body onload="lpload();">
    <form id="form1" runat="server">
    <table cellspacing="0" cellpadding="0" width="354" id="tbllogin" border="0">
        <tr>
            <td width="22">
            </td>
            <td width="100">
            </td>
            <td width="120">
            </td>
            <td width="90">
            </td>
            <td width="22">
            </td>
        </tr>
         <tr><td colspan="5">&nbsp;</td></tr>
        <tr>
            <td colspan="5" align="center">
                <img src="menu/mimages/fusionsm.gif" alt="" />
            </td>
        </tr>
        <tr><td colspan="5">&nbsp;</td></tr>
        <tr>
            <td align="center" class="bluelabel" colspan="5">
                <asp:Label ID="lang3471" runat="server">Please Log In...</asp:Label>
            </td>
        </tr>
        <tr><td colspan="5">&nbsp;</td></tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Label ID="lblname" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">User Name</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtuid" runat="server" Font-Size="X-Small" Font-Names="Arial" Width="120px"></asp:TextBox>
            </td>
            <td colspan="2">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Label ID="lblpass" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Password</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtpass" runat="server" Font-Size="X-Small" Font-Names="Arial" Width="120px"
                    TextMode="Password"></asp:TextBox>
            </td>
            <td align="center">
                <asp:ImageButton ID="btnlogin" runat="server" ImageUrl="images/appbuttons/bgbuttons/blogin.gif"></asp:ImageButton>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <table id="tblupdate" class="details" cellspacing="0" cellpadding="0" width="354">
        <tr>
            <td class="redlabel" align="center">
                <asp:Label ID="lang3472" runat="server">The Fusion Site is being updated and will be back on-line shortly.</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="redlabel" align="center">
                <asp:Label ID="lang3473" runat="server">We apologize for the inconvenience.</asp:Label>
            </td>
        </tr>
    </table>
    <input id="pmapp" type="hidden" runat="server" /><input id="logbad" type="hidden"
        runat="server" />
    <input type="hidden" id="lblupdate" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblutcoffset" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />

    <input type="hidden" id="lblretstring" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblwonum" runat="server" />
    <input type="hidden" id="lbldnum" runat="server" />
    </form>
</body>
</html>
