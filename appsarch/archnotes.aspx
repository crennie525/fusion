<%@ Page Language="vb" AutoEventWireup="false" Codebehind="archnotes.aspx.vb" Inherits="lucy_r12.archnotes" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>archnotes</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<script language="JavaScript" src="../scripts1/archnotesaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td><asp:label id="Label1" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True">Task#</asp:label></td>
					<td><asp:label id="lbltsk" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True"></asp:label></td>
					<td><asp:label id="Label2" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True">Sub-Task#</asp:label></td>
					<td><asp:label id="lblsub" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True"></asp:label></td>
					<td><asp:imagebutton id="ImageButton1" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td colSpan="5"><asp:textbox id="txtnote" runat="server" CssClass="plainlabel" TextMode="MultiLine" Height="168px"
							Width="384px"></asp:textbox></td>
				</tr>
			</table>
			<input id="lblptid" type="hidden" runat="server" NAME="lblptid"><input type="hidden" id="lbllog" runat="server" NAME="lbllog">
			<input type="hidden" id="lblrev" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
