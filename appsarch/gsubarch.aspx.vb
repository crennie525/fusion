

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class gsubarch
    Inherits System.Web.UI.Page
	Protected WithEvents lang336 As System.Web.UI.WebControls.Label

	Protected WithEvents lang335 As System.Web.UI.WebControls.Label

	Protected WithEvents lang334 As System.Web.UI.WebControls.Label

	Protected WithEvents lang333 As System.Web.UI.WebControls.Label

	Protected WithEvents lang332 As System.Web.UI.WebControls.Label

	Protected WithEvents lang331 As System.Web.UI.WebControls.Label

	Protected WithEvents lang330 As System.Web.UI.WebControls.Label

	Protected WithEvents lang329 As System.Web.UI.WebControls.Label

	Protected WithEvents lang328 As System.Web.UI.WebControls.Label

	Protected WithEvents lang327 As System.Web.UI.WebControls.Label

	Protected WithEvents lang326 As System.Web.UI.WebControls.Label

	Protected WithEvents lang325 As System.Web.UI.WebControls.Label

	Protected WithEvents lang324 As System.Web.UI.WebControls.Label

	Protected WithEvents lang323 As System.Web.UI.WebControls.Label

	Protected WithEvents lang322 As System.Web.UI.WebControls.Label

	Protected WithEvents lang321 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sid, cid, fuid, tid, sql, username, eqid, ro As String
    Dim Filter, SubVal As String
    Dim ds As DataSet
    Dim dslev As DataSet
    Dim dr As SqlDataReader
    Dim gtasks As New Utilities
    Dim Login, rev As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dlhd As System.Web.UI.WebControls.DataList
    Protected WithEvents addtask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dgtasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents tdtnum As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tddesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdskill As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdqty As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmin As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdrd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdrdt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltool As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllube As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnote As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
            username = HttpContext.Current.Session("username").ToString()
            lblusername.Value = username
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                lblro.Value = ro
                rev = Request.QueryString("rev").ToString
                lblrev.Value = rev
                If ro = "1" Then
                    'dgtasks.Columns(0).Visible = False
                    'dgtasks.Columns(10).Visible = False
                    addtask.ImageUrl = "../images/appbuttons/bgbuttons/addtaskdis.gif"
                    addtask.Enabled = False
                End If

                tid = Request.QueryString("tid").ToString
                lbltid.Value = tid
                If Len(tid) <> 0 AndAlso tid <> "" AndAlso tid <> "0" Then
                    cid = Request.QueryString("cid").ToString
                    lblcid.Value = cid
                    fuid = Request.QueryString("fuid").ToString
                    lblfuid.Value = fuid
                    sid = Request.QueryString("sid").ToString
                    lblsid.Value = sid
                    eqid = Request.QueryString("eqid").ToString
                    lbleqid.Value = eqid
                    Filter = "funcid = '" & fuid & "' and tasknum = '" & tid & "'"
                    lblfilt.Value = Filter
                    SubVal = "(compid, funcid, tasknum, subtask) values ('" & cid & "', '" & fuid & "', "
                    lblsubval.Value = SubVal
                    gtasks.Open()
                    BindTaskHead()
                    BindHead()
                    BindGrid()
                    Dim lock, lockby As String
                    Dim user As String = lblusername.Value

                    gtasks.Dispose()
                    lbltool.Value = "no"
                    lblpart.Value = "no"
                    lbllube.Value = "no"
                    lblnote.Value = "no"
                Else
                    Dim strMessage As String = tmod.getmsg("cdstr259" , "gsubarch.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "noeqid"
                End If

            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr260" , "gsubarch.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "noeqid"
            End Try

        End If

        'btnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'btnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    
    Private Sub BindHead()
        rev = lblrev.Value
        sql = "usp_getTaskHeadarch '" & fuid & "', '" & tid & "' and rev = '" & rev & "'"
        dr = gtasks.GetRdrData(sql)
        dlhd.DataSource = dr
        dlhd.DataBind()
        dr.Close()
    End Sub
    Private Sub BindTaskHead()
        sql = "select * from pmtasksarch where funcid = '" & fuid & "' and tasknum = '" & tid & "' and subtask = 0 and rev = '" & rev & "'"
        dr = gtasks.GetRdrData(sql)
        While dr.Read
            tdtnum.InnerHtml = dr.Item("tasknum").ToString
            tddesc.InnerHtml = dr.Item("taskdesc").ToString
            tdskill.InnerHtml = dr.Item("skill").ToString
            tdqty.InnerHtml = dr.Item("qty").ToString
            tdmin.InnerHtml = dr.Item("ttime").ToString
            tdrd.InnerHtml = dr.Item("rd").ToString
            tdrdt.InnerHtml = dr.Item("rdt").ToString

        End While
        dr.Close()
    End Sub
    Private Sub BindGrid()
        fuid = lblfuid.Value
        tid = tdtnum.InnerHtml
        rev = lblrev.Value
        sql = "select * from pmtasksarch where funcid = '" & fuid & "' and tasknum = '" & tid & "' " _
        + "and subtask <> 0 and rev = '" & rev & "' order by subtask"
        ds = gtasks.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Try
            dgtasks.DataSource = dv
            dgtasks.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Public Function PopulateFail(ByVal comid As String) As DataSet
        cid = lblcid.Value
        rev = lblrev.Value
        sql = "select failid, failuremode from componentfailmodesarch where comid = '" & comid & "' or failindex = '0' and rev = '" & rev & "' order by failuremode"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Public Function PopulatePreTech() As DataSet
        cid = lblcid.Value
        sql = "select ptid, pretech, ptindex from pmPreTech where compid = '" & cid & "' or ptindex = '0' order by ptindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Public Function PopulateComp() As DataSet
        fuid = lblfuid.Value
        rev = lblrev.Value
        sql = "select * from componentsarch where func_id = '" & fuid & "' or compindex = '0' and rev = '" & rev & "' order by compindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Public Function PopulateTaskTypes() As DataSet
        cid = lblcid.Value
        sql = "select ttid, tasktype, taskindex from pmTaskTypes where compid = '" & cid & "' or taskindex = '0' order by taskindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function

    Public Function PopulateStatus() As DataSet
        cid = lblcid.Value
        sql = "select statid, status, statusindex from pmStatus where compid = '" & cid & "' or statusindex = '0' order by statusindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
   
    Public Function PopulateSkills() As DataSet
        cid = lblcid.Value
        sid = lblsid.Value
        sql = "select skillid, skill, skillindex from pmSiteSkills where (compid = '" & cid & "' and siteid = '" & sid & "') or skillindex = '0' order by skillindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function
    Function PopulateCompFM(ByVal comp As String)
        If comp <> "0" Then
            sql = "select failid, failuremode " _
                     + "from componentfailmodes where comid = '" & comp & "'"
            dslev = gtasks.GetDSData(sql)
            Return dslev
        End If

    End Function
    Function PopulateFL(ByVal comp As String)
        If comp <> "0" Then
            sql = "select failid, failuremode " _
                    + "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
                    + "select failid from pmtaskfailmodes where comid = '" & comp & "')"
            dslev = gtasks.GetDSData(sql)
            Return dslev
        End If

    End Function
    Function PopulateTaskFM(ByVal comp As String, ByVal ttid As String)
        If comp <> "0" Then
            sql = "select * from pmtaskfailmodestpm where taskid = '" & ttid & "'"
            dslev = gtasks.GetDSData(sql)
            Return dslev
        End If
    End Function

    Private Sub addtask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles addtask.Click

        fuid = lblfuid.Value
        tid = tdtnum.InnerHtml
        gtasks.Open()
        Dim stcnt As Integer
        sql = "Select count(*) from pmTaskstpm " _
        + "where funcid = '" & fuid & "' and tasknum = '" & tid & "' and subtask <> 0"
        stcnt = gtasks.Scalar(sql)
        Dim newtst As String = stcnt + 1
        sql = "insert into pmtaskstpm (compid, siteid, deptid, cellid, eqid, funcid, tasknum, subtask, " _
        + "skillid, skillindex, skill, freqid, freqindex, freq, rdid, rd, rdindex, taskstatus, ptid, ptindex, pretech, tasktype) select distinct " _
        + "compid, siteid, deptid, cellid, eqid, funcid, '" & tid & "', '" & newtst & "', " _
        + "skillid, skillindex, skill, freqid, freqindex, freq, rdid, rd, rdindex, taskstatus, " _
        + "ptid, ptindex, pretech, tasktype from pmtasks " _
        + "where funcid = '" & fuid & "' and tasknum = '" & tid & "' and subtask = 0"
        gtasks.Update(sql)
        eqid = lbleqid.Value
        gtasks.UpMod(eqid)
        BindGrid()
        gtasks.Dispose()
    End Sub


	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgtasks.Columns(1).HeaderText = dlabs.GetDGPage("gsubarch.aspx", "dgtasks", "1")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(2).HeaderText = dlabs.GetDGPage("gsubarch.aspx", "dgtasks", "2")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(4).HeaderText = dlabs.GetDGPage("gsubarch.aspx", "dgtasks", "4")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(5).HeaderText = dlabs.GetDGPage("gsubarch.aspx", "dgtasks", "5")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(6).HeaderText = dlabs.GetDGPage("gsubarch.aspx", "dgtasks", "6")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(7).HeaderText = dlabs.GetDGPage("gsubarch.aspx", "dgtasks", "7")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang321.Text = axlabs.GetASPXPage("gsubarch.aspx", "lang321")
        Catch ex As Exception
        End Try
        Try
            lang322.Text = axlabs.GetASPXPage("gsubarch.aspx", "lang322")
        Catch ex As Exception
        End Try
        Try
            lang323.Text = axlabs.GetASPXPage("gsubarch.aspx", "lang323")
        Catch ex As Exception
        End Try
        Try
            lang324.Text = axlabs.GetASPXPage("gsubarch.aspx", "lang324")
        Catch ex As Exception
        End Try
        Try
            lang325.Text = axlabs.GetASPXPage("gsubarch.aspx", "lang325")
        Catch ex As Exception
        End Try
        Try
            lang326.Text = axlabs.GetASPXPage("gsubarch.aspx", "lang326")
        Catch ex As Exception
        End Try
        Try
            lang327.Text = axlabs.GetASPXPage("gsubarch.aspx", "lang327")
        Catch ex As Exception
        End Try
        Try
            lang328.Text = axlabs.GetASPXPage("gsubarch.aspx", "lang328")
        Catch ex As Exception
        End Try
        Try
            lang329.Text = axlabs.GetASPXPage("gsubarch.aspx", "lang329")
        Catch ex As Exception
        End Try
        Try
            lang330.Text = axlabs.GetASPXPage("gsubarch.aspx", "lang330")
        Catch ex As Exception
        End Try
        Try
            lang331.Text = axlabs.GetASPXPage("gsubarch.aspx", "lang331")
        Catch ex As Exception
        End Try
        Try
            lang332.Text = axlabs.GetASPXPage("gsubarch.aspx", "lang332")
        Catch ex As Exception
        End Try
        Try
            lang333.Text = axlabs.GetASPXPage("gsubarch.aspx", "lang333")
        Catch ex As Exception
        End Try
        Try
            lang334.Text = axlabs.GetASPXPage("gsubarch.aspx", "lang334")
        Catch ex As Exception
        End Try
        Try
            lang335.Text = axlabs.GetASPXPage("gsubarch.aspx", "lang335")
        Catch ex As Exception
        End Try
        Try
            lang336.Text = axlabs.GetASPXPage("gsubarch.aspx", "lang336")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                addtask.Attributes.Add("src", "../images2/eng/bgbuttons/addtask.gif")
            ElseIf lang = "fre" Then
                addtask.Attributes.Add("src", "../images2/fre/bgbuttons/addtask.gif")
            ElseIf lang = "ger" Then
                addtask.Attributes.Add("src", "../images2/ger/bgbuttons/addtask.gif")
            ElseIf lang = "ita" Then
                addtask.Attributes.Add("src", "../images2/ita/bgbuttons/addtask.gif")
            ElseIf lang = "spa" Then
                addtask.Attributes.Add("src", "../images2/spa/bgbuttons/addtask.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
