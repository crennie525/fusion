<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmarchtasks.aspx.vb" Inherits="lucy_r12.pmarchtasks" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>pmarchtasks</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="javascript" src="../scripts/pmtaskdivfuncarch.js"></script>
		<script language="JavaScript" src="../scripts1/pmarchtasksaspx_1.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="checkit();" >
		<div id="overDiv" style="Z-INDEX: 1000; VISIBILITY: hidden; POSITION: absolute"></div>
		<form id="form1" method="post" runat="server">
			<div class="FreezePaneOff" id="FreezePane" align="center">
				<div class="InnerFreezePane" id="InnerFreezePane"></div>
			</div>
			<div style="Z-INDEX: 100; LEFT: 0px; POSITION: absolute; TOP: 0px">
				<table cellSpacing="0" cellPadding="3" width="742">
					<tr>
						<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/archtop1transpm.gif" border="0"></td>
						<td class="thdrsingrt label" onmouseover="return overlib('Against what is the PM intended to protect?', ABOVE, LEFT)"
							onmouseout="return nd()" width="696"><asp:Label id="lang351" runat="server">Failure Modes, Causes And/Or Effects</asp:Label></td>
					</tr>
				</table>
				<table id="tbeq" cellSpacing="0" cellPadding="0" width="742">
					<tr>
						<td class="label" width="160" height="24"><asp:Label id="lang352" runat="server">Component Addressed:</asp:Label></td>
						<td class="plainlabel" width="242" id="tdcomp" runat="server"></td>
						<td class="label" width="30"><asp:Label id="lang353" runat="server">Qty:</asp:Label></td>
						<td class="plainlabel" width="282" id="tdcqty" runat="server"></td>
						<td class="label" width="28"></td>
					</tr>
				</table>
				<table id="tbeq2" cellSpacing="0" cellPadding="0" width="742">
					<tr>
						<td width="365">
							<table cellSpacing="0" cellPadding="0">
								<tr>
									<td width="20"></td>
									<td width="170"></td>
									<td width="23"></td>
									<td width="5"></td>
									<td width="195"></td>
									<td width="20"></td>
								</tr>
								<tr>
									<td class="transrowgray">&nbsp;</td>
									<td class="label transrowgray" align="center" bgColor="#edf0f3" colSpan="1"><asp:Label id="lang354" runat="server">Original Failure Modes</asp:Label></td>
									<td class="transrowdrk"></td>
									<td class="transrowdrk">&nbsp;</td>
									<td class="bluelabel transrowdrk" align="center" bgColor="#bad8fc" colSpan="1"><asp:Label id="lang355" runat="server">Revised Failure Modes</asp:Label></td>
									<td class="transrowdrk"></td>
								</tr>
								<tr>
									<td class="transrowgray">&nbsp;</td>
									<td align="center" class="transrowdrk"><asp:listbox id="lbofailmodes" runat="server" Width="150px" CssClass="plainlabel" ForeColor="Blue"
											Height="50px" SelectionMode="Multiple"></asp:listbox></td>
									<td vAlign="middle" align="center" class="transrowdrk"></td>
									<td>&nbsp;</td>
									<td align="center" class="transrowdrk"><asp:listbox id="lbCompFM" runat="server" Width="150px" CssClass="plainlabel" Height="50px" SelectionMode="Multiple"></asp:listbox></td>
									<td vAlign="middle" align="center" class="transrowdrk"></td>
								</tr>
							</table>
						</td>
						<td width="377">
							<table cellSpacing="0" cellPadding="0">
								<tr>
									<td width="150"></td>
									<td width="20"></td>
									<td width="150"></td>
									<td width="47"></td>
									<td width="10"></td>
								</tr>
								<tr>
									<td class="redlabel transrowdrk" align="center"><asp:Label id="lang356" runat="server">Not Addressed</asp:Label></td>
									<td bgColor="#bad8fc">&nbsp;</td>
									<td class="bluelabel transrowdrk" align="center"><asp:Label id="lang357" runat="server">Selected</asp:Label></td>
									<td class="transrowdrk">&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align="center" class="transrowdrk"><asp:listbox id="lbfaillist" runat="server" Width="150px" CssClass="plainlabel" ForeColor="Red"
											Height="50px" SelectionMode="Multiple"></asp:listbox></td>
									<td vAlign="middle" align="center" class="transrowdrk"></td>
									<td align="center" class="transrowdrk"><asp:listbox id="lbfailmodes" runat="server" Width="150px" CssClass="plainlabel" ForeColor="Blue"
											Height="50px" SelectionMode="Multiple"></asp:listbox></td>
									<td class="transrowdrk">&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="0" width="742">
					<tr>
						<td colSpan="2"><IMG height="2" src="../images/appbuttons/minibuttons/2PX.gif"></td>
					</tr>
					<tr>
						<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/archtop1transpm.gif" border="0"></td>
						<td class="thdrsingrt label" onmouseover="return overlib('What are you going to do?')"
							onmouseout="return nd()" width="684" colSpan="3"><asp:Label id="lang358" runat="server">Task Activity Details</asp:Label></td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="0" width="742">
					<tr>
						<td>
							<table cellSpacing="0" cellPadding="2" width="372">
								<tr>
									<td width="30"></td>
									<td width="140"></td>
									<td width="30"></td>
									<td width="140"></td>
									<td width="2"></td>
								</tr>
								<tr>
									<td class="label transrowgray" colSpan="4"><asp:Label id="lang359" runat="server">Original Task Description</asp:Label></td>
									<td></td>
								</tr>
								<tr>
									<td class="transrowgray" colSpan="4"><TEXTAREA id="txtodesc" onkeyup="copydesc(this.value);" style="FONT-SIZE: 12px; WIDTH: 296px; FONT-FAMILY: Arial; HEIGHT: 40px"
											name="txtdesc" rows="2" cols="34" runat="server" readonly></TEXTAREA></td>
									<td></td>
								</tr>
								<tr height="24">
									<td class="label transrowgray"><asp:Label id="lang360" runat="server">Type</asp:Label></td>
									<td id="tdtypeo" runat="server" class="plainlabel transrowgray"></td>
									<td class="label transrowgray">PdM</td>
									<td id="tdpto" runat="server" class="plainlabel transrowgray"></td>
								</tr>
							</table>
						</td>
						<td>
							<table cellSpacing="0" cellPadding="2" width="370">
								<tr>
									<td width="30"></td>
									<td width="140"></td>
									<td width="30"></td>
									<td width="90"></td>
									<td width="50"></td>
								</tr>
								<tr>
									<td class="label transrowdrk" colSpan="5"><asp:Label id="lang361" runat="server">Revised Task Description</asp:Label></td>
									<td></td>
								</tr>
								<tr>
									<td class="transrowdrk" colSpan="4"><TEXTAREA class="plainlabel" id="txtdesc" style="FONT-SIZE: 12px; WIDTH: 290px; FONT-FAMILY: Arial; HEIGHT: 40px"
											name="txtdesc" rows="2" cols="32" runat="server" readonly></TEXTAREA></td>
									<td class="transrowdrk">
										<br>
										<IMG onmouseover="return overlib('Add/Edit Measurements for this Task')" onclick="getMeasDiv();"
											onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/measblu.gif"
											width="27"></td>
								</tr>
								<tr>
									<td class="label transrowdrk"><asp:Label id="lang362" runat="server">Type</asp:Label></td>
									<td id="tdtype" runat="server" class="plainlabel transrowdrk"></td>
									<td class="label transrowdrk">PdM</td>
									<td colSpan="2" id="tdpt" runat="server" class="plainlabel transrowdrk"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="2" width="742">
					<tr>
						<td><IMG height="2" src="../images/appbuttons/minibuttons/2PX.gif" width="2"></td>
					</tr>
					<tr>
						<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/archtop1transpm.gif" border="0"></td>
						<td class="thdrsingrt label" onmouseover="return overlib('How are you going to do it?')"
							onmouseout="return nd()" width="554" colSpan="3"><asp:Label id="lang363" runat="server">Planning &amp; Scheduling Details</asp:Label></td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="0" width="742">
					<tr>
						<td vAlign="top">
							<table cellSpacing="0" cellPadding="2" width="372">
								<tr>
									<td width="92"></td>
									<td width="78"></td>
									<td width="16"></td>
									<td width="30"></td>
									<td width="30"></td>
									<td width="60"></td>
									<td width="20"></td>
									<td width="20"></td>
									<td width="31"></td>
									<td width="2"></td>
								</tr>
								<tr>
									<td class="label transrowgray" colSpan="2"><asp:Label id="lang364" runat="server">Original Skill</asp:Label></td>
									<td class="label transrowgray">Qty</td>
									<td class="label transrowgray" colSpan="6"><asp:Label id="lang365" runat="server">Min Ea</asp:Label></td>
									<td><IMG src="../images/appbuttons/minibuttons/2PX.gif"></td>
								</tr>
								<tr>
									<td colSpan="2" id="tdskillo" runat="server" class="plainlabel transrowgray"></td>
									<td id="tdqtyo" runat="server" class="plainlabel transrowgray"></td>
									<td colSpan="6" id="tdtro" runat="server" class="plainlabel transrowgray"></td>
								<tr>
									<td class="label transrowgray" style="WIDTH: 88px"><asp:Label id="lang366" runat="server">Frequency</asp:Label></td>
									<td id="tdfreqo" runat="server" class="plainlabel transrowgray"></td>
									<td class="label transrowgray" colSpan="3"><asp:Label id="lang367" runat="server">Status</asp:Label></td>
									<td colSpan="2" id="tdeqstato" runat="server" class="plainlabel transrowgray"></td>
									<td class="label transrowgray">DT</td>
									<td id="tdrdto" runat="server" class="plainlabel transrowgray"></td>
								</tr>
								<tr>
									<td class="bluelabel transrowgray"><asp:Label id="lang368" runat="server">Task Status</asp:Label></td>
									<td colSpan="9" id="tdtaskstat" runat="server" class="plainlabel transrowgray"></td>
								</tr>
							</table>
						</td>
						<td vAlign="top">
							<table cellSpacing="0" cellPadding="2" width="370">
								<tr>
									<td width="60"></td>
									<td width="70"></td>
									<td width="20"></td>
									<td width="30"></td>
									<td width="30"></td>
									<td width="40"></td>
									<td width="30"></td>
									<td width="20"></td>
									<td width="20"></td>
									<td width="35"></td>
								</tr>
								<tr>
									<td class="label transrowdrk" style="WIDTH: 134px" colSpan="2"><asp:Label id="lang369" runat="server">Revised Skill</asp:Label></td>
									<td class="label transrowdrk">Qty</td>
									<td class="label transrowdrk" colSpan="3"><asp:Label id="lang370" runat="server">Min Ea</asp:Label></td>
									<td class="label transrowdrk" colSpan="3"><asp:Label id="lang371" runat="server">P-F Interval</asp:Label></td>
								</tr>
								<tr>
									<td style="WIDTH: 134px" colSpan="2" id="tdskill" runat="server" class="plainlabel transrowdrk"></td>
									<td id="tdqty" runat="server" class="plainlabel transrowdrk"></td>
									<td colSpan="3" id="tdtr" runat="server" class="plainlabel transrowdrk"></td>
									<td id="tdpfint" runat="server" class="plainlabel transrowdrk"></td>
									<td class="transrowdrk"></td>
									<td class="transrowdrk"></td>
								</tr>
								<tr>
									<td class="label transrowdrk"><asp:Label id="lang372" runat="server">Frequency</asp:Label></td>
									<td style="WIDTH: 70px" id="tdfreq" runat="server" class="plainlabel transrowdrk"></td>
									<td class="label transrowdrk" colSpan="3"><asp:Label id="lang373" runat="server">Status</asp:Label></td>
									<td colSpan="2" id="tdeqstat" runat="server" class="plainlabel transrowdrk"><asp:dropdownlist id="ddeqstat" runat="server" Width="80px" CssClass="plainlabel"></asp:dropdownlist></td>
									<td class="label transrowdrk" style="WIDTH: 19px">DT</td>
									<td id="tdrdt" runat="server" class="plainlabel transrowdrk"></td>
								</tr>
								<tr>
									<td class="labelsm" colSpan="5"><asp:checkbox id="cbloto" runat="server"></asp:checkbox><asp:Label id="lang374" runat="server">LOTO</asp:Label><asp:checkbox id="cbcs" runat="server"></asp:checkbox>CS</td>
									<td class="bluelabel" align="right" colSpan="5"><img src="../images/appbuttons/minibuttons/spareparts.gif" onclick="GetSpareDiv();" onmouseover="return overlib('Choose Equipment Spare Parts for this Task')"
											onmouseout="return nd()"><IMG id="Img3" onmouseover="return overlib('Add/Edit Parts for this Task')" onclick="GetPartDiv();"
											onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/parttrans.gif" width="23" runat="server"><IMG onmouseover="return overlib('Add/Edit Tools for this Task')" onclick="GetToolDiv();"
											onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/tooltrans.gif" width="23"><IMG onmouseover="return overlib('Add/Edit Lubricants for this Task')" onclick="GetLubeDiv();"
											onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/lubetrans.gif" width="23"><IMG onmouseover="return overlib('Add/Edit Notes for this Task')" onclick="GetNoteDiv();"
											onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/notessm.gif" width="25">
										<IMG id="Img5" onmouseover="return overlib('Edit Pass/Fail Adjustment Levels - This Task', ABOVE, LEFT)"
											onclick="editadj('0');" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/screwd.gif"
											border="0" runat="server">
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="0" width="742" border="0">
					<tr id="Tr1" runat="server" NAME="Tr1">
						<td style="BORDER-TOP: #7ba4e0 thin solid" align="center"><IMG id="btnStart" height="20" src="../images/appbuttons/minibuttons/tostartbg.gif" width="20"
								runat="server"><IMG id="btnPrev" height="20" src="../images/appbuttons/minibuttons/prevarrowbg.gif"
								width="20" runat="server">
						</td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" align="center" height="30"><asp:label id="Label22" runat="server" CssClass="bluelabel" ForeColor="Blue" Font-Size="X-Small"
								Font-Names="Arial" Font-Bold="True">Task# </asp:label><asp:label id="lblpg" runat="server" CssClass="bluelabel" ForeColor="Blue" Font-Size="X-Small"
								Font-Names="Arial" Font-Bold="True"></asp:label><asp:label id="Label23" runat="server" CssClass="bluelabel" ForeColor="Blue" Font-Size="X-Small"
								Font-Names="Arial" Font-Bold="True"> of </asp:label><asp:label id="lblcnt" runat="server" CssClass="bluelabel" ForeColor="Blue" Font-Size="X-Small"
								Font-Names="Arial" Font-Bold="True"></asp:label></td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" align="center">
							<IMG id="btnNext" height="20" src="../images/appbuttons/minibuttons/nextarrowbg.gif"
								width="20" runat="server"><IMG id="btnEnd" height="20" src="../images/appbuttons/minibuttons/tolastbg.gif" width="20"
								runat="server">
						</td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" align="center"><asp:label id="Label26" runat="server" CssClass="greenlabel" Font-Bold="True" Font-Names="Arial"
								Font-Size="X-Small">Sub Tasks: </asp:label><asp:label id="lblsubcount" runat="server" CssClass="greenlabel" Font-Bold="True" Font-Names="Arial"
								Font-Size="X-Small"></asp:label><asp:label id="lblspg" runat="server" CssClass="details" ForeColor="Green" Font-Bold="True"
								Font-Names="Arial" Font-Size="X-Small"></asp:label><asp:label id="Label27" runat="server" CssClass="details" ForeColor="Green" Font-Bold="True"
								Font-Names="Arial" Font-Size="X-Small"> of </asp:label><asp:label id="lblscnt" runat="server" CssClass="details" ForeColor="Green" Font-Bold="True"
								Font-Names="Arial" Font-Size="X-Small"></asp:label></td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" align="right">&nbsp;<IMG onclick="GetNavGrid();" height="20" alt="" src="../images/appbuttons/bgbuttons/navgrid.gif"
								width="20">
							<asp:imagebutton id="btnaddsubtask" runat="server" CssClass="details" ImageUrl="../images/appbuttons/minibuttons/subtask.gif"></asp:imagebutton>
							<IMG id="imgrat" onmouseover="return overlib('Add/Edit Rationale for Task Changes')"
								onclick="GetRationale();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/rationale.gif"
								width="20" runat="server"><IMG id="sgrid" onmouseover="return overlib('Add/Edit Sub Tasks')" onclick="getsgrid();"
								onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/sgrid1.gif" width="20">
							<IMG id="Img4" onmouseover="return overlib('Edit Pass/Fail Adjustment Levels - All Tasks', ABOVE, LEFT)"
								onclick="editadj('pre');" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/screwall.gif"
								border="0" runat="server">
						</td>
					</tr>
					<tr>
						<td align="center" colspan="3"></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td width="45"></td>
						<td width="120"></td>
						<td width="45"></td>
						<td width="90"></td>
						<td width="367"></td>
					</tr>
				</table>
			</div>
			<input id="lblsvchk" type="hidden" runat="server" NAME="lblsvchk"> <input id="lblcompchk" type="hidden" name="lblcompchk" runat="server">
			<input id="lblsb" type="hidden" name="lblsb" runat="server"> <input id="lblfilt" type="hidden" name="lblfilt" runat="server">
			<input id="lblenable" type="hidden" name="lblenable" runat="server"> <input id="lblpgholder" type="hidden" name="lblpgholder" runat="server">
			<input id="lblcompfailchk" type="hidden" name="lblcompfailchk" runat="server"> <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
			<input id="lbltasklev" type="hidden" name="lbltasklev" runat="server"> <input id="lblcid" type="hidden" name="lblcid" runat="server">
			<input id="lblchk" type="hidden" name="lblchk" runat="server"> <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input id="lbleqid" type="hidden" name="lbleqid" runat="server"> <input id="lblstart" type="hidden" name="lblpmtype" runat="server">
			<input id="lblt" type="hidden" name="lblt" runat="server"> <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
			<input id="lblst" type="hidden" name="lblst" runat="server"> <input id="lblco" type="hidden" name="lblco" runat="server">
			<input id="lblpar" type="hidden" name="lblpar" runat="server"> <input id="lbltabid" type="hidden" name="lbltabid" runat="server">
			<input id="lblpmstr" type="hidden" name="lblpmstr" runat="server"> <input id="pgflag" type="hidden" name="pgflag" runat="server">
			<input id="lblpmid" type="hidden" name="lblpmid" runat="server"> <input id="lbldid" type="hidden" runat="server" NAME="lbldid">
			<input id="lblclid" type="hidden" runat="server" NAME="lblclid"><input id="lblsid" type="hidden" runat="server" NAME="lblsid">
			<input id="lbldel" type="hidden" runat="server" NAME="lbldel"><input id="lblsave" type="hidden" runat="server" NAME="lblsave">
			<input id="lblgrid" type="hidden" runat="server" NAME="lblgrid"><input id="lblcind" type="hidden" runat="server" NAME="lblcind">
			<input id="lblcurrsb" type="hidden" runat="server" NAME="lblcurrsb"><input id="lblcurrcs" type="hidden" runat="server" NAME="lblcurrcs">
			<input id="lbldocpmid" type="hidden" runat="server" NAME="lbldocpmid"><input id="lbldocpmstr" type="hidden" runat="server" NAME="lbldocpmstr">
			<input id="lblsesscnt" type="hidden" runat="server" NAME="lblsesscnt"><input id="appchk" type="hidden" name="appchk" runat="server">
			<input id="lbllog" type="hidden" name="lbllog" runat="server"><input id="lblfiltcnt" type="hidden" runat="server" NAME="lblfiltcnt">
			<input id="lblusername" type="hidden" runat="server" NAME="lblusername"> <input id="lbllock" type="hidden" name="lbllock" runat="server">
			<input id="lbllockedby" type="hidden" name="lbllockedby" runat="server"> <input type="hidden" id="lblhaspm" runat="server" NAME="lblhaspm">
			<input type="hidden" id="lbltyp" runat="server" NAME="lbltyp"> <input type="hidden" id="lbllid" runat="server" NAME="lbllid">
			<input type="hidden" id="lblnoeq" runat="server" NAME="lblnoeq"> <input type="hidden" id="lblro" runat="server" NAME="lblro">
			<input type="hidden" id="lbltpmalert" runat="server" NAME="lbltpmalert"><input type="hidden" id="lblrev" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
		</TR></TBODY></TABLE></FORM>
	</body>
</HTML>
