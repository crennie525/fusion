<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmarchimage.aspx.vb" Inherits="lucy_r12.pmarchimage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>pmarchimage</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/pmarchimageaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 5px; POSITION: absolute; TOP: 0px" cellPadding="0">
				<tr>
					<td colspan="3" align="center"><A onclick="getbig();" href="#"><IMG id="imgeq" height="216" src="../images/appimages/eqimg1.gif" width="216" border="0"
								runat="server"></A></td>
				</tr>
				<tr>
					<td class="bluelabel" id="tdcnt" align="center" runat="server" width="150"></td>
					<td align="center"><A onclick="getprev();" href="#"><IMG id="pr" alt="" src="../images/appbuttons/minibuttons/prevarrowbg.gif" border="0"
								runat="server" width="20" height="20"></A> <A onclick="getnext();" href="#">
							<IMG id="ne" alt="" src="../images/appbuttons/minibuttons/nextarrowbg.gif" border="0"
								runat="server" width="20" height="20"></A></td>
					<td align="center"><A onclick="getport();" href="#"><IMG id="po" alt="" src="../images/appbuttons/bgbuttons/picgrid.gif" border="0" runat="server"
								width="20" height="20"></A></td>
				</tr>
			</table>
			<input id="lblpic" type="hidden" runat="server" NAME="lblpic"><input type="hidden" id="lbleqid" runat="server" NAME="lbleqid">
			<input type="hidden" id="lblcur" runat="server" NAME="lblcur"><input type="hidden" id="lblcnt" runat="server" NAME="lblcnt">
			<input type="hidden" id="lblpareqid" runat="server" NAME="lblpareqid"><input type="hidden" id="lblurl" runat="server" NAME="lblurl">
			<input type="hidden" id="lblrev" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
