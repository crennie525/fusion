

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class pmarch
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim main As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim chk, did, clid, start, Login, typ As String
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdarch As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            Exit Sub
        End Try
        If Not IsPostBack Then
            If Request.QueryString("start").ToString = "yes" Then
                typ = Request.QueryString("typ").ToString
                'If typ = "loc" Then
                lblloc.Value = Request.QueryString("lid").ToString
                'End If
                lbldid.Value = Request.QueryString("did").ToString
                lblclid.Value = Request.QueryString("clid").ToString
                lblchk.Value = Request.QueryString("chk").ToString
                If lblloc.Value <> "" And lbldid.Value = "" Then
                    typ = "loc"
                End If
                Try
                    lblsid.Value = Request.QueryString("sid").ToString
                Catch ex As Exception
                    Try
                        lblsid.Value = Request.QueryString("psite").ToString
                    Catch ex1 As Exception

                    End Try
                End Try
                main.Open()
                GetArch(typ)
                main.Dispose()
            Else
                tdarch.InnerHtml = "Waiting for location..."
            End If

        End If
    End Sub
    Private Sub GetArch(ByVal typ As String)
        Dim sb As StringBuilder = New StringBuilder
        Dim dept As String = lbldid.Value
        Dim cell As String = lblclid.Value
        Dim lid As String = lblloc.Value
        Dim eqnum As String = "eqcopytest"
        Dim eqid As String = "129"
        Dim eqdesc As String = ""
        Dim sid, did, clid As String
        sid = lblsid.Value
        sb.Append("<table cellspacing=""0"" border=""0""><tr>")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""155""></tr>" & vbCrLf)
        chk = lblchk.Value
        Dim comp As String = HttpContext.Current.Session("comp").ToString()
        If typ = "loc" Then
            sql = "select distinct e.rev, e.siteid, e.dept_id, e.cellid, e.eqid, e.eqnum, isnull(e.eqdesc, 'No Description') as eqdesc, isnull(f.func_id, 0) as func_id, " _
           + "e.locked, e.lockedby, e.trans, e.transstatus, " _
           + "f.func, isnull(c.comid, 0) as comid, c.compnum, cnt = (select count(c.compnum) " _
           + "from componentsarch c where c.func_id = f.func_id and c.rev = f.rev) " _
           + "from equipmentarch e left outer join functionsarch f on f.eqid = e.eqid and f.rev = e.rev " _
           + "left outer join componentsarch c on c.func_id = f.func_id and c.rev = f.rev " _
           + "where(e.locid = '" & lid & "' and siteid = '" & sid & "' and dept_id is null)"
        Else
            If chk = "yes" Then
                sql = "select distinct e.rev, e.siteid, e.dept_id, e.cellid, e.eqid, e.eqnum, isnull(e.eqdesc, 'No Description') as eqdesc, isnull(f.func_id, 0) as func_id, " _
                + "e.locked, e.lockedby, e.trans, e.transstatus,  " _
                + "f.func, isnull(c.comid, 0) as comid, c.compnum, cnt = (select count(c.compnum) " _
                + "from componentsarch c where c.func_id = f.func_id and c.rev = f.rev) " _
                + "from equipmentarch e left outer join functionsarch f on f.eqid = e.eqid and f.rev = e.rev  " _
                + "left outer join componentsarch c on c.func_id = f.func_id and c.rev = f.rev  " _
                + "where(e.dept_id = '" & dept & "' and e.cellid = '" & cell & "' and e.compid = '" & comp & "')"
            Else
                sql = "select distinct e.rev, e.siteid, e.dept_id, e.cellid, e.eqid, e.eqnum, isnull(e.eqdesc, 'No Description') as eqdesc, isnull(f.func_id, 0) as func_id, " _
                + "e.locked, e.lockedby, e.trans, e.transstatus,  " _
                + "f.func, isnull(c.comid, 0) as comid, c.compnum, cnt = (select count(c.compnum) " _
                + "from componentsarch c where c.func_id = f.func_id and c.rev = f.rev) " _
                + "from equipmentarch e left outer join functionsarch f on f.eqid = e.eqid and f.rev = e.rev  " _
                + "left outer join componentsarch c on c.func_id = f.func_id and c.rev = f.rev  " _
                + "where(e.dept_id = '" & dept & "' and e.compid = '" & comp & "')"
            End If
        End If

        'h.Open()
        dr = main.GetRdrData(sql)
        '*** Multi Add ***
        Dim trans As String = "0"
        Dim tstat As String = "0"
        '*** End Multi Add ***
        Dim locby As String
        Dim lock As String = "0"
        Dim fid As String = "0"
        Dim cid As String = "0"
        Dim eid As String = "0"
        Dim rev As String = "0"
        Dim cidhold As Integer = 0
        Dim cnt As Integer = 0
        Dim ncnt As Integer = 0
        While dr.Read
            ncnt = 1
            If dr.Item("eqid") <> eid Then
                If eid <> "0" Then
                    sb.Append("</table></td></tr>")
                End If
                eid = dr.Item("eqid").ToString
                rev = dr.Item("rev").ToString
                sid = dr.Item("siteid").ToString
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                If clid <> "" Then
                    chk = "yes"
                Else
                    chk = "no"
                End If
                eqnum = dr.Item("eqnum").ToString
                eqdesc = dr.Item("eqdesc").ToString
                lock = dr.Item("locked").ToString
                locby = dr.Item("lockedby").ToString
                sb.Append("<tr><td><img id='i" + eid + "' ")
                sb.Append("onclick=""fclose('t" + eid + "', 'i" + eid + "');""")
                sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>")
                sb.Append("<td colspan=""2"" class=""plainlabel""><a href=""#"" onclick=""gotoeq('" & eid & "','" & rev & "')"" class=""linklabel"">" & eqnum & "</a> - " & eqdesc)
                'sb.Append("</td></tr>" & vbCrLf)
                '*** Multi Add ***
                trans = dr.Item("trans").ToString
                If trans = "0" OrElse Len(trans) = 0 Then
                    'sb.Append("</td></tr>" & vbCrLf)
                    'If lock = "0" OrElse Len(lock) = 0 Then
                    sb.Append(" - " & rev & "</td></tr>" & vbCrLf)
                    'Else
                    'sb.Append("&nbsp;<img src='../images/appbuttons/minibuttons/lillock.gif' ")
                    'sb.Append("onmouseover=""return overlib('" & tmod.getov("cov26" , "pmarch.aspx.vb") & ": " &  locby & "')"" ")
                    'sb.Append("onmouseout=""return nd()""></td></tr>" & vbCrLf)
                    'End If
                Else
                    tstat = dr.Item("transstatus").ToString
                    If tstat <> "4" Then
                        sb.Append("&nbsp;<img src='../images/appbuttons/minibuttons/warning.gif' ")
                        sb.Append("onmouseover=""return overlib('" & tmod.getov("cov27" , "pmarch.aspx.vb") & "')"" ")
                        sb.Append("onmouseout=""return nd()""></td></tr>" & vbCrLf)
                    End If

                End If
                '*** End Multi Add ***
                'If lock = "0" OrElse Len(lock) = 0 Then
                'sb.Append("</td></tr>" & vbCrLf)
                'Else
                'sb.Append("&nbsp;<img src='../images/appbuttons/minibuttons/lillock.gif' ")
                'sb.Append("onmouseover=""return overlib('" & tmod.getov("cov28" , "pmarch.aspx.vb") & ": " &  locby & "')"" ")
                'sb.Append("onmouseout=""return nd()""></td></tr>" & vbCrLf)
                'End If
                sb.Append("<tr><td></td><td colspan=""2""><table class=""details"" cellspacing=""0"" id='t" + eid + "' border=""0"">")
                sb.Append("<tr><td width==""15""></td><td width==""155""></td></tr>")
            End If


            If dr.Item("func_id").ToString <> fid Then
                If dr.Item("func_id").ToString <> "0" Then
                    eid = dr.Item("eqid").ToString
                    fid = dr.Item("func_id").ToString
                    sid = dr.Item("siteid").ToString
                    did = dr.Item("dept_id").ToString
                    clid = dr.Item("cellid").ToString
                    cidhold = dr.Item("cnt").ToString
                    sb.Append("<tr>" & vbCrLf & "<td colspan=""2""><table cellspacing=""0"" border=""0"">" & vbCrLf)
                    sb.Append("<tr><td><img id='i" + fid + "' onclick=""fclose('t" + fid + "', 'i" + fid + "');"" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                    sb.Append("<td><a href=""#"" onclick=""gotofu('" & fid & "', '" & eid & "','" & rev & "')"" class=""linklabelblk"">" & dr.Item("func").ToString & "</a></td></tr></table></td></tr>" & vbCrLf)
                    If dr.Item("comid").ToString <> cid Then
                        If dr.Item("comid").ToString <> "0" Then
                            cid = dr.Item("comid").ToString
                            eid = dr.Item("eqid").ToString
                            fid = dr.Item("func_id").ToString
                            sid = dr.Item("siteid").ToString
                            did = dr.Item("dept_id").ToString
                            clid = dr.Item("cellid").ToString
                            If cnt = 0 Then
                                cnt = cnt + 1
                                sb.Append("<tr><td width=""15""></td>" & vbCrLf & "<td width=""155""><table border=""0"" class=""details"" cellspacing=""0"" id=""t" + fid + """>" & vbCrLf)
                                sb.Append("<tr><td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & cid & "', '" & fid & "', '" & eid & "','" & rev & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                                If cnt = cidhold Then
                                    cnt = 0
                                    sb.Append("</table></td></tr>")
                                End If
                            End If
                        Else
                            'cnt = 0
                            'sb.Append("</table></td></tr>")
                        End If

                    End If
                Else
                    fid = "0"
                End If

            ElseIf dr.Item("comid").ToString <> cid Then
                If fid <> "0" Then
                    cid = dr.Item("comid").ToString
                    If cnt = 0 Then
                        cnt = cnt + 1
                        sb.Append("<tr><td></td><td></td>" & vbCrLf & "<td><table cellspacing=""0"" id=""t" + fid + """>" & vbCrLf)
                        sb.Append("<tr><td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & cid & "', '" & fid & "', '" & eid & "','" & rev & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                    Else
                        cnt = cnt + 1
                        sb.Append("<tr><td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & cid & "', '" & fid & "', '" & eid & "','" & rev & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                    End If
                    If cnt = cidhold Then
                        cnt = 0
                        sb.Append("</table></td></tr>")
                    End If
                    'Else
                    'cid = "0"
                    'cnt = 0
                    'sb.Append("</table></td></tr>")
                End If

            End If

        End While
        dr.Close()
        'h.Dispose()
        sb.Append("</td></tr></table></td></tr></table>")
        'Response.Write(sb.ToString)
        If ncnt <> 0 Then
            tdarch.InnerHtml = sb.ToString
        Else
            tdarch.InnerHtml = "<font class='plainlabelblue'>Waiting for Location...</font>"
        End If
    End Sub

End Class
