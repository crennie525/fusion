<%@ Page Language="vb" AutoEventWireup="false" Codebehind="archrationaledialog.aspx.vb" Inherits="lucy_r12.archrationaledialog" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PM Archive Optimization Rationale (Read Only)</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<script language="JavaScript" src="../scripts/sessrefdialog.js"></script>
		<script language="JavaScript" src="../scripts1/archrationaledialogaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="handleentry();">
		<form id="form1" method="post" runat="server">
			<iframe id="ifaddrat" src="" width="100%" height="100%" frameBorder="no"></iframe>
			<iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;"></iframe>
     <script type="text/javascript">
         document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script><input type="hidden" id="lblsessrefresh" runat="server" NAME="lblsessrefresh">
			<input id="lblpg" type="hidden" runat="server" NAME="lblpg"><input id="lbltaskid" type="hidden" runat="server" NAME="lbltaskid">
			<input id="lbldid" type="hidden" runat="server" NAME="lbldid"> <input id="lblclid" type="hidden" runat="server" NAME="lblclid">
			<input id="lbleqid" type="hidden" runat="server" NAME="lbleqid"> <input id="lblfuid" type="hidden" runat="server" NAME="lblfuid">
			<input id="lblcoid" type="hidden" runat="server" NAME="lblcoid"> <input type="hidden" id="lbllog" runat="server" NAME="lbllog">
			<input type="hidden" id="lblro" runat="server" NAME="lblro"> <input type="hidden" id="lblrev" runat="server">
			<input type="hidden" id="lblfslang" runat="server" NAME="lblfslang">
		</form>
	</body>
</HTML>
