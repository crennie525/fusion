

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmarchgrid
    Inherits System.Web.UI.Page
	Protected WithEvents lang347 As System.Web.UI.WebControls.Label

	Protected WithEvents lang346 As System.Web.UI.WebControls.Label

	Protected WithEvents lang345 As System.Web.UI.WebControls.Label

	Protected WithEvents lang344 As System.Web.UI.WebControls.Label

	Protected WithEvents lang343 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim ds As DataSet
    Dim tasksg As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim start, tl, cid, sid, did, clid, eqid, chk, fuid, Val, field, name, Filter, coid, typ, lid, rev As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rptrtasks As System.Web.UI.WebControls.Repeater
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents taskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        tasksg.Open()
        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            taskcnt.Value = "0"
            If start = "yes" Then
                tl = Request.QueryString("tl").ToString
                lbltasklev.Value = tl
                cid = Request.QueryString("cid").ToString
                lblcid.Value = cid
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                did = Request.QueryString("did").ToString
                lbldid.Value = did
                clid = Request.QueryString("clid").ToString
                lblclid.Value = clid
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                chk = Request.QueryString("chk").ToString
                lblchk.Value = chk
                fuid = Request.QueryString("fuid").ToString
                lblfuid.Value = fuid
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                lid = Request.QueryString("lid").ToString
                lbllid.Value = lid
                rev = Request.QueryString("rev").ToString
                lblrev.Value = rev
                Try
                    coid = Request.QueryString("comid").ToString
                Catch ex As Exception
                    coid = ""
                End Try

                lblcoid.Value = coid
                lblfilt.Value = Filter
                If fuid <> "0" Then
                    LoadPage(fuid)
                    If coid <> "" AndAlso coid <> "0" Then
                        GoToTask(coid)
                    End If
                Else
                    fuid = "na"
                    LoadPage(fuid)
                End If

            Else
                fuid = "na"
                LoadPage(fuid)
            End If
        End If
        tasksg.Dispose()
    End Sub
    Private Sub GoToTask(ByVal coid As String)
        Dim tn As Integer
        Dim tasknum As String
        tl = lbltasklev.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        chk = lblchk.Value
        fuid = lblfuid.Value
        typ = lbltyp.Value
        lid = lbllid.Value
        rev = lblrev.Value
        sql = "select tasknum from pmtasksarch where funcid = '" & fuid & "' and comid = '" & coid & "' and rev = '" & rev & "'"
        dr = tasksg.GetRdrData(sql)
        While dr.Read
            tasknum = dr.Item("tasknum").ToString
        End While
        dr.Close()
        'tasksg.Dispose()
        If tasknum <> "" Then
          
            Response.Redirect("pmarchtasks.aspx?start=yes&tl=5&chk=" & chk & "&cid=" & _
           cid + "&fuid=" & fuid & "&comid=" & coid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & _
           "&eqid=" & eqid & "&task=" & tasknum & "&typ=" & typ & "&lid=" & lid & "&rev=" & rev)
        Else

            LoadPage(fuid)
        End If


    End Sub
    Private Sub LoadPage(ByVal fuid As String)
        Dim cnt As Integer
        Dim sqlcnt As String
        rev = lblrev.Value
        If fuid = "na" Then
            cnt = 0
            sql = "select *, 'Orig' as 'o', 'Rev' as 'r' from pmtasksarch where pmtskid = 0 and rev = '" & rev & "'"
        Else
            cnt = 1
            'sql = "select t.pmtskid, t.tasknum, t.taskdesc, " _
            '+ "isnull(c.compnum + ' - ' + c.compdesc, 'None Selected') as compnum " _
            '+ "from pmtasks t left outer join components c on c.comid = t.comid " _
            '+ "where subtask = 0 and t.funcid = '" & fuid & "' " _
            '+ "order by tasknum"
            sql = "select t.pmtskid, t.tasknum, isnull(t.otaskdesc, 'Not Provided') as 'otaskdesc', " _
            + "isnull(t.taskdesc, 'Not Provided') as 'taskdesc', " _
            + "'Orig' as 'o', 'Rev' as 'r', isnull(c.compnum + ' - ' + c.compdesc, 'None Selected') as compnum " _
            + "from pmtasksarch t left outer join componentsarch c on c.comid = t.comid where subtask = 0 and t.funcid = '" & fuid & "' " _
            + "order by tasknum"
            sqlcnt = "select count(*) from pmtasksarch where subtask = 0 and funcid = '" & fuid & "' and rev = '" & rev & "'"
        End If
        If cnt <> 0 Then
            cnt = tasksg.Scalar(sqlcnt)
            taskcnt.Value = cnt
        Else
            taskcnt.Value = "0"
        End If
        ds = tasksg.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        rptrtasks.DataSource = dt
        Dim i, eint As Integer
        eint = 8
        For i = cnt To eint
            dt.Rows.InsertAt(dt.NewRow(), i)
        Next
        rptrtasks.DataBind()
    End Sub

    Private Sub rptrtasks_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrtasks.ItemCommand
        If e.CommandName = "Select" Then
            Dim tasknum, comid As String
            Try
                tasknum = CType(e.Item.FindControl("lbltn"), LinkButton).Text
            Catch ex As Exception
                tasknum = CType(e.Item.FindControl("lbltnalt"), LinkButton).Text
            End Try
            tl = lbltasklev.Value
            cid = lblcid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            eqid = lbleqid.Value
            chk = lblchk.Value
            fuid = lblfuid.Value
            coid = lblcoid.Value
            typ = lbltyp.Value
            lid = lbllid.Value
            rev = lblrev.Value
            Response.Redirect("pmarchtasks.aspx?start=yes&tl=5&chk=" & chk & "&cid=" & _
            cid + "&fuid=" & fuid & "&comid=" & coid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & _
            "&eqid=" & eqid & "&task=" & tasknum & "&typ=" & typ & "&lid=" & lid & "&rev=" & rev)

        End If
    End Sub
	



    Private Sub rptrtasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtasks.ItemDataBound


        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang343 As Label
                lang343 = CType(e.Item.FindControl("lang343"), Label)
                lang343.Text = axlabs.GetASPXPage("pmarchgrid.aspx", "lang343")
            Catch ex As Exception
            End Try
            Try
                Dim lang344 As Label
                lang344 = CType(e.Item.FindControl("lang344"), Label)
                lang344.Text = axlabs.GetASPXPage("pmarchgrid.aspx", "lang344")
            Catch ex As Exception
            End Try
            Try
                Dim lang345 As Label
                lang345 = CType(e.Item.FindControl("lang345"), Label)
                lang345.Text = axlabs.GetASPXPage("pmarchgrid.aspx", "lang345")
            Catch ex As Exception
            End Try
            Try
                Dim lang346 As Label
                lang346 = CType(e.Item.FindControl("lang346"), Label)
                lang346.Text = axlabs.GetASPXPage("pmarchgrid.aspx", "lang346")
            Catch ex As Exception
            End Try
            Try
                Dim lang347 As Label
                lang347 = CType(e.Item.FindControl("lang347"), Label)
                lang347.Text = axlabs.GetASPXPage("pmarchgrid.aspx", "lang347")
            Catch ex As Exception
            End Try

        End If

    End Sub






    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang343.Text = axlabs.GetASPXPage("pmarchgrid.aspx", "lang343")
        Catch ex As Exception
        End Try
        Try
            lang344.Text = axlabs.GetASPXPage("pmarchgrid.aspx", "lang344")
        Catch ex As Exception
        End Try
        Try
            lang345.Text = axlabs.GetASPXPage("pmarchgrid.aspx", "lang345")
        Catch ex As Exception
        End Try
        Try
            lang346.Text = axlabs.GetASPXPage("pmarchgrid.aspx", "lang346")
        Catch ex As Exception
        End Try
        Try
            lang347.Text = axlabs.GetASPXPage("pmarchgrid.aspx", "lang347")
        Catch ex As Exception
        End Try

    End Sub

End Class
