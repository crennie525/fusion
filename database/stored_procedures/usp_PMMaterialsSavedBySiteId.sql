USE [amgen]
GO
/****** Object:  StoredProcedure [dbo].[usp_PMMaterialsSavedBySiteId]    Script Date: 3/11/2017 8:56:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_PMMaterialsSavedBySiteId]
	@siteId INT = -1,
	@acid INT = -1
AS

-- having a siteid less then zero will return all records. 
-- if siteid is -1, it will return all records grouped by siteid. 

DECLARE @tblSumOrig TABLE (
siteid INT,
eqid INT,
freqstr varchar(50),
freq int,
freqyr decimal(10,2), 
origttime decimal(10,2),
origqty int,
yr_origttime decimal(10,2),
origrdt decimal(10,2),
yr_origrdt decimal(10,2),
opcost decimal(10,2),
yr_opcost decimal(10,2),
otcost decimal(10,2),
yr_otcost decimal(10,2),
olcost decimal(10,2),
yr_olcost decimal(10,2)
)

insert @tblSumOrig (siteid, eqid, freqstr, origttime, origqty, origrdt, opcost, otcost, olcost)
select e.siteid, e.eqid, t1.freq, t1.tottime, t1.skillqty, t1.downtime,

opcost = (
	select isnull(sum(po.total), 0) as 'Parts' from pmoTaskParts po
	left join pmtasks t on t.pmtskid = po.pmtskid 
	where t.eqid = e.eqid 
	AND t.origskillid = t1.skillid and t.origskill is not null and len(t.origskill) > 0
	and t.origskill <> 'select' and isnull(t.taskstatus, '') <> 'Add'),

otcost = (
	select isnull(sum(toc.total), 0) as 'Tools' from pmoTaskTools toc 
	left join pmtasks t on t.pmtskid = toc.pmtskid 
	where t.eqid = e.eqid 
	AND t.origskillid = t1.skillid  and t.origskill is not null and len(t.origskill) > 0
	and t.origskill <> 'select' and isnull(t.taskstatus, '') <> 'Add'),

olcost = (
	select isnull(sum(lo.total), 0) as 'Lubes' from pmoTaskLubes lo 
	left join pmtasks t on t.pmtskid = lo.pmtskid 
	where t.eqid = e.eqid 
	AND t.origskillid = t1.skillid  and t.origskill is not null and len(t.origskill) > 0
	and t.origskill <> 'select' and isnull(t.taskstatus, '') <> 'Add')

from equipment e
left join pmtottime t1 on t1.eqid = e.eqid and t1.freq <> 0
where e.eqid IN (SELECT eqid FROM dbo.Equipment WHERE (siteid = @siteId OR @siteId < 0)
				 AND (acid = @acid OR @acid = -1))
AND usetot = 1

insert @tblSumOrig (siteid, eqid, freqstr, origttime, origqty, origrdt, opcost, otcost, olcost)
select e.siteid, e.eqid, t.origfreq, t.origttime, t.origqty, t.origrdt,

opcost = (
	select isnull(sum(po.total), 0) as 'Parts' from pmoTaskParts po 
	where po.pmtskid = t.pmtskid  
	AND t.origskill is not null and len(t.origskill) > 0
	and t.origskill <> 'select' and isnull(t.taskstatus, '') <> 'Add'),

otcost = (
	select isnull(sum(toc.total), 0) as 'Tools' from pmoTaskTools toc 
	where toc.pmtskid = t.pmtskid 
	AND t.origskill is not null and len(t.origskill) > 0
	and t.origskill <> 'select' and isnull(t.taskstatus, '') <> 'Add'),

olcost = (
	select isnull(sum(lo.total), 0) as 'Lubes' from pmoTaskLubes lo 
	where lo.pmtskid = t.pmtskid  
	AND t.origskill is not null and len(t.origskill) > 0
	and t.origskill <> 'select' and isnull(t.taskstatus, '') <> 'Add')

from equipment e
left  join functions f on f.eqid = e.eqid
left  join pmtasks t on t.funcid = f.func_id
where e.eqid IN (SELECT eqid FROM dbo.Equipment WHERE (siteid = @siteId OR @siteId < 0) 
				 AND (acid = @acid OR @acid = -1))
AND t.origskill is not null and 
len(t.origskill) > 0 and t.origskill <> 'select' 
and isnull(t.taskstatus, '') <> 'Add' 
AND (usetot IS NULL OR usetot = 0)

update @tblSumOrig set freq = freqstr where freqstr is not null and
freqstr <> 'Select' and len(freqstr) <> 0 and freqstr <> 0
update @tblSumOrig set origttime = origttime * origqty
update @tblSumOrig set freq = 365 where freqstr is null
update @tblSumOrig set freq = 365 where freqstr = 'Select'
update @tblSumOrig set freqyr = 365/freq where freq <= 365 and freq > 0
update @tblSumOrig set freqyr = (((10 * 365)/freq) * .1) where freq > 365

update @tblSumOrig set 
yr_origttime = freqyr * origttime,
yr_opcost = freqyr * opcost,
yr_otcost = freqyr * otcost,
yr_olcost = freqyr * olcost

DECLARE @tblSumRev TABLE (
siteid INT,
eqid INT,
freqstr varchar(50),
freq int,
freqyr decimal(10,2), 
ttime decimal(10,2),
qty int,
yr_ttime decimal(10,2),
rdt decimal(10,2),
yr_rdt decimal(10,2),
rpcost decimal(10,2),
yr_rpcost decimal(10,2),
rtcost decimal(10,2),
yr_rtcost decimal(10,2),
rlcost decimal(10,2),
yr_rlcost decimal(10,2),
pmtskid int,
subtask int
) 

insert @tblSumRev (siteid, eqid, subtask, pmtskid, freqstr, ttime, qty, rdt, rpcost, rtcost, rlcost)
select t.siteid, t.eqid, t.subtask, t.pmtskid, t.freq, t.ttime, t.qty, t.rdt,

rpcost = (
	select isnull(sum(pr.total), 0) as 'Parts' from pmTaskParts pr 
	where pr.pmtskid = t.pmtskid 
	AND t.skill is not null and len(t.skill) > 0
	and t.skill <> 'select' and isnull(t.taskstatus, '') <> 'Delete'),
rtcost = (
	select isnull(sum(tr.total), 0) as 'Tools' from pmTaskTools tr 
	where tr.pmtskid = t.pmtskid  
	AND t.skill is not null and len(t.skill) > 0
	and t.skill <> 'select' and isnull(t.taskstatus, '') <> 'Delete'),
rlcost = (
	select isnull(sum(lr.total), 0) as 'Lubes' from pmTaskLubes lr 
	where lr.pmtskid = t.pmtskid  
	AND t.skill is not null and len(t.skill) > 0
	and t.skill <> 'select' and isnull(t.taskstatus, '') <> 'Delete')

from pmtasks t
where t.eqid IN (SELECT eqid FROM dbo.Equipment WHERE (siteid = @siteId OR @siteId < 0) 
				 AND (acid = @acid OR @acid = -1))
AND t.skill is not null and 
len(t.skill) > 0 and t.skill <> 'select' 
and isnull(t.taskstatus, '') <> 'Delete' --and t.freq is not null or len(t.freq) <> 0
and isnull(t.tpmhold, '0') <> 1 ---???

update @tblSumRev set ttime = 0 where subtask <> 0

update @tblSumRev set freq = freqstr where freqstr is not null and
freqstr <> 'Select' and len(freqstr) <> 0 and freqstr <> 0
update @tblSumRev set ttime = ttime * qty
update @tblSumRev set freq = 365 where freqstr is null
update @tblSumRev set freq = 365 where freqstr = 'Select'
update @tblSumRev set freqyr = 365/freq where freq <= 365 and freq > 0
update @tblSumRev set freqyr = (((10 * 365)/freq) * .1) where freq > 365

update @tblSumRev set 
yr_ttime = freqyr * ttime,
yr_rpcost = freqyr * rpcost,
yr_rtcost = freqyr * rtcost,
yr_rlcost = freqyr * rlcost

DECLARE @tblOriginalMaterials TABLE (
siteid INT,
eqid INT,
OriginalMaterials DECIMAL(10,2))

INSERT INTO @tblOriginalMaterials(siteid, eqid, OriginalMaterials)
	SELECT so.siteid, so.eqid, (sum(yr_opcost) + sum(yr_otcost) + sum(yr_olcost))
	FROM @tblSumOrig so
	GROUP BY so.eqid, so.siteid

DECLARE @tblOptimizedMaterials TABLE (
siteid INT,
eqid INT,
OptimizedMaterials DECIMAL(10,2))

INSERT INTO @tblOptimizedMaterials(siteid, eqid, OptimizedMaterials)
	SELECT sr.siteid, sr.eqid, (sum(yr_rpcost) + sum(yr_rtcost) + sum(yr_rlcost))
	FROM @tblSumRev sr
	GROUP BY sr.eqid, sr.siteid

-- return the totals grouped by siteid
IF @siteId = -1 BEGIN

	SELECT (SELECT SiteName FROM Sites WHERE SiteId = original.siteid) AS 'SiteName',
	SUM(original.OriginalMaterials) AS 'OriginalMaterials', SUM(optimized.OptimizedMaterials) AS 'OptimizedMaterials'
	FROM @tblOriginalMaterials original
	JOIN @tblOptimizedMaterials optimized ON optimized.eqid = original.eqid
	JOIN Sites s ON original.siteid = s.siteid
	WHERE original.OriginalMaterials IS NOT NULL
	AND optimized.OptimizedMaterials IS NOT NULL
	AND s.SiteName != 'Practice Site'
	GROUP BY original.siteid
END
ELSE BEGIN

	SELECT '' AS 'SiteName', 
	SUM(original.OriginalMaterials) AS 'OriginalMaterials', SUM(optimized.OptimizedMaterials) AS 'OptimizedMaterials'
	FROM @tblOriginalMaterials original
	JOIN @tblOptimizedMaterials optimized ON optimized.eqid = original.eqid
	JOIN Sites s ON original.siteid = s.siteid
	WHERE original.OriginalMaterials IS NOT NULL
	AND optimized.OptimizedMaterials IS NOT NULL
	AND s.SiteName != 'Practice Site'
END




















