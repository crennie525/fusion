
/****** Object:  StoredProcedure [dbo].[usp_PMHoursSavedBySiteId]    Script Date: 2/27/2017 8:48:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_PMHoursSavedBySiteId]
	@siteId INT = -1,
	@acid INT = -1
AS

-- having a siteid less then zero will return all records. 
-- if siteid is -1, it will return all records grouped by siteid. 

DECLARE @tblSumOrig TABLE (
siteid INT,
eqid INT,
childtot INT
)

--get all the child records total hours
INSERT @tblSumOrig (siteid, eqid, childtot)
SELECT e.siteid, e.eqid, e.childtot
FROM equipment e
WHERE childtot IS NOT NULL AND childtot != 0
AND (siteid = @siteId OR @siteId < 0) 
AND (acid = @acid OR @acid = -1)
AND desig = 2
AND desigmstr IS NOT NULL 
AND desigmstr > 0

declare @tblb table (
siteid INT, eqid int, skillid int, skill varchar(50), skillqty int, freq int, rdid int, rd varchar(50), 
downtime decimal(10,2), meterid int, meter varchar(50), meterunit varchar(50), meterfreq int, maxdays int,
pm varchar(500), tcnt int, tasktot int, downtot int, freqyr decimal(10,2), taskyr decimal(10,2), downyr decimal(10,2), who varchar(50)
)

INSERT into @tblb (siteid, eqid, skillid, skill, skillqty, freq, rdid, rd, meterid, meterfreq, maxdays, pm, tcnt, tasktot, downtot,
meter, meterunit, who)
select e.siteid, p.eqid, p.skillid, p.skill, p.qty, p.freq, p.rdid, p.rd, p.meterid, p.meterfreq, p.maxdays, 
pm = p.skill + '(' + cast(p.qty as varchar(10)) + ')/' + cast(p.freq as varchar(10)) + ' days/' + p.rd,
tcnt = (select count(*) from pmtasks t where t.skillid = p.skillid and t.qty = p.qty and t.rd = p.rd and
t.freq = p.freq and isnull(t.meterid, 0) = isnull(p.meterid, 0) 
and isnull(t.meterfreq, 0) = isnull(p.meterfreq, 0) 
and isnull(t.maxdays, 0) = isnull(p.maxdays, 0)
and t.eqid = p.eqid), sum(p.ttime), sum(p.rdt), m.meter, m.unit, 'rev' 
from pmtasks p 
JOIN equipment e ON p.eqid = e.eqid
left join meters m on m.meterid = p.meterid
where (e.siteid = @siteId OR @siteId < 0) 
AND (e.acid = @acid OR @acid = -1) 
AND desig IN (0, 1, 3)
AND p.skillid is not null and p.freq is not null and p.rd is not null and isnull(p.taskstatus, '') <> 'Delete'
and p.freq <> 0 
group by e.siteid, p.eqid, p.skillid, p.skill, p.qty, p.freq, p.rdid, p.rd, p.meterid, p.meterfreq, p.maxdays, m.meter, m.unit 
order by p.skill, p.qty, p.freq, p.rd

update @tblb set freqyr = 365/freq where freq <= 365 and freq <> 0
update @tblb set freqyr = (((10 * 365)/freq) * .1) where freq > 365 and freq <> 0
update @tblb set taskyr = freqyr * (skillqty * tasktot)
update @tblb set downyr = freqyr * downtot

DECLARE @tblOptimizedHours TABLE (
siteid INT,
eqid INT,
PMHours DECIMAL(10,2),
ChildrenCount INT,
OptimizedHours DECIMAL(10,2))

--get the master optimized hours
INSERT INTO @tblOptimizedHours(siteid, eqid, PMHours)
	SELECT siteid, eqid, cast(sum(taskyr)/60 as decimal(10,2))
	FROM @tblb 
	GROUP BY eqid, siteid

DECLARE @tblOptimizedChildren TABLE (
eqid INT,
ChildrenCount INT)

--get the number of children for the master
INSERT INTO @tblOptimizedChildren(eqid, ChildrenCount)
	SELECT oh.eqid, COUNT(*)
	FROM @tblOptimizedHours oh
	JOIN equipment e ON oh.eqid = e.desigmstr
	GROUP BY oh.eqid

UPDATE @tblOptimizedHours
SET oh.ChildrenCount = oc.ChildrenCount
FROM @tblOptimizedHours oh
JOIN @tblOptimizedChildren oc ON oh.eqid = oc.eqid

--remove any masters that don't have children
DELETE FROM @tblOptimizedHours
WHERE ChildrenCount IS NULL

--the total hours will be the master hours * the children count
UPDATE @tblOptimizedHours
SET OptimizedHours = PMHours * ChildrenCount

DECLARE @tblOriginalSummary TABLE (
siteid INT,
OriginalHours DECIMAL(10,2))

--get the original hour totals
INSERT INTO @tblOriginalSummary(siteid, OriginalHours)
SELECT siteid, SUM(childtot)
FROM @tblSumOrig
GROUP BY siteid

DECLARE @tblOptimizedSummary TABLE (
siteid INT,
OptimizedHours DECIMAL(10,2))

--get the optimized hour totals
INSERT INTO @tblOptimizedSummary(siteid, OptimizedHours)
SELECT siteid, SUM(OptimizedHours)
FROM @tblOptimizedHours
GROUP BY siteid

-- return the totals grouped by siteid
IF @siteId = -1 BEGIN

	SELECT 
		s.siteName,
		OriginalHours = ISNULL(original.OriginalHours, 0), 
		OptimizedHours = ISNULL(optimized.OptimizedHours, 0)
	FROM Sites s 
	LEFT JOIN @tblOriginalSummary original ON s.siteid = original.siteid
	LEFT JOIN @tblOptimizedSummary optimized ON optimized.siteid = original.siteid
	WHERE s.SiteName != 'Practice Site'
END
ELSE BEGIN

	SELECT '' AS 'SiteName',
	SUM(original.OriginalHours) AS 'OriginalHours', SUM(optimized.OptimizedHours) AS 'OptimizedHours'
	FROM @tblOriginalSummary original
	JOIN @tblOptimizedSummary optimized ON optimized.siteid = original.siteid
	JOIN Sites s ON original.siteid = s.siteid
	WHERE s.SiteName != 'Practice Site'
END








































