Imports System.Data.SqlClient

'********************************************************
'*
'********************************************************



Public Class jobplanmain2
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim job As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim uid, username, islabor, isplanner, issuper, Logged_In, ro, ms, appstr As String
    Dim start, jid, wo, jpref, plnr, plnrid, typ, sid, nid, stat, did, clid, chk, eqid, lid, fuid, coid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifinv As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("JobPlanMain.aspx?jump=no")
        Catch ex As Exception

        End Try
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Dim Login As String
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            Try
                username = HttpContext.Current.Session("username").ToString()
            Catch ex As Exception

            End Try
            If start = "jp" Then
                jid = Request.QueryString("jid").ToString
                ifinv.Attributes.Add("src", "jobplan2.aspx?start=jp&jid=" + jid + "&username=" + username + "&date=" + Now)
            ElseIf start = "st" Then
                jid = Request.QueryString("jid").ToString
                ifinv.Attributes.Add("src", "jobplan2.aspx?start=st&jid=" + jid + "&username=" + username + "&date=" + Now)
            ElseIf start = "rq" Then
                wo = Request.QueryString("wo").ToString
                jid = Request.QueryString("jid").ToString
                jpref = Request.QueryString("jpref").ToString
                plnr = Request.QueryString("plnr").ToString
                plnrid = Request.QueryString("plnrid").ToString
                ifinv.Attributes.Add("src", "jobplan2.aspx?start=rq&jid=" + jid + "&username=" + username + "&wo=" + wo + "&jpref=" + jpref + "&plnr=" + plnr + "&plnrid=" + plnrid + "&date=" + Now)
            ElseIf start = "wo" Then
                jid = Request.QueryString("jid").ToString
                wo = Request.QueryString("wo").ToString
                sid = Request.QueryString("sid").ToString
                uid = Request.QueryString("uid").ToString
                username = Request.QueryString("usrname").ToString
                islabor = Request.QueryString("islabor").ToString
                issuper = Request.QueryString("issuper").ToString
                isplanner = Request.QueryString("isplanner").ToString
                Logged_In = Request.QueryString("Logged_In").ToString
                ro = Request.QueryString("ro").ToString
                ms = Request.QueryString("ms").ToString
                appstr = Request.QueryString("appstr").ToString
                GetWo(wo, sid, start, uid, username, islabor, issuper, isplanner, Logged_In, ro, ms, appstr)

            Else
                jid = "0"
                ifinv.Attributes.Add("src", "jobplan2.aspx?start=no&jid=" + jid + "&date=" + Now)
            End If
        End If
    End Sub
    Private Sub GetWo(ByVal wo As String, ByVal sid As String, ByVal start As String, ByVal uid As String, _
    ByVal username As String, ByVal islabor As String, ByVal issuper As String, ByVal isplanner As String, _
    ByVal Logged_In As String, ByVal ro As String, ByVal ms As String, ByVal appstr As String)
        'typ = Request.QueryString("typ").ToString
        'chk = Request.QueryString("chk").ToString

        'stat = Request.QueryString("stat").ToString
        'did = Request.QueryString("did").ToString
        'clid = Request.QueryString("clid").ToString
        'eqid = Request.QueryString("eqid").ToString
        'nid = Request.QueryString("nid").ToString
        'fuid = Request.QueryString("fuid").ToString
        'coid = Request.QueryString("coid").ToString
        'lid = Request.QueryString("lid").ToString
        sql = "select deptid, cellid, status, eqid, ncid, funcid, comid, locid from workorder where wonum = '" & wo & "'"
        job.Open()
        dr = job.GetRdrData(sql)
        While dr.Read
            did = dr.Item("deptid").ToString
            clid = dr.Item("cellid").ToString
            stat = dr.Item("status").ToString
            eqid = dr.Item("eqid").ToString
            nid = dr.Item("ncid").ToString
            fuid = dr.Item("funcid").ToString
            coid = dr.Item("comid").ToString
            lid = dr.Item("locid").ToString
        End While
        dr.Close()
        job.Dispose()
        If clid <> "" Then
            chk = "yes"
        Else
            chk = "no"
        End If
        typ = ""
        Dim tst As String = "jobplan2.aspx?start=wo&jid=" + jid + "&wo=" + wo + "&typ=" + typ + "&sid=" + sid + "&stat=" + stat + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eqid + "&nid=" + nid + "&fuid=" + fuid + "&coid=" + coid + "&lid=" + lid + "&date=" + Now
        ifinv.Attributes.Add("src", "jobplan2.aspx?start=wo&jid=" + jid + "&wo=" + wo + "&typ=" + typ + "&sid=" + sid + "&stat=" + stat + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eqid + "&nid=" + nid + "&fuid=" + fuid + "&coid=" + coid + "&lid=" + lid + "&date=" + Now + "&uid=" + uid + "&username=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr)
    End Sub
End Class
