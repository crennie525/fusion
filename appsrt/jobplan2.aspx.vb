﻿Imports System.Data.SqlClient
Public Class jobplan2
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim sid, cid, start, jid, sql, username, eqid, wo, lid, did, clid, loctype, chk, fuid, coid, ncid, stat, isecd As String
    Dim mu As New mmenu_utils_a
    Dim Filter, filt, dt, df, tl, val, tli, jpref, plnr, plnrid, usr, wojtid As String
    Dim uid, islabor, isplanner, issuper As String
    Dim SubVal As String
    Dim ds As DataSet
    Dim dslev As DataSet
    Dim dr As SqlDataReader
    Dim gtasks As New Utilities
    Dim Login, ro, rostr, Logged_In, ms, appstr As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetFSOVLIBS()
        GetDGLangs()
        GetFSLangs()
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()

        If Not IsPostBack Then
            isecd = mu.ECD
            lblisecd.Value = isecd
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                imgadd.Attributes.Add("onclick", "")
                imgsav.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                imgsav.Attributes.Add("onclick", "")
                imgsav2.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                imgsav2.Attributes.Add("onclick", "")
                imgaddtask.Attributes.Add("src", "../images/appbuttons/bgbuttons/addtaskdis.gif")
                imgaddtask.Attributes.Add("onclick", "")

            Else
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = gtasks.CheckROS(rostr, "jp")
                    lblro.Value = ro
                    If ro = "1" Then
                        imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                        imgadd.Attributes.Add("onclick", "")
                        imgsav.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                        imgsav.Attributes.Add("onclick", "")
                        imgsav2.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                        imgsav2.Attributes.Add("onclick", "")
                        imgaddtask.Attributes.Add("src", "../images/appbuttons/bgbuttons/addtaskdis.gif")
                        imgaddtask.Attributes.Add("onclick", "")
                    End If
                End If
            End If

            gtasks.Open()
            start = Request.QueryString("start").ToString
            If start = "jp" Then
                jid = Request.QueryString("jid").ToString
                lbljid.Value = jid
                lblwho.Value = "jp"
                username = Request.QueryString("username").ToString
                lblusername.Value = username
            ElseIf start = "st" Then
                jid = Request.QueryString("jid").ToString
                lbljid.Value = jid
                lblwho.Value = "st"
                username = Request.QueryString("username").ToString
                lblusername.Value = username
            ElseIf start = "rq" Then
                lblwho.Value = "rq"
                wo = Request.QueryString("wo").ToString
                lblwo.Value = wo
                If wo <> "" Then
                    tdwo.InnerHtml = "Work Order# (Reference):&nbsp;&nbsp;&nbsp;" & wo
                End If
                jid = Request.QueryString("jid").ToString
                lbljid.Value = jid
                jpref = Request.QueryString("jpref").ToString
                lbljpref.Value = jpref
                tdref.InnerHtml = jpref
                plnr = Request.QueryString("plnr").ToString
                lblplanner.Value = plnr
                plnrid = Request.QueryString("plnrid").ToString
                lblplnrid.Value = plnrid
                If plnr <> "" Then
                    tdplnr.InnerHtml = plnr
                End If
                If jid = "" And Request.QueryString("wo").ToString <> "" Then
                    GetWoDetails(Request.QueryString("wo").ToString)
                    lblwho.Value = "rqw"
                End If
                username = Request.QueryString("username").ToString
                lblusername.Value = username
            ElseIf start = "wo" Then
                jid = Request.QueryString("jid").ToString
                lbljid.Value = jid
                lblwho.Value = "wo"
                wo = Request.QueryString("wo").ToString
                lblwo.Value = wo
                lblret.Value = wo
                If wo <> "" Then
                    tdwo.InnerHtml = "Work Order# (Reference):&nbsp;&nbsp;&nbsp;" & wo
                End If
                uid = Request.QueryString("uid").ToString
                lbluid.Value = uid
                username = Request.QueryString("username").ToString
                lblusername.Value = username
                islabor = Request.QueryString("islabor").ToString
                lblislabor.Value = islabor
                issuper = Request.QueryString("issuper").ToString
                lblissuper.Value = issuper
                isplanner = Request.QueryString("isplanner").ToString
                lblisplanner.Value = isplanner
                Logged_In = Request.QueryString("Logged_In").ToString
                lblLogged_In.Value = Logged_In
                ro = Request.QueryString("ro").ToString
                lblro.Value = ro
                ms = Request.QueryString("ms").ToString
                lblms.Value = ms
                appstr = Request.QueryString("appstr").ToString
                lblappstr.Value = appstr
                lblret.Value += "," & Request.QueryString("typ").ToString
                lblret.Value += "," & Request.QueryString("sid").ToString
                lblret.Value += "," & Request.QueryString("did").ToString
                lblret.Value += "," & Request.QueryString("clid").ToString
                lblret.Value += "," & Request.QueryString("chk").ToString
                lblret.Value += "," & Request.QueryString("eqid").ToString
                lblret.Value += "," & Request.QueryString("fuid").ToString
                lblret.Value += "," & Request.QueryString("coid").ToString
                lblret.Value += "," & Request.QueryString("lid").ToString
                lblret.Value += "," & Request.QueryString("nid").ToString
                lblret.Value += "," & Request.QueryString("stat").ToString

                lblret.Value += "," & Request.QueryString("uid").ToString
                lblret.Value += "," & Request.QueryString("username").ToString
                lblret.Value += "," & Request.QueryString("islabor").ToString
                lblret.Value += "," & Request.QueryString("issuper").ToString
                lblret.Value += "," & Request.QueryString("isplanner").ToString
                lblret.Value += "," & Logged_In
                lblret.Value += "," & ro
                lblret.Value += "," & ms
                lblret.Value += "," & appstr + ","

                lblwo.Value = Request.QueryString("wo").ToString
                lbldept.Value = Request.QueryString("did").ToString
                lblclid.Value = Request.QueryString("clid").ToString
                lblchk.Value = Request.QueryString("chk").ToString
                lbleqid.Value = Request.QueryString("eqid").ToString
                lblncid.Value = Request.QueryString("nid").ToString
                lblfuid.Value = Request.QueryString("fuid").ToString
                lblcoid.Value = Request.QueryString("coid").ToString
                lbllid.Value = Request.QueryString("lid").ToString
                GetWoDetails2(wo)

            Else
                jid = "0"
                lbljid.Value = jid
                lblwho.Value = "jp"
            End If
            lbleditmode.Value = "0"
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            cid = HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            CheckStuff()
            GetLists()
            'PopDepts(sid)
            'PopEq(sid)
            'PopNC(sid)
            BindHead(jid)
            If start <> "wo" Then
                GetDetails(jid)
            End If

            BindGrid(jid)
            lbltool.Value = "no"
            lblpart.Value = "no"
            lbllube.Value = "no"
            lblnote.Value = "no"
            gtasks.Dispose()
        Else
            If Request.Form("lblsubmit") = "addjob" Then
                lblsubmit.Value = ""
                gtasks.Open()
                AddPlan()
                jid = lbljid.Value
                BindHead(jid)
                gtasks.Dispose()
            ElseIf Request.Form("lblsubmit") = "getjob" Then
                lblsubmit.Value = ""
                gtasks.Open()
                jid = lbljid.Value
                BindHead(jid)
                BindGrid(jid)
                lbltool.Value = "no"
                lblpart.Value = "no"
                lbllube.Value = "no"
                lblnote.Value = "no"
                gtasks.Dispose()
            ElseIf Request.Form("lblsubmit") = "refreshit" Then
                lblsubmit.Value = ""
                gtasks.Open()
                jid = "0"
                lbljid.Value = jid
                BindHead(jid)
                BindGrid(jid)
                lbltool.Value = "no"
                lblpart.Value = "no"
                lbllube.Value = "no"
                lblnote.Value = "no"
                gtasks.Dispose()
            ElseIf Request.Form("lblsubmit") = "savjob" Then
                lblsubmit.Value = ""
                gtasks.Open()
                SavePlan()
                jid = lbljid.Value
                BindHead(jid)
                gtasks.Dispose()
            ElseIf Request.Form("lblsubmit") = "dojob" Then
                lblsubmit.Value = ""
                gtasks.Open()
                DoJob()
                gtasks.Dispose()
            ElseIf Request.Form("lblsubmit") = "savedets" Then
                lblsubmit.Value = ""
                gtasks.Open()
                SaveDetails()
                jid = lbljid.Value
                GetDetails(jid)
                gtasks.Dispose()
            ElseIf Request.Form("lblsubmit") = "addtask" Then
                lblsubmit.Value = ""
                gtasks.Open()
                AddTask()
                gtasks.Dispose()
            ElseIf Request.Form("lblsubmit") = "upfail" Then
                lblsubmit.Value = ""
                dgtasks.EditItemIndex = lblitemindex.Value
                gtasks.Open()
                jid = lbljid.Value
                BindGrid(jid)

                gtasks.Dispose()
            ElseIf Request.Form("lblsubmit") = "addcomp" Then
                lblsubmit.Value = ""
                gtasks.Open()
                AddCompOnly()
                jid = lbljid.Value
                BindHead(jid)
                GetDetails(jid)
                BindGrid(jid)
                gtasks.Dispose()
            End If
            If Request.Form("lblpchk") = "loc" Then
                lblpchk.Value = ""
                gtasks.Open()
                lid = lbllid.Value
                'PopEq(lid, "l")
                gtasks.Dispose()
            End If
            wo = lblwo.Value
            If wo <> "" Then
                tdwo.InnerHtml = "Work Order# (Reference):&nbsp;&nbsp;&nbsp;" & wo
            End If
        End If
    End Sub
    Private Sub AddCompOnly()
        Dim coid, comp As String
        coid = lblcomponly.Value
        comp = txtcomponly.Text
        jid = lbljid.Value
        wo = lblwo.Value
        sql = "update pmjobplans set deptid = null, cellid = null, eqid = null, funcid = null, " _
        + "comid = '" & coid & "' where jpid = '" & jid & "'"
        gtasks.Update(sql)
        If wo <> "" Then
            sql = "update wojobplans set deptid = null, cellid = null, eqid = null, funcid = null, " _
            + "comid = '" & coid & "' where jpid = '" & jid & "'"
            gtasks.Update(sql)
        End If
        lbldept.Value = ""
        lblclid.Value = ""
        lblchk.Value = ""
        lbleqid.Value = ""
        lblncid.Value = ""
        lblfuid.Value = ""
        lbllid.Value = ""
    End Sub
    Function PopulateCompFM(ByVal comp As String, ByVal ttid As String)

        If comp <> "0" And comp <> "" Then
            sql = "select failid, failuremode " _
            + "from componentfailmodes where comid = '" & comp & "' and " _
            + "failid not in (select failid from jpfailmodes where pmtskid = '" & ttid & "')"
            sql = "usp_getcfall_tsknajp '" & comp & "','" & ttid & "'"
            dslev = gtasks.GetDSData(sql)
            Return dslev
        End If

    End Function
    Function PopulateTaskFM(ByVal comp As String, ByVal ttid As String)
        'If comp <> "0" Then
        sql = "select * from jpfailmodes where pmtskid = '" & ttid & "'"
        sql = "usp_getcfall_tskjp '" & comp & "','" & ttid & "'"
        dslev = gtasks.GetDSData(sql)
        Return dslev
        'End If
    End Function
    Private Sub GetWoDetails2(ByVal wonum As String)
        sql = "select w.*, d.dept_line, cl.cell_name, f.func, c.compnum, wa.workarea, l.longdesc " _
      + "from workorder w " _
      + "left join dept d on d.dept_id = w.deptid " _
      + "left join cells cl on cl.cellid = w.cellid " _
      + "left join functions f on f.func_id = w.funcid " _
      + "left join components c on c.comid = w.comid " _
      + "left join workareas wa on wa.waid = w.waid " _
      + "left join wolongdesc l on l.wonum = w.wonum " _
      + "where w.wonum = '" & wonum & "'"
        dr = gtasks.GetRdrData(sql)
        Dim eid, eqnum, nid, sid, lid, loc, did, dept, clid, cell, cid, comp, fid, func As String
        Dim ld, ch, skill, skillid As String
        While dr.Read
            eid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            nid = dr.Item("ncid").ToString
            sid = dr.Item("siteid").ToString
            lid = dr.Item("locid").ToString
            loc = dr.Item("location").ToString
            did = dr.Item("deptid").ToString
            dept = dr.Item("dept_line").ToString
            clid = dr.Item("cellid").ToString
            cell = dr.Item("cell_name").ToString
            cid = dr.Item("comid").ToString
            comp = dr.Item("compnum").ToString
            fid = dr.Item("funcid").ToString
            func = dr.Item("func").ToString
            ld = dr.Item("ld").ToString
            If ld = "" Then
                If lid <> "" Then
                    ld = "L"
                ElseIf did <> "" Then
                    ld = "D"
                End If
            End If
            ch = dr.Item("chargenum").ToString
            skill = dr.Item("skill").ToString
            skillid = dr.Item("skillid").ToString
            If clid <> "" Then
                chk = "yes"
            Else
                chk = "no"
            End If
            txtlead.Text = dr.Item("leadcraft").ToString
            txtsup.Text = dr.Item("supervisor").ToString
            lbllead.Value = dr.Item("leadcraftid").ToString
            lblsup.Value = dr.Item("superid").ToString
        End While
        dr.Close()
        lblld.Value = ld
        trdepts.Attributes.Add("class", "details")
        trlocs3.Attributes.Add("class", "details")

        If ld = "L" Then
            lbltyp.Value = "locs"
            trdepts.Attributes.Add("class", "details")
            trlocs3.Attributes.Add("class", "view")
            lbllid.Value = lid
            lblloc.Value = loc
            tdloc3.InnerHtml = loc
            lbleq.Value = eqnum
            lbleqid.Value = eqid
            tdeq3.InnerHtml = eqnum
            lblfuid.Value = fuid
            lblfu.Value = func
            tdfu3.InnerHtml = func
            lblcomid.Value = cid
            lblcoid.Value = cid
            lblcomp.Value = comp
            tdco3.InnerHtml = comp

            tdcharge3.InnerHtml = ch
        ElseIf ld = "D" Then
            lbltyp.Value = "depts"
            trdepts.Attributes.Add("class", "view")
            trlocs3.Attributes.Add("class", "details")
            lbllid.Value = lid
            lblloc.Value = loc
            'tdloc.InnerHtml = loc
            lbleq.Value = eqnum
            lbleqid.Value = eqid
            tdeq.InnerHtml = eqnum
            '*
            lbldid.Value = did
            lbldept.Value = dept
            tddept.InnerHtml = dept
            lblclid.Value = clid
            lblcell.Value = cell
            tdcell.InnerHtml = cell
            lblfuid.Value = fuid
            lblfu.Value = func
            tdfu.InnerHtml = func
            lblcomid.Value = cid
            lblcoid.Value = cid
            lblcomp.Value = comp
            tdco.InnerHtml = comp
            lblncid.Value = ncid
            'lblnc.Value = nc
            'tdmisc.InnerHtml = nc
            lbllid.Value = lid
            lblloc.Value = loc
            'tdloc.InnerHtml = loc
            lbltyp.Value = "depts"
            '*

            tdcharge1.InnerHtml = ch

        End If
    End Sub
    Private Sub GetWoDetails(ByVal wonum As String)

        sql = "select deptid, cellid, locid, eqid, ncid, funcid, comid from workorder where wonum = '" & wonum & "'"

        dr = gtasks.GetRdrData(sql)
        While dr.Read
            lbldept.Value = dr.Item("deptid").ToString
            lblclid.Value = dr.Item("cellid").ToString
            If dr.Item("cellid").ToString <> "" Or dr.Item("cellid").ToString <> "0" Then
                lblchk.Value = "yes"
            Else
                lblchk.Value = "no"
            End If
            lbleqid.Value = dr.Item("eqid").ToString
            lblncid.Value = dr.Item("ncid").ToString
            lblfuid.Value = dr.Item("funcid").ToString
            lblcoid.Value = dr.Item("comid").ToString
            lbllid.Value = dr.Item("locid").ToString
        End While
        dr.Close()


    End Sub
    Private Sub SaveWODetails()
        Dim did, clid, lid, eqid, ncid, fuid, comid As String
        jid = lbljid.Value
        did = lbldept.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        ncid = lblncid.Value
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToInt64(ncid)
        Catch ex As Exception
            ncid = "0"
        End Try
        fuid = lblfuid.Value
        comid = lblcoid.Value
        lid = lbllid.Value
        wo = lblwo.Value

        sql = "update pmjobplans set deptid = '" & did & "', cellid = '" & clid & "', eqid = '" & eqid & "', " _
        + "ncid = '" & ncid & "', funcid = '" & fuid & "', comid = '" & comid & "', locid = '" & lid & "' " _
        + "where jpid = '" & jid & "'; " _
        + "update wojobplans set deptid = '" & did & "', cellid = '" & clid & "', eqid = '" & eqid & "', " _
        + "ncid = '" & ncid & "', funcid = '" & fuid & "', comid = '" & comid & "', locid = '" & lid & "' " _
        + "where jpid = '" & jid & "' and wonum = '" & wo & "'"
        gtasks.Update(sql)

    End Sub
    Private Sub GetDetails(ByVal jid As String)
        Dim who As String = lblwho.Value
        Dim componly, componlyid As String
        sql = "select skillall, skillid, skill from pmjobplans where jpid = '" & jid & "'"
        sql = "select w.*, d.dept_line, cl.cell_name, f.func, c.compnum, e.eqnum, l.location, p.userid, p.username, r.status " _
        + "from pmjobplans w " _
        + "left join dept d on d.dept_id = w.deptid " _
        + "left join cells cl on cl.cellid = w.cellid " _
        + "left join functions f on f.func_id = w.funcid " _
        + "left join components c on c.comid = w.comid " _
        + "left join pmlocations l on l.locid = w.locid " _
        + "left join equipment e on e.eqid = w.eqid " _
        + "left join pmsysusers p on p.userid = w.plnrid " _
        + "left join pmjpref r on r.jpref = w.jpref " _
        + "where w.jpid = '" & jid & "'"
        '
        Dim skall, ska, ski As String
        dr = gtasks.GetRdrData(sql)
        Dim nid, loc, dept, cell, eqnum, func, ch, skill, skillid, ld, owo, jpstat As String
        While dr.Read
            skall = dr.Item("skillall").ToString
            ska = dr.Item("skill").ToString
            ski = dr.Item("skillid").ToString
            tdskill.InnerHtml = ska

            componlyid = dr.Item("comid").ToString
            componly = dr.Item("compnum").ToString

            nid = dr.Item("ncid").ToString
            sid = dr.Item("siteid").ToString
            lid = dr.Item("locid").ToString
            loc = dr.Item("location").ToString
            did = dr.Item("deptid").ToString
            dept = dr.Item("dept_line").ToString
            clid = dr.Item("cellid").ToString
            cell = dr.Item("cell_name").ToString
            lbldept.Value = did
            lblclid.Value = clid
            lblncid.Value = ncid
            lblloc.Value = lid
            lblsid.Value = sid

            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            lbleqid.Value = eqid

            fuid = dr.Item("funcid").ToString
            func = dr.Item("func").ToString
            lblfuid.Value = fuid

            ch = dr.Item("chargenum").ToString

            If clid <> "" Then
                chk = "yes"
            Else
                chk = "no"
            End If

            txtlead.Text = dr.Item("leadcraft").ToString
            txtsup.Text = dr.Item("supervisor").ToString

            ld = dr.Item("ld").ToString


            lblplnrid.Value = dr.Item("userid").ToString
            lblplanner.Value = dr.Item("username").ToString
            tdplnr.InnerHtml = dr.Item("username").ToString
            lbljpref.Value = dr.Item("jpref").ToString
            tdref.InnerHtml = dr.Item("jpref").ToString
            owo = dr.Item("wonum").ToString
            jpstat = dr.Item("status").ToString
        End While
        dr.Close()
        If skall = "1" Then
            cball.Checked = True
            lblskillallid.Value = ski
            lblskillall.Value = ska
        Else
            cball.Checked = False
            lblskillallid.Value = ""
            lblskillall.Value = ""
        End If
        tdskill.InnerHtml = ska
        If ld = "" Then
            If did = "" And lid <> "" Then
                ld = "L"
            ElseIf did <> "" Then
                ld = "D"
            End If
            If ld <> "" Then
                sql = "update pmjobplans set ld = '" & ld & "' where jpid = '" & jid & "'"
                gtasks.Update(sql)
            End If
        End If
        lblld.Value = ld
        trdepts.Attributes.Add("class", "details")
        trlocs3.Attributes.Add("class", "details")

        If ld = "L" Then
            lbltyp.Value = "locs"
            trdepts.Attributes.Add("class", "details")
            trlocs3.Attributes.Add("class", "view")
            lbllid.Value = lid
            lblloc.Value = loc
            tdloc3.InnerHtml = loc
            lbleq.Value = eqnum
            lbleqid.Value = eqid
            tdeq3.InnerHtml = eqnum
            lblfuid.Value = fuid
            lblfu.Value = func
            tdfu3.InnerHtml = func
            lblcomid.Value = componlyid
            lblcoid.Value = componlyid
            lblcomp.Value = componly
            tdco3.InnerHtml = componly

            tdcharge3.InnerHtml = ch
        ElseIf ld = "D" Then
            lbltyp.Value = "depts"
            trdepts.Attributes.Add("class", "view")
            trlocs3.Attributes.Add("class", "details")
            lbllid.Value = lid
            lblloc.Value = loc
            'tdloc.InnerHtml = loc
            lbleq.Value = eqnum
            lbleqid.Value = eqid
            tdeq.InnerHtml = eqnum
            '*
            lbldid.Value = did
            lbldept.Value = dept
            tddept.InnerHtml = dept
            lblclid.Value = clid
            lblcell.Value = cell
            tdcell.InnerHtml = cell
            lblfuid.Value = fuid
            lblfu.Value = func
            tdfu.InnerHtml = func
            lblcomid.Value = componlyid
            lblcoid.Value = componlyid
            lblcomp.Value = componly
            tdco.InnerHtml = componly
            lblncid.Value = ncid
            'lblnc.Value = nc
            'tdmisc.InnerHtml = nc
            lbllid.Value = lid
            lblloc.Value = loc
            'tdloc.InnerHtml = loc
            lbltyp.Value = "depts"
            '*

            tdcharge1.InnerHtml = ch

        End If

        If who = "wo" Or who = "rqw" Then
            If componly <> "" Then
                eqid = lbleqid.Value
                If eqid = "" Then
                    If componlyid <> "" Then
                        txtcomponly.Text = componly
                        lblcoid.Value = componlyid
                    End If
                End If
            End If
            wo = lblwo.Value
            If wo <> "" Then
                tdwo.InnerHtml = "Work Order# (Reference):&nbsp;&nbsp;&nbsp;" & wo
            End If
            sql = "select leadcraft, supervisor, skillid from workorder where wonum = '" & wo & "'"
            dr = gtasks.GetRdrData(sql)
            While dr.Read
                txtlead.Text = dr.Item("leadcraft").ToString
                txtsup.Text = dr.Item("supervisor").ToString
                Try
                    'ddskillmain.SelectedValue = dr.Item("skillid").ToString
                Catch ex As Exception

                End Try
            End While
            dr.Close()
            If txtlead.Text <> "" Then
                'txtlead.Enabled = False
            End If
            If txtsup.Text <> "" Then
                'txtsup.Enabled = False
            End If

        ElseIf who = "jp" Or who = "rq" Then
            If jid <> "0" Then

                If lbljpref.Value = "" Then
                    imgplanner.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
                    imgplanner.Attributes.Add("onclick", "")
                    ddjpstat.Enabled = False
                Else
                    imgplanner.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifier.gif")
                    imgplanner.Attributes.Add("onclick", "getplanner('plan');")
                    Try
                        ddjpstat.SelectedValue = jpstat
                    Catch ex As Exception

                    End Try

                    If jpstat = "New" Or jpstat = "In Progress" Then
                        'lblwo.Value = owo
                        ddjpstat.Enabled = True
                    Else
                        lblwo.Value = ""
                        ddjpstat.Enabled = False
                    End If
                End If

            End If

            wo = lblwo.Value
            If wo <> "" Then
                tdwo.InnerHtml = "Work Order# (Reference):&nbsp;&nbsp;&nbsp;" & wo
                'dddepts.Enabled = False
                'ddcells.Enabled = False
                'ddeq.Enabled = False
                'ddnc.Enabled = False
                'ddfu.Enabled = False
                'ddcomp.Enabled = False
                'imgloc.Attributes.Add("class", "details")
            End If
        End If
    End Sub
    Private Sub GetLists()
        cid = lblcid.Value


        sid = lblsid.Value


        Dim who As String = lblwho.Value
        If who = "wo" Then
            sql = "select jptypeid, description from jobplantypes where scheduled = 0"
        Else
            sql = "select jptypeid from jobplantypes where scheduled = 1"
            dr = gtasks.GetRdrData(sql)
            'Dim test As String
            While dr.Read
                lblsched.Value = dr.Item("jptypeid").ToString & ","
                'test += dr.Item("jptypeid").ToString & ","
            End While
            'lblsched.Value = test
            dr.Close()
            sql = "select jptypeid, description from jobplantypes"
        End If
        dr = gtasks.GetRdrData(sql)
        ddtyp.DataSource = dr
        ddtyp.DataTextField = "description"
        ddtyp.DataValueField = "jptypeid"
        ddtyp.DataBind()
        dr.Close()
        ddtyp.Items.Insert(0, New ListItem("Select"))
        ddtyp.Items(0).Value = 0
    End Sub
    Private Sub CheckStuff()
        sid = lblsid.Value
        Dim scnt As Integer
        sql = "select count(*) from pmsiteskills where siteid = '" & sid & "'"
        scnt = gtasks.Scalar(sql)
        If scnt > 0 Then
            lblsskills.Value = "yes"
        Else
            lblsskills.Value = "no"
        End If

    End Sub
    Private Sub DoJob()
        Dim ji, js, skm As String
        'ddskillmain.SelectedIndex
        If skm <> 0 Then
            'ji = ddskillmain.SelectedValue.ToString
            lblskillallid.Value = ji
            'js = ddskillmain.SelectedItem.ToString
            lblskillall.Value = js
            jid = lbljid.Value
            Dim sskills As String = lblsskills.Value

            If cball.Checked = True Then
                If sskills = "yes" Then

                Else

                End If
                sql = "update pmjobtasks set skillid = '" & ji & "', skill = '" & js & "' where jpid = '" & jid & "'; " _
                + "update pmjobplans set skillall = 1 where jpid = '" & jid & "'; " _
                + "update pmjobtasks set skillindex = ( " _
                + "isnull((select skillindex from pmskills s where " _
                + "s.skillid = '" & ji & "'), 0)) where jpid = '" & jid & "' and skillid is not null"
                dgtasks.Columns(4).Visible = False
                'dgtasks.Columns(5).Visible = False
                'dgtasks.Columns(6).Visible = False

            Else
                sql = "update pmjobplans set skillall = 0 where jpid = '" & jid & "'"
                dgtasks.Columns(4).Visible = True
                'dgtasks.Columns(5).Visible = True
                'dgtasks.Columns(6).Visible = True
            End If

            gtasks.Update(sql)
            BindGrid(jid)
        End If

    End Sub
    Private Sub SaveDetails()
        Dim lead, sup, sk, ski, sd, nd, ch, jpr, plnr, stat, wo, typ As String
        jid = lbljid.Value
        lead = txtlead.Text
        sup = txtsup.Text
        'sd = txts.Text
        'nd = txtn.Text
        'ch = txtcharge.Text
        'ski = ddskillmain.SelectedValue.ToString
        'sk = ddskillmain.SelectedItem.ToString
        ski = lblskillid.Value
        sk = lblskill.Value
        jpr = lbljpref.Value
        plnr = lblplnrid.Value
        stat = ddjpstat.SelectedValue
        wo = lblwo.Value
        typ = lblwho.Value
        sql = "usp_updatejp '" & lead & "','" & sup & "','" & sk & "','" & ski & "','" & sd & "','" & nd & "','" & jid & "','" & ch & "','" & plnr & "','" & wo & "','" & stat & "','" & typ & "'"
        gtasks.Update(sql)
    End Sub
    Private Sub SavePlan()
        Dim rt, rtd, typ, s, stat As String
        jid = lbljid.Value
        rt = txtroute.Text
        rt = Replace(rt, "'", Chr(180), , , vbTextCompare)
        rt = Replace(rt, "--", "-", , , vbTextCompare)
        rt = Replace(rt, ";", ":", , , vbTextCompare)
        rtd = txtroutedesc.Text
        rtd = Replace(rtd, "'", Chr(180), , , vbTextCompare)
        rtd = Replace(rtd, "--", "-", , , vbTextCompare)
        rtd = Replace(rtd, ";", ":", , , vbTextCompare)
        usr = lblusername.Value
        typ = ddtyp.SelectedValue.ToString
        jpref = lbljpref.Value
        stat = ddjpstat.SelectedValue
        Dim jcnt As Integer
        sql = "select count(*) from pmjobplans where jpnum = '" & rt & "'"
        jcnt = gtasks.Scalar(sql)
        Dim oplan As String = lbloldjobplan.Value
        If rt <> oplan Then
            If jcnt = 0 Then
                jcnt = 1
            End If
        End If
        If jcnt = 1 Then
            Dim si As Integer
            sql = "select scheduled from jobplantypes where jptypeid = '" & typ & "'"
            si = gtasks.Scalar(sql)
            sql = "update pmjobplans set jpnum = '" & rt & "', " _
            + "description = '" & rtd & "', " _
            + "type = '" & typ & "', " _
            + "scheduled = '" & si & "', " _
            + "modby = '" & usr & "', moddate = getDate() " _
            + "where jpid = '" & jid & "'"
            gtasks.Update(sql)
            wo = lblwo.Value
            If wo <> "" Then
                sql = "update workorder set jpnum = '" & rt & "', jpid = '" & jid & "' where wonum = '" & wo & "'"
                gtasks.Update(sql)
            End If
            If jpref <> "" Then
                If stat = "New" Or stat = "In Progress" Then
                    sql = "update pmjpref set jpnum = '" & rt & "', jpid = '" & jid & "', plnrid = '" & plnr & "' " _
                    + "where wonum = '" & wo & "'; " _
                    + "update wojobplans set jpnum = '" & rt & "', description = '" & rtd & "', type = '" & typ & "' " _
                    + "where jpid = '" & jid & "' and wonum = '" & wo & "'"
                    gtasks.Update(sql)
                End If
            End If
            SaveDetails()
            BindGrid(jid)
        Else
            Dim strMessage As String = tmod.getmsg("cdstr465", "JobPlan.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
    Private Sub AddPlan()
        Dim rt, rtd, typ, stat As String
        rt = txtroute.Text
        rt = Replace(rt, "'", Chr(180), , , vbTextCompare)
        rt = Replace(rt, "--", "-", , , vbTextCompare)
        rt = Replace(rt, ";", ":", , , vbTextCompare)
        rtd = txtroutedesc.Text
        rtd = Replace(rtd, "'", Chr(180), , , vbTextCompare)
        rtd = Replace(rtd, "--", "-", , , vbTextCompare)
        rtd = Replace(rtd, ";", ":", , , vbTextCompare)
        typ = ddtyp.SelectedValue.ToString
        sid = lblsid.Value
        plnr = lblplnrid.Value
        jpref = lbljpref.Value
        stat = ddjpstat.SelectedValue
        usr = lblusername.Value
        If usr = "" Then
            Try
                usr = HttpContext.Current.Session("username").ToString()
            Catch ex As Exception

            End Try
        End If

        Dim jcnt As Integer
        sql = "select count(*) from pmjobplans where jpnum = '" & rt & "'"
        jcnt = gtasks.Scalar(sql)
        If jcnt = 0 Then
            Dim jpcnt As Integer
            If jpref = "" Then
                sql = "insert into pmjobplans (jpnum, description, type, siteid, createdby, createdate) values " _
                + "('" & rt & "','" & rtd & "','" & typ & "','" & sid & "','" & usr & "',getDate()) select @@identity"
            Else
                sql = "insert into pmjobplans (jpnum, jpref, plnrid, description, type, siteid, createdby, createdate) values " _
                + "('" & rt & "','" & jpref & "','" & plnr & "','" & rtd & "','" & typ & "','" & sid & "','" & usr & "',getDate()) select @@identity"
            End If
            'gtasks.Update(sql)
            jpcnt = gtasks.Scalar(sql)
            lbljid.Value = jpcnt
            wo = lblwo.Value
            'wojptaskmeasdetails
            'wojptaskmeasoutput
            If wo <> "" Then
                sql = "update workorder set jpnum = '" & rt & "', jpid = '" & jpcnt & "' where wonum = '" & wo & "'"
                gtasks.Update(sql)
                sql = "delete from wojobplans where wonum = '" & wo & "';delete from wojplubes where wonum = '" & wo & "';" _
                    + "delete from wojobtasks where wonum = '" & wo & "';delete from wojpfailmodes where wonum = '" & wo & "';" _
                    + "delete from wojpparts where wonum = '" & wo & "';delete from wojptools where wonum = '" & wo & "';" _
                    + "delete from wojppartsout where wonum = '" & wo & "';delete from wojptoolsout where wonum = '" & wo & "';" _
                    + "delete from wojplubesout where wonum = '" & wo & "';delete from wojptools where wonum = '" & wo & "';" _
                    + "delete from wojptaskmeasdetails where wonum = '" & wo & "';delete from wojptaskmeasoutput where wonum = '" & wo & "';" _
                    + "update workorder set estjplabhrs = 0, estjpmatcost = 0, estjplabcost = 0, estjptoolcost = 0, estjplubecost = 0 where wonum = '" & wo & "'"
                gtasks.Update(sql)
                If jpref = "" Then
                    sql = "insert into wojobplans (wonum, jpid, jpnum, description, type, siteid) values " _
                    + "('" & wo & "','" & jpcnt & "','" & rt & "','" & rtd & "','" & typ & "','" & sid & "')"
                Else
                    sql = "insert into wojobplans (wonum, jpid, jpnum, jpref, plnrid, description, type, siteid) values " _
                    + "('" & wo & "','" & jpcnt & "','" & rt & "','" & jpref & "','" & plnr & "','" & rtd & "','" & typ & "','" & sid & "')"
                End If
                gtasks.Update(sql)
                SaveWODetails()
            End If
            If jpref <> "" Then
                If stat = "New" Or stat = "In Progress" Then
                    sql = "update pmjpref set jpnum = '" & rt & "', jpid = '" & jpcnt & "', plnrid = '" & plnr & "' where wonum = '" & wo & "'"
                    gtasks.Update(sql)
                    If wo <> "" Then
                        SaveWODetails()
                    End If
                End If
            End If
            SaveDetails()
            BindGrid()
        Else
            Dim strMessage As String = tmod.getmsg("cdstr466", "JobPlan.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Private Sub BindHead(ByVal jid As String)
        sql = "select jpnum, description, type from pmjobplans where jpid = '" & jid & "'"
        dr = gtasks.GetRdrData(sql)
        txtroute.Text = ""
        txtroutedesc.Text = ""
        Try
            ddtyp.SelectedIndex = 0
        Catch ex As Exception

        End Try

        While dr.Read
            txtroute.Text = dr.Item("jpnum").ToString
            lbloldjobplan.Value = dr.Item("jpnum").ToString
            txtroutedesc.Text = dr.Item("description").ToString
            Try
                ddtyp.SelectedValue = dr.Item("type").ToString
            Catch ex As Exception

            End Try
        End While
        dr.Close()
    End Sub
    Private Sub BindGrid(Optional ByVal jid As String = "0")
        wo = lblwo.Value
        If jid <> "0" Then
            If wo = "" Then
                sql = "select t.*, isnull(p.comid, '0') as comid, '' as wojtid from pmjobtasks t " _
                            + "left join pmjobplans p on p.jpid = t.jpid where t.jpid = '" & jid & "' and t.subtask = 0 order by t.tasknum"
            Else
                sql = "select t.*, isnull(p.comid, '0') as comid, wt.wojtid from pmjobtasks t " _
                + "left join pmjobplans p on p.jpid = t.jpid " _
                + "left join wojobtasks wt on wt.pmtskid = t.pmtskid where t.jpid = '" & jid & "' and t.subtask = 0 order by t.tasknum"
            End If

        Else
            sql = "select t.*, '' as wojtid from pmjobtasks t where t.jpid = '0'"
        End If

        ds = gtasks.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        'Try
        dgtasks.DataSource = dv
        dgtasks.DataBind()
        'Catch ex As Exception

        'End Try
        ro = lblro.Value
        If ro = "1" Then
            dgtasks.Columns(0).Visible = False
            dgtasks.Columns(10).Visible = False
        End If

        If cball.Checked = True Then
            dgtasks.Columns(4).Visible = False
            'dgtasks.Columns(5).Visible = False
            'dgtasks.Columns(6).Visible = False
        Else
            dgtasks.Columns(4).Visible = True
            'dgtasks.Columns(5).Visible = True
            'dgtasks.Columns(6).Visible = True
        End If
    End Sub
    Public Function PopFail() As DataSet
        Dim chk As String = lblfailchk.Value
        Dim scnt As Integer
        sid = lblsid.Value
        coid = lblcoid.Value
        If chk = "" Then
            sql = "select count(*) from pmSiteFM where siteid = '" & sid & "'"
            scnt = gtasks.Scalar(sql)
            If scnt = 0 Then
                lblfailchk.Value = "open"
                chk = "open"
            Else
                lblfailchk.Value = "site"
                chk = "site"
            End If
        End If
        If chk = "open" Then
            sql = "select failid, failuremode from failuremodes order by failuremode"
        ElseIf chk = "site" Then
            sql = "select failid, failuremode from pmSiteFM where siteid = '" & sid & "' order by failuremode"
        Else
            Exit Function
        End If
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Public Function PopulateSkills() As DataSet
        cid = lblcid.Value
        sid = lblsid.Value
        Dim sskills As String = lblsskills.Value
        'If sskills = "yes" Then
        'sql = "select skillid, skill, skillindex from pmSiteSkills where (compid = '" & cid & "' and siteid = '" & sid & "') or skillindex = '0' order by skillindex"
        'Else
        sql = "select skillid, skill, skillindex from pmSkills order by skillindex"
        'End If

        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function
    Private Sub AddTask()
        jid = lbljid.Value
        wo = lblwo.Value
        If jid <> 0 Then
            'gtasks.Open()
            Dim stcnt As Integer
            sql = "Select count(*) from pmJobTasks " _
            + "where jpid = '" & jid & "'"
            stcnt = gtasks.Scalar(sql)
            stcnt += 1
            Dim ski, ska As String
            If cball.Checked = True Then
                ski = lblskillallid.Value
                ska = lblskillall.Value
                sql = "insert into pmjobtasks (jpid, tasknum, skillid, skill) values ('" & jid & "', '" & stcnt & "','" & ski & "','" & ska & "') select @@identity"

            Else
                sql = "insert into pmjobtasks (jpid, tasknum) values ('" & jid & "', '" & stcnt & "') select @@identity"
            End If
            Dim pmtskid As Integer

            'gtasks.Update(sql)
            pmtskid = gtasks.Scalar(sql)
            If cball.Checked = True Then
                sql = "update pmjobtasks set skillindex = ( " _
                + "isnull((select skillindex from pmskills s where " _
                + "s.skillid = '" & ski & "'), 0)) where pmtskid = '" & pmtskid & "'"
                gtasks.Update(sql)
            End If

            If wo <> "" Then
                sql = "insert into wojobtasks (pmtskid, jpid, tasknum, wonum) values ('" & pmtskid & "','" & jid & "', '" & stcnt & "','" & wo & "') "
                gtasks.Update(sql)
            End If
            BindGrid(jid)
            'gtasks.Dispose()
        Else
            Dim strMessage As String = tmod.getmsg("cdstr469", "JobPlan.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub

    Private Sub dgtasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.CancelCommand
        jid = lbljid.Value
        lbleditmode.Value = "0"
        dgtasks.EditItemIndex = -1
        gtasks.Open()
        BindGrid(jid)
        gtasks.Dispose()

        lbldboxid.Value = ""
        lbltaskdesc.Value = ""
        lblqboxid.Value = ""
        lblgridqty.Value = ""
        lblmboxid.Value = ""
        lblgridmins.Value = ""
        lblsboxid.Value = ""
        lblgridskillid.Value = ""
    End Sub

    Private Sub dgtasks_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.DeleteCommand
        Dim tid, jid, tnum As String
        Try
            tid = CType(e.Item.FindControl("lblttid"), Label).Text
            tnum = CType(e.Item.FindControl("lblt"), TextBox).Text
        Catch ex As Exception
            tid = CType(e.Item.FindControl("lbltida"), Label).Text
            tnum = CType(e.Item.FindControl("lblta"), Label).Text
        End Try
        jid = lbljid.Value
        wo = lblwo.Value
        sql = "usp_deljptask '" & jid & "', '" & tnum & "', '" & wo & "'"
        gtasks.Open()
        gtasks.Update(sql)
        eqid = lbleqid.Value
        'gtasks.UpMod(eqid)
        Try
            dgtasks.EditItemIndex = -1
        Catch ex As Exception

        End Try
        BindGrid(jid)
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.EditCommand
        jid = lbljid.Value
        lbleditmode.Value = "1"
        lbloldtasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text

        dgtasks.EditItemIndex = e.Item.ItemIndex
        gtasks.Open()
        BindGrid(jid)
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.ItemCommand

        If e.CommandName = "Tool" Then
            lbltool.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblfailret.Value = "1"
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
                lblwojtid.Value = CType(e.Item.FindControl("lblwojtide"), Label).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
                lblwojtid.Value = CType(e.Item.FindControl("lblwojtidi"), Label).Text
            End If

        End If
        If e.CommandName = "Part" Then
            lblpart.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblfailret.Value = "1"
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
                lblwojtid.Value = CType(e.Item.FindControl("lblwojtide"), Label).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
                lblwojtid.Value = CType(e.Item.FindControl("lblwojtidi"), Label).Text
            End If
        End If
        If e.CommandName = "Lube" Then
            lbllube.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblfailret.Value = "1"
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
                lblwojtid.Value = CType(e.Item.FindControl("lblwojtide"), Label).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
                lblwojtid.Value = CType(e.Item.FindControl("lblwojtidi"), Label).Text
            End If
        End If
        If e.CommandName = "Meas" Then
            lblmeas.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblfailret.Value = "1"
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
                lblwojtid.Value = CType(e.Item.FindControl("lblwojtide"), Label).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
                lblwojtid.Value = CType(e.Item.FindControl("lblwojtidi"), Label).Text
            End If
        End If
        If e.CommandName = "Note" Then
            lblnote.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblfailret.Value = "1"
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        Dim ttid As String
        wo = lblwo.Value
        If e.CommandName = "ToTask2" Then
            Dim Item As ListItem
            Dim f, fi, oa As String
            Dim ipar As Integer = 0
            Dim comp As String = lblcoid.Value
            gtasks.Open()
            For Each Item In CType(e.Item.FindControl("lbCompFM"), ListBox).Items
                If Item.Selected Then
                    'f = Item.Text.ToString
                    'fi = Item.Value.ToString
                    ttid = CType(e.Item.FindControl("lblttid"), Label).Text
                    'GetItems(f, fi, comp, ttid)
                    f = Item.Text.ToString
                    fi = Item.Value.ToString
                    Dim fiarr() As String = fi.Split("-")
                    fi = fiarr(0)
                    oa = fiarr(1)
                    If oa <> "0" Then
                        ipar = f.LastIndexOf("(")
                        If ipar <> -1 Then
                            f = Mid(f, 1, ipar)
                        End If
                    End If
                    GetItems(f, fi, comp, ttid, oa)
                End If
            Next


            sql = "usp_UpdateJPFM1 '" & ttid & "','" & wo & "'"
            gtasks.Update(sql)

            Dim descbox As TextBox = CType(e.Item.FindControl("txtdesc"), TextBox)
            Dim desc As String
            desc = CType(e.Item.FindControl("txtdesc"), TextBox).Text
            desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
            desc = Replace(desc, "--", "-", , , vbTextCompare)
            desc = Replace(desc, ";", " ", , , vbTextCompare)

            dgtasks.Dispose()
            jid = lbljid.Value
            BindGrid(jid)
            dgtasks.EditItemIndex = e.Item.ItemIndex
            descbox.Text = desc
            gtasks.Dispose()
        End If

        If e.CommandName = "RemTask" Then
            Dim Item As ListItem
            Dim f, fi, oa As String
            Dim ipar As Integer = 0
            Dim comp As String = lblcoid.Value
            gtasks.Open()
            Dim rf, rfd As String
            For Each Item In CType(e.Item.FindControl("lbfailmodes"), ListBox).Items
                If Item.Selected Then
                    'f = Item.Text.ToString
                    'fi = Item.Value.ToString
                    ttid = CType(e.Item.FindControl("lblttid"), Label).Text
                    'RemItems(f, fi, ttid)
                    f = Item.Text.ToString
                    fi = Item.Value.ToString
                    Dim fiarr() As String = fi.Split("-")
                    fi = fiarr(0)
                    oa = fiarr(1)
                    If oa <> "0" Then
                        ipar = f.LastIndexOf("(")
                        If ipar <> -1 Then
                            f = Mid(f, 1, ipar)
                        End If
                    End If
                    RemItems(f, fi, ttid, oa)
                    If Len(rf) = 0 Then
                        rf = f
                    Else
                        rf += "," & f
                    End If
                End If
            Next


            sql = "usp_UpdateJPFM1 '" & ttid & "','" & wo & "'"
            gtasks.Update(sql)

            dgtasks.Dispose()
            jid = lbljid.Value
            BindGrid(jid)
            dgtasks.EditItemIndex = e.Item.ItemIndex
            gtasks.Dispose()
        End If

    End Sub

    Private Sub dgtasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtasks.ItemDataBound
        If e.Item.ItemType = ListItemType.EditItem Then
            lblitemindex.Value = e.Item.ItemIndex
            lblcurrtask.Value = CType(e.Item.FindControl("lblttid"), Label).Text.ToString

            Dim dbox As TextBox = CType(e.Item.FindControl("txtdesc"), TextBox)
            Dim dboxid As String = dbox.ClientID
            lbldboxid.Value = dboxid


            Dim qbox As TextBox = CType(e.Item.FindControl("txtqty"), TextBox)
            Dim qboxid As String = qbox.ClientID
            lblqboxid.Value = qboxid


            Dim mbox As TextBox = CType(e.Item.FindControl("txttr"), TextBox)
            Dim mboxid As String = mbox.ClientID
            lblmboxid.Value = mboxid

            'Dim sbox As DropDownList = CType(e.Item.FindControl("ddskill"), DropDownList)
            'Dim sboxid As String = sbox.ClientID
            'lblsboxid.Value = sboxid
            Dim pmtskid As String = DataBinder.Eval(e.Item.DataItem, "pmtskid").ToString
            wo = lblwo.Value
            Dim lblskill As Label = CType(e.Item.FindControl("lblskill"), Label)
            Dim skillid As String
            skillid = lblskill.ClientID.ToString
            Dim imgskill As HtmlImage = CType(e.Item.FindControl("imgskill"), HtmlImage)
            imgskill.Attributes.Add("onclick", "getskill('" & skillid & "','" & pmtskid & "','" & sid & "','skill','" & wo & "');")

            Dim fchk = lblfailret.Value

            If lblfailret.Value <> "1" Then
                lbltaskdesc.Value = DataBinder.Eval(e.Item.DataItem, "taskdesc").ToString
                lblgridqty.Value = DataBinder.Eval(e.Item.DataItem, "qty").ToString
                lblgridmins.Value = DataBinder.Eval(e.Item.DataItem, "ttime").ToString
                lblgridskillid.Value = DataBinder.Eval(e.Item.DataItem, "skillid").ToString
            End If
        End If
    End Sub

    Private Sub dgtasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.UpdateCommand
        Dim tn, tid, desc, ski, tski, qty, tr, dt, otn, jid, fi, fs As String
        'addtask.Enabled = True
        qty = CType(e.Item.FindControl("txtqty"), TextBox).Text
        tr = CType(e.Item.FindControl("txttr"), TextBox).Text
        dt = CType(e.Item.FindControl("txtdt"), TextBox).Text
        desc = CType(e.Item.FindControl("txtdesc"), TextBox).Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", " ", , , vbTextCompare)
        tid = CType(e.Item.FindControl("lblttid"), Label).Text
        tn = CType(e.Item.FindControl("lblt"), TextBox).Text
        wo = lblwo.Value


        If ski <> "0" Then
            If cball.Checked = True Then
                ski = lblskillallid.Value
                tski = lblskillall.Value
            Else
                Try
                    ski = CType(e.Item.FindControl("ddskill"), DropDownList).SelectedItem.Value
                    tski = CType(e.Item.FindControl("ddskill"), DropDownList).SelectedItem.Text
                Catch ex As Exception
                    ski = "0"
                    tski = "Select"
                End Try
            End If

        End If
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToInt64(qty)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr470", "JobPlan.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        Dim txtchk As Long
        Try
            txtchk = System.Convert.ToDecimal(tr)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr471", "JobPlan.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        tr = qtychk * txtchk
        lbleditmode.Value = "0"

        If cball.Checked = True Then
            sql = "update pmjobtasks set qty = '" & qty & "', " _
            + "ttime = '" & tr & "', " _
            + "taskdesc = '" & desc & "', " _
            + "skillid = '" & ski & "', " _
            + "skill = '" & tski & "', " _
            + "rate = (select rate from pmskills where skillid = '" & ski & "') " _
            + "where pmtskid = '" & tid & "'; " _
            + "update pmjobtasks set labcost = ((qty * (ttime / 60)) * rate) " _
            + "where pmtskid = '" & tid & "'; " _
            + "update pmjobtasks set skillindex = ( " _
            + "isnull((select skillindex from pmskills s where " _
            + "pmjobtasks.skillid = s.skillid), 0)) where jpid = '" & jid & "'"
        Else
            '+ "rate = (select rate from pmskills where skillid = '" & ski & "') " _
            sql = "update pmjobtasks set qty = '" & qty & "', " _
            + "ttime = '" & tr & "', " _
            + "taskdesc = '" & desc & "' " _
            + "where pmtskid = '" & tid & "'; " _
            + "update pmjobtasks set labcost = ((qty * (ttime / 60)) * rate) " _
            + "where pmtskid = '" & tid & "'; " _
            + "update pmjobtasks set skillindex = ( " _
            + "isnull((select skillindex from pmskills s where " _
            + "pmjobtasks.skillid = s.skillid), 0)) where jpid = '" & jid & "'"
        End If

        gtasks.Open()
        gtasks.Update(sql)
        If wo <> "" Then
            If cball.Checked = True Then
                sql = "update wojobtasks set qty = '" & qty & "', " _
                + "ttime = '" & tr & "', " _
                + "skillid = '" & ski & "', " _
                + "skill = '" & tski & "', " _
                + "taskdesc = '" & desc & "', " _
                + "rate = (select rate from pmskills where skillid = '" & ski & "') " _
                + "where pmtskid = '" & tid & "' and wonum = '" & wo & "'; " _
                + "update wojobtasks set labcost = ((qty * (ttime / 60)) * rate) " _
                + "where pmtskid = '" & tid & "'"
            Else
                '+ "skillid = '" & ski & "', " _
                '+ "skill = '" & tski & "', " _
                '+ "rate = (select rate from pmskills where skillid = '" & ski & "') " _
                sql = "update wojobtasks set qty = '" & qty & "', " _
                + "ttime = '" & tr & "', " _
                + "taskdesc = '" & desc & "' " _
                + "where pmtskid = '" & tid & "' and wonum = '" & wo & "'; " _
                + "update wojobtasks set labcost = ((qty * (ttime / 60)) * rate) " _
                + "where pmtskid = '" & tid & "'"
            End If

            gtasks.Update(sql)
        End If
        sid = lblsid.Value
        Dim sskills As String = lblsskills.Value
        If sskills = "yes" Then
            sql = "update pmjobtasks  set skillindex = ( " _
                  + "isnull((select skillindex from pmsiteskills s where " _
                  + "pmjobtasks.skillid = s.skillid and pmjobtasks.siteid = s.siteid), 0)) " _
                  + "where pmjobtasks.pmtskid = '" & tid & "'"
        Else
            sql = "update pmjobtasks  set skillindex = ( " _
      + "isnull((select skillindex from pmskills s where " _
      + "pmjobtasks.skillid = s.skillid), 0)) " _
      + "where pmjobtasks.pmtskid = '" & tid & "'"
        End If

        gtasks.Update(sql)
        otn = lbloldtasknum.Value
        jid = lbljid.Value
        If otn <> tn Then

            sql = "usp_reorderjptasks '" & tid & "', '" & jid & "', '" & tn & "', '" & otn & "', '" & wo & "'"
            gtasks.Update(sql)
        End If
        eqid = lbleqid.Value
        dgtasks.EditItemIndex = -1
        BindGrid(jid)
        gtasks.Dispose()
    End Sub
    Private Sub RemItems(ByVal f As String, ByVal fi As String, ByVal ttid As String, ByVal oaid As String)
        Dim jp As String = lbljid.Value
        wo = lblwo.Value
        Try
            sql = "delete from jpfailmodes where jpid = '" & jp & "' and failid = '" & fi & "' and pmtskid = '" & ttid & "' and oaid = '" & oaid & "'"
            gtasks.Update(sql)
            If wo <> "" Then
                sql = "delete from wojpfailmodes where jpid = '" & jp & "' and failid = '" & fi & "' and pmtskid = '" & ttid & "', and wonum = '" & wo & "' and oaid = '" & oaid & "'"
                gtasks.Update(sql)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GetItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal ttid As String, ByVal oaid As String)
        Dim typ As String
        Dim jp As String = lbljid.Value
        wo = lblwo.Value
        Dim fcnt, fcnt2 As Integer
        
        If oaid <> "0" Then
            sql = "select count(*) from jpfailmodes where pmtskid = '" & ttid & "' and failid = '" & fi & "' and oaid = '" & oaid & "'"
            fcnt = gtasks.Scalar(sql)
            sql = "select count(*) from jpfailmodes where pmtskid = '" & ttid & "'"
            fcnt2 = gtasks.Scalar(sql)
        Else
            sql = "select count(*) from jpfailmodes where pmtskid = '" & ttid & "' and failid = '" & fi & "'"
            fcnt = gtasks.Scalar(sql)
            sql = "select count(*) from jpfailmodes where pmtskid = '" & ttid & "'"
            fcnt2 = gtasks.Scalar(sql)
        End If
       

        If comp <> "" Then
            typ = "hascomp"
        Else
            typ = "na"
        End If
        If fcnt2 >= 5 Then
            Dim strMessage As String = tmod.getmsg("cdstr472", "JobPlan.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf fcnt > 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr473", "JobPlan.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            'Try
            sql = "usp_addJPFailureMode " & jp & ", " & fi & ", '" & f & "', '" & comp & "', '" & ttid & "', '" & wo & "', '" & typ & "','" & oaid & "'"
            gtasks.Update(sql)
            eqid = lbleqid.Value
            'End Try
        End If
    End Sub

    Private Sub ddjpstat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddjpstat.SelectedIndexChanged
        gtasks.Open()
        Dim stat As String = ddjpstat.SelectedValue
        jpref = lbljpref.Value
        sql = "update pmjpref set status = '" & stat & "' where jpref = '" & jpref & "'"
        gtasks.Update(sql)
        If stat = "Complete" Or stat = "Cancelled" Then
            imgplanner.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
            imgplanner.Attributes.Add("onclick", "")
            ddjpstat.Enabled = False
        End If
        gtasks.Dispose()
    End Sub
    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgtasks.Columns(0).HeaderText = dlabs.GetDGPage("JobPlan.aspx", "dgtasks", "0")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(1).HeaderText = dlabs.GetDGPage("JobPlan.aspx", "dgtasks", "1")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(2).HeaderText = dlabs.GetDGPage("JobPlan.aspx", "dgtasks", "2")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(3).HeaderText = dlabs.GetDGPage("JobPlan.aspx", "dgtasks", "3")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(4).HeaderText = dlabs.GetDGPage("JobPlan.aspx", "dgtasks", "4")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(5).HeaderText = dlabs.GetDGPage("JobPlan.aspx", "dgtasks", "5")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(6).HeaderText = dlabs.GetDGPage("JobPlan.aspx", "dgtasks", "6")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(8).HeaderText = dlabs.GetDGPage("JobPlan.aspx", "dgtasks", "8")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1124.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1124")
        Catch ex As Exception
        End Try
        Try
            lang1125.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1125")
        Catch ex As Exception
        End Try
        Try
            lang1126.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1126")
        Catch ex As Exception
        End Try
        Try
            lang1127.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1127")
        Catch ex As Exception
        End Try
        Try
            'lang1128.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1128")
        Catch ex As Exception
        End Try
        Try
            'lang1129.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1129")
        Catch ex As Exception
        End Try
        Try
            lang1130.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1130")
        Catch ex As Exception
        End Try
        Try
            lang1131.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1131")
        Catch ex As Exception
        End Try
        Try
            lang1132.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1132")
        Catch ex As Exception
        End Try
        Try
            'lang1133.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1133")
        Catch ex As Exception
        End Try
        Try
            'lang1134.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1134")
        Catch ex As Exception
        End Try
        Try
            lang1135.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1135")
        Catch ex As Exception
        End Try
        Try
            lang1136.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1136")
        Catch ex As Exception
        End Try
        Try
            lang1137.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1137")
        Catch ex As Exception
        End Try
        Try
            'lang1138.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1138")
        Catch ex As Exception
        End Try
        Try
            'lang1139.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1139")
        Catch ex As Exception
        End Try
        Try
            lang1140.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1140")
        Catch ex As Exception
        End Try
        Try
            lang1141.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1141")
        Catch ex As Exception
        End Try
        Try
            'lang1142.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1142")
        Catch ex As Exception
        End Try
        Try
            'lang1143.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1143")
        Catch ex As Exception
        End Try
        Try
            lang1144.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1144")
        Catch ex As Exception
        End Try
        Try
            lang1145.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1145")
        Catch ex As Exception
        End Try
        Try
            'lang1146.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1146")
        Catch ex As Exception
        End Try
        Try
            'lang1147.Text = axlabs.GetASPXPage("JobPlan.aspx", "lang1147")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                imgaddtask.Attributes.Add("src", "../images2/eng/bgbuttons/addtask.gif")
            ElseIf lang = "fre" Then
                imgaddtask.Attributes.Add("src", "../images2/fre/bgbuttons/addtask.gif")
            ElseIf lang = "ger" Then
                imgaddtask.Attributes.Add("src", "../images2/ger/bgbuttons/addtask.gif")
            ElseIf lang = "ita" Then
                imgaddtask.Attributes.Add("src", "../images2/ita/bgbuttons/addtask.gif")
            ElseIf lang = "spa" Then
                imgaddtask.Attributes.Add("src", "../images2/spa/bgbuttons/addtask.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            cball.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("JobPlan.aspx", "cball") & "')")
            cball.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgadd.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("JobPlan.aspx", "imgadd") & "', ABOVE, LEFT)")
            imgadd.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'imgloc.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("JobPlan.aspx", "imgloc") & "')")
            'imgloc.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsav.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("JobPlan.aspx", "imgsav") & "')")
            imgsav.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsav2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("JobPlan.aspx", "imgsav2") & "')")
            imgsav2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'ovid157.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("JobPlan.aspx", "ovid157") & "')")
            'ovid157.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'ovid158.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("JobPlan.aspx", "ovid158") & "')")
            'ovid158.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'ovid159.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("JobPlan.aspx", "ovid159") & "', ABOVE, LEFT)")
            'ovid159.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub
End Class