﻿Imports System.Data.SqlClient
Public Class pmrrtelkupdialog
    Inherits System.Web.UI.Page
    Dim sid, typ, typx, ret, rtid, sql As String
    Dim mm As New Utilities
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
            Catch ex As Exception
                Dim dlang As New mmenu_utils_a
                lblfslang.Value = dlang.AppDfltLang
            End Try
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            Try
                typ = Request.QueryString("typ").ToString
                If typ = "RBASM3x" Then
                    typ = "RBASM3"
                    typx = "RBASM3x"
                    lbltypx.Value = "RBASM3x"
                    'iffreq.Attributes.Add("src", "pmrrtelkup.aspx?sid=" & sid & "&typ=" & typ & "&date=" & Now)
                ElseIf typ = "RBASTM2x" Then
                    typ = "RBASTM2"
                    typx = "RBASTM2x"
                    lbltypx.Value = "RBASTM2x"
                    'iffreq.Attributes.Add("src", "pmrrtelkup.aspx?sid=" & sid & "&typ=" & typ & "&date=" & Now)
                ElseIf typ = "RBASM3xa" Then
                    typ = "RBASM3"
                    typx = "RBASM3xa"
                    lbltypx.Value = "RBASM3xs"
                    'mm.Open()
                    'BindExporta()
                    'mm.Dispose()

                ElseIf typ = "RBASTM2xa" Then
                    typ = "RBASTM2"
                    typx = "RBASTM2xa"
                    lbltypx.Value = "RBASTM2xs"
                    'mm.Open()
                    'BindExportTa()
                    'mm.Dispose()

                ElseIf typ = "COMPx" Then
                    typ = "COMPx"
                    typx = "COMPx"
                    lbltypx.Value = "COMPx"
                    'mm.Open()
                    'BindExportTa()
                    'mm.Dispose()

                ElseIf typ = "L0" Then
                    typ = "L0"
                    typx = "L0"
                    lbltypx.Value = "L0"

                ElseIf typ = "L1" Then
                    typ = "L1"
                    typx = "L1"
                    lbltypx.Value = "L1"

                ElseIf typ = "L2" Then
                    typ = "L2"
                    typx = "L2"
                    lbltypx.Value = "L2"

                End If
            Catch ex As Exception

            End Try

            iffreq.Attributes.Add("src", "pmrrtelkup.aspx?sid=" & sid & "&typ=" & typ & "&date=" & Now)
        Else
            If Request.Form("lblret") = "excel" Then
                mm.Open()
                BindExport()
                mm.Dispose()
            ElseIf Request.Form("lblret") = "excelt" Then
                mm.Open()
                BindExportT()
                mm.Dispose()
            ElseIf Request.Form("lblret") = "excela" Then
                mm.Open()
                BindExporta()
                mm.Dispose()
            ElseIf Request.Form("lblret") = "excelta" Then
                mm.Open()
                BindExportTa()
                mm.Dispose()
            ElseIf Request.Form("lblret") = "excelclx" Then
                mm.Open()
                BindExportclx()
                mm.Dispose()
            ElseIf Request.Form("lblret") = "L0" Then
                mm.Open()
                BindExportL0()
                mm.Dispose()
            ElseIf Request.Form("lblret") = "L1" Then
                mm.Open()
                BindExportL1()
                mm.Dispose()
            ElseIf Request.Form("lblret") = "L2" Then
                mm.Open()
                BindExportL2()
                mm.Dispose()
            End If

        End If
    End Sub
    Private Sub BindExportclx()
        Dim lang As String = lblfslang.Value
        sid = lblsid.Value
        rtid = lblrtid.Value
        If lang = "FRE" Or lang = "fre" Then
            sql = "usp_getcompliballf"
        Else
            sql = "usp_getcompliball"
        End If

        Dim ds As New DataSet
        ds = mm.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
    Private Sub BindExport()
        Dim lang As String = lblfslang.Value
        sid = lblsid.Value
        rtid = lblrtid.Value
        If lang = "FRE" Or lang = "fre" Then
            sql = "usp_pmr_rep5exoutf '" & rtid & "','" & sid & "'"
        Else
            sql = "usp_pmr_rep5exout '" & rtid & "','" & sid & "'"
        End If
        Dim ds As New DataSet
        ds = mm.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
    Private Sub BindExporta()
        Dim lang As String = lblfslang.Value
        sid = lblsid.Value
        rtid = "0"
        If lang = "FRE" Or lang = "fre" Then
            sql = "usp_pmr_rep5exoutf '" & rtid & "','" & sid & "'"
        Else
            sql = "usp_pmr_rep5exout '" & rtid & "','" & sid & "'"
        End If

        Dim ds As New DataSet
        ds = mm.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub

    Private Sub BindExportT()
        Dim lang As String = lblfslang.Value
        sid = lblsid.Value
        rtid = lblrtid.Value
        If lang = "FRE" Or lang = "fre" Then
            sql = "usp_pmr_rep5exouttpmf '" & rtid & "','" & sid & "'"
        Else
            sql = "usp_pmr_rep5exouttpm '" & rtid & "','" & sid & "'"
        End If

        Dim ds As New DataSet
        ds = mm.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub

    Private Sub BindExportTa()
        Dim lang As String = lblfslang.Value
        sid = lblsid.Value
        rtid = "0"
        If lang = "FRE" Or lang = "fre" Then
            sql = "usp_pmr_rep5exouttpmf '" & rtid & "','" & sid & "'"
        Else
            sql = "usp_pmr_rep5exouttpm '" & rtid & "','" & sid & "'"
        End If

        Dim ds As New DataSet
        ds = mm.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub

    Private Sub BindExportL0()
        Dim lang As String = lblfslang.Value
        sid = lblsid.Value
        rtid = "0"
        'If lang = "FRE" Or lang = "fre" Then
        'sql = "usp_pmr_rep5exouttpmf '" & rtid & "','" & sid & "'"
        'Else
        sql = "usp_getlev0tots '" & sid & "'"
        'End If

        Dim ds As New DataSet
        ds = mm.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
    Private Sub BindExportL1()
        Dim lang As String = lblfslang.Value
        sid = lblsid.Value
        rtid = "0"
        'If lang = "FRE" Or lang = "fre" Then
        'sql = "usp_pmr_rep5exouttpmf '" & rtid & "','" & sid & "'"
        'Else
        sql = "usp_getlev1tots '" & sid & "'"
        'End If

        Dim ds As New DataSet
        ds = mm.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
    Private Sub BindExportL2()
        Dim lang As String = lblfslang.Value
        sid = lblsid.Value
        rtid = "0"
        'If lang = "FRE" Or lang = "fre" Then
        'sql = "usp_pmr_rep5exouttpmf '" & rtid & "','" & sid & "'"
        'Else
        sql = "usp_getlev2tots '" & sid & "'"
        'End If

        Dim ds As New DataSet
        ds = mm.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
End Class