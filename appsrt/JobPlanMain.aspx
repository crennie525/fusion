<%@ Page Language="vb" AutoEventWireup="false" Codebehind="JobPlanMain.aspx.vb" Inherits="lucy_r12.JobPlanMain" %>
<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PM Job Plans Main</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="javascript" src="../scripts/taskgrid.js"></script>
		<script language="JavaScript" src="../scripts1/JobPlanMainaspx_1.js"></script>
	 <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
     <!--
         function checkup(eqid) {
             document.getElementById("lbleqid").value = eqid;
             document.getElementById("subdiv").className = "view";
             document.getElementById("subdiv").style.position = "absolute";
             document.getElementById("subdiv").style.top = "120px";
             document.getElementById("subdiv").style.left = "120px";
             document.getElementById("ifrev").src = "PMUpDate.aspx?eqid=" + eqid;
         }

         function closerev() {
             document.getElementById("subdiv").className = "details";
         }
         function handleexit(id) {
             if (id == "no") {
                 closerev();
             }
             else {
                 closerev();
                 var eid = document.getElementById("lbleqid").value;
                 var sid = document.getElementById("lblsid").value;
                 document.getElementById("geteq").src = "PMGetPMMan.aspx?jump=yes&typ=no&eqid=" + eid + "&pmid=0&sid=" + sid;
             }
         }
         function handleproj(val, typ) {
             //alert(val + ", " + typ)
             window.location = "PMScheduling2.aspx?jump=yes&typ=no&eqid=" + val + "&date=" + Date();
         }

         function handlert(rid, who) {
             //if(who=="jp") {
             //who="yes"
             //}
             window.location = "jobplanmain2.aspx?start=jp&jid=" + rid + "&jpnum=" + who + "&date=" + Date();
         }

         function handlerq(jr, wo, pl, jp, pln) {
             window.location = "jobplanmain2.aspx?start=rq&jid=" + jp + "&wo=" + wo + "&plnr=" + pl + "&jpref=" + jr + "&plnrid=" + pln + "&date=" + Date();
         }

         function handleadd(st, rtn) {
             //alert(st + "," + rtn)
             window.location = "jobplanmain2.aspx?start=" + st + "&from=" + rtn;
         }
         function handlejump(id, eid) {
             document.getElementById("lbljump").value = "eq";
             checkarch();
             sid = document.getElementById("lblsid").value;
             document.getElementById("ifmain").src = "PMGetPMMan.aspx?jump=yes&typ=no&eqid=" + eid + "&pmid=" + id + "&sid=" + sid;
         }
         function geteq(eid) {
             //document.getElementById("lbljump").value = "eq";
             //checkarch();
             gettab("jp")
             document.getElementById("jpmain").src = "JobPlanList.aspx?jump=yes&typ=eq&eqid=" + eid + "&fu=0&co=0";
         }
         function getfu(fid, eid) {
             //document.getElementById("lbljump").value = "eq";
             //checkarch();
             gettab("jp")
             document.getElementById("ifmain").src = "JobPlanList.aspx?jump=yes&typ=fu&eqid=" + eid + "&fu=" + fid + "&co=0";
         }
         function getco(cid, fid, eid) {
             //document.getElementById("lbljump").value = "eq";
             //checkarch();
             gettab("jp")
             document.getElementById("ifmain").src = "JobPlanList.aspx?jump=yes&typ=co&eqid=" + eid + "&fu=" + fid + "&co=" + cid;
         }
         function checkarch() {
             //alert(document.getElementById("lblgetarch").value)
             var jump = document.getElementById("lbljump").value;
             if (jump == "jp" || jump == "st") {
                 gettab(jump);
             }
             if (document.getElementById("lblgetarch").value == "yes") {
                 document.getElementById("lblgetarch").value = "no"
                 chk = document.getElementById("lblchk").value;
                 did = document.getElementById("lbldid").value;
                 clid = document.getElementById("lblclid").value;
                 getarch(chk, did, clid)
             }
         }
         function getarch(chk, did, clid) {
             //alert()
             document.getElementById("lblchk").value = chk;
             document.getElementById("lbldid").value = did;
             document.getElementById("lblclid").value = clid;
             //document.getElementById("lblgetarch").value="yes";
             document.getElementById("ifarch").src = "PMArchRT.aspx?start=yes&chk=" + chk + "&did=" + did + "&clid=" + clid;
             if (document.getElementById("lbleqid").value != "") {
                 //alert()
                 document.getElementById("ifimg").src = "AssetImage.aspx?eqid=" + document.getElementById("lbleqid").value;
             }
         }
         function getimage(eqid) {
             document.getElementById("ifimg").src = "PMImageMan.aspx?eqid=" + eqid;
         }
         function handlelogout() {
             document.getElementById("form1").submit();
         }
         function handletasks(href, rev) {
             //alert(rev)
             document.getElementById("iftaskdet").src = href;
             if (rev == "dept") {
                 //alert()
                 checkarch()
                 //document.getElementById("ifarch").src="DevArch.aspx?start=no";
                 document.getElementById("ifimg").src = "AssetImage.aspx?eqid=0";
             }
             else if (rev == "cell") {
                 document.getElementById("ifimg").src = "AssetImage.aspx?eqid=0";
             }

         }
         function handlecnt(cnt) {
             try {
                 document.getElementById("taskcnt").value = cnt;
             }
             catch (err) {
             }
         }
         function handleapp(app) {
             if (app == "switch") document.getElementById("form1").submit();
         }
         function checkit() {
             window.setTimeout("setref();", 1205000);
         }
         function setref() {
             //setref
             //document.getElementById("form1").submit(); 
         }
         function gettab(name) {
             closeall();
             if (name == "jp") {
                 document.getElementById("tdjp").className = "thdrhov label";
                 document.getElementById("jpmain").src = "JobPlanList.aspx?jump=no&date=" + Date();
             }
             else if (name == "st") {
                 var ro = document.getElementById("lblro").value
                 document.getElementById("tdst").className = "thdrhov label";
                 document.getElementById("jpmain").src = "JobPlanRefReq.aspx?jump=no&date=" + Date() + "&ro=" + ro;

             }
         }
         function closeall() {
             document.getElementById("tdst").className = "thdr label";
             document.getElementById("tdjp").className = "thdr label";
             //document.getElementById("pm").className = 'details';
             //document.getElementById("eq").className = 'details';
         }
         -->
     </script>
	</HEAD>
	<body class="tbg" onload="checkarch();" >
		<form id="form1" method="post" runat="server">
			<table style="Z-INDEX: 1; LEFT: 7px; POSITION: absolute; TOP: 83px" cellSpacing="0" cellPadding="0"
				width="1040">
				<tr>
					<td class="thdrhov label" id="tdjp" onclick="gettab('jp');" width="180"><asp:Label id="lang1151" runat="server">Current Job Plans</asp:Label></td>
					<td class="thdr label" id="tdst" onclick="gettab('st');" width="180"><asp:Label id="lang1152" runat="server">Requested Job Plans</asp:Label></td>
					<td width="390">&nbsp;</td>
					<td width="3">&nbsp;</td>
					<td class="thdrsinglft" id="e1" width="26"><IMG src="../images/appbuttons/minibuttons/eqarch.gif" border="0"></td>
					<td class="thdrsingrt label" id="e2" align="left" width="234"><asp:Label id="lang1153" runat="server">Asset Hierarchy</asp:Label></td>
				</tr>
				<tr>
					<td class="tdborder" id="pm" vAlign="top" colSpan="3">
						<table cellSpacing="0" cellPadding="0">
							<tr>
								<td><iframe id="jpmain" src="JobPlanList.aspx?jump=no" frameBorder="no" width="800" scrolling="no"
										height="492" runat="server" style="BACKGROUND-COLOR: transparent" allowtransparency></iframe>
								</td>
							</tr>
						</table>
					</td>
					<td></td>
					<td vAlign="top" colSpan="2" rowSpan="2">
						<table>
							<tr>
								<td><iframe id="ifarch" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-TOP-STYLE: none; PADDING-TOP: 0px; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: transparent; BORDER-BOTTOM-STYLE: none"
										src="PMArchRT.aspx?start=yes" frameBorder="no" width="260" scrolling="yes" height="472"
										runat="server" allowtransparency></iframe>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lbleqid" type="hidden" name="lbleqid" runat="server"> <input id="lblsid" type="hidden" name="lblsid" runat="server">
			<input id="lbldid" type="hidden" name="lbldid" runat="server"><input id="lblclid" type="hidden" name="lblclid" runat="server">
			<input id="lblret" type="hidden" name="lblret" runat="server"><input id="lblchk" type="hidden" name="lblchk" runat="server">
			<input id="lbldchk" type="hidden" name="lbldchk" runat="server"><input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input id="lblcoid" type="hidden" name="lblcoid" runat="server"> <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
			<input id="lbltasklev" type="hidden" name="lbltasklev" runat="server"> <input id="lblcid" type="hidden" name="lblcid" runat="server">
			<input id="tasknum" type="hidden" name="tasknum" runat="server"><input id="taskcnt" type="hidden" name="taskcnt" runat="server">
			<input id="lblgetarch" type="hidden" name="lblgetarch" runat="server"> <input id="lblpmid" type="hidden" name="lblpmid" runat="server">
			<input id="lbljump" type="hidden" name="lbljump" runat="server"><input type="hidden" id="lblro" runat="server">
			
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
		<uc1:mmenu1 id="Mmenu11" runat="server"></uc1:mmenu1>
	</body>
</HTML>
