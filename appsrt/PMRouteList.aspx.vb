

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class PMRouteList
    Inherits System.Web.UI.Page
	Protected WithEvents ovid169 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid168 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid167 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid166 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang1178 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1177 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1176 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrtyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbpm As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbpmj As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbrbas As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbrbast As System.Web.UI.HtmlControls.HtmlInputRadioButton

    Dim sql As String
    Dim dr As SqlDataReader
    Dim rl As New Utilities
    Dim list, sid, subm, jump, typ, eq, fu, co, rtyp, ftyp, coi, wonum, wodesc, lang As String
    Dim sflag As Integer = 0
    Dim Tables As String = "pmroutes"
    Dim PK As String = "rid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 100
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String
    Dim intPgCnt, intPgNav As Integer
    Protected WithEvents tdlist As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents rbsort As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwodesc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtprob As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblrtedesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblrba As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblrbtpma As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lirtea As System.Web.UI.WebControls.ListItem

    Protected WithEvents licby As System.Web.UI.WebControls.ListItem
    Dim mutils As New mmenu_utils_a
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	

        coi = mutils.COMPI
        lblcoi.Value = coi
        ''If coi = "CAS" Or coi = "KRUG" Or coi = "DEMO" Or coi = "JDI" Or coi = "AGR" Then
        ''rbrbas.Disabled = False
        ''rbrbast.Disabled = False
        'rbrbas.Attributes.Add("class", "view")
        'rbrbast.Attributes.Add("class", "view")
        ''Else
        ''rbrbas.Disabled = True
        ''rbrbast.Disabled = True
        'rbrbas.Attributes.Add("class", "details")
        'rbrbast.Attributes.Add("class", "details")
        ''End If
        Try
            lblcompby.Value = HttpContext.Current.Session("usrname").ToString()
        Catch ex As Exception
            lblcompby.Value = "PM Admin"
        End Try
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        lang = lblfslang.Value
        getlabels()
        GetFSOVLIBS()

        GetFSLangs()
        'Put user code to initialize the page here

        If Not IsPostBack Then
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            lblsort.Value = "asc"
            jump = Request.QueryString("jump").ToString
            typ = Request.QueryString("typ").ToString
            rtyp = Request.QueryString("rtyp").ToString
            If jump = "yes" Then

                If typ = "eq" Then
                    lbleqid.Value = Request.QueryString("eqid").ToString
                ElseIf typ = "fu" Then
                    lbleqid.Value = Request.QueryString("eqid").ToString
                    lblfu.Value = Request.QueryString("fu").ToString
                ElseIf typ = "co" Then
                    lbleqid.Value = Request.QueryString("eqid").ToString
                    lblfu.Value = Request.QueryString("fu").ToString
                    lblco.Value = Request.QueryString("co").ToString
                End If
            Else
                typ = "no"
            End If
            lbltyp.Value = typ
            lblrtyp.Value = rtyp
            rl.Open()
            txtpg.Value = "1"
            If coi = "GLA" Or coi = "DEMO" Then
                'lblsort.Value = "desc"
                If lang = "fre" Then
                    rbsort.Items(1).Text = "Prochaine date"
                    rbsort.Items(2).Text = "Statut"
                Else
                    rbsort.Items(1).Text = "Next Date"
                    rbsort.Items(2).Text = "Status"
                End If
                GetRoutesSched(typ, rtyp)
            Else
                GetRoutes(typ, rtyp)
            End If

            rl.Dispose()
        Else
            If coi = "GLA" Or coi = "DEMO" Then
                'lblsort.Value = "desc"
                If lang = "fre" Then
                    rbsort.Items(1).Text = "Prochaine date"
                    rbsort.Items(2).Text = "Statut"
                Else
                    rbsort.Items(1).Text = "Next Date"
                    rbsort.Items(2).Text = "Status"
                End If
            End If
            If Request.Form("lblret") = "next" Then
                rl.Open()
                GetNext()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                rl.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                typ = lbltyp.Value
                rtyp = lblrtyp.Value
                If coi = "GLA" Or coi = "DEMO" Then
                    GetRoutesSched(typ, rtyp)
                Else
                    GetRoutes(typ, rtyp)
                End If

                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                rl.Open()
                GetPrev()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                rl.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                typ = lbltyp.Value
                rtyp = lblrtyp.Value
                If coi = "GLA" Or coi = "DEMO" Then
                    GetRoutesSched(typ, rtyp)
                Else
                    GetRoutes(typ, rtyp)
                End If
                rl.Dispose()
                lblret.Value = ""
            End If
            subm = Request.Form("lblsubmit").ToString
            If subm = "srch" Then
                lblsubmit.Value = ""
                typ = "no"
                lbltyp.Value = "no"
                rl.Open()
                rtyp = lblrtyp.Value
                If coi = "GLA" Or coi = "DEMO" Then
                    GetRoutesSched(typ, rtyp)
                Else
                    GetRoutes(typ, rtyp)
                End If
                rl.Dispose()
            ElseIf subm = "sort" Then
                lblsubmit.Value = ""
                rl.Open()
                typ = lbltyp.Value
                rtyp = lblrtyp.Value
                If coi = "GLA" Or coi = "DEMO" Then
                    GetRoutesSched(typ, rtyp)
                Else
                    GetRoutes(typ, rtyp)
                End If
                rl.Dispose()
            ElseIf subm = "del" Then
                lblsubmit.Value = ""
                rl.Open()
                DelRoute()
                typ = lbltyp.Value
                rtyp = lblrtyp.Value
                If coi = "GLA" Or coi = "DEMO" Then
                    GetRoutesSched(typ, rtyp)
                Else
                    GetRoutes(typ, rtyp)
                End If
                rl.Dispose()
            ElseIf subm = "swtch" Then
                lblsubmit.Value = ""
                rl.Open()
                typ = lbltyp.Value
                rtyp = lblrtyp.Value
                If coi = "GLA" Or coi = "DEMO" Then
                    GetRoutesSched(typ, rtyp)
                Else
                    GetRoutes(typ, rtyp)
                End If
                rl.Dispose()
            ElseIf subm = "retload" Then
                lblsubmit.Value = ""
                rl.Open()
                typ = lbltyp.Value
                rtyp = lblrtyp.Value
                If lblwonum.Value <> "" Then
                    savewodesc()
                End If
                GetRoutesSched(typ, rtyp)
                rl.Dispose()
            End If
            End If
    End Sub
    Private Sub getlabels()
        lang = lblfslang.Value

        If lang = "fre" Then
            lblrba.InnerHtml = "Rapport�"
            lblrbtpma.InnerHtml = "Rapport� depuis TPM"
            lirtea.Text = "# de route"
            licby.Text = "Cr�� par"
        Else
            lblrba.InnerHtml = "Report Based"
            lblrbtpma.InnerHtml = "Report Based TPM"
        End If
    End Sub
    Private Sub savewodesc()
        Dim wonum As String = lblwonum.Value
        'Dim lg As String = lblwodesc.Value
        Dim sh As String
        Dim lg As String = txtprob.Text
        lg = Replace(lg, "'", Chr(180), , , vbTextCompare)
        lg = Replace(lg, "--", "-", , , vbTextCompare)
        lg = Replace(lg, ";", ":", , , vbTextCompare)
        lg = Replace(lg, "/", " ", , , vbTextCompare)
        Dim lgcnt As Integer

        If Len(lg) > 79 Then
            sh = Mid(lg, 1, 79)
            sql = "update workorder set description = '" & sh & "' where wonum = '" & wonum & "'"
            rl.Update(sql)
            lg = Mid(lg, 80)
            sql = "select count(*) from wolongdesc where wonum = '" & wonum & "'"
            lgcnt = rl.Scalar(sql)
            If lgcnt <> 0 Then
                sql = "update wolongdesc set longdesc = '" & lg & "' where wonum = '" & wonum & "'"
                rl.Update(sql)
            Else
                sql = "insert into wolongdesc (wonum, longdesc) values ('" & wonum & "','" & lg & "')"
                rl.Update(sql)
            End If
        Else
            sql = "update workorder set description = '" & lg & "' where wonum = '" & wonum & "'" _
            + "delete from wolongdesc where wonum = '" & wonum & "'"
            rl.Update(sql)
        End If
        lblwonum.Value = ""
        lblwodesc.Value = ""
    End Sub
    Private Sub GetNext()
        coi = lblcoi.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            typ = lbltyp.Value
            rtyp = lblrtyp.Value
            If coi = "GLA" Or coi = "DEMO" Then
                GetRoutesSched(typ, rtyp)
            Else
                GetRoutes(typ, rtyp)
            End If
        Catch ex As Exception
            rl.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr495", "PMRouteList.aspx.vb")

            rl.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        coi = lblcoi.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            typ = lbltyp.Value
            typ = lbltyp.Value
            rtyp = lblrtyp.Value
            If coi = "GLA" Or coi = "DEMO" Then
                GetRoutesSched(typ, rtyp)
            Else
                GetRoutes(typ, rtyp)
            End If
        Catch ex As Exception
            rl.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr496", "PMRouteList.aspx.vb")

            rl.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub DelRoute()
        Dim rtid As String = lblrtid.Value
        sql = "delete from pmroutes where rid = '" & rtid & "'"
        rl.Update(sql)
        rtyp = lblrtyp.Value
        If rtyp = "RBAS" Then
            sql = "update pmtasks set rteid = NULL, rteseq = NULL, rteno = 0, rte = NULL where rteid = '" & rtid & "'"
            rl.Update(sql)
            sql = "update pmtaskstpm set rteid = NULL, rteseq = NULL, rteno = 0, rte = NULL where rteid = '" & rtid & "'"
            rl.Update(sql)
        ElseIf rtyp = "RBAST" Then
            sql = "update pmtaskstpm set rteid = NULL, rteseq = NULL, rteno = 0, rte = NULL where rteid = '" & rtid & "'"
            rl.Update(sql)
            sql = "update pmtasks set rteid = NULL, rteseq = NULL, rteno = 0, rte = NULL where rteid = '" & rtid & "'"
            rl.Update(sql)
        End If

    End Sub
    Private Sub GetRoutes(ByVal type As String, ByVal rtype As String)
        'Not a clue. Leave it to a training session involving Tibor with Canada
        'Should not be an issue with reqular routes
        'sql = "update pmtasks set rteid = NULL, rte = NULL, rteseq = NULL where rteid is NULL or rteid not in (select rid from pmroutes)"
        'rl.Update(sql)
        'sql = "update pmtaskstpm set rteid = NULL, rte = NULL, rteseq = NULL where rteid is NULL or rteid not in (select rid from pmroutes)"
        'rl.Update(sql)
        coi = mutils.COMPI
        lblcoi.Value = coi
        lang = lblfslang.Value
        Dim srch As String = txtsrch.Text
        sid = lblsid.Value
        Dim sortStr, sortVal, sortFilt As String
        sortStr = lblsort.Value
        sortVal = rbsort.SelectedValue.ToString
        If sortVal = "0" Then
            sortFilt = " order by route " & sortStr
            Sort = "route " & sortStr
        ElseIf sortVal = "1" Then
            sortFilt = " order by rttype " & sortStr
            Sort = "rttype " & sortStr
        ElseIf sortVal = "2" Then
            sortFilt = " order by createdby " & sortStr
            Sort = "createdby " & sortStr
        End If
        If Len(srch) > 0 Then
            srch = "%" & srch & "%"
            Filter = " siteid = ''" & sid & "'' and " _
            + "(route like ''" & srch & "'' or createdby like ''" & srch & "'' or description like ''" & srch & "'')"
            FilterCnt = " siteid = '" & sid & "' and " _
          + "(route like '" & srch & "' or createdby like '" & srch & "' or description like '" & srch & "') "
        Else
            Select Case type
                Case "no"
                    Filter = " siteid = ''" & sid & "'' "
                    FilterCnt = " siteid = '" & sid & "' "
                Case "eq"
                    eq = lbleqid.Value
                    Filter = " rid in (select s.rid from pmroute_stops s where s.eqid = ''" & eq & "'') "
                    FilterCnt = " rid in (select s.rid from pmroute_stops s where s.eqid = '" & eq & "') "
                Case "fu"
                    fu = lblfu.Value
                    Filter = " rid in (select s.rid from pmroute_stops s where s.pmid in " _
                    + "(select p.pmid from pmtrack p where p.funcid = ''" & fu & "'')) "
                    FilterCnt = " rid in (select s.rid from pmroute_stops s where s.pmid in " _
                    + "(select p.pmid from pmtrack p where p.funcid = '" & fu & "')) "
                    sql = "select distinct r.*, p.pmid, p.funcid from pmroutes r " _
                    + "left join pmroute_stops s on s.rid = r.rid " _
                    + "left join pmtrack p on p.pmid = s.pmid " _
                    + "where p.funcid = '" & fu & "' " & sortFilt
                Case "co"
                    co = lblco.Value
                    Filter = " rid in (select s.rid from pmroute_stops s where s.pmid in " _
                    + "(select p.pmid from pmtrack p where p.comid = ''" & co & "'')) "
                    FilterCnt = " rid in (select s.rid from pmroute_stops s where s.pmid in " _
                    + "(select p.pmid from pmtrack p where p.comid = '" & co & "')) "
                    sql = "select distinct r.*, p.pmid, p.comid from pmroutes r " _
                    + "left join pmroute_stops s on s.rid = r.rid " _
                    + "left join pmtrack p on p.pmid = s.pmid " _
                    + "where p.comid = '" & co & "' " & sortFilt
            End Select
        End If
        If rtype <> "" Then
            Filter += " and rttype = ''" & rtype & "''"
            FilterCnt += " and rttype = '" & rtype & "'"
            If rtype = "PM" Then
                rbpm.Checked = True
            ElseIf rtype = "PMMAX" Then
                rbpmj.Checked = True
            ElseIf rtype = "RBAS" Then
                rbrbas.Checked = True
            ElseIf rtype = "RBAST" Then
                rbrbast.Checked = True
            End If

        End If
        Dim sb As StringBuilder = New StringBuilder


        'sb.Append("<td  width=""20"">&nbsp;</td></tr>")
        sb.Append("<div style=""OVERFLOW: auto; width: 750px; height: 360px;"">") '<tr><td colspan=""6"">
        'sb.Append("<table width=""730"">")
        'sb.Append("<tr><td width=""150""></td><td width=""350""></td>")
        'sb.Append("<td width=""40""></td><td width=""90""></td><td width=""50""></td><td width=""50""></td></tr>")
        sb.Append("<table width=""730"">")
        If lang = "fre" Then
            sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""150""># de route</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""350"">" & tmod.getlbl("cdlbl130", "PMRouteList.aspx.vb") & "</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""40"">" & tmod.getlbl("cdlbl131", "PMRouteList.aspx.vb") & "</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""90"">Cr�� par</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""50"">" & tmod.getlbl("cdlbl133", "PMRouteList.aspx.vb") & "</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""50"">" & tmod.getlbl("cdlbl134", "PMRouteList.aspx.vb") & "</td>")
        Else
            sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""150"">" & tmod.getlbl("cdlbl129", "PMRouteList.aspx.vb") & "</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""350"">" & tmod.getlbl("cdlbl130", "PMRouteList.aspx.vb") & "</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""40"">" & tmod.getlbl("cdlbl131", "PMRouteList.aspx.vb") & "</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""90"">" & tmod.getlbl("cdlbl132", "PMRouteList.aspx.vb") & "</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""50"">" & tmod.getlbl("cdlbl133", "PMRouteList.aspx.vb") & "</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""50"">" & tmod.getlbl("cdlbl134", "PMRouteList.aspx.vb") & "</td>")
        End If






        Dim rowid As Integer = 0
        Dim bc, rtid, rt, desc, typ, cb As String
        If FilterCnt <> "" Then
            sql = "select count(*) from pmroutes where " & FilterCnt
        Else
            sql = "select count(*) from pmroutes"
        End If
        PageNumber = txtpg.Value
        intPgNav = rl.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav

        dr = rl.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        While dr.Read
            If rowid = 0 Then
                rowid = 1
                bc = "transrow"
            Else
                rowid = 0
                bc = "transrowblue"
            End If
            rtid = dr.Item("rid").ToString
            rt = dr.Item("route").ToString
            desc = dr.Item("description").ToString
            typ = dr.Item("rttype").ToString
            cb = dr.Item("createdby").ToString
            'If typ = "0" Then
            'typ = "PM"
            'Else
            'typ = "JP"
            'End If
            sb.Append("<tr><td class=""linklabel " & bc & """><a href=""#"" onclick=""rtret('" & rtid & "','" & typ & "');"">")
            sb.Append(rt & "</a></td>")
            sb.Append("<td class=""plainlabel " & bc & """>" & desc & "</td>")
            sb.Append("<td class=""plainlabel " & bc & """>" & typ & "</td>")
            sb.Append("<td class=""plainlabel " & bc & """>" & cb & "</td>")
            If rtype = "RBAS" Or rtype = "RBAST" Then
                sb.Append("<td align=""center"" class=""" & bc & """>")
                '<IMG onmouseover=""return overlib('" & tmod.getov("cov143", "PMRouteList.aspx.vb") & "', ABOVE, LEFT)"">")
                'sb.Append(" onclick=""printpm('" & rtid & "','" & typ & "');"" onmouseout=""return nd()"" src=""../images/appbuttons/minibuttons/printx.gif"">")
                'sb.Append("<IMG onmouseover=""return overlib('Print PM Route Output', ABOVE, LEFT)"")
                sb.Append("<IMG onclick=""printpmr('" & rtid & "','" & typ & "');"" onmouseout=""return nd()"" src=""../images/appbuttons/minibuttons/woprint.gif ""></td>")
            Else
                sb.Append("<td align=""center"" class=""" & bc & """><IMG onmouseover=""return overlib('" & tmod.getov("cov143", "PMRouteList.aspx.vb") & "', ABOVE, LEFT)""")
                sb.Append(" onclick=""printpm('" & rtid & "','" & typ & "');"" onmouseout=""return nd()"" src=""../images/appbuttons/minibuttons/printx.gif""></td>")
            End If

            sb.Append("<td align=""center"" class=""" & bc & """><IMG onmouseover=""return overlib('" & tmod.getov("cov144", "PMRouteList.aspx.vb") & "', ABOVE, LEFT)""")
            sb.Append(" onclick=""delroute('" & rtid & "');"" onmouseout=""return nd()"" src=""../images/appbuttons/minibuttons/del.gif""></td></tr>")


        End While
        dr.Close()
        sb.Append("</table></div>") '</td></tr></table>
        tdlist.InnerHtml = sb.ToString
    End Sub

    Private Sub GetRoutesSched(ByVal type As String, ByVal rtype As String)
        'Not a clue. Leave it to a training session involving Tibor with Canada
        'Should not be an issue with reqular routes
        'sql = "update pmtasks set rteid = NULL, rte = NULL, rteseq = NULL where rteid is NULL or rteid not in (select rid from pmroutes)"
        'rl.Update(sql)
        'sql = "update pmtaskstpm set rteid = NULL, rte = NULL, rteseq = NULL where rteid is NULL or rteid not in (select rid from pmroutes)"
        'rl.Update(sql)
        coi = mutils.COMPI
        lblcoi.Value = coi
        lang = lblfslang.Value
        Dim srch As String = txtsrch.Text
        sid = lblsid.Value
        Dim sortStr, sortVal, sortFilt As String
        sortStr = lblsort.Value
        sortVal = rbsort.SelectedValue.ToString
        If sortVal = "0" Then
            sortFilt = " order by route " & sortStr
            Sort = "route " & sortStr
        ElseIf sortVal = "1" Then
            sortFilt = " order by nextdate " & sortStr
            Sort = "nextdate " & sortStr
        ElseIf sortVal = "2" Then
            sortFilt = " order by rdid " & sortStr
            Sort = "rdid " & sortStr
        End If
        

        If Len(srch) > 0 Then
            srch = "%" & srch & "%"
            'Filter = " siteid = ''" & sid & "'' and " _
            '+ "(route like ''" & srch & "'' or createdby like ''" & srch & "'')"
            'FilterCnt = " siteid = '" & sid & "' and " _
            '+ "(route like '" & srch & "' or createdby like '" & srch & "') "

            Filter = " siteid = ''" & sid & "'' and " _
            + "(route like ''" & srch & "'' or createdby like ''" & srch & "'' or description like ''" & srch & "'')"
            FilterCnt = " siteid = '" & sid & "' and " _
          + "(route like '" & srch & "' or createdby like '" & srch & "' or description like '" & srch & "') "
        Else
            Select Case type
                Case "no"
                    Filter = " siteid = ''" & sid & "'' "
                    FilterCnt = " siteid = '" & sid & "' "
                Case "eq"
                    eq = lbleqid.Value
                    Filter = " rid in (select s.rid from pmroute_stops s where s.eqid = ''" & eq & "'') "
                    FilterCnt = " rid in (select s.rid from pmroute_stops s where s.eqid = '" & eq & "') "
                Case "fu"
                    fu = lblfu.Value
                    Filter = " rid in (select s.rid from pmroute_stops s where s.pmid in " _
                    + "(select p.pmid from pmtrack p where p.funcid = ''" & fu & "'')) "
                    FilterCnt = " rid in (select s.rid from pmroute_stops s where s.pmid in " _
                    + "(select p.pmid from pmtrack p where p.funcid = '" & fu & "')) "
                    sql = "select distinct r.*, p.pmid, p.funcid from pmroutes r " _
                    + "left join pmroute_stops s on s.rid = r.rid " _
                    + "left join pmtrack p on p.pmid = s.pmid " _
                    + "where p.funcid = '" & fu & "' " & sortFilt
                Case "co"
                    co = lblco.Value
                    Filter = " rid in (select s.rid from pmroute_stops s where s.pmid in " _
                    + "(select p.pmid from pmtrack p where p.comid = ''" & co & "'')) "
                    FilterCnt = " rid in (select s.rid from pmroute_stops s where s.pmid in " _
                    + "(select p.pmid from pmtrack p where p.comid = '" & co & "')) "
                    sql = "select distinct r.*, p.pmid, p.comid from pmroutes r " _
                    + "left join pmroute_stops s on s.rid = r.rid " _
                    + "left join pmtrack p on p.pmid = s.pmid " _
                    + "where p.comid = '" & co & "' " & sortFilt
            End Select
        End If
        If rtype <> "" Then
            Filter += " and rttype = ''" & rtype & "''"
            FilterCnt += " and rttype = '" & rtype & "'"
            If rtype = "PM" Then
                rbpm.Checked = True
            ElseIf rtype = "PMMAX" Then
                rbpmj.Checked = True
            ElseIf rtype = "RBAS" Then
                rbrbas.Checked = True
            ElseIf rtype = "RBAST" Then
                rbrbast.Checked = True
            End If

        End If
        Dim sb As StringBuilder = New StringBuilder


        'sb.Append("<td  width=""20"">&nbsp;</td></tr>")
        sb.Append("<div style=""OVERFLOW: auto; width: 830px; height: 360px;"">") '<tr><td colspan=""6"">
        'sb.Append("<table width=""730"">")
        'sb.Append("<tr><td width=""150""></td><td width=""350""></td>")
        'sb.Append("<td width=""40""></td><td width=""90""></td><td width=""50""></td><td width=""50""></td></tr>")
        sb.Append("<table width=""800"">")
        If lang = "fre" Then
            sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""170""># de route</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""300"">" & tmod.getlbl("cdlbl130", "PMRouteList.aspx.vb") & "</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""40"">Temps total</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""50"">Statut</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""60"">Fr�quence</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""70"">Prochaine date</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""20""><img alt="""" src=""../images/minibuttons/admin.gif"" /></td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""20"">Historique</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""30"">" & tmod.getlbl("cdlbl133", "PMRouteList.aspx.vb") & "</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""30"">" & tmod.getlbl("cdlbl134", "PMRouteList.aspx.vb") & "</td>")
        Else
            sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""170"">" & tmod.getlbl("cdlbl129", "PMRouteList.aspx.vb") & "</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""300"">" & tmod.getlbl("cdlbl130", "PMRouteList.aspx.vb") & "</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""40"">Total Time</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""50"">Status</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""60"">Frequency</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""70"">Next Date</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""20""><img alt="""" src=""../images/minibuttons/admin.gif"" /></td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""20"">History</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""30"">" & tmod.getlbl("cdlbl133", "PMRouteList.aspx.vb") & "</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""30""><img alt="""" src=""../images/appbuttons/minibuttons/del.gif""></td>")
        End If
        'sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""150"">" & tmod.getlbl("cdlbl129", "PMRouteList.aspx.vb") & "</td>")
        'sb.Append("<td class=""thdrsingg plainlabel"" width=""350"">" & tmod.getlbl("cdlbl130", "PMRouteList.aspx.vb") & "</td>")
        'sb.Append("<td class=""thdrsingg plainlabel"" width=""40"">" & tmod.getlbl("cdlbl131", "PMRouteList.aspx.vb") & "</td>")
        'sb.Append("<td class=""thdrsingg plainlabel"" width=""90"">" & tmod.getlbl("cdlbl132", "PMRouteList.aspx.vb") & "</td>")




        'sb.Append("<td class=""thdrsingg plainlabel"" width=""50"">" & tmod.getlbl("cdlbl134", "PMRouteList.aspx.vb") & "</td>")

        Dim rowid As Integer = 0
        Dim bc, rtid, rt, desc, typ, cb As String
        If FilterCnt <> "" Then
            sql = "select count(*) from pmroutes where " & FilterCnt
        Else
            sql = "select count(*) from pmroutes"
        End If
        PageNumber = txtpg.Value
        intPgNav = rl.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav
        Dim nd As DateTime
        Dim nnd, freq, pmstr, tt, rdid, rd As String

        If rtype = "RBAS" Then
            Fields = "*, tt = ((select sum(t.ttime) from pmtasks t where t.rteid = pmroutes.rid) + isnull(xtime, 0))"
        ElseIf rtype = "RBAST" Then
            Fields = "*, tt = ((select sum(t.ttime) from pmtaskstpm t where t.rteid = pmroutes.rid) + isnull(xtime, 0))"
        End If

        dr = rl.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        Dim nddf As DateTime
        Dim gored As String
        While dr.Read
            If rowid = 0 Then
                rowid = 1
                bc = "transrow"
            Else
                rowid = 0
                bc = "transrowblue"
            End If
            tt = dr.Item("tt").ToString
            pmstr = dr.Item("pm").ToString
            freq = dr.Item("freq").ToString
            Try
                nd = dr.Item("nextdate").ToString
                'nd = DateTime.ParseExact(nd, "MM/dd/yyyy", Nothing)
                'Dim ndd As DateTime = nd.ToString
                nddf = nd.ToString
                If nddf < Now Then
                    gored = "y"
                Else
                    gored = "n"
                End If
                FormatDateTime(nd, DateFormat.ShortDate)
                nnd = nd
            Catch ex As Exception
                nnd = ""
            End Try
            'tt = "test"
            rtid = dr.Item("rid").ToString
            rdid = dr.Item("rdid").ToString
            rt = dr.Item("route").ToString
            desc = dr.Item("description").ToString
            typ = dr.Item("rttype").ToString
            cb = dr.Item("createdby").ToString
            If rdid = "1" Then
                rd = "Running"
            ElseIf rdid = "2" Then
                rd = "Down"
            End If
            'If typ = "0" Then
            'typ = "PM"
            'Else
            'typ = "JP"
            'End If
            sb.Append("<tr><td class=""linklabel " & bc & """><a href=""#"" onclick=""rtret('" & rtid & "','" & typ & "');"">")
            sb.Append(rt & "</a></td>")
            sb.Append("<td class=""plainlabel " & bc & """>" & desc & "</td>")
            sb.Append("<td class=""plainlabel " & bc & """>" & tt & "</td>")
            sb.Append("<td class=""plainlabel " & bc & """>" & rd & "</td>")
            sb.Append("<td class=""plainlabel " & bc & """>" & freq & "</td>")
            If gored = "y" Then
                sb.Append("<td class=""plainlabelred " & bc & """>" & nnd & "</td>")
            Else
                sb.Append("<td class=""plainlabel " & bc & """>" & nnd & "</td>")
            End If

            sb.Append("<td class=""plainlabel " & bc & """><a href=""#divModal"" onclick=""getsched('" & rtid & "','" & rt & "','" & desc & "','" & nnd & "','" & pmstr & "')""><img alt="""" src=""../images/minibuttons/admin.gif"" /></a></td>")
            sb.Append("<td class=""plainlabel " & bc & """><a href=""#"" onclick=""gethist('" & rtid & "')""><img alt="""" src=""../images/minibuttons/excelexp.gif"" /></a></td>")

            If rtype = "RBAS" Or rtype = "RBAST" Then
                sb.Append("<td align=""center"" class=""" & bc & """><IMG onmouseover=""return overlib('" & tmod.getov("cov143", "PMRouteList.aspx.vb") & "', ABOVE, LEFT)""")
                'sb.Append(" onclick=""printpm('" & rtid & "','" & typ & "');"" onmouseout=""return nd()"" src=""../images/appbuttons/minibuttons/printx.gif"">")
                'sb.Append("<IMG onmouseover=""return overlib('Print PM Route Output', ABOVE, LEFT)"")
                sb.Append("<IMG onclick=""printpmr('" & rtid & "','" & typ & "');"" onmouseout=""return nd()"" src=""../images/appbuttons/minibuttons/woprint.gif""></td>")
            Else
                sb.Append("<td align=""center"" class=""" & bc & """><IMG onmouseover=""return overlib('" & tmod.getov("cov143", "PMRouteList.aspx.vb") & "', ABOVE, LEFT)""")
                sb.Append(" onclick=""printpm('" & rtid & "','" & typ & "');"" onmouseout=""return nd()"" src=""../images/appbuttons/minibuttons/printx.gif""></td>")
            End If

            sb.Append("<td align=""center"" class=""" & bc & """><IMG onmouseover=""return overlib('" & tmod.getov("cov144", "PMRouteList.aspx.vb") & "', ABOVE, LEFT)""")
            sb.Append(" onclick=""delroute('" & rtid & "');"" onmouseout=""return nd()"" src=""../images/appbuttons/minibuttons/del.gif""></td></tr>")


        End While
        dr.Close()
        sb.Append("</table></div>") '</td></tr></table>
        tdlist.InnerHtml = sb.ToString
    End Sub

    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1176.Text = axlabs.GetASPXPage("PMRouteList.aspx", "lang1176")
        Catch ex As Exception
        End Try
        Try
            lang1177.Text = axlabs.GetASPXPage("PMRouteList.aspx", "lang1177")
        Catch ex As Exception
        End Try
        Try
            lang1178.Text = axlabs.GetASPXPage("PMRouteList.aspx", "lang1178")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            ovid166.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMRouteList.aspx", "ovid166") & "', ABOVE, LEFT)")
            ovid166.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid167.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMRouteList.aspx", "ovid167") & "', ABOVE, LEFT)")
            ovid167.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid168.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMRouteList.aspx", "ovid168") & "', ABOVE, LEFT)")
            ovid168.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid169.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMRouteList.aspx", "ovid169") & "', ABOVE, LEFT)")
            ovid169.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
