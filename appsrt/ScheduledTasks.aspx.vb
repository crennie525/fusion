

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class ScheduledTasks
    Inherits System.Web.UI.Page
	Protected WithEvents imgi As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents imga As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang1220 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1219 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1218 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1217 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1216 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1215 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1214 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1213 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1212 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1211 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1210 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1209 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim man As New Utilities
    Dim sid, cid, srch As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = "10"
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rptrtasks As System.Web.UI.WebControls.Repeater
    Protected WithEvents btnprev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnnext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dddepts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcells As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ibfilt As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddsuper As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txts As System.Web.UI.WebControls.TextBox
    Protected WithEvents txte As System.Web.UI.WebControls.TextBox
    Protected WithEvents trnav As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdpg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblprint As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            man.Open()
            txtpg.Value = PageNumber
            LoadSuper()
            LoadSkill()
            'GetPdM()
            GetDept()
            Try
                ddsuper.SelectedIndex = 0
            Catch ex As Exception

            End Try

            GetPg(PageNumber)
            man.Dispose()
            ddcells.Enabled = False
            lblprint.Value = "no"
        Else
            If Request.Form("lblprint") = "go" Then
                GetPg(PageNumber, "yes")
            End If
        End If
        ibfilt.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov146" , "ScheduledTasks.aspx.vb") & "', BELOW, LEFT)")
        ibfilt.Attributes.Add("onmouseout", "return nd()")

    End Sub
    Private Sub GetCell()
        sid = HttpContext.Current.Session("dfltps").ToString
        Dim dept As String = dddepts.SelectedValue.ToString
        sql = "select c.cellid, c.cell_name from cells c where c.dept_id = '" & dept & "'"
        dr = man.GetRdrData(sql)
        ddcells.DataSource = dr
        ddcells.DataValueField = "cellid"
        ddcells.DataTextField = "cell_name"
        ddcells.DataBind()
        dr.Close()
        'ddskill.Items.Insert(0, "Select Skill")
        ddcells.Items.Insert(0, "All Cells")
        Try
            ddcells.SelectedIndex = 0
        Catch ex As Exception

        End Try

        ddcells.Enabled = True
    End Sub
    Private Sub GetDept()
        sid = HttpContext.Current.Session("dfltps").ToString
        sql = "select d.dept_id, d.dept_line from dept d where d.siteid = '" & sid & "'"
        dr = man.GetRdrData(sql)
        dddepts.DataSource = dr
        dddepts.DataValueField = "dept_id"
        dddepts.DataTextField = "dept_line"
        dddepts.DataBind()
        dr.Close()
        dddepts.Items.Insert(0, "All Departments")
        Try
            dddepts.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub LoadSuper()
        sql = "select distinct p.super, p.superid from pm p left join pmsuper s on p.superid = s.superid " _
        + " where(p.super Is Not null)"
        dr = man.GetRdrData(sql)
        ddsuper.DataSource = dr
        ddsuper.DataTextField = "super"
        ddsuper.DataValueField = "superid"
        ddsuper.DataBind()
        dr.Close()
        ddsuper.Items.Insert(0, New ListItem("Select All"))
        ddsuper.Items(0).Value = 0

    End Sub
    Private Sub LoadSkill()
        sql = "select distinct p.skillid, p.skill from pm p left join equipment e on e.eqid = p.eqid"
        dr = man.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataValueField = "skillid"
        ddskill.DataTextField = "skill"
        ddskill.DataBind()
        dr.Close()
        'ddskill.Items.Insert(0, "Select Skill")
        ddskill.Items.Insert(0, "All Skills")
        Try
            ddskill.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub GetPg(ByVal PageNumber As Integer, Optional ByVal pall As String = "no")
        Dim filt, s, e As String
        sid = HttpContext.Current.Session("dfltps").ToString
        cid = "0"
        Dim sup, nex, las As String
        Dim d As Integer = ddsuper.SelectedIndex
        If ddsuper.SelectedIndex <> 0 Then
            filt = " supervisor = '" & ddsuper.SelectedItem.ToString & "'"
            srch = " supervisor = ''" & ddsuper.SelectedItem.ToString & "''"
        End If
        If dddepts.SelectedIndex <> 0 Then
            If Len(filt) <> 0 Then
                filt += " and "
                srch += " and "
            End If
            filt += " deptid = '" & dddepts.SelectedValue.ToString & "'"
            srch += " deptid = ''" & dddepts.SelectedValue.ToString & "''"
            If ddcells.SelectedIndex <> 0 And ddcells.SelectedIndex <> -1 Then
                filt += " and cellid = '" & ddcells.SelectedValue.ToString & "'"
                srch += " and cellid = ''" & ddcells.SelectedValue.ToString & "''"
            End If
        End If
        If ddskill.SelectedIndex <> 0 Then
            If Len(filt) <> 0 Then
                filt += " and "
                srch += " and "
            End If
            filt += " skillid = '" & ddskill.SelectedValue.ToString & "'"
            srch += " skillid = ''" & ddskill.SelectedValue.ToString & "''"
        End If
       
        s = txts.Text
        e = txte.Text
        If Len(s) <> 0 Then
            If Len(e) <> 0 Then
                If Len(filt) <> 0 Then
                    filt += " and "
                    srch += " and "
                End If
                filt += "nextdate between '" & s & "' and '" & e & "'"
                srch += "nextdate between ''" & s & "'' and ''" & e & "''"
            Else
                If Len(filt) <> 0 Then
                    filt += " and "
                    srch += " and "
                End If
                filt += "nextdate >= '" & s & "'"
                srch += "nextdate >= ''" & s & "''"
            End If
        End If
        If pall = "no" Then
            Dim intPgCnt, intPgNav As Integer
            If Len(filt) = 0 Then
                sql = "select count(jpnum) from pmjobplans where siteid = '" & sid & "' and nextdate is not null"
                srch = " nextdate is not null"
            Else
                sql = "select count(jpnum) from pmjobplans where siteid = '" & sid & "' and " & filt
            End If


            intPgCnt = man.Scalar(sql)


            If intPgCnt > 0 Then
                'tdmsg.Attributes.Add("class", "details")
                'trdata.Attributes.Add("class", "view")
                'trnav.Attributes.Add("class", "view")
                txtpg.Value = PageNumber
                intPgNav = man.PageCount1(intPgCnt, PageSize)
                'srch = filt
                sql = "usp_getalljppg '" & sid & "', '" & PageNumber & "', '" & PageSize & "', '" & srch & "'"
                Dim ds As New DataSet
                'sql = "select distinct pm.pmid, e.eqnum, " _
                '+ "pmd = (" _
                '+ "case pm.ptid " _
                '+ "when 0 then 'None' " _
                '+ "else pm.pretech " _
                '+ "End " _
                '+ "), " _
                '+ "pm = (pm.skill + '/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd), " _
                '+ "Convert(char(10), pm.lastdate,101) as 'lastdate', Convert(char(10), pm.nextdate,101) as 'nextdate' " _
                '+ "from pm pm left join equipment e on e.eqid = pm.eqid " _
                '+ "where e.siteid = '" & sid & "' and nextdate is not null order by nextdate"
                ds = man.GetDSData(sql)
                Dim dt As New DataTable
                dt = ds.Tables(0)
                rptrtasks.DataSource = dt
                rptrtasks.DataBind()

                Dim i, eint As Integer
                eint = 15
                'For i = cnt To eint
                'dt.Rows.InsertAt(dt.NewRow(), i)
                'Next
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr504" , "ScheduledTasks.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            'updatepos(intPgNav)
            lblprint.Value = "no"
        Else
            lblfilt.Value = " where siteid = '" & sid & "' and " & filt
            lblprint.Value = "yes"
        End If


    End Sub
    Private Sub updatepos(ByVal intPgNav)
        PageNumber = txtpg.Value
        If intPgNav = 0 Then
            tdpg.InnerHtml = "Page 0 of 0"
        Else
            tdpg.InnerHtml = "Page " & PageNumber & " of " & intPgNav
        End If

        If PageNumber = 1 And intPgNav = 1 Then
            btnprev.Enabled = False
            btnnext.Enabled = False

        ElseIf PageNumber = 1 And intPgNav > 1 Then
            btnprev.Enabled = False
            btnnext.Enabled = True

        ElseIf PageNumber = intPgNav Then
            btnnext.Enabled = False
            btnprev.Enabled = True

        Else
            btnnext.Enabled = True
            btnprev.Enabled = True
        End If
    End Sub

    Private Sub btnnext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnnext.Click
        Dim pg As Integer = txtpg.Value
        PageNumber = pg + 1
        txtpg.Value = PageNumber
        man.Open()
        GetPg(PageNumber)
        man.Dispose()
    End Sub

    Private Sub btnprev_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnprev.Click
        Dim pg As Integer = txtpg.Value
        PageNumber = pg - 1
        txtpg.Value = PageNumber
        man.Open()
        GetPg(PageNumber)
        man.Dispose()
    End Sub

    Private Sub rptrtasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtasks.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Then
            Dim img As HtmlImage = CType(e.Item.FindControl("imgi"), HtmlImage)
            Dim link As LinkButton = CType(e.Item.FindControl("lbltn"), LinkButton)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "jpid").ToString
            img.Attributes("onclick") = "print('" & id & "');"
            link.Attributes("onclick") = "jump('" & id & "','st');"
        ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim img As HtmlImage = CType(e.Item.FindControl("imga"), HtmlImage)
            Dim link As LinkButton = CType(e.Item.FindControl("lbltnalt"), LinkButton)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "jpid").ToString
            img.Attributes("onclick") = "print('" & id & "');"
            link.Attributes("onclick") = "jump('" & id & "','st');"
        End If
    
If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
		Try
                Dim lang1209 As Label
			lang1209 = CType(e.Item.FindControl("lang1209"), Label)
			lang1209.Text = axlabs.GetASPXPage("ScheduledTasks.aspx","lang1209")
		Catch ex As Exception
		End Try
		Try
                Dim lang1210 As Label
			lang1210 = CType(e.Item.FindControl("lang1210"), Label)
			lang1210.Text = axlabs.GetASPXPage("ScheduledTasks.aspx","lang1210")
		Catch ex As Exception
		End Try
		Try
                Dim lang1211 As Label
			lang1211 = CType(e.Item.FindControl("lang1211"), Label)
			lang1211.Text = axlabs.GetASPXPage("ScheduledTasks.aspx","lang1211")
		Catch ex As Exception
		End Try
		Try
                Dim lang1212 As Label
			lang1212 = CType(e.Item.FindControl("lang1212"), Label)
			lang1212.Text = axlabs.GetASPXPage("ScheduledTasks.aspx","lang1212")
		Catch ex As Exception
		End Try
		Try
                Dim lang1213 As Label
			lang1213 = CType(e.Item.FindControl("lang1213"), Label)
			lang1213.Text = axlabs.GetASPXPage("ScheduledTasks.aspx","lang1213")
		Catch ex As Exception
		End Try
		Try
                Dim lang1214 As Label
			lang1214 = CType(e.Item.FindControl("lang1214"), Label)
			lang1214.Text = axlabs.GetASPXPage("ScheduledTasks.aspx","lang1214")
		Catch ex As Exception
		End Try
		Try
                Dim lang1215 As Label
			lang1215 = CType(e.Item.FindControl("lang1215"), Label)
			lang1215.Text = axlabs.GetASPXPage("ScheduledTasks.aspx","lang1215")
		Catch ex As Exception
		End Try
		Try
                Dim lang1216 As Label
			lang1216 = CType(e.Item.FindControl("lang1216"), Label)
			lang1216.Text = axlabs.GetASPXPage("ScheduledTasks.aspx","lang1216")
		Catch ex As Exception
		End Try
		Try
                Dim lang1217 As Label
			lang1217 = CType(e.Item.FindControl("lang1217"), Label)
			lang1217.Text = axlabs.GetASPXPage("ScheduledTasks.aspx","lang1217")
		Catch ex As Exception
		End Try
		Try
                Dim lang1218 As Label
			lang1218 = CType(e.Item.FindControl("lang1218"), Label)
			lang1218.Text = axlabs.GetASPXPage("ScheduledTasks.aspx","lang1218")
		Catch ex As Exception
		End Try
		Try
                Dim lang1219 As Label
			lang1219 = CType(e.Item.FindControl("lang1219"), Label)
			lang1219.Text = axlabs.GetASPXPage("ScheduledTasks.aspx","lang1219")
		Catch ex As Exception
		End Try
		Try
                Dim lang1220 As Label
			lang1220 = CType(e.Item.FindControl("lang1220"), Label)
			lang1220.Text = axlabs.GetASPXPage("ScheduledTasks.aspx","lang1220")
		Catch ex As Exception
		End Try

End If

 End Sub

    Private Sub ibfilt_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibfilt.Click
        man.Open()
        GetPg(PageNumber)
        man.Dispose()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1209.Text = axlabs.GetASPXPage("ScheduledTasks.aspx", "lang1209")
        Catch ex As Exception
        End Try
        Try
            lang1210.Text = axlabs.GetASPXPage("ScheduledTasks.aspx", "lang1210")
        Catch ex As Exception
        End Try
        Try
            lang1211.Text = axlabs.GetASPXPage("ScheduledTasks.aspx", "lang1211")
        Catch ex As Exception
        End Try
        Try
            lang1212.Text = axlabs.GetASPXPage("ScheduledTasks.aspx", "lang1212")
        Catch ex As Exception
        End Try
        Try
            lang1213.Text = axlabs.GetASPXPage("ScheduledTasks.aspx", "lang1213")
        Catch ex As Exception
        End Try
        Try
            lang1214.Text = axlabs.GetASPXPage("ScheduledTasks.aspx", "lang1214")
        Catch ex As Exception
        End Try
        Try
            lang1215.Text = axlabs.GetASPXPage("ScheduledTasks.aspx", "lang1215")
        Catch ex As Exception
        End Try
        Try
            lang1216.Text = axlabs.GetASPXPage("ScheduledTasks.aspx", "lang1216")
        Catch ex As Exception
        End Try
        Try
            lang1217.Text = axlabs.GetASPXPage("ScheduledTasks.aspx", "lang1217")
        Catch ex As Exception
        End Try
        Try
            lang1218.Text = axlabs.GetASPXPage("ScheduledTasks.aspx", "lang1218")
        Catch ex As Exception
        End Try
        Try
            lang1219.Text = axlabs.GetASPXPage("ScheduledTasks.aspx", "lang1219")
        Catch ex As Exception
        End Try
        Try
            lang1220.Text = axlabs.GetASPXPage("ScheduledTasks.aspx", "lang1220")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            Img1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("ScheduledTasks.aspx", "Img1") & "')")
            Img1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imga.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("ScheduledTasks.aspx", "imga") & "', ABOVE, LEFT)")
            imga.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgi.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("ScheduledTasks.aspx", "imgi") & "', ABOVE, LEFT)")
            imgi.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
