﻿Imports System.Data.SqlClient
Imports System.Text
Public Class pmrrtelkup
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim sql As String
    Dim dr As SqlDataReader
    Dim lu As New Utilities
    Dim list, sid, retid, retid1, retid2, retid3, retid4, retid5, retid6, eq, pm, jp, did, clid, locid, nc As String
    Dim skillid, eqid, skillqty, freq, rdid, pmstr, typ, typx, pmid, rtid As String
    Dim filter As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 100

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString '"14" '
            lblsid.Value = sid
            typ = Request.QueryString("typ").ToString '"14" '
            lbltyp.Value = typ
            If typ = "RBASWO" Then
                pmid = Request.QueryString("pmid").ToString '"14" '
                lblpmid.Value = pmid
            End If
            Try
                typx = Request.QueryString("typx").ToString '"14" '
                lbltypx.Value = typx
            Catch ex As Exception

            End Try
            lu.Open()
            GetRoutes(typ)
            lu.Dispose()
        Else
            If lblret.Value = "wo" Then
                lu.Open()
                savertid()
                lu.Dispose()
            End If
        End If
    End Sub
    Private Sub savertid()
        pmid = lblpmid.Value
        rtid = lblrtid.Value
        sql = "update PM set rtid = '" & rtid & "' where pmid = '" & pmid & "'"
        lu.Update(sql)
        Dim strMessage As String = "Route Saved As Attachment"
        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
    End Sub
    Protected Sub btnsrch1_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnsrch1.Click
        Dim srch As String
        srch = txtsrch.Text
        typ = lbltyp.Value
        'If srch <> "" Then
        lu.Open()
        GetRoutes(typ)
        lu.Dispose()
        'End If

    End Sub
    Private Sub GetRoutes(ByVal typ As String)
        Dim srch As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)

        Dim sb As StringBuilder = New StringBuilder
        sid = lblsid.Value
        Dim typr As String
        If typ = "RBASM2" Or typ = "RBASM" Or typ = "RBAS" Or typ = "RBASM3" Or typ = "RBASM3NP" Or typ = "RBASWO" Then
            typr = "RBAS"
        ElseIf typ = "RBASTM2" Or typ = "RBASTM" Or typ = "RBAST" Or typ = "RBASTM3" Or typ = "RBASTM3NP" Then
            typr = "RBAST"
        End If
        If srch = "" Then
            sql = "select distinct * from pmroutes where siteid = '" & sid & "' and rttype = '" & typr & "'"
        Else
            srch = "%" & srch & "%"
            sql = "select distinct * from pmroutes where siteid = '" & sid & "' and rttype = '" & typr & "' and " _
            + "(route like '" & srch & "' or createdby like '" & srch & "') "
        End If

        sb.Append("<table width=""580"">")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""190"">" & tmod.getlbl("cdlbl143", "PMRouteLU.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""370"">" & tmod.getlbl("cdlbl144", "PMRouteLU.aspx.vb") & "</td></tr>")
        'sb.Append("<td class=""details"" width=""20"">&nbsp;</td></tr>")
        sb.Append("<tr><td colspan=""3""><div style=""OVERFLOW: auto; width: 580px; height: 350px;"">")
        sb.Append("<table width=""560"">")
        sb.Append("<tr><td width=""190""></td><td width=""370""></td></tr>")
        Dim rowid As Integer = 0
        Dim bc, rtid, rt, desc As String
        dr = lu.GetRdrData(sql)
        While dr.Read
            If rowid = 0 Then
                rowid = 1
                bc = "white"
            Else
                rowid = 0
                bc = "#E7F1FD"
            End If
            rtid = dr.Item("rid").ToString
            rt = dr.Item("route").ToString
            desc = dr.Item("description").ToString
            If typ = "RBASWO" Then
                sb.Append("<tr bgcolor=""" & bc & """><td class=""linklabel""><a href=""#"" onclick=""rtretwo('" & typ & "','" & rtid & "');"">")
            Else
                sb.Append("<tr bgcolor=""" & bc & """><td class=""linklabel""><a href=""#"" onclick=""rtret('" & typ & "','" & rtid & "');"">")
            End If

            sb.Append(rt & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & desc & "</td></tr>")

        End While
        dr.Close()
        sb.Append("</table></div></td></tr></table>")
        tdlist.InnerHtml = sb.ToString

        'rep.Text = "What?"

    End Sub


End Class