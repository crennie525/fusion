<%@ Page Language="vb" AutoEventWireup="false" Codebehind="jobrefscope.aspx.vb" Inherits="lucy_r12.jobrefscope" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Job Request Scope</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/jobrefscopeaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
        <script language="javascript" type="text/javascript">
            function remreq() {
                document.getElementById("lblsubmit").value = "remreq";
                document.getElementById("form1").submit();
            }
            function checkit() {
                var log = document.getElementById("lbllog").value;
                if (log == "no") {
                    window.close();
                }
                else {
                    window.setTimeout("setref();", 1205000);
                }
                var chk = document.getElementById("lblsubmit").value;

                if (chk == "retreq") {
                    var id = document.getElementById("lbljpref").value;
                    window.parent.handlereturn(id);
                }
                else if (chk == "retonly") {
                    window.parent.handlereturn("none");
                }
                else if (chk == "rem") {
                    window.parent.handlereturn("rem");
                }
            }
        </script>
	</HEAD>
	<body onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td><asp:textbox id="txtscope" runat="server" CssClass="plainlabel" TextMode="MultiLine" Height="168px"
							Width="384px"></asp:textbox></td>
				</tr>
				<tr>
					<td align="right"><img  src="../images/appbuttons/minibuttons/savedisk1.gif" onclick="getreq();">
                    <img  src="../images/appbuttons/minibuttons/candisk1.gif" onclick="remreq();"></td>
				</tr>
			</table>
			<input id="lbljpref" type="hidden" runat="server" NAME="lblptid"><input type="hidden" id="lbllog" runat="server" NAME="lbllog">
			<input type="hidden" id="lblsubmit" runat="server"> <input type="hidden" id="lblwonum" runat="server">
			<input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
