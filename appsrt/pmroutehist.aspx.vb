﻿Imports System.Data.SqlClient
Public Class pmroutehist
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim sqa As New Utilities
    Dim sessid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sessid = Request.QueryString("rtid").ToString
        sqa.Open()
        ExportDG(sessid)
        sqa.Dispose()
    End Sub
    Private Sub ExportDG(ByVal sessid As String)
        BindExport(sessid)
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
    Private Sub BindExport(ByVal sessid As String)
        Dim osql As String
        sql = "select r.route, r.description, r.pm, h.nextdate, h.compdate, h.compby from pmroutes r left join pmrouteshist h on h.rid = r.rid where h.rid = '" & sessid & "'"
        Dim ds As New DataSet
        ds = sqa.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
    End Sub
End Class