<%@ Page Language="vb" AutoEventWireup="false" Codebehind="JobPlanList.aspx.vb" Inherits="lucy_r12.JobPlanList" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>JobPlanList</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="JavaScript" src="../scripts1/JobPlanListaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
        <script language="javascript" type="text/javascript">
            function rtret(id, ret) {
                var typ = document.getElementById("lblret").value;
                //alert(typ)
                if (typ == "wo") {
                    document.getElementById("lblval1").value = id;
                    document.getElementById("lblval2").value = ret;
                    document.getElementById("lblsubmit").value = "savewo";
                    document.getElementById("form1").submit();
                    //window.parent.handlert(id, ret);
                }
                else {
                    window.parent.handlert(id, ret);
                }
            }
            function checkit() {
                var chk = document.getElementById("lblsubmit").value;
                //alert(chk)
                if (chk == "return") {
                    var val1 = document.getElementById("lblval1").value;
                    var val2 = document.getElementById("lblval2").value;
                    window.parent.handlert(val1, val2);
                }
            }
            function delpm(id) {
                var conf = confirm("Are You Sure You Want to Delete This Job Plan?")
                if (conf == true) {
                    document.getElementById("lbldelid").value = id;
                    document.getElementById("lblsubmit").value = "delpm";
                    document.getElementById("form1").submit();
                }
                else {
                    alert("Action Cancelled")
                }
               
            }
        </script>
	</HEAD>
	<body class="tbg" onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; RIGHT: 5px; LEFT: 5px" width="770">
				<tr>
					<td width="120"></td>
					<td width="230"></td>
					<td width="20"></td>
					<td width="20"></td>
					<td width="20"></td>
					<td width="320"></td>
				</tr>
				<tr>
					<td class="thdrsingg plainlabel" colSpan="6"><asp:label id="lang1148" runat="server">Job Plan Options</asp:label></td>
				</tr>
				<tr>
					<td class="label"><asp:label id="lang1149" runat="server">Search Job Plans</asp:label></td>
					<td><asp:textbox id="txtsrch" runat="server" Width="220"></asp:textbox></td>
					<td><IMG onmouseover="return overlib('Search Job Plans', ABOVE, LEFT)" onmouseout="return nd()"
							onclick="srchrts();" src="../images/appbuttons/minibuttons/srchsm.gif"></td>
					<td><IMG id="imgadd" onmouseover="return overlib('Add a Job Plan', ABOVE, LEFT)" onmouseout="return nd()"
							onclick="addrt('no');" src="../images/appbuttons/minibuttons/addnew.gif" runat="server"></td>
					<td colSpan="2">&nbsp;</td>
				</tr>
				<tr>
					<td class="label"><asp:label id="lang1150" runat="server">Sort By</asp:label></td>
					<td class="plainlabel"><asp:radiobuttonlist id="rbsort" runat="server" RepeatDirection="Horizontal" Height="20px"
							CssClass="plainlabel">
							<asp:ListItem Value="0" Selected="True">Job Plan#</asp:ListItem>
							<asp:ListItem Value="1">Type</asp:ListItem>
							<asp:ListItem Value="2">Created By</asp:ListItem>
						</asp:radiobuttonlist></td>
					<td><IMG onmouseover="return overlib('Sort Ascending', ABOVE, LEFT)" onmouseout="return nd()"
							onclick="sort('asc');" src="../images/appbuttons/minibuttons/sasc.gif"></td>
					<td><IMG onmouseover="return overlib('Sort Descending', ABOVE, LEFT)" onmouseout="return nd()"
							onclick="sort('desc');" src="../images/appbuttons/minibuttons/sdesc.gif"></td>
				</tr>
				<tr>
					<td id="tdlist" colSpan="6" runat="server"></td>
				</tr>
				<tr>
					<td colSpan="6" align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lblsid" type="hidden" name="lblsid" runat="server"> <input id="lblsubmit" type="hidden" name="lblsubmit" runat="server">
			<input id="lblsort" type="hidden" name="lblsort" runat="server"><input id="lbltyp" type="hidden" name="lbltyp" runat="server">
			<input id="lbleqid" type="hidden" name="lbleqid" runat="server"> <input id="lblfu" type="hidden" name="lblfu" runat="server">
			<input id="lblco" type="hidden" name="lblco" runat="server"><input id="lblret" type="hidden" runat="server">
			<input id="txtpg" type="hidden" runat="server"> <input id="txtpgcnt" type="hidden" runat="server">
			<input id="lblfslang" type="hidden" runat="server"> <input id="lblwonum" type="hidden" runat="server">
			<input id="lblsavewo" type="hidden" runat="server"> <input id="lblval1" type="hidden" runat="server">
			<input type="hidden" id="lblval2" runat="server">
            <input type="hidden" id="lbldelid" runat="server" />
		</form>
	</body>
</HTML>
