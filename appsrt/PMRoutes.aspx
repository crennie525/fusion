<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PMRoutes.aspx.vb" Inherits="lucy_r12.PMRoutes" %>
<%@ Register TagPrefix="uc1" TagName="menu1" Src="../menu/menu1.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PMRoutes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts/dragitem.js"></script>
		<script language="JavaScript" src="../scripts1/PMRoutesaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<uc1:menu1 id="Menu11" runat="server"></uc1:menu1>
		<form id="form1" method="post" runat="server">
			<table width="978" border="0">
				<TBODY>
					<tr height="24">
						<td class="thdrsing label" colSpan="9"><asp:Label id="lang1180" runat="server">Add/Edit Routes</asp:Label></td>
					</tr>
					<tr>
						<td align="center" width="20"><IMG src="../images/appbuttons/minibuttons/routenum.gif"></td>
						<td id="tdjp" width="180"><asp:textbox id="txtroute" runat="server" Width="170px"></asp:textbox></td>
						<td width="20"><IMG src="../images/appbuttons/minibuttons/routeblnk.gif"></td>
						<td class="label" width="80"><asp:Label id="lang1181" runat="server">Description</asp:Label></td>
						<td width="320"><asp:textbox id="txtroutedesc" runat="server" Width="310px"></asp:textbox></td>
						<td width="20"><IMG src="../images/appbuttons/minibuttons/routeblnk.gif"></td>
						<td class="label" width="40"><asp:Label id="lang1182" runat="server">Type</asp:Label></td>
						<td width="100"><asp:dropdownlist id="ddtyp" runat="server">
								<asp:ListItem Value="0" Selected="True">Select</asp:ListItem>
								<asp:ListItem Value="PM">PM</asp:ListItem>
								<asp:ListItem Value="JP">Job Plan</asp:ListItem>
								<asp:ListItem Value="EQ">Equipment</asp:ListItem>
								<asp:ListItem Value="NC">Misc. Asset</asp:ListItem>
								<asp:ListItem Value="DC">Department\Cell</asp:ListItem>
								<asp:ListItem Value="LO">Location</asp:ListItem>
							</asp:dropdownlist></td>
						<td width="80"><IMG onmouseover="return overlib('Create a Route with the Name and Decription You Have Entered')"
								onclick="pmmanual();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/addnew.gif"
								id="imgadd" runat="server"><IMG onmouseover="return overlib('View Equipment Route Record List')" onclick="getrt('rt');"
								onmouseout="return nd()" src="../images/appbuttons/minibuttons/globem.gif"> <IMG onmouseover="return overlib('Save Changes to Route# and Description')" onclick="savetop();"
								onmouseout="return nd()" src="../images/appbuttons/minibuttons/savedisk1.gif">
						</td>
					</tr>
					<tr height="24">
						<td class="thdrsing label" colSpan="9"><asp:Label id="lang1183" runat="server">Current Route</asp:Label></td>
						</TD></tr>
					<tr>
						<td colSpan="9">
							<table width="880">
								<tr>
									<td class="bluelabel" width="70"><asp:Label id="lang1184" runat="server">Route#</asp:Label></td>
									<td class="plainlabel" id="tdrt" width="170" runat="server"></td>
									<td class="bluelabel" width="70"><asp:Label id="lang1185" runat="server">Description:</asp:Label></td>
									<td class="plainlabel" id="tdrtd" width="330" runat="server"></td>
									<td align="right" width="70"><input class="plainlabel lilmenu" id="btnsub" onmouseover="this.className='plainlabel lilmenuhov'"
											style="WIDTH: 65px; HEIGHT: 24px" onclick="addrt();" onmouseout="this.className='plainlabel lilmenu'" type="button"
											value="Add Seq#" name="btnsub" runat="server">
									</td>
									<td id="tdret" align="right" width="70" runat="server"><input class="plainlabel lilmenu" id="btnsub1" onmouseover="this.className='plainlabel lilmenuhov'"
											style="WIDTH: 65px; HEIGHT: 24px" onclick="retpm();" onmouseout="this.className='plainlabel lilmenu'" type="button" value="Return" name="btnsub"
											runat="server">
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</TBODY>
			</table>
			<table>
				<TBODY>
					<tr>
						<td colSpan="9"><asp:datalist id="dllist" runat="server">
								<HeaderTemplate>
									<table width="1140">
										<tr>
											<td width="30"></td>
											<td width="50"></td>
											<td width="140"></td>
											<td width="20"></td>
											<td width="140"></td>
											<td width="20"></td>
											<td width="140"></td>
											<td width="20"></td>
											<td width="140"></td>
											<td width="20"></td>
											<td width="180"></td>
											<td width="20"></td>
											<td width="180"></td>
											<td width="20"></td>
											<td width="20"></td>
										</tr>
										<tr>
											<td class="thdrsingg plainlabel"><asp:Label id="lang1186" runat="server">Edit</asp:Label></td>
											<td class="thdrsingg plainlabel">Seq#</td>
											<td class="thdrsingg plainlabel" id="tdeqt" runat="server"><asp:Label id="lang1187" runat="server">Equipment#</asp:Label></td>
											<td class="thdrsingg plainlabel" id="tdeqti" runat="server"><img src="../images/appbuttons/minibuttons/magnifier.gif"></td>
											<td class="thdrsingg plainlabel" id="tdnct" runat="server"><asp:Label id="lang1188" runat="server">Misc. Asset</asp:Label></td>
											<td class="thdrsingg plainlabel" id="tdncti" runat="server"><img src="../images/appbuttons/minibuttons/magnifier.gif"></td>
											<td class="thdrsingg plainlabel" id="tddt" runat="server"><asp:Label id="lang1189" runat="server">Department\Cell</asp:Label></td>
											<td class="thdrsingg plainlabel" id="tddti" runat="server"><img src="../images/appbuttons/minibuttons/magnifier.gif"></td>
											<td class="thdrsingg plainlabel" id="tdloct" runat="server"><asp:Label id="lang1190" runat="server">Location</asp:Label></td>
											<td class="thdrsingg plainlabel" id="tdlocti" runat="server"><img src="../images/appbuttons/minibuttons/magnifier.gif"></td>
											<td class="thdrsingg plainlabel" id="tdpmt" runat="server">PM</td>
											<td class="thdrsingg plainlabel" id="tdpmti" runat="server"><img src="../images/appbuttons/minibuttons/magnifier.gif"></td>
											<td class="thdrsingg plainlabel" id="tdjpt" runat="server"><asp:Label id="lang1191" runat="server">Job Plan</asp:Label></td>
											<td class="thdrsingg plainlabel" id="tdjpti" runat="server"><img src="../images/appbuttons/minibuttons/magnifier.gif"></td>
											<td class="thdrsingg plainlabel" id="tddel" runat="server"><img src="../images/appbuttons/minibuttons/del.gif"></td>
										</tr>
								</HeaderTemplate>
								<ItemTemplate>
									<tr>
										<td><asp:ImageButton id="Imagebutton2" runat="server" ToolTip="Edit Record" CommandName="Edit" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"></asp:ImageButton></td>
										<TD>
											<asp:label  Width="50px" CssClass="plainlabel" id="lblseq" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.stopsequence") %>'>
											</asp:label></TD>
										<TD id="tdeqi" runat="server">
											<asp:label   CssClass="plainlabel" id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
											</asp:label></TD>
										<td align="center" id="tdeqii" runat="server"><IMG id="Img2" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<TD id="tdnci" runat="server">
											<asp:label   CssClass="plainlabel" id="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ncnum") %>'>
											</asp:label></TD>
										<td align="center" id="tdncii" runat="server"><IMG id="Img3" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<TD id="tddi" runat="server">
											<asp:label   CssClass="plainlabel" id="Label6" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dcell") %>'>
											</asp:label></TD>
										<td align="center" id="tddii" runat="server"><IMG id="Img4" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<TD id="tdloci" runat="server">
											<asp:label   CssClass="plainlabel" id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
											</asp:label></TD>
										<td align="center" id="tdlocii" runat="server"><IMG id="Img8" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<TD id="tdpmi" runat="server">
											<asp:label   CssClass="plainlabel" id="Label13" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmnum") %>'>
											</asp:label></TD>
										<td align="center" id="tdpmii" runat="server"><IMG id="Img12" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<TD id="tdjpi" runat="server">
											<asp:label   CssClass="plainlabel" id="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.jpnum") %>'>
											</asp:label></TD>
										<td align="center" id="tdjpii" runat="server"><IMG id="Img13" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<td align="center"><asp:ImageButton id="ii" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton></td>
										<td class="details"><asp:textbox   CssClass="plainlabel" id="lblrsidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rsid") %>'>
											</asp:textbox></td>
									</tr>
								</ItemTemplate>
								<AlternatingItemTemplate>
									<tr bgcolor="#E7F1FD">
										<td><asp:ImageButton id="Imagebutton6" runat="server" ToolTip="Edit Record" CommandName="Edit" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"></asp:ImageButton></td>
										<TD>
											<asp:label  Width="50px" CssClass="plainlabel" id="lblseqa" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.stopsequence") %>'>
											</asp:label></TD>
										<TD id="tdeqa" runat="server">
											<asp:label   CssClass="plainlabel" id="Label9" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
											</asp:label></TD>
										<td align="center" id="tdeqai" runat="server"><IMG id="Img5" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<TD id="tdnca" runat="server">
											<asp:label   CssClass="plainlabel" id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ncnum") %>'>
											</asp:label></TD>
										<td align="center" id="tdncai" runat="server"><IMG id="Img11" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<TD id="tdda" runat="server">
											<asp:label   CssClass="plainlabel" id="Label7" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dcell") %>'>
											</asp:label></TD>
										<td align="center" id="tddai" runat="server"><IMG id="Img6" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<TD id="tdloca" runat="server">
											<asp:label   CssClass="plainlabel" id="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
											</asp:label></TD>
										<td align="center" id="tdlocai" runat="server"><IMG id="Img7" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<TD id="tdpma" runat="server">
											<asp:label   CssClass="plainlabel" id="Label15" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmnum") %>'>
											</asp:label></TD>
										<td align="center" id="tdpmai" runat="server"><IMG id="Img14" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<TD id="tdjpa" runat="server">
											<asp:label   CssClass="plainlabel" id="Label16" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.jpnum") %>'>
											</asp:label></TD>
										<td align="center" id="tdjpai" runat="server"><IMG id="Img15" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<td align="center"><asp:ImageButton id="ia" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton></td>
										<td class="details"><asp:textbox   CssClass="plainlabel" id="lblrsida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rsid") %>'>
											</asp:textbox></td>
									</tr>
								</AlternatingItemTemplate>
								<EditItemTemplate>
									<tr>
										<td><asp:ImageButton id="Imagebutton3" runat="server" ToolTip="Save Record" CommandName="Update" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:ImageButton>
											<asp:imagebutton id="Imagebutton5" runat="server" ToolTip="Cancel Changes" CommandName="Cancel" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"></asp:imagebutton></td>
										<TD>
											<asp:textbox  Width="40px" CssClass="plainlabel" id="txtseq" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.stopsequence") %>'>
											</asp:textbox></TD>
										<TD id="tdeqe" runat="server">
											<asp:label   CssClass="plainlabel" id="lbleqs" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
											</asp:label></TD>
										<td align="center" id="tdeqei" runat="server"><IMG id="lookupeq" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<TD id="tdnce" runat="server">
											<asp:label   CssClass="plainlabel" id="Label10" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ncnum") %>'>
											</asp:label></TD>
										<td align="center" id="tdncei" runat="server"><IMG id="nclookup" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<TD id="tdde" runat="server">
											<asp:label   CssClass="plainlabel" id="Label11" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dcell") %>'>
											</asp:label></TD>
										<td align="center" id="tddei" runat="server"><IMG id="dcelllookup" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<TD id="tdloce" runat="server">
											<asp:label   CssClass="plainlabel" id="lbllocs" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
											</asp:label></TD>
										<td align="center" id="tdlocei" runat="server"><IMG id="lookuploc" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<TD id="tdpme" runat="server">
											<asp:label   CssClass="plainlabel" id="lblpms" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmnum") %>'>
											</asp:label></TD>
										<td align="center" id="tdpmei" runat="server"><IMG id="lookuppm" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<TD id="tdjpe" runat="server">
											<asp:label   CssClass="plainlabel" id="lbljps" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.jpnum") %>'>
											</asp:label></TD>
										<td align="center" id="tdjpei" runat="server"><IMG id="lookupjp" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif"></td>
										<td align="center"><asp:ImageButton id="ie" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton></td>
										<td class="details"><asp:textbox   CssClass="plainlabel" id="lbleqid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqid") %>'>
											</asp:textbox></td>
										<td class="details"><asp:textbox   CssClass="plainlabel" id="lblpmid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmid") %>'>
											</asp:textbox></td>
										<td class="details"><asp:textbox   CssClass="plainlabel" id="lbljpid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.jpid") %>'>
											</asp:textbox></td>
										<td class="details"><asp:textbox   CssClass="plainlabel" id="lblrsid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rsid") %>'>
											</asp:textbox></td>
										<td class="details"><asp:textbox   CssClass="plainlabel" id="lbllocid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.locid") %>'>
											</asp:textbox></td>
									</tr>
								</EditItemTemplate>
								<FooterTemplate>
			</table>
			</FooterTemplate> </asp:datalist></TD></TR></TBODY></TABLE>
			<div onmouseup="mouseUp();restore('ifjp', 'trjp');" class="details" onmousedown="makeDraggable(this, 'ifjp', 'trjp');"
				id="jpdiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 999; BORDER-LEFT: black 1px solid; WIDTH: 540px; BORDER-BOTTOM: black 1px solid; HEIGHT: 450px">
				<table cellSpacing="0" cellPadding="0" width="530" bgColor="white">
					<tr bgColor="blue" height="20">
						<td class="whtlbl"><asp:Label id="lang1192" runat="server">Job Plan Record Lookup</asp:Label></td>
						<td align="right"><IMG onclick="closejp();" height="18" alt="" src="../images/close.gif" width="18"><br>
						</td>
					</tr>
					<tr class="details" id="trjp" height="450">
						<td class="bluelabelb" vAlign="middle" align="center" colSpan="2"><asp:Label id="lang1193" runat="server">Moving Window...</asp:Label></td>
					</tr>
					<tr>
						<td colSpan="2"><iframe id="ifjp" style="WIDTH: 540px; HEIGHT: 450px" src="" frameBorder="no" runat="server">
							</iframe>
						</td>
					</tr>
				</table>
			</div>
			<div onmouseup="mouseUp();restore('ifrt', 'trrt');" class="details" onmousedown="makeDraggable(this, 'ifrt', 'trrt');"
				id="rtdiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 999; BORDER-LEFT: black 1px solid; WIDTH: 580px; BORDER-BOTTOM: black 1px solid; HEIGHT: 450px">
				<table cellSpacing="0" cellPadding="0" width="580" bgColor="white">
					<tr bgColor="blue" height="20">
						<td class="whitelabel"><asp:Label id="lang1194" runat="server">Record Lookup</asp:Label></td>
						<td align="right"><IMG onclick="closert();" height="18" alt="" src="../images/close.gif" width="18"><br>
						</td>
					</tr>
					<tr class="details" id="trrt" height="450">
						<td class="bluelabelb" vAlign="middle" align="center" colSpan="2"><asp:Label id="lang1195" runat="server">Moving Window...</asp:Label></td>
					</tr>
					<tr>
						<td colSpan="2"><iframe id="ifrt" style="WIDTH: 580px; HEIGHT: 450px" src="" frameBorder="no" runat="server">
							</iframe>
						</td>
					</tr>
				</table>
			</div>
			<input id="lblroute" type="hidden" name="lblroute" runat="server"> <input id="lbldragid" type="hidden"><input id="lblrowid" type="hidden">
			<input id="lblsubmit" type="hidden" name="lblsubmit" runat="server"><input id="lblrtid" type="hidden" name="lblrtid" runat="server">
			<input id="lbleq" type="hidden" name="lbleq" runat="server"><input id="lbllocret" type="hidden" name="lbllocret" runat="server">
			<input id="lblseqret" type="hidden" name="lblseqret" runat="server"><input id="lblpm" type="hidden" name="lblpm" runat="server">
			<input id="lbltyp" type="hidden" name="lbltyp" runat="server"><input id="lbljp" type="hidden" name="lbljp" runat="server">
			<input id="lblsid" type="hidden" runat="server"> <input id="lblnewroute" type="hidden" name="lblnewroute" runat="server">
			<input id="lblrow" type="hidden" runat="server"><input type="hidden" id="lblfrom" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
