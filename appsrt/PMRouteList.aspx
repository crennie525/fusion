<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMRouteList.aspx.vb" Inherits="lucy_r12.PMRouteList" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMRouteList</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts/gridnav.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMRouteListaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    
    <script language="javascript" type="text/javascript">
    function printpmr(rtid, typ) {
            //alert(rtid)
        //alert(typ)
        var coi = document.getElementById("lblcoi").value;
            var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
            if (typ == "RBAS") {
                if (coi == "GLA") {
                    window.open("../reports/pmrhtml7np_new.aspx?rid=" + rtid, "repWin", popwin);
                }
                else {
                    window.open("../reports/pmrhtml7_new.aspx?rid=" + rtid, "repWin", popwin);
                }
                
            }
            else if (typ == "RBAST") {
                window.open("../reports/pmrhtml7tpm_new.aspx?rid=" + rtid, "repWin", popwin);
                //window.open("../reports/pmrhtml6t.aspx?rid=" + rtid + "&typ=" + typ, "repWin", popwin);
            }
            else {
                window.open("../reports/pmrhtml3.aspx?rid=" + rtid, "repWin", popwin);

            }

        }
        function gethist(rtid) {
            if (rtid != "") {
                window.open("pmroutehist.aspx?rtid=" + rtid + "&date=" + Date());
            }
        }
        function savedateret(dt, who) {
            //alert("hello")
            document.getElementById("lblrtnext").value = dt
            document.getElementById("txtn").value = dt;
            document.getElementById("ifnextstuff").src = "";
            if (who == "next") {
                alert("New Next Date Assigned");
            }
            else {
                alert("Route Completed and New Next Date Assigned");
            }

        }
        function saveworet(wonum, who) {
            document.getElementById("lblwonum").value = wonum;
            //alert(wonum)
            document.getElementById("tdwo").innerHTML = wonum;
            document.getElementById("tddmsg").className = "plainlabelred view";
        }
        function savedate() {
            var who = "next";
            //var nd = document.getElementById("lblrtnext").value;
            var rtid = document.getElementById("lblrtidret").value;
            var dt = document.getElementById("lblrtnext").value;
            var cby = "";
            if (dt != "") {
                document.getElementById("ifnextstuff").src = "pmroutenext.aspx?who=" + who + "&dt=" + dt + "&rtid=" + rtid + "&cby=" + cby;
            }
            else {
                alert("No New Next Date Entered");
            }
        }
        function checkcomp() {
            //document.getElementById("lblcompret").value = "1"
            var sid = "";
            var who = "comp";
            var desc = "";
            var rtid = document.getElementById("lblrtidret").value;
            var dt = document.getElementById("lblrtnext").value;
            var cby = document.getElementById("lblcompby").value;
            if (dt != "") {
                document.getElementById("ifnextstuff").src = "pmroutenext.aspx?who=" + who + "&dt=" + dt + "&rtid=" + rtid + "&cby=" + cby + "&sid=" + sid + "&desc=" + desc;
            }
            else {
                alert("No Next Date Entered");
            }
        }
        function reloadpg() {
            document.getElementById("lblsubmit").value = "retload";
            document.getElementById("form1").submit();
        }
        function handledt(ret) {
            document.getElementById("txtn").value = ret;
            document.getElementById("lblrtnext").value = ret;
            document.getElementById("ifcal").src = "";
        }
        
        function getcal1() {

            var who = "n";
            var dt = "0";
            var days = "no";
            document.getElementById("ifcal").src = "pmroutenextcal.aspx?who=" + who + "&days=" + days + "&dt=" + dt;
            document.getElementById("tbcorr").className = "details";
            document.getElementById("tdcal").className = "view";
        }
        function getsched(rtid, rt, desc, nnd, pmstr) {
            //alert("hello")
            //document.getElementById("tdrtid").innerHTML = rtid;
            document.getElementById("tdrtename").innerHTML = rt;
            document.getElementById("tdrtedesc").innerHTML = desc;
            document.getElementById("lblrtidret").value = rtid;
            document.getElementById("txtn").value = nnd;
            document.getElementById("lblrtnext").value = nnd;
            document.getElementById("tdpm").innerHTML = pmstr;
            //getcal1();
        }
        
        
        function checktyp(who) {
            document.getElementById("lblrtyp").value = who;
            var coi = document.getElementById("lblcoi").value;
            if (coi == "GLA" || coi == "DEMO") {
                document.getElementById("lblsubmit").value = "retload";
            }
            else {
                document.getElementById("lblsubmit").value = "swtch";
            }
            document.getElementById("lblsubmit").value = "swtch";
            document.getElementById("form1").submit();
        }
        function addrt(st) {
            var rtn = "rm"
            var typ = document.getElementById("lblrtyp").value;
            window.parent.handleadd(st, rtn, typ);
        }
        function rtret(id, typ) {
            //var typ = document.getElementById("lblrtyp").value;
            //alert(typ)
            window.parent.handlert(id, typ);
        }
        function showwo() {
            var conf = confirm("Does this Route require a Corrective Action Work Order?")
            if (conf == true) {
                var rt = document.getElementById("tdrtename").innerHTML;
                var desc = document.getElementById("tdrtedesc").innerHTML;
                document.getElementById("txtprob").innerHTML = "Route# " + rt + "\n";
                //Description: " + desc + "\n
                document.getElementById("ifcal").src = "";
                document.getElementById("tdcal").className = "details";
                document.getElementById("tbcorr").className = "view";
                
            }
            else {
                alert("Action Cancelled")
            }
        }
        function createwo2() {
            baz = document.getElementById('txtprob').value;
            //alert((baz.match(/\n/) && 'CR') + ' ' + (baz.match(/\n/) && 'LF'));
            //alert(document.getElementById("txtprob").value.indexOf("\r"))
            //alert(document.getElementById("txtprob").value.indexOf("\n"))
            //baz = baz.replace("\n", "<br />")
            //alert(baz.match(/\n/g) || [])
            //baz = baz.match(/\n/g) || []
            //document.getElementById('txtprob').value = "foo\nbar";
        }
        function createwo() {

            var chk = document.getElementById("lblwonum").value;
            if (chk == "") {
                var sid = document.getElementById("lblsid").value;
                var desc = document.getElementById("txtprob").value;
                desc = desc.replace("'", "`");
                desc =  desc.replace(/\\$/, " ");
                desc = desc.replace("#", "%23")
                //desc = desc.replace("\n", "<br />")
                //alert(desc)
                document.getElementById("lblwodesc").value = desc
                var who = "wo";
                var rtid = document.getElementById("lblrtidret").value;
                var dt = "";
                var cby = document.getElementById("lblcompby").value;
                if (desc != "") {
                    document.getElementById("ifnextstuff").src = "pmroutenext.aspx?who=" + who + "&dt=" + dt + "&rtid=" + rtid + "&cby=" + cby + "&sid=" + sid + "&desc=" + desc;
                }
                else {
                    alert("No Description Entered");
                }
            }
            else {
                alert("Please close this dialog to create a New Work Order");
            }


        }
        function checkit() {
            window.setTimeout("setref();", 1205000);

        }
        //onload="checkit();"
    </script>
    <style type="text/css">
        .windowModal
        {
            position: fixed;
            font-family: Arial, Helvetica, sans-serif;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background: rgba(0,0,0,0.8);
            z-index: 9999;
            opacity: 0;
            -webkit-transition: opacity 400ms ease-in;
            -moz-transition: opacity 400ms ease-in;
            transition: opacity 400ms ease-in;
            pointer-events: none;
        }
        .windowModal:target
        {
            opacity: 1;
            pointer-events: auto;
        }
        
        .windowModal > div
        {
            width: 460px;
            position: relative;
            margin: 10% auto;
            padding: 5px 20px 13px 20px;
            border-radius: 10px;
            background: #fff;
            background: -moz-linear-gradient(#fff, #999);
            background: -webkit-linear-gradient(#fff, #999);
            background: -o-linear-gradient(#fff, #999);
        }
        .close
        {
            background: #606061;
            color: #FFFFFF;
            line-height: 25px;
            position: absolute;
            right: -12px;
            text-align: center;
            top: -10px;
            width: 24px;
            text-decoration: none;
            font-weight: bold;
            -webkit-border-radius: 12px;
            -moz-border-radius: 12px;
            border-radius: 12px;
            -moz-box-shadow: 1px 1px 3px #000;
            -webkit-box-shadow: 1px 1px 3px #000;
            box-shadow: 1px 1px 3px #000;
        }
        
        .close:hover
        {
            background: #00d9ff;
        }
    </style>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <div id="divModal" class="windowModal">
        <div>
            <br />
            <a href="#close" title="Close" class="close" onclick="reloadpg();">X</a>
            <table border="0">
                <tr>
                    <td class="label" width="80">
                        Route
                    </td>
                    <td class="plainlabel" width="380" id="tdrtename" runat="server">
                    </td>
                </tr>
                <tr>
                    <td class="label" width="80" id="lblrtedesc" runat="server">
                        Description
                    </td>
                    <td class="plainlabel" width="380" id="tdrtedesc" runat="server">
                    </td>
                </tr>
                <tr>
                    <td class="label" width="80">
                        Details
                    </td>
                    <td class="plainlabel" width="380" id="tdpm" runat="server">
                    </td>
                </tr>
                <tr>
                    <td class="label" id="tdntd" runat="server">
                        Next Date
                    </td>
                    <td>
                        <asp:TextBox ID="txtn" runat="server" CssClass="plainlabel" ReadOnly="True" Width="100px"></asp:TextBox>
                        <img onclick="getcal1('n');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                            width="19" height="19" />
                        <img id="imgsav" onmouseover="return overlib('Save Next Date', ABOVE, LEFT)" onmouseout="return nd()"
                            onclick="savedate();" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif"
                            width="20" height="20" runat="server" />
                        <img id="imgcomp" onmouseover="return overlib('Complete this Route', ABOVE, LEFT)"
                            onmouseout="return nd()" onclick="checkcomp();" src="../images/appbuttons/minibuttons/comp.gif"
                            runat="server" />
                        <img id="imgwo" src="../images/appbuttons/minibuttons/woprint.gif" onclick="showwo();" />
                    </td>
                </tr>
                
            </table>
            <table>
                    <tr>
                        <td colspan="2" align="center" id="tdcal" runat="server" class="details" width="460">
                            <iframe style="width: 300px; height: 250px; background-color: White;" id="ifcal"
                                src="" frameborder="0" runat="server"></iframe>
                        </td>
                    </tr>
                </table>
            <table id="tbcorr" runat="server" class="details">
                <tr>
                    <td class="label" id="tdwod" runat="server">
                        Work Order Details
                    </td>
                </tr>
                <tr>
                    <td id="tdcorr" runat="server" width="460" class="plainlabel">
                    <asp:TextBox ID="txtprob" runat="server" CssClass="plainlabel" TextMode="MultiLine"
                    Height="120px" Width="360px"></asp:TextBox>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label" id="lblwo" runat="server" width="120">
                                    Work Order Number
                                </td>
                                <td class="plainlabel" id="tdwo" runat="server" width="80">
                                </td>
                                <td>
                                    <img alt="" src="../images/appbuttons/minibuttons/addwhite.gif" onclick="createwo();" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                <td class="details" id="tddmsg"><i>Please Close This Dialog to Save Your Work Order Description</i></td>
                </tr>
            </table>
        </div>
    </div>
    <iframe class="details" id="ifnextstuff" src="" frameborder="0" runat="server"></iframe>
    <table style="position: absolute; right: 5px; left: 5px" width="700" border="0">
        <tr>
            <td width="140">
            </td>
            <td width="250">
            </td>
            <td width="20">
            </td>
            <td width="20">
            </td>
            <td width="20">
            </td>
            <td width="280">
            </td>
        </tr>
        <tr>
            <td class="thdrsingg plainlabel" colspan="6">
                <asp:Label ID="lang1176" runat="server">Route List Options</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang1177" runat="server">Search Routes</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtsrch" runat="server" Width="240"></asp:TextBox>
            </td>
            <td>
                <img onmouseover="return overlib('Search Routes', ABOVE, LEFT)" onmouseout="return nd()"
                    onclick="srchrts();" src="../images/appbuttons/minibuttons/srchsm.gif" alt="" />
            </td>
            <td>
                <img onmouseover="return overlib('Add/Edit Routes', ABOVE, LEFT)" onmouseout="return nd()"
                    onclick="addrt('no');" src="../images/appbuttons/minibuttons/addnew.gif" alt="" />
            </td>
            <td colspan="2" class="plainlabel">
                <input type="radio" name="rptyp" id="rbrbas" onclick="checktyp('RBAS');" runat="server" />
                <label id="lblrba" runat="server"></label>
                <input type="radio" name="rptyp" id="rbrbast" onclick="checktyp('RBAST');" runat="server" />
                <label id="lblrbtpma" runat="server"></label>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang1178" runat="server">Sort By</asp:Label>
            </td>
            <td class="plainlabel">
                <asp:RadioButtonList ID="rbsort" runat="server" RepeatDirection="Horizontal"
                    Height="20px" CssClass="plainlabel">
                    <asp:ListItem Value="0" Selected="True" id="lirtea" runat="server" width="80">Route#</asp:ListItem>
                    <asp:ListItem Value="1">Type</asp:ListItem>
                    <asp:ListItem Value="2" id="licby" runat="server" width="80">Created By</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                <img onmouseover="return overlib('Sort Ascending', ABOVE, LEFT)" onmouseout="return nd()"
                    onclick="sort('asc');" src="../images/appbuttons/minibuttons/sasc.gif">
            </td>
            <td>
                <img onmouseover="return overlib('Sort Descending', ABOVE, LEFT)" onmouseout="return nd()"
                    onclick="sort('desc');" src="../images/appbuttons/minibuttons/sdesc.gif">
            </td>
        </tr>
        <tr>
            <td id="tdlist" colspan="6" runat="server">
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center">
                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                    border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblsid" type="hidden" runat="server">
    <input id="lblsubmit" type="hidden" runat="server">
    <input id="lblsort" type="hidden" runat="server">
    <input type="hidden" id="lbleqid" runat="server">
    <input type="hidden" id="lblfu" runat="server">
    <input type="hidden" id="lblco" runat="server"><input type="hidden" id="lblrtid"
        runat="server">
    <input type="hidden" id="txtpg" runat="server" name="txtpg">
    <input type="hidden" id="txtpgcnt" runat="server" name="txtpgcnt">
    <input type="hidden" id="lblret" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lbltyp" runat="server" />
    <input type="hidden" id="lblrtyp" runat="server" />
    <input type="hidden" id="lblcoi" runat="server" />
    <input type="hidden" id="lblrtidret" runat="server" />
    <input type="hidden" id="lblrtnext" runat="server" />
    <input type="hidden" id="lblrteretnew" runat="server" />
    <input type="hidden" id="lblcompret" runat="server" />
    <input type="hidden" id="lblcompby" runat="server" />
    <input type="hidden" id="lblwodesc" runat="server" />
    <input type="hidden" id="lblwonum" runat="server" />

    <input type="hidden" id="Hidden1" runat="server" />
    <input type="hidden" id="Hidden2" runat="server" />
    <input type="hidden" id="Hidden3" runat="server" />
    <input type="hidden" id="Hidden4" runat="server" />
    </form>
</body>
</html>
