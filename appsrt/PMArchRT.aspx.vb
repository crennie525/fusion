

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class PMArchRT
    Inherits System.Web.UI.Page
	Protected WithEvents lang1175 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim main As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim PageNumber As Integer
    Dim PageSize As Integer = 50
    Dim intPgNav As Integer
    Dim chk, did, clid, start, Login, sid, srch, comp As String
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdarch As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
If Not IsPostBack Then
            If Request.QueryString("start").ToString = "yes" Then
                Try
                    sid = Request.QueryString("sid").ToString
                Catch ex As Exception
                    sid = HttpContext.Current.Session("dfltps").ToString()
                End Try

                lblsid.Value = sid
                txtpg.Value = "1"
                main.Open()
                GetCount()
                GetArch()
                main.Dispose()
            Else
                tdarch.InnerHtml = "Waiting for location..."
            End If
        Else
            If Request.Form("lblret") = "next" Then
                main.Open()
                GetNext()
                main.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                main.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetArch()
                main.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                main.Open()
                GetPrev()
                main.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                main.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetArch()
                main.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "srch" Then
                main.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetArch()
                main.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetArch()
        Catch ex As Exception
            main.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr493" , "PMArchRT.aspx.vb")
 
            main.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetArch()
        Catch ex As Exception
            main.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr494" , "PMArchRT.aspx.vb")
 
            main.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetCount()
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", " ", , , vbTextCompare)
        sid = lblsid.Value
        comp = "0" 'lblcomp.Value
        If srch = "" Then
            srch = "%"
        Else
            srch = "%" & srch & "%"
        End If
        sql = "SELECT count(*) FROM equipment where compid = '" & comp & "' and siteid = '" & sid & "' and " _
        + "(eqnum like '" & srch & "' or eqdesc like '" & srch & "' or spl like '" & srch & "')"
        intPgNav = main.PageCount(sql, PageSize)
        txtpgcnt.Value = intPgNav
    End Sub
    Private Sub GetArch()
        Dim sid, did, clid, chk, lid, loc As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", " ", , , vbTextCompare)
        sid = lblsid.Value
        comp = "0" 'lblcomp.Value

        Dim eqnum, eqdesc As String
        PageNumber = txtpg.Value
        PageSize = 50 'lblpagesize.Value

        intPgNav = txtpgcnt.Value
        lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        Dim sb As StringBuilder = New StringBuilder

        sb.Append("<table cellspacing=""0"" border=""0""><tr>")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""155""></tr>" & vbCrLf)

        sql = "exec usp_getAllSAPg2 '" & comp & "','" & sid & "','" & PageNumber & "','" & PageSize & "','" & srch & "'"

        Dim ds As New DataSet
        ds = main.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)

        Dim trans As String = "0"
        Dim tstat As String = "0"
        '*** End Multi Add ***
        Dim locby As String
        Dim lock As String = "0"
        Dim fid As String = "0"
        Dim cid As String = "0"
        Dim eid As String = "0"
        Dim cidhold As Integer = 0

        Dim epcnt As Integer = 0
        Dim fpcnt As Integer = 0
        Dim cpcnt As Integer = 0
        Dim cnt As Integer = 0
        Dim totcnt As Integer = 0
        Dim parent As String
        Dim row As DataRow
        For Each row In dt.Rows
            totcnt += 1
            If row("eqid") <> eid Then
                If eid <> 0 Then
                    sb.Append("</table></td></tr>")
                End If

                eid = row("eqid").ToString
                sid = row("siteid").ToString
                did = row("dept_id").ToString
                clid = row("cellid").ToString
                lid = row("locid").ToString
                loc = row("location").ToString
                epcnt = row("epcnt").ToString
                If clid <> "" Then
                    chk = "yes"
                Else
                    chk = "no"
                End If
                eqnum = row("eqnum").ToString
                eqdesc = row("eqdesc").ToString
                lock = row("locked").ToString
                locby = row("lockedby").ToString
                parent = row("parent").ToString
                'If eqnum = loc Then
                'If parent <> "" Then
                'Dim parar() As String = parent.Split("~")
                'lid = parar(0)
                'loc = parar(1)
                'End If
                'End If
                sb.Append("<tr><td><img id='i" + eid + "' ")
                sb.Append("onclick=""getdets('" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & lid & "','" & eqnum & "');""")
                sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>")
                If epcnt <> 0 Then
                    sb.Append("<td><img src=""../images/appbuttons/minibuttons/gridpic.gif"" onclick=""geteqport('" + eid + "')""></td>" & vbCrLf)
                Else
                    sb.Append("<td><img src=""../images/appbuttons/minibuttons/gridpicdis.gif""></td>" & vbCrLf)
                End If
                If lid = "" Then
                    sb.Append("<td colspan=""2"" class=""plainlabel""><a href=""#""")
                    'sb.Append("<a class=""A1"" href=""#"" onclick=""getdeq('" & did & "','" & clid & "','" & eqid & "','" & eqnum & "','" & eqdesc & "','" & mcnt & "','" & dept & "','" & cell & "')"">")
                    sb.Append("onclick=""gotoeq('" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & lid & "')""")
                    sb.Append("class=""linklabel"" >" & eqnum & "</a> - " & eqdesc)
                Else
                    sb.Append("<td colspan=""2"" class=""plainlabel""><a href=""#""")
                    'sb.Append("<a class=""A1"" href=""#"" onclick=""getdeq('" & did & "','" & clid & "','" & eqid & "','" & eqnum & "','" & eqdesc & "','" & mcnt & "','" & dept & "','" & cell & "')"">")
                    sb.Append("onclick=""gotoeq('" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & lid & "')""")
                    sb.Append("class=""linklabel"" >" & eqnum & " - " & loc & "</a> - " & eqdesc)
                End If



                '*** Multi Add ***
                trans = row("trans").ToString
                If trans = "0" OrElse Len(trans) = 0 Then
                    'sb.Append("</td></tr>" & vbCrLf)
                    If lock = "0" OrElse Len(lock) = 0 Then
                        sb.Append("</td></tr>" & vbCrLf)
                    Else
                        sb.Append("&nbsp;<img src='../images/appbuttons/minibuttons/lillock.gif' ")
                        sb.Append("onmouseover=""return overlib('" & tmod.getov("cov313", "siteassets3.aspx.vb") & ": " & locby & "')"" ")
                        sb.Append("onmouseout=""return nd()""></td></tr>" & vbCrLf)
                    End If
                Else
                    tstat = row("transstatus").ToString
                    If tstat <> "4" Then
                        sb.Append("&nbsp;<img src='../images/appbuttons/minibuttons/warning.gif' ")
                        sb.Append("onmouseover=""return overlib('" & tmod.getov("cov314", "siteassets3.aspx.vb") & "')"" ")
                        sb.Append("onmouseout=""return nd()""></td></tr>" & vbCrLf)
                    End If

                End If
                '*** End Multi Add ***

                'onmouseover=""return overlib('" & row("dept_line").ToString & "', BELOW, LEFT)"" onmouseout=""return nd()""
                sb.Append("<tr><td></td><td colspan=""4""><table class=""details"" cellspacing=""0"" id='t" + eid + "' border=""0"">")
                sb.Append("<tr><td width=""15""></td><td width=""15""></td><td width=""135""></td></tr>")
            End If




        Next
        'h.Dispose()
        sb.Append("</td></tr></table></td></tr></table>")
        'Response.Write(sb.ToString)
        Dim totcntstr As Integer
        totcntstr = totcnt
        tdarch.InnerHtml = sb.ToString

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1175.Text = axlabs.GetASPXPage("PMArchRT.aspx", "lang1175")
        Catch ex As Exception
        End Try

    End Sub

End Class

