<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PMRouteLU.aspx.vb" Inherits="lucy_r12.PMRouteLU" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PMRouteLU</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/PMRouteLUaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
         function getnext() {

             var cnt = document.getElementById("txtpgcnt").value;
             var pg = document.getElementById("txtpg").value;
             pg = parseInt(pg);
             cnt = parseInt(cnt)
             if (pg < cnt) {
                 document.getElementById("lblret").value = "next"
                 document.getElementById("form1").submit();
             }
         }
         function getlast() {

             var cnt = document.getElementById("txtpgcnt").value;
             var pg = document.getElementById("txtpg").value;
             pg = parseInt(pg);
             cnt = parseInt(cnt)
             if (pg < cnt) {
                 document.getElementById("lblret").value = "last"
                 document.getElementById("form1").submit();
             }
         }
         function getprev() {

             var cnt = document.getElementById("txtpgcnt").value;
             var pg = document.getElementById("txtpg").value;
             pg = parseInt(pg);
             cnt = parseInt(cnt)
             if (pg > 1) {
                 document.getElementById("lblret").value = "prev"
                 document.getElementById("form1").submit();
             }
         }
         function getfirst() {

             var cnt = document.getElementById("txtpgcnt").value;
             var pg = document.getElementById("txtpg").value;
             pg = parseInt(pg);
             cnt = parseInt(cnt)
             if (pg > 1) {
                 document.getElementById("lblret").value = "first"
                 document.getElementById("form1").submit();
             }
         }
     </script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table width="480">
				<tr>
					<td class="label"><asp:Label id="lang1179" runat="server">Search Records</asp:Label><asp:textbox id="txtsrch" runat="server"></asp:textbox><asp:imagebutton id="btnsrch1" runat="server" Height="20px" Width="20px" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td id="tdlist" runat="server"></td>
				</tr>
                <tr id="trnav" runat="server">
				<td align="center">
					<table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
						border-right: blue 1px solid" cellspacing="0" cellpadding="0">
						<tr>
							<td style="border-right: blue 1px solid" width="20">
								<img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
									runat="server">
							</td>
							<td style="border-right: blue 1px solid" width="20">
								<img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
									runat="server">
							</td>
							<td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
								<asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
							</td>
							<td style="border-right: blue 1px solid" width="20">
								<img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
									runat="server">
							</td>
							<td width="20">
								<img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
									runat="server">
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</table>
			<input id="lblsid" type="hidden" runat="server"> <input id="lblret" type="hidden" runat="server">
			<input id="lbllist" type="hidden" runat="server"><input id="lblid" type="hidden" runat="server">
			<input id="lblid1" type="hidden" runat="server"><input id="lblid2" type="hidden" runat="server">
			<input id="lbleq" type="hidden" runat="server"> <input id="lblpm" type="hidden" runat="server">
			<input type="hidden" id="lblid3" runat="server"> <input type="hidden" id="lblid4" runat="server">
			<input type="hidden" id="lbldid" runat="server"> <input type="hidden" id="lblclid" runat="server">
			<input type="hidden" id="lbllocid" runat="server"> <input type="hidden" id="lblid5" runat="server">
			<input type="hidden" id="lbljp" runat="server"> <input type="hidden" id="lblnc" runat="server">
			<input type="hidden" id="lblid6" runat="server"><input type="hidden" id="dcell" runat="server">
		<input id="txtpg" type="hidden" runat="server">
	<input id="txtpgcnt" type="hidden" runat="server"><input id="lblfilt" type="hidden"
		runat="server">
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
