﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmrrtelkupdialog.aspx.vb" Inherits="lucy_r12.pmrrtelkupdialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report Based Route Lookup</title>
     <script language="javascript" type="text/javascript">
         function handleprint(typ, rtid) {
             var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
             var typx = document.getElementById("lbltypx").value;
             if (typx == "RBASM3x") {
                 document.getElementById("lblrtid").value = rtid
                 document.getElementById("lblret").value = "excel"
                 document.getElementById("form1").submit();
             }
             else if (typx == "RBASTM2x") {
                 document.getElementById("lblrtid").value = rtid
                 document.getElementById("lblret").value = "excelt"
                 document.getElementById("form1").submit();
             }
             else if (typx == "RBASM3xa") {
                 document.getElementById("lblrtid").value = rtid
                 document.getElementById("lblret").value = "excela"
                 document.getElementById("form1").submit();
             }
             else if (typx == "RBASTM2xa") {
                 document.getElementById("lblrtid").value = rtid
                 document.getElementById("lblret").value = "excelta"
                 document.getElementById("form1").submit();
             }
             else if (typx == "L0") {
                 document.getElementById("lblrtid").value = rtid
                 document.getElementById("lblret").value = "L0"
                 document.getElementById("form1").submit();
             }
             else if (typx == "L1") {
                 document.getElementById("lblrtid").value = rtid
                 document.getElementById("lblret").value = "L1"
                 document.getElementById("form1").submit();
             }
             else if (typx == "L2") {
                 document.getElementById("lblrtid").value = rtid
                 document.getElementById("lblret").value = "L2"
                 document.getElementById("form1").submit();
             }
             else {
                 if (typ == "RBASM" || typ == "RBASTM") {
                     window.open("../reports/pmrhtml5.aspx?rid=" + rtid + "&typ=" + typ, "repWin", popwin);
                 }
                 else if (typ == "RBASM2") {
                     window.open("../reports/pmrhtml6.aspx?rid=" + rtid + "&typ=" + typ, "repWin", popwin);
                 }
                 else if (typ == "RBASTM2") {
                     window.open("../reports/pmrhtml6t.aspx?rid=" + rtid + "&typ=" + typ, "repWin", popwin);
                 }
                 else if (typ == "RBASM3") {
                     window.open("../reports/pmrhtml7_new.aspx?rid=" + rtid + "&typ=" + typ, "repWin", popwin);
                 }
                 else if (typ == "RBASM3NP") {
                     window.open("../reports/pmrhtml7np_new.aspx?rid=" + rtid + "&typ=" + typ, "repWin", popwin);
                 }
                 else if (typ == "RBASTM3") {
                     window.open("../reports/pmrhtml7t.aspx?rid=" + rtid + "&typ=" + typ, "repWin", popwin);
                 }
                 else if (typ == "RBASTM3NP") {
                     window.open("../reports/pmrhtml7tnp.aspx?rid=" + rtid + "&typ=" + typ, "repWin", popwin);
                 }
                 else if (typ == "RBAS" || typ == "RBAST") {
                     window.open("../reports/pmrhtml4.aspx?rid=" + rtid + "&typ=" + typ, "repWin", popwin);
                 }
             }
         }
         function checksite() {
             var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
             var typx = document.getElementById("lbltypx").value;
             if (typx == "RBASM3xs") {
                 document.getElementById("lblrtid").value = "0";
                 document.getElementById("lblret").value = "excela"
                 document.getElementById("form1").submit();
                 window.close();
             }
             else if (typx == "RBASTM2xs") {
                 document.getElementById("lblrtid").value = "0"
                 document.getElementById("lblret").value = "excelta"
                 document.getElementById("form1").submit();
                 window.close();
             }
             else if (typx == "COMPx") {
                 document.getElementById("lblrtid").value = "0"
                 document.getElementById("lblret").value = "excelclx"
                 document.getElementById("form1").submit();
                 window.close();
             }
             else if (typx == "L0") {
                 document.getElementById("lblrtid").value = "0"
                 document.getElementById("lblret").value = "L0"
                 document.getElementById("form1").submit();
                 window.close();
             }
             else if (typx == "L1") {
                 document.getElementById("lblrtid").value = "0"
                 document.getElementById("lblret").value = "L1"
                 document.getElementById("form1").submit();
                 window.close();
             }
             else if (typx == "L2") {
                 document.getElementById("lblrtid").value = "0"
                 document.getElementById("lblret").value = "L2"
                 document.getElementById("form1").submit();
                 window.close();
             }
         }
         //onload="checksite();"
     </script>
</head>
<body onload="checksite();">
    <form id="form1" runat="server">
    <iframe id="iffreq" src="" width="600px" height="480px" frameborder="0" runat="server" />
     <asp:DataGrid ID="dgout" runat="server" ShowFooter="True">
        </asp:DataGrid>
    <input type="hidden" id="lbltypx" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblrtid" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
