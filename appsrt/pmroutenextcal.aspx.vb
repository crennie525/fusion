﻿Public Class pmroutenextcal
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim dt, days As String
    Dim calday As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Hide the title of the calendar control
        Calendar2.ShowTitle = False

        If Not IsPostBack Then
            Try
                Call Populate_MonthList()

                Call Populate_YearList()

                lblwho.Value = Request.QueryString("who").ToString
                Try
                    dt = Request.QueryString("dt").ToString
                    Try
                        calday = Request.QueryString("calday").ToString
                        lblcalday.Value = calday
                    Catch ex As Exception
                        lblcalday.Value = -1
                    End Try
                    Try
                        days = Request.QueryString("days").ToString
                        lbldays.Value = days
                    Catch ex As Exception
                        lbldays.Value = "no"
                    End Try
                    If days = "" Then
                        lbldays.Value = "no"
                    End If
                    If dt <> "0" Then
                        Dim sel As Date
                        sel = CDate(dt)
                        lblsel.Value = sel
                        Dim mnth As Integer = sel.Month - 1
                        Dim yr As Integer = sel.Year
                        drpCalMonth.SelectedIndex = mnth
                        drpCalYear.SelectedValue = yr
                        Calendar2.VisibleDate = sel
                        Calendar2.SelectedDate = sel

                    End If
                Catch ex As Exception

                End Try
            Catch ex As Exception
                lblwho.Value = "other"
            End Try

        End If
        'Calendar2.TodaysDate = CDate("January 1, 2013")

    End Sub
    Public Sub Set_Calendar(Sender As Object, E As EventArgs)

        'Whenever month or year selection changes display the calendar for that month/year 
        Dim yr As Integer = drpCalYear.SelectedItem.Value
        'If yr <> DateTime.Now.Year Then
        Calendar2.VisibleDate = Nothing
        Calendar2.SelectedDate = Nothing
        'End If
        Calendar2.TodaysDate = CDate(drpCalMonth.SelectedItem.Value & ", 1, " & drpCalYear.SelectedItem.Value)


    End Sub

    Private Sub Populate_YearList()
        Dim intYear As Integer

        'Year list can be changed by changing the lower and upper 
        'limits of the For statement    
        For intYear = DateTime.Now.Year - 1 To DateTime.Now.Year + 20
            drpCalYear.Items.Add(intYear)
        Next

        'Make the current year selected item in the list
        drpCalYear.Items.FindByValue(DateTime.Now.Year).Selected = True
    End Sub

    Private Sub Populate_MonthList()
        'Add each month to the list
        drpCalMonth.Items.Add("January")
        drpCalMonth.Items.Add("February")
        drpCalMonth.Items.Add("March")
        drpCalMonth.Items.Add("April")
        drpCalMonth.Items.Add("May")
        drpCalMonth.Items.Add("June")
        drpCalMonth.Items.Add("July")
        drpCalMonth.Items.Add("August")
        drpCalMonth.Items.Add("September")
        drpCalMonth.Items.Add("October")
        drpCalMonth.Items.Add("November")
        drpCalMonth.Items.Add("December")
        'Make the current month selected item in the list
        drpCalMonth.Items.FindByValue(MonthName(DateTime.Now.Month)).Selected = True
    End Sub

    Private Sub Calendar2_DayRender(sender As Object, e As System.Web.UI.WebControls.DayRenderEventArgs) Handles Calendar2.DayRender
        days = lbldays.Value
        If days <> "no" Then
            e.Day.IsSelectable = False
            Dim daysarr() As String = days.Split(",")
            Dim cday As String
            Dim nday As Integer
            Dim i As Integer
            For i = 0 To daysarr.Length - 1
                cday = daysarr(i)
                Select Case cday
                    Case "M"
                        nday = 1
                    Case "Tu"
                        nday = 2
                    Case "T"
                        nday = 2
                    Case "W"
                        nday = 3
                    Case "Th"
                        nday = 4
                    Case "F"
                        nday = 5
                    Case "Sa"
                        nday = 6
                    Case "Su"
                        nday = 0
                    Case Else
                        nday = -1
                End Select
                If nday <> -1 Then
                    If e.Day.Date.DayOfWeek = nday Then
                        e.Day.IsSelectable = True
                    Else

                    End If
                End If
            Next
        End If
    End Sub


    Private Sub Calendar2_SelectionChanged(sender As Object, e As System.EventArgs) Handles Calendar2.SelectionChanged
        lbldate.Value = Calendar2.SelectedDate
        lblretday.Value = Calendar2.SelectedDate.DayOfWeek
        lblch.Value = "1"
    End Sub

End Class