﻿Imports System.Data.SqlClient
Public Class pmroutenext
    Inherits System.Web.UI.Page
    Dim rl As New Utilities
    Dim dt, who, cby, rtid, sql, freq, ndate, sid, wonum, desc As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dt = Request.QueryString("dt").ToString
        who = Request.QueryString("who").ToString
        cby = Request.QueryString("cby").ToString
        rtid = Request.QueryString("rtid").ToString
        sid = Request.QueryString("sid").ToString
        desc = Request.QueryString("desc").ToString
        lblwho.Value = who
        If who = "next" Then
            rl.Open()
            savenext(rtid, dt)
            rl.Dispose()
        ElseIf who = "comp" Then
            rl.Open()
            savecomp(rtid, dt, cby)
            rl.Dispose()
        ElseIf who = "wo" Then
            rl.Open()
            genwo()
            rl.Dispose()
        End If
    End Sub
    Private Sub savecomp(ByVal rtid As String, ByVal dt As String, ByVal cby As String)
        sql = "select freq from pmroutes where rid = '" & rtid & "'"
        freq = rl.strScalar(sql)
        sql = "select cast('" & dt & "' as datetime) + cast('" & freq & "' as int)"
        ndate = rl.strScalar(sql)
        'Dim test As String = ndate
        sql = "update pmroutes set nextdate = '" & ndate & "', compby = '" & cby & "' where rid = '" & rtid & "'"
        rl.Update(sql)
        sql = "insert into pmrouteshist (rid, nextdate, compdate, compby) values('" & rtid & "','" & dt & "',getdate(),'" & cby & "')"
        rl.Update(sql)
        lblnew.Value = ndate
        lblsubmit.Value = "yes"
    End Sub
    Private Sub savenext(ByVal rtid As String, ByVal dt As String)
        sql = "update pmroutes set nextdate = '" & dt & "' where rid = '" & rtid & "'"
        rl.Update(sql)
        lblnew.Value = dt
        lblsubmit.Value = "yes"
    End Sub
    Private Sub genwo()
        sql = "insert into workorder (status, statusdate, changeby, changedate, reportedby, reportdate, worktype, " _
        + "siteid) " _
      + "values ('WAPPR', getDate(),'" & cby & "', getDate(),'" & cby & "', getDate(), 'CM' ,'" & sid & "') " _
      + "select @@identity"
        wonum = rl.Scalar(sql)
        lblwonum.Value = wonum
        'savewodesc(wonum)
        lblsubmit.Value = "yes"
    End Sub
    Private Sub savewodesc(ByVal wonum As String)
        'Dim wonum As String = lblwonum.Value
        Dim lg As String = desc
        Dim sh As String
        Dim lgcnt As Integer
        Dim test As String = lg
        If Len(lg) > 79 Then
            sh = Mid(lg, 1, 79)
            sql = "update workorder set description = '" & sh & "' where wonum = '" & wonum & "'"
            rl.Update(sql)
            lg = Mid(lg, 80)
            sql = "select count(*) from wolongdesc where wonum = '" & wonum & "'"
            lgcnt = rl.Scalar(sql)
            If lgcnt <> 0 Then
                sql = "update wolongdesc set longdesc = '" & lg & "' where wonum = '" & wonum & "'"
                rl.Update(sql)
            Else
                sql = "insert into wolongdesc (wonum, longdesc) values ('" & wonum & "','" & lg & "')"
                rl.Update(sql)
            End If
        Else
            sql = "update workorder set description = '" & lg & "' where wonum = '" & wonum & "'" _
            + "delete from wolongdesc where wonum = '" & wonum & "'"
            rl.Update(sql)
            
        End If
        lblwonum.Value = wonum
        lblsubmit.Value = "yes"
    End Sub
End Class