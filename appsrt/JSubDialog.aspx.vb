

'********************************************************
'*
'********************************************************



Public Class JSubDialog
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, fuid, tid, sid, Login, eqid, jpid, wonum, ro, rostr As String
    Dim jp As New Utilities
    Protected WithEvents ifsm As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                tid = Request.QueryString("tid").ToString
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                If ro <> "1" Then
                    rostr = HttpContext.Current.Session("rostr").ToString
                    If Len(rostr) <> 0 Then
                        ro = jp.CheckROS(rostr, "jp")
                        'lblro.Value = ro
                    End If
                End If
                If Len(tid) <> 0 AndAlso tid <> "" AndAlso tid <> "0" Then
                    'cid = Request.QueryString("cid").ToString
                    jpid = Request.QueryString("jpid").ToString
                    wonum = Request.QueryString("wo").ToString
                    'sid = Request.QueryString("sid").ToString

                    ifsm.Attributes.Add("src", "JSub.aspx?tid=" & tid & "&jpid=" & jpid & "&wo=" & wonum & "&ro=" & ro)
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr491" , "JSubDialog.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "noeqid"
                End If
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr492" , "JSubDialog.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "noeqid"
            End Try

        End If

    End Sub

End Class
