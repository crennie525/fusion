

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class JobPlanTaskList
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim ds As DataSet
    Dim dslev As DataSet
    Dim dr As SqlDataReader
    Dim gtasks As New Utilities
    Dim sql As String
    Dim jpid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgtasks As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            jpid = "21" 'Request.QueryString("jid").ToString
            gtasks.Open()
            BindGrid(jpid)
            gtasks.Dispose()
        End If
    End Sub
    Private Sub BindGrid(Optional ByVal jid As String = "0")
        If jid <> "0" Then
            sql = "select t.*, isnull(p.comid, '0') as comid from pmjobtasks t " _
            + "left join pmjobplans p on p.jpid = t.jpid where t.jpid = '" & jid & "' order by t.tasknum"
        Else
            sql = "select t.* from pmjobtasks t where t.jpid = '0'"
        End If

        ds = gtasks.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        dgtasks.DataSource = dv
        dgtasks.DataBind()
    End Sub
	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgtasks.Columns(0).HeaderText = dlabs.GetDGPage("JobPlanTaskList.aspx","dgtasks","0")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(1).HeaderText = dlabs.GetDGPage("JobPlanTaskList.aspx","dgtasks","1")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(2).HeaderText = dlabs.GetDGPage("JobPlanTaskList.aspx","dgtasks","2")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(3).HeaderText = dlabs.GetDGPage("JobPlanTaskList.aspx","dgtasks","3")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(4).HeaderText = dlabs.GetDGPage("JobPlanTaskList.aspx","dgtasks","4")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(5).HeaderText = dlabs.GetDGPage("JobPlanTaskList.aspx","dgtasks","5")
		Catch ex As Exception
		End Try

	End Sub

End Class
