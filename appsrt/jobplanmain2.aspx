<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="jobplanmain2.aspx.vb" Inherits="lucy_r12.jobplanmain2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>jobplanmain2</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
        <script language="javascript" type="text/javascript">
            function handlereturn(who, ret) {
                if (who == "wo") {
                    var retstr = ret;
                    var ret = retstr.split(",");
                    var won = ret[0];
                    var typ = ret[1];
                    var sid = ret[2];
                    var did = ret[3];
                    var clid = ret[4];
                    var chk = ret[5];
                    var eqid = ret[6];
                    var fuid = ret[7];
                    var coid = ret[8];
                    var lid = ret[9];
                    var nid = ret[10];
                    var stat = ret[11];
                    var uid = ret[12];
                    var username = ret[13];
                    var islabor = ret[14];
                    var issuper = ret[15];
                    var isplanner = ret[16];
                    var Logged_In = ret[17];
                    var ro = ret[18];
                    var ms = ret[19];
                    var appstr = ret[20];

                    window.location = "../appswo/wolabmain.aspx?jump=yes&wo=" + won + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&lid=" + lid + "&typ=" + typ + "&nid=" + nid + "&stat=" + stat + "&uid=" + uid + "&username=" + username + "&islabor=" + islabor + "&issuper=" + issuper + "&isplanner=" + isplanner + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
                }
                //jump back to st or list here
                else if (who == "jp") {
                    window.location = "JobPlanMain.aspx?ty=jp&jump=yes&date=" + Date()
                }
                else if (who == "rq" || who == "rqw") {
                    window.location = "JobPlanMain.aspx?typ=st&jump=yes&date=" + Date()
                }
                else {
                    window.location = "JobPlanMain.aspx?typ=jp&jump=no&date=" + Date()
                }
            }
            function handleadd(st, rtn) {
                //alert(st + "," + rtn)
                window.location = "jobplanmain2.aspx?start=" + st + "&from=" + rtn;
            }
        </script>
	</HEAD>
	<body >
		<uc1:mmenu1 id="Mmenu11" runat="server"></uc1:mmenu1>
		<form id="form1" method="post" runat="server">
			<table width="100%" height="120%" style="Z-INDEX: 1; POSITION: absolute; TOP: 76px; LEFT: 0px">
				<tr>
					<td>
						<iframe id="ifinv" src="JobPlan.aspx" runat="server" width="100%" height="100%" frameBorder="no">
						</iframe>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
