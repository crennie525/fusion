<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ScheduledTasks.aspx.vb" Inherits="lucy_r12.ScheduledTasks" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ScheduledTasks</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/ScheduledTasksaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="checkprint();" >
		<form id="Form2" method="post" runat="server">
			<table style="LEFT: 0px; POSITION: absolute; TOP: 0px" width="690">
				<TBODY>
					<tr>
						<td colSpan="3"><asp:repeater id="rptrtasks" runat="server">
								<HeaderTemplate>
									<table cellspacing="2" width="690">
										<tr bgcolor="white" height="26">
											<td class="thdrsingg plainlabel" width="140"><asp:Label id="lang1209" runat="server">Job Plan#</asp:Label></td>
											<td class="thdrsingg plainlabel" width="130"><asp:Label id="lang1210" runat="server">Work Order#</asp:Label></td>
											<td class="thdrsingg plainlabel" width="270"><asp:Label id="lang1211" runat="server">Scheduled Task</asp:Label></td>
											<td class="thdrsingg plainlabel" width="100"><asp:Label id="lang1212" runat="server">Next Date</asp:Label></td>
											<td class="thdrsingg plainlabel" width="40"><asp:Label id="lang1213" runat="server">Print</asp:Label></td>
										</tr>
								</HeaderTemplate>
								<ItemTemplate>
									<tr bgcolor="white" height="20">
										<td class="plainlabel">&nbsp;
											<asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"jpnum")%>' Runat = server>
											</asp:Label>
										</td>
										<td class="plainlabel">&nbsp;
											<asp:Label ID="lblpdm" Text='<%# DataBinder.Eval(Container.DataItem,"wonum")%>' Runat = server>
											</asp:Label>
										</td>
										<td class="plainlabel">
											<asp:LinkButton ID="lbltn"  CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"jp")%>' Runat = server>
											</asp:LinkButton>
										</td>
										<td class="plainlabel">
											<asp:Label ID="lbllast" Text='<%# DataBinder.Eval(Container.DataItem,"nextdate")%>' Runat = server>
											</asp:Label></td>
										<td class="plainlabel" align="center"><IMG id="imgi" runat="server" onmouseover="return overlib('Print This Job Plan', ABOVE, LEFT)"
												onclick="printpm('1');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/printx.gif">
										</td>
										<td class="details">
											<asp:Label ID="lblpmiditem" Text='<%# DataBinder.Eval(Container.DataItem,"jpid")%>' Runat = server>
											</asp:Label></td>
									</tr>
								</ItemTemplate>
								<AlternatingItemTemplate>
									<tr bgcolor="#E7F1FD" height="20">
										<td class="plainlabel">&nbsp;
											<asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"jpnum")%>' Runat = server>
											</asp:Label>
										</td>
										<td class="plainlabel">&nbsp;
											<asp:Label ID="lblpdmalt" Text='<%# DataBinder.Eval(Container.DataItem,"wonum")%>' Runat = server>
											</asp:Label>
										</td>
										<td class="plainlabel">
											<asp:LinkButton ID="lbltnalt"  CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"jp")%>' Runat = server>
											</asp:LinkButton>
										</td>
										<td class="plainlabel">
											<asp:Label ID="lbllastalt" Text='<%# DataBinder.Eval(Container.DataItem,"nextdate")%>' Runat = server>
											</asp:Label></td>
										<td class="plainlabel" align="center"><IMG id="imga" runat="server" onmouseover="return overlib('Print This Job Plan', ABOVE, LEFT)"
												onclick="printpm('1');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/printx.gif">
										</td>
										<td class="details">
											<asp:Label ID="lblpmidalt" Text='<%# DataBinder.Eval(Container.DataItem,"jpid")%>' Runat = server>
											</asp:Label></td>
									</tr>
								</AlternatingItemTemplate>
								<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></TD></TR>
			<tr id="trnav" style="BORDER-BOTTOM: 2px groove" runat="server">
				<td class="bluelabel" id="tdpg" runat="server"></td>
				<td class="label" align="center">&nbsp;</td>
				<td align="right"><asp:imagebutton id="btnprev" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bprev.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="btnnext" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bnext.gif"></asp:imagebutton>
				</td>
			</tr>
			<tr>
				<td colSpan="3">
					<table cellSpacing="0" width="690">
						<tr>
							<td width="26"></td>
							<td width="54"></td>
							<td width="170"></td>
							<td width="80"></td>
							<td width="170"></td>
							<td width="170"></td>
						</tr>
						<tr height="22">
							<td class="thdrsinglft"><IMG src="../images/appbuttons/minibuttons/pmgridhdr.gif" border="0"></td>
							<td class="thdrsingrt label" align="left" colSpan="5"><asp:Label id="lang1214" runat="server">PM Record Search</asp:Label></td>
						</tr>
						<tr>
							<td class="label" colSpan="2"><asp:Label id="lang1215" runat="server">Department</asp:Label></td>
							<td><asp:dropdownlist id="dddepts" runat="server" Width="160px" CssClass="plainlabel" AutoPostBack="True"></asp:dropdownlist></td>
							<td class="label"><asp:Label id="lang1216" runat="server">Station/Cell</asp:Label></td>
							<td><asp:dropdownlist id="ddcells" runat="server" Width="160px" CssClass="plainlabel"></asp:dropdownlist></td>
							<td align="right"><asp:imagebutton id="ibfilt" runat="server" ImageUrl="../images/appbuttons/minibuttons/filter.gif"></asp:imagebutton><IMG id="Img1" onmouseover="return overlib('Print Job Plan Filter Selections', BELOW, LEFT)"
									onclick="printpm('2');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/printx.gif" runat="server">
							</td>
						</tr>
						<tr>
							<td class="label" colSpan="2"><asp:Label id="lang1217" runat="server">Skill</asp:Label></td>
							<td><asp:dropdownlist id="ddskill" runat="server" CssClass="plainlabel"></asp:dropdownlist></td>
							<td class="label"></td>
							<td></td>
						</tr>
						<tr>
						</tr>
					</table>
					<table>
						<tr>
							<td class="label" width="80"><asp:Label id="lang1218" runat="server">Supervisor</asp:Label></td>
							<td width="190"><asp:dropdownlist id="ddsuper" runat="server" Width="180px" CssClass="plainlabel"></asp:dropdownlist></td>
							<td class="label" width="40"><asp:Label id="lang1219" runat="server">From:</asp:Label></td>
							<td width="120"><asp:textbox id="txts" runat="server" Width="120px" CssClass="plainlabel"></asp:textbox></td>
							<td width="20"><IMG onclick="getcal('s');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
									width="19"></td>
							<td class="label" width="40"><asp:Label id="lang1220" runat="server">To:</asp:Label></td>
							<td width="120"><asp:textbox id="txte" runat="server" Width="120px" CssClass="plainlabel"></asp:textbox></td>
							<td width="20"><IMG onclick="getcal('e');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
									width="19"></td>
						</tr>
					</table>
				</td>
			</tr>
			</TBODY></TABLE><input id="txtpg" type="hidden" name="txtpg" runat="server"> <input id="lblfilt" type="hidden" name="lblfilt" runat="server">
			<input id="lblprint" type="hidden" name="lblprint" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
