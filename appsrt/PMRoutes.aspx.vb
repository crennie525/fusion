

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class PMRoutes
    Inherits System.Web.UI.Page
	Protected WithEvents ovid171 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid170 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang1195 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1194 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1193 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1192 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1191 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1190 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1189 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1188 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1187 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1186 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1185 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1184 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1183 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1182 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1181 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1180 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim rt As New Utilities
    Dim rid, sid, start, subm, from As String
    Protected WithEvents ddtyp As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnsub As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents tdret As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnsub1 As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents tdrt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdrtd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblnewroute As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrow As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfrom As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents dllist As System.Web.UI.WebControls.DataList
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtroute As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtroutedesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents ifjp As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifrt As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblroute As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblseqret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dlliist As System.Web.UI.WebControls.DataList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("PMRoutesMain.aspx")
        Catch ex As Exception

        End Try
        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            from = Request.QueryString("from").ToString
            lblfrom.Value = from
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            If start = "yes" Then
                rid = Request.QueryString("rid").ToString
                lblroute.Value = rid
                rt.Open()
                GetDetails(rid)
                GetList(rid)
                rt.Dispose()
            Else
                rid = "0"
                rt.Open()
                GetList(rid)
                rt.Dispose()
            End If
        Else
            subm = Request.Form("lblsubmit").ToString
            If subm = "addrtman" Then
                lblsubmit.Value = ""
                rt.Open()
                AddRoute()
                rt.Dispose()
            ElseIf subm = "add" Then
                lblsubmit.Value = ""
                rt.Open()
                AddSeq()
                rt.Dispose()
            ElseIf subm = "savert" Then
                lblsubmit.Value = ""
                rt.Open()
                SaveRT()
                rid = lblroute.Value
                GetDetails(rid)
                GetList(rid)
                rt.Dispose()
            ElseIf subm = "lookup" Then
                lblsubmit.Value = ""
                rt.Open()
                rid = lblroute.Value
                GetDetails(rid)
                GetList(rid)
                rt.Dispose()
            End If
        End If
    End Sub
    Private Sub GetDetails(ByVal route As String)
        Dim rte, rtd As String
        sql = "select rid, route, description, rttype from pmroutes where rid = '" & route & "'"
        dr = rt.GetRdrData(sql)
        While dr.Read
            rte = dr.Item("route").ToString
            rtd = dr.Item("description").ToString
            Try
                ddtyp.SelectedValue = dr.Item("rttype").ToString
            Catch ex As Exception

            End Try
        End While
        dr.Close()
        imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")
        txtroute.Text = rte
        txtroutedesc.Text = rtd
        tdrt.InnerHtml = rte
        tdrtd.InnerHtml = rtd
    End Sub
    Private Sub GetList(Optional ByVal route As String = "0")
        If route <> "0" Then
            route = lblroute.Value
        End If
        sql = "select s.*, e.eqnum, n.ncnum, d.dept_line + '/' + c.cell_name as dcell, " _
        + "pmnum = (t.skill + '/' + t.freq + ' days/' + t.rd),  j.jpnum, " _
        + "l.location from pmroute_stops s " _
        + "left join equipment e on e.eqid = s.eqid " _
        + "left join noncritical n on n.ncid = s.ncid " _
        + "left join dept d on d.dept_id = s.deptid " _
        + "left join cells c on c.cellid = s.cellid " _
        + "left join pmtasks t on t.pmid = s.pmid and t.pmid <> 0 " _
        + "left join pmjobplans j on j.jpid = s.jpid " _
        + "left join pmlocations l on l.locid = s.locid " _
        + "where s.rid = '" & route & "' order by s.stopsequence"
        dr = rt.GetRdrData(sql)
        dllist.DataSource = dr
        dllist.DataBind()
        dr.Close()
    End Sub
    Private Sub SaveRT()
        Dim route As String = lblroute.Value
        Dim rte As String = txtroute.Text
        rte = Replace(rte, "'", "''")
        Dim rtd As String = txtroutedesc.Text
        rtd = Replace(rtd, "'", "''")
        sid = lblsid.Value
        Dim typ As String = ddtyp.SelectedValue.ToString
        Dim cnt As Integer
        sql = "select count(*) from pmroutes where route = '" & rte & "' and siteid = '" & sid & "'"
        cnt = rt.Scalar(sql)
        If cnt = 1 Then
            sql = "update pmroutes set route = '" & rte & "', description = '" & rtd & "', rttype = '" & typ & "' where rid = '" & route & "'"
            rt.Update(sql)
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr497" , "PMRoutes.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If


    End Sub
    Private Sub AddRoute()
        Dim txt, txtd, uid As String
        txt = txtroute.Text
        txtd = txtroutedesc.Text
        txt = Replace(txt, "'", "''")
        txtd = Replace(txt, "'", "''")
        uid = "pmadmin1" 'HttpContext.Current.Session("uid").ToString
        uid = Replace(uid, "'", "''")
        sid = lblsid.Value
        Dim typ As String = ddtyp.SelectedValue.ToString
        Dim rd As Integer
        Dim cnt As Integer
        sql = "select count(*) from pmroutes where route = '" & txt & "' and siteid = '" & sid & "'"
        cnt = rt.Scalar(sql)
        If cnt = 0 Then
            sql = "insert into pmroutes (siteid, route, rttype, description, createdby, createdate) " _
                   + "values ('" & sid & "','" & txt & "','" & typ & "','" & txtd & "','" & uid & "', getDate()) " _
                   + "select @@identity as 'identity'"
            rd = rt.Scalar(sql)
            lblroute.Value = rd
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr498" , "PMRoutes.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
    Private Sub AddSeq()
        rid = lblroute.Value
        sql = "usp_addrtseq '" & rid & "'"
        rt.Update(sql)
        GetList(rid)
    End Sub

    Private Sub dllist_ItemDataBound1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dllist.ItemDataBound
        Dim typ As String = ddtyp.SelectedValue.ToString
        Dim tc As HtmlTableCell
        Dim tb As HtmlTable
        If e.Item.ItemType = ListItemType.Header Then
            If typ <> "0" Then
                If typ = "PM" Then
                    tc = CType(e.Item.FindControl("tdjpt"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    tc = CType(e.Item.FindControl("tdjpti"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    tc = CType(e.Item.FindControl("tdnct"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    tc = CType(e.Item.FindControl("tdncti"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    'dllist.Width = New Unit(980)
                    'tb = CType(e.Item.FindControl("tblist"), HtmlTable)
                    'tb.Attributes.Add("width", "780")
                ElseIf typ = "JP" Then
                    tc = CType(e.Item.FindControl("tdpmt"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    tc = CType(e.Item.FindControl("tdpmti"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    'dllist.Width = New Unit(880)
                End If
            End If
        End If
       
        If e.Item.ItemType = ListItemType.EditItem Then
            If typ <> "0" Then
                If typ = "PM" Then
                    tc = CType(e.Item.FindControl("tdjpe"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    tc = CType(e.Item.FindControl("tdjpei"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    tc = CType(e.Item.FindControl("tdnce"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    tc = CType(e.Item.FindControl("tdncei"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                ElseIf typ = "JP" Then
                    tc = CType(e.Item.FindControl("tdpme"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    tc = CType(e.Item.FindControl("tdpmei"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    'dllist.Width = New Unit(880)
                End If
            End If
            Dim lid As String = CType(e.Item.FindControl("lbleqs"), Label).ClientID
            Dim lid1 As String = CType(e.Item.FindControl("lbleqid"), TextBox).ClientID
            Dim b1 As HtmlImage = CType(e.Item.FindControl("lookupeq"), HtmlImage)
            Dim lid2 As String = CType(e.Item.FindControl("lblpms"), Label).ClientID
            Dim lid3 As String = CType(e.Item.FindControl("lblpmid"), TextBox).ClientID
            Dim lid4 As String '= CType(e.Item.FindControl("lblpmd"), Label).ClientID
            Dim lid5 As String = CType(e.Item.FindControl("lbllocid"), TextBox).ClientID
            Dim lid6 As String = CType(e.Item.FindControl("lbllocs"), Label).ClientID
            Dim lid7 As String = CType(e.Item.FindControl("lbljpid"), TextBox).ClientID
            Dim lid8 As String = CType(e.Item.FindControl("lbljps"), Label).ClientID
            Dim b2 As HtmlImage = CType(e.Item.FindControl("lookuppm"), HtmlImage)
            Dim b3 As HtmlImage = CType(e.Item.FindControl("lookuploc"), HtmlImage)
            Dim b4 As HtmlImage = CType(e.Item.FindControl("lookupjp"), HtmlImage)
            b4.Attributes("onclick") = "getrt('jp','" & lid & "','" & lid1 & "','" & lid2 & "','" & lid3 & "', '" & lid4 & "', '" & lid5 & "', '" & lid6 & "', '" & lid7 & "', '" & lid8 & "');"
            b3.Attributes("onclick") = "getrt('loc','" & lid & "','" & lid1 & "','" & lid2 & "','" & lid3 & "', '" & lid4 & "', '" & lid5 & "', '" & lid6 & "', '" & lid7 & "', '" & lid8 & "');"
            b2.Attributes("onclick") = "getrt('pm','" & lid & "','" & lid1 & "','" & lid2 & "','" & lid3 & "', '" & lid4 & "', '" & lid5 & "', '" & lid6 & "', '" & lid7 & "', '" & lid8 & "');"
            b1.Attributes("onclick") = "getrt('eq','" & lid & "','" & lid1 & "','" & lid2 & "','" & lid3 & "', '" & lid4 & "', '" & lid5 & "', '" & lid6 & "', '" & lid7 & "', '" & lid8 & "');"
            Dim deleteButton As ImageButton = CType(e.Item.FindControl("ie"), ImageButton)
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete this Route Stop?')"
        ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
            If typ <> "0" Then
                If typ = "PM" Then
                    tc = CType(e.Item.FindControl("tdjpa"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    tc = CType(e.Item.FindControl("tdjpai"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    tc = CType(e.Item.FindControl("tdnca"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    tc = CType(e.Item.FindControl("tdncai"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                ElseIf typ = "JP" Then
                    tc = CType(e.Item.FindControl("tdpma"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    tc = CType(e.Item.FindControl("tdpmai"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                End If
            End If
            Dim deleteButton As ImageButton = CType(e.Item.FindControl("ia"), ImageButton)
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete this Route Stop?')"
        ElseIf e.Item.ItemType = ListItemType.Item Then
            If typ <> "0" Then
                If typ = "PM" Then
                    tc = CType(e.Item.FindControl("tdjpi"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    tc = CType(e.Item.FindControl("tdjpii"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    tc = CType(e.Item.FindControl("tdnci"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    tc = CType(e.Item.FindControl("tdncii"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                ElseIf typ = "JP" Then
                    tc = CType(e.Item.FindControl("tdpmi"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                    tc = CType(e.Item.FindControl("tdpmii"), HtmlTableCell)
                    tc.Attributes.Add("class", "details")
                End If
            End If
            Dim deleteButton As ImageButton = CType(e.Item.FindControl("ii"), ImageButton)
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete this Route Stop?')"
        End If
    End Sub

    Private Sub dllist_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dllist.UpdateCommand
        Dim rsid, rid, eqid, pmid, jpid, st, ost As String
        ost = lblrow.Value
        rid = lblroute.Value
        rsid = CType(e.Item.FindControl("lblrsid"), TextBox).Text
        eqid = CType(e.Item.FindControl("lbleqid"), TextBox).Text
        pmid = CType(e.Item.FindControl("lblpmid"), TextBox).Text
        jpid = CType(e.Item.FindControl("lbljpid"), TextBox).Text
        st = CType(e.Item.FindControl("txtseq"), TextBox).Text
        sql = "update pmroute_stops set eqid = '" & eqid & "', " _
        + "pmid = '" & pmid & "', jpid = '" & jpid & "' " _
        + "where rsid = '" & rsid & "'"
        rt.Open()
        rt.Update(sql)
        If ost <> st Then
            sql = "usp_reroutestops '" & rid & "', '" & st & "', '" & ost & "'"
            rt.Update(sql)
        End If
        Dim route As String = lblroute.Value
        dllist.EditItemIndex = -1
        GetList(route)
        rt.Dispose()

    End Sub

    Private Sub dllist_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dllist.EditCommand
        If e.Item.ItemType = ListItemType.Item Then
            lblrow.Value = CType(e.Item.FindControl("lblseq"), Label).Text
        Else
            lblrow.Value = CType(e.Item.FindControl("lblseqa"), Label).Text
        End If
        rt.Open()
        Dim route As String = lblroute.Value
        dllist.EditItemIndex = e.Item.ItemIndex
        GetList(route)
        rt.Dispose()
    End Sub

    Private Sub dllist_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dllist.CancelCommand
        dllist.EditItemIndex = -1
        rt.Open()
        Dim route As String = lblroute.Value
        GetList(route)
        rt.Dispose()
    End Sub

    Private Sub dllist_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dllist.DeleteCommand
        dllist.EditItemIndex = -1
        Dim rsid As String
        If e.Item.ItemType = ListItemType.AlternatingItem Then
            rsid = CType(e.Item.FindControl("lblrsida"), TextBox).Text
        ElseIf e.Item.ItemType = ListItemType.Item Then
            rsid = CType(e.Item.FindControl("lblrsidi"), TextBox).Text
        Else
            rsid = CType(e.Item.FindControl("lblrsid"), TextBox).Text
        End If
        sql = "usp_delroutestop '" & rsid & "'"
        rt.Open()
        rt.Update(sql)
        Dim route As String = lblroute.Value
        dllist.EditItemIndex = -1
        GetList(route)
        rt.Dispose()
    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang1180.Text = axlabs.GetASPXPage("PMRoutes.aspx","lang1180")
		Catch ex As Exception
		End Try
		Try
			lang1181.Text = axlabs.GetASPXPage("PMRoutes.aspx","lang1181")
		Catch ex As Exception
		End Try
		Try
			lang1182.Text = axlabs.GetASPXPage("PMRoutes.aspx","lang1182")
		Catch ex As Exception
		End Try
		Try
			lang1183.Text = axlabs.GetASPXPage("PMRoutes.aspx","lang1183")
		Catch ex As Exception
		End Try
		Try
			lang1184.Text = axlabs.GetASPXPage("PMRoutes.aspx","lang1184")
		Catch ex As Exception
		End Try
		Try
			lang1185.Text = axlabs.GetASPXPage("PMRoutes.aspx","lang1185")
		Catch ex As Exception
		End Try
		Try
			lang1186.Text = axlabs.GetASPXPage("PMRoutes.aspx","lang1186")
		Catch ex As Exception
		End Try
		Try
			lang1187.Text = axlabs.GetASPXPage("PMRoutes.aspx","lang1187")
		Catch ex As Exception
		End Try
		Try
			lang1188.Text = axlabs.GetASPXPage("PMRoutes.aspx","lang1188")
		Catch ex As Exception
		End Try
		Try
			lang1189.Text = axlabs.GetASPXPage("PMRoutes.aspx","lang1189")
		Catch ex As Exception
		End Try
		Try
			lang1190.Text = axlabs.GetASPXPage("PMRoutes.aspx","lang1190")
		Catch ex As Exception
		End Try
		Try
			lang1191.Text = axlabs.GetASPXPage("PMRoutes.aspx","lang1191")
		Catch ex As Exception
		End Try
		Try
			lang1192.Text = axlabs.GetASPXPage("PMRoutes.aspx","lang1192")
		Catch ex As Exception
		End Try
		Try
			lang1193.Text = axlabs.GetASPXPage("PMRoutes.aspx","lang1193")
		Catch ex As Exception
		End Try
		Try
			lang1194.Text = axlabs.GetASPXPage("PMRoutes.aspx","lang1194")
		Catch ex As Exception
		End Try
		Try
			lang1195.Text = axlabs.GetASPXPage("PMRoutes.aspx","lang1195")
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetFSOVLIBS()
		Dim axovlib as New aspxovlib
		Try
			imgadd.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("PMRoutes.aspx","imgadd") & "')")
			imgadd.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			ovid170.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("PMRoutes.aspx","ovid170") & "')")
			ovid170.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			ovid171.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("PMRoutes.aspx","ovid171") & "')")
			ovid171.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try

	End Sub

End Class
