﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmroutenext1.aspx.vb" Inherits="lucy_r12.pmroutenext1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
<style> 
.windowModal {
    position: fixed;
    font-family: Arial, Helvetica, sans-serif;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: rgba(0,0,0,0.8);
    z-index: 99999;
    opacity:0;
    -webkit-transition: opacity 400ms ease-in;
    -moz-transition: opacity 400ms ease-in;
    transition: opacity 400ms ease-in;
    pointer-events: none;
}
.windowModal:target {
    opacity:1;
    pointer-events: auto;
}

.windowModal > div {
    width: 400px;
    position: relative;
    margin: 10% auto;
    padding: 5px 20px 13px 20px;
    border-radius: 10px;
    background: #fff;
    background: -moz-linear-gradient(#fff, #999);
    background: -webkit-linear-gradient(#fff, #999);
    background: -o-linear-gradient(#fff, #999);
}
.close {
    background: #606061;
    color: #FFFFFF;
    line-height: 25px;
    position: absolute;
    right: -12px;
    text-align: center;
    top: -10px;
    width: 24px;
    text-decoration: none;
    font-weight: bold;
    -webkit-border-radius: 12px;
    -moz-border-radius: 12px;
    border-radius: 12px;
    -moz-box-shadow: 1px 1px 3px #000;
    -webkit-box-shadow: 1px 1px 3px #000;
    box-shadow: 1px 1px 3px #000;
}

.close:hover { background: #00d9ff; }
</style>
<script language="javascript" type="text/javascript">
    function checkdesc() {
        document.getElementById('add_redirect').click();
    }
</script>

</head>
<body onload="checkdesc();">

<a href="#divModal">Open Modal Window</a>
<a href="#divModal" id="add_redirect" class="details">Open Modal Window</a>

<div id="divModal" class="windowModal">
    <div>
        <a href="#close" title="Close" class="close">X</a>
        <h2>Modal Dialog</h2>
        <p>This example shows a modal window without using javascript only using html5 and css3, I try it it¡</p>
        <p>Using javascript, with new versions of html5 and css3 is not necessary can do whatever we want without using js libraries.</p>
    </div>
</div>
</body>