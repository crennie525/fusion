

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class jpfaillist
    Inherits System.Web.UI.Page
	Protected WithEvents btnaddtosite As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang1162 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1161 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1160 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1159 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1158 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1157 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim fail As New Utilities
    Dim cid, eqid, fuid, co, cod, coid, cvalu, app, sid, Login, jp, pmtskid, wonum As String
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtskid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbfailmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents cbopts As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ibtnret As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblopt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()



	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try

        Page.EnableViewState = True
        If Not IsPostBack Then
            Try
                cid = "0" 'Request.QueryString("cid").ToString
                lblcid.Value = cid
                'Session("comp") = cid
                lblsid.Value = HttpContext.Current.Session("dfltps").ToString
                'eqid = Request.QueryString("eqid").ToString
                'lbleqid.Value = eqid
                coid = Request.QueryString("coid").ToString
                lblcoid.Value = coid
                'Session("coid") = coid
                'cvalu = Request.QueryString("cvalu").ToString

                jp = Request.QueryString("jpid").ToString
                lbljpid.Value = jp
                wonum = Request.QueryString("wonum").ToString
                lblwo.Value = wonum
                pmtskid = Request.QueryString("pmtskid").ToString
                lblpmtskid.Value = pmtskid
                lblopt.Value = "0"
                lbfailmodes.AutoPostBack = False
                lbfailmaster.AutoPostBack = False
                fail.Open()
                If coid <> "" Then
                    PopCompHead(coid)
                End If
                PopFail(cid, coid, jp)
                PopFailList(cid, coid, jp, pmtskid)
                fail.Dispose()
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr482" , "jpfaillist.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try

        End If
        'ibtnret.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ibtnret.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
        'btntocomp.Attributes.Add("onclick", "DisableButton(this);")
        'btnfromcomp.Attributes.Add("onclick", "DisableButton(this);")

    End Sub
    Private Sub PopCompHead(ByVal coid As String)
        sql = "select compnum from components where comid = '" & coid & "'"
        cvalu = fail.strScalar(sql)
        tdcomp.InnerHtml = cvalu
    End Sub
    Private Sub PopFailList(ByVal cid As String, ByVal comid As String, ByVal jpnum As String, ByVal pmtskid As String)

        sql = "select compfailid, failuremode from componentfailmodes where compid = '" & cid & "' and " _
        + "comid = '" & coid & "' and comid <> '0' order by failuremode"

        sql = "select failid, failuremode from jpfailmodes where jpid = '" & jpnum & "' " _
        + "and failid not in (" _
         + "select failid from componentfailmodes where comid = '" & comid & "') and pmtskid = '" & pmtskid & "' order by failuremode"
        dr = fail.GetRdrData(sql)
        lbfailmodes.DataSource = dr
        lbfailmodes.DataValueField = "failid"
        lbfailmodes.DataTextField = "failuremode"
        lbfailmodes.DataBind()
        dr.Close()
        Try
            lbfailmodes.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub

    Private Sub PopFail(ByVal cid As String, ByVal comid As String, ByVal jpnum As String)
        Dim chk As String = lblfailchk.Value
        Dim scnt As Integer
        sid = lblsid.Value
        If chk = "" Then
            sql = "select count(*) from pmSiteFM where siteid = '" & sid & "'"
            scnt = fail.Scalar(sql)
            If scnt = 0 Then
                lblfailchk.Value = "open"
                chk = "open"
            Else
                lblfailchk.Value = "site"
                chk = "site"
            End If
        End If
        Dim dt, val, filt As String
        If chk = "open" Then
            dt = "FailureModes"
        ElseIf chk = "site" Then
            dt = "pmSiteFM"
        Else
            Exit Sub
        End If

        val = "failid, failuremode"
        If chk = "site" Then
            filt = " where siteid = '" & sid & "' and failid not in (" _
        + "select failid from componentfailmodes where comid = '" & comid & "') and " _
        + "failid not in (" _
        + "select failid from jpfailmodes where jpid = '" & jpnum & "') " _
        + "order by failuremode asc"
        Else
            filt = " where failid not in (" _
        + "select failid from componentfailmodes where comid = '" & comid & "') and " _
        + "failid not in (" _
        + "select failid from jpfailmodes where jpid = '" & jpnum & "') " _
        + "order by failuremode asc"
        End If
       
        dr = fail.GetList(dt, val, filt) 'removed compid = '" & cid & "' and 
        lbfailmaster.DataSource = dr
        lbfailmaster.DataTextField = "failuremode"
        lbfailmaster.DataValueField = "failid"
        lbfailmaster.DataBind()
        dr.Close()
    End Sub

    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToComp()
    End Sub
    Private Sub ToComp()
        jp = lbljpid.Value
        cid = lblcid.Value
        coid = lblcoid.Value
        pmtskid = lblpmtskid.Value
        wonum = lblwo.Value
        Dim Item As ListItem
        Dim f, fi As String
        fail.Open()
        For Each Item In lbfailmaster.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                GetItems(f, fi, coid, "na")
            End If
        Next
        PopFailList(cid, coid, jp, pmtskid)
        PopFail(cid, coid, jp)
        sql = "usp_UpdateJPFM1 '" & pmtskid & "','" & wonum & "'"
        fail.Update(sql)
        fail.Dispose()
    End Sub
    Private Sub GetItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal typ As String)
        Dim jpnum As String = lbljpid.Value
        pmtskid = lblpmtskid.Value
        wonum = lblwo.Value
        Dim fcnt, fcnt2 As Integer
        sql = "select count(*) from jpfailmodes where pmtskid = '" & pmtskid & "' and failid = '" & fi & "'"
        fcnt = fail.Scalar(sql)
        sql = "select count(*) from jpfailmodes where pmtskid = '" & pmtskid & "'"
        fcnt2 = fail.Scalar(sql)
        If fcnt > 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr483", "jpfaillist.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf fcnt2 >= 5 Then
            Dim strMessage As String = tmod.getmsg("cdstr484", "jpfaillist.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Try
                sql = "usp_addJPFailureMode '" & jpnum & "', '" & fi & "', '" & f & "', '" & comp & "','" & pmtskid & "','" & wonum & "','" & typ & " '"
                fail.Update(sql)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        FromComp()
    End Sub
    Private Sub FromComp()
        jp = lbljpid.Value
        cid = lblcid.Value
        coid = lblcoid.Value
        pmtskid = lblpmtskid.Value
        wonum = lblwo.Value
        Dim Item As ListItem
        Dim f, fi As String
        fail.Open()
        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                RemItems(fi, f)
            End If
        Next
        PopFailList(cid, coid, jp, pmtskid)
        PopFail(cid, coid, jp)
        sql = "usp_UpdateJPFM1 '" & pmtskid & "','" & wonum & "'"
        fail.Update(sql)
        fail.Dispose()
    End Sub
    Private Sub RemItems(ByVal fi As String, ByVal f As String)
        Dim jpnum As String = lblwo.Value
        wonum = lblwo.Value
        pmtskid = lblpmtskid.Value
        Try
            sql = "delete from jpfailmodes where jpid = '" & jpnum & "' and failid = '" & fi & "' and pmtskid = '" & pmtskid & "'"
            fail.Update(sql)
            If wonum <> "" Then
                sql = "delete from wojpfailmodes where jpid = '" & jpnum & "' and failid = '" & fi & "' and pmtskid = '" & pmtskid & "' and wonum = '" & wonum & "'"
                fail.Update(sql)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub cbopts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbopts.SelectedIndexChanged
        Dim opt As String = cbopts.SelectedValue.ToString
        lblopt.Value = opt
        If opt = "0" Then
            lbfailmodes.AutoPostBack = False
            lbfailmaster.AutoPostBack = False
        Else
            lbfailmodes.AutoPostBack = True
            lbfailmaster.AutoPostBack = True
        End If
    End Sub

    Private Sub lbfailmodes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbfailmodes.SelectedIndexChanged
        If lblopt.Value = "1" Then
            FromComp()
        End If
    End Sub

    Private Sub lbfailmaster_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbfailmaster.SelectedIndexChanged
        If lblopt.Value = "1" Then
            ToComp()
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1157.Text = axlabs.GetASPXPage("jpfaillist.aspx", "lang1157")
        Catch ex As Exception
        End Try
        Try
            lang1158.Text = axlabs.GetASPXPage("jpfaillist.aspx", "lang1158")
        Catch ex As Exception
        End Try
        Try
            lang1159.Text = axlabs.GetASPXPage("jpfaillist.aspx", "lang1159")
        Catch ex As Exception
        End Try
        Try
            lang1160.Text = axlabs.GetASPXPage("jpfaillist.aspx", "lang1160")
        Catch ex As Exception
        End Try
        Try
            lang1161.Text = axlabs.GetASPXPage("jpfaillist.aspx", "lang1161")
        Catch ex As Exception
        End Try
        Try
            lang1162.Text = axlabs.GetASPXPage("jpfaillist.aspx", "lang1162")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                ibtnret.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtnret.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtnret.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtnret.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtnret.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddtosite.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("jpfaillist.aspx", "btnaddtosite") & "')")
            btnaddtosite.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
