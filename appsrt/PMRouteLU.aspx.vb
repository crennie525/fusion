

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class PMRouteLU
    Inherits System.Web.UI.Page
	Protected WithEvents lang1179 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim lu As New Utilities
    Dim list, sid, retid, retid1, retid2, retid3, retid4, retid5, retid6, eq, pm, jp, did, clid, locid, nc As String
    Dim filter As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = "100"

    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdlist As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid4 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid5 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid6 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dcell As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllist As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents trnav As System.Web.UI.HtmlControls.HtmlTableRow
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsrch1 As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            list = Request.QueryString("list").ToString
            lbllist.Value = list
            retid = Request.QueryString("id").ToString
            lblid.Value = retid
            eq = Request.QueryString("eq").ToString
            lbleq.Value = eq
            retid2 = Request.QueryString("id2").ToString
            lblid2.Value = retid2
            pm = Request.QueryString("pm").ToString
            lblpm.Value = pm
            retid3 = Request.QueryString("id3").ToString
            lblid3.Value = retid3
            did = Request.QueryString("did").ToString
            lbldid.Value = did
            clid = Request.QueryString("clid").ToString
            lblclid.Value = clid
            retid4 = Request.QueryString("id4").ToString
            lblid4.Value = retid4
            locid = Request.QueryString("locid").ToString
            lbllocid.Value = locid
            retid5 = Request.QueryString("id5").ToString
            lblid5.Value = retid5
            jp = Request.QueryString("jp").ToString
            lbljp.Value = jp
            retid6 = Request.QueryString("id6").ToString
            lblid6.Value = retid6
            nc = Request.QueryString("nc").ToString
            lblnc.Value = nc
            If list <> "PM" And list <> "PMMAX" Then
                trnav.Attributes.Add("class", "details")
            End If
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            lu.Open()
            If list = "rt" Then
                GetRoutes()
            ElseIf list = "pmrt" Then
                GetPMRoutes()
            ElseIf list = "pmrtmax" Then
                GetPMMAXRoutes()
            ElseIf list = "eq" Then
                GetEq()
            ElseIf list = "dc" Then
                GetDC()
            ElseIf list = "nc" Then
                GetNC()
            ElseIf list = "pm" Then
                GetPM(PageNumber)
            ElseIf list = "pmmax" Then
                GetPMMAX(PageNumber)
            ElseIf list = "jp" Then
                GetJobPlans()
            End If
            lu.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                lu.Open()
                GetNext()
                lu.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                lu.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                list = lbllist.Value
                If list = "PM" Then
                    GetPM(PageNumber)
                Else
                    GetPMMAX(PageNumber)
                End If

                lu.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                lu.Open()
                GetPrev()
                lu.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                lu.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                list = lbllist.Value
                If list = "PM" Then
                    GetPM(PageNumber)
                Else
                    GetPMMAX(PageNumber)
                End If
                lu.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            list = lbllist.Value
            If list = "PM" Then
                GetPM(PageNumber)
            Else
                GetPMMAX(PageNumber)
            End If
        Catch ex As Exception
            lu.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr340", "PMMainMan.aspx.vb")

            lu.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            list = lbllist.Value
            If list = "PM" Then
                GetPM(PageNumber)
            Else
                GetPMMAX(PageNumber)
            End If
        Catch ex As Exception
            lu.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr341", "PMMainMan.aspx.vb")

            lu.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPMMAX(ByVal PageNumber As Integer)
        Dim filt, s, e, Filter, FilterCNT, srch As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        Dim sb As StringBuilder = New StringBuilder
        sid = lblsid.Value
        Filter = "e.siteid = ''" & sid & "''"
        FilterCNT = "e.siteid = '" & sid & "'"
        If srch <> "" Then
            Filter += " and e.eqnum like ''" & srch & "''"
            FilterCNT += " and e.eqnum like '" & srch & "'"
        End If

        sql = "select count(distinct pm.pmid) from pmmax pm " _
          + "left join equipment e on e.eqid = pm.eqid " _
          + "left join workorder w on w.wonum = pm.wonum " _
          + "left join pmroutes pr on pr.rid = pm.rid " _
              + " where " + FilterCNT

        Dim intPgCnt, intPgNav As Integer
        intPgCnt = lu.Scalar(sql)
        If intPgCnt > 0 Then
            txtpg.Value = PageNumber
            intPgNav = lu.PageCountRev(intPgCnt, PageSize)
            txtpgcnt.Value = intPgNav
            If intPgNav = 0 Then
                lblpg.Text = "Page 0 of 0"
            Else
                lblpg.Text = "Page " & PageNumber & " of " & intPgNav
            End If
            sql = "usp_getAllPMMAXPg2 '" & sid & "', '" & PageNumber & "', '" & PageSize & "', '" & Filter & "'"
            sb.Append("<table width=""520"">")
            sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""140"">" & tmod.getlbl("cdlbl135", "PMRouteLU.aspx.vb") & "</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""120"">PdM</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""260"">PM</td></tr>")
            'sb.Append("<td class=""details"" width=""20"">&nbsp;</td></tr>")
            sb.Append("<tr><td colspan=""3""><div style=""OVERFLOW: auto; width: 520px; height: 350px;"">")
            sb.Append("<table width=""500"">")
            sb.Append("<tr><td width=""140""></td><td width=""120""></td><td width=""240""></td></tr>")
            Dim rowid As Integer = 0
            Dim bc, pmid, pmd, pm, eq, eqid, did, dept, clid, cell, locid, loc, jp, jpid, nc, ncid As String
            jp = ""
            jpid = ""
            nc = ""
            ncid = "0"
            dr = lu.GetRdrData(sql)
            While dr.Read
                If rowid = 0 Then
                    rowid = 1
                    bc = "white"
                Else
                    rowid = 0
                    bc = "#E7F1FD"
                End If
                pmid = dr.Item("pmid").ToString
                'pmd = dr.Item("pmd").ToString
                pm = dr.Item("pm").ToString
                eq = dr.Item("eqnum").ToString
                eqid = dr.Item("eqid").ToString
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                dept = dr.Item("dcell").ToString
                locid = dr.Item("locid").ToString
                loc = dr.Item("location").ToString

                sb.Append("<tr bgcolor=""" & bc & """><td class=""plainlabel"">" & eq & "</td>")
                sb.Append("<td class=""plainlabel"">" & pmd & "</td>")
                sb.Append("<td class=""linklabel""><a href=""#"" onclick=""pmret('" & pmid & "','" & pm & "','" & eqid & "','" & eq & "','" & did & "','" & clid & "','" & dept & "','" & locid & "','" & loc & "','" & jp & "','" & jpid & "','" & nc & "','" & ncid & "');"">")
                sb.Append(pm & "</a></td></tr>")


            End While
            dr.Close()
            sb.Append("</table></div></td></tr></table>")
            tdlist.InnerHtml = sb.ToString
        End If
    End Sub
    Private Sub GetPM(ByVal PageNumber As Integer)
        Dim filt, s, e, Filter, FilterCNT, srch As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        Dim sb As StringBuilder = New StringBuilder
        sid = lblsid.Value
        Filter = "e.siteid = ''" & sid & "''"
        FilterCNT = "e.siteid = '" & sid & "'"
        If srch <> "" Then
            Filter += " and e.eqnum like ''" & srch & "''"
            FilterCNT += " and e.eqnum like '" & srch & "'"
        End If

        sql = "select count(distinct pm.pmid) from pm pm " _
          + "left join equipment e on e.eqid = pm.eqid " _
          + "left join workorder w on w.wonum = pm.wonum " _
          + "left join pmroutes pr on pr.rid = pm.rid " _
          + "left join pmtrack pt on pt.pmid = pm.pmid " _
              + " where " + FilterCNT

        Dim intPgCnt, intPgNav As Integer
        intPgCnt = lu.Scalar(sql)
        If intPgCnt > 0 Then
            txtpg.Value = PageNumber
            intPgNav = lu.PageCountRev(intPgCnt, PageSize)
            txtpgcnt.Value = intPgNav
            If intPgNav = 0 Then
                lblpg.Text = "Page 0 of 0"
            Else
                lblpg.Text = "Page " & PageNumber & " of " & intPgNav
            End If
            sql = "usp_getAllPMPg2 '" & sid & "', '" & PageNumber & "', '" & PageSize & "', '" & Filter & "'"
            sb.Append("<table width=""520"">")
            sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""140"">" & tmod.getlbl("cdlbl135", "PMRouteLU.aspx.vb") & "</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""120"">PdM</td>")
            sb.Append("<td class=""thdrsingg plainlabel"" width=""260"">PM</td></tr>")
            'sb.Append("<td class=""details"" width=""20"">&nbsp;</td></tr>")
            sb.Append("<tr><td colspan=""3""><div style=""OVERFLOW: auto; width: 520px; height: 350px;"">")
            sb.Append("<table width=""500"">")
            sb.Append("<tr><td width=""140""></td><td width=""120""></td><td width=""240""></td></tr>")
            Dim rowid As Integer = 0
            Dim bc, pmid, pmd, pm, eq, eqid, did, dept, clid, cell, locid, loc, jp, jpid, nc, ncid As String
            jp = ""
            jpid = ""
            nc = ""
            ncid = "0"
            dr = lu.GetRdrData(sql)
            While dr.Read
                If rowid = 0 Then
                    rowid = 1
                    bc = "white"
                Else
                    rowid = 0
                    bc = "#E7F1FD"
                End If
                pmid = dr.Item("pmid").ToString
                'pmd = dr.Item("pmd").ToString
                pm = dr.Item("pm").ToString
                eq = dr.Item("eqnum").ToString
                eqid = dr.Item("eqid").ToString
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                dept = dr.Item("dcell").ToString
                locid = dr.Item("locid").ToString
                loc = dr.Item("location").ToString

                sb.Append("<tr bgcolor=""" & bc & """><td class=""plainlabel"">" & eq & "</td>")
                sb.Append("<td class=""plainlabel"">" & pmd & "</td>")
                sb.Append("<td class=""linklabel""><a href=""#"" onclick=""pmret('" & pmid & "','" & pm & "','" & eqid & "','" & eq & "','" & did & "','" & clid & "','" & dept & "','" & locid & "','" & loc & "','" & jp & "','" & jpid & "','" & nc & "','" & ncid & "');"">")
                sb.Append(pm & "</a></td></tr>")


            End While
            dr.Close()
            sb.Append("</table></div></td></tr></table>")
            tdlist.InnerHtml = sb.ToString
        End If



    End Sub
    Private Sub GetEq()
        Dim sb As StringBuilder = New StringBuilder
        sid = lblsid.Value
        sql = "select e.eqid, e.eqnum, e.eqdesc, p.pmid, " _
        + "e.dept_id, e.cellid, d.dept_line + '/' + c.cell_name as dcell, e.locid, l.location  " _
        + "from equipment e " _
        + "left join pm p on p.eqid = e.eqid " _
        + "left join dept d on e.dept_id = d.dept_id " _
        + "left join cells c on e.cellid = c.cellid " _
        + "left join pmlocations l on l.locid = e.locid " _
        + "where e.siteid = '" & sid & "'"
        sb.Append("<table width=""520"">")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""150"">" & tmod.getlbl("cdlbl138" , "PMRouteLU.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""370"">" & tmod.getlbl("cdlbl139" , "PMRouteLU.aspx.vb") & "</td></tr>")
        'sb.Append("<td class=""details"" width=""20"">&nbsp;</td></tr>")
        sb.Append("<tr><td colspan=""3""><div style=""OVERFLOW: auto; width: 520px; height: 350px;"">")
        sb.Append("<table width=""500"">")
        sb.Append("<tr><td width=""150""></td><td width=""350""></td></tr>")
        Dim rowid As Integer = 0
        Dim bc, pmid, pmd, pm, eq, eqid, did, dept, clid, cell, locid, loc, jp, jpid, nc, ncid, desc As String
        Dim eqhld As String = "0"
        Dim pmhld As String = lblpm.Value
        Dim lochld As String = lbllocid.Value
        dr = lu.GetRdrData(sql)
        While dr.Read
            If lochld <> "" Then
                locid = dr.Item("locid").ToString
                If locid <> lochld Then
                    locid = "0"
                End If
            End If
            If pmhld <> "" Then
                pmid = dr.Item("pmid").ToString
                If pmid <> pmhld Then
                    pmid = "0"
                End If
            End If
            eqid = dr.Item("eqid").ToString
            If eqid <> eqhld Then
                eqhld = eqid
                If rowid = 0 Then
                    rowid = 1
                    bc = "white"
                Else
                    rowid = 0
                    bc = "#E7F1FD"
                End If
                pm = ""
                eq = dr.Item("eqnum").ToString
                desc = dr.Item("eqnum").ToString
                eqid = dr.Item("eqid").ToString
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                dept = dr.Item("dcell").ToString
                'locid = dr.Item("locid").ToString

                sb.Append("<tr bgcolor=""" & bc & """><td class=""linklabel""><a href=""#"" onclick=""pmret('" & pmid & "','" & pm & "','" & eqid & "','" & eq & "','" & did & "','" & clid & "','" & dept & "','" & locid & "','" & loc & "','" & jp & "','" & jpid & "','" & nc & "','" & ncid & "');"">")
                sb.Append(eq & "</a></td>")
                sb.Append("<td class=""plainlabel"">" & desc & "</td></tr>")
            End If


        End While
        dr.Close()
        sb.Append("</table></div></td></tr></table>")
        tdlist.InnerHtml = sb.ToString
    End Sub
    Private Sub GetNC()
        Dim sb As StringBuilder = New StringBuilder
        sid = lblsid.Value
        sql = "select e.ncid, e.ncnum, e.ncdesc, " _
        + "e.dept_id, e.cellid, d.dept_line + '/' + c.cell_name as dcell, e.locid, l.location  " _
        + "from noncritical e " _
        + "left join dept d on e.dept_id = d.dept_id " _
        + "left join cells c on e.cellid = c.cellid " _
        + "left join pmlocations l on l.locid = e.locid " _
        + "where e.siteid = '" & sid & "'"
        sb.Append("<table width=""520"">")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""150"">" & tmod.getlbl("cdlbl140" , "PMRouteLU.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""370"">" & tmod.getlbl("cdlbl141" , "PMRouteLU.aspx.vb") & "</td></tr>")
        'sb.Append("<td class=""details"" width=""20"">&nbsp;</td></tr>")
        sb.Append("<tr><td colspan=""3""><div style=""OVERFLOW: auto; width: 520px; height: 350px;"">")
        sb.Append("<table width=""500"">")
        sb.Append("<tr><td width=""150""></td><td width=""350""></td></tr>")
        Dim rowid As Integer = 0
        Dim bc, pmid, pmd, pm, eq, eqid, did, dept, clid, cell, locid, loc, jp, jpid, nc, ncid, desc As String
        Dim eqhld As String = "0"
        Dim pmhld As String = lblpm.Value
        dr = lu.GetRdrData(sql)
        While dr.Read

            eqid = dr.Item("ncid").ToString
            If eqid <> eqhld Then
                eqhld = eqid
                If rowid = 0 Then
                    rowid = 1
                    bc = "white"
                Else
                    rowid = 0
                    bc = "#E7F1FD"
                End If
                pm = ""
                nc = dr.Item("ncnum").ToString
                desc = dr.Item("ncnum").ToString
                ncid = dr.Item("ncid").ToString
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                dept = dr.Item("dcell").ToString
                locid = dr.Item("locid").ToString
                loc = dr.Item("location").ToString
                sb.Append("<tr bgcolor=""" & bc & """><td class=""linklabel""><a href=""#"" onclick=""pmret('" & pmid & "','" & pm & "','" & eqid & "','" & eq & "','" & did & "','" & clid & "','" & dept & "','" & locid & "','" & loc & "','" & jp & "','" & jpid & "','" & nc & "','" & ncid & "');"">")
                sb.Append(nc & "</a></td>")
                sb.Append("<td class=""plainlabel"">" & desc & "</td></tr>")
            End If


        End While
        dr.Close()
        sb.Append("</table></div></td></tr></table>")
        tdlist.InnerHtml = sb.ToString
    End Sub
    Private Sub GetDC()
        Dim sb As StringBuilder = New StringBuilder
        sid = lblsid.Value
        sql = "select distinct d.dept_id, c.cellid, e.eqid, p.pmid, n.ncid, n.ncnum, " _
        + "d.dept_line + '/' + isnull(c.cell_name, 'No Cell') as dcell, e.locid, l.location " _
        + "from dept d " _
        + "left join cells c on c.dept_id = d.dept_id " _
        + "left join equipment e on e.dept_id = d.dept_id " _
        + "left join pmlocations l on l.locid = e.locid " _
        + "left join pm p on p.eqid = e.eqid " _
        + "left join noncritical n on n.dept_id = d.dept_id " _
        + "where d.siteid = '" & sid & "'"

        sb.Append("<table width=""520"">")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""520"">" & tmod.getlbl("cdlbl142" , "PMRouteLU.aspx.vb") & "</td></tr>")

        sb.Append("<tr><td colspan=""3""><div style=""OVERFLOW: auto; width: 520px; height: 350px;"">")
        sb.Append("<table width=""500"">")
        sb.Append("<tr><td width=""500""></td></tr>")
        Dim rowid As Integer = 0
        Dim bc, pmid, pmd, pm, eq, eqid, did, dept, clid, cell, locid, loc, jp, jpid, nc, ncid, desc As String
        Dim dhld As String = "0"
        Dim chld As String = "0"
        Dim eqhld As String = lbleq.Value
        Dim pmhld As String = lblpm.Value
        dr = lu.GetRdrData(sql)
        While dr.Read

            If pmhld <> "" Then
                pmid = dr.Item("pmid").ToString
                If pmid <> pmhld Then
                    pmid = "0"
                End If
            End If
            If eqhld <> "" Then
                eqid = dr.Item("eqid").ToString
                If eqid <> eqhld Then
                    eqid = "0"
                End If
            End If
            dept = dr.Item("dcell").ToString
            If dept <> dhld Then
                dhld = dept
                If rowid = 0 Then
                    rowid = 1
                    bc = "white"
                Else
                    rowid = 0
                    bc = "#E7F1FD"
                End If
                pm = ""
                eq = ""
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                locid = dr.Item("locid").ToString
                loc = dr.Item("location").ToString
                sb.Append("<tr bgcolor=""" & bc & """><td class=""linklabel""><a href=""#"" onclick=""pmret('" & pmid & "','" & pm & "','" & eqid & "','" & eq & "','" & did & "','" & clid & "','" & dept & "','" & locid & "','" & loc & "','" & jp & "','" & jpid & "','" & nc & "','" & ncid & "');"">")
                sb.Append(dept & "</a></td></tr>")
            End If
        End While
        dr.Close()
        sb.Append("</table></div></td></tr></table>")
        tdlist.InnerHtml = sb.ToString
    End Sub
    Private Sub GetRoutes()
        Dim srch As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)

        Dim sb As StringBuilder = New StringBuilder
        sid = lblsid.Value
        If srch = "" Then
            sql = "select distinct * from pmroutes where siteid = '" & sid & "'"
        Else
            srch = "%" & srch & "%"
            sql = "select distinct * from pmroutes where siteid = '" & sid & "' and " _
            + "(route like '" & srch & "' or rttype like '" & srch & "' or createdby like '" & srch & "') "
        End If

        sb.Append("<table width=""520"">")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""150"">" & tmod.getlbl("cdlbl143", "PMRouteLU.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""370"">" & tmod.getlbl("cdlbl144", "PMRouteLU.aspx.vb") & "</td></tr>")
        'sb.Append("<td class=""details"" width=""20"">&nbsp;</td></tr>")
        sb.Append("<tr><td colspan=""3""><div style=""OVERFLOW: auto; width: 520px; height: 350px;"">")
        sb.Append("<table width=""500"">")
        sb.Append("<tr><td width=""150""></td><td width=""350""></td></tr>")
        Dim rowid As Integer = 0
        Dim bc, rtid, rt, desc As String
        dr = lu.GetRdrData(sql)
        While dr.Read
            If rowid = 0 Then
                rowid = 1
                bc = "white"
            Else
                rowid = 0
                bc = "#E7F1FD"
            End If
            rtid = dr.Item("rid").ToString
            rt = dr.Item("route").ToString
            desc = dr.Item("description").ToString
            sb.Append("<tr bgcolor=""" & bc & """><td class=""linklabel""><a href=""#"" onclick=""rtret('" & rtid & "');"">")
            sb.Append(rt & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & desc & "</td></tr>")

        End While
        dr.Close()
        sb.Append("</table></div></td></tr></table>")
        tdlist.InnerHtml = sb.ToString
    End Sub
    Private Sub GetPMMAXRoutes()
        Dim srch As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)

        Dim sb As StringBuilder = New StringBuilder
        sid = lblsid.Value
        If srch = "" Then
            sql = "select * from pmroutes where siteid = '" & sid & "' and rttype = 'PMMAX'"
        Else
            srch = "%" & srch & "%"
            sql = "select * from pmroutes where siteid = '" & sid & "' and rttype = 'PMMAX' and " _
            + "(route like '" & srch & "' or rttype like '" & srch & "' or createdby like '" & srch & "') "
        End If

        sb.Append("<table width=""520"">")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""150"">" & tmod.getlbl("cdlbl145", "PMRouteLU.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""370"">" & tmod.getlbl("cdlbl146", "PMRouteLU.aspx.vb") & "</td></tr>")
        'sb.Append("<td class=""details"" width=""20"">&nbsp;</td></tr>")
        sb.Append("<tr><td colspan=""3""><div style=""OVERFLOW: auto; width: 520px; height: 350px;"">")
        sb.Append("<table width=""500"">")
        sb.Append("<tr><td width=""150""></td><td width=""350""></td></tr>")
        Dim rowid As Integer = 0
        Dim bc, rtid, rt, desc As String
        dr = lu.GetRdrData(sql)
        While dr.Read
            If rowid = 0 Then
                rowid = 1
                bc = "white"
            Else
                rowid = 0
                bc = "#E7F1FD"
            End If
            rtid = dr.Item("rid").ToString
            rt = dr.Item("route").ToString
            desc = dr.Item("description").ToString
            sb.Append("<tr bgcolor=""" & bc & """><td class=""linklabel""><a href=""#"" onclick=""pmrtret('" & rtid & "','" & rt & "');"">")
            sb.Append(rt & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & desc & "</td></tr>")

        End While
        dr.Close()
        sb.Append("</table></div></td></tr></table>")
        tdlist.InnerHtml = sb.ToString
    End Sub
    Private Sub GetPMRoutes()
        Dim srch As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)

        Dim sb As StringBuilder = New StringBuilder
        sid = lblsid.Value
        If srch = "" Then
            sql = "select distinct * from pmroutes where siteid = '" & sid & "' and rttype = 'PM'"
        Else
            srch = "%" & srch & "%"
            sql = "select distinct * from pmroutes where siteid = '" & sid & "' and rttype = 'PM' and " _
            + "(route like '" & srch & "' or rttype like '" & srch & "' or createdby like '" & srch & "') "
        End If

        sb.Append("<table width=""520"">")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""150"">" & tmod.getlbl("cdlbl145", "PMRouteLU.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""370"">" & tmod.getlbl("cdlbl146", "PMRouteLU.aspx.vb") & "</td></tr>")
        'sb.Append("<td class=""details"" width=""20"">&nbsp;</td></tr>")
        sb.Append("<tr><td colspan=""3""><div style=""OVERFLOW: auto; width: 520px; height: 350px;"">")
        sb.Append("<table width=""500"">")
        sb.Append("<tr><td width=""150""></td><td width=""350""></td></tr>")
        Dim rowid As Integer = 0
        Dim bc, rtid, rt, desc As String
        dr = lu.GetRdrData(sql)
        While dr.Read
            If rowid = 0 Then
                rowid = 1
                bc = "white"
            Else
                rowid = 0
                bc = "#E7F1FD"
            End If
            rtid = dr.Item("rid").ToString
            rt = dr.Item("route").ToString
            desc = dr.Item("description").ToString
            sb.Append("<tr bgcolor=""" & bc & """><td class=""linklabel""><a href=""#"" onclick=""pmrtret('" & rtid & "','" & rt & "');"">")
            sb.Append(rt & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & desc & "</td></tr>")

        End While
        dr.Close()
        sb.Append("</table></div></td></tr></table>")
        tdlist.InnerHtml = sb.ToString
    End Sub
    Private Sub GetJobPlans()
        Dim srch As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)

        sid = lblsid.Value

        sql = "select r.*, e.eqnum, r.ncid, n.ncnum, " _
        + "r.deptid, r.cellid, d.dept_line + '/' + c.cell_name as dcell, r.locid, l.location " _
        + "from pmjobplans r " _
        + "left join equipment e on e.eqid = r.eqid " _
        + "left join noncritical n on n.ncid = r.ncid " _
        + "left join dept d on d.dept_id = r.deptid " _
        + "left join cells c on c.cellid = r.cellid " _
        + "left join pmlocations l on l.locid = r.locid " _
        + "where r.scheduled = 0 and r.siteid = '" & sid & "' "

        Dim sb As StringBuilder = New StringBuilder

        sb.Append("<table width=""480"">")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""140"">" & tmod.getlbl("cdlbl147" , "PMRouteLU.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""230"">" & tmod.getlbl("cdlbl148" , "PMRouteLU.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""90"">" & tmod.getlbl("cdlbl149" , "PMRouteLU.aspx.vb") & "</td>")

        sb.Append("<td  width=""20"">&nbsp;</td></tr>")
        sb.Append("<tr><td colspan=""3""><div style=""OVERFLOW: auto; width: 460px;"">")
        sb.Append("<table width=""460"">")
        sb.Append("<tr><td width=""140""></td><td width=""250""></td>")
        sb.Append("<td width=""90""></td></tr>")
        Dim rowid As Integer = 0
        Dim bc, pmid, pmd, pm, eq, eqid, did, dept, clid, cell, locid, loc, jp, jpid, nc, ncid, desc, typ As String
        dr = lu.GetRdrData(sql)
        While dr.Read
            If rowid = 0 Then
                rowid = 1
                bc = "white"
            Else
                rowid = 0
                bc = "#E7F1FD"
            End If
            jpid = dr.Item("jpid").ToString
            jp = dr.Item("jpnum").ToString
            desc = dr.Item("description").ToString
            typ = dr.Item("type").ToString
            pmid = "0"
            'pmd = dr.Item("pmd").ToString
            pm = ""
            eq = dr.Item("eqnum").ToString
            eqid = dr.Item("eqid").ToString
            If eqid = "" Then
                eqid = lbleq.Value
            End If
            did = dr.Item("deptid").ToString
            If did = "" Then
                did = lbldid.Value
            End If
            clid = dr.Item("cellid").ToString
            If clid = "" Then
                clid = lblclid.Value
            End If
            dept = dr.Item("dcell").ToString
            locid = dr.Item("locid").ToString
            If locid = "0" Then
                locid = lbllocid.Value
            End If
            loc = dr.Item("location").ToString
            ncid = dr.Item("ncid").ToString
            If ncid = "0" Then
                ncid = lblnc.Value
            End If
            nc = dr.Item("ncnum").ToString

            sb.Append("<tr bgcolor=""" & bc & """><td class=""linklabel""><a href=""#"" onclick=""pmret('" & pmid & "','" & pm & "','" & eqid & "','" & eq & "','" & did & "','" & clid & "','" & dept & "','" & locid & "','" & loc & "','" & jp & "','" & jpid & "','" & nc & "','" & ncid & "');"">")
            sb.Append(jp & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & desc & "</td>")
            sb.Append("<td class=""plainlabel"">" & typ & "</td></tr>")



        End While
        dr.Close()
        sb.Append("</table></div></td></tr></table>")
        tdlist.InnerHtml = sb.ToString
    End Sub

    Private Sub btnsrch1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsrch1.Click
        list = lbllist.Value
        PageNumber = 1
        txtpg.Value = "1"
        lu.Open()
        If list = "rt" Then
            GetRoutes()
        ElseIf list = "pmrt" Then
            GetPMRoutes()
        ElseIf list = "pmrtmax" Then
            GetPMMAXRoutes()
        ElseIf list = "eq" Then
            GetEq()
        ElseIf list = "dc" Then
            GetDC()
        ElseIf list = "nc" Then
            GetNC()
        ElseIf list = "pm" Then
            GetPM(PageNumber)
        ElseIf list = "pmmax" Then
            GetPMMAX(PageNumber)
        ElseIf list = "jp" Then
            GetJobPlans()
        End If
        lu.Dispose()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1179.Text = axlabs.GetASPXPage("PMRouteLU.aspx", "lang1179")
        Catch ex As Exception
        End Try

    End Sub

End Class
