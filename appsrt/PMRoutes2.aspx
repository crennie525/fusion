<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMRoutes2.aspx.vb" Inherits="lucy_r12.PMRoutes2" %>

<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title runat="server" id="pgtitle">PMRoutes2</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    <script language="JavaScript" src="../scripts/dragitem.js"></script>
    <script language="JavaScript" src="../scripts1/PMRoutes2aspx3.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function getpic(eqid, funcid, tasknum) {
            //alert(eqid + ", " + funcid + ", " + tasknum)
            var popwin = "directories=0,height=250,width=300,location=0,menubar=0,resizable=1,status=0,toolbar=0,scrollbars=1,top=200,left=500";
            var typ = document.getElementById("lbltyp").value;
            if (typ == "RBAS") {
                window.open("../pmpics/taskimagepm.aspx?eqid=" + eqid + "&funcid=" + funcid + "&tasknum=" + tasknum + "&date=" + Date(), "repWin", popwin);
            }
            else if (typ == "RBAST") {
                window.open("../tpmpics/taskimagepmtpm.aspx?eqid=" + eqid + "&funcid=" + funcid + "&tasknum=" + tasknum + "&date=" + Date(), "repWin", popwin);
            }
            
        }
        function savetop() {
            var rt = document.getElementById("txtroute").value
            var typ = document.getElementById("lbltyp").value;
            if (rt == "") {
                alert("Route Required")
            }
            else if (typ == "Select" || typ == "0" || typ == "") {
                alert("Route Type Required")
            }
            else {
                document.getElementById("lblsubmit").value = "savert";
                document.getElementById("form1").submit();
            }
        }
        function refit() {
            from = document.getElementById("lblfrom").value;
            var typ = document.getElementById("lbltyp").value;
            window.location = "PMRoutes2.aspx?start=no&from=" + from + "&typ=" + typ;
        }
        function GetType(app, sval) {
            //alert(app + ", " + sval)
            //var ghostoff = document.getElementById("lblghostoff").value;
            //var xstatus = document.getElementById("lblxstatus").value;
            //alert(ghostoff + ", " + xstatus)
            var xflg = 0;
            //if (ghostoff == "yes" && xstatus == "yes") {
            //    xflg = 1;
            //}
            var typlst
            var sstr
            var ptlst
            var ssti
            var ptolst
            var typ = document.getElementById("lbltyp").value;
            if (app == "d" && typ != "RBAST") {
                typlst = document.getElementById("ddtype");
                ptlst = document.getElementById("ddpt");
                sstr = typlst.options[typlst.selectedIndex].text;
                ssti = document.getElementById("ddtype").value;
                //sstr == "4 - Cond Monitoring" || sstr == "4 - Verifica e Controllo di Cond"
                if (ssti == "7") {
                    document.getElementById("ddpt").disabled = false;
                }
                else {
                    ptlst.value = "0";
                    document.getElementById("ddpt").disabled = true;
                }
            }
        }
        function retpm() {
            var pm = document.getElementById("lblfrom").value;
            var typ = document.getElementById("lbltyp").value;
            if (pm == "rm") {
                window.location = "PMRoutesMain.aspx?typ=" + typ;
            }
        }
        function addrt() {
            var rt = document.getElementById("txtroute").value;
            var typ = document.getElementById("lbltyp").value;
            var otyp = document.getElementById("lblrtyp").value;
            var ort = document.getElementById("lbloldrte").value;
            //alert(typ)
            if (typ == "RBAS" || typ == "RBAST") {
                if (rt == "") {
                    alert("Route Required")
                }
                else {
                    //alert(document.getElementById("lblroute").value)
                    //checkretpmr(sid, skillid, qty, freq, rdid, rteid)
                    var ptid = document.getElementById("lblptid").value;
                    var skillid = document.getElementById("lblskillid").value;
                    //alert(skillid)
                    var qty = document.getElementById("lblskillqty").value;
                    var freq = document.getElementById("lblfreq").value;
                    var rdid = document.getElementById("lblrdid").value;
                    var sid = document.getElementById("lblsid").value;
                    var rteid = document.getElementById("lblroute").value;
                    var shift = document.getElementById("lblshift").value;
                    var days = document.getElementById("lbldays").value;
                    checkretpmr(sid, ptid, skillid, qty, freq, rdid, rteid, days, shift);
                    //checkretpmr(sid, ptid, skillid, qty, freq, rdid, rteid)
                }
            }
            if (rt == "") {
                alert("Route Required")
            }
            else if (typ == "Select" || typ == "0" || typ == "") {
                alert("Route Type Required")
            }
            else if (otyp != typ && otyp != "") {
                alert("Please Save Route Changes Before Continuing")
            }
            else if (ort != rt && ort != "") {
                alert("Please Save Route Changes Before Continuing")
            }
            else {
                document.getElementById("lblsubmit").value = "add"
                document.getElementById("form1").submit();
            }
        }
        function pmmanual() {
            var rt = document.getElementById("txtroute").value;
            var typ = document.getElementById("lbltyp").value;
            if (rt == "") {
                alert("Route Required")
            }
            else if (typ == "Select" || typ == "0" || typ == "") {
                alert("Route Type Required")
            }
            else {
                document.getElementById("lblsubmit").value = "addrtman"
                document.getElementById("form1").submit();
            }
        }
        var rtflag;
        function handlepm(pmid, pm, eqid, eq, did, clid, dcell, locid, loc, jp, jpid, nc, ncid, retid, retid2, retid3, retid4, retid5, retid6) {
            /*alert(rtflag)
            alert("eq " + eqid)
            alert("pm " + pmid)
            alert("did " + did)
            alert("clid " + clid)
            alert("locid " + locid)
            alert("jpid " + jpid)
            alert("ncid " + ncid)*/
            document.getElementById("lbleq").value = eqid;
            document.getElementById("lblpm").value = pmid;
            document.getElementById("lbldidh").value = did;
            document.getElementById("lblcellidh").value = clid;
            document.getElementById("lbllocidh").value = locid;
            document.getElementById("lbljp").value = jpid;
            document.getElementById("lblncidh").value = ncid;
            if (rtflag == "eq" || rtflag == "pm" || rtflag == "jp" || rtflag == "eq" || rtflag == "pmmax") {
                if (eq != "") {
                    document.getElementById(retid).innerHTML = eq;
                }
            }
            if (rtflag == "pm" || rtflag == "jp" || rtflag == "pmmax") {
                if (dcell != "") {
                    document.getElementById(retid3).innerHTML = dcell;
                }
                if (loc != "") {
                    document.getElementById(retid4).innerHTML = loc;
                }
            }
            if (rtflag == "loc") {
                if (eq != "") {
                    document.getElementById(retid).innerHTML = eq;
                }
                if (pm != "") {
                    document.getElementById(retid2).innerHTML = pm;
                }
                if (dcell != "") {
                    document.getElementById(retid3).innerHTML = dcell;
                }
                if (loc != "") {
                    document.getElementById(retid4).innerHTML = loc;
                }
                if (jp != "") {
                    document.getElementById(retid5).innerHTML = jp;
                }
                if (nc != "") {
                    document.getElementById(retid6).innerHTML = nc;
                }
            }
            if (rtflag == "eq") {
                if (eq != "") {
                    document.getElementById(retid).innerHTML = eq;
                }
                if (pm != "") {
                    document.getElementById(retid2).innerHTML = pm;
                }
                if (dcell != "") {
                    document.getElementById(retid3).innerHTML = dcell;
                }
                if (loc != "") {
                    document.getElementById(retid4).innerHTML = loc;
                }
                if (jp != "") {
                    document.getElementById(retid5).innerHTML = jp;
                }
                if (nc != "") {
                    document.getElementById(retid6).innerHTML = nc;
                }
            }
            if (rtflag == "pm" || rtflag == "pmmax" || rtflag == "rbas") {
                if (pm != "") {
                    document.getElementById(retid2).innerHTML = pm;
                }
            }
            if (rtflag == "jp") {
                document.getElementById(retid5).innerHTML = jp;
                if (nc != "") {
                    document.getElementById(retid6).innerHTML = nc;
                }
            }
            if (rtflag == "nc") {
                if (eq != "") {
                    document.getElementById(retid).innerHTML = eq;
                }
                if (pm != "") {
                    document.getElementById(retid2).innerHTML = pm;
                }
                if (dcell != "") {
                    document.getElementById(retid3).innerHTML = dcell;
                }
                if (loc != "") {
                    document.getElementById(retid4).innerHTML = loc;
                }
                if (jp != "") {
                    document.getElementById(retid5).innerHTML = jp;
                }
                if (nc != "") {
                    document.getElementById(retid6).innerHTML = nc;
                }
            }
            if (rtflag == "dc") {
                document.getElementById(retid3).innerHTML = dcell;
            }
            closert();
        }
        //lblnc, lblncidh
        function handlepmrepret(eqstr) {
            alert(eqstr)
        }
        function getrt(rt, id, eq, id2, pm, id3, did, clid, id4, locid, id5, jp, id6, nc) {
            rtflag = rt;
            //alert(rt)
            document.getElementById("lbleq").value = "";
            document.getElementById("lblpm").value = "";
            document.getElementById("lbldidh").value = "";
            document.getElementById("lblcellidh").value = "";
            document.getElementById("lbllocidh").value = "";
            document.getElementById("lbljp").value = "";
            document.getElementById("lblncidh").value = "";

            sid = document.getElementById("lblsid").value;
            if (rt != "rbas") {
                document.getElementById("rtdiv").style.position = "absolute";
                document.getElementById("rtdiv").style.top = "100px";
                document.getElementById("rtdiv").style.left = "180px";
                document.getElementById("rtdiv").className = "view";
                document.getElementById("ifrt").src = "PMRouteLU.aspx?list=" + rt + "&id=" + id + "&eq=" + eq + "&id2=" + id2 + "&pm=" + pm + "&id3=" + id3 + "&did=" + did + "&clid=" + clid + "&id4=" + id4 + "&locid=" + locid + "&id5=" + id5 + "&jp=" + jp + "&id6=" + id6 + "&nc=" + nc;
            }
            else {
                //alert("eq")
                //var eReturn = 
                var eReturn = window.showModalDialog("../reports/EqSelectDialog.aspx?sid=" + sid + "&rid=" + id2 + "&who=rrep&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                if (eReturn) {
                    var ret = eReturn.split(",");
                    var eqstr = ret[0];
                    var eqid = eqstr.replace("(", "");
                    eqid = eqid.replace(")", "");
                    var skillid = ret[1];
                    var skillqty = ret[2];
                    var freq = ret[3];
                    var rdid = ret[4];
                    var pm = ret[5];
                    var rid = ret[6];
                    document.getElementById("lbleq").value = eqid;
                    document.getElementById(rid).innerHTML = pm;
                    document.getElementById("lblpmstr").value = pm;
                    document.getElementById("lblskillid").value = skillid;
                    document.getElementById("lblskillqty").value = skillqty;
                    document.getElementById("lblfreq").value = freq;
                    document.getElementById("lblrdid").value = rdid;
                }
            }
        }
        function reorderall() {
            var typ = document.getElementById("lbltyp").value;
            var rtid = document.getElementById("lblroute").value;
            if (typ == "RBAS") {
                var eReturn = window.showModalDialog("../reports/RBASTaskOrdDialog.aspx?rteid=" + rtid + "&date=" + Date(), "", "dialogHeight:980px; dialogWidth:980px; resizable=yes");
                if (eReturn) {
                    document.getElementById("lblsubmit").value = "gettasks"
                    //document.getElementById("lblstat").value = "done"
                    document.getElementById("form1").submit();
                }
            }
            else if (typ == "RBAST") {
                var eReturn = window.showModalDialog("../reports/RBASTaskOrdDialogtpm.aspx?rteid=" + rtid + "&date=" + Date(), "", "dialogHeight:980px; dialogWidth:980px; resizable=yes");
                if (eReturn) {
                    document.getElementById("lblsubmit").value = "gettasks"
                    //document.getElementById("lblstat").value = "done"
                    document.getElementById("form1").submit();
                }
            }
        }
        function srchloc(rt, id, eq, id2, pm, id3, did, clid, id4, locid, id5, jp, id6, nc) {
            //alert(nc)
            rtflag = rt;
            document.getElementById("rtdiv1").style.position = "absolute";
            document.getElementById("rtdiv1").style.top = "100px";
            document.getElementById("rtdiv1").style.left = "180px";
            document.getElementById("rtdiv1").className = "view";
            document.getElementById("ifrt1").src = "../locs/LocLook1.aspx?typ=rt&list=" + rt + "&id=" + id + "&eq=" + eq + "&id2=" + id2 + "&pm=" + pm + "&id3=" + id3 + "&did=" + did + "&clid=" + clid + "&id4=" + id4 + "&locid=" + locid + "&id5=" + id5 + "&jp=" + jp + "&id6=" + id6 + "&nc=" + nc;
            //var eReturn = window.showModalDialog("../locs/LocDialog.aspx?typ=rt&list=" + rt + "&id=" + id + "&eq=" + eq + "&id2=" + id2 + "&pm=" + pm + "&id3=" + id3 + "&did=" + did + "&clid=" + clid + "&id4=" + id4 + "&locid=" + locid + "&id5=" + id5 + "&jp=" + jp + "&id6=" + id6 + "&nc=" + nc, "", "dialogHeight:520px; dialogWidth:660px");
            //if (eReturn) {
            //var ret = eReturn.split(",");
            //document.getElementById("lbllid").value = ret[5];
            //document.getElementById("lbltyp").value = ret[1];
            //document.getElementById("lblloc").innerHTML = ret[2];
            //var lid = ret[5];
            //document.getElementById("lblpchk").value = "loc"
            //document.getElementById("form1").submit();

            //}
        }
        function closert() {
            document.getElementById("lbldragid").value = "";
            document.getElementById("lblrowid").value = "";
            document.getElementById("rtdiv").className = "details";
            document.getElementById("rtdiv1").className = "details";
        }
        function checkret() {
            var typ = document.getElementById("lbltyp").value;
            var ret = document.getElementById("lblret").value;
            var sid = document.getElementById("lblsid").value;
            var rteid = document.getElementById("lblroute").value;
            var stat = document.getElementById("lblstat").value;
            if ((typ == "RBAS" || typ == "RBAST") && (ret == "addRBAS" || ret == "addRBAST") && stat != "done") {
                var eReturn = window.showModalDialog("../reports/pmrselectdialog2.aspx?sid=" + sid + "&typ=" + typ + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                if (eReturn) {
                    //skillid + "," + qty + "," + freq + "," + rdid + "," + pm + "," + tcnt;
                    eReturn = eReturn.replace("'", "");
                    //alert(eReturn)
                    var ret = eReturn.split(",");
                    var ptid = ret[0];
                    var skillid = ret[1];
                    var qty = ret[2];
                    var freq = ret[3];
                    var rdid = ret[4];
                    var pm = ret[5];
                    var tcnt = ret[6];
                    var days = ret[7];
                    days = days.replace("'", "");
                    var shift = ret[8];
                    shift = shift.replace("'", "")
                    //if (shift == "'") shift = ""

                    document.getElementById("lblptid").value = ptid;
                    document.getElementById("lblskillid").value = skillid;
                    document.getElementById("lblskillqty").value = qty;
                    document.getElementById("lblfreq").value = freq;
                    document.getElementById("lblrdid").value = rdid;
                    document.getElementById("lblpmstr").value = pm;
                    //alert(days)
                    checkretpmr(sid, ptid, skillid, qty, freq, rdid, rteid, days, shift)
                    //var eReturn2 = window.showModalDialog("../reports/RBASloclookdialog.aspx?sid=" + sid + "&skillid=" + skillid + "&qty=" + qty + "&freq=" + freq + "&rdid=" + rdid + "&rteid=" + rteid + "&date=" + Date(), "", "dialogHeight:800px; dialogWidth:500px; resizable=yes");
                    //if (eReturn2) {
                    //var ret2 = eReturn2;

                    //}
                }
            }
        }
        function checkretpmr(sid, ptid, skillid, qty, freq, rdid, rteid, days, shift) {
            //alert(rteid)
            var eReturn2 = window.showModalDialog("../reports/RBASloclookdialog.aspx?sid=" + sid + "&ptid=" + ptid + "&skillid=" + skillid + "&qty=" + qty + "&freq=" + freq + "&rdid=" + rdid + "&rteid=" + rteid + "&days=" + days + "&shift=" + shift + "&date=" + Date(), "", "dialogHeight:800px; dialogWidth:500px; resizable=yes");
            if (eReturn2) {
                //var ret2 = eReturn2;
                var ret = eReturn2.split(",");
                var rteid = ret[0];
                var days = ret[1];
                var shift = ret[2];

                checkretloc(rteid, days, shift);
            }
        }
        function checkretloc(rteid, days, shift) {
            var eReturn3 = window.showModalDialog("../reports/EQSelectRBASdialog.aspx?rteid=" + rteid + "&days=" + days + "&shift=" + shift + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:600px; resizable=yes");
            if (eReturn3) {
                var ret3 = eReturn3;
                checkreteq(ret3);
            }
        }
        function checkreteq(rteid) {
            var eReturn4 = window.showModalDialog("../reports/RBASTasksdialog.aspx?rteid=" + rteid + "&date=" + Date(), "", "dialogHeight:640px; dialogWidth:700px; resizable=yes");
            if (eReturn4) {
                var ret4 = eReturn4;
                //alert(eReturn4)
                document.getElementById("lblroute").value = ret4;
                document.getElementById("lblsubmit").value = "gettasks"
                document.getElementById("lblstat").value = "done"
                document.getElementById("form1").submit();
            }

        }
        //
    </script>
</head>
<body class="tbg" onload="checkret();sstchur_SmartScroller_Scroll();">
    <form id="form1" method="post" runat="server">
    <table border="0" width="1080" style="left: 7px; position: absolute; top: 83px; z-index: 1;">
        <tr height="24">
            <td class="thdrsing label" colspan="9">
                <asp:Label ID="lang1196" runat="server">Add/Edit Routes</asp:Label>
            </td>
        </tr>
        <tr>
            <td width="20" align="center">
                <img src="../images/appbuttons/minibuttons/routenum.gif">
            </td>
            <td id="tdjp" width="180">
                <asp:TextBox ID="txtroute" runat="server" Width="170px"></asp:TextBox>
            </td>
            <td width="20">
                <img src="../images/appbuttons/minibuttons/routeblnk.gif">
            </td>
            <td class="label" width="80">
                <asp:Label ID="lang1197" runat="server">Description</asp:Label>
            </td>
            <td width="320">
                <asp:TextBox ID="txtroutedesc" runat="server" Width="310px"></asp:TextBox>
            </td>
            <td width="20">
                <img src="../images/appbuttons/minibuttons/routeblnk.gif">
            </td>
            <td class="label" width="40">
                <asp:Label ID="lang1198" runat="server" CssClass="details">Type</asp:Label>
            </td>
            <td width="100">
                <asp:DropDownList ID="ddtyp" runat="server" CssClass="details">
                    <asp:ListItem Value="0" Selected="True">Select</asp:ListItem>
                    <asp:ListItem Value="PM">PM</asp:ListItem>
                    <asp:ListItem Value="JP">Job Plan</asp:ListItem>
                    <asp:ListItem Value="EQ">Equipment</asp:ListItem>
                    <asp:ListItem Value="NC">Misc. Asset</asp:ListItem>
                    <asp:ListItem Value="DC">Department\Cell</asp:ListItem>
                    <asp:ListItem Value="LO">Location</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td width="80">
                <img id="imgadd" onmouseover="return overlib('Create a Route with the Name and Decription You Have Entered')"
                    onmouseout="return nd()" onclick="pmmanual();" src="../images/appbuttons/minibuttons/addnew.gif"
                    runat="server"><img onmouseover="return overlib('View Equipment Route Record List')"
                        onmouseout="return nd()" onclick="getrt('rt');" src="../images/appbuttons/minibuttons/globem.gif">
                <img id="imgsav" onmouseover="return overlib('Save Changes to Route# and Description')"
                    onmouseout="return nd()" onclick="savetop();" src="../images/appbuttons/minibuttons/savedisk1.gif"
                    runat="server">
            </td>
        </tr>
        <tr>
            <td class="thdrsing label" colspan="9" height="24">
                <asp:Label ID="lang1199" runat="server">Current Route</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="9">
                <table width="880">
                    <tr>
                        <td class="bluelabel" width="70">
                            <asp:Label ID="lang1200" runat="server">Route#</asp:Label>
                        </td>
                        <td id="tdrt" class="plainlabel" width="170" runat="server">
                        </td>
                        <td class="bluelabel" width="70">
                            <asp:Label ID="lang1201" runat="server">Description:</asp:Label>
                        </td>
                        <td id="tdrtd" class="plainlabel" width="330" runat="server">
                        </td>
                        <td width="70" align="right">
                            <input style="width: 65px; height: 24px" id="btnsub" class="plainlabel lilmenu" onmouseover="this.className='plainlabel lilmenuhov'"
                                onmouseout="this.className='plainlabel lilmenu'" onclick="addrt();" value="Add Seq#"
                                type="button" name="btnsub" runat="server">
                        </td>
                        <td id="tdret" width="70" align="right" runat="server">
                            <input style="width: 65px; height: 24px" id="btnsub1" class="plainlabel lilmenu"
                                onmouseover="this.className='plainlabel lilmenuhov'" onmouseout="this.className='plainlabel lilmenu'"
                                onclick="retpm();" value="Return" type="button" name="btnsub" runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trrb" runat="server">
            <td colspan="9" align="center">
                <table width="710" style="background-color: Silver">
                    <tr>
                        <td colspan="6" class="plainlabelred" align="center" id="tdnote1" runat="server">
                            Please note that Task Type will not be selected unless all Task Types for the tasks
                            in this route are the same.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" class="plainlabelred" align="center" id="tdnote2" runat="server">
                            Task Type is not required to save.
                        </td>
                    </tr>
                    <tr>
                        <td width="160" class="label" id="tdtt" runat="server">
                            Task Type
                        </td>
                        <td width="160" class="label" id="tdpdm" runat="server">
                            PdM
                        </td>
                        <td width="160" class="label" id="tdskill" runat="server">
                            Revised Skill
                        </td>
                        <td width="70" class="label" id="tdfreq" runat="server">
                            Frequency
                        </td>
                        <td width="100" class="label" id="tdstat" runat="server">
                            Status
                        </td>
                        <td width="30">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddtype" runat="server" CssClass="plainlabel" Width="140px"
                                Rows="1" DataTextField="tasktype" DataValueField="ttid">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddpt" runat="server" CssClass="plainlabel" Width="140px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddskill" runat="server" CssClass="plainlabel" Width="140px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox ID="txtfreq" runat="server" CssClass="plainlabel" Width="55px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddeqstat" runat="server" CssClass="plainlabel" Width="80px">
                            </asp:DropDownList>
                        </td>
                        <td align="center">
                            <asp:ImageButton ID="btnsavetask" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif">
                            </asp:ImageButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="9">
                <table>
                    <tr>
                        <td>
                            <input type="button" id="btntrans" runat="server" onclick="reorderall();" value="Reorder All Tasks" />
                        </td>
                        <td class="label">
                        <asp:Label Width="200px" CssClass="label" ID="lbladd" runat="server">
                        Additional Minutes Required:
                                            </asp:Label>
                            
                        </td>
                        <td>
                            <asp:TextBox ID="txtxtime" runat="server" CssClass="plainlabel" Width="55px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="btnsavextime" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif">
                            </asp:ImageButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="9">
                <table>
                    <tr>
                        <td>
                            <asp:DataGrid ID="dglist" runat="server" BackColor="transparent" GridLines="None"
                                CellSpacing="1" AutoGenerateColumns="False">
                                <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                                <ItemStyle CssClass="ptransrow"></ItemStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Edit">
                                        <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                                CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:ImageButton ID="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                                CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                            <asp:ImageButton ID="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                                CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Seq#">
                                        <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label Width="50px" CssClass="plainlabel" ID="lblseq" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.stopsequence") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox Width="40px" CssClass="plainlabelred" ID="txtseq" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.stopsequence") %>'>
                                            </asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Location">
                                        <HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="plainlabel" ID="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label CssClass="plainlabelred" ID="lbllocs" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
                                            </asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif">
                                        <HeaderStyle Width="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <img id="Img4" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif">
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <img id="lookuploc" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Equipment#">
                                        <HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="plainlabel" ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label CssClass="plainlabelred" ID="lbleqs" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
                                            </asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif">
                                        <HeaderStyle Width="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <img id="Img2" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif">
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <img id="lookupeq" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Misc Asset">
                                        <HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="plainlabel" ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ncnum") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label CssClass="plainlabelred" ID="lblnc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ncnum") %>'>
                                            </asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif">
                                        <HeaderStyle Width="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <img id="Img1" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif">
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <img id="lookupnc" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Department\Cell">
                                        <HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="plainlabel" ID="Label6" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dcell") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label CssClass="plainlabelred" ID="lbldcell" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dcell") %>'>
                                            </asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif">
                                        <HeaderStyle Width="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <img id="Img3" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif">
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <img id="lookupdcell" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PM">
                                        <HeaderStyle Width="180px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="plainlabel" ID="Label13" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmnum") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label CssClass="plainlabelred" ID="lblpms" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmnum") %>'>
                                            </asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif">
                                        <HeaderStyle Width="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <img id="Img5" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif">
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <img id="lookuppm" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Job Plan">
                                        <HeaderStyle Width="180px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="plainlabel" ID="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.jpnum") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label CssClass="plainlabelred" ID="lbljps" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.jpnum") %>'>
                                            </asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif">
                                        <HeaderStyle Width="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <img id="Img6" runat="server" src="../images/appbuttons/minibuttons/magnifierdis.gif">
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <img id="lookupjp" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Function">
                                        <HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="plainlabel" ID="Labelfuncl" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label CssClass="plainlabelred" ID="lblfunce" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
                                            </asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Component">
                                        <HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="plainlabel" ID="Labelfuncl2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label CssClass="plainlabelred" ID="lblfunce2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
                                            </asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Task">
                                        <HeaderStyle Width="420px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="plainlabel" ID="Labeltaskl" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.task") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox Width="420px" CssClass="plainlabel" ID="txttask" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.task") %>'>
                                            </asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>

                                    <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/pic.gif">
                                        <HeaderStyle HorizontalAlign="Center" Width="30px" CssClass="btmmenu plainlabel">
                                        </HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibpic" runat="server" ImageUrl="../images/appbuttons/minibuttons/pic.gif"
                                                CommandName="Pic"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                                    <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/del.gif">
                                        <HeaderStyle HorizontalAlign="Center" Width="30px" CssClass="btmmenu plainlabel">
                                        </HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibDel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                                CommandName="Delete"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                                    <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/trash.png">
                                        <HeaderStyle HorizontalAlign="Center" Width="30px" CssClass="btmmenu plainlabel">
                                        </HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibDel2" runat="server" ImageUrl="../images/appbuttons/minibuttons/trash.png"
                                                CommandName="Delete2"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                                    <asp:TemplateColumn Visible="False">
                                        <ItemTemplate>
                                            <asp:Label CssClass="plainlabel" ID="lbleqidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblpmidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lbljpidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.jpid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblrsidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rsid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lbllocidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.locid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblclidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cellid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lbldidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.deptid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblncidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ncid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblpmstri" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmstr") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblskillidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skillid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblskillqtyi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skillqty") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblfreqi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblrdidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rdid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblptidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ptid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblttidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ttid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblpmtskidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblfuncidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.funcid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lbltasknumi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label CssClass="plainlabel" ID="lbleqid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblpmid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lbljpid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.jpid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblrsid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rsid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lbllocid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.locid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblclid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cellid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lbldid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.deptid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblncid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ncid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblpmstre" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmstr") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblskillide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skillid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblskillqtye" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skillqty") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblfreqe" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblrdide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rdid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblptide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ptid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblttide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ttid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lblpmtskide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                            </asp:Label>
                                             <asp:Label CssClass="plainlabel" ID="lblfuncid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.funcid") %>'>
                                            </asp:Label>
                                            <asp:Label CssClass="plainlabel" ID="lbltasknum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                            </asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblroute" type="hidden" name="lblroute" runat="server">
    <input id="lbldragid" type="hidden"><input id="lblrowid" type="hidden">
    <input id="lblrtid" type="hidden" name="lblrtid" runat="server">
    <input id="lbleq" type="hidden" name="lbleq" runat="server"><input id="lbllocret"
        type="hidden" name="lbllocret" runat="server">
    <input id="lblseqret" type="hidden" name="lblseqret" runat="server"><input id="lblpm"
        type="hidden" name="lblpm" runat="server">
    <input id="lbltyp" type="hidden" name="lbltyp" runat="server"><input id="lbljp" type="hidden"
        name="lbljp" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lblnewroute" type="hidden" name="lblnewroute" runat="server">
    <input id="lblrow" type="hidden" name="lblrow" runat="server"><input id="lblfrom"
        type="hidden" name="lblfrom" runat="server">
    <input id="lblsubmit" type="hidden" runat="server">
    <input id="lbldidh" type="hidden" runat="server">
    <input id="lblcellidh" type="hidden" runat="server"><input id="lbllocidh" type="hidden"
        runat="server">
    <input id="lblncidh" type="hidden" runat="server"><input id="lblrtyp" type="hidden"
        runat="server">
    <input id="lblro" type="hidden" runat="server">
    <input id="lbloldrte" type="hidden" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblskillid" runat="server" />
    <input type="hidden" id="lblskillqty" runat="server" />
    <input type="hidden" id="lblfreq" runat="server" />
    <input type="hidden" id="lblrdid" runat="server" />
    <input type="hidden" id="lblpmstr" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblstat" runat="server" />
    <input type="hidden" id="lblptid" runat="server" />
    <input type="hidden" id="lbldays" runat="server" />
    <input type="hidden" id="lblshift" runat="server" />
    <input type="hidden" id="yCoord" runat="server" />
    <input type="hidden" id="xCoord" runat="server" />
    <input type="hidden" id="lblskillidb" runat="server" />
    <input type="hidden" id="lblttidb" runat="server" />
    <input type="hidden" id="lblptidb" runat="server" />
    <input type="hidden" id="lblrdidb" runat="server" />
    <input type="hidden" id="lblfreqb" runat="server" />
    <input type="hidden" id="lblpmtskid" runat="server" />
    <input type="hidden" id="lblrsid" runat="server" />
    <input type="hidden" id="lblnewttid" runat="server" />
    <input type="hidden" id="lblolddesc" runat="server" />
    <input type="hidden" id="lblcoi" runat="server" />
    </form>
    <uc1:mmenu1 ID="Mmenu11" runat="server"></uc1:mmenu1>
</body>
</html>
