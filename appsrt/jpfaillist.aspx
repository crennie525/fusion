<%@ Page Language="vb" AutoEventWireup="false" Codebehind="jpfaillist.aspx.vb" Inherits="lucy_r12.jpfaillist" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>jpfaillist</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/jpfaillistaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table width="422">
				<tr>
					<td class="thdrsingrt label" colSpan="2"><asp:Label id="lang1157" runat="server">Add Failure Modes from Master List Mini Dialog</asp:Label></td>
				</tr>
				<tr>
					<td class="label" width="172" height="26"><asp:Label id="lang1158" runat="server">Current Component:</asp:Label></td>
					<td class="labellt" id="tdcomp" width="250" height="26" runat="server"></td>
				</tr>
				<tr>
					<td colSpan="2">
						<hr style="BORDER-RIGHT: #0000ff 1px solid; BORDER-TOP: #0000ff 1px solid; BORDER-LEFT: #0000ff 1px solid; BORDER-BOTTOM: #0000ff 1px solid">
					</td>
				</tr>
			</table>
			<table width="422">
				<tr>
					<td class="label" align="center"><asp:Label id="lang1159" runat="server">Available Failure Modes</asp:Label></td>
					<td></td>
					<td class="label" align="center"><asp:Label id="lang1160" runat="server">Selected Failure Modes</asp:Label></td>
				</tr>
				<tr>
					<td align="center" width="200"><asp:listbox id="lbfailmaster" runat="server" Height="150px" SelectionMode="Multiple" Width="170px"></asp:listbox></td>
					<td vAlign="middle" align="center" width="22"><IMG class="details" id="todis" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
							width="20"> <IMG class="details" id="fromdis" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
							width="20">
						<asp:imagebutton id="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><asp:imagebutton id="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton><IMG class="details" id="btnaddtosite" onmouseover="return overlib('Choose Failure Modes for this Plant Site ')"
							onclick="getss();" onmouseout="return nd()" height="20" src="../images/appbuttons/minibuttons/plusminus.gif" width="20"></td>
					<td align="center" width="200"><asp:listbox id="lbfailmodes" runat="server" Height="150px" SelectionMode="Multiple" Width="170px"></asp:listbox></td>
				</tr>
				<tr>
					<td align="right" colSpan="3"><IMG id="ibtnret" onclick="handleexit();" height="19" alt="" src="../images/appbuttons/bgbuttons/return.gif"
							width="69" runat="server"></td>
				</tr>
				<tr>
					<td class="bluelabel" colSpan="3"><asp:Label id="lang1161" runat="server">List Box Options</asp:Label></td>
				</tr>
				<tr>
					<td class="label" colSpan="3"><asp:radiobuttonlist id="cbopts" runat="server" Width="400px" CssClass="labellt" AutoPostBack="True">
							<asp:ListItem Value="0" Selected="True">Multi-Select (use arrows to move single or multiple selections)</asp:ListItem>
							<asp:ListItem Value="1">Click-Once (move selected item by clicking that item)</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td class="plainlabelblue" colSpan="3"><asp:Label id="lang1162" runat="server">* Please note that the Multi-Select Mode must be used to remove an item from the Selected Failure Modes list if only one item is present.</asp:Label></td>
				</tr>
			</table>
			<input id="lblcid" type="hidden" name="lblcid" runat="server"> <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lblfuid" type="hidden" name="lblfuid" runat="server"> <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
			<input id="lblco" type="hidden" name="lblco" runat="server"><input id="lblapp" type="hidden" name="lblapp" runat="server">
			<input id="lblopt" type="hidden" name="lblopt" runat="server"><input id="lblfailchk" type="hidden" name="lblfailchk" runat="server">
			<input id="lblsid" type="hidden" name="lblsid" runat="server"><input id="lbllog" type="hidden" name="lbllog" runat="server">
			<input id="lblwo" type="hidden" name="lblwo" runat="server"> <input id="lblpmtskid" type="hidden" runat="server">
			<input type="hidden" id="lbljpid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
