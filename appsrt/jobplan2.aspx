﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="jobplan2.aspx.vb" Inherits="lucy_r12.jobplan2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" src="../scripts1/JobPlanaspx.js"></script>
    <script language="javascript" type="text/javascript">
    function pmmanual() {
            var rt = document.getElementById("txtroute").value;
            var rtd = document.getElementById("txtroutedesc").value;
            var typ = document.getElementById("ddtyp").value;
            var jpref = document.getElementById("lbljpref").value;
            var plnr = document.getElementById("lblplnrid").value;
            if (rt == "") {
                alert("Job Plan # Required");
            }
            else if (rt.length > 50) {
                alert("Job Plan # Limited to 50 Characters");
            }
            else if (rtd == "") {
                alert("Job Plan Description Required");
            }
            else if (rtd.length > 100) {
                alert("Job Plan Description Limited to 100 Characters");
            }
            else if (typ == "0") {
                alert("Job Plan Type Required");
            }
            else {
                if (jpref != "") {
                    if (plnr != "") {
                        ro = document.getElementById("lblro").value;
                        if (ro != "1") {
                            document.getElementById("lblsubmit").value = "addjob";
                            document.getElementById("form1").submit();
                        }
                    }
                    else {
                        alert("Planner Required!")
                    }
                }
                else {
                    ro = document.getElementById("lblro").value;
                    if (ro != "1") {
                        document.getElementById("lblsubmit").value = "addjob";
                        document.getElementById("form1").submit();
                    }
                }

            }
        }

        function savetop() {
            var jid = document.getElementById("lbljid").value;
            if (jid != "" && jid != "0") {
                var rt = document.getElementById("txtroute").value;
                var rtd = document.getElementById("txtroutedesc").value;
                var typ = document.getElementById("ddtyp").value;
                if (rt == "") {
                    alert("Job Plan # Required");
                }
                else if (rtd == "") {
                    alert("Job Plan Description Required");
                }
                else if (typ == "0") {
                    alert("Job Plan Type Required");
                }
                else {
                    ro = document.getElementById("lblro").value;
                    if (ro != "1") {
                        document.getElementById("lblsubmit").value = "savjob";
                        document.getElementById("form1").submit();
                    }
                }
            }
            else {
                alert("No Job Plan Created Yet")
            }
        }
        var popwin = "directories=0,height=500,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0,scrollbars=1";
        var popwinm = "directories=0,height=500,width=500,location=0,menubar=0,resizable=1,status=0,toolbar=0,scrollbars=1";
        function GetToolDiv() {
            cid = document.getElementById("lblcid").value;
            ptid = document.getElementById("lblpmtid").value;
            jp = document.getElementById("lbljid").value;
            tnum = document.getElementById("lbltasknum").value;
            wo = document.getElementById("lblwo").value;
            ro = document.getElementById("lblro").value;
            var wojtid = document.getElementById("lblwojtid").value;
            //if(wo=="") wo="0";
            if (ptid != "") {
                window.open("../inv/devTaskToolList.aspx?typ=wjp&ptid=" + ptid + "&cid=" + cid + "&jp=" + jp + "&tnum=" + tnum + "&wo=" + wo + "&ro=" + ro + "&wojtid=" + wojtid, "repWin", popwin);
            }
        }

        function GetPartDiv() {
            cid = document.getElementById("lblcid").value;
            ptid = document.getElementById("lblpmtid").value;
            jp = document.getElementById("lbljid").value;
            tnum = document.getElementById("lbltasknum").value;
            wo = document.getElementById("lblwo").value;
            ro = document.getElementById("lblro").value;
            var wojtid = document.getElementById("lblwojtid").value;
            //if(wo=="") wo="0";
            if (ptid != "") {
                window.open("../inv/devTaskPartList.aspx?typ=wjp&ptid=" + ptid + "&cid=" + cid + "&jp=" + jp + "&tnum=" + tnum + "&wo=" + wo + "&ro=" + ro + "&wojtid=" + wojtid, "repWin", popwin);
            }
        }
        function GetLubeDiv() {
            cid = document.getElementById("lblcid").value;
            ptid = document.getElementById("lblpmtid").value;
            jp = document.getElementById("lbljid").value;
            tnum = document.getElementById("lbltasknum").value;
            wo = document.getElementById("lblwo").value;
            ro = document.getElementById("lblro").value;
            var wojtid = document.getElementById("lblwojtid").value;
            //if(wo=="") wo="0";
            if (ptid != "") {
                window.open("../inv/devTaskLubeList.aspx?typ=wjp&ptid=" + ptid + "&cid=" + cid + "&jp=" + jp + "&tnum=" + tnum + "&wo=" + wo + "&ro=" + ro + "&wojtid=" + wojtid, "repWin", popwin);
            }
        }
        function GetMeasDiv() {
            cid = document.getElementById("lblcid").value;
            ptid = document.getElementById("lblpmtid").value;
            jp = document.getElementById("lbljid").value;
            tnum = document.getElementById("lbltasknum").value;
            wo = document.getElementById("lblwo").value;
            ro = document.getElementById("lblro").value;
            var wojtid = document.getElementById("lblwojtid").value;
            //if(wo=="") wo="0";
            if (ptid != "") {
                var eReturn = window.showModalDialog("../utils/tmdialog.aspx?typ=jp&taskid=" + ptid + "&jpid=" + jp + "&wo=" + wo + "&ro=" + ro + "&wojtid=" + wojtid, "", "dialogHeight:650px; dialogWidth:550px; resizable=yes");
            }
        }
        function getjp(typ) {
            var typchk = document.getElementById("lblwho").value;
            if (typchk == "wo") {
                typ = "wo";
            }
            var eReturn = window.showModalDialog("../appsrt/JobPlanDialog.aspx?jump=yes&typ=" + typ, "", "dialogHeight:510px; dialogWidth:710px; resizable=yes");
            if (eReturn) {
                document.getElementById("lbljid").value = eReturn;
                document.getElementById("lblsubmit").value = "getjob";
                document.getElementById("form1").submit();
            }
        }
        function addtask() {
            var tsk = document.getElementById("lbljid").value;
            var edt = document.getElementById("lbleditmode").value;

            if (tsk == "0" || tsk == "") {
                alert("No Job Plan Created!")
            }
            else if (edt == "1") {
                alert("Please Finish Editing Task!")
            }
            else {
                ro = document.getElementById("lblro").value;
                if (ro != "1") {
                    document.getElementById("lblsubmit").value = "addtask";
                    document.getElementById("form1").submit();
                }
            }
        }
        function refreshit() {
            //document.getElementById("lblsubmit").value = "refreshit";
            //document.getElementById("form1").submit();
            //window.location = "jobplanmain2.aspx?start=no&from=";
            window.parent.handleadd("no", "");
        }
        function getminsrch() {
        var did = document.getElementById("lbldid").value;
                //alert(did)
        if (did != "") {
            retminsrch();
        }
        else {
            var stat = document.getElementById("lblstat").value;
            var sid = document.getElementById("lblsid").value;
            var wo = document.getElementById("lbljid").value;
            var who = document.getElementById("lblwho").value;
            var typ;
            //alert(who + "," + wo)
            if (who != "wo") {
                if (wo == "") {
                    typ = "jp";
                }
                else {
                    typ = "jp";
                }
                var wt = document.getElementById("lblwt").value;
                if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
                    var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&jpid=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
                    if (eReturn) {
                        var ret = eReturn.split("~");
                        //document.getElementById("tdfail").innerHTML = "";
                        document.getElementById("lbldid").value = ret[0];
                        document.getElementById("lbldept").value = ret[1];
                        document.getElementById("tddept").innerHTML = ret[1];
                        document.getElementById("lblclid").value = ret[2];
                        document.getElementById("lblcell").value = ret[3];
                        document.getElementById("tdcell").innerHTML = ret[3];
                        document.getElementById("lbleqid").value = ret[4];
                        document.getElementById("lbleq").value = ret[5];
                        document.getElementById("tdeq").innerHTML = ret[5];
                        document.getElementById("lblfuid").value = ret[6];
                        document.getElementById("lblfu").value = ret[7];
                        document.getElementById("tdfu").innerHTML = ret[7];
                        document.getElementById("lblcomid").value = ret[8];
                        document.getElementById("lblcomp").value = ret[9];
                        document.getElementById("tdco").innerHTML = ret[9];
                        document.getElementById("lblncid").value = ret[10];
                        document.getElementById("lblnc").value = ret[11];
                        document.getElementById("tdmisc").innerHTML = ret[11];
                        document.getElementById("lbllid").value = ret[12];
                        document.getElementById("lblloc").value = ret[13];
                        //document.getElementById("tdloc").innerHTML = ret[13];
                        document.getElementById("trdepts").className = "view";
                        document.getElementById("lbltyp").value = "depts";
                    }
                }
            }
        }
    }
    function retminsrch() {
        //alert()
        var wo = document.getElementById("lbljid").value;
        var sid = document.getElementById("lblsid").value;
        //var wo = "";
        var typ = "jpret";
        var did = document.getElementById("lbldid").value;
        var dept = document.getElementById("lbldept").value;
        var clid = document.getElementById("lblclid").value;
        var cell = document.getElementById("lblcell").value;
        var eqid = document.getElementById("lbleqid").value;
        var eq = document.getElementById("lbleq").value;
        var fuid = document.getElementById("lblfuid").value;
        var fu = document.getElementById("lblfu").value;
        var coid = document.getElementById("lblcomid").value;
        var comp = document.getElementById("lblcomp").value;
        var lid = document.getElementById("lbllid").value;
        var loc = document.getElementById("lblloc").value;
        //if (cell == "" && who != "checkfu" && who != "checkeq") {
        who = "deptret";
        //}
        alert("../apps/appgetdialog.aspx?typ=" + typ + "&jpid=" + wo + "&site=" + sid + "&wo=" + wo + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who)
        var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&jpid=" + wo + "&site=" + sid + "&wo=" + wo + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
        if (eReturn) {
            clearall();
            var ret = eReturn.split("~");
            document.getElementById("lbldid").value = ret[0];
            document.getElementById("lbldept").value = ret[1];
            document.getElementById("tddept").innerHTML = ret[1];
            document.getElementById("lblclid").value = ret[2];
            document.getElementById("lblcell").value = ret[3];
            document.getElementById("tdcell").innerHTML = ret[3];
            document.getElementById("lbleqid").value = ret[4];
            document.getElementById("lbleq").value = ret[5];
            document.getElementById("tdeq").innerHTML = ret[5];
            document.getElementById("lblfuid").value = ret[6];
            document.getElementById("lblfu").value = ret[7];
            document.getElementById("tdfu").innerHTML = ret[7];
            document.getElementById("lblcomid").value = ret[8];
            document.getElementById("lblcomp").value = ret[9];
            document.getElementById("tdco").innerHTML = ret[9];
            document.getElementById("lbllid").value = ret[12];
            document.getElementById("lblloc").value = ret[13];
            document.getElementById("trdepts").className = "view";
            document.getElementById("lblrettyp").value = "depts";
            var did = ret[0];
            var eqid = ret[4];
            var clid = ret[2];
            var fuid = ret[6];
            var coid = ret[8];
            var lid = ret[12];
            var typ;
            //if(lid=="") {
            //typ = "reg";
            //}
            //else {
            //typ = "dloc";
            //}
            typ = "reg"
            var task = "";
            //window.location = "pmget.aspx?jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;
        }
    }
        function getlocs1() {
            var who = document.getElementById("lblwho").value;
            if (who != "wo") {
                var wt = document.getElementById("lblwt").value;
                if (wt != "PM" && wt != "TPM") {
                    var stat = document.getElementById("lblstat").value;
                    var sid = document.getElementById("lblsid").value;
                    var wo = document.getElementById("lbljid").value;
                    var lid = document.getElementById("lbllid").value;
                    var typ = "jp"
                    //alert(lid)
                    if (lid != "") {
                        typ = "jpret"
                    }
                    var eqid = document.getElementById("lbleqid").value;
                    var fuid = document.getElementById("lblfuid").value;
                    var coid = document.getElementById("lblcomid").value;
                    //alert("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&jpid=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid)
                    if (stat != "COMP" && stat != "CAN") {
                        var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&jpid=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
                        if (eReturn) {
                            var ret = eReturn.split("~");
                            document.getElementById("lbllid").value = ret[0];
                            document.getElementById("lblloc").value = ret[1];
                            document.getElementById("tdloc3").innerHTML = ret[2];

                            document.getElementById("lbleq").value = ret[4];
                            document.getElementById("tdeq3").innerHTML = ret[4];
                            document.getElementById("lbleqid").value = ret[3];

                            document.getElementById("lblfuid").value = ret[5];
                            document.getElementById("lblfu").value = ret[6];
                            document.getElementById("tdfu3").innerHTML = ret[6];

                            document.getElementById("lblcomid").value = ret[7];
                            document.getElementById("lblcomp").value = ret[8];
                            document.getElementById("tdco3").innerHTML = ret[8];

                            document.getElementById("lblncid").value = ret[10];
                            document.getElementById("lblnc").value = ret[11];
                            document.getElementById("tdmisc3").innerHTML = ret[11];

                            document.getElementById("trlocs3").className = "view";
                            document.getElementById("lbltyp").value = "locs";
                           

                        }
                    }
                }
            }
        }
        function clearall() {
            document.getElementById("lbldid").value = "";
            document.getElementById("lbldept").value = "";
            document.getElementById("tddept").innerHTML = "";
            document.getElementById("lblclid").value = "";
            document.getElementById("lblcell").value = "";
            document.getElementById("tdcell").innerHTML = "";
            document.getElementById("lbleqid").value = "";
            document.getElementById("lbleq").value = "";
            document.getElementById("tdeq").innerHTML = "";
            document.getElementById("lblfuid").value = "";
            document.getElementById("lblfu").value = "";
            document.getElementById("tdfu").innerHTML = "";
            document.getElementById("lblcomid").value = "";
            document.getElementById("lblcomp").value = "";
            document.getElementById("tdco").innerHTML = "";
            document.getElementById("lblncid").value = "";
            document.getElementById("lblnc").value = "";
            document.getElementById("tdmisc").innerHTML = "";
            document.getElementById("lbllid").value = "";
            document.getElementById("lblloc").value = "";
            document.getElementById("trdepts").className = "details";
            document.getElementById("lbltyp").value = "";

            document.getElementById("lbllid").value = "";
            document.getElementById("lblloc").value = "";
            document.getElementById("tdloc3").innerHTML = "";

            document.getElementById("lbleq").value = "";
            document.getElementById("tdeq3").innerHTML = "";
            document.getElementById("lbleqid").value = "";

            document.getElementById("lblfuid").value = "";
            document.getElementById("lblfu").value = "";
            document.getElementById("tdfu3").innerHTML = "";

            document.getElementById("lblcomid").value = "";
            document.getElementById("lblcomp").value = "";
            document.getElementById("tdco3").innerHTML = "";

            document.getElementById("lblncid").value = "";
            document.getElementById("lblnc").value = "";
            document.getElementById("tdmisc3").innerHTML = "";

            document.getElementById("trlocs3").className = "details";
        }
        function getsuper(typ) {
            if (typ == "sup") {
                if (document.getElementById("txtsup").disabled == false) {
                    opensuper(typ);
                }
            }
            if (typ == "lead") {
                if (document.getElementById("txtlead").disabled == false) {
                    opensuper(typ);
                }
            }
        }
        function opensuper(typ) {
            var skill = document.getElementById("lblskillid").value;
            var ro = document.getElementById("lblro").value;
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    var retarr = eReturn.split(",")
                    if (typ == "sup") {
                        document.getElementById("lblsup").value = retarr[0];
                        document.getElementById("txtsup").value = retarr[1];
                    }
                    else {
                        document.getElementById("lbllead").value = retarr[0];
                        document.getElementById("txtlead").value = retarr[1];
                    }
                }
            }
        }
        function setref() {
            
        }
        function getfm() {
            var dbid = document.getElementById("lbldboxid").value;
            //alert(dbid)
            var desc = document.getElementById(dbid).value;
            document.getElementById("lbltaskdesc").value = desc;
            //alert(document.getElementById("lbltaskdesc").value)

            var qbid = document.getElementById("lblqboxid").value;
            //alert(qbid)
            var qdesc = document.getElementById(qbid).value;
            document.getElementById("lblgridqty").value = qdesc;

            var mbid = document.getElementById("lblmboxid").value;
            //alert(mbid)
            var mdesc = document.getElementById(mbid).value;
            document.getElementById("lblgridmins").value = mdesc;

            /*
            var sbid = document.getElementById("lblsboxid").value;
            //alert(sbid)
            var sdesc = document.getElementById(sbid).value;
            document.getElementById("lblgridskillid").value = sdesc;
            */

            pmtskid = document.getElementById("lblcurrtask").value;
            coid = document.getElementById("lblcoid").value;
            jpid = document.getElementById("lbljid").value;
            wo = document.getElementById("lblwo").value;
            //alert("jpfaildialog.aspx?jpid=" + jpid + "&coid=" + coid + "&pmtskid=" + pmtskid + "&wonum=" + wo)
            var eReturn = window.showModalDialog("jpfaildialog.aspx?jpid=" + jpid + "&coid=" + coid + "&pmtskid=" + pmtskid + "&wonum=" + wo, "", "dialogHeight:400px; dialogWidth:470px; resizable=yes");
            if (eReturn) {
                ro = document.getElementById("lblro").value;
                document.getElementById("lblfailret").value = "1";
                if (ro != "1") {
                    //alert(document.getElementById("lblfailret").value)
                    document.getElementById("lblsubmit").value = "upfail";
                    document.getElementById("form1").submit();
                }
            }
        }
        function checkinv() {
            document.getElementById("lblfailret").Value = "0";
            try {
                //alert(document.getElementById("lbltaskdesc").value)
                var dbid = document.getElementById("lbldboxid").value;
                if (dbid != "") {
                    var desc = document.getElementById("lbltaskdesc").value;
                    document.getElementById(dbid).value = desc;
                    //document.getElementById("lbldboxid").value = "";
                    //document.getElementById("lbltaskdesc").value = "";
                }
                var qbid = document.getElementById("lblqboxid").value;
                if (qbid != "") {
                    var qdesc = document.getElementById("lblgridqty").value;
                    document.getElementById(qbid).value = qdesc;
                    //document.getElementById("lblqboxid").value = "";
                    //document.getElementById("lblgridqty").value = "";
                }
                var mbid = document.getElementById("lblmboxid").value;
                if (mbid != "") {
                    var mdesc = document.getElementById("lblgridmins").value;
                    document.getElementById(mbid).value = mdesc;
                    //document.getElementById("lblmboxid").value = "";
                    //document.getElementById("lblgridmins").value = "";
                }
                /*
                var sbid = document.getElementById("lblsboxid").value;
                if(sbid!="") {
                var sdesc = document.getElementById("lblgridskillid").value;
                document.getElementById(sbid).value = sdesc;
                //document.getElementById("lblsboxid").value = "";
                //document.getElementById("lblgridskillid").value = "";
                }
                */
                //alert(document.getElementById("lblret").value)
            }
            catch (err) {

            }

            var log = document.getElementById("lbllog").value;
            if (log == "no" || log == "noeqid") setref();
            else window.setTimeout("setref();", 1205000);

            var to = document.getElementById("lbltool").value;
            var pa = document.getElementById("lblpart").value;
            var lu = document.getElementById("lbllube").value;
            var no = document.getElementById("lblnote").value;
            var me = document.getElementById("lblmeas").value;

            if (to == "no") {
                //document.getElementById("if1").src="";
                //document.getElementById("if1").style.visibility="hidden";
                //document.getElementById("subdiv").style.display="none";
                //document.getElementById("subdiv").style.visibility="hidden";
            }
            else {
                document.getElementById("lbltool").value = "no";
                GetToolDiv();
            }
            if (pa == "no") {
                //document.getElementById("if2").src="";
                //document.getElementById("if2").style.visibility="hidden";
                //document.getElementById("subdiv2").style.display="none";
                //document.getElementById("subdiv2").style.visibility="hidden";
            }
            else {
                document.getElementById("lblpart").value = "no";
                //alert(pa)
                GetPartDiv();
            }
            if (lu == "no") {
                //document.getElementById("if3").src="";
                //document.getElementById("if3").style.visibility="hidden";
                //document.getElementById("subdiv3").style.visibility="hidden";
                //document.getElementById("subdiv3").style.display="none";
            }
            else {
                document.getElementById("lbllube").value = "no";
                GetLubeDiv();
            }
            if (me == "no") {
                //document.getElementById("if3").src="";
                //document.getElementById("if3").style.visibility="hidden";
                //document.getElementById("subdiv3").style.visibility="hidden";
                //document.getElementById("subdiv3").style.display="none";
            }
            else {
                document.getElementById("lblmeas").value = "no";
                GetMeasDiv();
            }
            if (no == "no") {
                //document.getElementById("if4").src="";
                //document.getElementById("if4").style.visibility="hidden";
                //document.getElementById("subdiv4").style.visibility="hidden";
                //document.getElementById("subdiv4").style.display="none";
            }
            else {
                document.getElementById("lblnote").value = "no";
                GetNoteDiv();
            }
        }

        function handlereturn() {
            var who = document.getElementById("lblwho").value;
            //alert(who)
            if (who == "wo") {
                var retstr = document.getElementById("lblret").value;
                //alert(retstr)
                window.parent.handlereturn(who, retstr)
            }
            //jump back to st or list here
            else if (who == "jp") {
                window.parent.handlereturn(who)
            }
            else if (who == "rq" || who == "rqw") {
                window.parent.handlereturn(who)
            }
            else {
                window.parent.handlereturn(who)
            }
        }

        function srchloc() {
            //alert("Coming Soon!")
            var lock = document.getElementById("lbllock").value;
            if (lock != "1") {
                var eReturn = window.showModalDialog("../locs/LocDialog.aspx?typ=eq", "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    var ret = eReturn.split(",");
                    document.getElementById("lbllid").value = ret[5];
                    document.getElementById("lbltyp").value = ret[1];
                    document.getElementById("lblloc").innerHTML = ret[2];
                    var lid = ret[5];
                    document.getElementById("lblpchk").value = "loc"
                    document.getElementById("form1").submit();

                }
            }
        }
       


        var x = 0;

        function gettyp() {
            //alert(document.getElementById("ddtyp").value)
            var jpi = document.getElementById("ddtyp").value;
            var retstr = document.getElementById("lblsched").value;
            var ret = retstr.split(",");
            var i;

            for (i in ret) {
                if (ret[i] == jpi) {
                    x = 1;
                }
            }
            if (x == 1) {
                document.getElementById("txts").disabled = false;
                document.getElementById("txtn").disabled = false;
                document.getElementById("txtfreq").disabled = false;
            }
        }
        function doskill() {
            var jid = document.getElementById("lbljid").value;
            if (jid != "" || jid != "0") {
                var cb = document.getElementById("cball");
                if (cb.checked == true) {
                    ro = document.getElementById("lblro").value;
                    if (ro != "1") {
                        document.getElementById("lblsubmit").value = "dojob";
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        function getcal(fld) {
            gettyp();
            if (x == 1) {
                var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
                if (eReturn) {
                    var fldret = "txt" + fld;
                    document.getElementById(fldret).value = eReturn;
                }
            }
        }
        function savedets() {
            var jid = document.getElementById("lbljid").value;
            if (jid != "" && jid != "0") {
                //alert(jid)
                gettyp();
                if (x == 1) {
                    var sk = document.getElementById("ddskillmain").value;
                    var fr = document.getElementById("txtfreq").value;
                    if (sk == "0") {
                        alert("Skill Required for Scheduling")
                    }
                    else if (fr == "0") {
                        alert("Frequency Required for Scheduling")
                    }
                    else {
                        ro = document.getElementById("lblro").value;
                        if (ro != "1") {
                            document.getElementById("lblsubmit").value = "savedets";
                            document.getElementById("form1").submit();
                        }
                    }
                }
                else {
                    ro = document.getElementById("lblro").value;
                    if (ro != "1") {
                        document.getElementById("lblsubmit").value = "savedets";
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        function getcharge(typ) {
            var eReturn = window.showModalDialog("../admin/ChargeDialog.aspx", "", "dialogHeight:350px; dialogWidth:350px; resizable=yes");
            if (eReturn) {
                document.getElementById("txtcharge").value = eReturn;
            }
        }


        function getplanner(typ) {
            var skill = "";
            var ro = document.getElementById("lblro").value;
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    var retarr = eReturn.split(",")
                    document.getElementById("lblplnrid").value = retarr[0];
                    document.getElementById("lblplanner").value = retarr[1];
                    document.getElementById("tdplnr").innerHTML = retarr[1];
                    var jid = document.getElementById("lbljid").value;
                    if (jid != "" && jid != "0") {
                        ro = document.getElementById("lblro").value;
                        if (ro != "1") {
                            document.getElementById("lblsubmit").value = "aplan";
                            document.getElementById("form1").submit();
                        }
                    }
                }
            }
        }
        function getcomp() {
            var eReturn = window.showModalDialog("../equip/complookupdialog.aspx", "", "dialogHeight:510px; dialogWidth:710px; resizable=yes");
            if (eReturn) {
                var retarr = eReturn.split(",")
                document.getElementById("lblcomponly").value = retarr[0];
                document.getElementById("txtcomponly").value = retarr[1];
                if (retarr[0] != "") {
                    ro = document.getElementById("lblro").value;
                    if (ro != "1") {
                        document.getElementById("lblsubmit").value = "addcomp";
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        function getsgrid() {
            //handleapp();
            tid = document.getElementById("lbloldtasknum").value; //document.getElementById("lbltaskid").value;
            jpid = document.getElementById("lbljid").value;
            wonum = document.getElementById("lblwo").value;
            ro = document.getElementById("lblro").value;
            //cid = document.getElementById("lblcid").value;
            //sid = document.getElementById("lblsid").value;
            //sav = document.getElementById("lblsave").value;
            if (tid == "") {
                alert("No Task Records Selected!")
            }
            //else if (sav=="no") {
            //alert("Task Record Is Not Saved!")
            //}
            else {
                var eReturn = window.showModalDialog("JSubDialog.aspx?tid=" + tid + "&jpid=" + jpid + "&wo=" + wonum + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:680px; dialogWidth:850px; resizable=yes");
                if (eReturn) {
                    document.getElementById("form1").submit();
                }
            }
        }
        function getskill(freqid, pmtskid, sid, fld, wo) {
            var eReturn = window.showModalDialog("jpfreqlistdialog.aspx?pmtskid=" + pmtskid + "&fld=" + fld + "&sid=" + sid + "&wo=" + wo, "", "dialogHeight:460px; dialogWidth:460px; resizable=yes");
            if (eReturn) {
                //alert(freqid)
                var ret = eReturn.split(",");
                document.getElementById(freqid).innerHTML = ret[1];
                document.getElementById("lblskillidret").value = ret[0];
            }

        }
        //end from external

        function getsk() {
            var who = document.getElementById("lblwho").value;
            if (who != "wo") {
                wo = document.getElementById("lbljid").value;
                sid = document.getElementById("lblsid").value;
                var stat = document.getElementById("lblstat").value;
                var wt = document.getElementById("lblwt").value;
                if ((stat != "COMP" && stat != "CAN") && wt != "PM" && wt != "TPM") {
                    var eReturn = window.showModalDialog("../labor/labskillsdialog.aspx?who=jp&sid=" + sid + "&jpid=" + wo, "", "dialogHeight:500px; dialogWidth:360px; resizable=yes");
                    if (eReturn) {
                        if (eReturn != "") {
                            var ret = eReturn.split("~")
                            document.getElementById("tdskill").innerHTML = ret[1];
                            document.getElementById("lblskill").value = ret[1];
                            document.getElementById("lblskillid").value = ret[0];

                        }
                    }
                }
            }
        }
    </script>
</head>
<body onload="checkinv();">
    <form id="form1" runat="server">
    <div style="z-index: 1; position: absolute; top: 2px; left: 2px">
        <table width="1040">
            <tr height="24">
                <td class="thdrsing label" colspan="11">
                    <asp:Label ID="lang1124" runat="server">Add/Edit Job Plans</asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" width="20">
                    <img src="../images/appbuttons/minibuttons/planb.gif">
                </td>
                <td class="label" width="10">
                    #
                </td>
                <td id="tdjp" width="170">
                    <asp:TextBox ID="txtroute" runat="server" CssClass="plainlabel" Width="170px"></asp:TextBox>
                </td>
                <td width="20">
                    <img src="../images/appbuttons/minibuttons/planb.gif">
                </td>
                <td class="label" width="80">
                    <asp:Label ID="lang1125" runat="server">Description</asp:Label>
                </td>
                <td width="320">
                    <asp:TextBox ID="txtroutedesc" runat="server" CssClass="plainlabel" Width="310px"></asp:TextBox>
                </td>
                <td width="20">
                    <img src="../images/appbuttons/minibuttons/planb.gif">
                </td>
                <td class="label" width="40">
                    <asp:Label ID="lang1126" runat="server">Type</asp:Label>
                </td>
                <td width="100">
                    <asp:DropDownList ID="ddtyp" runat="server" CssClass="plainlabel">
                    </asp:DropDownList>
                </td>
                <td width="228">
                    <img id="imgadd" class="hand" onmouseover="return overlib('Create a Job Plan with the Name and Decription You Have Entered', ABOVE, LEFT)"
                        onclick="pmmanual();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/addnew.gif"
                        runat="server"><img class="hand" onmouseover="return overlib('View Job Plan Record List')"
                            onclick="getjp('jp');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/globem.gif">
                    <img class="hand" onmouseover="return overlib('Clear Screen and Create a New Job Plan')"
                        onclick="refreshit();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/refreshit.gif">
                    <img class="hand" id="imgsav" onmouseover="return overlib('Save Changes to Job Plan # and Description')"
                        onclick="savetop();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/savedisk1.gif"
                        runat="server">
                </td>
            </tr>
            <tr>
                <td class="thdrsing label" colspan="11">
                    <asp:Label ID="lang1127" runat="server">Job Plan Details</asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="11">
                    <table>
                        <tr>
                            <td width="570" valign="top">
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td id="tddepts" class="bluelabel" runat="server">
                                                        Use Departments
                                                    </td>
                                                    <td>
                                                        <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif" />
                                                    </td>
                                                    <td id="tdlocs1" class="bluelabel" runat="server">
                                                        Use Locations
                                                    </td>
                                                    <td>
                                                        <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="trdepts" class="details" runat="server">
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="label" width="100">
                                                        Department
                                                    </td>
                                                    <td id="tddept" class="plainlabel" width="170" runat="server">
                                                    </td>
                                                    <td width="30">
                                                    </td>
                                                    <td class="label" width="100">
                                                        Equipment
                                                    </td>
                                                    <td id="tdeq" class="plainlabel" width="170" runat="server">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="label">
                                                        Station\Cell
                                                    </td>
                                                    <td id="tdcell" class="plainlabel" runat="server">
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td class="label">
                                                        Function
                                                    </td>
                                                    <td id="tdfu" class="plainlabel" runat="server">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="label">
                                                        <asp:Label ID="Label3" runat="server">Charge#</asp:Label>
                                                    </td>
                                                    <td id="tdcharge1" class="plainlabel" runat="server">
                                                    </td>
                                                    <td>
                                                        <img onclick="getcharge('wo','1');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                                                    </td>
                                                    <td class="label">
                                                        Component
                                                    </td>
                                                    <td id="tdco" class="plainlabel" runat="server">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                    </td>
                                                    <td class="label">
                                                        Misc Asset
                                                    </td>
                                                    <td id="tdmisc" class="plainlabel" runat="server">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="trlocs3" class="details" runat="server">
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="label" width="100">
                                                        Location
                                                    </td>
                                                    <td id="tdloc3" class="plainlabel" runat="server" width="170">
                                                    </td>
                                                    <td width="30">
                                                    </td>
                                                    <td class="label" width="100">
                                                        Function
                                                    </td>
                                                    <td id="tdfu3" class="plainlabel" runat="server" width="170">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="label">
                                                        Equipment
                                                    </td>
                                                    <td id="tdeq3" class="plainlabel" runat="server">
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td class="label">
                                                        Component
                                                    </td>
                                                    <td id="tdco3" class="plainlabel" runat="server">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="label">
                                                        <asp:Label ID="Label5" runat="server">Charge#</asp:Label>
                                                    </td>
                                                    <td id="tdcharge3" class="plainlabel" runat="server">
                                                    </td>
                                                    <td>
                                                        <img onclick="getcharge('wo','3');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                                                    </td>
                                                    <td class="label">
                                                        Misc Asset
                                                    </td>
                                                    <td id="tdmisc3" class="plainlabel" runat="server">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="470" valign="top">
                                <table>
                                    <tr>
                                        <td class="label">
                                            <asp:Label ID="lang1130" runat="server">Craft/Skill</asp:Label>
                                        </td>
                                        <td id="tdskill" class="plainlabel" runat="server">
                                        </td>
                                        <td>
                                            <img id="Img3" onclick="getsk();" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                                runat="server">
                                        </td>
                                        <td>
                                            <input id="cball" onmouseover="return overlib('Apply this Skill to All Tasks', LEFT)"
                                                onclick="doskill();" onmouseout="return nd()" type="checkbox" runat="server">
                                        </td>
                                        <td class="label">
                                            <asp:Label ID="lang1131" runat="server">Reference#</asp:Label>
                                        </td>
                                        <td class="plainlabelblue" id="tdref" runat="server">
                                            <asp:Label ID="lang1132" runat="server">None</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label" width="80">
                                            <asp:Label ID="lang1135" runat="server">Lead Craft</asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtlead" runat="server" CssClass="plainlabel" Width="155px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <img onclick="getsuper('lead');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                                        </td>
                                        <td class="label">
                                            <asp:Label ID="lang1136" runat="server">Planner</asp:Label>
                                        </td>
                                        <td class="plainlabelblue" id="tdplnr" runat="server">
                                            <asp:Label ID="lang1137" runat="server">None</asp:Label>
                                        </td>
                                        <td>
                                            <img id="imgplanner" onclick="getplanner('plan');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                                border="0" runat="server">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">
                                            <asp:Label ID="lang1140" runat="server">Supervisor</asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtsup" runat="server" CssClass="plainlabel" Width="155px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <img onclick="getsuper('sup');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                                        </td>
                                        <td class="label">
                                            <asp:Label ID="lang1141" runat="server">Status</asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddjpstat" runat="server" AutoPostBack="True">
                                                <asp:ListItem Value="New">New</asp:ListItem>
                                                <asp:ListItem Value="In Progress">In Progress</asp:ListItem>
                                                <asp:ListItem Value="Complete">Complete</asp:ListItem>
                                                <asp:ListItem Value="Cancelled">Cancelled</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bluelabel">
                                            <asp:Label ID="lang1144" runat="server">Component*</asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtcomponly" runat="server" CssClass="plainlabel" Width="155px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <img onmouseover="return overlib('Use to select a Common Component without Location and Asset Details', ABOVE, LEFT)"
                                                onclick="getcomp();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                                        </td>
                                        <td class="bluelabel" id="tdwo" colspan="2" runat="server">
                                        </td>
                                        <td align="right">
                                            <img class="hand" id="imgsav2" onmouseover="return overlib('Save Changes to Job Plan Details')"
                                                onclick="savedets();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                                runat="server">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="thdrsing label" style="height: 17px" colspan="11">
                    <asp:Label ID="lang1145" runat="server">Job Plan Tasks</asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="11">
                    <img class="hand" id="imgaddtask" onclick="addtask();" src="../images/appbuttons/bgbuttons/addtask.gif"
                        runat="server">&nbsp;<img class="hand" id="btnreturn" onclick="handlereturn();" height="19"
                            src="../images/appbuttons/bgbuttons/return.gif" width="69" runat="server">
                </td>
            </tr>
            <tr>
                <td colspan="11">
                    <asp:DataGrid ID="dgtasks" runat="server" BorderWidth="0" CellSpacing="1" CellPadding="3"
                        AutoGenerateColumns="False" AllowSorting="True" BackColor="transparent">
                        <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                        <ItemStyle CssClass="ptransrow"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Width="80px" Height="24px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                                    <img id="ibast" runat="server" class="hand" src="../images/appbuttons/minibuttons/subtask.gif"
                                        onclick="getsgrid();" width="20" height="20">
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Task#">
                                <HeaderStyle CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblta" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="lblt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'
                                        Width="20px">
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Task Description">
                                <HeaderStyle Width="280px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>'
                                        Width="270px">
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtdesc" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>'
                                        Height="70px" TextMode="MultiLine" Width="260px" MaxLength="1000">
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Failure Mode">
                                <HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.failuremode") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <table>
                                        <tr>
                                            <td class="bluelabel" align="center" width="160">
                                                <u>
                                                    <asp:Label ID="lang1146" runat="server">Component Failure Modes</asp:Label></u>
                                            </td>
                                            <td width="22">
                                            </td>
                                            <td class="bluelabel" align="center" width="160">
                                                <u>
                                                    <asp:Label ID="lang1147" runat="server">Selected Failure Modes</asp:Label></u>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:ListBox ID="lbCompFM" runat="server" CssClass="plainlabel" Width="150px" Height="70px"
                                                    SelectionMode="Multiple" DataSource='<%# PopulateCompFM(Container.DataItem("comid"),Container.DataItem("pmtskid")) %>'
                                                    DataTextField="failuremode" DataValueField="failid"></asp:ListBox>
                                            </td>
                                            <td>
                                                <img id="imgfm" runat="server" onclick="getfm();" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                                    border="0"><br>
                                                <asp:ImageButton ID="ibToTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"
                                                    CommandName="ToTask2"></asp:ImageButton><br>
                                                <asp:ImageButton ID="ibFromTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"
                                                    CommandName="RemTask"></asp:ImageButton>
                                            </td>
                                            <td align="center">
                                                <asp:ListBox ID="lbfailmodes" runat="server" CssClass="plainlabel" Width="150px"
                                                    Height="70px" SelectionMode="Multiple" DataSource='<%# PopulateTaskFM(Container.DataItem("comid"),Container.DataItem("pmtskid")) %>'
                                                    DataTextField="failuremode" DataValueField="failid"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Skill Required">
                                <HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblskill" Width="230px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                    </asp:Label>
                                    <img id="imgskill" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Skill Qty">
                                <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'
                                        ID="Label10" NAME="Label8">
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtqty" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Skill Min Ea">
                                <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'
                                        ID="Label12">
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txttr" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Down Time" Visible="False">
                                <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rdt") %>'
                                        ID="Label2">
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtdt" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.rdt") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Parts/Tools/Lubes/Meas">
                                <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton23" runat="server" ImageUrl="../images/appbuttons/minibuttons/parttrans.gif"
                                        ToolTip="Add Parts" CommandName="Part"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton23a" runat="server" ImageUrl="../images/appbuttons/minibuttons/tooltrans.gif"
                                        ToolTip="Add Tools" CommandName="Tool"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton23b" runat="server" ImageUrl="../images/appbuttons/minibuttons/lubetrans.gif"
                                        ToolTip="Add Lubricants" CommandName="Lube"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton23c" runat="server" ImageUrl="../images/appbuttons/minibuttons/measure.gif"
                                        ToolTip="Add Measurements" CommandName="Meas"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="pmtskid">
                                <ItemTemplate>
                                    <asp:Label ID="lbltida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblttid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Delete">
                                <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibDel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="pmtskid">
                                <ItemTemplate>
                                    <asp:Label ID="lblwojtidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wojtid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblwojtide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wojtid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lbljid" type="hidden" name="lbltid" runat="server">
    <input id="lblfilt" type="hidden" name="lblfilt" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lblwho" type="hidden" name="lblwho" runat="server">
    <input id="lblpart" type="hidden" name="lblpart" runat="server">
    <input id="lbltool" type="hidden" name="lbltool" runat="server">
    <input id="lbllube" type="hidden" name="lbllube" runat="server">
    <input id="lblnote" type="hidden" name="lblnote" runat="server">
    <input id="lbltasknum" type="hidden" name="lbltasknum" runat="server">
    <input id="lblpmtid" type="hidden" name="lblpmtid" runat="server">
    <input id="lbllog" type="hidden" name="lbllog" runat="server">
    <input id="lbllock" type="hidden" name="lbllock" runat="server">
    <input id="lbllockedby" type="hidden" name="lbllockedby" runat="server">
    <input id="lblusername" type="hidden" name="lblusername" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblsubmit" type="hidden" runat="server">
    <input id="lbloldtasknum" type="hidden" runat="server">
    <input id="lblskillid" type="hidden" runat="server">
    <input id="lblsup" type="hidden" runat="server">
    <input id="lbllead" type="hidden" runat="server">
    <input id="lbltyp" type="hidden" runat="server">
    <input id="lbllid" type="hidden" runat="server">
    <input id="lblpchk" type="hidden" runat="server">
    <input id="lblret" type="hidden" runat="server"><input id="lblchk" type="hidden"
        runat="server">
    <input id="lbldept" type="hidden" runat="server"><input id="lblclid" type="hidden"
        runat="server">
    <input id="lblfuid" type="hidden" runat="server">
    <input id="lblcoid" type="hidden" runat="server">
    <input id="lblfailchk" type="hidden" runat="server"><input id="lblwo" type="hidden"
        runat="server">
    <input id="lblsched" type="hidden" runat="server"><input id="lblncid" type="hidden"
        runat="server">
    <input id="lbleditmode" type="hidden" runat="server">
    <input id="lblitemindex" type="hidden" runat="server">
    <input id="lblcurrtask" type="hidden" runat="server"><input id="lblmeas" type="hidden"
        runat="server">
    <input id="lbljpref" type="hidden" runat="server"><input id="lblplanner" type="hidden"
        runat="server">
    <input id="lblplnrid" type="hidden" runat="server">
    <input id="lblcomponly" type="hidden" runat="server">
    <input id="lblsskills" type="hidden" runat="server"><input id="lblro" type="hidden"
        runat="server">
    <input id="lbltaskdesc" type="hidden" runat="server">
    <input id="lblgridskill" type="hidden" runat="server">
    <input id="lblgridskillid" type="hidden" runat="server">
    <input id="lblgridqty" type="hidden" runat="server">
    <input id="lblgridmins" type="hidden" runat="server">
    <input id="lbldboxid" type="hidden" runat="server">
    <input id="lblsboxid" type="hidden" name="lblsboxid" runat="server">
    <input id="lblqboxid" type="hidden" name="lblqboxid" runat="server">
    <input id="lblmboxid" type="hidden" name="lblmdboxid" runat="server">
    <input id="lblskillall" type="hidden" runat="server">
    <input id="lblskillallid" type="hidden" runat="server">
    <input id="lblfailret" type="hidden" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lbluid" runat="server" name="lbluid">
    <input type="hidden" id="lblislabor" runat="server" name="lblislabor">
    <input type="hidden" id="lblissuper" runat="server" name="lblissuper">
    <input type="hidden" id="lblisplanner" runat="server" name="lblisplanner">
    <input type="hidden" id="lblLogged_In" runat="server" name="lblLogged_In">
    <input type="hidden" id="Hidden1" runat="server" name="lblro">
    <input type="hidden" id="lblms" runat="server" name="lblms">
    <input type="hidden" id="lblappstr" runat="server" name="lblappstr">
    <input id="lblwt" type="hidden" name="lblwt" runat="server">
    <input id="lblstat" type="hidden" name="lblstat" runat="server">
    <input id="lblloc" type="hidden" name="lblloc" runat="server">
    <input id="lblwonum" type="hidden" name="lblwonum" runat="server">
    <input id="lbldid" type="hidden" name="lbldid" runat="server">
    <input id="lblcell" type="hidden" name="lblcell" runat="server">
    <input id="lblpar2" type="hidden" name="lblpar2" runat="server">
    <input id="lbleq" type="hidden" name="lbleq" runat="server">
    <input id="lblfu" type="hidden" name="lblfu" runat="server">
    <input id="lblcomp" type="hidden" name="lblcomp" runat="server">
    <input id="lblcomid" type="hidden" name="lblcomid" runat="server">
    <input id="lbltabs" type="hidden" name="lbltabs" runat="server">
    <input id="lblwaid" type="hidden" name="Hidden1" runat="server">
    <input id="lblworkarea" type="hidden" name="Hidden2" runat="server">
    <input id="lblnc" type="hidden" name="lblnc" runat="server">
    <input id="lblld" type="hidden" name="lblld" runat="server">
    <input id="lblisdown" type="hidden" name="lblisdown" runat="server">
    <input id="lbluser" type="hidden" name="lbluser" runat="server">
    <input id="lblskill" type="hidden" name="lblskill" runat="server">
    <input id="lbldis" type="hidden" name="lbldis" runat="server">
    <input id="lbljpid" type="hidden" name="lbljpid" runat="server">
    <input id="lbljump" type="hidden" name="lbljump" runat="server">
    <input id="lblsav" type="hidden" name="lblsav" runat="server">
    <input id="lblcnt" type="hidden" name="lblcnt" runat="server">
    <input id="lblsdays" type="hidden" name="lblsdays" runat="server">
    <input id="lblse" type="hidden" name="lblse" runat="server">
    <input id="lblle" type="hidden" name="lblle" runat="server">
    <input id="lblpmid" type="hidden" name="lblpmid" runat="server">
    <input id="lblchecksave" type="hidden" name="lblchecksave" runat="server">
    <input id="lblpmhid" type="hidden" name="lblpmhid" runat="server">
    <input id="lblmalert" type="hidden" name="lblmalert" runat="server">
    <input id="lblusesched" type="hidden" name="lblusesched" runat="server">
    <input id="lbltpmid" type="hidden" runat="server">
    <input id="lblttime" type="hidden" name="lblttime" runat="server">
    <input id="lblacttime" type="hidden" name="lblacttime" runat="server">
    <input id="lbldtime" type="hidden" name="Hidden1" runat="server">
    <input id="lblactdtime" type="hidden" name="lblactdtime" runat="server">
    <input id="lblusetdt" type="hidden" name="lblusetdt" runat="server">
    <input id="lblusetotal" type="hidden" name="lblusetotal" runat="server">
    <input id="lblfcust" type="hidden" name="lblfcust" runat="server">
    <input id="lblretday" type="hidden" name="lblretday" runat="server">
    <input id="lbldays" type="hidden" name="lbldays" runat="server">
    <input id="txtn" type="hidden" name="txtn" runat="server">
    <input id="lblworuns" type="hidden" runat="server">
    <input id="lblruns" type="hidden" runat="server">
    <input type="hidden" id="lblplan" runat="server">
    <input type="hidden" id="lblplanner2" runat="server">
    <input type="hidden" id="lbltstart" runat="server">
    <input type="hidden" id="lbltcomp" runat="server">
    <input type="hidden" id="lblskillidret" runat="server" />
    <input type="hidden" id="lbloldjobplan" runat="server" />
    <input type="hidden" id="lblwojtid" runat="server" />
    <input type="hidden" id="lblisecd" runat="server" />
    <input type="hidden" id="lblrettyp" runat="server" />
    </form>
</body>
</html>
