<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PMRoutesMain.aspx.vb" Inherits="lucy_r12.PMRoutesMain" %>
<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PM Routes Main</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="javascript" src="../scripts/taskgrid.js"></script>
		<script language="JavaScript" src="../scripts1/PMRoutesMainaspx_1.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
         function handlert(rid, typ) {
         //alert(typ)
             window.location = "PMRoutes2.aspx?start=yes&from=rm&rid=" + rid + "&typ=" + typ + "&date=" + Date();
         }
         function handleadd(st, rtn, typ) {
             window.location = "PMRoutes2.aspx?start=" + st + "&typ=" + typ + "&from=" + rtn;
         }
         function checktyp() {
             var typ = document.getElementById("lbltyp").value;
             ///alert(typ)
             //if (typ != "pm" && typ != "") {
                 document.getElementById("ifmain").src = "PMRouteList.aspx?jump=no&typ=" + typ + "&rtyp=" + typ;
             //}
         }
     </script>
	</HEAD>
	<body class="tbg" onload="checkarch();checktyp();" >
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 7px; POSITION: absolute; TOP: 83px; z-index: 1;" cellSpacing="0" cellPadding="2"
				width="1080">
				<tr>
					<td class="thdrsing label" width="840" colSpan="7"><asp:Label id="lang1206" runat="server">Current Routes</asp:Label></td>
					<td width="0"></td>
					<td class="thdrsing label" width="240"><asp:Label id="lang1207" runat="server">Asset Hierarchy Reference</asp:Label></td>
				</tr>
				<tr>
					<td class="tdborder" id="pm" vAlign="top" colSpan="7">
						<table cellSpacing="0" cellPadding="0">
							<tr>
								<td><iframe id="ifmain"  frameBorder="no" width="840" scrolling="no"
										height="520" runat="server" style="BACKGROUND-COLOR: transparent" allowtransparency></iframe>
								</td>
							</tr>
						</table>
					</td>
					<td></td>
					<td vAlign="top" rowSpan="2">
						<table>
							<tr>
								<td><iframe id="ifarch" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-TOP-STYLE: none; PADDING-TOP: 0px; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: transparent; BORDER-BOTTOM-STYLE: none"
										src="PMArchRT.aspx?start=yes" frameBorder="no" width="240" scrolling="no" height="472"
										runat="server" allowtransparency></iframe>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<div class="details" id="subdiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 999; BORDER-LEFT: black 1px solid; WIDTH: 360px; BORDER-BOTTOM: black 1px solid; HEIGHT: 100px">
				<table cellSpacing="0" cellPadding="0" width="360" bgcolor="white">
					<tr bgColor="blue" height="20">
						<td class="labelwht"><asp:Label id="lang1208" runat="server">Revision Alert</asp:Label></td>
						<td align="right"><IMG onclick="closerev();" height="18" alt="" src="../images/close.gif" width="18"><br>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><iframe id="ifrev" style="WIDTH: 360px; HEIGHT: 100px; BACKGROUND-COLOR: transparent" src=""
								frameBorder="no" runat="server" allowtransparency> </iframe>
						</td>
					</tr>
				</table>
			</div>
			
			<input id="lbltab" type="hidden" name="lbltab" runat="server"> <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lblsid" type="hidden" name="lblsid" runat="server"> <input id="lbldid" type="hidden" name="lbldid" runat="server"><input id="lblclid" type="hidden" name="lblclid" runat="server">
			<input id="lblret" type="hidden" name="lblret" runat="server"><input id="lblchk" type="hidden" name="lblchk" runat="server">
			<input id="lbldchk" type="hidden" name="lbldchk" runat="server"><input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input id="lblcoid" type="hidden" name="lblcoid" runat="server"> <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
			<input id="lbltasklev" type="hidden" name="lbltasklev" runat="server"> <input id="lblcid" type="hidden" name="lblcid" runat="server">
			<input id="tasknum" type="hidden" name="tasknum" runat="server"><input id="taskcnt" type="hidden" name="taskcnt" runat="server">
			<input id="lblgetarch" type="hidden" name="lblgetarch" runat="server"> <input id="lblpmid" type="hidden" runat="server" NAME="lblpmid">
			<input type="hidden" id="lbljump" runat="server" NAME="lbljump">
		<input type="hidden" id="lbltyp" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
</form>
		<uc1:mmenu1 id="Mmenu11" runat="server"></uc1:mmenu1>
	</body>
</HTML>
