<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PMArchRT.aspx.vb" Inherits="lucy_r12.PMArchRT" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PMArchRT</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" type="text/javascript" src="../scripts1/PMArchRTaspx.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/gridnav.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
     <!--
         function chksrch() {
             document.getElementById("lblret").value = "srch";
             document.getElementById("form1").submit();
         }
         function getdets(eqid, sid, did, clid, chk, lid, eqnum) {
             //alert(eqnum)
             eqnum = eqnum.replace(/#/, "%23");
             //alert(eqnum)
             var eReturn = window.showModalDialog("../equip/fulist2dialog.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:400px; dialogWidth:450px; resizable=yes");
             if (eReturn) {
                 var ret = eReturn;
                 var retarr = ret.split(",");
                 var fid = retarr[0];
                 var cid = retarr[1];
                 if (fid == "log") {
                     window.parent.setref();
                 }
                 else {
                     if (fid != "no") {
                         if (cid != "") {
                             //gotoco(eqid, fid, cid, sid, did, clid, chk, lid)
                             gotoco(cid, fid, eqid)
                         }
                         else {
                             //gotofu(eqid, fid, sid, did, clid, chk, lid)
                             gotofu(eqid, fid)
                         }
                     }
                 }
             }
         }
         function fclose(td, img) {
             var str = document.getElementById(img).src
             var lst = str.lastIndexOf("/") + 1
             var loc = str.substr(lst)
             if (loc == 'minus.gif') {
                 document.getElementById(img).src = '../images/appbuttons/bgbuttons/plus.gif';
                 try {
                     document.getElementById(td).className = "details";
                 }
                 catch (err) {

                 }
             }
             else {
                 document.getElementById(img).src = '../images/appbuttons/bgbuttons/minus.gif';
                 try {
                     document.getElementById(td).className = "view";
                 }
                 catch (err) {

                 }
             }
         }
         function gotoeq(eid) {
             window.parent.geteq(eid);
         }
         function gotofu(fid, eid) {
             window.parent.getfu(fid, eid);
         }
         function gotoco(cid, fid, eid) {
             window.parent.getco(cid, fid, eid);
         }
         //-->
     </script>
	</HEAD>
	<body  class="tbg">
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 0px; background-color: transparent;" cellSpacing="0" cellPadding="0" width="230"
				>
				<tr>
					<td align="center">
						<table cellpadding="1" cellspacing="0">
							<tr>
								<td class="label"><asp:Label id="lang1175" runat="server">Search</asp:Label></td>
								<td>
									<asp:TextBox id="txtsrch" runat="server" CssClass="plainlabel" Width="150px"></asp:TextBox></td>
								<td><img src="../images/appbuttons/minibuttons/srchsm1.gif" onclick="chksrch();"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst2.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev2.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="140"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext2.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast2.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td id="tdarch" runat="server"></td>
				</tr>
			</table>
			<input type="hidden" id="lblsid" runat="server" NAME="lblsid"> <input type="hidden" id="txtpg" runat="server">
			<input type="hidden" id="txtpgcnt" runat="server"> <input type="hidden" id="lblret" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
