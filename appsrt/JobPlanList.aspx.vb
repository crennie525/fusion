

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class JobPlanList
    Inherits System.Web.UI.Page
    Protected WithEvents ovid162 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid161 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid160 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang1150 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1149 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1148 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim rl As New Utilities
    Dim Tables As String = "pmjobplans"
    Dim PK As String = "jpid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*, tdesc = (select description from jobplantypes where jptypeid = pmjobplans.type)"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String
    Dim list, sid, subm, jump, typ, eq, fu, co, wo As String
    Protected WithEvents imgadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sflag As Integer = 0
    Dim intPgCnt, intPgNav As Integer
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsavewo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblval1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblval2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldelid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents rbsort As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents tdlist As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                sid = Request.QueryString("sid").ToString
            Catch ex As Exception
                sid = HttpContext.Current.Session("dfltps").ToString
            End Try

            lblsid.Value = sid
            lblsort.Value = "asc"
            jump = Request.QueryString("jump").ToString
            If jump = "yes" Then
                typ = Request.QueryString("typ").ToString
                If typ = "eq" Then
                    lbleqid.Value = Request.QueryString("eqid").ToString
                ElseIf typ = "fu" Then
                    lbleqid.Value = Request.QueryString("eqid").ToString
                    lblfu.Value = Request.QueryString("fu").ToString
                ElseIf typ = "co" Then
                    lbleqid.Value = Request.QueryString("eqid").ToString
                    lblfu.Value = Request.QueryString("fu").ToString
                    lblco.Value = Request.QueryString("co").ToString
                ElseIf typ = "wo" Then
                    lblret.Value = "wo"
                    'typ = "no"
                    Try
                        wo = Request.QueryString("wo").ToString
                        lblwonum.Value = wo
                        lblsavewo.Value = "yes"
                    Catch ex As Exception
                        wo = ""
                    End Try
                ElseIf typ = "jp" Then
                    lblret.Value = "jp"
                    typ = "no"
                End If
            Else
                typ = "no"
            End If
            lbltyp.Value = typ
            txtpg.Value = 1
            rl.Open()
            GetRoutes(typ)
            rl.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                rl.Open()
                GetNext()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                rl.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                typ = lbltyp.Value
                GetRoutes(typ)
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                rl.Open()
                GetPrev()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                rl.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                typ = lbltyp.Value
                GetRoutes(typ)
                rl.Dispose()
                lblret.Value = ""
            End If
            subm = Request.Form("lblsubmit").ToString
            If Request.Form("lblsubmit") = "srch" Then
                lblsubmit.Value = ""
                typ = "no"
                lbltyp.Value = "no"
                rl.Open()
                GetRoutes(typ)
                rl.Dispose()
            ElseIf Request.Form("lblsubmit") = "sort" Then
                lblsubmit.Value = ""
                rl.Open()
                typ = lbltyp.Value
                GetRoutes(typ)
                rl.Dispose()
            ElseIf Request.Form("lblsubmit") = "checknew" Then
                lblsubmit.Value = ""
                rl.Open()

                rl.Dispose()
            ElseIf Request.Form("lblsubmit") = "savewo" Then
                lblsubmit.Value = ""
                rl.Open()
                savewo()
                rl.Dispose()
            ElseIf Request.Form("lblsubmit") = "delpm" Then
                lblsubmit.Value = ""
                rl.Open()
                delpm()
                typ = lbltyp.Value
                GetRoutes(typ)
                rl.Dispose()
            End If
        End If
    End Sub
    Private Sub delpm()
        Dim delid As String = lbldelid.Value
        sql = "delete from pmjobplans where jpid = '" & delid & "';delete from jplubes where jpid = '" & delid & "';" _
                   + "delete from pmjobtasks where jpid = '" & delid & "';delete from jpfailmodes where jpid = '" & delid & "';" _
                   + "delete from jpparts where jpid = '" & delid & "';delete from jptools where jpid = '" & delid & "';" _
                   + "delete from jppartsout where jpid = '" & delid & "';delete from jptoolsout where jpid = '" & delid & "';" _
                   + "delete from jplubesout where jpid = '" & delid & "';delete from jptools where jpid = '" & delid & "';" _
                   + "delete from pmTaskMeasDetails where jpid = '" & delid & "';delete from pmTaskMeasOutput where jpid = '" & delid & "';"
        rl.Update(sql)
    End Sub
    Private Sub savewo()
        Dim wonum As String
        wonum = lblwonum.Value
        Dim jpid As String = lblval1.Value
        sql = "usp_addjptowo '" & jpid & "','" & wonum & "'"
        rl.Update(sql)
        lblsubmit.Value = "return"

    End Sub
    Private Sub CheckNew()

    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            typ = lbltyp.Value
            GetRoutes(typ)
        Catch ex As Exception
            rl.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr477", "JobPlanList.aspx.vb")

            rl.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            typ = lbltyp.Value
            GetRoutes(typ)
        Catch ex As Exception
            rl.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr478", "JobPlanList.aspx.vb")

            rl.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetRoutes(ByVal type As String)
        Dim ret As String = lblret.Value
        Dim srch As String = txtsrch.Text
        sid = lblsid.Value
        Dim sortStr, sortVal, sortFilt As String
        sortStr = lblsort.Value
        sortVal = rbsort.SelectedValue.ToString
        If sortVal = "0" Then
            sortFilt = " order by jpnum " & sortStr
            Sort = "jpnum " & sortStr
        ElseIf sortVal = "1" Then
            sortFilt = " order by type " & sortStr
            Sort = "type " & sortStr
        ElseIf sortVal = "2" Then
            sortFilt = " order by createdby " & sortStr
            Sort = "createdby " & sortStr
        End If
        Filter = "(jpref is null or jprefstat = ''Complete'') and siteid = ''" & sid & "'' "
        FilterCnt = "(jpref is null or jprefstat = 'Complete') and siteid = '" & sid & "' "
        If Len(srch) > 0 Then
            srch = "%" & srch & "%"
            Filter += " and jpnum like ''" & srch & "'' or createdby like ''" & srch & "'' or description like ''" & srch & "''"
            FilterCnt += " and jpnum like '" & srch & "' or createdby like '" & srch & "' or description like '" & srch & "'"
            'sql = "select r.*, t.description as tdesc from pmjobplans r " _
            '+ "left join jobplantypes t on t.jptypeid = r.type  where r.siteid = '" & sid & "' and " _
            '+ "(jpnum like '" & srch & "' or createdby like '" & srch & "') " & sortFilt & ""
        Else
            Select Case type
                Case "no"
                    'Filter = "" '"siteid = ''" & sid & "''"
                    'FilterCnt = "" '"siteid = '" & sid & "'"
                    sql = "select r.*, t.description as tdesc from pmjobplans r " _
                    + "left join jobplantypes t on t.jptypeid = r.type where r.siteid = '" & sid & "' " & sortFilt
                Case "wo"
                    sql = "select r.*, t.description as tdesc from pmjobplans r " _
                   + "left join jobplantypes t on t.jptypeid = r.type where r.siteid = '" & sid & "' " & sortFilt
                Case "eq"
                    eq = lbleqid.Value
                    Filter = "eqid = ''" & eq & "'' and (jpref is null or jprefstat = ''Complete'') "
                    FilterCnt = "eqid = '" & eq & "' and (jpref is null or jprefstat = 'Complete') "
                    sql = "select r.*, t.description as tdesc from pmjobplans r " _
                    + "left join jobplantypes t on t.jptypeid = r.type where r.siteid = '" & sid & "' " & sortFilt _
                    + "where r.eqid = '" & eq & "' " & sortFilt
                Case "fu"
                    fu = lblfu.Value
                    Filter = "fuid = ''" & fu & "'' and (jpref is null or jprefstat = ''Complete'') "
                    FilterCnt = "fuid = '" & fu & "' and (jpref is null or jprefstat = 'Complete') "
                    sql = "select r.*, t.description as tdesc from pmjobplans r " _
                    + "left join jobplantypes t on t.jptypeid = r.type where r.siteid = '" & sid & "' " & sortFilt _
                    + "where r.funcid = '" & fu & "' " & sortFilt
                Case "co"
                    co = lblco.Value
                    Filter = "comid = ''" & co & "'' and (jpref is null or jprefstat = ''Complete'') "
                    FilterCnt = "comid = '" & co & "' and (jpref is null or jprefstat = 'Complete') "
                    sql = "select r.*, t.description as tdesc from pmjobplans r " _
                    + "left join jobplantypes t on t.jptypeid = r.type where r.siteid = '" & sid & "' " & sortFilt _
                    + "where r.comid = '" & co & "' " & sortFilt
            End Select
        End If
        Dim sb As StringBuilder = New StringBuilder


        'sb.Append("<div style=""OVERFLOW: auto; WIDTH: 720px;  HEIGHT: 380px"">")
        sb.Append("<table width=""770"">")
        sb.Append("<tr><td colspan=""6""><div style=""OVERFLOW: auto; width: 770px; height: 360px;"">")
        sb.Append("<table width=""750"">")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""140"">" & tmod.getlbl("cdlbl116", "JobPlanList.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""240"">" & tmod.getlbl("cdlbl117", "JobPlanList.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""120"">" & tmod.getlbl("cdlbl118", "JobPlanList.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""130"">" & tmod.getlbl("cdlbl119", "JobPlanList.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""60"">Print</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""60"">Delete</td></tr>")
        Dim rowid As Integer = 0
        Dim bc, rtid, rt, desc, typ, cb As String
        If FilterCnt <> "" Then
            sql = "select count(*) from pmjobplans where " & FilterCnt
        Else
            sql = "select count(*) from pmjobplans"
        End If
        'PageNumber = txtpg.Value
        'intPgCnt = rl.Scalar(sql)
        'intPgNav = rl.PageCount(intPgCnt, PageSize)
        'lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        'txtpgcnt.Value = intPgNav

        PageNumber = txtpg.Value
        intPgNav = rl.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav

        dr = rl.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        'dr = rl.GetRdrData(sql)
        While dr.Read
            If rowid = 0 Then
                rowid = 1
                bc = "transrow"
            Else
                rowid = 0
                bc = "transrowblue"
            End If
            rtid = dr.Item("jpid").ToString
            rt = dr.Item("jpnum").ToString
            desc = dr.Item("description").ToString
            typ = dr.Item("tdesc").ToString
            cb = dr.Item("createdby").ToString
            Select Case typ
                Case "0"
                    typ = "Safety Plan"
            End Select
            sb.Append("<tr><td class=""linklabel " & bc & """><a href=""#"" onclick=""rtret('" & rtid & "', '" & rt & "');"">")
            sb.Append(rt & "</a></td>")
            sb.Append("<td class=""plainlabel " & bc & """>" & desc & "</td>")
            sb.Append("<td class=""plainlabel " & bc & """>" & typ & "</td>")
            sb.Append("<td class=""plainlabel " & bc & """>" & cb & "</td>")
            sb.Append("<td align=""center"" class=""" & bc & """><IMG onmouseover=""return overlib('" & tmod.getov("cov139", "JobPlanList.aspx.vb") & "', ABOVE, LEFT)""")
            sb.Append(" onclick=""printpm('" & rtid & "');"" onmouseout=""return nd()"" src=""../images/appbuttons/minibuttons/printx.gif""></td>")
            sb.Append("<td align=""center"" class=""" & bc & """><IMG onmouseover=""return overlib('Delete This Job Plan', ABOVE, LEFT)""")
            sb.Append(" onclick=""delpm('" & rtid & "');"" onmouseout=""return nd()"" src=""../images/appbuttons/minibuttons/del.gif""></td></tr>")


        End While
        dr.Close()
        sb.Append("</table></div></td></tr></table>")
        tdlist.InnerHtml = sb.ToString
    End Sub











    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1148.Text = axlabs.GetASPXPage("JobPlanList.aspx", "lang1148")
        Catch ex As Exception
        End Try
        Try
            lang1149.Text = axlabs.GetASPXPage("JobPlanList.aspx", "lang1149")
        Catch ex As Exception
        End Try
        Try
            lang1150.Text = axlabs.GetASPXPage("JobPlanList.aspx", "lang1150")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgadd.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("JobPlanList.aspx", "imgadd") & "', ABOVE, LEFT)")
            imgadd.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid160.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("JobPlanList.aspx", "ovid160") & "', ABOVE, LEFT)")
            ovid160.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid161.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("JobPlanList.aspx", "ovid161") & "', ABOVE, LEFT)")
            ovid161.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid162.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("JobPlanList.aspx", "ovid162") & "', ABOVE, LEFT)")
            ovid162.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
