<%@ Page Language="vb" AutoEventWireup="false" Codebehind="JobPlanTaskList.aspx.vb" Inherits="lucy_r12.JobPlanTaskList" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>JobPlanTaskList</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td>
						<asp:datagrid id="dgtasks" runat="server" BorderWidth="0" CellSpacing="1" CellPadding="3" AutoGenerateColumns="False"
							AllowSorting="True">
							<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
							<ItemStyle CssClass="plainlabel" BackColor="White"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Task#">
									<HeaderStyle CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblta runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
										</asp:Label>
									</ItemTemplate>
									
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Task Description">
									<HeaderStyle Width="280px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label3 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>' Width="270px">
										</asp:Label>
									</ItemTemplate>
									
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Failure Mode">
									<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.failuremode") %>'>
										</asp:Label>
									</ItemTemplate>
									
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Skill Required">
									<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
										</asp:Label>
									</ItemTemplate>
								
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Skill Qty">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>' ID="Label10" NAME="Label8">
										</asp:Label>
									</ItemTemplate>
									
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Skill Min Ea">
									<HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>' ID="Label12" >
										</asp:Label>
									</ItemTemplate>
									
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Down Time" Visible="False">
									<HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rdt") %>' ID="Label2" >
										</asp:Label>
									</ItemTemplate>
								
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
					</td>
				</tr>
			</table>
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
