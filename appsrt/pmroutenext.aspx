﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmroutenext.aspx.vb" Inherits="lucy_r12.pmroutenext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PM Route Next Date</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function retvals() {
            var ret = document.getElementById("lblsubmit").value;
            var wo = document.getElementById("lblwonum").value;
            var who = document.getElementById("lblwho").value;
            if (ret == "yes") {
                var ndate = document.getElementById("lblnew").value;
                var who = document.getElementById("lblwho").value;
                if (who == "next" || who == "comp") {
                    window.parent.savedateret(ndate, who);
                }
                else {
                    window.parent.saveworet(wo, who);
                }
            }
            
        }
    </script>
</head>
<body onload="retvals();">
    <form id="form1" runat="server">
    
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblnew" runat="server" />
    <input type="hidden" id="lblwonum" runat="server" />
    </form>
</body>
</html>
