<%@ Page Language="vb" AutoEventWireup="false" Codebehind="JSub.aspx.vb" Inherits="lucy_r12.JSub" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>JSub</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" src="../scripts1/JSubaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="checkinv();" >
		<form id="form1" method="post" runat="server">
			<table cellSpacing="1">
				<tr>
					<td class="thdrsing label" colSpan="7"><asp:Label id="lang1163" runat="server">Sub Task View - Details</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="7"><asp:datalist id="dlhd" runat="server">
							<HeaderTemplate>
								<table width="700">
							</HeaderTemplate>
							<ItemTemplate>
								<TR height="20">
									<TD class="label"><asp:Label id="lang1164" runat="server">Department</asp:Label></TD>
									<TD>
										<asp:label CssClass="plainlabel" id="lbldept" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dept_line") %>'>
										</asp:label></TD>
									<TD class="label"><asp:Label id="lang1165" runat="server">Station/Cell</asp:Label></TD>
									<TD colSpan="3">
										<asp:label CssClass="plainlabel" id="lblcell" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cell_name") %>'>
										</asp:label></TD>
								</TR>
								<TR height="20">
									<TD class="label"><asp:Label id="lang1166" runat="server">Equipment</asp:Label></TD>
									<TD>
										<asp:label CssClass="plainlabel" id="lbleq" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
										</asp:label></TD>
									<TD class="label"><asp:Label id="lang1167" runat="server">Function</asp:Label></TD>
									<TD>
										<asp:label CssClass="plainlabel" id="lblfunc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
										</asp:label></TD>
								</TR>
								<tr height="20">
									<TD class="label"><asp:Label id="lang1168" runat="server">Component</asp:Label></TD>
									<TD>
										<asp:label CssClass="plainlabel" id="lblcompon" runat="server" ForeColor="Black" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
										</asp:label></TD>
								</tr>
								<TR>
									<TD width="100"></TD>
									<TD width="200"></TD>
									<TD width="100"></TD>
									<TD width="200"></TD>
									<TD width="100"></TD>
									<TD width="200"></TD>
								</TR>
							</ItemTemplate>
						</asp:datalist></td>
				</tr>
			</table>
			<table cellSpacing="1">
				<tr>
					<td class="thdrsing label" colSpan="7"><asp:Label id="lang1169" runat="server">Current Task</asp:Label></td>
				</tr>
				<tr>
					<td class="btmmenu plainlabel" width="60"><asp:Label id="lang1170" runat="server">Task#</asp:Label></td>
					<td class="btmmenu plainlabel" width="420"><asp:Label id="lang1171" runat="server">Top Level Task Description</asp:Label></td>
					<td class="btmmenu plainlabel" width="100"><asp:Label id="lang1172" runat="server">Skill Required</asp:Label></td>
					<td class="btmmenu plainlabel" width="60">Qty</td>
					<td class="btmmenu plainlabel" width="60"><asp:Label id="lang1173" runat="server">Min Ea</asp:Label></td>
				</tr>
				<tr>
					<td id="tdtnum" class="plainlabel" runat="server"></td>
					<td id="tddesc" class="plainlabel" runat="server"></td>
					<td id="tdskill" class="plainlabel" runat="server"></td>
					<td id="tdqty" class="plainlabel" runat="server"></td>
					<td id="tdmin" class="plainlabel" runat="server"></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table>
				<tr>
					<td class="thdrsing label" colSpan="7"><asp:Label id="lang1174" runat="server">Sub Tasks</asp:Label></td>
				</tr>
				<tr>
					<td align="right"><asp:imagebutton id="addtask" runat="server" ImageUrl="../images/appbuttons/bgbuttons/addtask.gif"></asp:imagebutton>&nbsp;<IMG id="btnreturn" onclick="handlereturn();" src="../images/appbuttons/bgbuttons/return.gif"
							width="69" height="19" runat="server"></td>
				</tr>
				<tr>
					<td><asp:datagrid id="dgtasks" runat="server" GridLines="None" AllowSorting="True" AutoGenerateColumns="False"
							CellSpacing="1">
							<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
							<ItemStyle CssClass="plainlabel" BackColor="White"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:imagebutton id="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:imagebutton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="Task#">
									<HeaderStyle CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblta runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox cssclass="plainlabel wd40" id=lblt runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>' width="40px">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Sub Task#">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblsubt runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox cssclass="plainlabel wd40" id=lblst runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>' width="40px">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Sub Task Description">
									<HeaderStyle Width="280px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label3 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>' Width="270px">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox cssclass="plainlabel wd260" id=txtdesc runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>' Height="70px" TextMode="MultiLine" width="260px" MaxLength="1000">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Skill Qty">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>' ID="Label10">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txtqty" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Skill Min Ea">
									<HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>' ID="Label12" >
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txttr" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="Down Time">
									<HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rdt") %>' ID="Label2" >
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txtdt" runat="server" cssclass="plainlabel wd50" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.rdt") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Parts/Tools/Lubes">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:imagebutton id="Imagebutton23" runat="server" ImageUrl="../images/appbuttons/minibuttons/parttrans.gif"
											ToolTip="Add Parts" CommandName="Part"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton23a" runat="server" ImageUrl="../images/appbuttons/minibuttons/tooltrans.gif"
											ToolTip="Add Tools" CommandName="Tool"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton23b" runat="server" ImageUrl="../images/appbuttons/minibuttons/lubetrans.gif"
											ToolTip="Add Lubricants" CommandName="Lube"></asp:imagebutton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="pmtskid">
									<ItemTemplate>
										<asp:Label id="lbltida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblttide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Delete">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:ImageButton id="ibDel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
			<input id="lbljpid" type="hidden" name="lbljpid" runat="server"> <input id="lbltid" type="hidden" name="lbltid" runat="server">
			<input id="lblfuid" type="hidden" name="lblfuid" runat="server"> <input id="lblfilt" type="hidden" name="lblfilt" runat="server">
			<input id="lblsubval" type="hidden" name="lblsubval" runat="server"><input id="lblsid" type="hidden" name="lblsid" runat="server">
			<input id="lbloldtask" type="hidden" name="lbloldtask" runat="server"><input id="lblpart" type="hidden" name="lblpart" runat="server">
			<input id="lbltool" type="hidden" name="lbltool" runat="server"><input id="lbllube" type="hidden" name="lbllube" runat="server">
			<input id="lblnote" type="hidden" name="lblnote" runat="server"><input id="lbltasknum" type="hidden" name="lbltasknum" runat="server">
			<input id="lblpmtid" type="hidden" name="lblpmtid" runat="server"><input id="lbllog" type="hidden" name="lbllog" runat="server">
			<input id="lbllock" type="hidden" name="lbllock" runat="server"> <input id="lbllockedby" type="hidden" name="lbllockedby" runat="server">
			<input id="lblusername" type="hidden" name="lblusername" runat="server"><input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lblwo" type="hidden" runat="server"><input id="lblro" type="hidden" runat="server">
			<input id="lblpmtskid" type="hidden" runat="server"> <input type="hidden" id="lbltr" runat="server">
			<input type="hidden" id="lblcid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
