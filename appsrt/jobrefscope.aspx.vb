

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class jobrefscope
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    '
    Protected WithEvents bgbsubmit As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim sc As New Utilities
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim wo, scope, jpref, typ As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtscope As System.Web.UI.WebControls.TextBox
    Protected WithEvents ImageButton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpref As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load




        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            wo = Request.QueryString("wo").ToString
            lblwonum.Value = wo
            jpref = Request.QueryString("jpref").ToString
            lbljpref.Value = jpref
            Try
                typ = Request.QueryString("typ").ToString
                If typ = "rem" Then
                    sc.Open()
                    RemRef()
                    sc.Dispose()
                End If
            Catch ex As Exception
                typ = "no"
            End Try
        Else
            If Request.Form("lblsubmit") = "getreq" Then
                sc.Open()
                jpref = lbljpref.Value
                If jpref = "" Then
                    GetReq()
                    lblsubmit.Value = "retreq"
                Else
                    SaveReq()
                    lblsubmit.Value = "retonly"

                End If
                sc.Dispose()
            ElseIf Request.Form("lblsubmit") = "remreq" Then
                sc.Open()
                RemRef()
                lblsubmit.Value = "rem"
                sc.Dispose()
            End If
        End If
    End Sub
    Private Sub RemRef()
        wo = lblwonum.Value
        Dim jpref As String = lbljpref.Value
        sql = "usp_addremjpref '" & wo & "','" & jpref & "','','rem'"
        sc.Update(sql)
        lblsubmit.Value = "retonly"
    End Sub
    Private Sub SaveReq()
        jpref = lbljpref.Value
        scope = txtscope.Text
        scope = Replace(scope, "'", Chr(180), , , vbTextCompare)
        scope = Replace(scope, "--", "-", , , vbTextCompare)
        scope = Replace(scope, ";", ":", , , vbTextCompare)
        sql = "usp_upjprefscope '" & jpref & "','" & scope & "'"
        sc.Update(sql)
    End Sub
    Private Sub GetReq()
        Dim ret As Integer
        wo = lblwonum.Value
        scope = txtscope.Text
        scope = Replace(scope, "'", Chr(180), , , vbTextCompare)
        scope = Replace(scope, "--", "-", , , vbTextCompare)
        scope = Replace(scope, ";", ":", , , vbTextCompare)
        Dim jpref As String '= ""
        sql = "usp_addremjpref '" & wo & "','" & jpref & "','" & scope & "','add'"
        ret = sc.Scalar(sql)
        lbljpref.Value = ret
    End Sub







End Class
