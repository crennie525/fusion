<%@ Page Language="vb" AutoEventWireup="false" Codebehind="JobLoc.aspx.vb" Inherits="lucy_r12.JobLoc" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>JobLoc</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="bluelabel"><asp:Label id="lang1120" runat="server">Equipment Based</asp:Label></td>
				</tr>
				<tr>
					<td>
						<table>
							<tr>
								<td width="92"></td>
								<td width="180"></td>
							</tr>
							<tr id="deptdiv" runat="server">
								<td class="label"><asp:Label id="lang1121" runat="server">Department</asp:Label></td>
								<td class="label"><asp:dropdownlist id="dddepts" runat="server" CssClass="plainlabel" AutoPostBack="True" Width="170px"></asp:dropdownlist></td>
							</tr>
							<tr id="celldiv" runat="server">
								<td class="label"><asp:Label id="lang1122" runat="server">Station/Cell</asp:Label></td>
								<td class="label"><asp:dropdownlist id="ddcells" runat="server" CssClass="plainlabel" AutoPostBack="True" Width="170px"></asp:dropdownlist></td>
							</tr>
							<tr id="eqdiv" runat="server">
								<td class="label" style="HEIGHT: 1px"><asp:Label id="lang1123" runat="server">Equipment#</asp:Label></td>
								<td class="label" style="HEIGHT: 1px"><asp:dropdownlist id="ddeq" runat="server" CssClass="plainlabel" AutoPostBack="True" Width="170px"></asp:dropdownlist></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lblsid" runat="server"> <input type="hidden" id="lblcid" runat="server">
			<input type="hidden" id="lbldept" runat="server"> <input type="hidden" id="lblclid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
