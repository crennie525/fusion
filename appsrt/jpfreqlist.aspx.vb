

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class jpfreqlist
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dval As New Utilities
    Dim dr As SqlDataReader
    Dim vid, vind, val, pmtskid, fld, sid, wo As String
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents divval As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblpmtskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfld As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblvid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblvind As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            pmtskid = Request.QueryString("pmtskid").ToString
            fld = Request.QueryString("fld").ToString
            sid = Request.QueryString("sid").ToString
            wo = Request.QueryString("wo").ToString
            lblpmtskid.Value = pmtskid
            lblfld.Value = fld
            lblsid.Value = sid
            lblwo.Value = wo
            dval.Open()
            If fld = "skill" Then
                GetSkill()
            End If
            dval.Dispose()
        Else
            If Request.Form("lblsubmit") = "saveit" Then
                lblsubmit.Value = ""
                fld = lblfld.Value
                dval.Open()
                SaveSkill()
                dval.Dispose()
            End If
        End If
    End Sub
    Private Sub GetSkill()
        Dim cid As String = "0"
        sid = lblsid.Value
        Dim sb As New StringBuilder
        Dim scnt, scnt2 As Integer
        sql = "select count(*) from pmSiteSkills where (compid = '" & cid & "' and siteid = '" & sid & "') or skillindex = '0'"

        scnt = dval.Scalar(sql)


        If scnt > 1 Then
            sql = "select count(*) from pmsiteskills where skill = 'Select' and compid = '" & cid & "'" ' and siteid = '" & sid & "'"

            scnt2 = dval.Scalar(sql)

            sql = "select skillid, skill, skillindex from pmSiteSkills where (compid = '" & cid & "' and siteid = '" & sid & "') or skillindex = '0' order by skillindex"


            dr = dval.GetRdrData(sql)
            sb.Append("<table>")
            While dr.Read
                vid = dr.Item("skillid").ToString
                val = dr.Item("skill").ToString
                vind = "0"
                sb.Append("<tr><td class=""plainlabel"">")
                sb.Append("<a class=""A1"" href=""#"" onclick=""getval('" & vid & "','" & val & "','" & vind & "')"">")
                sb.Append(val & "</a>")
                sb.Append("</td></tr>")

            End While
            dr.Close()
            sb.Append("</table>")
            divval.InnerHtml = sb.ToString
        Else
            sql = "select skillid, skill, skillindex from pmSkills where skill <> 'Select' and compid = '" & cid & "' or skillindex = '0'" ' order by skillindex"
            dr = dval.GetRdrData(sql)
            sb.Append("<table>")
            While dr.Read
                vid = dr.Item("skillid").ToString
                val = dr.Item("skill").ToString
                vind = "0"
                sb.Append("<tr><td class=""plainlabel"">")
                sb.Append("<a class=""A1"" href=""#"" onclick=""getval('" & vid & "','" & val & "','" & vind & "')"">")
                sb.Append(val & "</a>")
                sb.Append("</td></tr>")

            End While
            dr.Close()
            sb.Append("</table>")
            divval.InnerHtml = sb.ToString
        End If
    End Sub
    Private Sub SaveSkill()
        pmtskid = lblpmtskid.Value
        sid = lblsid.Value
        vid = lblvid.Value
        val = lblval.Value
        vind = lblvind.Value
        wo = lblwo.Value
        sql = "update pmjobtasks set " _
        + "skillid = '" & vid & "', " _
        + "skill = '" & val & "', " _
        + "rate = (select rate from pmskills where skillid = '" & vid & "') " _
        + "where pmtskid = '" & pmtskid & "'"
        dval.Update(sql)
        If wo <> "" Then
            sql = "update wojobtasks set " _
            + "skillid = '" & vid & "', " _
            + "skill = '" & val & "', " _
            + "rate = (select rate from pmskills where skillid = '" & vid & "') " _
            + "where pmtskid = '" & pmtskid & "' and wonum = '" & wo & "' "
            dval.Update(sql)
        End If
        lblsubmit.Value = "go"
    End Sub
End Class
