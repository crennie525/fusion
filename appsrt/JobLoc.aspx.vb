

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class JobLoc
    Inherits System.Web.UI.Page
	Protected WithEvents lang1123 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1122 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1121 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1120 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim tasks As New Utilities
    Dim sql, cid, sid, eq, eqid, fu, fuid, did, clid As String
    Dim Filter, filt, dt, df, tl, val, tli As String
    Dim pmid, type, login As String
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dddepts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcells As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddeq As System.Web.UI.WebControls.DropDownList
    Protected WithEvents deptdiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents celldiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents eqdiv As System.Web.UI.HtmlControls.HtmlTableRow

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            cid = "0" 'HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            tasks.Open()
            PopDepts(sid)
            tasks.Dispose()
            ddcells.Enabled = False
            ddeq.Enabled = False

        End If
    End Sub
    Private Sub PopDepts(ByVal sid As String)
        filt = " where siteid = '" & sid & "'"
        sql = "select count(*) from Dept where siteid = '" & sid & "'"
        Dim dcnt As Integer = tasks.Scalar(sql)
        If dcnt > 0 Then
            dt = "Dept"
            val = "dept_id , dept_line "
            dr = tasks.GetList(dt, val, filt)
            dddepts.DataSource = dr
            dddepts.DataTextField = "dept_line"
            dddepts.DataValueField = "dept_id"
            dddepts.DataBind()
            dr.Close()
            dddepts.Enabled = True
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr461" , "JobLoc.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            dddepts.Enabled = False
        End If
        dddepts.Items.Insert(0, "Select Department")
    End Sub

    Private Sub dddepts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dddepts.SelectedIndexChanged
        Dim dept As String = dddepts.SelectedValue.ToString
        tasks.Open()
        If dddepts.SelectedIndex <> 0 Then
            'celldiv.Style.Add("display", "none")
            'celldiv.Style.Add("visibility", "hidden")
            'eqdiv.Style.Add("display", "none")
            'eqdiv.Style.Add("visibility", "hidden")
            ddcells.Enabled = False
            ddeq.Enabled = False
            Dim deptname As String = dddepts.SelectedItem.ToString
            lbldept.Value = dept
            dt = "dept"
            filt = " where dept_id = " & dept
            sql = "select count(*) from Cells where dept_id = '" & dept & "'"
            Filter = dept
            Dim chk As String = CellCheck(Filter)
            If chk = "no" Then
                'eqdiv.Style.Add("display", "none")
                'eqdiv.Style.Add("visibility", "hidden")
                ddcells.Enabled = False
                ddeq.Enabled = False
                PopEq(dept, "d")
            Else
                ddcells.Enabled = False
                ddeq.Enabled = False
                PopCells(dept)
            End If
        End If
        tasks.Dispose()

    End Sub
    Private Sub PopCells(ByVal dept As String)
        filt = " where dept_id = " & dept
        dt = "Cells"
        val = "cellid , cell_name "
        dr = tasks.GetList(dt, val, filt)
        ddcells.DataSource = dr
        ddcells.DataTextField = "cell_name"
        ddcells.DataValueField = "cellid"
        ddcells.DataBind()
        dr.Close()
        ddcells.Items.Insert(0, "Select Station/Cell")
        'celldiv.Style.Add("display", "block")
        'celldiv.Style.Add("visibility", "visible")
        ddcells.Enabled = True
    End Sub
    Private Sub PopEq(ByVal var As String, ByVal typ As String)

        Dim eqcnt As Integer
        If typ = "d" Then
            sql = "select count(*) from equipment where dept_id = '" & var & "'"
            filt = " where dept_id = '" & var & "'"
        Else
            sql = "select count(*) from equipment where cellid = '" & var & "'"
            filt = " where cellid = '" & var & "'"
        End If
        eqcnt = tasks.Scalar(sql)
        If eqcnt > 0 Then
            dt = "equipment"
            val = "eqid, eqnum "

            dr = tasks.GetList(dt, val, filt)
            ddeq.DataSource = dr
            ddeq.DataTextField = "eqnum"
            ddeq.DataValueField = "eqid"
            ddeq.DataBind()
            dr.Close()
            ddeq.Items.Insert(0, "Select Equipment")
            eqdiv.Style.Add("display", "block")
            eqdiv.Style.Add("visibility", "visible")
            ddeq.Enabled = True
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr462" , "JobLoc.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ddeq.Items.Insert(0, "Select Equipment")
            eqdiv.Style.Add("display", "block")
            eqdiv.Style.Add("visibility", "visible")
            ddeq.Enabled = False
        End If

    End Sub
    Public Function CellCheck(ByVal filt As String) As String
        Dim cells As String
        Dim cellcnt As Integer
        sql = "select count(*) from Cells where Dept_ID = '" & filt & "'"
        cellcnt = tasks.Scalar(sql)
        Dim nocellcnt As Integer
        If cellcnt = 0 Then
            cells = "no"
        ElseIf cellcnt = 1 Then
            sql = "select count(*) from Cells where Dept_ID = '" & filt & "' and cell_name = 'No Cells'"
            nocellcnt = tasks.Scalar(sql)
            If nocellcnt = 1 Then
                cells = "no"
            Else
                cells = "yes"
            End If
        Else
            cells = "yes"
        End If
        Return cells
    End Function

    Private Sub ddcells_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcells.SelectedIndexChanged
        Dim cell As String = ddcells.SelectedValue.ToString
        If ddcells.SelectedIndex <> 0 Then
            tasks.Open()
            lblclid.Value = cell
            PopEq(cell, "c")
            tasks.Dispose()
        End If
    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang1120.Text = axlabs.GetASPXPage("JobLoc.aspx","lang1120")
		Catch ex As Exception
		End Try
		Try
			lang1121.Text = axlabs.GetASPXPage("JobLoc.aspx","lang1121")
		Catch ex As Exception
		End Try
		Try
			lang1122.Text = axlabs.GetASPXPage("JobLoc.aspx","lang1122")
		Catch ex As Exception
		End Try
		Try
			lang1123.Text = axlabs.GetASPXPage("JobLoc.aspx","lang1123")
		Catch ex As Exception
		End Try

	End Sub

End Class
