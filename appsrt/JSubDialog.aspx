<%@ Page Language="vb" AutoEventWireup="false" Codebehind="JSubDialog.aspx.vb" Inherits="lucy_r12.JSubDialog" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title runat="server" id="pgtitle">Job Plan Sub Tasks Dialog</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<script language="JavaScript" src="../scripts1/JSubDialogaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="handleentry();" onunload="handleexit();" class="tbg">
		<form id="form1" method="post" runat="server">
			<table width="100%" height="100%">
				<tr>
					<td>
						<iframe id="ifsm" runat="server" src="" width="100%" height="150%" frameBorder="no"></iframe>
					</td>
				</tr>
			</table>
            <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;"></iframe>
     <script type="text/javascript">
         document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script>
			<input id="lblcid" type="hidden" runat="server" NAME="lblcid"> <input type="hidden" id="lbllog" runat="server" NAME="lbllog">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
