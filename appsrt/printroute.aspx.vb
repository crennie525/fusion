

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class printroute
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim route, typ As String
    Dim rt As New Utilities
    Dim dr As SqlDataReader

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Try
                Dim dlang As New mmenu_utils_a
                lblfslang.Value = dlang.AppDfltLang
            Catch ex1 As Exception
                lblfslang.Value = "eng"
            End Try
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Response.Clear()
            route = Request.QueryString("rt").ToString
            typ = Request.QueryString("typ").ToString
            rt.Open()
            GetRoute(route, typ)
            rt.Dispose()
        End If
    End Sub
    Private Sub GetRoute(ByVal route As String, ByVal typ As String)
        Dim sb As New System.Text.StringBuilder


        sb.Append("<html><head>")
        sb.Append("<LINK href=""../styles/pmcssa1.css"" type=""text/css"" rel=""stylesheet"">")
        sb.Append("</head><body>")
        Dim rd, rdd As String
        sql = "select route, description from pmroutes where rid = '" & route & "'"
        dr = rt.GetRdrData(sql)
        While dr.Read
            rd = dr.Item("route").ToString
            rdd = dr.Item("description").ToString
        End While
        dr.Close()
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
        sb.Append("<tr><td class=""bigbold"" align=""center"">" & tmod.getlbl("cdlbl150", "printroute.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td class=""plainlabel"" align=""center""><b>Route:&nbsp;&nbsp;</b>" & rd & "</td></tr>")
        sb.Append("<tr><td class=""plainlabel"" align=""center""><b>Description:&nbsp;&nbsp;</b>" & rdd & "</td></tr>")
        sb.Append("<tr><td>&nbsp;</td></tr>")
        sb.Append("</table>")

        If typ = "PM" Or typ = "RBAS" Then
            sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
            sb.Append("<tr><td width=""30"" class=""label""><u>Seq#</u></td>")
            sb.Append("<td width=""140"" class=""label""><u>Equipment#</u></td>")
            'sb.Append("<td width=""120"" class=""label""><u>Misc Asset#</u></td>")
            sb.Append("<td width=""140"" class=""label""><u>Dept/Cell</u></td>")
            sb.Append("<td width=""140"" class=""label""><u>Location</u></td>")
            sb.Append("<td width=""200"" class=""label""><u>PM</u></td></tr>")
        ElseIf typ = "JP" Then
            sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
            sb.Append("<tr><td width=""30"" class=""label""><u>Seq#</u></td>")
            sb.Append("<td width=""120"" class=""label""><u>Equipment#</u></td>")
            sb.Append("<td width=""120"" class=""label""><u>Misc Asset#</u></td>")
            sb.Append("<td width=""120"" class=""label""><u>Dept/Cell</u></td>")
            sb.Append("<td width=""120"" class=""label""><u>Location</u></td>")
            sb.Append("<td width=""150"" class=""label""><u>Job Plan</u></td></tr>")
        ElseIf typ = "EQ" Then
            sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
            sb.Append("<tr><td width=""30"" class=""label""><u>Seq#</u></td>")
            sb.Append("<td width=""120"" class=""label""><u>Equipment#</u></td>")
            sb.Append("<td width=""500"" class=""label""><u>Description</u></td><tr>")
        ElseIf typ = "NC" Then
            sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
            sb.Append("<tr><td width=""30"" class=""label""><u>Seq#</u></td>")
            sb.Append("<td width=""120"" class=""label""><u>Misc Asset#</u></td>")
            sb.Append("<td width=""500"" class=""label""><u>Description</u></td><tr>")
        ElseIf typ = "DC" Then
            sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
            sb.Append("<tr><td width=""30"" class=""label""><u>Seq#</u></td>")
            sb.Append("<td width=""620"" class=""label""><u>Dept/Cell</u></td></tr>")
        ElseIf typ = "LO" Then
            sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
            sb.Append("<tr><td width=""30"" class=""label""><u>Seq#</u></td>")
            sb.Append("<td width=""620"" class=""label""><u>Location</u></td></tr>")
        End If

        If typ = "RBAS" Then
            sql = "select distinct s.*, e.eqnum, e.eqdesc, n.ncnum, ncdesc, d.dept_line + '/' + c.cell_name as dcell, " _
        + "s.pmstr as pmnum,  j.jpnum, " _
        + "l.location from pmroute_stops s " _
        + "left join equipment e on e.eqid = s.eqid " _
        + "left join noncritical n on n.ncid = s.ncid " _
        + "left join dept d on d.dept_id = s.deptid " _
        + "left join cells c on c.cellid = s.cellid " _
        + "left join pmtasks t on t.pmid = s.pmid and t.pmid <> 0 " _
        + "left join pmjobplans j on j.jpid = s.jpid " _
        + "left join pmlocations l on l.locid = s.locid " _
        + "where s.rid = '" & route & "' order by s.stopsequence"
        Else
            sql = "select distinct s.*, e.eqnum, e.eqdesc, n.ncnum, ncdesc, d.dept_line + '/' + c.cell_name as dcell, " _
        + "pmnum = (t.skill + '/' + t.freq + ' days/' + t.rd),  j.jpnum, " _
        + "l.location from pmroute_stops s " _
        + "left join equipment e on e.eqid = s.eqid " _
        + "left join noncritical n on n.ncid = s.ncid " _
        + "left join dept d on d.dept_id = s.deptid " _
        + "left join cells c on c.cellid = s.cellid " _
        + "left join pmtasks t on t.pmid = s.pmid and t.pmid <> 0 " _
        + "left join pmjobplans j on j.jpid = s.jpid " _
        + "left join pmlocations l on l.locid = s.locid " _
        + "where s.rid = '" & route & "' order by s.stopsequence"
        End If
        
        dr = rt.GetRdrData(sql)
        Dim seq, eq, eqd, nc, ncd, dc, loc, pm, jp As String
        While dr.Read
            seq = dr.Item("stopsequence").ToString
            eq = dr.Item("eqnum").ToString
            eqd = dr.Item("eqdesc").ToString
            nc = dr.Item("ncnum").ToString
            ncd = dr.Item("ncdesc").ToString
            dc = dr.Item("dcell").ToString
            loc = dr.Item("location").ToString
            pm = dr.Item("pmnum").ToString
            jp = dr.Item("jpnum").ToString
            If typ = "PM" Or typ = "RBAS" Then
                'sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
                sb.Append("<tr><td class=""plainlabel"">" & seq & "</td>")
                sb.Append("<td class=""plainlabel"">" & eq & "</td>")
                'sb.Append("<td class=""plainlabel"">" & nc & "</td>")
                sb.Append("<td class=""plainlabel"">" & dc & "</td>")
                sb.Append("<td class=""plainlabel"">" & loc & "</td>")
                sb.Append("<td class=""plainlabel"">" & pm & "</td></tr>")
            ElseIf typ = "JP" Then
                'sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
                sb.Append("<tr><td width=""30"" class=""plainlabel"">" & seq & "</td>")
                sb.Append("<td class=""plainlabel"">" & eq & "</td>")
                sb.Append("<td class=""plainlabel"">" & nc & "</td>")
                sb.Append("<td class=""plainlabel"">" & dc & "</td>")
                sb.Append("<td class=""plainlabel"">" & loc & "</td>")
                sb.Append("<td class=""plainlabel"">" & jp & "</td></tr>")
            ElseIf typ = "EQ" Then
                'sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
                sb.Append("<tr><td width=""30"" class=""plainlabel"">" & seq & "</td>")
                sb.Append("<td width=""120"" class=""plainlabel"">" & eq & "</td>")
                sb.Append("<td width=""500"" class=""plainlabel"">" & eqd & "</td><tr>")
            ElseIf typ = "NC" Then
                'sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
                sb.Append("<tr><td width=""30"" class=""plainlabel"">" & seq & "</td>")
                sb.Append("<td width=""120"" class=""plainlabel"">" & nc & "</td>")
                sb.Append("<td width=""500"" class=""plainlabel"">" & ncd & "</td><tr>")
            ElseIf typ = "DC" Then
                'sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
                sb.Append("<tr><td width=""30"" class=""plainlabel"">" & seq & "</td>")
                sb.Append("<td width=""620"" class=""plainlabel"">" & dc & "</td></tr>")
            ElseIf typ = "LO" Then
                'sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
                sb.Append("<tr><td width=""30"" class=""plainlabel"">" & seq & "</td>")
                sb.Append("<td width=""620"" class=""plainlabel"">" & loc & "</td></tr>")
            End If

        End While
        dr.Close()
        sb.Append("</table>")
        Response.Write(sb.ToString)

    End Sub
End Class
