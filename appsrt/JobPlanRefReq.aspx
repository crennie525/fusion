<%@ Page Language="vb" AutoEventWireup="false" Codebehind="JobPlanRefReq.aspx.vb" Inherits="lucy_r12.JobPlanRefReq" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>JobPlanRefReq</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="JavaScript" src="../scripts1/JobPlanRefReqaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checknew();checkit();" class="tbg">
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; RIGHT: 5px; LEFT: 5px" width="720">
				<tr>
					<td class="thdrsingg plainlabel" colSpan="6"><asp:Label id="lang1154" runat="server">Job Plan Request Options</asp:Label></td>
				</tr>
				<tr>
					<td>
						<table>
							<tr>
								<td class="label"><asp:Label id="lang1155" runat="server">Search Job Plan Requests</asp:Label></td>
								<td><asp:textbox id="txtsrch" runat="server" Width="220"></asp:textbox></td>
								<td><IMG onmouseover="return overlib('Search Requests', ABOVE, LEFT)" onclick="srchrts();"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/srchsm.gif"></td>
								<td><IMG id="imgadd" onmouseover="return overlib('Add a Job Plan Request', ABOVE, LEFT)"
										onclick="addrt();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/addnew.gif"
										runat="server"></td>
								<td colSpan="2">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table>
							<tr>
								<td class="label"><asp:Label id="lang1156" runat="server">Sort By</asp:Label></td>
								<td class="plainlabel"><asp:radiobuttonlist id="rbsort" runat="server" Width="420px" RepeatDirection="Horizontal" Height="20px"
										CssClass="plainlabel">
										<asp:ListItem Value="0" Selected="True">Job Reference#</asp:ListItem>
										<asp:ListItem Value="1">Request Date</asp:ListItem>
										<asp:ListItem Value="2">Complete Date</asp:ListItem>
										<asp:ListItem Value="3">Planner</asp:ListItem>
										<asp:ListItem Value="4">Status</asp:ListItem>
									</asp:radiobuttonlist></td>
								<td><IMG onmouseover="return overlib('Sort Ascending', ABOVE, LEFT)" onclick="sort('asc');"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/sasc.gif"></td>
								<td><IMG onmouseover="return overlib('Sort Descending', ABOVE, LEFT)" onclick="sort('desc');"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/sdesc.gif"></td>
							</tr>
						</table>
					</td>
				<tr>
					<td id="tdlist" colSpan="6" runat="server"></td>
				</tr>
				<tr>
					<td colspan="6" align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lblsid" type="hidden" name="lblsid" runat="server"> <input id="lblsubmit" type="hidden" name="lblsubmit" runat="server">
			<input id="lblsort" type="hidden" name="lblsort" runat="server"><input id="lbltyp" type="hidden" name="lbltyp" runat="server">
			<input id="lbleqid" type="hidden" name="lbleqid" runat="server"> <input id="lblfu" type="hidden" name="lblfu" runat="server">
			<input id="lblco" type="hidden" name="lblco" runat="server"><input id="lblret" type="hidden" name="lblret" runat="server">
			<input type="hidden" id="lblnewjpref" runat="server"> <input type="hidden" id="txtpg" runat="server" NAME="txtpg">
			<input type="hidden" id="txtpgcnt" runat="server" NAME="txtpgcnt"><input type="hidden" id="lblro" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
