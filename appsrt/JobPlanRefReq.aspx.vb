

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class JobPlanRefReq
    Inherits System.Web.UI.Page
	Protected WithEvents ovid165 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid164 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid163 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang1156 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1155 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1154 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim rl As New Utilities
    Dim list, sid, subm, jump, typ, eq, fu, co, ro, rostr As String
    Protected WithEvents lblnewjpref As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Dim sflag As Integer = 0
    Dim Tables As String = "pmjpref"
    Dim PK As String = "jpref"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String
    Dim intPgCnt, intPgNav As Integer
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents rbsort As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents imgadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdlist As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                imgadd.Attributes.Add("onclick", "")
            Else
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = rl.CheckROS(rostr, "jp")
                    lblro.Value = ro
                    If ro = "1" Then
                        imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                        imgadd.Attributes.Add("onclick", "")
                    End If
                End If
            End If
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            lblsort.Value = "asc"
            rbsort.SelectedValue = "0"
            txtpg.Value = "1"
            rl.Open()
            GetRequests()
            rl.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                rl.Open()
                GetNext()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                rl.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                typ = lbltyp.Value
                GetRequests()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                rl.Open()
                GetPrev()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                rl.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                typ = lbltyp.Value
                GetRequests()
                rl.Dispose()
                lblret.Value = ""
            End If
            subm = Request.Form("lblsubmit").ToString
            If subm = "srch" Then
                lblsubmit.Value = ""
                typ = "no"
                lbltyp.Value = "no"
                rl.Open()
                GetRequests()
                rl.Dispose()
            ElseIf subm = "sort" Then
                lblsubmit.Value = ""
                rl.Open()
                typ = lbltyp.Value
                GetRequests()
                rl.Dispose()
            ElseIf subm = "addref" Then
                lblsubmit.Value = ""
                rl.Open()
                AddRef()
                rl.Dispose()
            End If
        End If
    End Sub
    Private Sub AddRef()
        Dim ret As Integer
        Dim wonum As String = "None"
        Dim jpref As String = ""
        sql = "usp_addremjpref '" & wonum & "','" & jpref & "','add'"
        ret = rl.Scalar(sql)
        If ret = 1 Then
            Dim strMessage As String =  tmod.getmsg("cdstr479" , "JobPlanRefReq.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            lblnewjpref.Value = ret
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            typ = lbltyp.Value
            GetRequests()
        Catch ex As Exception
            rl.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr480" , "JobPlanRefReq.aspx.vb")
 
            rl.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            typ = lbltyp.Value
            GetRequests()
        Catch ex As Exception
            rl.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr481" , "JobPlanRefReq.aspx.vb")
 
            rl.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetRequests()
        Dim ret As String = lblret.Value
        Dim srch As String = txtsrch.Text
        sid = lblsid.Value
        Dim sortStr, sortVal, sortFilt As String
        sortStr = lblsort.Value
        sortVal = rbsort.SelectedValue.ToString
        If sortVal = "0" Then
            sortFilt = " order by r.jpref " & sortStr
            Sort = "jpref " & sortStr
        ElseIf sortVal = "1" Then
            sortFilt = " order by r.requestdate " & sortStr
            Sort = "requestdate " & sortStr
        ElseIf sortVal = "2" Then
            sortFilt = " order by r.compdate " & sortStr
            Sort = "compdate " & sortStr
        ElseIf sortVal = "3" Then
            sortFilt = " order by p.planner " & sortStr
            Sort = "plnrid " & sortStr
        ElseIf sortVal = "4" Then
            sortFilt = " order by r.status " & sortStr
            Sort = "status " & sortStr
        End If
        Filter = "status <> ''Complete'' "
        FilterCnt = "status <> 'Complete' "
        If Len(srch) > 0 Then
            srch = "%" & srch & "%"
            sql = "select r.jpref, Convert(char(10),r.requestdate,101) as requestdate, Convert(char(10),r.compdate,101) as compdate, r.status, r.jpnum, r.jpid, r.plnrid, p.username as 'name' from  pmjpref r " _
            + "left join pmsysusers p on p.userid = r.plnrid where r.jpref like '" & srch & "' " _
            + " or p.name like '" & srch & "' " & sortFilt

            Filter += "and jpref like ''" & srch & "''"
            FilterCnt += "and jpref like '" & srch & "'"
            Fields = "wonum, jpref, Convert(char(10),requestdate,101) as requestdate, Convert(char(10),compdate,101) " _
            + "as compdate, status, jpnum, jpid, plnrid, name = (select username from pmsysusers where userid = pmjpref.plnrid " _
            + "and username like ''" & srch & "'')"
        Else
            sql = "select r.jpref, Convert(char(10),r.requestdate,101) as requestdate, Convert(char(10),r.compdate,101) as compdate, r.status, r.wonum, r.jpnum, r.jpid, r.plnrid, p.username as 'name' from  pmjpref r " _
             + "left join pmsysusers p on p.userid = r.plnrid " & sortFilt

            'Filter = ""
            'FilterCnt = ""
            Fields = "wonum, jpref, Convert(char(10),requestdate,101) as requestdate, Convert(char(10),compdate,101) " _
           + "as compdate, status, jpnum, jpid, plnrid, name = (select username from pmsysusers where userid = pmjpref.plnrid)"
        End If
        Dim sb As StringBuilder = New StringBuilder

        sb.Append("<table width=""720"">")
        sb.Append("<tr><td colspan=""6""><div style=""OVERFLOW: auto; width: 720px; height: 360px;"">")
        sb.Append("<table width=""700"">")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""100"">" & tmod.getlbl("cdlbl121" , "JobPlanRefReq.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""100"">" & tmod.getlbl("cdlbl122" , "JobPlanRefReq.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""140"">" & tmod.getlbl("cdlbl123" , "JobPlanRefReq.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""100"">" & tmod.getlbl("cdlbl124" , "JobPlanRefReq.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""100"">" & tmod.getlbl("cdlbl125" , "JobPlanRefReq.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""130"">" & tmod.getlbl("cdlbl126" , "JobPlanRefReq.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""60"">" & tmod.getlbl("cdlbl127" , "JobPlanRefReq.aspx.vb") & "</td></tr>")
        Dim rowid As Integer = 0
        Dim bc, jr, wo, rd, cd, pl, st, jp, jpid, plnrid As String
        If FilterCnt <> "" Then
            sql = "select count(*) from pmjpref where " & FilterCnt
        Else
            sql = "select count(*) from pmjpref"
        End If
        'PageNumber = txtpg.Value
        'intPgCnt = rl.Scalar(sql)
        'intPgNav = rl.PageCount(intPgCnt, PageSize)
        'lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        'txtpgcnt.Value = intPgNav

        PageNumber = txtpg.Value
        intPgNav = rl.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav

        dr = rl.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        'dr = rl.GetRdrData(sql)
        While dr.Read
            If rowid = 0 Then
                rowid = 1
                bc = "transrow"
            Else
                rowid = 0
                bc = "transrowblue"
            End If
            jr = dr.Item("jpref").ToString
            wo = dr.Item("wonum").ToString
            rd = dr.Item("requestdate").ToString
            cd = dr.Item("compdate").ToString
            pl = dr.Item("name").ToString
            st = dr.Item("status").ToString
            jp = dr.Item("jpnum").ToString
            jpid = dr.Item("jpid").ToString
            plnrid = dr.Item("plnrid").ToString
            sb.Append("<tr><td class=""linklabel " & bc & """><a href=""#"" onclick=""rtret('" & jr & "', '" & wo & "','" & pl & "','" & jpid & "','" & plnrid & "');"">")
            sb.Append(jr & "</a></td>")
            sb.Append("<td class=""plainlabel " & bc & """>" & wo & "</td>")
            sb.Append("<td class=""plainlabel " & bc & """>" & jp & "</td>")
            sb.Append("<td class=""plainlabel " & bc & """>" & rd & "</td>")
            sb.Append("<td class=""plainlabel " & bc & """>" & cd & "</td>")
            sb.Append("<td class=""plainlabel " & bc & """>" & pl & "</td>")
            sb.Append("<td class=""plainlabel " & bc & """>" & st & "</td></tr>")


        End While
        dr.Close()
        sb.Append("</table></div></td></tr></table>")
        tdlist.InnerHtml = sb.ToString
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1154.Text = axlabs.GetASPXPage("JobPlanRefReq.aspx", "lang1154")
        Catch ex As Exception
        End Try
        Try
            lang1155.Text = axlabs.GetASPXPage("JobPlanRefReq.aspx", "lang1155")
        Catch ex As Exception
        End Try
        Try
            lang1156.Text = axlabs.GetASPXPage("JobPlanRefReq.aspx", "lang1156")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgadd.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("JobPlanRefReq.aspx", "imgadd") & "', ABOVE, LEFT)")
            imgadd.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid163.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("JobPlanRefReq.aspx", "ovid163") & "', ABOVE, LEFT)")
            ovid163.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid164.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("JobPlanRefReq.aspx", "ovid164") & "', ABOVE, LEFT)")
            ovid164.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid165.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("JobPlanRefReq.aspx", "ovid165") & "', ABOVE, LEFT)")
            ovid165.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
