<%@ Page Language="vb" AutoEventWireup="false" Codebehind="jpfreqlist.aspx.vb" Inherits="lucy_r12.jpfreqlist" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>jpfreqlist</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/jpfreqlistaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
         function getval(vid, val, vind) {
             document.getElementById("lblvid").value = vid;
             document.getElementById("lblval").value = val;
             document.getElementById("lblvind").value = vind;
             document.getElementById("lblsubmit").value = "saveit";
             document.getElementById("form1").submit();
         }
         function checkit() {
             var chk = document.getElementById("lblsubmit").value;
             var val = document.getElementById("lblval").value;
             var vid = document.getElementById("lblvid").value;
             if (chk == "go") {
                 val = vid + "," + val;
                 window.parent.getval(val);
             }
             else if (chk == "cgo") {
                 val = vid + "," + val;
                 window.parent.getval(val);
             }
         }
     </script>
	</HEAD>
	<body  onload="checkit();">
		<form id="form1" method="post" runat="server">
			<div id="divval" style="BORDER-BOTTOM: black 1px solid; POSITION: absolute; BORDER-LEFT: black 1px solid; WIDTH: 400px; HEIGHT: 400px; OVERFLOW: auto; BORDER-TOP: black 1px solid; TOP: 4px; BORDER-RIGHT: black 1px solid; LEFT: 4px"
				runat="server"></div>
			<input type="hidden" id="lblpmtskid" runat="server" NAME="lblpmtskid"> <input type="hidden" id="lblfld" runat="server" NAME="lblfld">
			<input type="hidden" id="lblsid" runat="server" NAME="lblsid"> <input type="hidden" id="lblvid" runat="server" NAME="lblvid">
			<input type="hidden" id="lblsubmit" runat="server" NAME="lblsubmit"> <input type="hidden" id="lblval" runat="server" NAME="lblval">
			<input type="hidden" id="lblvind" runat="server" NAME="lblvind"> <input type="hidden" id="lblwo" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
