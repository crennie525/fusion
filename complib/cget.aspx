<%@ Page Language="vb" AutoEventWireup="false" Codebehind="cget.aspx.vb" Inherits="lucy_r12.cget" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>cget</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="JavaScript" src="../scripts1/cgetaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table id="tblcclass" width="650" runat="server">
				<tr>
					<td class="plainlabelblue" colSpan="3"><input id="rbget" CHECKED type="radio" name="rb" runat="server"><asp:Label id="lang1661" runat="server">Look Up Sub Classes on Select</asp:Label><input id="rbnot" type="radio" name="rb" runat="server"><asp:Label id="lang1662" runat="server">Just Return Component Class</asp:Label></td>
				</tr>
				<tr>
					<td class="thdrsingrt label" colSpan="3"><asp:Label id="lang1663" runat="server">Component Class Lookup</asp:Label></td>
				</tr>
				<tr id="tbleqrec" runat="server">
					<td class="bluelabel" width="80"><asp:Label id="lang1664" runat="server">Search</asp:Label></td>
					<td width="310"><asp:textbox id="txtsrch" runat="server" Width="250px"></asp:textbox><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
					<td width="210"></td>
				</tr>
				<tr>
					<td id="tdcomp" colSpan="3" align="center" runat="server"></td>
				</tr>
				<tr>
					<td colSpan="6" align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table id="tblsclass" class="details" width="650" runat="server">
				<tr>
					<td colSpan="3" align="right"><IMG onclick="goback();" src="../images/appbuttons/minibuttons/candisk1.gif"></td>
				</tr>
				<tr>
					<td class="thdrsingrt label" colSpan="3"><asp:Label id="lang1665" runat="server">Sub Class Lookup</asp:Label></td>
				</tr>
				<tr>
					<td id="tdscomp" colSpan="3" align="center" runat="server"></td>
				</tr>
				<tr>
					<td colSpan="6" align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img1" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img2" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="Label1" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img3" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="Img4" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table id="tblreturn" class="details" width="650" runat="server">
				<tr>
					<td colSpan="3" align="right"><IMG onclick="goback2();" src="../images/appbuttons/minibuttons/candisk1.gif"><IMG onclick="saveit();" src="../images/appbuttons/minibuttons/savedisk1.gif"></td>
				</tr>
				<tr>
					<td class="thdrsingrt label" colSpan="3"><asp:Label id="lang1666" runat="server">Lookup Results</asp:Label></td>
				</tr>
				<tr>
					<td class="bluelabel" height="20" width="180"><asp:Label id="lang1667" runat="server">Selected Component Class</asp:Label></td>
					<td id="tdnewclass" class="plainlabel" width="180" runat="server"></td>
					<td width="290">&nbsp;</td>
				</tr>
				<tr>
					<td class="bluelabel" height="20"><asp:Label id="lang1668" runat="server">Selected Sub Class</asp:Label></td>
					<td id="tdnewsub" class="plainlabel" runat="server"></td>
				</tr>
				<tr>
					<td class="bluelabel" height="20"><asp:Label id="lang1669" runat="server">Sub Class Failure Modes</asp:Label></td>
					<td id="tdnewfm" class="plainlabel" runat="server"></td>
				</tr>
				<tr id="trreplace" runat="server">
					<td class="plainlabelblue" colSpan="2"><input id="rbcc" CHECKED type="radio" name="rb1" runat="server"><asp:Label id="lang1670" runat="server">Copy Failure Modes to Component</asp:Label><input id="rbdc" type="radio" name="rb1" runat="server"><asp:Label id="lang1671" runat="server">Do Not Copy Failure Modes</asp:Label></td>
					<td id="tdcopyfm" class="plainlabelred details" runat="server"><input id="cbcopyfm" type="checkbox" runat="server"><asp:Label id="lang1672" runat="server">Replace Current Failure Modes?</asp:Label></td>
				</tr>
			</table>
			<input id="lblret" type="hidden" name="lblret" runat="server"> <input id="txtpg" type="hidden" name="txtpg" runat="server">
			<input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server"> <input id="lblacid" type="hidden" runat="server">
			<input id="lblcclass" type="hidden" runat="server"> <input id="lblscid" type="hidden" runat="server">
			<input type="hidden" id="lblsclass" runat="server"><input type="hidden" id="lblfmcnt" runat="server">
			<input type="hidden" id="lblnosub" runat="server"><input type="hidden" id="lblnofm" runat="server">
			<input type="hidden" id="lblfm" runat="server"> <input type="hidden" id="lblcomid" runat="server">
			<input type="hidden" id="lbllib" runat="server"> <input type="hidden" id="lblsid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
