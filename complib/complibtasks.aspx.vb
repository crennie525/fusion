

'********************************************************
'*
'********************************************************




Imports System.Data.SqlClient
Public Class complibtasks
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tli, cid, sid, did, clid, eqid, funid, comid, tid, se, cell, sql, fuid, typ, lid, ro As String
    Dim Filter, AddVal, SubVal, FiltFilt, login, app As String
    Dim ds As DataSet
    Dim dr As SqlDataReader
    Dim gtasks As New Utilities
    Dim bgtasks As New Utilities
    Dim bgtasks2 As New Utilities
    Dim dslev As DataSet
    Dim dshd As DataSet
    Dim appc As New AppUtils
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents addtask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblcurrsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblitemindex As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtfreq As System.Web.UI.WebControls.TextBox
    Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings.Item("custAppUrl")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgtasks As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        xCoord.Value = 0
        yCoord.Value = 0


	GetDGLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            comid = Request.QueryString("comid").ToString
            lblcomid.Value = comid
            lblcid.Value = "0"
            lblsid.Value = "12"
            lblfilt.Value = "comid = '" & comid & "'"
            lblcurrsort.Value = "tasknum asc"
            gtasks.Open()
            BindGrid("tasknum asc")
            gtasks.Dispose()

        End If
    End Sub
    Private Sub BindGrid(ByVal sortExp As String)
        Try
            'gtasks.Open()
        Catch ex As Exception

        End Try
        Dim compnum As String
       
        Filter = lblfilt.Value
        FiltFilt = lblfiltfilt.Value
        sql = "select compnum from complib where " & Filter & FiltFilt
        compnum = gtasks.strScalar(sql)
        lblcompnum.Value = compnum

        sql = "select count(*) from comptasks where " & Filter & FiltFilt
        Try
            Dim tcnt As Integer = gtasks.Scalar(sql)
            lbltcnt.Value = tcnt
        Catch ex As Exception

        End Try

        sql = "update comptasks set lotoid = 0 where lotoid is null " _
        + "update comptasks set conid = 0 where conid is null"
        gtasks.Update(sql)
        sql = "select * from comptasks where " & Filter & FiltFilt & " order by " & sortExp
        ds = gtasks.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Try
            dgtasks.DataSource = dv
            dgtasks.DataBind()
        Catch ex As Exception

        End Try
        'gtasks.Dispose()
    End Sub
    Public Function PopulatePreTech() As DataSet
        cid = lblcid.Value
        sql = "select ptid, pretech, ptindex from pmPreTech where compid = '" & cid & "' or ptindex = '0' order by ptindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Public Function PopulateTaskTypes() As DataSet
        cid = lblcid.Value
        sql = "select ttid, tasktype, taskindex from pmTaskTypes where compid = '" & cid & "' or taskindex = '0' order by taskindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function

    Public Function PopulateStatus() As DataSet
        cid = lblcid.Value
        sql = "select statid, status, statusindex from pmStatus where compid = '" & cid & "' or statusindex = '0' order by statusindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    
    Public Function PopulateSkills() As DataTable
        cid = lblcid.Value
        sid = lblsid.Value
        Dim scnt, scnt2 As Integer
        sql = "select count(*) from pmSiteSkills where (compid = '" & cid & "' and siteid = '" & sid & "') or skillindex = '0'"
        Try
            scnt = gtasks.Scalar(sql)
        Catch ex As Exception
            bgtasks.Open()
            scnt = bgtasks.Scalar(sql)
            bgtasks.Dispose()
        End Try

        If scnt > 1 Then
            sql = "select count(*) from pmsiteskills where skill = 'Select' and compid = '" & cid & "'" ' and siteid = '" & sid & "'"
            Try
                scnt2 = gtasks.Scalar(sql)
            Catch ex As Exception
                bgtasks2.Open()
                scnt2 = bgtasks2.Scalar(sql)
                bgtasks2.Dispose()
            End Try
            sql = "select skillid, skill, skillindex from pmSiteSkills where (compid = '" & cid & "' and siteid = '" & sid & "') or skillindex = '0' order by skillindex"


            Try
                dslev = gtasks.GetDSData(sql)
                Dim dt As New DataTable
                dt = dslev.Tables(0)
                If scnt2 = 0 Then
                    Dim anyRow As DataRow = dt.NewRow
                    anyRow("skill") = "Select"
                    anyRow("skillid") = "0"
                    dt.Rows.InsertAt(anyRow, 0)
                End If
                Return dt
            Catch ex As Exception

            End Try
        Else
            sql = "select skillid, skill, skillindex from pmSkills where  compid = '" & cid & "' or skillindex = '0' order by skillindex"
            Try
                dslev = gtasks.GetDSData(sql)
                Dim dt As New DataTable
                dt = dslev.Tables(0)
                Return dt
            Catch ex As Exception

            End Try

        End If

    End Function
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer

        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function

    Private Sub addtask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles addtask.Click
        se = lblcurrsort.Value
        Dim tskcnt As Integer
        Filter = lblfilt.Value
        Dim subfilt As String
        subfilt = " and subtask = '0'"
        'AddVal = lbladdval.Value
        gtasks.Open()
        'sql = "select count(*) from comptasks where " & Filter & subfilt
        'tskcnt = gtasks.Scalar(sql)
        'tskcnt = tskcnt + 1
        tli = 5
        cid = lblcid.Value
        sid = lblsid.Value
        comid = lblcomid.Value
        Dim usr As String = "PM Admin" 'HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        sql = "usp_AddCompTask '" & cid & "', '" & comid & "', '" & ustr & "'"
        gtasks.Scalar(sql)
        BindGrid(se)
        gtasks.Dispose()
        lbladdchk.Value = "yes"
    End Sub

    Private Sub dgtasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.EditCommand
        se = lblcurrsort.Value
        lbloldtask.Value = CType(e.Item.FindControl("lblta"), Label).Text
        dgtasks.EditItemIndex = e.Item.ItemIndex
        gtasks.Open()
        BindGrid(se)
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.CancelCommand
        dgtasks.EditItemIndex = -1
        se = lblcurrsort.Value
        gtasks.Open()
        BindGrid(se)
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.DeleteCommand
        Dim tnum, snum
        'ListItemType.SelectedItem()
        Try
            tnum = CType(e.Item.FindControl("lblta"), Label).Text
            snum = CType(e.Item.FindControl("lblsubt"), Label).Text
        Catch ex As Exception
            tnum = CType(e.Item.FindControl("lblt"), TextBox).Text
            snum = CType(e.Item.FindControl("lblst"), Label).Text
        End Try
        'If e.Item.ItemType = ListItemType.Item Then

        'ElseIf e.Item.ItemType = ListItemType.EditItem Then

        'End If

        comid = lblcomid.Value
        sql = "usp_delCompTask '" & comid & "', '" & tnum & "', '" & snum & "'"
        gtasks.Open()
        gtasks.Update(sql)
       
        Try
            dgtasks.EditItemIndex = -1
        Catch ex As Exception

        End Try
        se = lblcurrsort.Value
        BindGrid(se)
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.UpdateCommand
        gtasks.Open()
        Dim otn, tn, ts, fre, tfre, ski, tski, sta, tsta, lot, tlot, con, tcon, tas, ttas, mai, tmai, pre, tpre, tskd, fuid As String

        comid = lblcomid.Value
        sid = lblsid.Value
        otn = lbloldtask.Value
        Try
            fre = ""
            tfre = txtfreq.text
            Dim frechk As Long
            Try
                frechk = System.Convert.ToInt32(tfre)
            Catch ex As Exception
                Dim strMessage As String = "Frequency Must be a Non Decimal Numeric Value"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        Catch ex As Exception
            fre = "0"
            tski = ""
        End Try

        Try
            ski = CType(e.Item.FindControl("ddskill"), DropDownList).SelectedItem.Value
            tski = CType(e.Item.FindControl("ddskill"), DropDownList).SelectedItem.Text
        Catch ex As Exception
            ski = "0"
            tski = "Select"
        End Try
        Try
            sta = CType(e.Item.FindControl("ddeqstat"), DropDownList).SelectedItem.Value
            tsta = CType(e.Item.FindControl("ddeqstat"), DropDownList).SelectedItem.Text
        Catch ex As Exception
            sta = "0"
            tsta = "Select"
        End Try
        Try
            lot = CType(e.Item.FindControl("ddloto"), DropDownList).SelectedIndex
        Catch ex As Exception
            lot = "0"
        End Try
        Try
            tlot = CType(e.Item.FindControl("ddloto"), DropDownList).SelectedItem.Text
        Catch ex As Exception
            tlot = "Select"
        End Try
        Try
            con = CType(e.Item.FindControl("ddcs"), DropDownList).SelectedIndex
        Catch ex As Exception
            con = "0"
        End Try
        Try
            tcon = CType(e.Item.FindControl("ddcs"), DropDownList).SelectedItem.Text
        Catch ex As Exception
            tcon = "Select"
        End Try
        Try
            tas = CType(e.Item.FindControl("ddtype"), DropDownList).SelectedItem.Value
            ttas = CType(e.Item.FindControl("ddtype"), DropDownList).SelectedItem.Text
        Catch ex As Exception
            tas = "0"
            ttas = "Select"
        End Try


        Dim ci, cin, cn As String
       
        Try
            pre = CType(e.Item.FindControl("ddpt"), DropDownList).SelectedItem.Value
        Catch ex As Exception
            pre = "0"
        End Try
        Try
            tpre = CType(e.Item.FindControl("ddpt"), DropDownList).SelectedItem.Text
        Catch ex As Exception
            tpre = "Select"
        End Try


        Dim txt, qty As String
        qty = CType(e.Item.FindControl("txtqty"), TextBox).Text
        txt = CType(e.Item.FindControl("txttr"), TextBox).Text
        If qty = "" Then
            qty = "0"
        End If
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToInt64(qty)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr696" , "complibtasks.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If txt = "" Then
            txt = "0"
        End If
        Dim txtchk As Long
        Try
            txtchk = System.Convert.ToDecimal(txt)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr697" , "complibtasks.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        tskd = CType(e.Item.FindControl("txtdesc"), TextBox).Text
        tskd = gtasks.ModString2(tskd)
        tn = CType(e.Item.FindControl("lblt"), TextBox).Text
        Dim tnchk As Long
        Try

            tnchk = System.Convert.ToInt64(tn)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr698" , "complibtasks.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If tn = 0 Or Len(tn) = 0 Then
            tn = otn
        End If
        If tn < 1 Then
            tn = 1
        End If
        Try
            Dim tcnt As Integer = lbltcnt.Value
            If tn > tcnt Then
                tn = tcnt
            End If
        Catch ex As Exception

        End Try
        cn = lblcompnum.Value

        ts = CType(e.Item.FindControl("lblst"), Label).Text
        Filter = lblfilt.Value
        Dim ttid As String
        ttid = CType(e.Item.FindControl("lblttid"), Label).Text

        Dim fm1, fm1s, fm2, fm2s, fm3, fm3s, fm4, fm4s, fm5, fm5s As String
        Dim ofm1, ofm2, ofm3, ofm4, ofm5 As String
        '+ "comid = '" & ci & "', " _
        '+ "compindex = '" & cin & "', " _
        '+ "compnum = '" & cn & "', " _

        Dim usr As String = "PM Admin" 'HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)

        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_updatecomptasksg @pmtskid, @des, @qty, @ttime, @rdid, @rd, " _
        + "@fre, @freq, @lotoid, @conid, @ttid, @tasktype, @comid, " _
        + "@compnum, @filter, " _
        + "@pagenumber, " _
        + "@ustr, @pt, @ptstr, @ski, @skistr"

        Dim param = New SqlParameter("@pmtskid", SqlDbType.Int)
        param.Value = ttid
        cmd.Parameters.Add(param)
        Dim param01 = New SqlParameter("@des", SqlDbType.VarChar)
        If tskd = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = tskd
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@qty", SqlDbType.Int)
        If qty = "" Then
            param02.Value = "1"
        Else
            param02.Value = qty
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@ttime", SqlDbType.Decimal)
        If txt = "" Then
            param03.Value = "0"
        Else
            param03.Value = txt
        End If
        cmd.Parameters.Add(param03)
        Dim param04 = New SqlParameter("@rdid", SqlDbType.Int)
        If sta = "" Then
            param04.Value = "0"
        Else
            param04.Value = sta
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@rd", SqlDbType.VarChar)
        If tsta = "" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = tsta
        End If
        cmd.Parameters.Add(param05)
        Dim param06 = New SqlParameter("@fre", SqlDbType.Int)
        If fre = "" Then
            param06.Value = "0"
        Else
            param06.Value = fre
        End If
        cmd.Parameters.Add(param06)
        Dim param07 = New SqlParameter("@freq", SqlDbType.VarChar)
        If tfre = "" Or tfre = "Select" Then
            param07.Value = System.DBNull.Value
        Else
            param07.Value = tfre
        End If
        cmd.Parameters.Add(param07)
        Dim param08 = New SqlParameter("@lotoid", SqlDbType.Int)
        If lot = "" Then
            param08.Value = "0"
        Else
            param08.Value = lot
        End If
        cmd.Parameters.Add(param08)
        Dim param09 = New SqlParameter("@conid", SqlDbType.Int)
        If con = "" Then
            param09.Value = "0"
        Else
            param09.Value = con
        End If
        cmd.Parameters.Add(param09)
        Dim param10 = New SqlParameter("@ttid", SqlDbType.Int)
        If tas = "" Then
            param10.Value = "0"
        Else
            param10.Value = tas
        End If
        cmd.Parameters.Add(param10)
        Dim param11 = New SqlParameter("@tasktype", SqlDbType.VarChar)
        If ttas = "" Or ttas = "Select" Then
            param11.Value = System.DBNull.Value
        Else
            param11.Value = ttas
        End If
        cmd.Parameters.Add(param11)
        Dim param12 = New SqlParameter("@comid", SqlDbType.Int)
        If comid = "" Then
            param12.Value = "0"
        Else
            param12.Value = comid
        End If
        cmd.Parameters.Add(param12)
        Dim param13 = New SqlParameter("@compnum", SqlDbType.VarChar)
        If cn = "" Or cn = "Select" Then
            param13.Value = System.DBNull.Value
        Else
            param13.Value = cn
        End If
        cmd.Parameters.Add(param13)


        Dim param18 = New SqlParameter("@filter", SqlDbType.VarChar)
        If Filter = "" Then
            param18.Value = System.DBNull.Value
        Else
            param18.Value = Filter
        End If
        cmd.Parameters.Add(param18)

        Dim param19 = New SqlParameter("@pagenumber", SqlDbType.Int)
        param19.Value = otn
        cmd.Parameters.Add(param19)

        Dim param48 = New SqlParameter("@ustr", SqlDbType.VarChar)
        If ustr = "" Then
            param48.Value = System.DBNull.Value
        Else
            param48.Value = ustr
        End If
        cmd.Parameters.Add(param48)


        Dim param21 = New SqlParameter("@pt", SqlDbType.Int)
        If pre = "" Then
            param21.Value = "0"
        Else
            param21.Value = pre
        End If
        cmd.Parameters.Add(param21)
        Dim param22 = New SqlParameter("@ptstr", SqlDbType.VarChar)
        If tpre = "" Or pre = "Select" Then
            param22.Value = System.DBNull.Value
        Else
            param22.Value = tpre
        End If
        cmd.Parameters.Add(param22)

        Dim param23 = New SqlParameter("@ski", SqlDbType.Int)
        If ski = "" Then
            param23.Value = "0"
        Else
            param23.Value = ski
        End If
        cmd.Parameters.Add(param23)
        Dim param24 = New SqlParameter("@skistr", SqlDbType.VarChar)
        If tski = "" Or tski = "Select" Then
            param24.Value = System.DBNull.Value
        Else
            param24.Value = tski
        End If
        cmd.Parameters.Add(param24)


        Dim tpm As Integer
        Try
            gtasks.UpdateHack(cmd)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr699" , "complibtasks.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        ' Check for task reorder
        If otn <> tn Then
            comid = lblcomid.Value
            sql = "usp_reorderCompTasks '" & ttid & "', '" & comid & "', '" & tn & "', '" & otn & "'"
            gtasks.Update(sql)
        End If
        sql = "usp_UpdateFM1 '" & ttid & "'"
        gtasks.Update(sql)

        dgtasks.EditItemIndex = -1
        se = lblcurrsort.Value
        BindGrid(se)
        gtasks.Dispose()
        
    End Sub

    Private Sub dgtasks_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.ItemCommand
        Dim vc As String = e.CommandName
        Select Case vc
            Case "hd"
                dgtasks.Columns(4).Visible = False
            Case "sd"
                dgtasks.Columns(4).Visible = True

            Case "htt"
                dgtasks.Columns(6).Visible = False
            Case "stt"
                dgtasks.Columns(6).Visible = True
            Case "hfr"
                dgtasks.Columns(8).Visible = False
            Case "sfr"
                dgtasks.Columns(8).Visible = True

            Case "hcomp"
                dgtasks.Columns(10).Visible = False
                dgtasks.Columns(11).Visible = False
            Case "scomp"
                dgtasks.Columns(10).Visible = True
                dgtasks.Columns(11).Visible = True

            Case "hsk"
                dgtasks.Columns(10).Visible = False
                dgtasks.Columns(11).Visible = False
                dgtasks.Columns(125).Visible = False
            Case "ssk"
                dgtasks.Columns(10).Visible = True
                dgtasks.Columns(11).Visible = True
                dgtasks.Columns(12).Visible = True

            Case "hpt"
                dgtasks.Columns(16).Visible = False
            Case "spt"
                dgtasks.Columns(16).Visible = True

            Case "hes"
                dgtasks.Columns(18).Visible = False
            Case "ses"
                dgtasks.Columns(18).Visible = True


            Case "hlo"
                dgtasks.Columns(20).Visible = False
            Case "slo"
                dgtasks.Columns(20).Visible = True
            Case "hco"
                dgtasks.Columns(22).Visible = False
            Case "sco"
                dgtasks.Columns(22).Visible = True
        End Select
    End Sub

    Private Sub dgtasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtasks.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                         e.Item.ItemType <> ListItemType.Footer Then
            Dim deleteButton As ImageButton = CType(e.Item.FindControl("ibDel"), ImageButton)

            'We can now add the onclick event handler
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete Task #" & _
            DataBinder.Eval(e.Item.DataItem, "tasknum") & " - Sub Task #" & _
            DataBinder.Eval(e.Item.DataItem, "subtask") & "?')"

            Dim tasknum As Label = CType(e.Item.FindControl("lblta"), Label)
            Dim subtask As Label = CType(e.Item.FindControl("lblsubt"), Label)
            Dim subflg As String = DataBinder.Eval(e.Item.DataItem, "subtask").ToString
            If subflg <> "0" Then
                'tasknum.Attributes.Add("class", "graylabel")
                tasknum.Visible = False
                subtask.Attributes.Add("class", "graylabel")
            End If
        End If
        Dim subchk As String

        Try
            If e.Item.ItemType = ListItemType.EditItem Then
                

                lblcurrtask.Value = CType(e.Item.FindControl("lblttid"), Label).Text.ToString
                lblitemindex.Value = e.Item.ItemIndex

                'CType(e.Item.FindControl("ddcomp"), DropDownList).Attributes.Add("onchange", "updatecomp(this.value, this.selectedIndex, this.options[this.selectedIndex].text)")
                'CType(e.Item.FindControl("ibToTask"), ImageButton).Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov190" , "complibtasks.aspx.vb") & "')")
                'CType(e.Item.FindControl("ibToTask"), ImageButton).Attributes.Add("onmouseout", "return nd()")
                'CType(e.Item.FindControl("ibReuse"), ImageButton).Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov191" , "complibtasks.aspx.vb") & "')")
                'CType(e.Item.FindControl("ibReuse"), ImageButton).Attributes.Add("onmouseout", "return nd()")
                'CType(e.Item.FindControl("ibFromTask"), ImageButton).Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov192" , "complibtasks.aspx.vb") & "')")
                'CType(e.Item.FindControl("ibFromTask"), ImageButton).Attributes.Add("onmouseout", "return nd()")
            End If
        Catch ex As Exception

        End Try

        Try
            subchk = CType(e.Item.FindControl("lblst"), Label).Text
            If subchk <> "0" Then
                CType(e.Item.FindControl("ibast"), ImageButton).Enabled = False
                CType(e.Item.FindControl("lblt"), TextBox).Enabled = False
            End If
        Catch ex As Exception

        End Try
    End Sub
	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgtasks.Columns(0).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","0")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(1).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","1")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(2).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","2")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(3).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","3")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(4).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","4")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(5).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","5")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(6).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","6")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(7).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","7")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(8).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","8")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(9).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","9")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(10).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","10")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(11).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","11")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(13).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","13")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(14).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","14")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(15).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","15")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(16).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","16")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(17).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","17")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(20).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","20")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(21).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","21")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(22).HeaderText = dlabs.GetDGPage("complibtasks.aspx","dgtasks","22")
		Catch ex As Exception
		End Try

	End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                addtask.Attributes.Add("src", "../images2/eng/bgbuttons/addtask.gif")
            ElseIf lang = "fre" Then
                addtask.Attributes.Add("src", "../images2/fre/bgbuttons/addtask.gif")
            ElseIf lang = "ger" Then
                addtask.Attributes.Add("src", "../images2/ger/bgbuttons/addtask.gif")
            ElseIf lang = "ita" Then
                addtask.Attributes.Add("src", "../images2/ita/bgbuttons/addtask.gif")
            ElseIf lang = "spa" Then
                addtask.Attributes.Add("src", "../images2/spa/bgbuttons/addtask.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
