<%@ Page Language="vb" AutoEventWireup="false" Codebehind="complibtasks2.aspx.vb" Inherits="lucy_r12.complibtasks2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>complibtasks2</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="javascript" src="../scripts/pmtaskdivfunceditco_1016a.js"></script>
		<script language="JavaScript" src="../scripts1/complibtasks2aspx_1.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="JavaScript" type="text/javascript">
		<!--
         function GetType(app, sval) {
             //alert(app + ", " + sval)
             var typlst;
             var sstr;
             var ssidx;
             var ssti;
             if (app == "d") {
                 typlst = document.getElementById("ddtype");
                 ptlst = document.getElementById("ddpt");
                 sstr = typlst.options[typlst.selectedIndex].text
                 ssidx = typlst.value
                 document.getElementById("lblsstr").value = sstr;
                 document.getElementById("lblssidx").value = ssidx;
                 ssti = document.getElementById("ddtype").value;
                 //sstr == "4 - Cond Monitoring"
                 if (ssti == "7") {
                     document.getElementById("ddpt").disabled = false;
                 }
                 else {
                     ptlst.value = "0";
                     document.getElementById("ddpt").disabled = true;
                 }
             }
             else if (app == "o") {
                 document.getElementById("ddtype").value = sval;
                 typlst = document.getElementById("ddtypeo");
                 ptlst = document.getElementById("ddpt");
                 ptolst = document.getElementById("ddpto");
                 sstr = typlst.options[typlst.selectedIndex].text;
                 ssti = document.getElementById("ddtypeo").value;
                 if (ssti == "7") {
                     document.getElementById("ddpt").disabled = false;
                     document.getElementById("ddpto").disabled = false;
                 }
                 else {
                     ptlst.value = "0";
                     ptolst.value = "0";
                     document.getElementById("ddpt").disabled = true;
                     document.getElementById("ddpto").disabled = true;
                 }
             }
         }
         var oflag;

         function page_maint(id) {
             populateArrays();
             document.getElementById("lblsvchk").value = "0";
             if (id == "o") {
                 oflag = "1";
             }
             else {
                 oflag = "0"
             }
             checkEnable();
         }

         function optchk() {
             populateArrays();
             document.getElementById("lblsvchk").value = "0";
             checkEnable();
         }
         function doEnable() {
             if (document.getElementById("lblpg").innerHTML != "0") {
                 //alert(document.getElementById("lblenable").value)
                 document.getElementById("lblenable").value = "0";

                 document.getElementById("cbfixed").disabled = false;

                 //document.getElementById("ddcomp.disabled=false;
                 document.getElementById("lbfaillist").disabled = false;
                 document.getElementById("lbfailmodes").disabled = false;
                 document.getElementById("lbCompFM").disabled = false;
                 document.getElementById("txtcQty").disabled = false;
                 document.getElementById("ibReuse").disabled = false;
                 document.getElementById("ibToTask").disabled = false;
                 document.getElementById("ibFromTask").disabled = false;
                 document.getElementById("txtdesc").disabled = false;
                 document.getElementById("ddtype").disabled = false;
                 document.getElementById("txtfreq").disabled = false;
                 document.getElementById("txtpfint").disabled = false;
                 document.getElementById("txtqty").disabled = false;
                 document.getElementById("txttr").disabled = false;
                 document.getElementById("txtrdt").disabled = false;
                 //document.getElementById("ddpt.disabled=false;
                 var sstr;
                 var ssti;
                 var typlst;
                 typlst = document.getElementById("ddtype");
                 sstr = typlst.options[typlst.selectedIndex].text
                 ssti = document.getElementById("ddtype").value;
                 if (sstr == "7") {
                     document.getElementById("ddpt").disabled = false;
                     try {
                         document.getElementById("ddpto").disabled = false;
                     }
                     catch (err) {

                     }
                 }
                 document.getElementById("ddeqstat").disabled = false;
                 document.getElementById("ddskill").disabled = false;
                 document.getElementById("cbloto").disabled = false;

                 document.getElementById("cbcs").disabled = false;

                 //document.getElementById("btnaddtsk").disabled=false;
                 //document.getElementById("btnaddsubtask").disabled=false;
                 try {
                     document.getElementById("btndeltask").disabled = false;
                 }
                 catch (err) {
                     //do nothing - button not used in optimizer
                 }

                 document.getElementById("ibCancel").disabled = false;
                 //document.getElementById("btnsavetask").disabled=false;
                 /*
                 if (oflag == "1") {
                     document.getElementById("lbofailmodes.disabled = false;
                     document.getElementById("txtodesc.disabled = false;
                     document.getElementById("ddtypeo.disabled = false;
                     
                     document.getElementById("txtqtyo.disabled = false;
                     document.getElementById("txttro.disabled = false;
                     document.getElementById("txtordt.disabled = false;
                     document.getElementById("ddpto.disabled = false;
                     document.getElementById("ddeqstato.disabled = false;
                     document.getElementById("ddskillo.disabled = false;
                     document.getElementById("ddtaskstat.disabled = false;
                 }
                 */
             }
             else {
                 alert("No Task Records Are Loaded Yet!")
             }
         }
         function jumptpm() {
             funid = document.getElementById("lblfuid").value;
             comid = document.getElementById("lblco").value;
             
         }
         function editadj(typ) {
             ////window.parent.setref();
             var chken = document.getElementById("lblenable").value;
             ptid = document.getElementById("lbltaskid").value;
             var tpmhold = document.getElementById("lbltpmhold").value;
             if (chken != "1") {
                 if (ptid != "" && tpmhold != "1") {
                     var fuid = document.getElementById("lblfuid").value;
                     var tid = document.getElementById("lbltaskid").value;
                     var ro = document.getElementById("lblro").value;
                     if (typ == "pre") {
                         tid = 0;
                     }
                     var eReturn = window.showModalDialog("../appsman/PMSetAdjManDialog.aspx?fuid=" + fuid + "&typ=" + typ + "&tid=" + tid + "&ro=" + ro, "", "dialogHeight:520px; dialogWidth:930px; resizable=yes");
                     if (eReturn) {

                     }
                 }
             }
         }
         function DisableButton(b) {
             document.getElementById("ibToTask").className = "details";
             document.getElementById("ibFromTask").className = "details";
             //document.getElementById("ibfromo").className = "details";
             //document.getElementById("ibtoo").className = "details";
             document.getElementById("ibReuse").className = "details";
             document.getElementById("todis").className = "view";
             document.getElementById("fromdis").className = "view";
             //document.getElementById("todis1").className = "view";
             //document.getElementById("fromdis1").className = "view";
             document.getElementById("fromreusedis").className = "view";
             document.getElementById("form1").submit();
         }
         function FreezeScreen(msg) {
             scroll(0, 0);
             var outerPane = document.getElementById('FreezePane');
             var innerPane = document.getElementById('InnerFreezePane');
             if (outerPane) outerPane.className = 'FreezePaneOn';
             if (innerPane) innerPane.innerHTML = msg;
         }
         function checkdeltask() {
             var chken = document.getElementById("lblenable").value;
             if (chken != "1") {
                 tid = document.getElementById("lbltaskid").value;
                 var tpmhold = document.getElementById("lbltpmhold").value;
                 if (tid != "" && tid != "0" && tpmhold != "1") {
                     var deltask = confirm("Are you sure you want to Delete this Task?");
                     if (deltask == true) {
                         document.getElementById("lblcompchk").value = "7";
                         FreezeScreen('Your Data Is Being Processed...');
                         document.getElementById("form1").submit();
                     }
                     else if (deltask == false) {
                         return false;
                         alert("Action Cancelled")
                     }
                 }
             }
         }
         function checkdeltasktpm() {
             var chken = document.getElementById("lblenable").value;
             //if(chken!="1") {
             tid = document.getElementById("lbltaskid").value;
             var tpmhold = document.getElementById("lbltpmhold").value;
             //alert(tid + "," + tpmhold)
             if (tid != "" && tid != "0" && tpmhold != "0") {
                 //var deltask = confirm("Are you sure you want to Delete this Task from\nthe TPM Module and Enable Editing?");
                 var deltask = confirm("Removing this task from TPM will result in the loss of images and scheduling data.\nDo you want to continue?");
                 if (deltask == true) {
                     document.getElementById("lblcompchk").value = "deltpm";
                     FreezeScreen('Your Data Is Being Processed...');
                     document.getElementById("form1").submit();
                 }
                 else if (deltask == false) {
                     return false;
                     alert("Action Cancelled")
                 }
             }
             //}
         }
         function CheckChanges() {
             for (var i = 0; i < values.length; i++) {
                 var elem = document.getElementById(ids[i]);
                 if (elem)
                     if ((elem.type == 'checkbox' || elem.type == 'radio')
                  && values[i] != elem.checked)
                         var change = confirm("You have made changes to a Revised Task with a Task Status of Delete.\nAre you sure you want to save these changes?");
                     else if (!(elem.type == 'checkbox' || elem.type == 'radio') &&
                  elem.value != values[i])
                         var change = confirm("You have made changes to a Revised Task with a Task Status of Delete.\nAre you sure you want to save these changes?");

             }
             if (change == true) {
                 //document.getElementById("submit("btnNext_Click", '')
                 //return true;
                 document.getElementById("lblcompchk").value = "3";
                 FreezeScreen('Your Data Is Being Processed...');
                 document.getElementById("form1").submit();
             }
             else if (change == false) {
                 //document.getElementById("lblsvchk").value = "0";
                 return false;
             }
         }
         function valpgnums() {
             if (document.getElementById("lblenable").value != "1") {
                 var tpmhold = document.getElementById("lbltpmhold").value;
                 if (tpmhold != "1") {
                     var desc = document.getElementById("txtdesc").innerHTML;
                     //var odesc = document.getElementById("txtodesc").innerHTML;
                     if (desc.length > 500) {
                         alert("Maximum Lenth for Task Description is 500 Characters")
                     }
                     
                     
                     else if (isNaN(document.getElementById("txtqty").value)) {
                         alert("Revised Skill Qty is Not a Number")
                     }
                    
                     else if (isNaN(document.getElementById("txttr").value)) {
                         alert("Revised Skill Time is Not a Number")
                     }
                    
                     else if (isNaN(document.getElementById("txtrdt").value)) {
                         alert("Revised Running/Down Time is Not a Number")
                     }
                     
                     else {
                         document.getElementById("lblcompchk").value = "3";
                         FreezeScreen('Your Data Is Being Processed...');
                         document.getElementById("form1").submit();
                     }

                 }
             }
         }
         function GetGrid() {
             handleapp();
             document.getElementById("lblgrid").value = "yes";
             chk = document.getElementById("lblchk").value;
             cid = document.getElementById("lblcid").value;
             tid = document.getElementById("lbltaskid").value;
             funid = document.getElementById("lblfuid").value;
             comid = document.getElementById("lblco").value;
             clid = document.getElementById("lblclid").value;
             typ = document.getElementById("lbltyp").value;
             lid = document.getElementById("lbllid").value;
             pmid = "none"//document.getElementById("lbldocpmid").value;
             var ro = document.getElementById("lblro").value;
             if (pmid == "") pmid = "none";
             pmstr = document.getElementById("lbldocpmstr").Value
             if (pmstr == "") pmstr = "none";

             if (tid != "" || tid != "0") {
                 //alert("../apps/GTasksFunc.aspx?app=opt&tli=5a&funid=" + funid + "&comid=no&cid=" + cid + "&chk=" + chk + "&pmid=" + pmid + "&pmstr=" + pmstr + "&lid=" + lid + "&typ=" + typ + "&date=" + Date())
                 window.open("../apps/GTasksFunc2.aspx?app=opt&tli=5a&funid=" + funid + "&comid=no&cid=" + cid + "&chk=" + chk + "&pmid=" + pmid + "&pmstr=" + pmstr + "&lid=" + lid + "&typ=" + typ + "&date=" + Date() + "&ro=" + ro, target = "_top");
             }
             else {
                 alert("No Task Records Selected Yet!")
             }
         }

         function checkit() {
             var tnum = document.getElementById("lblpg").innerHTML;
             ////window.parent.handletnum(tnum);

             var log = document.getElementById("lbllog").value;
             if (log == "no") {
                 ////window.parent.doref();
             }
             else {
                 ////window.parent.setref();
             }
             //var chk = document.getElementById("lblusetot").value;
             //if (chk == "1") {
             //    document.getElementById("ddskillo").disabled = true;
             //    document.getElementById("txttro").disabled = true;
             //    
             //    document.getElementById("ddeqstato").disabled = true;
             //}
             populateArrays();
             var start = document.getElementById("lblstart").value;
             if (start == "yes1") {
                 GetNavGrid();
             }
             document.getElementById("lblstart").value = "no";

             //var grid = document.getElementById("lblgrid").value;
             //if (grid == "yes") {
             //    window.location.reload(false);
             //    document.getElementById("submit();
             //}
             var tpm = document.getElementById("lbltpmalert").value;
             var tpmhold = document.getElementById("lbltpmhold").value; //&&tpmhold!="1"
             if (tpm == "yes") {
                 var decision = confirm("This Task Meets Your Requirements for TPM\nDo You Want to Transfer this Task to TPM?")
                 if (decision == true) {
                     //document.getElementById("submit('ddcomp', '');
                     FreezeScreen('Your Data Is Being Processed...');
                     document.getElementById("lblcompchk").value = "uptpm";
                     document.getElementById("form1").submit();
                 }
                 else {
                     //document.getElementById("ddcomp").value = goback;

                     alert("Action Cancelled")
                 }
             }
             var chk = document.getElementById("lblpar").value;
             if (chk == "task") {
                 valu = document.getElementById("lbltaskid").value;
                 var num = document.getElementById("lblt").innerHTML;
                 //alert(num)
                 ////window.parent.handletask(chk, valu, num);
             }
             else if (chk == "comp") {
                 //alert(document.getElementById("lblco").value)
                 cvalu = document.getElementById("lblco").value;
                 tvalu = document.getElementById("lbltaskid").value;
                 tnum = document.getElementById("lblt").value;
                 //alert(tnum)
                 ////window.parent.handleco(chk, cvalu, tvalu, tnum);
             }
             page_maint('d');
         }
         function setref() {
             ////window.parent.setref();
         }
         function GetNavGrid() {
             handleapp();
             cid = document.getElementById("lblcid").value;
             sid = document.getElementById("lblsid").value;
             did = document.getElementById("lbldid").value;
             clid = document.getElementById("lblclid").value;
             eqid = document.getElementById("lbleqid").value;
             chk = document.getElementById("lblchk").value;
             fuid = document.getElementById("lblfuid").value;
             tcnt = document.getElementById("lblcnt").value;
             typ = document.getElementById("lbltyp").value;
             lid = document.getElementById("lbllid").value;
             if (tcnt != "0") {
                 ////window.parent.handletasks("PMOptTasksGrid.aspx?start=yes&tl=5&tc=0&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&typ=" + typ + "&lid=" + lid + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:800px")
                 //if (eReturn) {
                 //	document.getElementById("lblsvchk").value = "7";
                 //	document.getElementById("pgflag").value = eReturn;
                 //	document.getElementById("submit();
                 //}
             }
         }
         function tasklookup() {

             var chken = document.getElementById("lblenable").value;
             var ro = document.getElementById("lblro").value;
             var tpmhold = document.getElementById("lbltpmhold").value;
             if ((chken != "1" || ro == "1") && tpmhold != "1") {
                 ////window.parent.setref();
                 var str = document.getElementById("txtdesc").innerHTML;
                 var eReturn = window.showModalDialog("../apps/TaskLookup.aspx?str=" + str, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes");
                 //alert(eReturn)
                 if (eReturn != "no" && eReturn != "~none~") {
                     document.getElementById("txtdesc").innerHTML = eReturn;
                 }
                 else if (eReturn != "~none~") {
                     document.getElementById("form1").submit();
                 }
             }
         }
         function jumpto() {

             var chken = document.getElementById("lblenable").value;
             var ro = document.getElementById("lblro").value;
             var tpmhold = document.getElementById("lbltpmhold").value;
             if ((chken != "1" || ro == "1") && tpmhold != "1") {
                 ////window.parent.setref();
                 var typ = document.getElementById("ddtype").value;
                 var pdm = document.getElementById("ddpt").value;
                 var tid = document.getElementById("lbltaskid").value;
                 var sid = document.getElementById("lblsid").value;
                 var ro = document.getElementById("lblro").value;
                 var eReturn = window.showModalDialog("../apps/commontasksdialog.aspx?typ=" + typ + "&pdm=" + pdm + "&tid=" + tid + "&sid=" + sid + "&ro=" + ro, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes");
                 if (eReturn) {
                     if (eReturn == "task") {
                         document.getElementById("lblcompchk").value = "6";
                         //document.getElementById("lbltaskholdid").value = eReturn;
                         document.getElementById("form1").submit();
                     }
                 }
             }
         }
         var popwin = "directories=0,height=500,width=800,location=0,menubar=0,resizable=0,status=0,toolbar=0,scrollbars=1";
         var poprep = "directories=0,height=600,width=900,location=0,menubar=0,resizable=0,status=0,toolbar=0,scrollbars=1";
         var poprepsm = "directories=0,height=390,width=300,location=0,menubar=0,resizable=0,status=0,toolbar=0,scrollbars=1";

         function getValueAnalysis() {

             tl = document.getElementById("lbltasklev").value;
             cid = document.getElementById("lblcid").value;
             sid = document.getElementById("lblsid").value;
             did = document.getElementById("lbldid").value;
             clid = document.getElementById("lblclid").value;
             eqid = document.getElementById("lbleqid").value;
             //alert(eqid)
             chk = document.getElementById("lblchk").value;
             fuid = document.getElementById("lblfuid").value;
             if (fuid != "") {
                 ////window.parent.setref();
                 var ht = "2000"; //screen.Height - 20;
                 var wd = "1000"; //screen.Width - 20;
                 var eReturn = window.showModalDialog("../reports/reportdialog.aspx?who=pmo&tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:1000px; dialogWidth:2000px;resizable=yes");
                 //var eReturn = window.showModalDialog("../reports/reportdialog.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:610px; dialogWidth:610px");
                 if (eReturn) {
                     if (eReturn != "ret") {
                         document.getElementById("lblsvchk").value = "7";
                         document.getElementById("pgflag").value = eReturn;
                         //alert(eReturn)
                         document.getElementById("form1").submit();
                     }
                 }
             }
             else {
                 alert("No Task Records Selected Yet!")
             }
         }
         function GetLubeDiv() {

             var ro = document.getElementById("lblro").value;
             var lock = document.getElementById("lbllock").value;
             var tpmhold = document.getElementById("lbltpmhold").value;
             if (lock != "1" && tpmhold != "1") {
                 ////window.parent.setref();
                 handleapp();
                 cid = document.getElementById("lblcid").value;
                 ptid = document.getElementById("lbltaskid").value;
                 tnum = document.getElementById("lblpg").innerHTML;
                 if (ptid != "") {
                     window.open("../inv/optTaskLubeList.aspx?ptid=" + ptid + "&cid=" + cid + "&tnum=" + tnum + "&ro=" + ro, "repWin", popwin);
                     document.getElementById("form1").submit();
                 }
             }
         }
         function GetPartDiv() {

             var ro = document.getElementById("lblro").value;
             var lock = document.getElementById("lbllock").value;
             var tpmhold = document.getElementById("lbltpmhold").value;
             if (lock != "1" && tpmhold != "1") {
                 ////window.parent.setref();
                 handleapp();
                 cid = document.getElementById("lblcid").value;
                 ptid = document.getElementById("lbltaskid").value;
                 tnum = document.getElementById("lblpg").innerHTML;
                 //alert(tnum)
                 if (ptid != "") {
                     window.open("../inv/optTaskPartList.aspx?ptid=" + ptid + "&cid=" + cid + "&tnum=" + tnum + "&ro=" + ro, "repWin", popwin);
                     document.getElementById("form1").submit();
                 }
             }
         }
         function GetSpareDiv() {
             handleapp();
             var ro = document.getElementById("lblro").value;
             var lock = document.getElementById("lbllock").value;
             var tpmhold = document.getElementById("lbltpmhold").value;
             if (lock != "1" && tpmhold != "1") {
                 ////window.parent.setref();

                 eqid = document.getElementById("lbleqid").value;
                 cid = document.getElementById("lblcid").value;
                 ptid = document.getElementById("lbltaskid").value;
                 tnum = document.getElementById("lblpg").innerHTML;
                 if (ptid != "") {
                     window.open("../inv/sparepartpick.aspx?eqid=" + eqid + "&oflag=r&ptid=" + ptid + "&cid=" + cid + "&tnum=" + tnum + "&ro=" + ro, "repWin", popwin);
                     document.getElementById("form1").submit();
                 }
             }
         }
         function GetToolDiv() {

             var ro = document.getElementById("lblro").value;
             var lock = document.getElementById("lbllock").value;
             var tpmhold = document.getElementById("lbltpmhold").value;
             if (lock != "1" && tpmhold != "1") {
                 ////window.parent.setref();
                 handleapp();
                 cid = document.getElementById("lblcid").value;
                 ptid = document.getElementById("lbltaskid").value;
                 tnum = document.getElementById("lblpg").innerHTML;
                 if (ptid != "") {
                     window.open("../inv/optTaskToolList.aspx?ptid=" + ptid + "&cid=" + cid + "&tnum=" + tnum + "&ro=" + ro, "repWin", popwin);
                     document.getElementById("form1").submit();
                 }
             }
         }
         function GetNoteDiv() {

             var ro = document.getElementById("lblro").value;
             var lock = document.getElementById("lbllock").value;
             var tpmhold = document.getElementById("lbltpmhold").value;
             if (lock != "1" && tpmhold != "1") {
                 ////window.parent.setref();
                 ptid = document.getElementById("lbltaskid").value;
                 if (ptid != "") {
                     window.open("../apps/TaskNotes.aspx?ptid=" + ptid + "&ro=" + ro, "repWin", popwin);
                 }
             }
         }


         function GetRationale() {

             handleapp();
             tnum = document.getElementById("lblpg").innerHTML;
             tid = document.getElementById("lbltaskid").value;
             did = document.getElementById("lbldid").value;
             clid = document.getElementById("lblclid").value;
             eqid = document.getElementById("lbleqid").value;
             fuid = document.getElementById("lblfuid").value;
             coid = document.getElementById("lblco").value;
             sav = document.getElementById("lblsave").value;
             ro = document.getElementById("lblro").value;
             var tpmhold = document.getElementById("lbltpmhold").value;
             if (tid == "") {
                 alert("No Task Records Seleted!")
             }
             else if (sav == "no") {
                 alert("Task Record Is Not Saved!")
             }
             else {
                 if (tpmhold != "1") {
                     ////window.parent.setref();
                     var eReturn = window.showModalDialog("PMRationaleDialog.aspx?tnum=" + tnum + "&tid=" + tid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:650px; dialogWidth:850px; resizable=yes;");
                     if (eReturn) {
                         document.getElementById("form1").submit();
                     }
                 }
             }
         }

         function getsgrid() {

             handleapp();
             var tid = document.getElementById("lblpg").innerHTML; //document.getElementById("lbltaskid").value;
             var fuid = document.getElementById("lblfuid").value;
             var coid = document.getElementById("lblcoid").value;
             var cid = document.getElementById("lblcid").value;
             var sid = document.getElementById("lblsid").value;
             var sav = document.getElementById("lblsave").value;
             var eqid = document.getElementById("lbleqid").value;
             var tpmhold = document.getElementById("lbltpmhold").value;
             //alert(tid)
             if (tid == "" || tid == "0") {
                 alert("No Task Records Seleted!")
             }
             else if (sav == "no") {
                 alert("Task Record Is Not Saved!")
             }
             else {
                 if (tpmhold != "1") {
                     ////window.parent.setref();
                     var eReturn = window.showModalDialog("csubdialog.aspx?sid=" + sid + "&coid=" + coid + "&cid=" + cid + "&tid=" + tid + "&fuid=" + fuid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:680px; dialogWidth:850px; resizable=yes");
                     if (eReturn) {
                         document.getElementById("lblcompchk").value = "5"
                         document.getElementById("form1").submit();
                     }
                 }
             }
         }

         function OpenFile(newstr, type) {
             handleapp();
             parent.parent.main.location.href = "doc.aspx?file=" + newstr + "&type=" + type;

         }
         function GetFuncDiv() {
             ////window.parent.setref();
             handleapp();
             cid = document.getElementById("lblcid").value;
             eqid = document.getElementById("lbleqid").value;
             var eReturn = window.showModalDialog("../equip/FuncDialog.aspx?cid=" + cid + "&eqid=" + eqid, "", "dialogHeight:250px; dialogWidth:600px; resizable=yes");
             if (eReturn) {
                 document.getElementById("lblpchk").value = "func";
                 document.getElementById("form1").submit();
             }
         }

         function GetCompDiv() {

             handleapp();
             var chken = document.getElementById("lblenable").value;
             fuid = document.getElementById("lblfuid").value;
             tid = document.getElementById("lblpg").innerHTML;
             var tpmhold = document.getElementById("lbltpmhold").value;
             //alert(tid)
             if (fuid.length != 0 && fuid != "" && fuid != "0" && tpmhold != "1") {
                 ////window.parent.setref();
                 //if(tid!=""&&tid!="0") {
                 cid = document.getElementById("lblcid").value;
                 eqid = document.getElementById("lbleqid").value;
                 fuid = document.getElementById("lblfuid").value;
                 ptid = document.getElementById("lbltaskid").value;
                 tli = document.getElementById("lbltasklev").value;
                 chk = document.getElementById("lblchk").value;
                 ro = document.getElementById("lblro").value;
                 var eReturn = window.showModalDialog("../equip/CompDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&tli=" + tli + "&chk=" + chk + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:800px; resizable=yes");
                 if (eReturn == "log") {
                     //window.parent.handlelogout();
                 }
                 else if (eReturn != "no") {
                     document.getElementById("lblcompchk").value = eReturn;
                 }
                 document.getElementById("form1").submit();
                 //}
             }
         }
         function getss() {
             //window.parent.setref();
             cid = document.getElementById("lblcid").value;
             sid = document.getElementById("lblsid").value;
             ro = document.getElementById("lblro").value;
             var eReturn = window.showModalDialog("../admin/pmSiteSkillsDialog.aspx?sid=" + sid + "&cid=" + cid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:520px; dialogWidth:470px; resizable=yes");
             if (eReturn) {
                 document.getElementById("lblcompchk").value = "4";
                 document.getElementById("form1").submit()
             }
         }
         function GetCompCopy() {
             //handleapp();
             var chken = document.getElementById("lblenable").value;
             ptid = document.getElementById("lbltaskid").value;
             ro = document.getElementById("lblro").value;
             var tpmhold = document.getElementById("lbltpmhold").value;
             var fuid = document.getElementById("lblfuid").value;
             if (fuid.length != 0 && fuid != "" && fuid != "0" && tpmhold != "1") {
                 //window.parent.setref();
                 fuid = document.getElementById("lblfuid").value;
                 if (fuid.length != 0 || fuid != "" || fuid != "0") {
                     cid = document.getElementById("lblcid").value;
                     sid = document.getElementById("lblsid").value;
                     did = document.getElementById("lbldid").value;
                     clid = document.getElementById("lblclid").value;
                     eqid = document.getElementById("lbleqid").value;
                     ptid = document.getElementById("lbltaskid").value;
                     tli = document.getElementById("lbltasklev").value;
                     chk = document.getElementById("lblchk").value;
                     usr = document.getElementById("lblusername").value;
                     comid = document.getElementById("lblco").value;
                     var eReturn = window.showModalDialog("../equip/CompCopyMiniDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&tli=" + tli + "&chk=" + chk + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&usr=" + usr + "&ro=" + ro + "&date=" + Date() + "&coid=" + comid, "", "dialogHeight:700px; dialogWidth:880px; resizable=yes");
                     if (eReturn == "log") {
                         //window.parent.handlelogout();
                     }
                     else if (eReturn) {
                         document.getElementById("lblcompchk").value = "1";
                     }
                     document.getElementById("form1").submit()
                 }
             }

         }

         function getPFDiv() {

             //handleapp();
             var chken = document.getElementById("lblenable").value;
             var ro = document.getElementById("lblro").value;
             ptid = document.getElementById("lbltaskid").value;
             if (chken != "1" || ro == "1") {
                 if (ptid != "") {
                     //window.parent.setref();
                     var eq = document.getElementById("lbleqid").value;
                     var valu = document.getElementById("lbltaskid").value;
                     //document.getElementById("submit();
                     var eReturn = window.showModalDialog("../equip/PFIntDialog.aspx?tid=" + valu + "&eqid=" + eq + "&ro=" + ro, "", "dialogHeight:560px; dialogWidth:669px; resizable=yes");
                 }
                 if (eReturn) {
                     //alert(eReturn)
                     document.getElementById("txtpfint").value = eReturn;
                     document.getElementById("form1").submit()
                 }
             }
         }
         function GetFailDiv() {

             //handleapp();
             var chken = document.getElementById("lblenable").value;
             ro = document.getElementById("lblro").value;
             ptid = document.getElementById("lbltaskid").value;
             var tpmhold = document.getElementById("lbltpmhold").value;
             if ((chken != "1" || ro == "1") && tpmhold != "1") {
                 if (ptid != "") {
                     //window.parent.setref();
                     cid = document.getElementById("lblcid").value;
                     eqid = document.getElementById("lbleqid").value;
                     fuid = document.getElementById("lblfuid").value;
                     ptid = document.getElementById("lbltaskid").value;
                     coid = document.getElementById("lblco").value;
                     cvalu = document.getElementById("lblcompnum").value; //document.getElementById("ddcomp.options[document.getElementById("ddcomp.options.selectedIndex].text;
                     cind = document.getElementById("lblcoid").value;

                     if (cind != "0") {
                         var eReturn = window.showModalDialog("../equip/CompFailDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&cvalu=" + cvalu + "&ro=" + ro, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                     }
                     else {
                         alert("No Component Selected")
                     }
                 }
             }
             if (eReturn) {
                 document.getElementById("lblcompfailchk").value = "1";
                 document.getElementById("form1").submit()
             }
         }
         function controlrow(id) {
             document.getElementById("lbltabid").value = id;
             vwflg = document.getElementById(id).className;
             closerows();
             if (id == "tbeq" && vwflg == "details") {
                 document.getElementById(id).className = "view";
                 document.getElementById('tbeq2').className = "view";
                 document.getElementById('imgeq').src = "../images/appbuttons/optdown.gif";
             }
             else if (id == "tbdtls" && vwflg == "details") {
                 document.getElementById(id).className = "view";
                 document.getElementById("tbdtls2").className = "view";
                 document.getElementById('imgdtls').src = "../images/appbuttons/optdown.gif";
             }
             else if (id == "tbreqs" && vwflg == "details") {
                 document.getElementById(id).className = "view";
                 document.getElementById('imgreqs').src = "../images/appbuttons/optdown.gif";
             }
         }

         function closerows() {
             document.getElementById('tbeq').className = "details";
             document.getElementById('tbeq2').className = "details";
             document.getElementById('imgeq').src = "../images/appbuttons/optup.gif";
             document.getElementById('tbdtls').className = "details";
             document.getElementById('tbdtls2').className = "details";
             document.getElementById('imgdtls').src = "../images/appbuttons/optup.gif";
             document.getElementById('tbreqs').className = "details";
             document.getElementById('imgreqs').src = "../images/appbuttons/optup.gif";
         }
         function CheckDel(val) {

             if (val == "Delete") {
                 var decision = confirm("Are You Sure You Want to Mark this Task as Delete?")
                 if (decision == true) {
                     //document.getElementById("submit('ddcomp', '');
                     FreezeScreen('Your Data Is Being Processed...');
                     document.getElementById("lbldel").value = "delr"
                     document.getElementById("form1").submit();
                 }
                 else {
                     alert("Action Cancelled")
                 }

             }
         }
         function GoToPMLib() {
             handleapp();
             window.open("../equip/EQCopy.aspx?date=" + Date(), target = "_top");
         }
         function pmidtest() {
             alert(document.getElementById("lbldocpmid").value)
         }
         function getMeasDiv() {

             //handleapp();
             var chken = document.getElementById("lblenable").value;
             ptid = document.getElementById("lbltaskid").value;
             //alert(chken)
             var ro = document.getElementById("lblro").value;
             var tpmhold = document.getElementById("lbltpmhold").value;
             if ((chken != "1" || ro == "1") && tpmhold != "1") {
                 if (ptid != "") {
                     //window.parent.setref();
                     var coid = document.getElementById("lblco").value;
                     var valu = document.getElementById("lbltaskid").value;
                     var eReturn = window.showModalDialog("../utils/tmdialog.aspx?typ=pm&taskid=" + valu + "&coid=" + coid + "&ro=" + ro, "", "dialogHeight:650px; dialogWidth:550px; resizable=yes");
                 }
                 if (eReturn) {
                     document.getElementById("form1").submit()
                 }
             }
         }
         function handleapp() {
             //var app = document.getElementById("appchk").value;
             //if(app=="switch") //window.parent.rtop.handleapp(app);
         }
         function filldown(val) {
             var dt = document.getElementById("ddeqstat").options[document.getElementById("ddeqstat").options.selectedIndex].text;
             dt = document.getElementById("ddeqstat").value
             if (dt == "2") {
                 //if(document.getElementById("txtrdt").value!="") {
                 document.getElementById("txtrdt").value = val;
                 //}
             }
         }
         function zerodt(val) {
             var dt = document.getElementById("ddeqstat").options[document.getElementById("ddeqstat").options.selectedIndex].text;
             dt = document.getElementById("ddeqstat").value
             if (dt == "2") {
                 document.getElementById("txtrdt").value = "0";
             }
         }
         function copydesc(val) {
             var chk = document.getElementById('ddtaskstat').value;
             //alert(chk)
             if (chk != "Delete") {
                 document.getElementById('txtdesc').innerHTML = val;
             }
         }
         function getti() {
             //"taskimagepmdialog.aspx?funcid=" + funcid + "&eqid=" + eqid + "&tasknum=" + tasknum
             ptid = document.getElementById("lbltaskid").value;
             if (ptid != "") {
                 var eqid = document.getElementById("lbleqid").value;
                 var funcid = document.getElementById("lblfuid").value;
                 var tasknum = document.getElementById("lblpg").innerHTML;
                 var eReturn = window.showModalDialog("../pmpics/taskimagepmdialog.aspx?funcid=" + funcid + "&eqid=" + eqid + "&tasknum=" + tasknum, "", "dialogHeight:350px; dialogWidth:350px; resizable=yes");
             }
         }
         function checkpush() {
             var eReturn = window.showModalDialog("../complib/compushdialog.aspx", "", "dialogHeight:100px; dialogWidth:400px; resizable=yes");
             if (eReturn) {
                 if (eReturn != "") {
                     document.getElementById("lblnewkey").value = eReturn;
                     document.getElementById("lblcompchk").value = "pushcomp";
                     document.getElementById("form1").submit();
                 }
             }
         }
		-->
		</script>
	</HEAD>
	<body  onload="checkit();">
		<form id="form1" method="post" runat="server">
			<div id="FreezePane" class="FreezePaneOff" align="center">
				<div id="InnerFreezePane" class="InnerFreezePane"></div>
			</div>
			<div style="Z-INDEX: 1000; POSITION: absolute; VISIBILITY: hidden" id="overDiv"></div>
			<div style="POSITION: absolute; TOP: 4px; LEFT: 4px">
				<table cellSpacing="0" cellPadding="0" width="700">
					<tr>
						<td class="thdrsinglft" width="26" align="left"><IMG border="0" src="../images/appbuttons/minibuttons/3gearsh.gif"></td>
						<td class="thdrsingrt label" onmouseover="return overlib('Against what is the PM intended to protect?', ABOVE, LEFT)"
							onmouseout="return nd()" width="674"><asp:Label id="lang1767" runat="server">Failure Modes, Causes And/Or Effects CompLibTasks2</asp:Label></td>
					</tr>
				</table>
				<table id="tbeq" class="view" cellSpacing="0" cellPadding="1" width="700">
					<tr>
						<td class="label" height="20" width="140"><asp:Label id="lang1768" runat="server">Component Addressed:</asp:Label></td>
						<td id="tdcompnum" class="bluelabellt" width="242" runat="server"><asp:Label id="lang1769" runat="server">Component #</asp:Label></td>
						<td class="label" width="48"></td>
						<td class="label" width="30"><asp:Label id="lang1770" runat="server">Qty:</asp:Label></td>
						<td class="label" width="282"><asp:textbox id="txtcQty" runat="server" CssClass="plainlabel" Width="40px"></asp:textbox></td>
					</tr>
					<tr height="30">
						<td colSpan="7">
							<table cellSpacing="0" cellPadding="0" width="650" height="30">
								<tr>
									<td width="48">&nbsp;</td>
									<td class="label" width="180" align="center"><asp:Label id="lang1771" runat="server">Component Failure Modes</asp:Label></td>
									<td width="22"></td>
									<td class="redlabel" width="200" align="center"><asp:Label id="lang1772" runat="server">Not Addressed</asp:Label></td>
									<td width="22"></td>
									<td class="bluelabel" width="180" align="center"><asp:Label id="lang1773" runat="server">Selected</asp:Label></td>
									<td width="48">&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td align="center"><asp:listbox id="lbCompFM" runat="server" CssClass="plainlabel" Width="150px" SelectionMode="Multiple"
											Height="60px"></asp:listbox></td>
									<td><asp:imagebutton id="ibReuse" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><IMG id="fromreusedis" class="details" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
											width="20" height="20" runat="server"></td>
									<td align="center"><asp:listbox id="lbfaillist" runat="server" CssClass="plainlabel" Width="150px" SelectionMode="Multiple"
											Height="60px" ForeColor="Red"></asp:listbox></td>
									<td><IMG id="todis" class="details" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
											width="20" height="20" runat="server"> <IMG id="fromdis" class="details" src="../images/appbuttons/minibuttons/backgraybg.gif"
											width="20" height="20" runat="server">
										<asp:imagebutton id="ibToTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><br>
										<asp:imagebutton id="ibFromTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton></td>
									<td align="center"><asp:listbox id="lbfailmodes" runat="server" CssClass="plainlabel" Width="150px" SelectionMode="Multiple"
											Height="60px" ForeColor="Blue"></asp:listbox></td>
									<td>&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table cellSpacing="0" width="700">
					<tr>
						<td class="thdrsinglft" width="26" align="left"><IMG border="0" src="../images/appbuttons/minibuttons/3gearsh.gif"></td>
						<td class="thdrsingrt label" onmouseover="return overlib('What are you going to do?')"
							onmouseout="return nd()" width="674"><asp:Label id="lang1774" runat="server">Task Activity Details</asp:Label></td>
					</tr>
				</table>
				<table id="tbdtls" class="view" cellSpacing="0" cellPadding="1" width="700">
					<tr>
						<td width="125"></td>
						<td width="310"></td>
						<td width="25"></td>
						<td width="90"></td>
						<td width="60"></td>
						<td width="70"></td>
						<td width="20"></td>
					</tr>
					<tr>
						<td class="label" vAlign="top" rowSpan="3"><asp:Label id="lang1775" runat="server">Task Description</asp:Label></td>
						<td vAlign="top" rowSpan="3"><asp:textbox id="txtdesc" Width="300px" cssclass="plainlabel" Runat="server" Rows="5" MaxLength="1000"
								TextMode="MultiLine"></asp:textbox></td>
						<td width="25"><IMG id="btnlookup" onclick="tasklookup();" src="../images/appbuttons/minibuttons/lookupgbg.gif"
								runat="server"></td>
						<td class="label" width="90"><asp:Label id="lang1776" runat="server">Task Type</asp:Label></td>
						<td colSpan="3"><asp:listbox id="ddtype" runat="server" CssClass="plainlabel" Width="160px" Rows="1" DataValueField="ttid"
								DataTextField="tasktype"></asp:listbox></td>
					</tr>
					<tr>
						<td><IMG id="btnlookup2" onclick="jumpto();" src="../images/appbuttons/minibuttons/magnifier.gif"
								runat="server"></td>
						<td class="label"><asp:Label id="lang1777" runat="server">PdM Tech</asp:Label></td>
						<td colSpan="3"><asp:dropdownlist id="ddpt" runat="server" CssClass="plainlabel" Width="160px"></asp:dropdownlist></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td class="label" colSpan="3" align="right"><asp:Label id="lang1778" runat="server">Measurements Required?</asp:Label></td>
						<td><IMG onmouseover="return overlib('Add/Edit Measurements for this Task')" onmouseout="return nd()"
								onclick="getMeasDiv();" alt="" src="../images/appbuttons/minibuttons/measure.gif"
								width="27" height="20"></td>
					</tr>
				</table>
				<table cellSpacing="0" width="700">
					<tr>
						<td class="thdrsinglft" width="26" align="left"><IMG border="0" src="../images/appbuttons/minibuttons/3gearsh.gif"></td>
						<td class="thdrsingrt label" onmouseover="return overlib('How are you going to do it?')"
							onmouseout="return nd()" width="674" colSpan="3"><asp:Label id="lang1779" runat="server">Planning &amp; Scheduling Details</asp:Label></td>
					</tr>
				</table>
				<table id="tbreqs" class="view" cellSpacing="0" cellPadding="0" width="700">
					<tr>
						<td colSpan="3">
							<table width="700">
								<tr>
									<td class="label" width="110"><asp:Label id="lang1780" runat="server">Skill Required</asp:Label></td>
									<td width="180"><asp:dropdownlist id="ddskill" runat="server" Width="160px" cssclass="plainlabel"></asp:dropdownlist><IMG class="details" onmouseover="return overlib('Choose Skills for this Plant Site ')"
											onmouseout="return nd()" onclick="getss();" src="../images/appbuttons/minibuttons/plusminus.gif" width="20" height="20"></td>
									<td class="label" width="15">Qty</td>
									<td width="20"><asp:textbox id="txtqty" runat="server" CssClass="plainlabel" Width="30px"></asp:textbox></td>
									<td class="label" width="40"><asp:Label id="lang1781" runat="server">Min Ea</asp:Label></td>
									<td width="45"><asp:textbox id="txttr" runat="server" CssClass="plainlabel" Width="35px"></asp:textbox></td>
									<td class="label" width="80"><asp:Label id="lang1782" runat="server">P-F Interval</asp:Label></td>
									<td width="50"><asp:textbox id="txtpfint" runat="server" CssClass="plainlabel" Width="40px"></asp:textbox></td>
									<td class="label" width="30"><asp:Label id="lang1783" runat="server">Days</asp:Label></td>
									<td width="20"><IMG id="btnpfint" onmouseover="return overlib('Estimate Frequency using the PF Interval')"
											onmouseout="return nd()" onclick="getPFDiv();" alt="" src="../images/appbuttons/minibuttons/lilcalc.gif"
											width="20" height="20"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="0" width="700">
					<tr>
						<td colSpan="10">
							<table width="700">
								<tr>
									<td class="label" width="110"><asp:Label id="lang1784" runat="server">Equipment Status</asp:Label></td>
									<td width="95"><asp:dropdownlist id="ddeqstat" runat="server" CssClass="plainlabel" Width="90px"></asp:dropdownlist></td>
									<td class="label" width="40"><asp:Label id="lang1785" runat="server">DTime</asp:Label></td>
									<td width="40"><asp:textbox id="txtrdt" runat="server" CssClass="plainlabel" Width="35px"></asp:textbox></td>
									<td class="label" width="120"><asp:checkbox id="cbloto" runat="server"></asp:checkbox>LOTO&nbsp;&nbsp;<asp:checkbox id="cbcs" runat="server"></asp:checkbox>CS
									</td>
									<td class="label" width="60"><asp:Label id="lang1786" runat="server">Frequency</asp:Label></td>
									<td width="60"><asp:textbox id="txtfreq" runat="server" CssClass="plainlabel" Width="60px"></asp:textbox></td>
                                    <td width="20"><input id="cbfixed" runat="server" type="checkbox" onmouseover="return overlib('Check if Frequency is Fixed (Calendar Frequency Only)')"
											onmouseout="return nd()" /></td>
                                    <td width="80"><img class="details" alt="" onclick="getmeter();" onmouseover="return overlib('View, Add, or Edit Meter Frequency for this Task', ABOVE, LEFT)"
                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/meter1.gif" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td id="tdtpmhold" class="plainlabelred" colSpan="3" runat="server"></td>
						<td colSpan="7" align="right"><IMG id="img1" onmouseover="return overlib('Add/Edit Parts for this Task')" onmouseout="return nd()"
								onclick="GetPartDiv();" alt="" src="../images/appbuttons/minibuttons/parttrans.gif" width="23" height="19"
								runat="server"> <IMG onmouseover="return overlib('Add/Edit Tools for this Task')" onmouseout="return nd()"
								onclick="GetToolDiv();" alt="" src="../images/appbuttons/minibuttons/tooltrans.gif" width="23" height="19">
							<IMG onmouseover="return overlib('Add/Edit Lubricants for this Task')" onmouseout="return nd()"
								onclick="GetLubeDiv();" alt="" src="../images/appbuttons/minibuttons/lubetrans.gif"
								width="23" height="19">
						</td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="0" width="700">
					<tr id="trh4" class="view" runat="server">
						<td style="BORDER-TOP: #7ba4e0 thin solid" width="20" align="center"><IMG id="btnStart" src="../images/appbuttons/minibuttons/tostartbg.gif" width="20" height="20"
								runat="server">
						</td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" width="20" align="center"><IMG id="btnPrev" src="../images/appbuttons/minibuttons/prevarrowbg.gif" width="20" height="20"
								runat="server">
						</td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" class="bluelabel" width="110" align="center"><asp:Label id="lang1787" runat="server">Task#</asp:Label><asp:label id="lblpg" runat="server" CssClass="bluelabel" ForeColor="Blue" Font-Bold="True"
								Font-Names="Arial" Font-Size="X-Small"></asp:label>&nbsp;of&nbsp;<asp:label id="lblcnt" runat="server" CssClass="bluelabel" ForeColor="Blue" Font-Bold="True"
								Font-Names="Arial" Font-Size="X-Small"></asp:label></td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" width="20" align="center"><IMG id="btnNext" src="../images/appbuttons/minibuttons/nextarrowbg.gif" width="20" height="20"
								runat="server">
						</td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" width="30" align="center"><IMG id="btnEnd" src="../images/appbuttons/minibuttons/tolastbg.gif" width="20" height="20"
								runat="server">
						</td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" class="label" width="70" align="center"><asp:Label id="lang1788" runat="server">Task Order</asp:Label></td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" class="label" width="40"><asp:textbox id="txttaskorder" runat="server" CssClass="plainlabel" Width="35px"></asp:textbox></td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" class="greenlabel" width="150" align="left"><asp:label id="Label26" runat="server" CssClass="greenlabel" Font-Bold="True" Font-Names="Arial"
								Font-Size="X-Small"># of Sub Tasks: </asp:label><asp:label id="lblsubcount" runat="server" CssClass="greenlabel" Font-Bold="True" Font-Names="Arial"
								Font-Size="X-Small"></asp:label></td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" width="260" align="right"><IMG id="btnedittask" onclick="doEnable();" border="0" alt="" src="../images/appbuttons/minibuttons/lilpentrans.gif"
								width="19" height="19" runat="server"><asp:imagebutton id="btnaddtsk" runat="server" ImageUrl="../images/appbuttons/minibuttons/addnewbg1.gif"
								BorderStyle="None"></asp:imagebutton>
							<asp:imagebutton id="btndeltask" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"></asp:imagebutton><asp:imagebutton id="ibCancel" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"></asp:imagebutton><IMG id="btnsav" onclick="valpgnums();" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif"
								width="20" height="20" runat="server"><IMG id="sgrid" onmouseover="return overlib('Add/Edit Sub Tasks')" onmouseout="return nd()"
								onclick="getsgrid();" alt="" src="../images/appbuttons/minibuttons/sgrid1.gif" width="20" height="20" runat="server"></td>
					</tr>
					<tr>
						<td colSpan="10" align="center"><asp:label id="msglbl" runat="server" CssClass="redlabel" Width="360px" ForeColor="Red" Font-Bold="True"
								Font-Names="Arial" Font-Size="X-Small"></asp:label></td>
					</tr>
				</table>
			</div>
			<div style="Z-INDEX: 999; BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 630px; HEIGHT: 620px; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
				id="lstdiv" class="details">
				<table cellSpacing="0" cellPadding="0" width="630" bgColor="white">
					<tr height="20" bgColor="blue">
						<td class="labelwht"><asp:Label id="lang1789" runat="server">List Dialog</asp:Label></td>
						<td align="right"><IMG onclick="closelst();" alt="" src="../images/close.gif" width="18" height="18"><br>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><iframe style="WIDTH: 630px; HEIGHT: 620px" id="iflst" runat="server"></iframe>
						</td>
					</tr>
				</table>
			</div>
			<input id="lblsid" type="hidden" name="lblsid" runat="server"><input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
			<input id="lblcid" type="hidden" name="lblcid" runat="server"><input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
			<input id="lbldid" type="hidden" name="lbldid" runat="server"><input id="lblclid" type="hidden" name="lblclid" runat="server">
			<input id="lbleqid" type="hidden" name="lbleqid" runat="server"><input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input id="lblcoid" type="hidden" name="lblcoid" runat="server"><input id="lblfilt" type="hidden" name="lblfilt" runat="server">
			<input id="lblptid" type="hidden" name="lblptid" runat="server"><input id="lblpar" type="hidden" name="lblpar" runat="server">
			<input id="lblchk" type="hidden" name="lblchk" runat="server"><input id="lblco" type="hidden" name="lblco" runat="server">
			<input id="lblfail" type="hidden" name="lblfail" runat="server"><input id="lblsb" type="hidden" name="lblsb" runat="server">
			<input id="lblpgholder" type="hidden" name="lblpgholder" runat="server"><input id="lblt" type="hidden" name="lblt" runat="server">
			<input id="lblst" type="hidden" name="lblst" runat="server"><input id="lblsvchk" type="hidden" name="lblsvchk" runat="server">
			<input id="lblenable" type="hidden" name="lblenable" runat="server"><input id="lblcompchk" type="hidden" name="lblcompchk" runat="server">
			<input id="lblcompfailchk" type="hidden" name="lblcompfailchk" runat="server"><input id="lblstart" type="hidden" name="lblstart" runat="server">
			<input id="lblcurrsb" type="hidden" name="lblcurrsb" runat="server"><input id="lblcurrcs" type="hidden" name="lblcurrcs" runat="server">
			<input id="appchk" type="hidden" name="appchk" runat="server"><input id="lbllog" type="hidden" name="lbllog" runat="server">
			<input id="pgflag" type="hidden" name="pgflag" runat="server"><input id="lblfiltcnt" type="hidden" name="lblfiltcnt" runat="server">
			<input id="lot" type="hidden" name="lot" runat="server"><input id="cs" type="hidden" name="cs" runat="server">
			<input id="lblusername" type="hidden" name="lblusername" runat="server"> <input id="lbllock" type="hidden" name="lbllock" runat="server">
			<input id="lbllockedby" type="hidden" name="lbllockedby" runat="server"> <input id="lblhaspm" type="hidden" name="lblhaspm" runat="server">
			<input id="lbltyp" type="hidden" name="lbltyp" runat="server"> <input id="lbllid" type="hidden" name="lbllocid" runat="server">
			<input id="lblro" type="hidden" name="lblro" runat="server"><input id="lblnoeq" type="hidden" name="lblnoeq" runat="server">
			<input id="lbltpmalert" type="hidden" name="lbltpmalert" runat="server"><input id="lbltpmhold" type="hidden" name="lbltpmhold" runat="server">
			<input id="lbloldtask" type="hidden" name="lbloldtask" runat="server"><input id="lblsave" type="hidden" runat="server" NAME="lblsave" />
		
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblfreq" runat="server" />
<input type="hidden" id="lblfixed" runat="server" />
<input type="hidden" id="lblcomi" runat="server" />
             <input type="hidden" id="lblsstr" runat="server" />
             <input type="hidden" id="lblssidx" runat="server" />
</form>
	</body>
</HTML>
