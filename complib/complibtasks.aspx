<%@ Page Language="vb" AutoEventWireup="false" Codebehind="complibtasks.aspx.vb" Inherits="lucy_r12.complibtasks" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>complibtasks</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
	</HEAD>
	<body  onload="scrolltop();">
		<form id="form1" method="post" runat="server">
			<table style="Z-INDEX: 102; POSITION: absolute; TOP: 4px; LEFT: 4px" width="3200" id="scrollmenu">
				<tr>
					<td align="left"><asp:imagebutton id="addtask" runat="server" ImageUrl="../images/appbuttons/bgbuttons/addtask.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td><asp:datagrid id="dgtasks" runat="server" GridLines="None" AllowSorting="True" AutoGenerateColumns="False"
							CellSpacing="1">
							<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
							<ItemStyle CssClass="ptransrow"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:imagebutton id="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:imagebutton>
										<img id="ibast" runat="server" src="../images/appbuttons/minibuttons/subtask.gif" onclick="getsgrid();"
											width="20" height="20">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="tasknum desc" HeaderText="Task#">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblta runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id=lblt cssclass="plainlabel wd40" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>' width="40px">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="subtask desc" HeaderText="Sub Task#">
									<HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblsubt runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id=lblst runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<asp:ImageButton id="ImageButton1" runat="server" CommandName="hd" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
											ToolTip="Hide Description"></asp:ImageButton><BR>
										<asp:ImageButton id="ImageButton2" runat="server" CommandName="sd" ImageUrl="../images/appbuttons/minibuttons/show.gif"
											ToolTip="Show Description"></asp:ImageButton>
									</HeaderTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Description">
									<HeaderStyle Width="270px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label3 Width="270px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id=txtdesc runat="server" cssclass="plainlabel wd260" MaxLength="500" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>' width="260px" TextMode="MultiLine" Height="70px">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<asp:ImageButton id="ImageButton3" runat="server" CommandName="htt" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
											ToolTip="Hide Task Type"></asp:ImageButton><BR>
										<asp:ImageButton id="ImageButton4" runat="server" CommandName="stt" ImageUrl="../images/appbuttons/minibuttons/show.gif"
											ToolTip="Show Task Type"></asp:ImageButton>
									</HeaderTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="tasktype desc" HeaderText="Task Type">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasktype") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:dropdownlist id="ddtype" runat="server" DataSource="<%# PopulateTaskTypes %>" DataTextField="tasktype" DataValueField="ttid" SelectedIndex='<%# GetSelIndex(Container.DataItem("taskindex")) %>' >
										</asp:dropdownlist>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<asp:ImageButton id="Imagebutton5" runat="server" CommandName="hfr" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
											ToolTip="Hide Frequency"></asp:ImageButton><BR>
										<asp:ImageButton id="Imagebutton6" runat="server" CommandName="sfr" ImageUrl="../images/appbuttons/minibuttons/show.gif"
											ToolTip="Show Frequency"></asp:ImageButton>
									</HeaderTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="freq desc" HeaderText="Frequency">
									<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txtfreq" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.freq") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="compnum desc" HeaderText="Failure Modes This Task Will Address">
									<HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label21 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fm1") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fm1") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<asp:ImageButton id="Imagebutton11" runat="server" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
											CommandName="hsk" ToolTip="Hide Skill Required"></asp:ImageButton><BR>
										<asp:ImageButton id="Imagebutton12" runat="server" ImageUrl="../images/appbuttons/minibuttons/show.gif"
											CommandName="ssk" ToolTip="Show Skill Required"></asp:ImageButton>
									</HeaderTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="skill desc" HeaderText="Skill Required">
									<HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:dropdownlist id="ddskill" runat="server" DataSource="<%# PopulateSkills %>" DataTextField="skill" DataValueField="skillid" SelectedIndex='<%# GetSelIndex(Container.DataItem("skillindex")) %>'>
										</asp:dropdownlist>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Qty">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>' ID="Label10" NAME="Label8">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txtqty" runat="server" cssclass="plainlabel wd40" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Time">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>' ID="Label12" >
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txttr" runat="server" cssclass="plainlabel wd50" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderTemplate>
										<asp:ImageButton id="Imagebutton9" runat="server" CommandName="hpt" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
											ToolTip="Hide Predictive Technology"></asp:ImageButton><BR>
										<asp:ImageButton id="Imagebutton10" runat="server" CommandName="spt" ImageUrl="../images/appbuttons/minibuttons/show.gif"
											ToolTip="Show Predictive Technology"></asp:ImageButton>
									</HeaderTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="pretech desc" HeaderText="Predictive Technology">
									<HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label7" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pretech") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:dropdownlist id="ddpt" runat="server" DataSource="<%# PopulatePreTech %>" DataTextField="pretech" DataValueField="ptid" SelectedIndex='<%# GetSelIndex(Container.DataItem("ptindex")) %>'>
										</asp:dropdownlist>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<asp:ImageButton id="Imagebutton13" runat="server" CommandName="hes" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
											ToolTip="Hide PM Equipment Status"></asp:ImageButton><BR>
										<asp:ImageButton id="Imagebutton14" runat="server" CommandName="ses" ImageUrl="../images/appbuttons/minibuttons/show.gif"
											ToolTip="Show PM Equipment Status"></asp:ImageButton>
									</HeaderTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="rd desc" HeaderText="PM Equipment Status">
									<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:label id=Label75 runat="server" Font-Size="10pt" Font-Names="Arial" ForeColor="Black" Text='<%# DataBinder.Eval(Container, "DataItem.rd") %>'>
										</asp:label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:dropdownlist id="ddeqstat" runat="server" DataSource="<%# PopulateStatus %>" DataTextField="status" DataValueField="statid" SelectedIndex='<%# GetSelIndex(Container.DataItem("rdindex")) %>'>
										</asp:dropdownlist>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<asp:ImageButton id="Imagebutton15" runat="server" CommandName="hlo" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
											ToolTip="Hide LOTO"></asp:ImageButton><BR>
										<asp:ImageButton id="Imagebutton16" runat="server" CommandName="slo" ImageUrl="../images/appbuttons/minibuttons/show.gif"
											ToolTip="Show LOTO"></asp:ImageButton>
									</HeaderTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="loto desc" HeaderText="LOTO">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label11" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.loto") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:dropdownlist id="ddloto" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("lotoid")) %>'>
											<asp:ListItem Value="0">Select</asp:ListItem>
											<asp:ListItem Value="1">Yes</asp:ListItem>
											<asp:ListItem Value="2">No</asp:ListItem>
										</asp:dropdownlist>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<asp:ImageButton id="Imagebutton17" runat="server" CommandName="hco" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
											ToolTip="Hide Confined Space"></asp:ImageButton><BR>
										<asp:ImageButton id="Imagebutton18" runat="server" CommandName="sco" ImageUrl="../images/appbuttons/minibuttons/show.gif"
											ToolTip="Show Confined Space"></asp:ImageButton>
									</HeaderTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="confined desc" HeaderText="Confined Spaced">
									<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label19" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.confined") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:dropdownlist id="ddcs" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("conid")) %>'>
											<asp:ListItem Value="0">Select</asp:ListItem>
											<asp:ListItem Value="1">Yes</asp:ListItem>
											<asp:ListItem Value="2">No</asp:ListItem>
										</asp:dropdownlist>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Parts/Tools/Lubes">
									<HeaderStyle CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:imagebutton id="Imagebutton23" runat="server" ImageUrl="../images/appbuttons/minibuttons/parttrans.gif"
											ToolTip="Add Parts" CommandName="Part"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton23a" runat="server" ImageUrl="../images/appbuttons/minibuttons/tooltrans.gif"
											ToolTip="Add Tools" CommandName="Tool"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton23b" runat="server" ImageUrl="../images/appbuttons/minibuttons/lubetrans.gif"
											ToolTip="Add Lubricants" CommandName="Lube"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton23c" runat="server" ImageUrl="../images/appbuttons/minibuttons/notessm.gif"
											ToolTip="Add Notes to the Task" CommandName="Note"></asp:imagebutton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="pmtskid">
									<ItemTemplate>
										<asp:Label id="lbltid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblttid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Delete">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:ImageButton id="ibDel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
			<input id="lblfilt" type="hidden" runat="server"> <input id="lblfiltfilt" type="hidden" runat="server">
			<input id="lbltcnt" type="hidden" runat="server"> <input id="lblcomid" type="hidden" runat="server">
			<input id="lblcid" type="hidden" runat="server"> <input id="lblsid" type="hidden" runat="server">
			<input id="lblcurrsort" type="hidden" runat="server"> <input id="lbladdchk" type="hidden" runat="server">
			<input type="hidden" id="lbloldtask" runat="server"> <input type="hidden" id="lblcurrtask" runat="server">
			<input type="hidden" id="lblitemindex" runat="server"> <input id="xCoord" type="hidden" name="xCoord" runat="server">
			<input id="yCoord" type="hidden" name="yCoord" runat="server"> <input type="hidden" id="lblcompnum" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
