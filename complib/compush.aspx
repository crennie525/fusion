<%@ Page Language="vb" AutoEventWireup="false" Codebehind="compush.aspx.vb" Inherits="lucy_r12.compush" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>compush</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" src="../scripts1/compushaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checkret();">
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="bluelabel"><asp:Label id="lang1866" runat="server">New Search Key</asp:Label></td>
					<td>
						<asp:TextBox id="txtnewkey" runat="server" Width="170px" CssClass="plainlabel"></asp:TextBox></td>
					<td><img src="../images/appbuttons/minibuttons/savedisk1.gif" onclick="checknew();"></td>
				</tr>
			</table>
			<input type="hidden" id="lblsubmit" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
