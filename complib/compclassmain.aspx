<%@ Page Language="vb" AutoEventWireup="false" Codebehind="compclassmain.aspx.vb" Inherits="lucy_r12.compclassmain" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>compclassmain</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" src="../scripts1/compclassmainaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="thdrsing label" bgColor="#1d11ef" width="722" colSpan="3"><asp:Label id="lang1674" runat="server">Add/Edit Component Classes</asp:Label></td>
				</tr>
				<tr height="54">
					<td vAlign="top" colSpan="4">
						<table border="0" cellPadding="0" width="720">
							<tr id="deptdiv" runat="server">
								<td class="label" width="112"><asp:Label id="lang1675" runat="server">Component Class</asp:Label></td>
								<td width="180"><asp:textbox id="txtclass" runat="server" CssClass="plainlabel" Width="180px"></asp:textbox></td>
								<td width="22"><IMG id="imgadd" onclick="addclass();" src="../images/appbuttons/minibuttons/addnew.gif"
										runat="server"></td>
								<td width="22"><IMG id="imgsrch" onclick="srchclass();" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server"></td>
								<td width="22"><IMG id="Img1" onclick="saveclass();" src="../images/appbuttons/minibuttons/savedisk1.gif"
										runat="server"></td>
								<td width="380"><IMG onclick="refreshit();" src="../images/appbuttons/minibuttons/refreshit.gif"></td>
							</tr>
							<tr id="Tr2" runat="server">
								<td class="label" width="92"><asp:Label id="lang1676" runat="server">Description</asp:Label></td>
								<td colSpan="5"><asp:textbox id="txtclassdesc" runat="server" CssClass="plainlabel" Width="360px"></asp:textbox></td>
							</tr>
							<tr>
								<td colSpan="6">
									<div style="BORDER-BOTTOM: 2px groove; BORDER-LEFT: 1px groove; WIDTH: 712px; HEIGHT: 280px; BORDER-TOP: 1px groove; BORDER-RIGHT: 2px groove"
										id="dloc">
										<table cellSpacing="0" cellPadding="0" width="712">
											<tr>
												<td><IMG src="../images/appbuttons/minibuttons/4PX.gif"></td>
											</tr>
											<tr>
												<td class="btmmenu plainlabel" bgColor="#1d11ef" height="20" colSpan="5"><asp:Label id="lang1677" runat="server">Component Sub Classes</asp:Label></td>
											</tr>
											<tr>
												<td><IMG src="../images/appbuttons/minibuttons/4PX.gif"></td>
											</tr>
											<tr>
												<td class="label" width="80"><asp:Label id="lang1678" runat="server">Sub Class</asp:Label></td>
												<td width="180"><asp:textbox id="txtnewsub" runat="server" CssClass="plainlabel" Width="180px"></asp:textbox></td>
												<td width="22"><IMG id="Img2" onclick="addsub();" src="../images/appbuttons/minibuttons/addnew.gif"
														runat="server"></td>
												<td width="22"><IMG id="Img3" onclick="srchsubclass();" src="../images/appbuttons/minibuttons/magnifier.gif"
														runat="server"></td>
												<td width="408"></td>
											</tr>
											<tr>
												<td><IMG src="../images/appbuttons/minibuttons/4PX.gif"></td>
											</tr>
											<tr>
												<td id="tdchildren" colSpan="5" runat="server"></td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lblsubmit" runat="server"> <input type="hidden" id="lbloldclass" runat="server">
			<input type="hidden" id="lbloldsub" runat="server"><input type="hidden" id="lblacid" runat="server">
			<input type="hidden" id="lblscid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
