<%@ Page Language="vb" AutoEventWireup="false" Codebehind="complibuploadimage.aspx.vb" Inherits="lucy_r12.complibuploadimage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>complibuploadimage</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		
		<script language="JavaScript" src="../scripts/tpmimgtasknav.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/complibuploadimageaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="GetsScroll();" >
		<form id="form1" method="post" runat="server">
			<table width="520">
				<TBODY>
					<tr>
						<td class="thdrsing label" colSpan="3"><asp:Label id="lang1863" runat="server">Image Upload Dialog</asp:Label></td>
					</tr>
					<tr>
						<td class="bluelabel" width="150"><asp:Label id="lang1864" runat="server">Choose Image to Upload</asp:Label></td>
						<td class="plainlabel" width="240"><INPUT id="MyFile" style="WIDTH: 230px" type="file" size="5" name="MyFile" RunAt="Server"></td>
						<td width="130"><input class="plainlabel" id="btnupload" type="button" value="Upload" name="btnupload"
								runat="server"></td>
					</tr>
					<tr>
						<td class="thdrsing label" colSpan="3"><asp:Label id="lang1865" runat="server">Current Images</asp:Label></td>
					</tr>
					<tr>
						<td id="tblpics" align="center" colSpan="3" runat="server">
							<div id="picdiv" style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 520px; HEIGHT: 260px; OVERFLOW: auto; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
								onscroll="SetsDivPosition();" runat="server"></div>
						</td>
					</tr>
				</TBODY>
			</table>
			<input id="lblfuncid" type="hidden" runat="server" NAME="lblblockid"> <input type="hidden" id="lbltasknum" runat="server" NAME="lbltasknum">
			<input type="hidden" id="lblfunc" runat="server" NAME="lblfunc"><input id="lblimgid" type="hidden" runat="server" NAME="lblimgid">
			<input id="lblro" type="hidden" runat="server" NAME="lblro"><input id="spdivy" type="hidden" name="spdivy" runat="server">
			<input id="lblpicorder" type="hidden" runat="server" NAME="lblpicorder"> <input id="lblsubmit" type="hidden" runat="server" NAME="lblsubmit">
			<input id="lblneworder" type="hidden" runat="server" NAME="lblneworder"> <input id="lbloldorder" type="hidden" runat="server" NAME="lbloldorder">
			<input id="lblmaxorder" type="hidden" runat="server" NAME="lblmaxorder"> <input id="lblititles" type="hidden" runat="server" NAME="lblititles">
			<input id="lblref" type="hidden" runat="server" NAME="lblref"><input type="hidden" id="lbloldtask" runat="server" NAME="lbloldtask">
			<input type="hidden" id="lblmaxtask" runat="server" NAME="lblmaxtask"><input type="hidden" id="lbledit" runat="server" NAME="lbledit">
			<input type="hidden" id="lbltyp" runat="server" NAME="lbltyp"> <input type="hidden" id="lbleqid" runat="server" NAME="lbleqid">
			<input type="hidden" id="lblcomid" runat="server" NAME="lblcomid"> <input type="hidden" id="lblimg" runat="server" NAME="lblimg">
			<input type="hidden" id="lblbimg" runat="server" NAME="lblbimg"> <input type="hidden" id="lblnimg" runat="server" NAME="lblnimg">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
