<%@ Page Language="vb" AutoEventWireup="false" Codebehind="complibadd.aspx.vb" Inherits="lucy_r12.complibadd" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>complibadd</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/complibaddaspx_2.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
         function getecd1() {
             var compnum = document.getElementById("txtnewcomp").value;
             //if (compnum != "") {
             var eReturn = window.showModalDialog("compdiv1dialog.aspx?compnum=" + compnum + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
             if (eReturn) {
                 //alert(eReturn)
                 var ret = eReturn.split("~");
                 var chk = ret[0];
                 if (chk != "can") {
                     var chk2 = ret[1];
                     if (chk2 != compnum) {
                         document.getElementById("txtnewcomp").value = chk2;
                     }
                     document.getElementById("lblecd1id").value = chk;
                     document.getElementById("form1").submit();
                 }
                 else {
                     return false;
                 }

             }
             else {

                 return false;
             }
             //}
         }
         function DisableButton(b) {
             document.getElementById("btntocomp").className = "details";
             document.getElementById("btnfromcomp").className = "details";
             document.getElementById("todis").className = "view";
             document.getElementById("fromdis").className = "view";
             var ecd2 = document.getElementById("lblcurrecd2").value;
             var chk = document.getElementById("lblisecd").value;
             if (chk == "1") {
                 var eReturn = window.showModalDialog("../equip/compdivecd3dialog.aspx?ecd2=" + ecd2 + "&date=" + Date(), "", "dialogHeight:200px; dialogWidth:500px; resizable=yes");
                 if (eReturn) {
                     document.getElementById("lblnewecd3").value = eReturn;
                     document.getElementById("form1").submit();
                 }
             }
             else {
                 document.getElementById("form1").submit();
             }
         }
         function delimg() {
             var comid = document.getElementById("lblcoid").value;
             if (comid != "" && comid != "0") {
                 id = document.getElementById("lblimgid").value
                 if (id != "") {
                     document.getElementById("lblcompchk").value = "delimg";
                     document.getElementById("form1").submit();
                 }
                 else {
                     alert("No Image Selected")
                 }
             }
             else {
                 alert("No Component Selected")
             }

         }
         function showcomp() {
             var chk = document.getElementById("lblswitch").value;
             if (chk == "all") {
                 document.getElementById("lblswitch").value = "comp";
                 document.getElementById("lbfailmaster").className = "details";
                 document.getElementById("lbfailcomp").className = "view";
                 document.getElementById("aswitch").innerHTML = "Unassigned Failure Modes";
             }
             else {
                 document.getElementById("lblswitch").value = "all";
                 document.getElementById("lbfailmaster").className = "view";
                 document.getElementById("lbfailcomp").className = "details";
                 document.getElementById("aswitch").innerHTML = "Available Failure Modes";
             }
         }
         function showcompret() {
             var chk1 = document.getElementById("lblgetecd2").value;
             if (chk1 == "1") {
                 var ecd1 = document.getElementById("lblecd1id").value;
                 var ecd2txt = document.getElementById("lblecd2txt").value;
                 //alert(ecd1)
                 var eReturn = window.showModalDialog("compdivecd2dialog.aspx?ecd1=" + ecd1 + "&ecd2txt=" + ecd2txt + "&date=" + Date(), "", "dialogHeight:200px; dialogWidth:500px; resizable=yes");
                 if (eReturn) {
                     document.getElementById("lblgetecd2").value = "";
                     document.getElementById("lblcurrecd2").value = eReturn;
                 }
             }

             var chk = document.getElementById("lblswitch").value;
             if (chk == "all") {

                 document.getElementById("lbfailmaster").className = "view";
                 document.getElementById("lbfailcomp").className = "details";
                 document.getElementById("aswitch").innerHTML = "Available Failure Modes";
             }
             else {

                 document.getElementById("lbfailmaster").className = "details";
                 document.getElementById("lbfailcomp").className = "view";
                 document.getElementById("aswitch").innerHTML = "Unassigned Failure Modes";

             }
         }
     </script>
	</HEAD>
	<body  onload="showcompret();">
		<form id="form1" method="post" runat="server">
			<div style="WIDTH: 720px" id="FreezePane" class="FreezePaneOff" align="center">
				<div id="InnerFreezePane" class="InnerFreezePane"></div>
			</div>
			<div style="Z-INDEX: 1000; POSITION: absolute; VISIBILITY: hidden" id="overDiv"></div>
			<table style="POSITION: absolute; TOP: 10px; LEFT: 10px" id="eqdetdiv" cellSpacing="0"
				cellPadding="0" width="675">
				<tr>
					<td width="110"></td>
					<td width="90"></td>
					<td width="50"></td>
					<td width="160"></td>
					<td width="100"></td>
					<td width="140"></td>
					<td width="120"></td>
				</tr>
				<tr>
					<td colSpan="7">
						<table cellSpacing="0" cellPadding="1" width="675">
							<tr>
								<td id="tdnavtop" class="thdrsinglft" width="22"><IMG border="0" src="../images/appbuttons/minibuttons/eqarch.gif"></td>
								<td class="thdrsingrt label" width="653"><asp:Label id="lang1705" runat="server">Component Details</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang1706" runat="server">Search Key</asp:Label></td>
					<td colSpan="4"><asp:textbox id="txtcokey" runat="server" Width="180px" cssclass="plainlabel wd180" MaxLength="100"></asp:textbox></td>
					<td height="40" rowSpan="10" colSpan="2" align="center">
						<table>
							<tr>
								<td><IMG id="imgco" onclick="getbig();" src="../images/appimages/compimg.gif" width="216"
										height="206" runat="server">
								</td>
							</tr>
							<tr height="20">
								<td align="center">
									<table>
										<tr>
											<td class="bluelabel" width="80"><asp:Label id="lang1707" runat="server">Order</asp:Label></td>
											<td width="60"><asp:textbox id="txtiorder" runat="server" Width="40px" CssClass="plainlabel wd40"></asp:textbox></td>
											<td width="20"><IMG class="details" onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif"></td>
											<td width="20"><IMG onmouseover="return overlib('Add\Edit Images for this block', ABOVE, LEFT)" onmouseout="return nd()"
													onclick="addpic();" src="../images/appbuttons/minibuttons/addmod.gif"></td>
											<td width="20"><IMG id="imgdel" onmouseover="return overlib('Delete This Image', ABOVE, LEFT)" onclick="delimg();"
													src="../images/appbuttons/minibuttons/del.gif" runat="server" onmouseout="return nd()"></td>
											<td width="20"><IMG id="imgsavdet" onmouseover="return overlib('Save Image Order', ABOVE, LEFT)" onmouseout="return nd()"
													onclick="savdets();" src="../images/appbuttons/minibuttons/saveDisk1.gif" runat="server">
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="center">
									<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
										cellSpacing="0" cellPadding="0">
										<tr>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="134" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabel">Image 0 of 0</asp:label></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext.gif"
													runat="server"></td>
											<td width="20"><IMG id="ilast" onclick="getplast();" src="../images/appbuttons/minibuttons/llast.gif"
													runat="server"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang1708" runat="server">Common Name</asp:Label></td>
					<td colSpan="4"><asp:textbox id="txtconame" runat="server" Width="180px" cssclass="plainlabel wd180" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang1709" runat="server">Description</asp:Label></td>
					<td colSpan="4"><asp:textbox id="txtdesc" runat="server" Width="250px" cssclass="plainlabel wd250" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label">SPL</td>
					<td colSpan="4"><asp:textbox id="txtspl" runat="server" Width="250px" cssclass="plainlabel wd250" MaxLength="200"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang1710" runat="server">Designation</asp:Label></td>
					<td colSpan="4"><asp:textbox id="txtdesig" runat="server" Width="250px" cssclass="plainlabel wd250" MaxLength="200"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label">Mfg</td>
					<td colSpan="4"><asp:textbox id="txtmfg" runat="server" Width="250px" cssclass="plainlabel wd250" MaxLength="200"></asp:textbox></td>
				</tr>
				<tr id="tdpreadd" runat="server">
					<td colSpan="4" align="center" class="plainlabelred"><asp:Label id="lang1711" runat="server">Save New Component Details to add Failure Modes and Tasks</asp:Label></td>
					<td align="right"><asp:imagebutton id="btnsave1" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:imagebutton>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang1712" runat="server">Component Class</asp:Label></td>
					<td colSpan="3" class="plainlabel" id="tdcclass" runat="server"></td>
					<td>
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td><IMG id="btnaddasset" runat="server" class="details" onmouseover="return overlib('Add/Edit Asset Class', LEFT)"
										onmouseout="return nd()" onclick="getACDiv();" alt="" src="../images/appbuttons/minibuttons/addmod.gif"></td>
								<td><IMG id="btncget" runat="server" class="details" onmouseover="return overlib('Lookup Asset Class', LEFT)"
										onmouseout="return nd()" onclick="cget();" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang1713" runat="server">Sub Class</asp:Label></td>
					<td colSpan="3" class="plainlabel" id="tdsclass" runat="server"></td>
				</tr>
                 <tr>
                <td class="label" height="20">Occurrence Area</td>
                <td colspan="3"><asp:DropDownList ID="ddoca" runat="server" AutoPostBack="true">
                    </asp:DropDownList></td>
                    
                </tr>
                <tr>
                <td colspan="4" class="label">&nbsp;RTF&nbsp;<input type="checkbox" id="cbrtf" runat="server" />&nbsp;Detectable?&nbsp;<input type="checkbox" id="cbdet" runat="server" />
                &nbsp;RPN&nbsp;&nbsp;<asp:TextBox ID="txtrpn" runat="server" Width="50px" CssClass="plainlabel"></asp:TextBox></td>
                </tr>
				<tr id="tdpreadd0" runat="server">
					<td colSpan="5" align="center" class="plainlabel"><input type="radio" id="rbmso" name="rbst" runat="server" checked><asp:Label id="lang1714" runat="server">My Site Only</asp:Label><input type="radio" id="rbml" name="rbst" onclick="checkwarn();" runat="server"><asp:Label id="lang1715" runat="server">Master List</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="5">
						<table cellSpacing="2" cellPadding="0">
							<TBODY>
								<tr vAlign="middle" height="20">
									<td width="10"></td>
									<td class="bluelabel" align="center" width="188"><asp:Label id="aswitch" runat="server">Available Failure Modes</asp:Label>
                                    </td>
                                    <td width="22"><img src="../images/appbuttons/minibuttons/checkrel.gif" onclick="showcomp();" id="imgchk" runat="server" /></td>
									<td width="22"></td>
									<td class="bluelabel" align="center" width="210"><asp:Label id="lang2042" runat="server">Component Failure Modes</asp:Label></td>
									<td width="10"></td>
								</tr>
								<tr>
									<td style="WIDTH: 31px" class="label" vAlign="top" width="31" align="center"><br>
										<br>
									</td>
									<td colspan="2" align="center"><asp:listbox id="lbfailmaster" runat="server" Width="170px" CssClass="fmboxo" Height="90px" SelectionMode="Multiple"></asp:listbox>
                                    <asp:listbox id="lbfailcomp" runat="server" Width="200px" SelectionMode="Multiple" Height="70px" CssClass="details"></asp:listbox></td>
									<td vAlign="middle" width="22" align="center"><IMG id="todis" class="details" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
											width="20" height="20" runat="server"> <IMG id="fromdis" class="details" src="../images/appbuttons/minibuttons/backgraybg.gif"
											width="20" height="20" runat="server">
										<asp:imagebutton id="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><br>
										<asp:imagebutton id="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton></td>
									<td width="210" align="center"><asp:listbox id="lbfailmodes" runat="server" Width="170px" CssClass="fmboxo" Height="90px" SelectionMode="Multiple"></asp:listbox></td>
									<td></td>
								</tr>
								<tr>
									<td class="label" colSpan="5"><asp:Label id="lang1718" runat="server">Add a New Failure Mode</asp:Label><asp:textbox id="txtnewfail" runat="server" Width="170px" cssclass="plainlabel wd170"></asp:textbox>
										&nbsp;<asp:imagebutton id="btnaddfail" runat="server" ImageUrl="../images/appbuttons/minibuttons/addmod.gif"></asp:imagebutton></td>
								</tr>
							</TBODY>
						</table>
					</td>
				</tr>
                <tr>
					<td colSpan="5" class="plainlabelblue">To remove a failure mode that is assigned to an Occurrence Area the Occurrence Area needs to be selected.</td>
				</tr>
				<tr id="tdafteradd" runat="server">
					<td colSpan="4" align="center" class="plainlabelred"><asp:Label id="lang1719" runat="server">Click Save Button to Save Changes to Current Component Details</asp:Label></td>
					<td align="right"><asp:imagebutton id="btnsave" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:imagebutton>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td colSpan="7">
						<table cellSpacing="0" cellPadding="1" width="675">
							<tr>
								<td id="tdbot" class="thdrsinglft" width="22"><IMG border="0" src="../images/appbuttons/minibuttons/eqarch.gif"></td>
								<td class="thdrsingrt label" width="653"><asp:Label id="lang1720" runat="server">Common Tasks</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="7" align="right"><IMG id="Img2" onmouseover="return overlib('Add/Edit Task Steps for this Component', LEFT)"
							onmouseout="return nd()" onclick="getsgrid();" alt="" src="../images/appbuttons/minibuttons/addmod.gif" runat="server"></td>
				</tr>
				<tr>
					<td colSpan="7"><iframe style="BORDER-BOTTOM-STYLE: none; BORDER-RIGHT-STYLE: none; BACKGROUND-COLOR: transparent; BORDER-TOP-STYLE: none; BORDER-LEFT-STYLE: none"
							id="geteq" height="100" src="complibtaskview2.aspx?jump=no" frameBorder="no" width="675" allowTransparency
							scrolling="yes" runat="server"></iframe>
					</td>
				</tr>
			</table>
			<input id="lblcoid" type="hidden" runat="server"> <input id="lblsavetype" type="hidden" runat="server">
			<input id="lblpcnt" type="hidden" name="lblpcnt" runat="server"> <input id="lblcurrp" type="hidden" name="lblcurrp" runat="server">
			<input id="lblimgs" type="hidden" name="lblimgs" runat="server"> <input id="lblimgid" type="hidden" name="lblimgid" runat="server">
			<input id="lblovimgs" type="hidden" name="lblovimgs" runat="server"> <input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server">
			<input id="lblcurrimg" type="hidden" name="lblcurrimg" runat="server"> <input id="lblcurrbimg" type="hidden" name="lblcurrbimg" runat="server">
			<input id="lblbimgs" type="hidden" name="lblbimgs" runat="server"><input id="lbliorders" type="hidden" name="lbliorders" runat="server">
			<input id="lbloldorder" type="hidden" name="lbloldorder" runat="server"> <input id="lblovtimgs" type="hidden" name="lblovtimgs" runat="server">
			<input id="lblcurrtimg" type="hidden" name="lblcurrtimg" runat="server"> <input id="lblcompchk" type="hidden" name="lblcompchk" runat="server">
			<input id="lbllog" type="hidden" runat="server"> <input id="lblro" type="hidden" runat="server">
			<input type="hidden" id="lblnew" runat="server"> <input type="hidden" id="lblsid" runat="server">
			<input type="hidden" id="lbluser" runat="server"> <input type="hidden" id="lbloldcomp" runat="server">
			<input type="hidden" id="lbloldkey" runat="server">
		<input type="hidden" id="lblfailmode" runat="server" />
            <input type="hidden" id="lblswitch" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblisecd" runat="server" />
            <input type="hidden" id="lblgetecd2" runat="server" />
            <input type="hidden" id="lblgetecd3" runat="server" />
            <input type="hidden" id="lblcurrecd2" runat="server" />
            <input type="hidden" id="lblnewecd3" runat="server" />
            <input type="hidden" id="lblecd1id" runat="server" />
            <input type="hidden" id="lblecd2txt" runat="server" />
            <input type="hidden" id="lblnewecd1id" runat="server" />
            <input type="hidden" id="lblcomi" runat="server" />
</form>
	</body>
</HTML>
