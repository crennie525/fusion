

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class complibtaskview2
    Inherits System.Web.UI.Page

    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim tmod As New transmod
    Dim sid, cid, fuid, tid, sql, username, eqid, ro, comid, typ, tpm, mopt As String
    Dim Filter, SubVal As String
    Dim ds As DataSet
    Dim dslev As DataSet
    Dim dr As SqlDataReader
    Dim gtasks As New Utilities
    Protected WithEvents lbltpm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmopt As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgtasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltool As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllube As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnote As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                tpm = Request.QueryString("tpm").ToString
                If tpm = "no" Then
                    tpm = "N"
                End If
                lbltpm.Value = tpm

            Catch ex As Exception
                lbltpm.Value = "N"
            End Try
            Try
                comid = Request.QueryString("comid").ToString
            Catch ex As Exception
                comid = "0"
            End Try
            Try
                typ = Request.QueryString("typ").ToString
            Catch ex As Exception
                typ = "comp"
            End Try
            Try
                mopt = Request.QueryString("mopt").ToString
            Catch ex As Exception
                mopt = "0"
            End Try
            lblcomid.Value = comid
            lblmopt.Value = mopt
            gtasks.Open()
            If typ = "comp" Then
                BindGrid()
            Else
                BindGrid2()
            End If
            gtasks.Dispose()


        End If
    End Sub
    Private Sub BindGrid()
        comid = lblcomid.Value
        mopt = lblmopt.Value
        'sql = "select * from comptasks where comid = '" & comid & "' and subtask = 0 order by tasknum"
        If mopt = "0" Then
            sql = "select * from comptasks where comid = '" & comid & "' and subtask = 0 and comid <> 0 and comid is not null order by tasknum"
        Else
            sql = "select * from comptasks where  " _
                + "tasktype is not null and taskdesc is not null " _
                + "and skill is not null and (ttime is not null and ttime <> 0) " _
                + "and rd is not null and comid = '" & comid & "' and subtask = 0 and comid <> 0 and comid is not null order by tasknum"
        End If
        
        ds = gtasks.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Try
            dgtasks.DataSource = dv
            dgtasks.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BindGrid2()
        comid = lblcomid.Value
        tpm = lbltpm.Value
        mopt = lblmopt.Value
        If tpm = "N" Then
            'sql = "select * from pmtasks where comid = '" & comid & "' and subtask = 0 order by tasknum"
            If mopt = "0" Then
                sql = "select * from pmtasks where comid = '" & comid & "' and subtask = 0 and comid <> 0 and comid is not null order by tasknum"
            Else
                sql = "select * from pmtasks where  " _
               + "tasktype is not null and taskdesc is not null " _
               + "and skill is not null and (ttime is not null and ttime <> 0) " _
               + "and rd is not null and taskstatus is not null and comid = '" & comid & "' and subtask = 0 and comid <> 0 and comid is not null order by tasknum"
            End If
           
        Else
            sql = "select * from pmtaskstpm where comid = '" & comid & "' and subtask = 0 and comid <> 0 and comid is not null order by tasknum"
        End If

        ds = gtasks.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Try
            dgtasks.DataSource = dv
            dgtasks.DataBind()
        Catch ex As Exception

        End Try
    End Sub
	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgtasks.Columns(1).HeaderText = dlabs.GetDGPage("complibtaskview2.aspx","dgtasks","1")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(3).HeaderText = dlabs.GetDGPage("complibtaskview2.aspx","dgtasks","3")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(4).HeaderText = dlabs.GetDGPage("complibtaskview2.aspx","dgtasks","4")
		Catch ex As Exception
		End Try
		Try
			dgtasks.Columns(5).HeaderText = dlabs.GetDGPage("complibtaskview2.aspx","dgtasks","5")
		Catch ex As Exception
		End Try

	End Sub

End Class
