

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.IO
Public Class complibcodets
    Inherits System.Web.UI.Page
	Protected WithEvents lang1725 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1724 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1723 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1722 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1721 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim comp As New Utilities
    Dim dr As SqlDataReader
    Dim eq, fu, sql, dt, val, filt, cid, co, cod, coid, sid, did, clid, ro, appstr, typ, Login, ck, mfg As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtcokey As System.Web.UI.WebControls.Label
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents txtconame As System.Web.UI.WebControls.Label
    Protected WithEvents txtdesc As System.Web.UI.WebControls.Label
    Protected WithEvents txtspl As System.Web.UI.WebControls.Label
    Protected WithEvents txtdesig As System.Web.UI.WebControls.Label
    Protected WithEvents txtmfg As System.Web.UI.WebControls.Label
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents imgco As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents geteq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsavetype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrbimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbliorders As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovtimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrtimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                coid = Request.QueryString("coid").ToString
            Catch ex As Exception
                coid = "0"
            End Try

            lblcoid.Value = coid
            If coid <> "0" Then
                comp.Open()
                PopComp(coid)
                comp.Dispose()
               
            Else
               
            End If
        End If
    End Sub
    Private Sub PopComp(ByVal coid As String)
        sql = "select * from complib where comid = '" & coid & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            txtcokey.Text = dr.Item("compkey").ToString
            txtconame.Text = dr.Item("compnum").ToString
            txtdesc.Text = dr.Item("compdesc").ToString
            txtdesig.Text = dr.Item("desig").ToString
            txtspl.Text = dr.Item("spl").ToString
            txtmfg.Text = dr.Item("mfg").ToString
        End While
        dr.Close()
        cid = "0"
        PopFailList(cid, coid)
        LoadPics()
    End Sub
    Private Sub PopFailList(ByVal cid As String, ByVal coid As String)
        Dim faillist As New Utilities
        Dim lang As String = lblfslang.Value
        'Dim coi As String = lblcomi.Value

        faillist.Open()
        sql = "select distinct compfailid, failuremode from complibfm where compid = '" & cid & "' and " _
               + "comid = '" & coid & "'"
        sql = "usp_getcfall_coco '" & coid & "','" & lang & "'"
        dr = faillist.GetRdrData(sql)
        lbfailmodes.DataSource = dr
        lbfailmodes.DataValueField = "compfailid"
        lbfailmodes.DataTextField = "failuremode"
        lbfailmodes.DataBind()
        dr.Close()
        faillist.Dispose()

    End Sub
    Private Sub LoadPics()
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/tpmimages/"
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        'lblnsimage.Value = nsimage
        'lblstrfrom.Value = strfrom
        'imgco.Attributes.Add("src", "../images/appimages/compimg.gif")
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgco.Attributes.Add("src", "../images2/eng/compimg.gif")
        ElseIf lang = "fre" Then
            imgco.Attributes.Add("src", "../images2/fre/compimg.gif")
        ElseIf lang = "ger" Then
            imgco.Attributes.Add("src", "../images2/ger/compimg.gif")
        ElseIf lang = "ita" Then
            imgco.Attributes.Add("src", "../images2/ita/compimg.gif")
        ElseIf lang = "spa" Then
            imgco.Attributes.Add("src", "../images2/spa/compimg.gif")
        End If

        Dim img, imgs, picid As String

        coid = lblcoid.Value
        Dim pcnt As Integer
        sql = "select count(*) from comppictures where comid is not null and comid = '" & coid & "'"
        pcnt = comp.Scalar(sql)
        lblpcnt.Value = pcnt

        Dim currp As String = lblcurrp.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpg.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lblpg.Text = "Image 1 of " & pcnt
                lblcurrp.Value = "0"
            End If

            sql = "select p.pic_id, p.picurl, p.picurltn, p.picurltm, p.picorder " _
            + "from comppictures p where p.comid is not null and p.comid = '" & coid & "'" _
            + "order by p.picorder"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs, timg As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs, ovtimg, ovtimgs
            dr = comp.GetRdrData(sql)
            While dr.Read
                iflag += 1
                img = dr.Item("picurltm").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)
                bimg = dr.Item("picurl").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)

                timg = dr.Item("picurltn").ToString
                Dim timgarr() As String = timg.Split("/")
                ovtimg = timgarr(timgarr.Length - 1)

                picid = dr.Item("pic_id").ToString
                order = dr.Item("picorder").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If

                If ovtimgs = "" Then
                    ovtimgs = ovtimg
                Else
                    ovtimgs += "," & ovtimg
                End If

                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimg.Value = ovimg
                    lblcurrbimg.Value = ovbimg
                    lblcurrtimg.Value = ovtimg
                    imgco.Attributes.Add("src", img)
                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgid.Value = picid

                    'txtiorder.Text = order
                End If
                If imgs = "" Then
                    imgs = picid & ";" & img
                Else
                    imgs += "~" & picid & ";" & img
                End If
            End While
            dr.Close()
            lblimgs.Value = imgs
            lblbimgs.Value = bimgs
            lblovimgs.Value = ovimgs
            lblovbimgs.Value = ovbimgs
            lblovtimgs.Value = ovtimgs
            lbliorders.Value = iorders
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1721.Text = axlabs.GetASPXPage("complibcodets.aspx", "lang1721")
        Catch ex As Exception
        End Try
        Try
            lang1722.Text = axlabs.GetASPXPage("complibcodets.aspx", "lang1722")
        Catch ex As Exception
        End Try
        Try
            lang1723.Text = axlabs.GetASPXPage("complibcodets.aspx", "lang1723")
        Catch ex As Exception
        End Try
        Try
            lang1724.Text = axlabs.GetASPXPage("complibcodets.aspx", "lang1724")
        Catch ex As Exception
        End Try
        Try
            lang1725.Text = axlabs.GetASPXPage("complibcodets.aspx", "lang1725")
        Catch ex As Exception
        End Try
        Try
            lblpg.Text = axlabs.GetASPXPage("complibcodets.aspx", "lblpg")
        Catch ex As Exception
        End Try

    End Sub

End Class
