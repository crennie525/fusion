<%@ Page Language="vb" AutoEventWireup="false" Codebehind="compclass.aspx.vb" Inherits="lucy_r12.compclass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>compclass</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="JavaScript" src="../scripts1/compclassaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  bgColor="transparent" onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table width="310" cellSpacing="0" cellPadding="0">
				<tr>
					<td width="120"></td>
					<td width="180"></td>
					<td width="20"></td>
				</tr>
				<tr>
					<td colSpan="3"><asp:label id="lblpre" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"
							Width="310px" ForeColor="Red"></asp:label></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang1673" runat="server">Search Component Class</asp:Label></td>
					<td><asp:textbox id="txtsrch" runat="server" Width="170px"></asp:textbox></td>
					<td><asp:imagebutton id="ibtnsearch" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td colSpan="3" align="center"><asp:datagrid id="dgac" runat="server" AutoGenerateColumns="False" AllowCustomPaging="True" AllowPaging="True"
							GridLines="None" CellPadding="0" ShowFooter="True" CellSpacing="1">
							<FooterStyle BackColor="transparent"></FooterStyle>
							<AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
							<ItemStyle CssClass="ptransrow"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Height="20px" Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										&nbsp;
										<asp:ImageButton id="Imagebutton34" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit This Asset Class"></asp:ImageButton>
									</ItemTemplate>
									<FooterTemplate>
										&nbsp;
										<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
											CommandName="Add"></asp:ImageButton>
									</FooterTemplate>
									<EditItemTemplate>
										&nbsp;
										<asp:ImageButton id="Imagebutton35" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton36" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<ItemTemplate>
										<asp:Label id=lblacidi runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.acid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id=lblacid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.acid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Component Class">
									<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										&nbsp;
										<asp:Label id=Label24 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.assetclass") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id=txtnewac runat="server" MaxLength="50" Width="154px" Text='<%# DataBinder.Eval(Container, "DataItem.assetclass") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										&nbsp;
										<asp:TextBox id=Textbox20 runat="server" MaxLength="50" Width="154px" Text='<%# DataBinder.Eval(Container, "DataItem.assetclass") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Remove">
									<HeaderStyle Width="64px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:ImageButton id="Imagebutton37" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
											CommandName="Delete"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False"></PagerStyle>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3" align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lblcid" runat="server" NAME="lblcid"><input type="hidden" id="txtpg" runat="server" NAME="Hidden1"><input type="hidden" id="txtpgcnt" runat="server" NAME="txtpgcnt">
			<input type="hidden" id="lblret" runat="server" NAME="lblret"><input type="hidden" id="lblold" runat="server" NAME="lblold">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
