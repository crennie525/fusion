<%@ Page Language="vb" AutoEventWireup="false" Codebehind="complibcodets.aspx.vb" Inherits="lucy_r12.complibcodets" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>complibcodets</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/complibcodetsaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<div style="WIDTH: 720px" id="FreezePane" class="FreezePaneOff" align="center">
				<div id="InnerFreezePane" class="InnerFreezePane"></div>
			</div>
			<div style="Z-INDEX: 1000; POSITION: absolute; VISIBILITY: hidden" id="overDiv"></div>
			<table style="POSITION: absolute; TOP: 10px; LEFT: 10px" id="eqdetdiv" cellSpacing="0"
				cellPadding="0" width="985">
				<tr>
					<td width="110"></td>
					<td width="90"></td>
					<td width="50"></td>
					<td width="160"></td>
					<td width="100"></td>
					<td width="210"></td>
					<td width="140"></td>
					<td width="120"></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang1721" runat="server">Search Key</asp:Label></td>
					<td colSpan="4"><asp:label id="txtcokey" runat="server" Width="180px" cssclass="plainlabel wd180"></asp:label></td>
					<td class="bluelabel" align="center"><asp:Label id="lang1722" runat="server">Component Failure Modes</asp:Label></td>
					<td rowSpan="7" colSpan="2" align="center">
						<table>
							<tr>
								<td><IMG id="imgco" onclick="getbig();" src="../images/appimages/compimg.gif" width="216"
										height="206" runat="server">
								</td>
							</tr>
							<tr>
								<td align="center">
									<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
										cellSpacing="0" cellPadding="0">
										<tr>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="134" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabel">Image 0 of 0</asp:label></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext.gif"
													runat="server"></td>
											<td width="20"><IMG id="ilast" onclick="getplast();" src="../images/appbuttons/minibuttons/llast.gif"
													runat="server"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang1723" runat="server">Common Name</asp:Label></td>
					<td colSpan="4"><asp:label id="txtconame" runat="server" Width="180px" cssclass="plainlabel wd180" MaxLength="100"></asp:label></td>
					<td align="center" rowspan="6" valign="top"><asp:listbox id="lbfailmodes" runat="server" Width="170px" CssClass="fmboxo" Height="90px" SelectionMode="Multiple"></asp:listbox></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang1724" runat="server">Description</asp:Label></td>
					<td colSpan="4"><asp:label id="txtdesc" runat="server" Width="250px" cssclass="plainlabel wd250" MaxLength="100"></asp:label></td>
				</tr>
				<tr>
					<td class="label" height="20">SPL</td>
					<td colSpan="4"><asp:label id="txtspl" runat="server" Width="250px" cssclass="plainlabel wd250" MaxLength="200"></asp:label></td>
				</tr>
				<tr>
					<td class="label" height="20"><asp:Label id="lang1725" runat="server">Designation</asp:Label></td>
					<td colSpan="4"><asp:label id="txtdesig" runat="server" Width="250px" cssclass="plainlabel wd250" MaxLength="200"></asp:label></td>
				</tr>
				<tr>
					<td class="label" height="20">Mfg</td>
					<td colSpan="4"><asp:label id="txtmfg" runat="server" Width="250px" cssclass="plainlabel wd250" MaxLength="200"></asp:label></td>
				</tr>
				<tr>
					<td colSpan="5" align="right" height="50">&nbsp;</td>
				</tr>
			</table>
			<input id="lblcoid" type="hidden" runat="server" NAME="lblcoid"> <input id="lblsavetype" type="hidden" runat="server" NAME="lblsavetype">
			<input id="lblpcnt" type="hidden" name="lblpcnt" runat="server"> <input id="lblcurrp" type="hidden" name="lblcurrp" runat="server">
			<input id="lblimgs" type="hidden" name="lblimgs" runat="server"> <input id="lblimgid" type="hidden" name="lblimgid" runat="server">
			<input id="lblovimgs" type="hidden" name="lblovimgs" runat="server"> <input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server">
			<input id="lblcurrimg" type="hidden" name="lblcurrimg" runat="server"> <input id="lblcurrbimg" type="hidden" name="lblcurrbimg" runat="server">
			<input id="lblbimgs" type="hidden" name="lblbimgs" runat="server"><input id="lbliorders" type="hidden" name="lbliorders" runat="server">
			<input id="lbloldorder" type="hidden" name="lbloldorder" runat="server"> <input id="lblovtimgs" type="hidden" name="lblovtimgs" runat="server">
			<input id="lblcurrtimg" type="hidden" name="lblcurrtimg" runat="server"> <input id="lblcompchk" type="hidden" runat="server" NAME="lblcompchk">
			<input type="hidden" id="lbllog" runat="server" NAME="lbllog"> <input type="hidden" id="lblro" runat="server" NAME="lblro">
			<input type="hidden" id="lblfslang" runat="server" />
            <input type="hidden" id="lblcomi" runat="server" />
		</form>
	</body>
</HTML>
