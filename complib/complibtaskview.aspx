<%@ Page Language="vb" AutoEventWireup="false" Codebehind="complibtaskview.aspx.vb" Inherits="lucy_r12.complibtaskview" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>complibtaskview</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 0px; LEFT: 0px">
				<tr>
					<td><asp:datagrid id="dgtasks" runat="server" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="1"
							GridLines="None">
							<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
							<ItemStyle CssClass="ptransrow"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit" Visible="False">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:imagebutton id="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:imagebutton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="Task#">
									<HeaderStyle CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblta runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox cssclass="plainlabel wd40" id=lblt runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>' width="40px">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Step#">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblsubt runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox cssclass="plainlabel wd40" id=lblst runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>' width="40px">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Task Description">
									<HeaderStyle CssClass="btmmenu plainlabel" Width="700px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label3 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>' Width="270px">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox cssclass="plainlabel wd260" id=txtdesc runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>' Height="70px" TextMode="MultiLine" width="260px" MaxLength="1000">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="pmtskid">
									<ItemTemplate>
										<asp:Label id="lbltida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblttid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Delete" Visible="False">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:ImageButton id="ibDel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
			<input id="lblcid" type="hidden" runat="server" NAME="lblcid"> <input id="lbltid" type="hidden" runat="server" NAME="lbltid">
			<input id="lblfuid" type="hidden" runat="server" NAME="lblfuid"> <input id="lblfilt" type="hidden" runat="server" NAME="lblfilt">
			<input id="lblsubval" type="hidden" runat="server" NAME="lblsubval"><input id="lblsid" type="hidden" runat="server" NAME="lblsid">
			<input id="lbloldtask" type="hidden" runat="server" NAME="lbloldtask"><input id="lblpart" type="hidden" name="lblpart" runat="server">
			<input id="lbltool" type="hidden" name="lbltool" runat="server"><input id="lbllube" type="hidden" name="lbllube" runat="server">
			<input id="lblnote" type="hidden" name="lblnote" runat="server"><input id="lbltasknum" type="hidden" runat="server" NAME="lbltasknum">
			<input id="lblpmtid" type="hidden" runat="server" NAME="lblpmtid"><input id="lbllog" type="hidden" runat="server" NAME="lbllog">
			<input id="lbllock" type="hidden" name="lbllock" runat="server"> <input id="lbllockedby" type="hidden" name="lbllockedby" runat="server">
			<input id="lblusername" type="hidden" runat="server" NAME="lblusername"><input id="lbleqid" type="hidden" runat="server" NAME="lbleqid">
			<input type="hidden" id="lblro" runat="server" NAME="lblro"> <input type="hidden" id="lblcomid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
