<%@ Page Language="vb" AutoEventWireup="false" Codebehind="complibsrchkey.aspx.vb" Inherits="lucy_r12.complibsrchkey" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>complibsrchkey</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" src="../scripts1/complibsrchkeyaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table width="320" cellSpacing="0" cellPadding="0">
				<tr>
					<td>
						<table>
							<tr>
								<td class="label" width="80"><asp:Label id="lang1741" runat="server">Search</asp:Label></td>
								<td width="200"><asp:textbox id="txtsrch" runat="server" CssClass="plainlabel" Width="200px"></asp:textbox></td>
								<td width="20"><asp:imagebutton id="ibtnsearch" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm1.gif"></asp:imagebutton></td>
								<td width="20"><IMG onclick="resetsrch();" alt="" src="../images/appbuttons/minibuttons/switch.gif"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="btmmenu plainlabel" id="tdtitle" runat="server" height="20">Current 
						Search Keys</td>
				</tr>
				<tr>
					<td id="tdlookup" runat="server"><div id="taskdiv" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 320px; HEIGHT: 320px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
							runat="server"></div>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="190"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lbllog" runat="server" NAME="lbllog"><input type="hidden" id="lblstr" runat="server" NAME="lblstr">
			<input type="hidden" id="txtpg" runat="server" NAME="txtpg"> <input type="hidden" id="txtpgcnt" runat="server" NAME="txtpgcnt">
			<input type="hidden" id="lblret" runat="server"><input type="hidden" id="lblwho" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
