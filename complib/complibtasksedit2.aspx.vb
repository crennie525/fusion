Imports System.Data.SqlClient
Public Class complibtasksedit2
    Inherits System.Web.UI.Page
    Dim tl, cid, sid, did, clid, eqid, fuid, coid, sql, val, name, field, ttid, tnum, username, lid, typ, ro, appstr, intyp As String
    Dim co, fail, chk, tc, tpmhold, ghostoff, xstatus, who, rteid, rbskillid, rbqty, rbfreq, rbrdid As String
    Dim tskcnt As Integer
    Dim tasks As New Utilities
    Dim tasksadd As New Utilities
    Dim comi As New mmenu_utils_a
    Public dr As SqlDataReader
    Dim ds As DataSet
    Dim Tables As String = "pmtasks"
    Dim PK As String = "pmtskid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 1
    Dim Fields As String = "*, haspm = (select e.haspm from equipment e where e.eqid = pmtasks.eqid), usetot = (select e.usetot from equipment e where e.eqid = pmtasks.eqid)"
    Dim Filter As String = ""
    Dim CntFilter As String = ""
    Protected WithEvents imgdeltpm As System.Web.UI.HtmlControls.HtmlImage
    'imgdeltask
    Protected WithEvents imgdeltask As System.Web.UI.HtmlControls.HtmlImage
    'Protected WithEvents btndeltask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents msglbl As System.Web.UI.WebControls.Label
    Protected WithEvents trh4 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents sgrid As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents sgrid1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdcompnum As System.Web.UI.HtmlControls.HtmlTableCell
    Dim Group As String = ""
    Protected WithEvents lblcompnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblghostoff As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblxstatus As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btntpm As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblrteid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrbskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrbqty As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrbfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrbrdid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Sort As String = "tasknum, subtask asc"
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtcQty As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbofailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents ibfromo As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibtoo As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbCompFM As System.Web.UI.WebControls.ListBox
    Protected WithEvents ibReuse As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfaillist As System.Web.UI.WebControls.ListBox
    Protected WithEvents ibToTask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibFromTask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox

    Protected WithEvents ddpto As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddtypeo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddpt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddskillo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtqtyo As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttro As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfreqo As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddeqstato As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtordt As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddtaskstat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtqty As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttr As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpfint As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfreq As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddeqstat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtrdt As System.Web.UI.WebControls.TextBox
    Protected WithEvents cbloto As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cbcs As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents lblcnt As System.Web.UI.WebControls.Label
    Protected WithEvents txttaskorder As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label26 As System.Web.UI.WebControls.Label
    Protected WithEvents lblsubcount As System.Web.UI.WebControls.Label
    Protected WithEvents btnaddtsk As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibCancel As System.Web.UI.WebControls.ImageButton
    Protected WithEvents todis1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnaddnewfail As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromreusedis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtodesc As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents txtdesc As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents btnlookup As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnlookup2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdtpmhold As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnStart As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnPrev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnNext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnEnd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnedittask As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnsav As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblsvchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblenable As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpgholder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblst As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltabid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsave As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgrid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcind As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrsb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldocpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldocpmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsesscnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhaspm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnoeq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltpmalert As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltpmhold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusetot As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewkey As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewdesc As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lbltpmpmtskid As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillqty As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmeterid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrdid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfunc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusemeter As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnum As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lblusemetero As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmeterido As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lblskillido As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillqtyo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrdo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrdido As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lblmfido As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmfid As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lblfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfreqo As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents cbfixed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbfixedo As System.Web.UI.HtmlControls.HtmlInputCheckBox

    Protected WithEvents lblfixed As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfixedo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblintyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompchng As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim coi As String = comi.COMPI
        lblcomi.Value = coi
        If coi = "GSK" Then
            lblghostoff.Value = "yes"
            lblxstatus.Value = "yes"
        End If
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
        Page.EnableViewState = True
        If Request.Form("lbldel") = "delr" Then
            lbldel.Value = ""
            DelRevised()
        End If
        Dim login As String
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
            username = HttpContext.Current.Session("username").ToString()
            lblusername.Value = username
        Catch ex As Exception
            'Server.Transfer("/laipm3/NewLogin.aspx")
            lbllog.Value = "no"
            Exit Sub
        End Try
        If lbllog.Value <> "no" Then
            If lblsvchk.Value = "1" Then
                goNext()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "2" Then
                goPrev()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "3" Then
                'goSubNext()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "4" Then
                'goSubPrev()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "5" Then
                GoFirst()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "6" Then
                GoLast()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "7" Then
                GoNav()
                'lblsave.Value = "no"
            End If
            If Request.Form("lblcompchk") = "1" Then
                lblcompchk.Value = "0"
                tasks.Open()
                PopComp()
                'added in case components with tasks were copied on return
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                If PageNumber = 0 Then
                    PageNumber = 1
                End If
                'fuid = lblfuid.Value
                'Filter = "funcid = " & fuid & " and subtask = 0" '"comid = " & coid & " and 
                'CntFilter = "funcid = " & fuid & " and subtask = 0"
                LoadPage(PageNumber, Filter)
                'end new
                coid = lblco.Value
                Try
                    'ddcomp.SelectedValue = coid
                    'SaveTaskComp()
                Catch ex As Exception

                End Try
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "2" Then
                lblcompchk.Value = "0"
                tasks.Open()
                PopComp()
                'added in case components deleted on return
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                'end new
                tasks.Dispose()
                coid = lblco.Value
                Try
                    'ddcomp.SelectedValue = coid
                Catch ex As Exception

                End Try
            ElseIf Request.Form("lblcompchk") = "3" Then
                lblcompchk.Value = "0"
                tasksadd.Open()
                SaveTask2()
                tasksadd.Dispose()
            ElseIf Request.Form("lblcompchk") = "4" Then
                lblcompchk.Value = "0"
                tasks.Open()
                GetLists()
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "5" Then
                lblcompchk.Value = "0"
                tasks.Open()
                SubCount(PageNumber, Filter)
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "6" Then
                lblcompchk.Value = "0"
                tasks.Open()
                'LoadCommon()
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "7" Then
                lblcompchk.Value = "0"
                tasks.Open()
                DeleteTask()

                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "uptpm" Then
                lblcompchk.Value = "0"
                tasks.Open()
                UpTPM()
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "deltpm" Then
                lblcompchk.Value = "0"
                tasks.Open()
                DelTPM()
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                tasks.Dispose()

            End If

            If Request.Form("lblcompfailchk") = "1" Then
                lblcompfailchk.Value = "0"
                AddFail()
            End If
            Dim start As String = Request.QueryString("start").ToString

            If start = "no" Then
                lblpg.Text = "0"
                lblcnt.Text = "0"
                'lblspg.Text = "0"
                'lblscnt.Text = "0"
            Else
                lblstart.Value = "no"
            End If

            If Not IsPostBack Then
                Try
                    who = Request.QueryString("who").ToString
                    If who = "ed" Then
                        lblwho.Value = "ed"
                        lblghostoff.Value = "yes"
                        lblxstatus.Value = "yes"
                    End If
                Catch ex As Exception

                End Try
                appstr = HttpContext.Current.Session("appstr").ToString()
                CheckApps(appstr)
                'lblsave.Value = "no"
                If start = "yes" Then
                    Try
                        intyp = Request.QueryString("typ").ToString
                    Catch ex As Exception

                    End Try
                    lblintyp.Value = intyp
                    lblcnt.Text = "0"
                    lblsvchk.Value = "0"
                    lblenable.Value = "1"
                    tl = "5" 'Request.QueryString("tl").ToString
                    lbltasklev.Value = tl

                    sid = HttpContext.Current.Session("dfltps").ToString()
                    lblsid.Value = sid

                    coid = Request.QueryString("coid").ToString
                    lblcoid.Value = coid
                    lblco.Value = co
                    fuid = Request.QueryString("fuid").ToString
                    lblfuid.Value = fuid
                    Dim comp As String = Request.QueryString("comp").ToString
                    tdcompnum.InnerHtml = comp
                    Dim pmid, pmstr As String

                    lblsb.Value = "0"
                    Filter = "comid = " & coid & " and subtask = 0" '"comid = " & coid & " and 
                    CntFilter = "comid = " & coid & " and subtask = 0"
                    lblfilt.Value = Filter
                    lblfiltcnt.Value = CntFilter

                    tasks.Open()

                    GetLists()
                    lblsb.Value = "0"

                    Dim tasknum As String
                    Try
                        tasknum = Request.QueryString("task").ToString
                        If tasknum = "" Then
                            tasknum = "0"
                        End If
                        PageNumber = tasknum
                    Catch ex As Exception
                        tasknum = "0"
                    End Try
                    Try
                        tasknum = Request.QueryString("tasknum").ToString
                        If tasknum = "" Then
                            tasknum = "0"
                        End If
                        PageNumber = tasknum
                    Catch ex As Exception
                        tasknum = "0"
                    End Try

                    '**********Special Ops Section*****************

                    GetData(coid)

                    If intyp = "tasks" Then
                        AddCompTask()
                    ElseIf intyp = "tasks2" Then
                        lblsb.Value = "0"
                        fuid = RTrim(fuid)
                        Filter = "funcid = ''" & fuid & "'' and subtask = 0" '"comid = " & coid & " and 
                        CntFilter = "funcid = '" & fuid & "' and subtask = 0"
                        'CntFilter = "comid = " & coid & " and subtask = 0"
                        'Filter = "subtask = 0" 'comid = " & coid & " and 
                        'CntFilter = "comid = " & coid & " and subtask = 0"
                        lblfilt.Value = Filter
                        lblfiltcnt.Value = CntFilter
                        PageNumber = tasknum
                        LoadPage(PageNumber, Filter)
                    Else
                        LoadPage(PageNumber, Filter)
                    End If

                    '**********************************************

                    btnaddtsk.Enabled = True
                    Dim lock, lockby As String
                    Dim user As String = lblusername.Value
                    'Read Only
                    Try
                        ro = HttpContext.Current.Session("ro").ToString
                    Catch ex As Exception
                        ro = "0"
                    End Try
                    lblro.Value = ro

                    'End Read Only
                    lock = CheckLock(eqid)
                    If lock = "1" Then
                        lockby = lbllockedby.Value
                        If lockby = user Then
                            lbllock.Value = "0"
                        Else
                            lblro.Value = "1" '***use instead?
                            ro = "1"
                            Dim strMessage As String = "This record is locked for editing by " & lockby & "."
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        End If
                    ElseIf lock = "0" Then
                        'LockRecord(user, eq)
                    End If
                    If ro = "1" Then
                        ibToTask.Visible = False
                        ibFromTask.Visible = False
                        ibfromo.Visible = False
                        ibtoo.Visible = False
                        ibReuse.Visible = False
                        todis.Attributes.Add("class", "view")
                        fromdis.Attributes.Add("class", "view")
                        fromreusedis.Attributes.Add("class", "view")
                        todis1.Attributes.Add("class", "view")
                        fromdis1.Attributes.Add("class", "view")
                        btnedittask.Attributes.Add("src", "../images/appbuttons/minibuttons/lilpendis.gif")
                        btnaddtsk.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                        btnaddtsk.Enabled = False
                        'imgdeltask.Attributes.Add("src", "../images/appbuttons/minibuttons/deldis.gif")
                        'imgdeltask.Attributes.Add("onclick", "")
                        ibCancel.Attributes.Add("src", "../images/appbuttons/minibuttons/candisk1dis.gif")
                        ibCancel.Enabled = False
                        btnsav.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                        btnsav.Attributes.Add("onclick", "")
                        'btntpm.Enabled = False
                    End If
                    tasks.Dispose()

                Else



                    'tdtaskstat.InnerHtml = "No Task Records Selected Yet"
                    'ddtaskstat.SelectedValue = "Select"
                    btnaddtsk.Enabled = True
                    lblpg.Text = "0"
                    lblcnt.Text = "0"
                    'lblspg.Text = "0"
                    'lblscnt.Text = "0"
                End If

                Dim locked As String = lbllock.Value
                If locked = "1" Then
                    'btnedittask.Attributes.Add("class", "details")
                    'btnaddtsk.Attributes.Add("class", "details")
                    'ibCancel.Attributes.Add("class", "details")
                    'IMG2.Attributes.Add("class", "details")
                    'ggrid.Attributes.Add("class", "details")
                    'btnaddcomp.Attributes.Add("class", "details")
                    'imgrat.Attributes.Add("class", "details")
                End If
                'Disable all fields
                If ro <> "1" Then
                    DisableOptions()
                    txttaskorder.Enabled = False
                    ddtaskstat.Enabled = False 'orig
                    'ddcomp.Enabled = False
                    'lbfaillist.Enabled = False
                    'lbfailmodes.Enabled = False
                    ibToTask.Enabled = False
                    ibFromTask.Enabled = False
                    ibReuse.Enabled = False
                    txtdesc.Disabled = True
                    txtrdt.Enabled = False
                    txtordt.Enabled = False
                    txtodesc.Disabled = True 'orig
                    ddtype.Enabled = False
                    ddtypeo.Enabled = False 'orig
                    txtfreq.Enabled = False
                    txtfreqo.Enabled = False 'orig

                    cbfixed.Disabled = True
                    cbfixedo.Disabled = True

                    txtpfint.Enabled = False
                    ddskill.Enabled = False
                    ddskillo.Enabled = False 'orig
                    txtqty.Enabled = False
                    txtqtyo.Enabled = False 'orig
                    txttr.Enabled = False
                    txttro.Enabled = False 'orig
                    ddpt.Enabled = False
                    ddpto.Enabled = False 'orig
                    ddeqstat.Enabled = False
                    ddeqstato.Enabled = False 'orig
                    lblenable.Value = "1"
                    txtcQty.Enabled = False
                    'lbCompFM.Enabled = False
                    'lbofailmodes.Enabled = False 'orig
                    'buttons
                    btnaddtsk.Enabled = True
                    'btnaddsubtask.Enabled = False
                    'btndeltask.Enabled = False
                    ibCancel.Enabled = False
                    'btnsavetask.Enabled = False
                    'btntpm.Enabled = False
                End If

                'end disable
                'btnsavetask.Attributes.Add("onmouseover", "return overlib('Save Changes for this Task')")
                'btnsavetask.Attributes.Add("onmouseout", "return nd()")
                'btntpm.Attributes.Add("onmouseover", "return overlib('Send Task to Operator Care Module (TPM)')")
                'btntpm.Attributes.Add("onmouseout", "return nd()")
                'btndeltask.Attributes.Add("onmouseover", "return overlib('Delete this Task')")
                'btndeltask.Attributes.Add("onmouseout", "return nd()")
                'btnaddsubtask.Attributes.Add("onmouseover", "return overlib('Add a Sub-Task to this Task')")
                'btnaddsubtask.Attributes.Add("onmouseout", "return nd()")
                btnaddtsk.Attributes.Add("onmouseover", "return overlib('Add a New Task to the Selected Component')")
                btnaddtsk.Attributes.Add("onmouseout", "return nd()")
                btnaddtsk.Attributes.Add("onclick", "FreezeScreen('Your Data is Being Processed...');")
                btnedittask.Attributes.Add("onmouseover", "return overlib('Edit this Task')")
                btnedittask.Attributes.Add("onmouseout", "return nd()")
                ibToTask.Attributes.Add("onmouseover", "return overlib('Add a Failure Mode to this Task')")
                ibToTask.Attributes.Add("onmouseout", "return nd()")
                'ibToTask.Attributes.Add("onclick", "DisableButton(this);")
                ibReuse.Attributes.Add("onmouseover", "return overlib('Add a Failure Mode to this Task that was addessed in another Task')")
                ibReuse.Attributes.Add("onmouseout", "return nd()")
                'ibReuse.Attributes.Add("onclick", "DisableButton(this);")
                ibCancel.Attributes.Add("onmouseout", "return nd()")
                ibCancel.Attributes.Add("onmouseover", "return overlib('Cancel Changes')")
                ibFromTask.Attributes.Add("onmouseover", "return overlib('Remove a Failure Mode from this Task')")
                ibFromTask.Attributes.Add("onmouseout", "return nd()")
                'ibFromTask.Attributes.Add("onclick", "DisableButton(this);")
                btnPrev.Attributes.Add("onmouseover", "return overlib('Go to Previous Top-Level Task')")
                btnPrev.Attributes.Add("onmouseout", "return nd()")
                btnPrev.Attributes.Add("onclick", "confirmExit('tb', 'opt');")
                btnStart.Attributes.Add("onmouseover", "return overlib('Go to First Top-Level Task')")
                btnStart.Attributes.Add("onmouseout", "return nd()")
                btnStart.Attributes.Add("onclick", "confirmExit('ts', 'opt');")
                btnNext.Attributes.Add("onmouseover", "return overlib('Go to Next Top-Level Task')")
                btnNext.Attributes.Add("onmouseout", "return nd()")
                btnNext.Attributes.Add("onclick", "confirmExit('tf', 'opt');")
                btnEnd.Attributes.Add("onmouseover", "return overlib('Go to Last Top-Level Task')")
                btnEnd.Attributes.Add("onmouseout", "return nd()")
                btnEnd.Attributes.Add("onclick", "confirmExit('te', 'opt');")
                'btnsubprev.Attributes.Add("onmouseover", "return overlib('Go to Previous Sub-Task')")
                'btnsubprev.Attributes.Add("onmouseout", "return nd()")
                'btnsubprev.Attributes.Add("onclick", "confirmExit('sb');")
                'btnsubnext.Attributes.Add("onmouseover", "return overlib('Go to Next Sub-Task')")
                'btnsubnext.Attributes.Add("onmouseout", "return nd()")
                'btnsubnext.Attributes.Add("onclick", "confirmExit('sf');")
                'btnlookup.Attributes.Add("onmouseover", "return overlib('Get Task Descriptions Similar to this one. - Use keywords for best results')")
                'btnlookup.Attributes.Add("onmouseout", "return nd()")
                btnlookup2.Attributes.Add("onmouseover", "return overlib('Open the Common Tasks Dialog')")
                btnlookup2.Attributes.Add("onmouseout", "return nd()")
                'imgrat.Attributes.Add("onclick", "confirmExit('rat');")
                ibtoo.Attributes.Add("onmouseover", "return overlib('Add An Original Failure Mode to this Task')")
                ibtoo.Attributes.Add("onmouseout", "return nd()")
                'ibtoo.Attributes.Add("onclick", "DisableButton(this);")
                ibfromo.Attributes.Add("onmouseover", "return overlib('Remove An Original Failure Mode from this Task')")
                ibfromo.Attributes.Add("onmouseout", "return nd()")
                'ibfromo.Attributes.Add("onclick", "DisableButton(this);")
                'ddcomp.Attributes.Add("onchange", "chngchk();")
                'ddtypeo.Attributes.Add("onchange", "GetType('o', this.value);")
                ddtaskstat.Attributes.Add("onchange", "CheckDel(this.value);")
                ddtype.Attributes.Add("onchange", "GetType('d', this.value);")
                ' ddpto.Attributes.Add("onchange", "document.getElementById('ddpt').value=this.value;")
                'ddskillo.Attributes.Add("onchange", "document.getElementById('ddskill').value=this.value;")

                'ddeqstato.Attributes.Add("onchange", "document.getElementById('ddeqstat').value=this.value;zerodt('o', this.value);")
                'txtqtyo.Attributes.Add("onchange", "document.getElementById('txtqty').value=this.value;")
                'txttro.Attributes.Add("onkeyup", "filldown('o', this.value);")
                txttr.Attributes.Add("onkeyup", "filldown('r', this.value);")
                ddeqstat.Attributes.Add("onchange", "zerodt('d', this.value);")

                'txtordt.Attributes.Add("onkeyup", "document.getElementById('txtrdt').value=this.value;")


                'txtfreqo.Attributes.Add("onchange", "document.getElementById('txtfreq').value=this.value;")

                txtcQty.Attributes.Add("onkeyup", "checkcqty(this.value);")
            End If
        End If

    End Sub
    Private Sub AddCompTask()
        lblenable.Value = "0"
        tl = lbltasklev.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        coid = lblcoid.Value
        Dim compnum As String = lblcompnum.Value
        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        tl = 5
        'tasksadd.Open()
        sql = "usp_AddTask2 '" & tl & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & ustr & "', '" & coid & "', '" & compnum & "'"
        tasks.Scalar(sql)
        Filter = lblfilt.Value

        PageNumber = lblcnt.Text
        PageNumber = PageNumber + 1 '1 '
        lblcnt.Text = PageNumber
        field = "comid"
        val = coid
        intyp = lblintyp.Value
        If intyp = "tasks2" Then
            Filter = "funcid = ''" & fuid & "'' and subtask = 0" '"comid = " & coid & " and 
            CntFilter = "funcid = '" & fuid & "' and subtask = 0"
            lblfiltcnt.Value = CntFilter
        Else
            Filter = field & " = " & val
            lblfiltcnt.Value = Filter
        End If

        lblfilt.Value = Filter

        LoadPage(PageNumber, Filter)
        'tasksadd.Dispose()
    End Sub
    Private Sub GetData(ByVal comid As String)
        sql = "select c.compnum, c.func_id, f.eqid, e.dept_id, e.cellid, e.compid, e.siteid from components c " _
        + "left join functions f on f.func_id = c.func_id " _
        + "left join equipment e on e.eqid = f.eqid " _
        + "where c.comid = '" & comid & "'"
        dr = tasks.GetRdrData(sql)
        While dr.Read
            lblcompnum.Value = dr.Item("compnum").ToString
            lblfuid.Value = dr.Item("func_id").ToString
            lblcid.Value = dr.Item("compid").ToString
            lblsid.Value = dr.Item("siteid").ToString
            lbldid.Value = dr.Item("dept_id").ToString
            lblclid.Value = dr.Item("cellid").ToString
            lbleqid.Value = dr.Item("eqid").ToString
        End While
        dr.Close()
    End Sub
    Private Sub DelTPM()
        fuid = lblfuid.Value
        tnum = lblt.Value
        ttid = lbltaskid.Value
        Dim st As Integer = "0" 'lblsb.Value
        Dim tpmpmtskid As String = lbltpmpmtskid.Value
        sql = "usp_delTPMTaskPM '" & fuid & "', '" & tnum & "', '" & st & "', '" & tpmpmtskid & "'"
        'sql = "usp_delTPMTaskPM '" & fuid & "', '" & tnum & "', '" & st & "'"
        tasks.Update(sql)
        'sql = "update pmtasks set tpmhold = '0' where funcid = '" & fuid & "' and tasknum = '" & tnum & "'"
        sql = "update pmtasks set tpmhold = '0' where funcid = '" & fuid & "' and tasknum = '" & tnum & "'" ' and pmtskid = '" & ttid & "'"
        tasks.Update(sql)
    End Sub
    Private Sub UpTPM()
        Dim usemeter As String = lblusemeter.Value
        If usemeter <> "1" Then
            eqid = lbleqid.Value
            cid = lblcid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            fuid = lblfuid.Value
            ttid = lbltaskid.Value
            Dim ttyp As String = ddtype.SelectedValue.ToString
            'If ttyp = "2" Or ttyp = "3" Or ttyp = "5" Then
            Dim tasknum As String = lblt.Value
            Dim user As String = HttpContext.Current.Session("username").ToString '"PM Administrator"
            'create procedure [dbo].[usp_copyTPMTask] (@cid int, 
            '@sid int, @did int, @clid int, @eqid int , @fuid int, @user varchar(50), @pmtskid int) as
            sql = "usp_copyTPMTask '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & user & "','" & ttid & "','" & tasknum & "'"
            tasks.Update(sql)
            'ddskill.SelectedValue = "2"
            'SaveTask2()
            'Else
            'Dim strMessage As String = "Please Change Task Type to 1C Clean, 1L Lubricate, or 2 - Subj Inspection"
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'End If
        Else
            Dim strMessage As String = "Can`t Add Task with Meter Based Frequency to TPM"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If


    End Sub
    Private Sub CheckApps(ByVal appstr As String)
        Dim apparr() As String = appstr.Split(",")
        Dim e As String = "0"
        Dim t As String = "0"
        'eq,dev,opt,inv
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                If apparr(i) = "eq" Then
                    e = "1"
                End If
                If apparr(i) = "tpd" Or apparr(i) = "tpo" Then
                    t = "1"
                End If
            Next
            If e <> "1" Then
                lblnoeq.Value = "1"
                'btnaddcomp.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                'btnaddcomp.Attributes.Add("onclick", "")
                btnaddnewfail.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                btnaddnewfail.Attributes.Add("onclick", "")
                'imgcopycomp.Attributes.Add("onclick", "")
            End If
            If t <> "1" Then
                lblnoeq.Value = "1"
                'btntpm.Attributes.Add("src", "../images/appbuttons/minibuttons/compresstpmdis.gif")
                'btntpm.Enabled = False
                'imgdeltpm.Attributes.Add("src", "../images/appbuttons/minibuttons/cantpmdis.gif")
                'imgdeltpm.Attributes.Add("onclick", "")
            End If
        Else
            lblnoeq.Value = "0"
        End If

    End Sub
    Private Function CheckLock(ByVal eqid As String) As String
        Dim lock As String
        sql = "select locked, lockedby from equipment where eqid = '" & eqid & "'"
        Try
            dr = tasks.GetRdrData(sql)
            While dr.Read
                lock = dr.Item("locked").ToString
                lbllock.Value = dr.Item("locked").ToString
                lbllockedby.Value = dr.Item("lockedby").ToString
            End While
            dr.Close()
        Catch ex As Exception

        End Try
        Return lock
    End Function
    Private Sub GoToGrid()

        chk = lblchk.Value
        cid = lblcid.Value
        Dim tid As String = lbltaskid.Value
        Dim funid As String = lblfuid.Value
        Dim comid As String = lblco.Value
        clid = lblclid.Value

        Response.Redirect("../apps/GTasksFunc2.aspx?tli=5a&funid=" & funid & "&comid=no&cid=" & cid & "&chk=" & chk & "&date=" & Now() & " Target='_top'")
    End Sub
    Private Sub DisableOptions()
        'btnaddtsk.Enabled = False
        'btnaddsubtask.Enabled = False
        'btndeltask.Enabled = False
        'ibCancel.Enabled = False
        'btnsavetask.Enabled = False
    End Sub
    Private Sub EnableOptions()
        btnaddtsk.Enabled = True
        'btnaddsubtask.Enabled = True
        'btndeltask.Enabled = True
        ibCancel.Enabled = True
        'btnsavetask.Enabled = True
    End Sub
    Private Function SubCount(ByVal PageNumber As Integer, ByVal Filter As String) As Integer
        Dim scnt As Integer
        fuid = lblfuid.Value
        Try
            sql = "select count(*) from pmTasks where funcid = '" & fuid & "' and tasknum = " & PageNumber & " and subtask <> '0'"
            Try
                scnt = tasks.Scalar(sql)
            Catch ex As Exception
                scnt = tasksadd.Scalar(sql)
            End Try
            lblsubcount.Text = scnt
        Catch ex As Exception
            scnt = 0
        End Try

        Return scnt
    End Function
    Private Function SubTaskCnt(ByVal PageNumber As Integer, ByVal Filter As String) As Integer
        Dim tcnt As Integer
        sql = "select count(*) from pmTasks where " & Filter & " and tasknum <= " & PageNumber & " and subtask = '0'"
        tcnt = tasks.Scalar(sql)
        Dim scnt As Integer
        sql = "select count(*) from pmTasks where " & Filter & " and tasknum < " & PageNumber & " and subtask <> '0'"
        scnt = tasks.Scalar(sql)
        Dim wcnt = tcnt + scnt
        Return wcnt
    End Function


    Private Sub GoNav()
        Dim pg As String = pgflag.Value
        If pg <> "0" Then
            Filter = lblfilt.Value
            lblsb.Value = "0"
            PageNumber = pg
            tasks.Open()
            'Filter = Filter & " and tasknum = " & PageNumber
            LoadPage(PageNumber, Filter)
            tasks.Dispose()
            lblenable.Value = "1"
        End If
    End Sub
    Private Sub GoFirst()
        If Len(lblfuid.Value) <> 0 Then
            Filter = lblfilt.Value
            lblsb.Value = "0"
            PageNumber = 1
            'Filter = Filter & " and tasknum = " & PageNumber
            tasks.Open()
            LoadPage(PageNumber, Filter)
            lblenable.Value = "1"
            tasks.Dispose()
        End If
    End Sub
    Private Sub GoLast()
        If Len(lblfuid.Value) <> 0 Then
            Filter = lblfilt.Value
            lblsb.Value = "0"
            PageNumber = lblcnt.Text
            'Filter = Filter & " and tasknum = " & PageNumber
            tasks.Open()
            LoadPage(PageNumber, Filter)
            lblenable.Value = "1"
            tasks.Dispose()
        End If
    End Sub
    Private Sub DeleteTask()
        ro = lblro.Value
        If ro <> "1" Then
            fuid = lblfuid.Value
            tnum = lblt.Value
            Dim pmtskid As String = lbltaskid.Value
            'RBAS
            rteid = lblrteid.Value
            If rteid <> "0" And rteid <> "" Then
                rbskillid = lblrbskillid.Value
                rbqty = lblrbqty.Value
                rbfreq = lblrbfreq.Value
                rbrdid = lblrbrdid.Value
                RBASRem(pmtskid, rteid, rbskillid, rbqty, rbfreq, rbrdid)
            End If
            'sql = "sp_delPMTask '" & fuid & "', '" & tnum & "', '0'"
            sql = "usp_delpmtask '" & fuid & "', '" & pmtskid & "', '" & tnum & "'"
            tasks.Update(sql)
            PageNumber = lblpg.Text
            tskcnt = lblcnt.Text
            tskcnt = tskcnt - 1
            lblcnt.Text = tskcnt
            If PageNumber <= tskcnt Then
                PageNumber = PageNumber
            Else
                PageNumber = tskcnt
            End If
            If PageNumber = 0 Then
                PageNumber = 1
            End If
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
            Filter = lblfilt.Value
            LoadPage(PageNumber, Filter)
        End If

    End Sub
    Private Sub goNext()
        Filter = lblfilt.Value
        If Len(lblfuid.Value) <> 0 Then
            If lblsb.Value = "0" Then
                PageNumber = lblpg.Text
            Else
                PageNumber = lblpgholder.Value
            End If

            lblsb.Value = "0"
            PageNumber = PageNumber + 1
            'Filter = Filter & " and tasknum = " & PageNumber
            If PageNumber <= lblcnt.Text Then
                tasks.Open()
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            Else
                Dim strMessage As String = "No more Tasks in this direction"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            lblenable.Value = "1"
        End If

    End Sub
    Private Sub goPrev()
        If Len(lblfuid.Value) <> 0 Then
            Filter = lblfilt.Value
            If lblsb.Value = "0" Then
                PageNumber = lblpg.Text
            Else
                PageNumber = lblpgholder.Value
            End If
            lblsb.Value = "0"
            'PageNumber = lblpg.Text

            If PageNumber > 1 Then
                PageNumber = PageNumber - 1
                'Filter = Filter & " and tasknum = " & PageNumber
                tasks.Open()
                LoadPage(PageNumber, Filter)
                tasks.Dispose()

            Else

                Dim strMessage As String = "No more Tasks in this direction"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            lblenable.Value = "1"
        End If

    End Sub
    Private Sub PopComp()

        fuid = lblfuid.Value
        sql = "select comid, compnum + ' - ' + isnull(compdesc, '') as compnum " _
          + "from components where func_id = '" & fuid & "' order by crouting"
        dr = tasks.GetRdrData(sql)
        'ddcomp.DataSource = dr
        'ddcomp.DataTextField = "compnum"
        'ddcomp.DataValueField = "comid"
        'ddcomp.DataBind()
        dr.Close()
        'ddcomp.Items.Insert(0, New ListItem("Select"))
        'ddcomp.Items(0).Value = 0


    End Sub
    Private Sub AddFail()
        Dim comp As String = lblco.Value
        tasks.Open()
        PopCompFailList(comp)
        PopFailList(comp)
        PopTaskFailModes(comp)
        tasks.Dispose()
    End Sub
    Private Sub GetLists()
        'msglbl.Text = ""
        'cid = lblcid.Value
        cid = "0"


        sql = "select ttid, tasktype " _
        + "from pmTaskTypes where compid = '" & cid & "' or compid = '0' order by compid"
        dr = tasks.GetRdrData(sql)
        ddtype.DataSource = dr
        Try
            ddtype.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddtype.Items.Insert(0, New ListItem("Select"))
        ddtype.Items(0).Value = 0
        'orig
        dr = tasks.GetRdrData(sql)
        ddtypeo.DataSource = dr
        Try
            ddtypeo.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddtypeo.Items.Insert(0, New ListItem("Select"))
        ddtypeo.Items(0).Value = 0

        'sid = HttpContext.Current.Session("dfltps").ToString() 'lblsid.Value
        sid = lblsid.Value
        Dim scnt As Integer
        sql = "select count(*) from pmSiteSkills where siteid = '" & sid & "'"
        scnt = tasks.Scalar(sql)
        If scnt <> 0 Then
            sql = "select skillid, skill " _
            + "from pmSiteSkills where compid = '" & cid & "' and siteid = '" & sid & "'"
        Else
            sql = "select skillid, skill " _
            + "from pmSkills where compid = '" & cid & "' order by compid"
        End If
        dr = tasks.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        Try
            ddskill.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddskill.Items.Insert(0, New ListItem("Select"))
        ddskill.Items(0).Value = 0
        'orig
        dr = tasks.GetRdrData(sql)
        ddskillo.DataSource = dr
        ddskillo.DataTextField = "skill"
        ddskillo.DataValueField = "skillid"
        Try
            ddskillo.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddskillo.Items.Insert(0, New ListItem("Select"))
        ddskillo.Items(0).Value = 0


        sql = "select ptid, pretech " _
        + "from pmPreTech where compid = '" & cid & "' or compid = '0' order by compid"
        dr = tasks.GetRdrData(sql)
        ddpt.DataSource = dr
        ddpt.DataTextField = "pretech"
        ddpt.DataValueField = "ptid"
        Try
            ddpt.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddpt.Items.Insert(0, New ListItem("None"))
        ddpt.Items(0).Value = 0
        'orig
        dr = tasks.GetRdrData(sql)
        ddpto.DataSource = dr
        ddpto.DataTextField = "pretech"
        ddpto.DataValueField = "ptid"
        Try
            ddpto.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddpto.Items.Insert(0, New ListItem("None"))
        ddpto.Items(0).Value = 0

        sql = "select statid, status " _
        + "from pmStatus where compid = '" & cid & "' or compid = '0' order by compid"
        dr = tasks.GetRdrData(sql)
        ddeqstat.DataSource = dr
        ddeqstat.DataTextField = "status"
        ddeqstat.DataValueField = "statid"
        Try
            ddeqstat.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddeqstat.Items.Insert(0, New ListItem("Select"))
        ddeqstat.Items(0).Value = 0
        'orig
        dr = tasks.GetRdrData(sql)
        ddeqstato.DataSource = dr
        ddeqstato.DataTextField = "status"
        ddeqstato.DataValueField = "statid"
        Try
            ddeqstato.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddeqstato.Items.Insert(0, New ListItem("Select"))
        ddeqstato.Items(0).Value = 0



        PopComp()
    End Sub
    Private Sub CleanPage()
        lblt.Value = ""
        lbltaskid.Value = ""
        lblst.Value = ""
        Try
            ddtype.SelectedIndex = 0
        Catch ex As Exception

        End Try
        Try
            ddtypeo.SelectedIndex = 0
        Catch ex As Exception

        End Try
        txtfreq.Text = ""
        txtfreqo.Text = ""
        lblfreq.Value = ""
        lblfreqo.Value = ""
        lblfixed.Value = ""
        lblfixedo.Value = ""
        cbfixed.Checked = False
        cbfixedo.Checked = False
        Try
            ddpt.SelectedIndex = 0
        Catch ex As Exception

        End Try
        Try
            ddpto.SelectedIndex = 0
        Catch ex As Exception

        End Try
        Try
            ddskill.SelectedIndex = 0
        Catch ex As Exception

        End Try
        Try
            ddskillo.SelectedIndex = 0
        Catch ex As Exception

        End Try
        Try
            ddeqstat.SelectedIndex = 0
        Catch ex As Exception

        End Try
        Try
            ddeqstato.SelectedIndex = 0
        Catch ex As Exception

        End Try

        txtdesc.Value = ""
        txtodesc.Value = ""
        txtqty.Text = ""
        txtqtyo.Text = ""
        txttr.Text = ""
        txttro.Text = ""
        txtordt.Text = ""
        txtrdt.Text = ""
        lblcind.Value = ""
        Try
            ddtaskstat.SelectedIndex = 0
        Catch ex As Exception

        End Try

        cbloto.Checked = False
        cbcs.Checked = False
        lblco.Value = ""
        txtcQty.Text = ""
        lbfaillist.Items.Clear()
        lbfailmodes.Items.Clear()
        lbofailmodes.Items.Clear()
        lbCompFM.Items.Clear()
    End Sub
    Private Sub AddFail2()
        Dim comp As String = lblcoid.Value '= colblcoid.Value
        'tasks.Open()
        PopCompFailList(comp)
        PopFailList(comp)
        'PopTaskFailModes(comp)
        'tasks.Dispose()
    End Sub
    Private Sub LoadPage(ByVal PageNumber As Integer, ByVal Filter As String)
        '**** Added for PM Manager
        eqid = lbleqid.Value
        'Try
        'lblhaspm.Value = tasks.HasPM(eqid)
        'Catch ex As Exception
        'lblhaspm.Value = tasksadd.HasPM(eqid)
        'End Try
        Dim freq As String
        If lblsb.Value = "0" Then
            Dim scnt As Integer
            scnt = SubCount(PageNumber, Filter)
            'lblscnt.Text = scnt
            lblsubcount.Text = scnt
            If Len(Filter) = 0 Then
                Filter = " subtask = 0"
                CntFilter = " subtask = 0"
            Else
                Filter = Filter & " and subtask = 0"
                CntFilter = Filter & " and subtask = 0"
            End If

            lblpgholder.Value = PageNumber
            'lblspg.Text = "0"
            cbloto.Checked = False
            cbcs.Checked = False
        End If
        Dim tskcnt, optcnt As Integer
        Dim qtyo, skillido, rdido As String
        tskcnt = 0
        If tskcnt = 0 Then
            CntFilter = lblfilt.Value
            CntFilter = Filter.Replace("''", "'")
            sql = "select count(*) from pmtasks where " & CntFilter
            Try
                tskcnt = tasks.Scalar(sql)
            Catch ex As Exception
                tskcnt = tasksadd.Scalar(sql)
            End Try
        End If
        lblcnt.Text = tskcnt
        lblpg.Text = PageNumber
        If PageNumber = 0 And tskcnt > 0 Then
            PageNumber = 1
            lblpg.Text = PageNumber
        End If
        Dim usetot As String
        Dim tst As String = sql
        If tskcnt = 0 Then
            lblpg.Text = "0"
            lblcnt.Text = tskcnt
            CleanPage()
            AddFail2()
            Dim strMessage As String = "No Tasks entered for this Function yet"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")

        Else
            Tables = "pmtasks"
            PK = "pmtskid"
            Try
                dr = tasks.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            Catch ex As Exception
                dr = tasksadd.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            End Try
            Dim freqid, rdid, qty, skillid, ismeter, ts, ismetero, fixed, fixedo As String
            While dr.Read
                '***for meters
                fixed = dr.Item("fixed").ToString
                fixedo = dr.Item("fixedo").ToString

                ismeter = dr.Item("usemeter").ToString
                lblusemeter.Value = dr.Item("usemeter").ToString
                lblmeterid.Value = dr.Item("meterid").ToString

                ismetero = dr.Item("usemetero").ToString

                lblusemetero.Value = dr.Item("usemetero").ToString
                lblmeterido.Value = dr.Item("meterido").ToString
                lblmfid.Value = dr.Item("mfid").ToString
                lblmfido.Value = dr.Item("mfido").ToString
                '***
                lbltpmpmtskid.Value = dr.Item("tpmpmtskid").ToString
                lbltpmhold.Value = dr.Item("tpmhold").ToString
                lblusetot.Value = dr.Item("usetot").ToString
                lbltpmhold.Value = dr.Item("tpmhold").ToString
                txtpfint.Text = dr.Item("pfInterval").ToString
                lblt.Value = dr.Item("tasknum").ToString
                txttaskorder.Text = dr.Item("tasknum").ToString
                Dim task_test As String = dr.Item("tasknum").ToString
                ttid = dr.Item("pmtskid").ToString
                lbltaskid.Value = ttid
                lblst.Value = dr.Item("subtask").ToString
                Try
                    ddtype.SelectedValue = dr.Item("ttid").ToString
                Catch ex As Exception
                    ddtype.SelectedIndex = 0
                End Try
                Try
                    ddtypeo.SelectedValue = dr.Item("origttid").ToString 'orig
                Catch ex As Exception
                End Try
                freqid = dr.Item("freqid").ToString
                
                freq = dr.Item("freq").ToString

                txtfreq.Text = dr.Item("freq").ToString
                txtfreqo.Text = dr.Item("origfreq").ToString

                lblfreq.Value = dr.Item("freq").ToString
                lblfreqo.Value = dr.Item("origfreq").ToString
                Try
                    ddpt.SelectedValue = dr.Item("ptid").ToString
                Catch ex As Exception
                    ddpt.SelectedIndex = 0
                End Try
                Try
                    ddpto.SelectedValue = dr.Item("origptid").ToString 'orig
                Catch ex As Exception
                    ddpto.SelectedIndex = 0
                End Try
                qty = dr.Item("qty").ToString
                skillid = dr.Item("skillid").ToString
                qtyo = dr.Item("origqty").ToString
                skillido = dr.Item("origskillid").ToString
                '***for meters
                lblskillid.Value = skillid
                lblskillqty.Value = qty
                lblskillido.Value = skillido
                lblskillqtyo.Value = qtyo
                '***

                Try
                    ddskill.SelectedValue = dr.Item("skillid").ToString
                Catch ex As Exception
                    ddskill.SelectedIndex = 0
                End Try
                Try
                    ddskillo.SelectedValue = dr.Item("origskillid").ToString 'orig
                Catch ex As Exception
                    ddskillo.SelectedIndex = 0
                End Try
                rdid = dr.Item("rdid").ToString
                rdido = dr.Item("origrdid").ToString 'orig
                '***for meters
                lblrdid.Value = rdid
                lblrdido.Value = rdido
                '***

                Try
                    ddeqstat.SelectedValue = dr.Item("rdid").ToString
                Catch ex As Exception
                    ddeqstat.SelectedIndex = 0
                End Try
                Try
                    ddeqstato.SelectedValue = dr.Item("origrdid").ToString 'orig
                Catch ex As Exception
                    ddeqstato.SelectedIndex = 0
                End Try
                txtpfint.Text = dr.Item("pfInterval").ToString
                txtdesc.Value = dr.Item("taskdesc").ToString
                txtodesc.Value = dr.Item("otaskdesc").ToString 'orig
                qty = dr.Item("qty").ToString
                txtqty.Text = dr.Item("qty").ToString
                txtqtyo.Text = dr.Item("origqty").ToString 'orig
                txttr.Text = dr.Item("tTime").ToString
                txttro.Text = dr.Item("origtTime").ToString 'orig
                txtordt.Text = dr.Item("origrdt").ToString
                txtrdt.Text = dr.Item("rdt").ToString
                lblcind.Value = dr.Item("compindex").ToString
                'ddtaskstat.SelectedValue = dr.Item("taskstatusid").ToString
                ts = dr.Item("taskstatus").ToString
                Try
                    ddtaskstat.SelectedValue = dr.Item("taskstatus").ToString
                Catch ex As Exception
                    ddtaskstat.SelectedIndex = 0
                End Try

                If dr.Item("lotoid").ToString = "1" Then
                    cbloto.Checked = True
                Else
                    cbloto.Checked = False
                End If
                If dr.Item("conid").ToString = "1" Then
                    cbcs.Checked = True
                Else
                    cbcs.Checked = False
                End If
                co = dr.Item("comid").ToString
                If Len(co) = 0 Or co = "" Then
                    co = "0"
                End If
                lblco.Value = co
                lblfuid.Value = dr.Item("funcid").ToString
                txtcQty.Text = dr.Item("cqty").ToString
                If txtcQty.Text = "" Then
                    txtcQty.Text = "1"
                End If
                lblhaspm.Value = dr.Item("haspm").ToString
            End While
            'lblpar.Value = "task"
            dr.Close()
            'UpdateFM()
            lblrteid.Value = rteid
            If rteid <> "0" And rteid <> "" Then
                sql = "select skillid, qty, freq, rdid from pmroutes where rid = '" & rteid & "'"
                Try
                    dr = tasks.GetRdrData(sql)
                    While dr.Read
                        rbskillid = dr.Item("skillid").ToString
                        rbqty = dr.Item("qty").ToString
                        rbfreq = dr.Item("freq").ToString
                        rbrdid = dr.Item("rdid").ToString
                    End While
                    dr.Close()
                Catch ex As Exception
                    dr = tasksadd.GetRdrData(sql)
                    While dr.Read
                        rbskillid = dr.Item("skillid").ToString
                        rbqty = dr.Item("qty").ToString
                        rbfreq = dr.Item("freq").ToString
                        rbrdid = dr.Item("rdid").ToString
                    End While
                    dr.Close()
                End Try

                lblrbskillid.Value = rbskillid
                lblrbqty.Value = rbqty
                lblrbfreq.Value = rbfreq
                lblrbrdid.Value = rbrdid
            End If
            lblfixed.Value = fixed
            lblfixedo.Value = fixedo
            If fixed = "1" Then
                cbfixed.Checked = True
            Else
                cbfixed.Checked = False
            End If
            If fixedo = "1" Then
                cbfixedo.Checked = True
            Else
                cbfixedo.Checked = False
            End If
            If lbltpmhold.Value = "1" Then
                tdtpmhold.InnerHtml = "(TPM Task)"
                lbltpmalert.Value = "no"
                imgdeltpm.Attributes.Add("class", "visible")
                btntpm.Visible = False
            Else
                If ismeter = "1" Then
                    lbltpmhold.Value = "0"
                    tdtpmhold.InnerHtml = ""
                    'lbltpmalert.Value = ""
                    imgdeltpm.Attributes.Add("class", "details")
                    btntpm.Visible = False
                Else
                    lbltpmhold.Value = "0"
                    tdtpmhold.InnerHtml = ""
                    'lbltpmalert.Value = ""
                    imgdeltpm.Attributes.Add("class", "details")
                    btntpm.Visible = True
                End If
            End If
            Dim cind As String = lblcind.Value
            If co <> "0" Then
                lblco.Value = co
                Try
                    'ddcomp.SelectedValue = co
                Catch ex As Exception

                End Try
                PopCompFailList(co)
                PopFailList(co)
                PopTaskFailModes(co)
                PopoTaskFailModes(co)
                'UpdateFailStats(co)
                'PopDesc(co)
                lblco.Value = co
                chk = "comp"

            Else
                Try
                    'ddcomp.SelectedValue = co
                Catch ex As Exception

                End Try
                'lblcodesc.Text = ""
                lbfaillist.Items.Clear()
                lbfailmodes.Items.Clear()
                lbofailmodes.Items.Clear()
                lbCompFM.Items.Clear()
                lblco.Value = co
                chk = "comp"
            End If
            lblpar.Value = "comp"
            'UpDateRevs(ttid)
            fuid = lblfuid.Value
            'If lblsb.Value = "0" Then
            'lblpg.Text = PageNumber
            'If Len(Filter) = 0 Then
            'Filter = " subtask = 0"
            'Else
            'Filter = "funcid = " & fuid & " and subtask = 0"
            'End If
            'sql = "select count(*) from pmtasks where " & Filter
            'Try
            'tskcnt = tasks.Scalar(sql)
            'Catch ex As Exception
            'tskcnt = tasksadd.Scalar(sql)
            'End Try
            'Else
            'lblpg.Text = lblpgholder.Value
            'PageNumber = lblpgholder.Value
            'If Len(Filter) = 0 Then
            'Filter = " subtask = 0"
            'Else
            'Filter = "funcid = " & fuid & " and subtask = 0"
            'End If
            'sql = "select count(*) from pmtasks where " & Filter
            'Try
            'tskcnt = tasks.Scalar(sql)
            'Catch ex As Exception
            'tskcnt = tasksadd.Scalar(sql)
            'End Try

            'End If
            lblcnt.Text = tskcnt
            'CheckPgNav(tskcnt, PageNumber)
            'btnaddsubtask.Enabled = True
            GetMeterDetails(skillid, rdid, fuid, eqid, rdido, skillido)
            '************new
            sql = "select compnum, compdesc from components where comid = '" & co & "'"
            Try
                dr = tasks.GetRdrData(sql)
            Catch ex As Exception
                dr = tasksadd.GetRdrData(sql)
            End Try

            While dr.Read
                lblnewcomp.Value = dr.Item("compnum").ToString
                lblnewdesc.Value = dr.Item("compdesc").ToString
                tdcompnum.InnerHtml = dr.Item("compnum").ToString
            End While
            dr.Close()

            If ts <> "Delete" Then
                If usetot = "1" Then
                    If skillid <> "" And (freqid <> "0" And freqid <> "") And (rdid <> "0" And rdid <> "") And (qty <> "0" And qty <> "") Then
                        sql = "select count(*) from pmtottime where skillid = '" & skillid & "' " _
                                       + "and freq = '" & freq & "' and rdid = '" & rdid & "' and skillqty = '" & qty & "'"
                        Dim utcnt As Integer
                        Try
                            utcnt = tasks.Scalar(sql)
                        Catch ex As Exception
                            utcnt = tasksadd.Scalar(sql)
                        End Try
                        If utcnt = 0 Then
                            Dim strMessage As String = "No Total Time Assigned to this Skill \ Skill Quantity \ Frequency \ Equipment Status Yet"
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        End If
                    End If

                End If
            End If
            '************end new
        End If
        'tasks.Dispose()
    End Sub
    Private Sub GetMeterDetails(ByVal skillid As String, ByVal rdid As String, ByVal fuid As String, ByVal eqid As String, _
                                ByVal rdido As String, ByVal skillido As String)
        Dim rd, func, skill, eqnum, rdo, skillo, mfid, mfido As String
        sql = "declare @rd varchar(50), @func varchar(50), @skill varchar(50), @eqnum varchar(50), @xstatus varchar(50) "
        sql += "declare @rdo varchar(50), @skillo varchar(50), @mfid int, @mfido int "
        sql += "set @rd = (select status from pmstatus where statid = '" & rdid & "') "
        sql += "set @rdo = (select status from pmstatus where statid = '" & rdido & "') "
        sql += "set @func = (select func from functions where func_id = '" & fuid & "') "
        sql += "set @skill = (select skill from pmskills where skillid = '" & skillid & "') "
        sql += "set @skillo = (select skill from pmskills where skillid = '" & skillido & "') "
        sql += "set @eqnum = (select eqnum from equipment where eqid = '" & eqid & "') "
        sql += "set @xstatus = (select xstatus from equipment where eqid = '" & eqid & "') "
        sql += "select @rd as rd, @func as func, @skill as skill, @eqnum as eqnum, @rdo as rdo, @skillo as skillo, @xstatus as xstatus"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try
        While dr.Read
            rd = dr.Item("rd").ToString
            rdo = dr.Item("rdo").ToString
            func = dr.Item("func").ToString
            skill = dr.Item("skill").ToString
            skillo = dr.Item("skillo").ToString
            eqnum = dr.Item("eqnum").ToString
            xstatus = dr.Item("xstatus").ToString
        End While
        dr.Close()
        lblrd.Value = rd
        lblfunc.Value = func
        lblskill.Value = skill
        lbleqnum.Value = eqnum
        lblskillo.Value = skillo
        lblrdo.Value = rdo
        who = lblwho.Value
        If who <> "ed" Then
            lblxstatus.Value = xstatus
        Else
            xstatus = "yes"
            lblxstatus.Value = "yes"
        End If

        ghostoff = lblghostoff.Value
        If ghostoff = "yes" And xstatus = "yes" Then
            ddtypeo.Attributes.Add("onchange", "GetType('o', this.value);")
            ddpto.Attributes.Add("onchange", "")
            ddskillo.Attributes.Add("onchange", "")
            ddeqstato.Attributes.Add("onchange", "zerodt('o', this.value);")
            txtqtyo.Attributes.Add("onchange", "")
            txttro.Attributes.Add("onkeyup", "filldown('o', this.value);")
            txtordt.Attributes.Add("onkeyup", "")
            txtodesc.Attributes.Add("onkeyup", "")
            txtodesc.Attributes.Add("onchange", "")
            '***** CHECK THIS FOR METERS - ANYTHING FREQ RELATED
            txtfreqo.Attributes.Add("onchange", "")
        Else
            ddtypeo.Attributes.Add("onchange", "GetType('o', this.value);")
            ddpto.Attributes.Add("onchange", "document.getElementById('ddpt').value=this.value;")
            ddskillo.Attributes.Add("onchange", "document.getElementById('ddskill').value=this.value;")
            ddeqstato.Attributes.Add("onchange", "document.getElementById('ddeqstat').value=this.value;zerodt('o', this.value);")
            txtqtyo.Attributes.Add("onchange", "document.getElementById('txtqty').value=this.value;")
            txttro.Attributes.Add("onkeyup", "filldown('o', this.value);")
            txttr.Attributes.Add("onkeyup", "filldown('r', this.value);")
            ddeqstat.Attributes.Add("onchange", "zerodt('d', this.value);")
            txtordt.Attributes.Add("onkeyup", "document.getElementById('txtrdt').value=this.value;")
            '***** CHECK THIS FOR METERS - ANYTHING FREQ RELATED
            txtfreqo.Attributes.Add("onchange", "document.getElementById('txtfreq').value=this.value;")
        End If
    End Sub
    Private Sub PopFailList(ByVal comp As String)
        ttid = lbltaskid.Value
        Dim lang As String = lblfslang.Value
        sql = "select compfailid, failuremode " _
         + "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
         + "select failid from pmtaskfailmodes where comid = '" & comp & "')" ' and taskid = '" & ttid & "')"
        sql = "usp_getcfall_tskna '" & comp & "','" & lang & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        lbfaillist.DataSource = dr
        lbfaillist.DataTextField = "failuremode"
        lbfaillist.DataValueField = "compfailid"
        lbfaillist.DataBind()
        dr.Close()
    End Sub

    Private Sub PopCompFailList(ByVal comp As String)
        ttid = lbltaskid.Value
        Dim lang As String = lblfslang.Value
        sql = "select compfailid, failuremode " _
         + "from componentfailmodes where comid = '" & comp & "'"
        sql = "usp_getcfall_co '" & comp & "','" & lang & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        lbCompFM.DataSource = dr
        lbCompFM.DataTextField = "failuremode"
        lbCompFM.DataValueField = "compfailid"
        lbCompFM.DataBind()
        dr.Close()
    End Sub
    Private Sub PopTaskFailModes(ByVal comp As String)
        ttid = lbltaskid.Value
        Dim lang As String = lblfslang.Value
        'sql = "select * from pmtaskfailmodes where taskid = '" & ttid & "'"
        sql = "select * from pmtaskfailmodes where comid = '" & comp & "' and taskid = '" & ttid & "'"
        sql = "usp_getcfall_tsk '" & comp & "','" & ttid & "','" & lang & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        lbfailmodes.DataSource = dr
        lbfailmodes.DataTextField = "failuremode"
        lbfailmodes.DataValueField = "failid"
        lbfailmodes.DataBind()
        dr.Close()
    End Sub
    Private Sub PopoTaskFailModes(ByVal comp As String)
        ttid = lbltaskid.Value
        Dim lang As String = lblfslang.Value
        sql = "select * from pmotaskfailmodes where taskid = '" & ttid & "'"
        sql = "usp_getcfall_tsko '" & ttid & "','" & lang & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try


        lbofailmodes.DataSource = dr
        lbofailmodes.DataTextField = "failuremode"
        lbofailmodes.DataValueField = "failid"
        lbofailmodes.DataBind()
        dr.Close()
    End Sub
    Private Sub UpdateFailStats(ByVal comp As String)
        Dim fmcnt, fmcnta As Integer
        sql = "select count(*) from ComponentFailModes where comid = '" & comp & "'"
        Try
            fmcnt = tasks.Scalar(sql)
        Catch ex As Exception
            fmcnt = tasksadd.Scalar(sql)
        End Try

        sql = "select count(distinct failid) from pmTaskFailModes where comid = '" & comp & "'"
        Try
            fmcnta = tasks.Scalar(sql)
        Catch ex As Exception
            fmcnta = tasksadd.Scalar(sql)
        End Try

        fmcnta = fmcnt - fmcnta
    End Sub
    Private Sub UpDateRevs(ByVal Filter As String)
        sql = "select created, revised from pmtasks where pmtskid = '" & Filter & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        Dim cr, rv As String
        While dr.Read
            cr = dr.Item("created").ToString
            rv = dr.Item("revised").ToString
        End While
        dr.Close()
        If Len(rv) = 0 Then
            rv = "NA"
        End If
        'imgClock.Attributes.Add("onmouseover", "return overlib('Task Created: " & cr & "<br> Task Revised: " & rv & "')")
        'imgClock.Attributes.Add("onmouseout", "return nd()")
    End Sub


    Private Sub ibReuse_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibReuse.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        ttid = lbltaskid.Value
        Dim comp As String = lblcoid.Value '= ddcomp.SelectedValue.ToString
        tasks.Open()

        For Each Item In lbCompFM.Items
            If Item.Selected Then
                f = Item.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            lbltaskid.Value = ttid
            UpdateFM()
            'SaveTask()
            'lblenable.Value = "0"
            PageNumber = lblpg.Text
            Filter = lblfilt.Value
            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try

        tasks.Dispose()
    End Sub
    Private Sub GetItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Dim fcnt, fcnt2 As Integer
        If oaid <> "0" Then
            sql = "select count(*) from pmTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and oaid = '" & oaid & "'"
        Else
            sql = "select count(*) from pmTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and  (oaid is null or oaid = '0')"
        End If
        fcnt = tasks.Scalar(sql)
        sql = "select count(*) from pmTaskFailModes where taskid = '" & ttid & "'"
        fcnt2 = tasks.Scalar(sql)
        If fcnt2 >= 5 Then
            Dim strMessage As String = "Limit is 5 Failure Modes per Task"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf fcnt > 0 Then
            Dim strMessage As String = "Failure Modes Must Be Unique Within a Task"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Try
                sql = "sp_addTaskFailureMode " & ttid & ", " & fi & ", '" & f & "', '" & comp & "', 'dev','" & oaid & "'"
                tasks.Update(sql)
                Dim Item As ListItem
                Dim fm As String
                Dim ipar As Integer = 0
                For Each Item In lbfailmodes.Items
                    f = Item.ToString
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                    If Len(fm) = 0 Then
                        fm = f & "(___)"
                    Else
                        fm += " " & f & "(___)"
                    End If
                Next
                fm = tasks.ModString2(fm)
                sql = "update pmtasks set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
                tasks.Update(sql)

            Catch ex As Exception

            End Try
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
        End If
    End Sub
    Private Sub RemItems(ByVal f As String, ByVal fi As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Try
            sql = "sp_delTaskFailureMode '" & ttid & "', '" & fi & "','" & oaid & "'"
            tasks.Update(sql)
            Dim fm As String
            Dim ipar As Integer = 0
            Dim Item As ListItem
            For Each Item In lbfailmodes.Items
                f = Item.ToString
                ipar = f.LastIndexOf("(")
                If ipar <> -1 Then
                    f = Mid(f, 1, ipar)
                End If
                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
            Next
            fm = tasks.ModString2(fm)
            sql = "update pmtasks set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
            tasks.Update(sql)
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GetoItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Dim fcnt, fcnt2 As Integer
        If oaid <> "0" Then
            sql = "select count(*) from pmoTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and oaid = '" & oaid & "'"
        Else
            sql = "select count(*) from pmoTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and oaid is null"
        End If
        fcnt = tasks.Scalar(sql)
        sql = "select count(*) from pmoTaskFailModes where taskid = '" & ttid & "'"
        fcnt2 = tasks.Scalar(sql)
        If fcnt2 >= 5 Then
            Dim strMessage As String = "Limit is 5 Failure Modes per Task"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf fcnt > 0 Then
            Dim strMessage As String = "Failure Modes Must Be Unique Within a Task"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Try
                sql = "sp_addTaskFailureMode " & ttid & ", " & fi & ", '" & f & "', '" & comp & "', 'opt', '" & oaid & "'"
                tasks.Update(sql)
                Dim Item As ListItem
                Dim ofm, of1, ofi As String
                Dim ipar As Integer = 0
                For Each Item In Me.lbofailmodes.Items
                    of1 = Item.ToString
                    ipar = of1.LastIndexOf("(")
                    If ipar <> -1 Then
                        of1 = Mid(of1, 1, ipar)
                    End If
                    If Len(ofm) = 0 Then
                        ofm = of1 & "(___)"
                    Else
                        ofm += " " & of1 & "(___)"
                    End If
                Next
                ofm = tasks.ModString2(ofm)
                sql = "update pmtasks set ofm1 = '" & ofm & "' where pmtskid = '" & ttid & "'"
                tasks.Update(sql)
            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Sub RemoItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Try
            sql = "sp_deloTaskFailureMode " & ttid & ", " & fi & ", '" & f & "','" & oaid & "'"
            tasks.Update(sql)
            Dim Item As ListItem
            Dim ofm, of1, ofi As String
            Dim ipar As Integer = 0
            For Each Item In Me.lbofailmodes.Items
                of1 = Item.ToString
                ipar = of1.LastIndexOf("(")
                If ipar <> -1 Then
                    of1 = Mid(of1, 1, ipar)
                End If
                If Len(ofm) = 0 Then
                    ofm = of1 & "(___)"
                Else
                    ofm += " " & of1 & "(___)"
                End If
            Next
            ofm = tasks.ModString2(ofm)
            sql = "update pmtasks set ofm1 = '" & ofm & "' where pmtskid = '" & ttid & "'"
            tasks.Update(sql)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ibfromo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibfromo.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        ttid = lbltaskid.Value
        Dim comp As String = lblcoid.Value '= ddcomp.SelectedValue.ToString
        tasks.Open()

        For Each Item In Me.lbofailmodes.Items
            If Item.Selected Then
                f = Item.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                RemoItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopoTaskFailModes(comp)
            UpdateFailStats(comp)
            lbltaskid.Value = ttid
            'SaveTask()
            'lblenable.Value = "0"
            PageNumber = lblpg.Text
            Filter = lblfilt.Value
            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub

    Private Sub ibtoo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtoo.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        ttid = lbltaskid.Value
        Dim comp As String = lblcoid.Value '= ddcomp.SelectedValue.ToString
        tasks.Open()
        lblenable.Value = "0"
        For Each Item In Me.lbCompFM.Items
            If Item.Selected Then
                f = Item.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetoItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            PopoTaskFailModes(comp)
            UpdateFailStats(comp)
            'LoadPage(PageNumber, Filter) 'test

            'SaveTask()
            'lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            'PopComp()
            Filter = lblfilt.Value

            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub

    Private Sub ibToTask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibToTask.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        ttid = lbltaskid.Value
        Dim comp As String = lblcoid.Value '= ddcomp.SelectedValue.ToString
        tasks.Open()

        For Each Item In Me.lbfaillist.Items
            If Item.Selected Then
                f = Item.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetItems(f, fi, comp, oa)
            End If
        Next

        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            UpdateFM()
            'LoadPage(PageNumber, Filter) 'test
            'SaveTask()
            'lblenable.Value = "0"
            lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            'PopComp()
            Filter = lblfilt.Value

            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub
    Private Sub UpdateFM()
        Dim ttid As String = lbltaskid.Value
        sql = "usp_UpdateFM1 '" & ttid & "'"
        tasks.Update(sql)
    End Sub

    Private Sub ibFromTask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibFromTask.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        ttid = lbltaskid.Value
        Dim comp As String = lblcoid.Value '= ddcomp.SelectedValue.ToString
        tasks.Open()

        For Each Item In Me.lbfailmodes.Items
            If Item.Selected Then
                f = Item.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                RemItems(f, fi, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            UpdateFM()
            'LoadPage(PageNumber, Filter) 'test
            'SaveTask()
            'lblenable.Value = "0"
            lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            'PopComp()
            Filter = lblfilt.Value

            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub

    Private Sub btnaddtsk_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnaddtsk.Click
        lblenable.Value = "0"
        tl = lbltasklev.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        coid = lblcoid.Value
        Dim compnum As String = lblcompnum.Value
        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        tl = 5
        tasksadd.Open()

        sql = "usp_AddTask2 '" & tl & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & ustr & "', '" & coid & "', '" & compnum & "'"
        tasksadd.Scalar(sql)
        Filter = lblfilt.Value

        PageNumber = lblcnt.Text
        PageNumber = PageNumber + 1 '1 '
        lblcnt.Text = PageNumber
        field = "comid"
        val = coid
        intyp = lblintyp.Value
        If intyp = "tasks2" Then
            Filter = "funcid = ''" & fuid & "'' and subtask = 0" '"comid = " & coid & " and 
            CntFilter = "funcid = '" & fuid & "' and subtask = 0"
            lblfiltcnt.Value = CntFilter
        Else
            Filter = field & " = " & val
            lblfiltcnt.Value = Filter
        End If

        lblfilt.Value = Filter

        LoadPage(PageNumber, Filter)
        tasksadd.Dispose()
    End Sub
    Private Sub SaveTask2()
        Dim usemeter As String = lblusemeter.Value
        Dim usemetero As String = lblusemetero.Value
        Dim tn, otn As String
        tn = txttaskorder.Text
        Dim tnchk As Long
        Try

            tnchk = System.Convert.ToInt64(tn)
        Catch ex As Exception
            Dim strMessage As String = "Task Number Must Be a Numeric Value!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        otn = lblt.Value
        If tn = "0" Or Len(tn) = 0 Then
            tn = otn
        End If
        Dim lst As Integer = lblcnt.Text
        Try
            If tnchk > lst Then
                tn = lst
            End If
        Catch ex As Exception

        End Try
        If tn < 1 Then
            tn = 1
        End If
        ro = lblro.Value
        If ro <> "1" Then
            Dim t, st, tid, typ, fre, des, odes, rmt, pt, ski, qty, tr, eqs, lot, cs, ci, cn, cin As String
            Dim typor, typstror, freor, frestror, ptor, ptstror As String
            Dim skior, skistror, qtyor, tror, eqsor, eqsstror As String
            Dim typstr, frestr, rmtstr, ptstr, skistr, eqsstr, cqty, taskstat, taskstatid As String
            Dim fixed, fixedo As String
            If Len(lblfuid.Value) <> 0 Then
                t = lblt.Value

                st = lblsb.Value
                If ddtaskstat.SelectedIndex <> 0 Then
                    taskstatid = ddtaskstat.SelectedValue
                    taskstat = ddtaskstat.SelectedItem.ToString
                    taskstat = Replace(taskstat, "'", Chr(180), , , vbTextCompare)
                Else
                    taskstatid = "0"
                End If

                If ddtype.SelectedIndex <> 0 Then
                    typ = ddtype.SelectedValue
                    typstr = ddtype.SelectedItem.ToString
                    typstr = Replace(typstr, "'", Chr(180), , , vbTextCompare)
                Else
                    typ = "0"
                End If
                If ddtypeo.SelectedIndex <> 0 Then
                    typor = ddtypeo.SelectedValue 'orig +
                    typstror = ddtypeo.SelectedItem.ToString
                    typstror = Replace(typstror, "'", Chr(180), , , vbTextCompare)
                Else
                    typor = "0"
                End If
                Try
                    fre = "" 'txtfreq.Text
                    frestr = txtfreq.Text
                Catch ex As Exception
                    fre = "" 'txtfreq.Text
                    frestr = lblfreq.Value
                End Try
                If frestr = "" Then
                    frestr = lblfreq.Value
                End If
                Try
                    freor = "" 'txtfreqo.Text
                    frestror = txtfreqo.Text
                Catch ex As Exception
                    freor = "" 'txtfreqo.Text
                    frestror = lblfreqo.Value
                End Try
                If frestror = "" Then
                    frestror = lblfreqo.Value
                End If

                qty = txtqty.Text

                If Len(qty) = 0 Then
                    qty = "1"
                End If
                qtyor = txtqtyo.Text 'orig 
                If Len(qtyor) = 0 Then
                    qtyor = "1"
                End If
                tr = txttr.Text
                If Len(tr) = 0 Then
                    tr = "0"
                End If
                tror = txttro.Text 'orig
                If Len(tror) = 0 Then
                    tror = "0"
                End If
                If ddeqstat.SelectedIndex <> 0 Then
                    eqs = ddeqstat.SelectedValue
                    eqsstr = ddeqstat.SelectedItem.ToString
                    eqsstr = Replace(eqsstr, "'", Chr(180), , , vbTextCompare)
                Else
                    eqs = "0"
                End If
                If eqs = "0" Then
                    If usemeter = "1" Then
                        Dim strMessage1 As String = "Revised Status Required When Using Meter Frequency"
                        Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                        Exit Sub
                    End If
                End If
                If ddeqstato.SelectedIndex <> 0 Then
                    eqsor = ddeqstato.SelectedValue 'orig +
                    eqsstror = ddeqstato.SelectedItem.ToString
                    eqsstror = Replace(eqsstror, "'", Chr(180), , , vbTextCompare)
                Else
                    eqsor = "0"
                End If
                If eqsor = "0" Then
                    If usemetero = "1" Then
                        Dim strMessage1 As String = "Original Status Required When Using Meter Frequency"
                        Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                        Exit Sub
                    End If
                End If
                Dim ordt, rdt As String
                ordt = txtordt.Text
                rdt = txtrdt.Text
                If ordt = "" Then
                    ordt = "0"
                End If
                If rdt = "" Then
                    rdt = "0"
                End If

                des = txtdesc.Value
                des = tasks.ModString2(des)
                odes = txtodesc.Value 'orig
                odes = tasks.ModString2(odes)
                If cbloto.Checked = True Then
                    lot = "1"
                Else
                    lot = "0"
                End If
                If cbcs.Checked = True Then
                    cs = "1"
                Else
                    cs = "0"
                End If
                tid = lbltaskid.Value
                Filter = lblfilt.Value
                'If ddcomp.SelectedIndex <> 0 Then
                'ci = ddcomp.SelectedValue.ToString
                'Else
                'ci = "0"
                'End If

                ci = lblco.Value

                fuid = lblfuid.Value
                cqty = txtcQty.Text

                'cn = ddcomp.SelectedItem.ToString
                cn = lblcompnum.Value
                cn = tasks.ModString2(cn)
                Dim fm As String
                Dim Item As ListItem
                Dim f, fi As String
                Dim ipar As Integer = 0
                For Each Item In lbfailmodes.Items
                    f = Item.Text.ToString
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                    If Len(fm) = 0 Then
                        fm = f & "(___)"
                    Else
                        fm += " " & f & "(___)"
                    End If
                Next
                fm = tasks.ModString2(fm)
                Dim ofm, ofs, ofi As String
                For Each Item In lbofailmodes.Items
                    ofs = Item.ToString
                    ipar = ofs.LastIndexOf("(")
                    If ipar <> -1 Then
                        ofs = Mid(ofs, 1, ipar)
                    End If

                    If Len(ofm) = 0 Then
                        ofm = ofs & "(___)"
                    Else
                        ofm += " " & ofs & "(___)"
                    End If
                Next
                ofm = tasks.ModString2(ofm)
                Dim ttid As String = lbltaskid.Value
                Dim usr As String = HttpContext.Current.Session("username").ToString()
                Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
                Dim pf As String = txtpfint.Text
                eqid = lbleqid.Value
                Dim hpm As String = lblhaspm.Value
                sid = lblsid.Value

                If ddpt.SelectedIndex <> 0 Then
                    pt = ddpt.SelectedValue
                    ptstr = ddpt.SelectedItem.ToString
                    ptstr = Replace(ptstr, "'", Chr(180), , , vbTextCompare)
                Else
                    pt = "0"
                End If
                If ddpto.SelectedIndex <> 0 Then
                    ptor = ddpto.SelectedValue 'orig +
                    ptstror = ddpto.SelectedItem.ToString
                    ptstror = Replace(ptstror, "'", Chr(180), , , vbTextCompare)
                Else
                    ptor = "0"
                End If
                If ddskill.SelectedIndex <> 0 Then
                    ski = ddskill.SelectedValue
                    skistr = ddskill.SelectedItem.ToString
                    skistr = Replace(skistr, "'", Chr(180), , , vbTextCompare)
                Else
                    ski = "0"
                End If
                If ski = "0" Then
                    If usemeter = "1" Then
                        Dim strMessage1 As String = "Revised Skill Required When Using Meter Frequency"
                        Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                        Exit Sub
                    End If
                End If
                If ddskillo.SelectedIndex <> 0 Then
                    skior = ddskillo.SelectedValue 'orig +
                    skistror = ddskillo.SelectedItem.ToString
                    skistror = Replace(skistror, "'", Chr(180), , , vbTextCompare)
                Else
                    skior = "0"
                End If
                If skior = "0" Then
                    If usemetero = "1" Then
                        Dim strMessage1 As String = "Original Skill Required When Using Meter Frequency"
                        Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                        Exit Sub
                    End If
                End If
                fixed = lblfixed.Value
                fixedo = lblfixedo.Value
                If cbfixed.Checked = True Then
                    fixed = "1"
                End If
                If cbfixedo.Checked = True Then
                    fixedo = "1"
                End If
                If Len(ttid) <> 0 Then
                    sql = "exec usp_updatepmtasks '" & ttid & "','" & des & "','" & qty & "','" & tr & "','" & eqs & "','" & eqsstr & "', " _
                    + "'" & fre & "','" & frestr & "','" & lot & "','" & cs & "','" & typ & "','" & typstr & "','" & ci & "', " _
                    + "'" & cn & "','" & fm & "','" & ofm & "','" & cqty & "','" & pf & "','" & rdt & "','" & Filter & "', " _
                    + "'" & PageNumber & "','" & fuid & "', '" & ustr & "','" & eqid & "','" & sid & "'," _
                    + "'" & typor & "','" & typstror & "','" & freor & "','" & frestror & "','" & qtyor & "', " _
                    + "'" & tror & "','" & eqsor & "','" & eqsstror & "','" & taskstat & "','" & ordt & "','" & hpm & "', " _
                    + "'" & pt & "','" & ptstr & "','" & ski & "','" & skistr & "','" & ptor & "','" & ptstror & "','" & skior & "','" & skistror & "','" & odes & "'"

                    'Dim typor, typstror, freor, frestror, ptor, ptstror As String
                    'Dim skior, skistror, qtyor, tror, eqsor, eqsstror As String

                    Dim cmd As New SqlCommand
                    cmd.CommandText = "exec usp_updatepmtasksopt_orig @pmtskid, @des, @qty, @ttime, @rdid, @rd, " _
                    + "@fre, @freq, @lotoid, @conid, @ttid, @tasktype, @comid, " _
                    + "@compnum, @fm1, @ofm1, @cqty, @pfinterval, @rdt, @filter, " _
                    + "@pagenumber, @fuid, @ustr, @eqid, @sid, " _
                    + "@ottid, @otasktype, @ofre, @ofreq, @oqty, @ottime, @ordid, @ord, @taskstat, @ordt, @hpm, " _
                    + "@pt, @ptstr, @ski, @skistr, @ptor, @ptstror, @skior, @skistror, @odes, @fixed, @fixedo"

                    Dim param = New SqlParameter("@pmtskid", SqlDbType.Int)
                    param.Value = ttid
                    cmd.Parameters.Add(param)
                    Dim param01 = New SqlParameter("@des", SqlDbType.VarChar)
                    If des = "" Then
                        param01.Value = System.DBNull.Value
                    Else
                        param01.Value = des
                    End If
                    cmd.Parameters.Add(param01)
                    Dim param02 = New SqlParameter("@qty", SqlDbType.Int)
                    If qty = "" Then
                        param02.Value = "1"
                    Else
                        param02.Value = qty
                    End If
                    cmd.Parameters.Add(param02)
                    Dim param03 = New SqlParameter("@ttime", SqlDbType.Decimal)
                    If tr = "" Then
                        param03.Value = "0"
                    Else
                        param03.Value = tr
                    End If
                    cmd.Parameters.Add(param03)
                    Dim param04 = New SqlParameter("@rdid", SqlDbType.Int)
                    If eqs = "" Then
                        param04.Value = "0"
                    Else
                        param04.Value = eqs
                    End If
                    cmd.Parameters.Add(param04)
                    Dim param05 = New SqlParameter("@rd", SqlDbType.VarChar)
                    If eqsstr = "" Or eqsstr = "Select" Then
                        param05.Value = System.DBNull.Value
                    Else
                        param05.Value = eqsstr
                    End If
                    cmd.Parameters.Add(param05)
                    Dim param06 = New SqlParameter("@fre", SqlDbType.Int)
                    If fre = "" Then
                        param06.Value = "0"
                    Else
                        param06.Value = fre
                    End If
                    cmd.Parameters.Add(param06)
                    Dim param07 = New SqlParameter("@freq", SqlDbType.VarChar)
                    If frestr = "" Or frestr = "Select" Then
                        param07.Value = System.DBNull.Value
                    Else
                        param07.Value = frestr
                    End If
                    cmd.Parameters.Add(param07)
                    Dim param08 = New SqlParameter("@lotoid", SqlDbType.Int)
                    If lot = "" Then
                        param08.Value = "0"
                    Else
                        param08.Value = lot
                    End If
                    cmd.Parameters.Add(param08)
                    Dim param09 = New SqlParameter("@conid", SqlDbType.Int)
                    If cs = "" Then
                        param09.Value = "0"
                    Else
                        param09.Value = cs
                    End If
                    cmd.Parameters.Add(param09)
                    Dim param10 = New SqlParameter("@ttid", SqlDbType.Int)
                    If typ = "" Then
                        param10.Value = "0"
                    Else
                        param10.Value = typ
                    End If
                    cmd.Parameters.Add(param10)
                    Dim param11 = New SqlParameter("@tasktype", SqlDbType.VarChar)
                    If typstr = "" Or typstr = "Select" Then
                        param11.Value = System.DBNull.Value
                    Else
                        param11.Value = typstr
                    End If
                    cmd.Parameters.Add(param11)
                    Dim param12 = New SqlParameter("@comid", SqlDbType.Int)
                    If ci = "" Or ci = "Select" Then
                        param12.Value = "0"
                    Else
                        param12.Value = ci
                    End If
                    cmd.Parameters.Add(param12)
                    Dim param13 = New SqlParameter("@compnum", SqlDbType.VarChar)
                    If cn = "" Or cn = "Select" Then
                        param13.Value = System.DBNull.Value
                    Else
                        param13.Value = cn
                    End If
                    cmd.Parameters.Add(param13)
                    Dim param14 = New SqlParameter("@fm1", SqlDbType.VarChar)
                    If fm = "" Then
                        param14.Value = System.DBNull.Value
                    Else
                        param14.Value = fm
                    End If
                    cmd.Parameters.Add(param14)
                    Dim param140 = New SqlParameter("@ofm1", SqlDbType.VarChar)
                    If ofm = "" Then
                        param140.Value = System.DBNull.Value
                    Else
                        param140.Value = ofm
                    End If
                    cmd.Parameters.Add(param140)
                    Dim param15 = New SqlParameter("@cqty", SqlDbType.Int)
                    If cqty = "" Then
                        param15.Value = "1"
                    Else
                        param15.Value = cqty
                    End If
                    cmd.Parameters.Add(param15)
                    Dim param16 = New SqlParameter("@pfinterval", SqlDbType.Int)
                    If pf = "" Then
                        param16.Value = System.DBNull.Value
                    Else
                        param16.Value = pf
                    End If
                    cmd.Parameters.Add(param16)
                    Dim param17 = New SqlParameter("@rdt", SqlDbType.Decimal)
                    If rdt = "" Then
                        param17.Value = "0"
                    Else
                        param17.Value = rdt
                    End If
                    cmd.Parameters.Add(param17)
                    Dim param18 = New SqlParameter("@filter", SqlDbType.VarChar)
                    If Filter = "" Then
                        param18.Value = System.DBNull.Value
                    Else
                        param18.Value = Filter
                    End If
                    cmd.Parameters.Add(param18)
                    Dim param19 = New SqlParameter("@pagenumber", SqlDbType.Int)
                    param19.Value = lblt.Value
                    cmd.Parameters.Add(param19)
                    Dim param20 = New SqlParameter("@fuid", SqlDbType.Int)
                    If fuid = "" Then
                        param20.Value = "0"
                    Else
                        param20.Value = fuid
                    End If
                    cmd.Parameters.Add(param20)

                    Dim param48 = New SqlParameter("@ustr", SqlDbType.VarChar)
                    If ustr = "" Then
                        param48.Value = System.DBNull.Value
                    Else
                        param48.Value = ustr
                    End If
                    cmd.Parameters.Add(param48)
                    Dim param49 = New SqlParameter("@eqid", SqlDbType.Int)
                    param49.Value = eqid
                    cmd.Parameters.Add(param49)
                    Dim param50 = New SqlParameter("@sid", SqlDbType.Int)
                    param50.Value = sid
                    cmd.Parameters.Add(param50)

                    Dim param78 = New SqlParameter("@ottid", SqlDbType.Int)
                    If typor = "" Then
                        param78.Value = "0"
                    Else
                        param78.Value = typor
                    End If
                    cmd.Parameters.Add(param78)
                    Dim param79 = New SqlParameter("@otasktype", SqlDbType.VarChar)
                    If typstror = "" Or typstror = "Select" Then
                        param79.Value = System.DBNull.Value
                    Else
                        param79.Value = typstror
                    End If
                    cmd.Parameters.Add(param79)

                    Dim param80 = New SqlParameter("@ofre", SqlDbType.Int)
                    If freor = "" Then
                        param80.Value = "0"
                    Else
                        param80.Value = freor
                    End If
                    cmd.Parameters.Add(param80)
                    Dim param81 = New SqlParameter("@ofreq", SqlDbType.VarChar)
                    If frestror = "" Or frestror = "Select" Then
                        param81.Value = System.DBNull.Value
                    Else
                        param81.Value = frestror
                    End If
                    cmd.Parameters.Add(param81)

                    Dim param82 = New SqlParameter("@oqty", SqlDbType.Int)
                    If qtyor = "" Then
                        param82.Value = "1"
                    Else
                        param82.Value = qtyor
                    End If
                    cmd.Parameters.Add(param82)
                    Dim param83 = New SqlParameter("@ottime", SqlDbType.Decimal)
                    If tror = "" Then
                        param83.Value = "0"
                    Else
                        param83.Value = tror
                    End If
                    cmd.Parameters.Add(param83)

                    Dim param84 = New SqlParameter("@ordid", SqlDbType.Int)
                    If eqsor = "" Then
                        param84.Value = "0"
                    Else
                        param84.Value = eqsor
                    End If
                    cmd.Parameters.Add(param84)
                    Dim param85 = New SqlParameter("@ord", SqlDbType.VarChar)
                    If eqsstror = "" Or eqsstror = "Select" Then
                        param85.Value = System.DBNull.Value
                    Else
                        param85.Value = eqsstror
                    End If
                    cmd.Parameters.Add(param85)

                    Dim param86 = New SqlParameter("@taskstat", SqlDbType.VarChar)
                    If taskstat = "" Or taskstat = "Select" Then
                        param86.Value = System.DBNull.Value
                    Else
                        param86.Value = taskstat
                    End If
                    cmd.Parameters.Add(param86)
                    'Dim param87 = New SqlParameter("@taskstatid", SqlDbType.Int)
                    'If taskstatid = "" Then
                    'param87.Value = "0"
                    'Else
                    'param87.Value = taskstatid
                    'End If
                    'cmd.Parameters.Add(param87)

                    Dim param88 = New SqlParameter("@ordt", SqlDbType.Decimal)
                    If ordt = "" Then
                        param88.Value = "0"
                    Else
                        param88.Value = ordt
                    End If
                    cmd.Parameters.Add(param88)

                    Dim param89 = New SqlParameter("@hpm", SqlDbType.Int)
                    If hpm = "" Then
                        param89.Value = "0"
                    Else
                        param89.Value = hpm
                    End If
                    cmd.Parameters.Add(param89)

                    '+ "@pt, @ptstr, @ski, @skistr, @ptor, @ptstror, @skior, @skistror"
                    Dim param21 = New SqlParameter("@pt", SqlDbType.Int)
                    If pt = "" Then
                        param21.Value = "0"
                    Else
                        param21.Value = pt
                    End If
                    cmd.Parameters.Add(param21)
                    Dim param22 = New SqlParameter("@ptstr", SqlDbType.VarChar)
                    If ptstr = "" Or ptstr = "Select" Then
                        param22.Value = System.DBNull.Value
                    Else
                        param22.Value = ptstr
                    End If
                    cmd.Parameters.Add(param22)

                    Dim param23 = New SqlParameter("@ski", SqlDbType.Int)
                    If ski = "" Then
                        param23.Value = "0"
                    Else
                        param23.Value = ski
                    End If
                    cmd.Parameters.Add(param23)
                    Dim param24 = New SqlParameter("@skistr", SqlDbType.VarChar)
                    If skistr = "" Or ptstr = "Select" Then
                        param24.Value = System.DBNull.Value
                    Else
                        param24.Value = skistr
                    End If
                    cmd.Parameters.Add(param24)

                    Dim param25 = New SqlParameter("@ptor", SqlDbType.Int)
                    If ptor = "" Then
                        param25.Value = "0"
                    Else
                        param25.Value = ptor
                    End If
                    cmd.Parameters.Add(param25)
                    Dim param26 = New SqlParameter("@ptstror", SqlDbType.VarChar)
                    If ptstror = "" Or ptstror = "Select" Then
                        param26.Value = System.DBNull.Value
                    Else
                        param26.Value = ptstror
                    End If
                    cmd.Parameters.Add(param26)

                    Dim param27 = New SqlParameter("@skior", SqlDbType.Int)
                    If skior = "" Then
                        param27.Value = "0"
                    Else
                        param27.Value = skior
                    End If
                    cmd.Parameters.Add(param27)
                    Dim param28 = New SqlParameter("@skistror", SqlDbType.VarChar)
                    If skistror = "" Or ptstr = "Select" Then
                        param28.Value = System.DBNull.Value
                    Else
                        param28.Value = skistror
                    End If
                    cmd.Parameters.Add(param28)

                    Dim param29 = New SqlParameter("@odes", SqlDbType.VarChar)
                    If odes = "" Then
                        param29.Value = System.DBNull.Value
                    Else
                        param29.Value = odes
                    End If
                    cmd.Parameters.Add(param29)

                    Dim param30 = New SqlParameter("@fixed", SqlDbType.VarChar)
                    If fixed = "" Then
                        param30.Value = System.DBNull.Value
                    Else
                        param30.Value = fixed
                    End If
                    cmd.Parameters.Add(param30)
                    Dim param31 = New SqlParameter("@fixedo", SqlDbType.VarChar)
                    If fixedo = "" Then
                        param31.Value = System.DBNull.Value
                    Else
                        param31.Value = fixedo
                    End If
                    cmd.Parameters.Add(param31)

                    Dim tpm As Integer
                    Try
                        tpm = tasks.ScalarHack(cmd)
                    Catch ex As Exception
                        tpm = tasksadd.ScalarHack(cmd)
                    End Try

                    If tpm = 1 Then
                        lbltpmalert.Value = "yes"
                    Else
                        lbltpmalert.Value = "no"
                    End If

                    '*** New For Meters - Should add to procedure ***
                    Dim mfid, mfido As String
                    mfid = lblmfid.Value
                    mfido = lblmfido.Value
                    If mfid <> "" Then
                        sql = "update meterfreq set skillid = '" & ski & "', rdid = '" & eqs & "', skillqty = '" & qty & "' " _
                       + "where mfid = '" & mfid & "'"
                        Try
                            tasks.Update(sql)
                        Catch ex As Exception
                            tasksadd.Update(sql)
                        End Try
                    End If
                    If mfido <> "" Then
                        sql = "update meterfreqo set skillid = '" & skior & "', rdid = '" & eqsor & "', skillqty = '" & qtyor & "' " _
                   + "where mfid = '" & mfid & "'"
                        Try
                            tasks.Update(sql)
                        Catch ex As Exception
                            tasksadd.Update(sql)
                        End Try
                    End If
                    '***end new ***

                    'If otn <> tn Then
                    'fuid = lblfuid.Value
                    'sql = "usp_reorderPMTasks '" & ttid & "', '" & fuid & "', '" & tn & "', '" & otn & "'"
                    'Try
                    'tasks.Update(sql)
                    'Catch ex As Exception
                    'tasksadd.Update(sql)
                    'End Try

                    'End If

                    Dim currcnt As String = lblcurrsb.Value
                    Dim cscnt As String = lblcurrcs.Value
                    If st = 0 Then
                        If otn <> tn Then
                            PageNumber = tn
                        Else
                            PageNumber = lblpg.Text
                        End If

                    Else
                        PageNumber = currcnt + cscnt
                    End If

                    field = "funcid"
                    val = fuid
                    Filter = lblfilt.Value
                    Try
                        LoadPage(PageNumber, Filter)
                    Catch ex As Exception
                        LoadPage(PageNumber, Filter)
                    End Try

                    lblenable.Value = "1"
                    lblsave.Value = "yes"
                    eqid = lbleqid.Value
                    Try
                        tasks.UpMod(eqid)
                    Catch ex As Exception
                        tasksadd.UpMod(eqid)
                    End Try
                    'RBAS
                    rteid = lblrteid.Value
                    If rteid <> "0" And rteid <> "" Then
                        rbskillid = lblrbskillid.Value
                        rbqty = lblrbqty.Value
                        rbfreq = lblrbfreq.Value
                        rbrdid = lblrbrdid.Value
                        If rbskillid <> ski Then
                            RBASRem(ttid, rteid, rbskillid, rbqty, rbfreq, rbrdid)
                        ElseIf rbqty <> qty Then
                            RBASRem(ttid, rteid, rbskillid, rbqty, rbfreq, rbrdid)
                        ElseIf rbfreq <> frestr Then
                            RBASRem(ttid, rteid, rbskillid, rbqty, rbfreq, rbrdid)
                        ElseIf rbrdid <> eqs Then
                            RBASRem(ttid, rteid, rbskillid, rbqty, rbfreq, rbrdid)
                        End If
                    End If
                Else
                    Dim strMessage As String = "Problem Saving Record\nPlease review selections and try again."
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            Else
                Dim strMessage As String = "No Function Record Loaded Yet!"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

        End If

    End Sub
    Private Sub RBASRem(ByVal ttid As String, ByVal rteid As String, ByVal rbskillid As String, ByVal rbqty As String, ByVal rbfreq As String, ByVal rbrdid As String)
        'sql = "update pmtasks set rteid = null, rte = null, rteno = null, rteseq = null, rteseqold = null where pmtskid = '" & ttid & "'"
        'Try
        'tasks.Update(sql)
        'Catch ex As Exception
        'tasksadd.Update(sql)
        'End Try

        'sql = "update pmtasks set rteid = NULL, rtechng = 1 where rteid = '" & rteid & "' and rteno = 0"
        'Try
        'tasks.Update(sql)
        'Catch ex As Exception
        'tasksadd.Update(sql)
        'End Try

        'sql = "usp_rbas_dflt_seq '" & rteid & "','" & rbskillid & "','" & rbqty & "','" & rbfreq & "','" & rbrdid & "'"
        sql = "usp_rem_rbas_seq '" & rteid & "','" & ttid & "'"
        Try
            tasks.Update(sql)
        Catch ex As Exception
            tasksadd.Update(sql)
        End Try

    End Sub
    Private Sub DelRevised()
        ro = lblro.Value
        If ro <> "1" Then
            Dim tid As String = lbltaskid.Value
            Dim tnum As String = lblpg.Text
            sid = lblsid.Value
            fuid = lblfuid.Value
            tasks.Open()
            'SaveTask2()
            'RBAS
            rteid = lblrteid.Value
            If rteid <> "0" And rteid <> "" Then
                rbskillid = lblrbskillid.Value
                rbqty = lblrbqty.Value
                rbfreq = lblrbfreq.Value
                rbrdid = lblrbrdid.Value
                RBASRem(tid, rteid, rbskillid, rbqty, rbfreq, rbrdid)
            End If
            sql = "usp_delrevised '" & tid & "','" & tnum & "','" & fuid & "','" & sid & "'"
            tasks.Update(sql)
            PageNumber = lblpg.Text
            Filter = lblfilt.Value
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
            
            LoadPage(PageNumber, Filter)
            lblenable.Value = "1"
            tasks.Dispose()
        End If

    End Sub

    Private Sub ibCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibCancel.Click
        tasks.Open()
        GetLists()
        Filter = lblfilt.Value
        LoadPage(PageNumber, Filter)
    End Sub

    

    Protected Sub btntpm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntpm.Click
        tasks.Open()
        UpTPM()
        Filter = lblfilt.Value
        PageNumber = lblpg.Text
        LoadPage(PageNumber, Filter)
        tasks.Dispose()
    End Sub
End Class
