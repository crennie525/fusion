<%@ Page Language="vb" AutoEventWireup="false" Codebehind="cclasslookup.aspx.vb" Inherits="lucy_r12.cclasslookup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>cclasslookup</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="JavaScript" src="../scripts/sessrefdialog.js"></script>
		<script language="JavaScript" src="../scripts1/cclasslookupaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="resetsess();">
		<form id="form1" method="post" runat="server">
			<table id="tbltop" width="650" runat="server">
				<tr>
					<td class="thdrsingrt label" colSpan="3"><asp:Label id="lang1659" runat="server">Component Class Lookup Dialog</asp:Label></td>
				</tr>
				<tr id="tbleqrec" runat="server">
					<td class="bluelabel" width="80"><asp:Label id="lang1660" runat="server">Search</asp:Label></td>
					<td width="310"><asp:textbox id="txtsrch" runat="server" Width="250px"></asp:textbox><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
					<td width="210"></td>
				</tr>
				<tr>
					<td id="tdcomp" align="center" colSpan="3" runat="server"></td>
				</tr>
				<tr>
					<td align="center" colSpan="6">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<iframe id="ifsession" class="details" src="" frameBorder="no" runat="server" style="Z-INDEX: 0">
			</iframe><input type="hidden" id="lblsessrefresh" runat="server" NAME="lblsessrefresh">
			<input id="lblret" type="hidden" name="lblret" runat="server"> <input id="txtpg" type="hidden" name="txtpg" runat="server">
			<input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
