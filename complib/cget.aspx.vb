

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class cget
    Inherits System.Web.UI.Page
	Protected WithEvents lang1672 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1671 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1670 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1669 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1668 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1667 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1666 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1665 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1664 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1663 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1662 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1661 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim rl As New Utilities
    Dim Tables As String = "compclass"
    Dim PK As String = "acid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String
    Dim intPgCnt, intPgNav As Integer
    Dim comid, clib, sid As String
    Protected WithEvents lblacid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcclass As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblscid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents tblcclass As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblsubclass As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdscomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tblsclass As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblreturn As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdnewclass As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewsub As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewfm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblfmcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnosub As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnofm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdcopyfm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbcopyfm As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents trreplace As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents rbget As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbnot As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbcc As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbdc As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents lblfm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllib As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsclass As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents tbltop As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tbleqrec As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            comid = Request.QueryString("comid").ToString
            clib = Request.QueryString("lib").ToString
            sid = HttpContext.Current.Session("dfltps").ToString
            lblcomid.Value = comid
            lbllib.Value = clib
            lblsid.Value = sid
            txtpg.Value = "1"
            rl.Open()
            'GetFCnt()
            GetComps()
            rl.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                rl.Open()
                GetNext()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                rl.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetComps()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                rl.Open()
                GetPrev()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                rl.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetComps()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "getsub" Then
                rl.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                tblcclass.Attributes.Add("class", "details")
                tblsclass.Attributes.Add("class", "view")
                PopSub()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "savesub" Then
                rl.Open()
                SaveSub()
                rl.Dispose()
                'lblret.Value = ""
            ElseIf Request.Form("lblret") = "saveit" Then
                rl.Open()
                SaveAll()
                rl.Dispose()
                'lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetFCnt()
        comid = lblcomid.Value
        clib = lbllib.Value
        Dim fcnt As Integer
        If clib = "yes" Then
            sql = "select count(*) from complibfm where comid = '" & comid & "'"
        Else
            sql = "select count(*) from componentfailmodes where comid = '" & comid & "'"
        End If
        fcnt = rl.Scalar(sql)
        lblfmcnt.Value = fcnt
    End Sub
    Private Sub SaveSub()
        comid = lblcomid.Value
        clib = lbllib.Value
        Dim cclass, acid As String
        cclass = lblcclass.Value
        acid = lblacid.Value
        If clib = "yes" Then
            sql = "update complib set assetclass = '" & cclass & "', acid = '" & acid & "' where comid = '" & comid & "'"
        Else
            sql = "update components set assetclass = '" & cclass & "', acid = '" & acid & "' where comid = '" & comid & "'"
        End If
        rl.Update(sql)
        lblret.Value = "handlereturn"


    End Sub
    Private Sub SaveAll()
        comid = lblcomid.Value
        clib = lbllib.Value
        Dim cclass, acid As String
        cclass = lblcclass.Value
        acid = lblacid.Value
        Dim sclass, scid As String
        sclass = lblsclass.Value
        scid = lblscid.Value
        Dim sid As String = lblsid.Value
        If clib = "yes" Then
            sql = "update complib set assetclass = '" & cclass & "', acid = '" & acid & "', " _
            + "scid = '" & scid & "', subclass = '" & sclass & "' where comid = '" & comid & "'"
        Else
            sql = "update components set assetclass = '" & cclass & "', acid = '" & acid & "', " _
            + "scid = '" & scid & "', subclass = '" & sclass & "' where comid = '" & comid & "'"
        End If
        rl.Update(sql)
        If cbcopyfm.Checked = True Then
            If clib = "yes" Then
                sql = "delete from complibfm where comid = '" & comid & "'"

            Else
                sql = "delete from componentfailmodes where comid = '" & comid & "'"

            End If
            rl.Update(sql)
        End If
        If rbcc.Checked = True Then
            If clib = "yes" Then
                sql = "insert into complibfm (comid, compid, failindex, failid, failuremode) " _
                + "select '" & comid & "', '0', '0', c.failid, f.failuremode from compclasssubfm c " _
                + "left join failuremodes f on f.failid = c.failid " _
                + "where c.scid = '" & scid & "' and c.failid not in (select failid from complibfm where comid = '" & comid & "') order by failuremode"
            Else
                sql = "insert into componentfailmodes (comid, compid, siteid, failindex, failid, failuremode, addressed) " _
                 + "select '" & comid & "', '0', '" & sid & "', '0', c.failid, f.failuremode, '0' from compclasssubfm c " _
                + "left join failuremodes f on f.failid = c.failid " _
                + "where c.scid = '" & scid & "' and c.failid not in (select failid from componentfailmodes where comid = '" & comid & "') order by failuremode"
            End If
            rl.Update(sql)
        End If
        lblret.Value = "handlereturn"
    End Sub
    Private Sub PopSub()
        Dim cclass As String
        cclass = lblcclass.Value
        sql = "select s.*, f.failuremodes from compclasssub s left join compclasssubfmout f on f.scid = s.scid " _
        + "where assetclass = '" & cclass & "'"
        Dim scid, sclass, fcstr As String
        Dim sb As New StringBuilder

        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 684px;  HEIGHT: 260px"">")
        sb.Append("<table>")
        sb.Append("<tr><td class=""btmmenu plainlabel"" width=""180"">" & tmod.getlbl("cdlbl409" , "cget.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""452"">" & tmod.getlbl("cdlbl410" , "cget.aspx.vb") & "</td>")
        sb.Append("<td width=""20"">&nbsp;</td></tr>")

        dr = rl.GetRdrData(sql)
        While dr.Read
            scid = dr.Item("scid").ToString
            sclass = dr.Item("subclass").ToString
            fcstr = dr.Item("failuremodes").ToString
            sb.Append("<tr>")
            sb.Append("<td class=""linklabel""><a href=""#"" onclick=""rtret2('" & scid & "', '" & sclass & "', '" & fcstr & "');"">")
            sb.Append(sclass & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & fcstr & "</td>")
            sb.Append("</tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        sb.Append("</div")
        tdscomp.InnerHtml = sb.ToString

    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetComps()
        Catch ex As Exception
            rl.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr634" , "cget.aspx.vb")
 
            rl.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetComps()
        Catch ex As Exception
            rl.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr635" , "cget.aspx.vb")
 
            rl.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetComps()

        Dim srch As String = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        If Len(srch) > 0 Then
            srch = "%" & srch & "%"
            Filter = "assetclass like ''" & srch & "'' or classdesc like ''" & srch & "''"
            FilterCnt = "assetclass like '" & srch & "' or classdesc like '" & srch & "'"
        Else
            Filter = ""
            FilterCnt = ""
        End If
        Dim sb As StringBuilder = New StringBuilder

        sb.Append("<table width=""700"">")
        sb.Append("<tr><td colspan=""6""><div style=""OVERFLOW: auto; width: 700px; height: 260px;"">")
        sb.Append("<table width=""680"">")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""170"">" & tmod.getlbl("cdlbl411" , "cget.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""270"">" & tmod.getlbl("cdlbl412" , "cget.aspx.vb") & "</td>")
        Dim rowid As Integer = 0
        If FilterCnt <> "" Then
            sql = "select count(*) from compclass where " & FilterCnt
        Else
            sql = "select count(*) from compclass"
        End If
        PageNumber = txtpg.Value
        intPgCnt = rl.Scalar(sql)
        intPgNav = rl.PageCount(intPgCnt, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav
        dr = rl.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        Dim bc, ci, co, cd, cs, cg As String
        While dr.Read
            If rowid = 0 Then
                rowid = 1
                bc = "white"
            Else
                rowid = 0
                bc = "#E7F1FD"
            End If
            ci = dr.Item("acid").ToString
            co = dr.Item("assetclass").ToString
            cd = dr.Item("classdesc").ToString

            sb.Append("<tr bgcolor=""" & bc & """><td class=""linklabel""><a href=""#"" onclick=""rtret('" & ci & "', '" & co & "');"">")
            sb.Append(co & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & cd & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        dr.Close()
        sb.Append("</table></div></td></tr></table>")
        tdcomp.InnerHtml = sb.ToString
    End Sub


    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        rl.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        GetComps()
        rl.Dispose()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1661.Text = axlabs.GetASPXPage("cget.aspx", "lang1661")
        Catch ex As Exception
        End Try
        Try
            lang1662.Text = axlabs.GetASPXPage("cget.aspx", "lang1662")
        Catch ex As Exception
        End Try
        Try
            lang1663.Text = axlabs.GetASPXPage("cget.aspx", "lang1663")
        Catch ex As Exception
        End Try
        Try
            lang1664.Text = axlabs.GetASPXPage("cget.aspx", "lang1664")
        Catch ex As Exception
        End Try
        Try
            lang1665.Text = axlabs.GetASPXPage("cget.aspx", "lang1665")
        Catch ex As Exception
        End Try
        Try
            lang1666.Text = axlabs.GetASPXPage("cget.aspx", "lang1666")
        Catch ex As Exception
        End Try
        Try
            lang1667.Text = axlabs.GetASPXPage("cget.aspx", "lang1667")
        Catch ex As Exception
        End Try
        Try
            lang1668.Text = axlabs.GetASPXPage("cget.aspx", "lang1668")
        Catch ex As Exception
        End Try
        Try
            lang1669.Text = axlabs.GetASPXPage("cget.aspx", "lang1669")
        Catch ex As Exception
        End Try
        Try
            lang1670.Text = axlabs.GetASPXPage("cget.aspx", "lang1670")
        Catch ex As Exception
        End Try
        Try
            lang1671.Text = axlabs.GetASPXPage("cget.aspx", "lang1671")
        Catch ex As Exception
        End Try
        Try
            lang1672.Text = axlabs.GetASPXPage("cget.aspx", "lang1672")
        Catch ex As Exception
        End Try

    End Sub

End Class
