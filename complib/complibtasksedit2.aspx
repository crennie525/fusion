<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="complibtasksedit2.aspx.vb"
    Inherits="lucy_r12.complibtasksedit2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>complibtasksedit2</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="javascript" src="../scripts/pmtaskdivfunceditco_1016a.js"></script>
    <script language="JavaScript" type="text/javascript">
		<!--
        function getfail2() {
            //eqid, fuid, coid, sid, pmtskid, eqnum, func, comp, task
            var eqid = document.getElementById("lbleqid").value;
            var fuid = document.getElementById("lblfuid").value;
            var coid = document.getElementById("lblco").value;
            var sid = document.getElementById("lblsid").value;
            var task = document.getElementById("lbltaskid").value;
            var eqnum = "";
            var func = "";
            var comp = "";
            var pmtskid = document.getElementById("lblpg").innerHTML;

            var eReturn = window.showModalDialog("../apps/taskwodialog.aspx?eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&sid=" + sid + "&pmtskid=" + pmtskid + "&eqnum=" + eqnum + "&comp=" + comp + "&func=" + func + "&task=" + task, "", "dialogHeight:600px; dialogWidth:900px; resizable=yes");
            if (eReturn) {

            }
        }
        function getmeter() {
            var enable = document.getElementById("lblenable").value;
            var eqid = document.getElementById("lbleqid").value;
            var eqnum = document.getElementById("lbleqnum").value;
            var pmtskid = document.getElementById("lbltaskid").value;
            var fuid = document.getElementById("lblfuid").value;
            var skillid = document.getElementById("lblskillid").value;
            var skillqty = document.getElementById("lblskillqty").value;
            var rdid = document.getElementById("lblrdid").value;
            var func = document.getElementById("lblfunc").value;
            var skill = document.getElementById("lblskill").value;
            var rd = document.getElementById("lblrd").value;
            var meterid = document.getElementById("lblmeterid").value;
            var varflg = 0;
            if (enable == "0" && pmtskid != "") {
                if (skillid == "" || skillid == "0") {
                    skillid = document.getElementById("ddskill").value;
                    if (skillid == "" || skillid == "0") {
                        alert("Skill Required")
                        varflg = 1;
                    }
                    else {
                        var sklist = document.getElementById("ddskill");
                        var skstr = sklist.options[sklist.selectedIndex].text;
                        skill = skstr;
                    }
                }
                else if (skillqty == "") {
                    skillqty = document.getElementById("txtqty").value;
                    if (skillqty == "") {
                        alert("Skill Qty Required")
                        varflg = 1;
                    }
                }
                else if (rdid == "" || rdid == "0") {
                    rdid = document.getElementById("ddeqstat").value;
                    if (rdid == "" || rdid == "0") {
                        alert("Running or Down Value Required")
                        varflg = 1;
                    }
                    else {
                        var rdlist = document.getElementById("ddeqstat");
                        var rdstr = sklist.options[rdlist.selectedIndex].text;
                        rd = rdstr;
                    }
                }
                if (varflg == 0) {
                    var eReturn = window.showModalDialog("../equip/usemeterdialog.aspx?typ=reg&eqid=" + eqid + "&pmtskid=" + pmtskid + "&fuid=" + fuid + "&skillid=" + skillid
                                   + "&skillqty=" + skillqty + "&rdid=" + rdid + "&func=" + func + "&skill=" + skill + "&rd=" + rd
                                   + "&meterid=" + meterid + "&rdt=" + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:900px;resizable=yes");
                    if (eReturn) {
                        document.getElementById("lblcompchk").value = "2";
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        function getmetero() {
            var enable = document.getElementById("lblenable").value;
            var eqid = document.getElementById("lbleqid").value;
            var eqnum = document.getElementById("lbleqnum").value;
            var pmtskid = document.getElementById("lbltaskid").value;
            var fuid = document.getElementById("lblfuid").value;
            var skillid = document.getElementById("lblskillid").value;
            var skillqty = document.getElementById("lblskillqty").value;
            var rdid = document.getElementById("lblrdid").value;
            var skill = document.getElementById("lblskill").value;
            var rd = document.getElementById("lblrd").value;
            var func = document.getElementById("lblfunc").value;
            var meterid = document.getElementById("lblmeterid").value;
            var varflg = 0;
            if (skillid == "" || skillid == "0") {
                skillid = document.getElementById("ddskill").value;
                if (skillid == "" || skillid == "0") {
                    alert("Revised Skill Required")
                    varflg = 1;
                }
                else {
                    var sklist = document.getElementById("ddskill");
                    var skstr = sklist.options[sklist.selectedIndex].text;
                    skill = skstr;
                }
            }
            else if (skillqty == "") {
                skillqty = document.getElementById("txtqty").value;
                if (skillqty == "") {
                    alert("Revised Skill Qty Required")
                    varflg = 1;
                }
            }
            else if (rdid == "" || rdid == "0") {
                rdid = document.getElementById("ddeqstat").value;
                if (rdid == "" || rdid == "0") {
                    alert("Revised Running or Down Value Required")
                    varflg = 1;
                }
                else {
                    var rdlist = document.getElementById("ddeqstat");
                    var rdstr = sklist.options[rdlist.selectedIndex].text;
                    rd = rdstr;
                }
            }

            //orig
            if (varflg == 0) {
                var skillido = document.getElementById("lblskillido").value;
                var skillqtyo = document.getElementById("lblskillqtyo").value;
                var rdido = document.getElementById("lblrdido").value;
                var skillo = document.getElementById("lblskillo").value;
                var rdo = document.getElementById("lblrdo").value;
                var meterido = document.getElementById("lblmeterido").value;

                if (enable == "0" && pmtskid != "") {
                    if (skillido == "" || skillido == "0") {
                        skillido = document.getElementById("ddskillo").value;
                        if (skillido == "" || skillido == "0") {
                            alert("Original Skill Required")
                            varflg = 1;
                        }
                        else {
                            var sklist = document.getElementById("ddskillo");
                            var skstr = sklist.options[sklist.selectedIndex].text;
                            skillo = skstr;
                        }
                    }
                    else if (skillqtyo == "") {
                        skillqtyo = document.getElementById("txtqtyo").value;
                        if (skillqtyo == "") {
                            alert("Original Skill Qty Required")
                            varflg = 1;
                        }
                    }
                    else if (rdido == "" || rdido == "0") {
                        rdido = document.getElementById("ddeqstato").value;
                        if (rdido == "" || rdido == "0") {
                            alert("Original Running or Down Value Required")
                            varflg = 1;
                        }
                        else {
                            var rdlist = document.getElementById("ddeqstato");
                            var rdstr = sklist.options[rdlist.selectedIndex].text;
                            rdo = rdstr;
                        }
                    }
                    if (varflg == 0) {
                        var eReturn = window.showModalDialog("../equip/usemeterdialog.aspx?typ=orig&eqid=" + eqid + "&pmtskid=" + pmtskid + "&fuid=" + fuid + "&skillid=" + skillid
                                   + "&skillqty=" + skillqty + "&rdid=" + rdid + "&func=" + func + "&skill=" + skill + "&rd=" + rd
                                   + "&meterid=" + meterid + "&rdt=" + "&eqnum=" + eqnum + "&skillido=" + skillido
                                  + "&skillqtyo=" + skillqtyo + "&rdido=" + rdido + "&skillo=" + skillo + "&rdo=" + rdo
                                  + "&meterido=" + meterido + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:900px;resizable=yes");
                        if (eReturn) {
                            document.getElementById("lblcompchk").value = "2";
                            document.getElementById("form1").submit();
                        }
                    }
                }
            }

        }
        function doEnable() {

            var tpmhold = document.getElementById("lbltpmhold").value;
            var ro = document.getElementById("lblro").value;
            var pg = document.getElementById("lblpg").innerHTML;
            document.getElementById("lblenable").value = "0";
            if (pg != "0" && ro != "1" && tpmhold != "1") {
                var ismeter = document.getElementById("lblusemeter").value;
                document.getElementById("lblenable").value = "0";

                //document.getElementById("ddcomp.disabled=false;
                document.getElementById("lbfaillist").disabled = false;
                document.getElementById("lbfailmodes").disabled = false;
                document.getElementById("lbCompFM").disabled = false;
                document.getElementById("txtcQty").disabled = false;
                document.getElementById("ibReuse").disabled = false;
                document.getElementById("ibToTask").disabled = false;
                document.getElementById("ibFromTask").disabled = false;
                document.getElementById("txtdesc").disabled = false;
                document.getElementById("ddtype").disabled = false;
                if (ismeter != "1") {
                    document.getElementById("txtfreq").disabled = false;
                    document.getElementById("cbfixed").disabled = false;
                }
                document.getElementById("txtpfint").disabled = false;
                document.getElementById("txtqty").disabled = false;
                document.getElementById("txttr").disabled = false;
                document.getElementById("txtrdt").disabled = false;
                //document.getElementById("ddpt.disabled=false;
                var sstr;
                var ssti;
                var typlst;
                typlst = document.getElementById("ddtype");
                sstr = typlst.options[typlst.selectedIndex].text
                ssti = document.getElementById("ddtype").value;
                if (sstr == "7") {
                    document.getElementById("ddpt").disabled = false;
                    try {
                        document.getElementById("ddpto").disabled = false;
                    }
                    catch (err) {

                    }
                }
                document.getElementById("ddeqstat").disabled = false;
                document.getElementById("ddskill").disabled = false;
                document.getElementById("cbloto").disabled = false;

                document.getElementById("cbcs").disabled = false;

                //document.getElementById("btnaddtsk").disabled=false;
                //document.getElementById("btnaddsubtask").disabled=false;
                try {
                    document.getElementById("btndeltask").disabled = false;
                }
                catch (err) {
                    //do nothing - button not used in optimizer
                }
                try {
                    document.getElementById("btntpm").disabled = false;
                }
                catch (err) {

                }
                document.getElementById("ibCancel").disabled = false;
                //document.getElementById("btnsavetask").disabled=false;
                var ismeter = document.getElementById("lblusemeter").value;
                var ismetero = document.getElementById("lblusemetero").value;
                if (oflag == "1") {
                    document.getElementById("lbofailmodes").disabled = false;
                    document.getElementById("txtodesc").disabled = false;
                    document.getElementById("ddtypeo").disabled = false;
                    if (ismetero != "1") {
                        document.getElementById("txtfreqo").disabled = false;
                        document.getElementById("cbfixedo").disabled = false;
                    }
                    document.getElementById("txtqtyo").disabled = false;
                    document.getElementById("txttro").disabled = false;
                    document.getElementById("txtordt").disabled = false;
                    document.getElementById("ddpto").disabled = false;
                    document.getElementById("ddeqstato").disabled = false;
                    document.getElementById("ddskillo").disabled = false;
                    document.getElementById("ddtaskstat").disabled = false;
                }
            }
            else {
                if (pg == "0") {
                    alert("No Task Records Are Loaded Yet!")
                }
                else if (tpmhold == "1") {
                    alert("This has been designated as a TPM Task and Cannot be Edited!")
                }
            }
        }
        function jumptpm() {
            funid = document.getElementById("lblfuid").value;
            comid = document.getElementById("lblco").value;

        }
        function editadj(typ) {
            ////window.parent.setref();
            var chken = document.getElementById("lblenable").value;
            ptid = document.getElementById("lbltaskid").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            if (chken != "1") {
                if (ptid != "" && tpmhold != "1") {
                    var fuid = document.getElementById("lblfuid").value;
                    var tid = document.getElementById("lbltaskid").value;
                    var ro = document.getElementById("lblro").value;
                    if (typ == "pre") {
                        tid = 0;
                    }
                    var eReturn = window.showModalDialog("../appsman/PMSetAdjManDialog.aspx?fuid=" + fuid + "&typ=" + typ + "&tid=" + tid + "&ro=" + ro, "", "dialogHeight:520px; dialogWidth:930px; resizable=yes");
                    if (eReturn) {

                    }
                }
            }
        }
        function DisableButton(b) {
            document.getElementById("ibToTask").className = "details";
            document.getElementById("ibFromTask").className = "details";
            document.getElementById("ibfromo").className = "details";
            document.getElementById("ibtoo").className = "details";
            document.getElementById("ibReuse").className = "details";
            document.getElementById("todis").className = "view";
            document.getElementById("fromdis").className = "view";
            document.getElementById("todis1").className = "view";
            document.getElementById("fromdis1").className = "view";
            document.getElementById("fromreusedis").className = "view";
            document.getElementById("form1").submit();
        }
        function FreezeScreen(msg) {
            scroll(0, 0);
            var outerPane = document.getElementById('FreezePane');
            var innerPane = document.getElementById('InnerFreezePane');
            if (outerPane) outerPane.className = 'FreezePaneOn';
            if (innerPane) innerPane.innerHTML = msg;
        }
        function checkdeltask() {
            var chken = document.getElementById("lblenable").value;
            if (chken != "1") {
                tid = document.getElementById("lbltaskid").value;
                var tpmhold = document.getElementById("lbltpmhold").value;
                if (tid != "" && tid != "0" && tpmhold != "1") {
                    var deltask = confirm("Are you sure you want to Delete this Task?");
                    if (deltask == true) {
                        document.getElementById("lblcompchk").value = "7";
                        FreezeScreen('Your Data Is Being Processed...');
                        document.getElementById("form1").submit();
                    }
                    else if (deltask == false) {
                        return false;
                        alert("Action Cancelled")
                    }
                }
            }
        }
        function checkdeltasktpm() {
            var chken = document.getElementById("lblenable").value;
            //if(chken!="1") {
            tid = document.getElementById("lbltaskid").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            //alert(tid + "," + tpmhold)
            if (tid != "" && tid != "0" && tpmhold != "0") {
                var deltask = confirm("Removing this task from TPM will result in the loss of images and scheduling data.\nDo you want to continue?");
                if (deltask == true) {
                    document.getElementById("lblcompchk").value = "deltpm";
                    FreezeScreen('Your Data Is Being Processed...');
                    document.getElementById("form1").submit();
                }
                else if (deltask == false) {
                    return false;
                    alert("Action Cancelled")
                }
            }
            //}
        }
        function CheckChanges() {
            for (var i = 0; i < values.length; i++) {
                var elem = document.getElementById(ids[i]);
                if (elem)
                    if ((elem.type == 'checkbox' || elem.type == 'radio')
                  && values[i] != elem.checked)
                        var change = confirm("You have made changes to a Revised Task with a Task Status of Delete.\nAre you sure you want to save these changes?");
                    else if (!(elem.type == 'checkbox' || elem.type == 'radio') &&
                  elem.value != values[i])
                        var change = confirm("You have made changes to a Revised Task with a Task Status of Delete.\nAre you sure you want to save these changes?");
                    else
                        document.getElementById("lblcompchk").value = "3";
                FreezeScreen('Your Data Is Being Processed...');
                document.getElementById("form1").submit();

            }
            if (change == true) {
                //document.getElementById("submit("btnNext_Click", '')
                //return true;
                document.getElementById("lblcompchk").value = "3";
                FreezeScreen('Your Data Is Being Processed...');
                document.getElementById("form1").submit();
            }
            else if (change == false) {
                //document.getElementById("lblsvchk").value = "0";
                return false;
            }
        }
        function valpgnums() {
            if (document.getElementById("lblenable").value != "1") {
                var tpmhold = document.getElementById("lbltpmhold").value;
                if (tpmhold != "1") {
                    var desc = document.getElementById("txtdesc").innerHTML;
                    var odesc = document.getElementById("txtodesc").innerHTML;
                    var compchng = document.getElementById("txtcQty").value;
                    if (desc.length > 500) {
                        alert("Maximum Lenth for Task Description is 500 Characters")
                    }
                    else if (odesc.length > 500) {
                        alert("Maximum Lenth for Task Description is 500 Characters")
                    }
                    else if (isNaN(document.getElementById("txtqtyo").value)) {
                        alert("Original Skill Qty is Not a Number")
                    }
                    else if (isNaN(document.getElementById("txtqty").value)) {
                        alert("Revised Skill Qty is Not a Number")
                    }
                    else if (isNaN(document.getElementById("txtfreqo").value)) {
                        alert("Original Frequency is Not a Number")
                    }
                    else if (document.getElementById("txtfreqo").value < 1 && document.getElementById("txtfreqo").value != 0) {
                        alert("Original Frequency is Less Than 1")
                    }
                    else if (isNaN(document.getElementById("txtfreq").value)) {
                        alert("Revised Frequency is Not a Number")
                    }
                    else if (document.getElementById("txtfreq").value < 1) {
                        alert("Revised Frequency is Less Than 1")
                    }
                    else if (isNaN(document.getElementById("txttro").value)) {
                        alert("Original Skill Time is Not a Number")
                    }
                    else if (isNaN(document.getElementById("txttr").value)) {
                        alert("Revised Skill Time is Not a Number")
                    }
                    else if (isNaN(document.getElementById("txtordt").value)) {
                        alert("Original Running/Down Time is Not a Number")
                    }
                    else if (isNaN(document.getElementById("txtrdt").value)) {
                        alert("Revised Running/Down Time is Not a Number")
                    }
                    else if (document.getElementById("ddtaskstat").value == 'Delete') {
                        CheckChanges();
                    }

                    else if (compchng != "1" && compchng != "0" && compchng != "") {
                        vercompchng();
                    }
                    else {
                        document.getElementById("lblcompchk").value = "3";
                        FreezeScreen('Your Data Is Being Processed...');
                        document.getElementById("form1").submit();
                    }

                }
            }
        }

        function vercompchng() {
            var cqtychk = document.getElementById("txtcQty").value;
            var sklchk = document.getElementById("txttr").value;
            cqtychk = parseInt(cqtychk);
            sklchk = parseInt(sklchk);
            if (cqtychk > 1 && sklchk > -1) {
                //do pop up here
                var eReturn = window.showModalDialog("../appsopt/checkcompqtydialog.aspx", "", "dialogHeight:200px; dialogWidth:400px;resizable=yes")
                if (eReturn) {
                    //alert(eReturn)
                    if (eReturn == "1") {
                        multcomp();
                        document.getElementById("lblcompchk").value = "3";
                        FreezeScreen('Your Data Is Being Processed...');
                        document.getElementById("form1").submit();
                    }
                    else {
                        document.getElementById("lblcompchk").value = "3";
                        FreezeScreen('Your Data Is Being Processed...');
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        function GetGrid() {
            handleapp();
            document.getElementById("lblgrid").value = "yes";
            chk = document.getElementById("lblchk").value;
            cid = document.getElementById("lblcid").value;
            tid = document.getElementById("lbltaskid").value;
            funid = document.getElementById("lblfuid").value;
            comid = document.getElementById("lblco").value;
            clid = document.getElementById("lblclid").value;
            typ = document.getElementById("lbltyp").value;
            lid = document.getElementById("lbllid").value;
            pmid = "none"//document.getElementById("lbldocpmid").value;
            var ro = document.getElementById("lblro").value;
            if (pmid == "") pmid = "none";
            pmstr = document.getElementById("lbldocpmstr").Value
            if (pmstr == "") pmstr = "none";

            if (tid != "" || tid != "0") {
                //alert("../apps/GTasksFunc.aspx?app=opt&tli=5a&funid=" + funid + "&comid=no&cid=" + cid + "&chk=" + chk + "&pmid=" + pmid + "&pmstr=" + pmstr + "&lid=" + lid + "&typ=" + typ + "&date=" + Date())
                window.open("../apps/GTasksFunc2.aspx?app=opt&tli=5a&funid=" + funid + "&comid=no&cid=" + cid + "&chk=" + chk + "&pmid=" + pmid + "&pmstr=" + pmstr + "&lid=" + lid + "&typ=" + typ + "&date=" + Date() + "&ro=" + ro, target = "_top");
            }
            else {
                alert("No Task Records Selected Yet!")
            }
        }

        function checkit() {
            var tnum = document.getElementById("lblpg").innerHTML;
            ////window.parent.handletnum(tnum);

            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                ////window.parent.doref();
            }
            else {
                ////window.parent.setref();
            }
            var chk = document.getElementById("lblusetot").value;
            if (chk == "1") {
                document.getElementById("ddskillo").disabled = true;
                document.getElementById("txttro").disabled = true;
                document.getElementById("txtfreqo").disabled = true;
                document.getElementById("ddeqstato").disabled = true;
            }
            populateArrays();
            var start = document.getElementById("lblstart").value;
            if (start == "yes1") {
                GetNavGrid();
            }
            document.getElementById("lblstart").value = "no";

            var grid = document.getElementById("lblgrid").value;
            if (grid == "yes") {
                window.location.reload(false);
                document.getElementById("form1").submit();
            }
            var tpm = document.getElementById("lbltpmalert").value;
            var tpmhold = document.getElementById("lbltpmhold").value; //&&tpmhold!="1"
            if (tpm == "yes") {
                var decision = confirm("This Task Meets Your Requirements for TPM\nDo You Want to Transfer this Task to TPM?")
                if (decision == true) {
                    //document.getElementById("submit('ddcomp', '');
                    FreezeScreen('Your Data Is Being Processed...');
                    document.getElementById("lblcompchk").value = "uptpm";
                    document.getElementById("form1").submit();
                }
                else {
                    //document.getElementById("ddcomp").value = goback;

                    alert("Action Cancelled")
                }
            }
            var chk = document.getElementById("lblpar").value;
            if (chk == "task") {
                valu = document.getElementById("lbltaskid").value;
                var num = document.getElementById("lblt").innerHTML;
                //alert(num)
                ////window.parent.handletask(chk, valu, num);
            }
            else if (chk == "comp") {
                //alert(document.getElementById("lblco").value)
                cvalu = document.getElementById("lblco").value;
                tvalu = document.getElementById("lbltaskid").value;
                tnum = document.getElementById("lblt").value;
                //alert(tnum)
                ////window.parent.handleco(chk, cvalu, tvalu, tnum);
            }

        }
        function setref() {
            ////window.parent.setref();
        }
        function GetNavGrid() {
            handleapp();
            cid = document.getElementById("lblcid").value;
            sid = document.getElementById("lblsid").value;
            did = document.getElementById("lbldid").value;
            clid = document.getElementById("lblclid").value;
            eqid = document.getElementById("lbleqid").value;
            chk = document.getElementById("lblchk").value;
            fuid = document.getElementById("lblfuid").value;
            tcnt = document.getElementById("lblcnt").value;
            typ = document.getElementById("lbltyp").value;
            lid = document.getElementById("lbllid").value;
            if (tcnt != "0") {
                ////window.parent.handletasks("PMOptTasksGrid.aspx?start=yes&tl=5&tc=0&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&typ=" + typ + "&lid=" + lid + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:800px")
                //if (eReturn) {
                //	document.getElementById("lblsvchk").value = "7";
                //	document.getElementById("pgflag").value = eReturn;
                //	document.getElementById("submit();
                //}
            }
        }
        function tasklookup() {

            var chken = document.getElementById("lblenable").value;
            var ro = document.getElementById("lblro").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            if ((chken != "1" || ro == "1") && tpmhold != "1") {
                ////window.parent.setref();
                //var str = document.getElementById("txtdesc").innerHTML;
                var str = document.getElementById("txtdesc").value;
                //alert(str)
                var eReturn = window.showModalDialog("../apps/TaskLookup.aspx?str=" + str, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes");
                //alert(eReturn)
                if (eReturn != "no" && eReturn != "~none~") {
                    //document.getElementById("txtdesc").innerHTML = eReturn;
                    document.getElementById("txtdesc").value = eReturn;
                }
                else if (eReturn != "~none~") {
                    document.getElementById("form1").submit();
                }
            }
        }
        function jumpto() {

            var chken = document.getElementById("lblenable").value;
            var ro = document.getElementById("lblro").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            if ((chken != "1" || ro == "1") && tpmhold != "1") {
                ////window.parent.setref();
                var typ = document.getElementById("ddtype").value;
                var pdm = document.getElementById("ddpt").value;
                var tid = document.getElementById("lbltaskid").value;
                var sid = document.getElementById("lblsid").value;
                var ro = document.getElementById("lblro").value;
                var eReturn = window.showModalDialog("../apps/commontasksdialog.aspx?typ=" + typ + "&pdm=" + pdm + "&tid=" + tid + "&sid=" + sid + "&ro=" + ro, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    if (eReturn == "task") {
                        document.getElementById("lblcompchk").value = "6";
                        //document.getElementById("lbltaskholdid").value = eReturn;
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        var popwin = "directories=0,height=500,width=800,location=0,menubar=0,resizable=0,status=0,toolbar=0,scrollbars=1";
        var poprep = "directories=0,height=600,width=900,location=0,menubar=0,resizable=0,status=0,toolbar=0,scrollbars=1";
        var poprepsm = "directories=0,height=390,width=300,location=0,menubar=0,resizable=0,status=0,toolbar=0,scrollbars=1";

        function getValueAnalysis() {

            tl = document.getElementById("lbltasklev").value;
            cid = document.getElementById("lblcid").value;
            sid = document.getElementById("lblsid").value;
            did = document.getElementById("lbldid").value;
            clid = document.getElementById("lblclid").value;
            eqid = document.getElementById("lbleqid").value;
            //alert(eqid)
            chk = document.getElementById("lblchk").value;
            fuid = document.getElementById("lblfuid").value;
            if (fuid != "") {
                ////window.parent.setref();
                var ht = "2000"; //screen.Height - 20;
                var wd = "1000"; //screen.Width - 20;
                var eReturn = window.showModalDialog("../reports/reportdialog.aspx?who=pmo&tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:1000px; dialogWidth:2000px;resizable=yes");
                //var eReturn = window.showModalDialog("../reports/reportdialog.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:610px; dialogWidth:610px");
                if (eReturn) {
                    if (eReturn != "ret") {
                        document.getElementById("lblsvchk").value = "7";
                        document.getElementById("pgflag").value = eReturn;
                        //alert(eReturn)
                        document.getElementById("form1").submit();
                    }
                }
            }
            else {
                alert("No Task Records Selected Yet!")
            }
        }
        function GetLubeDiv() {

            var ro = document.getElementById("lblro").value;
            var lock = document.getElementById("lbllock").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            if (lock != "1" && tpmhold != "1") {
                ////window.parent.setref();
                handleapp();
                cid = document.getElementById("lblcid").value;
                ptid = document.getElementById("lbltaskid").value;
                tnum = document.getElementById("lblpg").innerHTML;
                if (ptid != "") {
                    window.open("../inv/optTaskLubeList.aspx?ptid=" + ptid + "&cid=" + cid + "&tnum=" + tnum + "&ro=" + ro, "repWin", popwin);
                    document.getElementById("form1").submit();
                }
            }
        }
        function GetPartDiv() {

            var ro = document.getElementById("lblro").value;
            var lock = document.getElementById("lbllock").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            if (lock != "1" && tpmhold != "1") {
                ////window.parent.setref();
                handleapp();
                cid = document.getElementById("lblcid").value;
                ptid = document.getElementById("lbltaskid").value;
                tnum = document.getElementById("lblpg").innerHTML;
                //alert(tnum)
                if (ptid != "") {
                    window.open("../inv/optTaskPartList.aspx?ptid=" + ptid + "&cid=" + cid + "&tnum=" + tnum + "&ro=" + ro, "repWin", popwin);
                    document.getElementById("form1").submit();
                }
            }
        }
        function GetSpareDiv() {
            handleapp();
            var ro = document.getElementById("lblro").value;
            var lock = document.getElementById("lbllock").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            if (lock != "1" && tpmhold != "1") {
                ////window.parent.setref();

                eqid = document.getElementById("lbleqid").value;
                cid = document.getElementById("lblcid").value;
                ptid = document.getElementById("lbltaskid").value;
                tnum = document.getElementById("lblpg").innerHTML;
                if (ptid != "") {
                    window.open("../inv/sparepartpick.aspx?eqid=" + eqid + "&oflag=r&ptid=" + ptid + "&cid=" + cid + "&tnum=" + tnum + "&ro=" + ro, "repWin", popwin);
                    document.getElementById("form1").submit();
                }
            }
        }
        function GetToolDiv() {

            var ro = document.getElementById("lblro").value;
            var lock = document.getElementById("lbllock").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            if (lock != "1" && tpmhold != "1") {
                ////window.parent.setref();
                handleapp();
                cid = document.getElementById("lblcid").value;
                ptid = document.getElementById("lbltaskid").value;
                tnum = document.getElementById("lblpg").innerHTML;
                if (ptid != "") {
                    window.open("../inv/optTaskToolList.aspx?ptid=" + ptid + "&cid=" + cid + "&tnum=" + tnum + "&ro=" + ro, "repWin", popwin);
                    document.getElementById("form1").submit();
                }
            }
        }
        function GetNoteDiv() {

            var ro = document.getElementById("lblro").value;
            var lock = document.getElementById("lbllock").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            if (lock != "1" && tpmhold != "1") {
                ////window.parent.setref();
                ptid = document.getElementById("lbltaskid").value;
                if (ptid != "") {
                    window.open("../apps/TaskNotes.aspx?ptid=" + ptid + "&ro=" + ro, "repWin", popwin);
                }
            }
        }


        function GetRationale() {

            handleapp();
            tnum = document.getElementById("lblpg").innerHTML;
            tid = document.getElementById("lbltaskid").value;
            did = document.getElementById("lbldid").value;
            clid = document.getElementById("lblclid").value;
            eqid = document.getElementById("lbleqid").value;
            fuid = document.getElementById("lblfuid").value;
            coid = document.getElementById("lblco").value;
            sav = document.getElementById("lblsave").value;
            ro = document.getElementById("lblro").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            if (tid == "") {
                alert("No Task Records Seleted!")
            }
            else if (sav == "no") {
                alert("Task Record Is Not Saved!")
            }
            else {
                if (tpmhold != "1") {
                    ////window.parent.setref();
                    var eReturn = window.showModalDialog("../appsopt/PMRationaleDialog.aspx?tnum=" + tnum + "&tid=" + tid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:650px; dialogWidth:850px; resizable=yes;");
                    if (eReturn) {
                        document.getElementById("form1").submit();
                    }
                }
            }
        }

        function getsgrid() {

            handleapp();
            var tcnt = document.getElementById("lblcnt").value;
            //tid = document.getElementById("lblpg").innerHTML; //
            tid = document.getElementById("txttaskorder").value;
            fuid = document.getElementById("lblfuid").value;
            cid = document.getElementById("lblcid").value;
            sid = document.getElementById("lblsid").value;
            sav = document.getElementById("lblsave").value;
            eqid = document.getElementById("lbleqid").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            if (tid == "" || tid == "0") {
                alert("No Task Records Seleted!")
            }
            else if (sav == "no") {
                alert("Task Record Is Not Saved!")
            }
            else {
                if (tpmhold != "1") {
                    ////window.parent.setref();
                    var eReturn = window.showModalDialog("../apps/GSubDialog.aspx?sid=" + sid + "&cid=" + cid + "&tid=" + tid + "&fuid=" + fuid + "&eqid=" + eqid + "&tcnt=" + tcnt + "&date=" + Date(), "", "dialogHeight:680px; dialogWidth:850px; resizable=yes");
                    if (eReturn) {
                        document.getElementById("lblcompchk").value = "5"
                        document.getElementById("form1").submit();
                    }
                }
            }
        }

        function OpenFile(newstr, type) {
            handleapp();
            parent.parent.main.location.href = "doc.aspx?file=" + newstr + "&type=" + type;

        }
        function GetFuncDiv() {
            ////window.parent.setref();
            handleapp();
            cid = document.getElementById("lblcid").value;
            eqid = document.getElementById("lbleqid").value;
            var eReturn = window.showModalDialog("../equip/FuncDialog.aspx?cid=" + cid + "&eqid=" + eqid, "", "dialogHeight:250px; dialogWidth:600px; resizable=yes");
            if (eReturn) {
                document.getElementById("lblpchk").value = "func";
                document.getElementById("form1").submit();
            }
        }

        function GetCompDiv() {

            handleapp();
            var chken = document.getElementById("lblenable").value;
            fuid = document.getElementById("lblfuid").value;
            tid = document.getElementById("lblpg").innerHTML;
            var tpmhold = document.getElementById("lbltpmhold").value;
            //alert(tid)
            if (fuid.length != 0 && fuid != "" && fuid != "0" && tpmhold != "1") {
                ////window.parent.setref();
                //if(tid!=""&&tid!="0") {
                cid = document.getElementById("lblcid").value;
                eqid = document.getElementById("lbleqid").value;
                fuid = document.getElementById("lblfuid").value;
                ptid = document.getElementById("lbltaskid").value;
                tli = document.getElementById("lbltasklev").value;
                chk = document.getElementById("lblchk").value;
                ro = document.getElementById("lblro").value;
                var eReturn = window.showModalDialog("../equip/CompDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&tli=" + tli + "&chk=" + chk + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:800px; resizable=yes");
                if (eReturn == "log") {
                    //window.parent.handlelogout();
                }
                else if (eReturn != "no") {
                    document.getElementById("lblcompchk").value = eReturn;
                }
                document.getElementById("form1").submit();
                //}
            }
        }
        function getss() {
            //window.parent.setref();
            cid = document.getElementById("lblcid").value;
            sid = document.getElementById("lblsid").value;
            ro = document.getElementById("lblro").value;
            var eReturn = window.showModalDialog("../admin/pmSiteSkillsDialog.aspx?sid=" + sid + "&cid=" + cid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:520px; dialogWidth:470px; resizable=yes");
            if (eReturn) {
                document.getElementById("lblcompchk").value = "4";
                document.getElementById("form1").submit()
            }
        }
        function GetCompCopy() {
            //handleapp();
            var chken = document.getElementById("lblenable").value;
            ptid = document.getElementById("lbltaskid").value;
            ro = document.getElementById("lblro").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            var fuid = document.getElementById("lblfuid").value;
            if (fuid.length != 0 && fuid != "" && fuid != "0" && tpmhold != "1") {
                //window.parent.setref();
                fuid = document.getElementById("lblfuid").value;
                if (fuid.length != 0 || fuid != "" || fuid != "0") {
                    cid = document.getElementById("lblcid").value;
                    sid = document.getElementById("lblsid").value;
                    did = document.getElementById("lbldid").value;
                    clid = document.getElementById("lblclid").value;
                    eqid = document.getElementById("lbleqid").value;
                    ptid = document.getElementById("lbltaskid").value;
                    tli = document.getElementById("lbltasklev").value;
                    chk = document.getElementById("lblchk").value;
                    usr = document.getElementById("lblusername").value;
                    comid = document.getElementById("lblco").value;
                    var eReturn = window.showModalDialog("../equip/CompCopyMiniDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&tli=" + tli + "&chk=" + chk + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&usr=" + usr + "&ro=" + ro + "&date=" + Date() + "&coid=" + comid, "", "dialogHeight:700px; dialogWidth:880px; resizable=yes");
                    if (eReturn == "log") {
                        //window.parent.handlelogout();
                    }
                    else if (eReturn) {
                        document.getElementById("lblcompchk").value = "1";
                    }
                    document.getElementById("form1").submit()
                }
            }

        }

        function getPFDiv() {

            //handleapp();
            var chken = document.getElementById("lblenable").value;
            var ro = document.getElementById("lblro").value;
            ptid = document.getElementById("lbltaskid").value;
            if (chken != "1" || ro == "1") {
                if (ptid != "") {
                    //window.parent.setref();
                    var eq = document.getElementById("lbleqid").value;
                    var valu = document.getElementById("lbltaskid").value;
                    //document.getElementById("submit();
                    var eReturn = window.showModalDialog("../equip/PFIntDialog.aspx?tid=" + valu + "&eqid=" + eq + "&ro=" + ro, "", "dialogHeight:560px; dialogWidth:669px; resizable=yes");
                }
                if (eReturn) {
                    //alert(eReturn)
                    document.getElementById("txtpfint").value = eReturn;
                    document.getElementById("form1").submit()
                }
            }
        }
        function GetFailDiv() {

            //handleapp();
            var chken = document.getElementById("lblenable").value;
            ro = document.getElementById("lblro").value;
            ptid = document.getElementById("lbltaskid").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            if ((chken != "1" || ro == "1") && tpmhold != "1") {
                if (ptid != "") {
                    //window.parent.setref();
                    cid = document.getElementById("lblcid").value;
                    eqid = document.getElementById("lbleqid").value;
                    fuid = document.getElementById("lblfuid").value;
                    ptid = document.getElementById("lbltaskid").value;
                    coid = document.getElementById("lblco").value;
                    cvalu = document.getElementById("lblcompnum").value; //document.getElementById("ddcomp.options[document.getElementById("ddcomp.options.selectedIndex].text;
                    cind = document.getElementById("lblcoid").value;

                    if (cind != "0") {
                        var eReturn = window.showModalDialog("../equip/CompFailDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&cvalu=" + cvalu + "&ro=" + ro, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                    }
                    else {
                        alert("No Component Selected")
                    }
                }
            }
            if (eReturn) {
                document.getElementById("lblcompfailchk").value = "1";
                document.getElementById("form1").submit()
            }
        }
        function controlrow(id) {
            document.getElementById("lbltabid").value = id;
            vwflg = document.getElementById(id).className;
            closerows();
            if (id == "tbeq" && vwflg == "details") {
                document.getElementById(id).className = "view";
                document.getElementById('tbeq2').className = "view";
                document.getElementById('imgeq').src = "../images/appbuttons/optdown.gif";
            }
            else if (id == "tbdtls" && vwflg == "details") {
                document.getElementById(id).className = "view";
                document.getElementById("tbdtls2").className = "view";
                document.getElementById('imgdtls').src = "../images/appbuttons/optdown.gif";
            }
            else if (id == "tbreqs" && vwflg == "details") {
                document.getElementById(id).className = "view";
                document.getElementById('imgreqs').src = "../images/appbuttons/optdown.gif";
            }
        }

        function closerows() {
            document.getElementById('tbeq').className = "details";
            document.getElementById('tbeq2').className = "details";
            document.getElementById('imgeq').src = "../images/appbuttons/optup.gif";
            document.getElementById('tbdtls').className = "details";
            document.getElementById('tbdtls2').className = "details";
            document.getElementById('imgdtls').src = "../images/appbuttons/optup.gif";
            document.getElementById('tbreqs').className = "details";
            document.getElementById('imgreqs').src = "../images/appbuttons/optup.gif";
        }
        function CheckDel(val) {

            if (val == "Delete") {
                var decision = confirm("Are You Sure You Want to Mark this Task as Delete?")
                if (decision == true) {
                    //document.getElementById("submit('ddcomp', '');
                    FreezeScreen('Your Data Is Being Processed...');
                    document.getElementById("lbldel").value = "delr"
                    document.getElementById("form1").submit();
                }
                else {
                    alert("Action Cancelled")
                }

            }
        }
        function GoToPMLib() {
            handleapp();
            window.open("../equip/EQCopy.aspx?date=" + Date(), target = "_top");
        }
        function pmidtest() {
            alert(document.getElementById("lbldocpmid").value)
        }
        function getMeasDiv() {

            //handleapp();
            var chken = document.getElementById("lblenable").value;
            ptid = document.getElementById("lbltaskid").value;
            //alert(chken)
            var ro = document.getElementById("lblro").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            if ((chken != "1" || ro == "1") && tpmhold != "1") {
                if (ptid != "") {
                    //window.parent.setref();
                    var coid = document.getElementById("lblco").value;
                    var valu = document.getElementById("lbltaskid").value;
                    var eReturn = window.showModalDialog("../utils/tmdialog.aspx?typ=pm&taskid=" + valu + "&coid=" + coid + "&ro=" + ro, "", "dialogHeight:650px; dialogWidth:550px; resizable=yes");
                }
                if (eReturn) {
                    document.getElementById("form1").submit()
                }
            }
        }
        function handleapp() {
            //var app = document.getElementById("appchk").value;
            //if(app=="switch") //window.parent.rtop.handleapp(app);
        }
        function copydesc(val) {
            var chk = document.getElementById('ddtaskstat').value;
            //alert(chk)
            var ghostoff = document.getElementById("lblghostoff").value;
            var xstatus = document.getElementById("lblxstatus").value;
            if (ghostoff == "yes" && xstatus == "yes") {
                if (chk != "Delete") {
                    document.getElementById('txtdesc').innerHTML = val;
                }
            }
        }
        function filldown(id, val) {
            var ghostoff = document.getElementById("lblghostoff").value;
            var xstatus = document.getElementById("lblxstatus").value;
            //alert(ghostoff + ", " + xstatus)
            var xflg = 0;
            if (ghostoff == "yes" && xstatus == "yes") {
                xflg = 1;
            }
            if (id == 'o') {
                if (xflg == 0) {
                    document.getElementById('txttr').value = val;
                }
                var dt = document.getElementById("ddeqstato").options[document.getElementById("ddeqstato").options.selectedIndex].text;
                if (dt == "Down") {
                    document.getElementById("txtordt").value = val;
                    if (xflg == 0) {
                        document.getElementById("txtrdt").value = val;
                    }
                }
            }
            else {
                document.getElementById("lblcompchng").value = "1";
                var dt = document.getElementById("ddeqstat").options[document.getElementById("ddeqstat").options.selectedIndex].text;
                if (dt == "Down") {
                    document.getElementById("txtrdt").value = val;
                }
            }
        }
        function zerodt(id, val) {
            var ghostoff = document.getElementById("lblghostoff").value;
            var xstatus = document.getElementById("lblxstatus").value;
            var xflg = 0;
            if (ghostoff == "yes" && xstatus == "yes") {
                xflg = 1;
            }
            if (id == 'o') {
                var dt = document.getElementById("ddeqstato").options[document.getElementById("ddeqstato").options.selectedIndex].text;
                if (dt != "Down") {
                    document.getElementById("txtordt").value = 0;
                    if (xflg == 0) {
                        document.getElementById("txtrdt").value = "0";
                    }
                }
                else {
                    document.getElementById("txtordt").value = document.getElementById("txttro").value;
                    if (xflg == 0) {
                        document.getElementById("txtrdt").value = document.getElementById("txttr").value;
                    }
                }
            }
            else {
                var dt = document.getElementById("ddeqstat").options[document.getElementById("ddeqstat").options.selectedIndex].text;
                if (dt != "Down") {
                    document.getElementById("txtrdt").value = "0";
                }
                else {
                    document.getElementById("txtrdt").value = document.getElementById("txttr").value;
                }
            }
        }
        function GetType(app, sval) {
            //alert(app + ", " + sval)
            var ghostoff = document.getElementById("lblghostoff").value;
            var xstatus = document.getElementById("lblxstatus").value;
            //alert(ghostoff + ", " + xstatus)
            var xflg = 0;
            if (ghostoff == "yes" && xstatus == "yes") {
                xflg = 1;
            }
            var typlst
            var sstr
            var ssti
            if (app == "d") {
                typlst = document.getElementById("ddtype");
                ptlst = document.getElementById("ddpt");
                sstr = typlst.options[typlst.selectedIndex].text;
                ssti = document.getElementById("ddtype").value;
                //sstr == "4 - Cond Monitoring"
                if (ssti == "7") {
                    document.getElementById("ddpt").disabled = false;
                }
                else {
                    ptlst.value = "0";
                    document.getElementById("ddpt").disabled = true;
                }
            }
            else if (app == "o") {
                if (xflg == 0) {
                    document.getElementById("ddtype").value = sval;
                }
                typlst = document.getElementById("ddtypeo");
                ptlst = document.getElementById("ddpt");
                ptolst = document.getElementById("ddpto");
                sstr = typlst.options[typlst.selectedIndex].text;
                ssti = document.getElementById("ddtypeo").value;
                if (ssti == "7") {
                    if (xflg == 0) {
                        document.getElementById("ddpt").disabled = false;
                    }
                    document.getElementById("ddpto").disabled = false;
                }
                else {
                    if (xflg == 0) {
                        ptlst.value = "0";
                        document.getElementById("ddpt").disabled = true;
                    }
                    ptolst.value = "0";
                    document.getElementById("ddpto").disabled = true;
                }
            }
        }

        function getti() {
            //"taskimagepmdialog.aspx?funcid=" + funcid + "&eqid=" + eqid + "&tasknum=" + tasknum
            ptid = document.getElementById("lbltaskid").value;
            if (ptid != "") {
                var eqid = document.getElementById("lbleqid").value;
                var funcid = document.getElementById("lblfuid").value;
                var tasknum = document.getElementById("lblpg").innerHTML;
                var eReturn = window.showModalDialog("../pmpics/taskimagepmdialog.aspx?funcid=" + funcid + "&eqid=" + eqid + "&tasknum=" + tasknum, "", "dialogHeight:350px; dialogWidth:350px; resizable=yes");
            }
        }
        function checkpush() {
            var eReturn = window.showModalDialog("../complib/compushdialog.aspx", "", "dialogHeight:100px; dialogWidth:400px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    document.getElementById("lblnewkey").value = eReturn;
                    document.getElementById("lblcompchk").value = "pushcomp";
                    document.getElementById("form1").submit();
                }
            }
        }
        function checkcqty(val) {
            //alert(val)
            document.getElementById("lblcompchng").value = "1";
            /*
            if (val > 1) {
            var decision = confirm("Update Total Task Time based on new Component Qty value?")
            if (decision == true) {
            //document.getElementById("submit('ddcomp', '');
            //FreezeScreen('Your Data Is Being Processed...');
            multcomp();
            alert("Save Required to record new time values")
            }
            else {
            alert("Action Cancelled")
            }

            }
            */
        }
        function multcomp() {
            if (document.getElementById("lblenable").value != "1") {
                var cqty = document.getElementById("txtcQty").value;
                var cmins = document.getElementById("txttr").value;
                var dmins = document.getElementById("txtrdt").value;
                if (cqty > 1 && cmins > 0) {
                    var newmins = cqty * cmins;
                    document.getElementById("txttr").value = newmins;
                }
                if (cqty > 1 && dmins > 0) {
                    var newdmins = cqty * dmins;
                    document.getElementById("txtrdt").value = newdmins;
                }
            }
        }
		-->
    </script>
</head>
<body class="tbg" onload="page_maint('o');checkit();">
    <div style="z-index: 1000; position: absolute; visibility: hidden" id="overDiv">
    </div>
    <form id="form1" method="post" runat="server">
    <div id="FreezePane" class="FreezePaneOff" align="center">
        <div id="InnerFreezePane" class="InnerFreezePane">
        </div>
    </div>
    <div style="z-index: 100; position: absolute; top: 0px; left: 0px">
        <table cellspacing="0" cellpadding="3" width="742">
            <tr>
                <td class="thdrsinglft" width="26" align="left">
                    <img border="0" src="../images/appbuttons/minibuttons/optbg.gif">
                </td>
                <td class="thdrsingrt label" onmouseover="return overlib('Against what is the PM intended to protect?', ABOVE, LEFT)"
                    onmouseout="return nd()" width="696">
                    Failure Modes, Causes And/Or Effects
                </td>
            </tr>
        </table>
        <table id="tbeq" class="view" cellspacing="0" cellpadding="1" width="742">
            <tr>
                <td class="label" height="20" width="140">
                    Component Addressed:
                </td>
                <td id="tdcompnum" class="bluelabellt" width="242" runat="server">
                    Component #
                </td>
                <td class="label" width="48">
                <img id="btnaddcomp" onmouseover="return overlib('Add/Edit Components for Selected Function', LEFT)"
                        onclick="GetCompDiv();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/addmod.gif"
                        runat="server"><img id="imgcopycomp" onmouseover="return overlib('Copy Components for Selected Function', LEFT)"
                            onclick="GetCompCopy();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                            runat="server">
                </td>
                <td class="label" width="30">
                    Qty:
                </td>
                <td class="label" width="282">
                    <asp:TextBox ID="txtcQty" runat="server" CssClass="plainlabel" Width="40px"></asp:TextBox>
                    <img id="img1" onmouseover="return overlib('Update Total Task Time based on new Component Qty value?', ABOVE, LEFT)"
                            onmouseout="return nd()" onclick="multcomp();" alt="" src="../images/appbuttons/minibuttons/pdown.gif"
                            width="18" height="18" runat="server" class="details">
                </td>
            </tr>
        </table>
        <table id="tbeq2" cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td width="365">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="20">
                            </td>
                            <td width="150">
                            </td>
                            <td style="width: 23px" width="23">
                            </td>
                            <td width="5">
                            </td>
                            <td width="150">
                            </td>
                            <td width="20">
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowgray">
                                &nbsp;
                            </td>
                            <td class="label transrowgray" align="center">
                                Original Failure Modes
                            </td>
                            <td style="width: 23px" class="transrowgray">
                            </td>
                            <td class="tbg">
                                &nbsp;
                            </td>
                            <td class="bluelabel transrowdrk" align="center">
                                Revised Failure Modes
                            </td>
                            <td class="transrowdrk">
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowgray">
                                &nbsp;
                            </td>
                            <td class="transrowgray" align="center">
                                <asp:ListBox ID="lbofailmodes" runat="server" Width="150px" CssClass="plainlabel"
                                    ForeColor="Blue" Height="50px" SelectionMode="Multiple"></asp:ListBox>
                            </td>
                            <td style="width: 23px" class="transrowgray" valign="middle" align="center">
                                <img id="todis1" class="details" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                                    width="20" height="20" runat="server">
                                <img id="fromdis1" class="details" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                    width="20" height="20" runat="server">
                                <asp:ImageButton ID="ibfromo" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardbg.gif">
                                </asp:ImageButton><br>
                                <asp:ImageButton ID="ibtoo" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbgdrk.gif">
                                </asp:ImageButton>
                            </td>
                            <td class="tbg">
                                &nbsp;
                            </td>
                            <td class="transrowdrk" align="center">
                                <asp:ListBox ID="lbCompFM" runat="server" Width="150px" CssClass="plainlabel" Height="50px"
                                    SelectionMode="Multiple"></asp:ListBox>
                            </td>
                            <td class="transrowdrk" valign="middle" align="center">
                                <img id="btnaddnewfail" onmouseover="return overlib('Add a New Failure Mode to the Component Selected Above')"
                                    onmouseout="return nd()" onclick="GetFailDiv();" alt="" src="../images/appbuttons/minibuttons/addmod.gif"
                                    runat="server"><br>
                                <asp:ImageButton ID="ibReuse" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardblu.gif">
                                </asp:ImageButton><img id="fromreusedis" class="details" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                    width="20" height="20" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="377">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="150">
                            </td>
                            <td width="20">
                            </td>
                            <td width="150">
                            </td>
                            <td width="47">
                            </td>
                            <td width="10">
                            </td>
                        </tr>
                        <tr>
                            <td class="redlabel transrowdrk" bgcolor="#bad8fc" align="center">
                                Not Addressed
                            </td>
                            <td class="transrowdrk">
                                &nbsp;
                            </td>
                            <td class="bluelabel transrowdrk" bgcolor="#bad8fc" align="center">
                                Selected
                            </td>
                            <td class="transrowdrk">
                                &nbsp;
                            </td>
                            <td class="tbg">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowdrk" align="center">
                                <asp:ListBox ID="lbfaillist" runat="server" Width="150px" CssClass="plainlabel" ForeColor="Red"
                                    Height="50px" SelectionMode="Multiple"></asp:ListBox>
                            </td>
                            <td class="transrowdrk" valign="middle" align="center">
                                <img id="todis" class="details" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                                    width="20" height="20" runat="server">
                                <img id="fromdis" class="details" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                    width="20" height="20" runat="server">
                                <asp:ImageButton ID="ibToTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardblu.gif">
                                </asp:ImageButton><br>
                                <asp:ImageButton ID="ibFromTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/backblu.gif">
                                </asp:ImageButton>
                            </td>
                            <td class="transrowdrk" align="center">
                                <asp:ListBox ID="lbfailmodes" runat="server" Width="150px" CssClass="plainlabel"
                                    ForeColor="Blue" Height="50px" SelectionMode="Multiple"></asp:ListBox>
                            </td>
                            <td class="transrowdrk">
                                &nbsp;
                            </td>
                            <td class="tbg">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td colspan="2">
                    <img src="../images/appbuttons/minibuttons/2PX.gif" height="2">
                </td>
            </tr>
            <tr>
                <td class="thdrsinglft" width="26" align="left">
                    <img border="0" src="../images/appbuttons/minibuttons/optbg.gif">
                </td>
                <td class="thdrsingrt label" onmouseover="return overlib('What are you going to do?')"
                    onmouseout="return nd()" width="684" colspan="3">
                    Task Activity Details
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="2" width="372">
                        <tr>
                            <td width="30">
                            </td>
                            <td width="140">
                            </td>
                            <td width="30">
                            </td>
                            <td width="140">
                            </td>
                            <td width="2">
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowgray" colspan="4">
                                Original Task Description
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowgray" colspan="4">
                                <textarea style="width: 296px; font-family: Arial; height: 40px; font-size: 12px"
                                    id="txtodesc" onkeyup="copydesc(this.value);" onchange="copydesc(this.value);"
                                    cols="34" name="txtdesc" runat="server"></textarea>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowgray">
                                Type
                            </td>
                            <td class="transrowgray">
                                <asp:DropDownList ID="ddtypeo" runat="server" CssClass="plainlabel" Width="140px"
                                    Rows="1" DataTextField="tasktype" DataValueField="ttid">
                                </asp:DropDownList>
                            </td>
                            <td class="label transrowgray">
                                PdM
                            </td>
                            <td class="transrowgray">
                                <asp:DropDownList ID="ddpto" runat="server" Width="140px" CssClass="plainlabel">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <img src="../images/appbuttons/minibuttons/2PX.gif">
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table cellspacing="0" cellpadding="2" width="370">
                        <tr>
                            <td width="30">
                            </td>
                            <td width="140">
                            </td>
                            <td width="30">
                            </td>
                            <td width="90">
                            </td>
                            <td width="50">
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowdrk" colspan="5">
                                Revised Task Description
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowdrk" colspan="4">
                                <textarea style="width: 290px; font-family: Arial; height: 40px; font-size: 12px"
                                    id="txtdesc" class="plainlabel" cols="32" name="txtdesc" runat="server"></textarea>
                            </td>
                            <td class="transrowdrk" bgcolor="#bad8fc">
                                <img id="sgrid1" onmouseover="return overlib('Add/Edit Sub Tasks')"
                                                onmouseout="return nd()" onclick="getsgrid();" alt="" src="../images/appbuttons/minibuttons/sgrid1.gif"
                                                width="20" height="20">
                                <img id="btnlookup2" onclick="tasklookup();" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    runat="server">
                                <br>
                                <img onmouseover="return overlib('Generate Work Order for This Task')" onmouseout="return nd()"
                                    onclick="getfail2();" alt="" src="../images/appbuttons/minibuttons/addmod.gif">
                                <img onmouseover="return overlib('Add/Edit Measurements for this Task')" onmouseout="return nd()"
                                    onclick="getMeasDiv();" alt="" src="../images/appbuttons/minibuttons/measblu.gif"
                                    width="27" height="20">
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowdrk">
                                Type
                            </td>
                            <td class="transrowdrk">
                                <asp:DropDownList ID="ddtype" runat="server" CssClass="plainlabel" Width="140px"
                                    Rows="1" DataTextField="tasktype" DataValueField="ttid">
                                </asp:DropDownList>
                            </td>
                            <td class="label transrowdrk">
                                PdM
                            </td>
                            <td class="transrowdrk" colspan="2">
                                <asp:DropDownList ID="ddpt" runat="server" Width="140px" CssClass="plainlabel">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="2" width="742">
            <tr>
                <td>
                    <img src="../images/appbuttons/minibuttons/2PX.gif" width="2" height="2">
                </td>
            </tr>
            <tr>
                <td class="thdrsinglft" width="26" align="left">
                    <img border="0" src="../images/appbuttons/minibuttons/optbg.gif">
                </td>
                <td class="thdrsingrt label" onmouseover="return overlib('How are you going to do it?')"
                    onmouseout="return nd()" width="554" colspan="3">
                    Planning &amp; Scheduling Details
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td valign="top">
                    <table cellspacing="0" cellpadding="1" width="372" border="0">
                        <tr>
                            <td width="60">
                            </td>
                            <td width="10">
                            </td>
                            <td width="50">
                            </td>
                            <td width="20">
                            </td>
                            <td width="20">
                            </td>
                            <td width="30">
                            </td>
                            <td width="70">
                            </td>
                            <td width="40">
                            </td>
                            <td width="25">
                            </td>
                            <td width="45">
                            </td>
                            <td width="2">
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowgray" bgcolor="#edf0f3" colspan="5">
                                <asp:Label ID="lang1029" runat="server">Original Skill</asp:Label>
                            </td>
                            <td class="label transrowgray" bgcolor="#edf0f3">
                                Qty
                            </td>
                            <td class="label transrowgray" bgcolor="#edf0f3" colspan="4">
                                <asp:Label ID="lang1030" runat="server">Min Ea</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowgray" colspan="5">
                                <asp:DropDownList ID="ddskillo" runat="server" CssClass="plainlabel" Width="144px">
                                </asp:DropDownList>
                            </td>
                            <td class="transrowgray">
                                <asp:TextBox ID="txtqtyo" runat="server" CssClass="plainlabel" Width="24PX"></asp:TextBox>
                            </td>
                            <td class="transrowgray" colspan="4">
                                <asp:TextBox ID="txttro" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowgray">
                                <asp:Label ID="lang1031" runat="server">Frequency</asp:Label>
                            </td>
                            <td class="transrowgray" colspan="2">
                                <asp:TextBox ID="txtfreqo" runat="server" CssClass="plainlabel" Width="55px"></asp:TextBox>
                            </td>
                            <td class="transrowgray">
                                <input id="cbfixedo" runat="server" type="checkbox" onmouseover="return overlib('Check if Frequency is Fixed (Calendar Frequency Only)')"
                                    onmouseout="return nd()" />
                            </td>
                            <td class="transrowgray">
                                <img onclick="getmetero();" onmouseover="return overlib('View, Add, or Edit Original Meter Frequency for this Task', ABOVE, LEFT)"
                                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/meter1.gif">
                            </td>
                            <td class="label transrowgray">
                                <asp:Label ID="lang1032" runat="server">Status</asp:Label>
                            </td>
                            <td class="transrowgray" colspan="2">
                                <asp:DropDownList ID="ddeqstato" runat="server" CssClass="plainlabel" Width="80px">
                                </asp:DropDownList>
                            </td>
                            <td class="label transrowgray">
                                DT
                            </td>
                            <td class="transrowgray">
                                <asp:TextBox ID="txtordt" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabel" colspan="2">
                                <asp:Label ID="lang1033" runat="server">Task Status</asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="ddtaskstat" runat="server" CssClass="plainlabel" AutoPostBack="False">
                                    <asp:ListItem Value="Select">Select</asp:ListItem>
                                    <asp:ListItem Value="UnChanged">UnChanged</asp:ListItem>
                                    <asp:ListItem Value="Add">Add</asp:ListItem>
                                    <asp:ListItem Value="Revised">Revised</asp:ListItem>
                                    <asp:ListItem Value="Delete">Delete</asp:ListItem>
                                    <asp:ListItem Value="Detailed Steps">Detailed Steps</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td id="tdtpmhold" class="plainlabelred" colspan="6" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table border="0" cellspacing="0" cellpadding="1" width="370">
                        <tr>
                            <td width="60">
                            </td>
                            <td width="60">
                            </td>
                            <td width="20">
                            </td>
                            <td width="20">
                            </td>
                            <td width="30">
                            </td>
                            <td width="70">
                            </td>
                            <td width="40">
                            </td>
                            <td width="25">
                            </td>
                            <td width="45">
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowdrk" colspan="4">
                                <asp:Label ID="lang1034" runat="server">Revised Skill</asp:Label>
                            </td>
                            <td class="label transrowdrk">
                                Qty
                            </td>
                            <td class="label transrowdrk">
                                <asp:Label ID="lang1035" runat="server">Min Ea</asp:Label>
                            </td>
                            <td class="label transrowdrk" colspan="3">
                                <asp:Label ID="lang1036" runat="server">P-F Interval</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowdrk" colspan="4">
                                <asp:DropDownList ID="ddskill" runat="server" CssClass="plainlabel" Width="140px">
                                </asp:DropDownList>
                            </td>
                            <td class="transrowdrk">
                                <asp:TextBox ID="txtqty" runat="server" CssClass="plainlabel" Width="30px"></asp:TextBox>
                            </td>
                            <td class="transrowdrk">
                                <asp:TextBox ID="txttr" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                            </td>
                            <td class="transrowdrk">
                                <asp:TextBox ID="txtpfint" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                            </td>
                            <td class="transrowdrk" colspan="2">
                                <img id="btnpfint" onmouseover="return overlib('Estimate Frequency using the PF Interval')"
                                    onmouseout="return nd()" onclick="getPFDiv();" alt="" src="../images/appbuttons/minibuttons/lilcalcblu.gif"
                                    width="20" height="20">
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowdrk">
                                <asp:Label ID="lang1037" runat="server">Frequency</asp:Label>
                            </td>
                            <td class="transrowdrk">
                                <asp:TextBox ID="txtfreq" runat="server" CssClass="plainlabel" Width="55px"></asp:TextBox>
                            </td>
                            <td class="transrowdrk">
                                <input id="cbfixed" runat="server" type="checkbox" onmouseover="return overlib('Check if Frequency is Fixed (Calendar Frequency Only)')"
                                    onmouseout="return nd()" />
                            </td>
                            <td class="transrowdrk">
                                <img onclick="getmeter();" onmouseover="return overlib('View, Add, or Edit Meter Frequency for this Task', ABOVE, LEFT)"
                                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/meter1.gif">
                            </td>
                            <td class="label transrowdrk">
                                <asp:Label ID="lang1038" runat="server">Status</asp:Label>
                            </td>
                            <td class="transrowdrk" colspan="2">
                                <asp:DropDownList ID="ddeqstat" runat="server" CssClass="plainlabel" Width="80px">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 19px" class="label transrowdrk">
                                DT
                            </td>
                            <td class="transrowdrk">
                                <asp:TextBox ID="txtrdt" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelsm" colspan="5">
                                <asp:CheckBox ID="cbloto" runat="server"></asp:CheckBox>LOTO&nbsp;<asp:CheckBox ID="cbcs"
                                    runat="server"></asp:CheckBox>CS
                            </td>
                            <td class="bluelabel" colspan="6" align="right">
                                <img id="Img3" onmouseover="return overlib('Add/Edit Parts for this Task')" onmouseout="return nd()"
                                    onclick="GetPartDiv();" alt="" src="../images/appbuttons/minibuttons/parttrans.gif"
                                    width="23" height="19" runat="server">
                                <img onmouseover="return overlib('Add/Edit Tools for this Task')" onmouseout="return nd()"
                                    onclick="GetToolDiv();" alt="" src="../images/appbuttons/minibuttons/tooltrans.gif"
                                    width="23" height="19">
                                <img onmouseover="return overlib('Add/Edit Lubricants for this Task')" onmouseout="return nd()"
                                    onclick="GetLubeDiv();" alt="" src="../images/appbuttons/minibuttons/lubetrans.gif"
                                    width="23" height="19">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="742">
            <tr id="trh4" class="view" runat="server">
                <td style="border-top: #7ba4e0 thin solid" width="20" align="center">
                    <img id="btnStart" src="../images/appbuttons/minibuttons/tostartbg.gif" width="20"
                        height="20" runat="server">
                </td>
                <td style="border-top: #7ba4e0 thin solid" width="20" align="center">
                    <img id="btnPrev" src="../images/appbuttons/minibuttons/prevarrowbg.gif" width="20"
                        height="20" runat="server">
                </td>
                <td style="border-top: #7ba4e0 thin solid" class="bluelabel" width="110" align="center">
                    Task#&nbsp;<asp:Label ID="lblpg" runat="server" CssClass="bluelabel" ForeColor="Blue"
                        Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"></asp:Label>&nbsp;of&nbsp;<asp:Label
                            ID="lblcnt" runat="server" CssClass="bluelabel" ForeColor="Blue" Font-Bold="True"
                            Font-Names="Arial" Font-Size="X-Small"></asp:Label>
                </td>
                <td style="border-top: #7ba4e0 thin solid" width="20" align="center">
                    <img id="btnNext" src="../images/appbuttons/minibuttons/nextarrowbg.gif" width="20"
                        height="20" runat="server">
                </td>
                <td style="border-top: #7ba4e0 thin solid" width="30" align="center">
                    <img id="btnEnd" src="../images/appbuttons/minibuttons/tolastbg.gif" width="20" height="20"
                        runat="server">
                </td>
                <td style="border-top: #7ba4e0 thin solid" class="label" width="70" align="center">
                    Task Order
                </td>
                <td style="border-top: #7ba4e0 thin solid" class="label" width="40">
                    <asp:TextBox ID="txttaskorder" runat="server" CssClass="plainlabel" Width="35px"
                        Enabled="False"></asp:TextBox>
                </td>
                <td style="border-top: #7ba4e0 thin solid" class="greenlabel" width="150" align="left">
                    <asp:Label ID="Label26" runat="server" CssClass="greenlabel" Font-Bold="True" Font-Names="Arial"
                        Font-Size="X-Small"># of Sub Tasks: </asp:Label><asp:Label ID="lblsubcount" runat="server"
                            CssClass="greenlabel" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"></asp:Label>
                </td>
                <td style="border-top: #7ba4e0 thin solid" width="260" align="right">
                    <img id="btnedittask" onclick="doEnable();" border="0" alt="" src="../images/appbuttons/minibuttons/lilpentrans.gif"
                        width="19" height="19" runat="server"><asp:ImageButton ID="btnaddtsk" runat="server"
                            ImageUrl="../images/appbuttons/minibuttons/addnewbg1.gif" BorderStyle="None"
                            Height="20px"></asp:ImageButton>
                    <asp:ImageButton ID="btntpm" runat="server" ImageUrl="../images/appbuttons/minibuttons/compresstpm.gif">
                    </asp:ImageButton>
                    <img id="imgdeltask" onmouseover="return overlib('Delete This Task', ABOVE, LEFT)"
                            onmouseout="return nd()" onclick="checkdeltask();" border="0" alt="" src="../images/appbuttons/minibuttons/del.gif"
                            runat="server"><asp:ImageButton ID="ibCancel" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif">
                    </asp:ImageButton><img id="btnsav" onclick="valpgnums();" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif"
                        width="20" height="20" runat="server"><img id="imgrat" onmouseover="return overlib('Add/Edit Rationale for Task Changes')"
                            onclick="GetRationale();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/rationale.gif"
                            width="20" runat="server"><img id="sgrid" onmouseover="return overlib('Add/Edit Sub Tasks')"
                                onmouseout="return nd()" onclick="getsgrid();" alt="" src="../images/appbuttons/minibuttons/sgrid1.gif"
                                width="20" height="20" runat="server"><img id="imgdeltpm" class="details" onmouseover="return overlib('Delete This Task From the TPM Module and Restore Editing', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="checkdeltasktpm();" border="0" alt="" src="../images/appbuttons/minibuttons/cantpm.gif"
                                    runat="server">
                </td>
            </tr>
            <tr>
                <td colspan="10" align="center">
                    <asp:Label ID="msglbl" runat="server" CssClass="redlabel" Width="360px" ForeColor="Red"
                        Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblsvchk" type="hidden" runat="server" name="lblsvchk">
    <input id="lblcompchk" type="hidden" name="lblcompchk" runat="server">
    <input id="lblsb" type="hidden" name="lblsb" runat="server">
    <input id="lblfilt" type="hidden" name="lblfilt" runat="server">
    <input id="lblenable" type="hidden" name="lblenable" runat="server">
    <input id="lblpgholder" type="hidden" name="lblpgholder" runat="server">
    <input id="lblcompfailchk" type="hidden" name="lblcompfailchk" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblstart" type="hidden" name="lblpmtype" runat="server">
    <input id="lblt" type="hidden" name="lblt" runat="server">
    <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
    <input id="lblst" type="hidden" name="lblst" runat="server">
    <input id="lblco" type="hidden" name="lblco" runat="server">
    <input id="lblpar" type="hidden" name="lblpar" runat="server">
    <input id="lbltabid" type="hidden" name="lbltabid" runat="server">
    <input id="lblpmstr" type="hidden" name="lblpmstr" runat="server">
    <input id="pgflag" type="hidden" name="pgflag" runat="server">
    <input id="lblpmid" type="hidden" name="lblpmid" runat="server">
    <input id="lbldid" type="hidden" runat="server" name="lbldid">
    <input id="lblclid" type="hidden" runat="server" name="lblclid"><input id="lblsid"
        type="hidden" runat="server" name="lblsid">
    <input id="lbldel" type="hidden" runat="server" name="lbldel"><input id="lblsave"
        type="hidden" runat="server" name="lblsave">
    <input id="lblgrid" type="hidden" runat="server" name="lblgrid"><input id="lblcind"
        type="hidden" runat="server" name="lblcind">
    <input id="lblcurrsb" type="hidden" runat="server" name="lblcurrsb"><input id="lblcurrcs"
        type="hidden" runat="server" name="lblcurrcs">
    <input id="lbldocpmid" type="hidden" runat="server" name="lbldocpmid"><input id="lbldocpmstr"
        type="hidden" runat="server" name="lbldocpmstr">
    <input id="lblsesscnt" type="hidden" runat="server" name="lblsesscnt"><input id="appchk"
        type="hidden" name="appchk" runat="server">
    <input id="lbllog" type="hidden" name="lbllog" runat="server"><input id="lblfiltcnt"
        type="hidden" runat="server" name="lblfiltcnt">
    <input id="lblusername" type="hidden" runat="server" name="lblusername">
    <input id="lbllock" type="hidden" name="lbllock" runat="server">
    <input id="lbllockedby" type="hidden" name="lbllockedby" runat="server">
    <input id="lblhaspm" type="hidden" runat="server" name="lblhaspm">
    <input id="lbltyp" type="hidden" runat="server" name="lbltyp">
    <input id="lbllid" type="hidden" runat="server" name="lbllid">
    <input id="lblnoeq" type="hidden" runat="server" name="lblnoeq">
    <input id="lblro" type="hidden" runat="server" name="lblro">
    <input id="lbltpmalert" type="hidden" name="lbltpmalert" runat="server"><input id="lbltpmhold"
        type="hidden" runat="server" name="lbltpmhold">
    <input id="lblusetot" type="hidden" runat="server" name="lblusetot">
    <input id="lblnewkey" type="hidden" runat="server" name="lblnewkey">
    <input type="hidden" id="lblnewcomp" runat="server" name="lblnewcomp">
    <input type="hidden" id="lblnewdesc" runat="server" name="lblnewdesc">
    <input type="hidden" id="lblcompnum" runat="server" name="lblcompnum">
    <input type="hidden" id="lblskillid" runat="server" />
    <input type="hidden" id="lblskill" runat="server" />
    <input type="hidden" id="lblskillqty" runat="server" />
    <input type="hidden" id="lblmeterid" runat="server" />
    <input type="hidden" id="lblrd" runat="server" />
    <input type="hidden" id="lblrdid" runat="server" />
    <input type="hidden" id="lblfunc" runat="server" />
    <input type="hidden" id="lblusemeter" runat="server" />
    <input type="hidden" id="lblusemetero" runat="server" />
    <input type="hidden" id="lblskillido" runat="server" />
    <input type="hidden" id="lblskillo" runat="server" />
    <input type="hidden" id="lblskillqtyo" runat="server" />
    <input type="hidden" id="lblmeterido" runat="server" />
    <input type="hidden" id="lblrdo" runat="server" />
    <input type="hidden" id="lblrdido" runat="server" />
    <input type="hidden" id="lblmfid" runat="server" />
    <input type="hidden" id="lblmfido" runat="server" />
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblfreq" runat="server" />
    <input type="hidden" id="lblfreqo" runat="server" />
    <input type="hidden" id="lblfixed" runat="server" />
    <input type="hidden" id="lblfixedo" runat="server" />
    <input type="hidden" id="lblghostoff" runat="server" />
    <input type="hidden" id="lblxstatus" runat="server" />
    <input type="hidden" id="lblintyp" runat="server" />
    <input type="hidden" id="lbltpmpmtskid" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblcompchng" runat="server" />
    <input type="hidden" id="lblcomi" runat="server" />
    <input id="lblfslang" type="hidden" runat="server" />
    <input type="hidden" id="lblrteid" runat="server" />
    <input type="hidden" id="lblrbskillid" runat="server" />
    <input type="hidden" id="lblrbqty" runat="server" />
    <input type="hidden" id="lblrbfreq" runat="server" />
    <input type="hidden" id="lblrbrdid" runat="server" />
    </form>
</body>
</html>
