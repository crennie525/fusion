

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class sfail
    Inherits System.Web.UI.Page
	Protected WithEvents lang1887 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1886 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1885 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1884 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1883 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1882 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1881 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim fail As New Utilities
    Protected WithEvents lblscid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim scid, ro, cid, sid, sclass As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtnewfail As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnaddfail As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfailmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents cbopts As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnaddtosite As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ibtnret As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblopt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()



	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            'Try
            scid = Request.QueryString("scid").ToString
            sclass = Request.QueryString("sclass").ToString
            lblscid.Value = scid
            If ro = "1" Then
                btnaddfail.Enabled = False
                btnaddfail.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                cbopts.Enabled = False
                btnaddtosite.Attributes.Add("class", "details")
                btntocomp.Visible = False
                btnfromcomp.Visible = False
                todis.Attributes.Add("class", "view")
                fromdis.Attributes.Add("class", "view")
                'imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
            Else
                'btnaddfail.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
                'btnaddfail.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
            End If

            lblopt.Value = "0"
            lbfailmodes.AutoPostBack = False
            lbfailmaster.AutoPostBack = False
            cid = "0"
            lblsid.Value = HttpContext.Current.Session("dfltps").ToString
            tdcomp.InnerHtml = sclass
            fail.Open()
            PopFail(cid, scid)
            PopFailList(cid, scid)
            fail.Dispose()

            'Catch ex As Exception
            'Dim strMessage As String =  tmod.getmsg("cdstr754" , "sfail.aspx.vb")
 
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'lbllog.Value = "noeqid"
            'End Try

        End If

        'ibtnret.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ibtnret.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
        'btntocomp.Attributes.Add("onclick", "DisableButton(this);")
        'btnfromcomp.Attributes.Add("onclick", "DisableButton(this);")

    End Sub
    Private Sub PopFailList(ByVal cid As String, ByVal scid As String)

        sql = "select c.failid, f.failuremode from compclasssubfm c left join failuremodes f on f.failid = c.failid " _
        + "where c.scid = '" & scid & "' order by f.failuremode"
        dr = fail.GetRdrData(sql)
        lbfailmodes.DataSource = dr
        lbfailmodes.DataValueField = "failid"
        lbfailmodes.DataTextField = "failuremode"
        lbfailmodes.DataBind()
        dr.Close()
        lbfailmodes.SelectedIndex = 0
    End Sub

    Private Sub PopFail(ByVal cid As String, ByVal scid As String)
        Dim chk As String = lblfailchk.Value
        Dim scnt As Integer
        sid = lblsid.Value
        If chk = "" Then
            sql = "select count(*) from pmSiteFM where siteid = '" & sid & "'"
            scnt = fail.Scalar(sql)
            If scnt = 0 Then
                lblfailchk.Value = "open"
                chk = "open"
            Else
                lblfailchk.Value = "site"
                chk = "site"
            End If
        End If
        Dim dt, val, filt As String
        If chk = "open" Then
            dt = "FailureModes"
        ElseIf chk = "site" Then
            dt = "pmSiteFM"
        Else
            Exit Sub
        End If

        val = "failid, failuremode"
        If chk = "site" Then
            filt = " where siteid = '" & sid & "' and failid not in (" _
         + "select failid from compclasssubfm where scid = '" & scid & "') order by failuremode asc"
        Else
            filt = " where failid not in (" _
         + "select failid from compclasssubfm where scid = '" & scid & "') order by failuremode asc"
        End If
        
        dr = fail.GetList(dt, val, filt) 'removed compid = '" & cid & "' and 
        lbfailmaster.DataSource = dr
        lbfailmaster.DataTextField = "failuremode"
        lbfailmaster.DataValueField = "failid"
        lbfailmaster.DataBind()
        dr.Close()
    End Sub

    Private Sub GetItems(ByVal failid As String, ByVal failstr As String)
        cid = "0"
        scid = lblscid.Value
        Dim fcnt As Integer
        sql = "select count(*) from compclasssubfm where scid = '" & scid & "' and " _
        + "failid = '" & failid & "'"
        fcnt = fail.Scalar(sql)
        If fcnt = 0 Then
            sql = "insert into compclasssubfm (scid, failid) values ('" & scid & "','" & failid & "')"
            fail.Update(sql)
        End If
    End Sub

    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToComp()
    End Sub
    Private Sub ToComp()
        Dim scid As String
        scid = lblscid.Value
        Dim Item As ListItem
        Dim f, fi, fst As String
        fail.Open()
        For Each Item In lbfailmaster.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                GetItems(fi, f)
            End If
        Next
        sql = "usp_updateClassSubFMOut '" & scid & "'"
        fail.Update(sql)
        PopFailList(cid, scid)
        PopFail(cid, scid)
        fail.Dispose()
    End Sub

    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        FromComp()
    End Sub
    Private Sub FromComp()
        scid = lblscid.Value
        Dim Item As ListItem
        Dim f, fi, fst As String
        fail.Open()
        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                RemItems(fi, f)
            End If
        Next
        sql = "usp_updateClassSubFMOut '" & scid & "'"
        fail.Update(sql)
        PopFailList(cid, scid)
        PopFail(cid, scid)
    End Sub
    Private Sub RemItems(ByVal failid As String, ByVal failstr As String)
        Dim scid As String
        scid = lblscid.Value
        Try
            sql = "delete from compclasssubfm where scid = '" & scid & "' and failid = '" & failid & "'"
            fail.Update(sql)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub cbopts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbopts.SelectedIndexChanged
        Dim opt As String = cbopts.SelectedValue.ToString
        lblopt.Value = opt
        If opt = "0" Then
            lbfailmodes.AutoPostBack = False
            lbfailmaster.AutoPostBack = False
        Else
            lbfailmodes.AutoPostBack = True
            lbfailmaster.AutoPostBack = True
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1881.Text = axlabs.GetASPXPage("sfail.aspx", "lang1881")
        Catch ex As Exception
        End Try
        Try
            lang1882.Text = axlabs.GetASPXPage("sfail.aspx", "lang1882")
        Catch ex As Exception
        End Try
        Try
            lang1883.Text = axlabs.GetASPXPage("sfail.aspx", "lang1883")
        Catch ex As Exception
        End Try
        Try
            lang1884.Text = axlabs.GetASPXPage("sfail.aspx", "lang1884")
        Catch ex As Exception
        End Try
        Try
            lang1885.Text = axlabs.GetASPXPage("sfail.aspx", "lang1885")
        Catch ex As Exception
        End Try
        Try
            lang1886.Text = axlabs.GetASPXPage("sfail.aspx", "lang1886")
        Catch ex As Exception
        End Try
        Try
            lang1887.Text = axlabs.GetASPXPage("sfail.aspx", "lang1887")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnaddfail.Attributes.Add("src", "../images2/eng/bgbuttons/badd.gif")
            ElseIf lang = "fre" Then
                btnaddfail.Attributes.Add("src", "../images2/fre/bgbuttons/badd.gif")
            ElseIf lang = "ger" Then
                btnaddfail.Attributes.Add("src", "../images2/ger/bgbuttons/badd.gif")
            ElseIf lang = "ita" Then
                btnaddfail.Attributes.Add("src", "../images2/ita/bgbuttons/badd.gif")
            ElseIf lang = "spa" Then
                btnaddfail.Attributes.Add("src", "../images2/spa/bgbuttons/badd.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                ibtnret.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtnret.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtnret.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtnret.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtnret.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddtosite.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("sfail.aspx", "btnaddtosite") & "')")
            btnaddtosite.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
