

'********************************************************
'*
'********************************************************



Public Class complibadddialog
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim comid, typ, tpm As String
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifrep As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try
        If Not IsPostBack Then
            Try
                typ = Request.QueryString("typ").ToString
                comid = Request.QueryString("comid").ToString
                If typ = "comp" Then
                    ifrep.Attributes.Add("src", "complibadd.aspx?comid=" & comid)
                Else
                    Try
                        tpm = Request.QueryString("tpm").ToString
                        If tpm = "no" Then
                            tpm = "N"
                        End If
                    Catch ex As Exception
                        tpm = "N"

                    End Try
                    ifrep.Attributes.Add("src", "complibedit.aspx?comid=" & comid & "&tpm=" & tpm)
                End If

            Catch ex As Exception
                ifrep.Attributes.Add("src", "complibadd.aspx")
            End Try

            'complibtasks.aspx

        End If
    End Sub

End Class
