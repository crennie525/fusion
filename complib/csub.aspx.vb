

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class csub
    Inherits System.Web.UI.Page
	Protected WithEvents lang1874 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1873 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1872 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1871 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1870 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1869 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1868 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1867 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sid, cid, coid, tid, sql, username, eqid, ro As String
    Dim Filter, SubVal As String
    Dim ds As DataSet
    Dim dslev As DataSet
    Dim dr As SqlDataReader
    Dim gtasks As New Utilities
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Login As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents addtask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dgtasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents tdtnum As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tddesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdskill As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdqty As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmin As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdrd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdrdt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltool As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllube As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnote As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            'Try
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                dgtasks.Columns(0).Visible = False
                dgtasks.Columns(10).Visible = False
                addtask.ImageUrl = "../images/appbuttons/bgbuttons/addtaskdis.gif"
                addtask.Enabled = False
            End If

            tid = Request.QueryString("tid").ToString
            lbltid.Value = tid
            If Len(tid) <> 0 AndAlso tid <> "" AndAlso tid <> "0" Then
                cid = Request.QueryString("cid").ToString
                lblcid.Value = cid
                coid = Request.QueryString("coid").ToString
                lblcoid.Value = coid
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                Filter = "comid = '" & coid & "' and tasknum = '" & tid & "'"
                lblfilt.Value = Filter
                SubVal = "(compid, comid, tasknum, subtask) values ('" & cid & "', '" & coid & "', "
                lblsubval.Value = SubVal
                gtasks.Open()
                BindTaskHead()

                BindGrid()
                Dim lock, lockby As String
                Dim user As String = lblusername.Value


                gtasks.Dispose()
                lbltool.Value = "no"
                lblpart.Value = "no"
                lbllube.Value = "no"
                lblnote.Value = "no"
            Else
                Dim strMessage As String = tmod.getmsg("cdstr745" , "csub.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "noeqid"
            End If

            'Catch ex As Exception
            'Dim strMessage As String =  tmod.getmsg("cdstr746" , "csub.aspx.vb")
 
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'lbllog.Value = "noeqid"
            'End Try

        End If

        'btnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'btnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Sub BindTaskHead()
        coid = lblcoid.Value
        sql = "select * from comptasks where comid = '" & coid & "' and tasknum = '" & tid & "' and subtask = 0"
        dr = gtasks.GetRdrData(sql)
        While dr.Read
            tdtnum.InnerHtml = dr.Item("tasknum").ToString
            tddesc.InnerHtml = dr.Item("taskdesc").ToString
            tdskill.InnerHtml = dr.Item("skill").ToString
            tdqty.InnerHtml = dr.Item("qty").ToString
            tdmin.InnerHtml = dr.Item("ttime").ToString
            tdrd.InnerHtml = dr.Item("rd").ToString
            tdrdt.InnerHtml = dr.Item("rdt").ToString

        End While
        dr.Close()
    End Sub
    Private Sub BindGrid()
        coid = lblcoid.Value
        tid = tdtnum.InnerHtml
        sql = "select * from comptasks where comid = '" & coid & "' and tasknum = '" & tid & "' " _
        + "and subtask <> 0 order by subtask"
        ds = gtasks.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Try
            dgtasks.DataSource = dv
            dgtasks.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Public Function PopulateFail(ByVal comid As String) As DataSet
        cid = lblcid.Value
        sql = "select failid, failuremode from componentfailmodes where comid = '" & comid & "' or failindex = '0' order by failuremode"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Public Function PopulatePreTech() As DataSet
        cid = lblcid.Value
        sql = "select ptid, pretech, ptindex from pmPreTech where compid = '" & cid & "' or ptindex = '0' order by ptindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
   
    Public Function PopulateTaskTypes() As DataSet
        cid = lblcid.Value
        sql = "select ttid, tasktype, taskindex from pmTaskTypes where compid = '" & cid & "' or taskindex = '0' order by taskindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function

    Public Function PopulateStatus() As DataSet
        cid = lblcid.Value
        sql = "select statid, status, statusindex from pmStatus where compid = '" & cid & "' or statusindex = '0' order by statusindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    
    Public Function PopulateSkills() As DataSet
        cid = lblcid.Value
        sid = lblsid.Value
        sql = "select skillid, skill, skillindex from pmSiteSkills where (compid = '" & cid & "' and siteid = '" & sid & "') or skillindex = '0' order by skillindex"
        dslev = gtasks.GetDSData(sql)
        Return dslev
    End Function
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function

    Private Sub addtask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles addtask.Click
        coid = lblcoid.Value
        tid = tdtnum.InnerHtml
        gtasks.Open()
        Dim stcnt As Integer = 0
        sql = "Select count(*) from compTasks " _
        + "where comid = '" & coid & "' and tasknum = '" & tid & "' and subtask <> 0"
        stcnt = gtasks.Scalar(sql)
        Dim newtst As String = stcnt + 1
        sql = "insert into comptasks (compid, siteid, deptid, cellid, eqid, funcid, comid, tasknum, subtask, " _
        + "skillid, skillindex, skill, freqid, freqindex, freq, rdid, rd, rdindex, taskstatus, ptid, ptindex, pretech, tasktype) select distinct " _
        + "compid, siteid, deptid, cellid, eqid, funcid, '" & coid & "', '" & tid & "', '" & newtst & "', " _
        + "skillid, skillindex, skill, freqid, freqindex, freq, rdid, rd, rdindex, taskstatus, " _
        + "ptid, ptindex, pretech, tasktype from comptasks " _
        + "where comid = '" & coid & "' and tasknum = '" & tid & "' and subtask = 0"
        gtasks.Update(sql)
        eqid = lbleqid.Value
        'gtasks.UpMod(eqid)
        BindGrid()
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.EditCommand
        Dim lock As String = lbllock.Value
        If lock <> "1" Then
            addtask.Enabled = False
            gtasks.Open()
            lbloldtask.Value = CType(e.Item.FindControl("lblsubt"), Label).Text
            dgtasks.EditItemIndex = e.Item.ItemIndex
            BindGrid()
            gtasks.Dispose()
        End If
    End Sub

    Private Sub dgtasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.CancelCommand
        addtask.Enabled = True
        gtasks.Open()
        dgtasks.EditItemIndex = -1
        BindGrid()
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.UpdateCommand
        addtask.Enabled = True
        Dim tn, otn, txt, qty, dt, desc, st, tid As String
        qty = CType(e.Item.FindControl("txtqty"), TextBox).Text
        txt = CType(e.Item.FindControl("txttr"), TextBox).Text
        dt = CType(e.Item.FindControl("txtdt"), TextBox).Text
        desc = CType(e.Item.FindControl("txtdesc"), TextBox).Text
        desc = gtasks.ModString2(desc)
        st = CType(e.Item.FindControl("lblst"), TextBox).Text
        tid = CType(e.Item.FindControl("lblttid"), Label).Text
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToInt64(qty)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr747" , "csub.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        Dim txtchk As Long
        Try
            txtchk = System.Convert.ToDecimal(txt)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr748" , "csub.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        Dim dtchk As Long
        Try
            dtchk = System.Convert.ToDecimal(dt)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr749" , "csub.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        sql = "update comptasks set qty = '" & qty & "', " _
        + "ttime = '" & txt & "', " _
        + "rdt = '" & dt & "', " _
        + "taskdesc = '" & desc & "' " _
        + "where pmtskid = '" & tid & "'"
        gtasks.Open()
        gtasks.Update(sql)
        tn = tdtnum.InnerHtml
        otn = lbloldtask.Value
        If otn <> st Then
            coid = lblcoid.Value
            sql = "usp_reorderCompLibSubTasks '" & tid & "', '" & coid & "', '" & tn & "', '" & st & "', '" & otn & "'"
            gtasks.Update(sql)
        End If
        eqid = lbleqid.Value
        gtasks.UpMod(eqid)
        dgtasks.EditItemIndex = -1
        BindGrid()
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.DeleteCommand
        Dim tnum, snum
        'ListItemType.SelectedItem()
        Try
            tnum = CType(e.Item.FindControl("lblta"), Label).Text
            snum = CType(e.Item.FindControl("lblsubt"), Label).Text
        Catch ex As Exception
            tnum = CType(e.Item.FindControl("lblt"), TextBox).Text
            snum = CType(e.Item.FindControl("lblst"), TextBox).Text
        End Try
        'If e.Item.ItemType = ListItemType.Item Then

        'ElseIf e.Item.ItemType = ListItemType.EditItem Then

        'End If

        coid = lblcoid.Value
        sql = "usp_delcomplibtask '" & coid & "', '" & tnum & "', '" & snum & "'"
        gtasks.Open()
        gtasks.Update(sql)
        eqid = lbleqid.Value
        gtasks.UpMod(eqid)
        Try
            dgtasks.EditItemIndex = -1
        Catch ex As Exception

        End Try
        BindGrid()
        gtasks.Dispose()
    End Sub

    Private Sub dgtasks_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtasks.ItemCommand
        If e.CommandName = "Tool" Then
            lbltool.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If

        End If
        If e.CommandName = "Part" Then
            lblpart.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        If e.CommandName = "Lube" Then
            lbllube.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        If e.CommandName = "Note" Then
            lblnote.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgtasks.Columns(0).HeaderText = dlabs.GetDGPage("csub.aspx", "dgtasks", "0")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(2).HeaderText = dlabs.GetDGPage("csub.aspx", "dgtasks", "2")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(3).HeaderText = dlabs.GetDGPage("csub.aspx", "dgtasks", "3")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(5).HeaderText = dlabs.GetDGPage("csub.aspx", "dgtasks", "5")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(6).HeaderText = dlabs.GetDGPage("csub.aspx", "dgtasks", "6")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(7).HeaderText = dlabs.GetDGPage("csub.aspx", "dgtasks", "7")
        Catch ex As Exception
        End Try
        Try
            dgtasks.Columns(8).HeaderText = dlabs.GetDGPage("csub.aspx", "dgtasks", "8")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1867.Text = axlabs.GetASPXPage("csub.aspx", "lang1867")
        Catch ex As Exception
        End Try
        Try
            lang1868.Text = axlabs.GetASPXPage("csub.aspx", "lang1868")
        Catch ex As Exception
        End Try
        Try
            lang1869.Text = axlabs.GetASPXPage("csub.aspx", "lang1869")
        Catch ex As Exception
        End Try
        Try
            lang1870.Text = axlabs.GetASPXPage("csub.aspx", "lang1870")
        Catch ex As Exception
        End Try
        Try
            lang1871.Text = axlabs.GetASPXPage("csub.aspx", "lang1871")
        Catch ex As Exception
        End Try
        Try
            lang1872.Text = axlabs.GetASPXPage("csub.aspx", "lang1872")
        Catch ex As Exception
        End Try
        Try
            lang1873.Text = axlabs.GetASPXPage("csub.aspx", "lang1873")
        Catch ex As Exception
        End Try
        Try
            lang1874.Text = axlabs.GetASPXPage("csub.aspx", "lang1874")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                addtask.Attributes.Add("src", "../images2/eng/bgbuttons/addtask.gif")
            ElseIf lang = "fre" Then
                addtask.Attributes.Add("src", "../images2/fre/bgbuttons/addtask.gif")
            ElseIf lang = "ger" Then
                addtask.Attributes.Add("src", "../images2/ger/bgbuttons/addtask.gif")
            ElseIf lang = "ita" Then
                addtask.Attributes.Add("src", "../images2/ita/bgbuttons/addtask.gif")
            ElseIf lang = "spa" Then
                addtask.Attributes.Add("src", "../images2/spa/bgbuttons/addtask.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
