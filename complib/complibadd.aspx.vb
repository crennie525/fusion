

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.IO
Public Class complibadd
    Inherits System.Web.UI.Page
    Protected WithEvents ovid210 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang1720 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1719 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1718 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1717 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1716 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1715 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1714 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1713 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1712 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1711 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1710 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1709 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1708 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1707 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1706 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1705 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim comp As New Utilities
    Dim dr As SqlDataReader
    Dim eq, fu, sql, dt, val, filt, cid, co, cod, coid, sid, did, clid, ro, appstr, typ, Login, ck, mfg, usr, isecd, lang As String
    Dim rtf, rpn, det As String
    Dim mu As New mmenu_utils_a
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtmfg As System.Web.UI.WebControls.TextBox
    Protected WithEvents ggrid As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblpcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrbimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbliorders As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovtimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrtimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddac As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblnew As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnsave1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tdpreadd As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdafteradd As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdpreadd0 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbmso As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbml As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldkey As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdcclass As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btncget As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdsclass As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnaddasset As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblsavetype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddoca As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblfailmode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbfailcomp As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblswitch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents aswitch As System.Web.UI.WebControls.Label
    Protected WithEvents imgchk As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblisecd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgetecd2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgetecd3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrecd2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewecd3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblecd1id As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblecd2txt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewecd1id As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbrtf As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents txtrpn As System.Web.UI.WebControls.TextBox
    Protected WithEvents cbdet As System.Web.UI.HtmlControls.HtmlInputCheckBox
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtconame As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdesig As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbfailmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents txtnewfail As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnaddfail As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtiorder As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents imgco As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgdel As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgsavdet As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtcokey As System.Web.UI.WebControls.TextBox

    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents geteq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents btnsave As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()

        GetFSLangs()
        Dim comi As New mmenu_utils_a
        Dim coi As String = comi.COMPI
        lblcomi.Value = coi
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
        End Try
        sid = HttpContext.Current.Session("dfltps").ToString
        lblsid.Value = sid
        If Not IsPostBack Then
            Try
                coid = Request.QueryString("comid").ToString
            Catch ex As Exception
                coid = "0"
            End Try
            isecd = mu.ECD
            lblisecd.Value = isecd
            If isecd = "1" Then
                lbfailcomp.SelectionMode = ListSelectionMode.Single
                lbfailmaster.SelectionMode = ListSelectionMode.Single
                btnsave1.Attributes.Add("onclick", "getecd1();")
            End If
            usr = HttpContext.Current.Session("username").ToString()
            lbluser.Value = usr
            lblcoid.Value = coid
            If coid <> "0" Then
                comp.Open()
                cid = "0"
                'PopAC()
                lblfailmode.Value = "norm"
                lblswitch.Value = "all"
                imgchk.Attributes.Add("class", "details")
                popoca()
                PopComp(coid)
                PopFailList(cid, coid)
                PopFail(cid, coid)
                PopFailComp(cid, coid)
                comp.Dispose()
                geteq.Attributes.Add("src", "complibtaskview2.aspx?typ=comp&comid=" & coid)
                tdpreadd.Attributes.Add("class", "details")
                rbmso.Disabled = True
                rbml.Disabled = True
                tdafteradd.Attributes.Add("class", "view")
                'btnsave.ImageUrl = "../images/appbuttons/minibuttons/savedisk1.gif"
                'lblsavetype.Value = "save"
                btncget.Attributes.Add("class", "view")
                btnaddasset.Attributes.Add("class", "view")
                btntocomp.Attributes.Add("class", "view")
                btnfromcomp.Attributes.Add("class", "view")
                todis.Attributes.Add("class", "details")
                fromdis.Attributes.Add("class", "details")
            Else
                tdpreadd.Attributes.Add("class", "view")
                rbmso.Disabled = False
                rbml.Disabled = False
                tdafteradd.Attributes.Add("class", "details")
                'btnsave.ImageUrl = "../images/appbuttons/minibuttons/savedisk1.gif"
                'lblsavetype.Value = "add"
                cid = "0"
                comp.Open()
                'PopAC()
                lblswitch.Value = "all"
                'popoca()
                'document.getElementById("btntocomp").className="details";
                'document.getElementById("btnfromcomp").className="details";
                'document.getElementById("todis").className="view";
                'document.getElementById("fromdis").className="view";
                btntocomp.Attributes.Add("class", "details")
                btnfromcomp.Attributes.Add("class", "details")
                todis.Attributes.Add("class", "view")
                fromdis.Attributes.Add("class", "view")

                comp.Dispose()
            End If
        Else
            If Request.Form("lblcompchk") = "1" Then
                lblcompchk.Value = ""
                co = lblcoid.Value
                comp.Open()
                PopComp(co)
                comp.Dispose()
            ElseIf Request.Form("lblcompchk") = "delimg" Then
                lblcompchk.Value = ""
                comp.Open()
                DelImg()
                LoadPics()
                comp.Dispose()
                'lblchksav.Value = "yes"
            ElseIf Request.Form("lblcompchk") = "checkpic" Then
                lblcompchk.Value = ""
                comp.Open()
                LoadPics()
                comp.Dispose()
            ElseIf Request.Form("lblcompchk") = "savedets" Then
                lblcompchk.Value = ""
                comp.Open()
                SaveDets()
                LoadPics()
                comp.Dispose()
            End If
            coid = lblcoid.Value
            If coid <> "" Then
                geteq.Attributes.Add("src", "complibtaskview2.aspx?typ=comp&comid=" & coid)
            End If

        End If
    End Sub
    Private Sub popoca()
        sql = "select * from ocareas"
        dr = comp.GetRdrData(sql)
        ddoca.DataSource = dr
        ddoca.DataTextField = "ocarea"
        ddoca.DataValueField = "oaid"
        ddoca.DataBind()
        dr.Close()
        ddoca.Items.Insert(0, "ALL")
    End Sub
    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsave.Click
        comp.Open()
        'Dim typ As String = lblsavetype.Value
        'If typ = "save" Then
        SaveComp()
        'Else
        'AddComp()
        'End If

        comp.Dispose()
    End Sub
    Private Sub AddComp()
        Dim ecd1id As String = lblnewecd1id.Value
        lang = lblfslang.Value
        isecd = lblisecd.Value
        ck = txtcokey.Text
        ck = comp.ModString1(ck)
        If Len(ck) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr662", "complibadd.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        'If Len(ck) > 0 Then
        co = txtconame.Text
        co = comp.ModString1(co)
        'If isecd = "1" And Len(co) > 5 Then
        '    Dim strMessage As String = "New Component Name (Error Code 1) Limited to 5 Characters"
        '    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        '    Exit Sub
        'Else
        If Len(co) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr663", "complibadd.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim desc As String = txtdesc.Text
        desc = comp.ModString1(desc)
        If Len(desc) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr664", "complibadd.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim spl As String = txtspl.Text
        spl = comp.ModString1(spl)
        If Len(spl) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr665", "complibadd.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim desig As String = txtdesig.Text
        desig = comp.ModString1(desig)
        If Len(desig) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr666", "complibadd.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim cocnt As Integer
        Dim ac, acid As String
        'If ddac.SelectedIndex <> 0 Then
        'ac = ddac.SelectedItem.ToString
        'acid = ddac.SelectedValue.ToString
        'End If
        Dim mfg As String = txtmfg.Text
        mfg = comp.ModString1(mfg)
        If Len(mfg) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr667", "complibadd.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If

        sql = "select count(*) from complib where compkey = '" & ck & "'"
        'If isecd = "1" Then
        'sql = "select count(*) from ecd1 where ecd1 = '" & co & "'"
        'cocnt = comp.Scalar(sql)
        'If cocnt > 0 Then
        'Dim strMessage As String = "New Component Name (Error Code 1) Already Exists"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End If
        'End If
        sid = lblsid.Value
        If rbmso.Checked = False Then
            sid = "0"
        End If
        usr = lbluser.Value
        cocnt = 0 'comp.Scalar(sql)
        'If cocnt = 0 Then
        sql = "usp_addComponentLib '" & ck & "', '" & co & "', '" & desc & "', '" & spl & "', '" & desig & "', '" & ac & "','" & acid & "','" & mfg & "','" & sid & "','" & usr & "','" & ecd1id & "','" & lang & "'"
        coid = comp.Scalar(sql)
        lblcoid.Value = coid
        popoca()
        PopComp(coid)
        'PopAC()
        'PopComp(coid)
        PopFailList(cid, coid)
        PopFail(cid, coid)
        PopFailComp(cid, coid)
        lblsavetype.Value = "save"
        'tdpreadd0.Attributes.Add("class", "details")
        tdpreadd.Attributes.Add("class", "details")
        rbmso.Disabled = True
        rbml.Disabled = True
        tdafteradd.Attributes.Add("class", "view")
        btntocomp.Attributes.Add("class", "view")
        btnfromcomp.Attributes.Add("class", "view")
        todis.Attributes.Add("class", "details")
        fromdis.Attributes.Add("class", "details")
        btncget.Attributes.Add("class", "view")
        btnaddasset.Attributes.Add("class", "view")
        'Else
        '    Dim strMessage As String =  tmod.getmsg("cdstr668" , "complibadd.aspx.vb")

        '   Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        '   comp.Dispose()
        'End If
        'Else
        'Dim strMessage As String =  tmod.getmsg("cdstr669" , "complibadd.aspx.vb")

        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'comp.Dispose()
        'End If

    End Sub
    Private Sub PopComp(ByVal coid As String)
        sql = "select * from complib where comid = '" & coid & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            rtf = dr.Item("rtf").ToString
            det = dr.Item("det").ToString
            rpn = dr.Item("rpn").ToString
            txtcokey.Text = dr.Item("compkey").ToString
            lbloldkey.Value = dr.Item("compkey").ToString
            txtconame.Text = dr.Item("compnum").ToString
            lbloldcomp.Value = dr.Item("compnum").ToString
            txtdesc.Text = dr.Item("compdesc").ToString
            txtdesig.Text = dr.Item("desig").ToString
            txtspl.Text = dr.Item("spl").ToString
            txtmfg.Text = dr.Item("mfg").ToString
            'Try
            'ddac.SelectedValue = dr.Item("acid").ToString
            'Catch ex As Exception
            tdcclass.InnerHtml = dr.Item("assetclass").ToString
            tdsclass.InnerHtml = dr.Item("subclass").ToString
            'End Try
        End While
        dr.Close()
        If rtf = "1" Then
            cbrtf.Checked = True
        Else
            cbrtf.Checked = False
        End If
        If det = "1" Then
            cbdet.Checked = True
        Else
            cbdet.Checked = False
        End If
        If rpn = "" Then
            rpn = "0"
        End If
        txtrpn.Text = rpn
        cid = "0"
        PopFailList(cid, coid)
        PopFail(cid, coid)
        PopFailComp(cid, coid)
        LoadPics()
    End Sub
    Private Sub SaveComp()
        Dim rtf As Integer
        If cbrtf.Checked = True Then
            rtf = 1
        Else
            rtf = 0
        End If
        If cbdet.Checked = True Then
            det = 1
        Else
            det = 0
        End If
        rpn = txtrpn.Text
        Dim rpni As Integer
        Try
            rpni = txtrpn.Text
        Catch ex As Exception
            rpni = 0
        End Try
        coid = lblcoid.Value
        ck = txtcokey.Text
        ck = comp.ModString1(ck)
        co = txtconame.Text
        co = comp.ModString1(co)
        Dim desc As String = txtdesc.Text
        desc = comp.ModString1(desc)
        Dim spl As String = txtspl.Text
        spl = comp.ModString1(spl)
        Dim desig As String = txtdesig.Text
        desig = comp.ModString1(desig)
        Dim cqty As String = "1"
        Dim cocnt As Integer
        Dim ac, acid As String
        'If ddac.SelectedIndex <> 0 Then
        'ac = ddac.SelectedItem.ToString
        'acid = ddac.SelectedValue.ToString
        'End If
        Dim mfg As String = txtmfg.Text
        mfg = comp.ModString1(mfg)
        If Len(mfg) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr670", "complibadd.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim oldkey As String = lbloldkey.Value
        Dim typ As String = lblsavetype.Value

        If ck <> oldkey Or typ = "add" Then
            sql = "select count(*) from complib where compkey = '" & ck & "'"
            cocnt = comp.Scalar(sql)
        Else
            If typ = "add" Then
                cocnt = 0
            Else
                cocnt = 0 ' 1
            End If

        End If



        'If (cocnt = 0 And typ = "add") Or (cocnt = 1 And typ = "save") Then

        sql = "usp_updateComponentLib_new '" & coid & "', '" & ck & "', '" & co & "', '" & cqty & "', '" & desc & "', '" & spl & "', '" & desig & "', '" & ac & "','" & acid & "','" & mfg & "','" & rtf & "','" & rpn & "','" & det & "'"
        Try
            comp.Update(sql)
            lblsavetype.Value = "save"
            'PopAC()
            PopComp(coid)
            PopFailList(cid, coid)
            PopFail(cid, coid)
            tdpreadd0.Attributes.Add("class", "details")
            tdpreadd.Attributes.Add("class", "details")
            btncget.Attributes.Add("class", "view")
            rbmso.Disabled = True
            rbml.Disabled = True
            tdafteradd.Attributes.Add("class", "view")
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr671", "complibadd.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            comp.Dispose()
        End Try
        'Else
        'Dim strMessage As String =  tmod.getmsg("cdstr672" , "complibadd.aspx.vb")

        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'comp.Dispose()
        'End If


    End Sub

    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        cid = "0" 'lblcid.Value
        coid = lblcoid.Value
        Dim Item As ListItem
        Dim f, fi As String
        Dim fail As New Utilities
        Dim sw As String = lblswitch.Value
        fail.Open()
        If sw = "comp" Then
            For Each Item In lbfailcomp.Items
                If Item.Selected Then
                    f = Item.Text.ToString
                    fi = Item.Value.ToString
                    GetItems(fi, f)
                End If
            Next
        Else
            For Each Item In lbfailmaster.Items
                If Item.Selected Then
                    f = Item.Text.ToString
                    fi = Item.Value.ToString
                    GetItems(fi, f)
                End If
            Next
        End If
        PopFailList(cid, coid)
        PopFail(cid, coid)
        fail.Dispose()
    End Sub

    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        cid = "0" 'lblcid.Value
        coid = lblcoid.Value
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        Dim fail As New Utilities
        fail.Open()
        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                RemItems(fi, f)
            End If
        Next
        PopFailList(cid, coid)
        PopFail(cid, coid)
        fail.Dispose()
    End Sub
    Private Sub RemItems(ByVal failid As String, ByVal failstr As String)
        isecd = lblisecd.Value
        coid = lblcoid.Value
        Dim sw As String = lblswitch.Value
        Dim fail As New Utilities
        fail.Open()
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If oca = "ALL" Then
            oaid = ""
        End If
        Dim ecd3id, ecd3cnt As Integer
        If isecd = "1" Then
            sql = "delete from ecd3 where ecd3id = (select ecd3id from complibfm where compfailid = '" & failid & "')"
            fail.Update(sql)
        End If
        If sw = "comp" Then
            sql = "update ComponentFailModes set oaid = NULL, ecd1id = NULL, ecd2id = NULL, ecd3id = NULL where compfailid = '" & failid & "';"

        Else

            sql = "usp_delComponentFailModeco '" & coid & "', '" & failid & "'" ','" & oaid & "'"


        End If

        fail.Update(sql)
        fail.Dispose()
    End Sub
    
    Private Sub GetItems(ByVal failid As String, ByVal failstr As String)
        isecd = lblisecd.Value
        cid = "0"
        coid = lblcoid.Value
        Dim sw As String = lblswitch.Value
        Dim fail As New Utilities
        fail.Open()
        Dim fcnt As Integer
        fcnt = 0
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If oca = "ALL" Or oaid = "" Then
            sql = "select count(*) from complibfm where compid = '" & cid & "' and " _
        + "comid = '" & coid & "' and failid = '" & failid & "' and oaid is null"
        Else
            sql = "select count(*) from complibfm where compid = '" & cid & "' and " _
        + "comid = '" & coid & "' and failid = '" & failid & "' and oaid = '" & oaid & "'"
        End If
        fcnt = fail.Scalar(sql)
        Dim ecd As String = lblcurrecd2.Value
        'lblcurrecd2.Value = ""
        Dim ecd1, ecd2, ecd3 As String
        If ecd <> "" Then
            Dim ecdarr() As String = ecd.Split("-")
            ecd2 = ecdarr(0).ToString
            ecd1 = ecdarr(1).ToString
        End If
        ecd3 = lblnewecd3.Value
        lblnewecd3.Value = ""
        Dim ecd3id, ecd3cnt As Integer
        If isecd = "1" Then
            If ecd3 <> "" Then
                sql = "select count(*) from ecd3 where ecd3 = '" & ecd3 & "' and ecd1id = '" & ecd1 & "' and ecd2id = '" & ecd2 & "'"
                ecd3cnt = fail.Scalar(sql)
                If ecd3cnt = 0 Then
                    sql = "insert into ecd3 (ecd3, ecddesc, ecd1id, ecd2id) values ('" & ecd3 & "','" & oca & "','" & ecd1 & "','" & ecd2 & "') select @@identity"
                    ecd3id = fail.Scalar(sql)
                Else
                    sql = "select ecd3id from ecd3 where ecd3 = '" & ecd3 & "' and ecd1id = '" & ecd1 & "' and ecd2id = '" & ecd2 & "'"
                    ecd3id = fail.Scalar(sql)
                End If
            Else
                Dim strMessage As String = "No Error Code 3 Value Created"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        End If


        If fcnt = 0 Then
            If oca = "ALL" Then
                oaid = ""
            End If
            If sw = "comp" And oca <> "ALL" Then
                If ecd <> "" Then
                    sql = "update complibfm set oaid = '" & oaid & "', ecd1id = '" & ecd1 & "', ecd2id = '" & ecd2 & "', ecd3id = '" & ecd3id & "' where compfailid = '" & failid & "'"
                Else
                    sql = "update complibfm set oaid = '" & oaid & "' where compfailid = '" & failid & "'"
                End If

            Else
                sql = "usp_addFailureModeco '" & cid & "', '" & coid & "', '" & failid & "', '" & failstr & "','" & oaid & "','" & ecd1 & "','" & ecd2 & "','" & ecd3id & "'"
            End If

            fail.Update(sql)
        End If
        fail.Dispose()

    End Sub
    
    Private Sub ddoca_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddoca.SelectedIndexChanged
        Dim coid As String = lblcoid.Value
        Dim cid As String = "0"
        comp.Open()
        isecd = lblisecd.Value
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If isecd = "1" And oca <> "ALL" Then
            checkecd()
        End If
        PopFailList(cid, coid)
        PopFailComp(cid, coid)
        comp.Dispose()
    End Sub
    Private Sub checkecd()
        isecd = lblisecd.Value
        Dim ecd2, ecd2id, ecd1id As String
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        Dim coid As String = lblcoid.Value
        ecd1id = lblecd1id.Value
        If isecd = "1" Then
            sql = "select count(*) from ecd2 where ecd1id = '" & ecd1id & "' and ecddesc = '" & oca & "'"
        Else
            sql = "select count(*) from complibfm where oaid = '" & oaid & "' and comid = '" & coid & "'"
        End If

        Dim oaidcnt As Integer = comp.Scalar(sql)
        If oaidcnt = 0 Then
            lblcurrecd2.Value = ""
            lblnewecd3.Value = ""
            lblgetecd2.Value = "1"
            lblecd2txt.Value = oca
            'lblecd1id.Value = ""
        Else
            If isecd = "1" Then
                sql = "select ecd1id, ecd2id from ecd2 where ecd1id = '" & ecd1id & "' and ecddesc = '" & oca & "'"
            Else
                sql = "select e.ecd2, e.ecd1id, c.ecd2id from complibfm c left join ecd2 e on e.ecd2id = c.ecd2id where c.oaid = '" & oaid & "' and c.comid = '" & coid & "'"
            End If

            dr = comp.GetRdrData(sql)
            While dr.Read
                ecd2id = dr.Item("ecd2id").ToString
                ecd1id = dr.Item("ecd1id").ToString
            End While
            dr.Close()
            lblcurrecd2.Value = ecd2id & "-" & ecd1id
            lblnewecd3.Value = ""
            lblgetecd2.Value = "0"
            lblecd1id.Value = ecd1id
        End If
    End Sub
    Private Sub PopFailComp(ByVal cid As String, ByVal coid As String)
        Dim faillist As New Utilities
        faillist.Open()
        sql = "select compfailid, failuremode from complibfm where compid = '" & cid & "' and " _
          + "comid = '" & coid & "' and oaid is null order by failuremode"
        dr = faillist.GetRdrData(sql)
        lbfailcomp.DataSource = dr
        lbfailcomp.DataValueField = "compfailid"
        lbfailcomp.DataTextField = "failuremode"
        lbfailcomp.DataBind()
        dr.Close()
        Dim fcnt As Integer = 0
        Dim sw As String = lblswitch.Value
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If oca = "All" Or ddoca.SelectedIndex = 0 Or ddoca.SelectedIndex = -1 Then
            lblfailmode.Value = "norm"
            imgchk.Attributes.Add("class", "details")
            lbfailcomp.Attributes.Add("class", "details")
            lbfailmaster.Attributes.Add("class", "view")
            aswitch.Text = "Available Failure Modes"
        Else

            sql = "select count(*) from complibfm where compid = '" & cid & "' and " _
          + "comid = '" & coid & "' and oaid is null"
            fcnt = faillist.Scalar(sql)
            If fcnt > 0 Then
                lblfailmode.Value = "rev"
                imgchk.Attributes.Add("class", "view")
                If sw = "comp" Then
                    lbfailcomp.Attributes.Add("class", "view")
                    lbfailmaster.Attributes.Add("class", "details")
                    aswitch.Text = "Unassigned Failure Modes"
                Else
                    lbfailcomp.Attributes.Add("class", "details")
                    lbfailmaster.Attributes.Add("class", "view")
                    aswitch.Text = "Available Failure Modes"
                End If
            Else
                lblfailmode.Value = "norm"
                imgchk.Attributes.Add("class", "details")
                If sw = "comp" Then
                    lbfailcomp.Attributes.Add("class", "details")
                    lbfailmaster.Attributes.Add("class", "view")
                    aswitch.Text = "Available Failure Modes"
                    lblswitch.Value = "all"

                End If
            End If
        End If
        faillist.Dispose()
    End Sub
    Private Sub PopFailList(ByVal cid As String, ByVal coid As String)
        Dim faillist As New Utilities
        Dim lang As String = lblfslang.Value
        Dim coi As String = lblcomi.Value
        faillist.Open()
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        Dim ocas As String
        If oca = "ALL" Then
            sql = "usp_getcfall_coco '" & coid & "','" & lang & "'"
        Else
            sql = "select distinct compfailid, failuremode from complibfm where compid = '" & cid & "' and " _
               + "comid = '" & coid & "' and oaid = '" & oaid & "' order by failuremode"
        End If

        dr = faillist.GetRdrData(sql)
        lbfailmodes.DataSource = dr
        lbfailmodes.DataValueField = "compfailid"
        lbfailmodes.DataTextField = "failuremode"
        lbfailmodes.DataBind()
        dr.Close()
        faillist.Dispose()

    End Sub
    Private Sub PopFail(ByVal cid As String, ByVal comid As String)
        Dim coi As String = lblcomi.Value
        Dim lang As String = lblfslang.Value
        Dim fail As New Utilities
        fail.Open()
        Dim chk As String = "open" 'lblfailchk.Value
        Dim scnt As Integer
        'sid = lblsid.Value
        'If chk = "" Then
        'sql = "select count(*) from pmSiteFM where siteid = '" & sid & "'"
        'scnt = fail.Scalar(sql)
        'If scnt = 0 Then
        'lblfailchk.Value = "open"
        'chk = "open"
        'Else
        '    lblfailchk.Value = "site"
        '    chk = "site"
        'End If
        'End If
        Dim dt, val, filt As String
        If coi <> "CAS" And coi <> "AGR" And coi <> "LAU" And coi <> "GLA" Then
            If chk = "open" Then
                dt = "FailureModes"
            ElseIf chk = "site" Then
                dt = "pmSiteFM"
            Else
                Exit Sub
            End If
            val = "failid, failuremode"
        Else
            chk = "open"
            dt = "FailureModes"
            If lang = "fre" Then
                val = "failid, fmeng"
            Else
                val = "failid, failuremode"
            End If
        End If
        


        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If

        If oca = "ALL" Then
            If chk = "site" Then
                filt = " where siteid = '" & sid & "' and failid not in (" _
                       + "select failid from complibfm where comid = '" & comid & "' and oaid is null) " 'order by failuremode asc"
            Else
                filt = " where failid not in (" _
       + "select failid from complibfm where comid = '" & comid & "' and oaid is null) " 'order by failuremode asc"
            End If

        Else
            If chk = "site" Then
                filt = " where siteid = '" & sid & "' and failid not in (" _
                           + "select failid from complibfm where comid = '" & comid & "' and oaid = '" & oaid & "') " 'order by failuremode asc"
            Else
                filt = " where failid not in (" _
           + "select failid from complibfm where comid = '" & comid & "' and oaid = '" & oaid & "') " 'order by failuremode asc"
            End If

        End If

        dr = fail.GetList(dt, val, filt) 'removed compid = '" & cid & "' and 
        lbfailmaster.DataSource = dr
        If lang = "fre" And (coi <> "CAS" Or coi <> "AGR" Or coi <> "LAU" Or coi <> "GLA") Then
            lbfailmaster.DataTextField = "fmeng"
        Else
            lbfailmaster.DataTextField = "failuremode"
        End If
        'lbfailmaster.DataTextField = "failuremode"
        lbfailmaster.DataValueField = "failid"
        lbfailmaster.DataBind()
        dr.Close()
        fail.Dispose()
    End Sub
    Private Sub LoadPics()
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/tpmimages/"
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        'lblnsimage.Value = nsimage
        'lblstrfrom.Value = strfrom

        'imgco.Attributes.Add("src", "../images/appimages/compimg.gif")
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgco.Attributes.Add("src", "../images2/eng/compimg.gif")
        ElseIf lang = "fre" Then
            imgco.Attributes.Add("src", "../images2/fre/compimg.gif")
        ElseIf lang = "ger" Then
            imgco.Attributes.Add("src", "../images2/ger/compimg.gif")
        ElseIf lang = "ita" Then
            imgco.Attributes.Add("src", "../images2/ita/compimg.gif")
        ElseIf lang = "spa" Then
            imgco.Attributes.Add("src", "../images2/spa/compimg.gif")
        End If

        Dim img, imgs, picid As String

        coid = lblcoid.Value
        Dim pcnt As Integer
        sql = "select count(*) from comppictures where comid is not null and comid = '" & coid & "'"
        pcnt = comp.Scalar(sql)
        lblpcnt.Value = pcnt

        Dim currp As String = lblcurrp.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpg.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lblpg.Text = "Image 1 of " & pcnt
                lblcurrp.Value = "0"
            End If

            sql = "select p.pic_id, p.picurl, p.picurltn, p.picurltm, p.picorder " _
            + "from comppictures p where p.comid is not null and p.comid = '" & coid & "'" _
            + "order by p.picorder"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs, timg As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs, ovtimg, ovtimgs
            dr = comp.GetRdrData(sql)
            While dr.Read
                iflag += 1
                img = dr.Item("picurltm").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)
                bimg = dr.Item("picurl").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)

                timg = dr.Item("picurltn").ToString
                Dim timgarr() As String = timg.Split("/")
                ovtimg = timgarr(timgarr.Length - 1)

                picid = dr.Item("pic_id").ToString
                order = dr.Item("picorder").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If

                If ovtimgs = "" Then
                    ovtimgs = ovtimg
                Else
                    ovtimgs += "," & ovtimg
                End If

                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimg.Value = ovimg
                    lblcurrbimg.Value = ovbimg
                    lblcurrtimg.Value = ovtimg
                    imgco.Attributes.Add("src", img)
                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgid.Value = picid

                    txtiorder.Text = order
                End If
                If imgs = "" Then
                    imgs = picid & ";" & img
                Else
                    imgs += "~" & picid & ";" & img
                End If
            End While
            dr.Close()
            lblimgs.Value = imgs
            lblbimgs.Value = bimgs
            lblovimgs.Value = ovimgs
            lblovbimgs.Value = ovbimgs
            lblovtimgs.Value = ovtimgs
            lbliorders.Value = iorders
        End If
    End Sub
    Private Sub DelImg()
        coid = lblcoid.Value
        Dim pid As String = lblimgid.Value
        Dim old As String = lbloldorder.Value
        typ = "co"
        sql = "usp_delcoimg '" & typ & "','" & coid & "','" & pid & "','" & old & "'"
        comp.Update(sql)
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim pic, picn, picm As String
        pic = lblcurrbimg.Value
        picn = lblcurrtimg.Value
        picm = lblcurrimg.Value
        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        'Try
        'If File.Exists(fpic) Then
        'File.Delete1(fpic)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicm) Then
        'File.Delete1(fpicm)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicn) Then
        'File.Delete1(fpicn)
        'End If
        'Catch ex As Exception

        'End Try
        lblcurrp.Value = "0"

    End Sub
    Private Sub SaveDets()
        Dim iord As String = txtiorder.Text
        Try
            Dim piord As Integer = System.Convert.ToInt32(iord)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr673", "complibadd.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim oldiord As Integer = lblcurrp.Value + 1
        If oldiord <> iord Then
            Dim old As String = lbloldorder.Value
            Dim neword As String = txtiorder.Text
            coid = lblcoid.Value
            typ = "co"
            sql = "usp_reordercoimg '" & typ & "','" & coid & "','" & neword & "','" & old & "'"
            comp.Update(sql)
        End If
    End Sub

    Private Sub btnsave1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsave1.Click
        comp.Open()
        'Dim typ As String = lblsavetype.Value
        'If typ = "save" Then
        'SaveComp()
        'Else
        AddComp()
        'End If

        comp.Dispose()
    End Sub

    Private Sub btnaddfail_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnaddfail.Click
        If txtnewfail.Text <> "" Then
            comp.Open()
            Dim failstr As String

            failstr = txtnewfail.Text
            failstr = Replace(failstr, "'", Chr(180), , , vbTextCompare)
            failstr = Replace(failstr, "--", "-", , , vbTextCompare)
            failstr = Replace(failstr, ";", ":", , , vbTextCompare)
            cid = "0"
            'cid = "0"
            coid = lblcoid.Value
            'coid = HttpContext.Current.Session("coid").ToString()
            sid = lblsid.Value
            Dim strchk As Integer
            sql = "select count(*) from FailureModes where failuremode = '" & failstr & "'"
            strchk = comp.Scalar(sql)
            If strchk = 0 Then
                Dim fid As Integer
                failstr = txtnewfail.Text
                sql = "insert into FailureModes " _
                + "(compid, failuremode, fmeng) values ('" & cid & "', '" & failstr & "','" & failstr & "') select @@identity as 'identity'"
                fid = comp.Scalar(sql)
                sql = "usp_addSiteFM '" & cid & "', '" & sid & "', '" & fid & "', '" & failstr & "'"
                comp.Update(sql)
            Else
                Dim strMessage As String = tmod.getmsg("cdstr674", "complibadd.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            PopFail(cid, coid)
        End If
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1705.Text = axlabs.GetASPXPage("complibadd.aspx", "lang1705")
        Catch ex As Exception
        End Try
        Try
            lang1706.Text = axlabs.GetASPXPage("complibadd.aspx", "lang1706")
        Catch ex As Exception
        End Try
        Try
            lang1707.Text = axlabs.GetASPXPage("complibadd.aspx", "lang1707")
        Catch ex As Exception
        End Try
        Try
            lang1708.Text = axlabs.GetASPXPage("complibadd.aspx", "lang1708")
        Catch ex As Exception
        End Try
        Try
            lang1709.Text = axlabs.GetASPXPage("complibadd.aspx", "lang1709")
        Catch ex As Exception
        End Try
        Try
            lang1710.Text = axlabs.GetASPXPage("complibadd.aspx", "lang1710")
        Catch ex As Exception
        End Try
        Try
            lang1711.Text = axlabs.GetASPXPage("complibadd.aspx", "lang1711")
        Catch ex As Exception
        End Try
        Try
            lang1712.Text = axlabs.GetASPXPage("complibadd.aspx", "lang1712")
        Catch ex As Exception
        End Try
        Try
            lang1713.Text = axlabs.GetASPXPage("complibadd.aspx", "lang1713")
        Catch ex As Exception
        End Try
        Try
            lang1714.Text = axlabs.GetASPXPage("complibadd.aspx", "lang1714")
        Catch ex As Exception
        End Try
        Try
            lang1715.Text = axlabs.GetASPXPage("complibadd.aspx", "lang1715")
        Catch ex As Exception
        End Try
        Try
            lang1716.Text = axlabs.GetASPXPage("complibadd.aspx", "lang1716")
        Catch ex As Exception
        End Try
        Try
            lang1717.Text = axlabs.GetASPXPage("complibadd.aspx", "lang1717")
        Catch ex As Exception
        End Try
        Try
            lang1718.Text = axlabs.GetASPXPage("complibadd.aspx", "lang1718")
        Catch ex As Exception
        End Try
        Try
            lang1719.Text = axlabs.GetASPXPage("complibadd.aspx", "lang1719")
        Catch ex As Exception
        End Try
        Try
            lang1720.Text = axlabs.GetASPXPage("complibadd.aspx", "lang1720")
        Catch ex As Exception
        End Try
        Try
            lblpg.Text = axlabs.GetASPXPage("complibadd.aspx", "lblpg")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddasset.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complibadd.aspx", "btnaddasset") & "')")
            btnaddasset.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            btncget.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complibadd.aspx", "btncget") & "')")
            btncget.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complibadd.aspx", "Img2") & "')")
            Img2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgdel.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complibadd.aspx", "imgdel") & "', ABOVE, LEFT)")
            imgdel.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsavdet.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complibadd.aspx", "imgsavdet") & "', ABOVE, LEFT)")
            imgsavdet.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid210.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complibadd.aspx", "ovid210") & "', ABOVE, LEFT)")
            ovid210.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub


End Class
