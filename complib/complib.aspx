<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="complib.aspx.vb" Inherits="lucy_r12.complib" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>complib</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="javascript" src="../scripts/smartscroll4.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="JavaScript" src="../scripts1/complibaspx_1.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
        <script language="javascript" type="text/javascript">
            function getsrch() {
                var eReturn = window.showModalDialog("../equip/maxsearchdialog.aspx?lib=no&srchtyp=noco" + "&date=" + Date(), "", "dialogHeight:720px; dialogWidth:890px; resizable=yes");
                if (eReturn) {
                    var ret = eReturn;
                    var retarr = ret.split(",");
                    document.getElementById("lbldid").value = retarr[0];
                    document.getElementById("lblclid").value = retarr[1];
                    document.getElementById("lbleqid").value = retarr[2];
                    document.getElementById("tdeq").innerHTML = retarr[3];

                    document.getElementById("lblfuid").value = retarr[4];

                    document.getElementById("tdfunc").innerHTML = retarr[5];

                    document.getElementById("tddept").innerHTML = retarr[7];
                    document.getElementById("tdcell").innerHTML = retarr[8];

                    document.getElementById("lbleq").value = retarr[3];
                    document.getElementById("lblfu").value = retarr[5];
                    document.getElementById("lbldept").value = retarr[7];
                    document.getElementById("lblcell").value = retarr[8];
                    document.getElementById("lblcopytyp").value = "dest";
                    document.getElementById("txtnewkey").value = document.getElementById("lblselkey").value;
                    document.getElementById("txtnewkey").disabled = true;
                }
                else {
                    document.getElementById("rbmso").checked = true;
                    //document.getElementById("rbrbteq").checked = true;
                    document.getElementById("lblcopytyp").value = "site";
                    document.getElementById("txtnewkey").disabled = false;
                }
            }
        </script>
	    <style type="text/css">
            #Img2
            {
                height: 16px;
            }
        </style>
	</HEAD>
	<body onload="GetsScroll();checkcopyret();" >
		<form id="form1" method="post" runat="server">
			<div style="POSITION: absolute; TOP: 0px; LEFT: 4px" id="scrollmenu"></div>
			<div id="FreezePane" class="FreezePaneOff" align="center">
				<div id="InnerFreezePane" class="InnerFreezePane"></div>
			</div>
			<div style="Z-INDEX: 1000; POSITION: absolute; VISIBILITY: hidden" id="overDiv"></div>
			<table style="Z-INDEX: 1; POSITION: absolute; TOP: 82px; LEFT: 4px" id="tbltop" cellSpacing="0"
				cellPadding="1" width="1040" runat="server">
				<tr>
					<td colSpan="3">
						<table cellSpacing="0" cellPadding="1" width="1040">
							<tr>
								<td id="tdnavtop" class="thdrsinglft" width="22"><IMG border="0" src="../images/appbuttons/minibuttons/eqarch.gif"></td>
								<td class="thdrsingrt label" width="978"><asp:Label id="lang1679" runat="server">Component Library</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="3">
						<table>
							<tr>
								<td class="details" width="325">
									<table>
										<tr>
											<td class="bluelabel"><asp:Label id="lang1680" runat="server">Select Language Database</asp:Label></td>
											<td><asp:dropdownlist id="dddb" runat="server" AutoPostBack="True">
													<asp:ListItem Value="laipm3_devdb2">English</asp:ListItem>
													<asp:ListItem Value="laipm3_presdb2">German</asp:ListItem>
													<asp:ListItem Value="pfizerpm3_ita">Italian</asp:ListItem>
												</asp:dropdownlist></td>
										</tr>
									</table>
								</td>
								<td class="bluelabel" width="80"><asp:Label id="lang1681" runat="server">Search Using</asp:Label></td>
								<td width="170"><asp:textbox id="txtsrch" runat="server" cssclass="plainlabel wd170" Width="160px"></asp:textbox></td>
								<td class="bluelabel" width="160"><u>AND\OR</u><asp:Label id="lang1682" runat="server">Component Class</asp:Label></td>
								<td width="190"><asp:textbox id="txtcompclass" runat="server" cssclass="plainlabel wd190" Width="160px"></asp:textbox><IMG onclick="getcclass();" src="../images/appbuttons/minibuttons/magnifier.gif">
								</td>
								<td class="bluelabel" width="130"><u>OR</u><asp:Label id="lang1683" runat="server">Using Search Key</asp:Label></td>
								<td width="200"><asp:textbox id="txtsrchkey" runat="server" Width="170px" CssClass="plainlabel"></asp:textbox><IMG onclick="getsrch1('key','txtsrchkey');" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
								<td width="20"><asp:imagebutton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
								<td width="20"><IMG onclick="refreshit();" src="../images/appbuttons/minibuttons/refreshit.gif"></td>
								<td width="20" align="right"><IMG id="Img2" onmouseover="return overlib('Add a Component to the Library', LEFT)" onmouseout="return nd()"
										onclick="addcomp();" alt="" src="../images/appbuttons/minibuttons/addmod.gif" runat="server">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="3">
						<table>
							<tr height="22">
								<td id="tdmc" class="thdrhov label" onclick="gettab('tdmc');" width="150" runat="server"><A class="label" href="#"><asp:Label id="lang1684" runat="server">My Components</asp:Label></A></td>
								<td id="tdsc" class="thdr label" onclick="gettab('tdsc');" width="150" runat="server"><A class="label" href="#"><asp:Label id="lang1685" runat="server">Plant Site Components</asp:Label></A></td>
								<td id="tdac" class="thdr label" onclick="gettab('tdac');" width="150" runat="server"><A class="label" href="#"><asp:Label id="lang1686" runat="server">Master List</asp:Label></A></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="treditmsg" class="details" runat="server">
					<td class="plainlabelred" colSpan="3" align="center"><asp:Label id="lang1687" runat="server">Please note that changes made to any Component will not be reflected in any Function currently using the current version of this Component.</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="3">
						<table id="tbleq" cellSpacing="0" cellPadding="0" width="1040" runat="server">
							<TBODY>
								<tr id="tbleqrec" runat="server">
									<td class="bluelabel" width="80"></td>
									<td width="540"></td>
									<td width="280"></td>
								</tr>
								<tr id="treq" runat="server">
									<td colSpan="3">
										<div style="WIDTH: 1040px; HEIGHT: 180px; OVERFLOW: auto" onscroll="SetsDivPosition();"
											id="spdiv"><asp:repeater id="rptreq" runat="server">
												<HeaderTemplate>
													<table cellspacing="1">
														<tr>
															<td class="btmmenu plainlabel" width="30px"><asp:Label id="lang1688" runat="server">Edit</asp:Label></td>
															<td class="btmmenu plainlabel" width="300px" height="20px"><asp:Label id="lang1689" runat="server">Search Key</asp:Label></td>
															<td class="btmmenu plainlabel" width="300px"><asp:Label id="lang1690" runat="server">Common Name</asp:Label></td>
															<td class="btmmenu plainlabel" width="300px"><asp:Label id="lang1691" runat="server">Description</asp:Label></td>
															<td class="btmmenu plainlabel" width="60px">Delete</td>
														</tr>
												</HeaderTemplate>
												<ItemTemplate>
													<tr class="tbg">
														<td><img src="../images/appbuttons/minibuttons/lilpentrans.gif" id="imgei" runat="server"></td>
														<td class="plainlabel">
															<asp:RadioButton AutoPostBack="True" OnCheckedChanged="GetCo1" id="rbco" Text='<%# DataBinder.Eval(Container.DataItem,"compkey")%>' runat="server" />
														</td>
														<td class="plainlabel">
															<asp:Label ID="lblcompnum"  Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>' Runat = server>
															</asp:Label></td>
														<td class="plainlabel">
															<asp:Label ID="lblcompdesc" Text='<%# DataBinder.Eval(Container.DataItem,"compdesc")%>' Runat = server>
															</asp:Label></td>
														<td><asp:ImageButton id="ibDel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton></td>
														<td class="details"><asp:Label ID="lblcomid" Text='<%# DataBinder.Eval(Container.DataItem,"comid")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lbldesig" Text='<%# DataBinder.Eval(Container.DataItem,"desig")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblspl" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblcby" Text='<%# DataBinder.Eval(Container.DataItem,"createdby")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblmby" Text='<%# DataBinder.Eval(Container.DataItem,"modifiedby")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblcdate" Text='<%# DataBinder.Eval(Container.DataItem,"createdate")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblmdate" Text='<%# DataBinder.Eval(Container.DataItem,"modifieddate")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblmfg" Text='<%# DataBinder.Eval(Container.DataItem,"mfg")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblkeyi" Text='<%# DataBinder.Eval(Container.DataItem,"compkey")%>' Runat = server>
															</asp:Label></td>
													</tr>
												</ItemTemplate>
												<AlternatingItemTemplate>
													<tr>
														<td class="transrowblue"><img src="../images/appbuttons/minibuttons/lilpentrans.gif" id="imgea" runat="server"></td>
														<td class="plainlabel transrowblue">
															<asp:RadioButton AutoPostBack="True"   OnCheckedChanged="GetCo1" id="rbcoalt" Text='<%# DataBinder.Eval(Container.DataItem,"compkey")%>' runat="server" />
														</td>
														<td class="plainlabel transrowblue">
															<asp:Label ID="lblcompnumalt" Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>' Runat = server>
															</asp:Label></td>
														<td class="plainlabel transrowblue">
															<asp:Label ID="lblcompdescalt" Text='<%# DataBinder.Eval(Container.DataItem,"compdesc")%>' Runat = server>
															</asp:Label></td>
														<td class="plainlabel transrowblue"><asp:ImageButton id="ibDela" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton></td>
														<td class="details"><asp:Label ID="lblcomidalt" Text='<%# DataBinder.Eval(Container.DataItem,"comid")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lbldesigalt" Text='<%# DataBinder.Eval(Container.DataItem,"desig")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblsplalt" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblcbyalt" Text='<%# DataBinder.Eval(Container.DataItem,"createdby")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblmbyalt" Text='<%# DataBinder.Eval(Container.DataItem,"modifiedby")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblcdatealt" Text='<%# DataBinder.Eval(Container.DataItem,"createdate")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblmdatealt" Text='<%# DataBinder.Eval(Container.DataItem,"modifieddate")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblmfgalt" Text='<%# DataBinder.Eval(Container.DataItem,"mfg")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblkeya" Text='<%# DataBinder.Eval(Container.DataItem,"compkey")%>' Runat = server>
															</asp:Label></td>
													</tr>
												</AlternatingItemTemplate>
												<FooterTemplate>
						</table>
						</FooterTemplate> </asp:repeater></DIV></td>
				</tr>
				<tr>
					<td colSpan="3" align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				</TD></TR>
				<tr>
					<td><IMG src="../images/appbuttons/minibuttons/4PX.gif"></td>
				</tr>
				<tr>
					<td colSpan="3">
						<table cellSpacing="0" cellPadding="1" width="1040">
							<tr>
								<td class="thdrsinglft" width="22"><IMG border="0" src="../images/appbuttons/minibuttons/eqarch.gif"></td>
								<td class="thdrsingrt label" width="978"><asp:Label id="lang1692" runat="server">Copy Details</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="3">
						<table width="1040">
							<tr>
								<td width="160"></td>
								<td width="180"></td>
								<td width="150"></td>
								<td width="190"></td>
								<td width="170"></td>
								<td width="190"></td>
							</tr>
							<tr>
								<td class="bluelabel"><asp:Label id="lang1693" runat="server">New Search Key</asp:Label></td>
								<td><asp:textbox id="txtnewkey" runat="server" Width="150px" CssClass="plainlabel"></asp:textbox><IMG onclick="getsrch1('key','txtnewkey');" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
								<td class="bluelabel"><asp:Label id="lang1694" runat="server">New Component Name</asp:Label></td>
								<td><asp:textbox id="txtnewcomp" runat="server" Width="160px" CssClass="plainlabel"></asp:textbox><IMG onclick="getsrch1('comp','txtnewcomp');" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
								<td class="bluelabel"><asp:Label id="lang1695" runat="server">New Component Description</asp:Label></td>
								<td><asp:textbox id="txtnewdesc" runat="server" Width="180px" CssClass="plainlabel"></asp:textbox></td>
							</tr>
							<tr>
								<td class="bluelabel" height="15"><asp:Label id="lang1696" runat="server">Department Destination</asp:Label></td>
								<td id="tddept" class="plainlabel"></td>
								<td class="bluelabel"><asp:Label id="lang1697" runat="server">Station\Cell Destination</asp:Label></td>
								<td id="tdcell" class="plainlabel"></td>
								<td class="plainlabelblue" colSpan="2" align="center"><input id="rbmso" onclick="getsite();" value="rbmso" CHECKED type="radio" name="rbc" runat="server"><asp:Label id="lang1698" runat="server">Copy to My Site Only</asp:Label><input id="rbteq" onclick="getsrch();" value="rbteq" type="radio" name="rbc" runat="server"><asp:Label id="lang1699" runat="server">Copy to Function</asp:Label><input id="rbstom" onclick="getstom();" value="rbteq" type="radio" name="rbc" runat="server"><asp:Label id="lang1700" runat="server">Copy to Master</asp:Label></td>
							</tr>
							<tr>
								<td class="bluelabel" height="15"><asp:Label id="lang1701" runat="server">Equipment Destination</asp:Label></td>
								<td id="tdeq" class="plainlabel"></td>
								<td class="bluelabel"><asp:Label id="lang1702" runat="server">Function Destination</asp:Label></td>
								<td id="tdfunc" class="plainlabel"></td>
								<td colSpan="2" align="right"><IMG id="ibtncopy" onclick="copycomp();" src="../images/appbuttons/bgbuttons/copy.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="3">
						<table cellSpacing="0" cellPadding="1" width="1040">
							<tr>
								<td class="thdrsinglft" width="22"><IMG border="0" src="../images/appbuttons/minibuttons/eqarch.gif"></td>
								<td class="thdrsingrt label" width="978"><asp:Label id="lang1703" runat="server">Component Details</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="3"><iframe style="BACKGROUND-COLOR: transparent; WIDTH: 1040px; HEIGHT: 280px" id="ifcodets"
							src="complibcodets.aspx?start=no" frameBorder="no" allowTransparency runat="server"></iframe>
					</td>
				</tr>
				<tr>
					<td colSpan="3">
						<table cellSpacing="0" cellPadding="1" width="1040">
							<tr>
								<td class="thdrsinglft" width="22"><IMG border="0" src="../images/appbuttons/minibuttons/eqarch.gif"></td>
								<td class="thdrsingrt label" width="978"><asp:Label id="lang1704" runat="server">Common Tasks</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="3"><iframe style="BORDER-BOTTOM-STYLE: none; BORDER-RIGHT-STYLE: none; BACKGROUND-COLOR: transparent; BORDER-TOP-STYLE: none; BORDER-LEFT-STYLE: none"
							id="geteq" height="100" src="complibtaskview2.aspx?jump=no" frameBorder="no" width="958" allowTransparency
							scrolling="yes" runat="server"></iframe>
					</td>
				</tr>
			</table>
			</TD></TR></TBODY></TABLE><input id="lblcid" type="hidden" runat="server"> <input id="lblpsid" type="hidden" runat="server">
			<input id="lbldb" type="hidden" runat="server"> <input id="txtpg" type="hidden" name="txtpg" runat="server">
			<input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server"><input id="spdivy" type="hidden" name="spdivy" runat="server">
			<input id="xCoord" type="hidden" runat="server" /><input id="yCoord" type="hidden" runat="server" /> <input id="lblret" type="hidden" runat="server">
			<input id="lblcoid" type="hidden" runat="server"> <input id="lblsid" type="hidden" runat="server">
			<input id="lblcurrtab" type="hidden" runat="server"> <input id="lbluser" type="hidden" runat="server">
			<input id="lbldid" type="hidden" name="lbldid" runat="server"><input id="lblclid" type="hidden" name="lblclid" runat="server">
			<input id="lbleqid" type="hidden" name="lbleqid" runat="server"> <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input id="lblcopytyp" type="hidden" name="lblcopytyp" runat="server"> <input id="lblcomp" type="hidden" runat="server">
			<input id="lbldesc" type="hidden" runat="server"> <input id="lbloloc" type="hidden" runat="server">
			<input id="lblkey" type="hidden" runat="server"> <input id="lblselkey" type="hidden" runat="server">
			<input id="lblcopyret" type="hidden" runat="server"> <input id="lblnewcoid" type="hidden" runat="server">
			<input id="lbldept" type="hidden" runat="server"> <input id="lblcell" type="hidden" runat="server">
			<input id="lbleq" type="hidden" runat="server"> <input id="lblfu" type="hidden" runat="server">
			<input id="lblnocopy" type="hidden" runat="server"> <input id="lblpmadmin" type="hidden" runat="server">
			<input id="lblcadm" type="hidden" runat="server"> <input id="lblcclassid" type="hidden" runat="server">
			<input type="hidden" id="lblfslang" runat="server" />
            <input type="hidden" id="lblcomi" runat="server" />
		</form>
		<uc1:mmenu1 id="Mmenu11" runat="server"></uc1:mmenu1>
	</body>
</HTML>
