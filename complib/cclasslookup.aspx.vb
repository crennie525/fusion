

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class cclasslookup
    Inherits System.Web.UI.Page
	Protected WithEvents lang1660 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1659 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim rl As New Utilities
    Dim Tables As String = "compclass"
    Dim PK As String = "acid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String
    Dim intPgCnt, intPgNav As Integer
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents tbltop As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tbleqrec As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try

        If Not IsPostBack Then
            txtpg.Value = "1"
            rl.Open()
            GetComps()
            rl.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                rl.Open()
                GetNext()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                rl.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetComps()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                rl.Open()
                GetPrev()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                rl.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetComps()
                rl.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetComps()
        Catch ex As Exception
            rl.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr632" , "cclasslookup.aspx.vb")
 
            rl.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetComps()
        Catch ex As Exception
            rl.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr633" , "cclasslookup.aspx.vb")
 
            rl.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetComps()

        Dim srch As String = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        If Len(srch) > 0 Then
            srch = "%" & srch & "%"
            Filter = "assetclass like ''" & srch & "'' or classdesc like ''" & srch & "''"
            FilterCnt = "assetclass like '" & srch & "' or classdesc like '" & srch & "'"
        Else
            Filter = ""
            FilterCnt = ""
        End If
        Dim sb As StringBuilder = New StringBuilder

        sb.Append("<table width=""700"">")
        sb.Append("<tr><td colspan=""6""><div style=""OVERFLOW: auto; width: 700px; height: 360px;"">")
        sb.Append("<table width=""680"">")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""170"">" & tmod.getlbl("cdlbl407" , "cclasslookup.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""270"">" & tmod.getlbl("cdlbl408" , "cclasslookup.aspx.vb") & "</td>")
        Dim rowid As Integer = 0
        If FilterCnt <> "" Then
            sql = "select count(*) from compclass where " & FilterCnt
        Else
            sql = "select count(*) from compclass"
        End If
        PageNumber = txtpg.Value
        intPgCnt = rl.Scalar(sql)
        intPgNav = rl.PageCount(intPgCnt, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav
        dr = rl.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        Dim bc, ci, co, cd, cs, cg As String
        While dr.Read
            If rowid = 0 Then
                rowid = 1
                bc = "white"
            Else
                rowid = 0
                bc = "#E7F1FD"
            End If
            ci = dr.Item("acid").ToString
            co = dr.Item("assetclass").ToString
            cd = dr.Item("classdesc").ToString

            sb.Append("<tr bgcolor=""" & bc & """><td class=""linklabel""><a href=""#"" onclick=""rtret('" & ci & "', '" & co & "');"">")
            sb.Append(co & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & cd & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        dr.Close()
        sb.Append("</table></div></td></tr></table>")
        tdcomp.InnerHtml = sb.ToString
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        rl.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        GetComps()
        rl.Dispose()
    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang1659.Text = axlabs.GetASPXPage("cclasslookup.aspx","lang1659")
		Catch ex As Exception
		End Try
		Try
			lang1660.Text = axlabs.GetASPXPage("cclasslookup.aspx","lang1660")
		Catch ex As Exception
		End Try

	End Sub

End Class
