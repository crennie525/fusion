<%@ Page Language="vb" AutoEventWireup="false" Codebehind="complibtaskeadittpm.aspx.vb" Inherits="lucy_r12.complibtaskeadittpm" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>complibtaskeadittpm</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="javascript" src="../scripts/pmtaskdivfuncedittpmco_1016a_1.js"></script>
		<script language="JavaScript" src="../scripts1/complibtaskeadittpmaspx_1016_1.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
     <!--
         function GetType(app, sval) {
             //alert(app + ", " + sval)
             var typlst
             var sstr
             var ssti
             if (app == "d") {
                 typlst = document.getElementById("ddtype");
                 //ptlst = document.getElementById("ddpt");
                 sstr = typlst.options[typlst.selectedIndex].text;
                 ssti = document.getElementById("ddtype").value;
                 //sstr == "4 - Cond Monitoring"
                 if (ssti == "7") {

                 }
                 else {
                     //ptlst.value="0";

                 }
             }
             else if (app == "o") {
                 document.getElementById("ddtype").value = sval;
                 typlst = document.getElementById("ddtypeo");
                 //ptlst = document.getElementById("ddpt");
                 //ptolst = document.getElementById("ddpto");
                 sstr = typlst.options[typlst.selectedIndex].text;
                 ssti = document.getElementById("ddtypeo").value;
                 if (ssti == "7") {

                 }
                 else {

                 }
             }
         }
         function GetRationale() {
             //handleapp();
             tnum = document.getElementById("lblpg").innerHTML;
             tid = document.getElementById("lbltaskid").value;
             did = document.getElementById("lbldid").value;
             clid = document.getElementById("lblclid").value;
             eqid = document.getElementById("lbleqid").value;
             fuid = document.getElementById("lblfuid").value;
             coid = document.getElementById("lblco").value;
             sav = document.getElementById("lblsave").value;
             ro = document.getElementById("lblro").value;
             if (tid == "") {
                 alert("No Task Records Seleted!")
             }
             else if (sav == "no") {
                 alert("Task Record Is Not Saved!")
             }
             else {
                 var eReturn = window.showModalDialog("../appsopttpm/PMRationaleDialogtpm.aspx?tnum=" + tnum + "&tid=" + tid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:650px; dialogWidth:850px; resizable=yes");
                 if (eReturn) {
                     document.getElementById("form1").submit();
                 }
             }
         }
         //-->
     </script>
	</HEAD>
	<body class="tbg" onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<div class="FreezePaneOff" id="FreezePane" align="center">
				<div class="InnerFreezePane" id="InnerFreezePane"></div>
			</div>
			<div id="overDiv" style="Z-INDEX: 1000; POSITION: absolute; VISIBILITY: hidden"></div>
			<div style="POSITION: absolute; TOP: 0px; LEFT: 0px">
				<table cellSpacing="0" cellPadding="0" width="780">
					<tr>
						<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/3gearsh.gif" border="0"></td>
						<td class="thdrsingrt label" onmouseover="return overlib('Against what is the TPM intended to protect?', ABOVE, LEFT)"
							onmouseout="return nd()" width="674"><asp:Label id="lang1742" runat="server">Failure Modes, Causes And/Or Effects</asp:Label></td>
					</tr>
				</table>
				<table class="view" id="tbeq" cellSpacing="0" cellPadding="1" width="780">
					<tr>
					<tr>
						<td class="label" height="20" width="140"><asp:Label id="lang1743" runat="server">Component Addressed:</asp:Label></td>
						<td id="tdcompnum" class="bluelabellt" width="242" runat="server"><asp:Label id="lang1744" runat="server">Component #</asp:Label></td>
						<td class="label" width="48"></td>
						<td class="label" width="30"><asp:Label id="lang1745" runat="server">Qty:</asp:Label></td>
						<td class="label" width="282"><asp:textbox id="txtcQty" runat="server" CssClass="plainlabel" Width="40px"></asp:textbox></td>
					</tr>
					</tr>
					<tr height="30">
						<td colSpan="7" align="center">
							<table height="30" cellSpacing="0" cellPadding="0" width="650">
								<tr>
									<td width="48">&nbsp;</td>
									<td class="label" align="center" width="180"><asp:Label id="lang1746" runat="server">Component Failure Modes</asp:Label></td>
									<td width="22"></td>
									<td class="redlabel" align="center" width="200"><asp:Label id="lang1747" runat="server">Not Addressed</asp:Label></td>
									<td width="22"></td>
									<td class="bluelabel" align="center" width="180"><asp:Label id="lang1748" runat="server">Selected</asp:Label></td>
									<td width="48">&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td align="center"><asp:listbox id="lbCompFM" runat="server" CssClass="plainlabel" Width="150px" Height="60px" SelectionMode="Multiple"></asp:listbox></td>
									<td><IMG id="btnaddnewfail" runat="server" onmouseover="return overlib('Add a New Failure Mode to the Component Selected Above')"
											onclick="GetFailDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
											width="20"><br>
										<asp:imagebutton id="ibReuse" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><IMG class="details" id="fromreusedis" runat="server" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
											width="20"></td>
									<td align="center"><asp:listbox id="lbfaillist" runat="server" CssClass="plainlabel" Width="150px" Height="60px"
											SelectionMode="Multiple" ForeColor="Red"></asp:listbox></td>
									<td><IMG class="details" id="todis" runat="server" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
											width="20"> <IMG class="details" id="fromdis" runat="server" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
											width="20">
										<asp:imagebutton id="ibToTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><br>
										<asp:imagebutton id="ibFromTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton></td>
									<td align="center"><asp:listbox id="lbfailmodes" runat="server" CssClass="plainlabel" Width="150px" Height="60px"
											SelectionMode="Multiple" ForeColor="Blue"></asp:listbox></td>
									<td>&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table cellSpacing="0" width="780">
					<tr>
						<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/3gearsh.gif" border="0"></td>
						<td class="thdrsingrt label" onmouseover="return overlib('What are you going to do?')"
							onmouseout="return nd()" width="674"><asp:Label id="lang1749" runat="server">Task Activity Details</asp:Label></td>
					</tr>
				</table>
				<table class="view" id="tbdtls" cellSpacing="0" cellPadding="1" width="780">
					<tr>
						<td width="125"></td>
						<td width="310"></td>
						<td width="25"></td>
						<td width="90"></td>
						<td width="60"></td>
						<td width="70"></td>
						<td width="20"></td>
					</tr>
					<tr>
						<td class="label" vAlign="top" rowSpan="3"><asp:Label id="lang1750" runat="server">Task Description</asp:Label></td>
						<td vAlign="top" rowSpan="3"><asp:textbox id="txtdesc" Width="300px" TextMode="MultiLine" MaxLength="1000" Rows="3" Runat="server"
								cssclass="plainlabel"></asp:textbox></td>
						<td width="25"><IMG id="btnlookup" onclick="tasklookup();" src="../images/appbuttons/minibuttons/lookupgbg.gif"
								runat="server"></td>
						<td class="label" width="90"><asp:Label id="lang1751" runat="server">Task Type</asp:Label></td>
						<td colSpan="3"><asp:listbox id="ddtype" runat="server" CssClass="plainlabel" Width="160px" Rows="1" DataTextField="tasktype"
								DataValueField="ttid"></asp:listbox></td>
					</tr>
					<tr>
						<td><IMG id="btnlookup2" onclick="jumpto();" src="../images/appbuttons/minibuttons/magnifier.gif"
								runat="server"></td>
						<td class="label" align="right" colSpan="3"><asp:Label id="lang1752" runat="server">Measurements Required?</asp:Label></td>
						<td><IMG onmouseover="return overlib('Add/Edit Measurements for this Task')" onclick="getMeasDiv();"
								onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/measure.gif"
								width="27"></td>
					</tr>
				</table>
				<table cellSpacing="0" width="780">
					<tr>
						<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/3gearsh.gif" border="0"></td>
						<td class="thdrsingrt label" onmouseover="return overlib('How are you going to do it?')"
							onmouseout="return nd()" width="674" colSpan="3"><asp:Label id="lang1753" runat="server">Planning &amp; Scheduling Details</asp:Label></td>
					</tr>
				</table>
				<table class="view" id="tbreqs" cellSpacing="0" cellPadding="0" width="780">
					<tr>
						<td colSpan="3">
							<table cellSpacing="1" cellPadding="1" width="780">
								<tr>
									<td class="label" width="100"><input type="checkbox" id="cbcust" runat="server" NAME="Checkbox1" onclick="checkcust('r');"><asp:Label id="lang1754" runat="server">Use Custom?</asp:Label></td>
									<td class="label" width="60"><asp:Label id="lang1755" runat="server">Frequency</asp:Label></td>
									<td width="90"><asp:textbox id="txtfreq" runat="server" CssClass="plainlabel" Width="80px"></asp:textbox></td>
									<td class="label" width="70"><asp:Label id="lang1756" runat="server">P-F Interval</asp:Label></td>
									<td width="50"><asp:textbox id="txtpfint" runat="server" CssClass="plainlabel" Width="40px"></asp:textbox></td>
									<td class="label" width="30"><asp:Label id="lang1757" runat="server">Days</asp:Label></td>
									<td align="left" width="60"><IMG id="btnpfint" onmouseover="return overlib('Estimate Frequency using the PF Interval')"
											onclick="getPFDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/lilcalc.gif"
											width="20"></td>
									<td class="label" width="100"><asp:Label id="lang1758" runat="server">Operator Qty</asp:Label></td>
									<td width="20"><asp:textbox id="txtqty" runat="server" CssClass="plainlabel" Width="30px"></asp:textbox></td>
									<td class="label" width="40"><asp:Label id="lang1759" runat="server">Min Ea</asp:Label></td>
									<td width="90"><asp:textbox id="txttr" runat="server" CssClass="plainlabel" Width="35px"></asp:textbox></td>
								</tr>
								<tr>
									<td colSpan="7" rowSpan="3">
										<table cellSpacing="1" cellPadding="1">
											<tr>
												<td class="label" onmouseover="return overlib('Use to indicate shift - Clears and Disables any Day Enries')"
													onmouseout="return nd()" align="center"><asp:Label id="lang1760" runat="server">Shift</asp:Label></td>
												<td class="label" onmouseover="return overlib('Selects All Days for Selected Shift')"
													onmouseout="return nd()" align="center"><asp:Label id="lang1761" runat="server">All</asp:Label></td>
												<td class="label"></td>
												<td class="label" align="center" width="23">M</td>
												<td class="label" align="center" width="23">Tu</td>
												<td class="label" align="center" width="23">W</td>
												<td class="label" align="center" width="23">Th</td>
												<td class="label" align="center" width="23">F</td>
												<td class="label" align="center" width="23">Sa</td>
												<td class="label" align="center" width="23">Su</td>
											</tr>
											<tr>
												<td><INPUT id="cb1o" type="checkbox" runat="server" NAME="cb1o"></td>
												<td><INPUT id="cb1" type="checkbox" runat="server" NAME="cb1"></td>
												<td class="label">1st Shift</td>
												<td align="center"><INPUT id="cb1mon" type="checkbox" name="Checkbox1" runat="server"></td>
												<td align="center"><INPUT id="cb1tue" type="checkbox" name="Checkbox2" runat="server"></td>
												<td align="center"><INPUT id="cb1wed" type="checkbox" name="Checkbox3" runat="server"></td>
												<td align="center"><INPUT id="cb1thu" type="checkbox" name="Checkbox4" runat="server"></td>
												<td align="center"><INPUT id="cb1fri" type="checkbox" name="Checkbox5" runat="server"></td>
												<td align="center"><INPUT id="cb1sat" type="checkbox" name="Checkbox6" runat="server"></td>
												<td align="center"><INPUT id="cb1sun" type="checkbox" name="Checkbox7" runat="server"></td>
											</tr>
											<tr>
												<td><INPUT id="cb2o" type="checkbox" runat="server" NAME="cb2o"></td>
												<td><INPUT id="cb2" type="checkbox" name="Checkbox1" runat="server"></td>
												<td class="label">2nd Shift</td>
												<td align="center"><INPUT id="cb2mon" type="checkbox" name="Checkbox1" runat="server"></td>
												<td align="center"><INPUT id="cb2tue" type="checkbox" name="Checkbox2" runat="server"></td>
												<td align="center"><INPUT id="cb2wed" type="checkbox" name="Checkbox3" runat="server"></td>
												<td align="center"><INPUT id="cb2thu" type="checkbox" name="Checkbox4" runat="server"></td>
												<td align="center"><INPUT id="cb2fri" type="checkbox" name="Checkbox5" runat="server"></td>
												<td align="center"><INPUT id="cb2sat" type="checkbox" name="Checkbox6" runat="server"></td>
												<td align="center"><INPUT id="cb2sun" type="checkbox" name="Checkbox7" runat="server"></td>
											</tr>
											<tr>
												<td><INPUT id="cb3o" type="checkbox" runat="server" NAME="cb3o"></td>
												<td><INPUT id="cb3" type="checkbox" name="Checkbox1" runat="server"></td>
												<td class="label">3rd Shift</td>
												<td align="center"><INPUT id="cb3mon" type="checkbox" name="Checkbox1" runat="server"></td>
												<td align="center"><INPUT id="cb3tue" type="checkbox" name="Checkbox2" runat="server"></td>
												<td align="center"><INPUT id="cb3wed" type="checkbox" name="Checkbox3" runat="server"></td>
												<td align="center"><INPUT id="cb3thu" type="checkbox" name="Checkbox4" runat="server"></td>
												<td align="center"><INPUT id="cb3fri" type="checkbox" name="Checkbox5" runat="server"></td>
												<td align="center"><INPUT id="cb3sat" type="checkbox" name="Checkbox6" runat="server"></td>
												<td align="center"><INPUT id="cb3sun" type="checkbox" name="Checkbox7" runat="server"></td>
											</tr>
										</table>
									</td>
									<td class="label"><asp:Label id="lang1762" runat="server">Equipment Status</asp:Label></td>
									<td colSpan="3"><asp:dropdownlist id="ddeqstat" runat="server" CssClass="plainlabel" Width="90px"></asp:dropdownlist></td>
								</tr>
								<tr>
									<td class="label"><asp:Label id="lang1763" runat="server">Down Time</asp:Label></td>
									<td><asp:textbox id="txtrdt" runat="server" CssClass="plainlabel" Width="30px"></asp:textbox></td>
									<td class="label" colSpan="2"><asp:checkbox id="cbloto" runat="server"></asp:checkbox>LOTO&nbsp;&nbsp;<asp:checkbox id="cbcs" runat="server"></asp:checkbox>CS
									</td>
								</tr>
								<tr>
									<td colSpan="5" align="right"><IMG id="img1" onmouseover="return overlib('Add/Edit Parts for this Task')" onmouseout="return nd()"
											onclick="GetPartDiv();" alt="" src="../images/appbuttons/minibuttons/parttrans.gif" width="23" height="19"
											runat="server"> <IMG onmouseover="return overlib('Add/Edit Tools for this Task')" onmouseout="return nd()"
											onclick="GetToolDiv();" alt="" src="../images/appbuttons/minibuttons/tooltrans.gif" width="23" height="19">
										<IMG onmouseover="return overlib('Add/Edit Lubricants for this Task')" onmouseout="return nd()"
											onclick="GetLubeDiv();" alt="" src="../images/appbuttons/minibuttons/lubetrans.gif"
											width="23" height="19">
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="0" width="780">
					<tr id="trh4" class="view" runat="server">
						<td style="BORDER-TOP: #7ba4e0 thin solid" width="20" align="center"><IMG id="btnStart" src="../images/appbuttons/minibuttons/tostartbg.gif" width="20" height="20"
								runat="server">
						</td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" width="20" align="center"><IMG id="btnPrev" src="../images/appbuttons/minibuttons/prevarrowbg.gif" width="20" height="20"
								runat="server">
						</td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" class="bluelabel" width="110" align="center"><asp:Label id="lang1764" runat="server">Task#</asp:Label><asp:label id="lblpg" runat="server" CssClass="bluelabel" ForeColor="Blue" Font-Bold="True"
								Font-Names="Arial" Font-Size="X-Small"></asp:label>&nbsp;of&nbsp;<asp:label id="lblcnt" runat="server" CssClass="bluelabel" ForeColor="Blue" Font-Bold="True"
								Font-Names="Arial" Font-Size="X-Small"></asp:label></td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" width="20" align="center"><IMG id="btnNext" src="../images/appbuttons/minibuttons/nextarrowbg.gif" width="20" height="20"
								runat="server">
						</td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" width="30" align="center"><IMG id="btnEnd" src="../images/appbuttons/minibuttons/tolastbg.gif" width="20" height="20"
								runat="server">
						</td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" class="label" width="70" align="center"><asp:Label id="lang1765" runat="server">Task Order</asp:Label></td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" class="label" width="40"><asp:textbox id="txttaskorder" runat="server" CssClass="plainlabel" Width="35px" Enabled="False"></asp:textbox></td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" class="greenlabel" width="150" align="left"><asp:label id="Label26" runat="server" CssClass="greenlabel" Font-Bold="True" Font-Names="Arial"
								Font-Size="X-Small"># of Sub Tasks: </asp:label><asp:label id="lblsubcount" runat="server" CssClass="greenlabel" Font-Bold="True" Font-Names="Arial"
								Font-Size="X-Small"></asp:label></td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" width="260" align="right"><IMG id="btnedittask" onclick="doEnable();" border="0" alt="" src="../images/appbuttons/minibuttons/lilpentrans.gif"
								width="19" height="19" runat="server"><asp:imagebutton id="btnaddtsk" runat="server" ImageUrl="../images/appbuttons/minibuttons/addnewbg1.gif"
								BorderStyle="None"></asp:imagebutton>
							<asp:imagebutton id="btndeltask" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"></asp:imagebutton><asp:imagebutton id="ibCancel" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"></asp:imagebutton><IMG id="btnsav" onclick="valpgnums();" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif"
								width="20" height="20" runat="server"><IMG id="sgrid" onmouseover="return overlib('Add/Edit Sub Tasks')" onmouseout="return nd()"
								onclick="getsgrid();" alt="" src="../images/appbuttons/minibuttons/sgrid1.gif" width="20" height="20" runat="server"></td>
					</tr>
					<tr>
						<td align="center" colSpan="10"><asp:label id="msglbl" runat="server" CssClass="redlabel" Width="360px" ForeColor="Red" Font-Size="X-Small"
								Font-Names="Arial" Font-Bold="True"></asp:label></td>
					</tr>
				</table>
			</div>
			<div class="details" id="lstdiv" style="Z-INDEX: 999; BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 630px; HEIGHT: 620px; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid">
				<table cellSpacing="0" cellPadding="0" width="630" bgcolor="white">
					<tr bgColor="blue" height="20">
						<td class="labelwht"><asp:Label id="lang1766" runat="server">List Dialog</asp:Label></td>
						<td align="right"><IMG onclick="closelst();" height="18" alt="" src="../images/close.gif" width="18"><br>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><iframe id="iflst" style="WIDTH: 630px; HEIGHT: 620px" runat="server"></iframe>
						</td>
					</tr>
				</table>
			</div>
			<input id="lblsid" type="hidden" name="lblsid" runat="server"><input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
			<input id="lblcid" type="hidden" name="lblcid" runat="server"><input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
			<input id="lbldid" type="hidden" name="lbldid" runat="server"><input id="lblclid" type="hidden" name="lblclid" runat="server">
			<input id="lbleqid" type="hidden" name="lbleqid" runat="server"><input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input id="lblcoid" type="hidden" name="lblcoid" runat="server"><input id="lblfilt" type="hidden" name="lblfilt" runat="server">
			<input id="lblptid" type="hidden" name="lblptid" runat="server"><input id="lblpar" type="hidden" name="lblpar" runat="server">
			<input id="lblchk" type="hidden" name="lblchk" runat="server"><input id="lblco" type="hidden" name="lblco" runat="server">
			<input id="lblfail" type="hidden" name="lblfail" runat="server"><input id="lblsb" type="hidden" name="lblsb" runat="server">
			<input id="lblpgholder" type="hidden" name="lblpgholder" runat="server"><input id="lblt" type="hidden" name="lblt" runat="server">
			<input id="lblst" type="hidden" name="lblst" runat="server"><input id="lblsvchk" type="hidden" name="lblsvchk" runat="server">
			<input id="lblenable" type="hidden" name="lblenable" runat="server"><input id="lblcompchk" type="hidden" name="lblcompchk" runat="server">
			<input id="lblcompfailchk" type="hidden" name="lblcompfailchk" runat="server"><input id="lblstart" type="hidden" name="lblstart" runat="server">
			<input id="lblcurrsb" type="hidden" name="lblcurrsb" runat="server"><input id="lblcurrcs" type="hidden" name="lblcurrcs" runat="server">
			<input id="appchk" type="hidden" name="appchk" runat="server"><input id="lbllog" type="hidden" name="lbllog" runat="server">
			<input id="pgflag" type="hidden" name="pgflag" runat="server"><input id="lblfiltcnt" type="hidden" name="lblfiltcnt" runat="server">
			<input id="lot" type="hidden" name="lot" runat="server"><input id="cs" type="hidden" name="cs" runat="server">
			<input id="lblusername" type="hidden" name="lblusername" runat="server"> <input id="lbllock" type="hidden" name="lbllock" runat="server">
			<input id="lbllockedby" type="hidden" name="lbllockedby" runat="server"> <input id="lblhaspm" type="hidden" name="lblhaspm" runat="server">
			<input id="lbltyp" type="hidden" name="lbltyp" runat="server"> <input id="lbllid" type="hidden" name="lbllocid" runat="server">
			<input id="lblro" type="hidden" runat="server" NAME="lblro"><input type="hidden" id="lblnoeq" runat="server" NAME="lblnoeq">
			<input type="hidden" id="lblfreqprev" runat="server" NAME="lblfreqprev"><input type="hidden" id="lblcbcust" runat="server" NAME="lblcbcust">
			<INPUT id="lcb1o" type="hidden" runat="server" NAME="lcb1o"> <INPUT id="lcb1" type="hidden" runat="server" NAME="lcb1">
			<INPUT id="lcb1mon" type="hidden" runat="server" NAME="lcb1mon"> <INPUT id="lcb1tue" type="hidden" runat="server" NAME="lcb1tue">
			<INPUT id="lcb1wed" type="hidden" runat="server" NAME="lcb1wed"> <INPUT id="lcb1thu" type="hidden" runat="server" NAME="lcb1thu">
			<INPUT id="lcb1fri" type="hidden" runat="server" NAME="lcb1fri"> <INPUT id="lcb1sat" type="hidden" runat="server" NAME="lcb1sat">
			<INPUT id="lcb1sun" type="hidden" runat="server" NAME="lcb1sun"> <INPUT id="lcb2o" type="hidden" runat="server" NAME="lcb2o">
			<INPUT id="lcb2" type="hidden" runat="server" NAME="lcb2"> <INPUT id="lcb2mon" type="hidden" runat="server" NAME="lcb2mon">
			<INPUT id="lcb2tue" type="hidden" runat="server" NAME="lcb2tue"> <INPUT id="lcb2wed" type="hidden" runat="server" NAME="lcb2wed">
			<INPUT id="lcb2thu" type="hidden" runat="server" NAME="lcb2thu"> <INPUT id="lcb2fri" type="hidden" runat="server" NAME="lcb2fri">
			<INPUT id="lcb2sat" type="hidden" runat="server" NAME="lcb2sat"> <INPUT id="lcb2sun" type="hidden" runat="server" NAME="lcb2sun">
			<INPUT id="lcb3o" type="hidden" runat="server" NAME="lcb3o"> <INPUT id="lcb3" type="hidden" runat="server" NAME="lcb3">
			<INPUT id="lcb3mon" type="hidden" runat="server" NAME="lcb3mon"> <INPUT id="lcb3tue" type="hidden" runat="server" NAME="lcb3tue">
			<INPUT id="lcb3wed" type="hidden" runat="server" NAME="lcb3wed"> <INPUT id="lcb3thu" type="hidden" runat="server" NAME="lcb3thu">
			<INPUT id="lcb3fri" type="hidden" runat="server" NAME="lcb3fri"> <INPUT id="lcb3sat" type="hidden" runat="server" NAME="lcb3sat">
			<INPUT id="lcb3sun" type="hidden" runat="server" NAME="lcb3sun"> <input type="hidden" id="lblcompnum" runat="server">
			<input type="hidden" id="lbltpm" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
