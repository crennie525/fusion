

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class complibsrchkey
    Inherits System.Web.UI.Page
	Protected WithEvents lang1741 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Login, ro, who, srch As String
    Dim sql As String
    Dim rep As New Utilities
    Protected WithEvents taskdiv As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tdtitle As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Sort As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdlookup As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            'Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try

        If Not IsPostBack Then
            Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                    ro = "0"
                End Try
                If ro = "1" Then
                    'pgtitle.InnerHtml = "Task Lookup (Read Only)"
                Else
                    'pgtitle.InnerHtml = "Task Lookup"
                End If
            Catch ex As Exception

            End Try
            who = Request.QueryString("who").ToString
            lblwho.Value = who
            If who = "key" Then
                tdtitle.InnerHtml = "Current Search Keys"
            Else
                tdtitle.InnerHtml = "Current Common Component Names"
            End If
            txtpg.Value = "1"
            rep.Open()
            GetSrchList()
            rep.Dispose()
        Else
                If Request.Form("lblret") = "next" Then
                    rep.Open()
                    GetNext()
                    rep.Dispose()
                    lblret.Value = ""
                ElseIf Request.Form("lblret") = "last" Then
                    rep.Open()
                    PageNumber = txtpgcnt.Value
                    txtpg.Value = PageNumber
                    GetSrchList()
                    rep.Dispose()
                    lblret.Value = ""
                ElseIf Request.Form("lblret") = "prev" Then
                    rep.Open()
                    GetPrev()
                    rep.Dispose()
                    lblret.Value = ""
                ElseIf Request.Form("lblret") = "first" Then
                    rep.Open()
                    PageNumber = 1
                    txtpg.Value = PageNumber
                    GetSrchList()
                    rep.Dispose()
                    lblret.Value = ""
                End If

                End If
    End Sub
    Private Sub GetSrchList()

        PageNumber = txtpg.Value

        who = lblwho.Value
        srch = txtsrch.Text
        If who = "key" Then
            If txtsrch.Text <> "" Then
                Filter = "compkey like ''%" & srch & "%''"
                FilterCnt = "compkey like '%" & srch & "%'"
            End If
            If Len(Filter) <> 0 Then
                sql = "select Count(distinct compkey) from complib where " & FilterCnt
            Else
                sql = "select Count(distinct compkey) from complib"
            End If
            Tables = "complib"
            PK = "comid"
            PageSize = "200"
            Fields = "distinct(compkey)"
            Sort = "compkey"
        Else
            If txtsrch.Text <> "" Then
                Filter = "compnum like ''%" & srch & "%''"
                FilterCnt = "compnum like '%" & srch & "%'"
            End If
            If Len(Filter) <> 0 Then
                sql = "select Count(distinct compnum) from complib where " & FilterCnt
            Else
                sql = "select Count(distinct compnum) from complib"
            End If
            Tables = "complib"
            PK = "comid"
            PageSize = "200"
            Fields = "distinct(compnum)"
            Sort = "compnum"
        End If


        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""100%"">")


        Dim dc As Integer = rep.PageCount(sql, PageSize)
        If dc = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & dc
        End If
        txtpgcnt.Value = dc

        dr = rep.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        While dr.Read
            If who = "key" Then
                sb.Append("<tr><td class=""suggest""><a href=""#"" class=""A1"" onclick=""handleexit1(this.innerHTML);"">" & dr("compkey").ToString() & "</a></td></tr>")
            Else
                sb.Append("<tr><td class=""suggest""><a href=""#"" class=""A1"" onclick=""handleexit1(this.innerHTML);"">" & dr("compnum").ToString() & "</a></td></tr>")
            End If

        End While
        dr.Close()
        rep.Dispose()
        sb.Append("</Table>")
        taskdiv.InnerHtml = sb.ToString
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetSrchList()
        Catch ex As Exception
            rep.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr684" , "complibsrchkey.aspx.vb")
 
            rep.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetSrchList()
        Catch ex As Exception
            rep.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr685" , "complibsrchkey.aspx.vb")
 
            rep.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        rep.Open()
        GetSrchList()
        rep.Dispose()

    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang1741.Text = axlabs.GetASPXPage("complibsrchkey.aspx","lang1741")
		Catch ex As Exception
		End Try

	End Sub

End Class
