

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class complibtasks2
    Inherits System.Web.UI.Page
    Protected WithEvents ovid226 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid225 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid224 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid223 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents ovid222 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid221 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents ovid220 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents btnpfint As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang1789 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1788 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1787 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1786 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1785 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1784 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1783 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1782 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1781 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1780 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1779 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1778 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1777 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1776 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1775 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1774 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1773 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1772 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1771 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1770 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1769 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1768 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1767 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tl, cid, sid, did, clid, eqid, fuid, coid, sql, val, name, field, ttid, tnum, typ, lid, appstr As String
    Dim co, fail, chk, tpmhold As String
    Dim tskcnt As Integer
    Dim tasks As New Utilities
    Dim tasksadd As New Utilities
    Dim taskssav As New Utilities
    Public dr As SqlDataReader
    Dim ds As DataSet
    Dim Tables As String = "comptasks"
    Dim PK As String = "pmtskid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 1
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim CntFilter As String = ""
    Dim TaskFilter As String = ""
    Dim Group As String = ""
    Dim Sort As String = "tasknum, subtask asc"
    Dim strScript, login, username, ro As String
    Dim haspm As Integer
    Dim tasknum As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtcQty As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbCompFM As System.Web.UI.WebControls.ListBox
    Protected WithEvents ibReuse As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfaillist As System.Web.UI.WebControls.ListBox
    Protected WithEvents ibToTask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibFromTask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents txtdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddtype As System.Web.UI.WebControls.ListBox
    Protected WithEvents ddpt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtqty As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttr As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpfint As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddeqstat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtrdt As System.Web.UI.WebControls.TextBox
    Protected WithEvents cbloto As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cbcs As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtfreq As System.Web.UI.WebControls.TextBox
    Protected WithEvents imgClock As System.Web.UI.WebControls.Image
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents lblcnt As System.Web.UI.WebControls.Label
    Protected WithEvents txttaskorder As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label26 As System.Web.UI.WebControls.Label
    Protected WithEvents lblsubcount As System.Web.UI.WebControls.Label
    Protected WithEvents btnaddtsk As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btndeltask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibCancel As System.Web.UI.WebControls.ImageButton
    Protected WithEvents msglbl As System.Web.UI.WebControls.Label
    Protected WithEvents btnaddnewfail As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromreusedis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnlookup As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnlookup2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdtpmhold As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img5 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents trh4 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents btnStart As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnPrev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnNext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnEnd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnedittask As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnsav As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents sgrid As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iflst As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblptid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfail As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpgholder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblst As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsvchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblenable As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrsb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lot As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhaspm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnoeq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltpmalert As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltpmhold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdcompnum As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblssidx As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbfixed As System.Web.UI.HtmlControls.HtmlInputCheckBox

    Protected WithEvents lblfixed As System.Web.UI.HtmlControls.HtmlInputHidden
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If lbllog.Value <> "no" Then
            'Dim ftr As Footer
            'ftr = Page.FindControl("Footer1")
            'ftr.loc = Footer.eloc.divpg
            If lblsvchk.Value = "1" Then
                goNext()
            ElseIf lblsvchk.Value = "2" Then
                goPrev()
            ElseIf lblsvchk.Value = "3" Then
                'goSubNext()
            ElseIf lblsvchk.Value = "4" Then
                'goSubPrev()
            ElseIf lblsvchk.Value = "5" Then
                GoFirst()
            ElseIf lblsvchk.Value = "6" Then
                GoLast()
            ElseIf lblsvchk.Value = "7" Then
                GoNav()
            End If
            If Request.Form("lblcompchk") = "3" Then
                lblcompchk.Value = "0"
                tasksadd.Open()
                SaveTask2()
                tasksadd.Dispose()
            ElseIf Request.Form("lblcompchk") = "4" Then
                lblcompchk.Value = "0"
                tasks.Open()
                GetLists()
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "5" Then
                lblcompchk.Value = "0"
                tasks.Open()
                SubCount(PageNumber, Filter)
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "6" Then
                lblcompchk.Value = "0"
                tasks.Open()
                'LoadCommon()
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            End If

            Dim start As String = Request.QueryString("start").ToString
            If start = "no" Then
                lblpg.Text = "0"
                lblcnt.Text = "0"
                lblsubcount.Text = "0"
            End If
            If Not IsPostBack Then
                If start = "yes" Then
                    appstr = HttpContext.Current.Session("appstr").ToString()
                    CheckApps(appstr)
                    lblcnt.Text = "0"
                    lblsvchk.Value = "0"
                    lblenable.Value = "1"
                    coid = Request.QueryString("coid").ToString
                    lblcoid.Value = coid
                    Dim comp As String = Request.QueryString("comp").ToString
                    tdcompnum.InnerHtml = comp
                    sid = HttpContext.Current.Session("dfltps").ToString
                   
                    lblsid.Value = sid
                    tasks.Open()

                    GetLists()

                    lblsb.Value = "0"
                    Filter = "comid = " & coid & " and subtask = 0"
                    CntFilter = "comid = " & coid & " and subtask = 0"
                    lblfilt.Value = Filter
                    lblfiltcnt.Value = CntFilter

                    LoadPage(PageNumber, Filter)
                    btnaddtsk.Enabled = True
                    Dim lock, lockby As String
                    Dim user As String = lblusername.Value

                    'Read Only
                    Try
                        ro = HttpContext.Current.Session("ro").ToString
                    Catch ex As Exception
                        ro = "0"
                    End Try
                    lblro.Value = ro
                    'End Read Only


                    If ro = "1" Then
                        ibToTask.Visible = False
                        ibFromTask.Visible = False
                        ibReuse.Visible = False
                        todis.Attributes.Add("class", "view")
                        fromdis.Attributes.Add("class", "view")
                        fromreusedis.Attributes.Add("class", "view")
                        btnedittask.Attributes.Add("src", "../images/appbuttons/minibuttons/lilpendis.gif")
                        btnaddtsk.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                        btnaddtsk.Enabled = False
                        btndeltask.Attributes.Add("src", "../images/appbuttons/minibuttons/deldis.gif")
                        btndeltask.Enabled = False
                        ibCancel.Attributes.Add("src", "../images/appbuttons/minibuttons/candisk1dis.gif")
                        ibCancel.Enabled = False
                        btnsav.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                        btnsav.Attributes.Add("onclick", "")
                    End If
                    tasks.Dispose()

                Else
                    btnaddtsk.Enabled = False
                End If


                'Disable all fields
                If ro <> "1" Then
                    txttaskorder.Enabled = False
                    'ddcomp.Enabled = False
                    ''lbfaillist.Enabled = False
                    ''lbfailmodes.Enabled = False
                    ibToTask.Enabled = False
                    ibFromTask.Enabled = False
                    txtdesc.Enabled = False
                    ddtype.Enabled = False
                    txtfreq.Enabled = False

                    cbfixed.Disabled = True

                    txtpfint.Enabled = False
                    ddskill.Enabled = False
                    txtqty.Enabled = False
                    txttr.Enabled = False
                    txtrdt.Enabled = False
                    ddpt.Enabled = False
                    ddeqstat.Enabled = False
                    'cbloto.Enabled = False
                    'cbcs.Enabled = False
                    lblenable.Value = "1"
                    txtcQty.Enabled = False
                    'lbCompFM.Enabled = False
                    'buttons

                    'btnaddsubtask.Enabled = False
                    btndeltask.Enabled = False
                    ibCancel.Enabled = False
                    'btnsavetask.Enabled = False
                    'btntpm.Enabled = False
                    'end disable
                End If

                'btnsavetask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov193" , "complibtasks2.aspx.vb") & "')")
                'btnsavetask.Attributes.Add("onmouseout", "return nd()")
                'btntpm.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov194" , "complibtasks2.aspx.vb") & "')")
                'btntpm.Attributes.Add("onmouseout", "return nd()")
                btndeltask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov195", "complibtasks2.aspx.vb") & "')")
                btndeltask.Attributes.Add("onmouseout", "return nd()")
                'btnaddsubtask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov196" , "complibtasks2.aspx.vb") & "')")
                'btnaddsubtask.Attributes.Add("onmouseout", "return nd()")
                btnaddtsk.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov197", "complibtasks2.aspx.vb") & "')")
                btnaddtsk.Attributes.Add("onmouseout", "return nd()")
                btnaddtsk.Attributes.Add("onclick", "FreezeScreen('Your Data is Being Processed...');")
                btnedittask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov198", "complibtasks2.aspx.vb") & "')")
                btnedittask.Attributes.Add("onmouseout", "return nd()")
                ibToTask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov199", "complibtasks2.aspx.vb") & "')")
                ibToTask.Attributes.Add("onmouseout", "return nd()")
                'ibToTask.Attributes.Add("onclick", "DisableButton(this);")
                ibReuse.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov200", "complibtasks2.aspx.vb") & "')")
                ibReuse.Attributes.Add("onmouseout", "return nd()")
                'ibReuse.Attributes.Add("onclick", "DisableButton(this);")
                ibCancel.Attributes.Add("onmouseout", "return nd()")
                ibCancel.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov201", "complibtasks2.aspx.vb") & "')")
                ibFromTask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov202", "complibtasks2.aspx.vb") & "')")
                ibFromTask.Attributes.Add("onmouseout", "return nd()")
                'ibFromTask.Attributes.Add("onclick", "DisableButton(this);")
                btnPrev.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov203", "complibtasks2.aspx.vb") & "')")
                btnPrev.Attributes.Add("onmouseout", "return nd()")
                btnPrev.Attributes.Add("onclick", "confirmExit('tb', 'dev');")
                btnStart.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov204", "complibtasks2.aspx.vb") & "')")
                btnStart.Attributes.Add("onmouseout", "return nd()")
                btnStart.Attributes.Add("onclick", "confirmExit('ts', 'dev');")
                btnNext.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov205", "complibtasks2.aspx.vb") & "')")
                btnNext.Attributes.Add("onmouseout", "return nd()")
                btnNext.Attributes.Add("onclick", "confirmExit('tf', 'dev');")
                btnEnd.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov206", "complibtasks2.aspx.vb") & "')")
                btnEnd.Attributes.Add("onmouseout", "return nd()")
                btnEnd.Attributes.Add("onclick", "confirmExit('te', 'dev');")
                btnlookup.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov207", "complibtasks2.aspx.vb") & "')")
                btnlookup.Attributes.Add("onmouseout", "return nd()")
                btnlookup2.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov208", "complibtasks2.aspx.vb") & "')")
                btnlookup2.Attributes.Add("onmouseout", "return nd()")
                'ddcomp.Attributes.Add("onchange", "chngchk();")
                ddtype.Attributes.Add("onchange", "GetType('d', this.value);")
                txttr.Attributes.Add("onkeyup", "filldown(this.value);")
                ddeqstat.Attributes.Add("onchange", "zerodt(this.value);")
                'tasks.Dispose()
            End If
        End If
    End Sub
    Private Sub CheckApps(ByVal appstr As String)
        Dim apparr() As String = appstr.Split(",")
        Dim e As String = "0"
        Dim t As String = "0"
        'eq,dev,opt,inv
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                If apparr(i) = "eq" Then
                    e = "1"
                End If
                If apparr(i) = "tpd" Or apparr(i) = "tpo" Then
                    t = "1"
                End If
            Next
            If e <> "1" Then
                lblnoeq.Value = "1"
                'btnaddcomp.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                'btnaddcomp.Attributes.Add("onclick", "")
                btnaddnewfail.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                btnaddnewfail.Attributes.Add("onclick", "")
                'imgcopycomp.Attributes.Add("onclick", "")
            End If
            If t <> "1" Then
                lblnoeq.Value = "1"
                'btntpm.Attributes.Add("src", "../images/appbuttons/minibuttons/compresstpmdis.gif")
                'btntpm.Enabled = False
                'imgdeltpm.Attributes.Add("src", "../images/appbuttons/minibuttons/cantpmdis.gif")
                'imgdeltpm.Attributes.Add("onclick", "")
            End If
        Else
            lblnoeq.Value = "0"
        End If

    End Sub
    Private Sub GoNav()
        Dim pg As String = pgflag.Value
        If pg <> "0" Then
            Filter = lblfilt.Value
            lblsb.Value = "0"
            PageNumber = pg
            tasks.Open()
            LoadPage(PageNumber, Filter)
            tasks.Dispose()
            lblenable.Value = "1"
        End If
    End Sub
    Private Function SubCount(ByVal PageNumber As Integer, ByVal Filter As String) As Integer
        Dim scnt As Integer
        coid = lblcoid.Value
        sql = "select count(*) from compTasks where comid = '" & coid & "' and tasknum = " & PageNumber & " and subtask <> '0'"
        Try
            scnt = tasks.Scalar(sql)
        Catch ex As Exception
            scnt = tasksadd.Scalar(sql)
        End Try
        lblsubcount.Text = scnt
        Return scnt
    End Function
    Private Sub LoadPage(ByVal PageNumber As Integer, ByVal Filter As String)
        '**** Added for PM Manager
        eqid = lbleqid.Value
        'Try
        'lblhaspm.Value = tasks.HasPM(eqid)
        'Catch ex As Exception
        'lblhaspm.Value = tasksadd.HasPM(eqid)
        'End Try


        If lblsb.Value = "0" Then
            Dim scnt As Integer
            scnt = SubCount(PageNumber, Filter)
            lblsubcount.Text = scnt
            lblpgholder.Value = PageNumber
            cbloto.Checked = False
            cbcs.Checked = False
        End If
        Dim tskcnt, optcnt As Integer
        tskcnt = 0 'lblcnt.Text
        If tskcnt = 0 Then
            CntFilter = lblfiltcnt.Value
            sql = "select count(*) from comptasks where " & CntFilter
            Try
                tskcnt = tasks.Scalar(sql)
            Catch ex As Exception
                tskcnt = tasksadd.Scalar(sql)
            End Try
        End If
        Filter = Filter
        lblcnt.Text = tskcnt
        lblpg.Text = PageNumber

        If tskcnt = 0 Then
            lblpg.Text = "0"
            lblcnt.Text = tskcnt
            Dim strMessage As String = tmod.getmsg("cdstr700", "complibtasks2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            'sql = "select tasknum from pmtasks where " & Filter & " order by tasknum"
            'Try
            'dr = tasks.GetRdrData(sql)
            'Catch ex As Exception
            'dr = tasksadd.GetRdrData(sql)
            'End Try

            'ddgoto.DataSource = dr
            'ddgoto.DataValueField = "tasknum"
            'ddgoto.DataTextField = "tasknum"
            'ddgoto.DataBind()
            'ddgoto.Items.Insert(0, New ListItem("GoTo"))
            'dr.Close()
            Tables = "comptasks"
            PK = "pmtskid"
            fuid = lblfuid.Value
            Dim fixed As String
            Try
                dr = tasks.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            Catch ex As Exception
                dr = tasksadd.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            End Try
            Dim test As String
            test = lbltaskid.Value
            If dr.Read Then
                fixed = dr.Item("fixed").ToString

                lbltpmhold.Value = dr.Item("tpmhold").ToString
                txtpfint.Text = dr.Item("pfInterval").ToString
                lblt.Value = dr.Item("tasknum").ToString
                txttaskorder.Text = dr.Item("tasknum").ToString
                lbloldtask.Value = dr.Item("tasknum").ToString
                txttaskorder.Text = dr.Item("tasknum").ToString
                ttid = dr.Item("pmtskid").ToString
                lbltaskid.Value = ttid
                lblst.Value = dr.Item("subtask").ToString
                Try
                    ddtype.SelectedValue = dr.Item("ttid").ToString
                Catch ex As Exception
                    ddtype.SelectedIndex = 0
                End Try
                lblsstr.Value = dr.Item("tasktype").ToString
                lblssidx.Value = dr.Item("ttid").ToString

                txtfreq.Text = dr.Item("freq").ToString
                lblfreq.Value = dr.Item("freq").ToString
                Try
                    ddpt.SelectedValue = dr.Item("ptid").ToString
                Catch ex As Exception
                    ddpt.SelectedIndex = 0
                End Try
                Try
                    ddskill.SelectedValue = dr.Item("skillid").ToString
                Catch ex As Exception
                    ddskill.SelectedIndex = 0
                End Try
                Try
                    ddeqstat.SelectedValue = dr.Item("rdid").ToString
                Catch ex As Exception
                    ddeqstat.SelectedIndex = 0
                End Try
                Dim desc As String = dr.Item("taskdesc").ToString
                desc = Replace(desc, "&#180;", "'")
                txtdesc.Text = desc

                txtqty.Text = dr.Item("qty").ToString
                txttr.Text = dr.Item("tTime").ToString
                txtrdt.Text = dr.Item("rdt").ToString

                If dr.Item("lotoid").ToString = "1" Then
                    cbloto.Checked = True
                Else
                    cbloto.Checked = False
                End If
                If dr.Item("conid").ToString = "1" Then
                    cbcs.Checked = True
                Else
                    cbcs.Checked = False
                End If
                co = dr.Item("comid").ToString
                If Len(co) = 0 Or co = "" Then
                    co = "0"
                End If
                lblcoid.Value = co
                txtcQty.Text = dr.Item("cqty").ToString
                If txtcQty.Text = "" Then
                    txtcQty.Text = "1"
                End If
                'lblhaspm.Value = dr.Item("haspm").ToString
            End If
            dr.Close()
            lblfixed.Value = fixed

            If fixed = "1" Then
                cbfixed.Checked = True
            Else
                cbfixed.Checked = False
            End If

            If lbltpmhold.Value = "1" Then
                tdtpmhold.InnerHtml = "(TPM Task)"
                lbltpmalert.Value = "no"
                'imgdeltpm.Attributes.Add("class", "visible")
                'btntpm.Visible = False
            Else '
                lbltpmhold.Value = "0"
                tdtpmhold.InnerHtml = ""
                'lbltpmalert.Value = ""
                'imgdeltpm.Attributes.Add("class", "details")
                'btntpm.Visible = True
            End If
            If co <> "0" Then
                lblco.Value = co
                Try
                    'ddcomp.SelectedValue = co
                Catch ex As Exception

                End Try
                PopCompFailList(co)
                PopFailList(co)
                PopTaskFailModes(co)
                'UpdateFailStats(co)
                lblco.Value = co
                chk = "comp"
            Else
                Try
                    'ddcomp.SelectedValue = co
                Catch ex As Exception

                End Try
                lbfaillist.Items.Clear()
                lbfailmodes.Items.Clear()
                lbCompFM.Items.Clear()
                lblco.Value = co
                chk = "comp"
            End If
            lblpar.Value = "comp"
            'UpDateRevs(ttid)
            'If lblsb.Value = "0" Then
            'lblpg.Text = PageNumber
            'Else
            'lblpg.Text = lblpgholder.Value
            'PageNumber = lblpgholder.Value
            'If Len(Filter) = 0 Then
            'Filter = " subtask = 0"
            'Else
            'Filter = Filter & " and subtask = 0"
            'End If
            'sql = "select count(*) from pmtasks where " & Filter
            'Try
            'tskcnt = tasks.Scalar(sql)
            'Catch ex As Exception
            'tskcnt = tasksadd.Scalar(sql)
            'End Try

            'tdtasknav.InnerHtml = "Page# " & PageNumber & " of " & tskcnt
            'End If
            lblcnt.Text = tskcnt
            'CheckPgNav(tskcnt, PageNumber)
            'btnaddsubtask.Enabled = True

        End If
    End Sub
    Private Function SubTaskCnt(ByVal PageNumber As Integer, ByVal Filter As String) As Integer
        Dim tcnt As Integer
        CntFilter = lblfiltcnt.Value
        sql = "select count(*) from compTasks where " & CntFilter & " and subtask = '0'"
        tcnt = tasks.Scalar(sql)
        Dim scnt As Integer
        sql = "select count(*) from compTasks where " & CntFilter & " and subtask <> '0'"
        scnt = tasks.Scalar(sql)
        Dim wcnt = tcnt + scnt
        Return wcnt
    End Function

    Private Sub GoFirst()
        Filter = lblfilt.Value
        lblsb.Value = "0"
        PageNumber = 1
        'Filter = Filter & " and tasknum = " & PageNumber
        tasks.Open()
        LoadPage(PageNumber, Filter)
        tasks.Dispose()
        lblenable.Value = "1"
    End Sub
    Private Sub GoLast()
        Filter = lblfilt.Value
        lblsb.Value = "0"
        PageNumber = lblcnt.Text
        'Filter = Filter & " and tasknum = " & PageNumber
        tasks.Open()
        LoadPage(PageNumber, Filter)
        tasks.Dispose()
        lblenable.Value = "1"
    End Sub
    Private Sub goNext()
        Filter = lblfilt.Value
        If lblsb.Value = "0" Then
            PageNumber = lblpg.Text
        Else
            PageNumber = lblpgholder.Value
        End If

        lblsb.Value = "0"
        PageNumber = PageNumber + 1
        'Filter = Filter & " and tasknum = " & PageNumber
        If PageNumber <= lblcnt.Text Then
            tasks.Open()
            LoadPage(PageNumber, Filter)
            tasks.Dispose()
        Else

            Dim strMessage As String = tmod.getmsg("cdstr701", "complibtasks2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        lblenable.Value = "1"
    End Sub
    Private Sub goPrev()
        Filter = lblfilt.Value
        'If lblsb.Value = "0" Or lblspg.Text = "1" Then
        'PageNumber = lblpg.Text
        'Else
        PageNumber = lblpgholder.Value
        'End If
        lblsb.Value = "0"
        PageNumber = lblpg.Text
        If PageNumber > 1 Then
            If PageNumber = 1 Then
                fuid = lblfuid.Value
                coid = lblcoid.Value
                Dim tcnt As Integer
                sql = "select count(*) from comptasks where comid = '" & coid & "' and tasknum = '0'"
                Dim task0 As New Utilities
                task0.Open()
                tcnt = task0.Scalar(sql)
                task0.Dispose()
                If tcnt = 1 Then
                    Try
                        PageNumber = PageNumber - 1
                        'Filter = Filter & " and tasknum = " & PageNumber
                        tasks.Open()
                        LoadPage(PageNumber, Filter)
                    Catch ex As Exception
                        Dim strMessage As String = tmod.getmsg("cdstr702", "complibtasks2.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        Exit Sub
                    End Try
                End If
            Else
                Try
                    PageNumber = PageNumber - 1
                    'Filter = Filter & " and tasknum = " & PageNumber
                    tasks.Open()
                    LoadPage(PageNumber, Filter)
                Catch ex As Exception
                    Dim strMessage As String = tmod.getmsg("cdstr703", "complibtasks2.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If

            'ElseIf lblspg.Text = "1" Then
            'PageNumber = PageNumber
            'Filter = Filter & " and tasknum = " & PageNumber
            'LoadPage(PageNumber, Filter)
        Else
            Dim strMessage As String = tmod.getmsg("cdstr704", "complibtasks2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        lblenable.Value = "1"
        tasks.Dispose()
    End Sub

    Private Sub UpDateRevs(ByVal Filter As String)
        sql = "select created, revised from comptasks where pmtskid = '" & Filter & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        Dim cr, rv As String
        While dr.Read
            cr = dr.Item("created").ToString
            rv = dr.Item("revised").ToString
        End While
        dr.Close()
        If Len(rv) = 0 Then
            rv = "NA"
        End If
        'imgClock.Attributes.Add("onmouseover", "return overlib('Task Created: " & cr & "<br> Task Revised: " & rv & "')")
        'imgClock.Attributes.Add("onmouseout", "return nd()")
    End Sub
    Private Sub CheckPgNav(ByVal tskcnt As Integer, ByVal PageNumber As Integer)
        If PageNumber = 1 Or tskcnt = 1 Then
            'btnPrev.Enabled = False
        Else
            'btnPrev.Enabled = True
        End If
        If PageNumber < tskcnt Then
            'btnNext.Enabled = True
        Else
            'btnNext.Enabled = False
        End If
    End Sub

    Private Sub GetLists()
        cid = "0" 'lblcid.Value

        sql = "select ttid, tasktype " _
        + "from pmTaskTypes where compid = '" & cid & "' or compid = '0' and LTRIM(RTRIM(tasktype)) <> 'Select' order by compid"
        dr = tasks.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataBind()
        dr.Close()
        ddtype.Items.Insert(0, New ListItem("Select"))
        ddtype.Items(0).Value = 0

        sid = lblsid.Value
        Dim scnt As Integer
        sql = "select count(*) from pmSiteSkills where siteid = '" & sid & "'"
        scnt = tasks.Scalar(sql)
        If scnt <> 0 Then
            sql = "select skillid, skill " _
            + "from pmSiteSkills where compid = '" & cid & "' and siteid = '" & sid & "'"
        Else
            sql = "select skillid, skill " _
            + "from pmSkills where compid = '" & cid & "' order by compid"
        End If
        dr = tasks.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("Select"))
        ddskill.Items(0).Value = 0

        sql = "select ptid, pretech " _
        + "from pmPreTech where compid = '" & cid & "' or compid = '0' order by compid"
        dr = tasks.GetRdrData(sql)
        ddpt.DataSource = dr
        ddpt.DataTextField = "pretech"
        ddpt.DataValueField = "ptid"
        ddpt.DataBind()
        dr.Close()
        ddpt.Items.Insert(0, New ListItem("None"))
        ddpt.Items(0).Value = 0

        sql = "select statid, status " _
        + "from pmStatus where compid = '" & cid & "' or compid = '0' order by compid"
        dr = tasks.GetRdrData(sql)
        ddeqstat.DataSource = dr
        ddeqstat.DataTextField = "status"
        ddeqstat.DataValueField = "statid"
        ddeqstat.DataBind()
        dr.Close()
        ddeqstat.Items.Insert(0, New ListItem("Select"))
        ddeqstat.Items(0).Value = 0
        'PopComp()
    End Sub

    Private Sub btnaddtsk_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnaddtsk.Click
        lblenable.Value = "0"
        tl = lbltasklev.Value
        cid = "0" 'lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        coid = lblcoid.Value
        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        tl = 5
        tasksadd.Open()

        sql = "usp_AddCoTask '" & cid & "', '" & coid & "', '" & usr & "'"
        tasksadd.Scalar(sql)
        Filter = lblfilt.Value

        PageNumber = lblcnt.Text
        PageNumber = PageNumber + 1
        lblcnt.Text = PageNumber
        field = "comid"
        val = coid
        Filter = field & " = " & val & " and subtask = 0"  '& " and tasknum = " & PageNumber

        LoadPage(PageNumber, Filter)
        tasksadd.Dispose()
    End Sub

    Private Sub btnsavetask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub
    Private Sub SaveTask2()
        Dim tn, otn As String
        tn = txttaskorder.Text
        otn = lblt.Value
        If tn = "0" Or Len(tn) = 0 Then
            tn = otn
        End If
        Dim tnchk As Long
        Try

            tnchk = System.Convert.ToInt64(tn)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr705", "complibtasks2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        Dim lst As Integer = lblcnt.Text
        Try
            If tnchk > lst Then
                tn = lst
            End If
        Catch ex As Exception

        End Try
        If tn < 1 Then
            tn = 1
        End If
        ro = lblro.Value
        If ro <> "1" Then
            Dim pf, t, st, tid, typ, fre, des, rmt, pt, ski, qty, tr, eqs, lot, cs, ci, cn, cin As String
            Dim typstr, frestr, rmtstr, ptstr, skistr, eqsstr, cqty, rdt, fixed As String
            Dim fm As String
            Dim Item As ListItem
            Dim f, fi As String
            Dim ipar As Integer = 0
            For Each Item In lbfailmodes.Items
                f = Item.Text.ToString
                ipar = f.LastIndexOf("(")
                If ipar <> -1 Then
                    f = Mid(f, 1, ipar)
                End If
                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
            Next
            fm = tasks.ModString2(fm)
            t = lblt.Value
            st = lblsb.Value

            typstr = lblsstr.Value
            typ = lblssidx.Value

            'Dim dtst As String = ddtype.SelectedIndex
            'If ddtype.SelectedIndex = 0 Then
            'typ = "0"
            'Else
            'typ = ddtype.SelectedValue
            'End If
            'typstr = ddtype.SelectedItem.ToString
            typstr = Replace(typstr, "'", Chr(180), , , vbTextCompare)

            fre = "0"

            frestr = txtfreq.Text
            frestr = Replace(frestr, "'", Chr(180), , , vbTextCompare)
            des = txtdesc.Text
            des = tasks.ModString2(des)

            rdt = txtrdt.Text

            qty = txtqty.Text
            Dim qtychk As Long
            Try
                qtychk = System.Convert.ToInt32(qty)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr706", "complibtasks2.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            If Len(qty) = 0 Then
                qty = "1"
            End If

            tr = txttr.Text
            If Len(tr) = 0 Then
                tr = "0"
            End If

            If ddeqstat.SelectedIndex = 0 Then
                eqs = "0"
            Else
                eqs = ddeqstat.SelectedValue
            End If
            eqsstr = ddeqstat.SelectedItem.ToString
            eqsstr = Replace(eqsstr, "'", Chr(180), , , vbTextCompare)
            If cbloto.Checked = True Then
                lot = "1"
            Else
                lot = "0"
            End If

            If cbcs.Checked = True Then
                cs = "1"
            Else
                cs = "0"
            End If

            tid = lbltaskid.Value
            Filter = lblfilt.Value
            'If ddcomp.SelectedIndex = 0 Then
            coid = lblcoid.Value
            'cin = "0"
            'Else
            'ci = ddcomp.SelectedValue.ToString
            'End If
            cn = tdcompnum.InnerHtml
            fuid = lblfuid.Value

            cqty = txtcQty.Text
            Dim cqtychk As Long
            Try
                cqtychk = System.Convert.ToInt32(cqty)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr707", "complibtasks2.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try

            pf = txtpfint.Text

            If cin Is Nothing Then
                cin = "0"
            End If

            'cn = ddcomp.SelectedItem.ToString
            cn = tasks.ModString2(cn)

            tl = lbltasklev.Value
            cid = lblcid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            eqid = lbleqid.Value
            fuid = lblfuid.Value

            Dim ttid As String = lbltaskid.Value

            sid = lblsid.Value

            PageNumber = lblpg.Text
            fuid = lblfuid.Value

            Dim usr As String = HttpContext.Current.Session("username").ToString()
            Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
            eqid = lbleqid.Value
            Dim hpm As String = lblhaspm.Value

            If ddpt.SelectedIndex = 0 Then
                pt = "0"
            Else
                pt = ddpt.SelectedValue
            End If
            ptstr = ddpt.SelectedItem.ToString
            ptstr = Replace(ptstr, "'", Chr(180), , , vbTextCompare)
            If ddskill.SelectedIndex = 0 Then
                ski = "0"
            Else
                ski = ddskill.SelectedValue
            End If
            skistr = ddskill.SelectedItem.ToString
            skistr = Replace(skistr, "'", Chr(180), , , vbTextCompare)
            fixed = lblfixed.Value
            If cbfixed.Checked = True Then
                fixed = "1"
            End If

            If Len(ttid) <> 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = "exec usp_updatecomplibtasks @pmtskid, @des, @qty, @ttime, @rdid, @rd, " _
            + "@fre, @freq, @lotoid, @conid, @ttid, @tasktype, @comid, " _
            + "@compnum, @fm1, @cqty, @pfinterval, @rdt, @filter, " _
            + "@pagenumber, @fuid, " _
            + "@ustr, @eqid, @sid, @hpm, @pt, @ptstr, @ski, @skistr"

                Dim param = New SqlParameter("@pmtskid", SqlDbType.VarChar)
                param.Value = ttid
                cmd.Parameters.Add(param)
                Dim param01 = New SqlParameter("@des", SqlDbType.VarChar)
                If des = "" Then
                    param01.Value = System.DBNull.Value
                Else
                    param01.Value = des
                End If
                cmd.Parameters.Add(param01)
                Dim param02 = New SqlParameter("@qty", SqlDbType.VarChar)
                If qty = "" Then
                    param02.Value = "1"
                Else
                    param02.Value = qty
                End If
                cmd.Parameters.Add(param02)
                Dim param03 = New SqlParameter("@ttime", SqlDbType.Decimal)
                If tr = "" Then
                    param03.Value = "0"
                Else
                    param03.Value = tr
                End If
                cmd.Parameters.Add(param03)
                Dim param04 = New SqlParameter("@rdid", SqlDbType.VarChar)
                If eqs = "" Then
                    param04.Value = "0"
                Else
                    param04.Value = eqs
                End If
                cmd.Parameters.Add(param04)
                Dim param05 = New SqlParameter("@rd", SqlDbType.VarChar)
                If eqsstr = "" Then
                    param05.Value = System.DBNull.Value
                Else
                    param05.Value = eqsstr
                End If
                cmd.Parameters.Add(param05)
                Dim param06 = New SqlParameter("@fre", SqlDbType.VarChar)
                If fre = "" Then
                    param06.Value = "0"
                Else
                    param06.Value = fre
                End If
                cmd.Parameters.Add(param06)
                Dim param07 = New SqlParameter("@freq", SqlDbType.VarChar)
                If frestr = "" Or frestr = "Select" Then
                    param07.Value = System.DBNull.Value
                Else
                    param07.Value = frestr
                End If
                cmd.Parameters.Add(param07)
                Dim param08 = New SqlParameter("@lotoid", SqlDbType.VarChar)
                If lot = "" Then
                    param08.Value = "0"
                Else
                    param08.Value = lot
                End If
                cmd.Parameters.Add(param08)
                Dim param09 = New SqlParameter("@conid", SqlDbType.VarChar)
                If cs = "" Then
                    param09.Value = "0"
                Else
                    param09.Value = cs
                End If
                cmd.Parameters.Add(param09)
                Dim param10 = New SqlParameter("@ttid", SqlDbType.VarChar)
                If typ = "" Then
                    param10.Value = "0"
                Else
                    param10.Value = typ
                End If
                cmd.Parameters.Add(param10)
                Dim param11 = New SqlParameter("@tasktype", SqlDbType.VarChar)
                If typstr = "" Or typstr = "Select" Then
                    param11.Value = System.DBNull.Value
                Else
                    param11.Value = typstr
                End If
                cmd.Parameters.Add(param11)
                Dim param12 = New SqlParameter("@comid", SqlDbType.VarChar)
                If coid = "" Or coid = "Select" Then
                    param12.Value = "0"
                Else
                    param12.Value = coid
                End If
                cmd.Parameters.Add(param12)
                Dim param13 = New SqlParameter("@compnum", SqlDbType.VarChar)
                If cn = "" Or cn = "Select" Then
                    param13.Value = System.DBNull.Value
                Else
                    param13.Value = cn
                End If
                cmd.Parameters.Add(param13)
                Dim param14 = New SqlParameter("@fm1", SqlDbType.VarChar)
                If fm = "" Then
                    param14.Value = System.DBNull.Value
                Else
                    param14.Value = fm
                End If
                cmd.Parameters.Add(param14)
                Dim param15 = New SqlParameter("@cqty", SqlDbType.VarChar)
                If cqty = "" Then
                    param15.Value = "1"
                Else
                    param15.Value = cqty
                End If
                cmd.Parameters.Add(param15)
                Dim param16 = New SqlParameter("@pfinterval", SqlDbType.VarChar)
                If pf = "" Then
                    param16.Value = System.DBNull.Value
                Else
                    param16.Value = pf
                End If
                cmd.Parameters.Add(param16)
                Dim param17 = New SqlParameter("@rdt", SqlDbType.Decimal)
                If rdt = "" Then
                    param17.Value = "0"
                Else
                    param17.Value = rdt
                End If
                cmd.Parameters.Add(param17)
                Dim param18 = New SqlParameter("@filter", SqlDbType.VarChar)
                If Filter = "" Then
                    param18.Value = System.DBNull.Value
                Else
                    param18.Value = Filter
                End If
                cmd.Parameters.Add(param18)
                Dim param19 = New SqlParameter("@pagenumber", SqlDbType.VarChar)
                param19.Value = lblt.Value
                cmd.Parameters.Add(param19)
                Dim param20 = New SqlParameter("@fuid", SqlDbType.VarChar)
                If fuid = "" Then
                    param20.Value = "0"
                Else
                    param20.Value = fuid
                End If
                cmd.Parameters.Add(param20)

                Dim param48 = New SqlParameter("@ustr", SqlDbType.VarChar)
                If ustr = "" Then
                    param48.Value = System.DBNull.Value
                Else
                    param48.Value = ustr
                End If
                cmd.Parameters.Add(param48)
                Dim param49 = New SqlParameter("@eqid", SqlDbType.VarChar)
                param49.Value = eqid
                cmd.Parameters.Add(param49)
                Dim param50 = New SqlParameter("@sid", SqlDbType.VarChar)
                param50.Value = sid
                cmd.Parameters.Add(param50)

                Dim param89 = New SqlParameter("@hpm", SqlDbType.VarChar)
                If hpm = "" Then
                    param89.Value = "0"
                Else
                    param89.Value = "0"
                End If
                cmd.Parameters.Add(param89)

                '@pt, @ptstr, @ski, @skistr
                Dim param21 = New SqlParameter("@pt", SqlDbType.VarChar)
                If pt = "" Then
                    param21.Value = "0"
                Else
                    param21.Value = pt
                End If
                cmd.Parameters.Add(param21)
                Dim param22 = New SqlParameter("@ptstr", SqlDbType.VarChar)
                If ptstr = "" Or ptstr = "Select" Then
                    param22.Value = System.DBNull.Value
                Else
                    param22.Value = ptstr
                End If
                cmd.Parameters.Add(param22)

                Dim param23 = New SqlParameter("@ski", SqlDbType.VarChar)
                If ski = "" Then
                    param23.Value = "0"
                Else
                    param23.Value = ski
                End If
                cmd.Parameters.Add(param23)
                Dim param24 = New SqlParameter("@skistr", SqlDbType.VarChar)
                If skistr = "" Or ptstr = "Select" Then
                    param24.Value = System.DBNull.Value
                Else
                    param24.Value = skistr
                End If
                cmd.Parameters.Add(param24)

                'Dim param30 = New SqlParameter("@fixed", SqlDbType.VarChar)
                'If fixed = "" Then
                'param30.Value = System.DBNull.Value
                'Else
                'param30.Value = fixed
                'End If
                'cmd.Parameters.Add(param30)

                Dim tpm As Integer
                Try
                    tpm = tasks.ScalarHack(cmd)
                Catch ex As Exception
                    tpm = tasksadd.ScalarHack(cmd)
                End Try

                If tpm = 1 Then
                    lbltpmalert.Value = "yes"
                Else
                    lbltpmalert.Value = "no"
                End If

                If otn <> tn Then
                    coid = lblcoid.Value
                    sql = "usp_reorderCompTasks '" & ttid & "', '" & coid & "', '" & tn & "', '" & otn & "'"
                    Try
                        tasks.Update(sql)
                    Catch ex As Exception
                        tasksadd.Update(sql)
                    End Try

                End If

                Dim currcnt As String = lblcurrsb.Value
                Dim cscnt As String = lblcurrcs.Value
                If st = 0 Then
                    If otn <> tn Then
                        PageNumber = tn
                    Else
                        PageNumber = lblpg.Text
                    End If

                Else
                    PageNumber = currcnt + cscnt
                End If
                'question this
                field = "pmtasks.funcid"
                val = fuid
                Filter = "comid = " & coid & " and subtask = 0"
                'end question
                LoadPage(PageNumber, Filter)
                lblenable.Value = "1"
            End If
        End If

    End Sub
    Private Sub PopFailList(ByVal comp As String)
        ttid = lbltaskid.Value
        Dim lang As String = lblfslang.Value
        Dim coi As String = lblcomi.Value
        'sql = "select count(*) " _
        '+ "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
        '+ "select failid from pmtaskfailmodes where comid = '" & comp & "' " _
        '+ "and parentfailid is not null)" ' and taskid = '" & ttid & "')"
        'Dim ercnt As Integer
        'ercnt = tasks.Scalar(sql)
        'If ercnt = 0 Then
        sql = "select compfailid, failuremode " _
        + "from complibfm where comid = '" & comp & "' and compfailid not in (" _
        + "select failid from comptaskfailmodes where comid = '" & comp & "')" ' and taskid = '" & ttid & "')"
        sql = "usp_getcfall_tsknaco '" & comp & "','" & lang & "'"
        'Else
        'sql = "select compfailid, failuremode " _
        '+ "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
        '+ "select failid from pmtaskfailmodes where comid = '" & comp & "' " _
        '+ "and parentfailid is null)" ' and taskid = '" & ttid & "')"
        'End If

        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try
        lbfaillist.DataSource = dr
        lbfaillist.DataTextField = "failuremode"
        lbfaillist.DataValueField = "compfailid"
        lbfaillist.DataBind()
        dr.Close()
    End Sub

    Private Sub PopCompFailList(ByVal comp As String)
        ttid = lbltaskid.Value
        Dim lang As String = lblfslang.Value
        Dim coi As String = lblcomi.Value
        sql = "select compfailid, failuremode " _
         + "from complibfm where comid = '" & comp & "'"
        sql = "usp_getcfall_coco '" & comp & "','" & lang & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        lbCompFM.DataSource = dr
        lbCompFM.DataTextField = "failuremode"
        lbCompFM.DataValueField = "compfailid"
        lbCompFM.DataBind()
        dr.Close()
    End Sub
    Private Sub PopTaskFailModes(ByVal comp As String)
        ttid = lbltaskid.Value
        Dim lang As String = lblfslang.Value
        Dim coi As String = lblcomi.Value
        'sql = "select * from pmtaskfailmodes where taskid = '" & ttid & "'"
        sql = "select * from comptaskfailmodes where comid = '" & comp & "' and taskid = '" & ttid & "'"
        sql = "usp_getcfall_tskco '" & comp & "','" & ttid & "','" & lang & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try
        lbfailmodes.DataSource = dr
        lbfailmodes.DataTextField = "failuremode"
        lbfailmodes.DataValueField = "failid"
        lbfailmodes.DataBind()
        dr.Close()
    End Sub
    Private Sub GetItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Dim fcnt, fcnt2 As Integer
        If oaid <> "0" Then
            sql = "select count(*) from compTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and oaid = '" & oaid & "'"
        Else
            sql = "select count(*) from compTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "' and  (oaid is null or oaid = '0')"
        End If
        'sql = "select count(*) from compTaskFailModes where taskid = '" & ttid & "' and failid = '" & fi & "'"
        fcnt = tasks.Scalar(sql)
        sql = "select count(*) from compTaskFailModes where taskid = '" & ttid & "'"
        fcnt2 = tasks.Scalar(sql)
        If fcnt2 >= 5 Then
            Dim strMessage As String = tmod.getmsg("cdstr708", "complibtasks2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf fcnt > 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr709", "complibtasks2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Try
                sql = "usp_addCompTaskFailureMode " & ttid & ", " & fi & ", '" & f & "', '" & comp & "', 'dev','" & oaid & "'"
                tasks.Update(sql)
                Dim Item As ListItem
                Dim fm As String
                Dim ipar As Integer = 0
                For Each Item In lbfailmodes.Items
                    f = Item.Text.ToString
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                    If Len(fm) = 0 Then
                        fm = f & "(___)"
                    Else
                        fm += " " & f & "(___)"
                    End If
                Next
                fm = tasks.ModString2(fm)
                sql = "update complibtasks set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
                tasks.Update(sql)
                eqid = lbleqid.Value
                tasks.UpMod(eqid)
            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Sub RemItems(ByVal f As String, ByVal fi As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Try
            sql = "usp_delCompTaskFailureMode '" & ttid & "', '" & fi & "'" ','" & oaid & "'"
            tasks.Update(sql)
            Dim fm As String
            Dim ipar As Integer = 0
            Dim Item As ListItem
            For Each Item In lbfailmodes.Items
                f = Item.Text.ToString
                ipar = f.LastIndexOf("(")
                If ipar <> -1 Then
                    f = Mid(f, 1, ipar)
                End If
                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
            Next
            fm = tasks.ModString2(fm)
            sql = "update comptasks set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
            tasks.Update(sql)
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub UpdateFailStats(ByVal comp As String)
        Dim fmcnt, fmcnta As Integer
        sql = "select count(*) from complibfm where comid = '" & comp & "'"
        Try
            fmcnt = tasks.Scalar(sql)
        Catch ex As Exception
            fmcnt = tasksadd.Scalar(sql)
        End Try
        sql = "select count(distinct failid) from compTaskFailModes where comid = '" & comp & "'"
        Try
            fmcnta = tasks.Scalar(sql)
        Catch ex As Exception
            fmcnta = tasksadd.Scalar(sql)
        End Try

        fmcnta = fmcnt - fmcnta
    End Sub

    Private Sub ibToTask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibToTask.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        Dim comp As String = lblcoid.Value 'ddcomp.SelectedValue.ToString
        tasks.Open()
        'SaveTask()
        'lblenable.Value = "0"
        For Each Item In lbfaillist.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            'UpdateFM()
            lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            Filter = lblfilt.Value
            'LoadPage(PageNumber, Filter)
            lblenable.Value = "0"
        Catch ex As Exception
        End Try
        tasks.Dispose()

    End Sub

    Private Sub ibReuse_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibReuse.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        Dim comp As String = lblcoid.Value 'ddcomp.SelectedValue.ToString
        tasks.Open()
        'SaveTask()
        'lblenable.Value = "0"
        For Each Item In lbCompFM.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            Filter = lblfilt.Value
            'LoadPage(PageNumber, Filter)
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
            lblenable.Value = "0"
        Catch ex As Exception

        End Try
        tasks.Dispose()
    End Sub

    Private Sub ibFromTask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibFromTask.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        Dim comp As String = lblcoid.Value 'ddcomp.SelectedValue.ToString
        tasks.Open()
        'SaveTask()
        'lblenable.Value = "0"
        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                RemItems(f, fi, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            'UpdateFM()
            lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            Filter = lblfilt.Value
            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub

    Private Sub btndeltask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btndeltask.Click
        PageNumber = lblpg.Text
        Filter = lblfilt.Value
        coid = lblcoid.Value
        tnum = lblt.Value
        Dim st As Integer = lblsb.Value
        sql = "usp_delCompLibTask '" & coid & "', '" & tnum & "', '" & st & "'"
        tasks.Open()
        tasks.Update(sql)
        tskcnt = lblcnt.Text
        tskcnt = tskcnt - 1
        lblcnt.Text = tskcnt
        If PageNumber <= tskcnt Then
            PageNumber = PageNumber
        Else
            PageNumber = tskcnt
        End If
        If PageNumber = 0 Then
            PageNumber = 1
        End If
        eqid = lbleqid.Value
        tasks.UpMod(eqid)

        LoadPage(PageNumber, Filter)
        tasks.Dispose()
    End Sub

    Private Sub ibCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibCancel.Click
        tasks.Open()
        GetLists()
        Filter = lblfilt.Value
        LoadPage(PageNumber, Filter)
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label26.Text = axlabs.GetASPXPage("complibtasks2.aspx", "Label26")
        Catch ex As Exception
        End Try
        Try
            lang1767.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1767")
        Catch ex As Exception
        End Try
        Try
            lang1768.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1768")
        Catch ex As Exception
        End Try
        Try
            lang1769.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1769")
        Catch ex As Exception
        End Try
        Try
            lang1770.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1770")
        Catch ex As Exception
        End Try
        Try
            lang1771.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1771")
        Catch ex As Exception
        End Try
        Try
            lang1772.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1772")
        Catch ex As Exception
        End Try
        Try
            lang1773.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1773")
        Catch ex As Exception
        End Try
        Try
            lang1774.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1774")
        Catch ex As Exception
        End Try
        Try
            lang1775.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1775")
        Catch ex As Exception
        End Try
        Try
            lang1776.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1776")
        Catch ex As Exception
        End Try
        Try
            lang1777.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1777")
        Catch ex As Exception
        End Try
        Try
            lang1778.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1778")
        Catch ex As Exception
        End Try
        Try
            lang1779.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1779")
        Catch ex As Exception
        End Try
        Try
            lang1780.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1780")
        Catch ex As Exception
        End Try
        Try
            lang1781.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1781")
        Catch ex As Exception
        End Try
        Try
            lang1782.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1782")
        Catch ex As Exception
        End Try
        Try
            lang1783.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1783")
        Catch ex As Exception
        End Try
        Try
            lang1784.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1784")
        Catch ex As Exception
        End Try
        Try
            lang1785.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1785")
        Catch ex As Exception
        End Try
        Try
            lang1786.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1786")
        Catch ex As Exception
        End Try
        Try
            lang1787.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1787")
        Catch ex As Exception
        End Try
        Try
            lang1788.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1788")
        Catch ex As Exception
        End Try
        Try
            lang1789.Text = axlabs.GetASPXPage("complibtasks2.aspx", "lang1789")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnpfint.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complibtasks2.aspx", "btnpfint") & "')")
            btnpfint.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            img1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complibtasks2.aspx", "img1") & "')")
            img1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid220.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complibtasks2.aspx", "ovid220") & "', ABOVE, LEFT)")
            ovid220.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid221.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complibtasks2.aspx", "ovid221") & "')")
            ovid221.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid222.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complibtasks2.aspx", "ovid222") & "')")
            ovid222.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid223.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complibtasks2.aspx", "ovid223") & "')")
            ovid223.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid224.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complibtasks2.aspx", "ovid224") & "')")
            ovid224.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid225.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complibtasks2.aspx", "ovid225") & "')")
            ovid225.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid226.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complibtasks2.aspx", "ovid226") & "')")
            ovid226.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            sgrid.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complibtasks2.aspx", "sgrid") & "')")
            sgrid.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
