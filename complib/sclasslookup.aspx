<%@ Page Language="vb" AutoEventWireup="false" Codebehind="sclasslookup.aspx.vb" Inherits="lucy_r12.sclasslookup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>sclasslookup</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="JavaScript" src="../scripts1/sclasslookupaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table id="tbltop" width="650" runat="server">
				<tr>
					<td align="right" colSpan="3"><img id="bgbreturn" runat="server" src="../images/appbuttons/bgbuttons/return.gif" onclick="handleexit();"></td>
				</tr>
				<tr>
					<td class="thdrsingrt label" colSpan="3"><asp:Label id="lang1875" runat="server">Sub Class Lookup Dialog</asp:Label></td>
				</tr>
				<tr id="tbleqrec" runat="server">
					<td class="bluelabel" width="80"><asp:Label id="lang1876" runat="server">Search</asp:Label></td>
					<td width="310"><asp:textbox id="txtsrch" runat="server" Width="250px"></asp:textbox><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
					<td width="210"></td>
				</tr>
				<tr>
					<td id="tdcomp" align="center" colSpan="3" runat="server"></td>
				</tr>
				<tr>
					<td align="center" colSpan="6">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="thdrsingrt label" colSpan="3"><asp:Label id="lang1877" runat="server">Current Component Class Details</asp:Label></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang1878" runat="server">Component Class</asp:Label></td>
					<td class="plainlabel" id="tdcclass" runat="server" colspan="2" height="20"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang1879" runat="server">Description</asp:Label></td>
					<td class="plainlabel" id="tdcdesc" runat="server" colspan="2" height="20"></td>
				</tr>
				<tr>
					<td><IMG src="../images/appbuttons/minibuttons/4PX.gif"></td>
				</tr>
				<tr>
					<td class="btmmenu plainlabel" bgColor="#1d11ef" height="20" colSpan="3"><asp:Label id="lang1880" runat="server">Component Sub Classes</asp:Label></td>
				</tr>
				<tr>
					<td colspan="3"><IMG src="../images/appbuttons/minibuttons/4PX.gif"></td>
				</tr>
				<tr>
					<td id="tdchildren" colSpan="3" runat="server"></td>
				</tr>
			</table>
			<input id="lblret" type="hidden" name="lblret" runat="server"> <input id="txtpg" type="hidden" name="txtpg" runat="server">
			<input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server"> <input type="hidden" id="lblacid" runat="server">
			<input type="hidden" id="lblcclass" runat="server"> <input type="hidden" id="lblsubmit" runat="server">
			<input type="hidden" id="lblsclass" runat="server"> <input type="hidden" id="lblscid" runat="server">
			<input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
