

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class complib
    Inherits System.Web.UI.Page
    Protected WithEvents lang1704 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1703 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1702 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1701 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1700 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1699 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1698 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1697 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1696 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1695 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1694 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1693 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1692 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1691 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1690 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1689 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1688 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1687 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1686 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1685 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1684 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1683 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1682 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1681 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1680 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1679 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim Tables As String = "complib"
    Dim PK As String = "comid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim sort As String
    Dim cid, srch, sid, did, clid As String
    Dim decPgNav As Decimal
    Dim intPgNav As Integer
    Dim coid, ro As String
    Dim copy As New Utilities
    Dim comi As New mmenu_utils_a
    Dim dept, cell, psid, usr, pmadmin, cadm, coi, lang As String
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifcodets As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents geteq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtnewcomp As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtnewdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdac As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcurrtab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcopytyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldesc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdsc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblkey As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rptreq As System.Web.UI.WebControls.Repeater
    Protected WithEvents tbleq As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tbleqrec As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents treq As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents txtnewkey As System.Web.UI.WebControls.TextBox
    Protected WithEvents rbmso As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbteq As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents ibtncopy As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblselkey As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcopyret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcell As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnocopy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbstom As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents lblpmadmin As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcadm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents treditmsg As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents txtcompclass As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtsrchkey As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblcclassid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Login As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dddb As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents Imagebutton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tbltop As System.Web.UI.HtmlControls.HtmlTable

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        xCoord.Value = 0
        yCoord.Value = 0
        GetFSOVLIBS()
        GetFSLangs()
        Dim coi As String = comi.COMPI
        lblcomi.Value = coi

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)
        Dim app As New AppUtils
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
            Exit Sub
        End Try

        Dim url As String = app.Switch
        If url <> "ok" Then
            'Response.Redirect(url)
        End If
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("complib.aspx")
        Catch ex As Exception

        End Try
        If Not IsPostBack Then
            Try
                pmadmin = HttpContext.Current.Session("pmadmin").ToString()
                cadm = HttpContext.Current.Session("cadm").ToString()
                lblpmadmin.Value = pmadmin
                lblcadm.Value = cadm
            Catch ex As Exception
                pmadmin = "no"
                cadm = "no"
                lblpmadmin.Value = ""
                lblcadm.Value = ""
            End Try
            Dim ap As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
            Dim db As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
            Dim mdba As String = System.Configuration.ConfigurationManager.AppSettings("MDBA")
            sid = HttpContext.Current.Session("dfltps").ToString
            usr = HttpContext.Current.Session("username").ToString()
            lbluser.Value = usr
            lblcurrtab.Value = "tdmc"
            lblcopytyp.Value = "site"
            'lblpsid.Value = "0"
            lblsid.Value = sid
            lbldb.Value = db
            txtpg.Value = "1"
            copy.Open()
            PopAC()
            PopComp(PageNumber)
            copy.Dispose()
            lblret.Value = ""
        Else
            If Request.Form("lblret") = "next" Then
                copy.Open()
                GetNext()
                copy.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                copy.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopComp(PageNumber)
                copy.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                copy.Open()
                GetPrev()
                copy.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                copy.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopComp(PageNumber)
                copy.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "checkcomp" Then
                PageNumber = txtpg.Value
                copy.Open()
                'PopAC()
                PopComp(PageNumber)
                copy.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "checkcopy" Then
                PageNumber = txtpg.Value
                copy.Open()
                Dim typ As String = lblcopytyp.Value
                'If typ = "dest" Then
                CheckCopy()
                'Else
                'CopyComp()
                'End If
                'PopAC()
                PopComp(PageNumber)
                copy.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub PopAC()
        'cid = "0" 'lblcid.Value
        'sql = "select * from compClass where compid = '" & cid & "'"
        'dr = copy.GetRdrData(sql)
        'ddac.DataSource = dr
        'ddac.DataTextField = "assetclass"
        'ddac.DataValueField = "acid"
        'ddac.DataBind()
        'dr.Close()
        'ddac.Items.Insert(0, "Select Component Class")
    End Sub
    Private Sub CheckCopy()
        cid = "0" 'lblcid.Value
        Dim eqcnt As Integer
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim orig As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
        Dim typ As String = lblcopytyp.Value
        If typ = "site" Then
            Dim newkey As String = txtnewkey.Text
            newkey = copy.ModString1(newkey)
            'If Len(newkey) > 0 Then
            If Len(newkey) > 50 Then
                Dim strMessage1 As String = tmod.getmsg("cdstr652", "complib.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                Exit Sub
            End If
            Dim cmd1 As New SqlCommand("select count(*) from [" & srvr & "].[" & orig & "].[dbo].[complib] where compkey = @newkey")
            Dim param1 = New SqlParameter("@newkey", SqlDbType.VarChar)
            param1.Value = newkey
            cmd1.Parameters.Add(param1)
            eqcnt = 0 'copy.ScalarHack(cmd1)
            If eqcnt = 0 Then
                Try
                    CopyComp()
                Catch ex As Exception
                    Dim strMessage2 As String = ex.Message
                    strMessage2 = Replace(strMessage2, "'", Chr(180), , , vbTextCompare) '"Problem Saving Record\nPlease review selections and try again."
                    Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                End Try
            Else
                lblnocopy.Value = "no"
                Dim strMessage3 As String = tmod.getmsg("cdstr653", "complib.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage3, "strKey1")
            End If
            'Else
            'lblnocopy.Value = "no"
            'Dim strMessage4 As String =  tmod.getmsg("cdstr654" , "complib.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage4, "strKey1")
            'End If
        Else
            Dim neweq As String = txtnewcomp.Text 'lblcomp.Value
            neweq = copy.ModString1(neweq)

            Dim fuid As String = lblfuid.Value
            If Len(neweq) > 0 Then
                If Len(neweq) > 100 Then
                    Dim strMessage1 As String = tmod.getmsg("cdstr655", "complib.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                    Exit Sub
                End If
                If typ = "dest" Then
                    'sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[equipment] where compid = '" & cid & "' and eqnum = '" & neweq & "'"
                    Dim cmd0 As New SqlCommand("select count(*) from [" & srvr & "].[" & orig & "].[dbo].[complib] where compnum = @neweq")
                    Dim param1 = New SqlParameter("@srvr", SqlDbType.VarChar)
                    param1.Value = srvr
                    cmd0.Parameters.Add(param1)
                    Dim param2 = New SqlParameter("@mdb", SqlDbType.VarChar)
                    param2.Value = mdb
                    cmd0.Parameters.Add(param2)
                    Dim param3 = New SqlParameter("@cid", SqlDbType.Int)
                    param3.Value = cid
                    cmd0.Parameters.Add(param3)
                    Dim param4 = New SqlParameter("@neweq", SqlDbType.VarChar)
                    param4.Value = neweq
                    cmd0.Parameters.Add(param4)
                    'copy.Open()
                    'eqcnt = copy.ScalarHack(cmd0)
                    'eqcnt = copy.Scalar(sql)

                    sql = "select count(*) from components where func_id = '" & fuid & "' " _
                   + "and compid = '" & cid & "' and compnum = '" & neweq & "'"
                    eqcnt = copy.Scalar(sql)
                Else
                    sql = "select count(*) from components where func_id = '" & fuid & "' " _
                    + "and compid = '" & cid & "' and compnum = '" & neweq & "'"
                    eqcnt = copy.Scalar(sql)

                End If
                '*** Multi Add ***


                If eqcnt = 0 Then
                    Try
                        CopyComp()
                    Catch ex As Exception
                        lblcopyret.Value = ""
                        Dim strMessage2 As String = ex.Message
                        strMessage2 = Replace(strMessage2, "'", Chr(180), , , vbTextCompare)
                        strMessage2 = Replace(strMessage2, """", Chr(180), , , vbTextCompare) '"Problem Saving Record\nPlease review selections and try again."
                        Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                    End Try
                Else
                    lblnocopy.Value = "no"
                    Dim strMessage3 As String = tmod.getmsg("cdstr656", "complib.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage3, "strKey1")
                End If
            Else
                lblnocopy.Value = "no"
                Dim strMessage4 As String = tmod.getmsg("cdstr657", "complib.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage4, "strKey1")
            End If

        End If

    End Sub


    Private Sub CopyComp()
        Dim oldcomp, eqid, fuid, compflag, nc, nd, taskflag, sid, did, clid, usr, mdb, typ, lang As String
        lang = lblfslang.Value
        mdb = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim orig As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString
        nc = txtnewcomp.Text
        nc = copy.ModString1(nc)
        If Len(nc) > 100 Then
            Dim strMessage1 As String = tmod.getmsg("cdstr658", "complib.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
            Exit Sub
        End If
        nd = txtnewdesc.Text
        nd = copy.ModString1(nd)
        If Len(nd) > 100 Then
            Dim strMessage1 As String = tmod.getmsg("cdstr659", "complib.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
            Exit Sub
        End If
        taskflag = "1"
        compflag = "1"
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        typ = lblcopytyp.Value
        sid = lblsid.Value
        oldcomp = lblcoid.Value
        usr = lbluser.Value
        Dim newkey As String = txtnewkey.Text
        If newkey = "" Then
            newkey = lblselkey.Value
        End If
        newkey = copy.ModString1(newkey)
        If typ = "dest" Then
            lblcopyret.Value = "dest"
            sql = "usp_copyCompSing1MDBco '" & cid & "', '" & oldcomp & "', '" & eqid & "', '" & fuid & "', " _
            + "'" & compflag & "', '" & nc & "', '" & nd & "', '" & taskflag & "', '" & sid & "', " _
            + "'" & did & "', '" & clid & "', '" & usr & "','" & mdb & "','" & orig & "','" & srvr & "','" & newkey & "','" & lang & "'"
        ElseIf typ = "master" Then
            lblcopyret.Value = "master"
            sql = "usp_copyCompSing1MDBcoTOM '" & cid & "', '" & oldcomp & "', '" & nc & "', '" & nd & "'," _
            + "'" & usr & "','" & mdb & "','" & orig & "','" & srvr & "','" & newkey & "','" & lang & "'"
        Else
            lblcopyret.Value = "site"
            sql = "usp_copyCompSing1MDBcoMS '" & cid & "', '" & oldcomp & "', '" & sid & "', '" & nc & "', '" & nd & "', " _
            + "'" & usr & "','" & mdb & "','" & orig & "','" & srvr & "','" & newkey & "','" & lang & "'"
        End If
        Dim ap As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")

        Dim fid, cmid, ofid, ocmid As String
        Dim oloc As String = lbloloc.Value
        Dim cloc As String = ap
        Dim pid, t, tn, tm, ts, td, tns, tnd, tms, tmd, nt, ntn, ntm, ont, ontn, ontm As String
        Dim piccnt As Integer
        If mdb <> orig Then
            Try
                Dim ds As New DataSet
                ds = copy.GetDSData(sql)
                Dim i As Integer
                Dim f As Integer = 0
                Dim c As Integer = 0
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    piccnt = ds.Tables(0).Rows(i)("piccnt").ToString
                    If piccnt = 0 Then
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        lblnewcoid.Value = cmid
                    Else
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        lblnewcoid.Value = cmid
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & cloc & t
                        td = Replace(td, "\", "/")


                        tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & cloc & tn
                        tnd = Replace(tnd, "\", "/")


                        tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & cloc & tm
                        tmd = Replace(tmd, "\", "/")

                        '***NEED WWW CONFIG REF HERE***
                        'ts = Replace(ts, "wwwlai1", "wwwlai2")
                        'tns = Replace(ts, "wwwlai1", "wwwlai2")
                        'tms = Replace(ts, "wwwlai1", "wwwlai2")

                        System.IO.File.Copy(ts, td, True)
                        System.IO.File.Copy(tns, tnd, True)
                        System.IO.File.Copy(tms, tmd, True)
                    End If
                Next
            Catch ex As Exception

            End Try
            'insert pm task image file copy here for mdb *********************************************

        Else
            'Try
            Dim ds As New DataSet
            ds = copy.GetDSData(sql)
            Dim i As Integer
            Dim f As Integer = 0
            Dim c As Integer = 0
            Dim x As Integer = ds.Tables(0).Rows.Count
            For i = 0 To (x - 1)
                piccnt = ds.Tables(0).Rows(i)("piccnt").ToString
                If piccnt = 0 Then
                    cmid = ds.Tables(0).Rows(i)("comid").ToString
                    lblnewcoid.Value = cmid
                Else
                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                    'fid = ds.Tables(0).Rows(i)("funcid").ToString
                    'ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                    cmid = ds.Tables(0).Rows(i)("comid").ToString
                    lblnewcoid.Value = cmid
                    ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                    t = ds.Tables(0).Rows(i)("picurl").ToString
                    tn = ds.Tables(0).Rows(i)("picurltn").ToString
                    tm = ds.Tables(0).Rows(i)("picurltm").ToString
                    t = Replace(t, "..", "")
                    tn = Replace(tn, "..", "")
                    tm = Replace(tm, "..", "")
                    If cmid = "" Then
                        f = f + 1
                        nt = "b-eqImg" & fid & "i"
                        ntn = "btn-eqImg" & fid & "i"
                        ntm = "btm-eqImg" & fid & "i"

                        ont = "b-eqImg" & ofid & "i"
                        ontn = "btn-eqImg" & ofid & "i"
                        ontm = "btm-eqImg" & ofid & "i"
                    Else
                        If typ = "site" Or typ = "master" Then
                            c = c + 1
                            nt = "c-clImg" & cmid & "i"
                            ntn = "ctn-clImg" & cmid & "i"
                            ntm = "ctm-clImg" & cmid & "i"

                            ont = "c-clImg" & ocmid & "i"
                            ontn = "ctn-clImg" & ocmid & "i"
                            ontm = "ctm-clImg" & ocmid & "i"
                        Else
                            c = c + 1
                            nt = "c-coImg" & cmid & "i"
                            ntn = "ctn-coImg" & cmid & "i"
                            ntm = "ctm-coImg" & cmid & "i"

                            ont = "c-clImg" & ocmid & "i"
                            ontn = "ctn-clImg" & ocmid & "i"
                            ontm = "ctm-clImg" & ocmid & "i"

                        End If

                    End If

                    ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                    ts = Replace(ts, "\", "/")
                    td = Server.MapPath("\") & ap & t
                    td = Replace(td, "\", "/")


                    tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                    tn = Replace(tn, "\", "/")
                    tnd = Server.MapPath("\") & ap & tn
                    tnd = Replace(tnd, "\", "/")


                    tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                    tms = Replace(tms, "\", "/")
                    tmd = Server.MapPath("\") & ap & tm
                    tmd = Replace(tmd, "\", "/")

                    System.IO.File.Copy(ts, td, True)
                    System.IO.File.Copy(tns, tnd, True)
                    System.IO.File.Copy(tms, tmd, True)
                End If

            Next
            'Catch ex As Exception

            'End Try
        End If



    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopComp(PageNumber)
        Catch ex As Exception
            copy.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr660", "complib.aspx.vb")

            copy.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopComp(PageNumber)
        Catch ex As Exception
            copy.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr661", "complib.aspx.vb")

            copy.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Public Sub GetCo1(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim mdb As String = lbldb.Value
        Dim rb As RadioButton = New RadioButton
        Dim eqid, geq, ckey As String
        rb = sender
        Dim rbi As String = rb.ClientID
        For Each i As RepeaterItem In rptreq.Items
            If i.ItemType = ListItemType.Item Then
                rb = CType(i.FindControl("rbco"), RadioButton)
                eqid = CType(i.FindControl("lblcomid"), Label).Text
                ckey = CType(i.FindControl("lblkeyi"), Label).Text

            ElseIf i.ItemType = ListItemType.AlternatingItem Then
                rb = CType(i.FindControl("rbcoalt"), RadioButton)
                eqid = CType(i.FindControl("lblcomidalt"), Label).Text
                ckey = CType(i.FindControl("lblkeya"), Label).Text

            End If
            If rbi = rb.ClientID Then
                geq = eqid
                lblselkey.Value = ckey
                Dim typ As String = lblcopytyp.Value
                If typ = "dest" Then
                    txtnewkey.Text = ckey
                End If
            Else
                rb.Checked = False
            End If
        Next
        lblcoid.Value = geq

        ifcodets.Attributes.Add("src", "complibcodets.aspx?start=yes&db=" & mdb & "&coid=" & geq)
        geteq.Attributes.Add("src", "complibtaskview2.aspx?start=yes&db=" & mdb & "&comid=" & geq)
    End Sub
    Private Sub PopComp(ByVal PageNumber As Integer)
        lang = lblfslang.Value
        cid = lblcid.Value
        psid = lblsid.Value
        ifcodets.Attributes.Add("src", "complibcodets.aspx?start=no")
        geteq.Attributes.Add("src", "complibtaskview2.aspx?start=no")
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        Dim ac As String = ""
        Dim acs As String = txtcompclass.Text
        Dim ksrch As String
        If acs <> "" Then
            ac = lblcclassid.Value 'txtcompclass.Text
        End If
        'If ddac.SelectedIndex <> 0 Then
        'ac = ddac.SelectedValue.ToString

        'End If
        Dim ct As String = lblcurrtab.Value
        usr = lbluser.Value
        Dim cts, ctss As String
        If ct = "tdmc" Then
            cts = " and createdby = '" & usr & "' and (lang = '" & lang & "' or lang is null)"
            ctss = " where createdby = '" & usr & "' and (lang = '" & lang & "' or lang is null)"
            tdmc.Attributes.Add("class", "thdrhov label")
            tdac.Attributes.Add("class", "thdr label")
            tdsc.Attributes.Add("class", "thdr label")
            rbstom.Disabled = False
        ElseIf ct = "tdsc" Then
            cts = " and sid = '" & psid & "' and (lang = '" & lang & "' or lang is null)"
            ctss = " where sid = '" & psid & "' and (lang = '" & lang & "' or lang is null)"
            tdsc.Attributes.Add("class", "thdrhov label")
            tdmc.Attributes.Add("class", "thdr label")
            tdac.Attributes.Add("class", "thdr label")
            rbstom.Disabled = False
        Else
            cts = " and sid is null and (lang = '" & lang & "' or lang is null)"
            ctss = " where sid is null and (lang = '" & lang & "' or lang is null)"
            tdmc.Attributes.Add("class", "thdr label")
            tdac.Attributes.Add("class", "thdrhov label")
            tdsc.Attributes.Add("class", "thdr label")
            rbstom.Disabled = True
        End If
        pmadmin = lblpmadmin.Value
        cadm = lblcadm.Value
        coi = lblcomi.Value
        If coi = "CAS" Then
            If pmadmin = "yes" Or cadm = "1" Then
                rbstom.Disabled = False
            Else
                rbstom.Disabled = True
            End If
        End If

        Dim intPgCnt As Integer
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim srchkey As String = txtsrchkey.Text 'lblcclassid.value '
        If srchkey = "" Then
            If Len(srch) = 0 Then
                srch = ""
                If acs = "" Then
                    sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[complib] " & ctss
                Else
                    If ac <> "" Then
                        sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[complib] where acid = '" & ac & "' " & cts
                    Else
                        sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[complib] where assetclass = '" & acs & "' " & cts
                    End If

                End If

                intPgCnt = copy.Scalar(sql)
            Else
                Dim csrch As String
                csrch = srch '"%" & srch & "%"
                If acs = "" Then
                    sql = "select count(*) from complib " _
                    + "where (compkey like '%" & csrch & "%' or compnum like '%" & csrch & "%' or " _
                    + "spl like '%" & csrch & "%' or compdesc like '%" & csrch & "%') " & cts

                    Dim cmd1 As New SqlCommand("select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[complib] " _
                    + "where (compkey like '%' + @srch + '%' or compnum like '%' + @srch + '%' or " _
                    + "spl like '%' + @srch + '%' or compdesc like '%' + @srch + '%' or desig like '%' + @srch + '%') " & cts)
                    Dim param7 = New SqlParameter("@srch", SqlDbType.VarChar)
                    param7.Value = csrch
                    cmd1.Parameters.Add(param7)
                    intPgCnt = copy.ScalarHack(cmd1)
                Else
                    If ac <> "" Then
                        sql = "select count(*) from complib " _
                   + "where (compkey like '%" & csrch & "%' or compnum like '%" & csrch & "%' or " _
                   + "spl like '%" & csrch & "%' or compdesc like '%" & csrch & "%') and acid = '" & ac & "' " & cts

                        Dim cmd1 As New SqlCommand("select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[complib] " _
                        + "where (compkey like '%' + @srch + '%' or compnum like '%' + @srch + '%' or " _
                        + "spl like '%' + @srch + '%' or compdesc like '%' + @srch + '%' or desig like '%' + @srch + '%') and acid = '" & ac & "' " & cts)
                        Dim param7 = New SqlParameter("@srch", SqlDbType.VarChar)
                        param7.Value = srch
                        cmd1.Parameters.Add(param7)
                        intPgCnt = copy.ScalarHack(cmd1)
                    Else
                        sql = "select count(*) from complib " _
                       + "where (compkey like '%" & csrch & "%' or compnum like '%" & csrch & "%' or " _
                       + "spl like '%" & csrch & "%' or compdesc like '%" & csrch & "%') and acid = '" & ac & "' " & cts

                        Dim cmd1 As New SqlCommand("select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[complib] " _
                        + "where (compkey like '%' + @srch + '%' or compnum like '%' + @srch + '%' or " _
                        + "spl like '%' + @srch + '%' or compdesc like '%' + @srch + '%' or desig like '%' + @srch + '%') and assetclass = '" & acs & "' " & cts)
                        Dim param7 = New SqlParameter("@srch", SqlDbType.VarChar)
                        param7.Value = srch
                        cmd1.Parameters.Add(param7)
                        intPgCnt = copy.ScalarHack(cmd1)
                    End If


                End If
            End If
        Else
            ksrch = srchkey '"%" & srchkey & "%"
            sql = "select count(*) from complib " _
            + "where (compkey like '%" & ksrch & "%') " & cts

            Dim cmd1 As New SqlCommand("select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[complib] " _
            + "where (compkey like '%' + @srch + '%') " & cts)
            Dim param7 = New SqlParameter("@srch", SqlDbType.VarChar)
            param7.Value = ksrch
            cmd1.Parameters.Add(param7)
            intPgCnt = copy.ScalarHack(cmd1)
        End If

        PageNumber = txtpg.Value
        intPgNav = copy.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav

        'Get Page
        'add acs and mod procs 
        If srchkey = "" Then
            sql = "usp_getAllCompPgMdb_new '" & cid & "', '" & PageNumber & "', '" & PageSize & "', '" & srch & "', '" & usr & "', '" & ac & "','" & ct & "','" & psid & "','" & acs & "','" & lang & "'"
        Else
            sql = "usp_getAllCompPgMdb_new2 '" & cid & "', '" & PageNumber & "', '" & PageSize & "', '" & ksrch & "', '" & usr & "', '" & ac & "','" & ct & "','" & psid & "','" & lang & "'"
        End If

        dr = copy.GetRdrData(sql)
        rptreq.DataSource = dr
        rptreq.DataBind()
        dr.Close()
    End Sub

    Private Sub ddac_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        copy.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        PopComp(PageNumber)
        copy.Dispose()

    End Sub

    Private Sub Imagebutton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click
        copy.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        PopComp(PageNumber)
        copy.Dispose()

    End Sub



    Private Sub rptreq_ItemDataBound1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptreq.ItemDataBound
        usr = lbluser.Value
        pmadmin = lblpmadmin.Value
        cadm = lblcadm.Value
        coi = lblcomi.Value

        Dim cby As String
        If e.Item.ItemType = ListItemType.Item Then
            Dim img As HtmlImage = CType(e.Item.FindControl("imgei"), HtmlImage)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "comid").ToString
            cby = DataBinder.Eval(e.Item.DataItem, "createdby").ToString
            Dim cur As String = lblcurrtab.Value

            Dim deleteButton As ImageButton = CType(e.Item.FindControl("ibDel"), ImageButton)
            'disabled 06/06/12
            If coi = "CAS" Then
                If cur = "tdac" And (pmadmin = "yes" Or cadm = "1") Then 'Or 1 = 1 ' Or cby = usr
                    img.Attributes.Add("src", "../images/appbuttons/minibuttons/lilpentrans.gif")
                    img.Attributes.Add("onclick", "getedit('" & id & "')")
                    treditmsg.Attributes.Add("class", "visible")

                    deleteButton.Attributes("onclick") = "javascript:return " & _
                    "confirm('Are you sure you want to delete Component " & _
                    DataBinder.Eval(e.Item.DataItem, "compnum") & " ?')"
                ElseIf cur = "tdac" Then
                    img.Attributes.Add("src", "../images/appbuttons/minibuttons/lilpendis.gif")
                    img.Attributes.Add("onclick", "")
                    treditmsg.Attributes.Add("class", "details")

                    deleteButton.Attributes("class") = "details"
                Else
                    img.Attributes.Add("src", "../images/appbuttons/minibuttons/lilpentrans.gif")
                    img.Attributes.Add("onclick", "getedit('" & id & "')")
                    treditmsg.Attributes.Add("class", "visible")

                    deleteButton.Attributes("onclick") = "javascript:return " & _
                    "confirm('Are you sure you want to delete Component " & _
                    DataBinder.Eval(e.Item.DataItem, "compnum") & " ?')"
                End If
            Else
                img.Attributes.Add("src", "../images/appbuttons/minibuttons/lilpentrans.gif")
                img.Attributes.Add("onclick", "getedit('" & id & "')")
                treditmsg.Attributes.Add("class", "visible")

                deleteButton.Attributes("onclick") = "javascript:return " & _
                "confirm('Are you sure you want to delete Component " & _
                DataBinder.Eval(e.Item.DataItem, "compnum") & " ?')"
            End If
            




        ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim img As HtmlImage = CType(e.Item.FindControl("imgea"), HtmlImage)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "comid").ToString
            cby = DataBinder.Eval(e.Item.DataItem, "createdby").ToString
            Dim cur As String = lblcurrtab.Value

            Dim deleteButton As ImageButton = CType(e.Item.FindControl("ibDela"), ImageButton)
            If coi = "CAS" Then
                If cur = "tdac" And (pmadmin = "yes" Or cadm = "1") Then 'Or 1 = 1 'Or cby = usr
                    img.Attributes.Add("src", "../images/appbuttons/minibuttons/lilpentrans.gif")
                    img.Attributes.Add("onclick", "getedit('" & id & "')")
                    treditmsg.Attributes.Add("class", "visible")
                    deleteButton.Attributes("onclick") = "javascript:return " & _
                    "confirm('Are you sure you want to delete Component " & _
                    DataBinder.Eval(e.Item.DataItem, "compnum") & " ?')"
                ElseIf cur = "tdac" Then
                    img.Attributes.Add("src", "../images/appbuttons/minibuttons/lilpendis.gif")
                    img.Attributes.Add("onclick", "")
                    treditmsg.Attributes.Add("class", "details")

                    deleteButton.Attributes("class") = "details"
                Else
                    img.Attributes.Add("src", "../images/appbuttons/minibuttons/lilpentrans.gif")
                    img.Attributes.Add("onclick", "getedit('" & id & "')")
                    treditmsg.Attributes.Add("class", "visible")

                    deleteButton.Attributes("onclick") = "javascript:return " & _
                    "confirm('Are you sure you want to delete Component " & _
                    DataBinder.Eval(e.Item.DataItem, "compnum") & " ?')"
                End If
            Else
                img.Attributes.Add("src", "../images/appbuttons/minibuttons/lilpentrans.gif")
                img.Attributes.Add("onclick", "getedit('" & id & "')")
                treditmsg.Attributes.Add("class", "visible")
                deleteButton.Attributes("onclick") = "javascript:return " & _
                "confirm('Are you sure you want to delete Component " & _
                DataBinder.Eval(e.Item.DataItem, "compnum") & " ?')"
            End If
            
        End If

        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang1679 As Label
                lang1679 = CType(e.Item.FindControl("lang1679"), Label)
                lang1679.Text = axlabs.GetASPXPage("complib.aspx", "lang1679")
            Catch ex As Exception
            End Try
            Try
                Dim lang1680 As Label
                lang1680 = CType(e.Item.FindControl("lang1680"), Label)
                lang1680.Text = axlabs.GetASPXPage("complib.aspx", "lang1680")
            Catch ex As Exception
            End Try
            Try
                Dim lang1681 As Label
                lang1681 = CType(e.Item.FindControl("lang1681"), Label)
                lang1681.Text = axlabs.GetASPXPage("complib.aspx", "lang1681")
            Catch ex As Exception
            End Try
            Try
                Dim lang1682 As Label
                lang1682 = CType(e.Item.FindControl("lang1682"), Label)
                lang1682.Text = axlabs.GetASPXPage("complib.aspx", "lang1682")
            Catch ex As Exception
            End Try
            Try
                Dim lang1683 As Label
                lang1683 = CType(e.Item.FindControl("lang1683"), Label)
                lang1683.Text = axlabs.GetASPXPage("complib.aspx", "lang1683")
            Catch ex As Exception
            End Try
            Try
                Dim lang1684 As Label
                lang1684 = CType(e.Item.FindControl("lang1684"), Label)
                lang1684.Text = axlabs.GetASPXPage("complib.aspx", "lang1684")
            Catch ex As Exception
            End Try
            Try
                Dim lang1685 As Label
                lang1685 = CType(e.Item.FindControl("lang1685"), Label)
                lang1685.Text = axlabs.GetASPXPage("complib.aspx", "lang1685")
            Catch ex As Exception
            End Try
            Try
                Dim lang1686 As Label
                lang1686 = CType(e.Item.FindControl("lang1686"), Label)
                lang1686.Text = axlabs.GetASPXPage("complib.aspx", "lang1686")
            Catch ex As Exception
            End Try
            Try
                Dim lang1687 As Label
                lang1687 = CType(e.Item.FindControl("lang1687"), Label)
                lang1687.Text = axlabs.GetASPXPage("complib.aspx", "lang1687")
            Catch ex As Exception
            End Try
            Try
                Dim lang1688 As Label
                lang1688 = CType(e.Item.FindControl("lang1688"), Label)
                lang1688.Text = axlabs.GetASPXPage("complib.aspx", "lang1688")
            Catch ex As Exception
            End Try
            Try
                Dim lang1689 As Label
                lang1689 = CType(e.Item.FindControl("lang1689"), Label)
                lang1689.Text = axlabs.GetASPXPage("complib.aspx", "lang1689")
            Catch ex As Exception
            End Try
            Try
                Dim lang1690 As Label
                lang1690 = CType(e.Item.FindControl("lang1690"), Label)
                lang1690.Text = axlabs.GetASPXPage("complib.aspx", "lang1690")
            Catch ex As Exception
            End Try
            Try
                Dim lang1691 As Label
                lang1691 = CType(e.Item.FindControl("lang1691"), Label)
                lang1691.Text = axlabs.GetASPXPage("complib.aspx", "lang1691")
            Catch ex As Exception
            End Try
            Try
                Dim lang1692 As Label
                lang1692 = CType(e.Item.FindControl("lang1692"), Label)
                lang1692.Text = axlabs.GetASPXPage("complib.aspx", "lang1692")
            Catch ex As Exception
            End Try
            Try
                Dim lang1693 As Label
                lang1693 = CType(e.Item.FindControl("lang1693"), Label)
                lang1693.Text = axlabs.GetASPXPage("complib.aspx", "lang1693")
            Catch ex As Exception
            End Try
            Try
                Dim lang1694 As Label
                lang1694 = CType(e.Item.FindControl("lang1694"), Label)
                lang1694.Text = axlabs.GetASPXPage("complib.aspx", "lang1694")
            Catch ex As Exception
            End Try
            Try
                Dim lang1695 As Label
                lang1695 = CType(e.Item.FindControl("lang1695"), Label)
                lang1695.Text = axlabs.GetASPXPage("complib.aspx", "lang1695")
            Catch ex As Exception
            End Try
            Try
                Dim lang1696 As Label
                lang1696 = CType(e.Item.FindControl("lang1696"), Label)
                lang1696.Text = axlabs.GetASPXPage("complib.aspx", "lang1696")
            Catch ex As Exception
            End Try
            Try
                Dim lang1697 As Label
                lang1697 = CType(e.Item.FindControl("lang1697"), Label)
                lang1697.Text = axlabs.GetASPXPage("complib.aspx", "lang1697")
            Catch ex As Exception
            End Try
            Try
                Dim lang1698 As Label
                lang1698 = CType(e.Item.FindControl("lang1698"), Label)
                lang1698.Text = axlabs.GetASPXPage("complib.aspx", "lang1698")
            Catch ex As Exception
            End Try
            Try
                Dim lang1699 As Label
                lang1699 = CType(e.Item.FindControl("lang1699"), Label)
                lang1699.Text = axlabs.GetASPXPage("complib.aspx", "lang1699")
            Catch ex As Exception
            End Try
            Try
                Dim lang1700 As Label
                lang1700 = CType(e.Item.FindControl("lang1700"), Label)
                lang1700.Text = axlabs.GetASPXPage("complib.aspx", "lang1700")
            Catch ex As Exception
            End Try
            Try
                Dim lang1701 As Label
                lang1701 = CType(e.Item.FindControl("lang1701"), Label)
                lang1701.Text = axlabs.GetASPXPage("complib.aspx", "lang1701")
            Catch ex As Exception
            End Try
            Try
                Dim lang1702 As Label
                lang1702 = CType(e.Item.FindControl("lang1702"), Label)
                lang1702.Text = axlabs.GetASPXPage("complib.aspx", "lang1702")
            Catch ex As Exception
            End Try
            Try
                Dim lang1703 As Label
                lang1703 = CType(e.Item.FindControl("lang1703"), Label)
                lang1703.Text = axlabs.GetASPXPage("complib.aspx", "lang1703")
            Catch ex As Exception
            End Try
            Try
                Dim lang1704 As Label
                lang1704 = CType(e.Item.FindControl("lang1704"), Label)
                lang1704.Text = axlabs.GetASPXPage("complib.aspx", "lang1704")
            Catch ex As Exception
            End Try

        End If

    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1679.Text = axlabs.GetASPXPage("complib.aspx", "lang1679")
        Catch ex As Exception
        End Try
        Try
            lang1680.Text = axlabs.GetASPXPage("complib.aspx", "lang1680")
        Catch ex As Exception
        End Try
        Try
            lang1681.Text = axlabs.GetASPXPage("complib.aspx", "lang1681")
        Catch ex As Exception
        End Try
        Try
            lang1682.Text = axlabs.GetASPXPage("complib.aspx", "lang1682")
        Catch ex As Exception
        End Try
        Try
            lang1683.Text = axlabs.GetASPXPage("complib.aspx", "lang1683")
        Catch ex As Exception
        End Try
        Try
            lang1684.Text = axlabs.GetASPXPage("complib.aspx", "lang1684")
        Catch ex As Exception
        End Try
        Try
            lang1685.Text = axlabs.GetASPXPage("complib.aspx", "lang1685")
        Catch ex As Exception
        End Try
        Try
            lang1686.Text = axlabs.GetASPXPage("complib.aspx", "lang1686")
        Catch ex As Exception
        End Try
        Try
            lang1687.Text = axlabs.GetASPXPage("complib.aspx", "lang1687")
        Catch ex As Exception
        End Try
        Try
            lang1688.Text = axlabs.GetASPXPage("complib.aspx", "lang1688")
        Catch ex As Exception
        End Try
        Try
            lang1689.Text = axlabs.GetASPXPage("complib.aspx", "lang1689")
        Catch ex As Exception
        End Try
        Try
            lang1690.Text = axlabs.GetASPXPage("complib.aspx", "lang1690")
        Catch ex As Exception
        End Try
        Try
            lang1691.Text = axlabs.GetASPXPage("complib.aspx", "lang1691")
        Catch ex As Exception
        End Try
        Try
            lang1692.Text = axlabs.GetASPXPage("complib.aspx", "lang1692")
        Catch ex As Exception
        End Try
        Try
            lang1693.Text = axlabs.GetASPXPage("complib.aspx", "lang1693")
        Catch ex As Exception
        End Try
        Try
            lang1694.Text = axlabs.GetASPXPage("complib.aspx", "lang1694")
        Catch ex As Exception
        End Try
        Try
            lang1695.Text = axlabs.GetASPXPage("complib.aspx", "lang1695")
        Catch ex As Exception
        End Try
        Try
            lang1696.Text = axlabs.GetASPXPage("complib.aspx", "lang1696")
        Catch ex As Exception
        End Try
        Try
            lang1697.Text = axlabs.GetASPXPage("complib.aspx", "lang1697")
        Catch ex As Exception
        End Try
        Try
            lang1698.Text = axlabs.GetASPXPage("complib.aspx", "lang1698")
        Catch ex As Exception
        End Try
        Try
            lang1699.Text = axlabs.GetASPXPage("complib.aspx", "lang1699")
        Catch ex As Exception
        End Try
        Try
            lang1700.Text = axlabs.GetASPXPage("complib.aspx", "lang1700")
        Catch ex As Exception
        End Try
        Try
            lang1701.Text = axlabs.GetASPXPage("complib.aspx", "lang1701")
        Catch ex As Exception
        End Try
        Try
            lang1702.Text = axlabs.GetASPXPage("complib.aspx", "lang1702")
        Catch ex As Exception
        End Try
        Try
            lang1703.Text = axlabs.GetASPXPage("complib.aspx", "lang1703")
        Catch ex As Exception
        End Try
        Try
            lang1704.Text = axlabs.GetASPXPage("complib.aspx", "lang1704")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                ibtncopy.Attributes.Add("src", "../images2/eng/bgbuttons/copy.gif")
            ElseIf lang = "fre" Then
                ibtncopy.Attributes.Add("src", "../images2/fre/bgbuttons/copy.gif")
            ElseIf lang = "ger" Then
                ibtncopy.Attributes.Add("src", "../images2/ger/bgbuttons/copy.gif")
            ElseIf lang = "ita" Then
                ibtncopy.Attributes.Add("src", "../images2/ita/bgbuttons/copy.gif")
            ElseIf lang = "spa" Then
                ibtncopy.Attributes.Add("src", "../images2/spa/bgbuttons/copy.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            Img2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("complib.aspx", "Img2") & "')")
            Img2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub rptreq_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptreq.ItemCommand
        If e.CommandName = "Delete" Then
            Dim ts, tn As String
            If e.Item.ItemType = ListItemType.Item Then
                ts = CType(e.Item.FindControl("lblcomid"), Label).Text
            ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
                ts = CType(e.Item.FindControl("lblcomidalt"), Label).Text
            End If
            If ts <> "" Then
                sql = "delete from complib where comid = '" & ts & "'; " _
                + "delete from comppictures where comid = '" & ts & "'; " _
                + "delete from complibfm where comid = '" & ts & "'; " _
                + "delete from comptasks where comid = '" & ts & "'; " _
                + "delete from comptaskfailmodes where comid = '" & ts & "' " _
                + "delete from compparts where comid = '" & ts & "'; " _
                + "delete from comppartsout where comid = '" & ts & "'; " _
                + "delete from comptools where comid = '" & ts & "'; " _
                + "delete from comptoolsout where comid = '" & ts & "'; " _
                + "delete from complubes where comid = '" & ts & "'; " _
                + "delete from complubesout where comid = '" & ts & "'"
                copy.Open()
                copy.Update(sql)
                Try
                    Dim pg As Integer = txtpg.Value
                    txtpg.Value = PageNumber
                    PopComp(PageNumber)
                Catch ex As Exception
                    copy.Dispose()
                    Dim strMessage As String = "Problem ReLoading Page"
                    copy.CreateMessageAlert(Me, strMessage, "strKey1")
                End Try
            End If
        End If
    End Sub
End Class
