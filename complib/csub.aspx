<%@ Page Language="vb" AutoEventWireup="false" Codebehind="csub.aspx.vb" Inherits="lucy_r12.csub" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>csub</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/csubaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checkinv();">
		<form id="form1" method="post" runat="server">
			<table cellSpacing="1">
				<tr>
					<td class="thdrsing label" colSpan="7"><asp:Label id="lang1867" runat="server">Current Task</asp:Label></td>
				</tr>
				<tr>
					<td class="btmmenu plainlabel" width="60"><asp:Label id="lang1868" runat="server">Task#</asp:Label></td>
					<td class="btmmenu plainlabel" width="420"><asp:Label id="lang1869" runat="server">Top Level Task Description</asp:Label></td>
					<td class="btmmenu plainlabel" width="100"><asp:Label id="lang1870" runat="server">Skill Required</asp:Label></td>
					<td class="btmmenu plainlabel" width="60">Qty</td>
					<td class="btmmenu plainlabel" width="60"><asp:Label id="lang1871" runat="server">Min Ea</asp:Label></td>
					<td class="btmmenu plainlabel" width="60"><asp:Label id="lang1872" runat="server">EQ Status</asp:Label></td>
					<td class="btmmenu plainlabel" width="70"><asp:Label id="lang1873" runat="server">Down Time</asp:Label></td>
				</tr>
				<tr>
					<td class="plainlabel" id="tdtnum" runat="server"></td>
					<td class="plainlabel" id="tddesc" runat="server"></td>
					<td class="plainlabel" id="tdskill" runat="server"></td>
					<td class="plainlabel" id="tdqty" runat="server"></td>
					<td class="plainlabel" id="tdmin" runat="server"></td>
					<td class="plainlabel" id="tdrd" runat="server"></td>
					<td class="plainlabel" id="tdrdt" runat="server"></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table>
				<tr>
					<td class="thdrsing label" colSpan="7"><asp:Label id="lang1874" runat="server">Sub Tasks</asp:Label></td>
				</tr>
				<tr>
					<td align="right"><asp:imagebutton id="addtask" runat="server" ImageUrl="../images/appbuttons/bgbuttons/addtask.gif"></asp:imagebutton>&nbsp;<IMG id="btnreturn" onclick="handlereturn();" height="19" src="../images/appbuttons/bgbuttons/return.gif"
							width="69" runat="server"></td>
				</tr>
				<tr>
					<td><asp:datagrid id="dgtasks" runat="server" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="1"
							GridLines="None">
							<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
							<ItemStyle CssClass="ptransrow"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:imagebutton id="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:imagebutton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="Task#">
									<HeaderStyle CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblta runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id=lblt runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>' width="40px">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Sub Task#">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblsubt runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id=lblst runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.subtask") %>' width="40px">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Sub Task Description">
									<HeaderStyle Width="280px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label3 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>' Width="270px">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id=txtdesc runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskdesc") %>' Height="70px" TextMode="MultiLine" width="260px" MaxLength="1000">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" SortExpression="skill desc" HeaderText="Skill Required">
									<HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:dropdownlist id="ddskill" runat="server" DataSource="<%# PopulateSkills %>" DataTextField="skill" DataValueField="skillid" SelectedIndex='<%# GetSelIndex(Container.DataItem("skillindex")) %>'>
										</asp:dropdownlist>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Skill Qty">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>' ID="Label10" NAME="Label8">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txtqty" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Skill Min Ea">
									<HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>' ID="Label12" >
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txttr" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Down Time">
									<HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rdt") %>' ID="Label2" >
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txtdt" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.rdt") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Parts/Tools/Lubes">
									<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:imagebutton id="Imagebutton23" runat="server" ImageUrl="../images/appbuttons/minibuttons/parttrans.gif"
											ToolTip="Add Parts" CommandName="Part"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton23a" runat="server" ImageUrl="../images/appbuttons/minibuttons/tooltrans.gif"
											ToolTip="Add Tools" CommandName="Tool"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton23b" runat="server" ImageUrl="../images/appbuttons/minibuttons/lubetrans.gif"
											ToolTip="Add Lubricants" CommandName="Lube"></asp:imagebutton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="pmtskid">
									<ItemTemplate>
										<asp:Label id="lbltida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblttid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Delete">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:ImageButton id="ibDel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
			<input id="lblcid" type="hidden" runat="server" NAME="lblcid"> <input id="lbltid" type="hidden" runat="server" NAME="lbltid">
			<input id="lblcoid" type="hidden" runat="server" NAME="lblfuid"> <input id="lblfilt" type="hidden" runat="server" NAME="lblfilt">
			<input id="lblsubval" type="hidden" runat="server" NAME="lblsubval"><input id="lblsid" type="hidden" runat="server" NAME="lblsid">
			<input id="lbloldtask" type="hidden" runat="server" NAME="lbloldtask"><input id="lblpart" type="hidden" name="lblpart" runat="server">
			<input id="lbltool" type="hidden" name="lbltool" runat="server"><input id="lbllube" type="hidden" name="lbllube" runat="server">
			<input id="lblnote" type="hidden" name="lblnote" runat="server"><input id="lbltasknum" type="hidden" runat="server" NAME="lbltasknum">
			<input id="lblpmtid" type="hidden" runat="server" NAME="lblpmtid"><input id="lbllog" type="hidden" runat="server" NAME="lbllog">
			<input id="lbllock" type="hidden" name="lbllock" runat="server"> <input id="lbllockedby" type="hidden" name="lbllockedby" runat="server">
			<input id="lblusername" type="hidden" runat="server" NAME="lblusername"><input id="lbleqid" type="hidden" runat="server" NAME="lbleqid">
			<input type="hidden" id="lblro" runat="server" NAME="lblro">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
