

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text

Public Class compclassmain
    Inherits System.Web.UI.Page
	Protected WithEvents lang1678 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1677 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1676 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1675 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1674 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, sql, ro As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String
    Dim dr As SqlDataReader
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldclass As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldsub As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblacid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblscid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim appset As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents deptdiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents imgadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgsrch As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Tr2 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents txtclass As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtclassdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdchildren As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtnewsub As System.Web.UI.WebControls.TextBox
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then

        Else
            If Request.Form("lblsubmit") = "getclass" Then
                appset.Open()
                GetClass()
                appset.Dispose()
            ElseIf Request.Form("lblsubmit") = "delclass" Then
                appset.Open()
                DelClass()
                appset.Dispose()
            ElseIf Request.Form("lblsubmit") = "delsub" Then
                appset.Open()
                DelSub()
                appset.Dispose()
            ElseIf Request.Form("lblsubmit") = "addclass" Then
                appset.Open()
                AddClass()
                appset.Dispose()
            ElseIf Request.Form("lblsubmit") = "addsub" Then
                appset.Open()
                AddSub()
                appset.Dispose()
            ElseIf Request.Form("lblsubmit") = "getsub" Then
                appset.Open()
                PopSub()
                appset.Dispose()
            ElseIf Request.Form("lblsubmit") = "saveclass" Then
                appset.Open()
                SaveClass()
                GetClass()
                appset.Dispose()
            End If
            lblsubmit.Value = ""
        End If
    End Sub
    Private Sub DelClass()
        Dim acid, cclass As String
        acid = lblacid.Value
        cclass = lbloldclass.Value
        Dim acnt As Integer
        If acid <> "" Then
            sql = "usp_delcompclass2 '" & acid & "', '" & cclass & "'"
            acnt = appset.Scalar(sql)
            If acnt = 0 Then
                acid = 0
                GetClass()
                PopSub()
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr644" , "compclassmain.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

        End If
    End Sub
    Private Sub DelSub()
        Dim scid As String
        scid = lblscid.Value
        Dim scnt As Integer
        If scid <> "" Then
            sql = "usp_delcompclasssub2 '" & scid & "'"
            scnt = appset.Scalar(sql)
            If scnt = 0 Then
                lblscid.Value = ""
                PopSub()
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr645" , "compclassmain.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

        End If


    End Sub
    Private Sub GetClass()
        Dim acid, cclass, cdesc As String
        acid = lblacid.Value
        cclass = lbloldclass.Value
        If acid <> "" Then
            sql = "select * from compclass where acid = '" & acid & "'"
            dr = appset.GetRdrData(sql)
            While dr.Read
                acid = dr.Item("acid").ToString
                cclass = dr.Item("assetclass").ToString
                cdesc = dr.Item("classdesc").ToString
            End While
            dr.Close()
            lblacid.Value = acid
            lbloldclass.Value = cclass
            txtclass.Text = cclass
            txtclassdesc.Text = cdesc
            PopSub()
        End If
    End Sub
    Private Sub SaveClass()
        Dim oclass, cclass, cdesc, acid As String
        cclass = txtclass.Text
        cclass = appset.ModString1(cclass)
        oclass = lbloldclass.Value
        cdesc = txtclassdesc.Text
        cdesc = appset.ModString1(cdesc)
        acid = lblacid.Value
        cid = "0"
        Dim scnt As Integer
        If oclass <> cclass Then
            Dim faill As String = cclass.ToLower
            sql = "select count(*) from compClass where lower(assetclass) = '" & faill & "' and compid = '" & cid & "'"
            scnt = appset.Scalar(sql)
            If scnt = 0 Then
                sql = "usp_updatecompclass '" & acid & "', '" & cclass & "', '" & cdesc & "'"
                appset.Update(sql)
            Else
                txtclass.Text = oclass
                Dim strMessage As String =  tmod.getmsg("cdstr646" , "compclassmain.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Else
            sql = "usp_updatecompclass '" & acid & "', '" & cclass & "', '" & cdesc & "'"
            appset.Update(sql)
        End If

    End Sub
    Private Sub AddClass()
        Dim cclass, cdesc As String
        Dim acid As Integer
        cclass = txtclass.Text
        cclass = appset.ModString1(cclass)
        cdesc = txtclassdesc.Text
        cdesc = appset.ModString1(cdesc)
        cid = "0"
        If Len(cclass) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr647" , "compclassmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(cdesc) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr648" , "compclassmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(cclass) > 0 Then
            Dim strchk As Integer
            Dim faill As String = cclass.ToLower
            sql = "select count(*) from compClass where lower(assetclass) = '" & faill & "' and compid = '" & cid & "'"
            strchk = appset.Scalar(sql)
            If strchk = 0 Then
                sql = "usp_addCompClass2 '" & cclass & "', '" & cdesc & "', '" & cid & "'"
                'appset.Update(sql)
                acid = appset.Scalar(sql)
                lblacid.Value = acid
                lbloldclass.Value = cclass
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr649" , "compclassmain.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If
    End Sub
    Private Sub AddSub()
        Dim cclass, sclass, acid As String
        cclass = lbloldclass.Value
        cclass = appset.ModString1(cclass)
        sclass = txtnewsub.Text
        sclass = appset.ModString1(sclass)
        acid = lblacid.Value
        If Len(sclass) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr650" , "compclassmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim scnt As String
        sql = "select count(*) from compclasssub where assetclass = '" & cclass & "' and subclass = '" & sclass & "'"
        scnt = appset.Scalar(sql)
        If scnt = 0 Then
            sql = "usp_addcompclasssub '" & cclass & "', '" & sclass & "', '" & acid & "'"
            appset.Update(sql)
            txtnewsub.Text = ""
            PopSub()
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr651" , "compclassmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Private Sub PopSub()
        Dim cclass As String
        cclass = lbloldclass.Value
        sql = "select s.*, f.failuremodes from compclasssub s left join compclasssubfmout f on f.scid = s.scid " _
        + "where assetclass = '" & cclass & "'"
        Dim scid, sclass, fcstr As String
        Dim sb As New StringBuilder

        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 684px;  HEIGHT: 110px"">")
        sb.Append("<table>")
        sb.Append("<tr><td class=""btmmenu plainlabel"" width=""180"">" & tmod.getlbl("cdlbl413" , "compclassmain.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""402"">" & tmod.getlbl("cdlbl784", "compclassmain.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""20""><img src=""../images/appbuttons/minibuttons/magnifier.gif""></td>")
        'sb.Append("<td class=""btmmenu plainlabel"" width=""50"">" & tmod.getlbl("cdlbl415" , "compclassmain.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""20""><img src=""../images/appbuttons/minibuttons/del.gif""></td>")
        sb.Append("<td width=""20"">&nbsp;</td></tr>")

        dr = appset.GetRdrData(sql)
        While dr.Read
            scid = dr.Item("scid").ToString
            sclass = dr.Item("subclass").ToString
            fcstr = dr.Item("failuremodes").ToString
            sb.Append("<tr>")
            sb.Append("<td class=""plainlabel"">" & sclass & "</td>")
            sb.Append("<td class=""plainlabel"">" & fcstr & "</td>")
            sb.Append("<td><img src=""../images/appbuttons/minibuttons/magnifier.gif"" onclick=""getfm('" & scid & "','" & sclass & "');""></td>")
            'sb.Append("<td><img src=""../images/appbuttons/minibuttons/magnifier.gif"" onclick=""gettasks('" & scid & "','" & sclass & "');""></td>")
            sb.Append("<td><img src=""../images/appbuttons/minibuttons/del.gif"" onclick=""delsub('" & scid & "');""></td>")
            sb.Append("</tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        sb.Append("</div")
        tdchildren.InnerHtml = sb.ToString



    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1674.Text = axlabs.GetASPXPage("compclassmain.aspx", "lang1674")
        Catch ex As Exception
        End Try
        Try
            lang1675.Text = axlabs.GetASPXPage("compclassmain.aspx", "lang1675")
        Catch ex As Exception
        End Try
        Try
            lang1676.Text = axlabs.GetASPXPage("compclassmain.aspx", "lang1676")
        Catch ex As Exception
        End Try
        Try
            lang1677.Text = axlabs.GetASPXPage("compclassmain.aspx", "lang1677")
        Catch ex As Exception
        End Try
        Try
            lang1678.Text = axlabs.GetASPXPage("compclassmain.aspx", "lang1678")
        Catch ex As Exception
        End Try

    End Sub

End Class
