<%@ Page Language="vb" AutoEventWireup="false" Codebehind="sfail.aspx.vb" Inherits="lucy_r12.sfail" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>sfail</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/sfailaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table width="422">
				<tr>
					<td class="thdrsingrt label" colSpan="2"><asp:Label id="lang1881" runat="server">Add/Edit Failure Modes Mini Dialog</asp:Label></td>
				</tr>
				<tr>
					<td class="label" height="26" width="172"><asp:Label id="lang1882" runat="server">Current Sub Class:</asp:Label></td>
					<td id="tdcomp" class="labellt" height="26" width="250" runat="server"></td>
				</tr>
				<tr class="details">
					<td class="label"><asp:Label id="lang1883" runat="server">Add a New Failure Mode</asp:Label></td>
					<td><asp:textbox id="txtnewfail" runat="server" MaxLength="50" Width="170px"></asp:textbox><asp:imagebutton id="btnaddfail" runat="server" ImageUrl="../images/appbuttons/bgbuttons/badd.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td colSpan="2">
						<hr style="BORDER-BOTTOM: #0000ff 1px solid; BORDER-LEFT: #0000ff 1px solid; BORDER-TOP: #0000ff 1px solid; BORDER-RIGHT: #0000ff 1px solid">
					</td>
				</tr>
			</table>
			<table width="422">
				<tr>
					<td class="label" align="center"><asp:Label id="lang1884" runat="server">Available Failure Modes</asp:Label></td>
					<td></td>
					<td class="label" align="center"><asp:Label id="lang1885" runat="server">Sub Class Failure Modes</asp:Label></td>
				</tr>
				<tr>
					<td width="200" align="center"><asp:listbox id="lbfailmaster" runat="server" Width="170px" Height="150px" SelectionMode="Multiple"></asp:listbox></td>
					<td vAlign="middle" width="22" align="center"><IMG id="todis" class="details" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
							width="20" height="20" runat="server"> <IMG id="fromdis" class="details" src="../images/appbuttons/minibuttons/backgraybg.gif"
							width="20" height="20" runat="server">
						<asp:imagebutton id="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><asp:imagebutton id="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton><IMG id="btnaddtosite" class="details" onmouseover="return overlib('Choose Failure Modes for this Plant Site ')"
							onmouseout="return nd()" onclick="getss();" src="../images/appbuttons/minibuttons/plusminus.gif" width="20" height="20" runat="server"></td>
					<td width="200" align="center"><asp:listbox id="lbfailmodes" runat="server" Width="170px" Height="150px" SelectionMode="Multiple"></asp:listbox></td>
				</tr>
				<tr>
					<td colSpan="3" align="right"><IMG id="ibtnret" onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
							width="69" height="19" runat="server"></td>
				</tr>
				<tr>
					<td class="bluelabel" colSpan="3"><asp:Label id="lang1886" runat="server">List Box Options</asp:Label></td>
				</tr>
				<tr>
					<td class="label" colSpan="3"><asp:radiobuttonlist id="cbopts" runat="server" Width="400px" CssClass="labellt" AutoPostBack="True">
							<asp:ListItem Value="0" Selected="True">Multi-Select (use arrows to move single or multiple selections)</asp:ListItem>
							<asp:ListItem Value="1">Click-Once (move selected item by clicking that item)</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td class="plainlabelblue" colSpan="3"><asp:Label id="lang1887" runat="server">* Please note that the Multi-Select Mode must be used to remove an item from the Sub Class Failure Modes list if only one item is present.</asp:Label></td>
				</tr>
			</table>
			<input id="lblscid" type="hidden" runat="server"> <input id="lblapp" type="hidden" name="lblapp" runat="server">
			<input id="lblopt" type="hidden" name="lblopt" runat="server"><input id="lblfailchk" type="hidden" name="lblfailchk" runat="server">
			<input id="lblsid" type="hidden" name="lblsid" runat="server"><input id="lbllog" type="hidden" name="lbllog" runat="server">
			<input id="lblro" type="hidden" name="lblro" runat="server"> <input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
