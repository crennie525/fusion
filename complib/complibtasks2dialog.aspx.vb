

'********************************************************
'*
'********************************************************



Public Class complibtasks2dialog
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tasknum, coid, tchk, comp, typ, fuid, tpm, who As String
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents iftd As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try
        If Not IsPostBack Then
            Try
                who = Request.QueryString("who").ToString
            Catch ex As Exception
                who = ""
            End Try
            typ = Request.QueryString("typ").ToString
            coid = Request.QueryString("coid").ToString
            comp = Request.QueryString("comp").ToString
            Try
                tasknum = Request.QueryString("tasknum").ToString
            Catch ex As Exception
                tasknum = ""
            End Try
            If typ = "comp" Then
                iftd.Attributes.Add("src", "complibtasks2.aspx?start=yes&coid=" & coid & "&comp=" & comp)
            Else
                Try
                    tpm = Request.QueryString("tpm").ToString
                    If tpm = "no" Then
                        tpm = "N"
                    End If
                Catch ex As Exception
                    tpm = "N"
                End Try
                fuid = Request.QueryString("fuid").ToString
                If tpm = "N" Then
                    iftd.Attributes.Add("src", "complibtasksedit2.aspx?who=" & who & "&start=yes&coid=" & coid & "&comp=" & comp & "&fuid=" & fuid & "&tpm=" & tpm & "&typ=" + typ + "&tasknum=" + tasknum)
                Else
                    iftd.Attributes.Add("src", "complibtaskeadittpm.aspx?start=yes&coid=" & coid & "&comp=" & comp & "&fuid=" & fuid & "&tpm=" & tpm & "&typ=" + typ + "&tasknum=" + tasknum)
                End If

            End If

        End If
    End Sub

End Class
