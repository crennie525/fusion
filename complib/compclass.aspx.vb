

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class compclass
    Inherits System.Web.UI.Page
	Protected WithEvents lang1673 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, sql, ro As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String
    Dim dr As SqlDataReader
    Protected WithEvents lblpre As System.Web.UI.WebControls.Label
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dgac As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim appset As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            'ro = Request.QueryString("ro").ToString
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgac.Columns(0).Visible = False
                dgac.Columns(3).Visible = False
            End If
            cid = Request.QueryString("cid")
            lblcid.Value = Request.QueryString("cid")
            cid = lblcid.Value
            appset.Open()
            PopAC(PageNumber)
            appset.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopAC(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopAC(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
        End If

    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        appset.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        PopAC(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub PopAC(ByVal PageNumber As Integer)
        'Try
        cid = lblcid.Value
        Dim srch As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", " ", , , vbTextCompare)
        If Len(srch) > 0 Then
            Filter = "assetclass like ''%" & srch & "%'' and compid = ''" & cid & "''"
            FilterCnt = "assetclass like '%" & srch & "%' and compid = '" & cid & "'"
        Else
            Filter = "compid = ''" & cid & "''"
            FilterCnt = "compid = '" & cid & "'"
        End If
        sql = "select count(*) " _
        + "from compClass where " & FilterCnt 'compid = '" & cid & "'"
        dgac.VirtualItemCount = appset.Scalar(sql)
        If dgac.VirtualItemCount = 0 Then
            lblpre.Text = "No Asset Class Records Found"
            dgac.Visible = True
            Tables = "compClass"
            PK = "acid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgac.DataSource = dr
            dgac.DataBind()
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgac.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgac.PageCount
        Else
            Tables = "compClass"
            PK = "acid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgac.DataSource = dr
            dgac.DataBind()
            dr.Close()
            lblpre.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgac.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgac.PageCount

        End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopAC(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr636" , "compclass.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopAC(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr637" , "compclass.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub dgac_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgac.EditCommand
        PageNumber = txtpg.Value 'dgac.CurrentPageIndex + 1
        lblold.Value = CType(e.Item.FindControl("Label24"), Label).Text
        dgac.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        PopAC(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgac_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgac.UpdateCommand
        cid = lblcid.Value
        Dim id, desc As String
        id = CType(e.Item.FindControl("lblacid"), Label).Text
        desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", ":", , , vbTextCompare)
        Dim old As String = lblold.Value
        If old <> desc Then
            If Len(desc) > 0 Then
                If Len(desc) > 50 Then
                    Dim strMessage As String =  tmod.getmsg("cdstr638" , "compclass.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    appset.Open()
                    Dim faill As String = desc.ToLower
                    sql = "select count(*) from compClass where lower(assetclass) = '" & faill & "' and compid = '" & cid & "'"
                    Dim strchk As Integer
                    strchk = appset.Scalar(sql)
                    If strchk = 0 Then
                        sql = "update compClass set assetclass = " _
                        + "'" & desc & "' where acid = '" & id & "'"
                        appset.Update(sql)
                        'appset.Dispose()
                        dgac.EditItemIndex = -1
                        PopAC(PageNumber)
                    Else
                        Dim strMessage As String =  tmod.getmsg("cdstr639" , "compclass.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        appset.Dispose()
                    End If
                    appset.Dispose()
                End If
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr640" , "compclass.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Else
            appset.Open()
            dgac.EditItemIndex = -1
            PopAC(PageNumber)
            appset.Dispose()
        End If


    End Sub

    Private Sub dgac_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgac.CancelCommand
        dgac.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        PopAC(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgac_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgac.DeleteCommand
        appset.Open()
        Dim id As String
        Try
            id = CType(e.Item.FindControl("lblacidi"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lblacid"), Label).Text
        End Try
        'id = CType(e.Item.Cells(1).Controls(1), Label).Text
        Dim cid As String = lblcid.Value
        sql = "usp_delCompClass '" & id & "', '" & cid & "'"
        appset.Update(sql)
        dgac.EditItemIndex = -1
        sql = "select Count(*) from compClass " _
                + "where compid = '" & cid & "'"
        PageNumber = appset.PageCount(sql, PageSize)
        'appset.Dispose()
        dgac.EditItemIndex = -1
        If dgac.CurrentPageIndex > PageNumber Then
            dgac.CurrentPageIndex = PageNumber - 1
        End If
        If dgac.CurrentPageIndex < PageNumber - 2 Then
            PageNumber = dgac.CurrentPageIndex + 1
        End If
        PopAC(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgac_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgac.ItemCommand
        Dim lname As String
        Dim lnamei As TextBox
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewac"), TextBox)
            lname = lnamei.Text
            lname = Replace(lname, "'", Chr(180), , , vbTextCompare)
            lname = Replace(lname, "--", "-", , , vbTextCompare)
            lname = Replace(lname, ";", ":", , , vbTextCompare)
            If lnamei.Text <> "" Then
                If lnamei.Text <> "" Then
                    If Len(lnamei.Text) > 50 Then
                        Dim strMessage As String =  tmod.getmsg("cdstr641" , "compclass.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Else
                        appset.Open()
                        Dim stat As String
                        cid = lblcid.Value
                        stat = lnamei.Text
                        Dim strchk As Integer
                        Dim faill As String = stat.ToLower
                        sql = "select count(*) from compClass where lower(assetclass) = '" & faill & "' and compid = '" & cid & "'"
                        strchk = appset.Scalar(sql)
                        If strchk = 0 Then
                            sql = "usp_addCompClass '" & lname & "', " & cid
                            appset.Update(sql)
                            Dim statcnt As Integer = dgac.VirtualItemCount + 1
                            lnamei.Text = ""
                            sql = "select Count(*) from compClass " _
                                            + "where compid = '" & cid & "'"
                            PageNumber = appset.PageCount(sql, PageSize)
                            PopAC(PageNumber)
                        Else
                            Dim strMessage As String =  tmod.getmsg("cdstr642" , "compclass.aspx.vb")
 
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        End If
                        appset.Dispose()
                    End If
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr643" , "compclass.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If
        End If
    End Sub
	

	

	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgac.Columns(0).HeaderText = dlabs.GetDGPage("compclass.aspx","dgac","0")
		Catch ex As Exception
		End Try
		Try
			dgac.Columns(2).HeaderText = dlabs.GetDGPage("compclass.aspx","dgac","2")
		Catch ex As Exception
		End Try
		Try
			dgac.Columns(3).HeaderText = dlabs.GetDGPage("compclass.aspx","dgac","3")
		Catch ex As Exception
		End Try

	End Sub

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang1673.Text = axlabs.GetASPXPage("compclass.aspx","lang1673")
		Catch ex As Exception
		End Try

	End Sub

End Class
