

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class sclasslookup
    Inherits System.Web.UI.Page
	Protected WithEvents lang1880 As System.Web.UI.WebControls.Label
    Protected WithEvents bgbreturn As System.Web.UI.HtmlControls.HtmlImage
	Protected WithEvents lang1879 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1878 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1877 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1876 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1875 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim rl As New Utilities
    Dim Tables As String = "compclasssub"
    Dim PK As String = "scid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String
    Dim intPgCnt, intPgNav As Integer
    Dim scid, sclass, acid, cclass As String
    Protected WithEvents lblcclass As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblssclass As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdcclass As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcdesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdchildren As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsclass As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblscid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents tbltop As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tbleqrec As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblacid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()



Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            acid = Request.QueryString("acid").ToString
            cclass = Request.QueryString("cclass").ToString
            lblacid.Value = acid
            lblcclass.Value = cclass
            txtpg.Value = "1"
            rl.Open()
            GetClass()
            GetComps()
            rl.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                rl.Open()
                GetNext()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                rl.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetComps()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                rl.Open()
                GetPrev()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                rl.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetComps()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "copysub" Then
                rl.Open()
                CopySub()
                rl.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub CopySub()
        acid = lblacid.Value
        cclass = lblcclass.Value
        scid = lblscid.Value
        sclass = lblsclass.Value
        sql = "usp_copycompclasssub '" & acid & "','" & cclass & "','" & scid & "'"
        rl.Update(sql)
        lblscid.Value = ""
        lblsclass.Value = ""
        PopSub()

    End Sub
    Private Sub GetClass()
        Dim acid, cclass, cdesc As String
        acid = lblacid.Value
        cclass = lblcclass.Value
        If acid <> "" Then
            sql = "select * from compclass where acid = '" & acid & "'"
            dr = rl.GetRdrData(sql)
            While dr.Read
                acid = dr.Item("acid").ToString
                cclass = dr.Item("assetclass").ToString
                cdesc = dr.Item("classdesc").ToString
            End While
            dr.Close()
            lblacid.Value = acid
            lblcclass.Value = cclass
            tdcclass.InnerHtml = cclass
            tdcdesc.InnerHtml = cdesc
            PopSub()
        End If
    End Sub
    Private Sub PopSub()
        Dim cclass As String
        cclass = lblcclass.Value
        sql = "select s.*, f.failuremodes from compclasssub s left join compclasssubfmout f on f.scid = s.scid " _
        + "where assetclass = '" & cclass & "'"
        Dim scid, sclass, fcstr As String
        Dim sb As New StringBuilder

        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 684px;  HEIGHT: 110px"">")
        sb.Append("<table>")
        sb.Append("<tr><td class=""btmmenu plainlabel"" width=""180"">" & tmod.getlbl("cdlbl422" , "sclasslookup.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""452"">" & tmod.getlbl("cdlbl423" , "sclasslookup.aspx.vb") & "</td>")
        sb.Append("<td width=""20"">&nbsp;</td></tr>")

        dr = rl.GetRdrData(sql)
        While dr.Read
            scid = dr.Item("scid").ToString
            sclass = dr.Item("subclass").ToString
            fcstr = dr.Item("failuremodes").ToString
            sb.Append("<tr>")
            sb.Append("<td class=""plainlabel"">" & sclass & "</td>")
            sb.Append("<td class=""plainlabel"">" & fcstr & "</td>")
            sb.Append("</tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        sb.Append("</div")
        tdchildren.InnerHtml = sb.ToString



    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetComps()
        Catch ex As Exception
            rl.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr752" , "sclasslookup.aspx.vb")
 
            rl.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetComps()
        Catch ex As Exception
            rl.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr753" , "sclasslookup.aspx.vb")
 
            rl.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetComps()
        acid = lblacid.Value
        Dim srch As String = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        If Len(srch) > 0 Then
            srch = "%" & srch & "%"
            Filter = "(assetclass like ''" & srch & "'' or classdesc like ''" & srch & "'' " _
            + "or subclass like ''" & srch & "'') and acid <> ''" & acid & "''"
            FilterCnt = "(assetclass like '" & srch & "' or classdesc like '" & srch & "' " _
            + "or subclass like '" & srch & "') and acid <> '" & acid & "'"
        Else
            Filter = "acid <> ''" & acid & "''"
            FilterCnt = "acid <> '" & acid & "'"
        End If
        Dim sb As StringBuilder = New StringBuilder

        sb.Append("<table width=""700"">")
        sb.Append("<tr><td colspan=""6""><div style=""OVERFLOW: auto; width: 740px; height: 250px;"">")
        sb.Append("<table width=""680"">")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""170"">" & tmod.getlbl("cdlbl424" , "sclasslookup.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""170"">" & tmod.getlbl("cdlbl425" , "sclasslookup.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""400"">" & tmod.getlbl("cdlbl426" , "sclasslookup.aspx.vb") & "</td>")
        Dim rowid As Integer = 0
        If FilterCnt <> "" Then
            sql = "select count(*) from compclasssub where " & FilterCnt
        Else
            sql = "select count(*) from compclasssub"
        End If
        PageNumber = txtpg.Value
        intPgCnt = rl.Scalar(sql)
        intPgNav = rl.PageCount(intPgCnt, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav
        Fields = "*, failuremodes = (select f.failuremodes from compclasssubfmout f where f.scid = compclasssub.scid)"
        dr = rl.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        Dim bc, ci, co, cd, cs, cg, si, so, fm As String
        While dr.Read
            If rowid = 0 Then
                rowid = 1
                bc = "white"
            Else
                rowid = 0
                bc = "#E7F1FD"
            End If
            ci = dr.Item("acid").ToString
            co = dr.Item("assetclass").ToString
            si = dr.Item("scid").ToString
            so = dr.Item("subclass").ToString
            fm = dr.Item("failuremodes").ToString

            sb.Append("<tr bgcolor=""" & bc & """>")
            sb.Append("<td class=""plainlabel"">" & co & "</td>")
            sb.Append("<td class=""linklabel""><a href=""#"" onclick=""rtret('" & si & "', '" & so & "');"">")
            sb.Append(so & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & fm & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        dr.Close()
        sb.Append("</table></div></td></tr></table>")
        tdcomp.InnerHtml = sb.ToString
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        rl.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        GetComps()
        rl.Dispose()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1875.Text = axlabs.GetASPXPage("sclasslookup.aspx", "lang1875")
        Catch ex As Exception
        End Try
        Try
            lang1876.Text = axlabs.GetASPXPage("sclasslookup.aspx", "lang1876")
        Catch ex As Exception
        End Try
        Try
            lang1877.Text = axlabs.GetASPXPage("sclasslookup.aspx", "lang1877")
        Catch ex As Exception
        End Try
        Try
            lang1878.Text = axlabs.GetASPXPage("sclasslookup.aspx", "lang1878")
        Catch ex As Exception
        End Try
        Try
            lang1879.Text = axlabs.GetASPXPage("sclasslookup.aspx", "lang1879")
        Catch ex As Exception
        End Try
        Try
            lang1880.Text = axlabs.GetASPXPage("sclasslookup.aspx", "lang1880")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                bgbreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                bgbreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                bgbreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                bgbreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                bgbreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
