<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="eqtab2.aspx.vb" Inherits="lucy_r12.eqtab2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>eqtab2</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/eqtab2aspx_2.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
     <!--
        function gotoeq(eid, sid, did, clid, chk, lid, eq, dept, cell, loc) {
            /*if (document.getElementById("rbopt").checked == true) {
                window.parent.gotopmopteq(eid, sid, did, clid, chk, lid, eq, dept, cell, loc);
            }
            else if (document.getElementById("rbdev").checked == true) {
                window.parent.gotopmdeveq(eid, sid, did, clid, chk, lid, eq, dept, cell, loc);
            }
            else if (document.getElementById("rbtopt").checked == true) {
                window.parent.gototpmopteq(eid, sid, did, clid, chk, lid, eq, dept, cell, loc);
            }
            else if (document.getElementById("rbtdev").checked == true) {
                window.parent.gototpmdeveq(eid, sid, did, clid, chk, lid, eq, dept, cell, loc);
            }
            else if (document.getElementById("rbtab").checked == true) {*/
                window.parent.gotopmtabeq(eid, sid, did, clid, chk, lid, eq, dept, cell, loc);
            //}*/
        }
        function gotofu(eid, fid, sid, did, clid, chk, lid, eq, dept, cell, loc) {
            if (document.getElementById("rbopt").checked == true) {
                window.parent.gotopmoptfu(eid, fid, sid, did, clid, chk, lid, eq, dept, cell, loc);
            }
            else if (document.getElementById("rbdev").checked == true) {
                window.parent.gotopmdevfu(eid, fid, sid, did, clid, chk, lid, eq, dept, cell, loc);
            }
            else if (document.getElementById("rbtopt").checked == true) {
                window.parent.gototpmoptfu(eid, fid, sid, did, clid, chk, lid, eq, dept, cell, loc);
            }
            else if (document.getElementById("rbtdev").checked == true) {
                window.parent.gototpmdevfu(eid, fid, sid, did, clid, chk, lid, eq, dept, cell, loc);
            }
            else if (document.getElementById("rbtab").checked == true) {
                window.parent.gotopmtabfu(eid, fid, sid, did, clid, chk, lid, eq, dept, cell, loc);
            }
        }
        function gotoco(eid, fid, cid, sid, did, clid, chk, lid, eq, dept, cell, loc) {
            if (document.getElementById("rbopt").checked == true) {
                window.parent.gotopmoptco(eid, fid, cid, sid, did, clid, chk, lid, eq, dept, cell, loc);
            }
            else if (document.getElementById("rbdev").checked == true) {
                window.parent.gotopmdevco(eid, fid, cid, sid, did, clid, chk, lid, eq, dept, cell, loc);
            }
            else if (document.getElementById("rbtopt").checked == true) {
                window.parent.gototpmoptco(eid, fid, cid, sid, did, clid, chk, lid, eq, dept, cell, loc);
            }
            else if (document.getElementById("rbtdev").checked == true) {
                window.parent.gototpmdevco(eid, fid, cid, sid, did, clid, chk, lid, eq, dept, cell, loc);
            }
            else if (document.getElementById("rbtab").checked == true) {
                window.parent.gotopmtabco(eid, fid, cid, sid, did, clid, chk, lid, eq, dept, cell, loc);
            }
        }
        function checkopts() {
            who = document.getElementById("lblcurropt").value;
            /*if (who == "rbopt") {
                document.getElementById("rbopt").checked = true;
            }
            else if (who == "rbdev") {
                document.getElementById("rbdev").checked = true;
            }
            else if (who == "rbtopt") {
                document.getElementById("rbtopt").checked = true;
            }
            else if (who == "rbtdev") {
                document.getElementById("rbtdev").checked = true;
            }
            else if (who == "rbtab") {
                document.getElementById("rbtab").checked = true;
            }*/
        }
        function resetsrch() {
            var sid = document.getElementById("lblsid").value;
            //var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lbluser").value;
            var appstr = document.getElementById("lblappstr").value;
            //alert(appstr)
            var ms = document.getElementById("lblms").value;
            window.location = "eqtab2.aspx?jump=no&typ=no&appstr=" + appstr + "&ms=" + ms + "&sid=" + sid + "&username=" + username + "&date=" + Date();
        }
        function getdets(eqid, sid, did, clid, chk, lid, eqnum) {
            //alert(eqnum)
            eqnum = eqnum.replace(/#/, "%23");
            //alert(eqnum)
            var eReturn = window.showModalDialog("../equip/fulistdetdialog.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:1040px; resizable=yes");
            if (eReturn) {
                var ret = eReturn;
                var retarr = ret.split(",");
                var fid = retarr[0];
                var cid = retarr[1];
                if (fid == "log") {
                    window.parent.setref();
                }
                else {
                    if (fid != "no") {
                        if (cid != "") {
                            gotoco(eqid, fid, cid, sid, did, clid, chk, lid)
                        }
                        else {
                            gotofu(eqid, fid, sid, did, clid, chk, lid)
                        }
                    }
                }
            }
        }
        function getsrch() {
            var eReturn = window.showModalDialog("../equip/maxsearchdialog.aspx?srchtyp=all" + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:1040px; resizable=yes");
            if (eReturn) {
                var ret = eReturn;
                //alert(ret)
                var retarr = ret.split(",");
                var eid = retarr[2];
                var sid = retarr[11];
                var did = retarr[0];
                var clid = retarr[1];
                var chk = retarr[9];
                var lid = retarr[10];
                //alert(ret + ", " + lid)
                var fid = retarr[4];
                var cid = retarr[12];
                if (fid != "" && cid != "") {
                    gotoco(eid, fid, cid, sid, did, clid, chk, lid)
                }
                else if (fid != "" && cid == "") {
                    gotofu(eid, fid, sid, did, clid, chk, lid)
                }
                else {
                    gotoeq(eid, sid, did, clid, chk, lid)
                }

            }
            else {

            }
        }
         //-->
    </script>
</head>
<body class="tbg" onload="GetsScroll();">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; background-color: transparent; top: 0px; left: 0px"
        cellspacing="0" cellpadding="0" width="780">
        <tbody>
            <tr>
                <td width="26">
                </td>
                <td width="404">
                </td>
                <td width="350">
                </td>
            </tr>
            <tr id="trdata" runat="server">
                <td id="tdarch" colspan="3" runat="server">
                </td>
            </tr>
            <tr>
                
                <td colspan="3" align="center">
                    <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                        border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
                                <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                    runat="server">
                            </td>
                            <td width="20">
                                <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                    runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="details">
                <td class="label" colspan="3" align="left">
                    <table>
                        <tr>
                            <td class="bluelabel">
                                <asp:Label ID="lang3122" runat="server">View "Site Work In Progress Report"</asp:Label>
                            </td>
                            <td>
                                <img onclick="getinprog();" border="0" alt="" src="../images/appimages/classiclookupnbg.gif"
                                    width="20" height="20">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td id="tdmsg" class="bluelabel" colspan="3" align="center" runat="server">
                </td>
            </tr>
        </tbody>
        <tr height="22">
            <td class="thdrsinglft" width="26">
                <img border="0" src="../images/appbuttons/minibuttons/pm3.gif">
            </td>
            <td class="thdrsingrt label" colspan="2" align="left">
                <asp:Label ID="lang3123" runat="server">PM Record Search</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table style="background-color: transparent" cellpadding="1" cellspacing="1" border="0"
                    width="780">
                    <tr>
                        <td class="label" width="250">
                            <asp:Label ID="lang3124" runat="server">Search PM Records (by Asset)</asp:Label>
                        </td>
                        <td width="240">
                            <asp:TextBox ID="txtsrch" runat="server" CssClass="plainlabel" Width="210px"></asp:TextBox>
                        </td>
                        <td width="20">
                            <asp:ImageButton ID="ibtnsearch" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm1.gif">
                            </asp:ImageButton>
                        </td>
                        <td width="20">
                            <img src="../images/appbuttons/minibuttons/magnifier.gif" onclick="getsrch();" onmouseover="return overlib('Use Max Search', LEFT)"
                                onmouseout="return nd()" />
                        </td>
                        <td width="230">
                            <img onclick="resetsrch();" alt="" src="../images/appbuttons/minibuttons/switch.gif" />
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel">
                            <asp:Label ID="lang3125" runat="server">Alternate Search Options:</asp:Label>
                        </td>
                        <td colspan="4">
                            <table cellpadding="1" cellspacing="1">
                                <tr>
                                    <td class="label" width="90">
                                        <asp:Label ID="lang3126" runat="server">Created By:</asp:Label>
                                    </td>
                                    <td width="160">
                                        <asp:DropDownList ID="ddcb" runat="server" CssClass="plainlabel" Width="150">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="label" width="90">
                                        <asp:Label ID="lang3127" runat="server">Modified By:</asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="ddmb" runat="server" CssClass="plainlabel" Width="150">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <asp:Label ID="lang3128" runat="server">From Date:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txts" runat="server" CssClass="plainlabel" Width="100px"></asp:TextBox>
                                        <img onclick="getcal('s');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                            width="19" height="19">
                                    </td>
                                    <td class="label">
                                        <asp:Label ID="lang3129" runat="server">To Date:</asp:Label>
                                    </td>
                                    <td width="160">
                                        <asp:TextBox ID="txtc" runat="server" CssClass="plainlabel" Width="100px"></asp:TextBox>
                                        <img onclick="getcal('c');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                            width="19" height="19">
                                    </td>
                                    <td width="20">
                                        <asp:ImageButton ID="Imagebutton1" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm1.gif">
                                        </asp:ImageButton>
                                    </td>
                                    <td width="20">
                                        <img onclick="resetsrch();" alt="" src="../images/appbuttons/minibuttons/switch.gif">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td id="tdsrchmsg" class="labelibl" colspan="5" align="center" runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lblsrch" type="hidden" name="Hidden1" runat="server">
    <input id="lbluser" type="hidden" name="Hidden1" runat="server">
    <input id="lblms" type="hidden" name="Hidden1" runat="server">
    <input id="lbldf" type="hidden" name="Hidden1" runat="server">
    <input id="lblfiltcnt" type="hidden" name="Hidden1" runat="server">
    <input id="lblfilt" type="hidden" name="Hidden1" runat="server">
    <input id="lblhref" type="hidden" name="lblhref" runat="server"><input id="txtpg"
        type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt" type="hidden" name="txtpgcnt"
            runat="server">
    <input id="lblret" type="hidden" name="lblret" runat="server"><input id="lbltyp"
        type="hidden" name="lbltyp" runat="server">
    <input id="lbleqid2" type="hidden" name="lbleqid2" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server"><input id="lblappstr"
        type="hidden" name="lblappstr" runat="server">
    <input id="spdivy" type="hidden" name="spdivy" runat="server"><input type="hidden"
        id="lblcurropt" runat="server">
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
    </FORM>
</body>
</html>
