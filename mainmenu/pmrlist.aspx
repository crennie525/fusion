﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmrlist.aspx.vb" Inherits="lucy_r12.pmrlist" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/reports.css" type="text/css" rel="stylesheet" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function handleprint(typ, rtid) {
            var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
            if (typ=="RBAS") {
            window.open("../reports/pmrhtml7_new.aspx?rid=" + rtid + "&typ=" + typ, "repWin", popwin);
            }
            else {
            typ = "RBASTM2"
                    window.open("../reports/pmrhtml6t.aspx?rid=" + rtid + "&typ=" + typ, "repWin", popwin);
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="dvlist" runat="server">
    
    </div>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lbltyp" runat="server" />
    </form>
</body>
</html>
