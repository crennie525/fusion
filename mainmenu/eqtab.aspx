<%@ Page Language="vb" AutoEventWireup="false" Codebehind="eqtab.aspx.vb" Inherits="lucy_r12.eqtab" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>eqtab</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		<script language="JavaScript" src="../scripts1/eqtabaspx_1.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  class="tbg" onload="GetsScroll();">
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; BACKGROUND-COLOR: transparent; TOP: 0px; LEFT: 0px" cellSpacing="0"
				cellPadding="0" width="710">
				<TBODY>
					<tr>
						<td width="26"></td>
						<td width="329"></td>
						<td width="355"></td>
					</tr>
					<tr id="trdata" runat="server">
						<td id="tdarch" colSpan="3" runat="server"></td>
					</tr>
					<tr>
						<td width="355" colSpan="2">
							<table>
								<tr>
									<td class="label"><asp:Label id="lang3104" runat="server">Jump To:</asp:Label></td>
									<td class="label"><INPUT id="rbdev" disabled type="radio" value="rbdev" name="rb" runat="server"><asp:Label id="lang3105" runat="server">PM Developer</asp:Label><br>
										<INPUT id="rbtdev" disabled type="radio" value="rbdev" name="rb" runat="server"><asp:Label id="lang3106" runat="server">TPM Developer</asp:Label></td>
									<td class="label">
										<INPUT id="rbopt" disabled type="radio" value="rbopt" name="rb" runat="server"><asp:Label id="lang3107" runat="server">PM Optimizer</asp:Label><br>
										<INPUT id="rbtopt" disabled type="radio" value="rbopt" name="rb" runat="server"><asp:Label id="lang3108" runat="server">TPM Optimizer</asp:Label></td>
								</tr>
							</table>
						</td>
						<td align="right" width="355">
							<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
								cellSpacing="0" cellPadding="0">
								<tr>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
											runat="server"></td>
									<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
											runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr class="details">
						<td class="label" align="left" colSpan="3">
							<table>
								<tr>
									<td class="bluelabel"><asp:Label id="lang3109" runat="server">View "Site Work In Progress Report"</asp:Label></td>
									<td><IMG onclick="getinprog();" height="20" alt="" src="../images/appimages/classiclookupnbg.gif"
											width="20" border="0"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="bluelabel" id="tdmsg" align="center" colSpan="3" runat="server"></td>
					</tr>
				</TBODY>
				<tr height="22">
					<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/pm3.gif" border="0"></td>
					<td class="thdrsingrt label" align="left" colSpan="2"><asp:Label id="lang3110" runat="server">PM Record Search</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="3">
						<table style="BACKGROUND-COLOR: transparent">
							<tr>
								<td class="label"><asp:Label id="lang3111" runat="server">Search PM Records (by Asset)</asp:Label></td>
								<td width="250"><asp:textbox id="txtsrch" runat="server" Width="250px" CssClass="plainlabel"></asp:textbox></td>
								<td width="20"><asp:imagebutton id="ibtnsearch" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm1.gif"></asp:imagebutton></td>
								<td width="240"><IMG onclick="resetsrch();" alt="" src="../images/appbuttons/minibuttons/switch.gif"></td>
							</tr>
							<tr height="18">
								<td class="bluelabel" colSpan="4"><asp:Label id="lang3112" runat="server">Alternate Search Options:</asp:Label></td>
							</tr>
							<tr height="26">
								<td colSpan="4">
									<table>
										<tr>
											<td class="label" width="90"><asp:Label id="lang3113" runat="server">Created By:</asp:Label></td>
											<td colSpan="2"><asp:dropdownlist id="ddcb" runat="server" CssClass="plainlabel"></asp:dropdownlist></td>
											<td class="label" width="90"><asp:Label id="lang3114" runat="server">Modified By:</asp:Label></td>
											<td colSpan="2"><asp:dropdownlist id="ddmb" runat="server" CssClass="plainlabel"></asp:dropdownlist></td>
										</tr>
										<TR>
											<td class="label"><asp:Label id="lang3115" runat="server">From Date:</asp:Label></td>
											<td><asp:textbox id="txts" runat="server" Width="120px" CssClass="plainlabel"></asp:textbox></td>
											<td width="50"><IMG onclick="getcal('s');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
													width="19"></td>
											<td class="label"><asp:Label id="lang3116" runat="server">To Date:</asp:Label></td>
											<td><asp:textbox id="txtc" runat="server" Width="120px" CssClass="plainlabel"></asp:textbox></td>
											<td width="50"><IMG onclick="getcal('c');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
													width="19"></td>
											<td width="20"><asp:imagebutton id="Imagebutton1" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm1.gif"></asp:imagebutton></td>
											<td width="20"><IMG onclick="resetsrch();" alt="" src="../images/appbuttons/minibuttons/switch.gif"></td>
										</TR>
									</table>
								</td>
							</tr>
							<tr>
								<td class="labelibl" id="tdsrchmsg" align="center" colSpan="4" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lblcid" type="hidden" name="lblcid" runat="server"> <input id="lblsrch" type="hidden" name="Hidden1" runat="server">
			<input id="lbluser" type="hidden" name="Hidden1" runat="server"> <input id="lblms" type="hidden" name="Hidden1" runat="server">
			<input id="lbldf" type="hidden" name="Hidden1" runat="server"> <input id="lblfiltcnt" type="hidden" name="Hidden1" runat="server">
			<input id="lblfilt" type="hidden" name="Hidden1" runat="server"> <input id="lblhref" type="hidden" name="lblhref" runat="server"><input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
			<input id="lblret" type="hidden" name="lblret" runat="server"><input id="lbltyp" type="hidden" name="lbltyp" runat="server">
			<input id="lbleqid2" type="hidden" name="lbleqid2" runat="server"> <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input id="lblcoid" type="hidden" name="lblcoid" runat="server"><input id="lblappstr" type="hidden" name="lblappstr" runat="server">
			<input type="hidden" id="spdivy" runat="server" NAME="spdivy">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
		</FORM>
	</body>
</HTML>
