<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NewMainMenu2.aspx.vb"
    Inherits="lucy_r12.NewMainMenu2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title id="pgtitle">NewMainMenu2</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/NewMainMenu2aspx_1.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function getotab() {
            var usrname = document.getElementById("lblusername").value;
            var sid = document.getElementById("lblsid").value;
            //alert("../appspmo123tab/pmo123tabmain.aspx?usrname=" + usrname + "&sid=" + sid)
            window.location = "../appspmo123tab/pmo123tabmain.aspx?usrname=" + usrname + "&sid=" + sid;
        }
        function handlesched() {
            var href = document.getElementById("lblhref").value;
            var sid = document.getElementById("lblsid").value;
            var ro = "0"; //document.getElementById("lblro").value;
            var eqid = "0";
            var won = document.getElementById("lblwo").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            href = href.replace(/&/g, "~")
            window.location = "../appsman/PMScheduling2.aspx?jump=yes&typ=wo&href=" + href + "&ro=" + ro + "&sid=" + sid + "&eqid=" + eqid + "&wo=" + won + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        function ctest() {
            parent.ifmain.handlesomething();
        }

        function handleloc(lid) {
            //document.getElementById("lbllid").value = lid;
            //document.getElementById("lbltyp").value = "sys";
            //document.getElementById("lblsubmit").value = "srch";
            //document.getElementById("form1").submit();
            var username = document.getElementById("lblusername").value;
            window.location = "../locs/pmlocmain.aspx?jump=yes&lid=" + lid + "&usrname=" + username;
        }
        function handlejump(id, eid) {
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            //document.getElementById("lbljump").value = "eq";
            //checkarch();
            //document.getElementById("geteq").src="PMGetPMMan.aspx?jump=yes&typ=no&eqid=" + eid + "&pmid=" + id;
            window.location = "../appsman/PMManager.aspx?jump=yes&eqid=" + eid + "&pmid=" + id + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        function handlejumptpm(id, eid) {
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ro = document.getElementById("lblro").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            //document.getElementById("lbljump").value = "eq";
            //checkarch();
            //document.getElementById("geteq").src="PMGetPMMan.aspx?jump=yes&typ=no&eqid=" + eid + "&pmid=" + id+ "&uid=" + "" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
            window.location = "../appsmantpm/TPMManager.aspx?jump=yes&sid=" + sid + "&eqid=" + eid + "&pmid=" + id + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        function gotoreq(href) {
            window.location = "../appswo/wolabmain.aspx?" + href
        }

       
        function gotowonew(href) {
            window.location = href;
        }
        function gotoeq(href) {
            var chk = "yes";
            window.location = href;
        }
        function gotoeqlist(eid) {
            closeall();
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var appstr = document.getElementById("lblappstr").value;
            var ms = document.getElementById("lblms").value;
            document.getElementById("tdeq").className = "thdrhov label";
            document.getElementById("treq").className = 'tdborder view';
            document.getElementById("ifmain").src = "eqtab2.aspx?jump=yes&typ=eq&eqid=" + eid + "&appstr=" + appstr + "&ms=" + ms + "&sid=" + sid + "&username=" + username + "&date=" + Date();
        }
        function gotowolisteq(eid) {
            //alert()
            closeall();
            var wo = ""; //document.getElementById("lblwonum").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            document.getElementById("tdwo").className = "thdrhov label";
            document.getElementById("trwo").className = 'tdborder view';
            document.getElementById("wo").className = 'tdborder view';
            document.getElementById("ifwo").src = "../appswo/wolist1.aspx?who=mm&jump=yes&typ=eq&sid=" + sid + "&wo=" + wo + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&eqid=" + eid + "&fuid=&coid=&Logged_In=" + Logged_In + "&ms=" + ms + "&appstr=" + appstr;
        }
        function gotopmlisteq(eid) {
            closeall();
            document.getElementById("tdpm").className = "thdrhov label";
            document.getElementById("trpm").className = 'tdborder view';
            document.getElementById("pm").className = 'tdborder view';
            var sid = document.getElementById("lblsid").value;
            document.getElementById("ifpm").src = "../appsman/PMMainMan.aspx?tab=yes&jump=yes&typ=eq&sid=" + sid + "&eqid=" + eid + "&pmid=0&fu=0&co=0";
        }
        function gotopmtabeq(eid, sid, did, clid, chk, lid, eq, dept, cell, loc) {
            var ustr = document.getElementById("lblusername").value;
            //alert("../appspmo123tab/pmo123tabmain.aspx?who=eqready&lvl=eq&start=yes&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eid + "&lid=" + lid + "&typ=&dept=" + dept + "&cell=" + cell + "&eq=" + eq + "&loc=" + loc + "&rettyp=&gototasks=1&usrname=" + ustr + "&fuid=&coid=")
           window.location = "../appspmo123tab/pmo123tabmain.aspx?who=eqready&lvl=eq&start=yes&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eid + "&lid=" + lid + "&typ=&dept=" + dept + "&cell=" + cell + "&eq=" + eq + "&loc=" + loc + "&rettyp=&gototasks=1&usrname=" + ustr + "&fuid=&coid=";
       }
       function gotopmtabfu(eid, fid, sid, did, clid, chk, lid, eq, dept, cell, loc) {
           var ustr = document.getElementById("lblusername").value;
           window.location = "../appspmo123tab/pmo123tabmain.aspx?who=eqready&lvl=eq&start=yes&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eid + "&lid=" + lid + "&typ=&dept=" + dept + "&cell=" + cell + "&eq=" + eq + "&loc=" + loc + "&rettyp=&gototasks=1&usrname=" + ustr + "&fuid=" + fid + "&coid=";
       }
       function gotopmtabco(eid, fid, cid, sid, did, clid, chk, lid, eq, dept, cell, loc) {
           var ustr = document.getElementById("lblusername").value;
           window.location = "../appspmo123tab/pmo123tabmain.aspx?who=eqready&lvl=eq&start=yes&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eid + "&lid=" + lid + "&typ=&dept=" + dept + "&cell=" + cell + "&eq=" + eq + "&loc=" + loc + "&rettyp=&gototasks=1&usrname=" + ustr + "&fuid=" + fid + "&coid=" + cid;
       }
        function gotopmopteq(eid, sid, did, clid, chk, lid) {
            var typ;
            if (did != "") {
                if (lid != "" && lid != "0") {
                    typ = "dloc";
                }
                else {
                    typ = "reg";
                }
            }
            else {
                if (lid != "" && lid != "0") {
                    typ = "loc";
                }
            }
            //alert(typ)
            var tli = "5";
            var comid = "0";
            var cell;
            if (clid != "") {
                cell = "yes"
            }
            else {
                cell = "yes"
            }
            var fid = "";
            var ustr = document.getElementById("lbluser").value;
            window.location = "../appsopt/PM3OptMain.aspx?jump=yes&cid=0&tli=" + tli + "&sid=" + sid + "&did=" + did + "&eqid=" + eid + "&clid=" + clid + "&funid=" + fid + "&comid=" + comid + "&chk=" + cell + "&app=pmopt&lid=" + lid + "&typ=" + typ + "&ustr=" + ustr;
        }

        function gototpmopteq(eid, sid, did, clid, chk, lid) {
            var typ;
            if (did != "") {
                if (lid != "" && lid != "0") {
                    typ = "dloc";
                }
                else {
                    typ = "reg";
                }
            }
            else {
                if (lid != "" && lid != "0") {
                    typ = "loc";
                }
            }
            var tli = "5";
            var comid = "0";
            var cell;
            if (clid != "") {
                cell = "yes"
            }
            else {
                cell = "yes"
            }
            var fid = "";
            window.location = "../appsopttpm/tpmopttasksmain.aspx?jump=yes&cid=0&tli=" + tli + "&sid=" + sid + "&did=" + did + "&eqid=" + eid + "&clid=" + clid + "&funid=" + fid + "&comid=" + comid + "&chk=" + cell + "&app=pmopt&lid=" + lid + "&typ=" + typ;
        }

        function gotopmdeveq(eid, sid, did, clid, chk, lid) {
            var typ;
            if (did != "") {
                if (lid != "" && lid != "0") {
                    typ = "dloc";
                }
                else {
                    typ = "reg";
                }
            }
            else {
                if (lid != "" && lid != "0") {
                    typ = "loc";
                }
            }
            var tli = "5";
            var comid = "0";
            var cell;
            if (clid != "") {
                cell = "yes"
            }
            else {
                cell = "yes"
            }
            var fid = "";
            window.location = "../apps/PMTasks.aspx?jump=yes&cid=0&tli=" + tli + "&sid=" + sid + "&did=" + did + "&eqid=" + eid + "&clid=" + clid + "&funid=" + fid + "&comid=" + comid + "&chk=" + cell + "&lid=" + lid + "&typ=" + typ;
        }
        function gototpmdeveq(eid, sid, did, clid, chk, lid) {
            var typ;
            if (did != "") {
                if (lid != "" && lid != "0") {
                    typ = "dloc";
                }
                else {
                    typ = "reg";
                }
            }
            else {
                if (lid != "" && lid != "0") {
                    typ = "loc";
                }
            }
            var tli = "5";
            var comid = "0";
            var cell;
            if (clid != "") {
                cell = "yes"
            }
            else {
                cell = "yes"
            }
            var fid = "";
            window.location = "../appstpm/tpmtasksmain.aspx?jump=yes&cid=0&tli=" + tli + "&sid=" + sid + "&did=" + did + "&eqid=" + eid + "&clid=" + clid + "&funid=" + fid + "&comid=" + comid + "&chk=" + cell + "&lid=" + lid + "&typ=" + typ;
        }
        function gotofu(href) {//eid, fid, sid, did, clid, chk
            var chk = "yes";
            window.location = href;
             }
        function gotofulist(fid) {
            closeall();
            document.getElementById("tdeq").className = "thdrhov label";
            document.getElementById("treq").className = 'tdborder view';
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var appstr = document.getElementById("lblappstr").value;
            var ms = document.getElementById("lblms").value;
            document.getElementById("ifmain").src = "eqtab2.aspx?jump=yes&typ=fu&fuid=" + fid + "&appstr=" + appstr + "&ms=" + ms + "&sid=" + sid + "&username=" + username + "&date=" + Date();
       
            //document.getElementById("ifmain").src = "eqtab2.aspx?jump=yes&typ=fu&fuid=" + fid + "&appstr=" + appstr + "&ms=" + ms + "&sid=" + sid + "&username=" + username + "&date=" + Date();
        }
        function gotowolistfu(fid, eid) {
            closeall();
            document.getElementById("tdwo").className = "thdrhov label";
            document.getElementById("trwo").className = 'tdborder view';
            document.getElementById("wo").className = 'tdborder view';
            var wo = ""; //document.getElementById("lblwonum").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            document.getElementById("tdwo").className = "thdrhov label";
            document.getElementById("trwo").className = 'tdborder view';
            document.getElementById("wo").className = 'tdborder view';
            document.getElementById("ifwo").src = "../appswo/wolist1.aspx?who=mm&jump=yes&typ=eq&sid=" + sid + "&wo=" + wo + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&eqid=" + eid + "&fuid=" + fid + "&coid=&Logged_In=" + Logged_In + "&ms=" + ms + "&appstr=" + appstr;
        }
        function gotopmlistfu(fid, eid) {
            closeall();
            document.getElementById("tdpm").className = "thdrhov label";
            document.getElementById("trpm").className = 'tdborder view';
            document.getElementById("pm").className = 'tdborder view';
            var sid = document.getElementById("lblsid").value;
            document.getElementById("ifpm").src = "../appsman/PMMainMan.aspx?jump=yes&typ=fu&sid=" + sid + "&eqid=" + eid + "&pmid=0&fuid=" + fid + "&co=0";
        }
        function gotopmoptfu(eid, fid, sid, did, clid, chk, lid) {
            var typ;
            if (did != "") {
                if (lid != "" && lid != "0") {
                    typ = "dloc";
                }
                else {
                    typ = "reg";
                }
            }
            else {
                if (lid != "" && lid != "0") {
                    typ = "loc";
                }
            }
            var tli = "5";
            var comid = "0";
            var cell;
            if (clid != "") {
                cell = "yes"
            }
            else {
                cell = "yes"
            }
            var ustr = document.getElementById("lbluser").value;
            window.location = "../appsopt/PM3OptMain.aspx?jump=yes&cid=0&tli=" + tli + "&sid=" + sid + "&did=" + did + "&eqid=" + eid + "&clid=" + clid + "&funid=" + fid + "&comid=" + comid + "&chk=" + cell + "&app=pmopt&lid=" + lid + "&typ=" + typ + "&ustr=" + ustr;
        }
        function gototpmoptfu(eid, fid, sid, did, clid, chk, lid) {
            var typ;
            if (did != "") {
                if (lid != "" && lid != "0") {
                    typ = "dloc";
                }
                else {
                    typ = "reg";
                }
            }
            else {
                if (lid != "" && lid != "0") {
                    typ = "loc";
                }
            }
            var tli = "5";
            var comid = "0";
            var cell;
            if (clid != "") {
                cell = "yes"
            }
            else {
                cell = "yes"
            }
            window.location = "../appsopttpm/tpmopttasksmain.aspx?jump=yes&cid=0&tli=" + tli + "&sid=" + sid + "&did=" + did + "&eqid=" + eid + "&clid=" + clid + "&funid=" + fid + "&comid=" + comid + "&chk=" + cell + "&app=pmopt&lid=" + lid + "&typ=" + typ;
        }
        function gotopmdevfu(eid, fid, sid, did, clid, chk, lid) {
            var typ;
            if (did != "") {
                if (lid != "" && lid != "0") {
                    typ = "dloc";
                }
                else {
                    typ = "reg";
                }
            }
            else {
                if (lid != "" && lid != "0") {
                    typ = "loc";
                }
            }
            var tli = "5";
            var comid = "0";
            var cell;
            if (clid != "") {
                cell = "yes"
            }
            else {
                cell = "yes"
            }
            window.location = "../apps/PMTasks.aspx?jump=yes&cid=0&tli=" + tli + "&sid=" + sid + "&did=" + did + "&eqid=" + eid + "&clid=" + clid + "&funid=" + fid + "&comid=" + comid + "&chk=" + cell + "&lid=" + lid + "&typ=" + typ;
        }
        function gototpmdevfu(eid, fid, sid, did, clid, chk, lid) {
            var typ;
            if (did != "") {
                if (lid != "" && lid != "0") {
                    typ = "dloc";
                }
                else {
                    typ = "reg";
                }
            }
            else {
                if (lid != "" && lid != "0") {
                    typ = "loc";
                }
            }
            var tli = "5";
            var comid = "0";
            var cell;
            if (clid != "") {
                cell = "yes"
            }
            else {
                cell = "yes"
            }
            window.location = "../appstpm/tpmtasksmain.aspx?jump=yes&cid=0&tli=" + tli + "&sid=" + sid + "&did=" + did + "&eqid=" + eid + "&clid=" + clid + "&funid=" + fid + "&comid=" + comid + "&chk=" + cell + "&lid=" + lid + "&typ=" + typ;
        }
        function gotoco(href) {//eid, fid, cid, sid, did, clid, chk
            var chk = "yes";
            window.location = href;
             }
        function gotowolistco(cid, fid, eid) {
            closeall();
            var wo = document.getElementById("lblwonum").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;

            document.getElementById("tdwo").className = "thdrhov label";
            document.getElementById("trwo").className = 'tdborder view';
            document.getElementById("wo").className = 'tdborder view';
            //"../appswo/wolist1.aspx?who=mm&jump=yes&typ=eq&sid=" + sid + "&wo=" + wo + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&eqid=" + eid + "&fuid=" + fid + "&coid=&Logged_In=" + Logged_In + "&ms=" + ms + "&appstr=" + appstr;
            document.getElementById("ifwo").src = "../appswo/wolist1.aspx?who=mm&jump=yes&typ=co&sid=" + sid + "&wo=" + wo + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&eqid=" + eid + "&pmid=0&fuid=" + fid + "&coid=" + cid + "&Logged_In=" + Logged_In + "&ms=" + ms + "&appstr=" + appstr;
        }
        function gotopmlistco(cid, fid, eid) {
            closeall();
            document.getElementById("tdpm").className = "thdrhov label";
            document.getElementById("trpm").className = 'tdborder view';
            document.getElementById("pm").className = 'tdborder view';
            var wo = ""; //document.getElementById("lblwonum").value;
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var username = document.getElementById("lblusername").value;
            var islabor = document.getElementById("lblislabor").value;
            var issuper = document.getElementById("lblissuper").value;
            var isplanner = document.getElementById("lblisplanner").value;
            var Logged_In = document.getElementById("lblLogged_In").value;
            var ms = document.getElementById("lblms").value;
            var appstr = document.getElementById("lblappstr").value;
            document.getElementById("tdwo").className = "thdrhov label";
            document.getElementById("trwo").className = 'tdborder view';
            document.getElementById("wo").className = 'tdborder view';
            document.getElementById("ifwo").src = "../appswo/wolist1.aspx?who=mm&jump=yes&typ=eq&sid=" + sid + "&wo=" + wo + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&eqid=" + eid + "&fuid=" + fid + "&coid=" + cid + "&Logged_In=" + Logged_In + "&ms=" + ms + "&appstr=" + appstr; ;
        }
        function gotopmoptco(eid, fid, cid, sid, did, clid, chk, lid) {
            var typ;
            if (did != "") {
                if (lid != "" && lid != "0") {
                    typ = "dloc";
                }
                else {
                    typ = "reg";
                }
            }
            else {
                if (lid != "" && lid != "0") {
                    typ = "loc";
                }
            }
            var tli = "5";
            var comid = cid;
            var cell;
            if (clid != "") {
                cell = "yes"
            }
            else {
                cell = "yes"
            }
            var ustr = document.getElementById("lbluser").value;
            window.location = "../appsopt/PM3OptMain.aspx?tab=yes&jump=yes&cid=0&tli=" + tli + "&sid=" + sid + "&did=" + did + "&eqid=" + eid + "&clid=" + clid + "&funid=" + fid + "&comid=" + comid + "&chk=" + cell + "&app=pmopt&lid=" + lid + "&typ=" + typ + "&ustr=" + ustr;
        }
        function gototpmoptco(eid, fid, cid, sid, did, clid, chk, lid) {
            var typ;
            if (did != "") {
                if (lid != "" && lid != "0") {
                    typ = "dloc";
                }
                else {
                    typ = "reg";
                }
            }
            else {
                if (lid != "" && lid != "0") {
                    typ = "loc";
                }
            }
            var tli = "5";
            var comid = cid;
            var cell;
            if (clid != "") {
                cell = "yes"
            }
            else {
                cell = "yes"
            }
            window.location = "../appsopttpm/tpmopttasksmain.aspx?jump=yes&cid=0&tli=" + tli + "&sid=" + sid + "&did=" + did + "&eqid=" + eid + "&clid=" + clid + "&funid=" + fid + "&comid=" + comid + "&chk=" + cell + "&app=pmopt&lid=" + lid + "&typ=" + typ;
        }
        function gotopmdevco(eid, fid, cid, sid, did, clid, chk, lid) {
            var typ;
            if (did != "") {
                if (lid != "" && lid != "0") {
                    typ = "dloc";
                }
                else {
                    typ = "reg";
                }
            }
            else {
                if (lid != "" && lid != "0") {
                    typ = "loc";
                }
            }
            var tli = "5";
            var comid = cid;
            var cell;
            if (clid != "") {
                cell = "yes"
            }
            else {
                cell = "yes"
            }
            window.location = "../apps/PMTasks.aspx?jump=yes&cid=0&tli=" + tli + "&sid=" + sid + "&did=" + did + "&eqid=" + eid + "&clid=" + clid + "&funid=" + fid + "&comid=" + comid + "&chk=" + cell + "&lid=" + lid + "&typ=" + typ;
        }
        function gototpmdevco(eid, fid, cid, sid, did, clid, chk, lid) {
            var typ;
            if (did != "") {
                if (lid != "" && lid != "0") {
                    typ = "dloc";
                }
                else {
                    typ = "reg";
                }
            }
            else {
                if (lid != "" && lid != "0") {
                    typ = "loc";
                }
            }
            var tli = "5";
            var comid = cid;
            var cell;
            if (clid != "") {
                cell = "yes"
            }
            else {
                cell = "yes"
            }
            window.location = "../appstpm/tpmtasksmain.aspx?jump=yes&cid=0&tli=" + tli + "&sid=" + sid + "&did=" + did + "&eqid=" + eid + "&clid=" + clid + "&funid=" + fid + "&comid=" + comid + "&chk=" + cell + "&lid=" + lid + "&typ=" + typ;
        }
        function fclose(td, img) {
            var str = document.getElementById(img).src
            var lst = str.lastIndexOf("/") + 1
            var loc = str.substr(lst)
            if (loc == 'minus.gif') {
                document.getElementById(img).src = '../images/appbuttons/bgbuttons/plus.gif';
                try {
                    document.getElementById(td).className = "details";
                }
                catch (err) {

                }
            }
            else {
                document.getElementById(img).src = '../images/appbuttons/bgbuttons/minus.gif';
                try {
                    document.getElementById(td).className = "viewcell";
                }
                catch (err) {

                }
            }
        }

        function getinprog() {
            window.open("../reports/inprog.aspx?date=" + Date());
        }
        function getcal(fld) {
            
            var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
            if (eReturn) {
                fld = "txt" + fld;
                document.getElementById(fld).value = eReturn;
                //document.getElementById("form1").submit();
            }
        }
        function showAns(id, h, imgid) {
            //alert(imgid)
            var row
            if (h == "top") row = "d";
            else if (h == "topa") row = "e";
            else if (h == "topf") row = "f";
            else if (h == "topg") row = "g";

            var ans = document.getElementById(row + id);
            //alert(row + id)
            if (ans) {
                if (ans.style.display != 'block') {
                    ans.style.display = 'block';
                    ans.style.visibility = 'visible';
                    document.getElementById(imgid).src = "../images/appbuttons/bgbuttons/minus.gif";
                }
                else {
                    ans.style.display = 'none';
                    ans.style.visibility = 'hidden';
                    document.getElementById(imgid).src = "../images/appbuttons/bgbuttons/plus.gif";
                }
            }
        }
        function checkit() {
            //window.setTimeout("setref();", 1205000);
        }
        function setref() {
            //document.getElementById("form1").submit(); 
            //window.location="../NewLogin.aspx?app=none&lo=yes"
        }
        var navflag = 0
        function opennav() {
            if (navflag == 0) {
                navflag = 1;
                document.getElementById("l1").className = "thdrsingtbo viewcell";

                document.getElementById("l2").className = "thdrsingrt label viewcell";

                document.getElementById("l3").className = "viewcell";

                document.getElementById("e1").className = "details";
                document.getElementById("e2").className = "details";
                document.getElementById("tdarch").className = "details";
                document.getElementById("imgnav").src = "../images/appbuttons/minibuttons/close1.gif";
            }
            else {
                navflag = 0;
                document.getElementById("l1").className = "details";
                document.getElementById("l2").className = "details";
                document.getElementById("l3").className = "details";
                document.getElementById("e1").className = "thdrsingtbo viewcell";
                document.getElementById("e2").className = "thdrsingrt label viewcell";
                document.getElementById("tdarch").className = "viewcell";
                document.getElementById("imgnav").src = "../images/appbuttons/minibuttons/open.gif";
            }
        }
        function handlehref(href) {
            //alert(href)
            window.location = href;
        }

        function handlelogout() {
            document.getElementById("form1").submit();
        }
        function checklogout() {
            document.getElementById("lblsess1").value = "logout";
            document.getElementById("form1").submit();
        } //
        function checklogout1() {
            document.getElementById("lbllogcheck").value = "go"
            document.getElementById("form1").submit();
        }
        function checkalert() {
            //alert("test");
        }

        //onunload="checklogout1();"


        function gettab(name) {
            closeall();
            if (name == "tdpm") {
                document.getElementById("tdpm").className = "thdrhov label";
                document.getElementById("trpm").className = 'tdborder viewrow';
                document.getElementById("pm").className = 'tdborder view';
                sid = document.getElementById("lblsid").value;
                document.getElementById("ifpm").src = "../appsman/PMMainMan.aspx?tab=yes&sid=" + sid;
                //alert(document.getElementById("trpm").className)
            }
            else if (name == "tdtpm") {
                document.getElementById("tdtpm").className = "thdrhov label";
                document.getElementById("trtpm").className = 'tdborder viewrow';
                document.getElementById("tpm").className = 'tdborder viewrow';
                sid = document.getElementById("lblsid").value;
                document.getElementById("iftpm").src = "../appsmantpm/PMMainMantpm.aspx?sid=" + sid;
                //alert(document.getElementById("trpm").className)
            }
            else if (name == "tdeq") {
                document.getElementById("tdeq").className = "thdrhov label";
                document.getElementById("treq").className = 'tdborder viewrow';

            }
            else if (name == "tdwo") {
                document.getElementById("tdwo").className = "thdrhov label";
                document.getElementById("trwo").className = 'tdborder viewrow';
                document.getElementById("wo").className = 'tdborder viewrow';
                var wo = document.getElementById("lblwonum").value;
                var sid = document.getElementById("lblsid").value;
                var uid = document.getElementById("lbluid").value;
                var username = document.getElementById("lblusername").value;
                var islabor = document.getElementById("lblislabor").value;
                var issuper = document.getElementById("lblissuper").value;
                var isplanner = document.getElementById("lblisplanner").value;
                var Logged_In = document.getElementById("lblLogged_In").value;
                var ms = document.getElementById("lblms").value;
                var appstr = document.getElementById("lblappstr").value;
                document.getElementById("ifwo").src = "../appswo/wolist1.aspx?jump=no&who=mm&sid=" + sid + "&wo=" + wo + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&eqid=&fuid=&coid=&Logged_In=" + Logged_In + "&ms=" + ms + "&appstr=" + appstr;
            }
        }
        function closeall() {
            var getman = document.getElementById("lblgetman").value;
            var gettan = document.getElementById("lblgettan").value;
            var getwo = document.getElementById("lblgetwo").value;

            document.getElementById("tdeq").className = "thdr label";
            document.getElementById("treq").className = 'details';
            if (getman == "0") {
                document.getElementById("tdpm").className = "thdr label";
                document.getElementById("trpm").className = 'details';
            }
            if (gettan == "0") {
                document.getElementById("tdtpm").className = "thdr label";
                document.getElementById("trtpm").className = 'details';
            }
            if (getwo == "0") {
                document.getElementById("tdwo").className = "thdr label";
                document.getElementById("trwo").className = 'details';
            }
        }</script>
</head>
<body class="tbg" >
    <form id="form1" method="post" runat="server">
    <table style="z-index: 1; position: absolute; background-color: transparent; top: 82px;
        left: 798px" cellspacing="0" cellpadding="0" width="269">
        <tr>
            <td id="l1" class="thdrsingtbo details" width="26" align="left">
                <img border="0" src="../images/appbuttons/minibuttons/lodhdr.gif">
            </td>
            <td id="l2" class="thdrsingrt label details" width="217">
                <asp:Label ID="lang3130" runat="server">Location Hierarchy</asp:Label>
            </td>
            <td id="e1" class="thdrsingtbo" width="26">
                <img border="0" src="../images/appbuttons/minibuttons/eqarch.gif">
            </td>
            <td id="e2" class="thdrsingrt label" width="257" align="left">
                <asp:Label ID="lang3131" runat="server">Plant Site Assets</asp:Label>
            </td>
        </tr>
        <tr>
            <td id="l3" class="details" colspan="2">
                <iframe style="border-bottom-style: none; padding-bottom: 0px; border-right-style: none;
                    background-color: transparent; margin: 0px; padding-left: 0px; padding-right: 0px;
                    border-top-style: none; border-left-style: none; padding-top: 0px" id="ifloc"
                    height="480" src="../genhold.htm" frameborder="no" width="263" allowtransparency
                    scrolling="yes" runat="server"></iframe>
            </td>
            <td id="tdarch" valign="top" colspan="2" runat="server">
                <iframe style="border-bottom-style: none; padding-bottom: 0px; border-right-style: none;
                    background-color: transparent; margin: 0px; padding-left: 0px; padding-right: 0px;
                    border-top-style: none; border-left-style: none; padding-top: 0px" id="ifarch"
                    height="524" src="../genhold.htm" frameborder="no" width="295" allowtransparency
                    scrolling="yes" runat="server"></iframe>
            </td>
        </tr>
    </table>
    <table style="z-index: 1; position: absolute; background-color: transparent; top: 82px;
        left: 7px" cellspacing="0" cellpadding="0" width="790">
        <tbody>
            <tr>
                <td class="thdrsinglft" width="26">
                    <img border="0" src="../images/appbuttons/minibuttons/pm3.gif">
                </td>
                <td class="thdrsingcntr label" width="464" align="left">
                    <asp:Label ID="lang3132" runat="server">Your PM Records</asp:Label>
                </td>
                <td class="thdrsingrt label" width="300" align="center">
                   &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center" id="tdmsg1" runat="server" colspan="3" class="plainlabelblue">
                    <img src="../images/appbuttons/minibuttons/14PX.gif" alt="" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <table>
                        <tr height="22">
                            <td id="tdeq" class="thdrhov label" onclick="gettab('tdeq');" width="210">
                                <asp:Label ID="lang3133" runat="server">Equipment Records</asp:Label>
                            </td>
                            <td id="tdpm" class="thdr label" width="160" runat="server">
                                <asp:Label ID="lang3134" runat="server">PM Records</asp:Label>
                            </td>
                            <td id="tdtpm" class="thdr label" width="160" runat="server">
                                <asp:Label ID="lang3135" runat="server">TPM Records</asp:Label>
                            </td>
                            <td id="tdwo" class="thdr label" width="150" runat="server">
                                <asp:Label ID="lang3136" runat="server">Work Orders</asp:Label>
                            </td>
                            <td width="12">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="treq">
                <td id="eq" class="tdborder" valign="top" colspan="3">
                    <table style="background-color: transparent" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <iframe style="background-color: transparent" id="ifmain" height="482" src="../genhold.htm"
                                    frameborder="no" width="790" allowtransparency runat="server"></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trpm" class="details">
                <td id="pm" valign="top" colspan="3">
                    <table style="background-color: transparent" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <iframe style="background-color: transparent" id="ifpm" height="482" src="../genhold.htm"
                                    frameborder="no" width="790" allowtransparency runat="server"></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trtpm" class="details">
                <td id="tpm" valign="top" colspan="3">
                    <table style="background-color: transparent" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <iframe id="iftpm" height="482" src="../genhold.htm" frameborder="no" width="790"
                                    runat="server"></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trwo" class="details">
                <td id="wo" valign="top" colspan="3">
                    <table style="background-color: transparent" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <iframe id="ifwo" height="482" src="../genhold.htm" frameborder="no" width="790"
                                    runat="server"></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <input id="lbluser" type="hidden" name="lbluser" runat="server"><input id="lblcid"
        type="hidden" name="lblcid" runat="server">
    <input id="txtpg" type="hidden" name="txtpg" runat="server"><input id="lblms" type="hidden"
        name="lblms" runat="server">
    <input id="lblfilt" type="hidden" name="lblfilt" runat="server"><input id="lblfiltcnt"
        type="hidden" name="lblfiltcnt" runat="server">
    <input id="lblsrch" type="hidden" name="lblsrch" runat="server">
    <input id="lbldf" type="hidden" name="lbldf" runat="server">
    <input id="lblsess" type="hidden" runat="server"><input id="lblsess1" type="hidden"
        name="lblsess1" runat="server">
    <input id="lbllogcheck" type="hidden" runat="server">
    <input id="lblfslang" type="hidden" runat="server">
    <input id="lblgetwo" type="hidden" runat="server">
    <input id="lblgetman" type="hidden" runat="server">
    <input id="lblgettan" type="hidden" runat="server"><input id="lblro" type="hidden"
        name="lblro" runat="server">
    <input id="lblwonum" type="hidden" name="lblwonum" runat="server">
    <input id="lblusername" type="hidden" name="lblusername" runat="server">
    <input id="lbluid" type="hidden" name="lbluid" runat="server">
    <input id="lblislabor" type="hidden" name="lblislabor" runat="server">
    <input id="lblissuper" type="hidden" name="lblissuper" runat="server">
    <input id="lblisplanner" type="hidden" name="lblisplanner" runat="server">
    <input id="lblsid" type="hidden" name="Hidden1" runat="server">
    <input id="lblappstr" type="hidden" runat="server">
    <input type="hidden" id="lblLogged_In" runat="server" name="lblLogged_In">
    <input type="hidden" id="Hidden1" runat="server" name="lblro">
    <input type="hidden" id="Hidden2" runat="server" name="lblms">
    <input type="hidden" id="Hidden3" runat="server" name="lblappstr">
    <input type="hidden" id="lblhref" runat="server" />
    <input type="hidden" id="lblwo" runat="server" />
    </form>
    <uc1:mmenu1 ID="Mmenu11" runat="server"></uc1:mmenu1>
</body>
</html>
