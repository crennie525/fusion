﻿Imports System.Data.SqlClient
Public Class toexcel
    Inherits System.Web.UI.Page
    Dim mm As New Utilities
    Dim typ, sql As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            typ = Request.QueryString("typ").ToString
            lbltyp.Value = typ
            Try
                lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
            Catch ex As Exception
                Dim dlang As New mmenu_utils_a
                lblfslang.Value = dlang.AppDfltLang
            End Try
            If typ = "COMPwk" Then
                mm.Open()
                BindExportclwk()
                mm.Dispose()
            Else
                'If Request.Form("lblret") = "excelclwk" Then
                'mm.Open()
                'BindExportclwk()
                'mm.Dispose()
            End If
        End If
    End Sub
    Private Sub BindExportclwk()
        Dim lang As String = lblfslang.Value
        If lang = "FRE" Or lang = "fre" Then
            sql = "usp_getcompliballwklyf"
        Else
            sql = "usp_getcompliballwkly"
        End If

        Dim ds As New DataSet
        ds = mm.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
End Class