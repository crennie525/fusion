<%@ Page Language="vb" AutoEventWireup="false" Codebehind="saadmin.aspx.vb" Inherits="lucy_r12.saadmin" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>saadmin</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/saadminaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="label" height="22"><asp:Label id="lang3137" runat="server">Search Started</asp:Label></td>
					<td class="plainlabel" id="tdss" runat="server"><asp:Label id="lang3138" runat="server">Default</asp:Label></td>
				</tr>
				<tr>
					<td class="label" height="22"><asp:Label id="lang3139" runat="server">Server Page Accessed</asp:Label></td>
					<td class="plainlabel" id="tdspa" runat="server"><asp:Label id="lang3140" runat="server">Not Provided</asp:Label></td>
				</tr>
				<tr>
					<td class="label" height="22"><asp:Label id="lang3141" runat="server">Search Completed at Server</asp:Label></td>
					<td class="plainlabel" id="tdscs" runat="server"><asp:Label id="lang3142" runat="server">Not Provided</asp:Label></td>
				</tr>
				<tr>
					<td class="label" height="22"><asp:Label id="lang3143" runat="server">Client Page Loaded</asp:Label></td>
					<td class="plainlabel" id="tdcpl" runat="server"><asp:Label id="lang3144" runat="server">Not Provided</asp:Label></td>
				</tr>
				<tr id="trmsg" runat="server" class="details">
					<td colspan="2" align="center" class="plainlabelblue" height="20"><asp:Label id="lang3145" runat="server">Increasing the Page Size will result in longer search times</asp:Label></td>
				</tr>
				<tr id="trpg" runat="server"  class="details">
					<td colspan="2" align="center">
						<table>
							<tr>
								<td class="label"  height="22"><asp:Label id="lang3146" runat="server">Page Size</asp:Label></td>
								<td><asp:TextBox id="txtps" runat="server" Width="50px"></asp:TextBox></td>
								<td><img src="../images/appbuttons/minibuttons/saveDisk1.gif" onclick="getpg();"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lblpg" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
