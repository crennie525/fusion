

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class siteassets3
    Inherits System.Web.UI.Page


    Protected WithEvents lang3181 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3180 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3179 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3178 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3177 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3176 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3175 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3174 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3173 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3172 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3171 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3170 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim nmm As New Utilities
    Dim appstr, srch, siteid, comp As String
    Dim PageNumber As Integer
    Dim PageSize As Integer = 50
    Dim intPgNav As Integer
    Dim wo, uid, username, islabor, isplanner, issuper, Logged_In, ro, ms, ustr As String
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblislabor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblissuper As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblLogged_In As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblms As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisplanner As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents rb1 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rb8 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rb9 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rb10 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rb11 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rb5 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rb7 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rb6 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rb4 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rb2 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rb12 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdarch As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsiteid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblt1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblt2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblt3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpagesize As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        lblt2.Value = Now
        If Not IsPostBack Then
            Logged_In = Request.QueryString("Logged_In").ToString
            lblLogged_In.Value = Logged_In
            ro = Request.QueryString("ro").ToString
            lblro.Value = ro
            ms = Request.QueryString("ms").ToString
            lblms.Value = ms
            'siteid = HttpContext.Current.Session("dfltps").ToString()
            comp = "0" 
            siteid = Request.QueryString("sid").ToString
            lblsiteid.Value = siteid
            lblcomp.Value = comp
            'appstr = HttpContext.Current.Session("appstr").ToString()
            Try
                uid = Request.QueryString("uid").ToString
            Catch ex As Exception
                uid = Request.QueryString("userid").ToString
            End Try
            lbluid.Value = uid
            Try
                username = Request.QueryString("usrname").ToString
            Catch ex As Exception
                username = Request.QueryString("username").ToString
            End Try
            lblusername.Value = username
            islabor = Request.QueryString("islabor").ToString
            lblislabor.Value = islabor
            issuper = Request.QueryString("issuper").ToString
            lblissuper.Value = issuper
            isplanner = Request.QueryString("isplanner").ToString
            lblisplanner.Value = isplanner
            appstr = Request.QueryString("appstr").ToString
            lblappstr.Value = appstr
            CheckApps(appstr)
            txtpg.Value = "1"
            lblpagesize.Value = PageSize
            nmm.Open()
            GetCount()
            GetArch()
            nmm.Dispose()

        Else
            If Request.Form("lblret") = "next" Then
                nmm.Open()
                GetNext()
                nmm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                nmm.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetArch()
                nmm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                nmm.Open()
                GetPrev()
                nmm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                nmm.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetArch()
                nmm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "srch" Then
                nmm.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetCount()
                GetArch()
                nmm.Dispose()
                lblret.Value = ""
            End If
        End If

    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetArch()
        Catch ex As Exception
            nmm.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1562", "siteassets3.aspx.vb")

            nmm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetArch()
        Catch ex As Exception
            nmm.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1563", "siteassets3.aspx.vb")

            nmm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub CheckApps(ByVal appstr As String)
        Dim apparr() As String = appstr.Split(",")
        'eq,dev,opt,inv
        Dim mutils As New mmenu_utils_a
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                Select Case apparr(i)
                    Case "opt"
                        'If mutils.GetOpt = 0 Then
                        'rb8.Disabled = False
                        'Else
                        'rb8.Disabled = True
                        'End If

                    Case "dev"
                        'If mutils.GetDev = 0 Then
                        'rb9.Disabled = False
                        'Else
                        'rb9.Disabled = True
                        'End If

                    Case "man"
                        If mutils.GetMan = 0 Then
                            rb7.Disabled = False
                        Else
                            rb7.Disabled = True
                        End If

                    Case "wo", "wr"
                        'If mutils.GetWR = 0 Then
                        'rb6.Disabled = False
                        'Else
                        'rb6.Disabled = True
                        'End If
                        'If mutils.GetWO = 0 Then
                        'rb4.Disabled = False
                        'Else
                        'rb4.Disabled = True
                        'End If

                    Case "eq"
                        'If mutils.GetEQ = 0 Then
                        'rb1.Disabled = False
                        'rb1.Checked = True
                        'rb5.Disabled = False
                        'Else
                        'rb1.Disabled = True
                        'rb1.Checked = False
                        'rb5.Disabled = True
                        'End If

                    Case "tpd"
                        'If mutils.GetTPD = 0 Then
                        'rb10.Disabled = False
                        'Else
                        'rb10.Disabled = True
                        'End If

                    Case "tpo"
                        'If mutils.GetTPO = 0 Then
                        'rb11.Disabled = False
                        'Else
                        'rb11.Disabled = True
                        'End If

                    Case "otab"
                        If mutils.GetOTab = 0 Then
                            'rb1.Checked = False
                            rb12.Disabled = False
                            rb12.Checked = True
                        Else
                            rb12.Disabled = True
                        End If

                End Select
            Next

        Else
            If mutils.GetEQ = 0 Then
                'rb1.Disabled = False
                'rb1.Checked = True
                'rb5.Disabled = False
            Else
                'rb1.Disabled = True
                'rb1.Checked = False
                'rb5.Disabled = True
            End If
            If mutils.GetOTab = 0 Then
                'rb1.Checked = False
                rb12.Disabled = False
                rb12.Checked = True
            Else
                rb12.Disabled = True
            End If

            If mutils.GetWR = 0 Then
                rb6.Disabled = False
            Else
                rb6.Disabled = True
            End If
            'If mutils.GetWO = 0 Then
            'rb4.Disabled = False
            'Else
            'rb4.Disabled = True
            'End If

            If mutils.GetMan = 0 Then
                rb7.Disabled = False
            Else
                rb7.Disabled = True
            End If

            If mutils.GetDev = 0 Then
                'rb9.Disabled = False
            Else
                'rb9.Disabled = True
            End If

            If mutils.GetOpt = 0 Then
                'rb8.Disabled = False
            Else
                'rb8.Disabled = True
            End If

            If mutils.GetTPD = 0 Then
                'rb10.Disabled = False
            Else
                'rb10.Disabled = True
            End If

            If mutils.GetTPO = 0 Then
                'rb11.Disabled = False
            Else
                'rb11.Disabled = True
            End If
        End If
        'If rb1.Disabled = True And rb12.Disabled = True Then
        'rb2.Checked = True
        'End If
    End Sub
    Private Sub GetCount()
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", " ", , , vbTextCompare)
        siteid = lblsiteid.Value
        comp = lblcomp.Value
        If srch = "" Then
            srch = "%"
        Else
            srch = "%" & srch & "%"
        End If
        sql = "SELECT count(*) FROM equipment where siteid = '" & siteid & "' and " _
        + "(eqnum like '" & srch & "' or eqdesc like '" & srch & "' or spl like '" & srch & "')"
        PageSize = lblpagesize.Value
        intPgNav = nmm.PageCount(sql, PageSize)
        txtpgcnt.Value = intPgNav
    End Sub
    Private Sub GetArch()
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", " ", , , vbTextCompare)
        siteid = lblsiteid.Value
        comp = lblcomp.Value

        Dim eqnum, eqdesc, dept, cell As String
        PageNumber = txtpg.Value
        PageSize = lblpagesize.Value

        intPgNav = txtpgcnt.Value
        lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        Dim sb As StringBuilder = New StringBuilder

        sb.Append("<table cellspacing=""0"" border=""0""><tr>")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""195""></tr>" & vbCrLf)

        sql = "exec usp_getAllSAPg2 '" & comp & "','" & siteid & "','" & PageNumber & "','" & PageSize & "','" & srch & "'"

        Dim ds As New DataSet
        ds = nmm.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)

        Dim trans As String = "0"
        Dim tstat As String = "0"
        '*** End Multi Add ***
        Dim locby As String
        Dim lock As String = "0"
        Dim fid As String = "0"
        Dim cid As String = "0"
        Dim eid As String = "0"
        Dim cidhold As Integer = 0
        Dim sid, did, clid, chk, lid, loc As String
        Dim epcnt As Integer = 0
        Dim fpcnt As Integer = 0
        Dim cpcnt As Integer = 0
        Dim cnt As Integer = 0
        Dim totcnt As Integer = 0
        Dim parent, parentid As String
        Dim row As DataRow
        Dim eper As Integer
        For Each row In dt.Rows
            totcnt += 1
            If row("eqid") <> eid Then
                If eid <> 0 Then
                    sb.Append("</table></td></tr>")
                End If
                dept = row("dept").ToString
                dept = Replace(dept, "#", "%23")
                cell = row("cell").ToString
                cell = Replace(cell, "#", "%23")
                eid = row("eqid").ToString
                sid = row("siteid").ToString
                did = row("dept_id").ToString
                clid = row("cellid").ToString
                lid = row("locid").ToString
                loc = row("location").ToString
                loc = Replace(loc, "#", "%23")
                epcnt = row("epcnt").ToString
                If clid <> "" Then
                    chk = "yes"
                Else
                    chk = "no"
                End If
                eqnum = row("eqnum").ToString
                eper = eqnum.IndexOf("%")
                If eper <> -1 Then
                    fixeq(eid, eqnum)
                    eqnum = nmm.ModString3(eqnum)
                End If
                eqdesc = row("eqdesc").ToString
                lock = row("locked").ToString
                locby = row("lockedby").ToString
                parent = row("parent").ToString
                parentid = row("parentid").ToString
                'If eqnum = loc Then
                'If parent <> "" Then
                'Dim parar() As String = parent.Split("~")
                'lid = parar(0)
                'loc = parar(1)
                'End If
                'End If
                sb.Append("<tr><td><img id='i" + eid + "' ")
                sb.Append("onclick=""getdets('" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & parentid & "','" & dept & "','" & cell & "','" & eqnum & "','" & parent & "');""")
                sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>")
                If epcnt <> 0 Then
                    sb.Append("<td><img src=""../images/appbuttons/minibuttons/gridpic.gif"" onclick=""geteqport('" + eid + "')""></td>" & vbCrLf)
                Else
                    sb.Append("<td><img src=""../images/appbuttons/minibuttons/gridpicdis.gif""></td>" & vbCrLf)
                End If
                If lid = "" Then
                    sb.Append("<td colspan=""2"" class=""plainlabel""><a href=""#""")
                    'sb.Append("<a class=""A1"" href=""#"" onclick=""getdeq('" & did & "','" & clid & "','" & eqid & "','" & eqnum & "','" & eqdesc & "','" & mcnt & "','" & dept & "','" & cell & "')"">")
                    sb.Append("onclick=""gotoeq('" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & parentid & "','" & dept & "','" & cell & "','" & eqnum & "','" & parent & "')""")
                    sb.Append("class=""linklabel"" >" & eqnum & "</a> - " & eqdesc)
                Else
                    sb.Append("<td colspan=""2"" class=""plainlabel""><a href=""#""")
                    'sb.Append("<a class=""A1"" href=""#"" onclick=""getdeq('" & did & "','" & clid & "','" & eqid & "','" & eqnum & "','" & eqdesc & "','" & mcnt & "','" & dept & "','" & cell & "')"">")
                    sb.Append("onclick=""gotoeq('" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & parentid & "','" & dept & "','" & cell & "','" & eqnum & "','" & parent & "')""")
                    sb.Append("class=""linklabel"" >" & eqnum & " - " & loc & "</a> - " & eqdesc)
                End If



                '*** Multi Add ***
                trans = row("trans").ToString
                If trans = "0" OrElse Len(trans) = 0 Then
                    'sb.Append("</td></tr>" & vbCrLf)
                    If lock = "0" OrElse Len(lock) = 0 Then
                        sb.Append("</td></tr>" & vbCrLf)
                    Else
                        sb.Append("&nbsp;<img src='../images/appbuttons/minibuttons/lillock.gif' ")
                        sb.Append("onmouseover=""return overlib('" & tmod.getov("cov313", "siteassets3.aspx.vb") & ": " & locby & "')"" ")
                        sb.Append("onmouseout=""return nd()""></td></tr>" & vbCrLf)
                    End If
                Else
                    tstat = row("transstatus").ToString
                    If tstat <> "4" Then
                        sb.Append("&nbsp;<img src='../images/appbuttons/minibuttons/warning.gif' ")
                        sb.Append("onmouseover=""return overlib('" & tmod.getov("cov314", "siteassets3.aspx.vb") & "')"" ")
                        sb.Append("onmouseout=""return nd()""></td></tr>" & vbCrLf)
                    End If

                End If
                '*** End Multi Add ***

                'onmouseover=""return overlib('" & row("dept_line").ToString & "', BELOW, LEFT)"" onmouseout=""return nd()""
                sb.Append("<tr><td></td><td colspan=""4""><table class=""details"" cellspacing=""0"" id='t" + eid + "' border=""0"">")
                sb.Append("<tr><td width=""15""></td><td width=""15""></td><td width=""135""></td></tr>")
            End If




        Next
        'h.Dispose()
        sb.Append("</td></tr></table></td></tr></table>")
        'Response.Write(sb.ToString)
        Dim totcntstr As Integer
        totcntstr = totcnt
        tdarch.InnerHtml = sb.ToString
        lblt3.Value = Now
    End Sub

    Private Sub fixeq(ByVal eqid As String, ByVal eqnum As String)
        eqnum = nmm.ModString3(eqnum)
        sql = "update equipment set eqnum = '" & eqnum & "' where eqid = '" & eqid & "'"
        nmm.Update(sql)
    End Sub









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3170.Text = axlabs.GetASPXPage("siteassets3.aspx", "lang3170")
        Catch ex As Exception
        End Try
        Try
            lang3171.Text = axlabs.GetASPXPage("siteassets3.aspx", "lang3171")
        Catch ex As Exception
        End Try
        Try
            lang3172.Text = axlabs.GetASPXPage("siteassets3.aspx", "lang3172")
        Catch ex As Exception
        End Try
        Try
            lang3173.Text = axlabs.GetASPXPage("siteassets3.aspx", "lang3173")
        Catch ex As Exception
        End Try
        Try
            lang3174.Text = axlabs.GetASPXPage("siteassets3.aspx", "lang3174")
        Catch ex As Exception
        End Try
        Try
            lang3175.Text = axlabs.GetASPXPage("siteassets3.aspx", "lang3175")
        Catch ex As Exception
        End Try
        Try
            lang3176.Text = axlabs.GetASPXPage("siteassets3.aspx", "lang3176")
        Catch ex As Exception
        End Try
        Try
            lang3177.Text = axlabs.GetASPXPage("siteassets3.aspx", "lang3177")
        Catch ex As Exception
        End Try
        Try
            lang3178.Text = axlabs.GetASPXPage("siteassets3.aspx", "lang3178")
        Catch ex As Exception
        End Try
        Try
            lang3179.Text = axlabs.GetASPXPage("siteassets3.aspx", "lang3179")
        Catch ex As Exception
        End Try
        Try
            lang3180.Text = axlabs.GetASPXPage("siteassets3.aspx", "lang3180")
        Catch ex As Exception
        End Try
        Try
            lang3181.Text = axlabs.GetASPXPage("siteassets3.aspx", "lang3181")
        Catch ex As Exception
        End Try

    End Sub



End Class
