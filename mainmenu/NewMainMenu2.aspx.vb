

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class NewMainMenu2
    Inherits System.Web.UI.Page
    Protected WithEvents lang3136 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3135 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3134 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3133 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3132 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3131 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3130 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim nmm As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim Tables As String = "equipment"
    Dim PK As String = "eqid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*, cnt = (select count(*) from functions f where f.eqid = eqid)"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim sort As String = "modifieddate desc"
    Dim cid, srch, ms, df, ps As String
    Dim wo, uid, username, islabor, isplanner, issuper, appstr, Logged_In, ro, dfo As String
    Dim decPgNav As Decimal
    Dim intPgNav As Integer
    Dim usr, sid, login, sess, curlang As String
    Protected WithEvents ifarch As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
    Protected WithEvents trnav As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents rbljump As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents btnprev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnnext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tdpg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmsg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgnav As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdarch As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifmain As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifpm As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifwo As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifloc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsess As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsess1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdtpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents iftpm As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbllogcheck As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgetwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgetman As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgettan As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblislabor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblissuper As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisplanner As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblLogged_In As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblhref As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rptreq As System.Web.UI.WebControls.Repeater
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ddcb As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddmb As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txts As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtc As System.Web.UI.WebControls.TextBox
    Protected WithEvents trdata As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdmsg1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsrchmsg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblms As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsrch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldf As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            dfo = HttpContext.Current.Session("dfltps").ToString()
            Try
                df = Request.QueryString("psid").ToString
            Catch ex As Exception
                df = Request.QueryString("sid").ToString
            End Try
            ps = Request.QueryString("psite").ToString

            If dfo <> df Then
                lbldf.Value = df
                Session("dfltps") = df
                Session("psite") = ps
                ro = Request.QueryString("ro").ToString
                appstr = Request.QueryString("appstr").ToString
                Logged_In = Request.QueryString("Logged_In").ToString
                ro = Request.QueryString("ro").ToString
                ms = Request.QueryString("ms").ToString
                usr = HttpContext.Current.Session("username").ToString()
                cid = "0"
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                uid = Request.QueryString("uid").ToString
                username = Request.QueryString("usrname").ToString
                islabor = Request.QueryString("islabor").ToString
                issuper = Request.QueryString("issuper").ToString
                isplanner = Request.QueryString("isplanner").ToString
                curlang = Request.QueryString("curlang").ToString
                Try
                    wo = Request.QueryString("wo").ToString
                Catch ex As Exception
                    wo = ""
                End Try

                Response.Redirect("NewMainMenu2.aspx?sid=" + dfo + "&userid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro + "&appstr=" + appstr + "&Logged_In=" + Logged_In + "&curlang=" + curlang + "&Logged_In=" + Logged_In + "&ms=" + ms + "&psite=" + ps)
            End If

        Catch ex As Exception

        End Try

        If Not IsPostBack Then
            'Try
            Try
                ro = Request.QueryString("ro").ToString 'HttpContext.Current.Session("ro").ToString
            Catch ex1 As Exception
                ro = "0"
            End Try
            'Catch ex As Exception

            'End Try

            appstr = Request.QueryString("appstr").ToString ' HttpContext.Current.Session("appstr").ToString()
            CheckApps(appstr)
            lblappstr.Value = appstr
            Logged_In = Request.QueryString("Logged_In").ToString
            lblLogged_In.Value = Logged_In
            ro = Request.QueryString("ro").ToString
            lblro.Value = ro
            ms = Request.QueryString("ms").ToString
            lblms.Value = ms
            'usr = HttpContext.Current.Session("username").ToString()
            'lbluser.Value = usr
            cid = "0" ' HttpContext.Current.Session("comp").ToString()
            lblcid.Value = cid
            'ms = HttpContext.Current.Session("ms").ToString()
            'lblms.Value = ms

            sid = Request.QueryString("sid").ToString '"12"
            lblsid.Value = sid
            Try
                uid = Request.QueryString("uid").ToString
            Catch ex As Exception
                uid = Request.QueryString("userid").ToString
            End Try
            lbluid.Value = uid
            Try
                username = Request.QueryString("usrname").ToString
            Catch ex As Exception
                username = Request.QueryString("username").ToString
            End Try
            lblusername.Value = username
            lbluser.Value = username
            islabor = Request.QueryString("islabor").ToString
            lblislabor.Value = islabor
            issuper = Request.QueryString("issuper").ToString
            lblissuper.Value = issuper
            isplanner = Request.QueryString("isplanner").ToString
            lblisplanner.Value = isplanner
            Try
                wo = Request.QueryString("wo").ToString
            Catch ex As Exception
                wo = ""
            End Try
            lblhref.Value = "wolabmain.aspx?sid=" + sid + "&wo=" + wo + "&uid=" _
            + "" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr
            lblwonum.Value = wo
            Dim stst As String = "siteassets3.aspx?start=no" + "&ro=" + ro + "&sid=" + sid + "&eqid=0&wo=" + wo + "&sid=" + sid + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&appstr=" + appstr
            ifmain.Attributes.Add("src", "eqtab2.aspx?appstr=" + appstr + "&ms=" + ms + "&sid=" + sid + "&username=" + username) '
            ifarch.Attributes.Add("src", "siteassets3.aspx?start=no" + "&ro=" + ro + "&sid=" + sid + "&eqid=0&wo=" + wo + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&appstr=" + appstr + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms)
        Else
            If Request.Form("lblsess1") = "logout" Then
                'Log Stuff
                AppUtils.LogOut(lblsess.Value)
                'End Log Stuff
            End If
        End If

        
        'btnprev.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yprev.gif'")
        'btnprev.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bprev.gif'")
        'btnnext.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ynext.gif'")
        'btnnext.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bnext.gif'")

    End Sub
    Private Sub CheckApps(ByVal appstr As String)
        Dim apparr() As String = appstr.Split(",")
        'eq,dev,opt,inv
        Dim mutils As New mmenu_utils_a
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                Select Case apparr(i)
                    Case "oman"
                        'tdmsg1.InnerHtml = "<img src=""../images/appbuttons/minibuttons/22PX.gif"" alt="""" /><b><a href=""#"" onclick=""getotab();"">Create a New Record to Optimize</a></b>"
                    Case "man"
                        If mutils.GetMan = 0 Then
                            lblgetman.Value = "0"
                            tdpm.Attributes.Add("onclick", "gettab('tdpm');")
                        Else
                            lblgetman.Value = "1"
                            tdpm.Attributes.Add("class", "thdrdis graylabel")
                        End If

                    Case "wo"
                        If mutils.GetWO = 0 Then
                            lblgetwo.Value = "0"
                            tdwo.Attributes.Add("onclick", "gettab('tdwo');")
                        Else
                            lblgetwo.Value = "1"
                            tdwo.Attributes.Add("class", "thdrdis graylabel")
                        End If

                    Case "tan"
                        If mutils.GetTan = 0 Then
                            lblgettan.Value = "0"
                            tdtpm.Attributes.Add("onclick", "gettab('tdtpm');")
                        Else
                            tdtpm.Attributes.Add("class", "thdrdis graylabel")
                            lblgettan.Value = "1"
                        End If


                End Select
            Next
        Else
            'tdmsg1.InnerHtml = "<img src=""../images/appbuttons/minibuttons/22PX.gif"" alt="""" /><b><a href=""#"" onclick=""getotab();"">Create a New Record to Optimize</a></b>"
            If mutils.GetMan = 0 Then
                lblgetman.Value = "0"
                tdpm.Attributes.Add("onclick", "gettab('tdpm');")
            Else
                lblgetman.Value = "1"
                tdpm.Attributes.Add("class", "thdrdis graylabel")
            End If
            If mutils.GetWO = 0 Then
                lblgetwo.Value = "0"
                tdwo.Attributes.Add("onclick", "gettab('tdwo');")
            Else
                lblgetwo.Value = "1"
                tdwo.Attributes.Add("class", "thdrdis graylabel")
            End If
            If mutils.GetTan = 0 Then
                lblgettan.Value = "0"
                tdtpm.Attributes.Add("onclick", "gettab('tdtpm');")
            Else
                lblgettan.Value = "1"
                tdtpm.Attributes.Add("class", "thdrdis graylabel")
            End If

        End If

    End Sub

    Private Sub GetArch()
        Dim siteid As String = "12" 'lbldf.Value
        Dim sb As StringBuilder = New StringBuilder
        Dim eqnum As String = "eqcopytest"
        Dim eqid As String = "129"
        sb.Append("<table cellspacing=""0"" border=""0""><tr>")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""155""></tr>" & vbCrLf)
        'sql = "select distinct e.siteid, e.dept_id, e.cellid, e.eqid, e.eqnum, f.func_id, " _
        '+ "f.func, c.comid, c.compnum, cnt = (select count(c.compnum) from components c " _
        '+ "where c.func_id = f.func_id) from equipment e join functions f on " _
        '+ "f.eqid = e.eqid join components c on c.func_id = f.func_id " _
        '+ " where siteid = '" & siteid & "'"

        sql = "select distinct e.siteid, e.dept_id, e.cellid, e.eqid, e.eqnum, f.func_id, " _
        + "f.func, c.comid, c.compnum, cnt = (select count(c.compnum) " _
        + "from components c where c.func_id = f.func_id) " _
        + "from equipment e left outer join functions f on f.eqid = e.eqid " _
        + "left outer join components c on c.func_id = f.func_id  where siteid = '" & siteid & "'"

        'h.Open()
        dr = nmm.GetRdrData(sql)
        Dim fid As String = "0"
        Dim cid As String = "0"
        Dim eid As String = "0"
        Dim cidhold As Integer = 0
        Dim sid, did, clid, chk As String
        Dim cnt As Integer = 0
        While dr.Read
            If dr.Item("eqid") <> eid Then
                If eid <> 0 Then
                    sb.Append("</table></td></tr>")
                End If
                eid = dr.Item("eqid").ToString
                sid = dr.Item("siteid").ToString
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                If clid <> "" Then
                    chk = "yes"
                Else
                    chk = "no"
                End If
                eqnum = dr.Item("eqnum").ToString
                sb.Append("<tr><td><img id='i" + eid + "' ")
                sb.Append("onclick=""fclose('t" + eid + "', 'i" + eid + "');""")
                sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>")
                sb.Append("<td colspan=""2"" ><a href=""#"" onclick=""gotoeq('" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "')"" class=""blklink"">" & eqnum & "</a></td></tr>" & vbCrLf)
                sb.Append("<tr><td></td><td colspan=""2""><table cellspacing=""0"" id='t" + eid + "' border=""0"">")
                sb.Append("<tr><td width==""15""></td><td width==""155""></td></tr>")
            End If


            If dr.Item("func_id").ToString <> fid Then
                eid = dr.Item("eqid").ToString
                fid = dr.Item("func_id").ToString
                sid = dr.Item("siteid").ToString
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                cidhold = dr.Item("cnt").ToString
                sb.Append("<tr>" & vbCrLf & "<td colspan=""2""><table cellspacing=""0"" border=""0"">" & vbCrLf)
                sb.Append("<tr><td><img id='i" + fid + "' onclick=""fclose('t" + fid + "', 'i" + fid + "');"" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                sb.Append("<td class=""label""><a href=""#"" onclick=""gotofu('" & eid & "', '" & fid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "')"" class=""blink"">" & dr.Item("func").ToString & "</a></td></tr></table></td></tr>" & vbCrLf)
                If dr.Item("comid").ToString <> cid Then
                    cid = dr.Item("comid").ToString
                    eid = dr.Item("eqid").ToString
                    fid = dr.Item("func_id").ToString
                    sid = dr.Item("siteid").ToString
                    did = dr.Item("dept_id").ToString
                    clid = dr.Item("cellid").ToString
                    If cnt = 0 Then
                        cnt = cnt + 1
                        sb.Append("<tr><td width=""15""></td>" & vbCrLf & "<td width=""155""><table border=""0"" class=""details"" cellspacing=""0"" id=""t" + fid + """>" & vbCrLf)
                        sb.Append("<tr><td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & eid & "', '" & fid & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                        If cnt = cidhold Then
                            cnt = 0
                            sb.Append("</table></td></tr>")
                        End If
                    End If
                End If
            ElseIf dr.Item("comid").ToString <> cid Then
                cid = dr.Item("comid").ToString
                If cnt = 0 Then
                    cnt = cnt + 1
                    sb.Append("<tr><td></td><td></td>" & vbCrLf & "<td><table cellspacing=""0"" id=""t" + fid + """>" & vbCrLf)
                    sb.Append("<tr><td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & eid & "', '" & fid & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                Else
                    cnt = cnt + 1
                    sb.Append("<tr><td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & eid & "', '" & fid & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                End If
                If cnt = cidhold Then
                    cnt = 0
                    sb.Append("</table></td></tr>")
                End If
            End If

        End While
        dr.Close()
        'h.Dispose()
        sb.Append("</td></tr></table></td></tr></table>")
        'Response.Write(sb.ToString)
        tdarch.InnerHtml = sb.ToString
    End Sub




    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim intTest As Int16
        Dim intZero As Int16

        intTest = 1 / intZero

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim intTest As Int16
        Dim intZero As Int16

        intTest = 1 / intZero
    End Sub
    'Private Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Error
    '    Server.ClearError()
    'End Sub





    Private Sub rptreq_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptreq.ItemDataBound


        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang3130 As Label
                lang3130 = CType(e.Item.FindControl("lang3130"), Label)
                lang3130.Text = axlabs.GetASPXPage("NewMainMenu2.aspx", "lang3130")
            Catch ex As Exception
            End Try
            Try
                Dim lang3131 As Label
                lang3131 = CType(e.Item.FindControl("lang3131"), Label)
                lang3131.Text = axlabs.GetASPXPage("NewMainMenu2.aspx", "lang3131")
            Catch ex As Exception
            End Try
            Try
                Dim lang3132 As Label
                lang3132 = CType(e.Item.FindControl("lang3132"), Label)
                lang3132.Text = axlabs.GetASPXPage("NewMainMenu2.aspx", "lang3132")
            Catch ex As Exception
            End Try
            Try
                Dim lang3133 As Label
                lang3133 = CType(e.Item.FindControl("lang3133"), Label)
                lang3133.Text = axlabs.GetASPXPage("NewMainMenu2.aspx", "lang3133")
            Catch ex As Exception
            End Try
            Try
                Dim lang3134 As Label
                lang3134 = CType(e.Item.FindControl("lang3134"), Label)
                lang3134.Text = axlabs.GetASPXPage("NewMainMenu2.aspx", "lang3134")
            Catch ex As Exception
            End Try
            Try
                Dim lang3135 As Label
                lang3135 = CType(e.Item.FindControl("lang3135"), Label)
                lang3135.Text = axlabs.GetASPXPage("NewMainMenu2.aspx", "lang3135")
            Catch ex As Exception
            End Try
            Try
                Dim lang3136 As Label
                lang3136 = CType(e.Item.FindControl("lang3136"), Label)
                lang3136.Text = axlabs.GetASPXPage("NewMainMenu2.aspx", "lang3136")
            Catch ex As Exception
            End Try

        End If

    End Sub

    Private Sub GetArchLabel()
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            img1.Attributes.Add("src", "../images2/eng/loch.gif")
        ElseIf lang = "fre" Then
            img1.Attributes.Add("src", "../images2/fre/loch.gif")
        ElseIf lang = "ger" Then
            img1.Attributes.Add("src", "../images2/ger/loch.gif")
        ElseIf lang = "ita" Then
            img1.Attributes.Add("src", "../images2/ita/loch.gif")
        ElseIf lang = "spa" Then
            img1.Attributes.Add("src", "../images2/spa/loch.gif")
        End If
    End Sub




    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3130.Text = axlabs.GetASPXPage("NewMainMenu2.aspx", "lang3130")
        Catch ex As Exception
        End Try
        Try
            lang3131.Text = axlabs.GetASPXPage("NewMainMenu2.aspx", "lang3131")
        Catch ex As Exception
        End Try
        Try
            lang3132.Text = axlabs.GetASPXPage("NewMainMenu2.aspx", "lang3132")
        Catch ex As Exception
        End Try
        Try
            lang3133.Text = axlabs.GetASPXPage("NewMainMenu2.aspx", "lang3133")
        Catch ex As Exception
        End Try
        Try
            lang3134.Text = axlabs.GetASPXPage("NewMainMenu2.aspx", "lang3134")
        Catch ex As Exception
        End Try
        Try
            lang3135.Text = axlabs.GetASPXPage("NewMainMenu2.aspx", "lang3135")
        Catch ex As Exception
        End Try
        Try
            lang3136.Text = axlabs.GetASPXPage("NewMainMenu2.aspx", "lang3136")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgnav.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("NewMainMenu2.aspx", "imgnav") & "', ABOVE, LEFT)")
            imgnav.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class


