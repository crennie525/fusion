

'********************************************************
'*
'********************************************************



Public Class saadmin
    Inherits System.Web.UI.Page
    Protected WithEvents lang3146 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3145 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3144 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3143 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3142 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3141 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3140 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3139 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3138 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3137 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdss As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdspa As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdscs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcpl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trmsg As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents txtps As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trpg As System.Web.UI.HtmlControls.HtmlTableRow

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Dim ss, spa, scs, cpl As DateTime
            Try
                ss = Request.QueryString("ss").ToString
                tdss.InnerHtml = ss
            Catch ex As Exception

            End Try
            Try
                spa = Request.QueryString("spa").ToString
                tdspa.InnerHtml = spa
            Catch ex As Exception

            End Try
            Try
                scs = Request.QueryString("scs").ToString
                tdscs.InnerHtml = scs
            Catch ex As Exception

            End Try
            Try

                cpl = Request.QueryString("cpl").ToString
                tdcpl.InnerHtml = cpl
            Catch ex As Exception

            End Try
            Dim ps As Integer
            Try
                ps = Request.QueryString("ps").ToString
                txtps.Text = ps
                lblpg.Value = "yes"
                trmsg.Attributes.Add("class", "view")
                trpg.Attributes.Add("class", "view")
            Catch ex As Exception

            End Try
        End If
    End Sub











    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3137.Text = axlabs.GetASPXPage("saadmin.aspx", "lang3137")
        Catch ex As Exception
        End Try
        Try
            lang3138.Text = axlabs.GetASPXPage("saadmin.aspx", "lang3138")
        Catch ex As Exception
        End Try
        Try
            lang3139.Text = axlabs.GetASPXPage("saadmin.aspx", "lang3139")
        Catch ex As Exception
        End Try
        Try
            lang3140.Text = axlabs.GetASPXPage("saadmin.aspx", "lang3140")
        Catch ex As Exception
        End Try
        Try
            lang3141.Text = axlabs.GetASPXPage("saadmin.aspx", "lang3141")
        Catch ex As Exception
        End Try
        Try
            lang3142.Text = axlabs.GetASPXPage("saadmin.aspx", "lang3142")
        Catch ex As Exception
        End Try
        Try
            lang3143.Text = axlabs.GetASPXPage("saadmin.aspx", "lang3143")
        Catch ex As Exception
        End Try
        Try
            lang3144.Text = axlabs.GetASPXPage("saadmin.aspx", "lang3144")
        Catch ex As Exception
        End Try
        Try
            lang3145.Text = axlabs.GetASPXPage("saadmin.aspx", "lang3145")
        Catch ex As Exception
        End Try
        Try
            lang3146.Text = axlabs.GetASPXPage("saadmin.aspx", "lang3146")
        Catch ex As Exception
        End Try

    End Sub

End Class
