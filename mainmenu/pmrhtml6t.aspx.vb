﻿Imports System.Data.SqlClient
Imports System.IO
Public Class pmrhtml6t
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim eqstr, skillid, qty, freq, rdid, mode, rid, rsid, typ, meas As String
    Dim tmod As New transmod
    Dim ridarr As ArrayList = New ArrayList
    Dim sb As New System.Text.StringBuilder
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        Dim comii As New mmenu_utils_a
        Dim comi As String = comii.COMPI
        lblcomi.Value = comi
        If comi = "SUN" Then
            lblfslang.Value = "fre"
        End If
        If Not IsPostBack Then
            rid = Request.QueryString("rid").ToString '"1389" '
            pms.Open()
            'getrsid(rid)
            PopWI(rid)
            pms.Dispose()
            divwi.InnerHtml = sb.ToString
        End If
    End Sub
    Private Sub PopWI(ByVal rteid As String)
        sql = "select rttype from pmroutes where rid = '" & rteid & "'"
        typ = "RBAST" 'pms.strScalar(sql)
        Dim coi As String = lblcomi.Value
        Dim cb1mon, cb1tue, cb1wed, cb1thu, cb1fri, cb1sat, cb1sun As String
        Dim cb2mon, cb2tue, cb2wed, cb2thu, cb2fri, cb2sat, cb2sun As String
        Dim cb3mon, cb3tue, cb3wed, cb3thu, cb3fri, cb3sat, cb3sun As String

        Dim fid As String = ""
        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim funcchk As String = ""
        Dim func As String = ""
        Dim subtask As String = ""
        Dim eqnum As String = ""
        Dim eqnumchk As String = ""
        Dim hdchk As Integer = 0
        Dim eqid As String
        Dim pic As String
        Dim loc As String = ""
        'eqid = eqstr
        'eqstr = eqstr.Replace("(", "")
        'eqstr = eqstr.Replace(")", "")
        'Dim eqarr() As String = eqstr.Split(",")
        'Dim i As Integer
        'Dim tst As String = eqarr.Length - 1

        Dim chk As Integer

        'For i = 0 To eqarr.Length - 1
        hdchk = 0
        'eqid = eqarr(i).ToString
        ' = "(" & eqid & ")"
        'sql = "select count(*) from pmtasks where eqid = '" & eqid & "' and skillid = '" & skillid & "' and qty = '" & qty & "' and freq = '" & freq & "' and rdid = '" & rdid & "'"
        'chk = pms.Scalar(sql)
        'If chk > 0 Then
        If typ = "RBAS" Then
            sql = "usp_pmr_rep4 '" & rteid & "'"
        ElseIf typ = "RBAST" Then
            sql = "usp_pmr_rep4T '" & rteid & "'"
        End If

        Dim parts, tools, lubes As String
        parts = ""
        tools = ""
        lubes = ""
        Dim sk, fr, rd, tt As String
        dr = pms.GetRdrData(sql)
        Dim pretech, pretechchk, comp, cqty, task, fm1, imgstr, tasknum, ttid As String
        'style=""page-break-after:always;""


        While dr.Read
            sk = dr.Item("skill").ToString
            fr = dr.Item("freq").ToString
            rd = dr.Item("rd").ToString
            tt = dr.Item("est").ToString
            skill = dr.Item("skill").ToString & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString
            pretech = dr.Item("pretech").ToString
            eqnum = dr.Item("eqnum").ToString
            comp = dr.Item("comp").ToString
            cqty = dr.Item("cqty").ToString
            task = dr.Item("task").ToString
            fm1 = dr.Item("fm1").ToString
            ttid = dr.Item("ttid").ToString
            tasknum = dr.Item("tasknum").ToString
            meas = dr.Item("meas").ToString

            cb1mon = dr.Item("cb1mon").ToString
            cb1tue = dr.Item("cb1tue").ToString
            cb1wed = dr.Item("cb1wed").ToString
            cb1thu = dr.Item("cb1thu").ToString
            cb1fri = dr.Item("cb1fri").ToString
            cb1sat = dr.Item("cb1sat").ToString
            cb1sun = dr.Item("cb1sun").ToString

            cb2mon = dr.Item("cb2mon").ToString
            cb2tue = dr.Item("cb2tue").ToString
            cb2wed = dr.Item("cb2wed").ToString
            cb2thu = dr.Item("cb2thu").ToString
            cb2fri = dr.Item("cb2fri").ToString
            cb2sat = dr.Item("cb2sat").ToString
            cb2sun = dr.Item("cb2sun").ToString

            cb3mon = dr.Item("cb3mon").ToString
            cb3tue = dr.Item("cb3tue").ToString
            cb3wed = dr.Item("cb3wed").ToString
            cb3thu = dr.Item("cb3thu").ToString
            cb3fri = dr.Item("cb3fri").ToString
            cb3sat = dr.Item("cb3sat").ToString
            cb3sun = dr.Item("cb3sun").ToString

            If ttid = 2 Then
                imgstr = "../images/appbuttons/minibuttons/tpmclean.gif"
            ElseIf ttid = 3 Then
                imgstr = "../images/appbuttons/minibuttons/tpmlube.gif"
            ElseIf ttid = 5 Or ttid = 6 Or ttid = 7 Then
                imgstr = "../images/appbuttons/minibuttons/tpminsp.gif"
            Else
                imgstr = "0"
            End If

            If hdchk = 0 Then
                hdchk = 1
                sb.Append("<Table cellSpacing=""1"" cellPadding=""2"" width=""620"" border=""0"">")
                sb.Append("<tr><td width=""40""></td><td width=""40""></td><td width=""540""></td></tr>")
                If coi = "CAS" Then
                    sb.Append("<tr><td colspan=""2""><img src=""../menu/mimages/cascades_logo.png""></td>")
                Else
                    sb.Append("<tr><td colspan=""2""><img src=""../menu/mimages/nav_logo.bmp""></td>")
                End If
                sb.Append("<td width=""540"" class=""tdborder1"">")

                sb.Append("<table width=""540"">")
                sb.Append("<tr><td class=""plainlabel"" width=""100"">Route:</td><td class=""plainlabel"" width=""150"">" & dr.Item("route").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" width=""100"">Description:</td><td class=""plainlabel"">" & dr.Item("description").ToString & "</td></tr>")
                sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
                sb.Append("<tr><td colspan=""4"">")
                sb.Append("<table width=""540"">")
                sb.Append("<tr><td class=""plainlabel"" width=""50"">" & checklang("lab1") & "</td><td class=""plainlabel"" width=""100"">" & sk & "</td>")
                sb.Append("<td class=""plainlabel"" width=""60"">" & checklang("lab2") & "</td><td class=""plainlabel"" width=""80"">" & fr & "</td>")
                sb.Append("<td class=""plainlabel"" width=""50"">" & checklang("lab3") & "</td><td class=""plainlabel"" width=""60"">" & rd & "</td>")
                sb.Append("<td class=""plainlabel"" width=""80"">" & checklang("lab") & "</td><td class=""plainlabel"" width=""60"">" & tt & "</td></tr>")
                If pretech <> "" Then
                    sb.Append("<tr><td class=""plainlabel"" width=""50"">PdM:</td><td class=""plainlabel"" width=""100"">" & pretech & "</td>")
                End If
                sb.Append("</table></td></tr>")
                sb.Append("</table></td></tr>")
                'sb.Append("</table></td></tr>")
            End If

            Dim skchk, skchk2 As String
            skchk = skill.ToLower
            skchk2 = skillchk.ToLower
            If eqnumchk <> eqnum Then
                eqnumchk = eqnum
                'hdchk = 1
                skillchk = skill
                start = 0
                flag = 0



                sb.Append("<tr><td colspan=""3"">")
                sb.Append("<table width=""620"">")
                sb.Append("<tr><td colspan=""4""><hr></td></tr>")
                sb.Append("<tr><td class=""label"" width=""100"">" & checklang("lab4") & "</td><td class=""plainlabel"" width=""150"">" & dr.Item("eqnum").ToString & "</td>")
                sb.Append("<td class=""label"" width=""100"">Description:</td><td class=""plainlabel"" width=""190"">" & dr.Item("eqdesc").ToString & "</td></tr>")
                sb.Append("</table></td></tr>")



            End If
            func = dr.Item("func").ToString
            pic = dr.Item("pic").ToString
            If flag = 0 Or pic <> "" Then
                flag = 1
                funcchk = func
                pic = dr.Item("pic").ToString
                loc = dr.Item("loc").ToString
                'If pic = "" Then
                'pic = "../images/appimages/funcimg.gif"
                'Else
                If File.Exists(pic) Then
                    pic = pic
                Else
                    pic = "" '"../images/appimages/funcimg.gif"
                End If
                'End If
                sb.Append("<tr><td colspan=""3"" class=""tdborder1"">")
                sb.Append("<table>")
                sb.Append("<tr><td class=""plainlabel"" width=""80"" valign=""top""><br />" & checklang("lab5") & "</td><td class=""plainlabel"" width=""280"" valign=""top""><br />" & dr.Item("func").ToString & "</td>")
                If pic = "" Then
                    sb.Append("<td width=""300"" align=""center"" valign=""center"" class=""plainlabel"">" & checklang("lab12") & "</td></tr>")
                Else
                    sb.Append("<td width=""300"" align=""center"" valign=""center""><img src=""" & pic & """></td></tr>")
                End If
                sb.Append("</table></td></tr>")

                sb.Append("<tr><td colspan=""3""><table cellspacing=""0"">")
                sb.Append("<tr height=""22"">")
                sb.Append("<td class=""box1b replabel"" width=""471"">" & tmod.getxlbl("xlb58", "PMALL.vb") & "</td>")
                sb.Append("<td class=""box1b replabel"" width=""67"">" & tmod.getxlbl("xlb272", "AssignmentReportPics.aspx.vb") & "</td>")
                sb.Append("<td class=""box1b replabel"" width=""38"">" & tmod.getxlbl("lang3417d", "reports2.aspx") & "&nbsp;</td>")
                sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab14") & "</td>")
                sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab15") & "</td>")
                sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab16") & "</td>")
                sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab17") & "</td>")
                sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab18") & "</td>")
                sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab19") & "</td>")
                sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab20") & "</td>")
                sb.Append("</tr>")
                'sb.Append("</table></td></tr>")
                Dim bclass As String

                sb.Append("<tr height=""30"">")
                sb.Append("<td class=""box1bg label"" rowspan=""1"" valign=""top"">" & func & " - " & tmod.getxlbl("xlb381", "PMAcceptancePkg.aspx.vb") & " " & tasknum & "</td>")
                If imgstr <> "0" Then
                    sb.Append("<td class=""box1 label"" rowspan=""3"" valign=""middle"" align=""center""><img src=""" & imgstr & """></td>")
                Else
                    sb.Append("<td class=""box1"" rowspan=""3"" valign=""middle"" align=""center"">&nbsp;</td>")
                End If


                sb.Append("<td class=""box1 plainlabel"" align=""center"">1</td>")
                If cb1mon = "1" Then bclass = "box1" Else bclass = "gbox1"
                'If cb1mon = "1" Then
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                'Else
                '    sb.Append("<td class=""" & bclass & """ width=""20""><img src=""gbox.gif""></td>")
                'End If

                If cb1tue = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb1wed = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb1thu = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb1fri = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb1sat = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb1sun = "1" Then bclass = "box1r" Else bclass = "gbox1r"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                sb.Append("</tr>")
                sb.Append("<tr height=""30"">")
                sb.Append("<td class=""box1 plainlabel"" rowspan=""2"" valign=""top""><font class=""label"">" & comp & "(" & cqty & ") - </font>" & task & "<br>" & fm1 & "<br>" & meas & "</td>")
                sb.Append("<td class=""box1 plainlabel"" align=""center"">2</td>")
                If cb2mon = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb2tue = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb2wed = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb2thu = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb2fri = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb2sat = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb2sun = "1" Then bclass = "box1r" Else bclass = "gbox1r"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                sb.Append("</tr>")
                sb.Append("<tr height=""30"">")
                sb.Append("<td class=""box1 plainlabel"" align=""center"">3</td>")
                If cb3mon = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb3tue = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb3wed = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb3thu = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb3fri = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb3sat = "1" Then bclass = "box1" Else bclass = "gbox1"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                If cb3sun = "1" Then bclass = "box1r" Else bclass = "gbox1r"
                sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                sb.Append("</tr>")

                sb.Append("</table></td></tr>")
                'sb.Append("<tr><td colspan=""3"" align=""center"">")
                'sb.Append("<table>")
                'sb.Append("<tr><td class=""plainlabel"" width=""380""><b>Task Description</b></td>")
                'sb.Append("<td class=""plainlabel"" width=""80""><b>Est Duration</b></td>")
                'sb.Append("<td class=""plainlabel"" width=""200""><b>Failure Modes</b></td></tr>")

                'sb.Append("<tr><td class=""plainlabel tdborder1"">" & dr.Item("task").ToString & "</td>")
                'sb.Append("<td class=""plainlabel tdborder1"">" & dr.Item("ttime").ToString & "</td>")
                'sb.Append("<td class=""plainlabel tdborder1"">" & dr.Item("fm1").ToString & "</td></tr>")
                'sb.Append("<tr><td colspan=""3""><hr></td></tr>")

                'sb.Append("</table></td></tr>")
            Else
                If func <> funcchk Then
                    funcchk = func
                    pic = dr.Item("pic").ToString
                    'If pic = "" Then
                    'pic = "../images/appimages/funcimg.gif"
                    'Else
                    If File.Exists(pic) Then
                        pic = pic
                    Else
                        pic = "" '"../images/appimages/funcimg.gif"
                    End If
                    'End If
                    'sb.Append("</table></td></tr>")

                    sb.Append("<tr><td colspan=""3"" class=""tdborder1"">")
                    sb.Append("<table >")
                    sb.Append("<tr><td class=""plainlabel"" width=""80"" valign=""top""><br />Function:</td><td class=""plainlabel"" width=""280"" valign=""top""><br />" & dr.Item("func").ToString & "</td>")
                    If pic = "" Then
                        sb.Append("<td width=""300"" align=""center"" valign=""center"" class=""plainlabel"">No Image Available</td></tr>")
                    Else
                        sb.Append("<td width=""300"" align=""center"" valign=""center""><img src=""" & pic & """></td></tr>")
                    End If

                    sb.Append("</table></td></tr>")

                    sb.Append("<tr><td colspan=""3""><table cellspacing=""0"">")
                    sb.Append("<tr height=""22"">")
                    sb.Append("<td class=""box1b replabel"" width=""471"">" & tmod.getxlbl("xlb58", "PMALL.vb") & "</td>")
                    sb.Append("<td class=""box1b replabel"" width=""67"">" & tmod.getxlbl("xlb272", "AssignmentReportPics.aspx.vb") & "</td>")
                    sb.Append("<td class=""box1b replabel"" width=""38"">" & tmod.getxlbl("lang3417d", "reports2.aspx") & "&nbsp;</td>")
                    sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab14") & "</td>")
                    sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab15") & "</td>")
                    sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab16") & "</td>")
                    sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab17") & "</td>")
                    sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab18") & "</td>")
                    sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab19") & "</td>")
                    sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab20") & "</td>")
                    sb.Append("</tr>")
                    'sb.Append("</table></td></tr>")
                    Dim bclass As String

                    sb.Append("<tr height=""30"">")
                    sb.Append("<td class=""box1bg label"" rowspan=""1"" valign=""top"">" & func & " - " & tmod.getxlbl("xlb381", "PMAcceptancePkg.aspx.vb") & " " & tasknum & "</td>")
                    If imgstr <> "0" Then
                        sb.Append("<td class=""box1 label"" rowspan=""3"" valign=""middle"" align=""center""><img src=""" & imgstr & """></td>")
                    Else
                        sb.Append("<td class=""box1"" rowspan=""3"" valign=""middle"" align=""center"">&nbsp;</td>")
                    End If


                    sb.Append("<td class=""box1 plainlabel"" align=""center"">1</td>")
                    If cb1mon = "1" Then bclass = "box1" Else bclass = "gbox1"
                    'If cb1mon = "1" Then
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    'Else
                    '    sb.Append("<td class=""" & bclass & """ width=""20""><img src=""gbox.gif""></td>")
                    'End If

                    If cb1tue = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb1wed = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb1thu = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb1fri = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb1sat = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb1sun = "1" Then bclass = "box1r" Else bclass = "gbox1r"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    sb.Append("</tr>")
                    sb.Append("<tr height=""30"">")
                    sb.Append("<td class=""box1 plainlabel"" rowspan=""2"" valign=""top""><font class=""label"">" & comp & "(" & cqty & ") - </font>" & task & "<br>" & fm1 & "<br>" & meas & "</td>")
                    sb.Append("<td class=""box1 plainlabel"" align=""center"">2</td>")
                    If cb2mon = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb2tue = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb2wed = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb2thu = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb2fri = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb2sat = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb2sun = "1" Then bclass = "box1r" Else bclass = "gbox1r"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    sb.Append("</tr>")
                    sb.Append("<tr height=""30"">")
                    sb.Append("<td class=""box1 plainlabel"" align=""center"">3</td>")
                    If cb3mon = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb3tue = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb3wed = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb3thu = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb3fri = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb3sat = "1" Then bclass = "box1" Else bclass = "gbox1"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    If cb3sun = "1" Then bclass = "box1r" Else bclass = "gbox1r"
                    sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                    sb.Append("</tr>")

                    sb.Append("</table></td></tr>")
                    'sb.Append("<tr><td colspan=""3"" align=""center"">")
                    'sb.Append("<table>")
                    'sb.Append("<tr><td class=""plainlabel"" width=""380""><b>Task Description</b></td>")
                    'sb.Append("<td class=""plainlabel"" width=""80""><b>Est Duration</b></td>")
                    'sb.Append("<td class=""plainlabel"" width=""200""><b>Failure Modes</b></td></tr>")



                    'sb.Append("<tr><td class=""plainlabel tdborder1"">" & dr.Item("task").ToString & "</td>")
                    'sb.Append("<td class=""plainlabel tdborder1"">" & dr.Item("ttime").ToString & "</td>")
                    'sb.Append("<td class=""plainlabel tdborder1"">" & dr.Item("fm1").ToString & "</td></tr>")
                    'sb.Append("<tr><td colspan=""3""><hr></td></tr>")

                    'sb.Append("</table></td></tr>")

                Else
                    If func = funcchk Then

                        sb.Append("<tr><td colspan=""3""><table cellspacing=""0"">")
                        sb.Append("<tr height=""22"">")
                        sb.Append("<td class=""box1b replabel"" width=""471"">" & tmod.getxlbl("xlb58", "PMALL.vb") & "</td>")
                        sb.Append("<td class=""box1b replabel"" width=""67"">" & tmod.getxlbl("xlb272", "AssignmentReportPics.aspx.vb") & "</td>")
                        sb.Append("<td class=""box1b replabel"" width=""38"">" & tmod.getxlbl("lang3417d", "reports2.aspx") & "&nbsp;</td>")
                        sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab14") & "</td>")
                        sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab15") & "</td>")
                        sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab16") & "</td>")
                        sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab17") & "</td>")
                        sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab18") & "</td>")
                        sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab19") & "</td>")
                        sb.Append("<td class=""box1b replabel"" width=""22"" align=""center"">" & checklang("lab20") & "</td>")
                        sb.Append("</tr>")
                        'sb.Append("</table></td></tr>")
                        Dim bclass As String

                        sb.Append("<tr height=""30"">")
                        sb.Append("<td class=""box1bg label"" rowspan=""1"" valign=""top"">" & func & " - " & tmod.getxlbl("xlb381", "PMAcceptancePkg.aspx.vb") & " " & tasknum & "</td>")
                        If imgstr <> "0" Then
                            sb.Append("<td class=""box1 label"" rowspan=""3"" valign=""middle"" align=""center""><img src=""" & imgstr & """></td>")
                        Else
                            sb.Append("<td class=""box1"" rowspan=""3"" valign=""middle"" align=""center"">&nbsp;</td>")
                        End If


                        sb.Append("<td class=""box1 plainlabel"" align=""center"">1</td>")
                        If cb1mon = "1" Then bclass = "box1" Else bclass = "gbox1"
                        'If cb1mon = "1" Then
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        'Else
                        '    sb.Append("<td class=""" & bclass & """ width=""20""><img src=""gbox.gif""></td>")
                        'End If

                        If cb1tue = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb1wed = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb1thu = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb1fri = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb1sat = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb1sun = "1" Then bclass = "box1r" Else bclass = "gbox1r"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        sb.Append("</tr>")
                        sb.Append("<tr height=""30"">")
                        sb.Append("<td class=""box1 plainlabel"" rowspan=""2"" valign=""top""><font class=""label"">" & comp & "(" & cqty & ") - </font>" & task & "<br>" & fm1 & "<br>" & meas & "</td>")
                        sb.Append("<td class=""box1 plainlabel"" align=""center"">2</td>")
                        If cb2mon = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb2tue = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb2wed = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb2thu = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb2fri = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb2sat = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb2sun = "1" Then bclass = "box1r" Else bclass = "gbox1r"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        sb.Append("</tr>")
                        sb.Append("<tr height=""30"">")
                        sb.Append("<td class=""box1 plainlabel"" align=""center"">3</td>")
                        If cb3mon = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb3tue = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb3wed = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb3thu = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb3fri = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb3sat = "1" Then bclass = "box1" Else bclass = "gbox1"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        If cb3sun = "1" Then bclass = "box1r" Else bclass = "gbox1r"
                        sb.Append("<td class=""" & bclass & """ width=""20"">&nbsp;</td>")
                        sb.Append("</tr>")

                        sb.Append("</table></td></tr>")

                        'sb.Append("<tr><td colspan=""3"" align=""center"">")
                        'sb.Append("<table>")
                        'sb.Append("<tr><td class=""plainlabel tdborder1"" width=""380"">" & dr.Item("task").ToString & "</td>")
                        'sb.Append("<td class=""plainlabel tdborder1"" width=""80"">" & dr.Item("ttime").ToString & "</td>")
                        'sb.Append("<td class=""plainlabel tdborder1"" width=""200"">" & dr.Item("fm1").ToString & "</td></tr>")
                        'sb.Append("<tr><td colspan=""3""><hr></td></tr>")

                        'sb.Append("</table></td></tr>")
                    End If


                End If



            End If




        End While
        dr.Close()

        sb.Append("</table></td></tr>")

        sb.Append("</Table>")
        'End If
        'Next


    End Sub
    Private Function checklang(ByVal lab As String)
        Dim lang As String = lblfslang.Value
        Dim ret As String
        Dim lab1 As String
        'Acune image de tâche
        If lang = "fre" Then
            If lab = "lab1" Then
                lab1 = "Métier:"
            ElseIf lab = "lab2" Then
                lab1 = "Fréquence:"
            ElseIf lab = "lab3" Then
                lab1 = "Statut:"
            ElseIf lab = "lab4" Then
                lab1 = "Équipement:"
            ElseIf lab = "lab5" Then
                lab1 = "Fonction:"
            ElseIf lab = "lab6" Then
                lab1 = "Détail de la composante:"
            ElseIf lab = "lab7" Then
                lab1 = "Nom:"
            ElseIf lab = "lab8" Then
                lab1 = "Tâche:"
            ElseIf lab = "lab9" Then
                lab1 = "Temps estimé:"
            ElseIf lab = "lab10" Then
                lab1 = "Modes de défaillances:"
            ElseIf lab = "lab11" Then
                lab1 = "Commentaire:"
            ElseIf lab = "lab12" Then
                lab1 = "Aucune image"
            ElseIf lab = "lab13" Then
                lab1 = "Temps total"
            ElseIf lab = "lab14" Then
                lab1 = "L"
            ElseIf lab = "lab15" Then
                lab1 = "M"
            ElseIf lab = "lab16" Then
                lab1 = "M"
            ElseIf lab = "lab17" Then
                lab1 = "J"
            ElseIf lab = "lab18" Then
                lab1 = "V"
            ElseIf lab = "lab19" Then
                lab1 = "S"
            ElseIf lab = "lab20" Then
                lab1 = "D"

            End If
        Else
            If lab = "lab1" Then
                lab1 = "Skill:"
            ElseIf lab = "lab2" Then
                lab1 = "Frequency:"
            ElseIf lab = "lab3" Then
                lab1 = "Status:"
            ElseIf lab = "lab4" Then
                lab1 = "Function:"
            ElseIf lab = "lab5" Then
                lab1 = "Equipment:"
            ElseIf lab = "lab6" Then
                lab1 = "Component Details:"
            ElseIf lab = "lab7" Then
                lab1 = "Name:"
            ElseIf lab = "lab8" Then
                lab1 = "Task:"
            ElseIf lab = "lab9" Then
                lab1 = "Est Duration:"
            ElseIf lab = "lab10" Then
                lab1 = "Failure Modes:"
            ElseIf lab = "lab11" Then
                lab1 = "Comments:"
            ElseIf lab = "lab12" Then
                lab1 = "No Image Available"
            ElseIf lab = "lab13" Then
                lab1 = "Total Time"
            ElseIf lab = "lab14" Then
                lab1 = "M"
            ElseIf lab = "lab15" Then
                lab1 = "Tu"
            ElseIf lab = "lab16" Then
                lab1 = "W"
            ElseIf lab = "lab17" Then
                lab1 = "Th"
            ElseIf lab = "lab18" Then
                lab1 = "F"
            ElseIf lab = "lab19" Then
                lab1 = "Sa"
            ElseIf lab = "lab20" Then
                lab1 = "Su"
            End If
        End If
        ret = lab1
        Return ret
    End Function
End Class