<%@ Page Language="vb" AutoEventWireup="false" Codebehind="siteassets3.aspx.vb" Inherits="lucy_r12.siteassets3" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>siteassets3</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/siteassets3aspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
        <script language="javascript" type="text/javascript">
            function geteqport(eq) {
                window.open("../equip/pmportfolio.aspx?eqid=" + eq + "&fuid=0&comid=0&date=" + Date());
            }
            function getfuport(eq, fu) {
                window.open("../equip/pmportfolio.aspx?eqid=" + eq + "&fuid=" + fu + "&comid=0&date=" + Date());
            }
            function getcoport(eq, fu, co) {
                window.open("../equip/pmportfolio.aspx?eqid=" + eq + "&fuid=" + fu + "&comid=" + co + "&date=" + Date());
            }

            function gotofu(eid, fid, sid, did, clid, chk, lid, dept, cell, eq, loc) {
                var ustr = document.getElementById("lblusername").value;
                if (document.getElementById("rb1").checked == true) {
                    var chk = "yes";

                    window.parent.gotofu("../equip/eqmain2.aspx?lvl=fu&start=yes&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eid + "&fuid=" + fid + "&lid=" + lid + "&typ=&ustr=" + ustr);
                }
                else if (document.getElementById("rb12").checked == true) {
                    var chk = "yes";
                    var ustr = document.getElementById("lblusername").value;
                    //alert("../appspmo123tab/pmo123tabmain.aspx?who=eqready&lvl=eq&start=yes&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eid + "&lid=" + lid + "&typ=&dept=" + dept + "&cell=" + cell + "&eq=" + eq + "&loc=" + loc + "&rettyp=&gototasks=1&usrname=" + ustr)
                    window.parent.gotofu("../appspmo123tab/pmo123tabmain.aspx?who=eqready&lvl=eq&start=yes&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eid + "&lid=" + lid + "&typ=&dept=" + dept + "&cell=" + cell + "&eq=" + eq + "&loc=" + loc + "&rettyp=&gototasks=1&usrname=" + ustr + "&fuid=" + fid + "&coid=");
                }
                else if (document.getElementById("rb5").checked == true) {
                    window.parent.gotoeqlist(eid);
                }
                else if (document.getElementById("rb6").checked == true) {
                    window.parent.gotowolistfu(fid, eid);
                }
                else if (document.getElementById("rb7").checked == true) {
                    window.parent.gotopmlistfu(fid, eid);
                }
                else if (document.getElementById("rb8").checked == true) {
                    window.parent.gotopmoptfu(eid, fid, sid, did, clid, chk, lid);
                }
                else if (document.getElementById("rb9").checked == true) {
                    window.parent.gotopmdevfu(eid, fid, sid, did, clid, chk, lid);
                }
                else if (document.getElementById("rb10").checked == true) {
                    window.parent.gototpmoptfu(eid, fid, sid, did, clid, chk, lid);
                }
                else if (document.getElementById("rb11").checked == true) {
                    window.parent.gototpmdevfu(eid, fid, sid, did, clid, chk, lid);
                }
                else {
                    window.open("../reports/AssignmentReport.aspx?typ=fu&eqid=" + eid + "&fuid=" + fid + "&date=" + Date());
                }
            }
            function gotoco(eid, fid, cid, sid, did, clid, chk, lid, dept, cell, eq, loc) {
                var ustr = document.getElementById("lblusername").value;
                if (document.getElementById("rb1").checked == true) {
                    var chk = "yes";
                    window.parent.gotoco("../equip/eqmain2.aspx?lvl=co&start=yes&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eid + "&fuid=" + fid + "&comid=" + cid + "&lid=" + lid + "&typ=&ustr=" + ustr);
                }
                else if (document.getElementById("rb12").checked == true) {
                    var chk = "yes";
                    var ustr = document.getElementById("lblusername").value;
                    //alert("../appspmo123tab/pmo123tabmain.aspx?who=eqready&lvl=eq&start=yes&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eid + "&lid=" + lid + "&typ=&dept=" + dept + "&cell=" + cell + "&eq=" + eq + "&loc=" + loc + "&rettyp=&gototasks=1&usrname=" + ustr)
                    window.parent.gotoco("../appspmo123tab/pmo123tabmain.aspx?who=eqready&lvl=eq&start=yes&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eid + "&lid=" + lid + "&typ=&dept=" + dept + "&cell=" + cell + "&eq=" + eq + "&loc=" + loc + "&rettyp=&gototasks=1&usrname=" + ustr + "&fuid=" + fid + "&coid=" + cid);
                }
                else if (document.getElementById("rb5").checked == true) {
                    window.parent.gotoeqlist(eid);
                }
                else if (document.getElementById("rb6").checked == true) {
                    window.parent.gotowolistco(cid, fid, eid);
                }
                else if (document.getElementById("rb7").checked == true) {
                    window.parent.gotopmlistco(cid, fid, eid);
                }
                else if (document.getElementById("rb8").checked == true) {
                    window.parent.gotopmoptco(eid, fid, cid, sid, did, clid, chk, lid);
                }
                else if (document.getElementById("rb9").checked == true) {
                    window.parent.gotopmdevco(eid, fid, cid, sid, did, clid, chk, lid);
                }
                else if (document.getElementById("rb10").checked == true) {
                    window.parent.gototpmoptco(eid, fid, cid, sid, did, clid, chk, lid);
                }
                else if (document.getElementById("rb11").checked == true) {
                    window.parent.gototpmdevco(eid, fid, cid, sid, did, clid, chk, lid);
                }
                else {
                    window.open("../reports/AssignmentReport.aspx?typ=co&eqid=" + eid + "&fuid=" + fid + "&coid=" + cid + "&date=" + Date());
                }
            }
            function fclose(td, img) {
                //alert(td + ", " + img)
                var str = document.getElementById(img).src
                var lst = str.lastIndexOf("/") + 1
                var loc = str.substr(lst)
                if (loc == 'minus.gif') {
                    document.getElementById(img).src = '../images/appbuttons/bgbuttons/plus.gif';
                    try {
                        document.getElementById(td).className = "details";
                    }
                    catch (err) {

                    }
                }
                else {
                    document.getElementById(img).src = '../images/appbuttons/bgbuttons/minus.gif';
                    try {
                        document.getElementById(td).className = "view";
                    }
                    catch (err) {

                    }
                }
            }
            function getinprog() {
                window.parent.getinprog();
                document.getElementById("rb1").checked = true;
            }
            function getnext() {

                var cnt = document.getElementById("txtpgcnt").value;
                var pg = document.getElementById("txtpg").value;
                pg = parseInt(pg);
                cnt = parseInt(cnt)
                if (pg < cnt) {
                    var d = new Date();
                    var curr_date = d.getDate();
                    var curr_month = d.getMonth();
                    curr_month++;
                    var curr_year = d.getFullYear();
                    var hr = d.getHours();
                    var mn = d.getMinutes();
                    var ss = d.getSeconds();
                    document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
                    document.getElementById("lblret").value = "next"
                    document.getElementById("form1").submit();
                }
            }
            function getlast() {

                var cnt = document.getElementById("txtpgcnt").value;
                var pg = document.getElementById("txtpg").value;
                pg = parseInt(pg);
                cnt = parseInt(cnt)
                if (pg < cnt) {
                    var d = new Date();
                    var curr_date = d.getDate();
                    var curr_month = d.getMonth();
                    curr_month++;
                    var curr_year = d.getFullYear();
                    var hr = d.getHours();
                    var mn = d.getMinutes();
                    var ss = d.getSeconds();
                    document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
                    document.getElementById("lblret").value = "last"
                    document.getElementById("form1").submit();
                }
            }
            function getprev() {

                var cnt = document.getElementById("txtpgcnt").value;
                var pg = document.getElementById("txtpg").value;
                pg = parseInt(pg);
                cnt = parseInt(cnt)
                if (pg > 1) {
                    var d = new Date();
                    var curr_date = d.getDate();
                    var curr_month = d.getMonth();
                    curr_month++;
                    var curr_year = d.getFullYear();
                    var hr = d.getHours();
                    var mn = d.getMinutes();
                    var ss = d.getSeconds();
                    document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
                    document.getElementById("lblret").value = "prev"
                    document.getElementById("form1").submit();
                }
            }
            function getfirst() {

                var cnt = document.getElementById("txtpgcnt").value;
                var pg = document.getElementById("txtpg").value;
                pg = parseInt(pg);
                cnt = parseInt(cnt)
                if (pg > 1) {
                    var d = new Date();
                    var curr_date = d.getDate();
                    var curr_month = d.getMonth();
                    curr_month++;
                    var curr_year = d.getFullYear();
                    var hr = d.getHours();
                    var mn = d.getMinutes();
                    var ss = d.getSeconds();
                    document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
                    document.getElementById("lblret").value = "first"
                    document.getElementById("form1").submit();
                }
            }
            function chksrch() {
                var srch = document.getElementById("txtsrch").value;
                if (srch != "") {
                    document.getElementById("lblret").value = "srch"
                    var d = new Date();
                    var curr_date = d.getDate();
                    var curr_month = d.getMonth();
                    curr_month++;
                    var curr_year = d.getFullYear();
                    var hr = d.getHours();
                    var mn = d.getMinutes();
                    var ss = d.getSeconds();
                    document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
                    document.getElementById("form1").submit();
                }
            }
            
            function dateDiff() {
                date1 = new Date();
                date2 = new Date();
                diff = new Date();

                if (document.getElementById("lblt1").value == "") {
                    date1temp = new Date(document.getElementById("lblt2").value);
                    date1.setTime(date1temp.getTime());
                }
                else {
                    date1temp = new Date(document.getElementById("lblt1").value);
                    date1.setTime(date1temp.getTime());
                }

                date2temp = new Date(document.getElementById("lbltl").value);
                date2.setTime(date2temp.getTime());



                // sets difference date to difference of first date and second date

                diff.setTime(Math.abs(date1.getTime() - date2.getTime()));

                timediff = diff.getTime();

                weeks = Math.floor(timediff / (1000 * 60 * 60 * 24 * 7));
                timediff -= weeks * (1000 * 60 * 60 * 24 * 7);

                days = Math.floor(timediff / (1000 * 60 * 60 * 24));
                timediff -= days * (1000 * 60 * 60 * 24);

                hours = Math.floor(timediff / (1000 * 60 * 60));
                timediff -= hours * (1000 * 60 * 60);

                mins = Math.floor(timediff / (1000 * 60));
                timediff -= mins * (1000 * 60);

                secs = Math.floor(timediff / 1000);
                timediff -= secs * 1000;
                //weeks + " weeks, " + days + " days, " + hours + " hours, " + 
                document.getElementById("td0").innerHTML = mins + " minutes, and " + secs + " seconds";


            }
            function getsaadmin() {
                var ss = document.getElementById("lblt1").value;
                var spa = document.getElementById("lblt2").value;
                var scs = document.getElementById("lblt3").value;
                var cpl = document.getElementById("lbltl").value;
                var ps = document.getElementById("lblpagesize").value;
                //alert("saadmin.aspx?ss=" + ss + "&spa=" + spa + "&scs=" + scs + "&cpl=" + cpl + "&ps=" + ps + "&date=" + Date())
                var eReturn = window.showModalDialog("saadmin.aspx?ss=" + ss + "&spa=" + spa + "&scs=" + scs + "&cpl=" + cpl + "&ps=" + ps + "&date=" + Date(), "", "dialogHeight:225px; dialogWidth:320px; resizable=yes");
                if (eReturn) {
                    //fld = "txt" + fld;	
                    document.getElementById("lblpagesize").value = eReturn;
                    document.getElementById("lblret").value = "srch";
                    var d = new Date();
                    var curr_date = d.getDate();

                    var curr_month = d.getMonth();
                    curr_month++;
                    var curr_year = d.getFullYear();
                    var hr = d.getHours();
                    var mn = d.getMinutes();
                    var ss = d.getSeconds();
                    document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
                    document.getElementById("form1").submit();
                }
            }
            function getsrch() {
                var eReturn = window.showModalDialog("../equip/maxsearchdialog.aspx?srchtyp=all" + "&date=" + Date(), "", "dialogHeight:720px; dialogWidth:890px; resizable=yes");
                if (eReturn) {
                    var ret = eReturn;
                    //alert(ret)
                    var retarr = ret.split(",");
                    var eid = retarr[2];
                    var sid = retarr[11];
                    var did = retarr[0];
                    var clid = retarr[1];
                    var chk = retarr[9];
                    var lid = retarr[10];

                    var fid = retarr[4];
                    var cid = retarr[12];
                    if (fid != "" && cid != "") {
                        gotoco(eid, fid, cid, sid, did, clid, chk, lid)
                    }
                    else if (fid != "" && cid == "") {
                        gotofu(eid, fid, sid, did, clid, chk, lid)
                    }
                    else {
                        gotoeq(eid, sid, did, clid, chk, lid)
                    }
                    /*
                    gotoeq(eid, sid, did, clid, chk, lid)
                    gotofu(eid, fid, sid, did, clid, chk, lid)
                    gotoco(eid, fid, cid, sid, did, clid, chk, lid)
                    document.getElementById("lbldid").value = retarr[0];
                    document.getElementById("lblclid").value = retarr[1];
                    document.getElementById("lbleqid").value = retarr[2];
                    document.getElementById("tdeq").innerHTML = retarr[3];
                    document.getElementById("lblfuid").value = retarr[4];
                    document.getElementById("tdfunc").innerHTML = retarr[5];
                    document.getElementById("tddept").innerHTML = retarr[7];
                    document.getElementById("tdcell").innerHTML = retarr[8];
                    document.getElementById("lbleq").value = retarr[3];
                    document.getElementById("lblfu").value = retarr[5];
                    document.getElementById("lbldept").value = retarr[7];
                    document.getElementById("lblcell").value = retarr[8];
                    document.getElementById("lblcopytyp").value = "dest";
                    document.getElementById("txtnewkey").value=document.getElementById("lblselkey").value;
                    document.getElementById("txtnewkey").disabled=true;
                    */
                }
                else {

                }
            }
            function getdets(eqid, sid, did, clid, chk, lid, dept, cell, eqnum, loc) {
                //alert(eqnum)
                eqnum = eqnum.replace(/#/, "%23");
                //alert(eqnum)
                var eReturn = window.showModalDialog("../equip/fulist2dialog.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:400px; dialogWidth:450px; resizable=yes");
                if (eReturn) {
                    var ret = eReturn;
                    var retarr = ret.split(",");
                    var fid = retarr[0];
                    var cid = retarr[1];
                    if (fid == "log") {
                        window.parent.setref();
                    }
                    else {
                        if (fid != "no") {
                            if (cid != "") {
                                gotoco(eqid, fid, cid, sid, did, clid, chk, lid, dept, cell, eqnum, loc)
                            }
                            else {
                                gotofu(eqid, fid, sid, did, clid, chk, lid, dept, cell, eqnum, loc)
                            }
                        }
                    }
                }
            }
            function gotoeq(eid, sid, did, clid, chk, lid, dept, cell, eq, loc) {
                //alert(lid)
                var ustr = document.getElementById("lblusername").value;
                /*if (document.getElementById("rb1").checked == true) {
                    var chk = "yes";
                    var ustr = document.getElementById("lblusername").value;
                    window.parent.gotoeq("../equip/eqmain2.aspx?lvl=eq&start=yes&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eid + "&lid=" + lid + "&typ=&ustr=" + ustr + "&dept=" + dept + "&cell=" + cell);
                }
                else*/
                if (document.getElementById("rb12").checked == true) {
                    var chk = "yes";
                    var ustr = document.getElementById("lblusername").value;
                    //alert("../appspmo123tab/pmo123tabmain.aspx?who=eqready&lvl=eq&start=yes&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eid + "&lid=" + lid + "&typ=&dept=" + dept + "&cell=" + cell + "&eq=" + eq + "&loc=" + loc + "&rettyp=&gototasks=1&usrname=" + ustr)
                    window.parent.gotoeq("../appspmo123tab/pmo123tabmain.aspx?who=tabret&lvl=eq&start=yes&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&eqid=" + eid + "&lid=" + lid + "&typ=&dept=" + dept + "&cell=" + cell + "&eq=" + eq + "&loc=" + loc + "&rettyp=&gototasks=1&usrname=" + ustr + "&fuid=&coid=");
                }
                //else if (document.getElementById("rb5").checked == true) {
                    //window.parent.gotoeqlist(eid);
                //}
                else if (document.getElementById("rb6").checked == true) {
                //alert("rb6")
                    window.parent.gotowolisteq(eid);
                }
                else if (document.getElementById("rb7").checked == true) {
                    window.parent.gotopmlisteq(eid);
                }
                //else if (document.getElementById("rb8").checked == true) {
                    //window.parent.gotopmopteq(eid, sid, did, clid, chk, lid);
                //}
                //else if (document.getElementById("rb9").checked == true) {
                    //window.parent.gotopmdeveq(eid, sid, did, clid, chk, lid);
                //}
                //else if (document.getElementById("rb10").checked == true) {
                    //window.parent.gototpmopteq(eid, sid, did, clid, chk, lid);
                //}
                //else if (document.getElementById("rb11").checked == true) {
                    //window.parent.gototpmdeveq(eid, sid, did, clid, chk, lid);
                //}
                else {
                    window.open("../reports/AssignmentReport.aspx?typ=eq&eqid=" + eid + "&date=" + Date());
                }
            }
            function checktime() {
                var d = new Date();
                var curr_date = d.getDate();
                var curr_month = d.getMonth();
                curr_month++;
                var curr_year = d.getFullYear();
                var hr = d.getHours();
                var mn = d.getMinutes();
                var ss = d.getSeconds();
                document.getElementById("lbltl").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
                document.getElementById("td1").innerHTML = document.getElementById("lblt1").value;
                document.getElementById("td2").innerHTML = document.getElementById("lblt2").value;
                document.getElementById("td3").innerHTML = document.getElementById("lblt3").value;
                dateDiff();
            }

        </script>
	</HEAD>
	<body  bgcolor="transparent" onload="checktime();">
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 0px" cellSpacing="0" cellPadding="0" width="265"
				bgcolor="transparent">
				<tr>
					<td class="labelibl">
						<INPUT id="rb12" disabled type="radio" name="rb" runat="server" VALUE="rb12"><asp:Label id="Label1" runat="server">Jump to PMO Tabbed</asp:Label><br>
						<INPUT id="rb7" type="radio" name="rb" runat="server" disabled VALUE="rb7"><asp:Label id="lang3176" runat="server">Jump to PM Records List</asp:Label><br>
						<INPUT id="rb6" type="radio" name="rb" runat="server" disabled VALUE="rb6"><asp:Label id="lang3177" runat="server">Jump to Work Order Records List</asp:Label><br>
						<INPUT id="rb2" type="radio" name="rb" runat="server" VALUE="rb2"><asp:Label id="lang3179" runat="server">View Task Assignment</asp:Label><br>
						<INPUT id="rb3" onclick="getinprog();" type="radio" name="rb"><asp:Label id="lang3180" runat="server">View Work In Progress</asp:Label></td>
				</tr>
                
				<tr>
					<td>
						<div style="HEIGHT: 1px">
							<hr color="gray" SIZE="1">
						</div>
					</td>
				</tr>
                
				<tr>
					<td align="center">
						<table cellpadding="1" cellspacing="0">
							<tr>
								<td class="label"><asp:Label id="lang3181" runat="server">Search</asp:Label></td>
								<td>
									<asp:TextBox id="txtsrch" runat="server" CssClass="plainlabel" Width="130px"></asp:TextBox></td>
								<td><img src="../images/appbuttons/minibuttons/srchsm1.gif" onclick="chksrch();"></td>
								<td><img src="../images/appbuttons/minibuttons/magnifier.gif" onclick="getsrch();" onmouseover="return overlib('Use Max Search', LEFT)"
										onmouseout="return nd()"></td>
							</tr>
							<tr class="details">
								<td colspan="4" align="center">
									<table>
										<tr>
											<td class="plainlabel" id="td0"></td>
											<td><img src="../images/appbuttons/minibuttons/magnifier.gif" onclick="getsaadmin();"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="details">
								<td class="label">T1</td>
								<td class="plainlabel" id="td1" colspan="3"></td>
							</tr>
							<tr class="details">
								<td class="label">T2</td>
								<td class="plainlabel" id="td2" colspan="3"></td>
							</tr>
							<tr class="details">
								<td class="label">T3</td>
								<td class="plainlabel" id="td3" colspan="3"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst2.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev2.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="140"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext2.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast2.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
                <tr><td><img src="../images/appbuttons/minibuttons/2PX.gif" alt="" /></td></tr>
				<tr>
                
					<td >
                    <div id="tdarch" runat="server" style="overflow: auto; height: 375px">
                    </div>
                    </td>
                    
				</tr>
			</table>
			<input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
			<input type="hidden" id="lblret" runat="server" NAME="lblret"> <input type="hidden" id="lblsiteid" runat="server" NAME="lblsiteid">
			<input type="hidden" id="lblcomp" runat="server" NAME="lblcomp"> <input type="hidden" id="lblt1" runat="server" NAME="lblt1">
			<input type="hidden" id="lblt2" runat="server" NAME="lblt2"> <input type="hidden" id="lblt3" runat="server" NAME="lblt3">
			<input type="hidden" id="lbltl" runat="server" NAME="lbltl"> <input type="hidden" id="lblpagesize" runat="server" NAME="lblpagesize">
			<input type="hidden" id="lblfslang" runat="server" /> <input type="hidden" id="lblusername" runat="server" NAME="lblusername">
			<input type="hidden" id="lbluid" runat="server" NAME="lbluid"> <input type="hidden" id="lblislabor" runat="server" NAME="lblislabor">
			<input type="hidden" id="lblissuper" runat="server" NAME="lblissuper"> <input type="hidden" id="lblisplanner" runat="server" NAME="lblisplanner">
			<input type="hidden" id="lblLogged_In" runat="server" NAME="lblLogged_In"> <input type="hidden" id="lblro" runat="server" NAME="lblro">
			<input type="hidden" id="lblms" runat="server" NAME="lblms"> <input type="hidden" id="lblappstr" runat="server" NAME="lblappstr">
		</form>
	</body>
</HTML>
