

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class eqtab2
    Inherits System.Web.UI.Page


    Protected WithEvents lang3129 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3128 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3127 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3126 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3125 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3124 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3123 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3122 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3121 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3120 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3119 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3118 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3117 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim nmm As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim Tables As String = "equipment"
    Dim PK As String = "eqid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*, cnt = (select count(*) from functions f where f.eqid = eqid)"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim sort As String = "modifieddate desc"
    Dim cid, srch, ms, df, ps As String
    Dim decPgNav As Decimal
    Dim intPgNav As Integer
    Dim usr, uid, login, jump, eqid, fuid, coid, typ, appstr, sid As String
    Protected WithEvents lblcurropt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ddcb As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddmb As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txts As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtc As System.Web.UI.WebControls.TextBox
    Protected WithEvents Imagebutton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents trdata As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdarch As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents rbdev As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbtdev As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbopt As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbtopt As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbtab As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdmsg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsrchmsg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsrch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblms As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhref As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            usr = Request.QueryString("username").ToString
            lbluser.Value = usr
            cid = "0"
            lblcid.Value = cid
            ms = Request.QueryString("ms").ToString
            lblms.Value = ms
            appstr = Request.QueryString("appstr").ToString
            CheckApp(appstr)
            tdsrchmsg.InnerHtml = "Searches are limited to your current selected plant site/location. " _
            + "Please visit the PM Library if you would like to Copy or Review accepted records"
            df = Request.QueryString("sid").ToString
            lbldf.Value = df
            Try
                jump = Request.QueryString("jump").ToString
                If jump = "yes" Then
                    typ = Request.QueryString("typ").ToString
                    lbltyp.Value = typ
                    If typ = "eq" Then
                        eqid = Request.QueryString("eqid").ToString
                        lbleqid2.Value = eqid
                    End If
                End If
            Catch ex As Exception
                lbltyp.Value = "no"
            End Try
            txtpg.Value = "1"
            nmm.Open()
            GetBy(ms, df)
            'GetPMS(PageNumber)
            GetArch()
            nmm.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                nmm.Open()
                GetNext()
                nmm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                nmm.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetArch()
                'GetPMS(PageNumber)
                nmm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                nmm.Open()
                GetPrev()
                nmm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                nmm.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetArch()
                'GetPMS(PageNumber)
                nmm.Dispose()
                lblret.Value = ""
            End If
        End If
        Try
            'df = Request.QueryString("psid").ToString
            'ps = Request.QueryString("psite").ToString
            'lbldf.Value = df
            'Session("dfltps") = df
            'Session("psite") = ps
            'Response.Redirect("NewMainMenu2.aspx")
        Catch ex As Exception

        End Try
    End Sub
    Private Sub CheckApp(ByVal appstr)
        Dim apparr() As String = appstr.Split(",")
        Dim app As Integer = 0
        Dim tflg As Integer = 0
        'eq,dev,opt,inv
        If appstr <> "all" And appstr <> "yes" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                If apparr(i) = "dev" Then
                    'rbdev.Disabled = False
                    app += 1
                ElseIf apparr(i) = "opt" Then
                    'rbopt.Disabled = False
                    'rbopt.Checked = True
                    lblcurropt.Value = "rbopt"
                    app += 1
                ElseIf apparr(i) = "otab" Then
                    tflg = 1
                    'rbtab.Disabled = False
                    'rbtab.Checked = True
                    lblcurropt.Value = "rbtab"
                    app += 1
                ElseIf apparr(i) = "tpd" Then
                    'rbtdev.Disabled = False
                    app += 1
                ElseIf apparr(i) = "tpo" Then
                    'rbtopt.Disabled = False
                    app += 1
                End If
            Next

            If tflg = 1 Then
                'rbtab.Disabled = False
                'rbtab.Checked = True
                'lblcurropt.Value = "rbtab"
            Else
                'rbopt.Disabled = False
                'rbopt.Checked = True
                'lblcurropt.Value = "rbopt"
            End If
            If app = 0 Then
                lblappstr.Value = "no"
            Else
                lblappstr.Value = "yes"
                'If rbopt.Disabled = True And rbdev.Disabled = True And tflg <> 1 Then
                'If rbtopt.Disabled = True Then
                'rbtdev.Checked = True
                'lblcurropt.Value = "rbtdev"
                'Else
                'rbtopt.Checked = True
                'lblcurropt.Value = "rbtopt"
                'End If
                'Else
                'If rbopt.Disabled = True And tflg <> 1 Then
                'rbdev.Checked = True
                'lblcurropt.Value = "rbdev"
                'Else
                'If tflg <> 1 Then
                'rbopt.Checked = True
                'lblcurropt.Value = "rbopt"
                'End If

                'End If
            End If
        Else
            lblappstr.Value = "yes"
            'rbdev.Disabled = False
            'rbopt.Disabled = False
            'rbtab.Disabled = False
            'rbtab.Checked = True
            lblcurropt.Value = "rbtab"
            'rbtdev.Disabled = False
            'rbtopt.Disabled = False
        End If
        'End If

        
    End Sub
    Private Sub GetBy(ByVal ms As String, ByVal dfltps As String)
        Dim filt As String
        cid = lblcid.Value
        'If ms = "0" Then
        filt = " where siteid = '" & dfltps & "'"
        'Else

        'End If
        sql = "select distinct createdby from equipment" & filt & " and createdby is not null"
        dr = nmm.GetRdrData(sql)
        ddcb.DataSource = dr
        ddcb.DataValueField = "createdby"
        ddcb.DataTextField = "createdby"
        ddcb.DataBind()
        dr.Close()
        ddcb.Items.Insert("0", "Select")
        sql = "select distinct modifiedby from equipment" & filt & " and createdby is not null"
        dr = nmm.GetRdrData(sql)
        ddmb.DataSource = dr
        ddmb.DataValueField = "modifiedby"
        ddmb.DataTextField = "modifiedby"
        ddmb.DataBind()
        dr.Close()
        ddmb.Items.Insert("0", "Select")
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetArch()
        Catch ex As Exception
            nmm.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1558", "eqtab2.aspx.vb")

            nmm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetArch()
        Catch ex As Exception
            nmm.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1559", "eqtab2.aspx.vb")

            nmm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetArch()
        Dim siteid As String = lbldf.Value
        Dim comp As String = "0"
        'Dim siteid As String = "3"
        Dim sb As StringBuilder = New StringBuilder
        Dim eqnum, eqdesc As String
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 780px; HEIGHT: 280px; background-color: transparent;"" id=""spdiv"" onscroll=""SetsDivPosition();"">")
        sb.Append("<table cellspacing=""2"" cellpadding=""0"" border=""0"" width=""780""><tr>")
        sb.Append("<td width=""15""></td>" & vbCrLf)
        sb.Append("<td width=""15""></td>" & vbCrLf)
        sb.Append("<td width=""15""></td>" & vbCrLf)
        'sb.Append("<td width=""135""></tr>" & vbCrLf)
        sb.Append("<td width=""150""></td>" & vbCrLf)
        sb.Append("<td width=""285""></td>" & vbCrLf)
        sb.Append("<td width=""210""></td>" & vbCrLf)
        sb.Append("<td width=""90""></td></tr>" & vbCrLf)

        sb.Append("<tr bgcolor=""white"" height=""20"">")
        sb.Append("<td></td>" & vbCrLf)
        sb.Append("<td></td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" colspan=""2"">" & tmod.getlbl("cdlbl611", "eqtab2.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"">" & tmod.getlbl("cdlbl612", "eqtab2.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"">" & tmod.getlbl("cdlbl613", "eqtab2.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"">" & tmod.getlbl("cdlbl614", "eqtab2.aspx.vb") & "</td></tr>" & vbCrLf)


        '<tr bgColor="transparent" height="20">
        '<td width="15" class="plainlabel"></td>
        '<td class="thdrsingg plainlabel" width="140">" & tmod.getlbl("cdlbl615" , "eqtab2.aspx.vb") & "</td>
        '<td class="thdrsingg plainlabel" width="230">" & tmod.getlbl("cdlbl616" , "eqtab2.aspx.vb") & "</td>
        '<td class="thdrsingg plainlabel" width="210">" & tmod.getlbl("cdlbl617" , "eqtab2.aspx.vb") & "</td>
        '<td class="thdrsingg plainlabel" width="130">" & tmod.getlbl("cdlbl618" , "eqtab2.aspx.vb") & "</td>
        '</tr>
        sql = "select distinct e.siteid, e.locid, e.dept_id, " _
        + "e.cellid, e.eqid, e.eqnum, isnull(eqdesc, 'No Description') as eqdesc, " _
        + "e.spl, Convert(char(10),e.modifieddate,101) as modifieddate, e.locked, e.lockedby, e.trans, e.transstatus, " _
        + "epcnt = (select count(*) from pmpictures p where p.eqid = e.eqid and p.funcid is null and p.comid is null) " _
        + "from equipment e " _
        + "where e.siteid = '" & siteid & "'"
        typ = lbltyp.Value
        If typ = "no" Then
            FilterCNT = lblfiltcnt.Value
            Filter = lblfilt.Value
            usr = lbluser.Value
            If FilterCNT = "" Then
                Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
                Dim ustr1 As String = Replace(usr, "'", "''''")
                FilterCNT += " and e.modifiedby = '" & ustr & "'"
                Filter += " and e.modifiedby = ''" & ustr1 & "''"
            End If
        Else
            Dim isrch As String = lblsrch.Value
            If isrch = "yes" Then
                FilterCNT = lblfiltcnt.Value
                Filter = lblfilt.Value
            Else
                If typ = "eq" Then
                    eqid = lbleqid2.Value
                    FilterCNT = " and e.eqid = '" & eqid & "'"
                    Filter = " and e.eqid = ''" & eqid & "''"
                ElseIf typ = "fu" Then
                    fuid = lblfuid.Value
                    FilterCNT = " and f.fuid = '" & fuid & "'"
                    Filter = " and f.fuid = ''" & fuid & "''"
                End If
            End If
            
        End If


        sql = "select count(distinct(e.eqid)) " _
        + "from equipment e left outer join functions f on f.eqid = e.eqid " _
        + "left outer join components c on c.func_id = f.func_id  " _
        + "where e.siteid = '" & siteid & "' " & FilterCNT



        PageNumber = txtpg.Value
        intPgNav = nmm.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav

        If intPgNav <> 0 Then
            sql = "usp_geteqlistpg2 '" & comp & "', '" & PageNumber & "', '" & PageSize & "', '" & Filter & "', '" & siteid & "'"

            'h.Open()
            dr = nmm.GetRdrData(sql)
            '*** Multi Add ***
            Dim trans As String = "0"
            Dim tstat As String = "0"
            '*** End Multi Add ***
            Dim locby As String
            Dim lock As String = "0"
            Dim fid As String = "0"
            Dim cid As String = "0"
            Dim eid As String = "0"
            Dim cidhold As Integer = 0
            Dim sid, did, clid, chk, lid As String
            Dim spl, modby, fspl, fmodby, cdesc, cspl As String
            Dim dept, cell, loc As String
            Dim fhold As String = ""
            Dim chold As String = ""
            Dim ehold As String = ""
            Dim hbg As String = "transrowblue" '"#E7F1FD"
            Dim nbg As String = "transrow" '"transparent"
            Dim bg As String = hbg

            Dim fhbg As String = "#E7F1FD"
            Dim fnbg As String = "white"
            Dim fbg As String = fhbg

            Dim chbg As String = "#E7F1FD"
            Dim cnbg As String = "white"
            Dim cbg As String = chbg

            Dim epcnt As Integer = 0
            Dim fpcnt As Integer = 0
            Dim cpcnt As Integer = 0
            Dim parent, parentid As String
            Dim cnt As Integer = 0
            While dr.Read
                If dr.Item("eqid") <> eid Then
                    If eid <> 0 Then
                        sb.Append("</table></td></tr>" & vbCrLf)
                    End If
                    parent = dr.Item("parent").ToString
                    parentid = dr.Item("parentid").ToString

                    dept = dr.Item("dept_line").ToString
                    dept = Replace(dept, "#", "%23")
                    cell = dr.Item("cell_name").ToString
                    cell = Replace(cell, "#", "%23")
                    loc = dr.Item("location").ToString
                    loc = Replace(loc, "#", "%23")

                    eid = dr.Item("eqid").ToString
                    sid = dr.Item("siteid").ToString
                    did = dr.Item("dept_id").ToString
                    clid = dr.Item("cellid").ToString
                    lid = dr.Item("locid").ToString
                    epcnt = dr.Item("epcnt").ToString
                    If clid <> "" Then
                        chk = "yes"
                    Else
                        chk = "no"
                    End If
                    eqnum = dr.Item("eqnum").ToString
                    eqdesc = dr.Item("eqdesc").ToString
                    spl = dr.Item("spl").ToString
                    modby = dr.Item("modifieddate").ToString
                    lock = dr.Item("locked").ToString
                    locby = dr.Item("lockedby").ToString
                    If ehold <> eid Then
                        ehold = eid
                        If bg = hbg Then
                            bg = nbg
                        Else
                            bg = hbg
                        End If
                    End If

                    sb.Append("<tr  height=""20""><td><img id='i" + eid + "' ")
                    sb.Append("onclick=""getdets('" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & parentid & "','" & eqnum & "','" & dept & "','" & cell & "','" & parent & "');""")
                    sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                    If epcnt <> 0 Then
                        sb.Append("<td><img src=""../images/appbuttons/minibuttons/gridpic.gif"" onclick=""geteqport('" + eid + "')""></td>" & vbCrLf)
                    Else
                        sb.Append("<td><img src=""../images/appbuttons/minibuttons/gridpicdis.gif""></td>" & vbCrLf)
                    End If
                    sb.Append("<td colspan=""2"" class=""plainlabel " & bg & """ ><a href=""#""")
                    sb.Append("onclick=""gotoeq('" & eid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & chk & "', '" & parentid & "','" & eqnum & "','" & dept & "','" & cell & "','" & parent & "')""")
                    sb.Append("class=""linklabel"" >" & eqnum & "</a></td>" & vbCrLf)
                    sb.Append("<td class=""plainlabel " & bg & """ >" & eqdesc & "</td>" & vbCrLf)
                    sb.Append("<td class=""plainlabel " & bg & """ >" & spl & "</td>" & vbCrLf)
                    sb.Append("<td class=""plainlabel " & bg & """ >" & modby & "</td></tr>" & vbCrLf)

                    sb.Append("<tr><td></td><td colspan=""6""><table class=""details"" cellspacing=""0"" id='t" + eid + "' border=""0"">")
                    sb.Append("<tr><td width=""15""></td><td width=""15""></td><td width=""15""></td><td width=""700""></td></tr>" & vbCrLf)

                End If

            End While
            dr.Close()
            'h.Dispose()
            sb.Append("</td></tr></table></td></tr></table>")
        Else
            sb.Append("<tr><td class=""bluelabel"" align=""center"" colspan=""6"">No Records Modified by " & usr & "</td></tr></table>")
            'tdmsg.InnerHtml = "No Records Modified by " & usr
        End If

        sb.Append("</div>")
        'Response.Write(sb.ToString)
        tdarch.InnerHtml = sb.ToString
    End Sub


    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        Dim field, srch As String
        Dim SrchFilt As String = ""
        Dim CntFilt As String = ""
        Dim cs = lbldf.Value
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", " ", , , vbTextCompare)
        If Len(srch) > 0 Then
            SrchFilt += " and (e.eqnum like ''%" & srch & "%'' " _
                + "or e.eqdesc like ''%" & srch & "%'' " _
                + "or e.spl like ''%" & srch & "%'') "
            CntFilt += " and (e.eqnum like '%" & srch & "%' " _
                + "or e.eqdesc like '%" & srch & "%' " _
                + "or e.spl like '%" & srch & "%') "
        End If


        lblfilt.Value = SrchFilt
        lblfiltcnt.Value = CntFilt
        lblsrch.Value = "yes"
        PageNumber = "1"
        'Try
        Dim nmm1 As New Utilities
        nmm.Open()
        'GetPMS(PageNumber)
        GetArch()
        nmm.Dispose()
        'Catch ex As Exception

        'End Try

    End Sub

    Private Sub Imagebutton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click
        Dim field, srch As String
        Dim SrchFilt As String = ""
        Dim CntFilt As String = ""
        Dim sd As String = txts.Text
        Dim cd As String = txtc.Text
        If Len(sd) > 0 And Len(cd) > 0 Then
            'SrchFilt += " and (e.createdate >= ''" & sd & "'') "
            'CntFilt += " and (e.createdate >= '" & sd & "') "
            'SrchFilt += " and (e.modifieddate <= ''" & sd & "'') "
            'CntFilt += " and (e.modifieddate <= '" & sd & "') "
            SrchFilt += " and ((e.createdate between ''" & sd & "'' and ''" & cd & "'') " _
                + "or (e.modifieddate between ''" & sd & "'' and ''" & cd & "'')) "
            CntFilt += " and (e.createdate between '" & sd & "' and '" & cd & "' " _
                + "or e.modifieddate between '" & sd & "' and '" & cd & "') "
        ElseIf Len(sd) > 0 And Len(cd) = 0 Then
            SrchFilt += " and (e.createdate >= ''" & sd & "'' " _
                + "or e.modifieddate >= ''" & sd & "'') "
            CntFilt += " and (e.createdate >= '" & sd & "' " _
                + "or e.modifieddate >= '" & sd & "') "
        ElseIf Len(sd) = 0 And Len(cd) > 0 Then
            SrchFilt += " and (e.modifieddate <= ''" & sd & "'' " _
                + "or e.modifieddate <= ''" & sd & "'') "
            CntFilt += " and (e.modifieddate <= '" & sd & "' " _
                + "or e.modifieddate <= '" & sd & "') "
        End If

        If ddcb.SelectedIndex <> 0 Then
            Dim c As String = ddcb.SelectedValue.ToString
            Dim ustr As String = Replace(c, "'", Chr(180), , , vbTextCompare)
            Dim ustr1 As String = Replace(c, "'", "''''")
            SrchFilt += " and e.createdby = ''" & ustr & "''"
            CntFilt += " and e.createdby = '" & ustr & "'"
        End If

        If ddmb.SelectedIndex <> 0 Then
            Dim m As String = ddmb.SelectedValue.ToString
            Dim ustr As String = Replace(m, "'", Chr(180), , , vbTextCompare)
            Dim ustr1 As String = Replace(m, "'", "''''")
            SrchFilt += " and e.modifiedby = ''" & ustr1 & "''"
            CntFilt += " and e.modifiedby = '" & ustr & "'"
        End If

        lblfilt.Value = SrchFilt
        lblfiltcnt.Value = CntFilt
        lblsrch.Value = "yes"
        PageNumber = "1"
        'Try
        Dim nmm1 As New Utilities
        nmm.Open()
        'GetPMS(PageNumber)
        GetArch()
        nmm.Dispose()
        'Catch ex As Exception

        'End Try
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3117.Text = axlabs.GetASPXPage("eqtab2.aspx", "lang3117")
        Catch ex As Exception
        End Try
        Try
            lang3118.Text = axlabs.GetASPXPage("eqtab2.aspx", "lang3118")
        Catch ex As Exception
        End Try
        Try
            lang3119.Text = axlabs.GetASPXPage("eqtab2.aspx", "lang3119")
        Catch ex As Exception
        End Try
        Try
            lang3120.Text = axlabs.GetASPXPage("eqtab2.aspx", "lang3120")
        Catch ex As Exception
        End Try
        Try
            lang3121.Text = axlabs.GetASPXPage("eqtab2.aspx", "lang3121")
        Catch ex As Exception
        End Try
        Try
            lang3122.Text = axlabs.GetASPXPage("eqtab2.aspx", "lang3122")
        Catch ex As Exception
        End Try
        Try
            lang3123.Text = axlabs.GetASPXPage("eqtab2.aspx", "lang3123")
        Catch ex As Exception
        End Try
        Try
            lang3124.Text = axlabs.GetASPXPage("eqtab2.aspx", "lang3124")
        Catch ex As Exception
        End Try
        Try
            lang3125.Text = axlabs.GetASPXPage("eqtab2.aspx", "lang3125")
        Catch ex As Exception
        End Try
        Try
            lang3126.Text = axlabs.GetASPXPage("eqtab2.aspx", "lang3126")
        Catch ex As Exception
        End Try
        Try
            lang3127.Text = axlabs.GetASPXPage("eqtab2.aspx", "lang3127")
        Catch ex As Exception
        End Try
        Try
            lang3128.Text = axlabs.GetASPXPage("eqtab2.aspx", "lang3128")
        Catch ex As Exception
        End Try
        Try
            lang3129.Text = axlabs.GetASPXPage("eqtab2.aspx", "lang3129")
        Catch ex As Exception
        End Try

    End Sub



End Class
