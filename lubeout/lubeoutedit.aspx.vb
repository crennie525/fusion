

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class lubeoutedit
    Inherits System.Web.UI.Page
	Protected WithEvents imgei As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents imgee As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents imga As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents img As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang3084 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim meas As New Utilities
    Dim gmeas As New Utilities
    Dim dslev As DataSet
    Dim eqid, fuid, coid, ro, Login As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddfunc As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dgmeas As System.Web.UI.WebControls.DataGrid
    Protected WithEvents tdfunc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgmag As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcharturl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchngfunc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchngcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfunc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            'lbllog.Value = "no"
            'Exit Sub
        End Try
        If Not IsPostBack Then
            Dim charturl As String = System.Configuration.ConfigurationManager.AppSettings("chartURL")
            lblcharturl.Value = charturl

            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                dgmeas.Columns(0).Visible = False

            End If
            meas.Open()
            Try
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                Dim fcnt As Integer = PopFunc()
                lblfcnt.Value = fcnt
                imgmag.Attributes.Add("class", "details")
                If fcnt <> 0 Then
                    tdfunc.Attributes.Add("class", "label view")
                    ddfunc.Attributes.Add("class", "view")
                Else
                    tdfunc.Attributes.Add("class", "details")
                    ddfunc.Attributes.Add("class", "details")
                End If

            Catch ex As Exception
                eqid = "0"
                lbleqid.Value = eqid
                imgmag.Attributes.Add("class", "details")
                tdfunc.Attributes.Add("class", "details")
                ddfunc.Attributes.Add("class", "details")
            End Try
            Try
                fuid = Request.QueryString("fuid").ToString
                If fuid = "" Then
                    fuid = "0"
                End If
                lblfuid.Value = fuid
                imgmag.Attributes.Add("class", "view")
                tdfunc.Attributes.Add("class", "details")
                ddfunc.Attributes.Add("class", "details")
            Catch ex As Exception
                fuid = "0"
                lblfuid.Value = fuid
            End Try
            Try
                coid = Request.QueryString("coid").ToString
                If coid = "" Then
                    coid = "0"
                End If
                lblcoid.Value = coid
                imgmag.Attributes.Add("class", "view")
                tdfunc.Attributes.Add("class", "details")
                ddfunc.Attributes.Add("class", "details")
            Catch ex As Exception
                coid = "0"
                lblcoid.Value = coid
            End Try

            GetMeas(eqid, fuid, coid)
            meas.Dispose()
        Else
            If Request.Form("lblsubmit") = "chng" Then
                lblsubmit.Value = ""
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = lblcoid.Value
                meas.Open()
                GetMeas(eqid, fuid, coid)
                meas.Dispose()
            End If

        End If
    End Sub
    Private Function PopFunc() As Integer
        sql = "select count(*) funcid from outlube where eqid = '" & eqid & "' and funcid is not null"
        Dim fcnt As Integer
        fcnt = meas.Scalar(sql)
        eqid = lbleqid.Value
        sql = "select func_id, func from functions where func_id in (" _
        + "select funcid from outlube where eqid = '" & eqid & "')"
        dr = meas.GetRdrData(sql)
        ddfunc.DataSource = dr
        ddfunc.DataTextField = "func"
        ddfunc.DataValueField = "func_id"
        ddfunc.DataBind()
        dr.Close()
        ddfunc.Items.Insert(0, New ListItem("Select"))
        ddfunc.Items(0).Value = 0
        Return fcnt
    End Function
    Private Sub GetMeas(ByVal eqid As String, Optional ByVal fuid As String = "0", Optional ByVal coid As String = "0")
        Dim mcnt As Integer

        If fuid = "0" And coid = "0" Then
            'sql = "select count(*) from outmeasure where eqid = '" & eqid & "'"
            'mcnt = meas.Scalar(sql)
            sql = "select o.olid, o.lubetitle, o.lubenum, o.lubetask, o.lubeid, o.siteid, o.deptid, o.cellid, o.loc, o.eqid, o.funcid, " _
            + "o.comid, o.createdby, o.createdate, f.func, c.compnum, " _
            + "cnt = (select count(*) from outlubetrack t where t.olid = o.olid) from outlube o " _
            + "left join functions f on f.func_id = o.funcid left join components c on c.comid = o.comid where o.eqid = '" & eqid & "'"
        ElseIf fuid <> "0" And coid = "0" Then
            'sql = "select count(*) from outmeasure where eqid = '" & eqid & "' and funcid = '" & fuid & "'"
            'mcnt = meas.Scalar(sql)
            sql = "select o.olid, o.lubetitle, o.lubenum, o.lubetask, o.lubeid, o.siteid, o.deptid, o.cellid, o.loc, o.eqid, o.funcid, " _
              + "o.comid, o.createdby, o.createdate, f.func, c.compnum, " _
              + "cnt = (select count(*) from outlubetrack t where t.olid = o.olid) from outlube o " _
             + "left join functions f on f.func_id = o.funcid left join components c on c.comid = o.comid where o.eqid = '" & eqid & "' and o.funcid = '" & fuid & "'"
        ElseIf fuid <> "0" And coid <> "0" Then
            'sql = "select count(*) from outmeasure where eqid = '" & eqid & "' and funcid = '" & fuid & "' " _
            '+ "and comid = '" & coid & "'"
            'mcnt = meas.Scalar(sql)
            sql = "select o.olid, o.lubetitle, o.lubenum, o.lubetask, o.lubeid, o.siteid, o.deptid, o.cellid, o.loc, o.eqid, o.funcid, " _
            + "o.comid, o.createdby, o.createdate, f.func, c.compnum, " _
            + "cnt = (select count(*) from outlubetrack t where t.olid = o.olid) from outlube o " _
           + "left join functions f on f.func_id = o.funcid left join components c on c.comid = o.comid where o.eqid = '" & eqid & "' and o.funcid = '" & fuid & "' " _
           + "and o.comid = '" & coid & "'"
        End If

        dr = meas.GetRdrData(sql)
        dgmeas.DataSource = dr
        dgmeas.DataBind()


    End Sub

    Private Sub ddfunc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddfunc.SelectedIndexChanged
        eqid = lbleqid.Value
        If ddfunc.SelectedIndex <> 0 Then
            fuid = ddfunc.SelectedValue
            lblfuid.Value = fuid
            lblfunc.Value = ddfunc.SelectedItem.ToString
            lblcoid.Value = "0"
            meas.Open()
            GetMeas(eqid, fuid)
            meas.Dispose()

        Else
            lblfuid.Value = "0"
            lblcoid.Value = "0"
            meas.Open()
            GetMeas(eqid)
            meas.Dispose()

        End If
        lblchngfunc.Value = "yes"
    End Sub

    Private Sub dgmeas_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgmeas.ItemDataBound
        Dim cnt As String
        Dim icnt As Integer

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            cnt = DataBinder.Eval(e.Item.DataItem, "cnt").ToString
            Try
                icnt = Convert.ToInt32(cnt)
            Catch ex As Exception
                icnt = 0
            End Try
            Dim img As HtmlImage = CType(e.Item.FindControl("imgedit"), HtmlImage)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "olid").ToString
            img.Attributes.Add("onclick", "getedit('" & id & "')")

          
            Dim img1 As HtmlImage = CType(e.Item.FindControl("img"), HtmlImage)
          
            If icnt > 0 Then
                img1.Attributes("onclick") = "print('" & id & "');"
            Else
                img1.Attributes("class") = "details"
            End If
            Dim imgei As HtmlImage = CType(e.Item.FindControl("imgei"), HtmlImage)
            imgei.Attributes.Add("onclick", "gettskedit('" & id & "');")

            Dim img2 As HtmlImage = CType(e.Item.FindControl("img1"), HtmlImage)
            img2.Attributes.Add("onclick", "getadd('" & id & "');")
        End If
    End Sub
	

	

	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgmeas.Columns(0).HeaderText = dlabs.GetDGPage("lubeoutedit.aspx","dgmeas","0")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(1).HeaderText = dlabs.GetDGPage("lubeoutedit.aspx","dgmeas","1")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(2).HeaderText = dlabs.GetDGPage("lubeoutedit.aspx","dgmeas","2")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(3).HeaderText = dlabs.GetDGPage("lubeoutedit.aspx","dgmeas","3")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(4).HeaderText = dlabs.GetDGPage("lubeoutedit.aspx","dgmeas","4")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(5).HeaderText = dlabs.GetDGPage("lubeoutedit.aspx","dgmeas","5")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(6).HeaderText = dlabs.GetDGPage("lubeoutedit.aspx","dgmeas","6")
		Catch ex As Exception
		End Try

	End Sub

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang3084.Text = axlabs.GetASPXPage("lubeoutedit.aspx","lang3084")
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetFSOVLIBS()
		Dim axovlib as New aspxovlib
		Try
			img.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("lubeoutedit.aspx","img") & "', ABOVE, LEFT)")
			img.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			Img1.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("lubeoutedit.aspx","Img1") & "', ABOVE, LEFT)")
			Img1.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			imga.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("lubeoutedit.aspx","imga") & "', ABOVE, LEFT)")
			imga.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			imgee.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("lubeoutedit.aspx","imgee") & "', ABOVE, LEFT)")
			imgee.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			imgei.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("lubeoutedit.aspx","imgei") & "', ABOVE, LEFT)")
			imgei.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			imgmag.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("lubeoutedit.aspx","imgmag") & "', ABOVE, LEFT)")
			imgmag.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try

	End Sub

End Class
