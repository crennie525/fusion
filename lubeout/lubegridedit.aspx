<%@ Page Language="vb" AutoEventWireup="false" Codebehind="lubegridedit.aspx.vb" Inherits="lucy_r12.lubegridedit" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>lubegridedit</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/lubegrideditaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 2px; LEFT: 2px" width="310" cellpadding="1" cellspacing="1">
				<tr>
					<td class="label" width="100"><asp:Label id="lang3077" runat="server">Lube Task Title</asp:Label></td>
					<td width="200">
						<asp:TextBox id="txtmtitle" runat="server" Width="150px" CssClass="plainlabel"></asp:TextBox></td>
				</tr>
				<tr>
					<td class="label" colspan="2" height="20"><asp:Label id="lang3078" runat="server">Lube Task Description</asp:Label></td>
				</tr>
				<tr>
					<td colspan="2">
						<asp:TextBox id="txttask" runat="server" Width="300px" Height="100px" TextMode="MultiLine"></asp:TextBox></td>
				</tr>
				<tr>
					<td colspan="2">
						<table>
							<tr>
								<td class="label" width="80"><asp:Label id="lang3079" runat="server">Lubricant</asp:Label></td>
								<td><asp:TextBox id="txtlube" runat="server" Width="190px" CssClass="plainlabel" ReadOnly="True"></asp:TextBox></td>
								<td width="20"><IMG onmouseover="return overlib('Add Lubricant for this Task')" onclick="GetLubeDiv();"
										onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/lubetrans.gif"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="right" colspan="2"><IMG id="btnadd" onclick="addToTask();" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif"
							runat="server">&nbsp;<IMG id="btnreturn" onclick="handleexit();" alt="" src="../images/appbuttons/minibuttons/candisk1.gif"
							runat="server">
					</td>
				</tr>
			</table>
			<input id="lbltaskid" type="hidden" runat="server" NAME="lbltaskid"> <input id="lblcid" type="hidden" runat="server" NAME="lblcid">
			<input id="lblrettype" type="hidden" runat="server" NAME="lblrettype"><input id="lbltypeid" type="hidden" runat="server" NAME="lbltypeid">
			<input id="lblmeasid" type="hidden" runat="server" NAME="lblmeasid"><input id="lblchr" type="hidden" runat="server" NAME="lblchr">
			<input id="lblout" type="hidden" runat="server" NAME="lblout"><input id="lbllog" type="hidden" name="lbllog" runat="server">
			<input id="lbltyp" type="hidden" runat="server" NAME="lbltyp"> <input type="hidden" id="lblwonum" runat="server" NAME="lblwonum">
			<input type="hidden" id="lbljpid" runat="server" NAME="lbljpid"> <input type="hidden" id="lblro" runat="server" NAME="lblro">
			<input type="hidden" id="lblrev" runat="server" NAME="lblrev"> <input type="hidden" id="lbldid" runat="server" NAME="lbldid">
			<input type="hidden" id="lblsid" runat="server" NAME="lblsid"> <input type="hidden" id="lblclid" runat="server" NAME="lblclid">
			<input type="hidden" id="lbllocid" runat="server" NAME="lbllocid"> <input type="hidden" id="lbleqid" runat="server" NAME="lbleqid">
			<input type="hidden" id="lblfuid" runat="server" NAME="lblfuid"> <input type="hidden" id="lblcoid" runat="server" NAME="lblcoid">
			<input type="hidden" id="lblcurrtab" runat="server" NAME="lblcurrtab"> <input type="hidden" id="lbllubeid" runat="server" NAME="lbllubeid">
			<input type="hidden" id="lblolid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
