<%@ Page Language="vb" AutoEventWireup="false" Codebehind="lubesmain.aspx.vb" Inherits="lucy_r12.lubesmain" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>lubesmain</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script language="JavaScript" src="../scripts/reportnav.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/lubesmainaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="checkit();checkpage();" >
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 0px; LEFT: 4px" id="scrollmenu" cellSpacing="0"
				cellPadding="0" width="1080">

				<tr>
					<td width="26"></td>
					<td width="218"></td>
					<td width="270"></td>
					<td width="5"></td>
					<td width="26"></td>
					<td width="174"></td>
					<td width="5"></td>
					<td width="26"></td>
					<td width="275"></td>
				</tr>
				<tr>
					<td colSpan="9">
						<table cellSpacing="0" cellPadding="0" width="1025">
							<tr>
								<td width="230"><IMG src="../menu/mimages/fusionsm.gif"></td>
								<td vAlign="top" width="420"><IMG src="../images/appheaders/meas.gif"></td>
								<td vAlign="top" width="375" align="right"><IMG id="btnreturn" onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="9">
						<table width="1025">
							<tr>
								<td id="tdeq" class="plainlabelred" height="30" colSpan="2" runat="server"><asp:Label id="lang3087" runat="server">No Equipment Record Selected</asp:Label></td>
								<td id="tdfu" class="plainlabelred" height="30" colSpan="2" runat="server"><asp:Label id="lang3088" runat="server">No Function Record Selected</asp:Label></td>
								<td id="tdco" class="plainlabelred" height="30" colSpan="2" runat="server"><asp:Label id="lang3089" runat="server">No Component Record Selected</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="trhead" runat="server">
					<td class="thdrsinglft"><IMG src="../images/appbuttons/minibuttons/luberhdr.gif"></td>
					<td class="thdrsingrt label" colSpan="5" align="left"><asp:Label id="lang3090" runat="server">Lubrication Location Details</asp:Label></td>
					<td>&nbsp;</td>
					<td class="thdrsinglft"><IMG src="../images/appbuttons/minibuttons/luberhdr.gif"></td>
					<td class="thdrsingrt label" align="left"><asp:Label id="lang3091" runat="server">Add Lube Task</asp:Label></td>
				</tr>
				<tr id="trmain" runat="server">
					<td vAlign="top" colSpan="2">
						<table width="244">
							<tr>
								<td class="btmmenu plainlabel" colSpan="2"><asp:Label id="lang3092" runat="server">Search Equipment</asp:Label></td>
							</tr>
							<tr>
								<td class="label" width="84"><asp:Label id="lang3093" runat="server">Equipment#</asp:Label></td>
								<td><asp:textbox id="txteqnum" runat="server" Width="160px" CssClass="plainlabel"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang3094" runat="server">Description</asp:Label></td>
								<td><asp:textbox id="txteqdesc" runat="server" Width="160px" CssClass="plainlabel"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label">SPL</td>
								<td><asp:textbox id="txtspl" runat="server" Width="160px" CssClass="plainlabel"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label">OEM</td>
								<td><asp:textbox id="txtoem" runat="server" Width="160px" CssClass="plainlabel"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang3095" runat="server">Model</asp:Label></td>
								<td><asp:textbox id="txtmodel" runat="server" Width="160px" CssClass="plainlabel"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang3096" runat="server">Serial#</asp:Label></td>
								<td><asp:textbox id="txtserial" runat="server" Width="160px" CssClass="plainlabel"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang3097" runat="server">Asset Class</asp:Label></td>
								<td><asp:dropdownlist id="ddac" runat="server" CssClass="plainlabel"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td colSpan="2" align="right"><IMG onclick="srcheq();" src="../images/appbuttons/minibuttons/srchsm.gif">
									<IMG onclick="goback();" src="../images/appbuttons/minibuttons/refreshit.gif">
								</td>
							</tr>
						</table>
					</td>
					<td vAlign="top">
						<table width="270">
							<tr>
								<td class="btmmenu plainlabel"><asp:Label id="lang3098" runat="server">Equipment Records</asp:Label></td>
							</tr>
							<tr>
								<td>
									<div style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 270px; HEIGHT: 168px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
										id="diveq" runat="server"></div>
								</td>
							</tr>
							<tr>
								<td align="center">
									<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
										cellSpacing="0" cellPadding="0">
										<tr>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="190" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
													runat="server"></td>
											<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
													runat="server"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td></td>
					<td vAlign="top" colSpan="2">
						<table width="200">
							<tr>
								<td class="btmmenu plainlabel"><asp:Label id="lang3099" runat="server">Equipment Functions</asp:Label></td>
							</tr>
							<tr>
								<td><iframe style="BACKGROUND-COLOR: transparent" id="iffunc" height="88" src="lubeoutfunc.aspx"
										frameBorder="no" width="200" allowTransparency scrolling="no" runat="server"></iframe>
								</td>
							</tr>
							<tr>
								<td class="btmmenu plainlabel"><asp:Label id="lang3100" runat="server">Function Components</asp:Label></td>
							</tr>
							<tr>
								<td><iframe style="BACKGROUND-COLOR: transparent" id="ifcomp" height="88" src="lubeoutcomp.aspx"
										frameBorder="no" width="200" allowTransparency scrolling="no" runat="server"></iframe>
								</td>
							</tr>
						</table>
					</td>
					<td></td>
					<td vAlign="top" colSpan="2"><iframe style="BACKGROUND-COLOR: transparent" id="ifmeas" height="230" src="lubeout.aspx?curr=none"
							frameBorder="no" width="312" allowTransparency scrolling="no" runat="server"></iframe>
					</td>
				</tr>
				<tr>
					<td colSpan="9">
						<table cellSpacing="0">
							<tr>
								<td id="tdcm1" class="thdrsingorangelft hand" onclick="getcm();" width="26"><IMG id="imgcm" src="../images/appbuttons/minibuttons/luberhdro.gif"></td>
								<td id="tdcm2" class="thdrsingorangert label hand" onclick="getcm();" width="149" align="left"><asp:Label id="lang3101" runat="server">Lube Tasks</asp:Label></td>
								<td width="5">&nbsp;</td>
								<td id="tdtm1" class="thdrsinglft hand" onclick="gettm();" width="26"><IMG id="imgtm" src="../images/appbuttons/minibuttons/luberhdr.gif"></td>
								<td id="tdtm2" class="thdrsingrt label hand" onclick="gettm();" width="149" align="left"><asp:Label id="lang3102" runat="server">PM Lube Tasks</asp:Label></td>
								<td width="5">&nbsp;</td>
								<td id="tdrt1" class="thdrsinglft hand" onclick="getrt();" width="26"><IMG id="imgrt" src="../images/appbuttons/minibuttons/luberhdr.gif"></td>
								<td id="tdrt2" class="thdrsingrt label hand" onclick="gettr();" width="149" align="left"><asp:Label id="lang3103" runat="server">Lube Routes</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="9"><iframe style="BACKGROUND-COLOR: transparent" id="ifsel" height="380" src="lubeoutedit.aspx"
							frameBorder="no" width="1080" allowTransparency runat="server"></iframe>
					</td>
				</tr>
			</table>
			<input id="lbleqid" type="hidden" name="lbleqid" runat="server"> <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input id="lblcid" type="hidden" name="lblcid" runat="server"><input id="lblpgemstxt" type="hidden" name="lblpgemstxt" runat="server">
			<input id="lbleqnum" type="hidden" name="lbleqnum" runat="server"><input id="lblpgemshref" type="hidden" name="Hidden1" runat="server">
			<input id="lbltasklev" type="hidden" name="lbltasklev" runat="server"> <input id="lblsid" type="hidden" name="lblsid" runat="server">
			<input id="lbldid" type="hidden" name="lbldid" runat="server"> <input id="lblclid" type="hidden" name="lblclid" runat="server">
			<input id="lblchk" type="hidden" name="lblchk" runat="server"> <input id="lblmod" type="hidden" name="lblmod" runat="server">
			<input id="lblredir" type="hidden" name="lblredir" runat="server"><input id="lblsubmit" type="hidden" name="lblsubmit" runat="server">
			<input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server"><input id="txtpg" type="hidden" name="txtpg" runat="server">
			<input id="lbleqdesc" type="hidden" name="lbleqdesc" runat="server"> <input id="lblspl" type="hidden" name="lblspl" runat="server">
			<input id="lblmodel" type="hidden" name="lblmodel" runat="server"> <input id="lbloem" type="hidden" name="lbloem" runat="server">
			<input id="lblserial" type="hidden" name="lblserial" runat="server"> <input id="lblac" type="hidden" name="lblac" runat="server">
			<input id="lbleqnumsrch" type="hidden" name="lbleqnumsrch" runat="server"> <input id="lblrepcnt" type="hidden" name="lblrepcnt" runat="server">
			<input id="lblmeascnt" type="hidden" name="lblmeascnt" runat="server"> <input id="lblfunctions" type="hidden" name="lblfunctions" runat="server">
			<input id="lblcomponents" type="hidden" name="lblcomponents" runat="server"> <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
			<input id="lblcharturl" type="hidden" name="lblcharturl" runat="server"> <input id="lblcurrtab" type="hidden" name="lblcurrtab" runat="server">
			<input id="xCoord" type="hidden" name="xCoord" runat="server"> <input id="yCoord" type="hidden" name="yCoord" runat="server">
			<input id="lbllog" type="hidden" name="lbllog" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
