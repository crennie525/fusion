

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text

Public Class lubelistmini
    Inherits System.Web.UI.Page
	Protected WithEvents lang3080 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, Login As String
    Dim dr As SqlDataReader
    Dim rep As New Utilities
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpart As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsrch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblfilter As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilterwd As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Sort As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents divlube As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            txtpg.Value = "1"
            rep.Open()
            GetLubes(PageNumber)
            rep.Dispose()
        Else
            If Request.Form("lblsubmit") = "next" Then
                rep.Open()
                GetNext()
                rep.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "last" Then
                rep.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetLubes(PageNumber)
                rep.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "prev" Then
                rep.Open()
                GetPrev()
                rep.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "first" Then
                rep.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetLubes(PageNumber)
                rep.Dispose()
                lblsubmit.Value = ""
            End If

        End If
    End Sub
    Private Sub GetLubes(ByVal PageNumber As Integer)
        Dim li, lo As String
        Dim sb As New StringBuilder
        'Get Count
        Dim Filt As String = lblfilter.Value
        If Len(Filt) > 0 Then
            Filter = lblfilterwd.Value
            sql = "select count(*)from lubricants where " & Filt
        Else
            Filter = ""
            sql = "select count(*)from lubricants"
        End If
        Dim intPgCnt, intPgNav As Integer

        intPgNav = rep.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav

        Tables = "lubricants"
        PK = "lubeid"
        PageSize = "200"
        Fields = "*"
        dr = rep.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        sb.Append("<table>")
        While dr.Read
            li = dr.Item("lubeid").ToString
            lo = dr.Item("lubenum").ToString
            sb.Append("<tr><td>")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getlube('" & li & "','" & lo & "')"">")
            sb.Append(lo)
            sb.Append("</td></tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        divlube.InnerHtml = sb.ToString
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetLubes(PageNumber)
        Catch ex As Exception
            rep.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1548" , "lubelistmini.aspx.vb")
 
            rep.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetLubes(PageNumber)
        Catch ex As Exception
            rep.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1549" , "lubelistmini.aspx.vb")
 
            rep.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub btnsrch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsrch.Click
        Dim Filt As String = txtpart.Text
        Filt = Replace(Filt, "'", Chr(180), , , vbTextCompare)
        Filt = Replace(Filt, "--", "-", , , vbTextCompare)
        Filt = Replace(Filt, ";", ":", , , vbTextCompare)
        If Len(Filt) > 0 Then
            lblfilter.Value = "lubenum like '%" & Filt & "%' or description like '%" & Filt & "%' or location like '%" & Filt & "%'"
            lblfilterwd.Value = "lubenum like ''%" & Filt & "%'' or description like ''%" & Filt & "%'' or location like ''%" & Filt & "%''"
        End If
        rep.Open()
        txtpg.Value = "1"
        PageNumber = "1" 'txtipg.Value
        GetLubes(PageNumber)
        rep.Dispose()
    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang3080.Text = axlabs.GetASPXPage("lubelistmini.aspx","lang3080")
		Catch ex As Exception
		End Try

	End Sub

End Class
