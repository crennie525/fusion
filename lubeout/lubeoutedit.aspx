<%@ Page Language="vb" AutoEventWireup="false" Codebehind="lubeoutedit.aspx.vb" Inherits="lucy_r12.lubeoutedit" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>lubeoutedit</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script language="JavaScript" src="../scripts1/lubeouteditaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="checkstuff();scrolltop();" >
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 0px; LEFT: 4px" id="scrollmenu">
				<tr>
					<td>
						<table>
							<tr>
								<td id="tdfunc" class="label" width="80" runat="server"><asp:Label id="lang3084" runat="server">Function</asp:Label></td>
								<td width="220"><asp:dropdownlist id="ddfunc" runat="server" AutoPostBack="True"></asp:dropdownlist></td>
								<td width="750" align="right"><IMG id="imgmag" onmouseover="return overlib('View All Equipment Records', ABOVE, LEFT)"
										onmouseout="return nd()" onclick="getall();" alt="" src="../images/appbuttons/minibuttons/magnifier.gif" runat="server">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:datagrid id="dgmeas" runat="server" ShowFooter="True" CellPadding="0" GridLines="None" AllowPaging="True"
							AllowCustomPaging="True" AutoGenerateColumns="False" CellSpacing="1" BackColor="transparent"
							Width="1050px">
							<FooterStyle BackColor="transparent"></FooterStyle>
							<EditItemStyle Height="15px" BackColor="transparent"></EditItemStyle>
							<AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
							<ItemStyle CssClass="ptransrow"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="30px" Height="18px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<img id="imgedit" runat="server" src="../images/appbuttons/minibuttons/lilpentrans.gif">
									</ItemTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Title">
									<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label17" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.lubetitle") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label18" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.lubetitle") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Function">
									<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label1" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label11" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Component">
									<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label12" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label13" runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.compnum") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Lubricant">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label4 runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.lubenum") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label3" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.lubenum") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Lube Task">
									<HeaderStyle Width="330px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.lubetask") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label14" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.lubetask") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="History">
									<HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<IMG id="img" runat="server" onmouseover="return overlib('Print History', ABOVE, LEFT)"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/printex.gif">
										<IMG id="imgei" runat="server" onmouseover="return overlib('Edit History', ABOVE, LEFT)"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/pencil.gif"> <IMG id="Img1" runat="server" onmouseover="return overlib('Add History', ABOVE, LEFT)"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/addmod.gif">
									</ItemTemplate>
									<EditItemTemplate>
										<IMG id="imga" runat="server" onmouseover="return overlib('Print History', ABOVE, LEFT)"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/printex.gif">
										<IMG id="imgee" runat="server" onmouseover="return overlib('Edit History', ABOVE, LEFT)"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/pencil.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<ItemTemplate>
										<asp:Label id=lblpmtskid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.olid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblpmtskida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.olid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
								ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
						</asp:datagrid></td>
				</tr>
			</table>
			<input id="lblcharturl" type="hidden" name="lblcharturl" runat="server"> <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lblro" type="hidden" name="lblro" runat="server"> <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input id="lblcoid" type="hidden" name="lblcoid" runat="server"> <input id="lblfcnt" type="hidden" name="lblfcnt" runat="server">
			<input id="lblchngfunc" type="hidden" name="lblchngfunc" runat="server"> <input id="lblchngcomp" type="hidden" name="lblchngcomp" runat="server">
			<input id="lblsubmit" type="hidden" name="lblsubmit" runat="server"><input id="xCoord" type="hidden" name="xCoord" runat="server">
			<input id="yCoord" type="hidden" name="yCoord" runat="server"> <input id="lblfunc" type="hidden" name="lblfunc" runat="server">
			<input id="lblcomp" type="hidden" name="lblcomp" runat="server"> <input id="lbllog" type="hidden" name="lbllog" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
