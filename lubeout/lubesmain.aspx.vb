

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class lubesmain
    Inherits System.Web.UI.Page
	Protected WithEvents lang3103 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3102 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3101 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3100 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3099 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3098 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3097 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3096 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3095 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3094 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3093 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3092 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3091 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3090 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3089 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3088 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3087 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim eqid As String
    Dim dr As SqlDataReader
    Dim sql As String
    Dim rep As New Utilities
    Dim fuid, cid, psite, filt, dt, val, eq As String
    Dim Login As String
    Dim tl, sid, did, clid, chk As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim Sort As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txteqnum As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtoem As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmodel As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtserial As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddac As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdco As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trhead As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trmain As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents diveq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iffunc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifcomp As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifmeas As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifsel As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpgemstxt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpgemshref As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmod As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblredir As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqdesc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblspl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmodel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloem As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblserial As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblac As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnumsrch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrepcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmeascnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfunctions As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomponents As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcharturl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrtab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            'lbllog.Value = "no"
            'Exit Sub
        End Try
        If Not IsPostBack Then
            Dim charturl As String = System.Configuration.ConfigurationManager.AppSettings("chartURL")
            lblcharturl.Value = charturl
            cid = "0"
            lblcid.Value = cid
            psite = HttpContext.Current.Session("dfltps").ToString()
            lblsid.Value = psite
            txtpg.Value = "1"
            trhead.Attributes.Add("class", "view")
            trmain.Attributes.Add("class", "view")
            rep.Open()
            GetEqList()
            PopAC()
            rep.Dispose()
            lblrepcnt.Value = "14"
            lblcurrtab.Value = "cm"
        Else
            If Request.Form("lblsubmit") = "getfu" Then
                lblsubmit.Value = ""
                rep.Open()
                'GetFunctions()
                rep.Dispose()
            ElseIf Request.Form("lblsubmit") = "getco" Then
                lblsubmit.Value = ""
                rep.Open()
                'GetComponents()
                rep.Dispose()
            ElseIf Request.Form("lblsubmit") = "srcheq" Then
                lblsubmit.Value = ""
                txtpg.Value = "1"
                rep.Open()
                SrchEq()
                GetEqList()
                rep.Dispose()
            End If
            If Request.Form("lblsubmit") = "next" Then
                rep.Open()
                GetNext()
                rep.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "last" Then
                rep.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetEqList()
                rep.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "prev" Then
                rep.Open()
                GetPrev()
                rep.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "first" Then
                rep.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetEqList()
                rep.Dispose()
                lblsubmit.Value = ""
            End If
        End If
    End Sub
    Private Sub SrchEq()
        Dim Filter, eq, ed, sp, oe, mo, se, ac As String
        eq = txteqnum.Text
        If Len(eq) <> 0 Then
            eq = ModString(eq)
            Filter = "eqnum like ''%" & eq & "%'' "
            FilterCnt = "eqnum like '%" & eq & "%' "
            lbleqnumsrch.Value = eq
        End If
        ed = txteqdesc.Text
        If Len(ed) <> 0 Then
            ed = ModString(ed)
            If Len(Filter) = 0 Then
                Filter = "eqdesc like ''%" & ed & "%'' "
                FilterCnt = "eqdesc like '%" & ed & "%' "
            Else
                Filter += "and eqdesc like ''%" & ed & "%'' "
                FilterCnt += "and eqdesc like '%" & ed & "%' "
            End If
            lbleqdesc.Value = ed
        End If
        sp = txtspl.Text
        If Len(sp) <> 0 Then
            sp = ModString(sp)
            If Len(Filter) = 0 Then
                Filter = "spl like ''%" & sp & "%'' "
                FilterCnt = "spl like '%" & sp & "%' "
            Else
                Filter += "and spl like ''%" & sp & "%'' "
                FilterCnt += "and spl like '%" & sp & "%' "
            End If
            lblspl.Value = sp
        End If
        oe = txtoem.Text
        If Len(oe) <> 0 Then
            oe = ModString(oe)
            If Len(Filter) = 0 Then
                Filter = "oem like ''%" & oe & "%'' "
                FilterCnt = "oem like '%" & oe & "%' "
            Else
                Filter += "and oem like ''%" & oe & "%'' "
                FilterCnt += "and oem like '%" & oe & "%' "
            End If
            lbloem.Value = oe
        End If
        mo = txtmodel.Text
        If Len(mo) <> 0 Then
            mo = ModString(mo)
            If Len(Filter) = 0 Then
                Filter = "model like ''%" & mo & "%'' "
                FilterCnt = "model like '%" & mo & "%' "
            Else
                Filter += "and model like ''%" & mo & "%'' "
                FilterCnt += "and model like '%" & mo & "%' "
            End If
            lblmodel.Value = mo
        End If
        se = txtserial.Text
        If Len(se) <> 0 Then
            se = ModString(se)
            If Len(Filter) = 0 Then
                Filter = "serial like ''%" & se & "%'' "
                FilterCnt = "serial like '%" & se & "%' "
            Else
                Filter += "and serial like ''%" & se & "%'' "
                FilterCnt += "and serial like '%" & se & "%' "
            End If
            lblserial.Value = se
        End If
        If ddac.SelectedIndex <> 0 Then
            ac = ddac.SelectedValue.ToString
            If Len(Filter) = 0 Then
                Filter = "acid like ''%" & ac & "%'' "
                FilterCnt = "acid like '%" & ac & "%' "
            Else
                Filter += "and acid like ''%" & ac & "%'' "
                FilterCnt += "and acid like '%" & ac & "%' "
            End If
            lblac.Value = ac
        End If

    End Sub
    Private Sub ClearFields()
        lbleqnumsrch.Value = ""
        txteqnum.Text = ""
        lbleqnum.Value = ""
        txteqnum.Text = ""
        lbleqdesc.Value = ""
        txteqdesc.Text = ""
        lblspl.Value = ""
        txtspl.Text = ""
        lbloem.Value = ""
        txtoem.Text = ""
        lblmodel.Value = ""
        txtmodel.Text = ""
        lblserial.Value = ""
        txtserial.Text = ""
        lblac.Value = ""
        Try
            ddac.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetEqList()
        Catch ex As Exception
            rep.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1554" , "lubesmain.aspx.vb")
 
            rep.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetEqList()
        Catch ex As Exception
            rep.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1555" , "lubesmain.aspx.vb")
 
            rep.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Function ModString(ByVal str As String)
        str = Replace(str, "'", Chr(180), , , vbTextCompare)
        str = Replace(str, "--", "-", , , vbTextCompare)
        str = Replace(str, ";", ":", , , vbTextCompare)
        Return str
    End Function
    Private Sub GetEqList()
        PageNumber = txtpg.Value
        Dim sb As New StringBuilder
        Dim eqnum, eqdesc As String
        Dim Filter, eq, ed, sp, oe, mo, se, ac As String
        eq = lbleqnumsrch.Value '
        If Len(eq) <> 0 Then
            eq = ModString(eq)
            Filter = "eqnum like ''%" & eq & "%'' "
            FilterCnt = "eqnum like '%" & eq & "%' "
            txteqnum.Text = eq
        End If
        ed = lbleqdesc.Value '
        If Len(ed) <> 0 Then
            ed = ModString(ed)
            If Len(Filter) = 0 Then
                Filter = "eqdesc like ''%" & ed & "%'' "
                FilterCnt = "eqdesc like '%" & ed & "%' "
            Else
                Filter += "and eqdesc like ''%" & ed & "%'' "
                FilterCnt += "and eqdesc like '%" & ed & "%' "
            End If
            txteqdesc.Text = ed
        End If
        sp = lblspl.Value '
        If Len(sp) <> 0 Then
            sp = ModString(sp)
            If Len(Filter) = 0 Then
                Filter = "spl like ''%" & sp & "%'' "
                FilterCnt = "spl like '%" & sp & "%' "
            Else
                Filter += "and spl like ''%" & sp & "%'' "
                FilterCnt += "and spl like '%" & sp & "%' "
            End If
            txtspl.Text = sp
        End If
        oe = lbloem.Value '
        If Len(oe) <> 0 Then
            oe = ModString(oe)
            If Len(Filter) = 0 Then
                Filter = "oem like ''%" & oe & "%'' "
                FilterCnt = "oem like '%" & oe & "%' "
            Else
                Filter += "and oem like ''%" & oe & "%'' "
                FilterCnt += "and oem like '%" & oe & "%' "
            End If
            txtoem.Text = oe
        End If
        mo = lblmodel.Value '
        If Len(mo) <> 0 Then
            mo = ModString(mo)
            If Len(Filter) = 0 Then
                Filter = "model like ''%" & mo & "%'' "
                FilterCnt = "model like '%" & mo & "%' "
            Else
                Filter += "and model like ''%" & mo & "%'' "
                FilterCnt += "and model like '%" & mo & "%' "
            End If
            txtmodel.Text = mo
        End If
        se = lblserial.Value '
        If Len(se) <> 0 Then
            se = ModString(se)
            If Len(Filter) = 0 Then
                Filter = "serial like ''%" & se & "%'' "
                FilterCnt = "serial like '%" & se & "%' "
            Else
                Filter += "and serial like ''%" & se & "%'' "
                FilterCnt += "and serial like '%" & se & "%' "
            End If
            txtserial.Text = se
        End If
        If ddac.SelectedIndex <> 0 And ddac.SelectedIndex <> -1 Then
            ac = lblac.Value '
            If Len(Filter) = 0 Then
                Filter = "acid like ''%" & ac & "%'' "
                FilterCnt = "acid like '%" & ac & "%' "
            Else
                Filter += "and acid like ''%" & ac & "%'' "
                FilterCnt += "and acid like '%" & ac & "%' "
            End If
            ddac.SelectedValue = ac
        End If
        If Len(Filter) = 0 Then
            psite = lblsid.Value
            FilterCnt = "siteid = '" & psite & "'"
            Filter = "siteid = ''" & psite & "''"
        End If
        sql = "select Count(*) from Equipment where " & FilterCnt
        Dim dc As Integer = rep.PageCount(sql, PageSize) 'rep.Scalar(sql)
        If dc = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & dc
        End If
        txtpgcnt.Value = dc
        ' sql = "select count(*) from pmTaskMeasDetMan m " _
        '+ "left join pm p on p.pmid = m.pmid where p.eqid = '" & eqid & "'"
        Tables = "equipment"
        PK = "eqid"
        PageSize = "200"
        Fields = "dept_id, cellid, eqid, eqnum, eqdesc, mcnt = ( " _
        + "select count(*) from pmtasklubesoutman m left join pmtrack t on t.pmtskid = m.pmtskid where t.eqid = equipment.eqid)"
        dr = rep.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        'sql = "select dept_id, cellid, eqid, eqnum, eqdesc from equipment where siteid = '" & psite & "'"
        sb.Append("<table>")
        'dr = rep.GetRdrData(sql)
        'If dr.Read Then
        Dim mcnt As String
        While dr.Read
            did = dr.Item("dept_id").ToString
            mcnt = dr.Item("mcnt").ToString
            clid = dr.Item("cellid").ToString
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            eqnum = Replace(eqnum, "/", "//", , , vbTextCompare)
            eqnum = Replace(eqnum, """", Chr(180) & Chr(180), , , vbTextCompare)
            eqnum = ModString(eqnum)
            eqdesc = dr.Item("eqdesc").ToString
            eqdesc = Replace(eqdesc, "/", "//", , , vbTextCompare)
            eqdesc = Replace(eqdesc, """", Chr(180) & Chr(180), , , vbTextCompare)
            eqdesc = ModString(eqdesc)
            sb.Append("<tr><td>")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getdeq('" & did & "','" & clid & "','" & eqid & "','" & eqnum & "','" & eqdesc & "','" & mcnt & "')"">")
            sb.Append(eqnum & " - " & eqdesc)
            sb.Append("</td></tr>")
        End While
        'Else
        'sb.Append("<tr><td class=""plainlabelred"">" & tmod.getlbl("cdlbl595" , "lubesmain.aspx.vb") & "</td></tr>")
        'End If
        dr.Close()
        sb.Append("</table>")
        diveq.InnerHtml = sb.ToString
    End Sub


    Private Sub PopAC()
        cid = lblcid.Value
        sql = "select * from pmAssetClass where compid = '" & cid & "' order by assetclass"
        dr = rep.GetRdrData(sql)
        ddac.DataSource = dr
        ddac.DataTextField = "assetclass"
        ddac.DataValueField = "acid"
        ddac.DataBind()
        dr.Close()
        ddac.Items.Insert(0, "Select Asset Class")
    End Sub

    Private Sub GetHead(ByVal eqid As String)
        Dim eqnum, eqdesc As String
        sql = "select eqnum, eqdesc from equipment where eqid = '" & eqid & "'"
        'rep.Open()
        dr = rep.GetRdrData(sql)
        While dr.Read
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
        End While
        dr.Close()
        'rep.Dispose()
        tdeq.InnerHtml = "Current Equipment: " & eqnum & " - " & eqdesc
        lbleqnum.Value = eqnum
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3087.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3087")
        Catch ex As Exception
        End Try
        Try
            lang3088.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3088")
        Catch ex As Exception
        End Try
        Try
            lang3089.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3089")
        Catch ex As Exception
        End Try
        Try
            lang3090.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3090")
        Catch ex As Exception
        End Try
        Try
            lang3091.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3091")
        Catch ex As Exception
        End Try
        Try
            lang3092.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3092")
        Catch ex As Exception
        End Try
        Try
            lang3093.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3093")
        Catch ex As Exception
        End Try
        Try
            lang3094.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3094")
        Catch ex As Exception
        End Try
        Try
            lang3095.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3095")
        Catch ex As Exception
        End Try
        Try
            lang3096.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3096")
        Catch ex As Exception
        End Try
        Try
            lang3097.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3097")
        Catch ex As Exception
        End Try
        Try
            lang3098.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3098")
        Catch ex As Exception
        End Try
        Try
            lang3099.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3099")
        Catch ex As Exception
        End Try
        Try
            lang3100.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3100")
        Catch ex As Exception
        End Try
        Try
            lang3101.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3101")
        Catch ex As Exception
        End Try
        Try
            lang3102.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3102")
        Catch ex As Exception
        End Try
        Try
            lang3103.Text = axlabs.GetASPXPage("lubesmain.aspx", "lang3103")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
