

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class lubegridedit
    Inherits System.Web.UI.Page
	Protected WithEvents ovid278 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang3079 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3078 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3077 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tm As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim taskid, eqid, func, comp, cid, type, login, typ, wonum, jpid, ro, rev, curr, olid As String
    Dim sid, did, clid, locid, fuid, comid, coid, uid As String
    Protected WithEvents lblolid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtmtitle As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttask As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlube As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrettype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltypeid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmeasid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblout As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrtab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllubeid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            'lbllog.Value = "no"
            'Exit Sub
        End Try
        If Not IsPostBack Then
            If lbllog.Value <> "no" Then
                'Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                lblro.Value = ro
                If ro = "1" Then
                    btnadd.Attributes.Add("src", "../images/appbuttons/bgbuttons/badddis.gif")
                    btnadd.Attributes.Add("onclick", "")
                Else
                    'btnadd.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
                    'btnadd.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
                End If
                eqid = ""
                Try
                    olid = Request.QueryString("olid").ToString
                    lblolid.Value = olid
                    If olid = "" Then
                        DisableAll()
                    End If
                   

                Catch ex As Exception
                    DisableAll()
                End Try


                cid = "0"
                lblcid.Value = cid


            End If
        Else
            If lbllog.Value <> "no" Then
                If Request.Form("lblrettype") = "add" Then
                    lblrettype.Value = ""
                    tm.Open()
                    AddLube()
                    tm.Dispose()
                End If

            End If
        End If
    End Sub
    Private Sub DisableAll()
        txtmtitle.Text = ""
        txtmtitle.Enabled = False

        txttask.Text = ""
        txttask.Enabled = False

        txtlube.Text = ""

    End Sub
    Private Sub ClearInput()
        txtmtitle.Text = ""
        txtmtitle.Enabled = True

        txttask.Text = ""
        txttask.Enabled = True

        txtlube.Text = ""
    End Sub
    Private Sub AddLube()
        Dim mtitle, mtask, mlube, mlubeid, olid As String
        olid = lblolid.Value

        mtitle = txtmtitle.Text
        If Len(mtitle) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1546" , "lubegridedit.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        mtitle = tm.ModString2(mtitle)
        mtask = txttask.Text
        If Len(mtask) > 500 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1547" , "lubegridedit.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        mtask = tm.ModString2(mtitle)
        mlube = txtlube.Text
        mlubeid = lbllubeid.Value

        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        coid = lblcoid.Value

        sql = "update outlube set lubetitle = '" & mtitle & "', lubetask = '" & mtask & "', " _
        + "lubeid = '" & mlubeid & "', lubenum = '" & mlube & "' where olid = '" & olid & "'"


        tm.Update(sql)

        lblrettype.Value = "exit"

        'ClearInput()
    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang3077.Text = axlabs.GetASPXPage("lubegridedit.aspx","lang3077")
		Catch ex As Exception
		End Try
		Try
			lang3078.Text = axlabs.GetASPXPage("lubegridedit.aspx","lang3078")
		Catch ex As Exception
		End Try
		Try
			lang3079.Text = axlabs.GetASPXPage("lubegridedit.aspx","lang3079")
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetFSOVLIBS()
		Dim axovlib as New aspxovlib
		Try
			ovid278.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("lubegridedit.aspx","ovid278") & "')")
			ovid278.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try

	End Sub

End Class
