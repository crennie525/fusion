﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMOReports.aspx.vb" Inherits="lucy_r12.PMOReports" %>
<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PMO Reports</title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="margin-top:100px;">
            <table style="width:100%;">
                <tr>
                    <td style="text-align: center;" colspan="2">
                        <b>Site</b>&nbsp;
                        <input class="easyui-combobox" id="ddSite" name="ddSite" style="width:225px;" />
                        &nbsp;&nbsp;
                        <b>Asset Class</b>&nbsp;
                        <input class="easyui-combobox" id="ddAssetClass" name="ddAssetClass" style="width:225px;" />
                        &nbsp;&nbsp;
                        <b>Equipment</b>&nbsp;
                        <input class="easyui-combobox" id="ddEquipment" name="ddEquipment" style="width:225px;" />
                        &nbsp;&nbsp;
                        <img style="vertical-align: middle;cursor: pointer;"src="../images/appbuttons/minibuttons/printx.gif" width="23" onclick="PrintPMOReport();">
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td style="width:25%;vertical-align:top;background-color:#ECECEC;border:#bdbcbc 1px solid;">
                        <div style="height: 700px; overflow:auto;">
                        <ul id="reportsTree" class="easyui-tree">
                            <li>
                                <span>PM Forms</span>
                                <ul>
                                    <li><span><a target="_blank" href="/docs/Function_ID_List.pdf">Function Id List</a></span></li>
                                    <li><span><a target="_blank" href="/docs/Component_ID_List.pdf">Component Id List</a></span></li>
                                    <li><span><a target="_blank" href="/docs/PMO_Checklist.pdf">PMO Checklist</a></span></li>
                                    <li><span><a target="_blank" href="/docs/PMO_Title_Sheet.pdf">PMO Title Sheet</a></span></li>
                                    <li><span><a target="_blank" href="/docs/valueanalysis.pdf">PM Data Collection Form</a></span></li>
                                </ul>
                            </li>
                            <li id="pmProgressReports">
                                <span>PM Progress Reports</span>
                                <ul>
                                    <li><span>Component Library Assignment Report</span></li>
                                    <li><span>Amgen Assignment Report (Excel)</span></li>
                                    <li><span>Site In Progress Report</span></li>
                                    <li><span>Site Asset Class Report</span></li>
                                </ul>
                            </li>
                            <li id="pmOptimizationReports">
                                <span>PM Optimization Reports</span>
                                <ul>
                                    <li><span>PM Optimization Overview - Site Rollup</span></li>
                                    <li><span>OP Optimization Overview - Site Rollup</span></li>
                                </ul>
                            </li>
                            <li id="pmDocReports">
                                <span>PM Documentation Reports</span>
                            </li>
                            <li id="pmMisc">
                                <span>Misc</span>
                                <ul>
                                    <li><span>CMMS Output - Asset Select</span></li>
                                    <li><span>Component Library Excel Output</span></li>
                                </ul>
                            </li>
                        </ul>
                        </div>
                    </td>
                    <td id="reportData" style="width:75%;vertical-align:top;padding-left:20px;text-align: left;" data-url="">
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField id="hSiteId" runat="server" />
        <asp:HiddenField id="hCOI" runat="server" />
    </form>
    <uc1:mmenu1 runat="server"></uc1:mmenu1>
    
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="scripts/pmoreports.js"></script>
    <link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />
    <link href="styles/pmoreports.css" rel="stylesheet" />
    
    <script type="text/javascript">
        $(document).ready(function () {
            Init();
        });
    </script>
</body>
</html>
