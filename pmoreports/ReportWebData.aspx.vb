﻿
Imports System.Web.Script.Serialization
Imports System.IO

Public Class ReportWebData
    Inherits System.Web.UI.Page

    <System.Web.Services.WebMethod()>
    Public Shared Function GetEquipmentPieChartData(eqid As Integer) As PieChartDataModel()

        Dim dbUtil As New Utilities
        dbUtil.Open()

        Dim dbReader = dbUtil.GetRdrData("exec usp_optValAnlEq5 " + eqid.ToString())
        Dim index As Integer = 0
        Dim PieChartArray(index) As PieChartDataModel

        While dbReader.Read

            Dim PieChartData As New PieChartDataModel
            PieChartData.Skill = dbReader.Item("skill")
            PieChartData.OriginalHours = dbReader.Item("ohr")
            PieChartData.OptimizedHours = dbReader.Item("rhr")

            ReDim Preserve PieChartArray(index)
            PieChartArray(index) = PieChartData
            index = index + 1
        End While

        dbReader.Close()
        dbReader = dbUtil.GetRdrData("exec usp_optValAnlEq5_op1 " + eqid.ToString())

        Dim OriginalHours As Integer = 0
        Dim OptimizedHours As Integer = 0
        While dbReader.Read
            OriginalHours += CType(dbReader.Item("ohr"), Integer)
            OptimizedHours += CType(dbReader.Item("rhr"), Integer)
        End While

        If OriginalHours > 0 Or OptimizedHours > 0 Then
            Dim PieChartData As New PieChartDataModel
            PieChartData.Skill = "OP Care"
            PieChartData.OriginalHours = OriginalHours
            PieChartData.OptimizedHours = OptimizedHours

            ReDim Preserve PieChartArray(index)
            PieChartArray(index) = PieChartData
        End If

        dbReader.Close()
        dbUtil.Dispose()

        Return PieChartArray

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetSitePieChartData(siteid As Integer) As PieChartDataModel()

        Dim dbUtil As New Utilities
        dbUtil.Open()

        Dim dbReader = dbUtil.GetRdrData("exec usp_optValAnlEq5PS " + siteid.ToString())
        Dim index As Integer = 0
        Dim PieChartArray(index) As PieChartDataModel

        While dbReader.Read

            Dim PieChartData As New PieChartDataModel
            PieChartData.Skill = dbReader.Item("skill")
            PieChartData.OriginalHours = dbReader.Item("ohr")
            PieChartData.OptimizedHours = dbReader.Item("rhr")

            ReDim Preserve PieChartArray(index)
            PieChartArray(index) = PieChartData
            index = index + 1
        End While

        dbReader.Close()
        dbReader = dbUtil.GetRdrData("exec usp_optValAnlEq5PS_op1 " + siteid.ToString())

        Dim OriginalHours As Integer = 0
        Dim OptimizedHours As Integer = 0
        While dbReader.Read
            OriginalHours += CType(dbReader.Item("ohr"), Integer)
            OptimizedHours += CType(dbReader.Item("rhr"), Integer)
        End While

        If OriginalHours > 0 Or OptimizedHours > 0 Then
            Dim PieChartData As New PieChartDataModel
            PieChartData.Skill = "OP Care"
            PieChartData.OriginalHours = OriginalHours
            PieChartData.OptimizedHours = OptimizedHours

            ReDim Preserve PieChartArray(index)
            PieChartArray(index) = PieChartData
        End If

        dbReader.Close()
        dbUtil.Dispose()

        Return PieChartArray

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetSites() As SiteDataModel()

        Dim dbUtil As New Utilities
        dbUtil.Open()

        Dim dbReader = dbUtil.GetRdrData("select siteid, sitename from sites where sitename != 'Practice Site'")
        Dim index As Integer = 0
        Dim siteArray(index) As SiteDataModel

        Dim allSites As New SiteDataModel
        allSites.SiteId = "-1"
        allSites.SiteName = "All Sites"

        ReDim Preserve siteArray(index)
        siteArray(index) = allSites
        index = index + 1

        While dbReader.Read

            Dim siteData As New SiteDataModel
            siteData.SiteId = dbReader.Item("SiteId")
            siteData.SiteName = dbReader.Item("SiteName")

            ReDim Preserve siteArray(index)
            siteArray(index) = siteData
            index = index + 1
        End While
        dbReader.Close()
        dbUtil.Dispose()

        Return siteArray

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetAssetClasses() As AssetClassDataModel()

        Dim dbUtil As New Utilities
        dbUtil.Open()

        Dim dbReader = dbUtil.GetRdrData("select assetclass, acid from pmAssetClass")
        Dim index As Integer = 0
        Dim acArray(index) As AssetClassDataModel

        Dim allAcs As New AssetClassDataModel
        allAcs.AssetClassId = "-1"
        allAcs.AssetClassName = "All Asset Classes"

        ReDim Preserve acArray(index)
        acArray(index) = allAcs
        index = index + 1

        While dbReader.Read

            Dim acData As New AssetClassDataModel
            acData.AssetClassId = dbReader.Item("acid")
            acData.AssetClassName = dbReader.Item("assetclass")

            ReDim Preserve acArray(index)
            acArray(index) = acData
            index = index + 1
        End While
        dbReader.Close()
        dbUtil.Dispose()

        Return acArray

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetEquipment(siteId As Integer, assetClassId As Integer) As EquipDataModel()

        Dim dbUtil As New Utilities
        dbUtil.Open()

        Dim dbReader = dbUtil.GetRdrData("exec usp_getPMOReportEquipment " + siteId.ToString() + "," + assetClassId.ToString())
        Dim index As Integer = 0
        Dim EquipmentArray(index) As EquipDataModel

        While dbReader.Read
            Dim pmoEquipData As New EquipDataModel
            pmoEquipData.EquipId = dbReader.Item("eqid")
            pmoEquipData.EquipName = dbReader.Item("EquipmentName")

            ReDim Preserve EquipmentArray(index)
            EquipmentArray(index) = pmoEquipData
            index = index + 1
        End While
        dbReader.Close()
        dbUtil.Dispose()

        Return EquipmentArray

    End Function

End Class