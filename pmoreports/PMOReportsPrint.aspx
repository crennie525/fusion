﻿
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMOReportsPrint.aspx.vb" Inherits="lucy_r12.PMOReportsPrint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PMO Reports</title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="reportData">
        </div>
    </form>
    
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="scripts/pmoreports.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var dataUrl = getParameterByName("dataUrl");
            dataUrl = decodeURIComponent(dataUrl);
            if (dataUrl != "") {
                $("#reportData").load(dataUrl);

                setTimeout(function() {
                    window.print();
                }, 3000);
            }
        });
    </script>
</body>
</html>
