﻿Public Class PMOReports
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (HttpContext.Current.Session("Logged_In") Is Nothing) Then
            Response.Redirect("/newlogin.aspx")
        End If

        Dim mutils As New mmenu_utils_a
        hCOI.Value = mutils.COMPI

        hSiteId.Value = HttpContext.Current.Session("dfltps").ToString()
    End Sub

End Class