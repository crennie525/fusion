﻿
function Init() {
    window.equipmentSelected = false;
    GetSites(LoadSites);
    GetAssetClasses(LoadAssetClasses);
    GetEquipment(-1, -1, LoadEquipment);
    $('#reportsTree').tree('collapseAll');
}

function PrintPMOReport() {
    var dataUrl = $("#reportData").attr("data-url");
    if (dataUrl != null) {
        window.open("../pmoreports/pmoreportsprint.aspx?dataUrl=" + encodeURIComponent(dataUrl), "repWin", "directories=0,height=500,width=1200,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1");
    } else {
        window.print();
    }
}

$('#reportsTree').tree({
    onClick: function () {
        LoadReport();
    }
});

$('#ddSite').combobox({
    onSelect: function (item) {

        var acId = $('#ddAssetClass').combobox('getValue');
        if (acId == "") {
            acId = -1;
        }

        GetEquipment(item.SiteId, acId, LoadEquipment);
    }
});

$('#ddAssetClass').combobox({
    onSelect: function (item) {

        var siteId = $('#ddSite').combobox('getValue');
        if (siteId == "") {
            siteId = -1;
        }

        GetEquipment(siteId, item.AssetClassId, LoadEquipment);
    }
});

function AddTreeNodes() {
    if (!window.equipmentSelected) {
        window.equipmentSelected = true;

        var progressReports = $("#reportsTree").tree('find', 'pmProgressReports');

        $("#reportsTree").tree('append', {
            parent: progressReports.target,
            data: [
                { text: 'Asset Missing Data Report' },
                { text: 'Components without Tasks Report' },
                { text: 'Failure Modes Not Addressed' },
                { text: 'Tasks with No Task Status' },
                { text: 'Tasks with No Task Type' },
                { text: 'Total PM Report' },
                { text: 'Total PM Report with Optimization' },
                { text: 'PM Assignment Report (Excel)' },
                { text: 'PM Assignment Report with images' },
                { text: 'OP Assignment Report' },
                { text: 'Equipment Details' },
                { text: 'Equipment Hierarchy Report' }
            ]
        });

        var pmoReports = $("#reportsTree").tree('find', 'pmOptimizationReports');

        $("#reportsTree").tree('append', {
            parent: pmoReports.target,
            data: [
                    { text: 'PM Optimization Overview' },
                    { text: 'PMO Overview - Original Tasks Pie Chart' },
                    { text: 'PMO Overview - Revised Tasks Pie Chart' },
                    { text: 'PM Optimization Overview - Asset Select' },
                    { text: 'PMO Time Overview (Asset)' },
                    { text: 'PM Optimization Raw Data (Asset)' },
                    { text: 'PM Optimization Field Report' },
                    { text: 'OP Optimization Overview' },
                    { text: 'OP Optimization Raw Data (Asset)' },
                    { text: 'OP Optimization Field Report' },
                    { text: 'PMO Total Time Dialog' }
            ]
        });

        var pmoDocReports = $("#reportsTree").tree('find', 'pmDocReports');

        $("#reportsTree").tree('append', {
            parent: pmoDocReports.target,
            data: [
                { text: 'PMO Rationale Report - All Records' },
                { text: 'PMO Rationale Report - with Rationale Only' },
                { text: 'PMO Rationale Report - with Changes Only' },
                { text: 'TPMO Rationale Report - All Records' },
                { text: 'TPMO Rationale Report - with Rationale Only' },
                { text: 'TPMO Rationale Report - with Changes Only' }
            ]
        });

        var pmoMisc = $("#reportsTree").tree('find', 'pmMisc');

        $("#reportsTree").tree('append', {
            parent: pmoMisc.target,
            data: [
                { text: 'OP Weekly Tasks' },
                { text: 'OP Non-Weekly Tasks' },
                { text: 'OP Weekly Tasks (Display)' },
                { text: 'OP Monthly/Weekly Chart' },
                { text: 'Operator Care Task Cards' },
                { text: 'CMMS Output - Asset Select' },
                { text: 'PM Report Output' },
                { text: 'Component Library Excel Output' }
            ]
        });

        $('#reportsTree').tree('collapseAll');
    }
}

$('#ddEquipment').combobox({
    onSelect: function () {
        AddTreeNodes();
        LoadReport();
    }
});

function LoadReport() {

    var node = $('#reportsTree').tree("getSelected");
    
    if (node != null) {
        var dataUrl = "";
        switch (node.text) {
            case "Asset Missing Data Report":
                dataUrl = "../reports/MissingData.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&fuid=&cid=0&tl=&sid=&did=&clid=&chk=";
                break;
            case "Components without Tasks Report":
                dataUrl = "../reports/compnotasks.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&fuid=&cid=0&tl=&sid=&did=&clid=&chk=";
                break;
            case "Failure Modes Not Addressed":
                dataUrl = "../reports/failnotasks.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&fuid=&cid=0&tl=&sid=&did=&clid=&chk=";
                break;
            case "Tasks with No Task Status":
                dataUrl = "../reports/ttno.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&fuid=&cid=0&tl=&sid=&did=&clid=&chk=&who=tt";
                break;
            case "Tasks with No Task Type":
                dataUrl = "../reports/ttno.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&fuid=&cid=0&tl=&sid=&did=&clid=&chk=&who=ts";
                break;
            case "Total PM Report":
                dataUrl = "../reports/assignmentreport2.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&typ=eq";
                break;
            case "Total PM Report with Optimization":
                dataUrl = "../reports/assignmentreport3.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&typ=eq";
                break;
            case "Component Library Assignment Report":
                dataUrl = "../reports/AssignmentReportCompLib.aspx?siteid=" + $("#hSiteId").val();
                break;
            case "PM Assignment Report (Excel)":
                window.open("../reports/AssignmentReportExcel.aspx?typ=eq&eqid=" + $('#ddEquipment').combobox('getValue'), "repWin", "directories=0,height=500,width=700,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1");
                break;
            case "Amgen Assignment Report (Excel)":
                window.open("../equip/eqmasterselect.aspx?sid=" + $("#hSiteId").val(), "repWin","directories=0,height=800,width=1000,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1");
                break;
            case "PM Assignment Report with images":
                dataUrl = "../reports/AssignmentReportPics.aspx?typ=eq&eqid=" + $('#ddEquipment').combobox('getValue');
                break;
            case "OP Assignment Report":
                dataUrl = "../reports/AssignmentReportTPM.aspx?typ=eq&eqid=" + $('#ddEquipment').combobox('getValue');
                break;
            case "Equipment Hierarchy Report":
                dataUrl = "../reports/EQReport.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&fuid=&cid=0&tl=&sid=&did=&clid=&chk=";
                break;
            case "PMO Rationale Report - All Records":
                dataUrl = "../reports/PMRationaleAll.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&rep=all";
                break;
            case "PMO Rationale Report - with Rationale Only":
                dataUrl = "../reports/PMRationaleAll.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&rep=ronly";
                break;
            case "PMO Rationale Report - with Changes Only":
                dataUrl = "../reports/PMRationaleAll.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&rep=chngonly";
                break;
            case "TPMO Rationale Report - All Records":
                dataUrl = "../reports/PMRationaleAllTPM.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&rep=all";
                break;
            case "TPMO Rationale Report - with Rationale Only":
                dataUrl = "../reports/PMRationaleAllTPM.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&rep=ronly";
                break;
            case "TPMO Rationale Report - with Changes Only":
                dataUrl = "../reports/PMRationaleAllTPM.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&rep=chngonly";
                break;
            case "PM Optimization Overview":
                dataUrl = "../reports/OVRollup.aspx?typ=eq&eqid=" + $('#ddEquipment').combobox('getValue') + "&sid=" + $("#hSiteId").val();
                break;
            case "PMO Overview - Original Tasks Pie Chart":
                dataUrl = "../reports/origpie.aspx?typ=eq&eqid=" + $('#ddEquipment').combobox('getValue') + "&sid=" + $("#hSiteId").val();
                break;
            case "PMO Overview - Revised Tasks Pie Chart":
                dataUrl = "../reports/revpie.aspx?typ=eq&eqid=" + $('#ddEquipment').combobox('getValue') + "&sid=" + $("#hSiteId").val();
                break;
            case "PM Optimization Overview - Site Rollup":
                dataUrl = "../reports/OVRollup.aspx?typ=site&sid=" + $("#hSiteId").val();
                break;
            case "PM Optimization Overview - Asset Select":
                window.open("../reports/EqSelectDialog.aspx?sid=" + $("#hSiteId").val() + "&who=PMO", "repWin", "directories=0,height=500,width=700,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1");
                break;
            case "PMO Time Overview (Asset)":
                dataUrl = "../appsdrag/pmotime.aspx?eqid=" + $('#ddEquipment').combobox('getValue');
                break;
            case "PM Optimization Raw Data (Asset)":
                dataUrl = "../reports/origrev.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&sid=" + $("#hSiteId").val() + "&eqnum=";
                break;
            case "OP Optimization Overview":
                dataUrl = "../reports/OVRollupTPM.aspx?typ=eq&eqid=" + $('#ddEquipment').combobox('getValue');
                break;
            case "OP Optimization Overview - Site Rollup":
                dataUrl = "../reports/OVRollupTPM.aspx?typ=site&sid=" + $("#hSiteId").val();
                break;
            case "OP Optimization Raw Data (Asset)":
                dataUrl = "../reports/origrevtpm.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&sid=" + $("#hSiteId").val() + "&eqnum=";
                break;
            case "OP Optimization Field Report":
                dataUrl = "../reports/FieldReportTPM.aspx?eqid=" + $('#ddEquipment').combobox('getValue') + "&sid=" + $("#hSiteId").val() + "&did=&clid=";
                break;
            case "PMO Total Time Dialog":
                window.open("../appsdrag/totpmsdialog.aspx?typ=1&eqid=" + $('#ddEquipment').combobox('getValue') + "&sid=" + $("#hSiteId").val() + "&eqnum=", "repWin","directories=0,height=500,width=700,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1");
                break;
            case "OP Weekly Tasks":
                dataUrl = "../reports/tpmweekly2.aspx?eqid=" + $('#ddEquipment').combobox('getValue');
                break;
            case "OP Non-Weekly Tasks":
                dataUrl = "../reports/tpmnonweekly.aspx?eqid=" + $('#ddEquipment').combobox('getValue');
                break;
            case "OP Weekly Tasks (Display)":
                dataUrl = "../reports/tpmweekly3.aspx?eqid=" + $('#ddEquipment').combobox('getValue');
                break;
            case "OP Monthly/Weekly Chart":
                dataUrl = "../reports/mwchart.aspx?eqid=" + $('#ddEquipment').combobox('getValue');
                break;
            case "Operator Care Task Cards":
                dataUrl = "../reports/tpmall.aspx?typ=0&eqid=" + $('#ddEquipment').combobox('getValue');
                break;
            case "CMMS Output - Asset Select":
                window.open("../reports/EqSelectDialog.aspx?sid=" + $("#hSiteId").val() + "&who=rep", "repWin", "directories=0,height=500,width=700,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1");
                break;
            case "PM Report Output":
                dataUrl = "../reports/pmrep2.aspx?eqid=" + $('#ddEquipment').combobox('getValue');
                break;
            case "Component Library Excel Output":
                window.open("../appsrt/pmrrtelkupdialog.aspx?sid=" + $("#hSiteId").val() + "&typ=COMPx", "repWin", "directories=0,height=500,width=700,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1");
                break;
            case "Equipment Details":
                window.open("../reports/eqdetails.aspx?eqid=" + $('#ddEquipment').combobox('getValue'), "repWin", "directories=0,height=500,width=700,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1");
                break;
            case "PM Optimization Field Report":
                dataUrl = "../reports/FieldReport.aspx?sid=" + $("#hSiteId").val() + "&eqid=" + $('#ddEquipment').combobox('getValue') + "&did=&clid=";
                break;
            case "Site Asset Class Report":
                dataUrl = "../reports/assetclass.aspx?sid=" + $("#hSiteId").val();
                break;
            case "Site In Progress Report":
                dataUrl = "../reports/inprog.aspx?coi=" + $("#hCOI").val();
                break;
            default:
                dataUrl = "";
                break;
        }

        if (dataUrl != "") {
            $("#reportData").attr("data-url", dataUrl);
            $("#reportData").load(dataUrl);
        }
    }
}

function LoadSites(response) {
    $('#ddSite').combobox({
        valueField: 'SiteId',
        textField: 'SiteName',
        data: response.d
    });
}

function LoadAssetClasses(response) {
    $('#ddAssetClass').combobox({
        valueField: 'AssetClassId',
        textField: 'AssetClassName',
        data: response.d
    });
}

function LoadEquipment(response) {

    if (response.d == "") {
        response.d = "[]";
    }

    $('#ddEquipment').combobox({
        valueField: 'EquipId',
        textField: 'EquipName',
        data: response.d
    });

    if (getParameterByName("eqid") != "") {
        $('#ddEquipment').combobox('setValue', getParameterByName("eqid"));
        AddTreeNodes();
    }
}

function GetSites(callback) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "../pmoreports/reportwebdata.aspx/GetSites",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            callback(response);
        }
    });
}

function GetAssetClasses(callback) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "../pmoreports/reportwebdata.aspx/GetAssetClasses",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            callback(response);
        }
    });
}

function GetEquipment(siteid, acid, callback) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "../pmoreports/reportwebdata.aspx/GetEquipment",
        data: "{'siteId':" + siteid + ",'assetClassId':" + acid + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            callback(response);
        }
    });
}

function RenderPieCharts(data) {

    var pieOriginalData = [];
    var pieOptimizedData = [];
    for (var i = 0; i < data.length; i++) {
        pieOriginalData.push(
            [data[i].Skill, data[i].OriginalHours]
        );
        pieOptimizedData.push(
            [data[i].Skill, data[i].OptimizedHours]
        );
    }

    BuildPieChart('pieChartOriginal', pieOriginalData, 'Resource Usage Original');
    BuildPieChart('pieChartOptimized', pieOptimizedData, 'Resource Usage Optimized');
}

function BuildPieChart(controlid, data, title) {

    $.jqplot(controlid, [data],
        {
            title: title,
            gridPadding: { top: 28, right: 10, bottom: 23, left: 10 },
            height: 200,
            grid: {
                drawBorder: false,
                shadow: false
            },
            seriesDefaults: {
                renderer: jQuery.jqplot.PieRenderer,
                rendererOptions: {
                    diameter: 145,
                    showDataLabels: true
                }
            },
            legend: { show: true, location: 'e', placement: 'outside' }
        }
    );
}

function GetEquipmentPieChartData() {

    var eqid = $('#ddEquipment').combobox('getValue');

    $.ajax({
       type: "POST",
        cache: false,
        url: "../pmoreports/reportwebdata.aspx/GetEquipmentPieChartData",
        data: "{'eqid':" + eqid + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            RenderPieCharts(response.d);
        }
    });
}

function GetSitePieChartData() {

    var siteId = $("#hSiteId").val();

    $.ajax({
        type: "POST",
        cache: false,
        url: "../pmoreports/reportwebdata.aspx/GetSitePieChartData",
        data: "{'siteid':" + siteId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            RenderPieCharts(response.d);
        }
    });
}

//this function gets a value from the query string
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)"; var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if (results == null)
        return "";
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
}