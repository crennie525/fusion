
function savdets() {
    var pchk = document.getElementById("lblpcntc").value;
    var nchk = document.getElementById("txtiorder").value;
    var cchk = document.getElementById("lblcurrpc").value;
    if (parseInt(nchk) > parseInt(pchk)) {
        document.getElementById("txtiorder").value = parseInt(cchk) + 1;
        alert("New Order Value Greater Than Image Count")
    }
    else if (parseInt(nchk) == 0) {
        document.getElementById("txtiorder").value = parseInt(cchk) + 1;
        alert("New Order Value Must Be Greater Than 0")
    }
    else {
        var img = document.getElementById("lblcurrimg").value;
        if (img != "") {
            var coid = document.getElementById("lblcoid").value;
            if (coid != "") {
                document.getElementById("lblcompchk").value = "savedets";
                document.getElementById("form1").submit();
            }
            else {
                alert("No Component Selected")
            }
        }
        else {
            alert("No Image Selected")
        }
    }
}
		function addpic() {
		var coid = document.getElementById("lblcoid").value;
		var ro = "0"; //document.getElementById("lblro").value;
		if(coid!="") {
		window.parent.setref();
			var eReturn = window.showModalDialog("../equip/equploadimagedialog.aspx?typ=co&eqid=&funcid=&comid=" + coid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:700px; resizable=yes");
			if (eReturn=="ok") {
				document.getElementById("lblcompchk").value = "checkpic";
				document.getElementById("form1").submit()
			}
		}
        else {
        alert("No Component Selected")
        }

		}
		function delimg() {
		var comid = document.getElementById("lblcomid").value;
		if(comid!="") {
			id = document.getElementById("lblimgid").value
			if(id!="") {
			document.getElementById("lblcompchk").value = "delimg";
			document.getElementById("form1").submit();
			}
			else {
			alert("No Image Selected")
			}
		}
        else {
        alert("No Component Selected")
        }

        }
		function getpnext() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(currp) + 1
			pcnt = parseInt(pcnt) - 1;
			if (currp<=pcnt) {
				var det = imgsarr[currp];
				var detarr = det.split(";")
				nimg = detarr[1];
				pid = detarr[0];
				document.getElementById("lblcurrimg").value= ovimgsarr[currp];
				document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
				document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
				//alert(document.getElementById("lblcurrbimg").value)
				document.getElementById("lblimgid").value= pid;
				document.getElementById("lblcurrp").value= currp;
				document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
				document.getElementById("imgeq").src=nimg;
				getdets(currp + 1, pid);
			}
			
		}
		function getplast() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(pcnt) - 1;
			pcnt = parseInt(pcnt) - 1;
			var det = imgsarr[currp];
			var detarr = det.split(";")
			nimg = detarr[1];
			pid = detarr[0];
			document.getElementById("lblcurrimg").value= ovimgsarr[currp];
			document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
			document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
			document.getElementById("lblimgid").value= pid;
			document.getElementById("lblcurrp").value= currp;
			document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
			document.getElementById("imgeq").src=nimg;	
			getdets(currp + 1, pid);
		}
		function getpprev() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(currp) - 1
			pcnt = parseInt(pcnt) - 1;
			if (currp>=0) {
				var det = imgsarr[currp];
				var detarr = det.split(";")
				nimg = detarr[1];
				pid = detarr[0];
				document.getElementById("lblcurrimg").value= ovimgsarr[currp];
				document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
				document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
				document.getElementById("lblimgid").value= pid;
				document.getElementById("lblcurrp").value= currp;
				document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
				document.getElementById("imgeq").src=nimg;
				getdets(currp + 1, pid);
			}
			
		}
		function getpfirst() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = 0; //parseInt(currp) - 1
			pcnt = parseInt(pcnt) - 1;
			var det = imgsarr[currp];
			var detarr = det.split(";")
			nimg = detarr[1];
			pid = detarr[0];
			document.getElementById("lblcurrimg").value= ovimgsarr[currp];
			document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
			document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
			document.getElementById("lblimgid").value= pid;
			document.getElementById("lblcurrp").value= currp;
			document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
			document.getElementById("imgeq").src=nimg;	
			getdets(currp + 1, pid);
		}
		function getdets(order, pic) {
        document.getElementById("lbloldorder").value = order;
        document.getElementById("lblimgid").value = pic;
        var iorder = document.getElementById("txtiorder");
        var ord = parseInt(order) - 1;
        //alert(ord)
        var iorders = document.getElementById("lbliorders").value;
        
        var iordersarr = iorders.split(",");
        var iordersstr = iordersarr[ord];
        //alert(iordersstr)
        if(iordersstr!="") {
        iorder.value = iordersstr;
        }
        else {
        iorder.value = "";
        }

        }
        function getport() {
        window.parent.setref();
		var co = document.getElementById("lblcoid").value;
		var fu = document.getElementById("lblfuid").value;
		var eq = document.getElementById("lbleqid").value;
		window.open("../equip/pmportfolio.aspx?eqid=" + eq + "&fuid=" + fu + "&comid=" + co);
		}
		function getbig() {
		
		var img = document.getElementById("lblcurrimg").value;
		if(img!="") {
		window.parent.setref();
		var currp = document.getElementById("lblcurrp").value;
		var bimgs = document.getElementById("lblbimgs").value;
		var bimgsarr = bimgs.split(",");
		var src = bimgsarr[currp];
		src = "../eqimages/" + src;
		window.open("../equip/BigPic.aspx?src=" + src)
		}
		}
		function GetCompCopy() {
			window.parent.setref();
			cid = "0"; //document.getElementById("lblcid").value;
			eqid = document.getElementById("lbleqid").value;
			fuid = document.getElementById("lblfuid").value;
			sid = document.getElementById("lblsid").value;
			ro = "0"; //document.getElementById("lblro").value;
			db = document.getElementById("lbldb").value;
			oloc = document.getElementById("lbloloc").value;
			did = document.getElementById("lbldid").value;
			clid = document.getElementById("lblclid").value;
			var eReturn = window.showModalDialog("../equip/CompCopyMiniDialog.aspx?usr=&cid=" + cid + "&sid=" + sid + "&eqid=" + eqid + "&fuid=" + fuid +  "&ro=" + ro + "&did=" + did + "&clid=" + clid + "&date=" + Date() + "&db=" + db + "&oloc=" + oloc, "", "dialogHeight:700px; dialogWidth:880px; resizable=yes");
			if (eReturn) {
				document.getElementById("lblcoid").value = eReturn;
				document.getElementById("lblcompchk").value = "1";
				document.getElementById("form1").submit()
			}
		}
		function GetCompDiv() {
			window.parent.setref();
			cid = "0"; //document.getElementById("lblcid").value;
			eqid = document.getElementById("lbleqid").value;
			fuid = document.getElementById("lblfuid").value;
			ro = "0"; //document.getElementById("lblro").value;
			did = document.getElementById("lbldid").value;
			clid = document.getElementById("lblclid").value;
			var eReturn = window.showModalDialog("../equip/CompDialog.aspx?usr=&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&did=" + did + "&clid=" + clid + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:700px; dialogWidth:800px; resizable=yes");
			if (eReturn) {
				document.getElementById("lblcompchk").value = "1";
				document.getElementById("form1").submit()
			}
		}
		function setref() {
		var upret = document.getElementById("lblupret").value;
		if(upret=="yes") {
		 document.getElementById("lblupret").value="";
		 fuid = document.getElementById("lblfuid").value;
		 window.parent.handlecompnew(fuid);
		}
		var log = document.getElementById("lbllog").value;
		if(log=="no") {
		window.parent.doref();
		}
		else {
		//window.parent.setref();
		}
		}
		function getcompedit(comid) {
		var tpm = "no"; //document.getElementById("lbltpm").value;
		try {
		var eReturn = window.showModalDialog("../complib/complibadddialog.aspx?tpm=" + tpm + "&typ=pm&comid=" + comid + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:800px; resizable=yes");
			if (eReturn) {
				if(eReturn=="log") {
				window.parent.doref();
				}
				else {
				document.getElementById("lblcompchk").value = "1";
				document.getElementById("form1").submit()
				}
			}
			else {
				document.getElementById("lblcompchk").value = "1";
				document.getElementById("form1").submit()
			}
		}
		catch(err) {
		
		}
		}
		function GetGrid() {
		fuid = document.getElementById("lblfuid").value;
		try {
		var eReturn = window.showModalDialog("../equip/compseqdialog.aspx?fuid=" + fuid + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:700px; resizable=yes");
			if (eReturn) {
				if(eReturn=="log") {
				window.parent.doref();
				}
				else {
				document.getElementById("lblcompchk").value = "1";
				document.getElementById("form1").submit()
				}
			}
			else {
				document.getElementById("lblcompchk").value = "1";
				document.getElementById("form1").submit()
			}
		}
		catch(err) {
		
		}
		}
		