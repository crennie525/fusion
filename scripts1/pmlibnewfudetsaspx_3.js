
function savdets() {
    var pchk = document.getElementById("lblpcntf").value;
    var nchk = document.getElementById("txtiorder").value;
    var cchk = document.getElementById("lblcurrpf").value;
    if (parseInt(nchk) > parseInt(pchk)) {
        document.getElementById("txtiorder").value = parseInt(cchk) + 1;
        alert("New Order Value Greater Than Image Count")
    }
    else if (parseInt(nchk) == 0) {
        document.getElementById("txtiorder").value = parseInt(cchk) + 1;
        alert("New Order Value Must Be Greater Than 0")
    }
    else {
        var img = document.getElementById("lblcurrimg").value;
        if (img != "") {
            var fuid = document.getElementById("lblfuid").value;
            if (fuid != "") {
                document.getElementById("lblpchk").value = "savedets";
                document.getElementById("form1").submit();
            }
            else {
                alert("No Function Selected")
            }
        }
        else {
            alert("No Image Selected")
        }
    }
}
		function addpic() {
		var fuid = document.getElementById("lblfuid").value;
		var ro = "0"; //document.getElementById("lblro").value;
		if(fuid!="") {
		window.parent.setref();
			var eReturn = window.showModalDialog("../equip/equploadimagedialog.aspx?typ=fu&eqid=&comid=&funcid=" + fuid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:700px; resizable=yes");
			if (eReturn=="ok") {
				document.getElementById("lblpchk").value = "checkpic";
				document.getElementById("form1").submit()
			}
		}
        else {
        alert("No Function Selected")
        }

		}
		function delimg() {
		var fuid = document.getElementById("lblfuid").value;
		if(fuid!="") {
			id = document.getElementById("lblimgid").value
			if(id!="") {
			document.getElementById("lblpchk").value = "delimg";
			document.getElementById("form1").submit();
			}
			else {
			alert("No Image Selected")
			}
		}
        else {
        alert("No Function Selected")
        }

        }
		function getpnext() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(currp) + 1
			pcnt = parseInt(pcnt) - 1;
			if (currp<=pcnt) {
				var det = imgsarr[currp];
				var detarr = det.split(";")
				nimg = detarr[1];
				pid = detarr[0];
				document.getElementById("lblcurrimg").value= ovimgsarr[currp];
				document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
				document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
				//alert(document.getElementById("lblcurrbimg").value)
				document.getElementById("lblimgid").value= pid;
				document.getElementById("lblcurrp").value= currp;
				document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
				document.getElementById("imgeq").src=nimg;
				getdets(currp + 1, pid);
			}
			
		}
		function getplast() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(pcnt) - 1;
			pcnt = parseInt(pcnt) - 1;
			var det = imgsarr[currp];
			var detarr = det.split(";")
			nimg = detarr[1];
			pid = detarr[0];
			document.getElementById("lblcurrimg").value= ovimgsarr[currp];
			document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
			document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
			document.getElementById("lblimgid").value= pid;
			document.getElementById("lblcurrp").value= currp;
			document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
			document.getElementById("imgeq").src=nimg;	
			getdets(currp + 1, pid);
		}
		function getpprev() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(currp) - 1
			pcnt = parseInt(pcnt) - 1;
			if (currp>=0) {
				var det = imgsarr[currp];
				var detarr = det.split(";")
				nimg = detarr[1];
				pid = detarr[0];
				document.getElementById("lblcurrimg").value= ovimgsarr[currp];
				document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
				document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
				document.getElementById("lblimgid").value= pid;
				document.getElementById("lblcurrp").value= currp;
				document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
				document.getElementById("imgeq").src=nimg;
				getdets(currp + 1, pid);
			}
			
		}
		function getpfirst() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = 0; //parseInt(currp) - 1
			pcnt = parseInt(pcnt) - 1;
			var det = imgsarr[currp];
			var detarr = det.split(";")
			nimg = detarr[1];
			pid = detarr[0];
			document.getElementById("lblcurrimg").value= ovimgsarr[currp];
			document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
			document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
			document.getElementById("lblimgid").value= pid;
			document.getElementById("lblcurrp").value= currp;
			document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
			document.getElementById("imgeq").src=nimg;	
			getdets(currp + 1, pid);
		}
		function getdets(order, pic) {
        document.getElementById("lbloldorder").value = order;
        document.getElementById("lblimgid").value = pic;
        var iorder = document.getElementById("txtiorder");
        var ord = parseInt(order) - 1;
        //alert(ord)
        var iorders = document.getElementById("lbliorders").value;
        
        var iordersarr = iorders.split(",");
        var iordersstr = iordersarr[ord];
        //alert(iordersstr)
        if(iordersstr!="") {
        iorder.value = iordersstr;
        }
        else {
        iorder.value = "";
        }

        }

		function getport() {
		window.parent.setref();
		var eq = document.getElementById("lbleqid").value;
		window.open("../equip/pmportfolio.aspx?eqid=" + eq + "&fuid=0&comid=0");
		}
		function getbig() {
		
		var img = document.getElementById("lblcurrimg").value;
		if(img!="") {
		window.parent.setref();
		var currp = document.getElementById("lblcurrp").value;
		var bimgs = document.getElementById("lblbimgs").value;
		var bimgsarr = bimgs.split(",");
		var src = bimgsarr[currp];
		src = "../eqimages/" + src;
		window.open("../eqimages/BigPic.aspx?src=" + src)
		}
		}
		function GetsScroll() {
		var strY = document.getElementById("spdivy").value;
        if(strY.indexOf("!~")!=0){
          var intS = strY.indexOf("!~");
          var intE = strY.indexOf("~!");
          var strPos = strY.substring(intS+2,intE);
          document.getElementById("spdiv").scrollTop = strPos;
        }
        var fuid = document.getElementById("lblfuid").value;
        var chk = document.getElementById("lblchk").value;
        if(chk=="yes") {
        document.getElementById("lblchk").value = "";
        window.parent.handlecompnew(fuid);
        }
        else if(chk=="rem") {
        window.parent.handlecompnewrem();
        }
        setref();
		}
		function SetsDivPosition(){
        var intY = document.getElementById("spdiv").scrollTop;
        document.getElementById("spdivy").value = "yPos=!~" + intY + "~!";
        }
        function GetFuncCopy() {
			window.parent.setref();
			cid = document.getElementById("lblcid").value;
			sid = document.getElementById("lblsid").value;
			did = document.getElementById("lbldid").value;
			clid = document.getElementById("lblclid").value;
			eqid = document.getElementById("lbleqid").value;
			db = document.getElementById("lbldb").value;
			oloc = document.getElementById("lbloloc").value;
			ro = "0"; //document.getElementById("lblro").value;
			var eReturn = window.showModalDialog("../equip/FuncCopyMiniDialog.aspx?cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&ro=" + ro + "&date=" + Date() + "&db=" + db + "&oloc=" + oloc, "", "dialogHeight:660px; dialogWidth:860px; resizable=yes");
			if (eReturn) {
				if (eReturn!="no") {
					document.getElementById("lblfuid").value = eReturn;
					document.getElementById("lblpchk").value = "func";
					//alert(document.getElementById("lblpchk").value)
					document.getElementById("form1").submit();
				}
			}
		}
		function GetFuncDiv() {
			window.parent.setref();
			cid = document.getElementById("lblcid").value;
			eqid = document.getElementById("lbleqid").value;
			db = document.getElementById("lbldb").value;
			oloc = document.getElementById("lbloloc").value;
			ro = "0"; //document.getElementById("lblro").value;
			var eReturn = window.showModalDialog("../equip/AddEditFuncDialog.aspx?cid=" + cid + "&eqid=" + eqid + "&date=" + Date() + "&ro=" + ro + "&db=" + db + "&oloc=" + oloc, "", "dialogHeight:600px; dialogWidth:750px; resizable=yes");
			if (eReturn) {
			document.getElementById("lblpchk").value = "func";
			document.getElementById("form1").submit();
			}
		}
		function setref() {
		var log = document.getElementById("lbllog").value;
		if(log=="no") {
		window.parent.doref();
		}
		else {
		//window.parent.setref();
		}
		}
		function getcompedit(fuid, eqid) {
		
		try {
		var eReturn = window.showModalDialog("../equip/funceditdialog.aspx?fuid=" + fuid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:200px; dialogWidth:400px; resizable=yes");
			if (eReturn) {
				if(eReturn=="log") {
				window.parent.doref();
				}
				else {
				document.getElementById("lblpchk").value = "func";
				document.getElementById("form1").submit()
				}
			}
			else {
				document.getElementById("lblpchk").value = "func";
				document.getElementById("form1").submit()
			}
		}
		catch(err) {
		
		}
		}
		function GetGrid() {
		var eqid = document.getElementById("lbleqid").value;
		try {
		var eReturn = window.showModalDialog("../equip/funcseqdialog.aspx?eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:700px; resizable=yes");
			if (eReturn) {
				if(eReturn=="log") {
				window.parent.doref();
				}
				else {
				document.getElementById("lblpchk").value = "func";
				document.getElementById("form1").submit()
				}
			}
			else {
				document.getElementById("lblpchk").value = "func";
				document.getElementById("form1").submit()
			}
		}
		catch(err) {
		
		}
		}
		