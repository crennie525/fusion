
		
		function GetPS() {
		cid = document.getElementById("lblcid").value;
		document.getElementById("psdiv").style.top="0px";
		document.getElementById("psdiv").style.left="270px";
		document.getElementById("psdiv").style.visibility="visible";
		}
		
		function handleps() {
		id = document.getElementById("ddps").value;
		if (isNaN(id)) {
			alert("No Plant Site Selected")
		}
		else {
		document.getElementById("lblsid").value=id;
		document.getElementById("lblpchk").value = "dfltps";
		closePS();
		document.getElementById("form1").submit();
		}
		}
		var docflag = 0;
		function OpenFile(newstr, type)
		{
		docflag = 1;
		handleapp();
		window.parent.handledoc("doc.aspx?file=" + newstr + "&type=" + type);
		//parent.main.location.href = "doc.aspx?file=" + newstr + "&type=" + type;
		
		}
		function CloseFile() {
		docflag = 1;
		handleapp();
		window.parent.handledoc("OptHolder.aspx");
		//parent.main.location.href = "OptHolder.aspx";
		}
		function GetEqDiv() {
		handleapp();
			sid = document.getElementById("lblsid").value;
			dept = document.getElementById("lbldept").value;
			cell = document.getElementById("lblclid").value;
			eqid = document.getElementById("lbleqid").value;
			lid = document.getElementById("lbllid").value;
			ro = document.getElementById("lblro").value;
			if(dept.length!=0||dept!=""||dept!="0") {
			var eReturn = window.showModalDialog("../equip/EqPopDialog.aspx?sid=" + sid + "&dept=" + dept + "&cell=" + cell + "&eqid=" + eqid + "&lid=" + lid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:500px; resizable=yes");
			if (eReturn) {
				if (eReturn=="log") {
					window.parent.handlelogout();
				}
				else if (eReturn!="no") {
					document.getElementById("lbleqid").value = eReturn;
					document.getElementById("lblpchk").value = "eq";
					document.getElementById("form1").submit();
				}
				//document.getElementById("lblpchk").value = "eq";
				//document.getElementById("form1").submit();
			}
			}
			else {
			alert("Error Retrieving Department Data")
			}
		}
		function GetEqCopy() {
		handleapp();
			sid = document.getElementById("lblsid").value;
			dept = document.getElementById("lbldept").value;
			cell = document.getElementById("lblclid").value;
			eqid = document.getElementById("lbleqid").value;
			lid = document.getElementById("lbllid").value;
			ro = document.getElementById("lblro").value;
			if(dept.length!=0||dept!=""||dept!="0") {
			var eReturn = window.showModalDialog("../equip/EQCopyMiniDialog.aspx?sid=" + sid + "&dept=" + dept + "&cell=" + cell + "&eqid=" + eqid + "&lid=" + lid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:660px; dialogWidth:660px; resizable=yes");
			if (eReturn) {
				if (eReturn=="log") {
					window.parent.handlelogout();
				}
				else if (eReturn!="no") {
					document.getElementById("lbleqid").value = eReturn;
					document.getElementById("lblpchk").value = "eq";
				}
				document.getElementById("lblpchk").value = "eq";
				document.getElementById("form1").submit();
			}
			}
			else {
			alert("Error Retrieving Department Data")
			}
		}
		function GetFuncDiv() {
		handleapp();
			cid = document.getElementById("lblcid").value;
			eqid = document.getElementById("lbleqid").value;
			ro = document.getElementById("lblro").value;
			if(eqid.length!=0||eqid!=""||eqid!="0") {
			var eReturn = window.showModalDialog("../equip/AddEditFuncDialog.aspx?cid=" + cid + "&eqid=" + eqid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:750px; resizable=yes");
			if (eReturn) {
				if (eReturn=="log") {
					window.parent.handlelogout();
				}
				else if (eReturn!="no") {
					document.getElementById("lblfuid").value = eReturn;
					document.getElementById("lblpchk").value = "func";
					
				}
				document.getElementById("lblpchk").value = "func";
				document.getElementById("form1").submit();
			}
			}
			else {
				alert("No Equipment Record Selected")
			}
		}
		function GetFuncCopy() {
		handleapp();
			cid = document.getElementById("lblcid").value;
			sid = document.getElementById("lblsid").value;
			did = document.getElementById("lbldid").value;
			clid = document.getElementById("lblclid").value;
			eqid = document.getElementById("lbleqid").value;
			ro = document.getElementById("lblro").value;
			if(eqid.length!=0||eqid!=""||eqid!="0") {
			var eReturn = window.showModalDialog("../equip/FuncCopyMiniDialog.aspx?cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:660px; dialogWidth:860px; resizable=yes");
			if (eReturn) {
				if (eReturn=="log") {
					window.parent.handlelogout();
				}
				else if (eReturn!="no") {
					document.getElementById("lblfuid").value = eReturn;
					document.getElementById("lblpchk").value = "func";	
				}
				document.getElementById("lblpchk").value = "func";	
				document.getElementById("form1").submit();
			}
			else {
				alert("No Equipment Record Selected")
			}
			}
			
		}
		function GetProcDiv() {
		handleapp();
			eqid = document.getElementById("lbleqid").value;
			var eqlst = document.getElementById("ddeq");
			var eqnum = eqlst.options[eqlst.selectedIndex].text
			if (eqid!="") {
			var eReturn = window.showModalDialog("../appsopt/OptDialog.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:580px; resizable=yes");
			}
			if (eReturn) {
			document.getElementById("lblpchk").value = "pr";
			document.getElementById("form1").submit();
			}
		}

		
		
		
		function setref() {
		window.parent.setref();
		//window.open("../NewLogin.aspx?app=none&lo=yes&date=" + Date(), target="_top");
		}
		function GoToPMLib() {
		handleapp();
		window.open("../equip/EQCopy.aspx?date=" + Date(), target="_top");
		}
		
		function handleapp() {
		var app = document.getElementById("appchk").value;
		if(app=="switch") window.open("../NewLogin.aspx?app=none&lo=yes&date=" + Date(), target="_top");
		}
		function hidetop() {
		document.getElementById("tdtoprt").className="Details";
		}
		function srchloc() {
		//alert("Coming Soon!")
		var eReturn = window.showModalDialog("../locs/LocDialog.aspx?typ=eq", "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
			if (eReturn) {
			var ret = eReturn.split(",");
			document.getElementById("lbllid").value = ret[5];
			document.getElementById("lbltyp").value = ret[1];
			document.getElementById("lblloc").innerHTML = ret[2];
			var lid = ret[5];
			document.getElementById("lblpchk").value = "loc"
			document.getElementById("form1").submit();
				
			}
		}
		function undoloc() {
		window.parent.handlerefresh();
		}
		function GetDrag() {
		eqid = document.getElementById("lbleqid").value;
		proc = document.getElementById("ddproc").value;
		var eqlst = document.getElementById("ddeq");
		var eqnum = eqlst.options[eqlst.selectedIndex].text
		if(eqid!="") {
		//alert(eqid)
		window.parent.handlegetdrag(eqid, eqnum, proc);
		}
		}
		function getbulk() {
		var eReturn = window.showModalDialog("bulkdialog.aspx?eq=null", "", "dialogHeight:450px; dialogWidth:450px; resizable=yes");
			if (eReturn) {
			//alert(eReturn)
			var ret = eReturn.split(",");
			var file = ret[0];
			var typ = ret[1];
			window.parent.handledoc("doc.aspx?file=" + file + "&type=" + typ);
			}
		}
		function getbulkg() {
			var eReturn = window.showModalDialog("proclibdialog.aspx", "", "dialogHeight:450px; dialogWidth:450px; resizable=yes");
			if (eReturn) {
			//alert(eReturn)
			var ret = eReturn.split(",");
			var file = ret[0];
			var typ = ret[1];
			window.parent.handledoc("doc.aspx?file=" + file + "&type=" + typ);
			}
		}
		function optit() {
		var conf = confirm("Are you sure you want to Archive the Current Equipment PM Record and Convert for Optimization?")
				if(conf==true) {
					//document.getElementById("lblans").value = "5";
					//document.getElementById("form1").submit();
					window.parent.handleoptit();
				}
				else {
					alert("Action Cancelled")
				}
		}
		function archit() {
		window.parent.handlearchit();
		}
		
		