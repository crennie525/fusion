

function savdets() {
    var pchk = document.getElementById("lblpcnt").value;
    var nchk = document.getElementById("txtiorder").value;
    var cchk = document.getElementById("lblcurrp").value;
    if (parseInt(nchk) > parseInt(pchk)) {
        document.getElementById("txtiorder").value = parseInt(cchk) + 1;
        alert("New Order Value Greater Than Image Count")
    }
    else if (parseInt(nchk) == 0) {
        document.getElementById("txtiorder").value = parseInt(cchk) + 1;
        alert("New Order Value Must Be Greater Than 0")
    }
    else {
        var img = document.getElementById("lblcurrimg").value;
        if (img != "") {
            var coid = document.getElementById("lblcoid").value;
            if (coid != "") {
                document.getElementById("lblcompchk").value = "savedets";
                document.getElementById("form1").submit();
            }
            else {
                alert("No Component Selected")
            }
        }
        else {
            alert("No Image Selected")
        }
    }
}
		function addpic() {
		    window.parent.setref();
		    var fuid = document.getElementById("lblfuid").value;
		    var eqid = document.getElementById("lbleqid").value;
		    var coid = document.getElementById("lblcoid").value;
		    var ro = document.getElementById("lblro").value;
		    if(coid!="") {
		        var eReturn = window.showModalDialog("../equip/equploadimagedialog.aspx?typ=co&eqid=" + eqid + "&funcid=" + fuid + "&comid=" + coid + "&ro=" + ro + "&date=", addPicCallBack, "dialogHeight:560px; dialogWidth:700px; resizable=yes");
			    if (eReturn) {
			        addPicCallBack();
			    }
		    }
            else {
		        alert("No Component Selected");
		    }
		}

        function addPicCallBack() {
            document.getElementById("lblcompchk").value = "checkpic";
            document.getElementById("form1").submit();
        }

		function delimg() {
		var comid = document.getElementById("lblcoid").value;
		if(comid!="") {
			id = document.getElementById("lblimgid").value
			if(id!="") {
			document.getElementById("lblcompchk").value = "delimg";
			document.getElementById("form1").submit();
			}
			else {
			alert("No Image Selected")
			}
		}
        else {
        alert("No Component Selected")
        }

        }
		function getpnext() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(currp) + 1
			pcnt = parseInt(pcnt) - 1;
			if (currp<=pcnt) {
				var det = imgsarr[currp];
				var detarr = det.split(";")
				nimg = detarr[1];
				pid = detarr[0];
				document.getElementById("lblcurrimg").value= ovimgsarr[currp];
				document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
				document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
				//alert(document.getElementById("lblcurrbimg").value)
				document.getElementById("lblimgid").value= pid;
				document.getElementById("lblcurrp").value= currp;
				document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
				document.getElementById("imgco").src=nimg;
				getdets(currp + 1, pid);
			}
			
		}
		function getplast() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(pcnt) - 1;
			pcnt = parseInt(pcnt) - 1;
			var det = imgsarr[currp];
			var detarr = det.split(";")
			nimg = detarr[1];
			pid = detarr[0];
			document.getElementById("lblcurrimg").value= ovimgsarr[currp];
			document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
			document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
			document.getElementById("lblimgid").value= pid;
			document.getElementById("lblcurrp").value= currp;
			document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
			document.getElementById("imgco").src=nimg;	
			getdets(currp + 1, pid);
		}
		function getpprev() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(currp) - 1
			pcnt = parseInt(pcnt) - 1;
			if (currp>=0) {
				var det = imgsarr[currp];
				var detarr = det.split(";")
				nimg = detarr[1];
				pid = detarr[0];
				document.getElementById("lblcurrimg").value= ovimgsarr[currp];
				document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
				document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
				document.getElementById("lblimgid").value= pid;
				document.getElementById("lblcurrp").value= currp;
				document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
				document.getElementById("imgco").src=nimg;
				getdets(currp + 1, pid);
			}
			
		}
		function getpfirst() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = 0; //parseInt(currp) - 1
			pcnt = parseInt(pcnt) - 1;
			var det = imgsarr[currp];
			var detarr = det.split(";")
			nimg = detarr[1];
			pid = detarr[0];
			document.getElementById("lblcurrimg").value= ovimgsarr[currp];
			document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
			document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
			document.getElementById("lblimgid").value= pid;
			document.getElementById("lblcurrp").value= currp;
			document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
			document.getElementById("imgco").src=nimg;	
			getdets(currp + 1, pid);
		}
		function getdets(order, pic) {
        document.getElementById("lbloldorder").value = order;
        document.getElementById("lblimgid").value = pic;
        var iorder = document.getElementById("txtiorder");
        var ord = parseInt(order) - 1;
        //alert(ord)
        var iorders = document.getElementById("lbliorders").value;
        
        var iordersarr = iorders.split(",");
        var iordersstr = iordersarr[ord];
        //alert(iordersstr)
        if(iordersstr!="") {
        iorder.value = iordersstr;
        }
        else {
        iorder.value = "";
        }

        }
		function GetType(val) {
			window.parent.handleswitch(val);
		}
		
		function FreezeScreen(msg) {
			scroll(0,0);
			var outerPane = document.getElementById('FreezePane');
			var innerPane = document.getElementById('InnerFreezePane');
			if (outerPane) outerPane.className = 'FreezePaneOn';
			if (innerPane) innerPane.innerHTML = msg;
		}
		function GoToTasks(par, val) {
		//var tcnt = document.getElementById("lbltaskcnt").value;
		//alert(tcnt)
		//if(tcnt=="0") {
		//alert("No Tasks Created For This Component, Please Jump From Functions")
		//}
		//else {
		window.parent.handletask('5');
		//}
		}
		function getss() {
		window.parent.setref();
			cid = document.getElementById("lblcid").value;
			sid = document.getElementById("lblsid").value;
			ro = document.getElementById("lblro").value;
			var eReturn = window.showModalDialog("../admin/pmSiteFMDialog.aspx?sid=" + sid + "&cid=" + cid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:520px; dialogWidth:470px; resizable=yes");
			if (eReturn) {
				//do nothing - outside operation
			}
		}
		function gridret() {
		var cid = document.getElementById("lblcid").value;
		var sid = document.getElementById("lblsid").value;
		var did = document.getElementById("lbldid").value;
		var clid = document.getElementById("lblclid").value;
		var eqid = document.getElementById("lbleqid").value;
		var fuid = document.getElementById("lblfuid").value;
		var who = document.getElementById("lblwho").value;
        window.location = "CompDivGrid.aspx?who=" + who + "&eqid=" + eqid  + "&fuid=" + fuid + "&cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid;
    
		}
		var val;
		function GoToTasks(par, val) {
		var tcnt = document.getElementById("lbltaskcnt").value;
		//alert(tcnt)
		if(tcnt=="0") {
		window.parent.handletask("func");
		}
		else {
		window.parent.handletask(val);
		}
		}
		
		function setref() {
		//document.getElementById("form1").submit();
		//window.location="../NewLogin.aspx?app=none&lo=yes"
		window.parent.setref();
		}
		function getnext() {
		var curr = document.getElementById("lblcur").innerHTML;
		var cnt = document.getElementById("lblcnt").innerHTML;
		
		var co = document.getElementById("lblparcoid").value;
		if(co=="") co = document.getElementById("lblcoid").value;
		
		//var co = document.getElementById("lblcoid").value;
		cnt = parseInt(cnt);
		curr = parseInt(curr) + 1;
		if (curr<=cnt) {
		//alert("../eqimages/a-fuImg" + fu + "i" + curr + ".jpg")
		document.getElementById("lblcur").innerHTML = curr;
		document.getElementById("imgco").src = "../eqimages/ctm-coImg" + co + "i" + curr + ".jpg"
		}
		}
		function getprev() {
		var curr = document.getElementById("lblcur").innerHTML;
		var cnt = document.getElementById("lblcnt").innerHTML;
		
		var co = document.getElementById("lblparcoid").value;
		if(co=="") co = document.getElementById("lblcoid").value;
		
		//var co = document.getElementById("lblcoid").value;
		cnt = parseInt(cnt);
		curr = parseInt(curr) - 1;
		if (curr>=1) {
		//alert("../eqimages/a-fuImg" + fu + "i" + curr + ".jpg")
		document.getElementById("lblcur").innerHTML = curr;
		document.getElementById("imgco").src = "../eqimages/ctm-coImg" + co + "i" + curr + ".jpg"
		}
		}
		function getport() {
		window.parent.setref();
		var co = document.getElementById("lblcoid").value;
		var fu = document.getElementById("lblfuid").value;
		var eq = document.getElementById("lbleqid").value;
		window.open("pmportfolio.aspx?eqid=" + eq + "&fuid=" + fu + "&comid=" + co);
		}
		function getbig() {
		window.parent.setref();
		var img = document.getElementById("lblcurrimg").value;
		if(img!="") {
		var currp = document.getElementById("lblcurrp").value;
		var bimgs = document.getElementById("lblbimgs").value;
		var bimgsarr = bimgs.split(",");
		var src = bimgsarr[currp];
		src = "../eqimages/" + src;
		window.open("BigPic.aspx?src=" + src)
		}
		}
		function GetCompCopy() {
		window.parent.setref();
			cid = document.getElementById("lblcid").value;
			eqid = document.getElementById("lbleqid").value;
			fuid = document.getElementById("lblfuid").value;
			sid = document.getElementById("lblsid").value;
			ro = document.getElementById("lblro").value;
			var eReturn = window.showModalDialog("../equip/CompCopyMiniDialog.aspx?cid=" + cid + "&sid=" + sid + "&eqid=" + eqid + "&fuid=" + fuid +  "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:880px; resizable=yes");
			if (eReturn) {
				document.getElementById("lblcoid").value = eReturn;
				document.getElementById("lblcompchk").value = "1";
				document.getElementById("form1").submit();
			}
		}
		function GoToPMLib() {
		window.open("../equip/EQCopy.aspx?date=" + Date(), target="_top");
		}
		function getACDiv() {
		window.parent.setref();
		//handleapp();
		var coid = document.getElementById("lblcoid").value;
		var ro = document.getElementById("lblro").value;
		var cid = "0";
		if(coid!=""&&coid!="0") {
		var eReturn = window.showModalDialog("../complib/compclassdialog.aspx?cid=" + cid + "&ro=" + ro, "", "dialogHeight:550px; dialogWidth:800px; resizable=yes");
		if (eReturn) {
			//document.getElementById("lblcompchk").value = "1";
			//document.getElementById("form1").submit();
		}
		}
		}
		function cget() {
		var coid = document.getElementById("lblcoid").value;
		var ro = document.getElementById("lblro").value;
		if(coid!=""&&coid!="0") {
		    var eReturn = window.showModalDialog("../complib/cgetdialog.aspx?lib=no&comid=" + coid, cgetCallback, "dialogHeight:550px; dialogWidth:780px; resizable=yes");
		if (eReturn) {
		    cgetCallback();
		}
		}
		}

		function cgetCallback() {
		    document.getElementById("lblcompchk").value = "1";
		    document.getElementById("form1").submit();
        }

        function crem() {
		var coid = document.getElementById("lblcoid").value;
		if(coid!=""&&coid!="0") {
			document.getElementById("lblcompchk").value = "crem";
			document.getElementById("form1").submit();
		}
		}
		
		