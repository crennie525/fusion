
		
		
		var docflag = 0;
		function OpenFile(newstr, type)
		{
		docflag = 1;
		handleapp();
		window.parent.handledoc("doctpm.aspx?file=" + newstr + "&type=" + type);
		//parent.main.location.href = "doctpm.aspx?file=" + newstr + "&type=" + type;
		
		}
		function CloseFile() {
		docflag = 1;
		handleapp();
		window.parent.handledoc("OptHoldertpm.aspx");
		//parent.main.location.href = "OptHoldertpm.aspx";
		}
		function GetEqDiv() {
		handleapp();
			sid = document.getElementById("lblsid").value;
			dept = document.getElementById("lbldept").value;
			cell = document.getElementById("lblclid").value;
			eqid = document.getElementById("lbleqid").value;
			lid = document.getElementById("lbllid").value;
			ro = document.getElementById("lblro").value;
			if(dept.length!=0||dept!=""||dept!="0") {
			var eReturn = window.showModalDialog("../equip/EqPopDialog.aspx?sid=" + sid + "&dept=" + dept + "&cell=" + cell + "&eqid=" + eqid + "&lid=" + lid + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:700px; dialogWidth:500px; resizable=yes");
			if (eReturn) {
				if (eReturn=="log") {
					window.parent.handlelogout();
				}
				else if (eReturn!="no") {
					document.getElementById("lbleqid").value = eReturn;
					document.getElementById("lblpchk").value = "eq";
					document.getElementById("form1").submit();
				}
				//document.getElementById("lblpchk").value = "eq";
				//document.getElementById("form1").submit();
			}
			}
			else {
			alert("Error Retrieving Department Data")
			}
		}
		function GetEqCopy() {
		handleapp();
			sid = document.getElementById("lblsid").value;
			dept = document.getElementById("lbldept").value;
			cell = document.getElementById("lblclid").value;
			eqid = document.getElementById("lbleqid").value;
			lid = document.getElementById("lbllid").value;
			ro = document.getElementById("lblro").value;
			if(dept.length!=0||dept!=""||dept!="0") {
			var eReturn = window.showModalDialog("../equip/EQCopyMiniDialogtpm.aspx?sid=" + sid + "&dept=" + dept + "&cell=" + cell + "&eqid=" + eqid + "&lid=" + lid + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:660px; dialogWidth:660px; resizable=yes");
			if (eReturn) {
				if (eReturn=="log") {
					window.parent.handlelogout();
				}
				else if (eReturn!="no") {
					document.getElementById("lbleqid").value = eReturn;
					document.getElementById("lblpchk").value = "eq";
				}
				document.getElementById("lblpchk").value = "eq";
				document.getElementById("form1").submit();
			}
			}
			else {
			alert("Error Retrieving Department Data")
			}
		}
		function GetFuncDiv() {
		handleapp();
			cid = document.getElementById("lblcid").value;
			eqid = document.getElementById("lbleqid").value;
			ro = document.getElementById("lblro").value;
			if(eqid.length!=0||eqid!=""||eqid!="0") {
			var eReturn = window.showModalDialog("../equip/AddEditFuncDialog.aspx?cid=" + cid + "&eqid=" + eqid + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:600px; dialogWidth:750px; resizable=yes");
			if (eReturn) {
				if (eReturn=="log") {
					window.parent.handlelogout();
				}
				else if (eReturn!="no") {
					document.getElementById("lblfuid").value = eReturn;
					document.getElementById("lblpchk").value = "func";
					
				}
				document.getElementById("lblpchk").value = "func";
				document.getElementById("form1").submit();
			}
			}
			else {
				alert("No Equipment Record Selected")
			}
		}
		function GetFuncCopy() {
		handleapp();
			cid = document.getElementById("lblcid").value;
			sid = document.getElementById("lblsid").value;
			did = document.getElementById("lbldept").value;
			clid = document.getElementById("lblclid").value;
			eqid = document.getElementById("lbleqid").value;
			ro = document.getElementById("lblro").value;
			if(eqid.length!=0||eqid!=""||eqid!="0") {
			var eReturn = window.showModalDialog("../equip/FuncCopyMiniDialogtpm.aspx?cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:660px; dialogWidth:860px; resizable=yes");
			if (eReturn) {
				if (eReturn=="log") {
					window.parent.handlelogout();
				}
				else if (eReturn!="no") {
					document.getElementById("lblfuid").value = eReturn;
					document.getElementById("lblpchk").value = "func";	
				}
				document.getElementById("lblpchk").value = "func";	
				document.getElementById("form1").submit();
			}
			else {
				alert("No Equipment Record Selected")
			}
			}
			
		}
		

		function checkpg() {
		var gototasks = document.getElementById("lblgototasks").value;
		if(docflag!=1) {
			//alert("docflag != 1 start")
			//closePS();
			
			chk = document.getElementById("lblpar").value;
			if (chk=="nopm") {
				document.getElementById("lblpar").value = "";
				AddNewPM();
			}
			var clean = document.getElementById("lblcleantasks").value;
			if(clean=="1") {
				document.getElementById("lblcleantasks").value = "2";
				window.parent.handletask("tpmopttasks.aspx?start=no");
			}
			
			var arch = document.getElementById("lblgetarch").value;
			if(arch=="yes") {
				document.getElementById("lblgetarch").value="no";
				eqid = document.getElementById("lbleqid").value;
				fuid = document.getElementById("lblfuid").value;
				did = document.getElementById("lbldept").value;
				clid = document.getElementById("lblclid").value;
				chk = document.getElementById("lblchk").value;
				lid = document.getElementById("lbllid").value;
				sid = document.getElementById("lblsid").value;
				window.parent.uparch(eqid, chk, did, clid, fuid, lid, sid);
			}		
				
			var tc = document.getElementById("lbltaskcnt").value;
			if (gototasks=="1") {
				document.getElementById("lblgototasks").value = "0";
				document.getElementById("lblcleantasks").value = "0";
				tl = document.getElementById("lbltl").value;
				cid = document.getElementById("lblcid").value;
				sid = document.getElementById("lblsid").value;
				did = document.getElementById("lbldept").value;
				clid = document.getElementById("lblclid").value;
				eqid = document.getElementById("lbleqid").value;
				var pmstr = document.getElementById("lbldocpmstr").value;
				var pmid = document.getElementById("lbldocpmid").value;
				chk = document.getElementById("lblchk").value;
				coid = document.getElementById("lblcoid").value;
				fuid = document.getElementById("lblfuid").value;
				tcnt = document.getElementById("lbltaskcnt").value;
				typ = document.getElementById("lbltyp").value;
				lid = document.getElementById("lbllid").value;
				if (tcnt=="1") {
					window.parent.handletasks("tpmopttaskgrid.aspx?comid=" + coid + "&start=yes&tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&typ=" + typ + "&lid=" + lid, "ok")
				}
				else if (tcnt=="0") {
					window.parent.handletasks("tpmopttasks.aspx?start=yes&tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&typ=" + typ + "&lid=" + lid, "ok")
				}
				else if (tcnt=="2") {
					window.parent.handletasks("tpmopttaskgrid.aspx?start=no", "dept")
				}
				else if (tcnt=="3") {
					window.parent.handletasks("tpmopttaskgrid.aspx?start=no", "cell")
				}
				else if (tcnt=="4") {
					window.parent.handletasks("tpmopttaskgrid.aspx?start=no", "eq")
				}
				else if (tcnt=="5") {
					window.parent.handletasks("tpmopttaskgrid.aspx?start=no", "fu")
				}
			}
			else {
				window.parent.handletasks("tpmopttaskgrid.aspx?start=no")
			}
		
			var log = document.getElementById("lbllog").value;
			if(log=="no") setref();
		}//for doc if
		else {
			//alert("docflag = 1 start")
			docflag = 0;
			if (gototasks=="1") {
				document.getElementById("lblgototasks").value = "0";
				document.getElementById("lblcleantasks").value = "0";
				tl = document.getElementById("lbltl").value;
				cid = document.getElementById("lblcid").value;
				sid = document.getElementById("lblsid").value;
				did = document.getElementById("lbldept").value;
				clid = document.getElementById("lblclid").value;
				eqid = document.getElementById("lbleqid").value;
				var pmstr = document.getElementById("lbldocpmstr").value;
				var pmid = document.getElementById("lbldocpmid").value;
				chk = document.getElementById("lblchk").value;
				coid = document.getElementById("lblcoid").value;
				fuid = document.getElementById("lblfuid").value;
				tcnt = document.getElementById("lbltaskcnt").value;
				typ = document.getElementById("lbltyp").value;
				lid = document.getElementById("lbllid").value;
				if (tcnt=="1") {
					window.parent.handletasks("tpmopttaskgrid.aspx?comid=" + coid + "&start=yes&tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&lid=" + lid + "&typ=" + typ, "ok")
				}
				else if (tcnt=="0") {
					window.parent.handletasks("tpmopttasks.aspx?start=yes&tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&lid=" + lid + "&typ=" + typ, "ok")
				}
				else if (tcnt=="2") {
					window.parent.handletasks("tpmopttaskgrid.aspx?start=no", "dept")
				}
				else if (tcnt=="3") {
					window.parent.handletasks("tpmopttaskgrid.aspx?start=no", "cell")
				}
				else if (tcnt=="4") {
					window.parent.handletasks("tpmopttaskgrid.aspx?start=no", "eq")
				}
				else if (tcnt=="5") {
					window.parent.handletasks("tpmopttaskgrid.aspx?start=no", "fu")
				}
			}
			var arch = document.getElementById("lblgetarch").value;
			if(arch=="yes") {
				document.getElementById("lblgetarch").value="no";
				eqid = document.getElementById("lbleqid").value;
				fuid = document.getElementById("lblfuid").value;
				did = document.getElementById("lbldept").value;
				clid = document.getElementById("lblclid").value;
				chk = document.getElementById("lblchk").value;
				lid = document.getElementById("lbllid").value;
				sid = document.getElementById("lblsid").value;
				window.parent.uparch(eqid, chk, did, clid, fuid, lid, sid);
			}		
		}
		}
		
		
		function setref() {
		window.parent.setref();
		//window.open("../NewLogin.aspx?app=none&lo=yes&date=" + Date(), target="_top");
		}
		function GoToPMLib() {
		handleapp();
		window.open("../equip/EQCopy.aspx?date=" + Date(), target="_top");
		}
		
		function handleapp() {
		var app = document.getElementById("appchk").value;
		if(app=="switch") window.open("../NewLogin.aspx?app=none&lo=yes&date=" + Date(), target="_top");
		}
		function hidetop() {
		document.getElementById("tdtoprt").className="Details";
		}
		function srchloc() {
		//alert("Coming Soon!")
		var eReturn = window.showModalDialog("../locs/LocDialog.aspx?typ=eq", "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
			if (eReturn) {
			var ret = eReturn.split(",");
			document.getElementById("lbllid").value = ret[5];
			document.getElementById("lbltyp").value = ret[1];
			document.getElementById("lblloc").innerHTML = ret[2];
			var lid = ret[5];
			document.getElementById("lblpchk").value = "loc"
			document.getElementById("form1").submit();
				
			}
		}
		function undoloc() {
		window.parent.handlerefresh();
		}
		function GetDrag() {
		eqid = document.getElementById("lbleqid").value;
		proc = document.getElementById("ddproc").value;
		//var eqlst = document.getElementById("ddeq");
			//var eqnum = eqlst.options[eqlst.selectedIndex].text
			var eqnum = document.getElementById("lbleq").value;
		if(eqid!="") {
		//alert(eqid)
		window.parent.handlegetdrag(eqid, eqnum, proc);
		}
		}
		function getbulk() {
		var eReturn = window.showModalDialog("bulkdialog.aspx?eq=null", "", "dialogHeight:450px; dialogWidth:450px; resizable=yes");
			if (eReturn) {
			//alert(eReturn)
			var ret = eReturn.split(",");
			var file = ret[0];
			var typ = ret[1];
			window.parent.handledoc("doctpm.aspx?file=" + file + "&type=" + typ);
			}
		}
		function getbulkg() {
			var eReturn = window.showModalDialog("proclibdialogtpm.aspx", "", "dialogHeight:450px; dialogWidth:450px; resizable=yes");
			if (eReturn) {
			//alert(eReturn)
			var ret = eReturn.split(",");
			var file = ret[0];
			var typ = ret[1];
			window.parent.handledoc("doctpm.aspx?file=" + file + "&type=" + typ);
			}
		}
		function optit() {
		var conf = confirm("Are you sure you want to Archive the Current Equipment PM Record and Convert for Optimization?")
				if(conf==true) {
					//document.getElementById("lblans").value = "5";
					//document.getElementById("form1").submit();
					window.parent.handleoptit();
				}
				else {
					alert("Action Cancelled")
				}
		}
		function archit() {
		//window.parent.handlearchit();
		document.getElementById("lblpchk").value = "archit";
		document.getElementById("form1").submit();
		}
		function getsrch() {
		var eReturn = window.showModalDialog("../equip/maxsearchdialog.aspx?srchtyp=all" + "&date=" + Date(), "", "dialogHeight:720px; dialogWidth:890px; resizable=yes");
			if (eReturn) {
				var ret = eReturn;
				//alert(ret)
				var retarr = ret.split(",");
				var eid = retarr[2];
				var sid = retarr[11];
				var did = retarr[0];
				var clid = retarr[1];
				var chk = retarr[9];
				var lid = retarr[10];
				
				var fid = retarr[4];
				var cid = retarr[12];
				if(fid!=""&&cid!="") {
				window.parent.gototpmoptco(eid, fid, cid, sid, did, clid, chk, lid);
				//gotoco(eid, fid, cid, sid, did, clid, chk, lid)
				}
				else if(fid!=""&&cid=="") {
				window.parent.gototpmoptfu(eid, fid, sid, did, clid, chk, lid);
				//gotofu(eid, fid, sid, did, clid, chk, lid)
				}
				else {
				window.parent.gototpmopteq(eid, sid, did, clid, chk, lid);
				//gotoeq(eid, sid, did, clid, chk, lid)
				}
				/*
				gotoeq(eid, sid, did, clid, chk, lid)
				gotofu(eid, fid, sid, did, clid, chk, lid)
				gotoco(eid, fid, cid, sid, did, clid, chk, lid)
				document.getElementById("lbldid").value = retarr[0];
				document.getElementById("lblclid").value = retarr[1];
				document.getElementById("lbleqid").value = retarr[2];
				document.getElementById("tdeq").innerHTML = retarr[3];
				document.getElementById("lblfuid").value = retarr[4];
				document.getElementById("tdfunc").innerHTML = retarr[5];
				document.getElementById("tddept").innerHTML = retarr[7];
				document.getElementById("tdcell").innerHTML = retarr[8];
				document.getElementById("lbleq").value = retarr[3];
				document.getElementById("lblfu").value = retarr[5];
				document.getElementById("lbldept").value = retarr[7];
				document.getElementById("lblcell").value = retarr[8];
				document.getElementById("lblcopytyp").value = "dest";
				document.getElementById("txtnewkey").value=document.getElementById("lblselkey").value;
				document.getElementById("txtnewkey").disabled=true;
				*/
			}
			else {
				
			}
		}
		
		