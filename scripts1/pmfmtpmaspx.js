
		
		function checkcnt(id, cnt) {
		val = id + "_0"
		var rb = document.getElementById(val).checked;
		if(rb==true) {
		var scnt = document.getElementById("lblfmcnt").value;
		var ccnt = document.getElementById("lblcurrcnt").value;
		//alert(scnt + ", " + ccnt)
		if(scnt!=ccnt) {
		alert("Task Record Incomplete. Please verify all Failure Modes")
		document.getElementById(val).checked = false;
		}
		}
		}
		
		function checkcomp() {
		var chk = document.getElementById("lblsubmit").value;
		if(chk=="wocomp") {
		window.parent.handlecomp();
		}
		}
		function getcal(fld) {
		    var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
				if (eReturn) {
				fld = "txt" + fld;	
				document.getElementById(fld).value = eReturn;
				document.getElementById("lblsubmit").value = "savedates";
				document.getElementById("form1").submit();
				}
			}
		//
		function checkfm(cond, id) {
			var newfmstr;
			var newfm;
			var fmstr = document.getElementById("lblfmstr").value;
			var fmarr = fmstr.split(",");
			for(var i=0; i<=fmarr.length-1; i++){
				var pf = fmarr[i];
				var pfarr = pf.split("~");
				if(pfarr[0]==id) {
					newfm = pfarr[0] + "~" + cond;
				}
				else {
					newfm = pf;
				}
				if(newfmstr=="") {
					newfmstr = newfm;
				}
				else {
					newfmstr += "," + newfm;
				}
			}
			document.getElementById("lblfmstr").value=newfmstr;
		}
		function checkwp(ans, id) {
			var newpstr;
			var newp;
			var pstr = document.getElementById("lblpstr").value;
			var parr = pstr.split(",");
			for(var i=0; i<=parr.length-1; i++){
				var p = parr[i];
				var paarr = p.split("~");
				if(paarr[0]==id) {
					newp = paarr[0] + "~" + ans;
				}
				else {
					newp = p;
				}
				if(newpstr=="") {
					newpstr = newp;
				}
				else {
					newpstr += "," + newp;
				}
			}
			document.getElementById("lblpstr").value=newpstr;
		}
		function checkwt(ans, id) {
			var newtstr;
			var newt;
			var tstr = document.getElementById("lbltstr").value;
			var tarr = tstr.split(",");
			for(var i=0; i<=tarr.length-1; i++){
				var t = tarr[i];
				var taarr = t.split("~");
				if(taarr[0]==id) {
					newt = paarr[0] + "~" + ans;
				}
				else {
					newt = t;
				}
				if(newtstr=="") {
					newtstr = newt;
				}
				else {
					newtstr += "," + newt;
				}
			}
			document.getElementById("lbltstr").value=newtstr;
		}
		function checkwl(ans, id) {
			var newlstr;
			var newl;
			var lstr = document.getElementById("lbllstr").value;
			var larr = lstr.split(",");
			for(var i=0; i<=larr.length-1; i++){
				var l = larr[i];
				var laarr = l.split("~");
				if(laarr[0]==id) {
					newl = paarr[0] + "~" + ans;
				}
				else {
					newl = l;
				}
				if(newlstr=="") {
					newlstr = newl;
				}
				else {
					newlstr += "," + newl;
				}
			}
			document.getElementById("lbllstr").value=newlstr;
		}
		function measalert() {
			var acts = document.getElementById("txts").value;
			var actf = document.getElementById("txtf").value;
			var acth = document.getElementById("txtacthours").value;
			if(acts="") {
				alert("Actual Start Date Required") 
			}
			else if(actf=="") {
				alert("Actual Complete Date Required") 
			}
			else if(acth=="") {
				alert("Actual Labor Hours Required") 
			}
			else if(isNaN(acth)) {
				alert("Actual Labor Hours Is Not A Number") 
			}
			else {
			var mflg = document.getElementById("lblmflg").value;
			var mcomp = document.getElementById("lblmcomp").value;
			if(mflg=="yes"&&mcomp=="incomp") {
			var conf = confirm("This Job Plan has Measurements that haven't been recorded.\nAre you sure you want to Continue?")
				if(conf==true) {
					compalert();
				}
				else {
					alert("Action Cancelled")
				}
			}
			else {
				var currm;
				var newm;
				var newmstr;
				var mid;
				var mstr = document.getElementById("lblmstr").value;
				var mstrarr = mstr.split(",");
				for(var i=0; i<=mstrarr.length-1; i++){
					currm = mstr[i];
					var midarr = currm.split("-")
					mid = midarr[1];
					newm = document.getElementById("currm").value;
					if(newm=="") newm="nr";
					if(newmstr!="") {
						newmstr = mid + "~" + newm;
					}
					else {
						newmstr += "," + mid + "~" + newm;
					}
				}
			document.getElementById("lblsubmit").value = "comp";
			document.getElementById("form1").submit();
			}
			}
		}
		function getmeas(tid) {
		var eReturn = window.showModalDialog("wojpmeasdialog.aspx?tid=" + tid, "", "dialogHeight:430px; dialogWidth:570px; resizable=yes");
				if (eReturn) {
				document.getElementById("lblmcomp").value=eReturn;
				}	
		}
		function getpart(typ, task) {
			var wo = document.getElementById("lblwo").value;
			var eReturn = window.showModalDialog("wojppartdialog.aspx?pmtskidid=" + task + "&wo=" + wo + "&typ=" + typ, "", "dialogHeight:440px; dialogWidth:570px; resizable=yes");
				if (eReturn) {

				}	
		}
		function gettsk(tsk) {
		
		var ycoord = document.getElementById("yCoord").value;
		if (ycoord!="") {
		var tp = parseInt(ycoord) + 10;
		tp = tp + "px"
		document.getElementById("tskdiv").style.top=tp;
		}
		else {
		document.getElementById("tskdiv").style.top="10px";
		}
		document.getElementById("tskdiv").style.left="10px";
		document.getElementById("tskdiv").className="view";
		document.getElementById("txttsk").value=tsk;
		}
		function closetsk() {
		document.getElementById("tskdiv").className="details";
		}
		function checkit() {
		var log = document.getElementById("lbllog").value;
		if(log=="no") {
		window.parent.doref();
		}
		else {
		window.parent.setref();
		}
		}
		function setref() {
		window.parent.setref();
		}
		