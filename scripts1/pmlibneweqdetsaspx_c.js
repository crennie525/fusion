
		
		function handleret() {
		window.parent.handleret();
		}
		function savdets() {
		    var pchk = document.getElementById("lblpcnt").value;
		    var nchk = document.getElementById("txtiorder").value;
		    var cchk = document.getElementById("lblcurrp").value;
		    if (parseInt(nchk) > parseInt(pchk)) {
		        document.getElementById("txtiorder").value = parseInt(cchk) + 1;
		        alert("New Order Value Greater Than Image Count")
		    }
		    else if (parseInt(nchk) == 0) {
		        document.getElementById("txtiorder").value = parseInt(cchk) + 1;
		        alert("New Order Value Must Be Greater Than 0")
		    }
		    else {
		        var img = document.getElementById("lblcurrimg").value;
		        if (img != "") {
		            var eqid = document.getElementById("lbleqid").value;
		            if (eqid != "") {
		                document.getElementById("lblpchk").value = "savedets";
		                document.getElementById("form1").submit();
		            }
		            else {
		                alert("No Equipment Selected")
		            }
		        }
		        else {
		            alert("No Image Selected")
		        }
		    }
		}
		function addpic() {
		var eqid = document.getElementById("lbleqid").value;
		var ro = "0"; //document.getElementById("lblro").value;
		if(eqid!="") {
		window.parent.setref();
			var eReturn = window.showModalDialog("../equip/equploadimagedialog.aspx?typ=eq&funcid=&comid=&eqid=" + eqid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:700px; resizable=yes");
			if (eReturn=="ok") {
				document.getElementById("lblpchk").value = "checkpic";
				document.getElementById("form1").submit()
			}
		}
        else {
        alert("No Equipment Selected")
        }

		}
		function delimg() {
		var eqid = document.getElementById("lbleqid").value;
		if(eqid!="") {
			id = document.getElementById("lblimgid").value
			if(id!="") {
			document.getElementById("lblpchk").value = "delimg";
			document.getElementById("form1").submit();
			}
			else {
			alert("No Image Selected")
			}
		}
        else {
        alert("No Equipment Selected")
        }

        }
		function getpnext() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(currp) + 1
			pcnt = parseInt(pcnt) - 1;
			if (currp<=pcnt) {
				var det = imgsarr[currp];
				var detarr = det.split(";")
				nimg = detarr[1];
				pid = detarr[0];
				document.getElementById("lblcurrimg").value= ovimgsarr[currp];
				document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
				document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
				//alert(document.getElementById("lblcurrbimg").value)
				document.getElementById("lblimgid").value= pid;
				document.getElementById("lblcurrp").value= currp;
				document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
				document.getElementById("imgeq").src=nimg;
				getdets(currp + 1, pid);
			}
			
		}
		function getplast() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(pcnt) - 1;
			pcnt = parseInt(pcnt) - 1;
			var det = imgsarr[currp];
			var detarr = det.split(";")
			nimg = detarr[1];
			pid = detarr[0];
			document.getElementById("lblcurrimg").value= ovimgsarr[currp];
			document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
			document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
			document.getElementById("lblimgid").value= pid;
			document.getElementById("lblcurrp").value= currp;
			document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
			document.getElementById("imgeq").src=nimg;	
			getdets(currp + 1, pid);
		}
		function getpprev() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(currp) - 1
			pcnt = parseInt(pcnt) - 1;
			if (currp>=0) {
				var det = imgsarr[currp];
				var detarr = det.split(";")
				nimg = detarr[1];
				pid = detarr[0];
				document.getElementById("lblcurrimg").value= ovimgsarr[currp];
				document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
				document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
				document.getElementById("lblimgid").value= pid;
				document.getElementById("lblcurrp").value= currp;
				document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
				document.getElementById("imgeq").src=nimg;
				getdets(currp + 1, pid);
			}
			
		}
		function getpfirst() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = 0; //parseInt(currp) - 1
			pcnt = parseInt(pcnt) - 1;
			var det = imgsarr[currp];
			var detarr = det.split(";")
			nimg = detarr[1];
			pid = detarr[0];
			document.getElementById("lblcurrimg").value= ovimgsarr[currp];
			document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
			document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
			document.getElementById("lblimgid").value= pid;
			document.getElementById("lblcurrp").value= currp;
			document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
			document.getElementById("imgeq").src=nimg;	
			getdets(currp + 1, pid);
		}
		function getdets(order, pic) {
        document.getElementById("lbloldorder").value = order;
        document.getElementById("lblimgid").value = pic;
        var iorder = document.getElementById("txtiorder");
        var ord = parseInt(order) - 1;
        //alert(ord)
        var iorders = document.getElementById("lbliorders").value;
        
        var iordersarr = iorders.split(",");
        var iordersstr = iordersarr[ord];
        //alert(iordersstr)
        if(iordersstr!="") {
        iorder.value = iordersstr;
        }
        else {
        iorder.value = "";
        }

        }
		var popwin = "directories=0,height=500,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0,scrollbars=1";
		function GetPartDiv() {
			var lock = document.getElementById("lbllock").value;
			if(lock!="1") {
			cid = document.getElementById("lblcid").value;
			eqid = document.getElementById("lbleqid").value;
			eqnum = document.getElementById("txteqname").value;
			ro = document.getElementById("lblro").value;
			if (eqid!="") {
			window.parent.setref();
			var eReturn = window.showModalDialog("../inv/sparepartlistdialog.aspx?eqid=" + eqid + "&cid=" + cid + "&eqn=" + eqnum + "&ro=" + ro, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes")
			//window.open("../inv/sparepartlist.aspx?eqid=" + eqid + "&cid=" + cid + "&eqn=" + eqnum + "&ro=" + ro, "repWin", popwin);
			//document.getElementById("form1").submit();
			}
		}
		}
		function gettrans() {
	var cb = document.getElementById("cbtrans");
	if(cb.checked==true) {
		document.getElementById("gldiv").className="view";
		document.getElementById("gldiv").style.position="absolute";
		document.getElementById("gldiv").style.top="5px";
		document.getElementById("gldiv").style.left="320px";
		var eq = document.getElementById("lbleqid").value;
		//alert(eq)
		document.getElementById("ifgl").src="translist.aspx?eq=" + eq;
	}
	}
function transret() {
closegl();
}
	function closegl() {
		document.getElementById("gldiv").className="details";
	}
		function lock() {
			document.getElementById("lblpchk").value = "lock";
			document.getElementById("form1").submit();
		}
		function unlock() {
			document.getElementById("lblpchk").value = "unlock";
			document.getElementById("form1").submit();
		}
		function FreezeScreen(msg) {
			//var lock = document.getElementById("lbllock").value;
			//if (lock=="0") {
			if (isNaN(document.getElementById("txtecr").value)) {
			alert("ECR is Not a Number")
		}
		else {
			scroll(0,0);
			var outerPane = document.getElementById('FreezePane');
			var innerPane = document.getElementById('InnerFreezePane');
			if (outerPane) outerPane.className = 'FreezePaneOn';
			if (innerPane) innerPane.innerHTML = msg;
			document.getElementById("lblpchk").value = "ac";
			document.getElementById("form1").submit();
		}
			//}
			//else {
			//var by = document.getElementById("lbllockedby").value;
			//alert("This Record Locked for Editing by " + by)
			//}
		}
		function GetType(val) {
			window.parent.handleswitch(val);
		}
		function gridret() {
		handleapp();
				var chk = document.getElementById("lblchk").value;
				var dchk = document.getElementById("lbldchk").value;
				var sid = document.getElementById("lblsid").value;
				var did = document.getElementById("lbldept").value;
				var clid = document.getElementById("lblclid").value;
				var cid = document.getElementById("lblcid").value;
				var lid = document.getElementById("lbllid").value;
				var typ = document.getElementById("lbltyp").value;
				window.parent.handleeq('eq', 'chk', '');
				window.parent.handleeq('func', 'chk', '');
				if(dchk=="yes"&&chk=="yes") {
				//alert(dchk + ", " + chk)
					window.location = "EQBotGrid.aspx?who=&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&clid=" + clid + "&sid=" + sid + "&cid=" + cid + "&lid=" + lid + "&typ=" + typ;
				}
				else if(dchk=="yes"&&chk=="no") {
					window.location = "EQBotGrid.aspx?who=&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&sid=" + sid + "&cid=" + cid + "&lid=" + lid + "&typ=" + typ;
				}
				else if(dchk=="no") {
					window.location = "EQBotGrid.aspx?who=&start=yes&dchk=" + dchk + "&chk=" + chk + "&sid=" + sid + "&cid=" + cid + "&lid=" + lid + "&typ=" + typ;
				}
    
		}
		var val;
		function GoToTasks(val) {
		var appstr = document.getElementById("lblappstr").value;
		if(appstr!="no") {
		window.parent.handletask(val);
		}
		}
		function checkit() {
		var lockchng = document.getElementById("lbllockchk").value;
		if (lockchng=="yes") {
		document.getElementById("lbllockchk").value="no";
		window.parent.handlearch();
		}
		var add = document.getElementById("lbladdchk").value;
		if(add=="yes") {
		document.getElementById("lbladdchk").value="";
		var eqid = document.getElementById("lbleqid").value;
		window.parent.handleeq('eq', 'chk', eqid);
		}
		var app = document.getElementById("appchk").value;
		if(app=="switch") window.parent.handleapp(app);

		var par = document.getElementById("lblpar").value;
		if (par=="eq") {
		var val = document.getElementById("lbleqid").value;
		var chk = "na";
		window.parent.handleeq(par, chk, val);
		}
		var val = document.getElementById("lblapp").value;
		window.parent.handleswitch(val);
		var del = document.getElementById("lbldel").value;
		if (del=="1") {
		gridret();
		}
		var chk = document.getElementById("lbllog").value;
		if(chk=="no") {
		window.parent.doref();
		}
		else {
		//window.parent.setref();
		}
		}
		function setref() {
		//window.location="../NewLogin.aspx?app=none&lo=yes"
		//window.parent.setref();
		}
		function getnext() {
		var curr = document.getElementById("lblcur").innerHTML;
		var cnt = document.getElementById("lblcnt").innerHTML;
		
		var eq = document.getElementById("lblpareqid").value;
		if(eq=="") eq = document.getElementById("lbleqid").value;
		//var eq = document.getElementById("lbleqid").value;
		cnt = parseInt(cnt);
		curr = parseInt(curr) + 1;
		if (curr<=cnt) {
		
		document.getElementById("lblcur").innerHTML = curr;
		document.getElementById("imgeq").src = "../eqimages/atm-eqImg" + eq + "i" + curr + ".jpg"
		}
		}
		function getprev() {
		var curr = document.getElementById("lblcur").innerHTML;
		var cnt = document.getElementById("lblcnt").innerHTML;
		
		var eq = document.getElementById("lblpareqid").value;
		if(eq=="") eq = document.getElementById("lbleqid").value;
		
		//var eq = document.getElementById("lbleqid").value;
		
		cnt = parseInt(cnt);
		curr = parseInt(curr) - 1;
		if (curr>=1) {
		//alert("../eqimages/a-eqImg" + eq + "i" + curr + ".jpg")
		document.getElementById("lblcur").innerHTML = curr;
		document.getElementById("imgeq").src = "../eqimages/atm-eqImg" + eq + "i" + curr + ".jpg"
		}
		}
		function getport() {
		window.parent.setref();
		var eq = document.getElementById("lbleqid").value;
		window.open("../equip/pmportfolio.aspx?eqid=" + eq + "&fuid=0&comid=0");
		}
		function getbig() {
		var img = document.getElementById("lblcurrimg").value;
		if(img!="") {
		window.parent.setref();
		var currp = document.getElementById("lblcurrp").value;
		var bimgs = document.getElementById("lblbimgs").value;
		var bimgsarr = bimgs.split(",");
		var src = bimgsarr[currp];
		src = "../eqimages/" + src;
		window.open("../equip/BigPic.aspx?src=" + src)
		}
		}
		function GoToPMLib() {
		window.open("../equip/EQCopy.aspx?date=" + Date(), target="_top");
		}
		function GetEqCopy() {
		window.parent.setref();
		handleapp();
			sid = document.getElementById("lblsid").value;
			dept = document.getElementById("lbldept").value;
			cell = document.getElementById("lblclid").value;
			eqid = document.getElementById("lbleqid").value;
			lid = document.getElementById("lbllid").value;
			ro = document.getElementById("lblro").value;
			//alert("../equip/EQCopyMiniDialog.aspx?sid=" + sid + "&dept=" + dept + "&cell=" + cell + "&eqid=" + eqid)
			var eReturn = window.showModalDialog("../equip/EQCopyMiniDialog.aspx?sid=" + sid + "&dept=" + dept + "&cell=" + cell + "&eqid=" + eqid + "&lid=" + lid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:660px; dialogWidth:660px; resizable=yes");
			if (eReturn) {
				if (eReturn!="no") {
					document.getElementById("lbleqid").value = eReturn;
					document.getElementById("lblpchk").value = "eq";
					document.getElementById("form1").submit();
				}
			}
		}
		function handleapp() {
		var app = document.getElementById("appchk").value;
		if(app=="switch") window.parent.handleapp(app);
		}
		function getACDiv() {
		window.parent.setref();
		//handleapp();
		var cid = document.getElementById("lblcid").value;
		var ro = document.getElementById("lblro").value;
		var eReturn = window.showModalDialog("../equip/AssetClassDialog.aspx?cid=" + cid + "&ro=" + ro, "", "dialogHeight:450px; dialogWidth:380px; resizable=yes");
		if (eReturn) {
			document.getElementById("lblpchk").value = "ac";
			document.getElementById("form1").submit()
		}
		}
		function getcharge(typ) {
		window.parent.setref();
			var eReturn = window.showModalDialog("../admin/ChargeDialog.aspx", "", "dialogHeight:350px; dialogWidth:350px; resizable=yes");
			if (eReturn) {	
				//document.getElementById("txtcharge").value = eReturn;
			}
		}
		function setref() {
		var log = document.getElementById("lbllog").value;
		if(log=="no") {
		window.parent.doref();
		}
		else {
		//window.parent.setref();
		}
		}
		
		