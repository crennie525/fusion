
		var save_array;
		var orig_array;
		var exp_array;
		var savestr;
		var origstr;
		var expstr;
		function load_save() {
		var log = document.getElementById("lbllog").value;
		if(log=="no") {
		window.parent.handlelogout();
		}
		else {
		savestr = document.getElementById("lblsave").value;
		document.getElementById("lblorig").value = savestr;
		save_array = savestr.split("/");
		orig_array = savestr.split("/");
		expstr = document.getElementById("lblexpand").value;
		document.getElementById("lblexpand").value = "";
		if(expstr.length!=0) {
		expand();
		}
		}
		}
		function expand() {
		//alert(expstr)
		exp_array = expstr.split("/");
		for (var i = 0; i < exp_array.length; i++) {
			var str = exp_array[i]
			var str_array = str.split("-");
			var td = str_array[0];
			var img = str_array[1];
			try {
				fclose(td, img);
			}
			catch(err) {
			}
		}
		}
		function checkbox(cb1, cb2, id, typ, aid, stat) {
		//alert(cb1 + "," + cb2 + "," + id + "," + typ + "," + aid + "," + stat)
		var typchk;
		var sav;
		var chng;
		var sav_na;
		var sav_pma;
		var sav_pmac;
		var sav_pmt;
		if(typ=="na") typchk = "pma";
		else if(typ=="nac") typchk="pmac";
		else if(typ=="nat") typchk="pmt";
		else typchk=typ;
		var fld = "td" + typchk + id;
		//alert(fld + ", " + cb2)
		//alert(typ)
		if (document.getElementById(cb1).checked==true) {
			if(typ=="na") {
			var conf = confirm("This will disable this Approval Phase and reset\nall High Level and Sub Tasks for this Phase\nto their default values\n\nDo you wish to continue?")
				if(conf==true) {
					document.getElementById("lblappreqid").value = typ + "/" + aid + "/";
					document.getElementById("lblappreqid").value += id;
					document.getElementById("lblchng").value = "ADIS";
					document.getElementById("form1").submit();
				}
				else {
					document.getElementById(cb1).checked=false;
					alert("Action Cancelled")
				}
			}
			else if(typ=="nac") {
			var conf = confirm("This will disable this High Level Task and reset\nall Sub Tasks for this High Level Task\nto their default values\n\nDo you wish to continue?")
				if(conf==true) {
					document.getElementById("lblappreqid").value = typ + "/" + aid + "/";
					document.getElementById("lblappreqid").value += id;
					document.getElementById("lblchng").value = "ADIS";
					document.getElementById("form1").submit();
				}
				else {
					document.getElementById(cb1).checked=false;
					alert("Action Cancelled")
				}	
			}
			else if(typ=="nat") {
				document.getElementById("lblappreqid").value = typ + "/" + aid + "/";
				document.getElementById("lblappreqid").value += id;
				document.getElementById("lblchng").value = "ADIS";
				document.getElementById("form1").submit();
				//chng
				//sav = aid + "-chng-0";
				//chng = aid + "-chng-1";
				//save_array[sav] = chng;
				//pmt
				//sav = aid + "-nat-1";
				//sav_nat = aid + "-nat-0";
				//save_array[sav] = sav_nat;
				//document.getElementById(fld).innerHTML="N/A"
			}
			else {
				if(stat=="dis") {
					document.getElementById("lblappreqid").value = typ + "/" + aid + "/";
					document.getElementById("lblappreqid").value += id;
					document.getElementById("lblchng").value = "AEN";
					document.getElementById("form1").submit();
				}
				else {
					var savstr = save_array[stat];
					//alert(savstr)
					var sav_array = savstr.split("-");
					sav_array[1] = "1";
					
					if(typ=="pma") {
					//should be disabled if top-level
					sav_array[2] = "1";
					}
					else if(typ=="pmac") {
					//should be disabled until sub tasks are completed
					sav_array[3] = "1";
					}
					else if(typ=="pmt") {
					sav_array[4] = "1";
					}
				savstr = sav_array[0] + "-" + sav_array[1] + "-" + sav_array[2] + "-" + sav_array[3] + "-" + sav_array[4];
				save_array[stat] = savstr;
				//added temp?
				document.getElementById("lblorig").value = "";
				//alert(document.getElementById("lblorig").value)
				document.getElementById("lblorig").value = savstr;
				//alert(document.getElementById("lblorig").value)
				document.getElementById("lblchng").value = "ASAV";
				//alert(savstr)
				document.getElementById("form1").submit();
				}	
			document.getElementById(fld).innerHTML="COMPLETE"
			}
			document.getElementById(cb2).checked=false
		}
		else {
			var savstr = save_array[stat];
			//alert(savstr)
			var sav_array = savstr.split("-");
			sav_array[1] = "1";
					
			if(typ=="pma") {
			//should be disabled if top-level
			sav_array[2] = "0";
			}
			else if(typ=="pmac") {
			//should be disabled until sub tasks are completed
			sav_array[3] = "0";
			}
			else if(typ=="pmt") {
			sav_array[4] = "0";
			}
			savstr = sav_array[0] + "-" + sav_array[1] + "-" + sav_array[2] + "-" + sav_array[3] + "-" + sav_array[4];
			save_array[stat] = savstr;
			//added temp?
			document.getElementById("lblorig").value = "";
			//alert(document.getElementById("lblorig").value)
			document.getElementById("lblorig").value = savstr;
			//alert(document.getElementById("lblorig").value)
			document.getElementById("lblchng").value = "ASAV";
			//alert(savstr)
			document.getElementById("form1").submit();
			document.getElementById(fld).innerHTML="INCOMPLETE"
		}
		}
		
		function jumpb(id, gid, grp, ht) {
		//var grp = document.getElementById("tdphase").innerHTML;
		window.parent.handlejp(id, gid, grp, ht);
		}
		function jump(id, grp) {
		
		window.parent.handlepjump(id, grp);
		}
		
		function fclose(td, img) {
		//alert(td + ", " + img)
		document.getElementById("lblexpand").value += td + "-" + img + "/";
		var str = document.getElementById(img).src
		var lst = str.lastIndexOf("/") + 1
		var loc = str.substr(lst)
		if(loc=='minus.gif') {
			document.getElementById(img).src='../images/appbuttons/bgbuttons/plus.gif';
			try {
			document.getElementById(td).className="details";
			}
			catch(err) {
			
			}
		}
		else {
			document.getElementById(img).src='../images/appbuttons/bgbuttons/minus.gif';
			try {
			document.getElementById(td).className="view";
			}
			catch(err) {
			
			}
		}
		}
		function ganttit() {
		window.parent.handlegantt();
		}
		function saveit() {
		document.getElementById("lblsave").value = ""
		var savstr = "";
		for (var i = 0; i < save_array.length; i++) {
		savstr += save_array[i] + "/";  
		}
		document.getElementById("lblsave").value = savstr;
		document.getElementById("lblchng").value = "ASAV";
		document.getElementById("form1").submit();
		}
		
		
		