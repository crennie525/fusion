

		
		
		function getAddType() {
			var eReturn = window.showModalDialog("mtdialog.aspx", "", "dialogHeight:310px; dialogWidth:360px; resizable=yes");
			if (eReturn) {
				document.getElementById("lblrettype").value = "type";
				document.getElementById("form1").submit();
			}
		}
		function getAddMeas() {
			var type = document.getElementById("lbltypeid").value;
			if (type!="0") {
				var eReturn = window.showModalDialog("mdialog.aspx?type=" + type, "", "dialogHeight:320px; dialogWidth:440px; resizable=yes");
				if (eReturn) {
					document.getElementById("lblrettype").value = "meas";
					document.getElementById("form1").submit();
				}
			}
			else {
				alert("No Measure Type Selected")
			}
		}
		function addToTask() {
			var type = document.getElementById("lbltypeid").value;
			var meas = document.getElementById("lblmeasid").value;
			var t1 = document.getElementById("t1").value;
			var t2 = document.getElementById("t2").value;
			var t3 = document.getElementById("t3").value;
			var flag = "0";
			if (type!="0"&&meas!="0") {
				var chr = document.getElementById("lblchr").value;
				switch(chr) {
					case "range":
						if(isNaN(t1)==true) flag="2a";
						else if(isNaN(t2)==true) flag="2b";
						else if(t3.length!=0&&isNaN(t3)==true) flag="2c";
						else if (t1.length!=0&&t2.length!=0) flag="1";
						else flag="2";
						break;
					case "spec":
						if(isNaN(t3)==true) flag="3a";
						else if (t3.length!=0) flag="1";
						else flag="3";
						break;
					case "max":
						if(isNaN(t1)==true) flag="4a";
						else if(t3.length!=0&&isNaN(t3)==true) flag="4b";
						else if (t1.length!=0) flag="1";
						else flag="4";
						break;
					case "rec":
						flag="1";
						break;
				}
				if (flag=="2") alert("Range Input Incomplete");
				else if (flag=="2a") alert("Hi Limit Must Be Numeric");
				else if (flag=="2b") alert("Lo Limit Must Be Numeric");
				else if (flag=="2c") alert("Specification value Must Be Numeric");
				else if (flag=="3") alert("No Specific value supplied");
				else if (flag=="3a") alert("Specific value Must Be Numeric");
				else if (flag=="4") alert("Not to Exceed value was not supplied");
				else if (flag=="4a") alert("Not to Exceed value Must Be Numeric");
				else if (flag=="4b") alert("Specification value Must Be Numeric");
				else if (flag=="1") {
					document.getElementById("lblrettype").value = "add";
					document.getElementById("form1").submit();
				}
				else alert("Problem with Input");
			}
			else {
				if (type=="0") alert("Measure Type not selected");
				else if (meas=="0") alert("Measurement not selected");
				else ("Problem verifying inputs");
			}
		}
		function startlog() {
			var log = document.getElementById("lbllog").value;
			if(log=="no"||log=="nodeptid") setref();
			else window.setTimeout("setref();", 300000);
		}
		function handleexit() {
		window.parent.handleexit();
		}
		function setref() {
		//handleexit();
		}
		
		