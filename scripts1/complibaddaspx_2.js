
function savdets() {
    var pchk = document.getElementById("lblpcnt").value;
    var nchk = document.getElementById("txtiorder").value;
    var cchk = document.getElementById("lblcurrp").value;
    if (parseInt(nchk) > parseInt(pchk)) {
        document.getElementById("txtiorder").value = parseInt(cchk) + 1;
        alert("New Order Value Greater Than Image Count")
    }
    else if (parseInt(nchk) == 0) {
        document.getElementById("txtiorder").value = parseInt(cchk) + 1;
        alert("New Order Value Must Be Greater Than 0")
    }
    else {
        var img = document.getElementById("lblcurrimg").value;
        if (img != "") {
            var coid = document.getElementById("lblcoid").value;
            if (coid != "") {
                document.getElementById("lblcompchk").value = "savedets";
                document.getElementById("form1").submit();
            }
            else {
                alert("No Component Selected")
            }
        }
        else {
            alert("No Image Selected")
        }
    }
}
		function addpic() {
		//complibuploaddialog
		window.parent.setref();

		var coid = document.getElementById("lblcoid").value;
		var ro = document.getElementById("lblro").value;
		if(coid!=""&&coid!="0") {
			var eReturn = window.showModalDialog("complibuploaddialog.aspx?typ=co&comid=" + coid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:700px; resizable=yes");
			if (eReturn=="ok") {
				document.getElementById("lblcompchk").value = "checkpic";
				document.getElementById("form1").submit();
			}
		}
        else {
        alert("No Component Selected")
        }
		}
		
		function getpnext() {
		var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(currp) + 1
			pcnt = parseInt(pcnt) - 1;
			if (currp<=pcnt) {
				var det = imgsarr[currp];
				var detarr = det.split(";")
				nimg = detarr[1];
				pid = detarr[0];
				document.getElementById("lblcurrimg").value= ovimgsarr[currp];
				document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
				document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
				//alert(document.getElementById("lblcurrbimg").value)
				document.getElementById("lblimgid").value= pid;
				document.getElementById("lblcurrp").value= currp;
				document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
				document.getElementById("imgco").src=nimg;
				getdets(currp + 1, pid);
			}	
		}
		function getplast() {
		var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(pcnt) - 1;
			pcnt = parseInt(pcnt) - 1;
			var det = imgsarr[currp];
			var detarr = det.split(";")
			nimg = detarr[1];
			pid = detarr[0];
			document.getElementById("lblcurrimg").value= ovimgsarr[currp];
			document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
			document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
			document.getElementById("lblimgid").value= pid;
			document.getElementById("lblcurrp").value= currp;
			document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
			document.getElementById("imgco").src=nimg;	
			getdets(currp + 1, pid);	
		}
		function getpprev() {
		var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(currp) - 1
			pcnt = parseInt(pcnt) - 1;
			if (currp>=0) {
				var det = imgsarr[currp];
				var detarr = det.split(";")
				nimg = detarr[1];
				pid = detarr[0];
				document.getElementById("lblcurrimg").value= ovimgsarr[currp];
				document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
				document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
				document.getElementById("lblimgid").value= pid;
				document.getElementById("lblcurrp").value= currp;
				document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
				document.getElementById("imgco").src=nimg;
				getdets(currp + 1, pid);
			}
				
		}
		function getpfirst() {
		var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = 0; //parseInt(currp) - 1
			pcnt = parseInt(pcnt) - 1;
			var det = imgsarr[currp];
			var detarr = det.split(";")
			nimg = detarr[1];
			pid = detarr[0];
			document.getElementById("lblcurrimg").value= ovimgsarr[currp];
			document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
			document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
			document.getElementById("lblimgid").value= pid;
			document.getElementById("lblcurrp").value= currp;
			document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
			document.getElementById("imgco").src=nimg;	
			getdets(currp + 1, pid);	
        }
        function getdets(order, pic) {
        document.getElementById("lbloldorder").value = order;
        document.getElementById("lblimgid").value = pic;
        var iorder = document.getElementById("txtiorder");
        var ord = parseInt(order) - 1;
        //alert(ord)
        var iorders = document.getElementById("lbliorders").value;
        
        var iordersarr = iorders.split(",");
        var iordersstr = iordersarr[ord];
        //alert(iordersstr)
        if(iordersstr!="") {
        iorder.value = iordersstr;
        }
        else {
        iorder.value = "";
        }
        }
       
		function FreezeScreen(msg) {
			scroll(0,0);
			var outerPane = document.getElementById('FreezePane');
			var innerPane = document.getElementById('InnerFreezePane');
			if (outerPane) outerPane.className = 'FreezePaneOn';
			if (innerPane) innerPane.innerHTML = msg;
		}
		function getstasks() {
		
		}
		function savecdets() {
		
		}
		function getport() {
		window.parent.setref();
		var co = document.getElementById("lblcoid").value;
		var fu = document.getElementById("lblfuid").value;
		var eq = document.getElementById("lbleqid").value;
		window.open("../equip/pmportfolio.aspx?eqid=" + eq + "&fuid=" + fu + "&comid=" + co);
		}
		function getbig() {
		window.parent.setref();
		var img = document.getElementById("lblcurrimg").value;
		if(img!="") {
		var currp = document.getElementById("lblcurrp").value;
		var bimgs = document.getElementById("lblbimgs").value;
		var bimgsarr = bimgs.split(",");
		var src = bimgsarr[currp];
		src = "../eqimages/" + src;
		window.open("../equip/BigPic.aspx?src=" + src)
		}
		}
		function getsgrid() {
		var comid = document.getElementById("lblcoid").value;
		//txtconame
		var comp = document.getElementById("txtconame").value;
		if(comid!=""&&comid!="0") {
		var eReturn = window.showModalDialog("complibtasks2dialog.aspx?typ=comp&coid=" + comid + "&comp=" + comp + "&date=" + Date(), "", "dialogHeight:480px; dialogWidth:850px;resizable=yes");
		if (eReturn) {
		document.getElementById("geteq").src="complibtaskview2.aspx?typ=comp&comid=" + comid;
		}
		else {
		document.getElementById("geteq").src="complibtaskview2.aspx?typ=comp&comid=" + comid;
		}
		}
		}
		function getACDiv() {
		window.parent.setref();
		//handleapp();
		var coid = document.getElementById("lblcoid").value;
		var ro = document.getElementById("lblro").value;
		var cid = "0";
		if(coid!=""&&coid!="0") {
		var eReturn = window.showModalDialog("../complib/compclassdialog.aspx?cid=" + cid + "&ro=" + ro, "", "dialogHeight:660px; dialogWidth:800px; resizable=yes");
		if (eReturn) {
			//document.getElementById("lblcompchk").value = "1";
			//document.getElementById("form1").submit();
		}
		}
		}
		function cget() {
		var coid = document.getElementById("lblcoid").value;
		var ro = document.getElementById("lblro").value;
		if(coid!=""&&coid!="0") {
		var eReturn = window.showModalDialog("cgetdialog.aspx?lib=yes&comid=" + coid, "", "dialogHeight:650px; dialogWidth:780px; resizable=yes");
		if (eReturn) {
			document.getElementById("lblcompchk").value = "1";
			document.getElementById("form1").submit();
		}
		}
		}
		function checkwarn() {
		//needs to be determined
		//will approval be necessary for master list additions
		}
		