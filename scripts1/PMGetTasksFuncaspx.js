
		
		function jumptpm(typ) {
		
		funid = document.getElementById("lblfuid").value;
		eqid = document.getElementById("lbleqid").value;
		
		if(eqid=="") {
		alert("No Equipment Record Selected")
		}
		else if(funid=="") {
		//typ = "eq"
		window.parent.tpmjump(eqid, funid, typ);
		}
		else {
		//typ = "fu"
		window.parent.tpmjump(eqid, funid, typ);
		}
		
		}
		function getappr() {
		window.parent.setref();
		var eqid = document.getElementById("lbleqid").value;
		var eReturn = window.showModalDialog("../utils/PMApprovalDialog.aspx?eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:650px; dialogWidth:860px; resizable=yes" );
		if(eReturn) {

		}
		}
		function gettpma() {
		
		var fuid = document.getElementById("lblfuid").value;
		if(fuid!=""&&fuid!="Select Function") {
		window.parent.setref();
			var eReturn = window.showModalDialog("../reports/TPMAlert.aspx?fuid=" + fuid, "", "dialogHeight:650px; dialogWidth:800px; resizable=yes");
			if (eReturn) {
			//alert(eReturn)
			var ret = eReturn.split(",");
			var comid = ret[0];
			var task = ret[1];
			document.getElementById("lblgototasks").value = "0";
			document.getElementById("lblcleantasks").value = "0";
			tl = document.getElementById("lbltl").value;
			cid = document.getElementById("lblcid").value;
			sid = document.getElementById("lblsid").value;
			did = document.getElementById("lbldept").value;
			clid = document.getElementById("lblclid").value;
			eqid = document.getElementById("lbleqid").value;
			var pmstr = document.getElementById("lbldocpmstr").value;
			var pmid = document.getElementById("lbldocpmid").value;
			chk = document.getElementById("lblchk").value;
			coid = document.getElementById("lblcoid").value;
			fuid = document.getElementById("lblfuid").value;
			tcnt = document.getElementById("lbltaskcnt").value;
			typ = document.getElementById("lbltyp").value;
			lid = document.getElementById("lbllid").value;
			window.parent.handletask("PMTaskDivFuncGrid.aspx?comid=" + comid + "&task=" + task + "&start=yes&tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&lid=" + lid + "&typ=" + typ, "ok")
			}
		}
		}
		function GetPS() {
		cid = document.getElementById("lblcid").value;
		document.getElementById("psdiv").style.top="0px";
		document.getElementById("psdiv").style.left="270px";
		document.getElementById("psdiv").style.visibility="visible";
		}
		function closePS() {
		document.getElementById("psdiv").style.visibility="hidden";
		}
		function handleps() {
		id = document.getElementById("ddps").value;
		if (isNaN(id)) {
			alert("No Plant Site Selected")
		}
		else {
		document.getElementById("lblsid").value=id;
		document.getElementById("lblpchk").value = "dfltps";
		closePS();
		document.getElementById("form1").submit();
		}
		}
		function OpenFile(newstr, type)
		{
		parent.main.location.href = "doc.aspx?file=" + newstr + "&type=" + type;
		
		}
		function GetEqDiv() {
		
		handleapp();
			sid = document.getElementById("lblsid").value;
			dept = document.getElementById("lbldept").value;
			cell = document.getElementById("lblclid").value;
			eqid = document.getElementById("lbleqid").value;
			lid = document.getElementById("lbllid").value;
			ro = document.getElementById("lblro").value;
			if(dept.length!=0||dept!=""||dept!="0") {
			window.parent.setref();
			var eReturn = window.showModalDialog("../equip/EqPopDialog.aspx?sid=" + sid + "&dept=" + dept + "&cell=" + cell + "&eqid=" + eqid + "&lid=" + lid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:500px; resizable=yes");
			if (eReturn) {
				if (eReturn=="log") {
					window.parent.handlelogout();
				}
				else if (eReturn!="no") {
					document.getElementById("lbleqid").value = eReturn;
					document.getElementById("lblpchk").value = "eq";
					document.getElementById("form1").submit();
				}
				//document.getElementById("lblpchk").value = "eq";
				//document.getElementById("form1").submit();
			}
			}
			else {
			alert("Error Retrieving Department Data")
			}
		}
		function GetEqCopy() {
		
		handleapp();
			sid = document.getElementById("lblsid").value;
			dept = document.getElementById("lbldept").value;
			cell = document.getElementById("lblclid").value;
			eqid = document.getElementById("lbleqid").value;
			lid = document.getElementById("lbllid").value;
			ro = document.getElementById("lblro").value;
			if(dept.length!=0||dept!=""||dept!="0") {
			window.parent.setref();
			var eReturn = window.showModalDialog("../equip/EQCopyMiniDialog.aspx?sid=" + sid + "&dept=" + dept + "&cell=" + cell + "&eqid=" + eqid + "&lid=" + lid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:660px; dialogWidth:660px; resizable=yes");
			if (eReturn) {
				if (eReturn=="log") {
					window.parent.handlelogout();
				}
				else if (eReturn!="no") {
					document.getElementById("lbleqid").value = eReturn;
					document.getElementById("lblpchk").value = "eq";
				}
				document.getElementById("lblpchk").value = "eq";
				document.getElementById("form1").submit();
			}
			}
			else {
			alert("Error Retrieving Department Data")
			}
		}
		function GetFuncDiv() {
		
		handleapp();
			cid = document.getElementById("lblcid").value;
			eqid = document.getElementById("lbleqid").value;
			ro = document.getElementById("lblro").value;
			if(eqid.length!=0||eqid!=""||eqid!="0") {
			window.parent.setref();
			var eReturn = window.showModalDialog("../equip/AddEditFuncDialog.aspx?cid=" + cid + "&eqid=" + eqid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:750px; resizable=yes");
			if (eReturn) {
				if (eReturn=="log") {
					window.parent.handlelogout();
				}
				else if (eReturn!="no") {
					document.getElementById("lblfuid").value = eReturn;
					document.getElementById("lblpchk").value = "func";
					
				}
				document.getElementById("lblpchk").value = "func";
				document.getElementById("form1").submit();
			}
			}
			else {
				alert("No Equipment Record Selected")
			}
		}
		function GetFuncCopy() {
		
		handleapp();
			cid = document.getElementById("lblcid").value;
			sid = document.getElementById("lblsid").value;
			did = document.getElementById("lbldept").value;
			clid = document.getElementById("lblclid").value;
			eqid = document.getElementById("lbleqid").value;
			ro = document.getElementById("lblro").value;
			if(eqid.length!=0||eqid!=""||eqid!="0") {
			window.parent.setref();
			var eReturn = window.showModalDialog("../equip/FuncCopyMiniDialog.aspx?cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:660px; dialogWidth:860px; resizable=yes");
			if (eReturn) {
				if (eReturn=="log") {
					window.parent.handlelogout();
				}
				else if (eReturn!="no") {
					document.getElementById("lblfuid").value = eReturn;
					document.getElementById("lblpchk").value = "func";	
				}
				document.getElementById("lblpchk").value = "func";	
				document.getElementById("form1").submit();
			}
			else {
				alert("No Equipment Record Selected")
			}
			}
		}
		function GetProcDiv() {
		
		handleapp();
			eqid = document.getElementById("lbleqid").value;
			var eqlst = document.getElementById("ddeq");
			var eqnum = eqlst.options[eqlst.selectedIndex].text
			if (eqid!="") {
			window.parent.setref();
			var eReturn = window.showModalDialog("../appsopt/OptDialog.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:580px; resizable=yes");
			}
			if (eReturn) {
			document.getElementById("lblpchk").value = "pr";
			document.getElementById("form1").submit();
			}
		}

		function checkpg() {
		var log = document.getElementById("lbllog").value;
		if(log=="no") {
		window.parent.handlelogout();
		}
		else {
		window.parent.setref();
		}
		var app = document.getElementById("appchk").value;
		if(app=="switch") window.parent.handleapp(app);
		chk = document.getElementById("lblpar").value;
		var chk = document.getElementById("lblpar").value;
			////alert(chk)
			if (chk=="site") {
				valu = document.getElementById("lblsid").value;
				window.parent.handleeq(chk, valu);
			}
			else if (chk=="dept") {
				valu = document.getElementById("lbldept").value;
				window.parent.handleeq(chk, valu);
			}
			else if (chk=="cell") {
				valu = document.getElementById("lblclid").value;
				window.parent.handleeq(chk, valu);
			}
			else if (chk=="eq") {
				valu = document.getElementById("lbleqid").value;
				window.parent.handleeq(chk, valu);
			}
			else if (chk=="func") {
				valu = document.getElementById("lblfuid").value;
				window.parent.handleeq(chk, valu);
			}
			else if (chk=="level") {
				valu = document.getElementById("lbltl").value;
				window.parent.handleeq(chk, valu);
			}
			else if (chk=="nopm") {
				document.getElementById("lblpar").value = "";
				AddNewPM();
			}
		var clean = document.getElementById("lblcleantasks").value;
			if(clean=="1") {
				document.getElementById("lblcleantasks").value = "2";
				window.parent.handletasks("PMTaskDivFunc.aspx?start=no")
			}
		var gototasks = document.getElementById("lblgototasks").value;
			if (gototasks=="1") {
				document.getElementById("lblgototasks").value = "0";
				document.getElementById("lblcleantasks").value = "0";
				tl = document.getElementById("lbltl").value;
				cid = document.getElementById("lblcid").value;
				sid = document.getElementById("lblsid").value;
				did = document.getElementById("lbldept").value;
				clid = document.getElementById("lblclid").value;
				eqid = document.getElementById("lbleqid").value;
				pmstr = document.getElementById("lblpmnum").value;
				doctyp = document.getElementById("lbldoctype").value;
				chk = document.getElementById("lblchk").value;
				coid = document.getElementById("lblcoid").value;
				//alert(coid)
				fuid = document.getElementById("lblfuid").value;
				tcnt = document.getElementById("lbltaskcnt").value;
				pmtyp = document.getElementById("lblpmtyp").value;
				typ = document.getElementById("lbltyp").value;
				lid = document.getElementById("lbllid").value;
				task = document.getElementById("lbltask").value;
				//alert(tcnt)
				if (tcnt=="1") {
				//alert("ok")
				window.parent.handletasks("PMTaskDivFuncGrid.aspx?comid=" + coid + "&start=yes&tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&typ=" + typ + "&lid=" + lid + "&task=" + task, "ok")
				}
				else if (tcnt=="0") {
				//alert("PMTaskDivFunc.aspx?start=yes&tl=5&pmtyp=")
				window.parent.handletasks("PMTaskDivFunc.aspx?start=yes&tl=5&pmtyp=" + pmtyp + "&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&typ=" + typ + "&lid=" + lid, "ok")
				}
				else if (tcnt=="2") {
				window.parent.handletasks("PMTaskDivFuncGrid.aspx?start=no", "dept")
				}
				else if (tcnt=="3") {
				window.parent.handletasks("PMTaskDivFuncGrid.aspx?start=no", "cell")
				}
				else if (tcnt=="4") {
				window.parent.handletasks("PMTaskDivFuncGrid.aspx?start=no", "eq")
				}
				else if (tcnt=="5") {
				window.parent.handletasks("PMTaskDivFuncGrid.aspx?start=no", "fu")
				}
			}
			 var arch = document.getElementById("lblgetarch").value;//document.getElementById("lbleqid").value!=""||
        if(arch=="yes") {
        document.getElementById("lblgetarch").value="no";
		eqid = document.getElementById("lbleqid").value;
		fuid = document.getElementById("lblfuid").value;
		did = document.getElementById("lbldept").value;
        clid = document.getElementById("lblclid").value;
        chk = document.getElementById("lblchk").value;
        lid = document.getElementById("lbllid").value;
        sid = document.getElementById("lblsid").value;
        //alert(fuid)
		window.parent.uparch(eqid, chk, did, clid, fuid, lid, sid);
			}
			
			window.setTimeout("checkit2();", 100);
		}
		function checkit2() {
			var chk2 = document.getElementById("lblpar2").value;
			if (chk2=="chk") {
				valu = document.getElementById("lblchk").value;
				//alert(valu)
				window.parent.handleeq2(chk2, valu);
			}
			else if (chk2=="dchk") {
				valu = document.getElementById("lbldchk").value;
				window.parent.handleeq2(chk2, valu);
			}
		}
		function handleapp() {
		var app = document.getElementById("appchk").value;
		if(app=="switch") window.parent.handleapp(app);
		}
		function srchloc() {
		window.parent.setref();
		//alert("Coming Soon!")
		var eReturn = window.showModalDialog("../locs/LocDialog.aspx?typ=eq", "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
			if (eReturn) {
			var ret = eReturn.split(",");
			document.getElementById("lbllid").value = ret[5];
			document.getElementById("lbltyp").value = ret[1];
			document.getElementById("lblloc").innerHTML = ret[2];
			var lid = ret[5];
			document.getElementById("lblpchk").value = "loc"

			document.getElementById("form1").submit();
				
			}
		}
		function undoloc() {
		window.parent.handlerefresh();
		}
		
		function optit() {
		var conf = confirm("Are you sure you want to Archive the Current Equipment PM Record and Convert for Optimization?")
				if(conf==true) {
					//document.getElementById("lblans").value = "5";
					//document.getElementById("form1").submit();
					window.parent.handleoptit();
				}
				else {
					alert("Action Cancelled")
				}
		}
		function archit() {
		//window.parent.handlearchit();
		document.getElementById("lblpchk").value = "archit";
		document.getElementById("form1").submit();
		}
		function getsrch() {
		var eReturn = window.showModalDialog("../equip/maxsearchdialog.aspx?srchtyp=all" + "&date=" + Date(), "", "dialogHeight:720px; dialogWidth:890px; resizable=yes");
			if (eReturn) {
				var ret = eReturn;
				//alert(ret)
				var retarr = ret.split(",");
				var eid = retarr[2];
				var sid = retarr[11];
				var did = retarr[0];
				var clid = retarr[1];
				var chk = retarr[9];
				var lid = retarr[10];
				
				var fid = retarr[4];
				var cid = retarr[12];
				if(fid!=""&&cid!="") {
				window.parent.gotopmdevco(eid, fid, cid, sid, did, clid, chk, lid);
				//gotoco(eid, fid, cid, sid, did, clid, chk, lid)
				}
				else if(fid!=""&&cid=="") {
				window.parent.gotopmdevfu(eid, fid, sid, did, clid, chk, lid);
				//gotofu(eid, fid, sid, did, clid, chk, lid)
				}
				else {
				window.parent.gotopmdeveq(eid, sid, did, clid, chk, lid);
				//gotoeq(eid, sid, did, clid, chk, lid)
				}
				/*
				gotoeq(eid, sid, did, clid, chk, lid)
				gotofu(eid, fid, sid, did, clid, chk, lid)
				gotoco(eid, fid, cid, sid, did, clid, chk, lid)
				document.getElementById("lbldid").value = retarr[0];
				document.getElementById("lblclid").value = retarr[1];
				document.getElementById("lbleqid").value = retarr[2];
				document.getElementById("tdeq").innerHTML = retarr[3];
				document.getElementById("lblfuid").value = retarr[4];
				document.getElementById("tdfunc").innerHTML = retarr[5];
				document.getElementById("tddept").innerHTML = retarr[7];
				document.getElementById("tdcell").innerHTML = retarr[8];
				document.getElementById("lbleq").value = retarr[3];
				document.getElementById("lblfu").value = retarr[5];
				document.getElementById("lbldept").value = retarr[7];
				document.getElementById("lblcell").value = retarr[8];
				document.getElementById("lblcopytyp").value = "dest";
				document.getElementById("txtnewkey").value=document.getElementById("lblselkey").value;
				document.getElementById("txtnewkey").disabled=true;
				*/
			}
			else {
				
			}
		}
		
		