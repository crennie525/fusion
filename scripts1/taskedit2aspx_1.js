
		
		function editadj(typ) {
		var fuid = document.getElementById("lblfuid").value;	
		var tid = document.getElementById("lbltaskid").value;
		var ro = document.getElementById("lblro").value;
		if (typ=="pre") {
		tid = 0;
		}
		var eReturn = window.showModalDialog("../appsman/PMSetAdjManDialog.aspx?fuid=" + fuid + "&typ=" + typ + "&tid=" + tid + "&ro=" + ro, "", "dialogHeight:520px; dialogWidth:930px; resizable=yes");
			if (eReturn) {
					if (eReturn!="log") {
						document.getElementById("form1").submit();
						//window.parent.setref();
					}
					else {
						window.parent.setref();
					}
				}
				else {
					document.getElementById("form1").submit();
				}
		}
		function DisableButton(b) {
			document.getElementById("ibToTask").className="details";
			document.getElementById("ibFromTask").className="details";
			document.getElementById("ibfromo").className="details";
			document.getElementById("ibtoo").className="details";
			document.getElementById("ibReuse").className="details";
			document.getElementById("todis").className="view";
			document.getElementById("fromdis").className="view";
			document.getElementById("todis1").className="view";
			document.getElementById("fromdis1").className="view";
			document.getElementById("fromreusedis").className="view";
			document.getElementById("form1").submit();
		}
		function FreezeScreen(msg) {
			scroll(0,0);
			var outerPane = document.getElementById('FreezePane');
			var innerPane = document.getElementById('InnerFreezePane');
			if (outerPane) outerPane.className = 'FreezePaneOn';
			if (innerPane) innerPane.innerHTML = msg;
		}
		function CheckChanges() {
		for (var i = 0; i < values.length; i++)
			{
			var elem = document.getElementById(ids[i]);
			if (elem)
			if ((elem.type == 'checkbox' || elem.type == 'radio')
                  && values[i] != elem.checked)
            var change = confirm("You have made changes to a Revised Task with a Task Status of Delete.\nAre you sure you want to save these changes?");
			else if (!(elem.type == 'checkbox' || elem.type == 'radio') &&
                  elem.value != values[i])
            var change = confirm("You have made changes to a Revised Task with a Task Status of Delete.\nAre you sure you want to save these changes?");
			 
			}
			if (change==true) {
				
				document.getElementById("lblcompchk").value="3";
				FreezeScreen('Your Data Is Being Processed...');
				document.getElementById("form1").submit();
			}
			else if (change==false) {
				//document.getElementById("lblsvchk").value = "0";
				return false;
			}
		}
		function valpgnums() {
		if (document.getElementById("lblenable").value!="1") {
		var desc = document.getElementById("txtdesc").innerHTML;
		var odesc = document.getElementById("txtodesc").innerHTML;
		if(desc.length>500) {
			alert("Maximum Lenth for Task Description is 500 Characters")
		}
		else if(odesc.length>500) {
			alert("Maximum Lenth for Task Description is 500 Characters")
		}
		else if (isNaN(document.getElementById("txtqtyo").value)) {
			alert("Original Skill Qty is Not a Number")
		}
		else if (isNaN(document.getElementById("txtqty").value)) {
			alert("Revised Skill Qty is Not a Number")
		}
		else if (isNaN(document.getElementById("txttro").value)) {
			alert("Original Skill Time is Not a Number")
		}
		else if (isNaN(document.getElementById("txttr").value)) {
			alert("Revised Skill Time is Not a Number")
		}
		else if (isNaN(document.getElementById("txtordt").value)) {
			alert("Original Running/Down Time is Not a Number")
		}
		else if (isNaN(document.getElementById("txtrdt").value)) {
			alert("Revised Running/Down Time is Not a Number")
		}
		else if (document.getElementById("ddtaskstat").value=='Delete') {
			CheckChanges();
		}
		else {
			document.getElementById("lblcompchk").value="3";
			FreezeScreen('Your Data Is Being Processed...');
			document.getElementById("form1").submit();
		}
		
		}
		}
		function GetGrid() {
		handleapp();
		document.getElementById("lblgrid").value = "yes";
		chk = document.getElementById("lblchk").value;
		cid = document.getElementById("lblcid").value;
		tid = document.getElementById("lbltaskid").value;
		funid = document.getElementById("lblfuid").value;
		comid = document.getElementById("lblco").value;
		clid = document.getElementById("lblclid").value;
		pmid = "none"//document.getElementById("lbldocpmid").value;
		if (pmid=="") pmid = "none";
		pmstr = document.getElementById("lbldocpmstr").Value
		if (pmstr=="") pmstr = "none";
		
		if (tid!=""||tid!="0") {
		window.open("../apps/GTasksFunc2.aspx?app=opt&tli=5a&funid=" + funid + "&comid=no&cid=" + cid + "&chk=" + chk + "&pmid=" + pmid + "&pmstr=" + pmstr + "&date=" + Date(), target="_top");
		}
		else {
		alert("No Task Records Selected Yet!")
		}
		}
		
		function checkit() {
		var tchk = document.getElementById("lblusetot").value;
		if(tchk=="1") {
		document.getElementById("txttro").disabled=true;
		document.getElementById("txtqtyo").disabled=true;
		document.getElementById("txtordt").disabled=true;
		}	
			
		populateArrays();
		var start = document.getElementById("lblstart").value;
		if (start=="yes1") {
			GetNavGrid();
		}
		document.getElementById("lblstart").value = "no";
		
		var grid = document.getElementById("lblgrid").value;
		if (grid=="yes") {
		 window.location.reload( false );
		document.getElementById("form1").submit();
		}
		var log = document.getElementById("lbllog").value;
		if(log=="no") setref();
		else window.setTimeout("setref();", 1205000);
		}
		function setref() {
		window.parent.setref();
		}
		function GetNavGrid() {
		handleapp();
		cid = document.getElementById("lblcid").value;
		sid = document.getElementById("lblsid").value;
		did = document.getElementById("lbldid").value;
		clid = document.getElementById("lblclid").value;
		eqid = document.getElementById("lbleqid").value;
		chk = document.getElementById("lblchk").value;
		fuid = document.getElementById("lblfuid").value;
		tcnt = document.getElementById("lblcnt").value;
		if (tcnt!="0") {
			window.parent.handletasks("PMOptTasksGrid.aspx?start=yes&tl=5&tc=0&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:800px")
			//if (eReturn) {
			//	document.getElementById("lblsvchk").value = "7";
			//	document.getElementById("pgflag").value = eReturn;
			//	document.getElementById("form1").submit();
			//}
		}
		}
	function tasklookup() {
	var chken = document.getElementById("lblenable").value;
	if(chken!="1") {
	var str = document.getElementById("txtdesc").innerHTML;
	var eReturn = window.showModalDialog("../apps/TaskLookup.aspx?str=" + str, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes");
		//alert(eReturn)
		if (eReturn!="no"&&eReturn!="~none~") {
				document.getElementById("txtdesc").innerHTML = eReturn;
		}
		else if (eReturn!="~none~") {
			document.getElementById("form1").submit();
		}
	}
	}
	function jumpto() {
		var typ = document.getElementById("ddtype").value;
		var pdm = document.getElementById("ddpt").value;
		var tid = document.getElementById("lbltaskid").value;
		var sid = document.getElementById("lblsid").value;
		var ro = document.getElementById("lblro").value;
		var eReturn = window.showModalDialog("../apps/commontasksdialog.aspx?typ=" + typ + "&pdm=" + pdm + "&tid=" + tid + "&sid=" + sid + "&ro=" + ro, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes");
			if (eReturn) {
				if(eReturn=="task") {
					document.getElementById("lblcompchk").value = "6";
					//document.getElementById("lbltaskholdid").value = eReturn;
					document.getElementById("form1").submit();
				}
			}
		}
	var popwin = "directories=0,height=500,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0,scrollbars=1";
	var poprep = "directories=0,height=600,width=900,location=0,menubar=0,resizable=1,status=0,toolbar=0,scrollbars=1";
	var poprepsm = "directories=0,height=390,width=300,location=0,menubar=0,resizable=1,status=0,toolbar=0,scrollbars=1";
	
	function getValueAnalysis() {
	tl = document.getElementById("lbltasklev").value;
	cid = document.getElementById("lblcid").value;
	sid = document.getElementById("lblsid").value;
	did = document.getElementById("lbldid").value;
	clid = document.getElementById("lblclid").value;
	eqid = document.getElementById("lbleqid").value;
	//alert(eqid)
	chk = document.getElementById("lblchk").value;
	fuid = document.getElementById("lblfuid").value;
	if (fuid!="") {
		var ht = screen.Height - 20;
		var wd = screen.Width - 20;
		var eReturn = window.showModalDialog("../reports/reportdialog.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight: 1000px; dialogWidth:2000px;resizable=yes");
		//var eReturn = window.showModalDialog("../reports/reportdialog.aspx?tl=5&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:610px; dialogWidth:610px");
		if(eReturn) {
		document.getElementById("lblsvchk").value = "7";
		document.getElementById("pgflag").value = eReturn;
		document.getElementById("form1").submit();
		}
	}
	else {
	alert("No Task Records Selected Yet!")
	}
	}
	function GetLubeDiv() {
	handleapp();
	var lock = document.getElementById("lbllock").value;
	if(lock!="1") {
	cid = document.getElementById("lblcid").value;
	ptid = document.getElementById("lbltaskid").value;
	tnum = document.getElementById("lblpg").innerHTML;
	var ro = document.getElementById("lblro").value;
	if (ptid!="") {
		window.open("../inv/optTaskLubeList.aspx?ptid=" + ptid + "&cid=" + cid + "&tnum=" + tnum + "&ro=" + ro, "repWin", popwin);
		document.getElementById("form1").submit();
		}
	}
	}
	function GetPartDiv() {
	handleapp();
	var lock = document.getElementById("lbllock").value;
	if(lock!="1") {
	cid = document.getElementById("lblcid").value;
	ptid = document.getElementById("lbltaskid").value;
	tnum = document.getElementById("lblpg").innerHTML;
	var ro = document.getElementById("lblro").value;
	if (ptid!="") {
		window.open("../inv/optTaskPartList.aspx?ptid=" + ptid + "&cid=" + cid + "&tnum=" + tnum + "&ro=" + ro, "repWin", popwin);
		document.getElementById("form1").submit();
	}
	}
	}
	function GetToolDiv() {
	var lock = document.getElementById("lbllock").value;
	if(lock!="1") {
	handleapp();
		cid = document.getElementById("lblcid").value;
		ptid = document.getElementById("lbltaskid").value;
		tnum = document.getElementById("lblpg").innerHTML;
		var ro = document.getElementById("lblro").value;
		if (ptid!="") {
		window.open("../inv/optTaskToolList.aspx?ptid=" + ptid + "&cid=" + cid + "&tnum=" + tnum + "&ro=" + ro, "repWin", popwin);
		document.getElementById("form1").submit();
		}
		}
		}
	function GetNoteDiv() {
	var lock = document.getElementById("lbllock").value;
	var ro = document.getElementById("lblro").value;
	if(lock!="1") {
	handleapp();
		ptid = document.getElementById("lbltaskid").value;
		if (ptid!="") {
		window.open("../apps/TaskNotes.aspx?ptid=" + ptid + "&ro=" + ro, "repWin", popwin);
		document.getElementById("form1").submit();
		}
		}
		}
		
		function GetRationale() {
		handleapp();
		tnum = document.getElementById("lblpg").innerHTML;
		tid = document.getElementById("lbltaskid").value;
		did = document.getElementById("lbldid").value;
		clid = document.getElementById("lblclid").value;
		eqid = document.getElementById("lbleqid").value;
		fuid = document.getElementById("lblfuid").value;
		coid = document.getElementById("lblco").value;
		sav = document.getElementById("lblsave").value;
		var ro = document.getElementById("lblro").value;
		if (tid=="") {
		alert("No Task Records Seleted!")
		}
		else if (sav=="no") {
		alert("Task Record Is Not Saved!")
		}
		else {
		var eReturn = window.showModalDialog("../appsopt/PMRationaleDialog.aspx?tnum=" + tnum + "&tid=" + tid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:730px; dialogWidth:750px; resizable=yes");
		if (eReturn) {
				document.getElementById("form1").submit();
		}
		}
		}
		
		function getsgrid() {
		handleapp();
		tid = document.getElementById("lblpg").innerHTML; //document.getElementById("lbltaskid").value;
		fuid = document.getElementById("lblfuid").value;
		cid = document.getElementById("lblcid").value;
		sid = document.getElementById("lblsid").value;
		sav = document.getElementById("lblsave").value;
		eqid = document.getElementById("lbleqid").value;
		if (tid==""||tid=="0") {
		alert("No Task Records Seleted!")
		}
		else if (sav=="no") {
		alert("Task Record Is Not Saved!")
		}
		else {
		var eReturn = window.showModalDialog("../apps/GSubDialog.aspx?sid=" + sid + "&cid=" + cid + "&tid=" + tid + "&fuid=" + fuid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:680px; dialogWidth:850px; resizable=yes");
		if (eReturn) {
				document.getElementById("lblcompchk").value = "5"
				document.getElementById("form1").submit();
		}
		}
		}
		
		function OpenFile(newstr, type)
		{
		handleapp();
		parent.parent.main.location.href = "doc.aspx?file=" + newstr + "&type=" + type;
		
		}
		function GetFuncDiv() {
		handleapp();
			cid = document.getElementById("lblcid").value;
			eqid = document.getElementById("lbleqid").value;
			var eReturn = window.showModalDialog("../equip/FuncDialog.aspx?cid=" + cid + "&eqid=" + eqid, "", "dialogHeight:250px; dialogWidth:600px; resizable=yes");
			if (eReturn) {
			document.getElementById("lblpchk").value = "func";
			document.getElementById("form1").submit();
			}
		}
		
		function GetCompDiv() {
		handleapp();
		var chken = document.getElementById("lblenable").value;
			fuid = document.getElementById("lblfuid").value;
			if(fuid.length!=0||fuid!=""||fuid!="0") {
				cid = document.getElementById("lblcid").value;
				sid = document.getElementById("lblsid").value;
				eqid = document.getElementById("lbleqid").value;
				fuid = document.getElementById("lblfuid").value;
				ptid = document.getElementById("lbltaskid").value;
				tli = document.getElementById("lbltasklev").value;
				chk = document.getElementById("lblchk").value;
				var ro = document.getElementById("lblro").value;
				var eReturn = window.showModalDialog("../equip/CompDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&tli=" + tli + "&chk=" + chk + "&sid=" + sid + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:700px; dialogWidth:800px; resizable=yes");
				if (eReturn=="log") {
					window.parent.handlelogout();
				}
				else if (eReturn) {
				document.getElementById("lblcompchk").value = "1";
				}
				document.getElementById("form1").submit();
			}
		}
		function getss() {
			cid = document.getElementById("lblcid").value;
			sid = document.getElementById("lblsid").value;
			var ro = document.getElementById("lblro").value;
			var eReturn = window.showModalDialog("../admin/pmSiteSkillsDialog.aspx?sid=" + sid + "&cid=" + cid + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:520px; dialogWidth:470px; resizable=yes");
			if (eReturn) {
				document.getElementById("lblcompchk").value = "4";
				document.getElementById("form1").submit()
			}
		}
		function GetCompCopy() {
		handleapp();
		var chken = document.getElementById("lblenable").value;
		//if(chken!="1") {
			if (ptid!="") {
				fuid = document.getElementById("lblfuid").value;
				if(fuid.length!=0||fuid!=""||fuid!="0") {
				cid = document.getElementById("lblcid").value;
				sid = document.getElementById("lblsid").value;
				did = document.getElementById("lbldid").value;
				clid = document.getElementById("lblclid").value;
				eqid = document.getElementById("lbleqid").value;
				ptid = document.getElementById("lbltaskid").value;
				tli = document.getElementById("lbltasklev").value;
				chk = document.getElementById("lblchk").value;
				usr = document.getElementById("lblusername").value;
				var ro = document.getElementById("lblro").value;
				//alert("../equip/CompCopyMiniDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&tli=" + tli + "&chk=" + chk + "&sid=" + sid + "&date=" + Date())
				//var eReturn = window.showModalDialog("../equip/CompCopyMiniDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&tli=" + tli + "&chk=" + chk + "&sid=" + sid + "&date=" + Date(), "", "dialogHeight:650px; dialogWidth:730px");
				var eReturn = window.showModalDialog("../equip/CompCopyMiniDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&tli=" + tli + "&chk=" + chk + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&usr=" + usr + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:700px; dialogWidth:880px; resizable=yes");
					if (eReturn=="log") {
						window.parent.handlelogout();
					}
						else if (eReturn) {
						document.getElementById("lblcompchk").value = "1";
					}
					document.getElementById("form1").submit()
				}
			}
		//}
		
		}
		function getPFDiv() {
		handleapp();
		var chken = document.getElementById("lblenable").value;
		if(chken!="1") {
		var eq = document.getElementById("lbleqid").value;
		var valu = document.getElementById("lbltaskid").value;
		var eReturn = window.showModalDialog("../equip/PFIntDialog.aspx?tid=" + valu + "&eqid=" + eq, "", "dialogHeight:560px; dialogWidth:669px; resizable=yes");
		}
		if (eReturn) {
				document.getElementById("txtpfint").value = eReturn;
				document.getElementById("form1").submit()
		}
		}
		function GetFailDiv() {
		var chken = document.getElementById("lblenable").value;
		if(chken!="1") {
			if (ptid!="") {
			cid = document.getElementById("lblcid").value;
			eqid = document.getElementById("lbleqid").value;
			fuid = document.getElementById("lblfuid").value;
			ptid = document.getElementById("lbltaskid").value;
			coid = document.getElementById("lblco").value;
			var ro = document.getElementById("lblro").value;
			cvalu = document.getElementById("tdcompnum").innerHTML; 
			cind = document.getElementById("lblco").value; //document.getElementById("ddcomp").value;
			if(cind!="0") {
			var eReturn = window.showModalDialog("../equip/CompFailDialog.aspx?ptid=" + ptid + "&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid + "&cvalu=" + cvalu + "&ro=" + ro, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
			}
			else {
			alert("No Component Selected")
			}
			}	
		}
		if (eReturn) {
			document.getElementById("lblcompfailchk").value = "1";
			document.getElementById("form1").submit()
		}
		}
		function controlrow(id) {
		document.getElementById("lbltabid").value = id;
		vwflg = document.getElementById(id).className;
		closerows();
		if (id=="tbeq" && vwflg == "details") {
		document.getElementById(id).className = "view";
		document.getElementById('tbeq2').className = "view";
		document.getElementById('imgeq').src = "../images/appbuttons/optdown.gif";
		}
		else if (id=="tbdtls" && vwflg == "details") {
		document.getElementById(id).className = "view";
		document.getElementById("tbdtls2").className = "view";
		document.getElementById('imgdtls').src = "../images/appbuttons/optdown.gif";
		}
		else if (id=="tbreqs" && vwflg == "details") {
		document.getElementById(id).className = "view";
		document.getElementById('imgreqs').src = "../images/appbuttons/optdown.gif";
		}
		}
		
		function closerows() {
		document.getElementById('tbeq').className = "details";
		document.getElementById('tbeq2').className = "details";
		document.getElementById('imgeq').src = "../images/appbuttons/optup.gif";
		document.getElementById('tbdtls').className = "details";
		document.getElementById('tbdtls2').className = "details";
		document.getElementById('imgdtls').src = "../images/appbuttons/optup.gif";
		document.getElementById('tbreqs').className = "details";
		document.getElementById('imgreqs').src = "../images/appbuttons/optup.gif";
		}
		function CheckDel(val) {
		
		if (val=="Delete") {
		//alert(val)
		document.getElementById("lbldel").value = "delr"
		document.getElementById("form1").submit();
		}
		}
		function GoToPMLib() {
		handleapp();
		window.open("../equip/EQCopy.aspx?date=" + Date(), target="_top");
		}
		function pmidtest() {
		alert(document.getElementById("lbldocpmid").value)
		}
		function getMeasDiv() {
		var chken = document.getElementById("lblenable").value;
		if(chken!="1") {
		var coid = document.getElementById("lblco").value;
		var valu = document.getElementById("lbltaskid").value;
		var ro = document.getElementById("lblro").value;
		var eReturn = window.showModalDialog("../utils/tmdialog.aspx?typ=pm&taskid=" + valu + "&coid=" + coid + "&ro=" + ro, "", "dialogHeight:650px; dialogWidth:550px; resizable=yes");
		}
		if (eReturn) {
			document.getElementById("form1").submit();
		}
		}
		function handleapp() {
		//var app = document.getElementById("appchk").value;
		//if(app=="switch") window.parent.rtop.handleapp(app);
		}
		function filldown(id, val) {
		if(id=='o') {
			document.getElementById('txttr').value=val;
			
			var dt = document.getElementById("tdostat").innerHTML;
			if(dt=="Down") {
					document.getElementById("txtordt").value=val;
					document.getElementById("txtrdt").value=val;
			}
		}
		else {
			
			var dt = document.getElementById("tdstat").innerHTML;
			if(dt=="Down") {
				document.getElementById("txtrdt").value=val;
			}
		}
		}
		function zerodt(id, val) {

			if(id=='o') {
				var dt = document.getElementById("ddeqstato").options[document.getElementById("ddeqstato").options.selectedIndex].text;
				if(dt!="Down") {
					document.getElementById("txtordt").value=0;
					document.getElementById("txtrdt").value="0";
				}
				else {
					document.getElementById("txtordt").value=document.getElementById("txttro").value;
					document.getElementById("txtrdt").value=document.getElementById("txttr").value;
				}
			}
			else {	
				var dt = document.getElementById("ddeqstat").options[document.getElementById("ddeqstat").options.selectedIndex].text;
				if(dt!="Down") {
					document.getElementById("txtrdt").value="0";
				}
				else {
					document.getElementById("txtrdt").value=document.getElementById("txttr").value;
				}
			}
		}
		
		