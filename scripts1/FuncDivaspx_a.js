

function savdets() {
    var pchk = document.getElementById("lblpcnt").value;
    var nchk = document.getElementById("txtiorder").value;
    var cchk = document.getElementById("lblcurrp").value;
    if (parseInt(nchk) > parseInt(pchk)) {
        document.getElementById("txtiorder").value = parseInt(cchk) + 1;
        alert("New Order Value Greater Than Image Count")
    }
    else if (parseInt(nchk) == 0) {
        document.getElementById("txtiorder").value = parseInt(cchk) + 1;
        alert("New Order Value Must Be Greater Than 0")
    }
    else {
        var img = document.getElementById("lblcurrimg").value;
        if (img != "") {
            var fuid = document.getElementById("lblfuid").value;
            if (fuid != "") {
                document.getElementById("lblpchk").value = "savedets";
                document.getElementById("form1").submit();
            }
            else {
                alert("No Function Selected")
            }
        }
        else {
            alert("No Image Selected")
        }
    }
}
		function addpic() {
		    window.parent.setref();
		    var fuid = document.getElementById("lblfuid").value;
		    var eqid = document.getElementById("lbleqid").value;
		    var ro = document.getElementById("lblro").value;
		    if(fuid!="") {
		        var eReturn = window.showModalDialog("../equip/equploadimagedialog.aspx?typ=fu&eqid=" + eqid + "&comid=&funcid=" + fuid + "&ro=" + ro + "&date=", addpicCallback, "dialogHeight:560px; dialogWidth:700px; resizable=yes");
			    if (eReturn) {
			        addpicCallback();
			    }
		    }
            else {
		        alert("No Function Selected");
		    }
		}

        function addpicCallback() {
            document.getElementById("lblpchk").value = "checkpic";
            document.getElementById("form1").submit();
        }

		function delimg() {
		var fuid = document.getElementById("lblfuid").value;
		if(fuid!="") {
			id = document.getElementById("lblimgid").value
			if(id!="") {
			document.getElementById("lblpchk").value = "delimg";
			document.getElementById("form1").submit();
			}
			else {
			alert("No Image Selected")
			}
		}
        else {
        alert("No Function Selected")
        }

        }
		function getpnext() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(currp) + 1
			pcnt = parseInt(pcnt) - 1;
			if (currp<=pcnt) {
				var det = imgsarr[currp];
				var detarr = det.split(";")
				nimg = detarr[1];
				pid = detarr[0];
				document.getElementById("lblcurrimg").value= ovimgsarr[currp];
				document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
				document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
				//alert(document.getElementById("lblcurrbimg").value)
				document.getElementById("lblimgid").value= pid;
				document.getElementById("lblcurrp").value= currp;
				document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
				document.getElementById("imgfu").src=nimg;
				getdets(currp + 1, pid);
			}
			
		}
		function getplast() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(pcnt) - 1;
			pcnt = parseInt(pcnt) - 1;
			var det = imgsarr[currp];
			var detarr = det.split(";")
			nimg = detarr[1];
			pid = detarr[0];
			document.getElementById("lblcurrimg").value= ovimgsarr[currp];
			document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
			document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
			document.getElementById("lblimgid").value= pid;
			document.getElementById("lblcurrp").value= currp;
			document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
			document.getElementById("imgfu").src=nimg;	
			getdets(currp + 1, pid);
		}
		function getpprev() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(currp) - 1
			pcnt = parseInt(pcnt) - 1;
			if (currp>=0) {
				var det = imgsarr[currp];
				var detarr = det.split(";")
				nimg = detarr[1];
				pid = detarr[0];
				document.getElementById("lblcurrimg").value= ovimgsarr[currp];
				document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
				document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
				document.getElementById("lblimgid").value= pid;
				document.getElementById("lblcurrp").value= currp;
				document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
				document.getElementById("imgfu").src=nimg;
				getdets(currp + 1, pid);
			}
			
		}
		function getpfirst() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = 0; //parseInt(currp) - 1
			pcnt = parseInt(pcnt) - 1;
			var det = imgsarr[currp];
			var detarr = det.split(";")
			nimg = detarr[1];
			pid = detarr[0];
			document.getElementById("lblcurrimg").value= ovimgsarr[currp];
			document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
			document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
			document.getElementById("lblimgid").value= pid;
			document.getElementById("lblcurrp").value= currp;
			document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
			document.getElementById("imgfu").src=nimg;	
			getdets(currp + 1, pid);
		}
		function getdets(order, pic) {
        document.getElementById("lbloldorder").value = order;
        document.getElementById("lblimgid").value = pic;
        var iorder = document.getElementById("txtiorder");
        var ord = parseInt(order) - 1;
        //alert(ord)
        var iorders = document.getElementById("lbliorders").value;
        
        var iordersarr = iorders.split(",");
        var iordersstr = iordersarr[ord];
        //alert(iordersstr)
        if(iordersstr!="") {
        iorder.value = iordersstr;
        }
        else {
        iorder.value = "";
        }

        }
		function FreezeScreen(msg) {
			scroll(0,0);
			var outerPane = document.getElementById('FreezePane');
			var innerPane = document.getElementById('InnerFreezePane');
			if (outerPane) outerPane.className = 'FreezePaneOn';
			if (innerPane) innerPane.innerHTML = msg;
		}
		function GetType(val) {
			window.parent.handleswitch(val);
		}
		function gridret() {
        var eqid = document.getElementById("lbleqid").value;
        var cid = document.getElementById("lblcid").value;
		var sid = document.getElementById("lblsid").value;
		var did = document.getElementById("lbldid").value;
		var clid = document.getElementById("lblclid").value;
		var who = document.getElementById("lblwho").value;
		window.parent.handleeq('func', 'chk', '');
        window.location = "FuncDivGrid.aspx?who=" + who + "&eqid=" + eqid + "&cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid;
    
		}
		var val;
		function GoToTasks(par, val) {
		window.parent.handletask('5');
		}
		
		function checkit() {
		var chk = document.getElementById("lbllog").value;
		if(chk=="no") {
		window.parent.doref();
		}
		else {
		window.parent.setref();
		}
		var add = document.getElementById("lbladdchk").value;
		if(add=="yes") {
		document.getElementById("lbladdchk").value="";
		var fuid = document.getElementById("lblfuid").value;
		window.parent.handleeq('func', 'chk', fuid);
		}
		var par = document.getElementById("lblpar").value;
		//alert(par)
		if (par=="func") {
		val = document.getElementById("lblfuid").value;
		var chk = "na";
		//alert(par + ", " + chk + ", " + val)
		window.parent.handleeq(par, chk, val);
		}
		var del = document.getElementById("lbldel").value;
		if (del=="1") {
		gridret();
		}
		var val = document.getElementById("lblapp").value;
		window.parent.handleswitch(val);
		
		}
		
		function setref() {
		//document.getElementById("form1").submit();
		//window.location="../NewLogin.aspx?app=none&lo=yes"
		window.parent.setref();
		}
		function getnext() {
		var curr = document.getElementById("lblcur").innerHTML;
		var cnt = document.getElementById("lblcnt").innerHTML;
		
		var fu = document.getElementById("lblparfuid").value;
		if(fu=="") fu = document.getElementById("lblfuid").value;
		
		//var fu = document.getElementById("lblfuid").value;
		cnt = parseInt(cnt);
		curr = parseInt(curr) + 1;
		if (curr<=cnt) {
		//alert("../eqimages/a-fuImg" + fu + "i" + curr + ".jpg")
		document.getElementById("lblcur").innerHTML = curr;
		document.getElementById("imgfu").src = "../eqimages/btm-fuImg" + fu + "i" + curr + ".jpg"
		}
		}
		function getprev() {
		var curr = document.getElementById("lblcur").innerHTML;
		var cnt = document.getElementById("lblcnt").innerHTML;
		
		var fu = document.getElementById("lblparfuid").value;
		if(fu=="") fu = document.getElementById("lblfuid").value;
		
		//var fu = document.getElementById("lblfuid").value;
		cnt = parseInt(cnt);
		curr = parseInt(curr) - 1;
		if (curr>=1) {
		//alert("../eqimages/a-fuImg" + fu + "i" + curr + ".jpg")
		document.getElementById("lblcur").innerHTML = curr;
		document.getElementById("imgfu").src = "../eqimages/btm-fuImg" + fu + "i" + curr + ".jpg"
		}
		}
		function getport() {
		window.parent.setref();
		var fu = document.getElementById("lblfuid").value;
		var eq = document.getElementById("lbleqid").value;
		window.open("pmportfolio.aspx?eqid=" + eq + "&fuid=" + fu + "&comid=0");
		}
		function getbig() {
		window.parent.setref();
		var img = document.getElementById("lblcurrimg").value;
		if(img!="") {
		var currp = document.getElementById("lblcurrp").value;
		var bimgs = document.getElementById("lblbimgs").value;
		var bimgsarr = bimgs.split(",");
		var src = bimgsarr[currp];
		src = "../eqimages/" + src;
		window.open("BigPic.aspx?src=" + src)
		}
		}
		function GetFuncCopy() {
		window.parent.setref();
			cid = document.getElementById("lblcid").value;
			sid = document.getElementById("lblsid").value;
			did = document.getElementById("lbldid").value;
			clid = document.getElementById("lblclid").value;
			eqid = document.getElementById("lbleqid").value;
			ro = document.getElementById("lblro").value;
			var eReturn = window.showModalDialog("../equip/FuncCopyMiniDialog.aspx?cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:660px; dialogWidth:860px; resizable=yes");
			if (eReturn) {
				if (eReturn!="no") {
					document.getElementById("lblfuid").value = eReturn;
					document.getElementById("lblpchk").value = "func";
					document.getElementById("form1").submit();
				}
			}
		}
		function GoToPMLib() {
		window.open("../equip/EQCopy.aspx?date=" + Date(), target="_top");
		}
		
		