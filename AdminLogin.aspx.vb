﻿Imports System.Data.SqlClient
Imports System.TimeZone
Public Class AdminLogin
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Private comp, compname, usrlev, usrname, dfltps, ms, sql, loggedin, grpndx, app, admin, cadm, uid, ua, apprgrp, em, appstr, ro, logout As String
    Private mup, hdr, mbg, mb, cpg, userid, islabor, rostr, issuper, isplanner, Logged_In, curlang As String
    Dim psite, lo, sessid, cursess As String
    Dim timeadjust As Integer
    Dim dr As SqlDataReader
    Dim login As New Utilities
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim appc As New AppUtils
        Dim url As String = appc.Switch
        If url <> "ok" Then
            Response.Redirect(url)
        Else

        End If

        If Not Me.IsPostBack Then
            Try
                lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
                ddlang.SelectedValue = HttpContext.Current.Session("curlang").ToString()
            Catch ex As Exception
                Dim dlang As New mmenu_utils_a
                lblfslang.Value = dlang.AppDfltLang
                ddlang.SelectedValue = dlang.AppDfltLang
            End Try
            GetFSLangs()
            Try
                logout = Request.QueryString("logout").ToString

                If logout = "yes" Then
                    Dim sess As String
                    Dim app As New AppUtils
                    sess = HttpContext.Current.Session.SessionID
                    AppUtils.LogOut(sess)
                End If
            Catch ex As Exception

            End Try
            logbad.Value = "ok"
            'Dim up As String = System.Configuration.ConfigurationManager.AppSettings("appUpdate").ToString
            lblupdate.Value = "no"
            Try
                mup = Request.QueryString("mup").ToString
            Catch ex As Exception
                mup = "0"
            End Try
            If mup = "yes" Then
                '?mup=yes&hdr=" + hdr + "&mb=" + mb + "&mbg" + mbg + "&cpg=" + cpg
                hdr = Request.QueryString("hdr").ToString
                mb = Request.QueryString("mb").ToString
                mbg = Request.QueryString("mbg").ToString
                cpg = Request.QueryString("cpg").ToString
                cpg = Replace(cpg, "../", "")
                Session("hdr") = hdr
                Session("mb") = mb
                Session("mbg") = mbg
                Response.Redirect(cpg)
            Else
                Try
                    loggedin = HttpContext.Current.Session("Logged_In").ToString
                    If loggedin <> "no" Then
                        Try
                            app = Request.QueryString("app").ToString
                        Catch ex As Exception

                        End Try
                        Try
                            lo = Request.QueryString("lo").ToString
                        Catch ex As Exception
                            lo = "no"
                        End Try

                        If loggedin = "yes" AndAlso lo = "no" Then
                            sessid = HttpContext.Current.Session.SessionID
                            cursess = HttpContext.Current.Session("sess").ToString
                            sql = "update logtrack set sessionid = '" & sessid & "' where sessionid = '" & cursess & "'"
                            login.Open()
                            login.Update(sql)
                            login.Dispose()
                            Session.Abandon()
                        Else
                            Response.Redirect("mainmenu/NewMainMenu2.aspx")
                        End If
                    End If

                Catch ex As Exception

                End Try
            End If
        Else
            Try
                Dim sess As String
                Dim app As New AppUtils
                sess = HttpContext.Current.Session.SessionID
                AppUtils.LogOut(sess)
            Catch ex As Exception

            End Try
        End If
        'btnlogin.Attributes.Add("onmouseover", "this.src='images/appbuttons/bgbuttons/ylogin.gif'")
        'btnlogin.Attributes.Add("onmouseout", "this.src='images/appbuttons/bgbuttons/blogin.gif'")

    End Sub
    Private Function CalcOffset() As Integer
        Dim srvrtime As DateTime = DateTime.Now
        Dim unitime As DateTime = srvrtime.ToUniversalTime
        Dim tz As TimeZone = TimeZone.CurrentTimeZone
        Dim srvroffset As TimeSpan = tz.GetUtcOffset(srvrtime)
        Dim t1 As Integer = srvroffset.Hours
        Dim t2 As Integer = srvroffset.Minutes
        Dim c1, c2 As Integer
        Dim useroffset As Integer
        useroffset = lblutcoffset.Value
        c1 = useroffset * 60
        c2 = useroffset
        timeadjust = useroffset - t1
        Return timeadjust

    End Function

    Protected Sub btnlogin_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnlogin.Click
        Dim intralog As New mmenu_utils_a
        Dim isintra As Integer = intralog.INTRA
        If isintra = 1 Then
            GoIntra()
        Else
            GoReg()
        End If
    End Sub
    Private Sub GoReg()
        Dim unilog As New mmenu_utils_a
        Dim isuni As Integer = unilog.UNI
        Dim dfltpsa, psitea As String
        If isuni = 0 Then
            dfltpsa = "12"
            psitea = "Practice Site"
        Else
            dfltpsa = "3"
            psitea = "LAI Sample"
        End If
        Try
            timeadjust = CalcOffset()
            Session("timeadjust") = timeadjust
        Catch ex As Exception
            timeadjust = 0
            Session("timeadjust") = timeadjust
        End Try

        curlang = lblfslang.Value
        login.Open()
        Dim u, p As String
        u = txtuid.Text
        u = Replace(u, "'", Chr(180), , , vbTextCompare)
        p = txtpass.Text
        p = login.Encrypt(p) '
        Dim cmd1 As New SqlCommand("exec usp_sysusercnt @u, @p")
        Dim param0 = New SqlParameter("@u", SqlDbType.VarChar)
        param0.Value = u
        cmd1.Parameters.Add(param0)
        Dim param01 = New SqlParameter("@p", SqlDbType.VarChar)
        param01.Value = p
        cmd1.Parameters.Add(param01)
        'sql = "select count(*) from PMSysUsers where uid = '" & u & "' and passwrd = '" & p & "'"
        Dim chk, achk As Integer
        Dim exp, expd, li As String
        chk = login.ScalarHack(cmd1)
        If chk = 0 Then
            Dim aconn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("aConn"))
            Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
            Dim appdb As String = System.Configuration.ConfigurationManager.AppSettings("custAdminDB")
            aconn.Open()
            Dim cmd As New SqlCommand
            cmd.CommandText = "exec usp_sysusercnt @u, @p"
            Dim param02 = New SqlParameter("@u", SqlDbType.VarChar)
            param02.Value = u
            cmd.Parameters.Add(param02)
            Dim param03 = New SqlParameter("@p", SqlDbType.VarChar)
            param03.Value = p
            cmd.Parameters.Add(param03)
            cmd.Connection = aconn
            cmd.CommandTimeout = 120
            achk = cmd.ExecuteScalar()
            If achk = 1 Then
                sql = "exec usp_getgrpndx1 @u, @srvr, @appdb"
                cmd.CommandText = sql
                Dim param04 = New SqlParameter("@srvr", SqlDbType.VarChar)
                param04.Value = srvr
                cmd.Parameters.Add(param04)
                Dim param05 = New SqlParameter("@appdb", SqlDbType.VarChar)
                param05.Value = appdb
                cmd.Parameters.Add(param05)
                cmd.Connection = aconn
                cmd.CommandTimeout = 120
                dr = cmd.ExecuteReader()
                'dr = login.GetRdrData(sql)
                While dr.Read
                    comp = dr.Item("compid").ToString
                    compname = dr.Item("compname").ToString
                    usrlev = dr.Item("groupname").ToString
                    cadm = dr.Item("admin").ToString
                    usrname = dr.Item("username").ToString
                    dfltps = dr.Item("dfltps").ToString
                    ms = dr.Item("multisite").ToString
                    uid = dr.Item("uid").ToString
                    exp = dr.Item("expires").ToString
                    expd = dr.Item("expiration").ToString
                    li = dr.Item("loggedin").ToString
                    ua = dr.Item("userlicense").ToString
                    psite = dr.Item("sitename").ToString
                    apprgrp = dr.Item("apprgrp").ToString
                    userid = dr.Item("userid").ToString
                    islabor = dr.Item("islabor").ToString
                    issuper = dr.Item("issuper").ToString
                    isplanner = dr.Item("isplanner").ToString

                    'appstr = dr.Item("appstr").ToString
                    'em = dr.Item("email").ToString
                End While
                dr.Close()
                Dim expflg As Integer = 0
                If exp = "Y" Then
                    Dim dat As Date = Now
                    If dat > expd Then
                        expflg = 1
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr1667", "NewLogin.aspx.vb")
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    End If
                End If
                If expflg <> 1 Then 'And logflg <> 1 Then
                    Dim ustr As String = Replace(usrname, "'", Chr(180), , , vbTextCompare)
                    Dim cap As String = System.Configuration.ConfigurationManager.AppSettings.Item("custAppName").ToString
                    Dim sessid As String = HttpContext.Current.Session.SessionID
                    Session("sess") = sessid
                    'Try
                    'sql = "declare @cnt int, @sess varchar(120) " _
                    '+ "set @sess = '" & sessid & "' " _
                    '+ "set @cnt = (select count(*) from logtrack where sessionid = @sess) " _
                    '+ "if @cnt = 0 begin insert into logtrack (userid, logintime, app, sessionid) values ('" & ustr & "', getDate() , '" & cap & "', '" & sessid & "') end " _
                    '+ "else begin set @sess = @sess + '_reenter' " _
                    '+ "insert into logtrack (userid, logintime, app, sessionid) values ('" & ustr & "', getDate() , '" & cap & "', '" & sessid & "') end "
                    'login.Update(sql)
                    'Catch ex As Exception
                    sql = "insert into logtrack (userid, logintime, app, sessionid) values ('" & ustr & "', getDate() , '" & cap & "', '" & sessid & "')"
                    login.Update(sql)
                    'End Try

                    Session("comp") = comp
                    Session("compname") = compname
                    Session("userlevel") = usrlev
                    Session("cadm") = cadm
                    Session("grpndx") = grpndx
                    Session("username") = usrname
                    Session("uid") = u
                    Session("userid") = userid
                    Session("islabor") = islabor
                    Session("issuper") = issuper
                    Session("dfltps") = dfltpsa
                    Session("psite") = psitea
                    Session("ms") = ms
                    Session("Logged_In") = "yes"
                    Logged_In = "yes"
                    Session("ua") = ua
                    Session("app") = app
                    Session("appstr") = "all"
                    Session("practice") = dfltpsa
                    Session("pmadmin") = "yes"
                    Session("email") = em
                    Session("ro") = "0"
                    Session("rostr") = "0"
                    Session("isplanner") = isplanner
                    If IsDBNull(apprgrp) Or Len(apprgrp) = 0 Then
                        Session("apprgrp") = "no"
                    Else
                        Session("apprgrp") = apprgrp
                    End If
                    'If uid <> "pmadmin1" Then
                    sql = "select isnull(hdr, '0') as 'hdr', isnull(mbg, '1') as mbg, isnull(mb, '0') as mb from mopts where uid = '" & u & "'"
                    dr = login.GetRdrData(sql)
                    While dr.Read
                        hdr = dr.Item("hdr").ToString
                        mbg = dr.Item("mbg").ToString
                        mb = dr.Item("mb").ToString
                    End While
                    dr.Close()
                    If hdr = "" Then
                        hdr = "0"
                        mbg = "1"
                        mb = "0"
                    End If
                    Session("hdr") = hdr
                    Session("mb") = mb
                    Session("mbg") = mbg
                    appstr = "all"
                    If ua = "1" Then
                        Response.Redirect("mainmenu/NewMainMenu2.aspx?sid=" + dfltpsa + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro + "&appstr=" + appstr + "&Logged_In=" + Logged_In + "&curlang=" + curlang + "&Logged_In=" + Logged_In + "&ms=" + ms + "&psite=" + psitea)
                    Else
                        Response.Redirect("security/userlic.aspx?sid=" + dfltps + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro + "&appstr=" + appstr + "&Logged_In=" + Logged_In + "&curlang=" + curlang + "&ms=" + ms + "&psite=" + psite)
                    End If
                    'Else
                    'Response.Redirect("mainmenu/NewMainMenu2.aspx")
                    'End If
                Else
                    Response.Write("Login Failed " & chk)
                End If
            Else
                'lblchk.Value = "fail"
                Dim strMessage As String = tmod.getmsg("cdstr1668", "NewLogin.aspx.vb")
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                aconn.Close()
                Exit Sub

            End If
        ElseIf chk = 1 Then
            'sql = "usp_getgrpndx1 '" & u & "'"
            Dim cmd2 As New SqlCommand("exec usp_getgrpndx1 @u")
            Dim param06 = New SqlParameter("@u", SqlDbType.VarChar)
            param06.Value = u
            cmd2.Parameters.Add(param06)
            'dr = login.GetRdrData(sql)
            dr = login.GetRdrDataHack(cmd2)

            While dr.Read
                comp = dr.Item("compid").ToString
                compname = dr.Item("compname").ToString
                usrlev = dr.Item("groupname").ToString
                cadm = dr.Item("admin").ToString
                usrname = dr.Item("username").ToString
                dfltps = dr.Item("dfltps").ToString
                ms = dr.Item("multisite").ToString
                uid = dr.Item("uid").ToString
                exp = dr.Item("expires").ToString
                expd = dr.Item("expiration").ToString
                li = dr.Item("loggedin").ToString
                ua = dr.Item("userlicense").ToString
                psite = dr.Item("sitename").ToString
                apprgrp = dr.Item("apprgrp").ToString
                appstr = dr.Item("appstr").ToString
                ro = dr.Item("readonly").ToString
                rostr = dr.Item("rostr").ToString
                userid = dr.Item("userid").ToString
                islabor = dr.Item("islabor").ToString
                issuper = dr.Item("issuper").ToString
                isplanner = dr.Item("isplanner").ToString
                'em = dr.Item("email").ToString
            End While
            dr.Close()
            Dim expflg As Integer = 0
            If exp = "Y" Then
                Dim dat As Date = Now
                If dat > expd Then
                    expflg = 1
                Else
                    Dim strMessage As String = tmod.getmsg("cdstr1669", "NewLogin.aspx.vb")
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If
            If expflg <> 1 Then 'And logflg <> 1 Then
                Dim ustr As String = Replace(usrname, "'", Chr(180), , , vbTextCompare)
                Dim cap As String = System.Configuration.ConfigurationManager.AppSettings.Item("custAppName").ToString
                Dim sessid As String = HttpContext.Current.Session.SessionID
                Session("sess") = sessid
                'Try
                'sql = "declare @cnt int, @sess varchar(120) " _
                '+ "set @sess = '" & sessid & "' " _
                '+ "set @cnt = (select count(*) from logtrack where sessionid = @sess) " _
                '+ "if @cnt = 0 begin insert into logtrack (userid, logintime, app, sessionid) values ('" & ustr & "', getDate() , '" & cap & "', '" & sessid & "') end " _
                '+ "else begin set @sess = @sess + '_reenter' " _
                '+ "insert into logtrack (userid, logintime, app, sessionid) values ('" & ustr & "', getDate() , '" & cap & "', '" & sessid & "') end "
                'login.Update(sql)
                'Catch ex As Exception
                sql = "insert into logtrack (userid, logintime, app, sessionid) values ('" & ustr & "', getDate() , '" & cap & "', '" & sessid & "')"
                login.Update(sql)
                'End Try
                'sql = "insert into logtrack (userid, logintime, app, sessionid) values ('" & ustr & "', getDate() , '" & cap & "', '" & sessid & "')"
                'login.Update(sql)
                Session("comp") = comp
                Session("compname") = compname
                Session("userlevel") = usrlev
                Session("cadm") = cadm
                Session("grpndx") = grpndx
                Session("username") = usrname
                Session("uid") = u
                Session("userid") = userid
                Session("islabor") = islabor
                Session("issuper") = issuper
                Session("dfltps") = dfltps
                Session("psite") = psite
                Session("ms") = ms
                Session("Logged_In") = "yes"
                Logged_In = "yes"
                Session("ua") = ua
                Session("app") = app
                Session("appstr") = appstr
                Session("email") = em
                Session("practice") = dfltpsa
                Session("pmadmin") = "no"
                Session("ro") = ro
                Session("rostr") = rostr
                Session("isplanner") = isplanner
                If IsDBNull(apprgrp) Or Len(apprgrp) = 0 Then
                    Session("apprgrp") = "no"
                Else
                    Session("apprgrp") = apprgrp
                End If
                sql = "select isnull(hdr, '0') as 'hdr', isnull(mbg, '1') as mbg, isnull(mb, '0') as mb from mopts where uid = '" & u & "'"
                dr = login.GetRdrData(sql)
                While dr.Read
                    hdr = dr.Item("hdr").ToString
                    mbg = dr.Item("mbg").ToString
                    mb = dr.Item("mb").ToString
                End While
                dr.Close()
                If hdr = "" Then
                    hdr = "0"
                    mbg = "1"
                    mb = "0"
                End If
                Session("hdr") = hdr
                Session("mb") = mb
                Session("mbg") = mbg

                If ua = "1" Then
                    Response.Redirect("mainmenu/NewMainMenu2.aspx?sid=" + dfltps + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro + "&appstr=" + appstr + "&Logged_In=" + Logged_In + "&curlang=" + curlang + "&Logged_In=" + Logged_In + "&ms=" + ms + "&psite=" + psite)
                Else
                    Response.Redirect("security/userlic.aspx?sid=" + dfltps + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro + "&appstr=" + appstr + "&Logged_In=" + Logged_In + "&curlang=" + curlang + "&ms=" + ms + "&psite=" + psite)
                End If
            Else
                Response.Write("Login Failed " & chk)
            End If
        Else
            Response.Write("Login Failed " & chk)
        End If
        login.Dispose()
    End Sub
    Private Sub GoIntra()
        Try
            timeadjust = CalcOffset()
            Session("timeadjust") = timeadjust
        Catch ex As Exception
            timeadjust = 0
            Session("timeadjust") = timeadjust
        End Try

        Dim chk, achk As Integer
        Dim exp, expd, li As String
        Dim ustr As String = Replace(usrname, "'", Chr(180), , , vbTextCompare)
        Dim cap As String = System.Configuration.ConfigurationManager.AppSettings.Item("custAppName").ToString
        Dim sessid As String = HttpContext.Current.Session.SessionID
        Session("sess") = sessid

        login.Open()
        Dim u, p, s, pbd As String
        u = txtuid.Text
        u = Replace(u, "'", Chr(180), , , vbTextCompare)
        p = txtpass.Text
        p = login.Encrypt(p) '

        If u = "pmadmin1" Then
            Dim bdlog As String
            bdlog = login.BDLOGIN
            If pbd = bdlog Then
                Session("comp") = "0"
                Session("compname") = "Nissan"
                Session("userlevel") = "pmadmin"
                Session("cadm") = "1"
                Session("grpndx") = "1"
                Session("username") = "LAI Administrator"
                Session("uid") = u
                Session("dfltps") = "12"
                Session("psite") = "Practice Site"
                Session("ms") = "1"
                Session("Logged_In") = "yes"
                Session("ua") = "1"
                Session("app") = ""
                Session("appstr") = "all"
                Session("email") = ""
                Session("practice") = "12"
                Session("pmadmin") = "yes"
                Session("ro") = "0"
                Session("rostr") = "0"
                Session("apprgrp") = "no"
                Session("hdr") = "0"
                Session("mb") = "1"
                Session("mbg") = "0"

                Session("userid") = "0"
                Session("islabor") = "0"
                Session("issuper") = "0"
                Session("isplanner") = "0"


                Response.Redirect("mainmenu/NewMainMenu2.aspx?sid=" + dfltps + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro + "&appstr=" + appstr + "&Logged_In=" + Logged_In + "&curlang=" + curlang + "&Logged_In=" + Logged_In + "&ms=" + ms + "&psite=" + psite)
            Else
                Dim strMessage As String = "Login Failed"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        ElseIf u = "pmadmin" Then
            sql = "select varvalue1 as compid, 'pmadmin' as uid, 'PM Administrator' as username, 1 as userlicense, " _
            + "1 as admin, 1 as groupindex, 'pmadmin' as groupname, " _
            + "varvalue3 as dfltps, 1 as multisite, varvalue2 as compname, 1 as expires, '' as expiration, " _
            + "0 as loggedin, '' as email, varvalue4 as sitename, " _
            + "'' as apprgrp, 0 as readonly, 'all' as appstr, '' as rostr " _
            + "from pmintravars where varname = 'admin' and varvalue = '" & p & "'"
            dr = login.GetRdrData(sql)
            Dim pchk As Integer = 0
            While dr.Read
                pchk += 1
                comp = dr.Item("compid").ToString
                compname = dr.Item("compname").ToString
                usrlev = dr.Item("groupname").ToString
                cadm = dr.Item("admin").ToString
                usrname = dr.Item("username").ToString
                dfltps = dr.Item("dfltps").ToString
                ms = dr.Item("multisite").ToString
                uid = dr.Item("uid").ToString
                exp = dr.Item("expires").ToString
                expd = dr.Item("expiration").ToString
                li = dr.Item("loggedin").ToString
                ua = dr.Item("userlicense").ToString
                psite = dr.Item("sitename").ToString
                apprgrp = dr.Item("apprgrp").ToString
                appstr = "all"
                appstr = dr.Item("appstr").ToString
                ro = dr.Item("readonly").ToString
                rostr = dr.Item("rostr").ToString

                userid = "0" 'dr.Item("userid").ToString
                islabor = "0" 'dr.Item("islabor").ToString
                issuper = "0" 'dr.Item("issuper").ToString
                isplanner = "0" 'dr.Item("isplanner").ToString
            End While
            dr.Close()
            If pchk = 0 Then
                Dim strMessage1 As String = "Login Failed"
                Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                Exit Sub
            Else
                ustr = Replace(usrname, "'", Chr(180), , , vbTextCompare)
                cap = System.Configuration.ConfigurationManager.AppSettings.Item("custAppName").ToString
                sessid = HttpContext.Current.Session.SessionID
                Session("sess") = sessid
                sql = "insert into logtrack (userid, logintime, app, sessionid) values ('" & u & "', getDate() , '" & cap & "', '" & sessid & "')"
                login.Update(sql)
                Session("comp") = comp
                Session("compname") = compname
                Session("userlevel") = usrlev
                Session("cadm") = cadm
                Session("grpndx") = grpndx
                Session("username") = usrname
                Session("uid") = u
                Session("dfltps") = dfltps
                Session("psite") = psite
                Session("ms") = ms
                Session("Logged_In") = "yes"
                Session("ua") = ua
                Session("app") = app
                Session("appstr") = appstr
                Session("email") = em
                Session("practice") = "12"
                Session("pmadmin") = "yes"
                Session("ro") = ro
                Session("rostr") = rostr

                Session("userid") = userid
                Session("islabor") = islabor
                Session("issuper") = issuper
                Session("isplanner") = isplanner
                If IsDBNull(apprgrp) Or Len(apprgrp) = 0 Then
                    Session("apprgrp") = "no"
                Else
                    Session("apprgrp") = apprgrp
                End If
                sql = "select isnull(hdr, '0') as 'hdr', isnull(mbg, '1') as mbg, isnull(mb, '0') as mb from mopts where uid = '" & u & "'"
                dr = login.GetRdrData(sql)
                While dr.Read
                    hdr = dr.Item("hdr").ToString
                    mbg = dr.Item("mbg").ToString
                    mb = dr.Item("mb").ToString
                End While
                dr.Close()
                If hdr = "" Then
                    hdr = "0"
                    mbg = "1"
                    mb = "0"
                End If
                Session("hdr") = hdr
                Session("mb") = mb
                Session("mbg") = mbg
                If ua = "1" Then
                    Response.Redirect("mainmenu/NewMainMenu2.aspx?sid=" + dfltps + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro + "&appstr=" + appstr + "&Logged_In=" + Logged_In + "&curlang=" + curlang + "&Logged_In=" + Logged_In + "&ms=" + ms + "&psite=" + psite)
                Else
                    Response.Redirect("security/userlic.aspx?sid=" + dfltps + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro + "&appstr=" + appstr + "&Logged_In=" + Logged_In + "&curlang=" + curlang + "&ms=" + ms + "&psite=" + psite)
                End If

            End If
        Else
            Dim cmd1 As New SqlCommand("exec usp_sysusercnt @u, @p")
            Dim param0 = New SqlParameter("@u", SqlDbType.VarChar)
            param0.Value = u
            cmd1.Parameters.Add(param0)
            Dim param01 = New SqlParameter("@p", SqlDbType.VarChar)
            param01.Value = p
            cmd1.Parameters.Add(param01)
            'sql = "select count(*) from PMSysUsers where uid = '" & u & "' and passwrd = '" & p & "'"
            ustr = Replace(usrname, "'", Chr(180), , , vbTextCompare)
            chk = login.ScalarHack(cmd1)
            If chk = 1 Then
                'sql = "usp_getgrpndx1 '" & u & "'"
                Dim cmd2 As New SqlCommand("exec usp_getgrpndx1 @u")
                Dim param06 = New SqlParameter("@u", SqlDbType.VarChar)
                param06.Value = u
                cmd2.Parameters.Add(param06)
                'dr = login.GetRdrData(sql)
                dr = login.GetRdrDataHack(cmd2)

                While dr.Read
                    comp = dr.Item("compid").ToString
                    compname = dr.Item("compname").ToString
                    usrlev = dr.Item("groupname").ToString
                    cadm = dr.Item("admin").ToString
                    usrname = dr.Item("username").ToString
                    dfltps = dr.Item("dfltps").ToString
                    ms = dr.Item("multisite").ToString
                    uid = dr.Item("uid").ToString
                    exp = dr.Item("expires").ToString
                    expd = dr.Item("expiration").ToString
                    li = dr.Item("loggedin").ToString
                    ua = dr.Item("userlicense").ToString
                    psite = dr.Item("sitename").ToString
                    apprgrp = dr.Item("apprgrp").ToString
                    appstr = dr.Item("appstr").ToString
                    ro = dr.Item("readonly").ToString
                    rostr = dr.Item("rostr").ToString
                    userid = dr.Item("userid").ToString
                    islabor = dr.Item("islabor").ToString
                    issuper = dr.Item("issuper").ToString
                    isplanner = dr.Item("isplanner").ToString
                    'em = dr.Item("email").ToString
                End While
                dr.Close()
                Dim expflg As Integer = 0
                If exp = "Y" Then
                    Dim dat As Date = Now
                    If dat > expd Then
                        expflg = 1
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr1669", "NewLogin.aspx.vb")
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    End If
                End If
                If expflg <> 1 Then 'And logflg <> 1 Then
                    ustr = Replace(usrname, "'", Chr(180), , , vbTextCompare)
                    'Try
                    'sql = "declare @cnt int, @sess varchar(120) " _
                    '+ "set @sess = '" & sessid & "' " _
                    '+ "set @cnt = (select count(*) from logtrack where sessionid = @sess) " _
                    '+ "if @cnt = 0 begin insert into logtrack (userid, logintime, app, sessionid) values ('" & ustr & "', getDate() , '" & cap & "', '" & sessid & "') end " _
                    '+ "else begin set @sess = @sess + '_reenter' " _
                    '+ "insert into logtrack (userid, logintime, app, sessionid) values ('" & ustr & "', getDate() , '" & cap & "', '" & sessid & "') end "
                    'login.Update(sql)
                    'Catch ex As Exception
                    sql = "insert into logtrack (userid, logintime, app, sessionid) values ('" & ustr & "', getDate() , '" & cap & "', '" & sessid & "')"
                    login.Update(sql)
                    'End Try
                    'sql = "insert into logtrack (userid, logintime, app, sessionid) values ('" & ustr & "', getDate() , '" & cap & "', '" & sessid & "')"
                    'login.Update(sql)
                    Session("comp") = comp
                    Session("compname") = compname
                    Session("userlevel") = usrlev
                    Session("cadm") = cadm
                    Session("grpndx") = grpndx
                    Session("username") = usrname
                    Session("uid") = u
                    Session("userid") = userid
                    Session("islabor") = islabor
                    Session("issuper") = issuper
                    Session("isplanner") = isplanner
                    Session("dfltps") = dfltps
                    Session("psite") = psite
                    Session("ms") = ms
                    Session("Logged_In") = "yes"
                    Session("ua") = ua
                    Session("app") = app
                    Session("appstr") = appstr
                    Session("email") = em
                    Session("practice") = "12"
                    Session("pmadmin") = "no"
                    Session("ro") = ro
                    Session("rostr") = rostr
                    If IsDBNull(apprgrp) Or Len(apprgrp) = 0 Then
                        Session("apprgrp") = "no"
                    Else
                        Session("apprgrp") = apprgrp
                    End If
                    sql = "select isnull(hdr, '0') as 'hdr', isnull(mbg, '1') as mbg, isnull(mb, '0') as mb from mopts where uid = '" & u & "'"
                    dr = login.GetRdrData(sql)
                    While dr.Read
                        hdr = dr.Item("hdr").ToString
                        mbg = dr.Item("mbg").ToString
                        mb = dr.Item("mb").ToString
                    End While
                    dr.Close()
                    If hdr = "" Then
                        hdr = "0"
                        mbg = "1"
                        mb = "0"
                    End If
                    Session("hdr") = hdr
                    Session("mb") = mb
                    Session("mbg") = mbg



                    If ua = "1" Then
                        Response.Redirect("mainmenu/NewMainMenu2.aspx?sid=" + dfltps + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro + "&appstr=" + appstr + "&Logged_In=" + Logged_In + "&curlang=" + curlang + "&Logged_In=" + Logged_In + "&ms=" + ms + "&psite=" + psite)
                    Else
                        Response.Redirect("security/userlic.aspx?sid=" + dfltps + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro + "&appstr=" + appstr + "&Logged_In=" + Logged_In + "&curlang=" + curlang + "&ms=" + ms + "&psite=" + psite)
                    End If
                Else
                    Response.Write("Login Failed " & chk)
                    Dim strMessage As String = "Login Failed"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
            Else
                Response.Write("Login Failed " & chk)
                Dim strMessage As String = "Login Failed"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        End If

        login.Dispose()
    End Sub
    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        'Label1.Text = axlabs.GetASPXPage("NewLogin.aspx","Label1")
        lang3470.Text = axlabs.GetASPXPage("NewLogin.aspx", "lang3470")
        lang3471.Text = axlabs.GetASPXPage("NewLogin.aspx", "lang3471")
        lang3472.Text = axlabs.GetASPXPage("NewLogin.aspx", "lang3472")
        lang3473.Text = axlabs.GetASPXPage("NewLogin.aspx", "lang3473")
        lblname.Text = axlabs.GetASPXPage("NewLogin.aspx", "lblname")
        lblpass.Text = axlabs.GetASPXPage("NewLogin.aspx", "lblpass")

        'Image2
        'btnlogin
        Dim lang As String = lblfslang.Value
        Select Case lang
            Case "eng"
                Image2.ImageUrl = "images/appimages/welcome.jpg"
                btnlogin.ImageUrl = "images/appbuttons/bgbuttons/blogin.gif"
            Case "fre"
                Image2.ImageUrl = "images/appimages/welcome_fre.jpg"
                btnlogin.ImageUrl = "images/appbuttons/bgbuttons/blogin_fre.gif"
            Case "ger"
                Image2.ImageUrl = "images/appimages/welcome_ger.jpg"
                btnlogin.ImageUrl = "images/appbuttons/bgbuttons/blogin_ger.gif"
            Case "ita"
                Image2.ImageUrl = "images/appimages/welcome_ita.jpg"
                btnlogin.ImageUrl = "images/appbuttons/bgbuttons/blogin_ita.gif"
            Case "spa"
                Image2.ImageUrl = "images/appimages/welcome_spa.jpg"
                btnlogin.ImageUrl = "images/appbuttons/bgbuttons/blogin_spa.gif"

        End Select
    End Sub

    Private Sub ddlang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlang.SelectedIndexChanged
        Dim lang As String = ddlang.SelectedValue.ToString
        Session("curlang") = lang
        lblfslang.Value = lang
        GetFSLangs()
    End Sub
End Class